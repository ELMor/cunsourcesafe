VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFactura 
   Caption         =   "Asignaci�n de RNF's a Conciertos"
   ClientHeight    =   8070
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8070
   ScaleWidth      =   11880
   Begin TabDlg.SSTab SSTab1 
      Height          =   7980
      Left            =   45
      TabIndex        =   2
      Top             =   45
      Width           =   9420
      _ExtentX        =   16616
      _ExtentY        =   14076
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Conciertos a Aplicar"
      TabPicture(0)   =   "FA00110.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tv"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Conciertos Aplicables"
      TabPicture(1)   =   "FA00110.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "tvConcAplicables"
      Tab(1).ControlCount=   1
      Begin ComctlLib.TreeView tv 
         Height          =   7575
         Left            =   45
         TabIndex        =   3
         Top             =   360
         Width           =   9300
         _ExtentX        =   16404
         _ExtentY        =   13361
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin ComctlLib.TreeView tvConcAplicables 
         Height          =   7575
         Left            =   -74955
         TabIndex        =   4
         Top             =   360
         Width           =   9300
         _ExtentX        =   16404
         _ExtentY        =   13361
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "Asignar"
      Height          =   495
      Left            =   9540
      TabIndex        =   1
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton cmdCrear 
      Caption         =   "Crear arbol"
      Height          =   495
      Left            =   9540
      TabIndex        =   0
      Top             =   90
      Visible         =   0   'False
      Width           =   1215
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   11025
      Top             =   90
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":0038
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":0352
            Key             =   "Asignado"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":066C
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":0986
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":0CA0
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":0FBA
            Key             =   "Revisado"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":12D4
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00110.frx":15EE
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private rq(1 To 19) As New rdoQuery

Public FechaProceso As String

Private varFact As New Factura
Private miConcierto As Concierto
Private ConcAplicables As Concierto
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As RNF


Private Enum TipoDeNodo
  tnRaiz = 0
  tnNodo = 1
  tnCatRaiz = 2
  tnCatFinal = 3
  tnErrorNodo = 4
End Enum

' se usa para diferenciar el key entre ramas iguales
Dim iContNodos As Integer

Private Enum TipoDeArbol
  taConcierto = 0
  taCategoria = 1
  taConcMod = 2
End Enum

Public Function pConciertos(objFactura As Factura) As Factura
    Set varFact = objFactura
    Load frmFactura
      'Set frmSeleccion.varFact = objFactura
      frmFactura.Show (vbModal)
            
    Set objFactura = varFact
      Unload frmFactura
End Function

Public Sub InitTV(codConcierto As Long)
   
'    varFact.InicializarConciertos

    MostrarConciertos
End Sub

Public Sub MostrarConciertos()
    Dim n As Node
    
    On Error Resume Next
    
    tv.ImageList = ImageList
    tvConcAplicables.ImageList = ImageList
    
    tv.Nodes.Clear
    
    For Each miConcierto In varFact.ConciertosAplicables
    
        Set n = tv.Nodes.Add(, , miConcierto.Entidad.FecInicio & miConcierto.Entidad.Ruta, miConcierto.Entidad.Name, "Concierto")
    
        Expand miConcierto.Entidad, n, tv
    
        iContNodos = 0
    Next
End Sub

Private Sub cmdAsignar_Click()
    On Error GoTo error
   
    varFact.AsignarRNFs

'    If varFact.Pacientes(1).Asistencias(1).PropuestasFactura(1).NodosFactura.Count > 0 Then
    
    Call MostrarFactura(tvConcAplicables)
        
'    If ConcAplicables.Entidad.Nodos.Count > 0 Then
 '       tvConcAplicables.Nodes.Clear
        
 '       Set n = tvConcAplicables.Nodes.Add(, , ConcAplicables.Entidad.FecInicio & ConcAplicables.Entidad.Ruta & "/", ConcAplicables.Entidad.Name, "Concierto")
        
 '       Expand varfact.Pacientes(1).Asistencias(1).propuestasfactura(1).NodosFactura, tvConcAplicables
 '   End If
    
    MostrarConciertos
Exit Sub
error:
'    Resume
    MsgBox "Error n� : " & Err.Number & " " & Err.Description
End Sub

Private Sub cmdCrear_Click()
  Call InitTV(96)
  cmdAsignar.Enabled = True
End Sub

Private Sub Form_Load()
  Me.Move 0, 0
  FechaProceso = "22/07/1999 00:00:00"

'  InitRQ
'  Call InitTV(74, "Osasunbidea")
    'Set varfact = New Factura
    CargarRNF
End Sub

Private Sub Expand(nodoPadre As Nodo, NodoArbol As Node, treev As TreeView)
    Dim Nodo As Nodo
    Dim str As String
    Dim n As Node
    Dim RNF As RNF
    Dim objTramoPrecioCantidad As TramoPrecioCantidad

    
    On Error GoTo error
    
    ' Mostar sus categorias
    For Each Nodo In nodoPadre.Categorias
        str = ""
        If Nodo.Suficiente Then
            str = "S"
        End If
        If Nodo.Necesario Then
            str = str & "N"
        End If
        Set n = treev.Nodes.Add(NodoArbol, tvwChild, , "(" & str & IIf(Nodo.PrecioRef >= 0, Nodo.PrecioRef, "D:" & Nodo.PrecioDia) & "-" & Nodo.Descuento & "Dto ) " & Nodo.Name, "CatCerrado")
        
        For Each objTramoPrecioCantidad In Nodo.TramosPrecioCantidad
            treev.Nodes.Add n, tvwChild, , "Hasta : " & objTramoPrecioCantidad.HastaCantidad & " x " & objTramoPrecioCantidad.Precio, "CatCerrado"
        Next

        
        Call Expand(Nodo, n, treev)
    Next
    
    ' Mostrar sus nodos
    For Each Nodo In nodoPadre.Nodos
        If Nodo.EsNodoFacturable Then
            Set n = treev.Nodes.Add(NodoArbol, tvwChild, , "(" & IIf(Nodo.PrecioRef >= 0, Nodo.PrecioRef, "D:" & Nodo.PrecioDia) & "-" & Nodo.Descuento & "Dto ) " & Nodo.Name & Nodo.Ruta, "Facturable")
        
        Else
            str = ""
            If Nodo.Suficiente Then
                str = "S"
            End If
            If Nodo.Necesario Then
                str = str & "N"
            End If
            Set n = treev.Nodes.Add(NodoArbol, tvwChild, , "(" & str & IIf(Nodo.PrecioRef >= 0, Nodo.PrecioRef, "D:" & Nodo.PrecioDia) & "-" & Nodo.Descuento & "Dto ) " & Nodo.Name & Nodo.Ruta, "NodoCerrado")
        
        End If
        Call Expand(Nodo, n, treev)
    Next
    
    ' Mostrar sus RNFs
    For Each RNF In nodoPadre.RNFs
        Set n = treev.Nodes.Add(NodoArbol, tvwChild, , "(" & RNF.CodRNF & ") " & RNF.CodCateg & " (" & RNF.Cantidad & " Ud)" & " " & Format(RNF.Fecha, "dd/mm/yyyy") & Nodo.Ruta, "Revisado")
    Next
    
Exit Sub
error:
    If Err.Number = 35602 Then
        MsgBox Err.Number & " " & Err.Description
        Exit Sub
    Else
        MsgBox Err.Number & " " & Err.Description
        Resume
    End If
End Sub


Private Function TipoNodo(n As Node) As TipoDeNodo
    On Error GoTo ErrorNodo
    Dim Key As String
    
    Key = Right(n.Key, Len(n.Key) - 8)
    
    ' es un nodo
    If Key Like "*/" Then
        If Key Like ".*.*.*/" Then
            ' es un nodo intermedio
            TipoNodo = tnNodo
        Else
            ' es un raiz de concierto
            TipoNodo = tnRaiz
        End If
        
    ' es una categor�a
    Else
        If Key Like "*/*.*.*." Then
            ' es categor�a intermedia o final
            TipoNodo = tnCatFinal
        Else
            ' es categor�a raiz
            TipoNodo = tnCatRaiz
        End If
    End If
    Exit Function

ErrorNodo:
    TipoNodo = tnErrorNodo
End Function


Function ContNodos() As String
    ContNodos = Format(iContNodos, "00000000")
End Function

Sub CargarRNF()
    ' deben estar ordenados por CodCateg y Fecha
    Set varFact = New Factura
    Set Paciente = New Paciente
    
    Paciente.Codigo = 33433343
    Paciente.Direccion = "C/ Olite 6, 31002 PAMPLONA"
'    paciente.FecIngreso = "31/07/1999"
    Paciente.FecNacimiento = "21/11/1971"
    Paciente.Name = "A. I."
    Set Asistencia = New Asistencia
    With Asistencia
        .Codigo = 1999093628
        .Codigo = 1999064021
        .FecIngreso = "31/07/1999"
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4635
            .CodCateg = 16
            .Fecha = "03/11/1999 00:00:00"
            .Cantidad = 2
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"

        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4270
            .CodCateg = 16
            
            .Fecha = "03/11/1999 00:00:00"
            .Cantidad = 3
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "001"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "001"
                
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4263
            .CodCateg = 12
            .Fecha = "01/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4236
            .Fecha = "01/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4674
            .Fecha = "01/11/1999 00:00:00"
            .Cantidad = 6
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4675
            .Fecha = "01/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4675
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 5
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "001"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "001"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4646
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4644
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4653
            .Fecha = "08/11/1999 00:00:00"
            .Cantidad = 2
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4653
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "001"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "001"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4653
            .Fecha = "12/11/1999 00:00:00"
            .Cantidad = 3
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "002"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "002"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4639
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 4639
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "001"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "001"
        
        Set NuevoRNF = New RNF
        With NuevoRNF
            .CodCateg = 3064
'            .CodCateg = 7802
            .Fecha = "11/11/1999 00:00:00"
            .Cantidad = 1
            .Asignado = False
        End With
        .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
        .FecInicio = "01/01/1999 00:00:00"
        .FecFin = "31/12/1999 00:00:00"
        
        .AddConcierto 81

        .AddConcierto 84
'        .AddConcierto 97
    End With
    
    Paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
    
    Set Asistencia = New Asistencia
    Asistencia.Codigo = 3456356
    Asistencia.FecIngreso = "31/07/1999"
    Set NuevoRNF = New RNF
    With NuevoRNF
        .CodCateg = 4653
        .Fecha = "11/11/1999 00:00:00"
        .Cantidad = 71
        .Asignado = False
    End With
    Asistencia.RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
    
    Set NuevoRNF = New RNF
    With NuevoRNF
        .CodCateg = 4236
        .Fecha = "01/11/1999 00:00:00"
        .Cantidad = 2
        .Asignado = False
    End With
    Asistencia.RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
    
    Set NuevoRNF = New RNF
    With NuevoRNF
        .CodCateg = 4263
'        .CodCateg = 7802
        .Fecha = "02/11/1999 00:00:00"
        .Cantidad = 3
        .Asignado = False
    End With
    Asistencia.RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & "000"
        NuevoRNF.Key = NuevoRNF.CodCateg & "-" & "000"
        
    
    Asistencia.FecInicio = "01/01/1999 00:00:00"
    Asistencia.FecFin = "30/11/1999 00:00:00"
    

'    Asistencia.AddConcierto 81, 294
'    Asistencia.AddConcierto 84
    
'        Asistencia.AddConcierto 87
'
'    Paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
    
    varFact.Pacientes.Add Paciente, CStr(Paciente.Codigo)
        
End Sub


Private Sub MostrarFactura(treev As TreeView)
    Dim Nodo As Nodo
    Dim npaciente As New Node
    Dim nAsistencia As New Node
    Dim nPropuestaFactura As New Node
    Dim nNodoFactura As New Node
    Dim str As String
    
    treev.Nodes.Clear
    
    On Error GoTo error
                
    For Each Paciente In varFact.Pacientes
        Set npaciente = treev.Nodes.Add(, , , "Paciente " & Paciente.Codigo)

        For Each Asistencia In Paciente.Asistencias
            Set nAsistencia = treev.Nodes.Add(npaciente, tvwChild, , "Asistencia " & Asistencia.Codigo)
            
            For Each PropuestaFactura In Asistencia.PropuestasFactura
                Set nPropuestaFactura = treev.Nodes.Add(nAsistencia, tvwChild, , "Propuesta de Factura : " & PropuestaFactura.Name)
                
                For Each NodoFactura In PropuestaFactura.NodosFactura
                    Set nNodoFactura = treev.Nodes.Add(nPropuestaFactura, tvwChild, , NodoFactura.Name, "Facturable")
                    
                    For Each Nodo In NodoFactura.Nodos
                        str = "-" & IIf(Nodo.FactOblig, "F-", "") & IIf(Nodo.EsNodoFacturable, "NF-", "") & IIf(Nodo.LinOblig, "L-", "")
                        Set nLineaFactura = treev.Nodes.Add(nNodoFactura, tvwChild, , str & " " & Nodo.Name & " (" & Nodo.Cantidad & IIf(Nodo.PrecioRef >= 0, "x" & Nodo.PrecioRef & " Pts)", "Dias x" & Nodo.PrecioDia & " Pts/dia)") & "-" & Nodo.Descuento & "Dto", "Facturable")
                          
                          For Each RNF In Nodo.RNFs
                            Set n = treev.Nodes.Add(nLineaFactura, tvwChild, , "(" & RNF.CodRNF & ") " & RNF.CodCateg & " (" & RNF.Cantidad & " Ud)", "NodoCerrado")
                        
                        Next RNF
                    Next Nodo
                Next NodoFactura
            Next PropuestaFactura
        Next Asistencia
    Next Paciente
                
Exit Sub
error:
    If Err.Number = 35602 Then
        MsgBox Err.Number & " " & Err.Description
        Exit Sub
    Else
        MsgBox Err.Number & " " & Err.Description
    End If
End Sub


