VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_INSALUD 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Listado de Facturas del INSALUD"
   ClientHeight    =   2205
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6120
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   6120
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3285
      TabIndex        =   17
      Top             =   1710
      Width           =   1815
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   765
      TabIndex        =   16
      Top             =   1680
      Width           =   1815
   End
   Begin VB.TextBox txtAnyo 
      BackColor       =   &H80000003&
      Height          =   315
      Left            =   5220
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   1035
      Width           =   675
   End
   Begin VB.ComboBox cboMeses 
      Height          =   315
      Left            =   1710
      TabIndex        =   13
      Top             =   1035
      Width           =   3435
   End
   Begin VB.TextBox txtDesTipEcon 
      BackColor       =   &H80000003&
      Height          =   315
      Left            =   2295
      Locked          =   -1  'True
      TabIndex        =   11
      Text            =   "INSALUD"
      Top             =   90
      Width           =   3600
   End
   Begin VB.ComboBox cboCodEntidad 
      Height          =   315
      Left            =   2295
      TabIndex        =   8
      Top             =   570
      Width           =   3615
   End
   Begin VB.TextBox txtCodEntidad 
      Height          =   315
      Left            =   1695
      TabIndex        =   7
      Top             =   570
      Width           =   495
   End
   Begin VB.TextBox txtCodTipEcon 
      BackColor       =   &H80000003&
      Height          =   315
      Left            =   1695
      Locked          =   -1  'True
      TabIndex        =   6
      Text            =   "S"
      Top             =   90
      Width           =   495
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Ambulatorio"
      Height          =   375
      Left            =   2160
      TabIndex        =   5
      Top             =   3465
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Ambulatorio"
      Height          =   375
      Left            =   270
      TabIndex        =   4
      Top             =   3465
      Width           =   1815
   End
   Begin VB.PictureBox Picture1 
      Height          =   135
      Left            =   5355
      Picture         =   "fa00157.frx":0000
      ScaleHeight     =   75
      ScaleWidth      =   165
      TabIndex        =   3
      Top             =   3285
      Visible         =   0   'False
      Width           =   225
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   330
      Left            =   4725
      TabIndex        =   2
      Top             =   3285
      Width           =   600
      _Version        =   196608
      _ExtentX        =   1058
      _ExtentY        =   582
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
   Begin VB.CommandButton cmdAmbul 
      Caption         =   "Ambulatorio"
      Height          =   375
      Left            =   2160
      TabIndex        =   1
      Top             =   3060
      Width           =   1815
   End
   Begin VB.CommandButton cmdHospit 
      Caption         =   "Hospit"
      Height          =   375
      Left            =   270
      TabIndex        =   0
      Top             =   3060
      Width           =   1815
   End
   Begin VB.TextBox txtMeses 
      Height          =   285
      Left            =   1710
      TabIndex        =   15
      Top             =   1035
      Width           =   330
   End
   Begin VB.TextBox txtCodPersona 
      Height          =   315
      Left            =   1710
      TabIndex        =   18
      Top             =   585
      Width           =   495
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Mes a Facturar:"
      Height          =   255
      Index           =   1
      Left            =   180
      TabIndex        =   12
      Top             =   1080
      Width           =   1440
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Entidad:"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   10
      Top             =   645
      Width           =   1440
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo Econ�mico:"
      Height          =   255
      Left            =   180
      TabIndex        =   9
      Top             =   165
      Width           =   1440
   End
End
Attribute VB_Name = "frm_INSALUD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ConvY = 54.0809
Const ConvX = 54.6101086

Private Type HospitForf
  NOrden As Long        'Orden para unir los arrays
  Asegurado As String   'N� Seg. Social
  Inspe As String       'Texto fijo
  Historia As Long      'N� de Historia
  Nombre As String      'Nombre del Paciente
  Fecentrada As String  'Fecha Ingreso
  fecInterv As String   'Fecha Intervenci�n.
  FecSalida As String   'Fecha Alta
  CEstancias As Double  'Cantidad Por Estancias
  CSuplementos As Double 'Cantidad Por Suplementos
  Diagnostico As String  'Diagn�stico
  Especialidad As String 'Especialidad
  Forfaits As Byte
End Type
Dim LineasForf() As HospitForf

Private Type ForfLineas
  NOrden As Long
  CodDiag As String
  Descrip As String
  CodProc As String
  DescProc As String
  ImpUnit As Double
  Precio As Double
End Type
Dim DesgForf() As ForfLineas

Private Type ForfResumen
  Cantidad As Double
  Diagnost As String
  proceso As String
  Precio As Double
  Importe As Double
End Type
Dim ResumenForf() As ForfResumen

Private Type HospitEstan
  NOrden As Long
  Asegurado As String
  Inspe As String
  Historia As Long
  Nombre As String
  Fecentrada As String
  fecInterv As String
  FecSalida As String
  NEstancias As Integer
  CEstancias As Double
  CSuplementos As Double
  Diagnostico As String
  Especialidad As String
  CodFactura As Long
End Type
Dim LineasEstan() As HospitEstan

Private Type HospitLineas
  NOrden As Long
  Cantidad As Double
  Descrip As String
  Precio As Double
  Importe As Double
  Imprimir As Boolean
End Type
Dim DesgEstanc() As HospitLineas


Private Type Ambulatorio
  NOrden As Long
  Asegurado As String
  Inspe As String
  Historia As Long
  Nombre As String
  NumServicios As Integer
  Dias As String
  PriVisita As Integer
  SucVisita As Integer
  PrimPrecio As Double
  SucPrecio As Double
  CostoTotal As Double
  Servicio As String
  Diagnostico As String
  PET As Integer
  RNM As Integer
  blnAmbu As Boolean    ' Indica si ha de mostrarse en la factura Ambulatoria
End Type
Dim LineasAmbul() As Ambulatorio

Private Type DegloseAmbul
  NOrden As Long
  Cantidad As Double
  Descrip As String
  Precio As Double
  Importe As Double
  Dias As String
  PET As Integer
  RNM As Integer
End Type
Dim DesgAmbul() As DegloseAmbul

Private Type CabAmbul
  Cantidad As Double
  Descripcion As String
  Precio As Double
  Moneda As String
  Importe As Double
  orden As Integer
  PET As Integer
  RNM As Integer
End Type
Dim CabeceraAmbul() As CabAmbul

Private Type CabHospit
  Cantidad As Double
  Clave1 As String
  Clave2 As String
  Precio As Double
  Moneda As String
  Importe As Double
End Type
Dim CabeceraHospit() As CabHospit


Private Type LineasMadrid
  orden As Integer        ' \
  Asegurado As String     '  |
  Inspe As String         '   > Comunes a los cuatro
  Historia As Long        '  |  tipos de facturas
  Nombre As String        ' /
  NumServ As Integer      ' \
  DiasServ As String      '  |
  PriVisit As Integer     '  |
  SucVisit As Integer     '   > Factura realizada
  PriPrecio As Double     '  |  por Consultas
  SucPrecio As Double     '  |      ---------
  Costo As Double         ' /
  FecEnt As String        ' \
  FecInt As String        '  |
  FecSal As String        '   \ Factura realizada
  Numero As Single        '   / por Estancias y Forfaits (en este caso no hay n�mero)
  EstCosto As Double      '  |      ---------
  SupCosto As Double      ' /
  Cantidad As Single      ' \
  Descrip As String       '  \
  Precio As Double        '  /  Desglose de las lineas
  Importe As Double       ' /   --------
  Servicio As String      '     (Comun a Consultas y Desglose)
  Especialidad As String  '     (Estancias)
  Diagnostico As String   '     (Comun a todas las facturas)
  TipoLinea As Integer    '     Indica si es forfait, estancia, consulta o resto
  Forfaits As Integer     '     N�mero de forfaits de una persona
  CodDiag As String       '\
  DescDiag As String      ' \   Diagnostico y procedimiento
  CodProc As String       ' /   del forfait aplicado
  DesProc As String       '/        -------
End Type
Dim LinMadrid() As LineasMadrid

Private Type Cabecera
  orden As Integer
  Cantidad As Single
  Desc1 As String
  Desc2 As String
  Precio As Single
  Moneda As String
  Importe As Double
  Tipo As Integer '1-> Formato Forfaits 2-> Estancias 3-> Resto
End Type
Dim MadridCab() As Cabecera

Dim TotEstancias As Long
Dim PrecioEstanc As Double
Dim ImporteEstanc As Double
Dim ImporteCitost As Double
Dim ImporteProtesis As Double
Dim ImporteHemoderivados As Double
Dim ProtesisForfaits As Double

Dim PacEstancias As Boolean
Dim Hospitalizados As Boolean
Dim Ambulatorios As Boolean
Dim PET As Boolean
Dim RNM As Boolean
Dim Madrid As Boolean

'N�mero de las facturas
Dim FacturaAmbu As String
Dim FacturaEstanc As String
Dim FacturaForf As String
Dim FacturaPET As String

Dim QryEstancias As New rdoQuery
Dim QryForfaits As New rdoQuery
Dim QryAmbulatorios As New rdoQuery
Dim QryMadrid As New rdoQuery

Dim QryEst(1 To 10) As New rdoQuery
Dim QryFor(1 To 10) As New rdoQuery
Dim QryAmb(1 To 10) As New rdoQuery
Dim QryMad(1 To 10) As New rdoQuery


Dim Copias As Integer

'Campos comunes a varias lineas de madrid
Dim Asegurado As String
Dim Historia As Long
Dim Nombre As String
Dim Servicio As String
Dim Especia As String
Dim Diagnostico As String
Dim FechaEnt As String
Dim FechaSal As String


Sub AmbulDetalle()
Dim Texto As String

  With vsPrinter1
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .Action = paStartDoc
    .MarginTop = 15 * ConvX
    .MarginLeft = 25 * ConvY
    .MarginRight = 20 * ConvY
    
    'CABECERA DE LA FACTURA
    .CurrentX = 25 * ConvY
    Texto = "CENTRO SANITARIO CL�NICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "HOJA NUM."
    .Text = Texto
    .Paragraph = ""
    '///////////////////////////////////////////////////////////////////
    ' ******************CORREGIDO POR PATXI ***************************
    'Texto = "2"
    Texto = vsPrinter1.CurrentPage + 1
    '******************************************************************
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    
    
    .CurrentX = 265 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .CurrentX = 85 * ConvY
    Texto = "FACTURACION DEL MES DE DICIEMBRE/99"
    .Text = Texto
    .CurrentX = 185 * ConvY
    Texto = "ENTIDAD HU"
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    .CurrentX = 25 * ConvY
    Texto = "NOTA A CARGO DEL INSALUD DE HUESCA, POR CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO, " & _
            "EN EL MES DE NOVIEMBRE / 99, DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS " & _
            "CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .CurrentY = .CurrentY - 200
    .CurrentX = 225 * ConvY
    Texto = "REFERENCIA N. 1610002/99"
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .BrushStyle = 1
    .DrawRectangle 24 * ConvY, 34 * ConvX, 278 * ConvY, 43 * ConvX
    .CurrentX = 25 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 130 * ConvY
    Texto = "N.SERVICIOS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "N.VISITAS"
    .Text = Texto
    .CurrentX = 187 * ConvY
    Texto = "P R E C I O"
    .Text = Texto
    .CurrentX = 216 * ConvY
    Texto = "COSTO"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "SERVICIO"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "OR"
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "INSPE"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 132 * ConvY
    Texto = "NUM  DIAS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "PRI    SUC"
    .Text = Texto
    .CurrentX = 181 * ConvY
    Texto = "PRIMER SUCESIVA"
    .Text = Texto
    .CurrentX = 217 * ConvY
    Texto = "TOTAL"
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "CL DENOMIN"
    .Text = Texto
    Texto = "DIAGNOSTICO"
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    Texto = "**********"
    .CurrentX = 57 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "ORTOPEDIA"
    .Text = Texto
    .Paragraph = ""
    Texto = "01"
    .CurrentX = 30 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "2200024690"
    .CurrentX = 57 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "SUBD"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "BERNARD,ALBERO,MARIA ROSARIO"
    .Text = Texto
    Texto = "1"
    .CurrentX = 137 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "27"
    .CurrentX = 149 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "01"
    .CurrentX = 160 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "02"
    .CurrentX = 170 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "11.727"
    .CurrentX = 192 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "0"
    .CurrentX = 212 * ConvY - .TextWidth(Texto)
    .Text = Texto
    Texto = "11.727"
    .CurrentX = 228 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "23 TRATAMIE"
    .Text = Texto
    Texto = "C.O.T."
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    
    .Action = paEndDoc
  End With
End Sub


Function AsignarOrden(Descripcion As String) As Integer
Dim orden As Integer

  orden = 0
  Select Case UCase(Descripcion)
  Case "PRIMERA CONSULTA"
    orden = 1
  Case "REVISION"
    orden = 2
  Case "ASISTENCIA URGENCIAS"
    orden = 3
  Case "SESION QUIMIOTERAPIA"
    orden = 4
  Case "ECOGRAFIA ABDOMINAL"
    orden = 5
  Case "ECOGRAFIA GINECOLOGICA"
    orden = 6
  Case "ANGIOGRAFIA DIGITAL"
    orden = 7
  Case "PRUEBAS INMUNOL�GICAS"
    orden = 8
  Case "SESION ACELERADOR LINEAL"
    orden = 9
  Case "ASISTENCIA ONCOLOGICA"
    orden = 10
  Case "REHABILITACION"
    orden = 11
  Case "HEMODIALISIS"
    orden = 12
  Case "TOMOGRAFIA AXIAL COMPUTERIZADA"
    orden = 13
  Case "LITOTRICIA"
    orden = 14
  Case "TRANSPLANTE RENAL"
    orden = 15
  Case "SUPLEMENTO BICARBONATO"
    orden = 16
  Case "GAMMAGRAFIA BASAL"
    orden = 17
  Case "GAMMAGRAFIA TRAS-T.3 (O TSH)"
    orden = 18
  Case "RASTRAO CORPORAL TOTAL"
    orden = 19
  Case "GAMMAGRAF.SEC. GLANDULAS SALIVARES"
    orden = 20
  Case "GAMMAGRAF�A SECUENCIAL TRAS EST�MULO"
    orden = 21
  Case "LOCALIZACI�N GAMMAGR�FICA"
    orden = 22
  Case "ANGIOGAMMAGRAF�A HEPOTOESPL�NICA"
    orden = 23
  Case "GAMMAGRAF�A EST�TICA-HEP�TICA"
    orden = 24
  Case "GAM.HEPATOBIL.(HEPATOCOLANGIOGRAF.)"
    orden = 25
  Case "RENOGRAMA ISOT�PICO BASAL"
    orden = 26
  Case "FLUJO PLASMATICORENAL"
    orden = 27
  Case "ESTUDIO GAMMAGR�FICO PULMONAR"
    orden = 28
  Case "EST. DE PERFUSI�N CONTALIO(ESFUERZO-REPOSO YOTRO RADIO, F�RMACOS)"
    orden = 29
  Case "EST.SECUENC.ANGIOGAM.GRAF.CEREBRAL"
    orden = 30
  Case "ESTUDIO CUERPO COMPLETO"
    orden = 31
  Case "GAMMAGRAF�A ZOMAS LOCALIZADAS"
    orden = 32
  Case "VOLEMIA"
    orden = 33
  Case "ERECTROCIN�TICA"
    orden = 34
  Case "FERROCIN�TICA"
    orden = 35
  Case "EST. FEBOGR�FICO MIEMBROS INFERIORES"
    orden = 36
  Case "OTROS ESTUDIOS FLEBOGR�FICOS"
    orden = 37
  Case "EST.GAMMAGR�FICO DE CUERPO CON GALIO"
    orden = 38
  Case "GAMM.SELECTIVA CON GALIO"
    orden = 39
  Case "R.I.A."
    orden = 40
  Case "TRAT.COMPLETO DE HIPERTIROIDISMO CON RADIO-YODO."
    orden = 41
  Case "TRATAMIENTO INTRAARTICULARES (YTRIO-90)"
    orden = 42
  End Select
  
  If orden <> 0 Then
    AsignarOrden = orden
  Else
    AsignarOrden = 32000
  End If
End Function

Sub CabAmbMadrid(orden As Integer)
Dim Texto As String
Dim Pagina As Integer
  With vsPrinter1
    .Preview = False
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .Action = paStartDoc
    .MarginTop = 15 * ConvX
    .MarginLeft = 25 * ConvY
    .MarginRight = 20 * ConvY
    
    'CABECERA DE LA FACTURA DEL FORMATO AMBULATORIOS DE MADRID
    .CurrentX = 25 * ConvY
    Texto = "CENTRO SANITARIO CL�NICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "HOJA NUM."
    .Text = Texto
    .Paragraph = ""
    Texto = Pagina
    .CurrentX = 265 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .CurrentX = 85 * ConvY
    Texto = "FACTURACION DEL MES DE " & UCase(Me.cboMeses.Text)
    .Text = Texto
    .CurrentX = 185 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    .CurrentX = 25 * ConvY
    Texto = "NOTA A CARGO DEL INSALUD DE MADRID, POR LA ASISTENCIA EN ESTE CENTRO DE LOS PACIENTES QUE " & _
            "SE DETALLAN ACOMPA�ANDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .CurrentY = .CurrentY - 200
    .CurrentX = 225 * ConvY
    Texto = "REFERENCIA N. " & orden & "/2000"
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 130 * ConvY
    Texto = "N.SERVICIOS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "N.VISITAS"
    .Text = Texto
    .CurrentX = 187 * ConvY
    Texto = "P R E C I O"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "SERVICIO"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "OR"
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "INSPE"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 132 * ConvY
    Texto = "NUM  DIAS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "PRI   SUC"
    .Text = Texto
    .CurrentX = 181 * ConvY
    Texto = "PRIMER SUCESIVA"
    .Text = Texto
    .CurrentX = 217 * ConvY
    Texto = "TOTAL"
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "CL DENOMIN"
    .Text = Texto
    Texto = "DIAGNOSTICO"
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    While .CurrentX < 277 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
  End With
End Sub

Sub CabeceraAmbulatorio()
Dim Texto As String

  With vsPrinter1
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .Action = paStartDoc
    .MarginTop = 15 * ConvX
    .MarginLeft = 25 * ConvY
    .MarginRight = 20 * ConvY
    
    'CABECERA DE LA FACTURA
    .CurrentX = 25 * ConvY
    Texto = "CENTRO SANITARIO CL�NICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "HOJA NUM."
    .Text = Texto
    '///////////////////////////////////////////////////////////////////
    ' ******************CORREGIDO POR PATXI ***************************
    'Texto = "2"
    Texto = vsPrinter1.CurrentPage + 1
    .CurrentX = 265 * ConvY - .TextWidth(Texto)
    .Text = Texto
    '******************************************************************
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACION DEL MES DE " & UCase(Me.cboMeses.Text)
    .Text = Texto
    .CurrentX = 185 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    .CurrentX = 25 * ConvY
    Texto = "NOTA A CARGO DEL " & UCase(Me.cboCodEntidad.Text) & " , POR CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO, " & _
            "EN EL MES DE " & UCase(Me.cboMeses) & ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS " & _
            "CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .CurrentY = .CurrentY - 200
    .CurrentX = 225 * ConvY
    Texto = "REFERENCIA N. " & FacturaAmbu
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 130 * ConvY
    Texto = "N.SERVICIOS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "N.VISITAS"
    .Text = Texto
    .CurrentX = 187 * ConvY
    Texto = "P R E C I O"
    .Text = Texto
    .CurrentX = 216 * ConvY
    Texto = "COSTO"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "SERVICIO"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "OR"
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "INSPE"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 132 * ConvY
    Texto = "NUM  DIAS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "PRI    SUC"
    .Text = Texto
    .CurrentX = 181 * ConvY
    Texto = "PRIMER SUCESIVA"
    .Text = Texto
    .CurrentX = 217 * ConvY
    Texto = "TOTAL"
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "CL DENOMIN"
    .Text = Texto
    Texto = "DIAGNOSTICO"
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    While .CurrentX < 277 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
        
  End With
End Sub
Sub CabeceraPET()
Dim Texto As String

  With vsPrinter1
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .Action = paStartDoc
    .MarginTop = 15 * ConvX
    .MarginLeft = 25 * ConvY
    .MarginRight = 20 * ConvY
    
    'CABECERA DE LA FACTURA
    .CurrentX = 25 * ConvY
    Texto = "CENTRO SANITARIO CL�NICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "HOJA NUM."
    .Text = Texto
    '///////////////////////////////////////////////////////////////////
    ' ******************CORREGIDO POR PATXI ***************************
    'Texto = "2"
    Texto = vsPrinter1.CurrentPage + 1
    .CurrentX = 265 * ConvY - .TextWidth(Texto)
    .Text = Texto
    '******************************************************************
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACION DEL MES DE " & UCase(Me.cboMeses.Text)
    .Text = Texto
    .CurrentX = 185 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    .CurrentX = 25 * ConvY
    Texto = "NOTA A CARGO DEL " & UCase(Me.cboCodEntidad.Text) & " , POR CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO, " & _
            "EN EL MES DE " & UCase(Me.cboMeses) & ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS " & _
            "CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .CurrentY = .CurrentY - 200
    .CurrentX = 225 * ConvY
    Texto = "REFERENCIA N. " & FacturaPET
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 130 * ConvY
    Texto = "N.SERVICIOS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "N.VISITAS"
    .Text = Texto
    .CurrentX = 187 * ConvY
    Texto = "P R E C I O"
    .Text = Texto
    .CurrentX = 216 * ConvY
    Texto = "COSTO"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "SERVICIO"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "OR"
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "INSPE"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 132 * ConvY
    Texto = "NUM  DIAS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "PRI    SUC"
    .Text = Texto
    .CurrentX = 181 * ConvY
    Texto = "PRIMER SUCESIVA"
    .Text = Texto
    .CurrentX = 217 * ConvY
    Texto = "TOTAL"
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "CL DENOMIN"
    .Text = Texto
    Texto = "DIAGNOSTICO"
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    While .CurrentX < 277 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
        
  End With
End Sub
Sub CabeceraRNM()
Dim Texto As String

  With vsPrinter1
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .Action = paStartDoc
    .MarginTop = 15 * ConvX
    .MarginLeft = 25 * ConvY
    .MarginRight = 20 * ConvY
    
    'CABECERA DE LA FACTURA
    .CurrentX = 25 * ConvY
    Texto = "CENTRO SANITARIO CL�NICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "HOJA NUM."
    .Text = Texto
    '///////////////////////////////////////////////////////////////////
    ' ******************CORREGIDO POR PATXI ***************************
    'Texto = "2"
    Texto = vsPrinter1.CurrentPage + 1
    .CurrentX = 265 * ConvY - .TextWidth(Texto)
    .Text = Texto
    '******************************************************************
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACION DEL MES DE " & UCase(Me.cboMeses.Text)
    .Text = Texto
    .CurrentX = 185 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    .CurrentX = 25 * ConvY
    Texto = "NOTA A CARGO DEL " & UCase(Me.cboCodEntidad.Text) & " , POR CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO, " & _
            "EN EL MES DE " & Me.cboMeses & ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS " & _
            "CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .CurrentY = .CurrentY - 200
    .CurrentX = 225 * ConvY
    Texto = "REFERENCIA N."
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 130 * ConvY
    Texto = "N.SERVICIOS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "N.VISITAS"
    .Text = Texto
    .CurrentX = 187 * ConvY
    Texto = "P R E C I O"
    .Text = Texto
    .CurrentX = 216 * ConvY
    Texto = "COSTO"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "SERVICIO"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "OR"
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "INSPE"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 132 * ConvY
    Texto = "NUM  DIAS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "PRI    SUC"
    .Text = Texto
    .CurrentX = 181 * ConvY
    Texto = "PRIMER SUCESIVA"
    .Text = Texto
    .CurrentX = 217 * ConvY
    Texto = "TOTAL"
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "CL DENOMIN"
    .Text = Texto
    Texto = "DIAGNOSTICO"
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    While .CurrentX < 277 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
        
  End With
End Sub
Sub CabEstMadrid(orden As Integer)
Dim Texto As String
Dim Pagina As Integer
Dim FacturaMadrid As Integer
  With vsPrinter1
    .Orientation = orLandscape
    .Action = paStartDoc
    'CABECERA DE LA FACTURA DE ESTANCIAS DE MADRID
    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    Texto = Pagina
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACION DEL MES DE " & UCase(Me.cboMeses)
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL INSALUD DE MADRID POR LA ASISTENCIA EN ESTE CENTRO, DE LOS PACIENTES QUE SE " & _
            "DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & orden & "/2000"
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    'CABECERA DE LAS LINEAS
    .CurrentX = 20 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "N."
    .Text = Texto
    .FontUnderline = True
    .CurrentX = 125 * ConvY
    Texto = "F.ENTRA"
    .Text = Texto
    .CurrentX = 145 * ConvY
    Texto = "F.INT"
    .Text = Texto
    .CurrentX = 160 * ConvY
    Texto = "F.SALIDA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "N"
    .Text = Texto
    .CurrentX = 200 * ConvY
    Texto = "C O S T O S"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "ESPECIALI"
    .Text = Texto
    .Paragraph = ""
    .FontUnderline = False
    .CurrentX = 20 * ConvY
    Texto = "ORD"
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 55 * ConvY
    Texto = "INSP."
    .Text = Texto
    .CurrentX = 68 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 125 * ConvY
    Texto = "MM.DD.H"
    .Text = Texto
    .CurrentX = 144 * ConvY
    Texto = "MM-DD"
    .Text = Texto
    .CurrentX = 162 * ConvY
    Texto = "DD-HH"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "EST"
    .Text = Texto
    .CurrentX = 192 * ConvY
    Texto = "ESTANCIAS"
    .Text = Texto
    .CurrentX = 212 * ConvY
    Texto = "SUPLEM."
    .Text = Texto
    .CurrentX = 230 * ConvY
    Texto = "DIAGNOSTICO"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "CL.DENOM"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 19 * ConvY
    While .CurrentX < 274 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
  End With
End Sub

Sub CabForfMadrid(orden As Integer)
Dim Texto As String
Dim Pagina As Integer
Dim FacturaMadrid As String

  With vsPrinter1
    'CABECERA DE LA FACTURA CUANDO TENGA FORMATO DE FORFAITS
    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    Texto = Pagina
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACI�N DEL MES DE " & UCase(Me.cboMeses.Text)
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL INSALUD DE MADRID, POR LA ASISTENCIA EN ESTE CENTRO, DE LOS PACIENTES " & _
            "QUE SE DETALLAN, ACOMPA�ANDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE CUMPLIMENTADOS"
    .Text = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & orden & "/2000"
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    'CABECERA DE LAS LINEAS
    .CurrentX = 20 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "N."
    .Text = Texto
    .FontUnderline = True
    .CurrentX = 125 * ConvY
    Texto = "F.ENTRA"
    .Text = Texto
    .CurrentX = 145 * ConvY
    Texto = "F.INT"
    .Text = Texto
    .CurrentX = 160 * ConvY
    Texto = "F.SALIDA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "N"
    .Text = Texto
    .CurrentX = 200 * ConvY
    Texto = "C O S T O S"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "ESPECIALI"
    .Text = Texto
    .Paragraph = ""
    .FontUnderline = False
    .CurrentX = 20 * ConvY
    Texto = "ORD"
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 55 * ConvY
    Texto = "INSP."
    .Text = Texto
    .CurrentX = 68 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 125 * ConvY
    Texto = "MM.DD.H"
    .Text = Texto
    .CurrentX = 144 * ConvY
    Texto = "MM-DD"
    .Text = Texto
    .CurrentX = 162 * ConvY
    Texto = "DD-MM"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "EST"
    .Text = Texto
    .CurrentX = 192 * ConvY
    Texto = "ESTANCIAS"
    .Text = Texto
    .CurrentX = 212 * ConvY
    Texto = "SUPLEM."
    .Text = Texto
    .CurrentX = 230 * ConvY
    Texto = "DIAGNOSTICO"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "CL.DENOM"
    .Text = Texto
    While .CurrentX <= 270 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
  End With
  
End Sub

Sub Estancias()
Dim Texto As String
Dim Insalud As New Persona
Dim Cabecera As Double
Dim Total As Double

  With vsPrinter1
    .Action = paStartDoc
    .X1 = 20 * ConvY
    .Y1 = 28 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 68 * ConvX
    .Picture = Me.Picture1.Picture
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .MarginRight = 20 * ConvY
    Cabecera = 30 * ConvX
    
    'CABECERA DE LA FACTURA
    .CurrentY = Cabecera
    .CurrentX = 55 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 55 * ConvY
    .CurrentY = .CurrentY + 200
    Texto = "C.I.F. 31-3168001-J"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "N�INSC. S.S.:31/32.361/24"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "COD.BCO.: 0049 COD.SUC.:022 N�CTA:5822-0"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "REFERENCIA N.: " & UCase(FacturaEstanc)
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "29 DE FEBRERO DEL 2000"
    .Text = Texto
    Insalud.Codigo = Me.txtCodPersona
    .MarginLeft = 180 * ConvY
    .CurrentY = Cabecera
    .CurrentX = 190 * ConvY
    Texto = UCase(Insalud.Name)
    .Text = Texto
    .CurrentX = 190 * ConvY
    .CurrentY = .CurrentY + 200
    Texto = "C.I.F.: - " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 190 * ConvY
    Texto = UCase(Insalud.Direccion)
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 190 * ConvY
    Texto = UCase(Insalud.CPPoblac)
    .Text = Texto
    .CurrentY = .CurrentY + 1000
    
    .MarginLeft = 25 * ConvY
    .TextAlign = taCenterBaseline
    .FontUnderline = True
    Texto = UCase(Insalud.Name)
    .Paragraph = Texto
    .TextAlign = taJustBaseline
    .FontUnderline = False
    .Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR LA HOSPITALIZACI�N EN " & _
            "ESTE CENTRO EN EL MES DE " & UCase(Me.cboMeses) & " DE " & Me.txtAnyo & ", DE LOS PACIENTES QUE A CONTINUACION SE " & _
            "DETALLAN, ACOMPA��NDOSE LOS CORRESPONDIENTES PARTES DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .Paragraph = ""
    'Hay estancias por lo cual imprimiremos la l�nea en la cabecera de la factura...
    If ImporteEstanc <> 0 Then
      Texto = Format(TotEstancias, "###,##0.0")
      .CurrentX = 35 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentX = 40 * ConvY
      .Text = "ESTANCIAS"
      .CurrentX = 130 * ConvY
      .Text = "A"
      Texto = Format(PrecioEstanc, "###,##0.##")
      .CurrentX = 170 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentX = 180 * ConvY
      .Text = "PESETAS"
      Texto = Format(ImporteEstanc, "###,###,##0.##")
      .CurrentX = 265 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .Paragraph = ""
    End If
    'Si hay Citost�ticos y Factores imprimiremos las l�nea en la cabecera de la factura...
    If ImporteCitost <> 0 Then
      .CurrentX = 40 * ConvY
      .Text = "CITOSTATICOS Y FACTORES"
      .CurrentX = 180 * ConvY
      .Text = "PESETAS"
      Texto = Format(ImporteCitost, "###,###,##0.##")
      .CurrentX = 265 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .Paragraph = ""
    End If
    'Si hay pr�tesis en la factura imprimiremos la l�nea de pr�tesis en la cabecera de factura...
    If ImporteProtesis <> 0 Then
      .CurrentX = 40 * ConvY
      .Text = "PROTESIS"
      .CurrentX = 180 * ConvY
      .Text = "PESETAS"
      Texto = Format(ImporteProtesis, "###,###,##0.##")
      .CurrentX = 265 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .Paragraph = ""
    End If
    'Si hay hemoderivados en la factura imprimiremos la l�nea en la cabecera de factura...
    If ImporteHemoderivados <> 0 Then
      .CurrentX = 40 * ConvY
      .Text = "HEMODERIVADOS"
      .CurrentX = 180 * ConvY
      .Text = "PESETAS"
      Texto = Format(ImporteHemoderivados, "###,###,##0.##")
      .CurrentX = 265 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .Paragraph = ""
    End If
    .CurrentX = 220 * ConvY
    While .CurrentX < 265 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .CurrentX = 180 * ConvY
    Texto = "T O T A L "
    .Text = Texto
    Total = ImporteEstanc + ImporteCitost + ImporteProtesis + ImporteHemoderivados
    Texto = Format(Total, "###,###,##0.##")
    .CurrentX = 265 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    .Paragraph = "POR EL CENTRO CONCERTADO"
    
    .Action = paEndDoc
  End With
End Sub




Sub CabeceraEstancias()
Dim Texto As String
  With vsPrinter1
    .Orientation = orLandscape
    .Action = paStartDoc
    'CABECERA DE LA FACTURA
    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    '///////////////////////////////////////////////////////////////////
    ' ******************CORREGIDO POR PATXI ***************************
    'Texto = "2"
    Texto = vsPrinter1.CurrentPage + 1
    '******************************************************************
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACI�N DEL MES DE " & UCase(Me.cboMeses)
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL INSALUD DE " & UCase(Me.cboCodEntidad.Text) & " POR LA HOSPITALIZACI�N DEL MES DE " & UCase(Me.cboMeses) & " / " & Me.txtAnyo & _
            "DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA " & _
            "DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & UCase(FacturaEstanc)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    'CABECERA DE LAS LINEAS
    .CurrentX = 20 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "N."
    .Text = Texto
    .FontUnderline = True
    .CurrentX = 125 * ConvY
    Texto = "F.ENTRA"
    .Text = Texto
    .CurrentX = 145 * ConvY
    Texto = "F.INT"
    .Text = Texto
    .CurrentX = 160 * ConvY
    Texto = "F.SALIDA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "N"
    .Text = Texto
    .CurrentX = 200 * ConvY
    Texto = "C O S T O S"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "ESPECIALI"
    .Text = Texto
    .Paragraph = ""
    .FontUnderline = False
    .CurrentX = 20 * ConvY
    Texto = "ORD"
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 55 * ConvY
    Texto = "INSP."
    .Text = Texto
    .CurrentX = 68 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 125 * ConvY
    Texto = "MM.DD.H"
    .Text = Texto
    .CurrentX = 144 * ConvY
    Texto = "MM-DD"
    .Text = Texto
    .CurrentX = 162 * ConvY
    Texto = "DD-HH"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "EST"
    .Text = Texto
    .CurrentX = 192 * ConvY
    Texto = "ESTANCIAS"
    .Text = Texto
    .CurrentX = 212 * ConvY
    Texto = "SUPLEM."
    .Text = Texto
    .CurrentX = 230 * ConvY
    Texto = "DIAGNOSTICO"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "CL.DENOM"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 19 * ConvY
    While .CurrentX < 275 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
  End With
  
End Sub

Sub CabeceraForfaits()
Dim Texto As String
  With vsPrinter1
    'CABECERA DE LA FACTURA
    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    '///////////////////////////////////////////////////////////////////
    ' ******************CORREGIDO POR PATXI ***************************
    'Texto = "2"
    Texto = vsPrinter1.CurrentPage + 1
    '******************************************************************
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACI�N DEL MES DE " & UCase(Me.cboMeses)
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & UCase(Me.txtCodEntidad.Text)
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL " & UCase(Me.cboCodEntidad.Text) & " POR LOS PROCESOS QUIR�RGICOS EN EL MES DE " & UCase(Me.cboMeses) & " / " & Me.txtAnyo & _
            "DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA " & _
            "DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & UCase(FacturaForf)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    'CABECERA DE LAS LINEAS
    .CurrentX = 20 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "N."
    .Text = Texto
    .FontUnderline = True
    .CurrentX = 125 * ConvY
    Texto = "F.ENTRA"
    .Text = Texto
    .CurrentX = 145 * ConvY
    Texto = "F.INT"
    .Text = Texto
    .CurrentX = 160 * ConvY
    Texto = "F.SALIDA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "N"
    .Text = Texto
    .CurrentX = 200 * ConvY
    Texto = "C O S T O S"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "ESPECIALI"
    .Text = Texto
    .Paragraph = ""
    .FontUnderline = False
    .CurrentX = 20 * ConvY
    Texto = "ORD"
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 55 * ConvY
    Texto = "INSP."
    .Text = Texto
    .CurrentX = 68 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 125 * ConvY
    Texto = "MM.DD.H"
    .Text = Texto
    .CurrentX = 144 * ConvY
    Texto = "MM-DD"
    .Text = Texto
    .CurrentX = 162 * ConvY
    Texto = "DD-HH"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "EST"
    .Text = Texto
    .CurrentX = 192 * ConvY
    Texto = "ESTANCIAS"
    .Text = Texto
    .CurrentX = 212 * ConvY
    Texto = "SUPLEM."
    .Text = Texto
    .CurrentX = 230 * ConvY
    Texto = "DIAGNOSTICO"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "CL.DENOM"
    .Text = Texto
'    While .CurrentX <= 260 * ConvY
'      .Text = "-"
'    Wend
    .Paragraph = ""
  End With
  
End Sub
Sub CargarMeses()

  Me.cboMeses.AddItem "Enero"
  Me.cboMeses.AddItem "Febrero"
  Me.cboMeses.AddItem "Marzo"
  Me.cboMeses.AddItem "Abril"
  Me.cboMeses.AddItem "Mayo"
  Me.cboMeses.AddItem "Junio"
  Me.cboMeses.AddItem "Julio"
  Me.cboMeses.AddItem "Agosto"
  Me.cboMeses.AddItem "Septiembre"
  Me.cboMeses.AddItem "Octubre"
  Me.cboMeses.AddItem "Noviembre"
  Me.cboMeses.AddItem "Diciembre"
  
End Sub

Function FechaAFormatoMMDDHH(fecha As Date) As String
Dim MM As String
Dim DD As String
Dim HH As String

  MM = Format(fecha, "MM")
  DD = Format(fecha, "DD")
  HH = Format(fecha, "HH")
  
  If CInt(MM) = Me.txtMeses Then
    MM = "00"
  Else
    MM = Right("00" & MM, 2)
  End If
  DD = Right("00" & DD, 2)
  HH = Right("00" & HH, 2)
  FechaAFormatoMMDDHH = MM & "-" & DD & "-" & HH
  
End Function

Function FechaAFormatoDDHH(fecha As Date) As String
Dim DD As String
Dim HH As String

  DD = Format(fecha, "DD")
  HH = Format(fecha, "HH")
  
  DD = Right("00" & DD, 2)
  HH = Right("00" & HH, 2)
  FechaAFormatoDDHH = DD & "-" & HH
  
End Function


Function FechaAFormatoDD(fecha As Date) As String
Dim DD As String
Dim HH As String

  DD = Format(fecha, "DD")
  DD = Right("00" & DD, 2)
  FechaAFormatoDD = DD
  
End Function



Sub Hospitalizado()
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double

  'Inicializamos el total de la factura a 0
  Total = 0
  'Obtenemos los datos del Insalud que se est� facturando.
  Insalud.Codigo = Me.txtCodPersona
  
  With vsPrinter1
    'Como es la cabecera de la factura de hospitalizados incluimos el escudo de la cl�nica.
    .Preview = False
    .FontName = "Arial"
    .FontSize = 9
    .Orientation = orLandscape
    .Action = paStartDoc
    .X1 = 20 * ConvY
    .Y1 = 30 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 70 * ConvX
    .Picture = Me.Picture1.Picture
    
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .FontName = "Arial"
    .FontSize = 9
    .CurrentX = 50 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Name
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = "C.I.F.: " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Direccion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "C.I.F.: Q-3168001-J"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.CP & " " & Insalud.Poblacion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "N� INSC. S.S.: 31/32.361/24"
    .Paragraph = Texto
    Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
    .Paragraph = Texto
    Texto = "REFERENCIA: " & UCase(FacturaForf)
    .Paragraph = Texto
    Texto = "29 DE FEBRERO DEL 2000"
    .Paragraph = Texto
    .Paragraph = ""
    .MarginLeft = 25 * ConvY
    .TextAlign = taCenterBaseline
    .FontUnderline = True
    Texto = Insalud.Name
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .FontUnderline = False
    .Paragraph = ""
    .Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR LOS PROCESOS QUIRURGICOS EN EL MES DE " & UCase(Me.cboMeses.Text) & _
            "/ " & Me.txtAnyo & ", DE LOS PACIENTES QUE A CONTINUACION SE DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES " & _
            "PARTES DE ASISTENCIA DEBIDAMENTES AUTORIZADOS"
    .Paragraph = Texto
    .Paragraph = ""
    
    'Cogemos el resumen de los forfaits y lo imprimimos.
    For x = 1 To UBound(ResumenForf)
      .CurrentX = 33 * ConvY
      Texto = ResumenForf(x).Cantidad
      .Text = Texto
      .CurrentX = 38 * ConvY
      Texto = ""
      Cont = 0
      While .TextWidth(Texto) <= 85 * ConvY And Cont < Len(ResumenForf(x).Diagnost)
        Cont = Cont + 1
        Texto = Left(UCase(ResumenForf(x).Diagnost), Cont)
      Wend
      .Text = Texto
      .CurrentX = 125 * ConvY
      Cont = 0
      Texto = ""
      While .TextWidth(Texto) <= 79 * ConvY And Cont < Len(ResumenForf(x).proceso)
        Cont = Cont + 1
        Texto = Left(UCase(ResumenForf(x).proceso), Cont)
      Wend
      .Text = Texto
      .CurrentX = 210 * ConvY
      Texto = "A"
      .Text = Texto
      Texto = Format(ResumenForf(x).Precio, "##,###,##0.##")
      .CurrentX = 238 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentX = 240 * ConvY
      Texto = "PESETAS"
      .Text = Texto
      Texto = Format(ResumenForf(x).Precio * ResumenForf(x).Cantidad, "###,###,##0.##")
      Total = Total + CDbl(Texto)
      .CurrentX = 280 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentY = .CurrentY + 300
    Next
    'Si hay pr�tesis en la factura imprimiremos la l�nea de pr�tesis en la cabecera de factura...
    If ProtesisForfaits <> 0 Then
      .CurrentX = 38 * ConvY
      .Text = "PROTESIS"
      .CurrentX = 240 * ConvY
      .Text = "PESETAS"
      Texto = Format(ProtesisForfaits, "###,###,##0.##")
      .CurrentX = 280 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .Paragraph = ""
      Total = Total + ProtesisForfaits
    End If
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .CurrentX = 240 * ConvY
    .Text = "T O T A L"
    Texto = Format(Total, "###,###,##0.##")
    .CurrentX = 280 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    .Paragraph = "POR EL CENTRO CONCERTADO"

    .Action = paEndDoc
  End With

End Sub

Sub Ambulatorio()
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double

  'Inicializamos el total de la factura a 0
  Total = 0
  'Obtenemos los datos del Insalud que se est� facturando.
  Insalud.Codigo = Me.txtCodPersona
  
  With vsPrinter1
    'Como es la cabecera de la factura de hospitalizados incluimos el escudo de la cl�nica.
    .Preview = False
    .FontName = "Arial"
    .FontSize = 9
    .Orientation = orLandscape
    .Action = paStartDoc
    .X1 = 20 * ConvY
    .Y1 = 30 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 70 * ConvX
    .Picture = Me.Picture1.Picture
    
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .FontName = "Arial"
    .FontSize = 9
    .CurrentX = 50 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Name
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = "C.I.F.: " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Direccion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "C.I.F.: Q-3168001-J"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.CP & " " & Insalud.Poblacion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "N� INSC. S.S.: 31/32.361/24"
    .Paragraph = Texto
    Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
    .Paragraph = Texto
    Texto = "REFERENCIA: " & UCase(FacturaAmbu)
    .Paragraph = Texto
    Texto = "29 DE FEBRERO DEL 2000"
    .Paragraph = Texto
    .Paragraph = ""
    .MarginLeft = 25 * ConvY
    .TextAlign = taCenterBaseline
    .FontUnderline = True
    Texto = Insalud.Name
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .FontUnderline = False
    .Paragraph = ""
    .Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR CONSULTAS EXTERNAS EN REGIMEN " & _
            "AMBULATORIO EN ESTE CENTRO EN EL MES DE " & UCase(Me.cboMeses.Text) & "/ " & Me.txtAnyo & _
            ", DE LOS PACIENTES QUE A CONTINUACION SE DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES " & _
            "PARTES DE ASISTENCIA DEBIDAMENTES AUTORIZADOS"
    .Paragraph = Texto
    .Paragraph = ""
    
    'Cogemos el resumen de los forfaits y lo imprimimos.
    For x = 1 To UBound(CabeceraAmbul)
      If CabeceraAmbul(x).PET = 0 And CabeceraAmbul(x).RNM = 0 Then
        .CurrentX = 33 * ConvY
        Texto = ""
        If CabeceraAmbul(x).Cantidad <> 0 Then
          Texto = CabeceraAmbul(x).Cantidad
        End If
        .Text = Texto
        '.CurrentX = 38 * ConvY
        .CurrentX = 50 * ConvY
        Texto = ""
        Cont = 0
        While .TextWidth(Texto) <= 85 * ConvY And Cont < Len(CabeceraAmbul(x).Descripcion)
          Cont = Cont + 1
          Texto = Left(UCase(CabeceraAmbul(x).Descripcion), Cont)
        Wend
        .Text = Texto
        If CabeceraAmbul(x).Precio <> 0 Then
        .CurrentX = 210 * ConvY
            Texto = "A"
            .Text = Texto
            Texto = Format(CabeceraAmbul(x).Precio, "##,###,##0")
            .CurrentX = 238 * ConvY - .TextWidth(Texto)
            .Text = Texto
            .CurrentX = 240 * ConvY
            Texto = "PESETAS"
            .Text = Texto
        Else
            Texto = ""
        End If
        If CabeceraAmbul(x).Cantidad > 0 Then
            Texto = Format(CabeceraAmbul(x).Precio * CabeceraAmbul(x).Cantidad, "###,###,##0.##")
        ElseIf CabeceraAmbul(x).Importe > 0 Then
            Texto = Format(CabeceraAmbul(x).Importe, "###,###,##0")
        End If
        Total = Total + CDbl(Texto)
        .CurrentX = 280 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentY = .CurrentY + 300
      End If
    Next
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .CurrentX = 240 * ConvY
    .Text = "T O T A L"
    Texto = Format(Total, "###,###,##0.##")
    .CurrentX = (280 * ConvY) - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    .Paragraph = "POR EL CENTRO CONCERTADO"

    .Action = paEndDoc
  End With

End Sub
Sub ImprimirMadrid()
Dim x As Long

  
  With vsPrinter1
    .Preview = True
    .Action = paStartDoc
    Call Me.CabeceraForfaits
    .Action = paEndDoc
    .Preview = False
    For x = 1 To UBound(MadridCab)
      If MadridCab(x).Tipo = 1 Then
        Call Me.PrimeraMadrid(MadridCab(x).orden)
        Call Me.CabForfMadrid(MadridCab(x).orden)
        Call Me.LinForfMadrid(MadridCab(x).orden)
      ElseIf MadridCab(x).Tipo = 2 Then
        Call Me.PrimeraMadrid(MadridCab(x).orden)
        Call Me.CabEstMadrid(MadridCab(x).orden)
        Call Me.LinEstMadrid(MadridCab(x).orden)
      ElseIf MadridCab(x).Tipo = 3 Then
        Call Me.PrimeraMadrid(MadridCab(x).orden)
        Call Me.CabAmbMadrid(MadridCab(x).orden)
        Call Me.LineasRestoMadrid(MadridCab(x).orden)
      ElseIf MadridCab(x).Tipo = 4 Then
        Call Me.PrimeraMadrid(MadridCab(x).orden)
        Call Me.CabAmbMadrid(MadridCab(x).orden)
        Call Me.LinAmbMadrid(MadridCab(x).orden)
      End If
    Next
  End With
End Sub

Sub LinAmbMadrid(orden As Integer)
Dim x, j, Cont As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim DepAnt As String

  DepAnt = ""
  With vsPrinter1
    For x = 1 To UBound(LinMadrid)
      'Comprobamos si la l�nea se corresponde
      If LinMadrid(x).orden = orden Then
        Cont = Cont + 1
        'Miramos si el departamento es distinto para ponerlo
        If DepAnt <> LinMadrid(x).Servicio Then
          .Paragraph = ""
          Texto = "***********" & "    " & UCase(LinMadrid(x).Servicio)
          .CurrentX = 25 * ConvY
          .Text = Texto
          .Paragraph = ""
          .Paragraph = ""
          DepAnt = LinMadrid(x).Servicio
        End If
        'LINEAS DEL FICHERO
        Texto = Cont
        .CurrentX = 30 * ConvY - .TextWidth(Texto)
        .Text = Texto
         Texto = LinMadrid(x).Asegurado 'Numero de la seguridad social del asegurado
        .CurrentX = 58 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 60 * ConvY
        Texto = LinMadrid(x).Inspe
        .Text = Texto
        .CurrentX = 73 * ConvY
        If Copias = 4 Then
          Texto = LinMadrid(x).Historia & " " & LinMadrid(x).Nombre
        Else
          Texto = LinMadrid(x).Nombre
        End If
        While .TextWidth(Texto) > 50 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .Text = Texto
        'N� de servicios
        Texto = LinMadrid(x).NumServ
        .CurrentX = 139 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'D�as
        Texto = LinMadrid(x).DiasServ
        .CurrentX = 141 * ConvY
        .Text = Texto
        'N� de visitas (primera y sucesiva)
        Texto = LinMadrid(x).PriVisit
        If Texto <> 0 Then
          .CurrentX = 163 * ConvY - .TextWidth(Texto)
          .Text = Texto
        End If
        Texto = LinMadrid(x).SucVisit
        If Texto <> 0 Then
          .CurrentX = 170 * ConvY - .TextWidth(Texto)
          .Text = Texto
        End If
        'Precio (primera y sucesiva)
        Texto = Format(LinMadrid(x).PriPrecio, "###,###,##0.##")
        If Texto <> 0 Then
          .CurrentX = 192 * ConvY - .TextWidth(Texto)
          .Text = Texto
        End If
        Texto = Format(LinMadrid(x).SucPrecio, "###,###,##0.##")
        If Texto <> 0 Then
          .CurrentX = 211 * ConvY - .TextWidth(Texto)
          .Text = Texto
        End If
        'Costo total
        If LinMadrid(x).Costo = 0 Then
          LinMadrid(x).Costo = (LinMadrid(x).PriVisit * LinMadrid(x).PriPrecio) + (LinMadrid(x).SucVisit * LinMadrid(x).SucPrecio)
        End If
        Texto = Format(LinMadrid(x).Costo, "###,###,##0.##")
        .CurrentX = 228 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'Servicio
        Texto = UCase(LinMadrid(x).Servicio)
        While .TextWidth(Texto) > 21 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .CurrentX = 232 * ConvY
        .Text = Texto
        'Diagn�stico
        Texto = UCase(LinMadrid(x).Diagnostico)
        Texto = Left(Texto, 10)
        .CurrentX = 277 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .Paragraph = ""
        .Paragraph = ""
        If .CurrentY > 180 * ConvX Then
          .NewPage
          Call Me.CabAmbMadrid(MadridCab(x).orden)
        End If
      End If
    Next
    .Action = paEndDoc
  End With
      
End Sub

Sub LineasRestoMadrid(orden As Integer)
Dim x, j, Cont As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim DepAnt As String
Dim AseAnt As String

  DepAnt = ""
  AseAnt = ""
  Cont = 0
  With vsPrinter1
    For x = 1 To UBound(LinMadrid)
      If LinMadrid(x).orden = orden Then
        Cont = Cont + 1
        If DepAnt <> LinMadrid(x).Servicio Then
          .Paragraph = ""
          Texto = "***********" & "    " & UCase(LinMadrid(x).Servicio)
          .CurrentX = 25 * ConvY
          .Text = Texto
          DepAnt = LinMadrid(x).Servicio
          .Paragraph = ""
          .Paragraph = ""
        End If
        If AseAnt <> LinMadrid(x).Asegurado Then
          AseAnt = LinMadrid(x).Asegurado
          .Paragraph = ""
          Texto = Cont 'Contador que nos indicar� la l�nea que le corresponde
          .CurrentX = 30 * ConvY - .TextWidth(Texto)
          .Text = Texto
          Texto = LinMadrid(x).Asegurado 'N�mero de la seguridad social del asegurado
          .CurrentX = 56 * ConvY - .TextWidth(Texto)
          .Text = Texto
          .CurrentX = 60 * ConvY
          Texto = LinMadrid(x).Inspe
          .Text = Texto
          .CurrentX = 73 * ConvY
          Texto = LinMadrid(x).Nombre
          'While .TextWidth(Texto) > 50 * ConvY
          '  Texto = Left(Texto, Len(Texto) - 1)
          'Wend
          .Text = Texto
          .Paragraph = ""
        End If
        'Cantidad
        LinMadrid(x).Cantidad = LinMadrid(x).Importe / LinMadrid(x).Precio
        Texto = LinMadrid(x).Cantidad
        .CurrentX = 70 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'descripcion
        .CurrentX = 73 * ConvY
        Texto = LinMadrid(x).Descrip
        .Text = Texto
        'Importe
        Texto = Format(LinMadrid(x).Precio, "###,###,##0.##")
        .CurrentX = 211 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'Costo total
        If LinMadrid(x).Importe = 0 Then
          LinMadrid(x).Importe = LinMadrid(x).Cantidad * LinMadrid(x).Precio
        End If
        Texto = Format(LinMadrid(x).Importe, "###,###,##0.##")
        .CurrentX = 228 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'Servicio
        .CurrentX = 232 * ConvY
        Texto = UCase(LinMadrid(x).Servicio)
        While .TextWidth(Texto) > 21 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .Text = Texto
        'Diagn�stico
        Texto = Left(UCase(LinMadrid(x).Diagnostico), 9)
        .CurrentX = 277 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .Paragraph = ""
        If UCase(LinMadrid(x).Descrip) = "SESION QUIMIOTERAPIA" Then
          .CurrentX = 73 * ConvY
          .Text = Left(LinMadrid(x).DiasServ, Len(LinMadrid(x).DiasServ) - 1)
        End If
        If .CurrentY > 180 * ConvX Then
          .NewPage
          Call Me.CabAmbMadrid(MadridCab(x).orden)
        End If
      End If
    Next
    .Action = paEndDoc
  End With
      
End Sub

Sub LinEstMadrid(orden As Integer)
Dim x, j, Cont As Integer
Dim Texto As String
Dim Daignos As String
Dim Especia As String

  With Me.vsPrinter1
    For x = 1 To UBound(LinMadrid)
      If orden = LinMadrid(x).orden Then
        Cont = Cont + 1
        Texto = Cont
        .CurrentX = 24 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = LinMadrid(x).Asegurado 'Numero de la seguridad social del asegurado
        .CurrentX = 50 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 55 * ConvY
        Texto = LinMadrid(x).Inspe 'Texto Fijo "SUBD"
        .Text = Texto
        .CurrentX = 68 * ConvY
        Texto = LinMadrid(x).Nombre 'Nombre del Paciente
        While .TextWidth(Texto) > 50 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .Text = Texto
        .CurrentX = 125 * ConvY
        Texto = LinMadrid(x).FecEnt 'Decha de entrada en formato MM-DD-HH
        .Text = Texto
        .CurrentX = 146 * ConvY
        .Text = "00-00"
        .CurrentX = 165 * ConvY
        Texto = LinMadrid(x).FecSal 'Fecha de alta DD-HH
        .Text = Texto
        Texto = LinMadrid(x).NumServ 'N�mero de estancias
        .CurrentX = 185 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = Format(LinMadrid(x).EstCosto, "###,###,##0.##") 'Coste de las estancias, en este caso el forfait
        .CurrentX = 208 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = Format(LinMadrid(x).SupCosto, "###,###,##0.##") 'Coste de los suplementos
        .CurrentX = 228 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Diagnostico = UCase(Left(LinMadrid(x).Diagnostico, 10)) 'Dian�stico de salida
        .CurrentX = 253 * ConvY - .TextWidth(Diagnostico)
        .Text = Diagnostico
        Especia = UCase(Left(LinMadrid(x).Especialidad, 10))
        .CurrentX = 275 * ConvY - .TextWidth(Especia)
        .Text = Especia
        .Paragraph = ""
        .Paragraph = ""
        If .CurrentY > 180 * ConvX Then
          .NewPage
          Call Me.CabEstMadrid(MadridCab(x).orden)
        End If
      End If
    Next
    .Action = paEndDoc
  End With
End Sub

Sub LinForfMadrid(orden As Integer)
Dim x, j, Cont As Integer
Dim Texto As String
Dim Diag, Proc As String

  With vsPrinter1
    For x = 1 To UBound(LinMadrid)
      If LinMadrid(x).orden = orden Then
        'Comprobaremos que no tenemos que realizar un salto de p�gina para mantener el pacientes
        'y los forfaits unidos.
        If .CurrentY + (200 * (LinMadrid(x).Forfaits + 1)) > 190 * ConvX Then
          .NewPage
          Call Me.CabForfMadrid(MadridCab(x).orden)
        End If
        'LINEAS DEL FICHERO
        Texto = Cont 'Contador
        .CurrentX = 28 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = LinMadrid(x).Asegurado 'N�mero de la seguridad social del asegurado
        .CurrentX = 50 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 55 * ConvY
        Texto = LinMadrid(x).Inspe 'Texto Fijo "FORF"
        .Text = Texto
        .CurrentX = 68 * ConvY
        If Copias = 4 Then
          Texto = LinMadrid(x).Historia & " " & LinMadrid(x).Nombre 'Historia y nombre del paciente
        Else
          Texto = LinMadrid(x).Nombre 'Nombre Del Paciente
        End If
        While .TextWidth(Texto) > 50 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .Text = Texto
        .CurrentX = 125 * ConvY
        Texto = LinMadrid(x).FecEnt 'Fecha de entrada en formato MM-DD-HH
        .Text = Texto
        .CurrentX = 146 * ConvY
        Texto = LinMadrid(x).FecInt 'Fecha de la intervenci�n.
        .Text = Texto
        .CurrentX = 165 * ConvY
        Texto = Format(LinMadrid(x).EstCosto, "###,###,##0.##") 'Costo de los forfaits
        .CurrentX = 208 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = Format(LinMadrid(x).SupCosto, "###,###,##0.##") 'Coste de los suplementos
        .CurrentX = 228 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = LinMadrid(x).Diagnostico 'Diagn�stico de salida
        .CurrentX = 253 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = LinMadrid(x).Especialidad 'Especialidad
        .CurrentX = 275 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .Paragraph = ""
        Diag = UCase(LinMadrid(x).CodDiag & " " & LinMadrid(x).DescDiag)
        Proc = UCase(LinMadrid(x).CodProc & " " & LinMadrid(x).DesProc)
        While .TextWidth(Diag) > 78 * ConvY
          Diag = Left(Diag, Len(Diag) - 1)
        Wend
        .CurrentX = 60 * ConvY
        Texto = Diag
        .Text = Texto
        .CurrentX = 140 * ConvY
        Texto = Proc
        .Text = Texto
        .Paragraph = ""
        .Paragraph = ""
      End If
    Next
  End With
End Sub

Sub PrimeraMadrid(orden As Integer)
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double
Dim FacturaMadrid As String
Dim Cont2 As Long


    'Inicializamos el total de la factura a 0
    Total = 0
    'Obtenemos los datos del Insalud que se est� facturando.
    Insalud.Codigo = Me.txtCodPersona
    
    With vsPrinter1
      .Preview = True
      .CurrentX = 250
      .Text = ""
      .Paragraph = ""
      .Preview = False
      .FontName = "Arial"
      .FontSize = 9
      .Orientation = orLandscape
      .Action = paStartDoc
      .X1 = 20 * ConvY
      .Y1 = 20 * ConvX
      .X2 = 50 * ConvY
      .Y2 = 60 * ConvX
      .Picture = Me.Picture1.Picture
      
      .MarginTop = 30 * ConvX
      .MarginLeft = 50 * ConvY
      .FontName = "Arial"
      .FontSize = 9
      .CurrentX = 50 * ConvY
      Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
      .Text = Texto
      .CurrentX = 195 * ConvY
      Texto = UCase(Insalud.Name)
      .Text = Texto
      .CurrentY = .CurrentY + 200
      .CurrentX = 50 * ConvY
      Texto = "AVDA. PIO XII, 36"
      .Text = Texto
      .CurrentX = 195 * ConvY
      Texto = "C.I.F.: " & Insalud.DNI
      .Text = Texto
      .CurrentY = .CurrentY + 200
      .CurrentX = 50 * ConvY
      Texto = "31008 - PAMPLONA"
      .Text = Texto
      .CurrentX = 195 * ConvY
      Texto = Insalud.Direccion
      .Text = Texto
      .CurrentY = .CurrentY + 200
      .CurrentX = 50 * ConvY
      Texto = "C.I.F.: Q-3168001-J"
      .Text = Texto
      .CurrentX = 195 * ConvY
      Texto = Insalud.CP & " " & Insalud.Poblacion
      .Text = Texto
      .CurrentY = .CurrentY + 200
      .CurrentX = 50 * ConvY
      Texto = "N� INSC. S.S.: 31/32.361/24"
      .Paragraph = Texto
      Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
      .Paragraph = Texto
      Texto = "REFERENCIA: " & orden & "/2000"
      .Paragraph = Texto
      Texto = "31 DE ENERO DE 2000"
      .Paragraph = Texto
      .Paragraph = ""
      .MarginLeft = 25 * ConvY
      .TextAlign = taCenterBaseline
      .FontUnderline = True
      Texto = UCase(Insalud.Name)
      .Paragraph = Texto
      .TextAlign = taLeftBaseline
      .FontUnderline = False
      .Paragraph = ""
      .Paragraph = ""
      Texto = "NOTA A CARGO DEL INSALUD DE MADRID POR LA ASISTENCIA EN ESTE CENTRO CURANTE EL MES DE " & _
              UCase(Me.cboMeses.Text) & " DE " & Me.txtAnyo & ", DE LOS PACIENTES QUE A CONTINUACI�N " & _
              "SE DETALLAN, ACOMPA��NDOSE DE LOS CORRESPONDIENTES PARTES DEBIDAMENTE AUTORIZADOS."
      .Paragraph = Texto
      .Paragraph = ""
      .Paragraph = ""
      'Aqu� no acompa�amos las l�neas que acompa�an a la factura ya que al ser Madrid esta cabecera vale para
      'diferentes facturas
      For Cont2 = 1 To UBound(MadridCab)
        If MadridCab(Cont2).orden = orden Then
          If MadridCab(Cont2).Cantidad <> 0 Then
            Texto = Format(MadridCab(Cont2).Cantidad, "###,##0.0")
            .CurrentX = 35 * ConvY - .TextWidth(Texto)
            .Text = Texto
          End If
          .CurrentX = 40 * ConvY
          Texto = UCase(MadridCab(Cont2).Desc1)
          .Text = Texto
          .CurrentX = 130 * ConvY
          .Text = "A"
          If MadridCab(Cont2).Precio <> 0 Then
            Texto = Format(MadridCab(Cont2).Precio, "###,##0.##")
            .CurrentX = 170 * ConvY - .TextWidth(Texto)
            .Text = Texto
          End If
          .CurrentX = 180 * ConvY
          .Text = "PESETAS"
          If MadridCab(Cont2).Importe = 0 Then
            MadridCab(Cont2).Importe = MadridCab(Cont2).Cantidad * MadridCab(Cont2).Precio
          End If
          Texto = Format(MadridCab(Cont2).Importe, "###,###,##0.##")
          .CurrentX = 265 * ConvY - .TextWidth(Texto)
          .Text = Texto
          .Paragraph = ""
        End If
      Next
        
      .Action = paEndDoc
    End With
End Sub


Sub PrimeraRNM()
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double

  'Inicializamos el total de la factura a 0
  Total = 0
  'Obtenemos los datos del Insalud que se est� facturando.
  Insalud.Codigo = Me.txtCodPersona
  
  With vsPrinter1
    'Como es la cabecera de la factura de hospitalizados incluimos el escudo de la cl�nica.
    .Preview = False
    .FontName = "Arial"
    .FontSize = 9
    .Orientation = orLandscape
    .Action = paStartDoc
    .X1 = 20 * ConvY
    .Y1 = 30 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 70 * ConvX
    .Picture = Me.Picture1.Picture
    
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .FontName = "Arial"
    .FontSize = 9
    .CurrentX = 50 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Name
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = "C.I.F.: " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Direccion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "C.I.F.: Q-3168001-J"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.CP & " " & Insalud.Poblacion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "N� INSC. S.S.: 31/32.361/24"
    .Paragraph = Texto
    Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
    .Paragraph = Texto
    Texto = "REFERENCIA: "
    .Paragraph = Texto
    Texto = "29 DE FEBRERO DEL 2000"
    .Paragraph = Texto
    .Paragraph = ""
    .MarginLeft = 25 * ConvY
    .TextAlign = taCenterBaseline
    .FontUnderline = True
    Texto = Insalud.Name
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .FontUnderline = False
    .Paragraph = ""
    .Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & ", POR CONSULTAS EXTERNAS EN REGIMEN " & _
            "AMBULATORIO EN ESTE CENTRO EN EL MES DE " & UCase(Me.cboMeses.Text) & "/ " & Me.txtAnyo & _
            ", DE LOS PACIENTES QUE A CONTINUACION SE DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES " & _
            "PARTES DE ASISTENCIA DEBIDAMENTES AUTORIZADOS"
    .Paragraph = Texto
    .Paragraph = ""
    
    'Cogemos el resumen de los forfaits y lo imprimimos.
    For x = 1 To UBound(CabeceraAmbul)
      If CabeceraAmbul(x).RNM = 1 Then
        .CurrentX = 33 * ConvY
        If CabeceraAmbul(x).Cantidad <> 0 Then
          Texto = CabeceraAmbul(x).Cantidad
        End If
        .Text = Texto
        .CurrentX = 38 * ConvY
        Texto = ""
        Cont = 0
        While .TextWidth(Texto) <= 85 * ConvY And Cont < Len(CabeceraAmbul(x).Descripcion)
          Cont = Cont + 1
          Texto = Left(UCase(CabeceraAmbul(x).Descripcion), Cont)
        Wend
        .Text = Texto
        .CurrentX = 210 * ConvY
        Texto = "A"
        .Text = Texto
        Texto = Format(CabeceraAmbul(x).Precio, "##,###,##0.##")
        .CurrentX = 238 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 240 * ConvY
        Texto = "PESETAS"
        .Text = Texto
        Texto = Format(CabeceraAmbul(x).Precio * CabeceraAmbul(x).Cantidad, "###,###,##0.##")
        Total = Total + CDbl(Texto)
        .CurrentX = 280 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentY = .CurrentY + 300
      End If
    Next
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .CurrentX = 240 * ConvY
    .Text = "T O T A L"
    Texto = Format(Total, "###,###,##0.##")
    .CurrentX = 280 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    .Paragraph = "POR EL CENTRO CONCERTADO"

    .Action = paEndDoc
  End With

End Sub

Sub PrimeraPET()
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double

  'Inicializamos el total de la factura a 0
  Total = 0
  'Obtenemos los datos del Insalud que se est� facturando.
  Insalud.Codigo = Me.txtCodPersona
  
  With vsPrinter1
    'Como es la cabecera de la factura de hospitalizados incluimos el escudo de la cl�nica.
    .Preview = False
    .FontName = "Arial"
    .FontSize = 9
    .Orientation = orLandscape
    .Action = paStartDoc
    .X1 = 20 * ConvY
    .Y1 = 30 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 70 * ConvX
    .Picture = Me.Picture1.Picture
    
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .FontName = "Arial"
    .FontSize = 9
    .CurrentX = 50 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Name
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = "C.I.F.: " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Direccion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "C.I.F.: Q-3168001-J"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.CP & " " & Insalud.Poblacion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "N� INSC. S.S.: 31/32.361/24"
    .Paragraph = Texto
    Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
    .Paragraph = Texto
    Texto = "REFERENCIA: " & UCase(FacturaPET)
    .Paragraph = Texto
    Texto = "29 DE FEBRERO DEL 2000"
    .Paragraph = Texto
    .Paragraph = ""
    .MarginLeft = 25 * ConvY
    .TextAlign = taCenterBaseline
    .FontUnderline = True
    Texto = Insalud.Name
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .FontUnderline = False
    .Paragraph = ""
    .Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR CONSULTAS EXTERNAS EN REGIMEN " & _
            "AMBULATORIO EN ESTE CENTRO EN EL MES DE " & UCase(Me.cboMeses.Text) & "/ " & Me.txtAnyo & _
            ", DE LOS PACIENTES QUE A CONTINUACION SE DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES " & _
            "PARTES DE ASISTENCIA DEBIDAMENTES AUTORIZADOS"
    .Paragraph = Texto
    .Paragraph = ""
    
    'Cogemos el resumen de los forfaits y lo imprimimos.
    For x = 1 To UBound(CabeceraAmbul)
      If CabeceraAmbul(x).PET = 1 Then
        .CurrentX = 33 * ConvY
        If CabeceraAmbul(x).Cantidad <> 0 Then
          Texto = CabeceraAmbul(x).Cantidad
        End If
        .Text = Texto
        .CurrentX = 38 * ConvY
        Texto = ""
        Cont = 0
        While .TextWidth(Texto) <= 85 * ConvY And Cont < Len(CabeceraAmbul(x).Descripcion)
          Cont = Cont + 1
          Texto = Left(UCase(CabeceraAmbul(x).Descripcion), Cont)
        Wend
        .Text = Texto
        .CurrentX = 210 * ConvY
        Texto = "A"
        .Text = Texto
        Texto = Format(CabeceraAmbul(x).Precio, "##,###,##0.##")
        .CurrentX = 238 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 240 * ConvY
        Texto = "PESETAS"
        .Text = Texto
        Texto = Format(CabeceraAmbul(x).Precio * CabeceraAmbul(x).Cantidad, "###,###,##0.##")
        Total = Total + CDbl(Texto)
        .CurrentX = 280 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentY = .CurrentY + 300
      End If
    Next
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .CurrentX = 240 * ConvY
    .Text = "T O T A L"
    Texto = Format(Total, "###,###,##0.##")
    .CurrentX = (280 * ConvY) - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    .Paragraph = "POR EL CENTRO CONCERTADO"

    .Action = paEndDoc
  End With

End Sub

Sub IniciarQRYs()
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiSqL As String
Dim x As Integer


  '------------------------------------------------------------------------------------------------
  'RECOGER DATOS FORFAITS
  '------------------------------------------------------------------------------------------------
  '1) Recogida de los datos
  MiSelect = "select  FA0400.FA04CODFACT, FA0400.CI32CODTIPECON, FA0400.CI13CODENTIDAD, " & _
             "FA0400.FA04NUMFACT, FA0400.FA04FECFACTURA, FA0400.AD07CODPROCESO, " & _
             "FA0400.AD01CODASISTENCI, FA0400.FA09CODNODOCONC, FA0400.FA04INDCOMPENSA, " & _
             "FA0400.FA04CANTFACT, AD2500.AD12CODTIPOASIST, CI1300.CI13DESENTIDAD, " & _
             "AD1200.AD12DESTIPOASIST, AD0100.AD01FECINICIO, AD0100.AD01FECFIN, AD0100.AD01DIAGNOSSALI, " & _
             "CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||' '||CI2200.CI22NOMBRE AS PERSONA, CI2200.CI22NUMHISTORIA, " & _
             "CI2200.CI22NUMSEGSOC, AD0200.AD02CODDPTO, AD0200.AD02DESDPTO, AD0500.AD05FECFINRESPON, " & _
             "FA0400.FA04FECHASTA " & _
             "From FA0400,CI1300, AD0500, AD0200, AD0100, CI2200, AD2500, AD1200, AD1100 "
  MiWhere = "Where FA0400.CI32CODTIPECON = CI1300.CI32CODTIPECON " & _
            "And FA0400.CI13CODENTIDAD = CI1300.CI13CODENTIDAD " & _
            "And FA0400.AD01CODASISTENCI = AD0500.AD01CODASISTENCI " & _
            "And FA0400.AD07CODPROCESO = AD0500.AD07CODPROCESO " & _
            "And AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And AD2500.AD25FECFIN IS NULL " & _
            "And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
            "And AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "And FA0400.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
            "And AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST " & _
            "And FA0400.FA04FECFACTURA >= TO_DATE(?,'DD/MM/YYYY') " & _
            "And FA0400.FA04FECFACTURA <= LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " & _
            "AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "AND AD1100.AD11FECFIN IS NULL " & _
            "AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " & _
            " AND EXISTS " & _
            "       (SELECT /*+ rule */ AD01CODASISTENCI " & _
            "       From AD1800 " & _
            "       Where  AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "           AND AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "           AND AD1800.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "           AND AD1800.AD19CODTIPODOCUM = 1 " & _
            "       ) "
            ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            ' ****************************MODIFICADO POR PATXI***************
            ' Pongo que los hospitalizados sean los que tengan cama facturada.
'MiWhere = MiWhere & " AND EXISTS " & _
            "       (SELECT FA04NUMFACT " & _
            "        FROM    FA0300, " & _
            "                FA0500 " & _
            "        WHERE  FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " & _
            "               AND FA0500.FA08CODGRUPO=5 " & _
            "               AND FA0300.FA04NUMFACT = FA0400.FA04CODFACT " & _
            "        ) "
MiWhere = MiWhere & "And AD2500.AD12CODTIPOASIST = '1' " & _
            "And FA0400.CI32CODTIPECON = 'S' " & _
            "And FA0400.CI13CODENTIDAD  = ? " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And FA0400.FA04INDCOMPENSA <> 2 " & _
            "And FA0400.FA04NUMFACREAL  IS NULL " & _
            "And FA0400.FA09CODNODOCONC_FACT IN (389,387,385,388,454) " & _
            "And FA0400.FA04CANTFACT <> 0"
'MiWhere = MiWhere & " AND FA0400.FA04FECHASTA < TO_DATE('1/2/2000','DD/MM/YYYY')"
            
            ' ///////////////////////////////////////////////////////////////
  MiOrder = "Order By  AD0200.AD02CODDPTO, PERSONA ASC"
  MiSqL = Trim(MiSelect) & " " & Trim(MiWhere) & " " & Trim(MiOrder)
  QryForfaits.SQL = MiSqL
  Set QryForfaits.ActiveConnection = objApp.rdoConnect
  QryForfaits.Prepared = True
  
  'Seleccionamos las lineas distintas de 0
  MiSqL = "Select FA16NUMLINEA,FA16DESCRIP, FA16PRECIO, FA16IMPORTE From FA1600 " & _
          "Where FA16IMPORTE <> 0 And FA04CODFACT = ?"
  QryFor(1).SQL = MiSqL
  
  'Seleccionamos los rnfs de esas lineas.
  MiSqL = "Select * From FA0300 " _
        & " Where FA04NUMFACT = ? " _
        & "     And FA16NUMLINEA = ?" _
        & "     AND FA03CANTFACT <>0 "
        
  QryFor(2).SQL = MiSqL
  
  'Comprobamos si es un forfait.
  MiSqL = "Select * From FA1500 Where FA15CODATRIB = ?"
  QryFor(3).SQL = MiSqL
  
  'Buscaremos las claves que nos indiquen el forfait
  MiSqL = "Select * From FA4600 Where FA46CODDESEXTERNO = ?"
  QryFor(4).SQL = MiSqL
  
  'Comprobaremos, si se trata de una pr�tesis busc�ndolo en FA0503J
  MiSqL = "Select * From FA0503J Where FA05CODCATEG = ?"
  QryFor(5).SQL = MiSqL
                                                  
  For x = 1 To 5
    Set QryFor(x).ActiveConnection = objApp.rdoConnect
    QryFor(x).Prepared = True
  Next
  
  '------------------------------------------------------------------------------------------------
  'RECOGER DATOS ESTANCIAS
  '------------------------------------------------------------------------------------------------
  '1) Recogida de los datos
  ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  ' ********************************MODIFICADO POR PATXI *****************
  ' He quitado la l�nea "And AD2500.AD12CODTIPOASIST = '1' " & _
  ' porque se trata de sacar todo lo facturado por estancias
  
  MiSelect = "select  FA0400.FA04CODFACT, FA0400.CI32CODTIPECON, FA0400.CI13CODENTIDAD, " & _
             "FA0400.FA04NUMFACT, FA0400.FA04FECFACTURA, FA0400.AD07CODPROCESO, " & _
             "FA0400.AD01CODASISTENCI, FA0400.FA09CODNODOCONC, FA0400.FA04INDCOMPENSA, " & _
             "FA0400.FA04CANTFACT, AD2500.AD12CODTIPOASIST, CI1300.CI13DESENTIDAD, " & _
             "AD1200.AD12DESTIPOASIST, AD0100.AD01FECINICIO, AD0100.AD01FECFIN, AD0100.AD01DIAGNOSSALI, " & _
             "CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||' '||CI2200.CI22NOMBRE AS PERSONA, CI2200.CI22NUMHISTORIA, " & _
             "CI2200.CI22NUMSEGSOC, AD0200.AD02CODDPTO, AD0200.AD02DESDPTO, AD0500.AD05FECFINRESPON, " & _
             "FA0400.FA04FECHASTA " & _
             "From FA0400,CI1300, AD0500, AD0200, AD0100, CI2200, AD2500, AD1200, AD1100 "
  MiWhere = "Where FA0400.CI32CODTIPECON = CI1300.CI32CODTIPECON " & _
            "And FA0400.CI13CODENTIDAD = CI1300.CI13CODENTIDAD " & _
            "And FA0400.AD01CODASISTENCI = AD0500.AD01CODASISTENCI " & _
            "And FA0400.AD07CODPROCESO = AD0500.AD07CODPROCESO " & _
            "And AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
            "And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
            "And AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "And FA0400.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
            "And AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And AD2500.AD25FECFIN IS NULL " & _
            "And FA0400.FA04FECFACTURA >= TO_DATE(?,'DD/MM/YYYY') " & _
            "And FA0400.FA04FECFACTURA <= LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " & _
            "And FA0400.CI32CODTIPECON = 'S' " & _
            "And FA0400.CI13CODENTIDAD = ? " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And FA0400.FA04INDCOMPENSA <> 2 " & _
            "And FA0400.FA04NUMFACREAL IS NULL " & _
            "And FA0400.FA09CODNODOCONC_FACT = 393 " & _
            "And FA0400.FA04CANTFACT <> 0"
  MiWhere = MiWhere & "AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "AND AD1100.AD11FECFIN IS NULL " & _
            "AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " & _
            " AND EXISTS " & _
            "       (SELECT /*+ rule */ AD01CODASISTENCI " & _
            "       From AD1800 " & _
            "       Where  AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "           AND AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "           AND AD1800.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "           AND AD1800.AD19CODTIPODOCUM = 1 " & _
            "       ) "
           
  ' //////////////////////////////////////////////////////////////////////////
  
  MiOrder = "Order By AD0200.AD02CODDPTO, PERSONA"
  
  
  MiSqL = Trim(MiSelect) & " " & Trim(MiWhere) & " " & Trim(MiOrder)
  QryEstancias.SQL = MiSqL
  Set QryEstancias.ActiveConnection = objApp.rdoConnect
  QryEstancias.Prepared = True
  
  'Cogemos las lineas con precio <> 0
  MiSqL = "Select FA16NUMLINEA,FA16DESCRIP, FA16PRECIO, FA16IMPORTE From FA1600 " & _
          "Where FA16IMPORTE <> 0 And FA04CODFACT = ?"
  QryEst(1).SQL = MiSqL
  
  'Cogemos los rnfs de esas l�neas
  MiSqL = "Select * " _
        & " From FA0300 " _
        & " Where FA04NUMFACT = ? " _
        & "     And FA16NUMLINEA = ?" _
        & "     AND FA03CANTFACT <>0 "
  QryEst(2).SQL = MiSqL
  
  'Comprobamos si son pr�tesis
  MiSqL = "Select * From FA0503J Where FA05CODCATEG = ?"
  QryEst(3).SQL = MiSqL
  
  'Cogemos el nombre de la categor�a
  MiSqL = "Select * From FA0500 Where FA05CODCATEG = ?"
  QryEst(4).SQL = MiSqL
  
  For x = 1 To 4
    Set QryEst(x).ActiveConnection = objApp.rdoConnect
    QryEst(x).Prepared = True
  Next
                
  '------------------------------------------------------------------------------------------------
  'RECOGER DATOS AMBULATORIOS
  '------------------------------------------------------------------------------------------------
  '1) Recogida de los datos
  MiSelect = "select  FA0400.FA04CODFACT, FA0400.CI32CODTIPECON, FA0400.CI13CODENTIDAD, " & _
             "FA0400.FA04NUMFACT, FA0400.FA04FECFACTURA, FA0400.AD07CODPROCESO, " & _
             "FA0400.AD01CODASISTENCI, FA0400.FA09CODNODOCONC, FA0400.FA04INDCOMPENSA, " & _
             "FA0400.FA04CANTFACT, AD2500.AD12CODTIPOASIST, CI1300.CI13DESENTIDAD, " & _
             "AD1200.AD12DESTIPOASIST, AD0100.AD01FECINICIO, AD0100.AD01FECFIN, AD0100.AD01DIAGNOSSALI, " & _
             "CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||' '||CI2200.CI22NOMBRE AS PERSONA, CI2200.CI22NUMHISTORIA, " & _
             "CI2200.CI22NUMSEGSOC, AD0200.AD02CODDPTO, AD0200.AD02DESDPTO, AD0500.AD05FECFINRESPON, " & _
             "FA0400.FA04FECHASTA " & _
             "From FA0400,CI1300, AD0500, AD0200, AD0100, CI2200, AD2500, AD1200, AD1100 "
  MiWhere = "Where FA0400.CI32CODTIPECON = CI1300.CI32CODTIPECON " & _
            "And FA0400.CI13CODENTIDAD = CI1300.CI13CODENTIDAD " & _
            "And FA0400.AD01CODASISTENCI = AD0500.AD01CODASISTENCI " & _
            "And FA0400.AD07CODPROCESO = AD0500.AD07CODPROCESO " & _
            "And AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
            "And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
            "And AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "And FA0400.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
            "And AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And AD2500.AD25FECFIN IS NULL " & _
            "And FA0400.FA04FECFACTURA >= TO_DATE(?,'DD/MM/YYYY') " & _
            "And FA0400.FA04FECFACTURA <= LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " & _
            "And FA0400.CI32CODTIPECON = 'S' " & _
            "And FA0400.CI13CODENTIDAD = ? " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And FA0400.FA04INDCOMPENSA <> 2 " & _
            "And FA0400.FA04NUMFACREAL IS NULL " & _
            "And FA0400.FA04CANTFACT <> 0"
            ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            ' ****************************MODIFICADO POR PATXI***************
  MiWhere = MiWhere & "AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "AND AD1100.AD11FECFIN IS NULL " & _
            "AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " & _
            " AND EXISTS " & _
            "       (SELECT /*+ rule */ AD01CODASISTENCI " & _
            "       From AD1800 " & _
            "       Where  AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "           AND AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "           AND AD1800.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "           AND AD1800.AD19CODTIPODOCUM = 1 " & _
            "       ) "
            ' Pongo que los ambulatorios sean los que no tengan cama facturada.
'MiWhere = MiWhere & " AND NOT EXISTS " & _
            "       (SELECT FA04NUMFACT " & _
            "        FROM    FA0300, " & _
            "                FA0500 " & _
            "        WHERE  FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " & _
            "               AND FA0500.FA08CODGRUPO=5 " & _
            "               AND FA0300.FA04NUMFACT = FA0400.FA04CODFACT " & _
            "        ) "
MiWhere = MiWhere & " AND AD2500.AD12CODTIPOASIST >1 "
'MiWhere = MiWhere & " AND FA0400.FA04FECHASTA >= TO_DATE('1/2/2000','DD/MM/YYYY')"

            ' ////////////////////////////////////////////////////////////////
  MiOrder = "Order By AD0200.AD02CODDPTO, PERSONA"
  MiSqL = Trim(MiSelect) & " " & Trim(MiWhere) & " " & Trim(MiOrder)
  
  QryAmbulatorios.SQL = MiSqL
  Set QryAmbulatorios.ActiveConnection = objApp.rdoConnect
  QryAmbulatorios.Prepared = True
  
  'Seleccionamos las l�neas de factura con precio <> 0
  MiSqL = "Select FA16NUMLINEA,FA16DESCRIP, FA16PRECIO, FA16IMPORTE From FA1600 " & _
          "Where FA16IMPORTE <> 0 And FA04CODFACT = ?"
  QryAmb(1).SQL = MiSqL
    
  MiSqL = "Select * " _
        & " From FA0300 " _
        & " Where FA04NUMFACT = ? " _
        & "     And FA16NUMLINEA = ? " _
        & "     AND FA03CANTFACT <>0 " _
        & " ORDER BY FA05CODCATEG"
  QryAmb(2).SQL = MiSqL
  
  'Una vez obtenidos los rnfs de la l�nea comprobaremos si se trata de
  '1� Consulta, Revision, Asistecia Oncol�gica, Medicaci�n, Pruebas
  MiSqL = "Select * From FA0500 Where FA05CODCATEG = ?"
  QryAmb(3).SQL = MiSqL
  
  'Comprobaremos que no se trata de una pr�tesis, ya que en ese caso no tiene que
  'indicarse la fecha, agrup�ndose como PROTESIS en la cabecera de factura.
  MiSqL = "Select * From FA0503J Where FA05CODCATEG = ?"
  QryAmb(4).SQL = MiSqL
  
  For x = 1 To 4
    Set QryAmb(x).ActiveConnection = objApp.rdoConnect
    QryAmb(x).Prepared = True
  Next
  
  MiSelect = "Select  FA0400.FA04CODFACT, FA0400.CI32CODTIPECON, FA0400.CI13CODENTIDAD," & _
             "FA0400.FA04NUMFACT, FA0400.FA04FECFACTURA, FA0400.AD07CODPROCESO, " & _
             "FA0400.AD01CODASISTENCI, FA0400.FA09CODNODOCONC,FA0400.FA04INDCOMPENSA, " & _
             "FA0400.FA04CANTFACT, AD2500.AD12CODTIPOASIST, CI1300.CI13DESENTIDAD, " & _
             "AD1200.AD12DESTIPOASIST, AD0100.AD01FECINICIO, AD0100.AD01FECFIN, AD0100.AD01DIAGNOSSALI, " & _
             "CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||' '||CI2200.CI22NOMBRE AS PERSONA, " & _
             "CI2200.CI22NUMHISTORIA,CI2200.CI22NUMSEGSOC, AD0200.AD02CODDPTO, AD0200.AD02DESDPTO, " & _
             "AD0500.AD05FECFINRESPON,FA0400.FA09CODNODOCONC_FACT, " & _
             "FA0400.FA04FECHASTA " & _
             "From FA0400, CI1300, AD0500, AD0200, AD0100, CI2200, AD2500, AD1200, AD1100"
  MiWhere = "Where FA0400.CI32CODTIPECON = CI1300.CI32CODTIPECON " & _
            "And FA0400.CI13CODENTIDAD = CI1300.CI13CODENTIDAD " & _
            "And FA0400.AD01CODASISTENCI = AD0500.AD01CODASISTENCI " & _
            "And FA0400.AD07CODPROCESO = AD0500.AD07CODPROCESO " & _
            "And AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
            "And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
            "And AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "And FA0400.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
            "And AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And AD2500.AD25FECFIN IS NULL " & _
            "And FA0400.FA04FECFACTURA >= TO_DATE(?,'DD/MM/YYYY') " & _
            "And FA0400.FA04FECFACTURA <= LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " & _
            "And FA0400.CI32CODTIPECON = 'S' And FA0400.CI13CODENTIDAD  = 'M' " & _
            "And AD0500.AD05FECFINRESPON IS NULL " & _
            "And FA0400.FA04INDCOMPENSA <> 2 " & _
            "And FA0400.FA04NUMFACREAL  IS NULL " & _
            "And FA0400.FA04CANTFACT <> 0 "
              ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            ' ****************************MODIFICADO POR PATXI***************
  MiWhere = MiWhere & "AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "AND AD1100.AD11FECFIN IS NULL " & _
            "AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " & _
            " AND EXISTS " & _
            "       (SELECT /*+ rule */ AD01CODASISTENCI " & _
            "       From AD1800 " & _
            "       Where  AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " & _
            "           AND AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " & _
            "           AND AD1800.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
            "           AND AD1800.AD19CODTIPODOCUM = 1 " & _
            "       ) "
            ' ///////////////////////////////////////////////////////////////
MiOrder = "Order By  AD0200.AD02CODDPTO, PERSONA ASC"
  MiSqL = Trim(MiSelect) & " " & Trim(MiWhere) & " " & Trim(MiOrder)
  QryMadrid.SQL = MiSqL
  Set QryMadrid.ActiveConnection = objApp.rdoConnect
  QryMadrid.Prepared = True

End Sub

Sub HospitDetalle(Mes As String)
Dim Texto As String
  With vsPrinter1
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .Action = paStartDoc
    .MarginTop = 15 * ConvX
    .MarginLeft = 20 * ConvY
    .MarginRight = 20 * ConvY
    Call Me.CabeceraForfaits
    
    .Action = paEndDoc
  End With
End Sub

Sub LineasEstancias()
Dim x, j As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
  With vsPrinter1
    For x = 1 To UBound(LineasEstan)
     'LINEAS DEL FICHERO
      Texto = x 'Contador
      .CurrentX = 24 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = LineasEstan(x).Asegurado 'N�mero de la seguridad social del asegurado
      .CurrentX = 50 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentX = 55 * ConvY
      Texto = LineasEstan(x).Inspe 'Texto Fijo "SUBD"
      .Text = Texto
      .CurrentX = 68 * ConvY
      If Copias = 4 Then
        Texto = LineasEstan(x).Historia & " " & LineasEstan(x).Nombre 'Nombre del Paciente con su historia
      Else
        Texto = LineasEstan(x).Nombre 'Nombre del Paciente
      End If
      While .TextWidth(Texto) > 50 * ConvY
        Texto = Left(Texto, Len(Texto) - 1)
      Wend
      .Text = Texto
      .CurrentX = 125 * ConvY
      Texto = LineasEstan(x).Fecentrada 'Fecha de entrada en formato MM-DD-HH
      .Text = Texto
      .CurrentX = 146 * ConvY
      Texto = LineasEstan(x).fecInterv 'Fecha de la intervenci�n
      .Text = Texto
      .CurrentX = 165 * ConvY
      Texto = LineasEstan(x).FecSalida 'Fecha de alta DD-HH
      .Text = Texto
      Texto = LineasEstan(x).NEstancias 'N�mero de estancias
      .CurrentX = 185 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = Format(LineasEstan(x).CEstancias, "###,###,##0.##") 'Coste de las estancias en este caso el forfait
      .CurrentX = 208 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = Format(LineasEstan(x).CSuplementos, "###,###,##0.##") 'Coste de los suplementos
      .CurrentX = 228 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Diagnos = UCase(Left(LineasEstan(x).Diagnostico, 10)) 'Diagn�stico de salida
      .CurrentX = 253 * ConvY - .TextWidth(Diagnos)
      .Text = Diagnos
      Especia = UCase(Left(LineasEstan(x).Especialidad, 10)) 'Especialidad
      .CurrentX = 275 * ConvY - .TextWidth(Especia)
      .Text = Especia
      .Paragraph = ""
      For j = 1 To UBound(DesgEstanc())
        If DesgEstanc(j).NOrden = LineasEstan(x).NOrden And DesgEstanc(j).Imprimir = True Then
          'Esa linea de mediaci�n se corresponde con la factura
          Texto = DesgEstanc(j).Cantidad
          .CurrentX = 66 * ConvY - .TextWidth(Texto)
          .Text = Texto
          .CurrentX = 68 * ConvY
          Texto = DesgEstanc(j).Descrip
          .Text = Texto
          'Texto = DesgEstanc(j).Precio
          Texto = Format(DesgEstanc(j).Precio, "###,###,##0.##")
          .CurrentX = 208 * ConvY - .TextWidth(Texto)
          .Text = Texto
          'Texto = DesgEstanc(j).Importe
          Texto = Format(DesgEstanc(j).Importe, "###,###,##0.##")
          .CurrentX = 228 * ConvY - .TextWidth(Texto)
          .Text = Texto
          .CurrentX = 253 * ConvY - .TextWidth(Diagnos)
          .Text = Diagnos
          .CurrentX = 275 * ConvY - .TextWidth(Especia)
          .Text = Especia
          .Paragraph = ""
          If .CurrentY > 180 * ConvX Then
            .NewPage
            Call Me.CabeceraEstancias
          End If
        End If
      Next
      .Paragraph = ""
      If .CurrentY > 180 * ConvX Then
        .NewPage
        Call Me.CabeceraEstancias
      End If
    Next
    .Action = paEndDoc
  End With

End Sub

Sub LineasAmbulatorio()
Dim x, j As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim DepAnt As String
Dim nDesg As Integer

  DepAnt = ""
  With vsPrinter1
    For x = 1 To UBound(LineasAmbul)
      If LineasAmbul(x).blnAmbu Then
        'Miramos si el departamento es distinto para ponerlo
        If DepAnt <> LineasAmbul(x).Servicio Then
          Texto = "***********"
          .CurrentX = 25 * ConvY
          .Text = Texto
          Texto = UCase(LineasAmbul(x).Servicio)
          .CurrentX = 55 * ConvY
          .Text = Texto
          .Paragraph = ""
          .Paragraph = ""
          DepAnt = LineasAmbul(x).Servicio
        End If
        'LINEAS DEL FICHERO
        If LineasAmbul(x).blnAmbu Then
              Texto = x 'Contador
              .CurrentX = 30 * ConvY - .TextWidth(Texto)
              .Text = Texto
              Texto = LineasAmbul(x).Asegurado 'N�mero de la seguridad social del asegurado
              .CurrentX = 56 * ConvY - .TextWidth(Texto)
              .Text = Texto
              .CurrentX = 60 * ConvY
              Texto = LineasAmbul(x).Inspe 'Texto Fijo "SUBD"
              .Text = Texto
              .CurrentX = 73 * ConvY
              If Copias = 4 Then
                Texto = LineasAmbul(x).Historia & " " & LineasAmbul(x).Nombre 'Nombre del Paciente con su historia
              Else
                Texto = LineasAmbul(x).Nombre 'Nombre del Paciente
              End If
              While .TextWidth(Texto) > 50 * ConvY
                Texto = Left(Texto, Len(Texto) - 1)
              Wend
              .Text = Texto
              'N� de servicios
              Texto = LineasAmbul(x).NumServicios
              .CurrentX = 139 * ConvY - .TextWidth(Texto)
              .Text = Texto
              'D�as
              Texto = LineasAmbul(x).Dias
              .CurrentX = 141 * ConvY
              .Text = Texto
              'N� de visitas (primera y sucesiva)
              Texto = LineasAmbul(x).PriVisita
              If Texto <> 0 Then
                .CurrentX = 163 * ConvY - .TextWidth(Texto)
                .Text = Texto
              End If
              Texto = LineasAmbul(x).SucVisita
              If Texto <> 0 Then
                .CurrentX = 170 * ConvY - .TextWidth(Texto)
                .Text = Texto
              End If
              'Precio (primera y sucesiva)
              'Texto = LineasAmbul(x).PrimPrecio
              Texto = Format(LineasAmbul(x).PrimPrecio, "###,###,##0")
              If Texto <> 0 Then
                .CurrentX = 192 * ConvY - .TextWidth(Texto)
                .Text = Texto
              End If
              'Texto = LineasAmbul(x).SucPrecio
              Texto = Format(LineasAmbul(x).SucPrecio, "###,###,##0")
              If Texto <> 0 Then
                .CurrentX = 211 * ConvY - .TextWidth(Texto)
                .Text = Texto
              End If
              'Costo total
              LineasAmbul(x).CostoTotal = LineasAmbul(x).PrimPrecio * LineasAmbul(x).PriVisita + LineasAmbul(x).SucPrecio * LineasAmbul(x).SucVisita
              'Texto = LineasAmbul(x).CostoTotal
              Texto = Format(LineasAmbul(x).CostoTotal, "###,###,##0")
              .CurrentX = 228 * ConvY - .TextWidth(Texto)
              .Text = Texto
              'Servicio
              Texto = UCase(LineasAmbul(x).Servicio)
              While .TextWidth(Texto) > 21 * ConvY
                Texto = Left(Texto, Len(Texto) - 1)
              Wend
              .CurrentX = 232 * ConvY
              .Text = Texto
              'Diagn�stico
              Texto = LineasAmbul(x).Diagnostico
              .CurrentX = 277 * ConvY - .TextWidth(Texto)
              .Text = Texto
              .Paragraph = ""
                          
              On Error Resume Next
              nDesg = 0
              nDesg = UBound(DesgAmbul)
              For j = 1 To nDesg
                If DesgAmbul(j).NOrden = LineasAmbul(x).NOrden Then
                  If DesgAmbul(j).PET = 0 And DesgAmbul(j).RNM = 0 Then
                    'Esa linea de mediaci�n se corresponde con la factura
                    Texto = DesgAmbul(j).Cantidad
                    .CurrentX = 69 * ConvY - .TextWidth(Texto)
                    .Text = Texto
                    .CurrentX = 73 * ConvY
                    Texto = DesgAmbul(j).Descrip
                    .Text = Texto
              
                    'D�as
                    Texto = DesgAmbul(j).Dias
                    .CurrentX = 141 * ConvY
                    .Text = Texto
              
                    'Texto = DesgAmbul(j).Precio
                    Texto = Format(DesgAmbul(j).Precio, "###,###,##0")
                    .CurrentX = 211 * ConvY - .TextWidth(Texto)
                    .Text = Texto
                    'Texto = DesgAmbul(j).Importe
                    Texto = Format(DesgAmbul(j).Importe, "###,###,##0")
                    .CurrentX = 228 * ConvY - .TextWidth(Texto)
                    .Text = Texto
                    Texto = UCase(LineasAmbul(x).Servicio)
                    While .TextWidth(Texto) > 21 * ConvY
                      Texto = Left(Texto, Len(Texto) - 1)
                    Wend
                    .CurrentX = 232 * ConvY
                    .Text = Texto
                    Texto = LineasAmbul(x).Diagnostico
                    .CurrentX = 277 * ConvY - .TextWidth(Texto)
                    .Text = Texto
                    .Paragraph = ""
                  End If
                  If .CurrentY > 180 * ConvX Then
                    .NewPage
                    Call Me.CabeceraAmbulatorio
                  End If
                End If
              Next
              .Paragraph = ""
              If .CurrentY > 180 * ConvX Then
                .NewPage
                Call Me.CabeceraAmbulatorio
              End If
          End If
        End If
      Next
      .Action = paEndDoc
    End With
 
End Sub

Sub LineasPET()
Dim x, j As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim DepAnt As String

  DepAnt = ""
  With vsPrinter1
    For x = 1 To UBound(LineasAmbul)
      If LineasAmbul(x).PET = 1 Then
        'Miramos si el departamento es distinto para ponerlo
        If DepAnt <> LineasAmbul(x).Servicio Then
          Texto = "***********"
          .CurrentX = 51 * ConvY - .TextWidth(Texto)
          .Text = Texto
          Texto = UCase(LineasAmbul(x).Servicio)
          .CurrentX = 73 * ConvY
          .Text = Texto
          .Paragraph = ""
          .Paragraph = ""
          DepAnt = LineasAmbul(x).Servicio
        End If
        'LINEAS DEL FICHERO
        Texto = x 'Contador
        .CurrentX = 30 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = LineasAmbul(x).Asegurado 'N�mero de la seguridad social del asegurado
        .CurrentX = 56 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 60 * ConvY
        Texto = LineasAmbul(x).Inspe 'Texto Fijo "SUBD"
        .Text = Texto
        .CurrentX = 73 * ConvY
        If Copias = 4 Then
          Texto = LineasAmbul(x).Historia & " " & LineasAmbul(x).Nombre 'Nombre del Paciente con su historia
        Else
          Texto = LineasAmbul(x).Nombre 'Nombre del Paciente
        End If
        While .TextWidth(Texto) > 50 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .Text = Texto
        'N� de servicios
        Texto = LineasAmbul(x).NumServicios
        .CurrentX = 139 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'D�as
        Texto = LineasAmbul(x).Dias
        .CurrentX = 141 * ConvY
        .Text = Texto
        'N� de visitas (primera y sucesiva)
        Texto = LineasAmbul(x).PriVisita
        If Texto <> 0 Then
          .CurrentX = 163 * ConvY - .TextWidth(Texto)
          .Text = Texto
        End If
        Texto = LineasAmbul(x).SucVisita
        If Texto <> 0 Then
          .CurrentX = 170 * ConvY - .TextWidth(Texto)
          .Text = Texto
        End If
        'Servicio
        Texto = UCase(LineasAmbul(x).Servicio)
        While .TextWidth(Texto) > 21 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .CurrentX = 232 * ConvY
        .Text = Texto
        'Diagn�stico
        Texto = LineasAmbul(x).Diagnostico
        .CurrentX = 277 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .Paragraph = ""
        For j = 1 To UBound(DesgAmbul())
          If DesgAmbul(j).NOrden = LineasAmbul(x).NOrden Then
            If DesgAmbul(j).PET = 1 Then
              'Esa linea de mediaci�n se corresponde con la factura
              Texto = DesgAmbul(j).Cantidad
              .CurrentX = 69 * ConvY - .TextWidth(Texto)
              .Text = Texto
              .CurrentX = 73 * ConvY
              Texto = DesgAmbul(j).Descrip
              .Text = Texto
              'D�as
              Texto = DesgAmbul(j).Dias
              .CurrentX = 141 * ConvY
              .Text = Texto
              Texto = DesgAmbul(j).Precio
              .CurrentX = 211 * ConvY - .TextWidth(Texto)
              .Text = Texto
              Texto = DesgAmbul(j).Importe
              .CurrentX = 228 * ConvY - .TextWidth(Texto)
              .Text = Texto
              Texto = UCase(LineasAmbul(x).Servicio)
              While .TextWidth(Texto) > 21 * ConvY
                Texto = Left(Texto, Len(Texto) - 1)
              Wend
              .CurrentX = 232 * ConvY
              .Text = Texto
              Texto = LineasAmbul(x).Diagnostico
              .CurrentX = 277 * ConvY - .TextWidth(Texto)
              .Text = Texto
              .Paragraph = ""
              If .CurrentY > 180 * ConvX Then
                .NewPage
                Call Me.CabeceraAmbulatorio
              End If
            End If
          End If
        Next
        .Paragraph = ""
        If .CurrentY > 180 * ConvX Then
          .NewPage
          Call Me.CabeceraAmbulatorio
        End If
      End If
    Next
    .Action = paEndDoc
  End With

End Sub
Sub LineasRNM()
Dim x, j As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim DepAnt As String

  DepAnt = ""
  With vsPrinter1
    For x = 1 To UBound(LineasAmbul)
      If LineasAmbul(x).RNM = 1 Then
        'Miramos si el departamento es distinto para ponerlo
        If DepAnt <> LineasAmbul(x).Servicio Then
          Texto = "***********"
          .CurrentX = 40 * ConvY - .TextWidth(Texto)
          .Text = Texto
          Texto = UCase(LineasAmbul(x).Servicio)
          .CurrentX = 57 * ConvY
          .Text = Texto
          .Paragraph = ""
          .Paragraph = ""
          DepAnt = LineasAmbul(x).Servicio
        End If
        'LINEAS DEL FICHERO
        Texto = x 'Contador
        .CurrentX = 30 * ConvY - .TextWidth(Texto)
        .Text = Texto
        Texto = LineasAmbul(x).Asegurado 'N�mero de la seguridad social del asegurado
        .CurrentX = 56 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 60 * ConvY
        Texto = LineasAmbul(x).Inspe 'Texto Fijo "SUBD"
        .Text = Texto
        .CurrentX = 73 * ConvY
        If Copias = 4 Then
          Texto = LineasAmbul(x).Historia & " " & LineasAmbul(x).Nombre 'Nombre del Paciente con su historia
        Else
          Texto = LineasAmbul(x).Nombre 'Nombre del Paciente
        End If
        While .TextWidth(Texto) > 50 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .Text = Texto
        'N� de servicios
        Texto = LineasAmbul(x).NumServicios
        .CurrentX = 139 * ConvY - .TextWidth(Texto)
        .Text = Texto
        'D�as
        Texto = LineasAmbul(x).Dias
        .CurrentX = 141 * ConvY
        .Text = Texto
        'Servicio
        Texto = UCase(LineasAmbul(x).Servicio)
        While .TextWidth(Texto) > 21 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        .CurrentX = 232 * ConvY
        .Text = Texto
        'Diagn�stico
        Texto = LineasAmbul(x).Diagnostico
        .CurrentX = 277 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .Paragraph = ""
        For j = 1 To UBound(DesgAmbul())
          If DesgAmbul(j).NOrden = LineasAmbul(x).NOrden Then
            If DesgAmbul(j).RNM = 1 Then
              'Esa linea de mediaci�n se corresponde con la factura
              Texto = DesgAmbul(j).Cantidad
              .CurrentX = 69 * ConvY - .TextWidth(Texto)
              .Text = Texto
              .CurrentX = 73 * ConvY
              Texto = DesgAmbul(j).Descrip
              .Text = Texto
              Texto = DesgAmbul(j).Precio
              .CurrentX = 211 * ConvY - .TextWidth(Texto)
              .Text = Texto
              Texto = DesgAmbul(j).Importe
              .CurrentX = 228 * ConvY - .TextWidth(Texto)
              .Text = Texto
              Texto = UCase(LineasAmbul(x).Servicio)
              While .TextWidth(Texto) > 21 * ConvY
                Texto = Left(Texto, Len(Texto) - 1)
              Wend
              .CurrentX = 232 * ConvY
              .Text = Texto
              Texto = LineasAmbul(x).Diagnostico
              .CurrentX = 277 * ConvY - .TextWidth(Texto)
              .Text = Texto
              .Paragraph = ""
            End If
          End If
        Next
        If .CurrentY > 180 * ConvX Then
          .NewPage
          Call Me.CabeceraAmbulatorio
        End If
        .Paragraph = ""
        If .CurrentY > 180 * ConvX Then
          .NewPage
          Call Me.CabeceraAmbulatorio
        End If
      End If
    Next
    .Action = paEndDoc
  End With

End Sub

Sub LineasForfaits()
Dim x, j, Cont As Integer
Dim Texto As String
Dim Diag, Proc As String

  With vsPrinter1
    For x = 1 To UBound(LineasForf)
      'Comprobaremos que no tenemos que realizar un salto de p�gina para mantener el paciente
      'y los forfaits unidos.
      If .CurrentY + (200 * (LineasForf(x).Forfaits + 1)) > 190 * ConvX Then
        .NewPage
        Call Me.CabeceraForfaits
      End If
     'LINEAS DEL FICHERO
      Texto = x 'Contador
      .CurrentX = 28 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = LineasForf(x).Asegurado 'N�mero de la seguridad social del asegurado
      .CurrentX = 50 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentX = 55 * ConvY
      Texto = LineasForf(x).Inspe 'Texto Fijo "FORF"
      .Text = Texto
      .CurrentX = 68 * ConvY
      If Copias = 4 Then
        Texto = LineasForf(x).Historia & " " & LineasForf(x).Nombre 'Nombre del Paciente
      Else
        Texto = LineasForf(x).Nombre 'Nombre del Paciente
      End If
      While .TextWidth(Texto) > 50 * ConvY
        Texto = Left(Texto, Len(Texto) - 1)
      Wend
      .Text = Texto
      .CurrentX = 125 * ConvY
      Texto = LineasForf(x).Fecentrada 'Fecha de entrada en formato MM-DD-HH
      .Text = Texto
      .CurrentX = 146 * ConvY
      Texto = LineasForf(x).fecInterv 'Fecha de la intervenci�n
      .Text = Texto
      .CurrentX = 165 * ConvY
      Texto = LineasForf(x).FecSalida 'Fecha de alta DD-HH
      .Text = Texto
      Texto = Format(LineasForf(x).CEstancias, "###,###,##0.##") 'Coste de las estancias en este caso el forfait
      .CurrentX = 208 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = Format(LineasForf(x).CSuplementos, "###,###,##0.##") 'Coste de los suplementos
      .CurrentX = 228 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = LineasForf(x).Diagnostico 'Diagn�stico de salida
      .CurrentX = 253 * ConvY - .TextWidth(Texto)
      .Text = Texto
      Texto = LineasForf(x).Especialidad 'Especialidad
      .CurrentX = 275 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .Paragraph = ""
      For j = 1 To UBound(DesgForf)
        If DesgForf(j).NOrden = LineasForf(x).NOrden Then
          .CurrentX = 60 * ConvY
            Cont = 0
          Texto = ""
          Diag = UCase(DesgForf(j).CodDiag & " " & DesgForf(j).Descrip)
          Proc = UCase(DesgForf(j).CodProc & " " & DesgForf(j).DescProc)
          While .TextWidth(Texto) <= 78 * ConvY And Cont < Len(Diag)
            Cont = Cont + 1
            Texto = Left(Diag, Cont)
          Wend
          .Text = Texto
          .CurrentX = 140 * ConvY
          Texto = Proc 'Descripci�n 2
          .Text = Texto
          .Paragraph = ""
        End If
      Next
      .Paragraph = ""
    Next
    .Action = paEndDoc
  End With
End Sub


Sub OrdenarAmbulatorio()
Dim Cont1 As Integer
Dim Cont2 As Integer
Dim TmpCant As Long
Dim TmpDesc As String
Dim tmpPrecio As Double
Dim tmpMoneda As String
Dim tmpImpor As Double
Dim tmpOrden As Integer
Dim tmpPET As Integer
Dim tmpRMN As Integer

    ' ORDENAR LAS LINEAS
    For Cont1 = UBound(CabeceraAmbul) To 1 Step -1
      For Cont2 = Cont1 To UBound(CabeceraAmbul) - 1
        If CabeceraAmbul(Cont2).orden > CabeceraAmbul(Cont2 + 1).orden Then
          'Copiar a dos variables los valores del siguiente
          TmpCant = CabeceraAmbul(Cont2 + 1).Cantidad
          TmpDesc = CabeceraAmbul(Cont2 + 1).Descripcion
          tmpPrecio = CabeceraAmbul(Cont2 + 1).Precio
          tmpMoneda = CabeceraAmbul(Cont2 + 1).Moneda
          tmpImpor = CabeceraAmbul(Cont2 + 1).Importe
          tmpOrden = CabeceraAmbul(Cont2 + 1).orden
          tmpPET = CabeceraAmbul(Cont2 + 1).PET
          tmpRMN = CabeceraAmbul(Cont2 + 1).RNM
          'Copiar los valores al siguiente
          CabeceraAmbul(Cont2 + 1).Cantidad = CabeceraAmbul(Cont2).Cantidad
          CabeceraAmbul(Cont2 + 1).Descripcion = CabeceraAmbul(Cont2).Descripcion
          CabeceraAmbul(Cont2 + 1).Importe = CabeceraAmbul(Cont2).Importe
          CabeceraAmbul(Cont2 + 1).Moneda = CabeceraAmbul(Cont2).Moneda
          CabeceraAmbul(Cont2 + 1).orden = CabeceraAmbul(Cont2).orden
          CabeceraAmbul(Cont2 + 1).Precio = CabeceraAmbul(Cont2).Precio
          CabeceraAmbul(Cont2 + 1).PET = CabeceraAmbul(Cont2).PET
          CabeceraAmbul(Cont2 + 1).RNM = CabeceraAmbul(Cont2).RNM
          'Copiar los valores al anterior
          CabeceraAmbul(Cont2).Cantidad = TmpCant
          CabeceraAmbul(Cont2).Descripcion = TmpDesc
          CabeceraAmbul(Cont2).Importe = tmpImpor
          CabeceraAmbul(Cont2).Moneda = tmpMoneda
          CabeceraAmbul(Cont2).orden = tmpOrden
          CabeceraAmbul(Cont2).Precio = tmpPrecio
          CabeceraAmbul(Cont2).PET = tmpPET
          CabeceraAmbul(Cont2).RNM = tmpRMN
        ElseIf CabeceraAmbul(Cont2).orden < CabeceraAmbul(Cont2 + 1).orden Then
          Exit For
        End If
      Next
    Next

End Sub

Sub RecogerAmbulatoriaMadrid(factura As Long)
Dim Cont As Integer
Dim ContLin As Integer
Dim rsFact As rdoResultset
Dim rsRNFs As rdoResultset
Dim rsCateg As rdoResultset
Dim rsProtesis As rdoResultset
Dim Protesis As Boolean
Dim AsignarA As String
Dim Encontrado As Boolean
Dim ContArray As Integer
Dim i As Integer
Dim LinCab As Long
Dim LinMad As Long
Dim Linea As Integer

  Cont = 0
  ContLin = 0
  ContArray = 0
  On Error Resume Next
  LinMad = UBound(LinMadrid)
  If Err <> 0 Then
    LinMad = 0
    On Error GoTo 0
  End If
  'Recogemos de la base de datos todas las l�neas de la factura
  QryAmb(1).rdoParameters(0) = factura
  Set rsFact = QryAmb(1).OpenResultset
  If Not rsFact.EOF Then
    While Not rsFact.EOF
      LinMad = LinMad + 1
      ReDim Preserve LinMadrid(1 To LinMad)
      'Como tanto pr�tesis como medicaci�n est�n desglosadas a�adimos una linea a Madrid
      'por cada l�nea de factura.
      LinMadrid(LinMad).Asegurado = Asegurado
      LinMadrid(LinMad).Historia = Historia
      LinMadrid(LinMad).Nombre = Nombre
      LinMadrid(LinMad).Servicio = Servicio
      LinMadrid(LinMad).Diagnostico = Diagnostico
      'Buscaremos los rnfs para ver si son consultas o por el contrario son pruebas
      QryAmb(2).rdoParameters(0) = factura
      QryAmb(2).rdoParameters(1) = rsFact("FA16NUMLINEA")
      Set rsRNFs = QryAmb(2).OpenResultset
      If Not rsRNFs.EOF Then
        While Not rsRNFs.EOF
          'Una vez obtenidos los rnfs de la linea comprobaremos si se trata de
          '1� Consulta, Revisi�n, Asistencia Oncol�gica, Medicaci�n, Pruebas
          QryAmb(3).rdoParameters(0) = rsRNFs("FA05CODCATEG")
          Set rsCateg = QryAmb(3).OpenResultset
          If Not rsCateg.EOF Then
            If rsCateg("FA05CODORIGC2") = 0 Then
              If rsCateg("FA05CODORIGC1") = 109 Or rsCateg("FA05CODORIGC2") = 154 Then
                'Asistencia oncol�gica, miraremos si es 1� Consulta o revisi�n
                If rsCateg("FA05CODORIGC3") = 1 Or rsCateg("FA05CODORIGC3") = 7 Then
                  'Primera Consulta
                  LinMadrid(LinMad).NumServ = LinMadrid(LinMad).NumServ + 1
                  LinMadrid(LinMad).DiasServ = LinMadrid(LinMad).DiasServ & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                  LinMadrid(LinMad).PriVisit = LinMadrid(LinMad).PriVisit + 1
                  LinMadrid(LinMad).PriPrecio = rsRNFs("FA03PRECIOFACT")
                ElseIf rsCateg("FA05CODORIGC3") = 2 Then
                  'Revisi�n
                  LinMadrid(LinMad).NumServ = LinMadrid(LinMad).NumServ + 1
                  LinMadrid(LinMad).DiasServ = LinMadrid(LinMad).DiasServ & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                  LinMadrid(LinMad).SucVisit = LinMadrid(LinMad).SucVisit + 1
                  LinMadrid(LinMad).SucPrecio = rsRNFs("FA03PRECIOFACT")
                End If
                'Lo asignaremos a la factura de asistencias oncol�gicas.
                AsignarA = "ASISONC"
              ElseIf (rsCateg("FA05CODORIGC3") = 1 Or rsCateg("FA05CODORIGC3") = 7) And rsCateg("FA05CODORIGC2") = 0 Then
                'Primera Consulta
                LinMadrid(LinMad).NumServ = LinMadrid(LinMad).NumServ + 1
                LinMadrid(LinMad).PriVisit = LinMadrid(LinMad).PriVisit + 1
                LinMadrid(LinMad).DiasServ = LinMadrid(LinMad).DiasServ & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                LinMadrid(LinMad).PriPrecio = rsRNFs("FA03PRECIOFACT")
                'En la cabecera lo asignaremos a la factura de primeras visitas
                AsignarA = "PRIVIS"
              ElseIf rsCateg("FA05CODORIGC3") = 2 And rsCateg("FA05CODORIGC2") = 0 Then
                'Revisi�nes
                LinMadrid(LinMad).NumServ = LinMadrid(LinMad).NumServ + 1
                LinMadrid(LinMad).SucVisit = LinMadrid(LinMad).SucVisit + 1
                LinMadrid(LinMad).DiasServ = LinMadrid(LinMad).DiasServ & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                LinMadrid(LinMad).SucPrecio = rsRNFs("FA03PRECIOFACT")
                'En la cabecera lo asginaremos a la factura de revisiones
                AsignarA = "REVIS"
              Else
                AsignarA = "OTROS"
              End If
            ElseIf rsCateg("FA08CODGRUPO") = 1 Then
              'Medicaci�n (A�adiremos la linea y lo asignaremos a la factura de Medicaci�n)
              LinMad = LinMad + 1
              ReDim Preserve LinMadrid(1 To LinMad)
              LinMadrid(LinMad).Cantidad = rsRNFs("FA03CANTFACT")
              LinMadrid(LinMad).Descrip = rsFact("FA16DESCRIP")
              LinMadrid(LinMad).Precio = rsRNFs("FA03PRECIOFACT")
              LinMadrid(LinMad).Importe = LinMadrid(LinMad).Precio * LinMadrid(LinMad).Cantidad
              LinMadrid(LinMad).Servicio = Servicio
              LinMadrid(LinMad).Diagnostico = Diagnostico
              LinMadrid(LinMad).Nombre = Nombre
              LinMadrid(LinMad).Asegurado = Asegurado
              LinMadrid(LinMad).Historia = Historia
              AsignarA = "CITOST"
            Else
              'Comprobaremos que no se trata de una pr�tesis, ya que en ese caso la incluiremos
              'en la factura de pr�tesis.
              QryAmb(4).rdoParameters(0) = rsRNFs("FA05CODCATEG")
              Set rsProtesis = QryAmb(4).OpenResultset
              If Not rsProtesis.EOF Then
                Protesis = True
              Else
                Protesis = False
              End If
              'Pruebas (A�adiremos la l�nea de desglose, si no existe y en cabecera lo a�adiremos
              '         a su propia linea, as� como las fechas en el desglose)
              'Para a�adir la linea de desglose comprobaremos que no sea un pet
              'LinMad = LinMad + 1
              'ReDim Preserve LinMadrid(1 To LinMad)
              LinMadrid(LinMad).Cantidad = LinMadrid(LinMad).Cantidad + rsRNFs("FA03CANTFACT")
              LinMadrid(LinMad).Descrip = rsFact("FA16DESCRIP")
              LinMadrid(LinMad).Precio = 0 & rsRNFs("FA03PRECIOFACT")
              LinMadrid(LinMad).Importe = rsFact("FA16IMPORTE")
              LinMadrid(LinMad).DiasServ = LinMadrid(LinMad).DiasServ & Me.FechaAFormatoDD(rsRNFs("FA03FECHA")) & ","
              If Protesis = True Then
                'Lo a�adimos en la l�nea de protesis
                AsignarA = "PROTESIS"
              ElseIf Protesis = False Then
                'Buscamos en la cabecera y si no est� la l�nea la creamos.
                AsignarA = "OTROS"
              End If
            End If
            On Error Resume Next
            LinCab = UBound(MadridCab)
            If Err <> 0 Then
              LinCab = 0
              On Error GoTo 0
            End If
            Select Case AsignarA
            Case "ASISONC"
              If LinCab = 0 Then
                Encontrado = False
              Else
                Encontrado = False
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "Asistencia oncol�gica" Then
                    Encontrado = True
                    LinCab = Cont
                    Exit For
                  End If
                Next
              End If
              If Encontrado = False Then
                LinCab = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Desc1 = "Asistencia oncol�gica"
                MadridCab(LinCab).Cantidad = 1
                MadridCab(LinCab).Precio = rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).orden = LinCab
                MadridCab(LinCab).Tipo = 4
              Else
                MadridCab(LinCab).Cantidad = MadridCab(LinCab).Cantidad + 1
              End If
              LinMadrid(LinMad).orden = LinCab
            Case "PRIVIS"
              If LinCab = 0 Then
                Encontrado = False
              Else
                Encontrado = False
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "Primera Consulta" Then
                    Encontrado = True
                    LinCab = Cont
                    Exit For
                  End If
                Next
              End If
              If Encontrado = False Then
                Linea = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Desc1 = "Primera Consulta"
                MadridCab(LinCab).Cantidad = 1
                MadridCab(LinCab).Precio = rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).orden = LinCab
                MadridCab(LinCab).Tipo = 4
              Else
                MadridCab(LinCab).Cantidad = MadridCab(LinCab).Cantidad + 1
              End If
              LinMadrid(LinMad).orden = LinCab
            Case "REVIS"
              If LinCab = 0 Then
                Encontrado = False
              Else
                Encontrado = False
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "Revisi�n" Then
                    Encontrado = True
                    LinCab = Cont
                    Exit For
                  End If
                Next
              End If
              If Encontrado = False Then
                LinCab = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Desc1 = "Revisi�n"
                MadridCab(LinCab).Cantidad = 1
                MadridCab(LinCab).Precio = rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).orden = LinCab
                MadridCab(LinCab).Tipo = 4
              Else
                MadridCab(LinCab).Cantidad = MadridCab(LinCab).Cantidad + 1
              End If
              LinMadrid(LinMad).orden = LinCab
            Case "CITOST"
              If LinCab = 0 Then
                Encontrado = False
              Else
                Encontrado = False
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "MEDICACI�N" Then
                    Encontrado = True
                    LinCab = Cont
                    Exit For
                  End If
                Next
              End If
              If Encontrado = False Then
                LinCab = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Desc1 = "MEDICACI�N"
                MadridCab(LinCab).Importe = rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).orden = LinCab
                MadridCab(LinCab).Tipo = 3
              Else
                MadridCab(LinCab).Importe = MadridCab(LinCab).Importe + (rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT"))
              End If
              LinMadrid(LinMad).orden = LinCab
            Case "PROTESIS"
              If LinCab = 0 Then
                Encontrado = False
              Else
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "Pr�tesis" Then
                    Encontrado = True
                    LinCab = Cont
                    Exit For
                  End If
                Next
              End If
              If Encontrado = False Then
                LinCab = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Desc1 = "Pr�tesis"
                MadridCab(LinCab).Importe = rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).orden = LinCab
                MadridCab(LinCab).Tipo = 3
              Else
                MadridCab(LinCab).Importe = MadridCab(LinCab).Importe + (rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT"))
              End If
              LinMadrid(LinMad).orden = LinCab
            Case Else
              Encontrado = False
              If LinCab = 0 Then
                Encontrado = False
              Else
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = rsFact("FA16DESCRIP") Then
                    Encontrado = True
                    LinCab = Cont
                    Exit For
                  End If
                Next
              End If
              If Encontrado = False Then
                LinCab = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Cantidad = rsRNFs("FA03CANTFACT")
                MadridCab(LinCab).Desc1 = rsFact("FA16DESCRIP")
                MadridCab(LinCab).Precio = rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).orden = LinCab
                MadridCab(LinCab).Tipo = 3
              Else
                MadridCab(LinCab).Cantidad = MadridCab(LinCab).Cantidad + rsRNFs("FA03CANTFACT")
              End If
              LinMadrid(LinMad).orden = LinCab
            End Select
          End If
          rsRNFs.MoveNext
        Wend
      End If
      rsFact.MoveNext
    Wend
  End If
End Sub

Sub RecogerDatos(Entidad As String)
Dim MiSqL As String
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiRs As rdoResultset
Dim Cont As Integer
Dim ContLin As Integer
Dim ContResu As Integer
Dim Asignado As Boolean
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim sqlRNFs As String
Dim rsRNFs As rdoResultset
Dim sqlAtrib As String
Dim rsAtrib As rdoResultset
Dim sqlCodExt As String
Dim rsCodExt As rdoResultset
Dim sqlProtesis As String
Dim rsProtesis As rdoResultset
Dim SQL As String
Dim rsSequ As rdoResultset
Dim x As Integer
Dim FactForfMala As String
Dim qryDatos As rdoQuery

  'Inicializamos los contadores
  Cont = 0
  ContLin = 0
  ContResu = 0
  ProtesisForfaits = 0
  
  'Le indicamos a las SQL de selecci�n la fecha y la entidad
  'QryForfaits.rdoParameters(0) = "31/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  
  '///////////////////////////////////////////////////////////////////
  ' ******************CORREGIDO POR PATXI ***************************
  QryForfaits.rdoParameters(0) = "1/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  QryForfaits.rdoParameters(1) = "1/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  QryForfaits.rdoParameters(2) = Me.txtCodEntidad
  '******************************************************************
  '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  
  Set MiRs = QryForfaits.OpenResultset
  FacturaForf = ""
  FactForfMala = ""
  If Not MiRs.EOF Then
    Hospitalizados = True
    While Not MiRs.EOF
        If FacturaForf = "" And Left(MiRs("FA04NUMFACT"), 3) = "13/" Then
            FacturaForf = MiRs("FA04NUMFACT")
        ElseIf FactForfMala = "" Then
            FactForfMala = MiRs("FA04NUMFACT")
        End If
      Cont = Cont + 1
      ReDim Preserve LineasForf(1 To Cont)
      LineasForf(Cont).NOrden = Cont
      LineasForf(Cont).Asegurado = MiRs("CI22NUMSEGSOC") & ""
      LineasForf(Cont).Inspe = "FORF"
      LineasForf(Cont).Historia = MiRs("CI22NUMHISTORIA") & ""
      LineasForf(Cont).Nombre = MiRs("PERSONA") & ""
      LineasForf(Cont).Fecentrada = Me.FechaAFormatoMMDDHH(MiRs("AD01FECINICIO"))
      LineasForf(Cont).fecInterv = "00-00"
      LineasForf(Cont).FecSalida = Me.FechaAFormatoDDHH(CDate(MiRs("AD01FECFIN") & ""))
      LineasForf(Cont).CEstancias = 0
      LineasForf(Cont).CSuplementos = 0
      LineasForf(Cont).Diagnostico = Left(UCase(MiRs("AD01DIAGNOSSALI")) & "", 9)
      LineasForf(Cont).Especialidad = Left(UCase(MiRs("AD02DESDPTO")) & "", 9)
      'Buscamos las l�neas
      QryFor(1).rdoParameters(0) = MiRs("FA04CODFACT")
      Set rsFact = QryFor(1).OpenResultset
      If Not rsFact.EOF Then
        While Not rsFact.EOF
          'Buscamos los rnfs
          QryFor(2).rdoParameters(0) = MiRs("FA04CODFACT")
          QryFor(2).rdoParameters(1) = rsFact("FA16NUMLINEA")
          Set rsRNFs = QryFor(2).OpenResultset
          If Not rsRNFs.EOF Then
            While Not rsRNFs.EOF
              If rsRNFs!FA03cantfact <> 0 Then
                'Comprobamos si es un forfait.
                QryFor(3).rdoParameters(0) = rsRNFs("FA15CODATRIB")
                Set rsAtrib = QryFor(3).OpenResultset
                If Not rsAtrib.EOF Then
                  If rsAtrib("FA15INDSUFICIENTE") = 1 And rsAtrib("FA15INDFACTOBLIG") = 1 Then
                    'Es un forfait
                    LineasForf(Cont).CEstancias = LineasForf(Cont).CEstancias + rsFact("FA16IMPORTE")
                    'Buscaremos las claves que nos indiquen el forfait
                    If Not IsNull(rsAtrib("FA46CODDESEXTERNO")) Then
                      QryFor(4).rdoParameters(0) = rsAtrib("FA46CODDESEXTERNO")
                      Set rsCodExt = QryFor(4).OpenResultset
                      If Not rsCodExt.EOF Then
                        'Asignar a una tabla los valores
                        ContLin = ContLin + 1
                        ReDim Preserve DesgForf(1 To ContLin)
                        DesgForf(ContLin).CodDiag = rsCodExt("FA46CODDIAG") & ""
                        DesgForf(ContLin).Descrip = rsCodExt("FA46DESCRIP") & ""
                        DesgForf(ContLin).CodProc = rsCodExt("FA46CODPROC") & ""
                        DesgForf(ContLin).DescProc = rsCodExt("FA46DESPROC") & ""
                        DesgForf(ContLin).NOrden = Cont
                        'Comprobaremos si est� en la tabla que utilizaremos para el resumen, y en caso
                        'contrario la incluiremos en la tabla
                        If ContResu = 0 Then 'Es el primer forfait
                          ContResu = ContResu + 1
                          ReDim Preserve ResumenForf(1 To ContResu)
                          ResumenForf(ContResu).Cantidad = 1
                          ResumenForf(ContResu).Diagnost = rsCodExt("FA46CODDIAG") & " " & rsCodExt("FA46DESCRIP")
                          ResumenForf(ContResu).proceso = rsCodExt("FA46CODPROC") & " " & rsCodExt("FA46DESPROC")
                          ResumenForf(ContResu).Precio = rsFact("FA16IMPORTE")
                        Else
                          Asignado = False
                          For x = 1 To UBound(ResumenForf())
                            If ResumenForf(x).proceso = rsCodExt("FA46CODPROC") & " " & rsCodExt("FA46DESPROC") Then
                              'ResumenForf(x).Cantidad = ResumenForf(x).Cantidad + 1
                              ResumenForf(x).Cantidad = ResumenForf(x).Cantidad + rsRNFs!FA03cantfact
                              Asignado = True
                            End If
                          Next
                          If Asignado = False Then
                            ContResu = ContResu + 1
                            ReDim Preserve ResumenForf(1 To ContResu)
                            ResumenForf(ContResu).Cantidad = 1
                            ResumenForf(ContResu).Diagnost = rsCodExt("FA46CODDIAG") & " " & rsCodExt("FA46DESCRIP")
                            ResumenForf(ContResu).proceso = rsCodExt("FA46CODPROC") & " " & rsCodExt("FA46DESPROC")
                            ResumenForf(ContResu).Precio = rsFact("FA16IMPORTE")
                          End If
                        End If
                        'Indicamos al paciente el n�mero de forfaits que se le han aplicado
                        LineasForf(Cont).Forfaits = LineasForf(Cont).Forfaits + 1
                      End If
                    End If
                  Else
                    'Comprobaremos, si se trata de una pr�tesis busc�ndolo en FA0503J
                    QryFor(5).rdoParameters(0) = rsRNFs("FA05CODCATEG")
                    Set rsProtesis = QryFor(5).OpenResultset
                    If Not rsProtesis.EOF Then
                      LineasForf(Cont).CSuplementos = LineasForf(Cont).CSuplementos + CLng(0 & rsRNFs("FA03PRECIOFACT") * rsRNFs("FA03CANTFACT"))
                      ProtesisForfaits = ProtesisForfaits + CLng(0 & rsRNFs("FA03PRECIOFACT") * rsRNFs("FA03CANTFACT"))
                    End If
                  End If
                End If
              End If
              rsRNFs.MoveNext
            Wend
          End If
          rsFact.MoveNext
        Wend
      End If
      MiRs.MoveNext
    Wend
    
    If FacturaForf = "" Then
        ' Si no se ha encontrado ninguna factura de tipo "13/" -> se genera
        SQL = "SELECT   FA04NUMFACT_SEQUENCE.NEXTVAL " _
            & " FROM    DUAL"
        Set rsSequ = objApp.rdoConnect.OpenResultset(SQL)
        FacturaForf = "13/" & rsSequ(0)
        rsSequ.Close
        
        ' Se actualiza el n�mero de factura
        SQL = "UPDATE FA0400 " _
            & " SET     FA04NUMFACT = ? " _
            & " WHERE   FA04NUMFACT = ? "
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = FacturaForf
        qryDatos(1) = FactForfMala
        qryDatos.Execute
        qryDatos.Close
        'FacturaForf = FactForfMala
    End If
  Else
    'No se ha recogido ning�n paciente.
    Hospitalizados = False
  End If
End Sub
Sub RecogerDatosMadrid()
Dim MiSqL As String
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiRs As rdoResultset
Dim Cont As Integer
Dim ContLin As Integer
Dim ContResu As Integer
Dim Asignado As Boolean
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim sqlRNFs As String
Dim rsRNFs As rdoResultset
Dim sqlAtrib As String
Dim rsAtrib As rdoResultset
Dim sqlCodExt As String
Dim rsCodExt As rdoResultset
Dim sqlProtesis As String
Dim rsProtesis As rdoResultset
Dim x As Integer
Dim forf As Integer
Dim Amb As Integer
Dim Est As Integer

  'Inicializamos los contadores
  Cont = 0
  ContLin = 0
  ContResu = 0
  ProtesisForfaits = 0
  
  'Le indicamos a las SQL de selecci�n la fecha y la entidad
  QryMadrid.rdoParameters(0) = "01/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  QryMadrid.rdoParameters(1) = "01/" & Format(CInt(Me.txtMeses.Text) + 1, "00") & "/" & Me.txtAnyo.Text
  
  Set MiRs = QryMadrid.OpenResultset
  If Not MiRs.EOF Then
    Madrid = True
    
    While Not MiRs.EOF
      'Rellenamos los campos comunes a todos las lineas
      Asegurado = MiRs("CI22NUMSEGSOC")
      Historia = MiRs("CI22NUMHISTORIA")
      Nombre = MiRs("PERSONA")
      Servicio = MiRs("AD02DESDPTO")
      Diagnostico = MiRs("AD01DIAGNOSSALI")
      FechaEnt = IIf(Not IsNull(MiRs("AD01FECINICIO")), Me.FechaAFormatoMMDDHH(MiRs("AD01FECINICIO")), "")
      FechaSal = IIf(Not IsNull(MiRs("AD01FECFIN")), Me.FechaAFormatoDDHH(MiRs("AD01FECFIN")), "")
      'Comprobaremos cual ha sido el nodo de concierto con el que ha sido facturado.
      If Not IsNull(MiRs("FA09CODNODOCONC_FACT")) Then
        Select Case MiRs("FA09CODNODOCONC_FACT")
        Case 389  'Forfait Ambulatorio
          Call Me.RecogerAmbulatoriaMadrid(MiRs("FA04CODFACT"))
          Amb = Amb + 1
        Case 387  'Forfait Catarata, Pr�tesis de rodilla y cadera
          Call Me.RecogerForfaitsMadrid(MiRs("FA04CODFACT"))
          forf = forf + 1
        Case 385  'Forfait de Intervenciones
          Call Me.RecogerForfaitsMadrid(MiRs("FA04CODFACT"))
          forf = forf + 1
        Case 388  'Forfait Pruebas
          Call Me.RecogerForfaitsMadrid(MiRs("FA04CODFACT"))
          forf = forf + 1
        Case 454  'Acto M�dico
        Case 393  'Estancias
          Call Me.RecogerEstanciasMadrid(MiRs("FA04CODFACT"))
          Est = Est + 1
        Case Else
        End Select
      End If
      MiRs.MoveNext
    Wend
    MsgBox "Forfaits " & forf & " Estancias " & Est & " Ambulatorios " & Amb
  Else
    'No se ha recogido ning�n paciente.
    Madrid = False
  End If
End Sub

Sub RecogerDatosEstancias(Entidad As String)
Dim MiSqL As String
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiRs As rdoResultset
Dim Cont As Integer
Dim ContLin As Integer
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim sqlRNFs As String
Dim rsRNFs As rdoResultset
Dim Citostaticos As Boolean
Dim sqlProtesis As String
Dim rsProtesis As rdoResultset
Dim FactEstancMala As String
Dim SQL As String
Dim rsSequ As rdoResultset
Dim qryDatos As rdoQuery

  Cont = 0
  ContLin = 0
  TotEstancias = 0
  PrecioEstanc = 0
  ImporteEstanc = 0
  ImporteCitost = 0
  ImporteProtesis = 0
  ImporteHemoderivados = 0
  
  'QryEstancias.rdoParameters(0) = "31/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo
  
  
  '///////////////////////////////////////////////////////////////////
  ' ******************CORREGIDO POR PATXI ***************************
  QryEstancias.rdoParameters(0) = "1/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo
  QryEstancias.rdoParameters(1) = "1/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo
  QryEstancias.rdoParameters(2) = Me.txtCodEntidad
  '******************************************************************
  '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  FacturaEstanc = ""
  FactEstancMala = ""
  Set MiRs = QryEstancias.OpenResultset
  If Not MiRs.EOF Then
    PacEstancias = True
    While Not MiRs.EOF
        If FacturaEstanc = "" And Left(MiRs("FA04NUMFACT"), 3) = "13/" Then
            FacturaEstanc = MiRs("FA04NUMFACT")
        ElseIf FactEstancMala = "" Then
            FactEstancMala = MiRs("FA04NUMFACT")
        End If
      Cont = Cont + 1
      ReDim Preserve LineasEstan(1 To Cont)
      LineasEstan(Cont).NOrden = Cont
      LineasEstan(Cont).Asegurado = MiRs("CI22NUMSEGSOC") & ""
      LineasEstan(Cont).Inspe = "SUBD"
      LineasEstan(Cont).Historia = MiRs("CI22NUMHISTORIA") & ""
      LineasEstan(Cont).Nombre = MiRs("PERSONA") & ""
      LineasEstan(Cont).Fecentrada = Me.FechaAFormatoMMDDHH(MiRs("AD01FECINICIO"))
      LineasEstan(Cont).fecInterv = "00-00"
      LineasEstan(Cont).FecSalida = Me.FechaAFormatoDDHH(IIf(IsNull(MiRs("AD01FECFIN")), MiRs!FA04FECHASTA, MiRs("AD01FECFIN")))
      LineasEstan(Cont).NEstancias = 0
      LineasEstan(Cont).CEstancias = 0
      LineasEstan(Cont).CSuplementos = 0
      LineasEstan(Cont).Diagnostico = MiRs("AD01DIAGNOSSALI") & ""
      LineasEstan(Cont).Especialidad = MiRs("AD02DESDPTO")
      
      QryEst(1).rdoParameters(0) = MiRs("FA04CODFACT")
      Set rsFact = QryEst(1).OpenResultset
      If Not rsFact.EOF Then
        While Not rsFact.EOF
          Citostaticos = False
          If Trim(rsFact("FA16DESCRIP")) = "Estancias" Then
            LineasEstan(Cont).CEstancias = LineasEstan(Cont).CEstancias + rsFact("FA16IMPORTE")
            ImporteEstanc = ImporteEstanc + rsFact("FA16IMPORTE")
          Else
            ContLin = ContLin + 1
            ReDim Preserve DesgEstanc(1 To ContLin)
            DesgEstanc(ContLin).NOrden = Cont
            DesgEstanc(ContLin).Descrip = rsFact("FA16DESCRIP") & ""
            DesgEstanc(ContLin).Importe = 0 & rsFact("FA16IMPORTE")
            DesgEstanc(ContLin).Imprimir = True
          End If
          QryEst(2).rdoParameters(0) = MiRs("FA04CODFACT")
          QryEst(2).rdoParameters(1) = rsFact("FA16NUMLINEA")
          Set rsRNFs = QryEst(2).OpenResultset
          If Not rsRNFs.EOF Then
            While Not rsRNFs.EOF
              If Trim(rsFact("FA16DESCRIP")) = "Estancias" Then
                LineasEstan(Cont).NEstancias = LineasEstan(Cont).NEstancias + rsRNFs("FA03CANTFACT")
                TotEstancias = TotEstancias + rsRNFs("FA03CANTFACT")
                PrecioEstanc = 0 & rsRNFs("FA03PRECIOFACT")
              Else
                QryEst(3).rdoParameters(0) = rsRNFs("FA05CODCATEG")
                Set rsProtesis = QryEst(3).OpenResultset
                If Not rsProtesis.EOF Then
                  LineasEstan(Cont).CSuplementos = LineasEstan(Cont).CSuplementos + rsFact("FA16IMPORTE")
                  DesgEstanc(ContLin).Imprimir = False
                  ImporteProtesis = ImporteProtesis + rsFact("FA16IMPORTE")
                Else
                  '********************************************************
                  ' CORREGIDO POR PATXI
                  'DesgEstanc(ContLin).Cantidad = DesgEstanc(ContLin).Cantidad + rsRNFs("FA03CANTIDAD")
                  DesgEstanc(ContLin).Cantidad = DesgEstanc(ContLin).Cantidad + rsRNFs("FA03CANTFACT")
                  '********************************************************
                  
                  DesgEstanc(ContLin).Precio = 0 & rsRNFs("FA03PRECIOFACT")
                  Citostaticos = True
                End If
              End If
              rsRNFs.MoveNext
            Wend
            If Citostaticos = True Then
              ImporteCitost = ImporteCitost + rsFact("FA16IMPORTE")
            End If
          End If
          rsFact.MoveNext
        Wend
      End If
      MiRs.MoveNext
    Wend
    If FacturaEstanc = "" Then
        ' Si no se ha encontrado ninguna factura de tipo "13/" -> se genera
        SQL = "SELECT   FA04NUMFACT_SEQUENCE.NEXTVAL " _
            & " FROM    DUAL"
        Set rsSequ = objApp.rdoConnect.OpenResultset(SQL)
        FacturaEstanc = "13/" & rsSequ(0)
        rsSequ.Close
        
        ' Se actualiza el n�mero de factura
        SQL = "UPDATE FA0400 " _
            & " SET     FA04NUMFACT = ? " _
            & " WHERE   FA04NUMFACT = ? "
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = FacturaEstanc
        qryDatos(1) = FactEstancMala
        qryDatos.Execute
        qryDatos.Close
        ' FacturaEstanc = FactEstancMala
    End If
    
  Else
    'No hay ning�n paciente en las estancias.
    PacEstancias = False
  End If
  If ContLin = 0 Then
    ReDim DesgEstanc(1 To 1)
  End If
End Sub

Sub RecogerDatosAmbulatorios(Entidad As String)
Dim MiSqL As String
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiRs As rdoResultset
Dim Cont As Integer
Dim ContLin As Integer
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim sqlRNFs As String
Dim rsRNFs As rdoResultset
Dim SqlCateg As String
Dim rsCateg As rdoResultset
Dim Protesis As Boolean
Dim sqlProtesis As String
Dim rsProtesis As rdoResultset
Dim AsignarA As String
Dim Encontrado As Boolean
Dim blnLocalizado As Boolean
Dim ContArray As Integer
Dim i As Integer
Dim blnHonorarios As Boolean
Dim FactAmbuMala As String
Dim FactPETMala As String
Dim SQL As String
Dim rsSequ As rdoResultset
Dim qryDatos As rdoQuery

  Cont = 0
  ContLin = 0
  ContArray = 0
  PET = False
  RNM = False
  
  'QryAmbulatorios.rdoParameters(0) = "31/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  
  
  '///////////////////////////////////////////////////////////////////
  ' ******************CORREGIDO POR PATXI ***************************
  QryAmbulatorios.rdoParameters(0) = "1/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  QryAmbulatorios.rdoParameters(1) = "1/" & Format(Me.txtMeses.Text, "00") & "/" & Me.txtAnyo.Text
  QryAmbulatorios.rdoParameters(2) = Me.txtCodEntidad
  '******************************************************************
  '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  
  FacturaAmbu = ""
  FactAmbuMala = ""
  Set MiRs = QryAmbulatorios.OpenResultset
  If Not MiRs.EOF Then
    Ambulatorios = True
    While Not MiRs.EOF
        If FacturaAmbu = "" And Left(MiRs("FA04NUMFACT"), 3) = "13/" Then
            FacturaAmbu = MiRs("FA04NUMFACT")
        ElseIf FactAmbuMala = "" Then
            FactAmbuMala = MiRs("FA04NUMFACT")
        End If
      Cont = Cont + 1
      ReDim Preserve LineasAmbul(1 To Cont)
      LineasAmbul(Cont).NOrden = Cont
      LineasAmbul(Cont).Asegurado = MiRs("CI22NUMSEGSOC") & ""
      LineasAmbul(Cont).Inspe = "SUBD"
      LineasAmbul(Cont).Historia = MiRs("CI22NUMHISTORIA") & ""
      LineasAmbul(Cont).Nombre = MiRs("PERSONA") & ""
      LineasAmbul(Cont).Servicio = MiRs("AD02DESDPTO")
      LineasAmbul(Cont).Diagnostico = MiRs("AD01DIAGNOSSALI") & ""
      QryAmb(1).rdoParameters(0) = MiRs("FA04CODFACT")
      Set rsFact = QryAmb(1).OpenResultset
      If Not rsFact.EOF Then
        While Not rsFact.EOF
          'Buscaremos los rnfs para ver si son consultas o por el contrario son pruebas.
          QryAmb(2).rdoParameters(0) = MiRs("FA04CODFACT")
          QryAmb(2).rdoParameters(1) = rsFact("FA16NUMLINEA")
          Set rsRNFs = QryAmb(2).OpenResultset
          If Not rsRNFs.EOF Then
            While Not rsRNFs.EOF
              'Una vez obtenidos los rnfs de la l�nea comprobaremos si se trata de
              '1� Consulta, Revision, Asistecia Oncol�gica, Medicaci�n, Pruebas
              QryAmb(3).rdoParameters(0) = rsRNFs("FA05CODCATEG")
              Set rsCateg = QryAmb(3).OpenResultset
              If Not rsCateg.EOF Then
                If IsNull(rsCateg("FA05CODORIGC2")) Then
                    blnHonorarios = False
                ElseIf rsCateg("FA05CODORIGC2") = 0 Then
                    blnHonorarios = True
                Else
                    blnHonorarios = False
                End If
                If blnHonorarios Then
                  If rsCateg("FA05CODORIGC1") = 109 Or rsCateg("FA05CODORIGC1") = 154 Or rsCateg("FA05CODORIGC1") = 210 Then
                    'Asistencia oncol�gica, miraremos si es 1� consulta o revisi�n.
                    If rsCateg("FA05CODORIGC3") = 1 Or rsCateg("FA05CODORIGC3") = 7 Or rsCateg("FA05CODORIGC3") = 4 Then
                      'PrimeraConsulta.
                      LineasAmbul(Cont).NumServicios = LineasAmbul(Cont).NumServicios + 1
                      LineasAmbul(Cont).Dias = LineasAmbul(Cont).Dias & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                      LineasAmbul(Cont).PriVisita = LineasAmbul(Cont).PriVisita + 1
                      LineasAmbul(Cont).PrimPrecio = rsRNFs("FA03PRECIOFACT")
                      
                    ElseIf rsCateg("FA05CODORIGC3") = 2 Then
                      'Revisi�n.
                      LineasAmbul(Cont).NumServicios = LineasAmbul(Cont).NumServicios + 1
                      LineasAmbul(Cont).Dias = LineasAmbul(Cont).Dias & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                      LineasAmbul(Cont).SucVisita = LineasAmbul(Cont).SucVisita + 1
                      LineasAmbul(Cont).SucPrecio = rsRNFs("FA03PRECIOFACT")
                      
                    End If
                    'En la cabecera lo asignaremos dentro de asistencia oncol�gica.
                    AsignarA = "ASISONC"
                    LineasAmbul(Cont).blnAmbu = True
                  ElseIf rsCateg("FA05CODORIGC3") = 1 Or rsCateg("FA05CODORIGC3") = 7 Or rsCateg("FA05CODORIGC3") = 4 Then
                    'Primera Consulta
                    LineasAmbul(Cont).NumServicios = LineasAmbul(Cont).NumServicios + 1
                    LineasAmbul(Cont).PriVisita = LineasAmbul(Cont).PriVisita + 1
                    LineasAmbul(Cont).Dias = LineasAmbul(Cont).Dias & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                    LineasAmbul(Cont).PrimPrecio = 0 & rsRNFs("FA03PRECIOFACT")
                    'En la cabecera lo asignaremos dentro de las primeras visitas.
                    AsignarA = "PRIVIS"
                    LineasAmbul(Cont).blnAmbu = True
                  ElseIf rsCateg("FA05CODORIGC3") = 2 Then
                    'Revisiones
                    LineasAmbul(Cont).NumServicios = LineasAmbul(Cont).NumServicios + 1
                    LineasAmbul(Cont).SucVisita = LineasAmbul(Cont).SucVisita + 1
                    LineasAmbul(Cont).Dias = LineasAmbul(Cont).Dias & FechaAFormatoDD(rsRNFs("FA03FECHA")) & ", "
                    LineasAmbul(Cont).SucPrecio = 0 & rsRNFs("FA03PRECIOFACT")
                    'En la cabecera lo asignaremos dentro de las revisiones
                    AsignarA = "REVIS"
                    LineasAmbul(Cont).blnAmbu = True
                  End If
                ElseIf rsCateg("FA08CODGRUPO") = 1 Then
                  'Medicaci�n (A�adiremos la l�nea al desglose y en cabecera lo asignamos a citos...)
                  ' Primero se mira si existe esa l�nea
                  blnLocalizado = False
                  For i = ContLin To 1 Step -1
                    If DesgAmbul(i).NOrden = Cont And DesgAmbul(i).Descrip = rsCateg("FA05DESIG") And DesgAmbul(i).Precio = rsRNFs("FA03PRECIOFACT") Then
                        blnLocalizado = True
                        Exit For
                    ElseIf DesgAmbul(i).NOrden < Cont Then
                        ' Se ha pasado a otro paciente
                        Exit For
                    End If
                  Next i
                  If Not blnLocalizado Then   ' Significa que no se ha encontrado
                    ContLin = ContLin + 1
                    ReDim Preserve DesgAmbul(1 To ContLin)
                    DesgAmbul(ContLin).NOrden = Cont
                    DesgAmbul(ContLin).Cantidad = rsRNFs("FA03CANTFACT")
                    DesgAmbul(ContLin).Descrip = rsCateg("FA05DESIG")
                    DesgAmbul(ContLin).Precio = rsRNFs("FA03PRECIOFACT")
                    DesgAmbul(ContLin).Importe = DesgAmbul(ContLin).Precio * DesgAmbul(ContLin).Cantidad
                  Else
                    DesgAmbul(i).Cantidad = DesgAmbul(i).Cantidad + rsRNFs("FA03CANTFACT")
                    DesgAmbul(ContLin).Importe = DesgAmbul(ContLin).Precio * DesgAmbul(ContLin).Cantidad
                  End If
                  AsignarA = "CITOST"
                  LineasAmbul(Cont).blnAmbu = True
                Else
                  'Comprobaremos que no se trata de una pr�tesis, ya que en ese caso no tiene que
                  'indicarse la fecha, agrup�ndose como PROTESIS en la cabecera de factura.
                  QryAmb(4).rdoParameters(0) = rsRNFs("FA05CODCATEG")
                  Set rsProtesis = QryAmb(4).OpenResultset
                  If Not rsProtesis.EOF Then
                    Protesis = True
                  Else
                    Protesis = False
                  End If
                  'Pruebas (A�adiremos la l�nea de desglose, si no existe y en cabecera lo a�adiremos
                  '         a su propia linea, as� como las fechas en el desglose)
                  'Para a�adir la linea de desglose comprobaremos que no sea un pet
                  ContLin = ContLin + 1
                  ReDim Preserve DesgAmbul(1 To ContLin)
                  DesgAmbul(ContLin).NOrden = Cont
                  Select Case rsCateg("FA05CODCATEG")
                        Case 13155      ' Estudio Simple TAC
                            DesgAmbul(ContLin).Cantidad = rsRNFs("FA03CANTFACT")
                            DesgAmbul(ContLin).Descrip = "TOMOGRAFIA AXIAL COMPUTERIZADA"
                        Case 13156      ' Estudio Doble TAC
                            DesgAmbul(ContLin).Cantidad = 1 * rsRNFs("FA03CANTFACT")
                            DesgAmbul(ContLin).Descrip = "TOMOGRAFIA AXIAL COMPUTERIZADA"
                        Case 13157      ' Estudio Triple TAC
                            DesgAmbul(ContLin).Cantidad = 1 * rsRNFs("FA03CANTFACT")
                            DesgAmbul(ContLin).Descrip = "TOMOGRAFIA AXIAL COMPUTERIZADA"
                        Case 13815, 13088   ' Estudio Simple RMN
                            DesgAmbul(ContLin).Cantidad = rsRNFs("FA03CANTFACT")
                            DesgAmbul(ContLin).Descrip = "RESONANCIA MAGNETICA"
                        Case 13816, 13089   ' Estudio Doble RMN
                            DesgAmbul(ContLin).Cantidad = 1 * rsRNFs("FA03CANTFACT")
                            DesgAmbul(ContLin).Descrip = "RESONANCIA MAGNETICA"
                        Case 13817, 13090   ' Estudio Triple RMN
                            DesgAmbul(ContLin).Cantidad = 1 * rsRNFs("FA03CANTFACT")
                            DesgAmbul(ContLin).Descrip = "RESONANCIA MAGNETICA"
                        Case Else
                            DesgAmbul(ContLin).Cantidad = rsRNFs("FA03CANTFACT")
                            'DesgAmbul(ContLin).Descrip = rsCateg("FA05DESIG")
                            DesgAmbul(ContLin).Descrip = rsFact!FA16DESCRIP
                  End Select
                  DesgAmbul(ContLin).Precio = 0 & rsRNFs("FA03PRECIOFACT")
                  DesgAmbul(ContLin).Importe = DesgAmbul(ContLin).Precio * DesgAmbul(ContLin).Cantidad
                  'If DesgAmbul(ContLin).Cantidad = 1 Then
                    If Not IsNull(rsRNFs!FA03FECHA) Then
                        DesgAmbul(ContLin).Dias = Day(rsRNFs!FA03FECHA)
                    End If
                  'End If
                  
                  If Protesis = True Then
                    'Lo a�adimos en la l�nea de protesis
                    AsignarA = "PROTESIS"
                  ElseIf Protesis = False Then
                    'Buscamos en la cabecera y si no est� la l�nea la creamos.
                    AsignarA = "OTROS"
                  End If
                  If rsCateg("FA05DESIG") = "ESTADIAJE ONCOLOGICO" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "FLUORODOPA" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "HONORARIOS DIAGN. CON 18 FDG" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "METABOLISMO CARDIACO" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "METABOLISMO CEREBRO" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "METABOLISMO ONCOLOGICO" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "PERFUSION CARDIACA" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "PERFUSION CEREBRO" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "PERFUSION ONCOLOGICA" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "VIABILIDAD CARDIACA" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  ElseIf rsCateg("FA05DESIG") = "VIABILIDAD ONCOLOGICA" Then
                    LineasAmbul(Cont).PET = 1
                    DesgAmbul(ContLin).PET = 1
                    AsignarA = "PET"
                    PET = True
                  'ElseIf rsCateg("FA05DESIG") = "R.N.M." Then
                  ElseIf rsCateg("FA05CODCATEG") = 13815 _
                        Or rsCateg("FA05CODCATEG") = 13816 _
                        Or rsCateg("FA05CODCATEG") = 13817 _
                        Or rsCateg("FA05CODCATEG") = 13088 _
                        Or rsCateg("FA05CODCATEG") = 13089 _
                        Or rsCateg("FA05CODCATEG") = 13090 Then
                    LineasAmbul(Cont).RNM = 1
                    DesgAmbul(ContLin).RNM = 1
                    AsignarA = "RNM"
                    RNM = True
                  Else
                    LineasAmbul(Cont).blnAmbu = True
                  End If
                End If
              End If
              If ContArray <> 0 Then
                Encontrado = False
                Select Case AsignarA
                  Case "ASISONC"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = "Asistencia oncologica" Then '"Asistencia oncologica" Then
                        CabeceraAmbul(i).Cantidad = CabeceraAmbul(i).Cantidad + 1
                        Encontrado = True
                        Exit For
                      End If
                    Next
                  Case "PRIVIS"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = "Primera consulta" Then '"Primera consulta" Then
                        CabeceraAmbul(i).Cantidad = CabeceraAmbul(i).Cantidad + 1
                        Encontrado = True
                        Exit For
                      End If
                    Next
                  Case "REVIS"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = "Revision" Then '"Revision" Then
                        CabeceraAmbul(i).Cantidad = CabeceraAmbul(i).Cantidad + 1
                        Encontrado = True
                        Exit For
                      End If
                    Next
                  Case "CITOST"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = "Citostaticos y factores" Then
                        'CabeceraAmbul(i).Importe = CabeceraAmbul(i).Importe + DesgAmbul(ContLin).Importe
                        CabeceraAmbul(i).Importe = CabeceraAmbul(i).Importe + rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT")
                        Encontrado = True
                        Exit For
                      End If
                    Next
                  Case "PROTESIS"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = "Protesis" Then
                        CabeceraAmbul(i).Importe = CabeceraAmbul(i).Importe + DesgAmbul(ContLin).Importe
                        'CabeceraAmbul(i).Importe = CabeceraAmbul(i).Importe + rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT")
                        Encontrado = True
                        Exit For
                      End If
                    Next
                  Case "PET"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = DesgAmbul(ContLin).Descrip Then 'rsCateg("FA05DESIG") Then
                        CabeceraAmbul(i).Cantidad = CabeceraAmbul(i).Cantidad + DesgAmbul(ContLin).Cantidad
                        Encontrado = True
                        Exit For
                      End If
                    Next
                    If FacturaPET = "" And Left(MiRs("FA04NUMFACT"), 3) = "13/" Then
                        FacturaPET = MiRs!FA04NUMFACT
                    ElseIf FactPETMala = "" Then
                        FactPETMala = MiRs("FA04NUMFACT")
                    End If
                  Case "RNM"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = DesgAmbul(ContLin).Descrip Then 'rsCateg("FA05DESIG") Then
                        CabeceraAmbul(i).Cantidad = CabeceraAmbul(i).Cantidad + DesgAmbul(ContLin).Cantidad
                        Encontrado = True
                        Exit For
                      End If
                    Next
                  Case "OTROS"
                    For i = 1 To UBound(CabeceraAmbul)
                      If CabeceraAmbul(i).Descripcion = DesgAmbul(ContLin).Descrip Then
                        CabeceraAmbul(i).Cantidad = CabeceraAmbul(i).Cantidad + DesgAmbul(ContLin).Cantidad
                        Encontrado = True
                        Exit For
                      End If
                    Next
                End Select
                If Encontrado = False Then
                  ContArray = ContArray + 1
                  ReDim Preserve CabeceraAmbul(1 To ContArray)
                  Select Case AsignarA
                  Case "ASISONC"
                    CabeceraAmbul(ContArray).Descripcion = "Asistencia oncologica"
                    CabeceraAmbul(ContArray).Cantidad = CabeceraAmbul(ContArray).Cantidad + 1
                    CabeceraAmbul(ContArray).Precio = rsRNFs("FA03PRECIOFACT")
                    CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  Case "PRIVIS"
                    CabeceraAmbul(ContArray).Descripcion = "Primera consulta"
                    CabeceraAmbul(ContArray).Cantidad = CabeceraAmbul(ContArray).Cantidad + 1
                    CabeceraAmbul(ContArray).Precio = 0 & rsRNFs("FA03PRECIOFACT")
                    CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  Case "REVIS"
                    CabeceraAmbul(ContArray).Descripcion = "Revision"
                    CabeceraAmbul(ContArray).Cantidad = CabeceraAmbul(ContArray).Cantidad + 1
                    CabeceraAmbul(ContArray).Precio = rsRNFs("FA03PRECIOFACT")
                    CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  Case "CITOST"
                    CabeceraAmbul(ContArray).Descripcion = "Citostaticos y factores"
                    CabeceraAmbul(ContArray).Importe = DesgAmbul(ContLin).Importe
                    CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  Case "PROTESIS"
                    CabeceraAmbul(ContArray).Descripcion = "Protesis"
                    CabeceraAmbul(ContArray).Importe = DesgAmbul(ContLin).Importe
                    CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  Case "PET"
                    CabeceraAmbul(ContArray).Descripcion = DesgAmbul(ContLin).Descrip
                    CabeceraAmbul(ContArray).Cantidad = DesgAmbul(ContLin).Cantidad
                    CabeceraAmbul(ContArray).Precio = DesgAmbul(ContLin).Precio
                    CabeceraAmbul(ContArray).PET = 1
                  Case "RNM"
                    CabeceraAmbul(ContArray).Descripcion = DesgAmbul(ContLin).Descrip
                    CabeceraAmbul(ContArray).Cantidad = DesgAmbul(ContLin).Cantidad
                    CabeceraAmbul(ContArray).Precio = DesgAmbul(ContLin).Precio
                    CabeceraAmbul(ContArray).RNM = 1
                  Case Else
                    CabeceraAmbul(ContArray).Descripcion = DesgAmbul(ContLin).Descrip
                    CabeceraAmbul(ContArray).Cantidad = DesgAmbul(ContLin).Cantidad
                    CabeceraAmbul(ContArray).Precio = DesgAmbul(ContLin).Precio
                    CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  End Select
                  'CabeceraAmbul(ContArray).Precio = DesgAmbul(ContLin).Precio
                End If
              Else
                ContArray = ContArray + 1
                ReDim Preserve CabeceraAmbul(1 To ContArray)
                'CabeceraAmbul(ContArray).Cantidad = DesgAmbul(ContLin).Cantidad
                Select Case AsignarA
                Case "ASISONC"
                  CabeceraAmbul(ContArray).Descripcion = "Asistencia oncologica"
                  CabeceraAmbul(ContArray).Cantidad = 1
                  CabeceraAmbul(ContArray).Precio = rsRNFs("FA03PRECIOFACT")
                  CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  CabeceraAmbul(ContArray).PET = 0
                  CabeceraAmbul(ContArray).RNM = 0
                Case "PRIVIS"
                  CabeceraAmbul(ContArray).Descripcion = "Primera consulta"
                  CabeceraAmbul(ContArray).Cantidad = 1
                  CabeceraAmbul(ContArray).Precio = rsRNFs("FA03PRECIOFACT")
                  CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  CabeceraAmbul(ContArray).PET = 0
                  CabeceraAmbul(ContArray).RNM = 0
                Case "REVIS"
                  CabeceraAmbul(ContArray).Descripcion = "Revision"
                  CabeceraAmbul(ContArray).Cantidad = 1
                  CabeceraAmbul(ContArray).Precio = rsRNFs("FA03PRECIOFACT")
                  CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  CabeceraAmbul(ContArray).PET = 0
                  CabeceraAmbul(ContArray).RNM = 0
                Case "CITOST"
                  CabeceraAmbul(ContArray).Descripcion = "Citostaticos y factores"
                  CabeceraAmbul(ContArray).Importe = DesgAmbul(ContLin).Importe
                  CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  CabeceraAmbul(ContArray).PET = 0
                  CabeceraAmbul(ContArray).RNM = 0
                Case "PROTESIS"
                  CabeceraAmbul(ContArray).Descripcion = "Protesis"
                  CabeceraAmbul(ContArray).Importe = DesgAmbul(ContLin).Importe
                  CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                  CabeceraAmbul(ContArray).PET = 0
                  CabeceraAmbul(ContArray).RNM = 0
                Case Else
                  CabeceraAmbul(ContArray).Descripcion = DesgAmbul(ContLin).Descrip
                  CabeceraAmbul(ContArray).Cantidad = DesgAmbul(ContLin).Cantidad
                  CabeceraAmbul(ContArray).Precio = DesgAmbul(ContLin).Precio
                  CabeceraAmbul(ContArray).orden = AsignarOrden(CabeceraAmbul(ContArray).Descripcion)
                End Select
              End If
              rsRNFs.MoveNext
            Wend
          End If
          rsFact.MoveNext
        Wend
      End If
      MiRs.MoveNext
    Wend
    Call OrdenarAmbulatorio
    If FacturaAmbu = "" Then
        ' Si no se ha encontrado ninguna factura de tipo "13/" -> se genera
        SQL = "SELECT   FA04NUMFACT_SEQUENCE.NEXTVAL " _
            & " FROM    DUAL"
        Set rsSequ = objApp.rdoConnect.OpenResultset(SQL)
        FacturaAmbu = "13/" & rsSequ(0)
        rsSequ.Close
        
        ' Se actualiza el n�mero de factura
        SQL = "UPDATE FA0400 " _
            & " SET     FA04NUMFACT = ? " _
            & " WHERE   FA04NUMFACT = ? "
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = FacturaAmbu
        qryDatos(1) = FactAmbuMala
        qryDatos.Execute
        qryDatos.Close
        ' FacturaAmbu = FactAmbuMala
    End If
    
    If PET And FacturaPET = "" Then
        ' Si no se ha encontrado ninguna factura de tipo "13/" -> se genera
        SQL = "SELECT   FA04NUMFACT_SEQUENCE.NEXTVAL " _
            & " FROM    DUAL"
        Set rsSequ = objApp.rdoConnect.OpenResultset(SQL)
        FacturaPET = "13/" & rsSequ(0)
        rsSequ.Close
        
        ' Se actualiza el n�mero de factura
        SQL = "UPDATE FA0400 " _
            & " SET     FA04NUMFACT = ? " _
            & " WHERE   FA04NUMFACT = ? "
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = FacturaPET
        qryDatos(1) = FactPETMala
        qryDatos.Execute
        qryDatos.Close
        ' FacturaPET = FactPETMala
    End If
  Else
    'No hay ning�n paciente de ambulatorio, con lo cual no lo imprimiremos
    Ambulatorios = False
  End If
End Sub


Sub RecogerEstanciasMadrid(factura As Long)

Dim Cont As Integer
Dim ContLin As Integer
Dim rsFact As rdoResultset
Dim rsRNFs As rdoResultset
Dim rsProtesis As rdoResultset
Dim rsNombre As rdoResultset
Dim x As Integer
Dim LinMad As Long
Dim LinCab As Long
Dim Encontrado As Boolean

  Cont = 0
  ContLin = 0
  
  On Error Resume Next
  LinMad = UBound(LinMadrid)
  If Err <> 0 Then
    LinMad = 0
    On Error GoTo 0
  End If
  'Analizamos todas las l�neas de esa factura de estancias
  QryEst(1).rdoParameters(0) = factura
  Set rsFact = QryEst(1).OpenResultset
  If Not rsFact.EOF Then
    While Not rsFact.EOF
      LinMad = LinMad + 1
      ReDim Preserve LinMadrid(1 To LinMad)
      'Como cada l�nea incluye un medicamento o una pr�tesis distinta
      LinMadrid(LinMad).Asegurado = Asegurado
      LinMadrid(LinMad).Inspe = "SUBD"
      LinMadrid(LinMad).Historia = Historia
      LinMadrid(LinMad).Nombre = Nombre
      LinMadrid(LinMad).FecEnt = FechaEnt
      LinMadrid(LinMad).FecInt = "00-00"
      LinMadrid(LinMad).FecSal = FechaSal
      LinMadrid(LinMad).Numero = 0
      LinMadrid(LinMad).EstCosto = 0
      LinMadrid(LinMad).SupCosto = 0
      LinMadrid(LinMad).Diagnostico = Diagnostico
      LinMadrid(LinMad).Servicio = Servicio
      
      If Trim(rsFact("FA16DESCRIP")) = "Estancias" Then
        LinMadrid(LinMad).EstCosto = LinMadrid(LinMad).EstCosto + rsFact("FA16IMPORTE")
      Else
        LinMadrid(LinMad).Descrip = rsFact("FA16DESCRIP")
        LinMadrid(LinMad).Importe = rsFact("FA16IMPORTE")
      End If
      QryEst(2).rdoParameters(0) = factura
      QryEst(2).rdoParameters(1) = rsFact("FA16NUMLINEA")
      Set rsRNFs = QryEst(2).OpenResultset
      If Not rsRNFs.EOF Then
        While Not rsRNFs.EOF
          If Trim(rsFact("FA16DESCRIP")) = "Estancias" Then
            LinMadrid(LinMad).NumServ = LinMadrid(LinMad).NumServ + rsRNFs("FA03CANTFACT")
            LinMadrid(LinMad).Precio = 0 & rsRNFs("FA03PRECIOFACT")
            'Buscaremos en las lineas de cabecera si est� la de estancias y si no la a�adiremos
            On Error Resume Next
            LinCab = UBound(MadridCab)
            If Err <> 0 Then
              LinCab = 0
              On Error GoTo 0
            End If
            If LinCab = 0 Then
              Encontrado = False
            Else
              Encontrado = False
              For Cont = 1 To LinCab
                If MadridCab(Cont).Desc1 = "Estancias" Then
                  LinCab = Cont
                  Encontrado = True
                  Exit For
                End If
              Next
              If Encontrado = False Then
                LinCab = LinCab + 1
                ReDim Preserve MadridCab(1 To LinCab)
                MadridCab(LinCab).Cantidad = rsRNFs("FA03CANTFACT")
                MadridCab(LinCab).Desc1 = "Estancias"
                MadridCab(LinCab).Precio = rsRNFs("FA03PRECIOFACT")
                MadridCab(LinCab).Moneda = "PESETAS"
                MadridCab(LinCab).Tipo = 2 'Estancias
                MadridCab(LinCab).orden = LinCab
                LinMadrid(LinMad).orden = LinCab
              Else
                MadridCab(LinCab).Cantidad = MadridCab(LinCab).Cantidad + rsRNFs("FA03CANTFACT")
                LinMadrid(LinMad).orden = LinCab
              End If
            End If
            LinMadrid(LinMad).orden = LinCab
          Else
            LinMadrid(LinMad).Cantidad = LinMadrid(LinMad).Cantidad + rsRNFs("FA03CANTFACT")
            QryEst(4).rdoParameters(0) = rsRNFs("FA05CODCATEG")
            Set rsNombre = QryEst(4).OpenResultset
            If Not rsNombre.EOF Then
              LinMadrid(LinMad).Descrip = rsFact("FA16DESCRIP")
            End If
            LinMadrid(LinMad).Precio = rsRNFs("FA03PRECIOFACT")
            LinMadrid(LinMad).DiasServ = LinMadrid(LinMad).DiasServ & Me.FechaAFormatoDD(rsRNFs("FA03FECHA")) & ","
            'Comprobaremos si se trata de una pr�tesis o de medicaci�n
            QryEst(3).rdoParameters(0) = rsRNFs("FA05CODCATEG")
            Set rsProtesis = QryEst(3).OpenResultset
            If Not rsProtesis.EOF Then
              'Se trata de una pr�tesis con lo cual lo asignaremos a la cabecera de pr�tesis
              LinCab = UBound(MadridCab)
              If LinCab = 0 Then
                Encontrado = False
              Else
                Encontrado = False
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "Pr�tesis" Then
                    LinCab = Cont
                    Encontrado = True
                    Exit For
                  End If
                Next
                If Encontrado = False Then
                  LinCab = LinCab + 1
                  ReDim Preserve MadridCab(1 To LinCab)
                  MadridCab(LinCab).Desc1 = "Pr�tesis"
                  MadridCab(LinCab).Moneda = "PESETAS"
                  MadridCab(LinCab).Importe = rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT")
                  MadridCab(LinCab).Tipo = 3 'Resto
                  MadridCab(LinCab).orden = LinCab
                  LinMadrid(LinMad).orden = LinCab
                Else
                  MadridCab(LinCab).Importe = MadridCab(LinCab).Importe + (rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT"))
                  LinMadrid(LinMad).orden = LinMad
                End If
              End If
            Else
              'Se trata de medicaci�n
              LinCab = UBound(MadridCab)
              If LinCab = 0 Then
                Encontrado = False
              Else
                Encontrado = False
                For Cont = 1 To LinCab
                  If MadridCab(Cont).Desc1 = "MEDICACI�N" Then
                    LinCab = Cont
                    Encontrado = True
                    Exit For
                  End If
                Next
                If Encontrado = False Then
                  LinCab = LinCab + 1
                  ReDim Preserve MadridCab(1 To LinCab)
                  MadridCab(LinCab).Desc1 = "MEDICACI�N"
                  MadridCab(LinCab).Moneda = "PESETAS"
                  MadridCab(LinCab).Importe = rsRNFs("FA03CANTFACT") * rsRNFs("FAO3PRECIOFACT")
                  MadridCab(LinCab).Tipo = 3
                  MadridCab(LinCab).orden = LinCab
                  LinMadrid(LinMad).orden = LinCab
                Else
                  MadridCab(LinCab).Importe = MadridCab(LinCab).Importe + (rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT"))
                  LinMadrid(LinMad).orden = LinCab
                End If
              End If
            End If
          End If
          rsRNFs.MoveNext
        Wend
      End If
      rsFact.MoveNext
    Wend
  End If
  
End Sub

Sub RecogerForfaitsMadrid(factura As Long)
Dim MiSqL As String
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiRs As rdoResultset
Dim Cont As Integer
Dim ContLin As Integer
Dim ContResu As Integer
Dim Asignado As Boolean
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim sqlRNFs As String
Dim rsRNFs As rdoResultset
Dim sqlAtrib As String
Dim rsAtrib As rdoResultset
Dim sqlCodExt As String
Dim rsCodExt As rdoResultset
Dim rsProtesis As rdoResultset
Dim x As Integer
Dim LinMad As Long
Dim LinCab As Long
Dim Encontrado As Boolean

  'Inicializamos los contadores
  Cont = 0
  ContLin = 0
  ContResu = 0
  ProtesisForfaits = 0
  
  On Error Resume Next
  LinCab = UBound(LinMadrid)
  If Err <> 0 Then
    LinCab = 0
    On Error GoTo 0
  End If
 
  'Buscamos las l�neas
  QryFor(1).rdoParameters(0) = factura
  Set rsFact = QryFor(1).OpenResultset
  If Not rsFact.EOF Then
    While Not rsFact.EOF 'Analizamos las l�neas.
      LinMad = LinMad + 1
      'A�adimos los datos comunes a todas las lineas
      ReDim Preserve LinMadrid(1 To LinMad)
      LinMadrid(LinMad).Asegurado = Asegurado
      LinMadrid(LinMad).Inspe = "FORF"
      LinMadrid(LinMad).Historia = Historia
      LinMadrid(LinMad).Nombre = Nombre
      LinMadrid(LinMad).FecEnt = FechaEnt
      LinMadrid(LinMad).FecInt = ""
      LinMadrid(LinMad).FecSal = FechaSal
      LinMadrid(LinMad).Diagnostico = Diagnostico
      LinMadrid(LinMad).Especialidad = Servicio
      QryFor(2).rdoParameters(0) = factura
      QryFor(2).rdoParameters(1) = rsFact("FA16NUMLINEA")
      'Buscamos los rnfs para saber que es lo que se ha facturado.
      Set rsRNFs = QryFor(2).OpenResultset
      If Not rsRNFs.EOF Then
        While Not rsRNFs.EOF 'Analizamos cada uno de los rnfs, para saber donde lo colocamos.
          'Comprobamos si es un forfait
          QryFor(3).rdoParameters(0) = rsRNFs("FA15CODATRIB")
          Set rsAtrib = QryFor(3).OpenResultset
          If Not rsAtrib.EOF Then
            'Si es suficiente y facturacion obligatoria se trata de un forfait.
            If rsAtrib("FA15INDSUFICIENTE") = 1 And rsAtrib("FA15INDFACTOBLIG") = 1 Then
              LinMadrid(LinCab).EstCosto = LinMadrid(LinCab).EstCosto + rsFact("FA16IMPORTE")
              'Buscaremos las claves que nos indiquen el forfait
              If Not IsNull(rsAtrib("FA46CODDESEXTERNO")) Then
                QryFor(4).rdoParameters(0) = rsAtrib("FA46CODDESEXTERNO")
                Set rsCodExt = QryFor(4).OpenResultset
                If Not rsCodExt.EOF Then
                  'Asignamos a la tabla de las lineas los valores
                  LinMadrid(LinCab).CodDiag = rsCodExt("FA46CODDIAG")
                  LinMadrid(LinCab).DescDiag = rsCodExt("FA46DESCRIP")
                  LinMadrid(LinCab).CodProc = rsCodExt("FA46CODPROC")
                  LinMadrid(LinCab).DesProc = rsCodExt("FA46DESPROC")
                  On Error Resume Next
                  LinCab = UBound(MadridCab)
                  If Err <> 0 Then
                    LinCab = 0
                  End If
                  On Error GoTo 0
                  If LinCab = 0 Then
                    Encontrado = False
                  Else
                    Encontrado = False
                    For x = 1 To LinCab
                      If (MadridCab(x).Desc1 = Trim(rsCodExt("FA46CODDIAG")) & " " & _
                         Trim(rsCodExt("FA46DESCRIP"))) And _
                         (MadridCab(x).Desc2 = Trim(rsCodExt("FA46CODPROC")) & " " & _
                         Trim(rsCodExt("FA46DESPROC"))) Then
                        Encontrado = True
                        LinCab = x
                        Exit For
                      End If
                    Next
                  End If
                  If Encontrado = False Then
                    LinCab = LinCab + 1
                    ReDim Preserve MadridCab(1 To LinCab)
                    MadridCab(LinCab).Desc1 = Trim(rsCodExt("FA46CODDIAG")) & " " & Trim(rsCodExt("FA46DESCRIP"))
                    MadridCab(LinCab).Desc2 = Trim(rsCodExt("FA46CODPROC")) & " " & Trim(rsCodExt("FA46DESPROC"))
                    MadridCab(LinCab).Cantidad = 1
                    MadridCab(LinCab).Precio = rsFact("FA16IMPORTE")
                    MadridCab(LinCab).orden = LinCab
                  Else
                    MadridCab(LinCab).Cantidad = MadridCab(LinCab).Cantidad + 1
                  End If
                  LinMadrid(LinMad).orden = LinCab
                End If
              End If
            Else
              'Comprobaremos si se trata de una pr�tesis busc�ndolo en FA0503J
              QryFor(5).rdoParameters(0) = rsRNFs("FA05CODCATEG")
              Set rsProtesis = QryFor(5).OpenResultset
              If Not rsProtesis.EOF Then
                LinMadrid(LinCab).Cantidad = rsRNFs("FA03CANTFACT")
                LinMadrid(LinCab).Descrip = rsFact("FA16DESCRIP")
                LinMadrid(LinCab).Precio = rsRNFs("FA03PRECIOFACT")
                LinMadrid(LinCab).Importe = rsRNFs("FA03CANTFACT") * rsRNFs("FA03PRECIOFACT")
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                'BUSCAREMOS EN LA CABECERA LA LINEA DE PR�TESIS Y SUMAREMOS
                'EL VALOR DE ESTAS O LA A�ADIREMOS SI NO EXISTE ASIGNAR N� DE ORDEN
                On Error Resume Next
                LinCab = UBound(MadridCab)
                If Err <> 0 Then
                  LinCab = 0
                End If
                On Error GoTo 0
                If LinCab = 0 Then
                  Encontrado = False
                Else
                  Encontrado = False
                  For x = 1 To LinCab
                    If MadridCab(x).Desc1 = "Pr�tesis" Then
                      Encontrado = True
                      LinCab = x
                      Exit For
                    End If
                  Next
                End If
                If Encontrado = False Then
                  LinCab = LinCab + 1
                  ReDim Preserve MadridCab(1 To LinCab)
                  MadridCab(LinCab).Desc1 = "Pr�tesis"
                  MadridCab(LinCab).Moneda = "PESETAS"
                  MadridCab(LinCab).Importe = rsFact("FA16IMPORTE")
                  MadridCab(LinCab).orden = LinCab
                Else
                  MadridCab(LinCab).Importe = MadridCab(LinCab).Importe + rsFact("FA16IMPORTE")
                End If
                LinMadrid(LinMad).orden = LinCab
              End If
            End If
          End If
          rsRNFs.MoveNext
        Wend
      End If
      rsFact.MoveNext
    Wend
  End If

End Sub
Private Sub cboCodEntidad_Click()
Dim SQL As String
Dim RS As rdoResultset
Dim sqlPersona As String
Dim rsPersona As rdoResultset

  If Trim(Me.cboCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'" & _
              "ORDER BY CI13DESENTIDAD"

    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        sqlPersona = "Select CI21CODPERSONA_REC From CI2900 " & _
                     "Where CI32CODTIPECON = 'S' And CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "'"
        Set rsPersona = objApp.rdoConnect.OpenResultset(sqlPersona)
        If Not rsPersona.EOF Then
          txtCodPersona = rsPersona("CI21CODPERSONA_REC")
        End If
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If
End Sub


Private Sub cboCodEntidad_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodEntidad.Clear
  
  If Trim(txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI32CODTIPECON = '" & Me.txtCodTipEcon & "' AND CI13FECFIVGENT IS NULL " & _
        "ORDER BY CI13DESENTIDAD"
        
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        cboCodEntidad.AddItem RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    End If
  Else
    MsgBox "Debe seleccionar un tipo econ�mico", vbInformation + vbOKOnly, "Aviso"
  End If
End Sub


Private Sub cboCodEntidad_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub cboCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset
Dim sqlPersona As String
Dim rsPersona As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'" & _
              "ORDER BY CI13DESENTIDAD"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        sqlPersona = "Select CI21CODPERSONA_REC From CI2900 " & _
                     "Where CI32CODTIPECON = 'S' And CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "'"
        Set rsPersona = objApp.rdoConnect.OpenResultset(sqlPersona)
        If Not rsPersona.EOF Then
          txtCodPersona = rsPersona("CI21CODPERSONA_REC")
        End If
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If

End Sub


Private Sub cboMeses_Click()
Dim rsfecha As rdoResultset

  If cboMeses.Text <> "" Then
    Select Case cboMeses.Text
      Case "Enero"
        Me.txtMeses.Text = 1
      Case "Febrero"
        Me.txtMeses.Text = 2
      Case "Marzo"
        Me.txtMeses.Text = 3
      Case "Abril"
        Me.txtMeses.Text = 4
      Case "Mayo"
        Me.txtMeses.Text = 5
      Case "Junio"
        Me.txtMeses.Text = 6
      Case "Julio"
        Me.txtMeses.Text = 7
      Case "Agosto"
        Me.txtMeses.Text = 8
      Case "Septiembre"
        Me.txtMeses.Text = 9
      Case "Octubre"
        Me.txtMeses.Text = 10
      Case "Noviembre"
        Me.txtMeses.Text = 11
      Case "Diciembre"
        Me.txtMeses.Text = 12
      Case Else
        MsgBox "El Mes no es v�lido", vbExclamation + vbOKOnly, "Impresi�n de Listados INSALUD"
    End Select
    
    Set rsfecha = objApp.rdoConnect.OpenResultset("Select SysDate From Dual")
    If Not rsfecha.EOF Then
      If CInt(Format(rsfecha(0), "MM")) >= txtMeses.Text Then
        Me.txtAnyo.Text = Year(rsfecha(0))
      Else
        Me.txtAnyo.Text = Year(rsfecha(0)) - 1
      End If
    End If
  End If
End Sub

Private Sub cboMeses_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub


Private Sub cmdHospit_Click()
    
  With vsPrinter1
    .Preview = True
    .Orientation = orLandscape
    Call Me.Hospitalizado
    .Preview = False
    Call Me.Hospitalizado
  End With
End Sub



Private Sub cmdImprimir_Click()
  
  Screen.MousePointer = vbHourglass
  
  ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  ' ********************MAS MODIFICACIONES DE PATXI *************
  ' /////////////////////////////////////////////////////////////
  
  Erase LineasForf
  Erase DesgForf
  Erase ResumenForf
  Erase LineasEstan
  Erase DesgEstanc
  Erase LineasAmbul
  Erase DesgAmbul
  Erase CabeceraAmbul
  Erase CabeceraHospit
  Erase LinMadrid
  Erase MadridCab
  
  FacturaPET = ""
  ' /////////////////////////////////////////////////////////////
  
  
  'Comprobamos que se ha seleccionado el insalud y el mes a facturar
  If Me.txtCodEntidad.Text = "" Then
    MsgBox "Debe seleccionar una entidad", vbOKOnly + vbInformation, "Aviso"
    If Me.cboCodEntidad.Enabled = True Then
      cboCodEntidad.SetFocus
    End If
    Exit Sub
  End If
  
  If Me.txtMeses.Text = "" Then
    MsgBox "Debe seleccionar el mes a facturar", vbOKOnly + vbInformation, "Aviso"
    If Me.cboMeses.Enabled = True Then
      cboMeses.SetFocus
    End If
    Exit Sub
  End If

  'Si estos datos existen empezamos a realizar la impresi�n de los datos.
  If Me.txtCodEntidad = "M" Then
    Call Me.RecogerDatosMadrid
  Else
'    Call Me.RecogerDatos(Me.txtCodEntidad)
'    Call Me.RecogerDatosEstancias(Me.txtCodEntidad)
    Call Me.RecogerDatosAmbulatorios(Me.txtCodEntidad)
  End If
  
  With vsPrinter1
    .Preview = True
    .Action = paStartDoc
    Call Me.CabeceraForfaits
    .Action = paEndDoc
    .Preview = False
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
'    If PacEstancias = True Then
'      .Action = paStartDoc
'      For Copias = 1 To 1   '4
'        Call Me.CabeceraEstancias
'        Call Me.LineasEstancias
'        Call Me.Estancias
'      Next
'      .Action = paEndDoc
'    End If
'    If Hospitalizados = True Then
'      For Copias = 1 To 1   '4
'        .Action = paStartDoc
'        Call Me.CabeceraForfaits
'        Call Me.LineasForfaits
'        Call Me.Hospitalizado
'        .Action = paEndDoc
'      Next
'    End If
    If Ambulatorios = True Then
      For Copias = 1 To 1   '4
        Call Me.Ambulatorio
        Call Me.CabeceraAmbulatorio
        Call Me.LineasAmbulatorio
      Next
    End If
'    If PET = True Then
'      For Copias = 1 To 1   '4
'        Call Me.PrimeraPET
'        Call Me.CabeceraPET
'        Call Me.LineasPET
'      Next
'    End If
'
'    If RNM = True Then
'      For Copias = 1 To 1   '4
'        Call Me.PrimeraRNM
'        Call Me.CabeceraRNM
'        Call Me.LineasRNM
'      Next
'    End If
    
  End With

  If Madrid = True Then
    Call Me.ImprimirMadrid
  End If

  Screen.MousePointer = vbDefault
End Sub


Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Command1_Click()
Dim Texto

  With vsPrinter1
    .Preview = True
    Call Me.AmbulDetalle
    .Preview = False
    Call Me.AmbulDetalle
  End With
End Sub


Private Sub Form_Load()
  
  Call Me.IniciarQRYs
  Call CargarMeses

End Sub

Private Sub txtCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset
Dim sqlPersona As String
Dim rsPersona As rdoResultset

  Me.txtCodEntidad = UCase(Me.txtCodEntidad)
  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'" & _
              "ORDER BY CI13DESENTIDAD"
              
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodEntidad.Text = RS("CI13DESENTIDAD") & ""
        sqlPersona = "Select CI21CODPERSONA_REC From CI2900 " & _
                     "Where CI32CODTIPECON = 'S' And CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "'"
        Set rsPersona = objApp.rdoConnect.OpenResultset(sqlPersona)
        If Not rsPersona.EOF Then
          txtCodPersona = rsPersona("CI21CODPERSONA_REC")
        End If
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If


End Sub



