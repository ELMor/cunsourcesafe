VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frm_ListadosProtesis 
   Caption         =   "Listados de Pr�tesis"
   ClientHeight    =   3210
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5730
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   5730
   StartUpPosition =   2  'CenterScreen
   Begin SSCalendarWidgets_A.SSDateCombo SSFecInicio 
      Height          =   375
      Left            =   840
      TabIndex        =   4
      Top             =   1560
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      BackColor       =   -2147483639
      DefaultDate     =   ""
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Default         =   -1  'True
      Height          =   495
      Left            =   1800
      TabIndex        =   3
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      Begin VB.OptionButton optOsasunbidea 
         Caption         =   "Osasunbidea"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2880
         TabIndex        =   2
         Top             =   360
         Width           =   1815
      End
      Begin VB.OptionButton optInsalud 
         Caption         =   "Insalud"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   600
         TabIndex        =   1
         Top             =   360
         Width           =   1815
      End
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSFecFin 
      Height          =   375
      Left            =   3480
      TabIndex        =   5
      Top             =   1560
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      BackColor       =   -2147483639
      DefaultDate     =   ""
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
   End
   Begin VB.Label Label2 
      Caption         =   "Hasta"
      Height          =   255
      Left            =   2880
      TabIndex        =   7
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Desde"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   1560
      Width           =   615
   End
End
Attribute VB_Name = "frm_ListadosProtesis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim con As rdoConnection
Dim strAhora As String

Private Sub cmdImprimir_Click()
Dim strWhere As String

If Not optInsalud And Not optOsasunbidea Then
  MsgBox "�Desea el listado para Insalud o para Osasunbidea?", vbQuestion, " "
  Exit Sub
End If
If SSFecFin.Date = "" Or SSFecInicio.Date = "" Then
  MsgBox "Especifique el periodo de tiempo del listado", vbCritical, " "
  Exit Sub
End If
strWhere = "FA1503J.CI32CODTIPECON='S' AND "
If optOsasunbidea Then
  strWhere = strWhere & "FA1503J.CI13CODENTIDAD='NA' AND "
Else
  strWhere = strWhere & "FA1503J.CI13CODENTIDAD<>'NA' AND "
End If

strWhere = strWhere & "FA1503J.FR65FECHA >= " & fFechaCrystal(SSFecInicio.Date)
 strWhere = strWhere & " AND FA1503J.FR65FECHA <= " & fFechaCrystal(SSFecFin.Date)

Call Imprimir_API(strWhere, "ListadoProtesis.rpt") ', formula)
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
   Dim RS As rdoResultset
   Dim Dia, Mes, A�o As String
   Dim FechaAhora As String
'   Set RS = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
'  strAhora = RS(0)
'  FechaAhora = Format(strAhora, "dd/mm/yyyy")
'  dtcFecInicio.Date = DateAdd("d", -1, FechaAhora)
'  txtHora = Format(strHora_Sistema, "HH")
 ' dtcFecFin.Date = DateAdd("d", -1, FechaAhora)
  Set con = rdoEnvironments(0).OpenConnection("Oracle73", False, False, _
  "DSN=Oracle73;servername=EST_PROD.cun;uid=cun;pwd=TUY")
End Sub



