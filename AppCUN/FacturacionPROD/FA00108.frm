VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmCategorias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento del �rbol de categor�as"
   ClientHeight    =   8085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   11685
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton InsertarCategor�as 
      Caption         =   "Salvar Cambios"
      Height          =   285
      Left            =   7245
      TabIndex        =   10
      Top             =   450
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   285
      Left            =   10305
      TabIndex        =   9
      Top             =   405
      Width           =   1320
   End
   Begin VB.CommandButton cmdGrabarPantalla 
      Caption         =   "Salvar Cambios"
      Height          =   285
      Left            =   8865
      TabIndex        =   8
      Top             =   405
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "Iniciar"
      Height          =   285
      Left            =   10305
      TabIndex        =   0
      Top             =   45
      Width           =   1320
   End
   Begin MSMask.MaskEdBox MSKHora 
      Height          =   285
      Left            =   9270
      TabIndex        =   2
      Top             =   45
      Width           =   870
      _ExtentX        =   1535
      _ExtentY        =   503
      _Version        =   327681
      AllowPrompt     =   -1  'True
      MaxLength       =   8
      Mask            =   "##:##:##"
      PromptChar      =   "_"
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCProceso 
      Height          =   285
      Left            =   7335
      TabIndex        =   1
      Top             =   45
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   503
      _StockProps     =   93
      MinDate         =   "1980/1/1"
      MaxDate         =   "3000/12/31"
      ShowCentury     =   -1  'True
   End
   Begin ComctlLib.TreeView tvwCategorias 
      Height          =   5505
      Left            =   5895
      TabIndex        =   4
      Top             =   765
      Width           =   5730
      _ExtentX        =   10107
      _ExtentY        =   9710
      _Version        =   327682
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin ComctlLib.TreeView tvwRamas 
      Height          =   6180
      Left            =   45
      TabIndex        =   3
      Top             =   90
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   10901
      _Version        =   327682
      Indentation     =   36
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame frmAtrib 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   1250
      Index           =   0
      Left            =   90
      TabIndex        =   25
      Top             =   6795
      Visible         =   0   'False
      Width           =   11430
      Begin VB.Frame Frame1 
         Caption         =   "Precio Calculado Relativo al nodo :"
         Height          =   1230
         Left            =   3060
         TabIndex        =   29
         Top             =   0
         Width           =   2895
         Begin VB.TextBox txtRutaRel 
            Height          =   285
            Left            =   50
            TabIndex        =   30
            ToolTipText     =   "Ruta del atributo"
            Top             =   225
            Width           =   2790
         End
         Begin MSMask.MaskEdBox txtRelPor 
            Height          =   285
            Left            =   1575
            TabIndex        =   31
            Top             =   890
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   7
            Format          =   "##0.###"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox txtPrecioFijo 
            Height          =   285
            Left            =   1575
            TabIndex        =   32
            Top             =   575
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   15
            Format          =   "###,###,##0.###"
            PromptChar      =   " "
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "Precio Fijo"
            Height          =   195
            Index           =   10
            Left            =   75
            TabIndex        =   34
            Top             =   600
            Width           =   735
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "% del importe"
            Height          =   195
            Index           =   9
            Left            =   75
            TabIndex        =   33
            Top             =   915
            Width           =   930
         End
      End
      Begin VB.CommandButton cmdGrabar 
         Caption         =   "Grabar"
         Height          =   375
         Left            =   6075
         TabIndex        =   55
         Top             =   810
         Width           =   1005
      End
      Begin VB.CommandButton cmdRecargar 
         Caption         =   "Recargar"
         Height          =   375
         Left            =   7155
         TabIndex        =   54
         Top             =   810
         Width           =   1005
      End
      Begin VB.TextBox txtRUTA 
         Height          =   285
         Left            =   5805
         TabIndex        =   35
         ToolTipText     =   "Ruta del atributo"
         Top             =   90
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton cmdDtoCantidad 
         Caption         =   "Crear"
         Height          =   255
         Left            =   1485
         TabIndex        =   28
         Top             =   905
         Width           =   1245
      End
      Begin VB.CheckBox chkIndFranqUnit 
         Alignment       =   1  'Right Justify
         Caption         =   "Franquicia Unitaria ?"
         Height          =   255
         Left            =   6255
         TabIndex        =   27
         Top             =   90
         Width           =   1770
      End
      Begin VB.CheckBox chkIndFranqSuma 
         Alignment       =   1  'Right Justify
         Caption         =   "Franquicia Suma ?"
         Height          =   255
         Left            =   6255
         TabIndex        =   26
         Top             =   360
         Width           =   1770
      End
      Begin MSMask.MaskEdBox txtPrecio 
         Height          =   285
         Left            =   1485
         TabIndex        =   36
         Top             =   105
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         MaxLength       =   15
         Format          =   "###,###,##0.###"
         PromptChar      =   " "
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFin 
         Height          =   285
         Left            =   9675
         TabIndex        =   37
         Top             =   330
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         Enabled         =   0   'False
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCInicio 
         Height          =   285
         Left            =   9675
         TabIndex        =   38
         Top             =   30
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         Enabled         =   0   'False
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin MSMask.MaskEdBox txtPrecioDia 
         Height          =   285
         Left            =   1485
         TabIndex        =   39
         Top             =   510
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         MaxLength       =   11
         Format          =   "###,###,##0.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox txtDescuento 
         Height          =   285
         Left            =   9675
         TabIndex        =   40
         Top             =   630
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         MaxLength       =   7
         Format          =   "##0.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox txtPeriodoGara 
         Height          =   285
         Left            =   9675
         TabIndex        =   41
         Top             =   930
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         MaxLength       =   5
         Format          =   "#,###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   4
         Left            =   5490
         TabIndex        =   42
         ToolTipText     =   "C�digo del atributo"
         Top             =   360
         Visible         =   0   'False
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         PromptChar      =   " "
      End
      Begin VB.TextBox txtCodAtrib 
         Height          =   285
         Left            =   6165
         TabIndex        =   53
         Top             =   90
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "Precio x Cantidad"
         Height          =   255
         Index           =   16
         Left            =   45
         TabIndex        =   49
         Top             =   930
         Width           =   1245
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "Per�odo Garant�a"
         Height          =   195
         Index           =   15
         Left            =   8340
         TabIndex        =   48
         Top             =   1005
         Width           =   1245
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicio"
         Height          =   195
         Index           =   14
         Left            =   8340
         TabIndex        =   47
         Top             =   105
         Width           =   870
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Fin"
         Height          =   195
         Index           =   13
         Left            =   8340
         TabIndex        =   46
         Top             =   405
         Width           =   705
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "% Descuento"
         Height          =   195
         Index           =   11
         Left            =   8340
         TabIndex        =   45
         Top             =   705
         Width           =   945
      End
      Begin VB.Label al 
         Caption         =   "Precio D�a"
         Height          =   255
         Index           =   7
         Left            =   45
         TabIndex        =   44
         Top             =   525
         Width           =   975
      End
      Begin VB.Label al 
         Caption         =   "Precio Referencia"
         Height          =   255
         Index           =   6
         Left            =   45
         TabIndex        =   43
         Top             =   135
         Width           =   1785
      End
   End
   Begin VB.Frame frmAtrib 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   1250
      Index           =   1
      Left            =   135
      TabIndex        =   11
      Top             =   6795
      Visible         =   0   'False
      Width           =   11430
      Begin VB.ComboBox cboCodEntidad 
         Height          =   315
         Left            =   7155
         TabIndex        =   56
         Text            =   "cboCodEntidad"
         Top             =   855
         Width           =   3930
      End
      Begin VB.TextBox txtCodEntidad 
         Height          =   285
         Left            =   7155
         TabIndex        =   57
         Text            =   " "
         Top             =   855
         Width           =   495
      End
      Begin VB.CheckBox chkIndDesglose 
         Alignment       =   1  'Right Justify
         Caption         =   "Mostrar Desglose ?"
         Height          =   255
         Left            =   5385
         TabIndex        =   19
         Top             =   75
         Width           =   2010
      End
      Begin VB.CheckBox chkIndSuplemento 
         Alignment       =   1  'Right Justify
         Caption         =   "Es un suplemento ?"
         Height          =   255
         Left            =   5385
         TabIndex        =   18
         Top             =   475
         Width           =   2010
      End
      Begin VB.CheckBox chkIndFacturacion 
         Alignment       =   1  'Right Justify
         Caption         =   "Facturaci�n Obligatoria ?"
         Height          =   255
         Left            =   2430
         TabIndex        =   17
         Top             =   475
         Width           =   2235
      End
      Begin VB.CheckBox chkIndLinObig 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea Obligatoria ?"
         Height          =   255
         Left            =   2430
         TabIndex        =   16
         Top             =   75
         Width           =   2235
      End
      Begin VB.CheckBox chkIndDescont 
         Alignment       =   1  'Right Justify
         Caption         =   "Descontable ?"
         Height          =   255
         Left            =   2430
         TabIndex        =   15
         Top             =   875
         Width           =   2235
      End
      Begin VB.CheckBox chkIndExcluido 
         Alignment       =   1  'Right Justify
         Caption         =   "Excluido ?"
         Height          =   255
         Left            =   0
         TabIndex        =   14
         Top             =   875
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.CheckBox chkIndSuficiente 
         Alignment       =   1  'Right Justify
         Caption         =   "Suficiente ?"
         Height          =   255
         Left            =   0
         TabIndex        =   13
         Top             =   475
         Width           =   1455
      End
      Begin VB.CheckBox chkIndNecesario 
         Alignment       =   1  'Right Justify
         Caption         =   "Necesario ?"
         Height          =   255
         Left            =   0
         TabIndex        =   12
         Top             =   75
         Width           =   1455
      End
      Begin MSMask.MaskEdBox txtNoOrden 
         Height          =   285
         Left            =   10020
         TabIndex        =   21
         Top             =   75
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         MaxLength       =   15
         Format          =   "###,###,##0.###"
         PromptChar      =   " "
      End
      Begin VB.CheckBox chkIndOpcional 
         Alignment       =   1  'Right Justify
         Caption         =   "Opcional ?"
         Height          =   195
         Left            =   10170
         TabIndex        =   20
         Top             =   135
         Visible         =   0   'False
         Width           =   1110
      End
      Begin VB.Label al 
         Caption         =   "N� de orden en factura"
         Height          =   255
         Index           =   12
         Left            =   8175
         TabIndex        =   23
         Top             =   100
         Width           =   1785
      End
      Begin VB.Label al 
         Caption         =   "Entidad Responsable"
         Height          =   255
         Index           =   8
         Left            =   5385
         TabIndex        =   22
         Top             =   900
         Width           =   1695
      End
   End
   Begin VB.Frame frmAtrib 
      BorderStyle     =   0  'None
      Caption         =   "C�digo de grupo:"
      Height          =   1185
      Index           =   3
      Left            =   90
      TabIndex        =   58
      Top             =   6795
      Visible         =   0   'False
      Width           =   11475
      Begin VB.ComboBox cboDesOrigen 
         Height          =   315
         Left            =   2835
         TabIndex        =   65
         Top             =   540
         Width           =   4470
      End
      Begin VB.CommandButton cmdGrabarGrupo 
         Caption         =   "&Grabar"
         Height          =   420
         Left            =   7605
         TabIndex        =   64
         Top             =   315
         Width           =   1095
      End
      Begin VB.TextBox txtCodOrigen 
         Height          =   285
         Left            =   1485
         TabIndex        =   63
         Top             =   540
         Width           =   1275
      End
      Begin VB.ComboBox cboGrupo 
         Height          =   315
         Left            =   1485
         TabIndex        =   62
         Top             =   135
         Width           =   5820
      End
      Begin VB.TextBox txtGrupo 
         Height          =   285
         Left            =   1485
         TabIndex        =   61
         Top             =   135
         Width           =   330
      End
      Begin VB.TextBox txtVista 
         Height          =   285
         Left            =   6120
         TabIndex        =   66
         Top             =   135
         Width           =   330
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "C�digo de origen:"
         Height          =   195
         Index           =   1
         Left            =   135
         TabIndex        =   60
         Top             =   585
         Width           =   1245
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "C�digo de grupo:"
         Height          =   195
         Index           =   0
         Left            =   135
         TabIndex        =   59
         Top             =   180
         Width           =   1215
      End
   End
   Begin VB.Frame frmAtrib 
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   1185
      Index           =   2
      Left            =   90
      TabIndex        =   50
      Top             =   6795
      Visible         =   0   'False
      Width           =   11475
      Begin VB.TextBox txtDescripcion 
         Height          =   825
         Left            =   0
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   51
         Text            =   "FA00108.frx":0000
         Top             =   345
         Width           =   11505
      End
      Begin VB.Label al 
         AutoSize        =   -1  'True
         Caption         =   "Esta descripci�n sustituye a la del nodo o categoria en la linea de la factura :"
         Height          =   195
         Index           =   17
         Left            =   0
         TabIndex        =   52
         Top             =   45
         Width           =   5415
      End
   End
   Begin TabDlg.SSTab SSTabAtrib 
      Height          =   1710
      Left            =   45
      TabIndex        =   24
      Top             =   6345
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   3016
      _Version        =   327681
      Tabs            =   4
      TabsPerRow      =   8
      TabHeight       =   397
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Precios"
      TabPicture(0)   =   "FA00108.frx":0006
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Varios"
      TabPicture(1)   =   "FA00108.frx":0022
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Descripci�n"
      TabPicture(2)   =   "FA00108.frx":003E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Propiedades"
      TabPicture(3)   =   "FA00108.frx":005A
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
   End
   Begin VB.Label lblXY 
      AutoSize        =   -1  'True
      Caption         =   "lblXY"
      Height          =   195
      Left            =   10305
      TabIndex        =   7
      Top             =   180
      Visible         =   0   'False
      Width           =   945
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "lbl Drag_Drop"
      Height          =   195
      Left            =   10350
      TabIndex        =   6
      Top             =   45
      Visible         =   0   'False
      Width           =   975
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   6030
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   11
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":0076
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":0390
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":06AA
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":09C4
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":0CDE
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":0FF8
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":1312
            Key             =   "Cat"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":162C
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":1946
            Key             =   "CatAbierto"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":1C60
            Key             =   "EditandoBorrado"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00108.frx":1F7A
            Key             =   "Drag"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Proceso:"
      Height          =   255
      Index           =   0
      Left            =   5895
      TabIndex        =   5
      Top             =   135
      Width           =   1365
   End
   Begin VB.Menu menu 
      Caption         =   "Menu Categor�as"
      Visible         =   0   'False
      Begin VB.Menu mnudchoNuevo 
         Caption         =   "&Nuevo"
      End
      Begin VB.Menu mnudchoCopiar 
         Caption         =   "&Cortar"
         Visible         =   0   'False
      End
      Begin VB.Menu mnudchoPegar 
         Caption         =   "&Pegar"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDchoEliminar 
         Caption         =   "&Eliminar"
      End
      Begin VB.Menu a 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAtributos 
         Caption         =   "&Nuevos atributos"
      End
      Begin VB.Menu b 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCambioNombre 
         Caption         =   "Cambiar &Nombre"
      End
   End
End
Attribute VB_Name = "frmCategorias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private rq(1 To 30) As New rdoQuery
Private qry(1 To 6) As New rdoQuery
Private cSrc$, cDst$
Private Dragging As Boolean
Public FechaProceso As String
Dim TipoCateg As String
Dim Grupo As String
Dim CatGrupo As String
Dim Path As String
Dim NodoHijo As String
Dim nodoPadre As String
Dim insHijo As String
Dim insPadre As String
Dim PathPadre As String
Dim txtHijo As String
Dim txtPadre As String
Dim Modificar As Boolean
Dim Validacion As Boolean
Dim CatPadre As String
Dim Transaccion As Boolean
Dim Expandir As Boolean
Dim Categ As String


Private Sub InitQRY(SQLInsert As String)
    Dim i As Integer
    
'   Borrar una rama de atributos posteriores a la fecha de proceso
    qry(1).SQL = "DELETE FROM FA1500 " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECINICIO )"
    
'   Marcar (L�gicamente) una rama de registros de atributos con fecha de fin='9/9/0999 00:00:00'
'   estas ramas ser�n las que se copien para crear los nuevos atrib.
'   Parametros:
'            0 - RUTA sin barra final (si la tuviera)
'            1 - Fecha proceso
    qry(5).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN )"
                
'   Borrar (L�gicamente) una rama de registros de atributos con fecha de fin=fecha de proceso
'   Parametros:
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - RUTA sin barra final (si la tuviera)
    qry(2).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA LIKE ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS'))"
                
    qry(3).SQL = SQLInsert
    
    ' DEPURAR los atributos
    qry(4).SQL = "DELETE FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN"
    
'   Grabar el descuento en los atributos recien generados y que cumplan una serie de condiciones.

'   Parametros:
'            0 - Descuento
'            1 - RUTA sin barra final (si la tuviera)
'            2 - Fecha proceso

    qry(6).SQL = "UPDATE FA1500 SET " & _
                        "FA15DESCUENTO = ? " & _
                 "WHERE FA15RUTA LIKE ? " & _
                   "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') = FA15FECINICIO )" & _
                   "AND (TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS') = FA15FECFIN )" & _
                   "AND ((FA15PRECIOREF IS NOT NULL) " & _
                   "  OR (FA15PRECIODIA IS NOT NULL)" & _
                   "  OR (FA15FRANQSUMA <> 0)" & _
                   "  OR (FA15FRANQUNI  <> 0)" & _
                   "  OR (FA15FACTOBLIG <> 0)" & _
                   "  OR (FA15LINOBLIG <> 0))" & _
                   "AND (FA15INDDESCONT <>0)"

    For i = 1 To UBound(qry)
        Set qry(i).ActiveConnection = objApp.rdoConnect
        qry(i).Prepared = True
    Next
    
End Sub

Private Sub Actualizar(Codigo As String, Nombre As String, FechaProceso As String, Descuento As CamposAtrib)
    Dim Ruta As String
    
    ' quitar la barra final si la tiene
    If Right(Codigo, 1) = "/" Then
        Ruta = Left(Codigo, Len(Codigo) - 1)
    Else
        Ruta = Codigo
    End If
    
    ' borrar los atributos posteriores a la fecha
    qry(1).rdoParameters(0) = Ruta & "%"
    qry(1).rdoParameters(1) = FechaProceso & " " & MSKHora.Text
    qry(1).Execute
    
    ' marcar los registros de atributos afectados por la modificacion
    qry(5).rdoParameters(0) = Ruta & "%"
    qry(5).rdoParameters(1) = FechaProceso & " " & MSKHora.Text
    qry(5).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    qry(3).rdoParameters(0) = FechaProceso & " " & MSKHora.Text
    qry(3).rdoParameters(1) = Ruta & "%"
    qry(3).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    If Descuento.Actualizar = True Then
        qry(6).rdoParameters(0) = Descuento.Valor
        qry(6).rdoParameters(1) = Ruta & "%"
        qry(6).rdoParameters(2) = FechaProceso & " " & MSKHora.Text
        qry(6).Execute
    End If
    ' Borrar los registros de atributos afectados por la modificacion
    qry(2).rdoParameters(0) = FechaProceso & " " & MSKHora.Text
    qry(2).rdoParameters(1) = Ruta & "%"
'    qry(2).rdoParameters(2) = FechaProceso
    qry(2).Execute
    
    ' depurar los atributos
    qry(4).Execute
    
End Sub
Private Sub ActualizarConciertos(Codigo As String, Nombre As String, FechaProceso As String, Descuento As CamposAtrib, Concierto As String)
    Dim Ruta As String
    
    
    ' borrar los atributos posteriores a la fecha
    qry(1).rdoParameters(0) = "." & Concierto & ".%/%." & Codigo & ".%"
    qry(1).rdoParameters(1) = FechaProceso & " " & MSKHora.Text
    qry(1).Execute
    
    ' marcar los registros de atributos afectados por la modificacion
    qry(5).rdoParameters(0) = "." & Concierto & ".%/%." & Codigo & ".%"
    qry(5).rdoParameters(1) = FechaProceso & " " & MSKHora.Text
    qry(5).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    qry(3).rdoParameters(0) = FechaProceso & " " & MSKHora.Text
    qry(3).rdoParameters(1) = "." & Concierto & "%/%." & Codigo & ".%"
    qry(3).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    If Descuento.Actualizar = True Then
        qry(6).rdoParameters(0) = Descuento.Valor
        qry(6).rdoParameters(1) = "." & Concierto & ".%/%." & Codigo & ".%"
        qry(6).rdoParameters(2) = FechaProceso & " " & MSKHora.Text
        qry(6).Execute
    End If
    ' Borrar los registros de atributos afectados por la modificacion
    qry(2).rdoParameters(0) = FechaProceso & " " & MSKHora.Text
    qry(2).rdoParameters(1) = "." & Concierto & ".%/%." & Codigo & ".%"
'    qry(2).rdoParameters(2) = FechaProceso
    qry(2).Execute
    
    ' depurar los atributos
    qry(4).Execute
    
End Sub



Private Sub cboCodEntidad_Click()
Dim MiRs As rdoResultset
Dim mIsQl As String

  If Trim(cboCodEntidad.Text) <> "" Then
    mIsQl = "SELECT CI13CODENTIDAD FROM CI1300 WHERE CI13DESENTIDAD = '" & cboCodEntidad.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      txtCodEntidad.Text = MiRs("CI13CODENTIDAD")
    Else
      MsgBox "NO EXISTE LA ENTIDAD INDICADA O LA DESCRIPCI�N ES ERRONEA"
      txtCodEntidad.Text = ""
    End If
  Else
    txtCodEntidad.Text = ""
  End If
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
'Introducimos en el combo box todas las entidades activas en la BD
Private Sub cboCodEntidad_DropDown()
Dim MiRs As rdoResultset
Dim mIsQl As String

  cboCodEntidad.Clear
  mIsQl = "SELECT CI13CODENTIDAD, CI13DESENTIDAD FROM CI1300"
  Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
  If Not MiRs.EOF Then
    MiRs.MoveFirst
    While Not MiRs.EOF
      cboCodEntidad.AddItem MiRs("CI13DESENTIDAD")
      MiRs.MoveNext
    Wend
  Else
    MsgBox "NO HAY ENTIDADES EN LA BD"
  End If

End Sub
Private Sub cboCodEntidad_LostFocus()
Dim MiRs As rdoResultset
Dim mIsQl As String

  'Comprobamos que la descripci�n de la entidad coincida con una que est� en la BD.
  If Trim(cboCodEntidad.Text) <> "" Then
    mIsQl = "SELECT CI13CODENTIDAD FROM CI1300 WHERE CI13DESENTIDAD = '" & cboCodEntidad.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      txtCodEntidad.Text = MiRs("CI13CODENTIDAD")
    Else
      MsgBox "NO EXISTE LA ENTIDAD INDICADA O LA DESCRIPCI�N ES ERRONEA"
      txtCodEntidad.Text = ""
      If cboCodEntidad.Enabled = True Then cboCodEntidad.SetFocus
    End If
  Else
    'Si la descripci�n es nula asignamos valor nulo a el textbox que contiene el c�digo.
    cboCodEntidad.Text = ""
    txtCodEntidad.Text = ""
  End If

End Sub

Private Sub cboDesOrigen_Click()
Dim MiRs As rdoResultset
Dim mIsQl As String

  If Trim(cboDesOrigen.Text) <> "" Then
    mIsQl = "SELECT CODIGO FROM " & Me.txtVista.Text & " WHERE DESIGNACION = '" & cboDesOrigen.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Me.txtCodOrigen.Text = MiRs("CODIGO")
    Else
      MsgBox "No hay categor�as en el grupo seleccionado", vbInformation + vbOKOnly, "Aviso"
      txtCodOrigen.Text = ""
    End If
  Else
    MsgBox "Debe seleccionar un origen", vbOKOnly + vbInformation, "Aviso"
  End If
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
'Introducimos en el combo box todas las entidades activas en la BD
Private Sub cboDesOrigen_DropDown()
Dim MiRs As rdoResultset
Dim mIsQl As String

  cboDesOrigen.Clear
  If Trim(Me.txtVista.Text) <> "" Then
    mIsQl = "SELECT DESIGNACION FROM " & Me.txtVista.Text
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveFirst
      While Not MiRs.EOF
        If Not IsNull(MiRs("DESIGNACION")) Then
          cboDesOrigen.AddItem MiRs("DESIGNACION")
        End If
        MiRs.MoveNext
      Wend
    Else
      MsgBox "No hay categor�as dentro del grupo seleccionado", vbOKOnly + vbInformation, "Aviso"
    End If
  Else
    MsgBox "Debe seleccionar un grupo para poder seleccionar las categor�as", vbOKOnly + vbInformation, "Aviso"
  End If

End Sub
Private Sub cboDesOrigen_LostFocus()
Dim MiRs As rdoResultset
Dim mIsQl As String

  If Trim(cboDesOrigen.Text) <> "" Then
    mIsQl = "SELECT CODIGO FROM " & Me.txtVista.Text & " WHERE DESIGNACION = '" & cboDesOrigen.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Me.txtCodOrigen.Text = MiRs("CODIGO")
    Else
      MsgBox "No hay categor�as en el grupo seleccionado", vbInformation + vbOKOnly, "Aviso"
      txtCodOrigen.Text = ""
    End If
  Else
    MsgBox "Debe seleccionar un origen", vbOKOnly + vbInformation, "Aviso"
  End If
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub

Private Sub cboGrupo_Click()
Dim MiRs As rdoResultset
Dim mIsQl As String

  If Trim(cboCodEntidad.Text) <> "" Then
    mIsQl = "SELECT FA08CODGRUPO,FA08VISTAASOC FROM FA0800 WHERE FA08DESIG = '" & Me.cboGrupo.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Me.txtGrupo.Text = MiRs("FA08CODGRUPO")
      Me.txtVista.Text = MiRs("FA08VISTAASOC")
    Else
      MsgBox "NO EXISTE LA EL GRUPO INDICADO O LA DESCRIPCI�N ES ERRONEA"
      Me.txtGrupo.Text = ""
    End If
  Else
    Me.txtGrupo.Text = ""
  End If

End Sub
'Introducimos en el combo box todas las entidades activas en la BD
Private Sub cboGrupo_DropDown()
Dim MiRs As rdoResultset
Dim mIsQl As String

  cboGrupo.Clear
  mIsQl = "SELECT FA08DESIG FROM FA0800 Where FA11CODESTGRUPO = 1"
  Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
  If Not MiRs.EOF Then
    MiRs.MoveFirst
    While Not MiRs.EOF
      Me.cboGrupo.AddItem MiRs("FA08DESIG")
      MiRs.MoveNext
    Wend
  Else
    MsgBox "NO HAY GRUPOS DE ORIGEN EN LA BD"
  End If

End Sub
Private Sub cboGrupo_LostFocus()
Dim MiRs As rdoResultset
Dim mIsQl As String

  'Comprobamos que la descripci�n de la entidad coincida con una que est� en la BD.
  If Trim(Me.cboGrupo.Text) <> "" Then
    mIsQl = "SELECT FA08CODGRUPO,FA08VISTAASOC FROM FA0800 WHERE FA08DESIG = '" & Me.cboGrupo.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Me.txtGrupo.Text = MiRs("FA08CODGRUPO")
      Me.txtVista.Text = MiRs("FA08VISTAASOC") & ""
    Else
      MsgBox "NO EXISTE EL GRUPO INDICADO O LA DESCRIPCI�N ES ERRONEA"
      txtCodEntidad.Text = ""
      If cboGrupo.Enabled = True Then cboGrupo.SetFocus
    End If
  Else
    'Si la descripci�n es nula asignamos valor nulo a el textbox que contiene el c�digo.
    cboGrupo.Text = ""
    txtGrupo.Text = ""
  End If

End Sub

Private Sub chkindDescont_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub chkindExcluido_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub chkindFacturacion_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub

Private Sub chkIndFranqSuma_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub

Private Sub chkIndFranqUnit_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub


Private Sub chkindNecesario_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub chkindOpcional_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub chkindSuficiente_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
'A�adimos una categor�a hija dentro del arbol de ramas de categor�as
Private Sub AppendHija(Padre$, padreKey$, Hijo$, hijoText$, Grupo$)
    Dim n As Node, RS As rdoResultset
    Dim str As String
    Dim RutaOrigen As String

    On Error Resume Next
    Set n = tvwRamas.Nodes.Item(padreKey & "dummy")
    str = n.Key

    On Error GoTo 0

    On Error GoTo YaEsHijo
    objApp.BeginTrans
      'Grabamos los datos correspondientes a la rama en la BD dentro de la funci�n GrabarRama
      Call GrabarRama(Padre, hijoText, Hijo, padreKey, Grupo)
      Set n = tvwRamas.Nodes.Add(padreKey, tvwChild, padreKey & CatPadre & ".", Left(hijoText, 99), "CatCerrado", "Editando")
      Expandir = True
      n.EnsureVisible
      Expandir = False
      n.Tag = CatPadre
      tvwCategorias.Nodes.Remove ("C" & Hijo)
    objApp.CommitTrans
    On Error GoTo 0
    Transaccion = True
Exit Sub

YaEsHijo:
    On Error GoTo 0
    objApp.RollbackTrans
    MsgBox error
    Exit Sub
End Sub
'De momento no hace nada
Private Sub CambiarHija(Padre$, padreKey$, NuevoKey$, Hijo$, hijoText$)
    Dim n As Node, RS As rdoResultset
    Dim str As String
    Dim RutaOrigen As String

    On Error Resume Next
    Set n = tvwRamas.Nodes.Item(padreKey & "dummy")
    str = n.Key
    On Error GoTo 0

    On Error GoTo YaEsHijo
    objApp.BeginTrans
      rq(6).SQL = "Insert into FA0600 (FA06CODRAMACAT, FA05CODCATEG, FA05CODCATEG_P, " & _
                "FA06FECINICIO, FA06FECFIN, FA15RUTA) values (?,?,?,?,?,?)"
      rq(7).SQL = "Insert into FA1500 (FA15CODATRIB, FA15RUTA, FA15FECINICIO, FA15FECFIN, " & _
                "FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, FA15INDDESCONT, " & _
                "FA15INDLINOBLIG, FA15INDFACTOBLIG, FA15INDOPCIONAL) VALUES (?,?,?,?,0,0,0,0,0,0,0)"
      rq(20).SQL = "Update FA0600 set FA06FECFIN = ? where FA15RUTA = ?"
      rq(21).SQL = "Update FA1500 set FA15FECFIN = ? where FA15RUTA = ?"

      rq(6).rdoParameters(5) = NuevoKey & Padre & "."
      rq(6).rdoParameters(0) = fNextClave("FA06CODRAMACAT", "FA0600")
      rq(6).rdoParameters(1) = Padre
      rq(6).rdoParameters(2) = insPadre
      rq(6).rdoParameters(3) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
      rq(6).rdoParameters(4) = "31/12/3000 00:00:00"
      rq(6).Execute
      rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
      rq(7).rdoParameters(1) = NuevoKey & Padre & "."
      rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
      rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
      rq(7).Execute
      rq(20).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
      rq(20).rdoParameters(1) = padreKey
      rq(20).Execute
      rq(21).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
      rq(21).rdoParameters(1) = padreKey
      rq(21).Execute

      tvwRamas.Nodes.Remove padreKey
      Set n = tvwRamas.Nodes.Add(NuevoKey, tvwChild, NuevoKey & Hijo & ".", hijoText, "CatCerrado", "Editando")
      n.EnsureVisible
    objApp.CommitTrans
    On Error GoTo 0
Exit Sub

YaEsHijo:
    On Error GoTo 0
    objApp.RollbackTrans
    MsgBox "error"
    Exit Sub
End Sub
'A�adimos un grupo entero dentro del �rbol de ramas de categor�as.
Private Sub AppendGrupo(Padre$, padreKey$, Hijo$, hijoText$)
    Dim n As Node, RS As rdoResultset
    Dim str As String
    Dim RutaOrigen As String
    Dim mIsQl As String
    Dim MiRs As rdoResultset

    On Error Resume Next
    Set n = tvwRamas.Nodes.Item(padreKey & "dummy")
    str = n.Key
    On Error GoTo 0
    CatGrupo = 0
    On Error GoTo YaEsHijo
    objApp.BeginTrans
      'Grabamos el grupo dentro de la BD
      Call GrabarGrupo(Padre, hijoText, Hijo, padreKey)
      mIsQl = "Select * from fa9901J where fa08codgrupo = " & Grupo & " and indutilizado = 0"
      Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        While Not MiRs.EOF
          'Grabamos dentro de la BD cada una de las categor�as que pertenecen al grupo.
          Call GrabarRama(CatGrupo, MiRs("desorigen"), MiRs("codorigen"), padreKey & CatGrupo & ".", Grupo)
          MiRs.MoveNext
        Wend
      End If
      'A�adimos el nodo al �rbol.
      Set n = tvwRamas.Nodes.Add(padreKey, tvwChild, padreKey & Hijo & ".", hijoText, "CatCerrado", "Editando")
      n.Tag = CatGrupo
      'A�adimos la rama al �rbol
      tvwRamas.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
    objApp.CommitTrans
    CatGrupo = 0
    'Quitamos el grupo del treeview de las categor�as
    tvwCategorias.Nodes.Remove ("G" & Grupo)
    On Error GoTo 0
    Transaccion = True
Exit Sub

YaEsHijo:
    On Error GoTo 0
    objApp.RollbackTrans
    MsgBox error
    Exit Sub
    Resume Next
End Sub
' Borramos una rama del �rbol de categor�as
Sub BorrarRama(Padre$, Hijo$, Camino$, n As ComctlLib.Node)
Dim mIsQl As String
Dim MiRs As rdoResultset
Dim NODX As Node

  On Error GoTo ErrorenBorrado
  'Comprobamos si esa categor�a tiene hijos (en ese caso no se puede borrar).
  rq(4).rdoParameters(0).Value = Hijo
  rq(4).rdoParameters(1).Value = Me.SDCProceso & " " & MSKHora.Text
  rq(4).rdoParameters(2).Value = Me.SDCProceso & " " & MSKHora.Text
  Set MiRs = rq(4).OpenResultset
  If Not MiRs.EOF Then
    MsgBox "Esta categor�a tiene hijas y no se puede borrar."
  Else
    If tvwRamas.SelectedItem.Image = "Borrado" Then
      rq(26).rdoParameters(0) = Padre
      rq(26).rdoParameters(1) = Hijo
      rq(26).rdoParameters(2) = Me.SDCProceso & " " & MSKHora.Text
      rq(26).rdoParameters(3) = Me.SDCProceso & " " & MSKHora.Text
      Set MiRs = rq(26).OpenResultset
      If Not MiRs.EOF Then
        MsgBox MiRs(0)
      End If
      rq(15).rdoParameters(3) = Format(MiRs(0), "DD/MM/YYYY") & " 00:00:00"
      rq(16).rdoParameters(2) = Format(MiRs(0), "DD/MM/YYYY") & " 00:00:00"
    Else
      rq(15).rdoParameters(3) = "31/12/3000 00:00:00"
      rq(16).rdoParameters(2) = "31/12/3000 00:00:00"
    End If
    'Ponemos en la tabla 06 la fecha de proceso como fecha de fin de validez
    rq(15).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
    rq(15).rdoParameters(1) = Hijo
    rq(15).rdoParameters(2) = Padre
    rq(15).Execute
    'Ponemos en la tabla de atributos la fecha de proceso como fecha de fin de validez
    rq(16).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
    rq(16).rdoParameters(1) = Camino
    rq(16).Execute
    rq(17).rdoParameters(0) = Hijo
    Set MiRs = rq(17).OpenResultset
    'Buscamos los conciertos que tengan esa categor�a y le ponemos la fecha de f�n de validez
    'MiSqL = "Select FA15RUTA from FA1500 where FA15RUTA like '%./%." & Padre & "." & Hijo & ".'"
    mIsQl = "Select FA15RUTA from FA1500 where FA15ESCAT=1 AND FA15CODFIN = " & Hijo & " AND FA15RUTA like '%./%." & Padre & "." & Hijo & ".'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, , , rdExecDirect)
    If Not MiRs.EOF Then
      While Not MiRs.EOF
        rq(24).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
        rq(24).rdoParameters(1) = MiRs(0)
        rq(24).Execute
        MiRs.MoveNext
      Wend
    End If
    'Quitamos la categor�a del �rbol de ramas de categor�as.
    tvwRamas.Nodes.Remove n.Key
    Transaccion = True
  End If
Exit Sub

ErrorenBorrado:
  MsgBox "Se ha producido un error en el borrado", vbInformation + vbOKOnly, "Aviso"
  objApp.rdoConnect.RollbackTrans
End Sub
Private Sub CopiarAtribDeCat(RutaOrigen As String, RutaDest As String)
    rq(12).rdoParameters(0) = RutaOrigen
    rq(12).rdoParameters(1) = RutaDest
    rq(12).rdoParameters(2) = RutaOrigen
    rq(12).Execute
End Sub

Private Sub chkIndLinObig_Click()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub

Private Sub cmdGrabar_Click()

  Call ValidarCampos
  If Validacion = True Then
    On Error GoTo ErrorenGrabacion
    objApp.BeginTrans
      'Ponemos fecha de f�n a los antiguos.
      rq(22).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & Me.MSKHora.Text
      rq(22).rdoParameters(1) = Me.txtRUTA.Text
      rq(22).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & Me.MSKHora.Text
      rq(22).Execute
      'Insertamos en la tabla 15 los valores correspondientes a los nuevos atributos de la categor�a
      rq(13).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
      rq(13).rdoParameters(1) = Me.txtPrecio.Text
      rq(13).rdoParameters(2) = Me.txtPrecioDia.Text
      rq(13).rdoParameters(3) = Me.txtDescuento.Text
      rq(13).rdoParameters(4) = Me.chkIndFranqUnit.Value
      rq(13).rdoParameters(5) = Me.chkIndFranqSuma.Value
      rq(13).rdoParameters(6) = Me.txtPeriodoGara.Text
      rq(13).rdoParameters(7) = Me.chkIndNecesario
      rq(13).rdoParameters(8) = Me.chkIndExcluido.Value
      rq(13).rdoParameters(9) = Me.chkIndSuficiente.Value
      rq(13).rdoParameters(10) = Me.chkIndDescont.Value
      rq(13).rdoParameters(11) = Me.chkIndLinObig.Value
      rq(13).rdoParameters(12) = Me.chkIndFacturacion.Value
      rq(13).rdoParameters(13) = Me.chkIndOpcional.Value
      rq(13).rdoParameters(14) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & Me.MSKHora.Text
      rq(13).rdoParameters(15) = "31/12/3000 00:00:00"
      rq(13).rdoParameters(16) = Me.txtRUTA.Text
      rq(13).rdoParameters(17) = Me.txtCodEntidad.Text
      rq(13).rdoParameters(18) = chkIndDesglose.Value
      rq(13).rdoParameters(19) = txtDescripcion.Text
      rq(13).rdoParameters(20) = Me.txtRutaRel.Text
      rq(13).rdoParameters(21) = Me.txtPrecioFijo.Text
      rq(13).rdoParameters(22) = Me.txtRelPor.Text
      rq(13).rdoParameters(23) = txtNoOrden.Text
      rq(13).rdoParameters(24) = Me.chkIndSuplemento.Value
      rq(13).Execute
      'Borramos los que sobran
      rq(27).Execute
    objApp.CommitTrans
    Modificar = False
    cmdGrabar.Enabled = False
    cmdRecargar.Enabled = False
    Transaccion = True
  End If
Exit Sub
ErrorenGrabacion:
  MsgBox "Se ha producido un error en la grabaci�n, compruebe los datos e int�ntelo m�s tarde", vbCritical + vbOKOnly, "Aviso"
  objApp.RollbackTrans

End Sub

Private Sub cmdGrabarGrupo_Click()
Dim respuesta As Integer
Dim mIsQl As String

  respuesta = MsgBox("�Desea guardar los c�digos de grupo y de origen?", vbYesNo + vbInformation, "Atenci�n")
  If respuesta = vbYes Then
    mIsQl = "Update FA0500 Set FA08CODGRUPO = " & Me.txtGrupo.Text & _
                "? And FA05CODORIGEN = '" & Me.txtCodOrigen.Text & "'" & _
                "Where FA05CODCATEG = " & Me.tvwRamas.SelectedItem.Tag
    objApp.rdoConnect.Execute mIsQl
  End If
  
End Sub

Private Sub cmdGrabarPantalla_Click()
  objApp.CommitTrans
  Transaccion = False
End Sub
Private Sub cmdIniciar_Click()
  Call InitTVCateg
  Call InitTVRamas
End Sub
Private Sub cmdRecargar_Click()
    Call tvwRamas_NodeClick(tvwRamas.SelectedItem)
    cmdGrabar.Enabled = False
    cmdRecargar.Enabled = False
End Sub
Private Sub cmdSalir_Click()
Dim respuesta
  If Transaccion = True Then
    respuesta = MsgBox("Se ha realizado cambios, �desea guardarlos?", vbQuestion + vbYesNo, "Aviso")
    If respuesta = vbYes Then
      objApp.CommitTrans
      Transaccion = False
    Else
      objApp.RollbackTrans
      Transaccion = False
    End If
  Else
    objApp.RollbackTrans
    Transaccion = False
  End If
  Unload Me
End Sub
Private Sub Form_Load()
Dim MiRs As rdoResultset
Dim Dia, MES, ANYO As String

  Set MiRs = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRs.EOF Then
      Dia = Day(MiRs(0))
      MES = Month(MiRs(0))
      ANYO = Year(MiRs(0)) - 1
      SDCProceso.MinDate = Format("01/01/" & ANYO, "dd/mm/yyyy")
      'SDCProceso.MinDate = "01/01/1999"
  End If
  SDCProceso.MaxDate = Format(DateAdd("m", 12, SDCProceso.MinDate))
  MSKHora.Text = Format("00:00:00", "hh:mm:ss")
  Call InitRQ
  Transaccion = False
  objApp.BeginTrans
  objCW.blnAutoDisconnect = False
  objCW.SetClockEnable False
End Sub

Sub ValidarCampos()
Dim md As rdoConnection

  Validacion = True
  If Trim(Me.SDCInicio.Date) <> "" Then
    If Not IsDate(SDCInicio.Date) Then
      SDCInicio.Date = ""
      If SDCInicio.Enabled = True Then SDCInicio.SetFocus
      Validacion = False
      MsgBox "El campo Fecha de inicio de Validez es un campo de tipo fecha", vbInformation + vbOKOnly, "Tipo de datos no v�lido"
      Exit Sub
    End If
  End If

  If Trim(Me.SDCFin.Date) <> "" Then
    If Not IsDate(SDCFin.Date) Then
      SDCFin.Date = ""
      If SDCFin.Enabled = True Then SDCFin.SetFocus
      Validacion = False
      MsgBox "El campo Fecha de fin de validez es un campo de tipo fecha", vbInformation + vbOKOnly, "Tipo de datos no v�lido"
      Exit Sub
    End If
  End If


  If Trim(txtPrecio) <> "" Then
    If Not IsNumeric(Me.txtPrecio) Then
      txtPrecio = ""
      If txtPrecio.Enabled = True Then txtPrecio.SetFocus
      Validacion = False
      MsgBox "El campo Precio es un campo de tipo num�rico", vbInformation + vbOKOnly, "Tipo de datos no v�lido"
      Exit Sub
    End If
  End If

  If Trim(Me.txtPrecioDia) <> "" Then
    If Not IsNumeric(Me.txtPrecioDia) Then
      txtPrecioDia = ""
      If txtPrecioDia.Enabled = True Then txtPrecioDia.SetFocus
      Validacion = False
      MsgBox "El campo Precio/dia es un campo de tipo num�rico", vbInformation + vbOKOnly, "Tipo de datos no v�lido"
      Exit Sub
    End If
  End If

  If Trim(txtDescuento) <> "" Then
    If Not IsNumeric(Me.txtDescuento) Then
      txtDescuento = ""
      If txtDescuento.Enabled = True Then txtDescuento.SetFocus
      Validacion = False
      MsgBox "El campo Descuento es un campo de tipo num�rico", vbInformation + vbOKOnly, "Tipo de dato no v�lido"
      Exit Sub
    End If
  End If

  
  If Trim(txtPeriodoGara) <> "" Then
    If Not IsNumeric(txtPeriodoGara) Then
      txtPeriodoGara = ""
      If txtPeriodoGara.Enabled = True Then txtPeriodoGara.SetFocus
      Validacion = False
      MsgBox "El campo Periodo de Garant�a es un campo de tipo num�rico", vbInformation + vbOKOnly, "Tipo de dato no v�lido"
      Exit Sub
    End If
  End If

End Sub
Private Sub GrabarRama(Padre$, NodoHijo As Variant, CatHija As Variant, Ruta As String, Grupo As String)
Dim mIsQl As String
Dim MiRs As rdoResultset
    
    'PADRE->PADRE (c�DIGO DE LA CATEGOR�A PADRE)
    'NODOHIJO -> HIJO (TEXTO DE LA CATEGOR�A HIJA)
    'CATHIJA -> CATEGORIAS HIJA (C�DIGO DE LA CATEGOR�A HIJA)
    'RUTA -> RUTA (RUTA DE LA CATEGOR�A PADRE)
    
    'Comprobamos que la categor�a no exista en la tabla 05
    rq(14).rdoParameters(0) = CatHija
    rq(14).rdoParameters(1) = Grupo
    Set MiRs = rq(14).OpenResultset
    If Not MiRs.EOF Then
      'La categor�a ya existe no se inserta
      rq(6).rdoParameters(1) = MiRs(0)
      CatPadre = MiRs(0)
      rq(6).rdoParameters(5) = Ruta & MiRs(0) & "."
      rq(7).rdoParameters(1) = Ruta & MiRs(0) & "."
    Else
      rq(5).rdoParameters(0) = fNextClave("FA05CODCATEG", "FA0500")
      CatPadre = rq(5).rdoParameters(0)
      rq(5).rdoParameters(1) = NodoHijo 'DESIGNACION
      rq(5).rdoParameters(2) = Grupo 'GRUPO
      rq(5).rdoParameters(3) = CatHija
      rq(5).rdoParameters(4) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
      rq(5).rdoParameters(5) = "31/12/3000 00:00:00"
      rq(5).rdoParameters(6) = Padre
      rq(5).Execute
      rq(6).rdoParameters(1) = rq(5).rdoParameters(0)
      rq(6).rdoParameters(5) = Ruta & rq(5).rdoParameters(0) & "."
      rq(7).rdoParameters(1) = Ruta & rq(5).rdoParameters(0) & "."
    End If
    rq(6).rdoParameters(0) = fNextClave("FA06CODRAMACAT", "FA0600")
    rq(6).rdoParameters(2) = Padre
    rq(6).rdoParameters(3) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
    rq(6).rdoParameters(4) = "31/12/3000 00:00:00"
    rq(6).Execute
    rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
    rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
    rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
    rq(7).Execute
    'A�adimos la categor�a a todos los conciertos en los que la rama padre est� contenida.
    'MiSqL = "Select FA15RUTA from FA1500 where FA15RUTA like '%./%." & Padre & ".' and FA15FECFIN = TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS')"
    mIsQl = "Select FA15RUTA from FA1500 where FA15ESCAT=1 AND FA15CODFIN =" & Padre & " and FA15FECFIN = TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS')"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, , , rdExecDirect)
    If Not MiRs.EOF Then
      While Not MiRs.EOF
        rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
        rq(7).rdoParameters(1) = MiRs(0) & CatPadre & "."
        rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
        rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
        rq(7).Execute
        MiRs.MoveNext
      Wend
    End If
End Sub
Private Sub GrabarGrupo(Padre$, NodoHijo As Variant, CatHija As Variant, Ruta As String)
Dim mIsQl As String
Dim MiRs As rdoResultset

    ' Crear la rama de conciertos con un hijo que es NODO
    rq(19).rdoParameters(0) = CatHija
    Set MiRs = rq(19).OpenResultset
    If Not MiRs.EOF Then
      'La categor�a ya existe no se inserta
      rq(6).rdoParameters(1) = MiRs(0)
      CatPadre = MiRs(0)
      rq(6).rdoParameters(5) = Ruta & MiRs(0) & "."
      rq(7).rdoParameters(1) = Ruta & MiRs(0) & "."
    Else
      rq(5).rdoParameters(0) = fNextClave("FA05CODCATEG", "FA0500")
      CatPadre = rq(5).rdoParameters(0)
      rq(5).rdoParameters(1) = NodoHijo 'DESIGNACION
      rq(5).rdoParameters(2) = Grupo 'GRUPO
      rq(5).rdoParameters(3) = ""
      rq(5).rdoParameters(4) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
      rq(5).rdoParameters(5) = "31/12/3000 00:00:00"
      rq(5).rdoParameters(6) = Padre
      rq(5).Execute
      rq(6).rdoParameters(1) = rq(5).rdoParameters(0)
      rq(6).rdoParameters(5) = Ruta & rq(5).rdoParameters(0) & "."
      rq(7).rdoParameters(1) = Ruta & rq(5).rdoParameters(0) & "."
    End If
    rq(6).rdoParameters(0) = fNextClave("FA06CODRAMACAT", "FA0600")
    rq(6).rdoParameters(2) = Padre
    rq(6).rdoParameters(3) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
    rq(6).rdoParameters(4) = "31/12/3000 00:00:00"
    rq(6).Execute
    rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
    rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
    rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
    rq(7).Execute
    'Creamos la variable Catgrupo para asignar el valor de la categor�a padre
    'cuando queremos pasar un grupo
    If Val(CatGrupo) = 0 Then
      If IsNull(rq(5).rdoParameters(0)) Then
        CatGrupo = MiRs(0)
      Else
        CatGrupo = rq(5).rdoParameters(0)
      End If
    End If
  
End Sub
Private Sub CrearAtribVacio(Ruta As String)
    ' Crear los atributos de un nuevo nodo en vacio
    rq(8).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
    rq(8).rdoParameters(1) = Ruta
    rq(8).rdoParameters(2) = FechaProceso
    rq(8).rdoParameters(3) = "31/12/3000 00:00:00"
    rq(8).Execute
End Sub
Sub InitTVCateg()
    Dim RS As rdoResultset, n As Node
    Dim mIsQl As String, MiRs As rdoResultset
    tvwCategorias.Nodes.Clear

    rq(25).rdoParameters(0) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " 00:00:00"
    rq(25).OpenResultset
    Set RS = rq(1).OpenResultset
    If Not RS.EOF Then
      While Not RS.EOF
        If Not IsNull(RS(0)) Then
          Set n = tvwCategorias.Nodes.Add(, , "G" & RS(0), Left(RS(1), 50), "CatCerrado", "Editando")
          n.Tag = RS(0)
          tvwCategorias.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
        End If
        RS.MoveNext
      Wend
    End If
    RS.Close
    Set RS = Nothing
End Sub
Sub InitTVRamas()
    Dim RS As rdoResultset, n As Node
    tvwRamas.Nodes.Clear
    rq(3).rdoParameters(0) = Format(Me.SDCProceso.Date, "dd/mm/yyyy") & " 00:00:00"
    rq(3).rdoParameters(1) = Format(Me.SDCProceso.Date, "dd/mm/yyyy") & " 00:00:00"
    Set RS = rq(3).OpenResultset
    While Not RS.EOF
      Set n = Me.tvwRamas.Nodes.Add(, , "/." & RS(0) & ".", RS(1), "NodoCerrado", "Editando")
      n.Tag = RS(0)
      tvwRamas.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
      RS.MoveNext
    Wend
    RS.Close
    Set RS = Nothing
End Sub
'Inicializacion de los rdoQuery
Private Sub InitRQ()
    Dim i%
    ' Select de inicializacion de grupos (el primer nivel del treeview son los grupos)
    rq(1).SQL = "Select /*+ RULE */ DISTINCT FA9901J.FA08CODGRUPO, FA08DESIG From FA9901J WHERE INDUTILIZADO = 0"

    ' Select para recoger las pruebas que cuelgan de un grupo
    rq(2).SQL = "SELECT CODORIGEN, DESORIGEN FROM FA9901J " & _
                "WHERE FA08CODGRUPO=? AND INDUTILIZADO = 0 ORDER BY DESORIGEN"

    ' Select para recoger las categorias hijas de una categor�a
    rq(3).SQL = "SELECT FA0600.FA05CODCATEG, FA0500.FA05DESIG FROM FA0500,FA0600 " & _
                "WHERE FA0600.FA05CODCATEG_P IS NULL " & _
                "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') >= FA06FECINICIO " & _
                "AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN)" & _
                "AND FA0500.FA05CODCATEG = FA0600.FA05CODCATEG"

    ' Select para recoger las categorias hijas de una categor�a
    rq(4).SQL = "SELECT FA0600.FA05CODCATEG, FA0500.FA05DESIG, FA06FECFIN FROM FA0500,FA0600 " & _
                "WHERE FA0600.FA05CODCATEG_P = ? " & _
                "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') >= FA06FECINICIO AND  TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN)" & _
                "AND FA0500.FA05CODCATEG = FA0600.FA05CODCATEG ORDER BY FA0500.FA05DESIG"

    ' Crea nuevo categor�a desde �rbol
    rq(5).SQL = "Insert into FA0500 (FA05CODCATEG, FA05DESIG, FA08CODGRUPO, FA05CODORIGEN, " & _
                "FA05FECINICIO,FA05FECFIN,FA05CODCATEG_P) values (?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?)"
    rq(6).SQL = "Insert into FA0600 (FA06CODRAMACAT, FA05CODCATEG, FA05CODCATEG_P, " & _
                "FA06FECINICIO, FA06FECFIN, FA15RUTA) values (?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?)"
    rq(7).SQL = "Insert into FA1500 (FA15CODATRIB, FA15RUTA, FA15FECINICIO, FA15FECFIN, " & _
                "FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, FA15INDDESCONT, " & _
                "FA15INDLINOBLIG, FA15INDFACTOBLIG, FA15INDOPCIONAL) VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),0,0,0,0,0,0,0)"
    rq(8).SQL = "UPDATE FA9901J SET INDUTILIZADO = 1 WHERE CODORIGEN = ? AND FA08CODGRUPO = ?"

    ' Comprobar que la categor�a no existe dentro del arbol, a la hora de mover una categor�a
    rq(9).SQL = "Select FA05CODCATEG from FA0600 where FA05CODCATEG = ? " & _
                " AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') >= FA06FECINICIO AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN)"

    ' Copiar los atributos de la rama del concierto modelo al destino
    rq(10).SQL = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA, FA15PRECIOREF, FA15PRECIODIA, FA15FRANQSUMA, FA15FRANQUNI, FA15PERIODOGARA, FA15FECINICIO, FA15FECFIN, FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, FA15INDDESCONT, FA15INDLINOBLIG, FA15INDFACTOBLIG, FA15INDOPCIONAL, FA15DESCUENTO, CI13CODENTIDAD) " & _
                 "(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, REPLACE(FA15RUTA, ?, ?), FA15PRECIOREF, FA15PRECIODIA, FA15FRANQSUMA, FA15FRANQUNI, FA15PERIODOGARA, FA15FECINICIO, FA15FECFIN, FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, FA15INDDESCONT, FA15INDLINOBLIG, FA15INDFACTOBLIG, FA15INDOPCIONAL, FA15DESCUENTO, CI13CODENTIDAD " & _
                 "from dual, fa1500 where fa15ruta like ? || '%')"

    ' Copiar los atributos de la rama de categorias al destino
    rq(11).SQL = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA, FA15PRECIOREF, FA15FECINICIO, FA15FECFIN) " & _
                 "(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, REPLACE(FA15RUTA, ?, ?), FA15PRECIOREF, FA15FECINICIO, FA15FECFIN from dual, fa1500 where fa15ruta like ? || '%')"

    ' Seleccionar los atributos de una categor�a
    rq(12).SQL = "Select * From FA1500 where FA15RUTA = ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') >= FA15FECINICIO AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN)"

    ' Graba los atributos de una categor�a
    rq(13).SQL = "INSERT INTO FA1500 (FA15CODATRIB,FA15PRECIOREF,FA15PRECIODIA,FA15DESCUENTO," & _
                 "FA15INDFRANQUNI,FA15INDFRANQSUMA,FA15PERIODOGARA,FA15INDNECESARIO," & _
                 "FA15INDEXCLUIDO,FA15INDSUFICIENTE,FA15INDDESCONT,FA15INDLINOBLIG," & _
                 "FA15INDFACTOBLIG,FA15INDOPCIONAL,FA15FECINICIO,FA15FECFIN,FA15RUTA,CI13CODENTIDAD," & _
                 "FA15INDDESGLOSE, FA15DESCRIPCION, FA15RUTAREL, FA15RELFIJO, FA15RELPOR, FA15ORDIMP, FA15INDSUPLEMENTO)" & _
                 "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,?,?,?)"

    ' Comprueba si la categor�a ya est� dada de alta en FA0500
    rq(14).SQL = "Select FA05CODCATEG from FA0500 where FA05CODORIGEN = ? and FA08CODGRUPO = ?"
    'Borrar un hijo
'    rq(6).SQL = "Delete from " & tr & " where " & p & "=? and " & h & "=?"

    rq(15).SQL = "Update FA0600 set FA06FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') where FA05CODCATEG = ? " & _
                 "and  FA05CODCATEG_P = ? and FA06FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    rq(16).SQL = "Update FA1500 set FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') where FA15RUTA = ? and FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    rq(17).SQL = "Select FA08CODGRUPO, FA05CODORIGEN from FA0500 where FA05CODCATEG = ?"
    rq(18).SQL = "UPDATE FA9901J SET INDUTILIZADO = 0 WHERE CODORIGEN = ? AND FA08CODGRUPO = ?"

    ' Comprueba si la categor�a ya est� dada de alta en FA0500
    rq(19).SQL = "Select FA05CODCATEG from FA0500 where FA08CODGRUPO = ? AND FA05CODORIGEN IS NULL"

    rq(20).SQL = "Update FA0600 set FA06FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') where FA15RUTA = ?"
    rq(21).SQL = "Update FA1500 set FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') where FA15RUTA = ?"

    rq(22).SQL = "Update FA1500 set FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') where FA15RUTA = ? " & _
                        " AND FA15FECFIN > TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    rq(27).SQL = "Delete from FA1500 Where FA15FECINICIO >= FA15FECFIN"
    'busqueda para la insercion like %/%.padre.
    rq(23).SQL = "Select FA15RUTA from FA1500 where FA15RUTA like ?"
    'borrado like %/%.padre.hijo.
    rq(24).SQL = "Update FA1500 set FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') where FA15RUTA like ?"
        
    'Ayuda para la selecci�n de las categor�as no utilizadas (rq(1))
    rq(25).SQL = "Update FA9900 SET FA99VALOR = ? WHERE FA99CODPROP =1"


    'En el caso de que est� borrada seleccionamos la fecha de fin de una categor�a
    rq(26).SQL = "SELECT FA0600.FA06FECFIN FROM FA0600 " & _
        "WHERE FA0600.FA05CODCATEG_P = ? AND FA0600.FA05CODCATEG = ? " & _
        "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') >= FA06FECINICIO AND  TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN)"
      
    For i = 1 To 27
        Set rq(i).ActiveConnection = objApp.rdoConnect
        rq(i).Prepared = True
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim respuesta
  If Transaccion = True Then
    respuesta = MsgBox("Se ha realizado cambios, �desea guardarlos?", vbQuestion + vbYesNoCancel, "Aviso")
    If respuesta = vbYes Then
      objApp.CommitTrans
    ElseIf respuesta = vbNo Then
      objApp.RollbackTrans
    ElseIf respuesta = vbCancel Then
      Cancel = 1
    End If
  Else
    objApp.RollbackTrans
  End If
  objCW.blnAutoDisconnect = True
  objCW.SetClockEnable True
End Sub

Private Sub InsertarCategor�as_Click()
Dim MiSqLDeP As String
Dim MiSqLPrU As String
Dim rsDepart As rdoResultset
Dim rsPruebas As rdoResultset
Dim Texto As String
Dim Path As String
Dim CodPadre As String
Dim mIsQl As String
Dim MiRs As rdoResultset
Dim n As Node
        
    On Error GoTo ErrorenNuevo
    objApp.BeginTrans
      MiSqLDeP = "Select FA0500.FA05CODCATEG, FA05DESIG, FA08CODGRUPO, FA05CODORIGEN, FA0600.FA15RUTA from FA0500, FA0600 " & _
                        "Where FA05CODORIGEN is not NULL AND FA0500.FA05CODCATEG = FA0600.FA05CODCATEG " & _
                        "ORDER BY FA08CODGRUPO, FA05CODORIGEN"
      Set rsDepart = objApp.rdoConnect.OpenResultset(MiSqLDeP)
      If Not rsDepart.EOF Then
        While Not rsDepart.EOF
          MiSqLPrU = "Select GC01FPCLAV, GC01FCDESCR, GC01FCFVALI FROM GC0100 " & _
                          "WHERE GC01FCTREG = " & rsDepart("FA08CODGRUPO") & " AND GC01FPCLAV LIKE '" & rsDepart("FA05CODORIGEN") & "%'"
          Set rsPruebas = objApp.rdoConnect.OpenResultset(MiSqLPrU)
          If Not rsPruebas.EOF Then
            While Not rsPruebas.EOF
                rq(5).rdoParameters(0) = fNextClave("FA05CODCATEG", "FA0500")
                CatPadre = rq(5).rdoParameters(0)
                rq(5).rdoParameters(1) = rsPruebas("GC01FCDESCR")
                rq(5).rdoParameters(2) = rsDepart("FA08CODGRUPO")
                rq(5).rdoParameters(3) = rsPruebas("GC01FPCLAV")
                rq(5).rdoParameters(4) = Format("01/01/1980", "DD/MM/YYYY") & " " & MSKHora.Text
                rq(5).rdoParameters(5) = "31/12/3000 00:00:00"
                rq(5).rdoParameters(6) = rsDepart("FA05CODCATEG")
                rq(5).Execute
                rq(6).rdoParameters(1) = rq(5).rdoParameters(0)
                rq(6).rdoParameters(5) = rsDepart("FA15RUTA") & rq(5).rdoParameters(0) & "."
                rq(7).rdoParameters(1) = rsDepart("FA15RUTA") & rq(5).rdoParameters(0) & "."
                rq(6).rdoParameters(0) = fNextClave("FA06CODRAMACAT", "FA0600")
                rq(6).rdoParameters(2) = rsDepart("FA05CODCATEG")
                rq(6).rdoParameters(3) = Format("01/01/1980", "DD/MM/YYYY") & " " & MSKHora.Text
                rq(6).rdoParameters(4) = "31/12/3000 00:00:00"
                rq(6).Execute
                rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
                rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
                rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
                rq(7).Execute
                rsPruebas.MoveNext
            Wend
          End If
          rsDepart.MoveNext
        Wend
      End If
      'INSERTAR EL REGISTRO EN LA FA0500,FA0600 Y FA1500
    objApp.CommitTrans
  Exit Sub
  
  
ErrorenNuevo:
    If Err = 91 Then
    Else
      'Resume Next
      MsgBox error
      objApp.CommitTrans
      objApp.RollbackTrans
    End If
        
End Sub

Private Sub mnuAtributos_Click()
    Dim aAtributos(1 To 13) As CamposAtrib
    Dim i As Integer
    Dim Porcentaje As Single
    Dim strRedondeo As String
    Dim miSQLInsert As String
    Dim miSQLSelect As String
    Dim miSQLFrom As String
    Dim miSQLCompleta As String
    Dim Cadena As String
    Dim IntPosComa As Integer
    Dim Concierto As String
    
    'Llamar a la ventana de petici�n de atributos
    If VentanaAtributos(aAtributos, tvwRamas.SelectedItem.Text) = True Then
    
        ' Generar la SQL de creaci�n de nuevos atributos con los parametros no configurables
        miSQLInsert = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA,  FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15FECINICIO, FA15FECFIN"
        miSQLSelect = ")(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, FA15RUTA, FA15INDFRANQSUMA, FA15INDFRANQUNI, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS')"
        miSQLFrom = " FROM DUAL, FA1500 where fa15ruta like ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS')))"
    
    
        ' a�adir a la SQL los nombres de los campos y los valores posibles
        
        ' las modificaciones de precios dependen de si se modifica
        ' el precio referencia, el precio dia o la variaci�n es porcentual
        miSQLInsert = miSQLInsert & ", " & aAtributos(1).campo
        miSQLInsert = miSQLInsert & ", " & aAtributos(2).campo
            
        If aAtributos(FA15PRECIOREF).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).Valor
            miSQLSelect = miSQLSelect & ", NULL"
                
        ElseIf aAtributos(FA15PRECIODIA).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", NULL"
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).Valor
        
        ElseIf aAtributos(FA15Variaci�nPorcentual).Actualizar = True Then
            ' calculo del porcentaje a aplicar
            Porcentaje = (100 + aAtributos(FA15Variaci�nPorcentual).Valor) / 100
            If aAtributos(FA15Redondeo).Actualizar = False Then
                ' no se aplica redondeo
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje)
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje)
            Else
                strRedondeo = str(aAtributos(FA15Redondeo).Valor)
                miSQLSelect = miSQLSelect & ", MOD( " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje) & ", " & strRedondeo & " ) * " & strRedondeo
                miSQLSelect = miSQLSelect & ", MOD( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & ", " & strRedondeo & " ) * " & strRedondeo
'                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje)
            End If
                
        Else
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo
            
        End If
        
        
        For i = 3 To UBound(aAtributos) - 2
            miSQLInsert = miSQLInsert & ", " & aAtributos(i).campo
            
            If (aAtributos(i).Actualizar = True) And (i <> FA15DESCUENTO) Then
                If aAtributos(i).Valor = "" Then
                    miSQLSelect = miSQLSelect & ", Null"
                Else
                    If IsNumeric(aAtributos(i).Valor) Then
                        miSQLSelect = miSQLSelect & ", " & str(aAtributos(i).Valor)
                    Else
                        miSQLSelect = miSQLSelect & ", " & aAtributos(i).Valor
                    End If
                End If
            Else
                miSQLSelect = miSQLSelect & ", " & aAtributos(i).campo
            End If
        Next
        ' concatenar las tres partes de la SQL
        miSQLCompleta = miSQLInsert + miSQLSelect + miSQLFrom
        
        InitQRY (miSQLCompleta)
        Call Actualizar(tvwRamas.SelectedItem.Key, tvwRamas.SelectedItem.Text, Me.SDCProceso, aAtributos(FA15DESCUENTO))
        Cadena = frmSelConciertos.ActualizarConciertos(tvwRamas.SelectedItem.Tag, Me.SDCProceso & " " & Me.MSKHora.Text)
        If Cadena <> "" Then
          While Cadena <> ""
            IntPosComa = InStr(1, Cadena, ",")
            Concierto = Left(Cadena, IntPosComa - 1)
            If Concierto <> 0 Then
              Call ActualizarConciertos(tvwRamas.SelectedItem.Tag, tvwRamas.SelectedItem.Text, Me.SDCProceso, aAtributos(FA15DESCUENTO), Concierto)
            End If
            Cadena = Right(Cadena, Len(Cadena) - IntPosComa)
          Wend
        End If
    End If

End Sub

Private Sub mnuCambioNombre_Click()
  Me.tvwRamas.StartLabelEdit
End Sub

Private Sub mnudchoCopiar_Click()
  Dim n As Nodes
  Path = tvwRamas.SelectedItem.Key
  NodoHijo = tvwRamas.SelectedItem.Tag
  nodoPadre = tvwRamas.SelectedItem.Parent.Tag
  PathPadre = tvwRamas.SelectedItem.Parent.Key
  insPadre = nodoPadre
  insHijo = NodoHijo
  txtPadre = tvwRamas.SelectedItem.Parent.Text
  txtHijo = tvwRamas.SelectedItem.Text
  MsgBox "Copiar " & Path & " " & NodoHijo & " " & nodoPadre
End Sub
'Borraremos la categor�a seleccionada del �rbol de categor�as.
Private Sub mnuDchoEliminar_Click()
  On Error GoTo ErrorEliminar
  Path = tvwRamas.SelectedItem.Key
  If Path <> "/.0." Then
    NodoHijo = tvwRamas.SelectedItem.Tag
    nodoPadre = tvwRamas.SelectedItem.Parent.Tag
    Call BorrarRama(nodoPadre, NodoHijo, Path, tvwRamas.SelectedItem)
    Call Me.InitTVCateg
  Else
    MsgBox "No se puede borrar la categor�a cl�nica", vbInformation + vbOKOnly, "Atenci�n"
  End If
Exit Sub
ErrorEliminar:
  If Err = 91 Then
  Else
    MsgBox error
  End If
End Sub

Private Sub mnudchoNuevo_Click()
Dim Texto As String
Dim Path As String
Dim CodPadre As String
Dim mIsQl As String
Dim MiRs As rdoResultset
Dim n As Node

    On Error GoTo ErrorenNuevo
    Path = tvwRamas.SelectedItem.Key
    CodPadre = tvwRamas.SelectedItem.Tag
    Texto = InputBox("Nombre de la nueva categoria...", "Nueva categor�a")
    If Texto <> "" Then
      objApp.BeginTrans
        rq(5).rdoParameters(0) = fNextClave("FA05CODCATEG", "FA0500")
        CatPadre = rq(5).rdoParameters(0)
        rq(5).rdoParameters(1) = Texto
        rq(5).rdoParameters(2) = Null
        rq(5).rdoParameters(3) = Null
        rq(5).rdoParameters(4) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
        rq(5).rdoParameters(5) = "31/12/3000 00:00:00"
        rq(5).rdoParameters(6) = CodPadre
        rq(5).Execute
        rq(6).rdoParameters(1) = rq(5).rdoParameters(0)
        rq(6).rdoParameters(5) = Path & rq(5).rdoParameters(0) & "."
        rq(7).rdoParameters(1) = Path & rq(5).rdoParameters(0) & "."
        rq(6).rdoParameters(0) = fNextClave("FA06CODRAMACAT", "FA0600")
        rq(6).rdoParameters(2) = CodPadre
        rq(6).rdoParameters(3) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
        rq(6).rdoParameters(4) = "31/12/3000 00:00:00"
        rq(6).Execute
        rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
        rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
        rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
        rq(7).Execute
        'A�adimos la categor�a a todos los conciertos en los que la rama padre est� contenida.
        'MiSqL = "Select FA15RUTA from FA1500 where FA15RUTA like '%./%." & CodPadre & ".' and FA15FECFIN = TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS')"
        mIsQl = "Select FA15RUTA from FA1500 where FA15ESCAT=1 AND FA15CODFIN=" & CodPadre & " and FA15FECFIN = TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS')"
        Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, , , rdExecDirect)
        If Not MiRs.EOF Then
          While Not MiRs.EOF
            rq(7).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
            rq(7).rdoParameters(1) = MiRs(0) & CatPadre & "."
            rq(7).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
            rq(7).rdoParameters(3) = "31/12/3000 00:00:00"
            rq(7).Execute
            MiRs.MoveNext
          Wend
        End If
      objApp.CommitTrans
      Set n = tvwRamas.Nodes.Add(Path, tvwChild, Path & CatPadre & ".", Left(Texto, 99), "CatCerrado", "Editando")
      n.EnsureVisible
      n.Tag = CatPadre
    End If
  Exit Sub
ErrorenNuevo:
    If Err = 91 Then
    Else
      MsgBox error
      objApp.RollbackTrans
    End If
        
End Sub

Private Sub mnudchoPegar_Click()
  PathPadre = tvwRamas.SelectedItem.Key
  insPadre = tvwRamas.SelectedItem.Tag
  Call CambiarHija(insPadre, Path, PathPadre, insHijo, txtHijo)
End Sub


Private Sub SSTabAtrib_Click(PreviousTab As Integer)
    frmAtrib(PreviousTab).Visible = False
    frmAtrib(SSTabAtrib.Tab).Visible = True
End Sub

Private Sub tvwCategorias_Collapse(ByVal Node As ComctlLib.Node)
  If Node.Image = "CatAbierto" Then
      Node.Image = "CatCerrado"
  End If
End Sub
Private Sub tvwCategorias_Expand(ByVal Node As ComctlLib.Node)
Dim NODX As Node
Dim MiRsNodos As rdoResultset
Dim str As String
Dim mIsQl As String

  On Error Resume Next
  Err.Clear
  Set NODX = tvwCategorias.Nodes.Item(Node.Key & "dummy")
  str = NODX.Key
  On Error GoTo 0
  If str <> "" Then
    str = Right(NODX.Parent.Key, Len(NODX.Parent.Key) - 1)
    tvwCategorias.Nodes.Remove NODX.Key
    rq(2).rdoParameters(0) = str
    Set MiRsNodos = rq(2).OpenResultset
    If Not MiRsNodos.EOF Then
      While Not MiRsNodos.EOF
        'Llenamos el array que contendr� las categorias y el nombre
        Set NODX = tvwCategorias.Nodes.Add(Node, tvwChild, "C" & MiRsNodos("CODORIGEN"), Left(MiRsNodos("DESORIGEN"), 99), "Hoja", "Editando")
        MiRsNodos.MoveNext
      Wend
      NODX.Parent.Image = "CatAbierto"
      MiRsNodos.Close
    End If
  Else
    If Node.Image = "CatCerrado" Then
      Node.Image = "CatAbierto"
    End If
  End If

End Sub
Private Sub tvwCategorias_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    On Error GoTo IsNothing

    cSrc = Right(tvwCategorias.HitTest(x, Y).Key, Len(tvwCategorias.HitTest(x, Y).Key) - 1)
    TipoCateg = Left(tvwCategorias.HitTest(x, Y).Key, 1)
    If TipoCateg = "C" Then
      Grupo = Right(tvwCategorias.HitTest(x, Y).Parent.Key, Len(tvwCategorias.HitTest(x, Y).Parent.Key) - 1)
    Else
      Grupo = cSrc
    End If
    Categ = tvwCategorias.HitTest(x, Y).Text
    Set tvwCategorias.DropHighlight = Nothing
    tvwCategorias.DragIcon = ImageList1.ListImages(11).Picture
    tvwCategorias.Drag
    Dragging = True

    'lblXY.Top = y
    'lblXY.Left = x

    'lbl.Top = tvwCategorias.Top + y
    'lbl.Left = tvwCategorias.Left + x
    'lbl.Tag = "C"   ' Categor�a
    Exit Sub
IsNothing:
    Dragging = False
    lbl.Drag 0


End Sub

Private Sub tvwRamas_AfterLabelEdit(Cancel As Integer, NewString As String)
Dim mIsQl As String

    On Error GoTo error
    
    ' confirmar que se quiere cambiar el nombre
    If MsgBox("� Est� seguro de cambiar el nombre ?" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
              "Nombre Actual : " & tvwRamas.SelectedItem.Text & Chr(10) & Chr(13) & _
              "Nombre Nuevo : " & NewString _
            , vbYesNo, "Cambiar nombre a un nodo") = vbYes Then
        
        ' Iniciar transaccion
        objApp.BeginTrans
            
        ' ejecutar la SQL de cambio de nombre
        mIsQl = "Update FA0500 Set FA05DESIG = '" & NewString & "' Where FA05CODCATEG = " & tvwRamas.SelectedItem.Tag
        objApp.rdoConnect.Execute mIsQl
        'rq(22).rdoParameters(0).Value = NewString
        'rq(22).rdoParameters(1).Value = tv.SelectedItem.Tag
        'rq(22).Execute
        
        ' Ejecutar los cambios
        objApp.CommitTrans
    
    Else
        Cancel = True
    End If
    
Exit Sub

error:
    MsgBox "No se ha podido cambiar el Nombre", vbCritical + vbOKOnly, "Error"
    Cancel = True
    
    ' Deshacer los cambios
    objApp.RollbackTrans
    
End Sub

Private Sub tvwRamas_DragDrop(Source As Control, x As Single, Y As Single)

    If Dragging Then
        On Error GoTo IsNothing
        cDst = tvwRamas.HitTest(x, Y).Tag
        Screen.MousePointer = vbArrowHourglass
        If TipoCateg = "G" Then
          Call AppendGrupo(cDst, tvwRamas.HitTest(x, Y).Key, cSrc, Categ) 'tvwCategorias.HitTest(lblXY.Left, lblXY.Top).Text)
        Else
          Call AppendHija(cDst, tvwRamas.HitTest(x, Y).Key, cSrc, Categ, Grupo) 'tvwCategorias.HitTest(lblXY.Left, lblXY.Top).Text)
        End If
        Screen.MousePointer = 0
IsNothing:
        Dragging = False
        lbl.Drag 0
    End If
    Screen.MousePointer = 0

End Sub
Private Sub tvwRamas_Expand(ByVal Node As ComctlLib.Node)
    Dim n As Node, RS As rdoResultset, str$
    Dim PosBarra As Integer
    Dim PosPunto As Integer
    Dim CatRaiz As Long

    On Error Resume Next
    If Node.Image = "NodoCerrado" Then
      Node.Image = "NodoAbierto"
    End If

    Err.Clear
    Set n = tvwRamas.Nodes.Item(Node.Key & "dummy")
    str = n.Key
    On Error GoTo 0
    If str <> "" Then   ' es la primera vez que se abre
      rq(4).rdoParameters(0).Value = n.Parent.Tag
      rq(4).rdoParameters(1).Value = Me.SDCProceso & " " & MSKHora.Text
      rq(4).rdoParameters(2).Value = Me.SDCProceso & " " & MSKHora.Text
      tvwRamas.Nodes.Remove n.Key
      Set RS = rq(4).OpenResultset
      While Not RS.EOF
        If RS("FA06FECFIN") <> "31/12/3000" Then
         If Expandir = False Then
            Set n = tvwRamas.Nodes.Add(Node, tvwChild, Node.Key & RS(0) & ".", Left(RS(1), 99), "Borrado", "EditandoBorrado")
         End If
        Else
          If Expandir = False Then
            Set n = tvwRamas.Nodes.Add(Node, tvwChild, Node.Key & RS(0) & ".", Left(RS(1), 99), "CatCerrado", "Editando")
          End If
        End If
        n.Tag = RS(0)
        If Expandir = False Then
          tvwRamas.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
        End If
        RS.MoveNext
      Wend
      RS.Close
    End If
End Sub
'Private Sub tvwCategorias_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'    If Dragging Then
'        lbl.Drag 1
'    End If
'End Sub
Private Sub tvwRamas_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  If Button = vbRightButton Then
    On Error GoTo Salir
    tvwRamas.SelectedItem = tvwRamas.HitTest(x, Y)
    'llamamos al men� contextual con el bot�n derecho.
    PopupMenu menu
    On Error GoTo 0
  End If
Salir:
End Sub
Private Sub tvwRamas_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
    'If Dragging Then
   '    lbl.Drag 1
    'End If

End Sub
Private Sub tvwRamas_NodeClick(ByVal Node As ComctlLib.Node)
Dim MiRs As rdoResultset
Dim respuesta As Integer
Dim mIsQl As String

  If Node.Children > 0 Then
    Call tvwRamas_Expand(Node)
  End If
  If Modificar = True Then
    respuesta = MsgBox("�Desea guardar los cambios producidos en los atributos?", vbQuestion + vbYesNo, "Aviso")
    If respuesta = vbYes Then
      Call cmdGrabar_Click
    End If
  End If

  rq(12).rdoParameters(0) = Node.Key
  rq(12).rdoParameters(1) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
  rq(12).rdoParameters(2) = Format(Me.SDCProceso.Date, "DD/MM/YYYY") & " " & MSKHora.Text
  Set MiRs = rq(12).OpenResultset
  'Me.frmDatos.Caption = Node.Text
  If Not MiRs.EOF Then
    Me.txtCodAtrib.Text = IIf(IsNull(MiRs("FA15CODATRIB")), "", MiRs("FA15CODATRIB"))
    Me.txtRUTA.Text = IIf(IsNull(MiRs("FA15RUTA")), "", MiRs("FA15RUTA"))
    Me.SDCInicio.Date = IIf(IsNull(MiRs("FA15FECINICIO")), "", MiRs("FA15FECINICIO"))
    Me.SDCFin.Date = IIf(IsNull(MiRs("FA15FECFIN")), "", MiRs("FA15FECFIN"))
    Me.txtPrecio.Text = IIf(IsNull(MiRs("FA15PRECIOREF")), "", MiRs("FA15PRECIOREF"))
    Me.chkIndFranqUnit.Value = IIf(IsNull(MiRs("FA15INDFRANQUNI")), 0, MiRs("FA15INDFRANQUNI"))
    Me.chkIndFranqSuma.Value = IIf(IsNull(MiRs("FA15INDFRANQSUMA")), 0, MiRs("FA15INDFRANQSUMA"))
    Me.txtPeriodoGara.Text = IIf(IsNull(MiRs("FA15PERIODOGARA")), "", MiRs("FA15PERIODOGARA"))
    Me.chkIndNecesario.Value = IIf(IsNull(MiRs("FA15INDNECESARIO")), 0, MiRs("FA15INDNECESARIO"))
    Me.chkIndSuficiente.Value = IIf(IsNull(MiRs("FA15INDSUFICIENTE")), 0, MiRs("FA15INDSUFICIENTE"))
    Me.chkIndExcluido.Value = IIf(IsNull(MiRs("FA15INDEXCLUIDO")), 0, MiRs("FA15INDEXCLUIDO"))
    Me.chkIndOpcional.Value = IIf(IsNull(MiRs("FA15INDOPCIONAL")), 0, MiRs("FA15INDOPCIONAL"))
    Me.chkIndDescont.Value = IIf(IsNull(MiRs("FA15INDDESCONT")), 0, MiRs("FA15INDDESCONT"))
    Me.chkIndLinObig.Value = IIf(IsNull(MiRs("FA15INDLINOBLIG")), 0, MiRs("FA15INDLINOBLIG"))
    Me.chkIndFacturacion.Value = IIf(IsNull(MiRs("FA15INDFACTOBLIG")), 0, MiRs("FA15INDFACTOBLIG"))
    Me.txtDescuento.Text = IIf(IsNull(MiRs("FA15DESCUENTO")), "", MiRs("FA15DESCUENTO"))
    Me.txtPrecioDia.Text = IIf(IsNull(MiRs("FA15PRECIODIA")), "", MiRs("FA15PRECIODIA"))
    Me.txtCodEntidad.Text = IIf(IsNull(MiRs("CI13CODENTIDAD")), "", MiRs("CI13CODENTIDAD"))
    Me.chkIndDesglose.Value = IIf(IsNull(MiRs("FA15INDDESGLOSE")), 0, MiRs("FA15INDDESGLOSE"))
    Me.txtDescripcion.Text = IIf(IsNull(MiRs("FA15DESCRIPCION")), "", MiRs("FA15DESCRIPCION"))
    Me.txtRutaRel.Text = IIf(IsNull(MiRs("FA15RUTAREL")), "", MiRs("FA15RUTAREL"))
    Me.txtPrecioFijo.Text = IIf(IsNull(MiRs("FA15RELFIJO")), "", MiRs("FA15RELFIJO"))
    Me.txtRelPor.Text = IIf(IsNull(MiRs("FA15RELPOR")), "", MiRs("FA15RELPOR"))
    Me.txtNoOrden.Text = IIf(IsNull(MiRs("FA15ORDIMP")), "", MiRs("FA15ORDIMP"))
    Me.chkIndSuplemento.Value = IIf(IsNull(MiRs("FA15INDSUPLEMENTO")), 0, MiRs("FA15INDSUPLEMENTO"))
    Call txtCodEntidad_LostFocus
    Modificar = False
    Me.cmdGrabar.Enabled = False
    Me.cmdRecargar.Enabled = False
  Else
    Me.txtCodAtrib.Text = ""
    Me.txtRUTA.Text = ""
    Me.SDCInicio.Text = ""
    Me.SDCFin.Text = ""
    Me.txtPrecio.Text = ""
    Me.txtPeriodoGara.Text = ""
    Me.chkIndNecesario.Value = 0
    Me.chkIndSuficiente.Value = 0
    Me.chkIndExcluido.Value = 0
    Me.chkIndOpcional.Value = 0
    Me.chkIndDescont.Value = 0
    Me.chkIndLinObig.Value = 0
    Me.chkIndFacturacion.Value = 0
    Me.txtDescuento.Text = ""
    Me.txtPrecioDia.Text = ""
    Me.txtCodEntidad.Text = ""
    Me.cboCodEntidad.Text = ""
    Me.chkIndDesglose.Value = 0
    Me.txtDescripcion.Text = ""
    Me.txtRutaRel.Text = ""
    Me.txtPrecioFijo.Text = ""
    Me.txtRelPor.Text = ""
    Me.txtNoOrden.Text = ""
    Me.chkIndSuplemento.Value = 0
    Modificar = False
    Me.cmdGrabar.Enabled = False
    Me.cmdRecargar.Enabled = False
  End If
  If Node.Children = 0 Then
    mIsQl = "SELECT FA08CODGRUPO, FA05CODORIGEN FROM FA0500 WHERE FA05CODCATEG = " & Node.Tag
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, 3)
    If Not MiRs.EOF Then
      Me.txtGrupo = MiRs("FA08CODGRUPO") & ""
      Call txtGrupo_LostFocus
      Me.txtCodOrigen = MiRs("FA05CODORIGEN") & ""
    End If
    Me.cboGrupo.Enabled = True
    Me.txtCodOrigen.Enabled = True
    Me.cmdGrabarGrupo.Enabled = True
  Else
    Me.cboGrupo.Enabled = False
    Me.txtCodOrigen.Enabled = False
    Me.cmdGrabarGrupo.Enabled = False
  End If
End Sub

Private Sub txtCodEntidad_GotFocus()
  txtCodEntidad.SelStart = 0
  txtCodEntidad.SelLength = Len(txtCodEntidad)

End Sub
Private Sub txtCodEntidad_LostFocus()
Dim MiRs As rdoResultset
Dim mIsQl As String

  On Error GoTo Errores
  If Trim(txtCodEntidad.Text) <> "" Then
    mIsQl = "SELECT CI13DESENTIDAD FROM CI1300 WHERE CI13CODENTIDAD = '" & txtCodEntidad.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      cboCodEntidad.Text = MiRs("CI13DESENTIDAD")
    Else
      MsgBox "NO EXISTE LA ENTIDAD INDICADA O EL CODIGO ES ERRONEO"
      cboCodEntidad.Text = ""
      txtCodEntidad.SetFocus
    End If
  Else
    txtCodEntidad.Text = ""
    cboCodEntidad.Text = ""
  End If

Exit Sub
Errores:
  MsgBox "Se ha producido un error", vbOKOnly + vbCritical, "C�digo de entidad"
  txtCodEntidad.SetFocus

End Sub

Private Sub txtCodOrigen_LostFocus()
Dim MiRs As rdoResultset
Dim mIsQl As String

  On Error GoTo Errores
  If Trim(txtCodEntidad.Text) <> "" And Trim(txtVista.Text) <> "" Then
    mIsQl = "SELECT DESIGNACION FROM " & txtVista.Text & "WHERE CODIGO = " & txtCodOrigen.Text
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      cboDesOrigen.Text = MiRs("DESIGNACION")
    Else
      MsgBox "NO EXISTE LA CATEGOR�A INDICADA O EL CODIGO ES ERRONEO"
      cboDesOrigen.Text = ""
      txtCodOrigen.SetFocus
    End If
  Else
    txtCodOrigen.Text = ""
    cboDesOrigen.Text = ""
  End If

Exit Sub
Errores:
  MsgBox "Se ha producido un error", vbOKOnly + vbCritical, "C�digo de origen"
  txtCodOrigen.SetFocus


End Sub


Private Sub txtGrupo_GotFocus()
  txtGrupo.SelStart = 0
  txtGrupo.SelLength = Len(txtGrupo)

End Sub
Private Sub txtGrupo_LostFocus()
Dim MiRs As rdoResultset
Dim mIsQl As String

  On Error GoTo Errores
  If Trim(txtGrupo.Text) <> "" Then
    mIsQl = "SELECT FA08DESIG FROM FA0800 WHERE FA08CODGRUPO = '" & txtGrupo.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(mIsQl, rdOpenStatic, rdConcurReadOnly)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      cboGrupo.Text = MiRs("FA08DESIG")
    Else
      MsgBox "NO EXISTE EL GRUPO INDICADO O EL CODIGO ES ERRONEO"
      'CBOORIGEN.Text = ""
      '.SetFocus
    End If
  Else
    txtGrupo.Text = ""
    cboGrupo.Text = ""
  End If

Exit Sub
Errores:
  MsgBox "Se ha producido un error", vbOKOnly + vbCritical, "C�digo de Grupo de Origen"

End Sub

Private Sub txtDescuento_Change()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub txtDescuento_GotFocus()
  txtDescuento.SelStart = 0
  txtDescuento.SelLength = Len(txtDescuento)
End Sub

Private Sub txtDescuento_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtDescuento)
End Sub

Private Sub txtPeriodoGara_Change()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub txtPeriodoGara_GotFocus()
  txtPeriodoGara.SelStart = 0
  txtPeriodoGara.SelLength = Len(txtPeriodoGara)
End Sub

Private Sub txtPeriodoGara_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub

Private Sub txtPrecio_Change()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub txtPrecio_GotFocus()
  txtPrecio.SelStart = 0
  txtPrecio.SelLength = Len(txtPrecio)
End Sub

Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtPrecio)
End Sub

Private Sub txtPrecioDia_Change()
  Modificar = True
  If Me.cmdGrabar.Enabled = False Then
    cmdGrabar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    cmdRecargar.Enabled = True
  End If
End Sub
Private Sub txtPrecioDia_GotFocus()
  txtPrecioDia.SelStart = 0
  txtPrecioDia.SelLength = Len(txtPrecioDia)
End Sub


Private Sub txtPrecioDia_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtPrecioDia)
End Sub


