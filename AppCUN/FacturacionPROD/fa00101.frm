VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Grupo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Grupos"
   ClientHeight    =   6855
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6855
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   0
      Left            =   90
      TabIndex        =   7
      Top             =   360
      Width           =   9420
      Begin TabDlg.SSTab tabTab1 
         Height          =   3060
         Index           =   0
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   9150
         _ExtentX        =   16140
         _ExtentY        =   5398
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "fa00101.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(4)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "SDCGrupo(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "SDCGrupo(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboGrupo(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtGrupo(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtGrupo(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtGrupo(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "fa00101.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtGrupo 
            BackColor       =   &H0000FFFF&
            DataField       =   "FA08CODGRUPO"
            Height          =   315
            Index           =   0
            Left            =   1740
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de Grupo"
            Top             =   360
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.TextBox txtGrupo 
            BackColor       =   &H00FFFF00&
            DataField       =   "FA08DESVISTA"
            Height          =   285
            Index           =   2
            Left            =   1755
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Grupo"
            Top             =   1140
            Width           =   6255
         End
         Begin VB.TextBox txtGrupo 
            BackColor       =   &H00FFFF00&
            DataField       =   "FA08DESIG"
            Height          =   285
            Index           =   1
            Left            =   1755
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del Grupo"
            Top             =   765
            Width           =   3375
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   0
            Left            =   -74880
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Grupos| Grupos"
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
         End
         Begin SSDataWidgets_B.SSDBCombo cboGrupo 
            DataField       =   "FA11CODESTGRUPO"
            Height          =   315
            Index           =   0
            Left            =   1755
            TabIndex        =   3
            Tag             =   "Estado Grupo|Estado Grupo"
            Top             =   1500
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo SDCGrupo 
            DataField       =   "FA08FECINICIO"
            Height          =   285
            Index           =   0
            Left            =   1755
            TabIndex        =   15
            Tag             =   "Fecha Inicio"
            Top             =   1890
            Width           =   1725
            _Version        =   65537
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483634
            Enabled         =   0   'False
            MinDate         =   "1900/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo SDCGrupo 
            DataField       =   "FA08FECFIN"
            Height          =   285
            Index           =   1
            Left            =   1755
            TabIndex        =   16
            Tag             =   "Fecha Fin"
            Top             =   2250
            Width           =   1725
            _Version        =   65537
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483634
            Enabled         =   0   'False
            MinDate         =   "1900/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Final Validez:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   90
            TabIndex        =   18
            Top             =   2250
            Width           =   1500
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Inicio Validez:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   315
            TabIndex        =   17
            Top             =   1890
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Estado:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   480
            TabIndex        =   13
            Top             =   1500
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   480
            TabIndex        =   12
            Top             =   1110
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   960
            TabIndex        =   11
            Top             =   360
            Visible         =   0   'False
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   480
            TabIndex        =   10
            Top             =   780
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   6570
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFecha 
      Height          =   285
      Left            =   1935
      TabIndex        =   19
      Tag             =   "Fecha Fin"
      Top             =   5175
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BevelType       =   0
      BevelWidth      =   0
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Frame fraCategorias 
      Caption         =   "Categorias"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   0
      Left            =   90
      TabIndex        =   14
      Top             =   4005
      Width           =   9420
      Begin VB.Timer Timer1 
         Interval        =   1
         Left            =   135
         Top             =   270
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2205
         Index           =   1
         Left            =   90
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "Categorias|Categorias"
         Top             =   270
         Width           =   9135
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16113
         _ExtentY        =   3889
         _StockProps     =   79
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Grupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Constantes del array de txt
Const intFA08CODGRUPO = 0
Const intFA08DESIG = 1
Const intFA08DESVISTA = 2

'Constantes del array de combo
Const intFA11CODESTGRUPO = 0

'Constante del array de grid
Const intMaestro = 0
Const intCategorias = 1

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMultiInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm

Dim FechaDia As Date
Dim FechaBajaAnt As Date
Dim Columna As Integer

Private Sub cboGrupo_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboGrupo_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboGrupo_CloseUp(Index As Integer)
Select Case Index
        Case intFA11CODESTGRUPO
            Call objWinInfo.CtrlDataChange
    End Select
End Sub

Private Sub cboGrupo_GotFocus(Index As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboGrupo_LostFocus(Index As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Form_Load()
  Dim SQL       As String
  Dim strKey    As String
  Dim MiRsFecha As rdoResultset
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Grupo"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(intMaestro)
    .strTable = "FA0800"
    Call .FormAddOrderField("FA08CODGRUPO", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "FA0800")
    'Call .FormAddFilterWhere(strKey, "FA08CODGRUPO", "C�digo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FA08DESIG", "Designaci�n", cwString)
    'Call .FormAddFilterWhere(strKey, "FA08DESVISTA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FA11CODESTGRUPO", "Estado", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FA08FECINICIO", "Fec. Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "FA08FECFIN", "Fec. Final", cwDate)
    
    Call .FormAddFilterOrder(strKey, "FA08CODGRUPO", "C�digo")
    Call .FormAddFilterOrder(strKey, "FA08DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "FA08DESVISTA", "Descripci�n")
    
    'Impreso definido debajo de App.path\rpt
    'Call .objPrinter.Add("FA0801", "Listado de Grupos")

  End With
   
  With objMultiInfo
    Set .objFormContainer = fraCategorias(0)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(intCategorias)
    .intCursorSize = 0
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "FA0500"

    Call .FormAddOrderField("FA0500.FA05CODCATEG", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("FA08CODGRUPO", txtGrupo(intFA08CODGRUPO))
  End With
  
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "Grupo", "FA08CODGRUPO", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "C�digo", "FA05CODCATEG", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Descripci�n", "FA05DESIG", cwString)
    Call .GridAddColumn(objMultiInfo, "Origen", "FA05CODORIGEN", cwString)
    Call .GridAddColumn(objMultiInfo, "Fec. Inicio", "FA05FECINICIO", cwDate)
    Call .GridAddColumn(objMultiInfo, "Fec. Fin", "FA05FECFIN", cwDate)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(cboGrupo(intFA11CODESTGRUPO)).blnInFind = True
    .CtrlGetInfo(txtGrupo(intFA08CODGRUPO)).blnValidate = False
    .CtrlGetInfo(txtGrupo(intFA08CODGRUPO)).blnInFind = True
    .CtrlGetInfo(txtGrupo(intFA08DESIG)).blnInFind = True
    .CtrlGetInfo(txtGrupo(intFA08DESVISTA)).blnInFind = True
  
    Call .FormChangeColor(objMultiInfo)
    
    SQL = "SELECT FA11CODESTGRUPO, FA11DESIG FROM FA1100"
    SQL = SQL & " ORDER BY FA11DESIG"
    .CtrlGetInfo(cboGrupo(intFA11CODESTGRUPO)).strSQL = SQL
    .CtrlGetInfo(cboGrupo(intFA11CODESTGRUPO)).blnForeign = True
              
    Call .WinRegister
    Call .WinStabilize
  End With
 
    grdDBGrid1(intMaestro).Columns(1).Width = 500
    grdDBGrid1(intMaestro).Columns(2).Width = 2500
    grdDBGrid1(intMaestro).Columns(3).Width = 2500
    grdDBGrid1(intMaestro).Columns(4).Width = 1500
    
    grdDBGrid1(intMaestro).Columns(1).Visible = False
    
    grdDBGrid1(intCategorias).Columns(3).Visible = False
    grdDBGrid1(intCategorias).Columns(4).Visible = False
    grdDBGrid1(intCategorias).Columns(5).Width = 4000
    grdDBGrid1(intCategorias).Columns(6).Width = 1000
    grdDBGrid1(intCategorias).Columns(7).Width = 1500
    grdDBGrid1(intCategorias).Columns(8).Width = 1500
    grdDBGrid1(intCategorias).Columns(7).Locked = True
    grdDBGrid1(intCategorias).Columns(8).Locked = True
        
    Screen.MousePointer = vbDefault 'Call objApp.SplashOff
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    FechaDia = MiRsFecha(0)
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Public Sub MostrarDataCombo(ByVal Grid As SSDBGrid, SDC As Control, ByVal Columna As Integer)
'Este procedimiento se utiliza para posicionar el datacombo correctamente sobre la celda activa

Dim EstadoGrid As Boolean

  EstadoGrid = Grid.RowChanged
  If Columna = 7 Or Columna = 8 Then
    DoEvents
    'Asignar las coordenadas al DateCombo y recortar para encajar
    On Error GoTo EscondeBloque
    SDC.Top = Int(Grid.Columns(Columna).Top) + fraCategorias(0).Top + Grid.Top + (Grid.Row * Grid.RowHeight) + 290
    SDC.Left = Int(Grid.Columns(Columna).Left) + fraCategorias(0).Left + Grid.Left
    SDC.Width = Int(Grid.Columns(Columna).Width)
    SDC.Height = Grid.RowHeight
    If IsDate(Grid.Columns(Columna).Text) Then
      If Grid.IsAddRow Then
        If Columna = 7 Then
          SDC.DefaultDate = Format(FechaDia, "dd/mm/yyyy")
        ElseIf Columna = 8 Then
          SDC.DefaultDate = "31/12/3000"
        End If
      Else
        If Columna = 7 Then
          SDC.DefaultDate = Format$(Grid.Columns(7).Text, "dd/mm/yyyy")
          If Format(Grid.Columns(7).Text, "dd/mm/yyyy") < Format(FechaDia, "dd/mm/yyyy") Then
            SDC.Enabled = False
          Else
            SDC.MinDate = Format(FechaDia, "dd/mm/yyyy")
            SDC.Enabled = True
          End If
        ElseIf Columna = 8 Then
          If Format(Grid.Columns(Columna).Text, "dd/mm/yyyy") <> "31/12/3000" Then
            SDC.Enabled = False
            SDC.MinDate = "1/1/1900"
          Else
            SDC.MinDate = Format(Grid.Columns(7).Text, "dd/mm/yyyy")
            SDC.Enabled = True
          End If
          SDC.DefaultDate = Format$(Grid.Columns(8).Text, "dd/mm/yyyy")
        End If
      End If
    Else
      SDC.DefaultDate = ""
    End If
    SDC.Refresh
    If Grid.LeftCol <= 2 Then
      If (SDC.Left + SDC.Width) < (Grid.Left + Grid.Width) Then
        If Grid.Row < Grid.VisibleRows Then
          SDC.ZOrder 0
          SDC.Visible = True
          Exit Sub
        End If
      End If
    End If
  End If
  SDC.Visible = False
  SDC.ZOrder 1
  Exit Sub
  
EscondeBloque:
  SDC.Visible = False
End Sub



Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub grdDBGrid1_BeforeDelete(Index As Integer, Cancel As Integer, DispPromptMsg As Integer)
  
  grdDBGrid1(1).Columns(8).Text = Format(FechaDia, "DD/MM/YYYY")
  Cancel = 1
  
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
  If Index = 1 Then
  SDCFecha.MinDate = "1/1/1900"
    If grdDBGrid1(1).Col = 7 Then
      If grdDBGrid1(1).Columns(7).Text <> "" Then
        SDCFecha.Date = grdDBGrid1(1).Columns(7).Text
        Columna = 7
      End If
    ElseIf grdDBGrid1(1).Col = 8 Then
      If grdDBGrid1(1).Columns(8).Text <> "" Then
        SDCFecha.Date = grdDBGrid1(1).Columns(8).Text
        Columna = 8
      End If
    End If
  End If
End Sub


Private Sub grdDBGrid1_ColResize(Index As Integer, ByVal ColIndex As Integer, Cancel As Integer)
  If Index = 1 Then
    Timer1.Enabled = True
  End If
End Sub


Private Sub grdDBGrid1_RowResize(Index As Integer, Cancel As Integer)
  If Index = 1 Then
    Timer1.Enabled = True
  End If
End Sub

Private Sub grdDBGrid1_Scroll(Index As Integer, Cancel As Integer)
Static gridrow As Integer
Static leftgridcol%
  If Index = 1 Then
    leftgridcol = grdDBGrid1(1).LeftCol
    If grdDBGrid1(1).Row = gridrow Then
      SDCFecha.Visible = False
    End If
    gridrow = grdDBGrid1(1).Row
  End If
End Sub

Private Sub grdDBGrid1_ScrollAfter(Index As Integer)
  MostrarDataCombo grdDBGrid1(1), SDCFecha, Columna
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    If strFormName = objMultiInfo.strName Then
        'secuencia de Categoria
        If grdDBGrid1(intCategorias).Columns(4).Text = "" Then
            grdDBGrid1(intCategorias).Columns(4).Text = fNextClave("FA05CODCATEG", "FA0500")
            objMultiInfo.rdoCursor("FA05CODCATEG") = grdDBGrid1(intCategorias).Columns(4).Text
            Call objPipe.PipeSet("FA05CODCATEG", grdDBGrid1(intCategorias).Columns(4).Text)
            Call objPipe.PipeSet("FA05DESIG", grdDBGrid1(intCategorias).Columns(5).Text)
        End If
     End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
    If strFormName = objDetailInfo.strName Then
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            'secuencia de Grupo
            txtGrupo(intFA08CODGRUPO).Text = fNextClave("FA08CODGRUPO", "FA0800")
            objDetailInfo.rdoCursor("FA08CODGRUPO") = txtGrupo(intFA08CODGRUPO).Text
        End If
    End If
       
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objDetailInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  
  End Select

End Sub


Private Sub SDCFecha_Change()
  If Columna = 7 Then
    grdDBGrid1(1).Columns(7).Text = SDCFecha.Date
  ElseIf Columna = 8 Then
    grdDBGrid1(1).Columns(8).Text = SDCFecha.Date
  End If
  SDCFecha.MinDate = "1/1/1900"
End Sub

Private Sub SDCFecha_Click()
  If Columna = 7 Then
    grdDBGrid1(1).Columns(7).Text = SDCFecha.Date
  ElseIf Columna = 8 Then
    grdDBGrid1(1).Columns(8).Text = SDCFecha.Date
  End If
SDCFecha.MinDate = "1/1/1900"
End Sub




Private Sub SDCGrupo_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
  tlbToolbar1.Buttons(2).Enabled = True
End Sub

Private Sub SDCGrupo_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
  SDCGrupo(Index).MinDate = "01/01/1900"
End Sub


Private Sub SDCGrupo_DropDown(Index As Integer)
  SDCGrupo(Index).MinDate = FechaDia
End Sub


Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub Timer1_Timer()
  MostrarDataCombo grdDBGrid1(1), SDCFecha, Columna
  Timer1.Enabled = False
End Sub

Private Sub tlbToolbar1_ButtonClick(ByVal btnButton As Button)
Dim Resp As Integer
  If Me.ActiveControl.Name = "fraFrame1" Or Me.ActiveControl.Name = "txtGrupo" Or Me.ActiveControl.Name = "SDCGrupo" Or Me.ActiveControl.Name = "cboGrupo" Then
    SDCGrupo(0).MinDate = "01/01/1900"
    If btnButton.Index = 8 Then
      If SDCGrupo(1).Date = "31/12/3000" Then
        Resp = MsgBox("Para dar de baja un grupo deberemos modificar su fecha de fin de validez, �desea que el programa lo haga por vd.?", vbYesNo + vbInformation, "Aviso")
        If Resp = vbYes Then
          SDCGrupo(1).Date = FechaDia
        End If
      Else
        MsgBox "Grupo ya dado de baja", vbOKOnly + vbInformation, "Aviso"
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If btnButton.Index = 2 Then
        SDCGrupo(0).Date = FechaDia
        SDCGrupo(1).Date = "31/12/3000"
        SDCGrupo(1).Enabled = False
      Else
        SDCGrupo(1).Enabled = True
      End If
  
      If Trim(SDCGrupo(0).Date) <> "" Then
        If SDCGrupo(0).Date < Format(FechaDia, "DD/MM/YYYY") Then
          SDCGrupo(0).Enabled = False
        Else
          SDCGrupo(0).Enabled = True
        End If
      Else
        SDCGrupo(0).Enabled = True
      End If
    End If
  Else
    If btnButton.Index = 2 Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Me.grdDBGrid1(1).Columns(7).Text = Format(FechaDia, "DD-MM-YYYY")
      Me.grdDBGrid1(1).Columns(8).Text = "31-12-3000"
    ElseIf btnButton.Index = 8 Then
      If Format(grdDBGrid1(1).Columns(7).Text, "DD/MM/YYYY") > FechaDia Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Else
        Me.grdDBGrid1(1).Columns(8).Text = Format(FechaDia, "DD-MM-YYYY")
        grdDBGrid1_Change (1)
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    Columna = grdDBGrid1(1).Col
    SDCFecha.MinDate = "1/1/1900"
    MostrarDataCombo grdDBGrid1(1), SDCFecha, Columna
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtGrupo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtGrupo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtGrupo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



