   FUNCTION fafn08 (c_fact NUMBER, n_lin NUMBER)
   /*Funci�n que devuelve la fecha de una linea de factura */
      RETURN DATE IS
      f_fact   DATE;
   BEGIN
      SELECT MAX (fa03fecha)
        INTO f_fact
        FROM fa0300
       WHERE fa04numfact = c_fact
         AND fa16numlinea = n_lin;
      RETURN (f_fact);
   END fafn08;
