   FUNCTION fnfa02 (s VARCHAR2)
      RETURN NUMBER IS
      i   INTEGER;
      b   INTEGER;
      e   INTEGER;
      f   INTEGER;
      n   NUMBER;
   BEGIN
      b                          := 0;
      e                          := 0;
      n                          := 0;

      IF s IS NOT NULL THEN
         FOR i IN 1 .. LENGTH (s) LOOP
            IF     b = 0
               AND ASCII (SUBSTR (s, i, 1)) > 48
               AND ASCII (SUBSTR (s, i, 1)) < 58 THEN
               b                          := i;
            END IF;
         END LOOP;

         f                          := 1;

         IF b > 0 THEN
            FOR i IN b .. LENGTH (s) LOOP
               IF     f = 1
                  AND ASCII (SUBSTR (s, i, 1)) >= 48
                  AND ASCII (SUBSTR (s, i, 1)) < 58 THEN
                  e                          := i;
               ELSE
                  f                          := 0;
               END IF;
            END LOOP;
         END IF;

         IF     b > 0
            AND b <= e THEN
            n                          := TO_NUMBER (SUBSTR (s, b, e - b + 1));
         ELSE
            n                          := 0;
         END IF;
      END IF;

      RETURN n;
   END;
