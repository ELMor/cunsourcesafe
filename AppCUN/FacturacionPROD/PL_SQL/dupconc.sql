   PROCEDURE dupconc (corig NUMBER, cdest NUMBER, nombre VARCHAR2) IS
      CURSOR c14 (c NUMBER) IS
         SELECT *
           FROM fa1400
          WHERE fa09codnodoconc_p = c;

      CURSOR c15 (c NUMBER) IS
         SELECT *
           FROM fa1500
          WHERE fa15codconc = c;

      CURSOR c07 (a NUMBER) IS
         SELECT *
           FROM fa0700
          WHERE fa15codatrib = a;

      ruta   VARCHAR2 (2000);
      nca    NUMBER;
   BEGIN
      INSERT INTO fa0900
                  (
                     fa09codnodoconc,
                     fa09desig,
                     fa09indconcierto,
                     fa09indfacturable
                  )
           VALUES (cdest, nombre, -1, 0);

      COMMIT;

      FOR r14 IN c14 (corig) LOOP
         SELECT fa14codnodoconc_sequence.nextval
           INTO r14.fa14codnodoconc
           FROM dual;
         r14.fa09codnodoconc_p      := cdest;

         INSERT INTO fa1400
                     (
                        fa14codnodoconc,
                        fa09codnodoconc_p,
                        fa09codnodoconc_h,
                        fa05codcateg_h,
                        fa14fecinicio,
                        fa14fecfin
                     )
              VALUES (
                 r14.fa14codnodoconc,
                 r14.fa09codnodoconc_p,
                 r14.fa09codnodoconc_h,
                 r14.fa05codcateg_h,
                 r14.fa14fecinicio,
                 r14.fa14fecfin
              );
      END LOOP;

      COMMIT;

      FOR r15 IN c15 (corig) LOOP
         SELECT fa15codatrib_sequence.nextval
           INTO nca
           FROM dual;
         ruta                       :=
                '.' || cdest || SUBSTR (r15.fa15ruta, 2 + LENGTH (TO_CHAR (corig)));

         INSERT INTO fa1500
                     (
                        fa15codatrib,
                        fa15ruta,
                        fa15fecinicio,
                        fa15fecfin,
                        fa15precioref,
                        fa15franquni,
                        fa15franqsuma,
                        fa15periodogara,
                        fa15indnecesario,
                        fa15indsuficiente,
                        fa15indexcluido,
                        fa15indopcional,
                        fa15inddescont,
                        fa15indlinoblig,
                        fa15indfactoblig,
                        fa15descuento,
                        fa15preciodia,
                        ci13codentidad,
                        fa15indfranquni,
                        fa15indfranqsuma,
                        fa15codconc,
                        fa15codfin,
                        fa15escat,
                        fa15inddesglose,
                        fa15indsuplemento,
                        fa15descripcion,
                        fa15rutarel,
                        fa15relfijo,
                        fa15relpor,
                        fa15ordimp,
                        fa15indtramacum,
                        fa15indtramdias,
                        fa15plazointer,
                        fa15indnecrnforigen,
                        fa15indnecrnfdestino,
                        fa15indrecogercant,
                        fa15indrecogervalor,
                        ci32codtipecon,
                        fa15indcontforf,
                        fa46coddesexterno
                     )
              VALUES (
                 nca,
                 ruta,
                 r15.fa15fecinicio,
                 r15.fa15fecfin,
                 r15.fa15precioref,
                 r15.fa15franquni,
                 r15.fa15franqsuma,
                 r15.fa15periodogara,
                 r15.fa15indnecesario,
                 r15.fa15indsuficiente,
                 r15.fa15indexcluido,
                 r15.fa15indopcional,
                 r15.fa15inddescont,
                 r15.fa15indlinoblig,
                 r15.fa15indfactoblig,
                 r15.fa15descuento,
                 r15.fa15preciodia,
                 r15.ci13codentidad,
                 r15.fa15indfranquni,
                 r15.fa15indfranqsuma,
                 r15.fa15codconc,
                 r15.fa15codfin,
                 r15.fa15escat,
                 r15.fa15inddesglose,
                 r15.fa15indsuplemento,
                 r15.fa15descripcion,
                 r15.fa15rutarel,
                 r15.fa15relfijo,
                 r15.fa15relpor,
                 r15.fa15ordimp,
                 r15.fa15indtramacum,
                 r15.fa15indtramdias,
                 r15.fa15plazointer,
                 r15.fa15indnecrnforigen,
                 r15.fa15indnecrnfdestino,
                 r15.fa15indrecogercant,
                 r15.fa15indrecogervalor,
                 r15.ci32codtipecon,
                 r15.fa15indcontforf,
                 r15.fa46coddesexterno
              );

         FOR r07 IN c07 (r15.fa15codatrib) LOOP
            r07.fa15codatrib           := nca;

            INSERT INTO fa0700
                        (
                           fa15codatrib,
                           fa07coddescuent,
                           fa07cantini,
                           fa07acantfin,
                           fa07precio,
                           fa07desig
                        )
                 VALUES (
                    r07.fa15codatrib,
                    r07.fa07coddescuent,
                    r07.fa07cantini,
                    r07.fa07acantfin,
                    r07.fa07precio,
                    r07.fa07desig
                 );
         END LOOP;

         COMMIT;
      END LOOP;
   END;
