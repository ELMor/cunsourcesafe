   FUNCTION fafn12 (c_asist NUMBER, c_proc NUMBER)
   /*Funci�n que devuelve el tipo de asistencia ambulatoria para Osasunbidea */
      RETURN NUMBER IS
      t_asist   NUMBER;
   BEGIN
      SELECT MIN (fa4000.fa40codigo)
        INTO t_asist
        FROM fa0400,
             fa1600,
             fa4100,
             fa4000
       WHERE fa0400.fa04codfact = fa1600.fa04codfact
         AND fa1600.fa16descrip = fa4100.fa41descripcion
         AND fa4100.fa40codreg = fa4000.fa40codreg
         AND fa4000.fa43concepto = 'CO'
         AND fa0400.ad01codasistenci = c_asist
         AND fa0400.ad07codproceso = c_proc;
      RETURN (TO_CHAR (TO_NUMBER (t_asist)));
   END fafn12;
