



   FUNCTION fafn02 (numhistoria IN NUMBER, numdpto IN NUMBER, datefecha IN DATE)
      RETURN NUMBER IS
      numtipo   NUMBER;
   BEGIN
   /* FUNCION QUE MIRA SI HAY GARANTIAS DE OSASUNBIDEA */
   /* Primero se mira si tiene alguna consulta en el plazo de un mes */
      SELECT/*+ rule */
          DECODE (COUNT (*), 0, 0, 1)
        INTO numtipo
        FROM fa0400,
             fa0500,
             fa0300,
             ad0700
       WHERE ad0700.ad07codproceso = fa0300.ad07codproceso
         AND fa0500.fa05codcateg = fa0300.fa05codcateg
         AND fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0400.fa04cantfact > 0
         AND MONTHS_BETWEEN (datefecha, fa0300.fa03fecha) BETWEEN 0 AND 1
         AND datefecha > fa0300.fa03fecha
         AND ad0700.ci22numhistoria = numhistoria
         AND fa0300.ad02coddpto_r = numdpto
         AND fa0500.fa08codgrupo = 7
         AND fa0500.fa05codorigc2 = 0;

      /* raise_application_error( -20502, 'numTipo: '||numTIPO); */
      IF numtipo = 0 THEN
         /* Se mira si tiene alg�n ingreso en el plazo de un mes */
         SELECT/*+ rule */ DECODE (COUNT (*), 0, 0, 2)
           INTO numtipo
           FROM fa0500,
                fa0300,
                ad0700
          WHERE ad0700.ad07codproceso = fa0300.ad07codproceso
            AND fa0500.fa05codcateg = fa0300.fa05codcateg
            AND MONTHS_BETWEEN (datefecha, fa0300.fa03fecha) BETWEEN 0 AND 1
            AND datefecha > fa0300.fa03fecha
            AND ad0700.ci22numhistoria = numhistoria
            AND fa0300.ad02coddpto_r = numdpto
            AND fa0500.fa08codgrupo = 5;
      END IF;

      IF numtipo = 0 THEN
         /* Entonces se mira si tiene un forfait en el plazo de 3 meses */
         /* Se mira en IBM s�lo si la fecha es anterior al 1/4/ 2000 */
         /* ya que en caso contrario no estar�a en IBM en un plazo inferior a 3 meses */
         IF datefecha < TO_DATE ('1/4/2000', 'DD/MM/YYYY') THEN
            SELECT DECODE (COUNT (*), 0, 0, 3)
              INTO numtipo
              FROM rsna
             WHERE rshist = numhistoria
               AND SUBSTR (rsserv, 1, 3) = numdpto
               AND MONTHS_BETWEEN (datefecha, TO_DATE (rsffin, 'YYYYMMDD')) BETWEEN 0
                       AND 3
               AND rstipo = 5;
         END IF;
      END IF;

      IF numtipo = 0 THEN
         /* Entonces se mira si tiene un forfait en el plazo de 3 meses en las facturas de SNA */
         SELECT DECODE (COUNT (*), 0, 0, 3)
           INTO numtipo
           FROM ad0700,
                fa0400,
                fa5200,
                fa5300
          WHERE fa0400.fa04codfact = fa5300.fa04codfact
            AND ad0700.ad07codproceso = fa0400.ad07codproceso
            AND fa5200.fa52fecproceso = fa5300.fa52fecproceso
            AND fa5200.fa52numreg = fa5300.fa52numreg
            AND ad0700.ci22numhistoria = numhistoria
            AND fa53concepto = 'FQ'
            AND MONTHS_BETWEEN (datefecha, TO_DATE (fa5200.fa52fecfin, 'YYYYMMDD')) BETWEEN 0
                    AND 1
            AND fa5200.fa52coddpto = numdpto;
      END IF;

      IF numtipo = 0 THEN
         /* Entonces se mira si tiene un transplante en el plazo de 1 a�o */
         IF datefecha < TO_DATE ('1/1/2001', 'DD/MM/YYYY') THEN
            SELECT DECODE (COUNT (*), 0, 0, 4)
              INTO numtipo
              FROM rsna
             WHERE rshist = numhistoria
               AND SUBSTR (rsserv, 1, 4) = numdpto
               AND MONTHS_BETWEEN (datefecha, TO_DATE (rsffin, 'YYYYMMDD')) BETWEEN 0
                       AND 12
               AND rstipo = 22;
         END IF;
      END IF;

      IF numtipo = 0 THEN
         /* Entonces se mira si tiene un transplante en el plazo de 1 a�o en las facturas de SNA */
         SELECT DECODE (COUNT (*), 0, 0, 4)
           INTO numtipo
           FROM ad0700,
                fa0400,
                fa5200,
                fa5300
          WHERE fa0400.fa04codfact = fa5300.fa04codfact
            AND ad0700.ad07codproceso = fa0400.ad07codproceso
            AND fa5200.fa52fecproceso = fa5300.fa52fecproceso
            AND fa5200.fa52numreg = fa5300.fa52numreg
            AND ad0700.ci22numhistoria = numhistoria
            AND fa53concepto = 'TR'
            AND MONTHS_BETWEEN (datefecha, TO_DATE (fa5200.fa52fecfin, 'YYYYMMDD')) BETWEEN 0
                    AND 1
            AND fa5200.fa52coddpto = numdpto;
      END IF;

      RETURN (numtipo);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN (numtipo);
   END fafn02;
