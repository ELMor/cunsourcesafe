   FUNCTION fafn13 (intini DATE, intfin DATE, ini DATE, fin DATE)
      RETURN NUMBER IS
   /*Esta funcion calcula el tama�o en d�as de la interseccion de dos intervalos de fechas. Los parametros
     son Inicio de Intervalo1, Final de Intervalo1,Inicio de Intervalo2 y Final de Intervalo2*/
   BEGIN
      IF    ini IS NULL
         OR fin IS NULL THEN
         RETURN 0;
      END IF;

      /* --.---.--+------+------- */
      IF intini > fin THEN
         RETURN 0;
      END IF;

   /* ---------+------+-.--.-- */
      IF intfin < ini THEN
         RETURN 0;
      END IF;

      /* ---.-----+------+---.--- */
      IF     intini >= ini
         AND intfin <= fin THEN
         RETURN intfin - intini;
      END IF;

      /* ---.-----+---.--+------- */
      IF     intini >= ini
         AND intfin >= fin THEN
         RETURN fin - intini;
      END IF;

   /* ---------+-.-.--+------- */
      IF     intini <= ini
         AND intfin >= fin THEN
         RETURN fin - ini;
      END IF;

      /* ---------+---.--+--.---- */
      IF     intini <= ini
         AND intfin <= fin THEN
         RETURN intfin - ini;
      END IF;
   END;
