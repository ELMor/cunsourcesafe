   PROCEDURE pruequcat (act NUMBER, cat NUMBER, pr NUMBER DEFAULT NULL) IS
      /* Da de alta en Categorķas y Conciertos la pr01codactuacion siempre y
         cuando sea equivalente a la categorķa CAT que debe esta definida
         Lo unico que se permite modificar es el precio con un tanto por uno
         sobre el de referencia; es decir, el precio de la nueva categoria es el
         de la de referencia multiplicado por PR*/
      des   VARCHAR2 (2000);
      r05   fa0500%ROWTYPE;
      r06   fa0600%ROWTYPE;
      r15   fa1500%ROWTYPE;
      nwc   NUMBER;
      nwr   NUMBER;
      nwa   NUMBER;
      exi   NUMBER;

      CURSOR c1 IS
         SELECT fa09codnodoconc
           FROM fa0900
          WHERE fa09indconcierto = -1;

      CURSOR c2 (cc NUMBER, ca NUMBER) IS
         SELECT fa15codatrib
           FROM fa1500
          WHERE fa15codconc = cc
            AND fa15escat = 1
            AND fa15codfin = ca;
   BEGIN
      SELECT COUNT (*)
        INTO exi
        FROM fa0500
       WHERE fa08codgrupo = 7
         AND fa05codorigen = act;

      IF exi > 0 THEN
         /*Ya esta en la fa0500*/
         raise_application_error (-20101, 'Prueba_ya_existe_en_FA0500');
      END IF;

      /* Coger descripcion de la pr0100 */
      SELECT pr01descorta
        INTO des
        FROM pr0100
       WHERE pr12codactividad = 203
         AND pr01codactuacion = act;

      IF des IS NULL THEN
         /*no existe en la pr0100*/
         raise_application_error (-20101, 'Act_no_Existe_en_PR01_prueba');
      END IF;

      /* Insertar en la fa0500 y fa0600 */
      SELECT fa05codcateg_sequence.nextval
        INTO nwc
        FROM dual;
      SELECT *
        INTO r05
        FROM fa0500
       WHERE fa05codcateg = cat;
      r05.fa05codcateg           := nwc;
      r05.fa05desig              := des;
      r05.fa08codgrupo           := 7;
      r05.fa05codorigen          := act;

      INSERT INTO fa0500
                  (
                     fa05codcateg,
                     fa05desig,
                     fa08codgrupo,
                     fa05codorigen,
                     fa05fecinicio,
                     fa05fecfin,
                     fa05codcateg_p,
                     fa05codorigc1,
                     fa05codorigc2,
                     fa05codorigc3,
                     fa05codorigc4,
                     fa05precioref
                  )
           VALUES (
              r05.fa05codcateg,
              r05.fa05desig,
              r05.fa08codgrupo,
              r05.fa05codorigen,
              r05.fa05fecinicio,
              r05.fa05fecfin,
              r05.fa05codcateg_p,
              r05.fa05codorigc1,
              r05.fa05codorigc2,
              r05.fa05codorigc3,
              r05.fa05codorigc4,
              r05.fa05precioref
           );

      SELECT fa06codramacat_sequence.nextval
        INTO nwr
        FROM dual;
      SELECT *
        INTO r06
        FROM fa0600
       WHERE fa05codcateg = cat;
      r06.fa06codramacat         := nwr;
      r06.fa05codcateg           := nwc;

      INSERT INTO fa0600
                  (
                     fa06codramacat,
                     fa05codcateg,
                     fa05codcateg_p,
                     fa06fecinicio,
                     fa06fecfin,
                     fa15ruta
                  )
           VALUES (
              r06.fa06codramacat,
              r06.fa05codcateg,
              r06.fa05codcateg_p,
              r06.fa06fecinicio,
              r06.fa06fecfin,
              r06.fa15ruta
           );

      FOR rc1 IN c1 LOOP
         FOR rc2 IN c2 (rc1.fa09codnodoconc, cat) LOOP
            SELECT fa15codatrib_sequence.nextval
              INTO nwa
              FROM dual;
            SELECT *
              INTO r15
              FROM fa1500
             WHERE fa15codatrib = rc2.fa15codatrib;
            r15.fa15codatrib           := nwa;
            des                        :=
              SUBSTR (
                 r15.fa15ruta, 1, LENGTH (r15.fa15ruta) - 1 -
                                     LENGTH (TO_CHAR (r15.fa15codfin))
              );
            r15.fa15ruta               := des || nwc || '.';

            IF pr IS NOT NULL THEN
               r15.fa15precioref          := r15.fa15precioref * pr;
            END IF;

            INSERT INTO fa1500
                        (
                           fa15codatrib,
                           fa15ruta,
                           fa15fecinicio,
                           fa15fecfin,
                           fa15precioref,
                           fa15franquni,
                           fa15franqsuma,
                           fa15periodogara,
                           fa15indnecesario,
                           fa15indsuficiente,
                           fa15indexcluido,
                           fa15indopcional,
                           fa15inddescont,
                           fa15indlinoblig,
                           fa15indfactoblig,
                           fa15descuento,
                           fa15preciodia,
                           ci13codentidad,
                           fa15indfranquni,
                           fa15indfranqsuma,
                           fa15codconc,
                           fa15codfin,
                           fa15escat,
                           fa15inddesglose,
                           fa15indsuplemento,
                           fa15descripcion,
                           fa15rutarel,
                           fa15relfijo,
                           fa15relpor,
                           fa15ordimp,
                           fa15indtramacum,
                           fa15indtramdias,
                           fa15plazointer,
                           fa15indnecrnforigen,
                           fa15indnecrnfdestino,
                           fa15indrecogercant,
                           fa15indrecogervalor,
                           ci32codtipecon
                        )
                 VALUES (
                    r15.fa15codatrib,
                    r15.fa15ruta,
                    r15.fa15fecinicio,
                    r15.fa15fecfin,
                    r15.fa15precioref,
                    r15.fa15franquni,
                    r15.fa15franqsuma,
                    r15.fa15periodogara,
                    r15.fa15indnecesario,
                    r15.fa15indsuficiente,
                    r15.fa15indexcluido,
                    r15.fa15indopcional,
                    r15.fa15inddescont,
                    r15.fa15indlinoblig,
                    r15.fa15indfactoblig,
                    r15.fa15descuento,
                    r15.fa15preciodia,
                    r15.ci13codentidad,
                    r15.fa15indfranquni,
                    r15.fa15indfranqsuma,
                    r15.fa15codconc,
                    r15.fa15codfin,
                    r15.fa15escat,
                    r15.fa15inddesglose,
                    r15.fa15indsuplemento,
                    r15.fa15descripcion,
                    r15.fa15rutarel,
                    r15.fa15relfijo,
                    r15.fa15relpor,
                    r15.fa15ordimp,
                    r15.fa15indtramacum,
                    r15.fa15indtramdias,
                    r15.fa15plazointer,
                    r15.fa15indnecrnforigen,
                    r15.fa15indnecrnfdestino,
                    r15.fa15indrecogercant,
                    r15.fa15indrecogervalor,
                    r15.ci32codtipecon
                 );
         END LOOP;
      END LOOP;
   END;
