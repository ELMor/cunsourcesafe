CREATE OR REPLACE PACKAGE fapk02 IS
   TYPE trtarifa IS RECORD(
      fa15codfin                    fa1500.fa15codfin%TYPE,
      fa15codatrib                  fa1500.fa15codatrib%TYPE,
      fa15precioref                 fa1500.fa15precioref%TYPE,
      fa15preciodia                 fa1500.fa15preciodia%TYPE,
      fa15fecinicio                 fa1500.fa15fecinicio%TYPE);
   TYPE tttarifa IS TABLE OF trtarifa
      INDEX BY BINARY_INTEGER;
   FUNCTION FacturarIH (fechafact DATE)
      RETURN NUMBER;
   FUNCTION FacturarEntidadColaboradora( fechafact DATE )
      RETURN NUMBER;
   FUNCTION FacturarInvestigacion( fechafact DATE )
      RETURN NUMBER;
   RECO_Entidad		 			   constant number := 803095;
   RECO_Investigacion	  		   constant number := 803096;
END fapk02;
/

CREATE OR REPLACE PACKAGE BODY fapk02 AS
   FUNCTION BorrarRNFs (
      nproceso     ad0700.ad07codproceso%TYPE,
      nasistenci   ad0100.ad01codasistenci%TYPE
   )
      RETURN NUMBER IS
/******************************************************************************
  FUNCTION      : BorrarRNFs()
  SCOPE         : PRIVATE
  DESCRIPTION   : elimina de la tabla de RNFs todos los registros correspondie
  				  ntes al proc-asist pasado como parametro. Si por cualquier
				  motivo falla la operaci�n fallase, hace ROLLBACK y devuelve
FALSE
  PARAMETERS IN : nProceso, nAsistenci
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
******************************************************************************/
   BEGIN
      DELETE
        FROM fa0300
       WHERE fa0300.ad07codproceso = nproceso
         AND fa0300.ad01codasistenci = nasistenci
         AND fa03indpermanente IS NULL;
      COMMIT;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         RETURN 1;
   END borrarrnfs;
/*****************************************************************************/
   FUNCTION ValorarRNFs (
      tarifa       tttarifa,
      codfact      NUMBER
   )
      RETURN NUMBER IS
/******************************************************************************
  FUNCION QUE VALORA LOS RNFS DE UN PROCESO-ASISTENCIA
******************************************************************************/
      precxcant   NUMBER;
      suminterv   NUMBER;
	  err		  NUMBER;
	  errm		  varchar2(1000);
      CURSOR currnfs (
         codfact   NUMBER
      ) IS
         SELECT *
           FROM fa0300
          WHERE fa0300.fa04numfact = codfact
          ORDER BY fa0300.fa05codcateg;
   BEGIN
/* HAY QUE TENER EN CUENTA QUE EL PRECIO PUEDE SER :
       DETERMINADO POR EL QUE YA TENGA EL PROPIO RNF
       POR CANTIDAD
       POR DIA
       POR TRAMOS (p.e. Estancias, Quirofanos, etc.)
       CONDICIONADO A OTRO NODO EN UN PORCENTAJE (p.e. Honorarios de
		 Anestesia, etc.) */
      FOR rnf IN currnfs (codfact) LOOP
	     BEGIN
         IF rnf.fa03preciofact IS NOT NULL THEN
            -- YA tiene precio no hay que modificarlo
            NULL;
         ELSIF tarifa(rnf.fa05codcateg).fa15precioref IS NOT NULL THEN
            -- Tiene precio de referencia
            UPDATE fa0300
               SET fa03preciofact = tarifa (rnf.fa05codcateg).fa15precioref,
                   fa0300.fa15codatrib =
                      tarifa (rnf.fa05codcateg).fa15codatrib
             WHERE fa03numrnf = rnf.fa03numrnf;
         ELSIF tarifa (rnf.fa05codcateg).fa15preciodia IS NOT NULL THEN
            -- Tiene precio por dia
            UPDATE fa0300
               SET fa03preciofact = tarifa (rnf.fa05codcateg).fa15preciodia,
                   fa0300.fa15codatrib =
                      tarifa (rnf.fa05codcateg).fa15codatrib
             WHERE fa03numrnf = rnf.fa03numrnf;
         ELSIF tarifa (rnf.fa05codcateg).fa15preciodia IS NULL THEN
            -- Hay que buscar si tiene precios por cantidad
            SELECT SUM (fa07precio)
              INTO precxcant
              FROM fa0700 t1,
                   gc1300 t2
             WHERE t1.fa15codatrib = tarifa (rnf.fa05codcateg).fa15codatrib
               AND t2.n BETWEEN 1 AND rnf.fa03cantidad
               AND (t1.fa07acantfin - t2.n) IN (SELECT MIN (fa07acantfin - n)
                                                  FROM fa0700 t3,
                                                       gc1300 t4
                                                 WHERE t3.fa15codatrib =
                                                                 t1.fa15codatrib
                                                   AND t4.n = t2.n
                                                   AND t3.fa07acantfin - t4.n >=
                                                                               0);
            IF precxcant IS NOT NULL THEN
               UPDATE fa0300
                  SET fa03preciofact = precxcant / rnf.fa03cantfact,
                      fa0300.fa15codatrib =
                         tarifa (rnf.fa05codcateg).fa15codatrib
                WHERE fa03numrnf = rnf.fa03numrnf;
            ELSIF rnf.fa05codcateg = 35353 THEN           --Honorarios Anestesia
               -- Hay que buscar si tiene precios condicionado (Hon.Anestesia)
               -- Directamente hacemos 25% de las intervenciones que hubiese
               SELECT SUM (fa03cantfact * fa03preciofact)
                 INTO suminterv
                 FROM fa0300,
                      fa0500
                WHERE fa0300.fa05codcateg = fa0500.fa05codcateg
                  AND fa08codgrupo = 6                          --Intervenciones
                  AND fa0300.ad01codasistenci = rnf.ad01codasistenci
                  AND fa0300.ad07codproceso = rnf.ad07codproceso;
               UPDATE fa0300
                  SET fa03preciofact = 0.25 * suminterv / rnf.fa03cantfact,
                      fa0300.fa15codatrib =
                         tarifa (rnf.fa05codcateg).fa15codatrib
                WHERE fa03numrnf = rnf.fa03numrnf;
            ELSE
               -- NO tiene precio -> se le pone precio 0
               UPDATE fa0300
                  SET fa03preciofact = 0,
                      fa0300.fa15codatrib =
                         tarifa (rnf.fa05codcateg).fa15codatrib
                WHERE fa03numrnf = rnf.fa03numrnf;
            END IF;
         END IF;
    	 EXCEPTION
		    WHEN OTHERS THEN
			   err:=SQLCODE;
			   errm:=SQLERRM(SQLCODE);
		       ROLLBACK;
			   INSERT INTO FA4500(FA45CODREGISTRO, FA45NUMOPERACION, FA45FECREGISTRO,
					  FA04CODFACT, FA16NUMLINEA, FA03NUMRNF, FA45TIPOSUCESO,
					  FA45DESSUCESO)
				 VALUES(FA45CODREGISTRO_SEQUENCE.NEXTVAL,-1,SYSDATE,CODFACT,1,
				 		RNF.FA03NUMRNF,err,errm);
				 COMMIT;
		         RETURN 2;
         END;
      END LOOP;
	  COMMIT;
      RETURN 0;
   END valorarrnfs;
/*****************************************************************************/
   FUNCTION CargarTarifa(teco varchar, tarifa in out tttarifa)
      return NUMBER is
   /**************************************************************************
   	 Funci�n que carga en un Array una tarifa de acto m�dico.
   ***************************************************************************/
      codconc   NUMBER;
      codnodo   NUMBER;
      -- Carga de tarifas
      CURSOR ctarifa (cn NUMBER, cf NUMBER) IS
         SELECT fa1500.fa15codfin,
                fa1500.fa15codatrib codatrib,
                fa1500.fa15precioref,
                fa1500.fa15preciodia,
                fa15fecinicio
           FROM fa1500
          WHERE fa15escat = 1
            AND fa15codconc = cn
            AND fa15ruta LIKE '.' || cn || '.' || cf || '.%'
            AND SYSDATE BETWEEN fa15fecinicio AND fa15fecfin;
   Begin
      -- Coger los nodos de facturacion relacionados con el teco.
      SELECT DISTINCT fa09codnodoconc, fa09codnodofact
        INTO codconc,
             codnodo
        FROM ci1300
       WHERE ci32codtipecon = teco;
      --Carga de Tarifa
      FOR rctarifa IN ctarifa (codconc, codnodo) LOOP
         tarifa (rctarifa.fa15codfin) := rctarifa;
      END LOOP;
	  return 0;
   End CargarTarifa;
/*****************************************************************************/
   FUNCTION CargarRNFs (
      codfac       NUMBER,
      nproceso     ad0700.ad07codproceso%TYPE,
      nasistenci   ad0100.ad01codasistenci%TYPE,
	  intCientif   number --Si true cojer solo lo de interes cientifico
   )
      RETURN number IS
/******************************************************************************
  FUNCION QUE CARGA LOS RNFS DE UN PROCESO-ASISTENCIA, ANTES SE BORRAN LOS QUE
  ESTUVIERAN PENDIENTES
******************************************************************************/
   BEGIN
      -- cargar los nuevos rnfs en la fa0300
      INSERT INTO fa0300
                  (
                     fa03numrnf,
                     fa05codcateg,
                     fa03precio,
                     fa03preciofact,
                     fa03cantidad,
                     fa03cantfact,
                     fa13codestrnf,
                     ad01codasistenci,
                     fa03fecha,
                     ci22numhistoria,
                     fa03observ,
                     ad07codproceso,
                     fa03indrealizado,
                     ad02coddpto_r,
                     sg02cod_r,
                     ad02coddpto_s,
                     sg02cod_s,
                     fa04numfact,
					 fa16numlinea
                  )
         SELECT fa03numrnf_sequence.nextval,
                fa05codcateg,
                precio,
                precio,
                cantidad,
                cantidad,
                1,
                ad01codasistenci,
                fecha,
                ci22numhistoria,
                obs,
                ad07codproceso,
                estado,
                dptorealiza,
                drrealiza,
                dptosolicita,
                drsolicita,
                codfac,
				1
           FROM fatmpj t1
          WHERE t1.ad07codproceso = nproceso
            AND t1.ad01codasistenci = nasistenci
			AND t1.ic=intCientif
            AND NOT EXISTS (SELECT ad07codproceso,
                                   ad01codasistenci,
                                   fa05codcateg,
                                   fa03fecha,
                                   fa03cantidad
                              FROM fa0300 t2
                             WHERE t2.ad07codproceso = t1.ad07codproceso
                               AND t2.ad01codasistenci = t1.ad01codasistenci
                               AND t2.fa05codcateg = t1.fa05codcateg
                               AND t2.fa03cantidad = t1.cantidad
                               AND t2.fa03fecha = t1.fecha
                               AND t2.fa03indfact <> 3);
      RETURN 0;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         RETURN 1;
   END cargarrnfs;
/*****************************************************************************/
   FUNCTION FacturarActoMedico (tarifa tttarifa, fechafact DATE, reco Number,
   			FixNumfac  varchar2, teco varchar2)
      RETURN NUMBER IS
/*****************************************************************************
  FUNCTION      : FacturarActoMedico()
  SCOPE         : PUBLIC
  DESCRIPTION   : Crea las facturas de una entidad ( de momento es una
  				  constante ) y aplicando un concierto determinado ( de momento
				  tambien es constante ), recogiendo todos los pacientes
				  pendientes de un determinado mes (parametro) y valorando
				  todos sus rnfs
  PARAMETERS IN : Mes ( se corresponde con el FA37MES ) determina que pacientes
  			 	  hay que facturar
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
******************************************************************************/
     estado    Number;
     codfac    Number;
	  suma		Number;
	  tasi		Number;
	  err		   Number;
	  errm		Varchar2(2000);
	  numfac	   Varchar2(20);
      -- cursor que contiene los pacientes de la fa3700 pendientes de facturar
      CURSOR curpacientes (teco VARCHAR2, mes DATE) IS
         SELECT *
           FROM fa3702J
          WHERE ci32codtipecon = teco
            AND fecha = mes;
   BEGIN
      FOR paciente IN curpacientes (teco, fechafact) LOOP
	  	 BEGIN
         -- borrar los rnfs del proc-asist que ya est�n en la FA0300
         estado:=BorrarRNFs (paciente.ad07codproceso, paciente.ad01codasistenci);
  --************************************************************
  --  GRABAR LA CABECERA ( una por cada proc-asist facturado )
  --************************************************************
        SELECT fa04codfact_sequence.nextval
          INTO codfac
          FROM dual;
        IF FixNumfac is null THEN
           Select teco||'/'||ltrim(to_char(ci22numhistoria,'000000'))||
                  to_char(fechafact,'YYMM')||paciente.ci13codentidad
           Into   numfac
           From   ad0100
           Where  ad01codasistenci=paciente.ad01codasistenci;
        ELSE
            numfac := FixNumfac;
        END IF;
        INSERT INTO fa0400
                    (
                       fa04codfact,fa04numfact,ci21codpersona,ci13codentidad,
                       fa04descrip,fa04cantfact,fa04descuento,ad01codasistenci,
                       fa04observ,fa09codnodoconc,ad07codproceso,ci32codtipecon,
                       fa04fecfactura,fa04indnmgc,fa04fecdesde,fa04fechasta,
                       fa04indcompensa,fa09codnodoconc_fact,fa04codfact_garan,
                       fa04fecfingarantia,fa04numacumulgaran
                    )
           VALUES(     codfac,numfac,reco,paciente.ci13codentidad,
		               NULL,0,0,paciente.ad01codasistenci,
		               NULL,NULL,paciente.ad07codproceso,
		               paciente.ci32codtipecon,fechafact,
		               1,NULL,NULL,0,NULL,NULL,NULL,NULL );
		COMMIT; --Hacemos commit para grabar la cabecera de factura.
        -- Cargar los rnfs del proc-asist en la FA0300
      estado:= estado + CargarRNFs (codfac, paciente.ad07codproceso,
						       paciente.ad01codasistenci, 0);
		COMMIT; --Commit de los movimientos.
        -- valorar los rnfs del proc-asist
      estado:=estado + ValorarRNFs (tarifa, codfac);
		COMMIT; --A la valoracion (grabar precios en los movimientos)
		/*Modificar la cantidad de la factura, sin decimales*/
		Select round(nvl(sum(fa03cantfact*fa03preciofact),0))
		into suma
		From FA0300
		where fa04numfact=codfac;
		Update fa0400
		set fa04cantfact=suma
		Where fa04codfact=codfac;
		COMMIT;
		/*Insertamos una linea de factura para que no quede huerfana*/
		Insert Into FA1600(FA04CODFACT, FA16NUMLINEA, FA14CODNODOCONC,
		 FA16DESCRIP,FA16PRCTGRNF, FA16PRECIO, FA16DCTO, FA16IMPORTE,
		 FA16ORDFACT,FA16FRANQUICIA, FA16CANTSINFRAN, FA15CODATRIB)
		Values(codfac,1,NULL,
		 'Importe '||teco||'-'||paciente.ci13codentidad,0,suma,0,suma,
		 1,0,0,null
		);
		COMMIT;
        -- marcar el paciente como facturado
		if paciente.tasi='H' then tasi:=1; else tasi:=2; end if;
		INSERT INTO FA3700( FA37CODPAC, AD01CODASISTENCI, AD07CODPROCESO,
			   				CI32CODTIPECON, CI13CODENTIDAD, AD12CODTIPOASIST,
							FA37INDFACT, FA37FECHA, FA37FECINICIO, FA37FECFIN,
							FA37MES)
		VALUES ( FA37CODPAC_SEQUENCE.NEXTVAL,paciente.ad01codasistenci,
			     paciente.ad07codproceso,paciente.ci32codtipecon,
				 paciente.ci13codentidad,tasi,decode(estado,0,1,0),sysdate,null,null,
				 fechafact);
        COMMIT;
		EXCEPTION
		   WHEN OTHERS THEN
		   ROLLBACK;
		   err:=SQLCODE;
		   errm:=SQLERRM(SQLCODE);
	       ROLLBACK;
		   INSERT INTO FA4500(FA45CODREGISTRO, FA45NUMOPERACION, FA45FECREGISTRO,
				  FA04CODFACT, FA16NUMLINEA, FA03NUMRNF, FA45TIPOSUCESO,
				  FA45DESSUCESO)
			 VALUES(FA45CODREGISTRO_SEQUENCE.NEXTVAL,-1,SYSDATE,codfac,1,
			 		0,err,errm);
			 COMMIT;
		END;
     END LOOP;
     RETURN estado;
   EXCEPTION
     WHEN OTHERS THEN
        ROLLBACK;
        RETURN 1;
   END FacturarActoMedico;
/*****************************************************************************/
   FUNCTION FacturarEntidadColaboradora( fechafact DATE )
      RETURN number IS
	  estado   number;
	  tarifaI  tttarifa;
   BEGIN
      estado:= CargarTarifa('I',tarifaI);
      estado:= estado + FacturarActoMedico (tarifaI, fechafact, RECO_Entidad,
	  		   					   NULL, 'I');
	  return estado;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         RETURN 1;
   end FacturarEntidadColaboradora;
/*****************************************************************************/
   FUNCTION FacturarInvestigacion( fechafact DATE )
      RETURN number IS
/******************************************************************************
  FUNCTION      : FacturarInvestigacion()
  SCOPE         : PUBLIC
  DESCRIPTION   : Crea las facturas de investigacion ( de momento es una
  				  constante ) y aplicando un concierto determinado ( de momento
				  tambien es constante ), recogiendo todos los pacientes
				  pendientes de un determinado mes (parametro) y valorando
				  todos sus rnfs
  PARAMETERS IN : Mes ( se corresponde con el FA37MES ) determina que pacientes
  			 	  hay que facturar
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
******************************************************************************/
   /*Este cursor devuelve Proc/Asist que tienen algo de Investigaci�n siendo
     su TECO cualquiera, incluyendo 'H'*/
	  cursor   cMovSueltosInv(fc Date) is
		select fa0400.ad07codproceso,fa0400.ad01codasistenci
		from fa0400,pr0400
		where fa04feccontabil=fc
		  and pr0400.ad07codproceso=fa0400.ad07codproceso
		  and pr0400.ad01codasistenci=fa0400.ad01codasistenci
		  and pr0400.PR04INDINTCIENTIF=-1
		  and pr0400.pr37codestado between 1 and 5
		union
		select fa0400.ad07codproceso,fa0400.ad01codasistenci
		from fa0400,fr6500
		where fa04feccontabil=fc
		  and fr6500.ad07codproceso=fa0400.ad07codproceso
		  and fr6500.ad01codasistenci=fa0400.ad01codasistenci
		  and fr6500.FR65INDINTERCIENT=-1
		union
		select fa0400.ad07codproceso,fa0400.ad01codasistenci
		from fa0400,ad0800,pruebaasistencia pa
		where fa04feccontabil=fc
		  and ad0800.ad07codproceso=fa0400.ad07codproceso
		  and ad0800.ad01codasistenci=fa0400.ad01codasistenci
		  and ad0800.ad08numcaso=rpad(pa.historia,6,'0')||rpad(pa.caso,4,'0')
		  and pa.investigacion in (1,-1);
     tarifaH  tttarifa;
	 estado   number;
	 codfact  number;
	 numfac	  varchar2(20);
	 cantfac  Number;
  BEGIN
  	 /*Cargar Tarifa de Investigacion*/
	 estado := CargarTarifa('H',tarifaH);
	 /*Todos los movimientos van a una unica factcura*/
	 Select 'H/'||to_char(fechafact,'YYYYMM')
	 into numfac
	 from dual;
     /*Proc/Asis que tienen movimientos de investigacion y no son
	   Del tipo 'H'*/
     for r in cMovSueltosInv(fechafact) loop
	 	/*Siguiente factura*/
	 	Select fa04codfact_sequence.nextval into codfact from dual;
		/*Grabamos la cabecera*/
        INSERT INTO fa0400(fa04codfact,fa04numfact,ci21codpersona,
		    ci13codentidad,fa04descrip,fa04cantfact,fa04descuento,
		    ad01codasistenci,fa04observ,fa09codnodoconc,ad07codproceso,
		    ci32codtipecon,fa04fecfactura,fa04indnmgc,fa04fecdesde,
		    fa04fechasta,fa04indcompensa,fa09codnodoconc_fact,fa04codfact_garan,
		    fa04fecfingarantia,fa04numacumulgaran)
		VALUES(codfact,numfac,RECO_Investigacion,
			'H',NULL,0,0,
			r.ad01codasistenci,'Investigacion',102,r.ad07codproceso,
			'H',fechafact,0,NULL,
			NULL,0,328,NULL,
			NULL,NULL
		);
		/*Limpiar*/
        estado := BorrarRNFs(r.ad07codproceso,r.ad01codasistenci);
		/*Cargar en FA0300*/
		estado := estado + CargarRNFs(codfact,r.ad07codproceso,
		                                r.ad01codasistenci,-1);
		/*Valorarlos*/
		estado := estado + ValorarRNFs(tarifaH,codfact);
		/*Modificar la cantidad de la factura, asegurando que no hay decimales*/
		Select round(sum(fa03cantfact*fa03preciofact))
		into cantfac
		From FA0300
		where fa04numfact=codfact;
		Update fa0400
		set fa04cantfact=cantfac
		Where fa04codfact=codfact;
		/*Insertamos una linea de factura para que no quede huerfana*/
		Insert Into FA1600(FA04CODFACT, FA16NUMLINEA, FA14CODNODOCONC,
		 FA16DESCRIP,FA16PRCTGRNF, FA16PRECIO, FA16DCTO, FA16IMPORTE,
		 FA16ORDFACT,FA16FRANQUICIA, FA16CANTSINFRAN, FA15CODATRIB)
		Values(codfact,1,NULL,
		 'Importe Investigacion',0,cantfac,0,cantfac,
		 1,0,0,null
		);
		/*Finalmente marcarla como hecha*/
		INSERT INTO FA3700( FA37CODPAC, AD01CODASISTENCI, AD07CODPROCESO,
			   				CI32CODTIPECON, CI13CODENTIDAD, AD12CODTIPOASIST,
							FA37INDFACT, FA37FECHA, FA37FECINICIO, FA37FECFIN,
							FA37MES)
		VALUES ( FA37CODPAC_SEQUENCE.NEXTVAL,r.ad01codasistenci,
			     r.ad07codproceso,'H','H',0,decode(estado,0,1,estado*10),
			     sysdate,null,null,fechafact);
		Commit;
	 end loop;
	 /*A continuacion a�adir a esta factura los tipos 'H'*/
	 estado := estado + FacturarActoMedico(tarifaH, fechafact , RECO_Investigacion ,
	 		   					  numfac , 'H' );
     return estado;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         RETURN 1;
   END FacturarInvestigacion;
/*****************************************************************************/
   FUNCTION FacturarIH (fechafact DATE)
      RETURN number IS
/*****************************************************************************
  FUNCTION      : FacturarIH()
  SCOPE         : PUBLIC
  DESCRIPTION   : Crea las facturas de una entidad ( de momento es una
  				  constante ) y aplicando un concierto determinado ( de momento
				  tambien es constante ), recogiendo todos los pacientes
				  pendientes de un determinado mes (parametro) y valorando
				  todos sus rnfs
  PARAMETERS IN : Mes ( se corresponde con el FA37MES ) determina que pacientes
  			 	  hay que facturar
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
******************************************************************************/
      estado   number:=0;
   BEGIN
	  estado:= estado + FacturarEntidadColaboradora(last_day(fechafact));
      estado:= estado + FacturarInvestigacion(last_day(fechafact));
	  return estado;
   END facturarih;
END fapk02;
/

