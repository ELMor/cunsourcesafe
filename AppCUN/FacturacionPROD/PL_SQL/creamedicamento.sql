
   PROCEDURE creamedicamento (codintfar NUMBER) IS
      /*Crea un medicamento cuyo fr73codintfar es el parametro del procedimiento; de forma que se le pone el
        nombre en la categor�a igual al que consta en la fr7300, el precioventa (no el concertado)*/
      reg73   fr7300%ROWTYPE;
      nc      NUMBER;
   BEGIN
      /*Seleccionamos el registro del medicamento para obtener el Nombre y el Precio*/
      SELECT *
        INTO reg73
        FROM fr7300
       WHERE fr73codproducto =
              (SELECT MAX (fr73codproducto)
                 FROM fr7300
                WHERE fr73codintfar = codintfar);
      /*Seleccionamos la categoria que mas se le parece (del mismo grupo terapeutico*/
      SELECT MAX (fa05codcateg)
        INTO nc
        FROM fa0500
       WHERE fa08codgrupo = 1
         AND fa05codorigen IN (SELECT fr73codintfar
                                 FROM fr7300
                                WHERE fr00codgrpterap =
                                       (SELECT fr00codgrpterap
                                          FROM fr7300
                                         WHERE fr73codintfar = codintfar));
      copycat (
         reg73.fr73desproducto, reg73.fr73precioventa, 1, nc, TO_CHAR (codintfar)
      );
   END;
