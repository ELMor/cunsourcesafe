   FUNCTION fafn01 (
      numad07codproceso     IN   NUMBER,
      numad01codasistenci   IN   NUMBER,
      datefecha             IN   DATE,
      numfa05codcateg       IN   NUMBER
   )
      RETURN VARCHAR2 IS
      strtexto    VARCHAR2 (2000);
      numinicio   INTEGER;

      /* Agrupa las descripciones de las categor�as en una sola l�nea */
      CURSOR crsrpruebas IS
         SELECT fa0500.fa05desig
           FROM fa0500,
                pr0400
          WHERE fa0500.fa05codorigen = pr0400.pr01codactuacion
            AND fa0500.fa05codcateg IN
                (SELECT fa05codcateg
                   FROM fa2600
                  WHERE fa27codgrupofac IN (SELECT fa27codgrupofac
                                              FROM fa2800
                                             WHERE fa05codcateg = numfa05codcateg))
            AND pr0400.ad01codasistenci = numad01codasistenci
            AND pr0400.ad07codproceso = numad07codproceso
            AND TO_CHAR (
                   NVL (pr0400.pr04fecfinact, pr0400.pr04fecadd), 'DD/MM/YYYY'
                ) = TO_CHAR (datefecha, 'DD/MM/YYYY')
            AND FLOOR (
                   TO_NUMBER (
                      TO_CHAR (
                         NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'HH24'
                      )
                   ) /
                      14
                ) = FLOOR (TO_NUMBER (TO_CHAR (datefecha, 'HH24')) / 14)
            AND pr0400.pr37codestado < 6;
   BEGIN
      /* Se toman de la FA0500-PR0400 todas las pruebas que cumplen las condiciones de ese grupo */
      numinicio                  := 0;

      FOR pruebas IN crsrpruebas LOOP
         IF numinicio = 0 THEN
            strtexto                   := pruebas.fa05desig;
            numinicio                  := 1;
         ELSE
            strtexto                   := strtexto || '; ' || pruebas.fa05desig;
         END IF;
      END LOOP;

      RETURN (strtexto);
   END fafn01;
