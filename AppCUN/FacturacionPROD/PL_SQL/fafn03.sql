   FUNCTION fafn03 (str VARCHAR2)
      RETURN VARCHAR2 IS
      var_str   VARCHAR2 (100);
   BEGIN
      var_str                    := str;

      WHILE (INSTR (var_str, '  ', 1, 1) <> 0) LOOP
         var_str                    := REPLACE (var_str, '  ', ' ');
      END LOOP;

      var_str                    := LTRIM (RTRIM (var_str));
      RETURN (var_str);
   END fafn03;
