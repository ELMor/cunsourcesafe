   PROCEDURE deleteentidad IS
      CURSOR c1 IS
         SELECT *
           FROM fa0400
          WHERE fa04numfact LIKE 'I%';
   BEGIN
      FOR r IN c1 LOOP
         DELETE
           FROM fa0300
          WHERE fa04numfact = r.fa04codfact;

         DELETE
           FROM fa1600
          WHERE fa04codfact = r.fa04codfact;

         DELETE
           FROM fa0400
          WHERE fa04codfact = r.fa04codfact;

         COMMIT;
      END LOOP;
   END;
