   PROCEDURE propagacategoria (cc NUMBER, pr NUMBER) IS
   /* CC es el codigo de categoria que se quiere insertar en todos los conciertos,
      PR es el precio de referencia que se quiere poner. CC debe existir ya en
      la FA0500 y en la FA0600 */
      CURSOR c1 IS
         SELECT fa09codnodoconc
           FROM fa0900
          WHERE fa09indconcierto = -1;

      CURSOR c2 (co NUMBER, ca NUMBER) IS
         SELECT fa15codatrib
           FROM fa1500
          WHERE fa15codconc = co
            AND fa15escat = 1
            AND fa15codfin = ca;

      fecini   DATE;
      fecfin   DATE;
      newatr   NUMBER;
      r15      VARCHAR2 (2500);
      cp       NUMBER;
   BEGIN
      SELECT fa05codcateg_p, fa06fecinicio, fa06fecfin
        INTO cp,
             fecini,
             fecfin
        FROM fa0600
       WHERE fa05codcateg = cc
         AND SYSDATE >= fa06fecinicio
         AND SYSDATE < fa06fecfin;

      IF cp IS NOT NULL THEN
         FOR r1 IN c1 LOOP
            FOR r2 IN c2 (r1.fa09codnodoconc, cp) LOOP
               SELECT fa15codatrib_sequence.nextval
                 INTO newatr
                 FROM dual;
               SELECT fa15ruta
                 INTO r15
                 FROM fa1500
                WHERE fa15codatrib = r2.fa15codatrib;

               INSERT INTO fa1500
                           (
                              fa15codatrib,
                              fa15ruta,
                              fa15fecinicio,
                              fa15fecfin,
                              fa15precioref
                           )
                    VALUES (newatr, r15 || cc || '.', fecini, fecfin, pr);
            END LOOP;
         END LOOP;
      ELSE
         raise_application_error (-12101, 'No tiene categor�a padre');
      END IF;

      COMMIT;
   END;
