CREATE OR REPLACE PACKAGE fapk01 IS
   /******************************************************************************
   NAME:       FAPK01
   PURPOSE:    Generaci�n de apuntes contables a partir de un rango de facturas

   PUBLIC INTERFACE:
   Object Name             Object Type            Description
   --------------------    -------------------     -----------
   t_RNF_record            USER DEFINED RECORD     Utilizada para pasar un regi
                                                   stro RNF a las funciones
                                                   FAFN14 y FAFN15
   FAPR01                  PROCEDURE               Generaci�n de apuntes cont.
   FAPR02                  PROCEDURE               Generaci�n de apuntes Dic/99
   FAFN14                  FUNCTION             Funci�n para calcular el precio
   FAFN15                  FUNCTION             Funci�n para calcular el coste

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05/04/2000                  1. Creaci�n del paquete
   1.1        10/04/2000                   1. Modificado el procedimiento FAPR01
                                         - recibe solo una fecha de inicio elabor
                                  aci�n en vez de
                                     fechas inicio y fin y codigos factura
                                   - el descuadre genera una excepci�n solo si no
                             es debido a importes decimales
   ******************************************************************************/
      dep_dif   CONSTANT NUMBER := 310;

      TYPE tt_acum_dpto IS TABLE OF NUMBER
         INDEX BY BINARY_INTEGER;

      PROCEDURE fapr01 (
         FecContabil1       fa0400.fa04fecfactura%TYPE,
         p_numero_asiento   fa2900.fa29numasiento%TYPE,
         indtodas           NUMBER
      );

      PROCEDURE fapr02 (cf NUMBER, fc DATE, na NUMBER);
   END fapk01;
/

CREATE OR REPLACE PACKAGE BODY fapk01 AS
   /*****************************************************************************/
      FUNCTION PrecioRNF (

   /******************************************************************************
     FUNCTION      : PrecioRNF()
     SCOPE         : PUBLIC

     DESCRIPTION   : calcula el PRECIO de un RFN basandose en la formula utilizada
                    en la funci�n. Si el precio es nulo, devuelve NULL
     PARAMETERS IN : p_RNF, registro de tipo t_RNF_record con los datos de un RNF
     RETURN VALUE  : (NUMBER) el precio calculado o NULL si el precio es nulo

   *******************************************************************************/
         nrnf   NUMBER,
         dptr   NUMBER,
         ccat   NUMBER,
         prec   NUMBER,
         cost   NUMBER,
         cant   NUMBER,
         cfac   NUMBER,
         nlin   NUMBER
      )
         RETURN NUMBER IS
         -- De momento la formula es : PRECIO = FA03PRECIOFACT
         p_return   NUMBER;
      BEGIN
         -- Si son Derechos de Quir�fano u Honorarios Anestesia, hay que acudir
         -- a la l�nea de factura.
         IF     ccat >= 35352
            AND ccat <= 35354 THEN
            SELECT fa16importe
              INTO p_return
              FROM fa1600
             WHERE fa1600.fa04codfact = cfac
               AND fa1600.fa16numlinea = nlin;
         ELSE
            p_return := prec * cant;
         END IF;

         IF p_return IS NULL THEN
            p_return := 0;
         END IF;

         RETURN p_return;
      END PrecioRNF;

   /*****************************************************************************/
      FUNCTION CosteRNF (

   /******************************************************************************
     FUNCTION      : CosteRNF()
     SCOPE         : PUBLIC

     DESCRIPTION   : calcula el COSTE de un RFN basandose en la formula utilizada
                    en la funci�n. Si el coste es nulo, devuelve NULL
     PARAMETERS IN : p_RNF, registro de tipo t_RNF_record con los datos de un RNF
     RETURN VALUE  : (NUMBER) el coste calculado o NULL si el coste es nulo

   ******************************************************************************/
         nrnf   NUMBER,
         dptr   NUMBER,
         ccat   NUMBER,
         prec   NUMBER,
         cost   NUMBER,
         cant   NUMBER,
         cfac   NUMBER,
         nlin   NUMBER
      )
         RETURN NUMBER IS
         -- El campo COSTECATEG de la estructura de tipo registro t_RNF_record es
         -- equivalente a FA3500.FA35COSTE
         p_return   NUMBER;
      BEGIN
         IF ccat IS NULL THEN
            p_return := NULL;
         ELSIF     ccat = 35352
               AND ccat = 35354 THEN  --Derechos de quirofano (50% DE LO FACTURADO)
            SELECT 0.5 * fa16importe
              INTO p_return
              FROM fa1600
             WHERE fa1600.fa04codfact = cfac
               AND fa1600.fa16numlinea = nlin;
         ELSIF ccat = 35353 THEN
            --Derechos de Anestesia= 25% del coste de intervenciones
            SELECT 0.25 * SUM (fa03cantfact * fa35coste)
              INTO p_return
              FROM fa0300,
                   fa0500,
                   fa3500
             WHERE fa0300.fa05codcateg = fa0500.fa05codcateg
               AND fa0300.fa05codcateg = fa3500.fa05codcateg
               AND fa08codgrupo = 6                                --Intervenciones
               AND fa0300.fa04numfact = cfac;
         ELSE
            p_return := cost * cant;
         END IF;

         RETURN p_return;
      END CosteRNF;

   /*****************************************************************************/
      PROCEDURE CheckFact (
         cf                       NUMBER,
         TieneMovimientos   OUT   BOOLEAN,
         DescLineas         OUT   BOOLEAN,
         DescRNF            OUT   BOOLEAN,
         DescRNFDec         OUT   BOOLEAN,
         Forfait            OUT   BOOLEAN,
         EsDeIBM            OUT   BOOLEAN
      ) IS
   /******************************************************************************
   Funcion        : Comprobaciones descuadre.
   Ambito         : Privada
   Descripcion    : Comprueba sumatorios lineas, rnf con cantfact.
   Retorna     : True si va todo bien, Flase si hay algun error.
   ******************************************************************************/
         NUMMOV     NUMBER;
         CANTFACT   NUMBER;
         IMPMOV     NUMBER;
         IMPLIN     NUMBER;
         IBM        NUMBER;
      BEGIN
         -- Si est� en RegFact es que es de IBM (Repartida all�)
         SELECT COUNT (*)
           INTO IBM
           FROM RegFact,
                fa0400
          WHERE ffact = cf
            AND fa04codfact = cf
            AND fa04numfacreal IS NULL;

         IF ibm > 0 THEN
            EsDeIBM := TRUE;
         ELSE
            EsDeIBM := FALSE;
         END IF;

         -- Comprobar si tiene mov en la fa0300
         SELECT COUNT (*)
           INTO NUMMOV
           FROM FA0300
          WHERE fa04numfact = cf;

         IF NUMMOV = 0 THEN
            TieneMovimientos := FALSE;
         ELSE
            TieneMovimientos := TRUE;
         END IF;

         -- Obtener cantidad Facturada.
         SELECT FA04CantFact
           INTO CantFact
           FROM FA0400
          WHERE fa0400.fa04codfact = cf;
         -- Comprobacion descuadres en suma de lineas.
         SELECT SUM (fa16importe)
           INTO IMPLIN
           FROM fa1600
          WHERE fa1600.fa04codfact = cf;

         IF IMPLIN IS NULL THEN
            DescLineas := TRUE;
         ELSE
            IF CANTFACT = IMPLIN THEN
               DescLineas := FALSE;
            ELSE
               DescLineas := TRUE;
            END IF;
         END IF;

         -- Comprobacion Descuadres en suma de RNF
         SELECT MAX (importemov)
           INTO IMPMOV
           FROM fa0420j
          WHERE fa04codfact = cf;

         IF IMPMOV IS NULL THEN
            DescRNF := TRUE;
            DescRNFDec := FALSE;
         ELSE
            IF CANTFACT = IMPMOV THEN
               DescRNF := FALSE;
            ELSE
               DescRNF := TRUE;

               IF ROUND (impmov) = ROUND (cantfact) THEN
                  DescRNFDec := TRUE;
               ELSE
                  DescRNFDec := FALSE;
               END IF;
            END IF;
         END IF;

         IF ROUND (IMPLIN) = ROUND (IMPMOV) THEN
            Forfait := FALSE;
         ELSE
            Forfait := TRUE;
         END IF;
      END CheckFact;

   /*****************************************************************************/
      FUNCTION Purge_Log (p_numero_asiento fa4500.fa45numoperacion%TYPE)
         RETURN BOOLEAN IS
   /******************************************************************************
     FUNCTION      : Purge_Log()
     SCOPE         : PRIVATE

     DESCRIPTION   : elimina de la tabla de log todos los registros
                    correspondientes al n�mero de operaci�n pasado como
                     parametro.
     PARAMETERS IN : p_numero_asiento
     RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)

   *******************************************************************************/
      BEGIN
         DELETE
           FROM fa4500
          WHERE fa45numoperacion = p_numero_asiento;

         COMMIT;
         RETURN TRUE;
      END purge_log;

   /*****************************************************************************/
      PROCEDURE CorregirTelef (cf NUMBER) IS
      BEGIN
   /******************************************************************************
     PROCEDURE     : CorregirTelef()
     SCOPE         : PRIVATE

     DESCRIPTION   : Inserta los RNF debidos a tel�fonos,Dietas acompa�ante y lava
                     nder�a
     PARAMETERS IN : cf (Codigo de factura)
     RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (SELECT or INSERT error)

   ******************************************************************************/
         INSERT INTO fa0300
                     (
                        FA03NUMRNF,
                        FA05CODCATEG,
                        FA13CODESTRNF,
                        FA04NUMFACT,
                        AD01CODASISTENCI,
                        FA03CANTIDAD,
                        FA03NUMRNF_P,
                        FA03PRECIO,
                        FA03INDFACT,
                        FA03PRECIOFACT,
                        FA03OBSERV,
                        FA03FRANQUNI,
                        FA03FRANQSUMA,
                        FA03DESCUENTO,
                        FA03PERIODOGARA,
                        FA03PRECIODIA,
                        FA03FECHA,
                        AD02CODDPTO_R,
                        SG02COD_R,
                        AD02CODDPTO_S,
                        SG02COD_S,
                        CI22NUMHISTORIA,
                        FA16NUMLINEA,
                        FA03INDDESGLOSE,
                        FA15CODATRIB,
                        AD07CODPROCESO,
                        FA03INDREALIZADO,
                        FA03CANTFACT,
                        SG02COD_ADD,
                        FA03FECADD,
                        SG02COD_UPD,
                        FA03FECUPD,
                        FA03OBSIMP,
                        FA03INDPERMANENTE
                     )
            SELECT fa03numrnf_sequence.nextval,
                   42284,
                   1,
                   fa0400.fa04codfact,
                   fa0400.ad01codasistenci,
                   1,
                   NULL,
                   fa16precio,
                   2,
                   fa16precio,
                   NULL,
                   NULL,
                   NULL,
                   0,
                   NULL,
                   NULL,
                   fa04fecfactura,
                   10,                                 --DPTO CENTRALITA TELEFONICA
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   fa16numlinea,
                   0,
                   NULL,
                   fa0400.ad07codproceso,
                   NULL,
                   1,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM fa0400,
                   fa1600
             WHERE fa0400.fa04codfact = cf
               AND fa1600.fa04codfact = fa0400.fa04codfact
               AND fa1600.fa14codnodoconc = 0
               AND UPPER (fa1600.fa16descrip) LIKE
                      '%TELEF%'
               AND fa0400.fa04cantfact > 0
               AND fa0400.fa04numfacreal IS NULL
               AND fa0400.fa04feccontabil IS NULL
               AND NOT EXISTS (SELECT fa05codcateg
                                 FROM fa0300
                                WHERE fa0300.fa04numfact = fa0400.fa04codfact
                                  AND fa0300.fa05codcateg = 42284);

         UPDATE FA0300
            SET AD02CODDPTO_R = 10
          WHERE FA05CODCATEG = 42284
            AND AD02CODDPTO_R <> 10
            AND FA04NUMFACT = CF;

         COMMIT;

         INSERT INTO fa0300
                     (
                        FA03NUMRNF,
                        FA05CODCATEG,
                        FA13CODESTRNF,
                        FA04NUMFACT,
                        AD01CODASISTENCI,
                        FA03CANTIDAD,
                        FA03NUMRNF_P,
                        FA03PRECIO,
                        FA03INDFACT,
                        FA03PRECIOFACT,
                        FA03OBSERV,
                        FA03FRANQUNI,
                        FA03FRANQSUMA,
                        FA03DESCUENTO,
                        FA03PERIODOGARA,
                        FA03PRECIODIA,
                        FA03FECHA,
                        AD02CODDPTO_R,
                        SG02COD_R,
                        AD02CODDPTO_S,
                        SG02COD_S,
                        CI22NUMHISTORIA,
                        FA16NUMLINEA,
                        FA03INDDESGLOSE,
                        FA15CODATRIB,
                        AD07CODPROCESO,
                        FA03INDREALIZADO,
                        FA03CANTFACT,
                        SG02COD_ADD,
                        FA03FECADD,
                        SG02COD_UPD,
                        FA03FECUPD,
                        FA03OBSIMP,
                        FA03INDPERMANENTE
                     )
            SELECT fa03numrnf_sequence.nextval,
                   42286,
                   1,
                   fa0400.fa04codfact,
                   fa0400.ad01codasistenci,
                   1,
                   NULL,
                   fa16precio,
                   2,
                   fa16precio,
                   NULL,
                   NULL,
                   NULL,
                   0,
                   NULL,
                   NULL,
                   fa04fecfactura,
                   19,                                                --dpto realiz
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   fa16numlinea,
                   0,
                   NULL,
                   fa0400.ad07codproceso,
                   NULL,
                   1,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM fa0400,
                   fa1600
             WHERE fa0400.fa04codfact = cf
               AND fa1600.fa04codfact = fa0400.fa04codfact
               AND fa1600.fa14codnodoconc = 0
               AND UPPER (fa1600.fa16descrip) LIKE
                      '%LAVAND%'
               AND fa0400.fa04cantfact > 0
               AND fa0400.fa04numfacreal IS NULL
               AND fa0400.fa04feccontabil IS NULL
               AND NOT EXISTS (SELECT fa05codcateg
                                 FROM fa0300
                                WHERE fa0300.fa04numfact = fa0400.fa04codfact
                                  AND fa0300.fa05codcateg = 42286);

         UPDATE FA0300
            SET AD02CODDPTO_R = 18
          WHERE FA05CODCATEG = 42285
            AND AD02CODDPTO_R <> 18
            AND FA04NUMFACT = CF;

         COMMIT;

         INSERT INTO fa0300
                     (
                        FA03NUMRNF,
                        FA05CODCATEG,
                        FA13CODESTRNF,
                        FA04NUMFACT,
                        AD01CODASISTENCI,
                        FA03CANTIDAD,
                        FA03NUMRNF_P,
                        FA03PRECIO,
                        FA03INDFACT,
                        FA03PRECIOFACT,
                        FA03OBSERV,
                        FA03FRANQUNI,
                        FA03FRANQSUMA,
                        FA03DESCUENTO,
                        FA03PERIODOGARA,
                        FA03PRECIODIA,
                        FA03FECHA,
                        AD02CODDPTO_R,
                        SG02COD_R,
                        AD02CODDPTO_S,
                        SG02COD_S,
                        CI22NUMHISTORIA,
                        FA16NUMLINEA,
                        FA03INDDESGLOSE,
                        FA15CODATRIB,
                        AD07CODPROCESO,
                        FA03INDREALIZADO,
                        FA03CANTFACT,
                        SG02COD_ADD,
                        FA03FECADD,
                        SG02COD_UPD,
                        FA03FECUPD,
                        FA03OBSIMP,
                        FA03INDPERMANENTE
                     )
            SELECT fa03numrnf_sequence.nextval,
                   42285,
                   1,
                   fa0400.fa04codfact,
                   fa0400.ad01codasistenci,
                   1,
                   NULL,
                   fa16precio,
                   2,
                   fa16precio,
                   NULL,
                   NULL,
                   NULL,
                   0,
                   NULL,
                   NULL,
                   fa04fecfactura,
                   18,                                                --dpto realiz
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   fa16numlinea,
                   0,
                   NULL,
                   fa0400.ad07codproceso,
                   NULL,
                   1,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM fa0400,
                   fa1600
             WHERE fa0400.fa04codfact = cf
               AND fa1600.fa04codfact = fa0400.fa04codfact
               AND fa1600.fa14codnodoconc = 0
               AND UPPER (fa1600.fa16descrip) LIKE
                      '%COMIDAS%'
               AND fa0400.fa04cantfact > 0
               AND fa0400.fa04numfacreal IS NULL
               AND fa0400.fa04feccontabil IS NULL
               AND NOT EXISTS (SELECT fa05codcateg
                                 FROM fa0300
                                WHERE fa0300.fa04numfact = fa0400.fa04codfact
                                  AND fa0300.fa05codcateg = 42285);

         UPDATE FA0300
            SET AD02CODDPTO_R = 19
          WHERE FA05CODCATEG = 42286
            AND AD02CODDPTO_R <> 19
            AND FA04NUMFACT = CF;

         COMMIT;

   --   taxi aereo
         INSERT INTO fa0300
                     (
                        FA03NUMRNF,
                        FA05CODCATEG,
                        FA13CODESTRNF,
                        FA04NUMFACT,
                        AD01CODASISTENCI,
                        FA03CANTIDAD,
                        FA03NUMRNF_P,
                        FA03PRECIO,
                        FA03INDFACT,
                        FA03PRECIOFACT,
                        FA03OBSERV,
                        FA03FRANQUNI,
                        FA03FRANQSUMA,
                        FA03DESCUENTO,
                        FA03PERIODOGARA,
                        FA03PRECIODIA,
                        FA03FECHA,
                        AD02CODDPTO_R,
                        SG02COD_R,
                        AD02CODDPTO_S,
                        SG02COD_S,
                        CI22NUMHISTORIA,
                        FA16NUMLINEA,
                        FA03INDDESGLOSE,
                        FA15CODATRIB,
                        AD07CODPROCESO,
                        FA03INDREALIZADO,
                        FA03CANTFACT,
                        SG02COD_ADD,
                        FA03FECADD,
                        SG02COD_UPD,
                        FA03FECUPD,
                        FA03OBSIMP,
                        FA03INDPERMANENTE
                     )
            SELECT fa03numrnf_sequence.nextval,
                   10459,
                   1,
                   fa0400.fa04codfact,
                   fa0400.ad01codasistenci,
                   1,
                   NULL,
                   fa16precio,
                   2,
                   fa16precio,
                   NULL,
                   NULL,
                   NULL,
                   0,
                   NULL,
                   NULL,
                   fa04fecfactura,
                   6,                                                 --dpto realiz
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   fa16numlinea,
                   0,
                   NULL,
                   fa0400.ad07codproceso,
                   NULL,
                   1,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM fa0400,
                   fa1600
             WHERE fa0400.fa04codfact = cf
               AND fa1600.fa04codfact = fa0400.fa04codfact
               AND fa1600.fa14codnodoconc = 0
               AND UPPER (fa1600.fa16descrip) LIKE
                      '%TAXI A%'
               AND fa0400.fa04cantfact > 0
               AND fa0400.fa04numfacreal IS NULL
               AND fa0400.fa04feccontabil IS NULL
               AND NOT EXISTS (SELECT fa05codcateg
                                 FROM fa0300
                                WHERE fa0300.fa04numfact = fa0400.fa04codfact
                                  AND fa0300.fa05codcateg = 10459);

         COMMIT;
      END corregirtelef;

   /*****************************************************************************/
      FUNCTION Insertar_Asiento (

   /******************************************************************************
     FUNCTION      : Insertar_Asiento()
     SCOPE         : PRIVATE

     DESCRIPTION   : la funci�n extrae el n�mero de cuenta a utilizar para
                     insertar el apunte y luego inserta el apunte
                     utilizando los valores pasados como par�metros.
     PARAMETERS IN : los valores para extraer el n�mero de cuenta y los valores
                     a insertar en la tabla FA2900
     RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (SELECT or INSERT error)

   ******************************************************************************/
         p_investigacion        BOOLEAN,
         p_departamento_input   fa2900.ad02coddpto%TYPE,
         p_departamento_solic   fa2900.ad02coddpto%TYPE,
         p_importe_apunte       fa2900.fa29importe%TYPE,
         p_tipo_economico       ci3200.ci32codtipecon%TYPE,
         p_entidad              ci1300.ci13codentidad%TYPE,
         p_tipo_asistencia      ad1200.ad12codtipoasist%TYPE,
         p_numero_asiento       fa2900.fa29numasiento%TYPE,
         p_sufijo_concepto      fa2200.fa22concepto%TYPE,
         p_codigo_factura       fa2900.fa04codfact%TYPE,
         p_linea_factura        fa2900.fa16numlinea%TYPE,
         p_numero_rnf           fa2900.fa03numrnf%TYPE
      )
         RETURN BOOLEAN IS
         -- Para extraer el n�mero de cuenta es necesario un cursor,
         -- puesto que los parametros pueden extraer m�s de un registro,
         -- aunque nos interese solo el primero extraido (el m�s completo).
         CURSOR curcuenta (t VARCHAR2, e VARCHAR2, d NUMBER, a NUMBER, c VARCHAR2) IS
            SELECT COD_CUENTA,
                   IND_DPTO_APUNTE,
                   COD_DPTO_APU,
                   COD_TIP_ACTIVIDAD,
                   CONCEPTO,
                   FIL,
                   ENLACE
              FROM FA2201J
             WHERE INPTECO = T
               AND INPENTI = E
               AND INPDPTO = D
               AND INPTASI = A
               AND INPDEHA = C
             ORDER BY SRT;

         rec_curcuenta_debe            curcuenta%ROWTYPE;
         rec_curcuenta_haber           curcuenta%ROWTYPE;
         v_departamento_apunte_debe    fa2900.ad02coddpto%TYPE;
         v_departamento_apunte_haber   fa2900.ad02coddpto%TYPE;
      BEGIN
         -- Abre el cursor, extrae el primer registro y cierra el cursor
         OPEN curcuenta (
            p_tipo_economico, p_entidad, p_departamento_input, p_tipo_asistencia,
            'D'
         );
         FETCH curcuenta INTO rec_curcuenta_debe;
         CLOSE curcuenta;
         OPEN curcuenta (
            p_tipo_economico, p_entidad, p_departamento_input, p_tipo_asistencia,
            'H'
         );
         FETCH curcuenta INTO rec_curcuenta_haber;
         CLOSE curcuenta;
          -- Departamento del apunte :
          -- si FA2200.FA22INDDPTOAPUNTE = TRUE (= -1) --> se utiliza el
         -- DEPARTAMENTO del RNF
          -- si FA2200.FA22INDDPTOAPUNTE = FALSE (<> -1, incluso NULL)
         -- --> se utiliza FA2200.AD02CODDPTO_APU
         SELECT DECODE (
                   rec_curcuenta_debe.ind_dpto_apunte, -1, p_departamento_input,
                   rec_curcuenta_debe.cod_dpto_apu
                )
           INTO v_departamento_apunte_debe
           FROM dual;

         IF p_investigacion THEN
            v_departamento_apunte_debe := p_departamento_solic;
         END IF;

         SELECT DECODE (
                   rec_curcuenta_haber.ind_dpto_apunte, -1, p_departamento_input,
                   rec_curcuenta_haber.cod_dpto_apu
                )
           INTO v_departamento_apunte_haber
           FROM dual;

         IF rec_curcuenta_debe.enlace IS NULL THEN     -- No tiene cuenta de enlace
            -- Inserta el APUNTE DEBE
            INSERT INTO fa2900
                        (
                           fa29numasiento,
                           fa24codcuenta,
                           ad02coddpto,
                           fa23codtipactividad,
                           fa29importe,
                           fa29descripcion,
                           fa31codtipdebehaber,
                           fa04codfact,
                           fa16numlinea,
                           fa03numrnf,
                           fa29fil
                        )
                 VALUES (
                    p_numero_asiento,
                    rec_curcuenta_debe.cod_cuenta,
                    v_departamento_apunte_debe,
                    rec_curcuenta_debe.cod_tip_actividad,
                    (+1) * p_importe_apunte,
                    rec_curcuenta_debe.concepto || ' ' || p_sufijo_concepto,
                    'D',
                    p_codigo_factura,
                    p_linea_factura,
                    p_numero_rnf,
                    rec_curcuenta_debe.fil
                 );

            -- Inserta el APUNTE HABER
            INSERT INTO fa2900
                        (
                           fa29numasiento,
                           fa24codcuenta,
                           ad02coddpto,
                           fa23codtipactividad,
                           fa29importe,
                           fa29descripcion,
                           fa31codtipdebehaber,
                           fa04codfact,
                           fa16numlinea,
                           fa03numrnf,
                           fa29fil
                        )
                 VALUES (
                    p_numero_asiento,
                    rec_curcuenta_haber.cod_cuenta,
                    v_departamento_apunte_haber,
                    rec_curcuenta_haber.cod_tip_actividad,
                    (-1) * p_importe_apunte,
                    rec_curcuenta_haber.concepto || ' ' || p_sufijo_concepto,
                    'H',
                    p_codigo_factura,
                    p_linea_factura,
                    p_numero_rnf,
                    rec_curcuenta_haber.fil
                 );
         ELSE                                           --Si tiene cuenta de Enlace
            -- Inserta el APUNTE DEBE
            INSERT INTO fa2900
                        (
                           fa29numasiento,
                           fa24codcuenta,
                           ad02coddpto,
                           fa23codtipactividad,
                           fa29importe,
                           fa29descripcion,
                           fa31codtipdebehaber,
                           fa04codfact,
                           fa16numlinea,
                           fa03numrnf,
                           fa29fil
                        )
                 VALUES (
                    p_numero_asiento,
                    rec_curcuenta_debe.cod_cuenta,
                    v_departamento_apunte_debe,
                    rec_curcuenta_debe.cod_tip_actividad,
                    (+1) * p_importe_apunte,
                    rec_curcuenta_debe.concepto || ' ' || p_sufijo_concepto,
                    'D',
                    p_codigo_factura,
                    p_linea_factura,
                    p_numero_rnf,
                    rec_curcuenta_debe.fil
                 );

            INSERT INTO fa2900
                        (
                           fa29numasiento,
                           fa24codcuenta,
                           ad02coddpto,
                           fa23codtipactividad,
                           fa29importe,
                           fa29descripcion,
                           fa31codtipdebehaber,
                           fa04codfact,
                           fa16numlinea,
                           fa03numrnf,
                           fa29fil
                        )
                 VALUES (
                    p_numero_asiento,
                    rec_curcuenta_debe.enlace,
                    NULL,
                    rec_curcuenta_debe.cod_tip_actividad,
                    (-1) * p_importe_apunte,
                    rec_curcuenta_debe.concepto || ' ' || p_sufijo_concepto,
                    'H',
                    p_codigo_factura,
                    p_linea_factura,
                    p_numero_rnf,
                    rec_curcuenta_debe.fil
                 );

            -- Inserta el APUNTE HABER
            INSERT INTO fa2900
                        (
                           fa29numasiento,
                           fa24codcuenta,
                           ad02coddpto,
                           fa23codtipactividad,
                           fa29importe,
                           fa29descripcion,
                           fa31codtipdebehaber,
                           fa04codfact,
                           fa16numlinea,
                           fa03numrnf,
                           fa29fil
                        )
                 VALUES (
                    p_numero_asiento,
                    rec_curcuenta_haber.cod_cuenta,
                    v_departamento_apunte_haber,
                    rec_curcuenta_haber.cod_tip_actividad,
                    (-1) * p_importe_apunte,
                    rec_curcuenta_haber.concepto || ' ' || p_sufijo_concepto,
                    'H',
                    p_codigo_factura,
                    p_linea_factura,
                    p_numero_rnf,
                    rec_curcuenta_haber.fil
                 );

            INSERT INTO fa2900
                        (
                           fa29numasiento,
                           fa24codcuenta,
                           ad02coddpto,
                           fa23codtipactividad,
                           fa29importe,
                           fa29descripcion,
                           fa31codtipdebehaber,
                           fa04codfact,
                           fa16numlinea,
                           fa03numrnf,
                           fa29fil
                        )
                 VALUES (
                    p_numero_asiento,
                    rec_curcuenta_haber.enlace,
                    NULL,
                    rec_curcuenta_haber.cod_tip_actividad,
                    (+1) * p_importe_apunte,
                    rec_curcuenta_haber.concepto || ' ' || p_sufijo_concepto,
                    'D',
                    p_codigo_factura,
                    p_linea_factura,
                    p_numero_rnf,
                    rec_curcuenta_haber.fil
                 );
         END IF;

         RETURN TRUE;
      END insertar_asiento;

   /*****************************************************************************/
      PROCEDURE Write_To_Log (

   /******************************************************************************
   PROCEDURE     : Write_To_Log()
   SCOPE         : PRIVATE

   DESCRIPTION   : inserta un registro en la tabla de log. Si el par�metro p_comm
              it_log es TRUE hace tambi�n un COMMIT
                  (es necesario cuando el registro es generado por un suceso a n
              ivel de factura)
   PARAMETERS IN : p_numero_asiento, p_mensaje, p_tipo_suceso, p_cod_factura, p
                 _numero_linea, p_numero_RNF,p_commit_log (TRUE = si hay que ha
              cer COMMIT)

   ******************************************************************************/
         p_numero_asiento   fa4500.fa45numoperacion%TYPE,
         p_mensaje          fa4500.fa45dessuceso%TYPE,
         p_tipo_suceso      fa4500.fa45tiposuceso%TYPE,
         p_cod_factura      fa4500.fa04codfact%TYPE,
         p_numero_linea     fa4500.fa16numlinea%TYPE,
         p_numero_rnf       fa4500.fa03numrnf%TYPE,
         p_commit_log       BOOLEAN
      ) IS
   -- Cualquier error en el procedimiento se propaga al procedimiento llamante.
      BEGIN
         -- INSERT en la tabla de log
         INSERT INTO fa4500
                     (
                        fa45codregistro,
                        fa45numoperacion,
                        fa45fecregistro,
                        fa04codfact,
                        fa16numlinea,
                        fa03numrnf,
                        fa45tiposuceso,
                        fa45dessuceso
                     )
              VALUES (
                 fa45codregistro_sequence.nextval,
                 p_numero_asiento,
                 SYSDATE,
                 p_cod_factura,
                 p_numero_linea,
                 p_numero_rnf,
                 p_tipo_suceso,
                 p_mensaje
              );

         -- Dependiendo del valor del par�metro correspondiente, COMMIT
         IF p_commit_log THEN
            COMMIT;
         END IF;
      END write_to_log;

   /*****************************************************************************/
      FUNCTION TASI_IBM (cf NUMBER)
         RETURN NUMBER IS
         tasi   NUMBER;
      BEGIN
         SELECT ftasi
           INTO tasi
           FROM regfact
          WHERE ffact = cf;
         RETURN tasi;
      END TASI_IBM;

   /*****************************************************************************/
      FUNCTION TASI_Estancias (cf NUMBER)
         RETURN NUMBER IS
         NumEstancias   NUMBER;
         tasi           NUMBER;
      BEGIN
   -- ----------------------------------------------------------------------------
   -- No me fio de la AD2500. Si ha tenido estancias es hospitalizado, si no, ambula.
   -- ----------------------------------------------------------------------------
         SELECT SUM (fa03cantfact)
           INTO NumEstancias
           FROM fa0300,
                fa0500
          WHERE fa0300.fa05codcateg = fa0500.fa05codcateg
            AND fa0500.fa08codgrupo = 5             -- Estancias de cualquier clase
            AND fa0300.fa04numfact IN       -- Esto por si estan en otra asistencia
                                     (SELECT fa04codfact
                                        FROM fa0400
                                       WHERE fa04numfact IN (SELECT FA04NUMFACT
                                                               FROM FA0400
                                                              WHERE FA04CODFACT =
                                                                                 CF)
                                         AND fa0400.fa04numfacreal IS NULL);

         IF NumEstancias > 0 THEN
            tasi := 1;
         ELSE
            tasi := 2;
         END IF;

         RETURN tasi;
      END TASI_Estancias;

   /*****************************************************************************/
      PROCEDURE RepartirFacturaIBM (
   /******************************************************************************
   PROCEDURE     : FAPR02() SOLO DICIEMBRE
   SCOPE         : PUBLIC

   DESCRIPTION   : Reparte Diciembre segunRegFact. ver comentarios en el c�digo
   PARAMETERS IN : ver comentarios en el c�digo

   ******************************************************************************/
                                    cf NUMBER, ad IN OUT tt_acum_dpto) IS
         bstoprep   BOOLEAN;
         regf       RegFact%ROWTYPE;
      BEGIN
         SELECT *
           INTO regf
           FROM RegFact
          WHERE ffact = cf;
         bstoprep := FALSE;

         IF     regf.fserv <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv, 1, 3))) := TO_NUMBER (regf.fimps);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_02 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_02, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_02);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_03 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_03, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_03);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_04 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_04, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_04);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_05 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_05, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_05);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_06 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_06, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_06);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_07 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_07, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_07);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_08 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_08, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_08);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_09 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_09, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_09);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_10 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_10, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_10);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_11 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_11, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_11);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_12 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_12, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_12);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_13 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_13, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_13);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_14 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_14, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_14);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_15 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_15, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_15);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_16 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_16, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_16);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_17 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_17, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_17);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_18 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_18, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_18);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_19 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_19, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_19);
         ELSE
            bstoprep := TRUE;
         END IF;

         IF     regf.fserv_20 <> 'FFFF'
            AND NOT bstoprep THEN
            ad (TO_NUMBER (SUBSTR (regf.fserv_20, 1, 3))) :=
                                                         TO_NUMBER (regf.fimps_20);
         END IF;
      END RepartirFacturaIBM;

   /*****************************************************************************/
      PROCEDURE RepartirFacturaPrecio (
   /*****************************************************************************
   Funcion RepartirCoste: Reparte coste + 5% los movimientos de la factura cf
   ******************************************************************************/
                                       cf NUMBER, ad IN OUT tt_acum_dpto) IS
         -- cursor para extraer los RNFs asociados a una linea factura (par�metros
         -- <cod_fact> )
         CURSOR currnf (cod_fact fa0300.fa04numfact%TYPE) IS
            SELECT fa03numrnf,
                   ad02coddpto_r,
                   fa0300.fa05codcateg codcateg,
                   fa03preciofact,
                   fa03cantfact,
                   fa16numlinea
              FROM fa0300
             WHERE fa04numfact = cod_fact;

         Importe   NUMBER;
      BEGIN
         FOR rec_currnf IN currnf (cf) LOOP
            BEGIN                     -- Inicio del bloque de elaboraci�n de un RNF
               Importe :=
                 PrecioRNF (
                    rec_currnf.fa03numrnf, rec_currnf.ad02coddpto_r,
                    rec_currnf.codcateg, rec_currnf.fa03preciofact, NULL,
                    rec_currnf.fa03cantfact, cf, rec_currnf.fa16numlinea
                 );

               IF    rec_currnf.ad02coddpto_r IS NULL
                  OR rec_currnf.ad02coddpto_r = 0 THEN
                  rec_currnf.ad02coddpto_r := dep_dif;  -- Si no tiene dpto el mov.
               END IF;

               -- Acumulamos en su departamento
               ad (rec_currnf.ad02coddpto_r) :=
                                            ad (rec_currnf.ad02coddpto_r) + Importe;
            END;
         END LOOP;
      END RepartirFacturaPrecio;

   /*****************************************************************************/
      PROCEDURE RepartirFacturaCoste (
   /*****************************************************************************
   Funcion RepartirCoste: Reparte coste + 5% los movimientos de la factura cf
   ******************************************************************************/
                                      cf NUMBER, ad IN OUT tt_acum_dpto) IS
         -- cursor para extraer los RNFs asociados a una linea factura (par�metros
         -- <cod_fact> )
         CURSOR currnf (cod_fact fa0300.fa04numfact%TYPE) IS
            SELECT fa03numrnf,
                   ad02coddpto_r,
                   fa0300.fa05codcateg codcateg,
                   NVL (fa35coste, fa03preciofact / 1.05) fa35coste,
                   fa03cantfact,
                   fa16numlinea
              FROM fa0300,
                   fa3500
             WHERE fa0300.fa05codcateg = fa3500.fa05codcateg (+)
               AND fa04numfact = cod_fact;

         Importe   NUMBER;
      BEGIN
         FOR rec_currnf IN currnf (cf) LOOP
            BEGIN                     -- Inicio del bloque de elaboraci�n de un RNF
               Importe :=
                 CosteRNF (
                    rec_currnf.fa03numrnf, rec_currnf.ad02coddpto_r,
                    rec_currnf.codcateg, NULL, rec_currnf.fa35coste,
                    rec_currnf.fa03cantfact, cf, rec_currnf.fa16numlinea
                 ) *
                    1.05;                                               -- Sumar 5%

               IF    rec_currnf.ad02coddpto_r IS NULL
                  OR rec_currnf.ad02coddpto_r = 0 THEN
                  rec_currnf.ad02coddpto_r := dep_dif;  -- Si no tiene dpto el mov.
               END IF;

               -- Acumulamos en su departamento
               ad (rec_currnf.ad02coddpto_r) :=
                                            ad (rec_currnf.ad02coddpto_r) + Importe;
            END;
         END LOOP;
      END RepartirFacturaCoste;

   /*****************************************************************************/
      PROCEDURE RepartirFacturaInvestigacion (

   /*****************************************************************************
   Funcion RepartirCoste: Reparte coste + 5% los movimientos de la factura cf
   ******************************************************************************/
         cf      NUMBER,
         tasi    NUMBER,
         numas   NUMBER,
         sufco   VARCHAR2
      ) IS
         -- cursor para extraer los RNFs asociados a una linea factura (par�metros
         -- <cod_fact> )
         CURSOR currnf (cod_fact fa0300.fa04numfact%TYPE) IS
            SELECT fa03numrnf,
                   ad02coddpto_r,
                   ad02coddpto_s,
                   fa0300.fa05codcateg codcateg,
                   fa03preciofact,
                   fa03cantfact,
                   fa16numlinea
              FROM fa0300
             WHERE fa04numfact = cod_fact;

         Importe   NUMBER;
         v_ret     BOOLEAN;
      BEGIN
         FOR rec_currnf IN currnf (cf) LOOP
            IF    rec_currnf.ad02coddpto_r IS NULL
               OR rec_currnf.ad02coddpto_r = 0 THEN
               rec_currnf.ad02coddpto_r := dep_dif;     -- Si no tiene dpto el mov.
            END IF;

            v_ret :=
              Insertar_Asiento (
                 TRUE, rec_currnf.ad02coddpto_r, rec_currnf.ad02coddpto_s,
                 rec_currnf.fa03preciofact * rec_currnf.fa03cantfact, 'H', 'H',
                 tasi, numas, sufco, cf, 1, rec_currnf.fa03numrnf
              );
         END LOOP;
      END RepartirFacturaInvestigacion;

   /*****************************************************************************/
      PROCEDURE RepartirFactura (
         cf            NUMBER,                                  --Codigo de factura
         fc            DATE,                             --Fecha de contabilizacion
         ad   IN OUT   tt_acum_dpto,                       --repartos departamentos
         na            NUMBER                          --numero de asiento contable
      ) IS
         ModoReparto        NUMBER;                      --1:prec,2:cost,3:no,4:ibm
         TieneMovimientos   BOOLEAN;
         DescLineas         BOOLEAN;                          --Descuadre en lineas
         DescRNF            BOOLEAN;                            -- Descuadre en RNF
         DescRNFDec         BOOLEAN;                          -- Debido a decimales
         Forfait            BOOLEAN;                                  -- Es forfait
         EsDeIBM            BOOLEAN;                            --Repartida por IBM
         NumEstancias       NUMBER;                           --Numero de estancias
         Repartido          NUMBER (15, 2);                       --Total Repartido
         CantFactu          NUMBER (15, 2);                         --Total Factura
         tasi               NUMBER;                               --Tipo Asistencia
         teco               VARCHAR2 (2);                              --Tipo Econ.
         enti               VARCHAR2 (2);                                --Entidad.
         iterDpt            NUMBER;
         v_return           BOOLEAN;
         sc                 VARCHAR2 (10);                        --sufijo concepto
      BEGIN
   -- ----------------------------------------------------------------------------
   -- Corregir RNF de telefonos, lavander�a y dietas acompa�ante
   -- Inserta movimientos si no los tuviera ya insertados.
   -- ----------------------------------------------------------------------------
         CorregirTelef (cf);
   -- ----------------------------------------------------------------------------
   -- Comprobaciones de lo bien que est� la factura:
   --  Si DescLineas=true => sum(fa16importe)<>fa04cantfact. Motivos:
   --      1.Se le ha puesto importe a la factura 'a mano'
   --      2.Decimales descuadrados entre lineas y cantfact
   --      En general, es un error: Repartir a coste+5% y resto a diferencias
   -- Si  DescRNF=true => sum(movimientos)<>fa04cantfact. Motivos:
   --      1.Decimales descuadrados entre movimientos y cantfact. Si el descuadre es
   --       inferior al numero de lineas expresado en pesetas, repartir precio. Si
   --        superior, repartir coste+5% (DescRNFDec=false)
   --      2.Es un Forfait de algun tipo (por estancias, quir�rgico,etc..)
   -- Tambi�n podemos encontrarnos una factura de importe '0' sin movimientos, lo
   -- que es perfectamente correcto pues se pueden facturar varias asistencias
   -- con una unica factura, y la asistencia ha de quedar facturada en la FA0400.
   -- ----------------------------------------------------------------------------
         CheckFact (
            cf, TieneMovimientos, DescLineas, DescRNF, DescRNFDec, Forfait,
            EsDeIBM
         );
   -- ----------------------------------------------------------------------------
   -- Seleccionamos Teco,Enti y cantidad de la factura
   -- ----------------------------------------------------------------------------
         SELECT ci32codtipecon, ci13codentidad, fa04cantfact
           INTO teco,
                enti,
                cantfactu
           FROM fa0400
          WHERE fa04codfact = cf;
         -- Sufijo de Facturacion (Ej: 'DIC-1999')
         SELECT TO_CHAR (fc, 'MON-YYYY')
           INTO sc
           FROM Dual;

   -- ----------------------------------------------------------------------------
   -- Ahora se reparte segun esos datos obtenidos
   -- ----------------------------------------------------------------------------
         IF teco = 'H' THEN                                         --Investigacion
            tasi := TASI_Estancias (cf);
            RepartirFacturaInvestigacion (cf, tasi, na, sc);
            ModoReparto := 1;
         ELSE
            IF EsDeIBM THEN
               RepartirFacturaIBM (cf, ad);
               tasi := TASI_IBM (cf);
               ModoReparto := 4;
            ELSE
               -- Se efect�a el reparto seg�n descuadres explicados
               IF     CantFactu = 0
                  AND NOT TieneMovimientos THEN
                  ModoReparto := 3;
               ELSE
                  IF DescLineas THEN
                     RepartirFacturaCoste (cf, ad);
                     tasi := TASI_Estancias (cf);
                     ModoReparto := 2;
                  ELSE
                     IF DescRNF THEN
                        IF DescRNFDec THEN
                           RepartirFacturaPrecio (cf, ad);
                           ModoReparto := 1;
                        ELSE
                           RepartirFacturaCoste (cf, ad);
                           ModoReparto := 2;
                        END IF;
                     ELSE
                        RepartirFacturaPrecio (cf, ad);
                        ModoReparto := 1;
                     END IF;
                  END IF;
               END IF;

               tasi := TASI_Estancias (cf);
            END IF;

   -- ----------------------------------------------------------------------------
   --  Ahora Insertar los acumulados en la FA2900
   -- ----------------------------------------------------------------------------
            iterDpt := ad.FIRST;
            Repartido := 0;

            WHILE iterDpt IS NOT NULL LOOP
               IF ad (iterDpt) <> 0 THEN
			      -- Asegurar que no hay importes decimales.
                  IF ad (iterDpt) < 0 THEN
                     ad (iterDpt) := CEIL (ad (iterDpt));
                  ELSE
                     ad (iterDpt) := FLOOR (ad (iterDpt));
                  END IF;

                  Repartido := Repartido + ad (iterDpt);
                  v_return :=
                    Insertar_Asiento (
                       FALSE, iterDpt, NULL, ad (iterDpt), teco, enti, tasi, na,
                       sc, cf, NULL, NULL
                    );
               END IF;

               iterDpt := ad.NEXT (iterDpt);               --Sigu. o null si ultimo
            END LOOP;

            -- Vemos si hay diferencias.(Solo si no es investigacion, claro)
            IF     Repartido <> CantFactu
               AND teco <> 'H' THEN
               v_return :=
                 Insertar_Asiento (
                    FALSE, dep_dif, NULL, CantFactu - Repartido, teco, enti, tasi,
                    na, sc, cf, NULL, NULL
                 );
            END IF;
         END IF;

         -- Por controlar, se marca el modo de repartir dicha factura.
         UPDATE FA0400
            SET FA04INDCONTABIL = ModoReparto,
                FA04FECCONTABIL = fc
          WHERE FA04CODFACT = cf;
      END RepartirFactura;

   /*****************************************************************************/
      PROCEDURE fapr02 (
         cf   NUMBER,                                           --Codigo de factura
         fc   DATE,                                     -- Fecha de contabilizacion
         na   NUMBER                                   --numero de asiento contable
      ) IS
         ad   tt_acum_dpto;                                --repartos departamentos

         -- cursor de inicializacion importes a departamentos
         CURSOR cur_acum_dpto IS
            SELECT DISTINCT ad02coddpto
              FROM AD0200;
      BEGIN
   -- ----------------------------------------------------------------------------
   -- Inicializaci�n de los importes acumulados por departamento
   -- ----------------------------------------------------------------------------
         FOR reg_acum_dpto IN cur_acum_dpto LOOP
            ad (reg_acum_dpto.ad02coddpto) := 0;
         END LOOP;

         RepartirFactura (cf, fc, ad, na);
         COMMIT;
      END fapr02;

   /*****************************************************************************/
      PROCEDURE fapr01 (

   /*******************************************************************************
     PROCEDURE     : FAPR01()
     SCOPE         : PUBLIC

     DESCRIPTION   : ver comentarios en el c�digo
     PARAMETERS IN : ver comentarios en el c�digo

   *******************************************************************************/
         FecContabil1       fa0400.fa04fecfactura%TYPE,         -- fecha de contab.
         p_numero_asiento   fa2900.fa29numasiento%TYPE,           -- n�mero asiento
         indtodas           NUMBER
      ) IS
         /*Facturas que se van a contabilizar*/
         CURSOR curf (fecha DATE, todas NUMBER) IS
            SELECT fa0400.fa04codfact
              FROM fa0400,
                   fa0424j
             WHERE fa0400.fa04codfact = fa0424j.fa04codfact
               AND (   fa0424j.fa03fecha = fecha
                    OR todas = -1)
               AND fa0400.fa04numfacreal IS NULL
               AND fa0400.fa04feccontabil IS NULL;

         -- cursor de inicializacion importes a departamentos
         CURSOR cur_acum_dpto IS
            SELECT DISTINCT ad02coddpto
              FROM AD0200;

         -- variables de tipo registro para la fetch de los cursores y para las
         -- funciones PRECIO y COSTE
         ad            tt_acum_dpto;                              --matriz repartos
         FecContabil   DATE;
         iterDpt       NUMBER;
      BEGIN
   -- ----------------------------------------------
   -- Escogemos ultimo dia del mes
   -- ----------------------------------------------
         SELECT LAST_DAY (TRUNC (FecContabil1))
           INTO FecContabil
           FROM dual;

   -- ----------------------------------------------
   -- Borra el contenido de la tabla de log
   -- Si falla --> EXCEPTION and ORACLE ERROR
   -- ----------------------------------------------
         IF NOT purge_log (p_numero_asiento) THEN
            raise_application_error (-20000, 'No puedo borrar el LOG de la FA4500');
         END IF;

   -- ----------------------------------------------------------------------------
   -- Inicializaci�n de los importes acumulados por departamento
   -- ----------------------------------------------------------------------------
         FOR reg_acum_dpto IN cur_acum_dpto LOOP
            ad (reg_acum_dpto.ad02coddpto) := 0;
         END LOOP;

   -- ----------------------------------------------------------------------------
   -- MAIN LOOP ("FACTURAS")
   -- ----------------------------------------------------------------------------
         FOR rec_curf IN curf (FecContabil, indtodas) LOOP
            --Contadores a '0' pesetas
            iterDpt := ad.FIRST;

            WHILE iterDpt IS NOT NULL LOOP
               ad (iterDpt) := 0;                               -- Re-inicializamos
               iterDpt := ad.NEXT (iterDpt);               --Sigu. o null si ultimo
            END LOOP;

            RepartirFactura (
               rec_curf.fa04codfact, FecContabil, ad, p_numero_asiento
            );
            COMMIT;
         END LOOP;                                      -- END MAIN LOOP (FACTURAS)

         COMMIT;
      END fapr01;
   END fapk01;
/

