   FUNCTION fafn07 (c_fact NUMBER, n_lin NUMBER)
   /*Funci�n que devuelve la cantidad de una linea de factura */
      RETURN NUMBER IS
      n_cant   NUMBER;
   BEGIN
      SELECT NVL (SUM (fa03cantidad), 0)
        INTO n_cant
        FROM fa0300
       WHERE fa04numfact = c_fact
         AND fa16numlinea = n_lin;
      RETURN (ROUND (n_cant, 0));
   END fafn07;
