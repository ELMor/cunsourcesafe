   PROCEDURE copycat (
      nom       VARCHAR2,
      pr        NUMBER,
      pa        NUMBER,
      cat       NUMBER,
      codorig   VARCHAR2
   ) IS
      /*
       Usado para dar de alta una categoria final que se parece a otra (CAT) con un nuevo
       nombre (NOM) y el precio relativo (tanto por uno) sobre la categoria CAT en cada concierto
       o precio absoluto (si pa=1)
      */
      r5    fa0500%ROWTYPE;
      r6    fa0600%ROWTYPE;
      r15   fa1500%ROWTYPE;
      aux   VARCHAR2 (2000);
      pos   NUMBER;

      CURSOR c1 (c NUMBER) IS
         SELECT *
           FROM fa1500
          WHERE fa15codconc IN (SELECT fa09codnodoconc
                                  FROM fa0900
                                 WHERE fa09indconcierto = -1)
            AND fa15escat = 1
            AND fa15codfin = c;
   BEGIN
      SELECT *
        INTO r6
        FROM fa0600
       WHERE fa05codcateg = cat;

      IF SQL%NOTFOUND THEN
         raise_application_error (-20010, 'Categoria no encontrada');
      END IF;

      SELECT *
        INTO r5
        FROM fa0500
       WHERE fa05codcateg = cat;

      IF SQL%NOTFOUND THEN
         raise_application_error (-20010, 'Categoria no encontrada');
      END IF;

      SELECT fa05codcateg_sequence.nextval
        INTO r5.fa05codcateg
        FROM dual;

      IF    nom IS NOT NULL
         OR nom <> '' THEN
         r5.fa05desig               := nom;
      END IF;

      r5.fa05codorigen           := TO_NUMBER (codorig);

      IF LENGTH (codorig) = 6 THEN
         r5.fa05codorigc1           := TO_NUMBER (SUBSTR (codorig, 1, 3));
         r5.fa05codorigc2           := TO_NUMBER (SUBSTR (codorig, 4, 1));
         r5.fa05codorigc3           := TO_NUMBER (SUBSTR (codorig, 5, 1));
         r5.fa05codorigc4           := TO_NUMBER (SUBSTR (codorig, 6, 1));
      END IF;

      SELECT fa06codramacat_sequence.nextval
        INTO r6.fa06codramacat
        FROM dual;
      r6.fa05codcateg            := r5.fa05codcateg;
      r6.fa15ruta                :=
                 REPLACE (r6.fa15ruta, '.' || cat || '.', '.' || r5.fa05codcateg ||
                                                             '.');

      INSERT INTO fa0500
                  (
                     fa05codcateg,
                     fa05desig,
                     fa08codgrupo,
                     fa05codorigen,
                     fa05fecinicio,
                     fa05fecfin,
                     fa05codcateg_p,
                     fa05codorigc1,
                     fa05codorigc2,
                     fa05codorigc3,
                     fa05codorigc4,
                     fa05precioref
                  )
           VALUES (
              r5.fa05codcateg,
              r5.fa05desig,
              r5.fa08codgrupo,
              r5.fa05codorigen,
              r5.fa05fecinicio,
              r5.fa05fecfin,
              r5.fa05codcateg_p,
              r5.fa05codorigc1,
              r5.fa05codorigc2,
              r5.fa05codorigc3,
              r5.fa05codorigc4,
              r5.fa05precioref
           );

      INSERT INTO fa0600
                  (
                     fa06codramacat,
                     fa05codcateg,
                     fa05codcateg_p,
                     fa06fecinicio,
                     fa06fecfin,
                     fa15ruta
                  )
           VALUES (
              r6.fa06codramacat,
              r6.fa05codcateg,
              r6.fa05codcateg_p,
              r6.fa06fecinicio,
              r6.fa06fecfin,
              r6.fa15ruta
           );

      FOR rc1 IN c1 (cat) LOOP
         r15                        := rc1;
         SELECT fa15codatrib_sequence.nextval
           INTO r15.fa15codatrib
           FROM dual;
         pos                        := INSTR (r15.fa15ruta, '/');
         aux                        := SUBSTR (r15.fa15ruta, pos);
         aux                        :=
                         REPLACE (aux, '.' || cat || '.', '.' || r5.fa05codcateg ||
                                                             '.');
         r15.fa15ruta               :=
                           REPLACE (r15.fa15ruta, SUBSTR (r15.fa15ruta, pos), aux);

         IF pr IS NOT NULL THEN
            IF pa <> 1 THEN
               r15.fa15precioref          := r15.fa15precioref * pr;
            ELSE
               r15.fa15precioref          := pr;
            END IF;
         END IF;

         INSERT INTO fa1500
                     (
                        fa15codatrib,
                        fa15ruta,
                        fa15fecinicio,
                        fa15fecfin,
                        fa15precioref,
                        fa15franquni,
                        fa15franqsuma,
                        fa15periodogara,
                        fa15indnecesario,
                        fa15indsuficiente,
                        fa15indexcluido,
                        fa15indopcional,
                        fa15inddescont,
                        fa15indlinoblig,
                        fa15indfactoblig,
                        fa15descuento,
                        fa15preciodia,
                        ci13codentidad,
                        fa15indfranquni,
                        fa15indfranqsuma,
                        fa15codconc,
                        fa15codfin,
                        fa15escat,
                        fa15inddesglose,
                        fa15indsuplemento,
                        fa15descripcion,
                        fa15rutarel,
                        fa15relfijo,
                        fa15relpor,
                        fa15ordimp,
                        fa15indtramacum,
                        fa15indtramdias,
                        fa15plazointer,
                        fa15indnecrnforigen,
                        fa15indnecrnfdestino,
                        fa15indrecogercant,
                        fa15indrecogervalor,
                        ci32codtipecon
                     )
              VALUES (
                 r15.fa15codatrib,
                 r15.fa15ruta,
                 r15.fa15fecinicio,
                 r15.fa15fecfin,
                 r15.fa15precioref,
                 r15.fa15franquni,
                 r15.fa15franqsuma,
                 r15.fa15periodogara,
                 r15.fa15indnecesario,
                 r15.fa15indsuficiente,
                 r15.fa15indexcluido,
                 r15.fa15indopcional,
                 r15.fa15inddescont,
                 r15.fa15indlinoblig,
                 r15.fa15indfactoblig,
                 r15.fa15descuento,
                 r15.fa15preciodia,
                 r15.ci13codentidad,
                 r15.fa15indfranquni,
                 r15.fa15indfranqsuma,
                 r15.fa15codconc,
                 r15.fa15codfin,
                 r15.fa15escat,
                 r15.fa15inddesglose,
                 r15.fa15indsuplemento,
                 r15.fa15descripcion,
                 r15.fa15rutarel,
                 r15.fa15relfijo,
                 r15.fa15relpor,
                 r15.fa15ordimp,
                 r15.fa15indtramacum,
                 r15.fa15indtramdias,
                 r15.fa15plazointer,
                 r15.fa15indnecrnforigen,
                 r15.fa15indnecrnfdestino,
                 r15.fa15indrecogercant,
                 r15.fa15indrecogervalor,
                 r15.ci32codtipecon
              );
      END LOOP;
   END;
