   FUNCTION fafn05 (c_asist NUMBER, c_proc NUMBER)
   /*Funci�n que devuelve la descripci�n del diagnostico principal de un p-a */
      RETURN VARCHAR2 IS
      str_rep   VARCHAR2 (2000);
   BEGIN
      SELECT MAX (ad3900.ad39descodigo)
        INTO str_rep
        FROM ad3900,
             ad4000
       WHERE ad3900.ad39codigo = ad4000.ad39codigo
         AND ad4000.ad01codasistenci = c_asist
         AND ad4000.ad07codproceso = c_proc
         AND ad4000.ad40tipodiagproc = 'P';
      RETURN (str_rep);
   END fafn05;
