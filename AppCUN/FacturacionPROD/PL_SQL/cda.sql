
   PROCEDURE cda (dskacun NUMBER) IS
      CURSOR f (nd NUMBER) IS
         SELECT fa04numfact
           FROM fa4900
          WHERE fa48coddiskacunsa = nd;

      stot   NUMBER;
      sdep   NUMBER;
      smov   NUMBER;
      ok     NUMBER;
   BEGIN
      FOR rf IN f (dskacun) LOOP
         SELECT SUM (importe)
           INTO stot
           FROM fa0408j
          WHERE fa04numfact = rf.fa04numfact;
         SELECT SUM (importe)
           INTO sdep
           FROM fa0407j
          WHERE fa04numfact = rf.fa04numfact;
         SELECT SUM (importe)
           INTO smov
           FROM fa0411j
          WHERE fa04numfact = rf.fa04numfact;

         INSERT INTO fatmp01
              VALUES (rf.fa04numfact, stot, sdep, smov);

         COMMIT;
      END LOOP;
   END;
