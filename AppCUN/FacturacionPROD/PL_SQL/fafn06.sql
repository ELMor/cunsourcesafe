



   FUNCTION fafn06 (
      c_asist   NUMBER,
      c_proc    NUMBER,
      t_asist   NUMBER,
      t_caso    NUMBER,
      n_pos     NUMBER,
      f_proc    VARCHAR2
   )
   /*Funci�n que devuelve la cabecera (hospitalizado y ambulatorio) de la factura de Osasunbidea con formato  */
      RETURN VARCHAR2 IS
      str_nss              VARCHAR2 (10);
      str_apel1            VARCHAR2 (18);
      str_apel2            VARCHAR2 (18);
      str_nomb             VARCHAR2 (14);
      str_citecip          VARCHAR2 (23);
      str_fecha_nac        VARCHAR2 (7);
      str_sexo             VARCHAR2 (1);
      str_cip              VARCHAR2 (5);
      str_depto            VARCHAR2 (3);
      str_inf              VARCHAR2 (12);
      str_tipo_consulta    VARCHAR2 (1);
      str_fecha_ingreso    VARCHAR2 (8);
      str_hora_ingreso     VARCHAR2 (9);
      str_hora_alta        VARCHAR2 (9);
      str_fecha_consulta   VARCHAR2 (7);
      str_circuns_ing      VARCHAR2 (1);
      str_circuns_alt      VARCHAR2 (1);
      str_rep              VARCHAR2 (200);
      hay_datos            CHAR (2);
   BEGIN
      SELECT DECODE (COUNT (*), 1, 'SI', 'NO')
        INTO hay_datos
        FROM fa2500
       WHERE ad01codasistenci = c_asist
         AND ad07codproceso = c_proc
         AND fa25valid = -1;

      IF (hay_datos = 'SI') THEN
         SELECT fa25numsegsoc,
                fa25apel1,
                fa25apel2,
                fa25nomb,
                fa25idcliente,
                TO_CHAR (fa25fechnac, 'DDMMYYY'),
                fa25sexo,
                fa25codpostal,
                TO_CHAR (ad02coddpto),
                NVL (LPAD (fa25expinf, 12, '0'), '            ')
           INTO str_nss,
                str_apel1,
                str_apel2,
                str_nomb,
                str_citecip,
                str_fecha_nac,
                str_sexo,
                str_cip,
                str_depto,
                str_inf
           FROM fa2500
          WHERE ad01codasistenci = c_asist
            AND ad07codproceso = c_proc
            AND fa25valid = -1;
      ELSE
         SELECT SUBSTR (ci22numsegsoc, 1, 10),
                SUBSTR (ci22priapel, 1, 18),
                SUBSTR (ci22segapel, 1, 18),
                SUBSTR (ci22nombre, 1, 14),
                TO_CHAR (ci22fecnacim, 'DDMMYYY'),
                TO_CHAR (ci30codsexo)
           INTO str_nss,
                str_apel1,
                str_apel2,
                str_nomb,
                str_fecha_nac,
                str_sexo
           FROM ad0700,
                ci2200
          WHERE ad0700.ci21codpersona = ci2200.ci21codpersona
            AND ad0700.ad07codproceso = c_proc;
         SELECT MAX (TO_CHAR (ad02coddpto))
           INTO str_depto
           FROM ad0500
          WHERE ad01codasistenci = c_asist
            AND ad07codproceso = c_proc
            AND ad05fecfinrespon IS NULL;
      END IF;

      SELECT MIN (TO_CHAR (ad2500.ad25fecinicio, 'YYYYMMDD')),
             MIN (TO_CHAR (ad2500.ad25fecinicio, 'HHDDMMYYY')),
             MIN (TO_CHAR (ad0100.ad01fecfin, 'HHDDMMYYY')),
             MIN (TO_CHAR (ad2500.ad25fecinicio, 'DDMMYYY')),
             MIN (DECODE (ad0100.ad01indurgente, -1, '1', '2')),
             MIN (DECODE (
                     ad0100.ad27codaltaasist, '1', '1', '2', '1', '3', '1', '4',
                     '3', '5', '2', '6', '4', '7', '1', '1'
                  ))
        INTO str_fecha_ingreso,
             str_hora_ingreso,
             str_hora_alta,
             str_fecha_consulta,
             str_circuns_ing,
             str_circuns_alt
        FROM ad0100,
             ad2500
       WHERE ad0100.ad01codasistenci = c_asist
         AND ad2500.ad01codasistenci = c_asist
         AND ad2500.ad12codtipoasist = t_asist;
      str_tipo_consulta          :=
                                  RPAD (NVL (fafn12 (c_asist, c_proc), ' '), 1, ' ');

      IF (t_asist = 1) THEN
         str_rep                    :=
           RPAD (NVL (str_nss, ' '), 10, ' ') ||
              RPAD (NVL (str_fecha_ingreso, ' '), 8, ' ') ||
              'HO' ||
              RPAD (NVL (str_apel1, ' '), 18, ' ') ||
              RPAD (NVL (str_apel2, ' '), 18, ' ');
         str_rep                    :=
           str_rep || RPAD (NVL (str_nomb, ' '), 14, ' ') ||
              RPAD (NVL (str_citecip, ' '), 23, ' ') ||
              RPAD (NVL (str_fecha_nac, ' '), 7, ' ');
         str_rep                    :=
           str_rep || RPAD (NVL (str_sexo, ' '), 1, ' ') ||
              RPAD (NVL (str_cip, ' '), 5, ' ') ||
              RPAD (NVL (str_hora_ingreso, ' '), 9, ' ');
         str_rep                    :=
           str_rep || str_circuns_ing || RPAD (NVL (str_hora_alta, ' '), 9, ' ') ||
              str_circuns_alt ||
              RPAD (NVL (str_depto, ' '), 3, ' ');
         str_rep                    :=
           str_rep || LPAD (TO_CHAR (NVL (t_caso, 0)), 10, ' ') ||
              LPAD (TO_CHAR (n_pos), 4, '0') ||
              f_proc ||
              RPAD (NVL (str_inf, ' '), 12, ' ');
      ELSIF (t_asist = 2) THEN
         str_rep                    :=
           RPAD (NVL (str_nss, ' '), 10, ' ') ||
              RPAD (NVL (str_fecha_ingreso, ' '), 8, ' ') ||
              'AM' ||
              RPAD (NVL (str_apel1, ' '), 18, ' ') ||
              RPAD (NVL (str_apel2, ' '), 18, ' ');
         str_rep                    :=
           str_rep || RPAD (NVL (str_nomb, ' '), 14, ' ') ||
              RPAD (NVL (str_citecip, ' '), 23, ' ') ||
              RPAD (NVL (str_fecha_nac, ' '), 7, ' ');
         str_rep                    :=
           str_rep || RPAD (NVL (str_sexo, ' '), 1, ' ') ||
              RPAD (NVL (str_cip, ' '), 5, ' ') ||
              RPAD (NVL (str_fecha_consulta, ' '), 7, ' ');
         str_rep                    :=
                 str_rep || str_tipo_consulta || RPAD (NVL (str_depto, ' '), 3, ' ');
         str_rep                    :=
           str_rep || LPAD (TO_CHAR (NVL (t_caso, 0)), 10, ' ') ||
              LPAD (TO_CHAR (n_pos), 4, '0') ||
              f_proc ||
              RPAD (NVL (str_inf, ' '), 12, ' ');
      END IF;

      RETURN (str_rep);
   END fafn06;
