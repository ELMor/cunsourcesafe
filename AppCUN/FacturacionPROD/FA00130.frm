VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "Comdlg32.ocx"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frm_Cobros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gesti�n de cobros y compensaciones de facturas"
   ClientHeight    =   7740
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   11295
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7740
   ScaleWidth      =   11295
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdAnotaciones 
      Caption         =   "   Anotac.       Cartas "
      Height          =   480
      Left            =   10215
      TabIndex        =   43
      Top             =   3420
      Width           =   915
   End
   Begin VB.CommandButton cmdAbonoAbono 
      Caption         =   "&Abono con      Abono"
      Height          =   465
      Left            =   10125
      TabIndex        =   41
      Top             =   4410
      Width           =   1005
   End
   Begin VB.CommandButton cmdAcuse 
      Caption         =   "&Acuse de Recibo Pago"
      Height          =   375
      Left            =   9225
      TabIndex        =   39
      Top             =   6570
      Width           =   1905
   End
   Begin VB.Frame frmPagos 
      Caption         =   " Pagos "
      Enabled         =   0   'False
      Height          =   3570
      Left            =   5175
      TabIndex        =   15
      Top             =   3870
      Width           =   3840
      Begin VB.CommandButton cmdFactComp 
         Caption         =   "Facturas"
         Height          =   240
         Left            =   900
         TabIndex        =   40
         Top             =   3060
         Width           =   735
      End
      Begin VB.TextBox txtTotPdtePagos 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2610
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   3060
         Width           =   915
      End
      Begin VB.TextBox txtTotPagos 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1710
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   3060
         Width           =   915
      End
      Begin VB.OptionButton optPagos 
         Caption         =   "Compensaciones"
         Height          =   240
         Index           =   1
         Left            =   45
         TabIndex        =   8
         Top             =   3285
         Width           =   1635
      End
      Begin VB.OptionButton optPagos 
         Caption         =   "Pagos"
         Height          =   240
         Index           =   0
         Left            =   45
         TabIndex        =   7
         Top             =   3060
         Value           =   -1  'True
         Width           =   780
      End
      Begin SSDataWidgets_B.SSDBGrid grdPagos 
         Bindings        =   "FA00130.frx":0000
         Height          =   2805
         Left            =   135
         TabIndex        =   6
         Top             =   225
         Width           =   3660
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   5
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2037
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "Fecha"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   7
         Columns(0).FieldLen=   256
         Columns(1).Width=   1693
         Columns(1).Caption=   "Cantidad"
         Columns(1).Name =   "Cantidad"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Numero"
         Columns(2).Name =   "Numero"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "Persona"
         Columns(3).Name =   "Persona"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1693
         Columns(4).Caption=   "Pendiente"
         Columns(4).Name =   "Pendiente"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   6456
         _ExtentY        =   4948
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdRecoPago 
      Caption         =   "    R&ECO        Pago"
      Height          =   465
      Left            =   10215
      TabIndex        =   38
      Top             =   6975
      Width           =   915
   End
   Begin VB.Frame frmFacturas 
      Caption         =   " Facturas "
      Enabled         =   0   'False
      Height          =   3570
      Left            =   0
      TabIndex        =   14
      Top             =   3870
      Width           =   5100
      Begin VB.CommandButton cmdCompNegat 
         Caption         =   "Comp. Neg."
         Height          =   285
         Left            =   2250
         TabIndex        =   42
         Top             =   3195
         Width           =   1005
      End
      Begin VB.TextBox txtTotFacturas 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3285
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   3195
         Width           =   1140
      End
      Begin VB.CheckBox chkTodas 
         Caption         =   "Mostrar todas las facturas"
         Height          =   195
         Left            =   90
         TabIndex        =   5
         Top             =   3240
         Width           =   2220
      End
      Begin SSDataWidgets_B.SSDBGrid grdFacturas 
         Height          =   2940
         Left            =   90
         TabIndex        =   4
         Top             =   225
         Width           =   4920
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   7
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   3096
         Columns(0).Caption=   "NoFactura"
         Columns(0).Name =   "NoFactura"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   2037
         Columns(1).Caption=   "Fecha"
         Columns(1).Name =   "Fecha"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1958
         Columns(2).Caption=   "Importe"
         Columns(2).Name =   "Importe"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   609
         Columns(3).Caption=   "Pag."
         Columns(3).Name =   "Seleccion"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   2
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "indCompensa"
         Columns(4).Name =   "indCompensa"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "CodFact"
         Columns(5).Name =   "CodFact"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "CantidadFact"
         Columns(6).Name =   "CantidadFact"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   8678
         _ExtentY        =   5186
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdRecoFactura 
      Caption         =   " R&ECO Factura"
      Height          =   465
      Left            =   9225
      TabIndex        =   36
      Top             =   6975
      Width           =   960
   End
   Begin VB.CommandButton cmdCtaCte 
      Caption         =   "   Cuenta     Corriente"
      Height          =   480
      Left            =   9180
      TabIndex        =   33
      Top             =   3420
      Width           =   915
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   240
      Left            =   9135
      TabIndex        =   29
      Top             =   3690
      Visible         =   0   'False
      Width           =   285
      _Version        =   196608
      _ExtentX        =   503
      _ExtentY        =   423
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
   Begin VB.CommandButton cmdReimprimirAbono 
      Caption         =   "&Reimprimir Abono"
      Height          =   375
      Left            =   9225
      TabIndex        =   28
      Top             =   6165
      Width           =   1905
   End
   Begin VB.Frame Frame1 
      Caption         =   " Saldo "
      Height          =   735
      Left            =   9135
      TabIndex        =   26
      Top             =   2610
      Width           =   2130
      Begin MSComDlg.CommonDialog cmdDlg1 
         Left            =   1350
         Top             =   405
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   327681
      End
      Begin VB.Label lblSaldo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         Left            =   135
         TabIndex        =   27
         Top             =   270
         Width           =   1860
      End
   End
   Begin VB.CommandButton cmdSupCompensa 
      Caption         =   "E&liminar Compensaci�n"
      Enabled         =   0   'False
      Height          =   375
      Left            =   9225
      TabIndex        =   13
      Top             =   5715
      Width           =   1905
   End
   Begin VB.CommandButton cmdModCompensa 
      Caption         =   "Modificar &Compensacion"
      Enabled         =   0   'False
      Height          =   375
      Left            =   9225
      TabIndex        =   12
      Top             =   5310
      Width           =   1905
   End
   Begin VB.CommandButton cmdSupPago 
      Caption         =   "&Eliminar un Pago"
      Height          =   375
      Left            =   9225
      TabIndex        =   10
      Top             =   4905
      Width           =   1905
   End
   Begin VB.CommandButton cmdModPago 
      Caption         =   "&Modificar un Pago"
      Height          =   465
      Left            =   9225
      TabIndex        =   11
      Top             =   4410
      Width           =   870
   End
   Begin VB.CommandButton cmdNuevoCobro 
      Caption         =   "Introducir &Pago Nuevo"
      Height          =   375
      Left            =   9225
      TabIndex        =   9
      Top             =   4005
      Width           =   1905
   End
   Begin VB.Frame fraFrame1 
      Caption         =   " Responsable econ�mico "
      Height          =   2070
      Index           =   0
      Left            =   0
      TabIndex        =   17
      Top             =   450
      Width           =   11295
      Begin VB.CommandButton cmdAbono 
         Caption         =   "Por Abono"
         Height          =   285
         Left            =   9495
         TabIndex        =   34
         Top             =   1035
         Width           =   1245
      End
      Begin TabDlg.SSTab tabPacientes 
         Height          =   1725
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   270
         Width           =   11040
         _ExtentX        =   19473
         _ExtentY        =   3043
         _Version        =   327681
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00130.frx":000D
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "IdPersona1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "cmdFactura"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cmdHojaFiliac"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         Begin VB.CommandButton cmdHojaFiliac 
            Caption         =   "Hoja Filiaci�n"
            Height          =   465
            Left            =   10170
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   1170
            Width           =   780
         End
         Begin VB.CommandButton cmdFactura 
            Caption         =   "Por Factura"
            Height          =   285
            Left            =   9360
            TabIndex        =   2
            Top             =   450
            Width           =   1245
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1365
            Left            =   90
            TabIndex        =   1
            Top             =   315
            Width           =   10140
            _ExtentX        =   17886
            _ExtentY        =   2408
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8790
            TabIndex        =   23
            Tag             =   "N�mero Historia"
            Top             =   420
            Visible         =   0   'False
            Width           =   1410
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   6840
            TabIndex        =   22
            Tag             =   "D.N.I|D.N.I "
            Top             =   405
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   7380
            TabIndex        =   21
            Tag             =   "Segundo Apellido|Segundo Apellido"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   7140
            TabIndex        =   20
            Tag             =   "Primer Apellido|Primer Apellido"
            Top             =   855
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   4575
            TabIndex        =   19
            Tag             =   "Nombre Persona|Nombre Persona"
            Top             =   405
            Visible         =   0   'False
            Width           =   1950
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   0
            Left            =   -74880
            TabIndex        =   24
            Top             =   450
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18865
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Frame frmConciertos 
      Caption         =   " Conciertos "
      Enabled         =   0   'False
      Height          =   1365
      Left            =   0
      TabIndex        =   0
      Top             =   2520
      Width           =   9015
      Begin SSDataWidgets_B.SSDBGrid grdConciertos 
         Height          =   1050
         Left            =   90
         TabIndex        =   3
         Top             =   225
         Width           =   8835
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   767
         Columns(0).Name =   "Marcado"
         Columns(0).AllowSizing=   0   'False
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Codigo"
         Columns(1).Name =   "Codigo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   13573
         Columns(2).Caption=   "Concierto"
         Columns(2).Name =   "Concierto"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   15584
         _ExtentY        =   1852
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   16
      Top             =   7470
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   476
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.CommandButton cmdAutomat 
      Caption         =   "Automat"
      Height          =   345
      Left            =   9225
      TabIndex        =   35
      Top             =   2745
      Width           =   1905
   End
   Begin idperson.IdPersona IdPersona2 
      Height          =   330
      Left            =   0
      TabIndex        =   37
      Top             =   0
      Visible         =   0   'False
      Width           =   915
      _ExtentX        =   1614
      _ExtentY        =   582
      BackColor       =   12648384
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Datafield       =   "CI21CodPersona"
      MaxLength       =   7
      blnAvisos       =   0   'False
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frm_Cobros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public intIndGrid As Integer
Dim objMasterInfo     As New clsCWForm
Dim Responsable As Long
Const ConvX = 54.0809
Const ConvY = 54.6101086

Private Type CompFactura
  NoFactura As String
  FechaFact As String
  CantFact As String
  ImpComp As String
End Type

Dim CompFact() As CompFactura
Dim qryCobros(1 To 20) As New rdoQuery

Sub ImprimirAbono(NoPago As Long, Responsable As String)

Dim Texto As String
Dim MiSql As String
Dim MiRs As rdoResultset
Dim Acumulado As Double
Dim Compensacion As String
Dim SqlPago As String
Dim rsPago As rdoResultset
Dim sqlTipoPago As String
Dim rsTipoPago As rdoResultset
Dim SqlCompensa As String
Dim rsCompensa As rdoResultset
Dim Cantidad As Double
Dim SumaComp As Double
Dim rsFact As rdoResultset
Dim sqlFact As String

  SqlPago = "Select * From FA1700 Where FA17CODPAGO = " & NoPago
  Set rsPago = objApp.rdoConnect.OpenResultset(SqlPago, 3)
  If Not rsPago.EOF Then
    With vsPrinter1
      .Preview = False
      .Action = 3
      .FontSize = 11
      .FontName = "Courier New"
      .CurrentY = 660
      .FontBold = True
      '.FontUnderline = True
      .TextAlign = taCenterBaseline
      Texto = "A   B   O   N   O"
      .Paragraph = Texto
      Texto = "================="
      .Paragraph = Texto
      If Not IsNull(rsPago("SG02COD_UPD")) Then
        .Paragraph = "                                               " & rsPago("SG02COD_UPD") & " " & rsPago("FA17FECUPD")
      Else
        .Paragraph = "                                               " & rsPago("SG02COD_ADD") & " " & rsPago("FA17FECADD")
      End If
      .Paragraph = ""
      '.FontBold = False
      '.FontUnderline = False
      .TextAlign = taLeftBaseline
      Texto = "NUMERO DE ABONO.......: " & Space(7 - Len(str(NoPago))) & NoPago
      .Paragraph = Texto
      Texto = "RESPONSABLE ECONOMICO.: " & IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
      .Paragraph = Texto
      sqlTipoPago = "Select FA20DESIG, FA20DESC From FA2000 Where FA20CODTIPPAGO = " & rsPago("FA20CODTIPPAGO")
      Set rsTipoPago = objApp.rdoConnect.OpenResultset(sqlTipoPago, 3)
      If Not rsTipoPago.EOF Then
        Texto = rsTipoPago("FA20DESIG") & " " & rsTipoPago("FA20DESC")
      End If
      Texto = "FORMA DE PAGO.........: " & Texto
      .Paragraph = Texto
      Texto = "CONCEPTO..............: " & rsPago("FA17OBSERV") & ""
      .Paragraph = Texto
      If Not IsNull(rsPago("FA17FECPAGO")) Then
        Texto = Format(rsPago("FA17FECPAGO"), "DD/MM/YYYY")
      Else
        Texto = ""
      End If
      Texto = "FECHA INTRODUCC. ABONO: " & Texto
      .Paragraph = Texto
      If Not IsNull(rsPago("FA17FECEFECTO")) Then
        Texto = Format(rsPago("FA17FECEFECTO"), "DD/MM/YYYY")
      Else
        Texto = ""
      End If
      Texto = "FECHA EFECTO CAJA.....: " & Texto
      .Paragraph = Texto
      If Not IsNull(rsPago("FA17CANTIDAD")) Then
        Cantidad = rsPago("FA17CANTIDAD")
      Else
        Cantidad = 0
      End If
      Texto = "IMPORTE...............: " & Space(12 - Len(Format(Cantidad, "###,###,###"))) & Format(Cantidad, "###,###,###")
      .Paragraph = Texto
      SqlCompensa = "Select Sum(FA18IMPCOMP) AS SUMA From FA1800 Where FA17CODPAGO = " & NoPago
      Set rsCompensa = objApp.rdoConnect.OpenResultset(SqlCompensa, 3)
      If Not rsCompensa.EOF Then
        If Not IsNull(rsCompensa("SUMA")) Then
          SumaComp = rsCompensa("SUMA")
        Else
          SumaComp = 0
        End If
      Else
        SumaComp = 0
      End If
      If SumaComp = 0 Then
        Compensacion = "N NO COMPENSADO"
      ElseIf Cantidad = SumaComp Then
        Compensacion = "C COMPENSADO"
      ElseIf Cantidad > SumaComp Then
        Compensacion = "P PARCIALM. COMPENSADO"
      End If
      Texto = "COMPONSACION..........: " & Compensacion
      .Paragraph = Texto
      SqlCompensa = "Select Count(*) as Suma From FA1800 Where FA17CODPAGO = " & NoPago
      Set rsCompensa = objApp.rdoConnect.OpenResultset(SqlCompensa, 3)
      If Not rsCompensa.EOF Then
        SumaComp = rsCompensa("SUMA")
      Else
        SumaComp = 0
      End If
      Texto = "N.APUNTES COMPENSADOS.: " & Space(12 - Len(Trim(str(SumaComp)))) & SumaComp
      .Paragraph = Texto
      .Paragraph = ""
      Texto = "TIPO FECHA          NUMERO NOMBRE                                  IMPORTE"
      .Paragraph = Texto
      Texto = "--------------------------------------------------------------------------"
      .Paragraph = Texto
      'Seleccionamos las facturas compensadas
      MiSql = "Select * From FA1800 Where FA17CODPAGO = " & NoPago
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        Acumulado = 0
        While Not MiRs.EOF
        sqlFact = "Select FA04FECFACTURA, FA04NUMFACT,CI22NUMHISTORIA From FA0400,AD0100 Where FA04CODFACT = " & MiRs("FA04CODFACT") & _
                  " And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI"
        Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact, 3)
        If Not rsFact.EOF Then
          Texto = "F   "
          Texto = Texto & " " & Format(rsFact("FA04FECFACTURA"), "DD/MM/YYYY")
          Texto = Texto & Right(Space(11) & rsFact("FA04NUMFACT"), 11) & " "
          Texto = Texto & Left(Trim(rsFact("CI22NUMHISTORIA")) & " " & Trim(Responsable) & Space(37), 37)
          Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        Else
          Texto = "F   "
          Texto = Texto & " " & Format(MiRs("FA18FECCOMP"), "DD/MM/YYYY")
          Texto = Texto & Space(8) & " "
          Texto = Texto & Left(Trim(Responsable) & Space(37), 37)
          Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        End If
        .Paragraph = Texto
          Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
          MiRs.MoveNext
        Wend
        Texto = "                                                              ------------"
        .Paragraph = Texto
        Texto = "                                          TOTAL COMPENSADO...."
        Texto = Texto & Space(12 - Len(Format(Acumulado, "###,###,##0"))) & Format(Acumulado, "###,###,##0")
        .Paragraph = Texto
        Texto = "                                                              ============"
        .Paragraph = Texto
      End If
      .Action = 6
    End With
  End If
End Sub

Sub CalcularSaldo(Persona As Long)
Dim SqlFacturas As String
Dim rsFacturas As rdoResultset
Dim SqlPagos As String
Dim rsPagos As rdoResultset
Dim Saldo As Double
Dim Facturas As Double
Dim Pagos As Double

  'Calculamos el importe total de las facturas de una persona
  qryCobros(3).rdoParameters(0) = Persona
  Set rsFacturas = qryCobros(3).OpenResultset
  
  If Not rsFacturas.EOF Then
    If Not IsNull(rsFacturas(0)) Then
      Facturas = FormatearMoneda(CDbl(rsFacturas(0)), tiTotalFact)
    Else
      Facturas = 0
    End If
  Else
    Facturas = 0
  End If
  
  'Calculamos el importe total de los pagos de una persona
  qryCobros(4).rdoParameters(0) = Persona
  Set rsPagos = qryCobros(4).OpenResultset
  If Not rsPagos.EOF Then
    If Not IsNull(rsPagos(0)) Then
      Pagos = rsPagos(0)
    Else
      Pagos = 0
    End If
  Else
    Pagos = 0
  End If
  Saldo = Facturas - Pagos
  lblSaldo = Format(Saldo, "#,###,###,##0.##")
  lblSaldo.Refresh
  
End Sub

Private Sub Conciertos(Persona As Long)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim MiRsDes As rdoResultset
Dim MiInsercion As String
  
  'Vaciamos todos los grid que contienen datos
  grdConciertos.RemoveAll
  grdFacturas.RemoveAll
  grdPagos.RemoveAll
  'Seleccionamos de las facturas, los distintos conciertos con los que se ha facturado a ese paciente.
  qryCobros(1).rdoParameters(0) = Persona
  Set MiRs = qryCobros(1).OpenResultset
  If Not MiRs.EOF Then
''    MiRs.MoveLast
 '   MiRs.MoveFirst
    While Not MiRs.EOF
      'Seleccionamos el nombre del concierto y lo a�adimos al grid de conciertos.
      If Not IsNull(MiRs(0)) Then
        qryCobros(2).rdoParameters(0) = MiRs(0)
        Set MiRsDes = qryCobros(2).OpenResultset
        If Not MiRsDes.EOF Then
          MiInsercion = False & Chr(9) & MiRs(0) & Chr(9) & MiRsDes("FA09DESIG")
          grdConciertos.AddItem MiInsercion
        End If
      End If
      MiRs.MoveNext
    Wend
  End If
End Sub

Sub IniciarQRYs()
Dim MiSql As String
Dim x As Integer

  
  '***qry1
  MiSql = "Select /*+ INDEX(FA0400 FA0402) */ distinct FA09CODNODOCONC " & _
          "From FA0400 Where CI21CODPERSONA = ? And FA04NUMFACREAL Is Null"
  qryCobros(1).SQL = MiSql
  
  MiSql = "Select  FA09DESIG From FA0900 Where FA09CODNODOCONC = ?"
  qryCobros(2).SQL = MiSql
  
  MiSql = "Select /*+ INDEX(FA0400 FA0402) */ Sum(FA04CANTFACT) as Facturas " & _
          "From FA0400 Where  CI21CODPERSONA = ? and FA04NUMFACREAL Is  Null"
  qryCobros(3).SQL = MiSql
  
  MiSql = "Select Sum(FA17CANTIDAD) as Pagos From FA1700 Where CI21CODPERSONA = ?"
  qryCobros(4).SQL = MiSql
      
      
  MiSql = "Select FA17FECPAGO, FA17CANTIDAD, FA1700.FA17CODPAGO, CI21CODPERSONA, " & _
          "NVL(Sum(FA18IMPCOMP),0) AS Suma From FA1700,FA1800 " & _
          "Where CI21CODPERSONA = ? " & _
          "And FA1700.FA17CODPAGO = FA1800.FA17CODPAGO(+)" & _
          "Group By FA17FECPAGO, FA1700.FA17CODPAGO, FA17CANTIDAD, CI21CODPERSONA " & _
          "Order by FA17CODPAGO"
  qryCobros(5).SQL = MiSql
         
  MiSql = "Select NVL(Sum(FA51IMPCOMP),0) as Suma From FA5100 Where FA17CODPAGO_POS = ?"
  qryCobros(6).SQL = MiSql
  
  MiSql = "Select NVL(Sum(FA51IMPCOMP),0) as Suma From FA5100 Where FA17CODPAGO_NEG = ?"
  qryCobros(7).SQL = MiSql
  

  For x = 1 To 7
    Set qryCobros(x).ActiveConnection = objApp.rdoConnect
    qryCobros(x).Prepared = True
  Next
End Sub

Private Sub Pagos(Persona As Long)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim MiRsPendiente As rdoResultset
Dim MiInsertar As String
Dim Acumulado As Double
Dim TotalP As Double
Dim PdteP As Double


  'Vaciamos el grid que contiene los pagos realizados por ese paciente.
  grdPagos.RemoveAll
  TotalP = 0
  PdteP = 0
  'Creamos la qry para poder calcular las compensaciones con otros abonos
  
  
  qryCobros(5).rdoParameters(0) = Persona
  Set MiRs = qryCobros(5).OpenResultset
  If Not MiRs.EOF Then
'    MiRs.MoveLast
'    MiRs.MoveFirst
    While Not MiRs.EOF
      'Calculamos la suma de las compensaciones abono con abono dependiendo de si es positivo o negativo
      If MiRs("FA17CANTIDAD") > 0 Then
        qryCobros(6).rdoParameters(0) = MiRs("FA17CODPAGO")
        Set MiRsPendiente = qryCobros(6).OpenResultset
      Else
        qryCobros(7).rdoParameters(0) = MiRs("FA17CODPAGO")
        Set MiRsPendiente = qryCobros(7).OpenResultset
      End If
      If Not MiRsPendiente.EOF Then
        If MiRs("FA17CANTIDAD") >= 0 Then
          Acumulado = MiRsPendiente("Suma")
        Else
          Acumulado = O - MiRsPendiente("SUMA")
        End If
      Else
        Acumulado = 0
      End If
      MiInsertar = Format(MiRs("FA17FECPAGO"), "DD/MM/YYYY") & Chr(9) & _
                        Format(MiRs("FA17CANTIDAD"), "###,###,##0.##") & Chr(9) & _
                        MiRs("FA17CODPAGO") & Chr(9) & MiRs("CI21CODPERSONA") & Chr(9) & _
                        Format(MiRs("FA17CANTIDAD") - MiRs("Suma") - Acumulado, "###,###,##0.##")
      Me.grdPagos.AddItem MiInsertar
      TotalP = TotalP + CDbl(MiRs("FA17CANTIDAD"))
      PdteP = PdteP + (MiRs("FA17CANTIDAD") - MiRs("Suma"))
      MiRs.MoveNext
    Wend
    Me.txtTotPagos = Format(TotalP, "###,###,##0.##")
    Me.txtTotPdtePagos = Format(PdteP, "###,###,##0.##")
    Me.cmdFactComp.Enabled = True
  Else
    Me.txtTotPagos = 0
    Me.txtTotPdtePagos = 0
    Me.cmdFactComp.Enabled = False
  End If
End Sub
Private Sub Facturas(Persona As Long, Todas As Boolean)

Dim MiSql As String
Dim MiRs As rdoResultset
Dim rsfecha As rdoResultset
Dim ANYO As String
Dim MisqlSuma As String
Dim MiRsSuma As rdoResultset
Dim MiInsertar As String
Dim IntCont As Integer
Dim Conc As Integer
Dim Compensa As Boolean
Dim Total As Double
Dim Conciertos As String
Dim qryFacts As New rdoQuery

  'Cogemos la fecha del sistema para calcular el 1 de Enero de hace 2 a�os en el caso de entidades
  If grdConciertos.Rows > 0 Then
    If Left(IdPersona1.Text, 2) = "80" Then
      Set rsfecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
      If Not rsfecha.EOF Then
        ANYO = Format(DateAdd("yyyy", -4, rsfecha(0)), "yyyy")
        ANYO = "01/01/" & ANYO
      End If
    Else
      ANYO = "01/01/1950"
    End If
    'Vaciamos el grid que contiene los n�meros de facturas de esa persona
    grdFacturas.RemoveAll
    Total = 0
    Conc = grdConciertos.Rows
    grdConciertos.MoveFirst
    Conciertos = ""
    For IntCont = 1 To Conc
      If grdConciertos.Columns(0).Value = True Then
        'Por cada uno de los conciertos  seleccionados mostramos las facturas para esa persona y concierto
        Conciertos = Conciertos & grdConciertos.Columns(1).Text & ","
      End If
      grdConciertos.MoveNext
    Next
    If Trim(Conciertos) = "" Then
      MsgBox "No ha seleccionado ninguno de los conciertos presentados", vbExclamation + vbOKOnly, "Gesti�n de Cobros"
      Exit Sub
    End If
    Conciertos = Left(Conciertos, Len(Conciertos) - 1)
    MiSql = "Select /*+ INDEX(FA0400,FA0402)*/ FA04NUMFACT, Max(FA0400.FA04CODFACT) As Factura," & _
            "Sum(FA04CANTFACT) As Suma,Max(FA04FECFACTURA) As Fecha," & _
            "Min(FA04INDCOMPENSA) As Compensada, COUNT(FA04CODFACT) As Cantidad " & _
            "From FA0400 " & _
            "Where FA04NUMFACREAL IS NULL " & _
            "And CI21CODPERSONA =  ? "
    If Not Todas Then
    MiSql = MiSql & "And EXISTS " & _
                    "(Select FA04NUMFACT " & _
                    "From FA0400 FA0400_1 " & _
                    "Where FA0400_1.FA04NUMFACREAL IS NULL " & _
                    "And FA0400_1.FA04NUMFACT = FA0400.FA04NUMFACT " & _
                    "And FA04INDNMGC IS NULL " & _
                    "And FA04INDCOMPENSA IN (1,0) " & _
                    "And CI21CODPERSONA = ?) "
    Else
    MiSql = MiSql & "And EXISTS " & _
                    "(Select FA04NUMFACT " & _
                    "From FA0400 FA0400_1 " & _
                    "Where FA0400_1.FA04NUMFACREAL IS NULL " & _
                    "And FA04INDNMGC IS NULL " & _
                    "And FA0400_1.FA04NUMFACT = FA0400.FA04NUMFACT " & _
                    "And CI21CODPERSONA = ?) "
    End If
    MiSql = MiSql & " Group By FA04NUMFACT"
    MiSql = MiSql & " Order by Fecha Asc"
    qryFacts.SQL = MiSql
    Set qryFacts.ActiveConnection = objApp.rdoConnect
    qryFacts.Prepared = True
  
    qryFacts.rdoParameters(0) = Me.IdPersona1.Text
    qryFacts.rdoParameters(1) = Me.IdPersona1.Text
    Set MiRs = qryFacts.OpenResultset
    If Not MiRs.EOF Then
'      MiRs.MoveLast
'      MiRs.MoveFirst
      While Not MiRs.EOF
        If MiRs("Compensada") = 0 Then
          Compensa = False
        ElseIf MiRs("Compensada") = 1 Then
          Compensa = False
        ElseIf MiRs("Compensada") = 2 Then
          Compensa = True
        End If
        MiInsertar = MiRs("FA04NUMFACT") & Chr(9) & MiRs("Fecha") & Chr(9) & _
                           FormatearMoneda(CDbl(MiRs("Suma")), tiTotalFact) & Chr(9) & _
                           Compensa & Chr(9) & MiRs("Compensada") & Chr(9) & MiRs("Factura") & _
                           Chr(9) & MiRs("Cantidad")
        grdFacturas.AddItem MiInsertar
        Total = Total + CDbl(MiRs("Suma"))
        MiRs.MoveNext
      Wend
      cmdNuevoCobro.Enabled = True
    End If
    Me.txtTotFacturas = Format(Total, "###,###,##0.##")
  End If
  qryFacts.Close
End Sub

Public Sub pGestion(Respon As Long)

  Responsable = Respon
  
  frm_Cobros.Show (vbModal)
  Unload Me
End Sub

Private Sub chkTodas_Click()
  If Trim(IdPersona1.Text) <> "" Then
    chkTodas.Enabled = False
    If chkTodas.Value = 1 Then
      Screen.MousePointer = 11
      Call Facturas(IdPersona1.Text, True)
      Screen.MousePointer = 0
    ElseIf chkTodas.Value = 0 Then
      Screen.MousePointer = 11
      Call Facturas(IdPersona1.Text, False)
      Screen.MousePointer = 0
    End If
    chkTodas.Enabled = True
  End If
End Sub

Private Sub cmdAbono_Click()
Dim Resultado As String
Dim MiSql As String
Dim MiRs As rdoResultset

  Resultado = InputBox("Introduzca el n� de abono", "Selecci�n por abono", 0)
  If Resultado <> "" Then
    MiSql = "Select CI21Codpersona From FA1700 Where FA17CODPAGO = " & Resultado
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      IdPersona1.Text = MiRs(0)
    Else
      MsgBox "No existe responsable para el abono " & Resultado
    End If
  End If
      
End Sub

Private Sub cmdAbonoAbono_Click()
Dim x
Dim rsfecha As rdoResultset
Dim FechaDia As String

  If Me.grdPagos.Columns(4).Value > 0 Then
    Set rsfecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
    If Not rsfecha.EOF Then
      FechaDia = Format(rsfecha(0), "dd/mm/yyyy")
    End If
    Call frm_CompensaAbonos.CompensaAbono(IdPersona1.Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2, FechaDia, True, CDbl(grdPagos.Columns(1).Text), grdPagos.Columns(2).Text)
    If Me.chkTodas.Value = 1 Then
      Call Facturas(IdPersona1.Text, True)
    Else
      Call Facturas(IdPersona1.Text, False)
    End If
    Call Pagos(IdPersona1.Text)
    Call CalcularSaldo(IdPersona1.Text)
  ElseIf grdPagos.Columns(4).Value = 0 Then
    MsgBox "No se puede compensar con otro abono uno ya compensado", vbOKOnly + vbExclamation, "Aviso"
  Else
    MsgBox "No es posible compensar un abono negativo, seleccione uno positivo", vbOKOnly + vbExclamation, "Aviso"
  End If
End Sub

Private Sub cmdAcuse_Click()
Dim Respuesta As Integer
Dim MiSql As String
Dim MiRs As rdoResultset
Dim Acuse As Integer
Dim Enviado As Integer
Dim CodPago As Long
Dim CodPersona As Long
Dim TipoPago As Integer
Dim Cantidad As Double
Dim fecha As Date

  'Preguntaremos si ese es el pago del que se quiere enviar el acuse de recibo
  Respuesta = MsgBox("�Desea enviar el acuse de recibo del pago n�mero " & Me.grdPagos.Columns(2).Value & "?", vbYesNo + vbExclamation, "Acuses de Recibo")
  If Respuesta = vbYes Then
    'Comprobaremos que se debe enviar el acuse
    MiSql = "Select FA17INDACUSE, FA17INDACUENVI, FA20CODTIPPAGO, FA17FECPAGO " & _
            "From FA1700 Where FA17CODPAGO = " & Me.grdPagos.Columns(2).Value
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      If Not IsNull(MiRs("FA17INDACUSE")) Then
        If MiRs("FA17INDACUSE") = 1 Then
          Acuse = 1
        Else
          Acuse = 0
        End If
      Else
        Acuse = 0
      End If
      If Not IsNull(MiRs("FA17INDACUENVI")) Then
        If MiRs("FA17INDACUENVI") = 1 Then
          Enviado = 1
        Else
          Enviado = 0
        End If
      Else
        Enviado = 0
      End If
      If Not IsNull(MiRs("FA20CODTIPPAGO")) Then
        TipoPago = MiRs("FA20CODTIPPAGO")
      End If
      If Not IsNull(MiRs("FA17FECPAGO")) Then
        fecha = MiRs("FA17FECPAGO")
      End If
      If Acuse = 1 And ENVIAD0 = 0 Then
        'Hay que enviar el acuse de recibo
        CodPersona = IdPersona1.Text
        CodPago = grdPagos.Columns(2).Value
        Cantidad = CLng(grdPagos.Columns(1).Value)
        Call frm_AcuseRecibos.ImprimirUno(CodPago, CodPersona, TipoPago, Cantidad, fecha)
        MiSql = "Update FA1700 Set FA17INDACUENVI = 1 Where FA17CODPAGO = " & CodPago
        objApp.rdoConnect.Execute MiSql
      Else
        'El acuse de recibo no debe ser enviado
        MsgBox "No es necesario mandar el acuse de recibo", vbOKOnly + vbInformation, "Acuses de recibo"
        Exit Sub
      End If
    End If
  End If
  
End Sub

Private Sub cmdAnotaciones_Click()
  Call frm_AnotacionesCartas.pGestion(IdPersona1.Text)
  Set frm_AnotacionesCartas = Nothing
End Sub

Private Sub cmdCompNegat_Click()
Dim x
Dim rsfecha As rdoResultset
Dim FechaDia As String
Dim Conciertos As String
  
  grdConciertos.MoveFirst
  For x = 1 To grdConciertos.Rows
    If grdConciertos.Columns(0).Value = True Then
      Conciertos = Conciertos & grdConciertos.Columns(1).Text & ";"
    End If
    grdConciertos.MoveNext
  Next
  If Trim(Conciertos) <> "" And Right(Conciertos, 1) = ";" Then
    Conciertos = Left(Conciertos, Len(Conciertos) - 1)
  Else
    Exit Sub
  End If
  
  If Me.grdFacturas.Columns(2).Value > 0 Then
    Set rsfecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
    If Not rsfecha.EOF Then
      FechaDia = Format(rsfecha(0), "dd/mm/yyyy")
    End If
    Call frm_CompensaFact.CompFactura(IdPersona1.Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2, FechaDia, Conciertos, CDbl(grdFacturas.Columns(2).Text), grdFacturas.Columns(0).Text)
    If Me.chkTodas.Value = 1 Then
      Call Facturas(IdPersona1.Text, True)
    Else
      Call Facturas(IdPersona1.Text, False)
    End If
    Call Pagos(IdPersona1.Text)
    Call CalcularSaldo(IdPersona1.Text)
  ElseIf grdPagos.Columns(4).Value = True Then
    MsgBox "No se puede compensar con otro abono uno ya compensado", vbOKOnly + vbExclamation, "Aviso"
  Else
    MsgBox "No es posible compensar un abono negativo, seleccione uno positivo", vbOKOnly + vbExclamation, "Aviso"
  End If
End Sub

Private Sub cmdCtaCte_Click()
Dim Nombre As String
Dim Saldo As Double
  If IdPersona1.Text <> "" Then
    Nombre = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
    Saldo = CDbl(Me.lblSaldo.Caption)
    Call frm_CtaCte.ConsultarCtaCte(IdPersona1.Text, Nombre, Saldo)
  End If
End Sub

Private Sub cmdFactComp_Click()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim Facturas As String
Dim Pago As Long
Dim Cantidad As String
Dim fecha As String
Dim Cont As Integer
Dim Resp As Integer
Dim Texto As String

  Facturas = ""
  Cont = 0
  
  Pago = Me.grdPagos.Columns(2).Value
  MiSql = "Select FA04NUMFACT, FA04CANTFACT, FA04FECFACTURA, FA17FECPAGO, FA17CANTIDAD, FA18IMPCOMP " & _
          "From FA0400,FA1800,FA1700 " & _
          "Where FA1700.FA17CODPAGO = " & Pago & " " & _
          "And FA1700.FA17CODPAGO = FA1800.FA17CODPAGO " & _
          "And FA1800.FA04CODFACT = FA0400.FA04CODFACT"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Cantidad = Format(Me.grdPagos.Columns(1).Value, "###,###,##0.##")
    fecha = Format(Me.grdPagos.Columns(0).Value, "dd/mm/yyyy")
    'Creamos el mensaje a mostrar en el msgbox
    Facturas = "El abono n� " & Pago & " de fecha " & fecha & " e importe " & Cantidad & _
               " compensa las siguientes facturas " & Chr(13) & Chr(10) & Chr(13) & Chr(10)
    While Not MiRs.EOF
      Cont = Cont + 1
      ReDim Preserve CompFact(1 To Cont)
      'Lo a�adimos a un array, por si lo quieren imprimir
      CompFact(Cont).NoFactura = MiRs("FA04NUMFACT") & ""
      CompFact(Cont).FechaFact = Format(MiRs("FA04FECFACTURA"), "DD/MM/YYYY")
      CompFact(Cont).CantFact = Format(MiRs("FA04CANTFACT"), "###,###,##0.##")
      CompFact(Cont).ImpComp = Format(MiRs("FA18IMPCOMP"), "###,###,##0.##")
      'A�adimos al mensaje cada una de las facturas que se mostrar�n
      Facturas = Facturas & "Factura n� " & MiRs("FA04NUMFACT") & _
                " de importe " & Format(MiRs("FA04CANTFACT"), "###,###,##0.##") & _
                " por " & Format(MiRs("FA18IMPCOMP"), "###,###,##0.##") & Chr(13) & Chr(10)
      MiRs.MoveNext
    Wend
  End If
  If Facturas <> "" Then
    Resp = MsgBox(Facturas, vbYesNo, "Facturas compensadas por un abono")
    If Resp = vbYes Then
      With vsPrinter1
        .Preview = False
        .FontName = "Courier New"
        .FontSize = 10
        .Action = paStartDoc
        .MarginLeft = 30 * ConvX
        Texto = "El abono n� " & Pago & " de fecha " & fecha & " e importe " & Cantidad & _
                " compensa las siguientes facturas"
        .Paragraph = Texto
        .Paragraph = ""
        .MarginLeft = 60 * ConvX
        Texto = "N� FACTURA  FECHA FACT  IMP. FACTURA  IMP COMPENSA"
        .Paragraph = Texto
        Texto = "----------  ----------  ------------  ------------"
        .Paragraph = Texto
        For Cont = 1 To UBound(CompFact)
          Texto = Right(Space(10) & CompFact(Cont).NoFactura, 10) & "  "
          Texto = Texto & Right(Space(10) & CompFact(Cont).FechaFact, 10) & "  "
          Texto = Texto & Right(Space(12) & CompFact(Cont).CantFact, 12) & "  "
          Texto = Texto & Right(Space(12) & CompFact(Cont).ImpComp, 12)
          .Paragraph = Texto
          If .CurrentY > 250 * ConvY Then
            .NewPage
            .MarginLeft = 30 * ConvX
            Texto = "El abono n� " & Pago & " de fecha " & fecha & " e importe " & Cantidad & _
                    " compensa las siguientes facturas"
            .Paragraph = Texto
            .Paragraph = ""
            .MarginLeft = 60 * ConvX
            Texto = "N� FACTURA  FECHA FACT  IMP. FACTURA  IMP COMPENSA"
            .Paragraph = Texto
            Texto = "----------  ----------  ------------  ------------"
            .Paragraph = Texto
          End If
        Next
        .Action = paEndDoc
      End With
    End If
  End If
End Sub

Private Sub cmdFactura_Click()
Dim Resultado As String
Dim MiSql As String
Dim MiRs As rdoResultset

  Resultado = InputBox("Introduzca el n� de factura", "Selecci�n por factura", 0)
  If Resultado <> "" Then
    If Left(Resultado, 3) <> "13/" Then
      Resultado = "13/" & Resultado
    End If
    MiSql = "Select CI21Codpersona From FA0400 Where FA04NUMFACT = '" & Resultado & "' And FA04NUMFACREAL Is Null"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      IdPersona1.Text = MiRs(0)
    Else
      MsgBox "No existe responsable para la factura " & Resultado
    End If
  End If
      
    
End Sub

Private Sub cmdHojaFiliac_Click()
   Dim vntdatos(1) As Variant
   vntdatos(1) = IdPersona1.Text
   
'   Call objSecurity.LaunchProcess("CI4017", vntdatos)
   Call objSecurity.LaunchProcess("CI4017", vntdatos)
End Sub

Private Sub cmdModCompensa_Click()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim rsfecha As rdoResultset
Dim FechaDia As Date
Dim Cadena As String
Dim Resultado As Integer
Dim Acumulado As Double
Dim Nombre As String
Dim ImpPago As Double
Dim ImpFact As Double
Dim AcumFact As Double

 Set rsfecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsfecha.EOF Then
    FechaDia = rsfecha(0)
  End If
  If Month(CDate(grdPagos.Columns(0).Text)) <> Month(FechaDia) Then
    MsgBox "No es posible modificar una compensaci�n de un mes distinto al actual", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  Else
    'Mostraremos un mensaje indicando las facturas que se compensan con ese pago.
    Cadena = "�Desea modificar la compensaci�n de fecha " & grdPagos.Columns(0).Text & _
                   " por un importe de " & grdPagos.Columns(1).Text & "?"
    Resultado = MsgBox(Cadena, vbYesNo + vbQuestion, "Aviso")
    If Resultado = vbYes Then
      'Calcularemos el pendiente de asignaci�n del pago en cuesti�n.
      MiSql = "Select FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        Acumulado = 0
        While Not MiRs.EOF
          Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
          MiRs.MoveNext
        Wend
      Else
        Acumulado = 0
      End If
      If Acumulado <> 0 Then
        Acumulado = Acumulado - CDbl(grdPagos.Columns(1).Text)
      End If
      'Recuperamos el importe del pago
      MiSql = "Select FA17CANTIDAD From FA1700 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        ImpPago = MiRs("FA17CANTIDAD")
      Else
        ImpPago = 0
      End If
      'Recuperaremos el total de la factura
      MiSql = "Select FA04CANTFACT From FA0400 Where FA04CODFACT = " & grdPagos.Columns(3).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        ImpFact = MiRs("FA04CANTFACT")
      Else
        ImpFact = 0
      End If
      'Calcularemos la cantidad ya facturada
      MiSql = "Select FA18IMPCOMP From FA1800 Where FA04CODFACT = " & grdPagos.Columns(3).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        AcumFact = 0
        While Not MiRs.EOF
          AcumFact = AcumFact + CDbl(MiRs("FA18IMPCOMP"))
          AcumFact = AcumFact - CDbl(grdPagos.Columns(1).Text)
          MiRs.MoveNext
        Wend
      Else
        AcumFact = 0
      End If
      
      'Crearemos una cadena con los datos de la persona que hace el pago
      Nombre = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
      'Llamaremos a la pantalla que nos servir� para modificar la compensaci�n.
      Call frm_ModCompensa.pEditarCompensacion(grdPagos.Columns(2).Text, grdPagos.Columns(3).Text, Nombre, ImpPago - Acumulado, ImpFact - AcumFact)
    End If
  End If
  If Me.chkTodas.Value = 1 Then
    Call Facturas(IdPersona1.Text, True)
  Else
    Call Facturas(IdPersona1.Text, False)
  End If
  Call Pagos(IdPersona1.Text)
  Call CalcularSaldo(idepersona1.Text)
End Sub

Private Sub cmdModPago_Click()
Dim x
Dim Conciertos As String
  grdConciertos.MoveFirst
  For x = 1 To grdConciertos.Rows
    If grdConciertos.Columns(0).Value = True Then
      Conciertos = Conciertos & grdConciertos.Columns(1).Text & ";"
    End If
    grdConciertos.MoveNext
  Next
  If Trim(Conciertos) <> "" And Right(Conciertos, 1) = ";" Then
    Conciertos = Left(Conciertos, Len(Conciertos) - 1)
  End If
  Call frm_NuevoPago.NuevoPago(IdPersona1.Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2, grdPagos.Columns(0).Text, Conciertos, False, CDbl(grdPagos.Columns(1).Text), grdPagos.Columns(2).Text)


End Sub

Private Sub cmdNuevoCobro_Click()
Dim x
Dim Conciertos As String
Dim rsfecha As rdoResultset
Dim FechaDia As String

 Set rsfecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsfecha.EOF Then
    FechaDia = Format(rsfecha(0), "dd/mm/yyyy")
  End If
  grdConciertos.MoveFirst
  For x = 1 To grdConciertos.Rows
    If grdConciertos.Columns(0).Value = True Then
      Conciertos = Conciertos & grdConciertos.Columns(1).Text & ";"
    End If
    grdConciertos.MoveNext
  Next
  If Me.grdConciertos.Rows <> 0 Then
    If Trim(Conciertos) <> "" And Right(Conciertos, 1) = ";" Then
      Conciertos = Left(Conciertos, Len(Conciertos) - 1)
    Else
      MsgBox "No se ha seleccionado ninguno de los conciertos presentados, " & _
             "con lo cual no se podr�n conmpensar facturas", vbExclamation + vbOKOnly, "Gesti�n de cobros"
    '  Exit Sub
    End If
  Else
    Conciertos = ""
  End If
  Call frm_NuevoPago.NuevoPago(IdPersona1.Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2, FechaDia, Conciertos, True, 0, 0)
  If Me.chkTodas.Value = 1 Then
    Call Facturas(IdPersona1.Text, True)
  Else
    Call Facturas(IdPersona1.Text, False)
  End If
  Call Pagos(IdPersona1.Text)
  Call CalcularSaldo(IdPersona1.Text)
End Sub


Private Sub cmdRecoFactura_Click()
Dim Respuesta As Integer
Dim Nombre As String
Dim Actual As String
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim TmpPersona As Double
Dim Texto As String
Dim Cantidad As String
Dim fecha As String
Dim NoFactura As String
Dim rsfecha As rdoResultset
Dim sqlUpdate As String
Dim proceso As Long
Dim Seguir As Boolean
Dim Codigo1 As Double
Dim Codigo2 As Double
Dim CodPersona As String

  If grdFacturas.Rows > 0 Then
    TmpPersona = Val(IdPersona1.Text)
    Cantidad = Format(grdFacturas.Columns(2).Value, "###,###,##0.##")
    fecha = Format(grdFacturas.Columns(1).Value, "dd/mm/yyyy")
    Actual = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
    Codigo1 = IdPersona1.Text
    NoFactura = grdFacturas.Columns(0).Text
    
    'Comprobaremos que la factura no est� compensada aunque sea parcialmente
    If Me.grdFacturas.Columns(4).Value <> 0 Then
      MsgBox "No se puede cambiar el RECO de una factura, aunque est� compensada en parte", vbInformation + vbOKOnly, "Aviso"
      Exit Sub
    End If
    Resultado = MsgBox("�El nuevo responsable es una persona jur�dica?", vbYesNoCancel + vbQuestion, "Cambio de RECO")
    If Resultado = vbYes Then
      CodPersona = InputBox("Introduzca el c�digo de la persona jur�dica al que se le asignar� " & _
                            "la factura", "Cambio de responsable de una factura", 0)
      If CodPersona <> "" Then
        MiSql = "Select CI23RAZONSOCIAL From CI2300 Where CI21CODPERSONA = " & CodPersona
        Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
        If Not MiRs.EOF Then
          Codigo1 = IdPersona1.Text
          Nombre = MiRs("CI23RAZONSOCIAL")
          Actual = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
          Codigo2 = CodPersona
          Seguir = True
        Else
          MsgBox "No existe el c�digo de persona jur�dica " & CodPersona, vbOKOnly + vbExclamation
          Seguir = False
        End If
      Else
        Seguir = False
      End If
      'La nueva persona es una persona jur�dica.
      'Seguir = True
    ElseIf Resultado = vbNo Then
      'La nueva persona es una persona f�sica, utlizaremos el idpersona2.
      Call IdPersona2.Buscar
      If Val(IdPersona2.Text) <> 0 Then
      'Comprobaremos que la persona seleccionada tiene el mismo tipo que la anterior.
        IdPersona1.Text = TmpPersona
        Nombre = IdPersona2.Nombre & " " & IdPersona2.Apellido1 & " " & IdPersona2.Apellido2
        Codigo2 = IdPersona2.Text
        Seguir = True
      Else
        IdPersona1.Text = TmpPersona
        Seguir = False
      End If
    Else
      Seguir = False
    End If
  End If
  If Seguir = True Then
    Texto = "�Desea remplazar a " & Codigo1 & " " & Actual & Chr(13) & Chr(10) & _
            " como responsable econ�mico por " & Codigo2 & " " & Nombre & Chr(13) & Chr(10) & _
            " para la factura con fecha " & fecha & " de importe " & Importe & " ptas."
    Respuesta = MsgBox(Texto, vbQuestion + vbYesNo, "Cambio de RECO en un pago")
    If Respuesta = vbYes Then
      sqlFact = "Select AD07CODPROCESO From FA0400 Where FA04NUMFACT = '" & NoFactura & "' AND (FA04INDNMGC <> 0 OR FA04INDNMGC IS NULL)"
      Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact, 3)
      If Not rsFact.EOF Then
        sqlUpdate = "Update FA0400 Set CI21CODPERSONA = " & Codigo2 & " Where FA04NUMFACT = '" & NoFactura & "'"
        objApp.rdoConnect.Execute sqlUpdate
        If Not IsNull(rsFact(0)) Then
          proceso = rsFact(0)
          sqlUpdate = "Update AD1100 Set CI21CODPERSONA = " & Codigo2 & " Where AD07CODPROCESO = " & proceso
          objApp.rdoConnect.Execute sqlUpdate
        End If
      End If
    End If
  End If
 
End Sub

 
  
   
    
     
      
       
        
         
          
           
            
             
              
               
                
                 
                  
                 
               
             
           
         
       
     
   
 
 
Private Sub cmdRecoPago_Click()
Dim Respuesta As Integer
Dim Nombre As String
Dim Actual As String
Dim SqlAsist As String
Dim rsAsist As rdoResultset
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim TmpPersona As Double
Dim Texto As String
Dim Cantidad As String
Dim fecha As String
Dim NoPago As Long
Dim rsfecha As rdoResultset
Dim sqlUpdate As String
Dim Resultado As Integer
Dim CodPersona As String
Dim MiRs As rdoResultset
Dim MiSql As String
Dim Seguir As Boolean

  TmpPersona = Val(IdPersona1.Text)
  Cantidad = Format(grdPagos.Columns(1).Value, "###,###,##0.##")
  fecha = Format(grdPagos.Columns(0).Value, "dd/mm/yyyy")
  Actual = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
  Codigo1 = IdPersona1.Text
  NoPago = grdPagos.Columns(2).Value
  
  'Comprobaremos que el pago no est� compensado aunque sea parcialmente
  If CDbl(grdPagos.Columns(1).Value) <> CDbl(grdPagos.Columns(4).Value) Then
    MsgBox "No se puede cambiar el RECO de un pago, aunque est� compensado en parte", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  End If
  'Comprobaremos que el pago es del mes actual
  Set rsfecha = objApp.rdoConnect.OpenResultset("Select SYSDATE From DUAL")
  If Not rsfecha.EOF Then
    If Month(fecha) <> Month(rsfecha(0)) Or Year(fecha) <> Year(rsfecha(0)) Then
      MsgBox "No se puede cambiar el RECO de un pago correspondiente a un mes distinto del actual", vbInformation + vbOKOnly, "Aviso"
      Exit Sub
    End If
  End If
  'Preguntaremos si el nuevo responsable econ�mico del pago es una persona juridica
  Resultado = MsgBox("�El nuevo responsable es una persona jur�dica?", vbYesNoCancel + vbQuestion, "Cambio de RECO")
  If Resultado = vbYes Then
    CodPersona = InputBox("Introduzca el c�digo de la persona jur�dica al que se le asignar� " & _
                          "el pago", "Cambio de responsable de un pago", 0)
    If CodPersona <> "" Then
      MiSql = "Select CI23RAZONSOCIAL From CI2300 Where CI21CODPERSONA = " & CodPersona
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        Codigo1 = IdPersona1.Text
        Nombre = MiRs("CI23RAZONSOCIAL")
        Actual = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
        Codigo2 = CodPersona
        Seguir = True
      Else
        MsgBox "No existe el c�digo de persona jur�dica " & CodPersona, vbOKOnly + vbExclamation
        Seguir = False
      End If
    Else
      Seguir = False
    End If
  ElseIf Resultado = vbNo Then
    Call IdPersona2.Buscar
    If Val(IdPersona2.Text) <> 0 Then
      'Comprobaremos que la persona seleccionada tiene el mismo tipo que la anterior.
      IdPersona1.Text = TmpPersona
      Nombre = IdPersona2.Nombre & " " & IdPersona2.Apellido1 & " " & IdPersona2.Apellido2
      Codigo2 = IdPersona2.Text
      Seguir = True
    Else
      IdPersona1.Text = TmpPersona
      Seguir = False
    End If
  Else
    Seguir = False
  End If
  If Seguir = True Then
    Texto = "�Desea remplazar a " & IdPersona1.Text & " " & Actual & Chr(13) & Chr(10) & _
            " como responsable econ�mico por " & IdPersona2.Text & " " & Nombre & Chr(13) & Chr(10) & _
            " para el pago con fecha " & fecha & " de importe " & Importe & " ptas."
    Respuesta = MsgBox(Texto, vbQuestion + vbYesNo, "Cambio de RECO en un pago")
    If Respuesta = vbYes Then
      sqlUpdate = "Update FA1700 Set CI21CODPERSONA = " & IdPersona2.Text & " Where FA17CODPAGO = " & NoPago
      objApp.rdoConnect.Execute sqlUpdate
    End If
  End If

End Sub

Private Sub cmdReimprimirAbono_Click()
  Call Me.ImprimirAbono(grdPagos.Columns(2).Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2)
End Sub

Private Sub cmdSupCompensa_Click()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim rsfecha As rdoResultset
Dim FechaDia As Date
Dim Cadena As String
Dim Resultado As Integer
Dim ImpFact As Double
Dim AcumComp As Double
  
 On Error GoTo ErrorenBorrado
 Set rsfecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsfecha.EOF Then
    FechaDia = rsfecha(0)
  End If
  If Month(CDate(grdPagos.Columns(0).Text)) <> Month(FechaDia) Then
    MsgBox "No es posible eliminar una compensaci�n de un mes distinto al actual", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  Else
    'Mostraremos un mensaje indicando las facturas que se compensan con ese pago.
    Cadena = "�Desea borrar la compensaci�n de fecha " & grdPagos.Columns(0).Text & _
                   " por un importe de " & grdPagos.Columns(1).Text & "?"
    Resultado = MsgBox(Cadena, vbYesNo + vbQuestion, "Aviso")
    If Resultado = vbYes Then
      objApp.rdoConnect.BeginTrans
        'Borraremos todas las compensaciones que se correspondan con ese pago
        MiSql = "Delete From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text & _
                  " And FA04CODFACT =  " & grdPagos.Columns(3).Text
        objApp.rdoConnect.Execute MiSql
        'Asignaremos el nuevo estado de la factura
        MiSql = "Select FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
        Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
        If Not MiRs.EOF Then
          AcumComp = 0
          MiRs.MoveLast
          MiRs.MoveFirst
          While Not MiRs.EOF
            AcumComp = AcumComp + CDbl(MiRs("FA18IMPCOMP"))
            MiRs.MoveNext
          Wend
        Else
          AcumComp = 0
        End If
        If AcumComp = 0 Then
          MiSql = "Update FA0400 Set FA04INDCOMPENSA = 0 Where FA04CODFACT = " & grdPagos.Columns(3).Text
        Else
          MiSql = "Update FA0400 Set FA04INDCOMPENSA = 1 Where FA04CODFACT = " & grdPagos.Columns(3).Text
        End If
        objApp.rdoConnect.Execute MiSql
      'Recargaremos la pantalla
     objApp.rdoConnect.CommitTrans
     Call Facturas(IdPersona1.Text, False)
     Call Pagos(IdPersona1.Text)
     Call CalcularSaldo(idepersona1.Text)
    End If
  End If
Exit Sub
ErrorenBorrado:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en el borrado de la compensaci�n. Vuelva a intentarlo", vbCritical + vbOKOnly, "Atenci�n"
End Sub

Private Sub cmdSupPago_Click()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim rsfecha As rdoResultset
Dim FechaDia As Date
Dim Cadena As String
Dim Resultado As Integer
Dim rsEfecto As rdoResultset
Dim sqlEfecto As String
Dim Seguir As Boolean

On Error GoTo ErrorEnSuprimir
  
  objApp.rdoConnect.BeginTrans
    sqlEfecto = "Select FA17FECEFECTO FROM FA1700 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
    Set rsEfecto = objApp.rdoConnect.OpenResultset(sqlEfecto, 3)
    If Not rsEfecto.EOF Then
      If Not IsNull(rsEfecto(0)) Then
        Seguir = False
      Else
        Seguir = True
      End If
    End If
    If Not Seguir Then
      MsgBox "No es posible eliminar un pago que tiene fecha de efecto", vbInformation + vbOKOnly, "Aviso"
      Exit Sub
    Else
      'Mostraremos un mensaje indicando las facturas que se compensan con ese pago.
      MiSql = "Select FA04CODFACT From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        Cadena = ""
        While Not MiRs.EOF
          Cadena = Cadena & "13/" & MiRs("FA04CODFACT") & Chr(13) & Chr(10)
          MiRs.MoveNext
        Wend
      Else
        Cadena = ""
      End If
      Cadena = "�Desea borrar el pago de fecha " & grdPagos.Columns(0).Text & " que compensa las facturas " & Chr(13) & Chr(10) & Cadena
      Resultado = MsgBox(Cadena, vbYesNo + vbQuestion, "Aviso")
      If Resultado = vbYes Then
        'En el caso de la factura figure como totalmente compensada, le quitaremos ese indicador, pasando a ser parcialmente
        'compensada o no compensada en su totalidad
        MiSql = "Select FA04CODFACT, FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
        Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
        If Not MiRs.EOF Then
          MiRs.MoveLast
          MiRs.MoveFirst
          While Not MiRs.EOF
            sqlFact = "Select FA04CANTFACT From FA0400 Where FA04CODFACT = " & MiRs("FA04CODFACT")
            Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact, 3)
            If Not rsFact.EOF Then
              If Not IsNull(rsFact("FA04CANTFACT")) Then
                If rsFact("FA04CANTFACT") = MiRs("FA18IMPCOMP") Then
                  sqlFact = "Update FA0400 Set FA04INDCOMPENSA = 0 Where FA04CODFACT = " & MiRs("FA04CODFACT")
                  objApp.rdoConnect.Execute sqlFact
                Else
                  sqlFact = "Update FA0400 Set FA04INDCOMPENSA = 1 Where FA04CODFACT = " & MiRs("FA04CODFACT")
                  objApp.rdoConnect.Execute sqlFact
                End If
              End If
            End If
            MiRs.MoveNext
          Wend
        End If
        'Borraremos todas las compensaciones que se correspondan con ese pago
        MiSql = "Delete From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
        objApp.rdoConnect.Execute MiSql
        If grdPagos.Columns(1).Value > 0 Then
          MiSql = "Delete From FA5100 Where FA17CODPAGO_POS = " & grdPagos.Columns(2).Text
        Else
          MiSql = "Delete From FA5100 Where FA17CODPAGO_NEG = " & grdPagos.Columns(2).Text
        End If
        objApp.rdoConnect.Execute MiSql
        'Borraremos el pago
        MiSql = "Delete From FA1700 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
        objApp.rdoConnect.Execute MiSql
         'Recargaremos la pantalla
         If Me.chkTodas.Value = 1 Then
            Call Facturas(IdPersona1.Text, True)
         Else
            Call Facturas(IdPersona1.Text, False)
        End If
         Call Pagos(IdPersona1.Text)
         Call CalcularSaldo(IdPersona1.Text)
      End If
    End If
  objApp.rdoConnect.CommitTrans
Exit Sub
ErrorEnSuprimir:
  MsgBox "Se ha producido un error y no se ha suprimido el pago", vbCritical + vbOKOnly, "Atenci�n"
  objApp.rdoConnect.RollbackTrans
  Resume Next
End Sub

Private Sub Form_Load()
  
  Call IniciarQRYs
  
  Dim intGridIndex      As Integer
  Call objApp.SplashOn
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleOpen, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
  
  
  With objMasterInfo
    '.strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPacientes
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    'If Responsable <> 0 Then
    '  .strWhere = "CI21CODPERSONA = " & Responsable
    'Else
      .strInitialWhere = "CI21CODPERSONA = 1000001"
   ' End If
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", False)
    .blnChanged = False
  End With
  
  
  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormCreateInfo(objMasterInfo)
    
    IdPersona1.ToolTipText = ""
    .CtrlGetInfo(IdPersona1).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
 Call .WinRegister
    Call .WinStabilize
    IdPersona1.BackColor = objApp.objUserColor.lngKey
'    blncommit = False
  End With
      
      

 Call objApp.SplashOff
 IdPersona1.Text = Responsable
    'Invisible bot�n de B�squeda
'  IdPersona1.blnAvisos = False

End Sub

Private Sub grdConciertos_InitColumnProps()

End Sub

Private Sub grdDBGrid1_Change(Index As Integer)

  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, Y As Single)
'  If Button = 2 Then
'    PopupMenu mnuEstado
'  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdFacturas_DblClick()
Dim MiSql As String
Dim NoFactura As Long
Dim rsPagos As rdoResultset
Dim Insertar As String
Dim noFact As String
Dim anumfact(1 To 1) As String

  If CInt(grdFacturas.Columns(6).Text) > 1 Then
  Else
    If grdFacturas.Rows > 0 Then
      If grdFacturas.Columns(0).Text >= "13/1600000" Then
        anumfact(1) = grdFacturas.Columns(0).Value
        Call ImprimirFact(anumfact)
      End If
    End If
  End If
'''  grdPagos.Columns(0).Width = 1811
'''  grdPagos.Columns(1).Width = 1319
'''  grdPagos.Columns(4).Visible = False
'''  NoFactura = Right(grdFacturas.Columns(0).Text, Len(grdFacturas.Columns(0).Text) - 4)
'''  'Buscaremos todos los pagos quellevan asociado el n�mero de esa factura.
'''  MiSql = "Select FA1700.FA17FECPAGO, FA1800.FA18IMPCOMP FROM FA1700, FA1800 " & _
'''              "Where FA1700.FA17CODPAGO = FA1800.FA17CODPAGO AND FA1800.FA04CODFACT = " & NoFactura
'''  Set rsPagos = objApp.rdoConnect.OpenResultset(MiSql, 3)
'''  If Not rsPagos.EOF Then
'''    grdPagos.RemoveAll
'''    rsPagos.MoveLast
'''    rsPagos.MoveFirst
'''    While Not rsPagos.EOF
'''      Insertar = Format(rsPagos("FA17FECPAGO"), "DD/MM/YYYY") & Chr(9) & _
'''                     Format(0 & rsPagos("FA18IMPCOMP"), "###,###,##0.##") & Chr(9) & " " & Chr(9) & " "
'''      grdPagos.AddItem Insertar
'''      rsPagos.MoveNext
'''    Wend
'''  Else
'''    grdPagos.RemoveAll
'''    MsgBox "La factura: " & grdFacturas.Columns(0).Text & " no tiene pagos que la compensen", vbInformation + vbOKOnly, "Aviso"
'''  End If
'''  'Fecha/Cantidad/N�mero/Persona
'''  'FA17CODPAGO/CI21CODPERSONA/FA17FECPAGO/FA17CANTIDAD
'''  'FA17CODPAGO/FA04CODFACT/FA18FECCOMP/FA18IMPCOMP
'''  'NoFactura/Fecha/Importe/Compensado(s/n)
'''

End Sub

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
  If Trim(IdPersona1.Text) <> "" And IsNumeric(IdPersona1.Text) Then
    If CLng(IdPersona1.Text) >= 800000 Then
      Call Conciertos(IdPersona1.Text)
      Call Pagos(IdPersona1.Text)
      Call CalcularSaldo(IdPersona1.Text)
      frmConciertos.Enabled = True
      frmFacturas.Enabled = True
      frmPagos.Enabled = True
      Me.txtTotFacturas = "0,"
    End If
  End If
End Sub

Private Sub IdPersona1_GotFocus()
'  frmCitasPaciente.MousePointer = vbHourglass
'  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.CtrlGotFocus
'  frmCitasPaciente.MousePointer = vbDefault
End Sub

Private Sub IdPersona1_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset
  
  If Trim(IdPersona1) <> "" Then
    If Left(IdPersona1.Text, 2) <> "80" Then
      MiSql = "Select CI23RAZONSOCIAL From CI2300 Where CI21CODPERSONA = " & IdPersona1.Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        GoTo Entidad
      Else
        Call objWinInfo.CtrlLostFocus
      End If
    Else
Entidad:
      'MiSqL = "select ci13desentidad from ci1300,ci2900 " & _
              "where ci1300.ci13codentidad = ci2900.ci13codentidad " & _
              "and ci1300.ci32codtipecon = ci2900.ci32codtipecon " & _
              "and ci2900.ci21codpersona_rec = " & IdPersona1.Text
      MiSql = "Select CI23RAZONSOCIAL From CI2300 Where CI21CODPERSONA = " & IdPersona1.Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        If Not IsNull(MiRs("CI23RAZONSOCIAL")) Then
          IdPersona1.Nombre = MiRs("CI23RAZONSOCIAL")
        End If
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  If strFormName = "fraFrame1(0)" And strCtrl = "IdPersona1" Then
     IdPersona1.SearchPersona
     objWinInfo.DataRefresh
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  'fraFrame1(9).ForeColor = fraFrame1(tabTab1(2).Tab + 1).ForeColor
  If strFormName = "fraFrame1(0)" Then
    IdPersona1.blnAvisos = True
    tlbToolbar1.Buttons(26).Enabled = True

  Else
    IdPersona1.blnAvisos = False
    tlbToolbar1.Buttons(26).Enabled = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
      objWinInfo.DataRefresh
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
    End If
  End If
  
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = "fraFrame1(0)" Then
'    Call IdPersona1.ReadPersona
'  End If

End Sub

Private Sub optPagos_Click(Index As Integer)
Dim MiSql As String
Dim NoFactura As String
Dim rsPagos As rdoResultset
Dim Insertar As String
Dim sqlFact As String
Dim rsFact As rdoResultset
  
  If optPagos(0).Value = True Then '1150/950/950
    grdPagos.RemoveAll
    Me.cmdModCompensa.Enabled = False
    Me.cmdSupCompensa.Enabled = False
    grdPagos.Columns(0).Width = 1150
    grdPagos.Columns(1).Width = 950
    grdPagos.Columns(4).Width = 950
    grdPagos.Columns(4).Visible = True
    If Trim(IdPersona1.Text) <> "" Then
      Call Pagos(IdPersona1.Text)
    End If
    If grdPagos.Rows > 0 Then
      Me.cmdSupPago.Enabled = True
      Me.cmdFactComp.Enabled = True
      Me.cmdModPago.Enabled = True
      Me.cmdAcuse.Enabled = True
    End If
  ElseIf optPagos(1).Value = True Then
    grdPagos.RemoveAll
    Me.cmdModPago.Enabled = False
    Me.cmdSupPago.Enabled = False
    Me.cmdFactComp.Enabled = False
    Me.cmdAcuse.Enabled = False
    grdPagos.Columns(0).Width = 1811
    grdPagos.Columns(1).Width = 1319
    grdPagos.Columns(4).Visible = False
    If grdFacturas.Rows <> 0 Then
      sqlFact = "Select FA04CODFACT From FA0400 Where FA04NUMFACT = '" & grdFacturas.Columns(0).Text & "' And FA04CANTFACT <> 0 " & _
                "And FA04NUMFACREAL IS NULL"
      Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact, 3)
      If Not rsFact.EOF Then
        NoFactura = ""
        While Not rsFact.EOF
          NoFactura = NoFactura & rsFact(0) & ","
          rsFact.MoveNext
        Wend
        If Trim(NoFactura) <> "" And Right(NoFactura, 1) = "," Then
          NoFactura = Left(NoFactura, Len(NoFactura) - 1)
        End If
      Else
        NoFactura = ""
      End If
      If NoFactura <> "" Then
        'Buscaremos todos los pagos que llevan asociado el n�mero de esa factura.
        MiSql = "Select FA17CODPAGO, Min(FA18FECCOMP) As Fecha, Sum(FA18IMPCOMP) As Importe, " & _
                "Min(FA04CODFACT) As Factura " & _
                "FROM FA1800 " & _
                "Where FA1800.FA04CODFACT IN (" & NoFactura & ") " & _
                "Group By FA17CODPAGO"
        Set rsPagos = objApp.rdoConnect.OpenResultset(MiSql, 3)
        If Not rsPagos.EOF Then
          grdPagos.RemoveAll
          rsPagos.MoveLast
          rsPagos.MoveFirst
          While Not rsPagos.EOF
            Insertar = Format(rsPagos("Fecha"), "DD/MM/YYYY") & Chr(9) & _
                           Format(0 & rsPagos("Importe"), "###,###,##0.##") & Chr(9) & _
                           rsPagos("FA17CODPAGO") & Chr(9) & rsPagos("Factura") & Chr(9) & " "
            grdPagos.AddItem Insertar
            rsPagos.MoveNext
          Wend
          Me.cmdSupCompensa.Enabled = True
          Me.cmdModCompensa.Enabled = True
        Else
          grdPagos.RemoveAll
          MsgBox "La factura: " & grdFacturas.Columns(0).Text & " no tiene pagos que la compensen", vbInformation + vbOKOnly, "Aviso"
        End If
      Else
        grdPagos.RemoveAll
        MsgBox "La factura: " & grdFacturas.Columns(0).Text & " no tiene pagos que la compensen", vbInformation + vbOKOnly, "Aviso"
      End If
    End If
  End If
  
  'Fecha/Importe 1811/1319
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabPacientes_Click(PreviousTab As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Me.MousePointer = vbDefault
End Sub

Private Sub tabPacientes_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)

End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  objMasterInfo.blnChanged = False
  If btnButton.Index = 16 Then
    'Call IdPersona1.Buscar
    If UCase(ActiveControl.Name) = "GRDCONCIERTOS" Then
      Call Facturas(IdPersona1.Text, False)
    ElseIf UCase(ActiveControl.Name) = "IDPERSONA1" Then
      If IdPersona1.Text <> "" Then
        Call Conciertos(IdPersona1.Text)
        Call Pagos(IdPersona1.Text)
        Me.cmdModPago.Enabled = True
        Me.cmdNuevoCobro.Enabled = True
        Me.cmdSupPago.Enabled = True
        Me.cmdFactComp.Enabled = True
        Me.txtTotFacturas = "0,"
      Else
        IdPersona1.Buscar
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   
End Sub


