VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_ListUrgencias 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Listado de Urgencias"
   ClientHeight    =   1230
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3570
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1230
   ScaleWidth      =   3570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   240
      Left            =   270
      TabIndex        =   5
      Top             =   855
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   423
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Iniciar Impresi�n"
      Height          =   330
      Left            =   945
      TabIndex        =   4
      Top             =   810
      Width           =   1725
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1755
      TabIndex        =   0
      Tag             =   "Fecha Inicio"
      Top             =   45
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1755
      TabIndex        =   1
      Tag             =   "Fecha Inicio"
      Top             =   360
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblFecInicio 
      Caption         =   "Fecha de Inicio:"
      Enabled         =   0   'False
      Height          =   255
      Left            =   180
      TabIndex        =   3
      Top             =   90
      Width           =   1650
   End
   Begin VB.Label lblFecFin 
      Caption         =   "Fecha de Final:"
      Enabled         =   0   'False
      Height          =   255
      Left            =   180
      TabIndex        =   2
      Top             =   405
      Width           =   1650
   End
End
Attribute VB_Name = "frm_ListUrgencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ArrayListado() As String

Private Sub cmdImprimir_Click()
Dim Seguir As Boolean
Dim Nombre As String
Dim Historia As String
Dim Tipo As String
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim SqLpAc As String
Dim RsPaC As rdoResultset
Dim SqLtIpO As String
Dim RsTiPo As rdoResultset
Dim Cont As Integer
Dim Listar As Boolean

  'Comprobamos que la fecha de inicio no sea mayor que la fecha final y que estas sean correctas
  If Not IsDate(Me.SDCFechaInicio.Date) Then
    MsgBox "La fecha de inicio no es correcta", vbOKOnly + vbCritical, "Aviso"
    Exit Sub
  End If
  If Not IsDate(Me.SDCFechaFin.Date) Then
    MsgBox "La fecha final no es correcta", vbOKOnly + vbCritical, "Aviso"
    Exit Sub
  End If
  If Me.SDCFechaFin.Date < Me.SDCFechaInicio.Date Then
    MsgBox "La fecha final no puede ser menor que la fecha de inicio", vbCritical + vbOKOnly, "Atenci�n"
    Exit Sub
  End If
  
  'Una vez comprobados las fechas seleccionamos de PR0400 todas los procesos y asistencias que tienen como departamento
  'urgencias y se encuentran entre dos fechas concretas.
  MiSqL = "Select /*+RULE*/ PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO, MIN(PR04FECINIACT) AS PR04FECINIACT, " & _
              "PR0400.CI21CODPERSONA " & _
              "From PR0400, FA0400 " & _
              "Where AD02CODDPTO = 216 AND PR0400.PR37CODESTADO < 6 " & _
              "AND PR04FECINIACT >= TO_DATE('" & Me.SDCFechaInicio & " 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
              "AND PR04FECINIACT <= TO_DATE('" & Me.SDCFechaFin & " 23:59:59','DD/MM/YYYY HH24:MI:SS') " & _
              "AND FA0400.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI " & _
              "AND FA0400.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO " & _
              "AND FA0400.FA04CODFACT IS NULL " & _
              "GROUP BY PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO, PR0400.CI21CODPERSONA " & _
              "ORDER BY PR04FECINIACT ASC"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Cont = 1
    While Not MiRs.EOF
      If Not IsNull(MiRs("CI21CODPERSONA")) Then
        SqLpAc = "Select CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL " & _
                      "From CI2200 WHERE CI21CODPERSONA = " & MiRs("CI21CODPERSONA")
        Set RsPaC = objApp.rdoConnect.OpenResultset(SqLpAc, 3)
        If Not RsPaC.EOF Then
          Nombre = ""
          If Not IsNull(RsPaC("CI22NOMBRE")) Then
            Nombre = Nombre & RsPaC("CI22NOMBRE") & " "
          End If
          If Not IsNull(RsPaC("CI22PRIAPEL")) Then
            Nombre = Nombre & RsPaC("CI22PRIAPEL") & " "
          End If
          If Not IsNull(RsPaC("CI22SEGAPEL")) Then
            Nombre = Nombre & RsPaC("CI22SEGAPEL")
          End If
          If Not IsNull(RsPaC("CI22NUMHISTORIA")) Then
            Historia = CStr(RsPaC("CI22NUMHISTORIA"))
          Else
            Historia = ""
          End If
        Else
          Nombre = ""
          Historia = ""
        End If
      End If
      If Not IsNull(MiRs("AD07CODPROCESO")) And Not IsNull(MiRs("AD01CODASISTENCI")) Then
        SqLtIpO = "SELECT CI13CODENTIDAD, CI32CODTIPECON FROM AD1100 " & _
                  "WHERE AD07CODPROCESO = " & MiRs("AD07CODPROCESO") & " " & _
                  "AND AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & " " & _
                  "AND AD11FECFIN IS NULL"
        Set RsTiPo = objApp.rdoConnect.OpenResultset(SqLtIpO, 3)
        If Not RsTiPo.EOF Then
          If Not IsNull(RsTiPo("CI13CODENTIDAD")) And Not IsNull(RsTiPo("CI32CODTIPECON")) Then
            Tipo = RsTiPo("CI32CODTIPECON") & "/" & RsTiPo("CI13CODENTIDAD")
            If RsTiPo("CI32CODTIPECON") = "P" Or RsTiPo("CI32CODTIPECON") = "M" Or RsTiPo("CI32CODTIPECON") = "J" Then
              Seguir = True
            Else
              Seguir = False
            End If
          Else
            Tipo = ""
            Seguir = False
          End If
        End If
      Else
        Tipo = ""
        Seguir = False
      End If
      If Seguir = True Then
        ReDim Preserve ArrayListado(1 To 6, 1 To Cont)
        ArrayListado(1, Cont) = Historia
        ArrayListado(2, Cont) = Nombre
        If Not IsNull(MiRs("PR04FECINIACT")) Then
          ArrayListado(3, Cont) = CStr(MiRs("PR04FECINIACT"))
        Else
          ArrayListado(3, Cont) = ""
        End If
        If Not IsNull(MiRs("AD07CODPROCESO")) Then
          ArrayListado(4, Cont) = CStr(MiRs("AD07CODPROCESO"))
        Else
          ArrayListado(4, Cont) = ""
        End If
        If Not IsNull(MiRs("AD01CODASISTENCI")) Then
          ArrayListado(5, Cont) = CStr(MiRs("AD01CODASISTENCI"))
        Else
          ArrayListado(5, Cont) = ""
        End If
        ArrayListado(6, Cont) = Tipo
        Cont = Cont + 1
      End If
      MiRs.MoveNext
    Wend
    Seguir = True
  Else
    MsgBox "No hay entradas en urgencias en las fechas seleccionadas", vbInformation + vbOKOnly, "Aviso"
    Seguir = False
  End If
  If Seguir Then
    Call Me.ImprimirListado
  End If
End Sub


Sub ImprimirListado()
Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer
Dim Tipo As String

  'Calculamos el n�mero de pagos que vamos a listar
  NoAbonos = UBound(ArrayListado, 2)
  Pagina = 1
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  vsPrinter.FontSize = 9
  vsPrinter.FontName = "Courier New"
  Texto = "LISTADO DE URGENCIAS                                                               PAG: " & Pagina
  vsPrinter.Paragraph = Texto
  Texto = "--------------------"
  vsPrinter.Paragraph = Texto
  vsPrinter.Paragraph = ""
  Texto = "N� HISTORIA NOMBRE                              FECHA Y HORA     PROCESO    ASISTENCIA TIPO"
  vsPrinter.Paragraph = Texto
  Texto = "----------- ----------------------------------- ---------------- ---------- ---------- ----"
  vsPrinter.Paragraph = Texto
  For Cont = 1 To NoAbonos
    Texto = Right(Space(11) & Trim(ArrayListado(1, Cont)), 11) & " " 'N� de Historia
    Texto = Texto & Left(Trim(ArrayListado(2, Cont)) & Space(30), 35) & " "  'Paciente
    Texto = Texto & Format(ArrayListado(3, Cont), "DD/MM/YYYY HH:MM") & " "  'Fecha de Cobro
    Texto = Texto & Right(Space(12) & Trim(ArrayListado(4, Cont)), 10) & " " 'Proceso
    Texto = Texto & Right(Space(12) & Trim(ArrayListado(5, Cont)), 10) & " "  'Asistencia
    Texto = Texto & Right(Space(4) & Trim(ArrayListado(6, Cont)), 4)
    vsPrinter.Paragraph = Texto
    If vsPrinter.CurrentY > 260 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      vsPrinter.NewPage
      vsPrinter.CurrentY = 10 * 54.6101086
      Pagina = Pagina + 1
      Texto = "LISTADO DE URGENCIAS                                                               PAG: " & Pagina
      vsPrinter.Paragraph = Texto
      Texto = "--------------------"
      vsPrinter.Paragraph = Texto
      vsPrinter.Paragraph = ""
      Texto = "N� HISTORIA NOMBRE                              FECHA Y HORA     PROCESO    ASISTENCIA TIPO"
      vsPrinter.Paragraph = Texto
      Texto = "----------- ----------------------------------- ---------------- ---------- ---------- ----"
      vsPrinter.Paragraph = Texto
    End If
  Next
  vsPrinter.Action = 6
End Sub




Private Sub Form_Load()

End Sub


