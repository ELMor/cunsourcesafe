VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PropuestaFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public Key As String

' aqui se guardan los diferentes nodos de factura
' por ejemplo : forfait T.H. , Acto M�dico, Estancias, etc.
Public NodosFactura As New Collection

' Se utiliza para contener los nodos de concierto
' aplicados, a partir de esta colecci�n se puede regenerar la propuesta
' con las posibles modificaciones que se hayan realizado en el concierto
' o en los RNFs
Public NodosAplicados As New Collection

' esta coleccion contiene las lineas que se a�aden
' desde la edici�n de factura
Public NuevasLineas As New Collection


' Coleccion que contiene los RECOs de la propuesta
'Public REcos As New Collection

Public NecesitaRegeneracion As Boolean
Public Modificada As Boolean

Public EsGarantia As Boolean

Private mvarName As String ' copia local

Public Property Let Name(ByVal vData As String)
'se usa cuando se asigna un valor a una propiedad, en el lado izquierdo de la asignaci�n.
'Syntax: X.Name = 5
    mvarName = vData
End Property

Public Property Get Name() As String
    Dim strName As String
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignaci�n.
'Syntax: Debug.Print X.Name
    If mvarName = "" Then
        strName = ""
        For Each NodoFactura In NodosFactura
            strName = strName & IIf(strName = "", "", " + ") & "(" & NodoFactura.NombreConcierto & ") " & NodoFactura.Name
        Next
        Name = strName
    Else
        Name = mvarName
    End If
End Property

Public Function Importe(Optional KeyREco As String) As Double
   Dim dblImporte As Double
   
   If IsMissing(KeyREco) Or KeyREco = "" Then
      ' acumular el importe de las lineas generadas por el concierto
      For Each Nodo In NodosFactura
         For Each Linea In Nodo.Nodos
            dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.Total), RedondearMoneda(Linea.LineaFact.Total, tiImporteLinea), 0)
            dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.TotalNoDescontable), RedondearMoneda(Linea.LineaFact.TotalNoDescontable, tiImporteLinea), 0)
         Next
      Next
      
      ' acumular el importe de las lineas a�adidas en la edici�n de factura
      For Each Linea In NuevasLineas
         dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.Total), RedondearMoneda(Linea.LineaFact.Total, tiImporteLinea), 0)
         dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.TotalNoDescontable), RedondearMoneda(Linea.LineaFact.TotalNoDescontable, tiImporteLinea), 0)
      Next
   
   Else
      ' acumular el importe de las lineas generadas por el concierto que sean de ese reco
      For Each Nodo In NodosFactura
         For Each Linea In Nodo.Nodos
            If Linea.KeyREco = KeyREco Then
               dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.Total), RedondearMoneda(Linea.LineaFact.Total, tiImporteLinea), 0)
               dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.TotalNoDescontable), RedondearMoneda(Linea.LineaFact.TotalNoDescontable, tiImporteLinea), 0)
            End If
         Next
      Next
      
      ' acumular el importe de las lineas a�adidas en la edici�n de factura que sean de ese reco
      For Each Linea In NuevasLineas
         If Linea.KeyREco = KeyREco Then
            dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.Total), RedondearMoneda(Linea.LineaFact.Total, tiImporteLinea), 0)
            dblImporte = dblImporte + IIf(IsNumeric(Linea.LineaFact.TotalNoDescontable), RedondearMoneda(Linea.LineaFact.TotalNoDescontable, tiImporteLinea), 0)
         End If
      Next
   End If
   
'   Importe = RedondearMoneda(dblImporte, tiImporteLinea)
   Importe = dblImporte
End Function

Public Function Prioridad() As Integer
   On Error GoTo error
   
   'por defecto ponerlo al final
   Prioridad = 32000
   
   ' si es una garantia que tenga la maxima prioridad (menor numero).
   If EsGarantia Then
      Prioridad = 0
   Else
      If NodosFactura.Item(1).OrdImp > 0 Then
         Prioridad = NodosFactura.Item(1).OrdImp
      End If
   End If
   
   Exit Function

error:

End Function

Public Sub Destruir()
   For Each obj In NodosFactura
      obj.Destruir
'      Set obj = Nothing
   Next
   Set NodosFactura = Nothing
   
   For Each obj In NodosAplicados
      If TypeOf objeto Is Nodo Then
         obj.Destruir
      End If
'      Set obj = Nothing
   Next
   Set NodosAplicados = Nothing
     
   For Each obj In NuevasLineas
      obj.Destruir
'      Set obj = Nothing
   Next
   Set NuevasLineas = Nothing
     
   Set RNFs = Nothing
   Set CodConciertos = Nothing
   
   Set ProcAsist = Nothing
   Set REcos = Nothing

End Sub

