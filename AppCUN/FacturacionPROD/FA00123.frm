VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmReimpresion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reimpresi�n de facturas"
   ClientHeight    =   3405
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7545
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   7545
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "&Facturar"
      Default         =   -1  'True
      Height          =   510
      Left            =   6480
      TabIndex        =   2
      Top             =   180
      Width           =   1005
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   510
      Left            =   6480
      TabIndex        =   1
      Top             =   2790
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid grdFacturas 
      Height          =   3150
      Left            =   135
      TabIndex        =   0
      Top             =   135
      Width           =   6270
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      HeadLines       =   0
      Col.Count       =   5
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      RowNavigation   =   1
      MaxSelectedRows =   10
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   2328
      Columns(0).Caption=   "Historia"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7752
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Paciente"
      Columns(2).Name =   "Direccion"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "FecNacim"
      Columns(3).Name =   "FecNacim"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "FecIngreso"
      Columns(4).Name =   "FecIngreso"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   11060
      _ExtentY        =   5556
      _StockProps     =   79
      Caption         =   "Facturas a imprimir"
   End
End
Attribute VB_Name = "frmReimpresion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdFacturar_Click()
    Dim x As Integer
    Dim Cadena As String
    Dim MiSql As String
    Dim RsAsistencias As rdoResultset
    Dim EnBlanco As Boolean
    Dim anumfact() As String
    Dim contFact As Integer
  
    'Comprobamos que al menos una de las casillas tenga valor
    EnBlanco = True
    Me.grdFacturas.MoveFirst
  
    contFact = 0
    For x = 1 To 10
        If grdFacturas.Columns(0).Text <> "" Then
            contFact = contFact + 1
            ReDim Preserve anumfact(1 To contFact) As String
            anumfact(contFact) = grdFacturas.Columns(0).Text
            EnBlanco = False
        End If
      
        grdFacturas.MoveNext
    Next
    If EnBlanco Then
        MsgBox "Se debe introducir al menos un n�mero de factura", vbExclamation, "Atenci�n"
        If grdFacturas.Enabled = True Then grdFacturas.SetFocus
        Exit Sub
    End If
    
    Call ImprimirFact(anumfact)
  
    ' Vaciar el grid
    Me.grdFacturas.MoveFirst
    For x = 1 To grdFacturas.MaxSelectedRows
        grdFacturas.Columns(0).Text = ""
        grdFacturas.Columns(1).Text = ""
        grdFacturas.MoveNext
    Next
    Me.grdFacturas.SetFocus
    Me.grdFacturas.MoveFirst
End Sub


Private Sub cmdSalir_Click()
  Me.Hide
End Sub


Private Sub Form_Load()
   Dim x As Integer
'  Call Me.IniciarQRY
  For x = 1 To 10
    Me.grdFacturas.AddItem Space(0)
  Next
End Sub

  
Private Sub grdFacturas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
   Dim strFactura As String
   Dim rsFactura As rdoResultset
   Dim rsNombre As rdoResultset
   Dim objREco As New Persona
   Dim MiSql As String

  If grdFacturas.Columns(0).Text <> "" Then
    strFactura = Me.grdFacturas.Columns(0).Text
    MiSql = "Select * from FA0400 Where FA04NUMFACT = '" & strFactura & "' AND FA04NUMFACREAL IS NULL"
    Set rsFactura = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not rsFactura.EOF Then
      With objREco
         .Codigo = rsFactura("CI21CODPERSONA")
         grdFacturas.Columns(1).Text = .Name
      End With
    Else
      MsgBox "La factura no existe en la base de datos", vbExclamation + vbOKOnly, "Atenci�n"
      grdFacturas.Columns(0).Text = ""
      grdFacturas.Columns(1).Text = ""
      grdFacturas.Columns(2).Text = ""
      grdFacturas.Columns(3).Text = ""
    End If
  Else
    grdFacturas.Columns(1).Text = ""
    grdFacturas.Columns(2).Text = ""
    grdFacturas.Columns(3).Text = ""
  End If

End Sub
Private Sub grdFacturas_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 22 Then
      KeyAscii = fValidarNumFactura(KeyAscii, grdFacturas.Columns(1).Text)
    End If
End Sub






