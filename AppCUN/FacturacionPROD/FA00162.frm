VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frm_Listados 
   Caption         =   "Listados"
   ClientHeight    =   3630
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6750
   LinkTopic       =   "Form1"
   ScaleHeight     =   3630
   ScaleWidth      =   6750
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   495
      Left            =   3480
      TabIndex        =   6
      Top             =   2640
      Width           =   1335
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Height          =   495
      Left            =   1440
      TabIndex        =   5
      Top             =   2640
      Width           =   1335
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcFechaFin 
      Height          =   375
      Left            =   1200
      TabIndex        =   4
      Top             =   1200
      Width           =   1695
      _Version        =   65537
      _ExtentX        =   2990
      _ExtentY        =   661
      _StockProps     =   93
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicio 
      Height          =   375
      Left            =   1200
      TabIndex        =   1
      Top             =   600
      Width           =   1695
      _Version        =   65537
      _ExtentX        =   2990
      _ExtentY        =   661
      _StockProps     =   93
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listados"
      Height          =   1815
      Left            =   3360
      TabIndex        =   0
      Top             =   240
      Width           =   3015
      Begin VB.OptionButton OptListados 
         Caption         =   "Ingresos"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   9
         Top             =   1320
         Width           =   1215
      End
      Begin VB.OptionButton OptListados 
         Caption         =   "Cambios de camas"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   8
         Top             =   840
         Width           =   2055
      End
      Begin VB.OptionButton OptListados 
         Caption         =   "Cambios de responsabilidad"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   2415
      End
   End
   Begin VB.Label lblFechaFin 
      Caption         =   "Fecha Fin"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   855
   End
   Begin VB.Label lblFechaInicio 
      Caption         =   "Fecha Inicio"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   975
   End
End
Attribute VB_Name = "frm_Listados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Dim con As rdoConnection
Dim strAhora As String



Private Sub cmdImprimir_Click()
Dim strWhere As String
Dim formula(1, 2)
If OptListados(0).Value = True Then
    strWhere = "AD0500.AD05FECINIRESPON >= TO_DATE('" & dtcFechaInicio.Text & "','DD/MM/YYYY')" & _
            "AND AD0500.AD05FECINIRESPON <= TO_DATE('" & dtcFechaFin.Text & " 23:59', 'DD/MM/YYYY HH24:MI')"
    formula(1, 1) = "Fecha"
    formula(1, 2) = "'Desde " & dtcFechaInicio.Text & " Hasta " & dtcFechaFin.Text & "'"
    Call Imprimir_API(strWhere, "CambiosResponsabilidad.rpt", formula)
End If
If OptListados(1).Value = True Then
    strWhere = " AD1604J.AD16FECCAMBIO >= TO_DATE('" & dtcFechaInicio.Text & "','DD/MM/YYYY')" & _
            "AND AD1604J.AD16FECCAMBIO <= TO_DATE('" & dtcFechaFin.Text & " 23:59', 'DD/MM/YYYY HH24:MI')"
    formula(1, 1) = "Fecha"
    formula(1, 2) = "'Desde " & dtcFechaInicio.Text & " Hasta " & dtcFechaFin.Text & "'"
    Call Imprimir_API(strWhere, "CambiosCamas.rpt", formula)
End If
If OptListados(2).Value = True Then
    strWhere = " PR0466J.PR04FECINIACT >= TO_DATE('" & dtcFechaInicio.Text & "','DD/MM/YYYY')" & _
            "AND PR0466J.PR04FECINIACT <= TO_DATE('" & dtcFechaFin.Text & " 23:59', 'DD/MM/YYYY HH24:MI')"
    formula(1, 1) = "Fecha"
    formula(1, 2) = "'Desde " & dtcFechaInicio.Text & " Hasta " & dtcFechaFin.Text & "'"
    Call Imprimir_API(strWhere, "Ingresos.rpt", formula)
End If
If OptListados(0).Value = False And OptListados(1).Value = False And OptListados(2).Value = False Then
    MsgBox "Debe seleccionar un listado", vbOKOnly, "Listados"
End If
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
   Dim RS As rdoResultset
   Dim Dia, Mes, A�o As String
   Dim FechaAhora As String
   Set RS = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  strAhora = RS(0)
  FechaAhora = Format(strAhora, "dd/mm/yyyy")
  dtcFechaInicio.Date = DateAdd("d", -1, FechaAhora)
  dtcFechaFin.Date = DateAdd("d", -1, FechaAhora)
  ' Set con = rdoEnvironments(0).OpenConnection("Oracle73", False, False, "DSN=Oracle73;servername=EST_PROD.cun;uid=cun;pwd=TUY")
End Sub

