VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frm_FactMasiva 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Proceso de Facturaci�n Masiva"
   ClientHeight    =   3345
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6405
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3345
   ScaleWidth      =   6405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   " Tipo Econ�mico y Entidad "
      Height          =   2625
      Left            =   90
      TabIndex        =   12
      Top             =   630
      Width           =   6225
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1320
         Left            =   3525
         TabIndex        =   17
         Top             =   1200
         Width           =   2310
         Begin VB.OptionButton optHopit 
            Caption         =   "Hospitalizados"
            Height          =   195
            Left            =   225
            TabIndex        =   7
            Top             =   315
            Width           =   1950
         End
         Begin VB.OptionButton optAmbul 
            Caption         =   "Ambulatorios"
            Height          =   195
            Left            =   225
            TabIndex        =   8
            Top             =   630
            Width           =   1950
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Todos"
            Height          =   195
            Left            =   225
            TabIndex        =   9
            Top             =   945
            Width           =   1950
         End
      End
      Begin VB.TextBox txtCodTipEcon 
         Height          =   315
         Left            =   1560
         TabIndex        =   1
         Top             =   270
         Width           =   495
      End
      Begin VB.ComboBox cboCodTipEcon 
         Height          =   315
         Left            =   2160
         TabIndex        =   3
         Top             =   270
         Width           =   3615
      End
      Begin VB.TextBox txtCodEntidad 
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   750
         Width           =   495
      End
      Begin VB.ComboBox cboCodEntidad 
         Height          =   315
         Left            =   2160
         TabIndex        =   4
         Top             =   750
         Width           =   3615
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   45
         TabIndex        =   10
         Top             =   2160
         Visible         =   0   'False
         Width           =   1575
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1575
         TabIndex        =   5
         Tag             =   "Fecha Inicio"
         Top             =   1230
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1575
         TabIndex        =   6
         Tag             =   "Fecha Inicio"
         Top             =   1710
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.CommandButton cmdFacturar 
         Caption         =   "Facturar"
         Height          =   375
         Left            =   1800
         TabIndex        =   11
         Top             =   2160
         Width           =   1575
      End
      Begin VB.Label lblFecInicio 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Inicio:"
         Height          =   255
         Left            =   45
         TabIndex        =   16
         Top             =   1275
         Width           =   1440
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Final:"
         Height          =   255
         Left            =   45
         TabIndex        =   15
         Top             =   1755
         Width           =   1440
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo Econ�mico:"
         Height          =   255
         Left            =   45
         TabIndex        =   14
         Top             =   390
         Width           =   1440
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Entidad:"
         Height          =   255
         Left            =   45
         TabIndex        =   13
         Top             =   870
         Width           =   1440
      End
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1665
      TabIndex        =   0
      Tag             =   "Fecha Inicio"
      Top             =   135
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Mes a Facturar :"
      Height          =   255
      Left            =   0
      TabIndex        =   18
      Top             =   225
      Visible         =   0   'False
      Width           =   1440
   End
End
Attribute VB_Name = "frm_FactMasiva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim QrySelec(1 To 15) As New rdoQuery

Private Type typeRNF
  Asistencia As Long
  CodCateg As String
  CodRNF As String
  Cantidad As Long
  fecha As String
End Type
Dim MatrizRNF() As typeRNF
Dim CadenaConceptos As String

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf

Dim NumHisto As Long
Dim PrimeraVez As Boolean
Dim NumFactura As String
Dim TotalPacientes As Long

Private Sub cboCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If

End Sub

Private Sub cboCodTipEcon_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If
End Sub

Private Sub cboCodTipEcon_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodTipEcon.Clear
  
  SQL = "Select CI32DESTIPECON From CI3200 Where CI32INDRESPECO = -1"
  Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
  If Not RS.EOF Then
    RS.MoveLast
    RS.MoveFirst
    While Not RS.EOF
      cboCodTipEcon.AddItem RS("CI32DESTIPECON") & ""
      RS.MoveNext
    Wend
  End If
End Sub

Private Sub cboCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If

End Sub

Private Sub cmdAceptar_Click()
   Dim Seguir As Boolean
   Dim MiSqL As String
   Dim MiRs As rdoResultset
   Dim sqlPersona As String
   Dim rsPersona As rdoResultset
   Dim Asistencia As Long
   Dim proceso As Long
   'Dim Persona As String
   Dim FechaInicio As String
   Dim FechaFin As String
   Dim Texto As String
   Dim Historia As Long
   Dim FecIni As String
   Dim FecFin As String
   Dim sqlIns37 As String
   Dim TipoAsist As Integer
   Dim Codigo As Long

  On Error GoTo ErrorenGrabacion
  
  objApp.rdoConnect.BeginTrans
  Seguir = ComprobarCampos()
  If Me.optAmbul.Value = True Then
    TipoAsist = 2
  Else
    TipoAsist = 1
  End If
  If Seguir Then
    
    If Not IsNull(Me.SDCFechaInicio.Date) Then
      FechaInicio = CStr(Me.SDCFechaInicio.Date)
    Else
      FechaInicio = ""
    End If
    If Not IsNull(Me.SDCFechaFin.Date) Then
      FechaFin = CStr(Me.SDCFechaFin.Date)
    Else
      FechaFin = ""
    End If
    
    'Seleccionamos todas los procesos-asistencia de ese tipo econ�mico.
    MiSqL = "Select AD0100.AD01CODASISTENCI, AD0700.AD07CODPROCESO,AD0700.CI21CODPERSONA," & _
                 "AD0100.CI22NUMHISTORIA,AD12CODTIPOASIST,AD0100.AD01FECINICIO,AD0100.AD01FECFIN " & _
                 "From AD0100,AD1100,AD2500,AD0700 " & _
                 "Where AD0100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
                 "AND AD1100.AD07CODPROCESO = AD0700.AD07CODPROCESO " & _
                 "AND AD1100.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
                 "AND CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "' "
    If Trim(Me.txtCodEntidad.Text) <> "" Then
        MiSqL = MiSqL & " AND CI13CODENTIDAD = '" & Me.txtCodEntidad & "' "
    End If
    'MiSql = MiSql & "AND AD01CODASISTENCI NOT IN (SELECT AD01CODASISTENCI FROM FA0400 " & _
                    "WHERE FA0400.AD01CODASISTENCI = AD1100.AD01CODASISTENCI)"
    If Me.optAmbul.Value = True Then
      MiSqL = MiSqL & " AND AD12CODTIPOASIST = 2 "
    ElseIf Me.optHopit.Value = True Then
      MiSqL = MiSqL & " AND AD12CODTIPOASIST = 1 "
    End If
    If Me.txtCodEntidad <> "NA" Then
      If optHopit.Value = True Then
        If FechaInicio <> "" Then
          MiSqL = MiSqL & " AND AD01FECINICIO >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
        End If
        If FechaFin <> "" Then
          MiSqL = MiSqL & " AND AD01FECINICIO <= TO_DATE('" & FechaFin & "','DD/MM/YYYY') "
        End If
      ElseIf Me.optAmbul.Value = True Then
        MiSqL = MiSqL & " AND AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
        MiSqL = MiSqL & " AND AD01FECFIN <= TO_DATE('" & FechaFin & "','DD/MM/YYYY') "
        'MiSqL = MiSqL & " OR AD01FECFIN IS NULL)"
      End If
      MiSqL = MiSqL & " AND AD11FECFIN IS NULL AND ad2500.ad25fecfin is null"
    Else
      If optHopit.Value = True Then
        If FechaInicio <> "" Then
          MiSqL = MiSqL & " AND AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
        End If
        If FechaFin <> "" Then
          MiSqL = MiSqL & " AND AD01FECFIN <= TO_DATE('" & FechaFin & "','DD/MM/YYYY') "
        End If
      ElseIf Me.optAmbul.Value = True Then
        MiSqL = MiSqL & " AND AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
      End If
    End If
    
    If optHopit.Value = True Then
      If FechaInicio <> "" Then
        MiSqL = MiSqL & " AND AD01FECINICIO >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
      End If
      If FechaFin <> "" Then
        MiSqL = MiSqL & " AND AD01FECINICIO <= TO_DATE('" & FechaFin & "','DD/MM/YYYY') "
      End If
    ElseIf Me.optAmbul.Value = True Then
      MiSqL = MiSqL & " AND (AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
      MiSqL = MiSqL & " OR AD01FECFIN IS NULL)"
    End If
    MiSqL = MiSqL & " ORDER BY AD01FECINICIO"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      TotalPacientes = MiRs.RowCount
      While Not MiRs.EOF
'        sqlPersona = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL From CI2200 Where CI21CODPERSONA = " & MiRs("CI21CODPERSONA") & ""
'        Set rsPersona = objApp.rdoConnect.OpenResultset(sqlPersona, 3)
'        If Not rsPersona.EOF Then
'          Persona = rsPersona("CI22NOMBRE") & " " & rsPersona("CI22PRIAPEL") & " " & rsPersona("CI22SEGAPEL")
'        End If
        If Not IsNull(MiRs("AD01CODASISTENCI")) Then
          Asistencia = CLng(MiRs("AD01CODASISTENCI"))
        Else
          Asistencia = 0
        End If
        If Not IsNull(MiRs("AD07CODPROCESO")) Then
          proceso = CLng(MiRs("AD07CODPROCESO"))
        Else
          proceso = 0
        End If
        If Not IsNull(MiRs("CI22NUMHISTORIA")) Then
          Historia = CLng(MiRs("CI22NUMHISTORIA"))
        Else
          Historia = 0
        End If
        If Not IsNull(MiRs("AD01FECINICIO")) Then
          FecIni = Format(MiRs("AD01FECINICIO"), "DD/MM/YYYY")
        Else
          FecIni = Null
        End If
        If Not IsNull(MiRs("AD01FECFIN")) Then
          FecFin = Format(MiRs("AD01FECFIN"), "DD/MM/YYYY")
        Else
          FecFin = ""
        End If
        Codigo = fNextClave("FA37CODPAC", "FA3700")
        sqlIns37 = "Insert Into FA3700 (FA37CODPAC, AD01CODASISTENCI, AD07CODPROCESO, CI32CODTIPECON, " & _
                   "CI13CODENTIDAD, AD12CODTIPOASIST, FA37INDFACT, FA37FECINICIO, FA37FECFIN) Values (" & _
                   Codigo & " ," & Asistencia & "," & proceso & ",'" & Me.txtCodTipEcon & "','" & _
                   Me.txtCodEntidad.Text & "'," & TipoAsist & ",0, TO_DATE('" & FechaInicio & "','DD/MM/YYYY') " & _
                   ",TO_DATE('" & FechaFin & "','DD/MM/YYYY'))"
        objApp.rdoConnect.Execute sqlIns37
        'Texto = "-1" & Chr(9) & Persona & Chr(9) & Proceso & Chr(9) & Asistencia & Chr(9) & Historia & Chr(9) & FecIni & Chr(9) & FecFin
        'grdPacientes.AddItem Texto
        MiRs.MoveNext
      Wend
    End If
  End If
  objApp.rdoConnect.CommitTrans
  Me.cmdAceptar.Visible = False
  Me.cmdFacturar.Visible = True
Exit Sub
ErrorenGrabacion:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en la carga de B.D.", vbCritical + vbOKOnly, "Aviso"
  Exit Sub
End Sub




Private Function ComprobarCampos() As Boolean
    
    'Comprobamos que haya un tipo econ�mico
    If Trim(Me.txtCodTipEcon.Text) = "" Then
      MsgBox "Se debe seleccionar un Tipo Econ�mico.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodTipEcon.Enabled = True Then
        Me.txtCodTipEcon.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que haya una entidad
    If Trim(Me.txtCodEntidad.Text) = "" Then
      MsgBox "Se debe seleccionar una entidad.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodEntidad.Enabled = True Then
        Me.txtCodEntidad.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que la fecha de inicio sea correcta
    If Not IsNull(Me.SDCFechaInicio.Date) Then
      If Not IsDate(Me.SDCFechaInicio.Date) Then
        MsgBox "La Fecha de Inicio no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaInicio.Enabled = True Then
          Me.SDCFechaInicio.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de finalizaci�n sea correcta
    If Not IsNull(Me.SDCFechaFin.Date) Then
      If Not IsDate(Me.SDCFechaFin.Date) Then
        MsgBox "La Fecha Fin no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaFin.Enabled = True Then
          Me.SDCFechaFin.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de inicio no sea mayor que la de finalizaci�n.
    If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
      MsgBox "La fecha de inicio no puede ser superior a la de finalizaci�n.", vbOKOnly + vbInformation, "Aviso"
      If Me.SDCFechaFin.Enabled = True Then
        Me.SDCFechaFin.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    ComprobarCampos = True
End Function


Private Sub cmdFacturar_Click()
Dim Cont As Integer
Dim TotLineas As Integer
Dim ProcLinea As Long
Dim AsisLinea As Long
Dim FecIniLinea As String
Dim FecFinLinea As String
Dim HistLinea As Long
Dim SqlSel37 As String
Dim rs37 As rdoResultset
Dim Sql01 As String
Dim rs01 As rdoResultset
Dim FecIni As String
Dim FecFin As String
Dim Historia As Long
Dim sqlUpdate As String

   Open "C:\F" & Format(Date, "yymmdd") & Format(Time, "HHMM") & ".LOG" For Append As #1
   
'  Open "C:\ESTAD" & Me.txtCodTipEcon.Text & Me.txtCodEntidad.Text & ".LOG" For Append As #1
  SqlSel37 = "Select * From FA3700 Where CI32CODTIPECON = '" & Me.txtCodTipEcon & "'" & _
             " And FA37INDFACT = 0" & _
             " And FA37MES >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY')" & _
             " And FA37MES <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY')"
  If Me.txtCodEntidad.Text <> "" Then
    SqlSel37 = SqlSel37 & " AND CI13CODENTIDAD = '" & Me.txtCodEntidad & "' "
  End If
  If Me.optHopit.Value = True Then
     SqlSel37 = SqlSel37 & " AND AD12CODTIPOASIST = 1"
  ElseIf Me.optAmbul.Value = True Then
    SqlSel37 = SqlSel37 & " AND AD12CODTIPOASIST = 2"
  End If
  Set rs37 = objApp.rdoConnect.OpenResultset(SqlSel37, 3)
  If Not rs37.EOF Then
    rs37.MoveLast
    rs37.MoveFirst
    MsgBox "Hay " & rs37.RowCount & " Historias para facturar"
     While Not rs37.EOF
     Cont = Cont + 1
     Me.Caption = Cont & "/" & rs37.RowCount & "     (" & rs37("AD07CODPROCESO") & _
                                                  " - " & rs37("AD01CODASISTENCI") & ")"
     Write #1, "*********"
     Write #1, Cont & "/" & rs37.RowCount & "     (" & rs37("AD07CODPROCESO") & _
                                                  " - " & rs37("AD01CODASISTENCI") & ")"
                                                  
'      frm_FactMasiva.Caption = "Facturando Paciente n� " & Cont & " de " & TotalPacientes
'      If grdPacientes.Columns(0).Value = -1 Then
      If Not IsNull(rs37("AD07CODPROCESO")) Then
        ProcLinea = rs37("AD07CODPROCESO")
      Else
        ProcLinea = 0
      End If
      If Not IsNull(rs37("AD01CODASISTENCI")) Then
        AsisLinea = rs37("AD01CODASISTENCI")
      Else
        AsisLinea = 0
      End If
      Sql01 = "Select * from AD0100 where AD01CODASISTENCI = " & AsisLinea
      Set rs01 = objApp.rdoConnect.OpenResultset(Sql01, 3)
      If Not rs01.EOF Then
        If Not IsNull(rs01("CI22NUMHISTORIA")) Then
          Historia = rs01("CI22NUMHISTORIA")
        Else
          Historia = 0
        End If
        
        If Not IsNull(rs37("FA37FECINICIO")) Then
          FecIni = Format(rs37("FA37FECINICIO"), "DD/MM/YYYY")
        Else
            If Not IsNull(rs01("AD01FECINICIO")) Then
              FecIni = Format(rs01("AD01FECINICIO"), "DD/MM/YYYY")
            Else
              FecIni = ""
            End If
        End If
        
        If Not IsNull(rs37("FA37FECFIN")) Then
          FecFin = Format(rs37("FA37FECFIN"), "DD/MM/YYYY")
        Else
            If Not IsNull(rs01("AD01FECFIN")) Then
              FecFin = Format(rs01("AD01FECFIN"), "DD/MM/YYYY")
            Else
              FecFin = ""
            End If
        End If
        
        Call FacturaMasiva(Historia, ProcLinea, AsisLinea, FecIni, FecFin, rs37("FA37MES"))
        If Me.txtCodTipEcon = "S" And Me.txtCodEntidad <> "NA" Then
          Call frm_Impresion.ImprimirHASSFactura(ProcLinea, AsisLinea, 2, 2000)
        End If
      End If
      sqlUpdate = "Update FA3700 Set FA37INDFACT = 1 Where AD01CODASISTENCI = " & AsisLinea & _
                  " AND AD07CODPROCESO = " & ProcLinea
      objApp.rdoConnect.Execute sqlUpdate
      rs37.MoveNext
    Wend
  End If
  Close #1
  MsgBox "Proceso Finalizado", vbOKOnly + vbInformation, "Facturaci�n Masiva"
End Sub


Private Sub Form_Load()
  Call modFacturacion.IniciarQRYFact
  Call modFacturacion.InicQrySelec
  Call modFacturacion.IniciarQRYMasiva
  objCW.blnAutoDisconnect = False
End Sub

Private Sub txtCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodEntidad.Text = RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If


End Sub
Private Sub txtCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI32DESTIPECON From CI3200 Where CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodTipEcon.Text = RS("CI32DESTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodTipEcon.Text = ""
      Me.txtCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If


End Sub
Private Sub cboCodEntidad_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If
End Sub

Private Sub cboCodEntidad_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodEntidad.Clear
  
  If Trim(txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI32CODTIPECON = '" & Me.txtCodTipEcon & "' " & _
          "And CI13FECFIVGENT IS NULL"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        cboCodEntidad.AddItem RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    End If
  Else
    MsgBox "Debe seleccionar un tipo econ�mico", vbInformation + vbOKOnly, "Aviso"
  End If
End Sub

