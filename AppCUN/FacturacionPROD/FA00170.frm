VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_PacientesporDpto 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Listado de Pacientes sin Factura por Departamento"
   ClientHeight    =   2400
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7005
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2400
   ScaleWidth      =   7005
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   420
      Left            =   1350
      TabIndex        =   9
      Top             =   1755
      Width           =   870
   End
   Begin VB.Frame Frame1 
      Caption         =   " Criterios de Selecci�n "
      Height          =   2310
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   6900
      Begin ComctlLib.ListView lstTiposEcon 
         Height          =   1410
         Left            =   3015
         TabIndex        =   7
         Top             =   765
         Width           =   3660
         _ExtentX        =   6456
         _ExtentY        =   2487
         View            =   2
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   420
         Left            =   270
         TabIndex        =   6
         Top             =   1710
         Width           =   870
      End
      Begin VB.CheckBox chkJur�dicas 
         Caption         =   "Responsable persona jur�dica"
         Height          =   195
         Left            =   405
         TabIndex        =   5
         Top             =   1080
         Width           =   2715
      End
      Begin VB.CheckBox chkFisicas 
         Caption         =   "Responsable persona f�sica"
         Height          =   195
         Left            =   405
         TabIndex        =   4
         Top             =   765
         Width           =   2715
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1665
         TabIndex        =   1
         Tag             =   "Fecha Inicio"
         Top             =   315
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin vsViewLib.vsPrinter vsPrinter 
         Height          =   195
         Left            =   135
         TabIndex        =   8
         Top             =   1845
         Visible         =   0   'False
         Width           =   330
         _Version        =   196608
         _ExtentX        =   582
         _ExtentY        =   344
         _StockProps     =   229
         Appearance      =   1
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ConvInfo        =   1418783674
         PageBorder      =   0
         Preview         =   0   'False
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   4905
         TabIndex        =   10
         Tag             =   "Fecha Inicio"
         Top             =   315
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Inicio:"
         Height          =   240
         Index           =   2
         Left            =   225
         TabIndex        =   3
         Top             =   360
         Width           =   1320
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha Final:"
         Height          =   240
         Index           =   3
         Left            =   3375
         TabIndex        =   2
         Top             =   360
         Width           =   1320
      End
   End
End
Attribute VB_Name = "frm_PacientesporDpto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type Facturas
  Historia As String
  Persona As String
  Asistencia As Long
  FecInicio As String
  FecFinal As String
  TipoEcon As String
  CodEntid As String
  TipoAsis As String
  Departam As String
End Type

Dim arFact() As Facturas

Const ConvY = 54.0809
Const ConvX = 54.6101086

Dim FechaSis As Date

Sub ImprimirDatos()
Dim Lineas As Integer
Dim Cont As Long
Dim PacAnt As String
Dim DeptAnt As String

  On Error Resume Next
    Lineas = UBound(arFact)
    If Err <> 0 Then
      Lineas = 0
    End If
  On Error GoTo 0
  
  DeptAnt = ""
  PacAnt = ""
  If Lineas <> 0 Then
    vsPrinter.Action = paStartDoc
  End If
  For Cont = 1 To Lineas
    If arFact(Cont).Departam <> DeptAnt Then
      If DeptAnt <> "" Then
        vsPrinter.NewPage
      End If
      vsPrinter.CurrentX = 20 * ConvX
      vsPrinter.FontBold = True
      vsPrinter.FontSize = 14
      vsPrinter.Text = arFact(Cont).Departam
      DeptAnt = arFact(Cont).Departam
      vsPrinter.Paragraph = ""
    End If
    If PacAnt <> arFact(Cont).Persona Then
      vsPrinter.Paragraph = ""
      vsPrinter.CurrentX = 30 * ConvY
      vsPrinter.FontBold = False
      vsPrinter.FontSize = 10
      vsPrinter.Text = arFact(Cont).Historia & " " & arFact(Cont).Persona
      PacAnt = arFact(Cont).Persona
      vsPrinter.Paragraph = ""
    End If
    vsPrinter.FontSize = 9
    vsPrinter.FontBold = False
    vsPrinter.CurrentX = 70 * ConvX
    vsPrinter.Text = arFact(Cont).Asistencia
    vsPrinter.CurrentX = 110 * ConvX
    vsPrinter.Text = arFact(Cont).TipoEcon & "/" & arFact(Cont).CodEntid
    vsPrinter.CurrentX = 150 * ConvX
    vsPrinter.Text = arFact(Cont).FecInicio & "-" & arFact(Cont).FecFinal
    vsPrinter.Paragraph = ""
    If vsPrinter.CurrentY > 270 * ConvY Then
      vsPrinter.NewPage
    End If
  Next
  vsPrinter.Action = paEndDoc
End Sub

Private Sub cmdImprimir_Click()
Dim X
Dim Texto As String
Dim lstItem As ListItem
Dim TipoEcon As String
Dim MiRs As rdoResultset
Dim MiSql As String
Dim QryDatos As New rdoQuery

  TipoEcon = ""
  'Buscaremos cual es la entidad seleccionada
  For Each lstItem In Me.lstTiposEcon.ListItems
    If lstItem.Selected = True Then
      TipoEcon = TipoEcon & "'" & lstItem.Tag & "',"
    End If
  Next
  If Trim(Me.SDCFechaFin.Date) = "" Then
    Me.SDCFechaFin.Date = FechaSis
  End If
         
  MiSql = "Select /*+ RULE */ FA0404J.CI22NUMHISTORIA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE AS Nombre, " & _
          "AD01CODASISTENCI, AD01FECINICIO, CI32CODTIPECON, CI13CODENTIDAD, AD01FECFIN, AD12CODTIPOASIST, " & _
          "AD02DESDPTO, RESPECON, CI33CODTIPPERS " & _
          "From FA0404J, CI2200 " & _
          "Where FA0404J.CI22NUMHISTORIA = CI2200.CI22NUMHISTORIA "
  If Not IsNull(Me.SDCFechaInicio.Date) Then
    MiSql = MiSql & "And AD01FECFIN >= TO_DATE('" & Format(SDCFechaInicio.Date, "DD/MM/YYYY") & "','DD/MM/YYYY') "
  End If
  If Not IsNull(Me.SDCFechaFin.Date) Then
    MiSql = MiSql & "And AD01FECFIN <= TO_DATE('" & Format(SDCFechaFin.Date, "DD/MM/YYYY") & "','DD/MM/YYYY') "
  End If
  If Me.chkFisicas.Value = True And Me.chkJur�dicas.Value = True Then
    MiSql = MiSql & "And CI33CODTIPPERS IN (1,2) "
  ElseIf Me.chkFisicas.Value = True And Me.chkJur�dicas.Value = False Then
    MiSql = MiSql & "And CI33CODTIPPERS IN (2) "
  ElseIf Me.chkFisicas.Value = False And Me.chkJur�dicas.Value = True Then
    MiSql = MiSql & "And CI33CODTIPPERS IN (1) "
  End If
  If Trim(TipoEcon) <> "" Then
    MiSql = MiSql & "And CI32CODTIPECON IN (" & Left(TipoEcon, Len(TipoEcon) - 1) & ") "
  End If
  MiSql = MiSql & " Order by AD02DESDPTO Asc, Nombre Asc"
  QryDatos.SQL = MiSql
  Set QryDatos.ActiveConnection = objApp.rdoConnect
  QryDatos.Prepared = True
  
  Set MiRs = QryDatos.OpenResultset
  If Not MiRs.EOF Then
    X = 0
    While Not MiRs.EOF
      X = X + 1
      ReDim Preserve arFact(1 To X)
      arFact(X).Historia = MiRs("CI22NUMHISTORIA")
      arFact(X).Persona = MiRs("Nombre")
      arFact(X).Asistencia = MiRs("AD01CODASISTENCI")
      arFact(X).TipoEcon = MiRs("CI32CODTIPECON")
      arFact(X).CodEntid = MiRs("CI13CODENTIDAD")
      arFact(X).FecInicio = Format(MiRs("AD01FECINICIO"), "DD/MM/YYYY")
      arFact(X).FecFinal = Format(MiRs("AD01FECFIN"), "DD/MM/YYYY")
      arFact(X).Departam = MiRs("AD02DESDPTO")
      MiRs.MoveNext
    Wend
  End If
  
  Call ImprimirDatos
End Sub

Private Sub cmdSalir_Click()
  
  Unload Me
  
End Sub

Private Sub Form_Load()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim TipEcon As New ListItem
Dim Texto As String
  
  MiSql = "Select SYSDATE From DUAL"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  FechaSis = MiRs(0)
  
  Me.SDCFechaInicio.Date = FechaSis
  Me.SDCFechaFin.Date = FechaSis
    
  MiSql = "Select CI32CODTIPECON, CI32DESTIPECON From CI3200 " & _
          "Where CI32INDRESPECO = -1 And CI32FECFIVGTEC Is Null"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Me.lstTiposEcon.ListItems.Clear
    While Not MiRs.EOF
      Texto = MiRs("CI32DESTIPECON")
      Set TipEcon = Me.lstTiposEcon.ListItems.Add(, , Texto)
      TipEcon.Tag = MiRs("CI32CODTIPECON")
      MiRs.MoveNext
    Wend
  End If

End Sub

Private Sub SDCFechaFin_LostFocus()
  If Trim(Me.SDCFechaFin.Date) <> "" And Trim(Me.SDCFechaInicio.Date) <> "" Then
    If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
      MsgBox "La fecha final no puede ser menor que la fecha de inicio", vbExclamation + vbOKOnly, "Aviso"
      Me.SDCFechaFin.Date = Me.SDCFechaInicio.Date
    End If
  End If
End Sub

Private Sub SDCFechaInicio_LostFocus()
  If Trim(Me.SDCFechaInicio.Date) <> "" And Trim(Me.SDCFechaFin.Date) <> "" Then
    If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
      MsgBox "La fecha final no puede ser menor que la fecha de inicio", vbExclamation + vbOKOnly, "Aviso"
      Me.SDCFechaInicio.Date = Me.SDCFechaFin.Date
    End If
  End If
End Sub

