VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frm_AsignarFactura 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asignar Fecha y N�mero de Factura"
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6645
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   6645
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.ListView lstEntidades 
      Height          =   3165
      Left            =   90
      TabIndex        =   10
      Top             =   405
      Width           =   4605
      _ExtentX        =   8123
      _ExtentY        =   5583
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   ""
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   ""
         Object.Tag             =   ""
         Text            =   ""
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   ""
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   555
      Left            =   4815
      TabIndex        =   6
      Top             =   4455
      Width           =   1455
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   555
      Left            =   4815
      TabIndex        =   5
      Top             =   3825
      Width           =   1455
   End
   Begin VB.TextBox txtNumFact 
      Height          =   285
      Left            =   2520
      TabIndex        =   0
      Top             =   4140
      Width           =   1320
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2160
      TabIndex        =   1
      Tag             =   "Fecha Inicio"
      Top             =   3735
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2115
      TabIndex        =   2
      Tag             =   "Fecha Inicio"
      Top             =   4545
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin ComctlLib.ListView lstAsistencias 
      Height          =   3165
      Left            =   4725
      TabIndex        =   11
      Top             =   405
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   5583
      View            =   3
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   ""
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Fecha en que se factur�..:"
      Height          =   255
      Index           =   3
      Left            =   180
      TabIndex        =   12
      Top             =   3780
      Width           =   2010
   End
   Begin VB.Label lbl13 
      Alignment       =   1  'Right Justify
      Caption         =   "13/"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   9
      Top             =   4185
      Width           =   345
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo de Asistencia"
      Height          =   255
      Index           =   2
      Left            =   4725
      TabIndex        =   8
      Top             =   135
      Width           =   1785
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo Econ�mico / Entidad"
      Height          =   255
      Index           =   1
      Left            =   135
      TabIndex        =   7
      Top             =   135
      Width           =   4530
   End
   Begin VB.Label Label1 
      Caption         =   "N� de Factura .................:"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   4
      Top             =   4185
      Width           =   2010
   End
   Begin VB.Label lblFecFin 
      Caption         =   "Fecha de Factura............:"
      Height          =   255
      Left            =   180
      TabIndex        =   3
      Top             =   4590
      Width           =   1965
   End
End
Attribute VB_Name = "frm_AsignarFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub CargarEntidadesAsistencias()
Dim SqLeNtI As String
Dim RsEnTi As rdoResultset
Dim SqlAsist As String
Dim rsAsist As rdoResultset
Dim lstItem As ListItem

  SqLeNtI = "Select CI32CODTIPECON, CI13CODENTIDAD, CI13DESENTIDAD From CI1300 Where CI13FECFIVGENT is Null " & _
            "And FA09CODNODOCONC Is Not Null"
  Set RsEnTi = objApp.rdoConnect.OpenResultset(SqLeNtI, 3)
  If Not RsEnTi.EOF Then
    RsEnTi.MoveLast
    RsEnTi.MoveFirst
    While Not RsEnTi.EOF
      If RsEnTi("CI32CODTIPECON") = "P" Then
        Set lstItem = lstEntidades.ListItems.Add(, , RsEnTi("CI13DESENTIDAD") & " " & RsEnTi("CI13CODENTIDAD"))
        lstItem.SubItems(1) = RsEnTi("CI32CODTIPECON")
        lstItem.SubItems(2) = RsEnTi("CI13CODENTIDAD")
      Else
        Set lstItem = lstEntidades.ListItems.Add(, , RsEnTi("CI13DESENTIDAD"))
        lstItem.SubItems(1) = RsEnTi("CI32CODTIPECON")
        lstItem.SubItems(2) = RsEnTi("CI13CODENTIDAD")
      End If
      RsEnTi.MoveNext
    Wend
  End If
  SqlAsist = "Select AD12CODTIPOASIST, AD12DESTIPOASIST From AD1200"
  Set rsAsist = objApp.rdoConnect.OpenResultset(SqlAsist, 3)
  If Not rsAsist.EOF Then
    rsAsist.MoveLast
    rsAsist.MoveFirst
    While Not rsAsist.EOF
      Set lstItem = lstAsistencias.ListItems.Add(, "K" & CStr(rsAsist("AD12CODTIPOASIST")), rsAsist("AD12DESTIPOASIST"))
      rsAsist.MoveNext
    Wend
  End If
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdAceptar_Click()
Dim lstItem As ListItem
Dim TipoEcon As String
Dim Entidad As String
Dim factura As String
Dim rsFact As rdoResultset
Dim sqlFact As String
Dim Actual As String
Dim Respuesta As Integer
Dim Texto As String
Dim NomEntidad As String
Dim Cont As Integer
Dim SQL As String

  'Buscaremos cual es la entidad seleccionada
  For Each lstItem In lstEntidades.ListItems
    If lstItem.Selected = True Then
      TipoEcon = lstItem.SubItems(1)
      Entidad = lstItem.SubItems(2)
      NomEntidad = lstItem.Text
    End If
  Next
      SQL = "Update FA0400 Set FA04NUMFACT = '" & factura & "' " & _
            ", FA04FECFACTURA = TO_DATE('" & Me.SDCFechaFin & "','DD/MM/YYYY') " & _
            "Where FA04NUMFACT LIKE '" & Actual & "%' "
      If Trim(Me.SDCFechaInicio.Date) <> "" Then
        SQL = SQL & "And FA04FECFACTURA = TO_DATE('" & Me.SDCFechaFin & "','DD/MM/YYYY')"
      Else
        SQL = SQL & "And FA04FECFACTURA IS NULL"
      End If
      objApp.rdoConnect.Execute SQL
'    End If
'  Next
Exit Sub
Salir:
  MsgBox "Salir"
  Unload Me
  
End Sub

Private Sub cmdAceptar2_Click()
Dim lstItem As ListItem
Dim Asistencias() As String
Dim TipoEcon As String
Dim Entidad As String
Dim factura As String
Dim rsFact As rdoResultset
Dim sqlFact As String
Dim Actual As String
Dim Respuesta As Integer
Dim Texto As String
Dim NomEntidad As String
Dim Cont As Integer
Dim SQL As String

  'Buscaremos cual es la entidad seleccionada
  For Each lstItem In lstEntidades.ListItems
    If lstItem.Selected = True Then
      TipoEcon = lstItem.SubItems(1)
      Entidad = lstItem.SubItems(2)
      NomEntidad = lstItem.Text
    End If
  Next
  'Buscaremos los tipos de asistencia seleccionados y los meteremos en un array.
  Cont = 0
  For Each lstItem In lstAsistencias.ListItems
    If lstItem.Selected = True Then
      Cont = Cont + 1
      ReDim Preserve Asistencias(1 To 2, 1 To Cont)
      Asistencias(1, Cont) = Right(lstItem.Key, Len(lstItem.Key) - 1)
      Asistencias(2, Cont) = lstItem.Text
    End If
  Next
  
  
  'Conseguimos el n�mero de factura en caso de que no haya sido introducido
  If Trim(Me.txtNumFact.Text) <> "" Then
    'Comprobaremos que el n�mero de factura es n�merico
    If Not IsNumeric(Trim(txtNumFact.Text)) Then
      MsgBox "Se ha introducido un n�mero de factura que no es num�rico", vbOKOnly + vbExclamation, "Atenci�n"
      GoTo Salir
    End If
    factura = "13/" & txtNumFact.Text
    sqlFact = "Select FA04CODFACT From FA0400 Where FA04NUMFACT = '" & factura & "'"
    Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact)
    If Not rsFact.EOF Then
      If MsgBox("La factura " & factura & " ya existe. �Desea asignar ese n�mero?", vbQuestion + vbYesNo, "Aviso") = vbNo Then
        GoTo Salir
      End If
    End If
  Else
    'Asignamos un nuevo n�mero
    factura = "13/" & fNextClave("FA04NUMFACT", "FA0400")
  End If
  
  'Por cada uno de las asistencias preguntamos si se quiere que se asigne
  For Cont = 1 To UBound(Asistencias, 2)
    Actual = TipoEcon & Entidad & Asistencias(1, Cont)
    Texto = "�Desea asignar a las facturas de " & NomEntidad & " " & Asistencias(2, Cont) & Chr(10) & Chr(13)
    Texto = Texto & "de fecha " & Format(Me.SDCFechaInicio, "dd/mm/yyyy") & Chr(10) & Chr(13)
    Texto = Texto & "el n�mero de factura " & factura & Chr(13) & Chr(10)
    Texto = Texto & "con fecha " & Format(Me.SDCFechaFin, "dd/mm/yyyy")
    Respuesta = MsgBox(Texto, vbYesNo + vbOKOnly, "Asignaci�n de n�mero de factura")
    If Respuesta = vbYes Then
      SQL = "Update FA0400 Set FA04NUMFACT = '" & factura & "' " & _
            ", FA04FECFACTURA = TO_DATE('" & Me.SDCFechaFin & "','DD/MM/YYYY') " & _
            "Where FA04NUMFACT LIKE '" & Actual & "%' "
      If Trim(Me.SDCFechaInicio.Date) <> "" Then
        SQL = SQL & "And FA04FECFACTURA = TO_DATE('" & Me.SDCFechaFin & "','DD/MM/YYYY')"
      Else
        SQL = SQL & "And FA04FECFACTURA IS NULL"
      End If
      objApp.rdoConnect.Execute SQL
    End If
  Next
Exit Sub
Salir:
  MsgBox "Salir"
  Unload Me
  
End Sub


Private Sub Form_Load()
  Call CargarEntidadesAsistencias
End Sub


Private Sub txtNumFact_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtNumFact_LostFocus()
  If Trim(txtNumFact.Text) <> "" Then
    If txtNumFact <= 1500000 Then
      MsgBox "El n�mero de factura debe ser superior a 1500000", vbExclamation + vbOKOnly, "Atenci�n"
      txtNumFact.Text = ""
      If txtNumFact.Enabled = True Then
        txtNumFact.SetFocus
      End If
    End If
  End If
    
End Sub


