VERSION 5.00
Begin VB.Form frm_ImpresionMasiva 
   Caption         =   "Impresi�n de facturas de ACUNSA"
   ClientHeight    =   1995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   1995
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Default         =   -1  'True
      Height          =   465
      Left            =   3330
      TabIndex        =   4
      Top             =   1395
      Width           =   1230
   End
   Begin VB.TextBox txtImpresora 
      Height          =   330
      Left            =   1665
      TabIndex        =   3
      Text            =   "\\CPU02223\HP"
      Top             =   630
      Width           =   2895
   End
   Begin VB.TextBox txtMes 
      Height          =   330
      Left            =   3915
      TabIndex        =   2
      Text            =   "0"
      Top             =   135
      Width           =   645
   End
   Begin VB.Label Label2 
      Caption         =   "Mes a Imprimir (empezando por 0)"
      Height          =   465
      Left            =   315
      TabIndex        =   1
      Top             =   225
      Width           =   2940
   End
   Begin VB.Label Label1 
      Caption         =   "Impresora"
      Height          =   465
      Left            =   315
      TabIndex        =   0
      Top             =   765
      Width           =   1230
   End
End
Attribute VB_Name = "frm_ImpresionMasiva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim qry(1) As New rdoQuery

Private Sub cmdImprimir_Click()
   Dim RS As rdoResultset
   Dim aFact() As String
   Dim I As Integer
   
   Call IniciarQRY
   
   qry(1).rdoParameters(0).Value = txtMes.Text
   Set RS = qry(1).OpenResultset(rdOpenStatic)
   
   If Not RS.EOF Then
      RS.MoveLast
      ReDim aFact(1 To RS.RowCount) As String
      
      RS.MoveFirst
      I = 1
      Do While Not RS.EOF
         aFact(I) = RS("FA04NUMFACT").Value
         RS.MoveNext
         I = I + 1
      Loop
      
      Call ImprimirFactMasiva(aFact, txtImpresora.Text)
      MsgBox "Proceso Finalizado", vbInformation + vbOKOnly, "Impresi�n masiva de facturas de Acunsa"
   Else
      MsgBox "No hay facturas para imprimir", vbInformation + vbOKOnly, "Impresi�n masiva de facturas de Acunsa"
   End If

End Sub


Sub IniciarQRY()
   Dim MiSql As String
   Dim X As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSql = "SELECT * FROM FA5000 WHERE FA50NUMMES = ? AND FA50ESTADO = 0 order by FA04NUMFACT"
  qry(1).SQL = MiSql
  
  'Activamos las querys.
'  For x = 1 To 16
    Set qry(1).ActiveConnection = objApp.rdoConnect
    qry(1).Prepared = True
'  Next
End Sub

