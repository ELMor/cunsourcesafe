VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmEditFactura 
   Appearance      =   0  'Flat
   BackColor       =   &H80000000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edici�n Factura"
   ClientHeight    =   7500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9990
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7500
   ScaleWidth      =   9990
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "Conciertos Aplicables"
      Height          =   450
      Left            =   8730
      TabIndex        =   28
      Top             =   945
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdAutoTextos 
      Caption         =   "&AutoTextos"
      Height          =   270
      Left            =   6030
      TabIndex        =   10
      Top             =   6840
      Width           =   990
   End
   Begin VB.TextBox txtFactura 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   8685
      TabIndex        =   26
      Top             =   540
      Width           =   1260
   End
   Begin VB.CommandButton cmdDesglose 
      Caption         =   "Ver &Desglose"
      Height          =   405
      Left            =   8730
      TabIndex        =   25
      Top             =   2880
      Width           =   1215
   End
   Begin VB.CommandButton cmdVisionGlobal 
      Caption         =   "Visi�n &Global"
      Height          =   405
      Left            =   8730
      TabIndex        =   24
      Top             =   3345
      Width           =   1215
   End
   Begin VB.CommandButton cmdConcierto 
      Caption         =   "Ver &Conciertos"
      Height          =   405
      Left            =   8730
      TabIndex        =   23
      Top             =   3810
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdA�adir 
      Caption         =   "&A�adir ..."
      Height          =   405
      Left            =   8730
      TabIndex        =   22
      Top             =   1770
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevaLinea 
      Caption         =   "&Nueva Linea"
      Height          =   405
      Left            =   8730
      TabIndex        =   21
      Top             =   4275
      Width           =   1215
   End
   Begin VB.CommandButton cmdEditarLinea 
      Caption         =   "&Editar L�nea"
      Height          =   405
      Left            =   8730
      TabIndex        =   20
      Top             =   4770
      Width           =   1215
   End
   Begin VB.ComboBox cboColetillas 
      Height          =   315
      ItemData        =   "FA00115.frx":0000
      Left            =   45
      List            =   "FA00115.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   17
      Top             =   7155
      Width           =   9870
   End
   Begin VB.TextBox txtColetillas 
      Height          =   285
      Left            =   360
      TabIndex        =   16
      Text            =   "Text1"
      Top             =   7155
      Width           =   690
   End
   Begin VB.Frame Frame2 
      Caption         =   "Responsable econ�mico"
      Height          =   1095
      Left            =   60
      TabIndex        =   2
      Top             =   1680
      Width           =   8025
      Begin SSDataWidgets_B.SSDBGrid grdssResponsable 
         Height          =   855
         Left            =   60
         TabIndex        =   11
         Top             =   180
         Width           =   7875
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   8
         stylesets.count =   1
         stylesets(0).Name=   "Selected"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   16711680
         stylesets(0).Picture=   "FA00115.frx":0004
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ActiveRowStyleSet=   "Selected"
         Columns.Count   =   8
         Columns(0).Width=   10769
         Columns(0).Caption=   "Responsable"
         Columns(0).Name =   "Responsable"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1429
         Columns(1).Caption=   "CodPersona"
         Columns(1).Name =   "CodPersona"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Entidad"
         Columns(2).Name =   "Entidad"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "Linea"
         Columns(3).Name =   "Linea"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "NFactura"
         Columns(4).Name =   "NFactura"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "EntiPrivado"
         Columns(5).Name =   "EntiPrivado"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   529
         Columns(6).Caption=   "TipoEco"
         Columns(6).Name =   "TipoEco"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   714
         Columns(7).Caption=   "EntidadResp"
         Columns(7).Name =   "EntidadResp"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   13891
         _ExtentY        =   1508
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pacientes"
      Height          =   1575
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   8535
      Begin SSDataWidgets_B.SSDBGrid grdSSPac 
         Height          =   1275
         Left            =   60
         TabIndex        =   1
         Top             =   180
         Width           =   8415
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   5
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   1402
         Columns(0).Caption=   "Historia"
         Columns(0).Name =   "Historia"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3704
         Columns(1).Caption=   "Paciente"
         Columns(1).Name =   "Paciente"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1746
         Columns(2).Caption=   "F. Ingreso"
         Columns(2).Name =   "F. Ingreso"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   4974
         Columns(3).Caption=   "Direcci�n"
         Columns(3).Name =   "Direcci�n"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2090
         Columns(4).Caption=   "F. Nacimiento"
         Columns(4).Name =   "F. Nacimiento"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   14843
         _ExtentY        =   2249
         _StockProps     =   79
         BackColor       =   -2147483633
      End
   End
   Begin VB.TextBox txtObservaciones 
      Height          =   780
      Left            =   45
      MaxLength       =   400
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Top             =   6300
      Width           =   5910
   End
   Begin VB.CommandButton cmdRedondeo 
      Caption         =   "&Redondeo"
      Height          =   405
      Left            =   8730
      TabIndex        =   12
      Top             =   5490
      Width           =   1215
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   405
      Left            =   8730
      TabIndex        =   9
      Top             =   6585
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Visi�n Preliminar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   8730
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   6090
      Width           =   1215
   End
   Begin VB.TextBox txtTotal 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   360
      Index           =   1
      Left            =   7350
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   6465
      Width           =   1275
   End
   Begin VB.TextBox txtDcto 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   1
      Left            =   7725
      TabIndex        =   5
      Top             =   6120
      Width           =   900
   End
   Begin SSDataWidgets_B.SSDBGrid grdssFact 
      Height          =   3210
      Left            =   60
      TabIndex        =   3
      Top             =   2835
      Width           =   8535
      ScrollBars      =   2
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   7
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   8837
      Columns(0).Caption=   "Descripci�n"
      Columns(0).Name =   "Descripci�n"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1931
      Columns(1).Caption=   "Precio"
      Columns(1).Name =   "Precio"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1244
      Columns(2).Caption=   "Dcto. %"
      Columns(2).Name =   "Dcto. %"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2064
      Columns(3).Caption=   "Importe"
      Columns(3).Name =   "Importe"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasForeColor=   -1  'True
      Columns(3).ForeColor=   16711680
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "U."
      Columns(4).Name =   "Cantidad"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Franquicia"
      Columns(5).Name =   "Franquicia"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "KeyLinea"
      Columns(6).Name =   "KeyLinea"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   15055
      _ExtentY        =   5662
      _StockProps     =   79
      Caption         =   "L�neas de Factura"
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFactura 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   8085
      TabIndex        =   18
      Tag             =   "Fecha Inicio"
      Top             =   2490
      Width           =   1845
      _Version        =   65537
      _ExtentX        =   3254
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label Label2 
      Caption         =   "N� de Factura"
      Height          =   255
      Index           =   4
      Left            =   8730
      TabIndex        =   27
      Top             =   225
      Width           =   1080
   End
   Begin VB.Label Label2 
      Caption         =   "Fecha de Factura"
      Height          =   255
      Index           =   3
      Left            =   8085
      TabIndex        =   19
      Top             =   2250
      Width           =   1635
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   240
      Left            =   1350
      TabIndex        =   15
      Top             =   6075
      Width           =   2925
   End
   Begin VB.Label Label2 
      Caption         =   "Observaciones:"
      Height          =   255
      Index           =   1
      Left            =   90
      TabIndex        =   14
      Top             =   6075
      Width           =   1170
   End
   Begin VB.Label Label2 
      Caption         =   "TOTAL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   6450
      TabIndex        =   6
      Top             =   6510
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Dcto %"
      Height          =   255
      Index           =   0
      Left            =   6495
      TabIndex        =   4
      Top             =   6165
      Width           =   675
   End
End
Attribute VB_Name = "frmEditFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf
Dim NumAsist As String
Dim NumProc As String
Dim Propuesta As String
Dim Asist As String
Dim NodoFactura As String
Dim CodPaciente As String
Dim UltimaLinea As Integer
Dim qry(1 To 10) As New rdoQuery
Dim qrys(1 To 8) As New rdoQuery
Dim lngAsistencia As Long
Dim lngProceso As Long

'Este tipo de usuario lo utilizaremos para agrupar rnfs de cara la impresi�n
Private Type TypeRNFs
  NumFact As Integer
  NumLinea As Integer
  Codigo As Long
  Descrip As String
  Cantidad As Integer
  Desglose As Boolean
End Type

Dim ArrayContador() As TypeRNFs
Dim ArrayOrdenacion() As Integer

Sub BuscarResponsableEconomico()
    Dim Cont As Integer
    Dim objREco As Persona
    
    On Error GoTo error
    
    Me.grdssResponsable.RemoveAll
    
    Cont = 0
    For Each objREco In Asistencia.REcos
        Cont = Cont + 1
        Me.grdssResponsable.AddItem objREco.Name & Chr(9) & objREco.Codigo & Chr(9) & "T" & Chr(9) & Cont & Chr(9) & "" & Chr(9) & IIf(objREco.TipoPersona = 1, "P", "E") & Chr(9) & objREco.CodTipEcon & Chr(9) & objREco.EntidadResponsable
        
        
        If Cont > 1 Then
'          Load txtObservaciones(Cont)
          Load txtTotal(Cont)
          Load txtDcto(Cont)
'          Load chkFinalFactura(Cont)
        End If
NextREco:

    Next
    
    Exit Sub
    
error:
    If Err.Number = 360 Then
        Resume NextREco
    End If
End Sub


Sub CargarComboColetillas()
Dim MiSql As String
Dim MiRs As rdoResultset

  MiSql = "Select FA19DESCOL From FA1900 Where FA19INDACT = -1 ORDER BY FA19CODCOL"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      cboColetillas.AddItem MiRs("FA19DESCOL")
      MiRs.MoveNext
    Wend
    cboColetillas.ListIndex = 0
  End If
  
   Dim objREco As Persona
   For Each objREco In Asistencia.REcos
      objREco.Coletilla = CrearCabecera(cboColetillas.Text, CStr(Asistencia.FecInicioFact), CStr(Asistencia.FecFinFact), Asistencia.DptoResp)
   Next objREco

End Sub

Function CrearCabecera(Coletilla As String, FechaIni As String, FechaFin As String, Dpto As String) As String
Dim Texto As String
Dim Pendiente As String
Dim intPuntos As Integer

   Pendiente = Coletilla
   
   ' Buscar la variable %DPTO% para sustituirla
   intPuntos = InStr(1, Pendiente, "%DPTO%")
   If intPuntos <> 0 Then
      Texto = Left(Pendiente, intPuntos - 1)
      Texto = Texto & UCase(Dpto)
      Pendiente = Right(Pendiente, IIf(Len(Pendiente) - intPuntos - 5 < 0, 0, Len(Pendiente) - intPuntos - 5))
      intPuntos = InStr(1, Pendiente, ".")
     
      Coletilla = Texto & Pendiente
   
   End If

   
   Pendiente = Coletilla
   
   ' buscar las fechas de inicio y fin para sustituirlas
   intPuntos = InStr(1, Pendiente, "...")
   If intPuntos <> 0 Then
     Texto = Left(Pendiente, intPuntos - 1)
     Texto = Texto & Format(FechaIni, "DD/MM/YYYY") & " "
     Pendiente = Right(Pendiente, IIf(Len(Pendiente) - intPuntos - 3 < 0, 0, Len(Pendiente) - intPuntos - 3))
     intPuntos = InStr(1, Pendiente, ".")
     If intPuntos <> 0 Then
       Pendiente = Left(Pendiente, intPuntos - 1)
       Texto = Texto & Pendiente & Format(FechaFin, "DD/MM/YYYY")
       CrearCabecera = Texto
       Exit Function
     Else
       CrearCabecera = Texto
       Exit Function
     End If
   Else
     CrearCabecera = ""
     Exit Function
   End If

End Function

Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer
  
  'Seleccionar la descripci�n de una categor�a.
  MiSql = "Select FA05DESIG from FA0500 Where FA05CODCATEG = ?"
  qry(3).SQL = MiSql
  
  'Insertar una cabecera de factura en FA0400
  MiSql = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
              "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV," & _
              "FA09CODNODOCONC,AD07CODPROCESO,CI32CODTIPECON,FA04FECFACTURA, FA04INDNMGC, FA04FECDESDE, FA04FECHASTA, FA04INDCOMPENSA ) " & _
              "Values (?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?)"
  qry(4).SQL = MiSql
  
  'Insertar una l�nea de factura en FA1600
  MiSql = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
              "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE,FA16ORDFACT, FA16FRANQUICIA, FA16CANTSINFRAN) Values (?,?,?,?,?,?,?,?,?,?,?)"
  qry(5).SQL = MiSql
  
  'Asignar a FA0300 el codigo de la factura y el n�mero de l�nea
  MiSql = "Update FA0300 " & _
                " set FA04NUMFACT = ?, FA16NUMLINEA = ?, FA03INDDESGLOSE = ?, FA15CODATRIB = ?, " & _
                " FA03PRECIOFACT = ?, FA03CANTFACT = ?, FA03OBSIMP = ?, FA03PERIODOGARA = ? " & _
                " Where FA03NUMRNF = ?"
  qry(6).SQL = MiSql
  
     
  MiSql = "Select * from fa4400 where upper(fa44nombre) = upper(?)"
  qry(7).SQL = MiSql
  
  
  'Activamos las querys.
  For x = 1 To 8
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub

Private Sub pAjustarDescuadre(Diferencia As Double)
   Dim objLinea As Nodo
   
   For Each NodoFACT In PropuestaFact.NodosFactura
      For Each objLinea In NodoFACT.Nodos
         If objLinea.KeyREco = varFact.KeyREcoAct And objLinea.Descont = True Then
            'Calcular Importe Final
            With objLinea.LineaFact
               .Importe = .Importe - (Diferencia / (100 - .Dto - .DescuentoManual) * 100)
            End With
            Exit Sub
         End If
      Next objLinea
   Next NodoFACT
End Sub

Public Function pFactura(objFactura As factura, Prop As String) As factura
    Dim Posicion As Integer

    Set varFact = objFactura
    'Buscamos el / para separar el n�mero de asistencia y la propuesta seleccionada
    Posicion = InStr(1, Prop, "/")
    If Posicion <> 0 Then
      'Propuesta seleccionada
      Propuesta = Right(Prop, Len(Prop) - Posicion)
      'N�mero de asistencia.
      NumAsist = Left(Prop, Posicion - 1)
      Posicion = InStr(1, NumAsist, "-")
      CodPaciente = Left(Prop, Posicion - 1)
      NumAsist = Right(NumAsist, Len(NumAsist) - Posicion)
    End If
    Load frmEditFactura
      'Set frmSeleccion.varFact = objFactura
    frmEditFactura.Show (vbModal)
            
'    Set objFactura = varFact
'   varFact.Destruir
   Set varFact = Nothing
   
    Unload frmEditFactura
End Function

Private Sub pCargarLineas(KeyREco As String)
    Dim strTexto As String
    Dim lngPrecio As Long
'    Dim lngSuma As Long
    Dim Dto As Double
    Dim IntCont As Integer
    Dim IntContNodo As Integer
    Dim intContLin As Integer
    Dim Cont1, Cont2 As Integer
    Dim TmpOrdImp As Integer
    Dim TmpClave1, TmpClave2 As Integer
    Dim intPos As Integer
    Dim objLinea As Nodo
    
    On Error Resume Next
    
    'Iniciamos el contador del Importe total.
'    lngSuma = 0
    IntCont = 1
    intContLin = 1
    IntContNodo = 1
    'Inicializamos el grid para cargar las nuevas l�neas
    grdssFact.RemoveAll
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
'    Set Paciente = varFact.Pacientes(CodPaciente)
'    Set Asistencia = Paciente.Asistencias(numAsist)
    intPos = InStr(1, Asistencia.Codigo, "-")
'    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    For Each NodoFACT In PropuestaFact.NodosFactura
        For Each objLinea In NodoFACT.Nodos
            'Si en una l�nea que se debe mostrar o facturar.
'            If objLinea.KeyREco = KeyREco Then
                'Si el indicador de Entidad del grid es T.
                If IntCont = 1 Then
                    ReDim ArrayOrdenacion(1 To 3, 1 To 1)
                Else
                    ReDim Preserve ArrayOrdenacion(1 To 3, 1 To IntCont)
                End If
                If objLinea.OrdImp = 0 Then
                    ' se le pone un numero suficientemente alto como para que vaya al final
                    ' de la factura, se deja un hueco donde iran las lineas a�adidas (OrdImp=32700)
                    ArrayOrdenacion(1, IntCont) = 32000
                    objLinea.OrdImp = 32000
                Else
                    ArrayOrdenacion(1, IntCont) = objLinea.OrdImp
                End If
                ArrayOrdenacion(2, IntCont) = IntContNodo
                ArrayOrdenacion(3, IntCont) = intContLin
                IntCont = IntCont + 1
'            End If
            intContLin = intContLin + 1
        Next
        IntContNodo = IntContNodo + 1
        intContLin = 1
    Next
    
    ' ORDENAR LAS LINEAS
    For Cont1 = UBound(ArrayOrdenacion, 2) To 1 Step -1
      For Cont2 = Cont1 To UBound(ArrayOrdenacion, 2) - 1
        If ArrayOrdenacion(1, Cont2) > ArrayOrdenacion(1, Cont2 + 1) Then
          'Copiar a dos variables los valores del siguiente
          TmpOrdImp = ArrayOrdenacion(1, Cont2 + 1)
          TmpClave1 = ArrayOrdenacion(2, Cont2 + 1)
          TmpClave2 = ArrayOrdenacion(3, Cont2 + 1)
          'Copiar los valores al siguiente
          ArrayOrdenacion(1, Cont2 + 1) = ArrayOrdenacion(1, Cont2)
          ArrayOrdenacion(2, Cont2 + 1) = ArrayOrdenacion(2, Cont2)
          ArrayOrdenacion(3, Cont2 + 1) = ArrayOrdenacion(3, Cont2)
          'Copiar los valores al anterior
          ArrayOrdenacion(1, Cont2) = TmpOrdImp
          ArrayOrdenacion(2, Cont2) = TmpClave1
          ArrayOrdenacion(3, Cont2) = TmpClave2
        ElseIf ArrayOrdenacion(1, Cont2) < ArrayOrdenacion(1, Cont2 + 1) Then
          Exit For
        End If
      Next
    Next
    ' AGRUPAR LA LINEAS CON LOS NODOS ORDENADOS segun el REco
    For Cont1 = UBound(ArrayOrdenacion, 2) To 1 Step -1
      For Cont2 = Cont1 To UBound(ArrayOrdenacion, 2) - 1
        If ArrayOrdenacion(2, Cont2) > ArrayOrdenacion(2, Cont2 + 1) Then
          'Copiar a dos variables los valores del siguiente
          TmpOrdImp = ArrayOrdenacion(1, Cont2 + 1)
          TmpClave1 = ArrayOrdenacion(2, Cont2 + 1)
          TmpClave2 = ArrayOrdenacion(3, Cont2 + 1)
          'Copiar los valores al siguiente
          ArrayOrdenacion(1, Cont2 + 1) = ArrayOrdenacion(1, Cont2)
          ArrayOrdenacion(2, Cont2 + 1) = ArrayOrdenacion(2, Cont2)
          ArrayOrdenacion(3, Cont2 + 1) = ArrayOrdenacion(3, Cont2)
          'Copiar los valores al anterior
          ArrayOrdenacion(1, Cont2) = TmpOrdImp
          ArrayOrdenacion(2, Cont2) = TmpClave1
          ArrayOrdenacion(3, Cont2) = TmpClave2
        ElseIf ArrayOrdenacion(2, Cont2) < ArrayOrdenacion(2, Cont2 + 1) Then
          Exit For
        End If
      Next
    Next
    
    For Cont1 = 1 To UBound(ArrayOrdenacion, 2)
        Set NodoFACT = PropuestaFact.NodosFactura(ArrayOrdenacion(2, Cont1))
        Set objLinea = New Nodo
        Set objLinea = NodoFACT.Nodos(ArrayOrdenacion(3, Cont1))
        'Le asignamos los que coincidan
        If objLinea.KeyREco = KeyREco Then
            If objLinea.EntidadResponsable = "" Then
                'objLinea.EntidadResponsable = CodPaciente
            End If
            If objLinea.LineaFact.DescuentoManual <> 0 Then
                Dto = objLinea.LineaFact.Dto + objLinea.LineaFact.DescuentoManual
            Else
                Dto = objLinea.LineaFact.Dto
            End If
            lngPrecio = RedondearMoneda(objLinea.LineaFact.Total + objLinea.LineaFact.TotalNoDescontable, tiImporteLinea)
            strTexto = objLinea.Name & " (" & objLinea.LineaFact.Cantidad & ")" _
                              & Chr$(9) & RedondearMoneda(objLinea.LineaFact.Precio + objLinea.LineaFact.PrecioNoDescontable, tiPrecioUnit) _
                              & Chr$(9) & Dto _
                              & Chr$(9) & RedondearMoneda(objLinea.LineaFact.Total + objLinea.LineaFact.TotalNoDescontable, tiImporteLinea) _
                              & Chr$(9) & objLinea.LineaFact.Cantidad _
                              & Chr$(9) & objLinea.Editable _
                              & Chr$(9) & objLinea.Key
            grdssFact.AddItem strTexto
'            lngSuma = lngSuma + lngPrecio
        End If
    Next
    
'*************************************************
'    mostrar las lineas de factura a�adidas
'*************************************************
    For Each objLinea In PropuestaFact.NuevasLineas
        'Le asignamos los que coincidan o est�n en blanco.
        If objLinea.KeyREco = KeyREco Then
            If objLinea.LineaFact.DescuentoManual <> 0 Then
              Dto = objLinea.LineaFact.Dto + objLinea.LineaFact.DescuentoManual
            Else
              Dto = objLinea.LineaFact.Dto
            End If
            
            lngPrecio = RedondearMoneda(objLinea.LineaFact.Total + objLinea.LineaFact.TotalNoDescontable, tiImporteLinea)
            strTexto = objLinea.Name & " (" & objLinea.LineaFact.Cantidad & ")" _
                            & Chr$(9) & RedondearMoneda(objLinea.LineaFact.Precio + objLinea.LineaFact.PrecioNoDescontable, tiPrecioUnit) _
                            & Chr$(9) & Dto _
                            & Chr$(9) & RedondearMoneda(objLinea.LineaFact.Total + objLinea.LineaFact.TotalNoDescontable, tiImporteLinea) _
                            & Chr$(9) & objLinea.LineaFact.Cantidad _
                            & Chr$(9) & objLinea.Editable _
                            & Chr$(9) & objLinea.Key
            grdssFact.AddItem strTexto
'            lngSuma = lngSuma + lngPrecio
        End If
    Next
'*************************************************

   Me.txtTotal(KeyREco).Text = PropuestaFact.Importe(KeyREco)
   txtObservaciones.Text = Asistencia.REcos(KeyREco).Observaciones
End Sub

Function pCalcularImportes(NoDescont As Double) As Double
   Dim strTexto As String
   Dim dblPrecio As Double
   Dim dblSuma As Double
   Dim dblSumaNoDto As Double
   Dim dblDtoManual As Double
    
   'Iniciamos el contador del Importe total.
   dblSuma = 0
   dblSumaNoDto = 0
   
   For Each NodoFACT In PropuestaFact.NodosFactura
      For Each Nodos In NodoFACT.Nodos
         If Nodos.KeyREco = varFact.KeyREcoAct Then
            ' guardamos el descuento manual para calcular los importes descontable sin tenerlo en cuenta
            dblDtoManual = Nodos.LineaFact.DescuentoManual
            Nodos.LineaFact.DescuentoManual = 0
            ' acumular los totales
            dblSuma = dblSuma + RedondearMoneda(Nodos.LineaFact.Total, tiImporteLinea)
            dblSumaNoDto = dblSumaNoDto + RedondearMoneda(Nodos.LineaFact.TotalNoDescontable, tiImporteLinea)
            ' recuperar el descuento manual
            Nodos.LineaFact.DescuentoManual = dblDtoManual
         End If
      Next Nodos
   Next NodoFACT
   
   For Each Nodos In PropuestaFact.NuevasLineas
      If Nodos.KeyREco = varFact.KeyREcoAct Then
         ' guardamos el descuento manual para calcular los importes descontable sin tenerlo en cuenta
         dblDtoManual = Nodos.LineaFact.DescuentoManual
         Nodos.LineaFact.DescuentoManual = 0
         ' acumular los totales
         dblSuma = dblSuma + RedondearMoneda(Nodos.LineaFact.Total, tiImporteLinea)
         dblSumaNoDto = dblSumaNoDto + RedondearMoneda(Nodos.LineaFact.TotalNoDescontable, tiImporteLinea)
         ' recuperar el descuento manual
         Nodos.LineaFact.DescuentoManual = dblDtoManual
      End If
   Next Nodos
   
   pCalcularImportes = dblSuma
   NoDescont = dblSumaNoDto
End Function


Private Sub pAsignarDescuento(DtoAplicar As Double)
   Dim objLinea As Nodo
    
   
   For Each NodoFACT In PropuestaFact.NodosFactura
      For Each objLinea In NodoFACT.Nodos
         GoSub AplicarDto
      Next objLinea
   Next NodoFACT
   
   For Each objLinea In PropuestaFact.NuevasLineas
      GoSub AplicarDto
   Next objLinea
Exit Sub

AplicarDto:
   If objLinea.KeyREco = varFact.KeyREcoAct And objLinea.Descont = True Then
      With objLinea.LineaFact
         ' anular el descuento anterior
         .DescuentoManual = 0
         ' aplicar el nuevo descuento teniendo en cuenta el descuento fijo que ya tuviera
         If .Dto = 0 Then
            .DescuentoManual = DtoAplicar
         Else
            .DescuentoManual = DtoAplicar * (100 - .Dto) / 100
         End If
      End With
   End If
   
Return
   
End Sub

Private Sub pCargarPaciente()
    Dim objPaciente As Paciente
    Dim objAsist As Asistencia
    Dim strTexto As String
        
    'Recorremos los diferentes pacientes que hemos a�adido a la colecci�n.
    For Each objPaciente In varFact.Pacientes
      'A�adimos cada uno de ellos al grid de pacientes.
      strTexto = objPaciente.Codigo _
                  & Chr$(9) & objPaciente.Name _
                  & Chr$(9) & " " _
                  & Chr$(9) & objPaciente.Direccion _
                  & Chr$(9) & objPaciente.FecNacimiento
      grdSSPac.AddItem strTexto
      For Each objAsist In objPaciente.Asistencias
         If objAsist.FechaAnterior <> "" Then
           Me.SDCFechaFactura.Date = objAsist.FechaAnterior
         End If
         If objAsist.NumFactAnt <> "" Then
           txtFactura.Text = objAsist.NumFactAnt
'           txtFactura.Enabled = False
         End If
         
      Next objAsist
'      If objPaciente.FechaAnterior <> "" Then
'
'        Me.SDCFechaFactura.Date = objPaciente.FechaAnterior
'      End If
'      If objPaciente.NumFactAnt <> "" Then
'        txtFactura.Text = objPaciente.NumFactAnt
'        txtFactura.Enabled = False
'      End If
    Next
End Sub

Private Sub cboColetillas_LostFocus()
   Dim objREco As Persona
   For Each objREco In Asistencia.REcos
      objREco.Coletilla = CrearCabecera(cboColetillas.Text, CStr(Asistencia.FecInicioFact), CStr(Asistencia.FecFinFact), Asistencia.DptoResp)
   Next objREco

End Sub

'Private Sub chkFinalFactura_Click(Index As Integer)
''  If chkFinalFactura(Index).Value = 1 Then
''    txtObservaciones(Index).Text = "Nota: Si se produce alg�n nuevo cargo hasta el momento de alta " & _
''                                                   "le remitiremos una nueva factura complementaria"
''  Else
''    txtObservaciones(Index).Text = ""
''  End If
'End Sub

Private Sub cmdAceptar_Click()
Dim Opcion As Integer
Dim anumfact() As String
Dim contFact As Integer
Dim x As Integer
Dim objREco As Persona
   
   cmdAceptar.SetFocus
   
    If ValidarNumFact <> True Then Exit Sub

       
    'Grabar la factura
'    Call GuardarFactura
    Asistencia.GrabarFactura PropuestaFact.Key, Me.SDCFechaFactura.Date
    
    'recoger los numeros de factura generados para imprimirlos
    contFact = 0
    For Each objREco In Asistencia.REcos
      If objREco.NFactura <> "" Then
          contFact = contFact + 1
          ReDim Preserve anumfact(1 To contFact) As String
          anumfact(contFact) = objREco.NFactura
      End If
    Next
   
   ' Ver si hay alguna factura que imprimir
   If contFact > 0 Then
       ' realizar la presentacion preliminar de la factura
       ' y ver si hay que salir o permanecer en la edici�n de factura
       If ImprimirFact(anumfact, True) = True Then
         Asistencia.Facturado = True
           ' Hay que salir de la edici�n de la factura
   '        Call Form_QueryUnload(0, 999)
           Unload Me
       Else
           ' La factura no es correcta, hay que borrarla y seguir en edicion
           
       End If
   End If
End Sub


Private Sub cmdA�adir_Click()
    Dim CodPersona As String
    Dim objREco As Persona
    'Llamamos a la pantalla de selecci�n de un nuevo responsable econ�mico.
    CodPersona = frmNuevoResponsable.pNuevoResponsable
    
    If CodPersona <> "" Then
        Set objREco = New Persona
        objREco.Codigo = CodPersona
        objREco.Coletilla = CrearCabecera(cboColetillas.Text, CStr(Asistencia.FecInicioFact), CStr(Asistencia.FecFinFact), Asistencia.DptoResp)

        Asistencia.AddREco objREco, Asistencia.FecInicio, Asistencia.FecFin
        BuscarResponsableEconomico
    End If
    
End Sub

Private Sub cmdAutoTextos_Click()
   Dim Texto As String
   Dim NuevoTexto As String
   
   Call frm_AutoTextos.pAutotexto(Texto)
   
   If Texto <> "" Then
      NuevoTexto = Left(txtObservaciones.Text, txtObservaciones.SelStart)
      NuevoTexto = NuevoTexto & Texto
      NuevoTexto = NuevoTexto & Right(txtObservaciones.Text, Len(txtObservaciones.Text) - txtObservaciones.SelStart - txtObservaciones.SelLength)
   
      txtObservaciones.Text = NuevoTexto
   End If
End Sub
'
'Private Sub cmdConcierto_Click()
'
'    Call frmConcSeleccionado.pConcSelec(varFact, CodPaciente, NumAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value, ArrayOrdenacion)
'
'End Sub



Private Sub cmdEditarLinea_Click()
    Dim Posicion
    Dim Designacion As String
  
    Designacion = grdssFact.Columns(0).Text
    Posicion = InStr(1, Designacion, "(") - 1
    Designacion = Left(Designacion, Posicion)
    
  'Comprobamos que la l�nea que queremos editar no tiene ninguna restricci�n
'  If grdssFact.Columns("Franquicia").Text = "True" Then

    'Llamamos a la pantalla que utilizamos para crear nuevas l�neas.
    'Call frmNuevaLinea.pNuevaLinea(varFact, CodPaciente, NumAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value)
    Call frm_EditLinea.pEditarLinea(varFact, CodPaciente, NumAsist, Propuesta, Me.grdssFact.Row + 1, grdssFact.Columns(4).Text, Designacion, grdssFact.Columns(1).Text, grdssFact.Columns(2).Text, grdssFact.Columns(3).Text, grdssFact.Columns(6).Text)
      
    Call pCargarLineas(varFact.KeyREcoAct)
End Sub

Private Sub RegenerarPropuesta()
'    Set Asistencia = varFact.Pacientes(CodPaciente).Asistencias(numAsist)
'    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    
    varFact.RegenerarPropuesta PropuestaFact, Asistencia
    
    'como se ha regenerado la propuesta hay que volver a cargar las lineas
    Me.grdssFact.RemoveAll
    
'    Call pCargarLineas(Me.grdssResponsable.Columns(3).Value)
End Sub

Private Sub RestaurarPropuesta()
'    Set Asistencia = varFact.Pacientes(CodPaciente).Asistencias(numAsist)
'    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    
    varFact.RestaurarPropuesta PropuestaFact, Asistencia
End Sub

Private Sub cmdNuevaLinea_Click()
    'Llamamos a la pantalla que utilizamos para crear nuevas l�neas.
    Call frmNuevaLinea.pNuevaLinea(varFact, CodPaciente, NumAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value)
    Me.grdssFact.RemoveAll
    
       
    With Asistencia.REcos(varFact.KeyREcoAct)
    If .Concierto.Concierto = 0 Then
    
       .Concierto.Concierto = 97
       .Concierto.NodoAFacturar = 328
       .EntidadResponsable = "A"
       .CodTipEcon = "P"
   
         BuscarResponsableEconomico
    End If
    End With
    
    Call pCargarLineas(varFact.KeyREcoAct)
End Sub

Private Sub cmdRedondeo_Click()
   Dim DtoMaximo As Double
   Dim strCantDto As String
   Dim dblCantDto As Double
   Dim dblDesto As Double
   Dim NoDtable As Double
   Dim Descuadre As Double
   NoDtable = 0
     
   DtoMaximo = pCalcularImportes(NoDtable)
   If DtoMaximo = 0 Then
      MsgBox "No hay lineas a las que aplicar el descuento", vbInformation + vbOKOnly, "Redondeo de factura"
      Exit Sub
   End If
   
      strCantDto = "-1"
   
'   While CDbl(strCantDto) > NoDtable + DtoMaximo Or CDbl(strCantDto) < NoDtable
'      strCantDto = InputBox("Cantidad que desea facturar. M�ximo: " & NoDtable + DtoMaximo & " M�nimo: " & NoDtable, "Redondeo de factura")
   While CDbl(strCantDto) < NoDtable
      strCantDto = InputBox("Cantidad que desea facturar. M�nimo: " & NoDtable, "Redondeo de factura")
      If strCantDto = "" Then
         Exit Sub
      End If
      If Not IsNumeric(strCantDto) Then
         Exit Sub
      End If
   Wend
   
   If strCantDto <> "0" Then
      dblCantDto = CDbl(strCantDto)
      dblDesto = DtoMaximo - (dblCantDto - NoDtable)
      dblDesto = ((dblDesto / DtoMaximo) * 100)
   End If
   
   Call pAsignarDescuento(dblDesto)
   
   ' despues de un primer calculo hay que ver si se ha conseguido el redondeo o
   ' si hay alg�n descuadre que se asignar� a la primera linea descontable (por convenio)
   Descuadre = PropuestaFact.Importe(varFact.KeyREcoAct) - dblCantDto
   If Descuadre <> 0 Then
      Call pAjustarDescuadre(Descuadre)
   End If
   
   Call pCargarLineas(varFact.KeyREcoAct)
End Sub


Private Sub cmdDesglose_Click()
    'Llamamos a la pantalla en la que se realiza el desglose.
    Call frmDesglose.pDesglose(varFact, CodPaciente, NumAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value, ArrayOrdenacion)
    
    ' si se ha realizado alguna modificacion marcar la propuesta como actualizable
    If PropuestaFact.NecesitaRegeneracion Then
        RegenerarPropuesta

        ' Recargar el grid de REcos
        BuscarResponsableEconomico
        varFact.KeyREcoAct = Me.grdssResponsable.Columns("Linea").Value
        Call pCargarLineas(varFact.KeyREcoAct)
    End If
End Sub

Private Sub cmdSalir_Click()
   Dim Opcion As Integer

    'Opcion = MsgBox("�Facturar otra propuesta?", vbQuestion + vbYesNo, "Facturaci�n")
    'If Opcion = vbYes Then
    '  ContinuarFactura = True
    'ElseIf Opcion = vbNo Then
    '  ContinuarFactura = False
    'End If
    Unload Me
End Sub

Private Sub cmdVisionGlobal_Click()
    Dim strProceso As String
    Dim strAsistencia As String
    If Paciente.Asistencias(1).ProcAsist.Count > 1 Then
        Call VisionGlobal(Paciente.CodPersona)
    Else
        Call VisionGlobal(Paciente.CodPersona, Paciente.Asistencias(1).ProcAsist(1).proceso, Paciente.Asistencias(1).ProcAsist(1).Asistencia)
    End If
End Sub

Private Sub Command1_Click()
   ' frmConcSeleccionado.Show
   frmConcSeleccionado.pConc varFact
End Sub

Private Sub Form_Load()
Dim Posicion As Integer
Dim intPos As Integer
Dim MiRsFecha
  
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
'  If Not MiRsFecha.EOF Then
'    Me.SDCFechaCierre.Date = Format(MiRsFecha(0), "dd mmmm yyyy hh:mm:ss")
'    FechaListado = Format(MiRsFecha(0), "dd mmmm yyyy hh:mm:ss")
'  End If
    Call IniciarQRY
'    Call IniciarQRYs
    Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
    If Not MiRsFecha.EOF Then
      Me.SDCFechaFactura.Date = Format(MiRsFecha(0), "dd/mm/yyyy")
    End If
    
    Set Paciente = varFact.Pacientes(CodPaciente)
    Set Asistencia = Paciente.Asistencias(NumAsist)
    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)

    Call pCargarPaciente
    If NumAsist <> "" Then
      intPos = InStr(1, NumAsist, "-")
      lngAsistencia = CLng(Left(NumAsist, intPos - 1))
      lngProceso = CLng(Right(NumAsist, Len(NumAsist) - intPos))
      
      BuscarResponsableEconomico
      
      Call pCargarLineas("1")
    End If
    Call CargarComboColetillas
'    txtObservaciones(1).Visible = True
    txtTotal(1).Visible = True
    txtDcto(1).Visible = True
'    chkFinalFactura(1).Visible = True
    UltimaLinea = 1
    Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
    If Not MiRsFecha.EOF Then
      Me.SDCFechaFactura.Date = Format(MiRsFecha(0), "dd/mm/yyyy")
    End If
  
  Call EvitarModif
   
   If objSecurity.strUser = "JOSE" Or objSecurity.strUser = "PRFAC" Then
      Command1.Visible = True
   Else
      Command1.Visible = False
   End If

End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
   Dim Opcion As Integer
   
   ' si ya ha grabado una factura no permitirle quedarse en edicion
'   If UnloadMode = 999 Then
      Opcion = MsgBox("�Facturar otra propuesta?", vbQuestion + vbYesNo, "Salir de la propuesta de factura")
'   Else
'      Opcion = MsgBox("�Facturar otra propuesta?", vbQuestion + vbYesNoCancel, "Salir de la propuesta de factura")
'   End If
   
   If Opcion = vbYes Then
      ContinuarFactura = True
      
      If PropuestaFact.Modificada Then
         RestaurarPropuesta
      End If
'      Unload Me
      
   ElseIf Opcion = vbNo Then
      ContinuarFactura = False
'      Unload Me
    
   Else
      ' no salir de la pantalla
      Cancel = 1
   End If

End Sub


Private Sub grdssFact_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    Dim Resultado As Integer
    Dim Texto As String

    DispPromptMsg = False
'    If grdssFact.Columns("Franquicia").Text = "False" Then
'        MsgBox "No se puede borrar una l�nea que implique a una franquicia", vbInformation + vbOKOnly, "Aviso"
'        Cancel = 1
'    Else
        Texto = "�Desea borrar la l�nea " & grdssFact.Columns(0).Text & " ?"
        Resultado = MsgBox(Texto, vbYesNo + vbQuestion, "Borrado de una l�nea")
        If Resultado = vbYes Then
                        
            ' BUSCAR LA LINEA A BORRAR ENTRE LAS LINEAS PROPUESTAS
            For Each NodoFACT In PropuestaFact.NodosFactura
                For Each Nodos In NodoFACT.Nodos
                    If Nodos.Key = grdssFact.Columns("KeyLinea").Text And Nodos.KeyREco = varFact.KeyREcoAct Then

                        NodoFACT.Nodos.Remove Nodos.Key

                        Call pCargarLineas(varFact.KeyREcoAct)
                        Exit Sub
                    End If
                Next Nodos
            Next NodoFACT
            
            ' BUSCAR LA LINEA A BORRAR ENTRE LAS LINEAS A�ADIDAS
            For Each Nodos In PropuestaFact.NuevasLineas
                If Nodos.Key = grdssFact.Columns("KeyLinea").Text And Nodos.KeyREco = varFact.KeyREcoAct Then

                    PropuestaFact.NuevasLineas.Remove Nodos.Key

                    Call pCargarLineas(varFact.KeyREcoAct)
                    Exit Sub
                End If
            Next Nodos

        End If
'    End If
End Sub

Private Sub grdssFact_DblClick()
'    Call frm_Desglose.pDesglose(varFact, CodPaciente, Asist, Propuesta, Me.grdssFact.Row + 1)
'    grdssFact.RemoveAll
'    If Me.grdssResponsable.Columns(2).Value = "T" Then
'      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
'    Else
'      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
'    End If
End Sub



Private Sub grdssResponsable_GotFocus()
    varFact.KeyREcoAct = grdssResponsable.Columns("Linea").Value
    
    Call pCargarLineas(varFact.KeyREcoAct)
    
End Sub

Private Sub grdssResponsable_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    'Llenamos el grid de l�neas de factura en base al responsable econ�mico que se corresponda
    'con la l�nea seleccionada, la selecci�n puede ser nula.
    
    varFact.KeyREcoAct = grdssResponsable.Columns("Linea").Value
   
    Me.grdssFact.RemoveAll
    
'    txtObservaciones(UltimaLinea).Visible = False
    txtTotal(UltimaLinea).Visible = False
    txtDcto(UltimaLinea).Visible = False
'    chkFinalFactura(UltimaLinea).Visible = False
    
'    txtObservaciones(varFact.KeyREcoAct).Visible = True
    txtTotal(varFact.KeyREcoAct).Visible = True
    txtDcto(varFact.KeyREcoAct).Visible = True
'    chkFinalFactura(varFact.KeyREcoAct).Visible = True
    UltimaLinea = varFact.KeyREcoAct
    Label1.Caption = grdssResponsable.Columns(0).Text

    Call pCargarLineas(varFact.KeyREcoAct)
      
End Sub


Private Sub SDCFechaFactura_LostFocus()
'   NoFactura = SDCFechaFactura.Text
End Sub

Private Sub txtDcto_KeyPress(Index As Integer, KeyAscii As Integer)
   KeyAscii = fValidarEnteros(KeyAscii)
End Sub

Private Sub txtDcto_LostFocus(Index As Integer)
   If txtDcto(Index) = 0 Then
      txtDcto(Index) = ""
   End If
   If Trim(txtDcto(Index)) <> "" Then
      'Call GenerarDescuento(Responsable, Cantidad)
   End If
   
   Asistencia.REcos(varFact.KeyREcoAct).Dto = txtDcto(Index)
   
   Call pCargarLineas(varFact.KeyREcoAct)
End Sub

Private Sub txtFactura_KeyPress(KeyAscii As Integer)

  KeyAscii = fValidarEnteros(KeyAscii)
  
End Sub

Private Sub txtFactura_LostFocus()
   Call ValidarNumFact
End Sub

Private Function ValidarNumFact() As Boolean
    Dim CadenaFact
    Dim MiSql As String
    Dim MiRs As rdoResultset

   ' por defecto no es valido
      ValidarNumFact = False
      
    If Trim(txtFactura.Text) <> "" Then
        If IsNumeric(txtFactura.Text) Then
            txtFactura.Text = "13/" & txtFactura.Text
        End If
        CadenaFact = txtFactura.Text
        
        MiSql = "Select FA04NUMFACREAL From FA0400 Where FA04NUMFACT = '" & CadenaFact & "'"
        Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
        If Not MiRs.EOF Then
            MiRs.MoveLast
            MiRs.MoveFirst
            While Not MiRs.EOF
                If IsNull(MiRs("FA04NUMFACREAL")) Then
                    MsgBox "El n�mero de factura introducido est� actualmente en uso", vbExclamation + vbOKOnly, "Atenci�n"
'                    txtFactura.Text = ""
                     txtFactura.Enabled = True
                    If txtFactura.Enabled = True Then
                        txtFactura.SetFocus
                    End If
                    Exit Function
                End If
                MiRs.MoveNext
            Wend
            
        Else
          MsgBox "El n�mero introducido no es el de una factura anulada", vbExclamation + vbOKOnly, "Atenci�n"
'          txtFactura.Text = ""
          txtFactura.Enabled = True
          If txtFactura.Enabled = True Then
                txtFactura.SetFocus
          End If
          Exit Function
        End If
    End If
    
    ValidarNumFact = True
    NoFactura = txtFactura.Text
End Function

Private Sub txtObservaciones_KeyDown(KeyCode As Integer, Shift As Integer)
   Dim ObsTotal As String
   Dim PosCursor As Integer
   
   If KeyCode = vbKeyF3 Then
      ' hay que recoger la ultima palabra tecleada para seleccionar su autotexto
      ObsTotal = txtObservaciones.Text
      PosCursor = txtObservaciones.SelStart
      
      If AutoTexto(ObsTotal, PosCursor) = True Then
         txtObservaciones.Text = ObsTotal
      End If
      
   End If
End Sub

Private Sub txtObservaciones_change()
   Asistencia.REcos(varFact.KeyREcoAct).Observaciones = txtObservaciones.Text
End Sub

' devuelve un booleano segun la operacion haya sido o no correcta
Private Function AutoTexto(Texto As String, Posicion As Integer) As Boolean
   Dim MiRs As rdoResultset
   Dim TextoNuevo As String
   Dim ClaveAutoTexto As String
   Dim PosAnt As Integer
   Dim Pos As Integer
   Dim PosPost As Integer
   
   AutoTexto = False
   
'   If Posicion = 0 Then
'      ' no se ha seleccionado nada
'      Exit Function
'   End If
   
   Pos = 1
   PosAnt = 0
   PosPost = Len(Texto) + 1
   
   Do While Pos <= Posicion And Pos <> 0
      ' buscar la posicion del espacio anterior
      Pos = InStr(Pos + 1, Texto, " ")
      If Pos = 0 Then   ' no hay espacio
         
      ElseIf Pos <= Posicion Then
         PosAnt = Pos
      ElseIf Pos > Posicion Then
         PosPost = Pos
         Pos = 0
      End If
   Loop
   
   
   ClaveAutoTexto = Trim(Mid(Texto, PosAnt + 1, PosPost - PosAnt))
   If ClaveAutoTexto <> "" Then
      
      qry(7).rdoParameters(0) = ClaveAutoTexto
      Set MiRs = qry(7).OpenResultset
      
      If Not MiRs.EOF() Then
      
         ' generar el nuevo texto
         TextoNuevo = Left(Texto, PosAnt)
         TextoNuevo = TextoNuevo & MiRs("FA44DESCRIP")
         TextoNuevo = TextoNuevo & Right(Texto, Len(Texto) - PosPost + 1)
         
         AutoTexto = True
      End If
   Else
   
      TextoNuevo = ""
   End If
   
   Texto = TextoNuevo
End Function

Private Sub EvitarModif()
   If Not blnPermitirModifFact Then
      Me.Caption = "Generaci�n de facturas"
      
      Label2(4).Visible = False
      Label2(3).Visible = False
      SDCFechaFactura.Visible = False
      txtFactura.Visible = False
      
      cmdA�adir.Visible = False
      cmdEditarLinea.Visible = False
      cmdNuevaLinea.Visible = False
      cmdRedondeo.Visible = False
   End If
End Sub

