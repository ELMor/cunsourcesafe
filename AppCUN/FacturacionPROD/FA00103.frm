VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{2001309F-B2CB-11D3-8513-00C04F51306C}#1.0#0"; "ctlDBTree.ocx"
Begin VB.Form frmA_Conciertos 
   BorderStyle     =   0  'None
   Caption         =   "Definici�n de Conciertos"
   ClientHeight    =   7890
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   11910
   ControlBox      =   0   'False
   DrawMode        =   15  'Merge Pen Not
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7890
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ctlConc.DefConc DefConc1 
      Height          =   4605
      Left            =   0
      TabIndex        =   13
      Top             =   2385
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   8123
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Conciertos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   0
      Left            =   0
      TabIndex        =   5
      Top             =   480
      Width           =   11850
      Begin TabDlg.SSTab tabTab1 
         Height          =   1440
         Index           =   0
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   315
         Width           =   11625
         _ExtentX        =   20505
         _ExtentY        =   2540
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00103.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtConciertos(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkConciertos"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtConciertos(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkFacturable"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtConciertos(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FA00103.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtConciertos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA09OBSFAC"
            Height          =   735
            Index           =   1
            Left            =   1785
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   1
            Tag             =   "Observaciones|Observaciones, aparecen en el pie de la factura"
            ToolTipText     =   "Observaciones, aparecen en el pie de la factura"
            Top             =   585
            Visible         =   0   'False
            Width           =   9435
         End
         Begin VB.CheckBox chkFacturable 
            BackColor       =   &H8000000A&
            Caption         =   "Facturable?"
            DataField       =   "FA09INDFACTURABLE"
            Height          =   255
            Left            =   10035
            TabIndex        =   11
            Tag             =   "Concierto (s/n)|�Es un concierto? s/n"
            Top             =   45
            Visible         =   0   'False
            Width           =   1200
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1245
            Index           =   0
            Left            =   -74910
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   90
            Width           =   11130
            _Version        =   131078
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19632
            _ExtentY        =   2196
            _StockProps     =   79
         End
         Begin VB.TextBox txtConciertos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA09DESIG"
            Height          =   285
            Index           =   2
            Left            =   1785
            MaxLength       =   50
            TabIndex        =   0
            Tag             =   "Designaci�n|Designaci�n del concierto"
            Top             =   240
            Width           =   7050
         End
         Begin VB.CheckBox chkConciertos 
            BackColor       =   &H8000000A&
            Caption         =   "Concierto?"
            DataField       =   "FA09INDCONCIERTO"
            Height          =   375
            Left            =   10035
            TabIndex        =   3
            Tag             =   "Concierto (s/n)|�Es un concierto? s/n"
            Top             =   270
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.TextBox txtConciertos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA09CODNODOCONC"
            Height          =   285
            Index           =   0
            Left            =   10905
            TabIndex        =   6
            Tag             =   "C�digo|C�digo de Concierto"
            Top             =   645
            Visible         =   0   'False
            Width           =   180
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Observaciones:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   225
            TabIndex        =   12
            Top             =   675
            Visible         =   0   'False
            Width           =   1485
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   45
            TabIndex        =   10
            Top             =   315
            Width           =   1485
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   10110
            TabIndex        =   8
            Top             =   690
            Visible         =   0   'False
            Width           =   705
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   9
      Top             =   7620
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   476
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu MnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu MnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Conciertos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objDetailInfo As New clsCWForm
Dim FechaDia As Date
Private qry(1 To 9) As New rdoQuery

Private Sub chkConciertos_Click()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub DefConc1_DtosCantidad(CodAtrib As String)
    If CodAtrib = "" Then Exit Sub
        
    frmA_DtosCantidad.CodAtrib = CodAtrib
    frmA_DtosCantidad.Show vbModal
    
    Set frmA_DtosCantidad = Nothing
End Sub

Private Sub Form_Resize()
    'cambiar el tama�o de la parte codewizard
    fraFrame1(0).Width = DefConc1.Width
    tabTab1(0).Width = fraFrame1(0).Width - 225
    grdDBGrid1(0).Width = tabTab1(0).Width - 500
    
    ' cambiar el tama�o del control de conciertos
    DefConc1.Height = Me.Height - 3450
    DefConc1.Width = Me.Width - 165
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    If strFormName = objDetailInfo.strName Then
'      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        ' Presentarlo en el treeView
        If txtConciertos(0).Text <> "" Then
          Call DefConc1.InitTV("0" & txtConciertos(0).Text, "" & txtConciertos(2).Text)
        End If
    End If
  
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    If strFormName = objDetailInfo.strName Then
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        ' Grabar sus atributos en vacio
        DefConc1.CrearAtribVacio "." & txtConciertos(0).Text & "./"
      End If
    End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  If strFormName = objDetailInfo.strName Then
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      If Trim(txtConciertos(2).Text) = "" Then
        Call MsgBox("La descripci�n del Concierto no puede quedar vacia", vbCritical + vbOKOnly, "Definici�n de Conciertos")
        blnCancel = True
      End If
    End If
  End If
End Sub

Private Sub txtConciertos_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  Dim MiRsFecha As rdoResultset
    
  Me.Move 0, 0, Screen.Width, Screen.Height - 500
'  Me.Height = 8600
'  Me.Height = Screen.Height - 500
'  Me.Width = Screen.Width
    
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
    
    objCW.blnAutoDisconnect = False
    objCW.SetClockEnable False

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
    

  With objDetailInfo
    .strName = "Conciertos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FA0900"
    .strWhere = "FA09INDCONCIERTO = -1"
    Call .FormAddOrderField("FA09DESIG", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "FA0900")
    Call .FormAddFilterWhere(strKey, "FA09DESIG", "Designaci�n", cwString)
'    Call .FormAddFilterWhere(strKey, "FA09DESCUENTO", "Descuento", cwNumeric)
'    Call .FormAddFilterWhere(strKey, "CI13CODENTIDAD", "Entidad", cwString)
'    Call .FormAddFilterWhere(strKey, "FA09FECINICIO", "Fec. Inicio", cwDate)
'    Call .FormAddFilterWhere(strKey, "FA09FECFIN", "Fec. Final", cwDate)
    
    Call .FormAddFilterOrder(strKey, "FA09DESIG", "Designaci�n")
'    Call .FormAddFilterOrder(strKey, "FA09DESCUENTO", "Descuento")
'    Call .FormAddFilterOrder(strKey, "CI13CODENTIDAD", "Entidad")
    
    'Call .objPrinter.Add("FA01001", "Listado de Conceptos Agrupantes")

  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtConciertos(0)).blnValidate = False
    ' codigo de concierto
    .CtrlGetInfo(txtConciertos(0)).blnInFind = False
    .CtrlGetInfo(txtConciertos(0)).blnInGrid = False
    ' Observaciones que apareceran en el pie de factura
    .CtrlGetInfo(txtConciertos(1)).blnInFind = False
    .CtrlGetInfo(txtConciertos(1)).blnInGrid = False
    ' Descripcion del concierto
    .CtrlGetInfo(txtConciertos(2)).blnInFind = True
    .CtrlGetInfo(txtConciertos(2)).blnInGrid = True
    .CtrlGetInfo(chkConciertos).blnInFind = False
    .CtrlGetInfo(chkConciertos).blnInGrid = False
    .CtrlGetInfo(chkFacturable).blnInFind = False
    .CtrlGetInfo(chkFacturable).blnInGrid = False
        
    
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
'  grdDBGrid1(0).Columns(0).Width = 500
'  grdDBGrid1(0).Columns(1).Width = 10770
  grdDBGrid1(0).Columns(1).Width = 10850
'  grdDBGrid1(0).Columns(2).Width = 8350
  Me.Refresh
  
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    FechaDia = MiRsFecha(0)
  End If
  
  Me.txtConciertos(0).Enabled = True
  Me.txtConciertos(2).Enabled = True
  Me.chkConciertos.Enabled = True
  Me.chkFacturable.Enabled = False
  Screen.MousePointer = vbDefault
  Call DefConc1.Inicializa(objApp)
  
  MsgBox "                  ATENCION !                  " & Chr$(10) & Chr$(13) & _
         "          Revise la fecha de proceso          " & Chr$(10) & Chr$(13) & _
         "Las modificaciones se grabar�n con dicha fecha", vbCritical + vbOKOnly, "Comprobar la fecha de proceso"
  
  If objSecurity.strUser = "JOSE" Or objSecurity.strUser = "PRFAC" Then
      DefConc1.ActivarRuta
  End If
  
  
  objApp.BeginTrans
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
                             
  If CambiarConcierto() = True Then
    intCancel = objWinInfo.WinExit
  Else
    intCancel = True
  End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
  objApp.RollbackTrans
  
  objCW.blnAutoDisconnect = True
  objCW.SetClockEnable True

  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    If strFormName = objDetailInfo.strName Then
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            chkConciertos.Value = 1
            objDetailInfo.rdoCursor("FA09INDCONCIERTO") = -1
            
            txtConciertos(0).Text = fNextClave("FA09CODCONC", "FA0900")
            objDetailInfo.rdoCursor("FA09CODNODOCONC") = txtConciertos(0).Text
        End If
    End If
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objDetailInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  
  End Select

End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim Resp As Integer
  If UCase(ActiveControl.Name) = "DEFCONC1" Then
    If btnButton.Key = cwToolBarButtonNew Then
      DefConc1.Nuevo
    ElseIf btnButton.Key = cwToolBarButtonDelete Then
      DefConc1.Borrar
    ElseIf btnButton.Key = cwToolBarButtonSave Then
      objApp.CommitTrans
      DefConc1.Modificado = False
      objApp.BeginTrans
    
    ElseIf btnButton.Key = cwToolBarButtonRefresh Then
      If txtConciertos(0).Text <> "" Then
        Call DefConc1.InitTV("0" & txtConciertos(0).Text, txtConciertos(2).Text)
      End If
      DefConc1.InitTV2
    Else
      If btnButton.Index >= 21 And btnButton.Index <= 24 Then
        If CambiarConcierto() = True Then
          ' redirigir los botones al codewizard
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          
          Call DefConc1.InitTV("0" & txtConciertos(0).Text, txtConciertos(2).Text)
        End If
      Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      End If
      
    End If
  Else
    If btnButton.Key = cwToolBarButtonNew Then
        If CambiarConcierto() = True Then
          ' redirigir los botones al codewizard
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          
          Call DefConc1.InitTV("0" & txtConciertos(0).Text, txtConciertos(2).Text)
        End If

    ElseIf btnButton.Key = cwToolBarButtonDelete Then
        DefConc1.BorrarConcierto "0" & txtConciertos(0).Text
    ElseIf btnButton.Key = cwToolBarButtonSave Then
      objApp.CommitTrans
      DefConc1.Modificado = False
      objApp.BeginTrans
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Call DefConc1.InitTV("0" & txtConciertos(0).Text, txtConciertos(2).Text)
      
    Else
'      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      
      If btnButton.Index >= 21 And btnButton.Index <= 24 Then
        If CambiarConcierto() = True Then
          ' redirigir los botones al codewizard
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          
          Call DefConc1.InitTV("0" & txtConciertos(0).Text, txtConciertos(2).Text)
        End If
      Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      End If
    End If
  End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub txtConciertos_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtConciertos_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Function CambiarConcierto(Optional intCancel As Integer) As Boolean
  CambiarConcierto = DefConc1.CambiarConcierto
End Function

Private Sub InitQRY(SQLInsert As String)
    Dim I As Integer
    
'   Borrar una rama de atributos posteriores a la fecha de proceso
    qry(1).SQL = "DELETE FROM FA1500 " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECINICIO )"
    
    qry(9).SQL = "DELETE FROM FA0700 WHERE FA15CODATRIB IN (select fa15codatrib FROM FA1500 " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECINICIO ))"

'   Marcar (L�gicamente) una rama de registros de atributos con fecha de fin='9/9/0999 00:00:00'
'   estas ramas ser�n las que se copien para crear los nuevos atrib.
'   Parametros:
'            0 - RUTA sin barra final (si la tuviera)
'            1 - Fecha proceso
    qry(2).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN )"
                
    ' esta es la sql que copia los atributos y los modifica a la vez
    ' se genera a partir de la ventana de petici�n de atributos.
    qry(3).SQL = SQLInsert
    
'   Grabar el descuento en los atributos recien generados y que cumplan una serie de condiciones.
'   para que solo se aplique en aquellos nodos que tengan precio o sea susceptibles de generar lineas
'   de factura y que sean descontables
'   Parametros:
'            0 - Descuento
'            1 - RUTA sin barra final (si la tuviera)
'            2 - Fecha proceso
    qry(4).SQL = "UPDATE FA1500 SET " & _
                        "FA15DESCUENTO = ? " & _
                 "WHERE FA15RUTA LIKE ? " & _
                   "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') = FA15FECINICIO ) " & _
                   "AND (TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS') = FA15FECFIN ) " & _
                   "AND (" & _
                   "     (FA15INDFRANQSUMA <> 0)" & _
                   "  OR (FA15INDFRANQUNI  <> 0)" & _
                   "  OR (FA15INDFACTOBLIG <> 0)" & _
                   "  OR (FA15INDLINOBLIG <> 0)" & _
                   "  OR (FA15INDSUPLEMENTO <> 0)" & _
                   "    ) " & _
                   "AND (FA15INDDESCONT <>0)"


'                   "     (FA15PRECIOREF IS NOT NULL)" & _
'                   "  OR (FA15PRECIODIA IS NOT NULL)" & _
'                   "  OR (FA15INDFRANQSUMA <> 0)" & _

'   Borrar (L�gicamente) una rama de registros de atributos con fecha de fin=fecha de proceso
'   Parametros:
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - RUTA sin barra final (si la tuviera)
    qry(5).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA LIKE ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS'))"
                
    
  ' DEPURAR los atributos
  ' se ejecuta primero la 7 (precios por cantidad) y despues la 6 (atributos)
    qry(6).SQL = "DELETE FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN"
    qry(7).SQL = "DELETE FROM FA0700 WHERE FA15CODATRIB IN (SELECT FA15CODATRIB FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN)"

'   Copiar los atributos de la rama del concierto modelo al destino
'   Parametros:
'            0 - ruta a buscar sin barra final (si la tuviera)
'            1 - Fecha Inicio (es la variable FechaProceso)
'            2 - Fecha Inicio (es la variable FechaProceso)
'            3 - Fecha Final(es la variable FechaProceso)
'            4 - Ruta origen sin barra final (si la tuviera)
'            5 - Ruta destino sin barra final (si la tuviera)
    qry(8).SQL = "INSERT INTO FA0700 (FA15CODATRIB, FA07CODDESCUENT, FA07ACANTFIN, FA07PRECIO) " & _
                    "(SELECT FA1500_1.FA15CODATRIB, FA07CODDESCUENT, FA07ACANTFIN, FA07PRECIO " & _
                "FROM FA1500, FA1500 FA1500_1, FA0700 " & _
                "WHERE FA1500_1.FA15RUTA= FA1500.FA15RUTA " & _
                    "AND FA1500.FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
                    "AND FA1500_1.FA15FECFIN = TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
                    "AND FA0700.FA15CODATRIB = FA1500.FA15CODATRIB)"

    For I = 1 To UBound(qry)
        Set qry(I).ActiveConnection = objApp.rdoConnect
        qry(I).Prepared = True
    Next
    
End Sub

Private Sub defconc1_CambiarAtribHijos(Codigo As String, Nombre As String, FechaProceso As String)
    Dim aAtributos(1 To 16) As CamposAtrib
    Dim I As Integer
    Dim Porcentaje As Single
    Dim strRedondeo As String
    Dim miSQLInsert As String
    Dim miSQLSelect As String
    Dim miSQLFrom As String
    Dim miSQLCompleta As String
    
    'Llamar a la ventana de petici�n de atributos
    If VentanaAtributos(aAtributos, Nombre) = True Then
        
        Screen.MousePointer = vbHourglass
        
        ' Generar la SQL de creaci�n de nuevos atributos con los parametros no configurables
        miSQLInsert = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA,  FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15FECINICIO, FA15FECFIN, FA15INDOPCIONAL, FA15INDTramDias, FA15INDTramAcum, FA15PlazoInter, FA15Descripcion, FA15RutaRel, FA15RelFijo, FA15RelPor, " & _
                      "FA15INDNecRnfOrigen, FA15INDNecRnfDestino, FA15INDRecogerCant, FA15INDRecogerValor, CI32CODTIPECON, FA15INDCONTFORF "
        miSQLSelect = ")(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, FA15RUTA, FA15INDFRANQSUMA, FA15INDFRANQUNI, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS'), FA15INDOPCIONAL, FA15INDTramDias, FA15INDTramAcum, FA15PlazoInter, FA15Descripcion, FA15RutaRel, FA15RelFijo, FA15RelPor, " & _
                      "FA15INDNecRnfOrigen, FA15INDNecRnfDestino, FA15INDRecogerCant, FA15INDRecogerValor, CI32CODTIPECON, FA15INDCONTFORF "
        miSQLFrom = " FROM DUAL, FA1500 where fa15ruta like ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS')))"
    
    
        ' a�adir a la SQL los nombres de los campos y los valores posibles
        
        ' las modificaciones de precios dependen de si se modifica
        ' el precio referencia, el precio dia o la variaci�n es porcentual
        miSQLInsert = miSQLInsert & ", " & aAtributos(1).campo
        miSQLInsert = miSQLInsert & ", " & aAtributos(2).campo
            
        If aAtributos(FA15PRECIOREF).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", " & IIf(Not IsNumeric(aAtributos(FA15PRECIOREF).Valor), " NULL", aAtributos(FA15PRECIOREF).Valor)
            miSQLSelect = miSQLSelect & ", NULL"
                
        ElseIf aAtributos(FA15PRECIODIA).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", NULL"
            miSQLSelect = miSQLSelect & ", " & IIf(Not IsNumeric(aAtributos(FA15PRECIODIA).Valor), " NULL", aAtributos(FA15PRECIODIA).Valor)
        
        ElseIf aAtributos(FA15Variaci�nPorcentual).Actualizar = True Then
            ' calculo del porcentaje a aplicar
            Porcentaje = (100 + aAtributos(FA15Variaci�nPorcentual).Valor) / 100
            If aAtributos(FA15Redondeo).Actualizar = False Then
                ' no se aplica redondeo
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje)
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje)
            Else
                strRedondeo = str(aAtributos(FA15Redondeo).Valor)
                miSQLSelect = miSQLSelect & ", ROUND ( " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje) & " / " & strRedondeo & " ) * " & strRedondeo
                miSQLSelect = miSQLSelect & ", ROUND ( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & " / " & strRedondeo & " ) * " & strRedondeo
'                miSQLSelect = miSQLSelect & ", MOD( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & ", " & strRedondeo & " ) * " & strRedondeo
            End If
                
        Else
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo
            
        End If
        
        
        For I = 3 To UBound(aAtributos) - 2
            miSQLInsert = miSQLInsert & ", " & aAtributos(I).campo
            
            If (aAtributos(I).Actualizar = True) And (I <> FA15DESCUENTO) Then
                If aAtributos(I).Valor = "" Then
                    miSQLSelect = miSQLSelect & ", Null"
                Else
                    If IsNumeric(aAtributos(I).Valor) Then
                        miSQLSelect = miSQLSelect & ", " & str(aAtributos(I).Valor)
                    Else
                        miSQLSelect = miSQLSelect & ", " & aAtributos(I).Valor
                    End If
                End If
            Else
                miSQLSelect = miSQLSelect & ", " & aAtributos(I).campo
            End If
        Next
        ' concatenar las tres partes de la SQL
        miSQLCompleta = miSQLInsert + miSQLSelect + miSQLFrom
        
        InitQRY (miSQLCompleta)
        Call Actualizar(Codigo, FechaProceso, aAtributos(FA15DESCUENTO))
        
        Screen.MousePointer = vbDefault
    End If
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Realiza las llamadas a las SQL correspondientes para actualizar los atributos de
' todas las ramas hijas de un nodo.
' recibe como parametros :
'       - Ruta : del nodo del que parte la actualizaci�n
'       - FechaProceso
'       - Descuento : tipo de datos que contiene el descuento a aplicar y si es aplicable
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub Actualizar(Ruta As String, FechaProceso As String, Descuento As CamposAtrib)
    
    objApp.BeginTrans
    On Error GoTo error
    
    ' quitar la barra final si la tiene
    If Right(Ruta, 1) = "/" Then
        Ruta = Left(Ruta, Len(Ruta) - 1)
    End If
    
    Ruta = Ruta & "%"
    
    ' borra primero los precios por cantidad
    qry(9).rdoParameters(0) = Ruta
    qry(9).rdoParameters(1) = FechaProceso
    qry(9).Execute

    ' borrar los atributos posteriores a la fecha
    qry(1).rdoParameters(0) = Ruta
    qry(1).rdoParameters(1) = FechaProceso
    qry(1).Execute
    
    ' marcar los registros de atributos afectados por la modificacion
    qry(2).rdoParameters(0) = Ruta
    qry(2).rdoParameters(1) = FechaProceso
    qry(2).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    qry(3).rdoParameters(0) = FechaProceso
    qry(3).rdoParameters(1) = Ruta
    qry(3).Execute
    
    ' aplicar ( si corresponde) el descuento
    If Descuento.Actualizar = True Then
        qry(4).rdoParameters(0) = Descuento.Valor
        qry(4).rdoParameters(1) = Ruta
        qry(4).rdoParameters(2) = FechaProceso
        qry(4).Execute
    End If
    
    ' Copiar los precios por cantidad
    qry(8).Execute
    
    ' Borrar los registros de atributos afectados por la modificacion
    qry(5).rdoParameters(0) = FechaProceso
    qry(5).rdoParameters(1) = Ruta
    qry(5).Execute
    
    
    ' depurar primero los precios por cantidad
    qry(7).Execute
'     depurar los atributos
    qry(6).Execute
    
    objApp.CommitTrans
    Exit Sub
error:
    Call MsgBox("Error en la actualizaci�n", vbCritical, "Error")
    objApp.RollbackTrans
'    Resume
End Sub


