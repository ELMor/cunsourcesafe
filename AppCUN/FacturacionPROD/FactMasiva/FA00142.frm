VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_FactMasiva 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Proceso de Facturaci�n Masiva"
   ClientHeight    =   7260
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   6405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "Facturar"
      Height          =   375
      Left            =   2340
      TabIndex        =   18
      Top             =   6840
      Width           =   1575
   End
   Begin VB.Frame Frame2 
      Caption         =   " Tipo Econ�mico y Entidad "
      Height          =   2625
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   6225
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1005
         Left            =   3525
         TabIndex        =   12
         Top             =   1530
         Width           =   2310
         Begin VB.OptionButton optHopit 
            Caption         =   "Hospitalizados"
            Height          =   195
            Left            =   225
            TabIndex        =   15
            Top             =   315
            Value           =   -1  'True
            Width           =   1950
         End
         Begin VB.OptionButton optAmbul 
            Caption         =   "Ambulatorios"
            Height          =   195
            Left            =   225
            TabIndex        =   14
            Top             =   630
            Width           =   1950
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Todos"
            Height          =   195
            Left            =   225
            TabIndex        =   13
            Top             =   945
            Visible         =   0   'False
            Width           =   1950
         End
      End
      Begin VB.TextBox txtCodTipEcon 
         Height          =   315
         Left            =   1560
         TabIndex        =   5
         Top             =   270
         Width           =   495
      End
      Begin VB.ComboBox cboCodTipEcon 
         Height          =   315
         Left            =   2160
         TabIndex        =   4
         Top             =   270
         Width           =   3615
      End
      Begin VB.TextBox txtCodEntidad 
         Height          =   315
         Left            =   1560
         TabIndex        =   3
         Top             =   750
         Width           =   495
      End
      Begin VB.ComboBox cboCodEntidad 
         Height          =   315
         Left            =   2160
         TabIndex        =   2
         Top             =   750
         Width           =   3615
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   1095
         TabIndex        =   1
         Top             =   2145
         Width           =   1575
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1575
         TabIndex        =   6
         Tag             =   "Fecha Inicio"
         Top             =   1230
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1575
         TabIndex        =   7
         Tag             =   "Fecha Inicio"
         Top             =   1710
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label lblFecInicio 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Inicio:"
         Height          =   255
         Left            =   45
         TabIndex        =   11
         Top             =   1275
         Width           =   1440
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Final:"
         Height          =   255
         Left            =   45
         TabIndex        =   10
         Top             =   1755
         Width           =   1440
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo Econ�mico:"
         Height          =   255
         Left            =   45
         TabIndex        =   9
         Top             =   390
         Width           =   1440
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Entidad:"
         Height          =   255
         Left            =   45
         TabIndex        =   8
         Top             =   870
         Width           =   1440
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   " Pacientes a Facturar "
      Height          =   4020
      Left            =   90
      TabIndex        =   16
      Top             =   2745
      Width           =   6225
      Begin SSDataWidgets_B.SSDBGrid grdPacientes 
         Height          =   3705
         Left            =   45
         TabIndex        =   17
         Top             =   225
         Width           =   6090
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         HeadLines       =   0
         Col.Count       =   7
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   556
         Columns(0).Caption=   "Selec"
         Columns(0).Name =   "Selec"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   5689
         Columns(1).Caption=   "Paciente"
         Columns(1).Name =   "Paciente"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1773
         Columns(2).Caption=   "Proceso"
         Columns(2).Name =   "Proceso"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1773
         Columns(3).Caption=   "Asistencia"
         Columns(3).Name =   "Asistencia"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "Historia"
         Columns(4).Name =   "Historia"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "FECINICIO"
         Columns(5).Name =   "FECINICIO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "FECFIN"
         Columns(6).Name =   "FECFIN"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   10742
         _ExtentY        =   6535
         _StockProps     =   79
         Caption         =   "Pacientes a facturar"
      End
   End
End
Attribute VB_Name = "frm_FactMasiva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim QrySelec(1 To 15) As New rdoQuery
Dim qry(1 To 10) As New rdoQuery

Private Type typeRNF
  Asistencia As Long
  CodCateg As String
  CodRNF As String
  Cantidad As Long
  Fecha As String
End Type
Dim MatrizRNF() As typeRNF
Dim CadenaConceptos As String

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf

Dim NumHisto As Long
Dim PrimeraVez As Boolean
Dim NumFactura As String
Dim TotalPacientes As Long

Private Sub cboCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If

End Sub

Private Sub cboCodTipEcon_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If
End Sub

Private Sub cboCodTipEcon_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodTipEcon.Clear
  
  SQL = "Select CI32DESTIPECON From CI3200 Where CI32INDRESPECO = -1"
  Set RS = ConD.OpenResultset(SQL, 3)
  If Not RS.EOF Then
    RS.MoveLast
    RS.MoveFirst
    While Not RS.EOF
      cboCodTipEcon.AddItem RS("CI32DESTIPECON") & ""
      RS.MoveNext
    Wend
  End If
End Sub

Private Sub cboCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If

End Sub

Private Sub cmdAceptar_Click()
Dim Seguir As Boolean
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim sqlPersona As String
Dim rsPersona As rdoResultset
Dim Asistencia As Long
Dim Proceso As Long
Dim Persona As String
Dim FechaInicio As String
Dim FechaFin As String
Dim Texto As String
Dim Historia As Long
Dim FecIni As String
Dim FecFin As String

  Me.grdPacientes.RemoveAll
  Seguir = ComprobarCampos()
  If Seguir Then
    
    If Not IsNull(Me.SDCFechaInicio.Date) Then
      FechaInicio = CStr(Me.SDCFechaInicio.Date)
    Else
      FechaInicio = ""
    End If
    If Not IsNull(Me.SDCFechaFin.Date) Then
      FechaFin = CStr(Me.SDCFechaFin.Date)
    Else
      FechaFin = ""
    End If
    
    'Seleccionamos todas los procesos-asistencia de ese tipo econ�mico.
    MiSqL = "Select AD0100.AD01CODASISTENCI, AD0700.AD07CODPROCESO,AD0700.CI21CODPERSONA," & _
                 "AD0100.CI22NUMHISTORIA,AD12CODTIPOASIST,AD0100.AD01FECINICIO,AD0100.AD01FECFIN " & _
                 "From AD0100,AD1100,AD2500,AD0700 " & _
                 "Where AD0100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
                 "AND AD1100.AD07CODPROCESO = AD0700.AD07CODPROCESO " & _
                 "AND AD1100.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
                 "AND CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "' " & _
                 "AND CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "' "
    'MiSql = MiSql & "AND AD01CODASISTENCI NOT IN (SELECT AD01CODASISTENCI FROM FA0400 " & _
                    "WHERE FA0400.AD01CODASISTENCI = AD1100.AD01CODASISTENCI)"
    If Me.optAmbul.Value = True Then
      MiSqL = MiSqL & " AND AD12CODTIPOASIST = 2 "
    ElseIf Me.optHopit.Value = True Then
      MiSqL = MiSqL & " AND AD12CODTIPOASIST = 1 "
    End If
    If Me.txtCodEntidad = "NA" Then
      If Me.optHopit.Value = True Then
        If FechaInicio <> "" Then
          MiSqL = MiSqL & " AND AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
       End If
        If FechaFin <> "" Then
          MiSqL = MiSqL & " AND AD01FECFIN <= TO_DATE('" & FechaFin & "','DD/MM/YYYY') "
        End If
      ElseIf Me.optAmbul.Value = True Then
        MiSqL = MiSqL & " AND (AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
        MiSqL = MiSqL & " OR AD01FECFIN IS NULL)"
      End If
      MiSqL = MiSqL & " AND AD11FECFIN IS NULL AND ad2500.ad25fecfin is null"
    Else
      If optAmbul.Value = True Then
        If FechaInicio <> "" Then
          MiSqL = MiSqL & " AND AD01FECINICIO >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
       End If
        If FechaFin <> "" Then
          MiSqL = MiSqL & " AND AD01FECINICIO <= TO_DATE('" & FechaFin & "','DD/MM/YYYY') "
        End If
      ElseIf Me.optHopit.Value = True Then
        MiSqL = MiSqL & " AND (AD01FECFIN >= TO_DATE('" & FechaInicio & "','DD/MM/YYYY') "
        MiSqL = MiSqL & " OR AD01FECFIN IS NULL)"
      End If
      MiSqL = MiSqL & " AND AD11FECFIN IS NULL AND ad2500.ad25fecfin is null"
    End If
    MiSqL = MiSqL & " ORDER BY AD01FECINICIO"
    Set MiRs = ConD.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      TotalPacientes = MiRs.RowCount
      While Not MiRs.EOF
        sqlPersona = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL From CI2200 Where CI21CODPERSONA = " & MiRs("CI21CODPERSONA") & ""
        Set rsPersona = ConD.OpenResultset(sqlPersona, 3)
        If Not rsPersona.EOF Then
          Persona = rsPersona("CI22NOMBRE") & " " & rsPersona("CI22PRIAPEL") & " " & rsPersona("CI22SEGAPEL")
        End If
        If Not IsNull(MiRs("AD01CODASISTENCI")) Then
          Asistencia = CLng(MiRs("AD01CODASISTENCI"))
        Else
          Asistencia = 0
        End If
        If Not IsNull(MiRs("AD07CODPROCESO")) Then
          Proceso = CLng(MiRs("AD07CODPROCESO"))
        Else
          Proceso = 0
        End If
        If Not IsNull(MiRs("CI22NUMHISTORIA")) Then
          Historia = CLng(MiRs("CI22NUMHISTORIA"))
        Else
          Historia = 0
        End If
        If Not IsNull(MiRs("AD01FECINICIO")) Then
          FecIni = Format(MiRs("AD01FECINICIO"), "DD/MM/YYYY")
        Else
          FecIni = Null
        End If
        If Not IsNull(MiRs("AD01FECFIN")) Then
          FecFin = Format(MiRs("AD01FECFIN"), "DD/MM/YYYY")
        Else
          FecFin = ""
        End If
        Texto = "-1" & Chr(9) & Persona & Chr(9) & Proceso & Chr(9) & Asistencia & Chr(9) & Historia & Chr(9) & FecIni & Chr(9) & FecFin
        grdPacientes.AddItem Texto
        MiRs.MoveNext
      Wend
    End If
  End If
  
End Sub
Sub BuscarPruebas(Proceso As Long, Asistencia As Long)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim SqlCateg As String
Dim rsCateg As rdoResultset
Dim Categoria As String
Dim Texto As String

  '*1
  QrySelec(1).rdoParameters(0) = Proceso
  QrySelec(1).rdoParameters(1) = Asistencia
  'MiSqL = "SELECT * FROM FA0300 WHERE AD07CODPROCESO = " & Proceso & " AND AD01CODASISTENCI = " & Asistencia & _
            " AND FA03INDREALIZADO = 0"
  'Set MiRs = COND.OpenResultset(MiSqL, 3)
  Set MiRs = QrySelec(1).OpenResultset(rdOpenKeyset)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      If Not IsNull(MiRs("FA05CODCATEG")) Then
        '*10
        QrySelec(10).rdoParameters(0) = MiRs("FA05CODCATEG")
        'SqlCateg = "Select FA05DESIG From FA0500 Where FA05CODCATEG = " & MiRs("FA05CODCATEG")
        'Set rsCateg = COND.OpenResultset(SqlCateg, 3)
        Set rsCateg = QrySelec(10).OpenResultset(rdOpenKeyset)
        If Not rsCateg.EOF Then
          Categoria = rsCateg("FA05DESIG")
          Texto = 1 & Chr(9) & Categoria & Chr(9) & Format(MiRs("FA03fecha"), "dd/mm/yyyy") & Chr(9) & 0 & MiRs("FA03CANTIDAD") & Chr(9) & MiRs("FA05CODCATEG")
          'Me.grdPruebas.AddItem Texto
        Else
          Categoria = ""
        End If
      End If
      MiRs.MoveNext
    Wend
  Else
'    Call cmdContinuar_Click
  End If
End Sub


Sub InicQrySelec()
Dim MiSqL As String
Dim x As Integer
  
  '+1
  'Proceso - Asistencia
  MiSqL = "SELECT * FROM FA0300 " & _
            "WHERE AD07CODPROCESO = ? AND AD01CODASISTENCI = ? " & _
            " AND FA03INDREALIZADO = 0"
  QrySelec(1).SQL = MiSqL
  '+2
  'Codigo de la persona
  MiSqL = "Select AD07CODPROCESO, AD07DESNOMBPROCE FROM AD0700 WHERE CI21CODPERSONA = ? " & _
                " AND AD07FECHORAINICI >= TO_DATE('01/01/1998','DD/MM/YYYY') ORDER BY AD07FECHORAINICI DESC"
  QrySelec(2).SQL = MiSqL
  '+3
  'Proceso
  MiSqL = "Select AD0800.AD01CODASISTENCI, AD0800.AD07CODPROCESO,AD0100.AD01FECINICIO," & _
               " AD0100.AD01FECFIN,AD0200.AD02DESDPTO,SG0200.SG02NOM,SG0200.SG02APE1,SG0200.SG02APE2," & _
               " AD1100.CI32CODTIPECON,AD1100.CI13CODENTIDAD" & _
               " FROM AD0800, AD0100,AD0200,SG0200,AD1100,AD0500" & _
               " WHERE AD0800.AD07CODPROCESO = ? " & _
               " AND (AD0100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI) " & _
               " AND (AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI AND " & _
                     "AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO) " & _
               " AND (AD0500.AD02CODDPTO = AD0200.AD02CODDPTO) AND " & _
                     "(AD0500.SG02COD = SG0200. SG02COD) " & _
               " AND AD0500.AD05FECFINRESPON IS NULL " & _
               " AND (AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI AND " & _
                     "AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO)" & _
               " AND AD1100.AD11FECFIN IS NULL " & _
               " ORDER BY AD0100.AD01FECINICIO DESC"
  
  'MiSqL = "Select AD01CODASISTENCI FROM  AD0800 WHERE AD07CODPROCESO = ? ORDER BY AD01CODASISTENCI DESC"
  QrySelec(3).SQL = MiSqL
  '+4
  'Asistencia
  MiSqL = "Select AD01FECINICIO, AD01FECFIN From AD0100 Where AD01CODASISTENCI = ?"
  QrySelec(4).SQL = MiSqL
  '+5
  'Asistencia-Proceso
  MiSqL = "Select FA04NUMFACT, FA04NUMFACREAL From FA0400 Where AD01CODASISTENCI = ? " & _
             " AND AD07CODPROCESO = ?"
  QrySelec(5).SQL = MiSqL
  '+6
  MiSqL = "SELECT FA08DESVISTA FROM FA0800 WHERE FA11CODESTGRUPO = 1"
  QrySelec(6).SQL = MiSqL
  '+7
  'Proceso-Asistencia
  MiSqL = "SELECT * FROM FA0300 WHERE AD07CODPROCESO = ? AND AD01CODASISTENCI = ? AND FA03INDREALIZADO <> 0" & _
              " AND FA04NUMFACT IS NULL " & _
             " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
  QrySelec(7).SQL = MiSqL
  '+8
  'N� de historia
  MiSqL = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, " & _
              "CI22FECNACIM, CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
              " from CI2200, CI1000" & _
              " where CI22NUMHISTORIA = ? " & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " AND CI1000.CI10INDDIRPRINC = -1)"
  QrySelec(8).SQL = MiSqL
  '+9
  MiSqL = " SELECT FA09CODNODOCONC FROM AD1100,CI1300 " & _
            " WHERE AD1100.CI32CODTIPECON=CI1300.CI32CODTIPECON " & _
            " AND AD1100.CI13CODENTIDAD=CI1300.CI13CODENTIDAD" & _
            " AND AD01CODASISTENCI = ? AND AD07CODPROCESO = ?"
  QrySelec(9).SQL = MiSqL
  
  '+10
  MiSqL = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  QrySelec(10).SQL = MiSqL
  
    ' borrar los registros de RNFs no facturados de la fa0300 para ese proceso, asistencia
    MiSqL = "DELETE from FA0300 " & _
            "Where AD07CODPROCESO = ? AND AD01CODASISTENCI =? AND FA04NUMFACT is NULL"
    QrySelec(11).SQL = MiSqL

  'Activamos las querys.
  For x = 1 To 11
    Set QrySelec(x).ActiveConnection = ConD
    QrySelec(x).Prepared = True
  Next


End Sub
' Mediante la funci�n GuardarFactura rellenaremos los ficheros FA0400 (cabeceras de factura) y
' FA1600 (L�neas de factura)
'
Sub GuardarFactura(Historia As Long)
Dim CodFactura As Long
Dim Dto As Double
Dim IntContLineas As Integer
Dim Facturada As Boolean
Dim SubImporte As Double
Dim IntCont As Integer
Dim rsPaciente As rdoResultset
Dim intPos As Integer
Dim lngProceso As Long
Dim lngAsistencia As Long
Dim Precio As Double
Dim Maximo As Integer
Dim Cont As Integer
Dim Cadena As String
Dim intPosPar As Integer
Dim x As Integer

  On Error GoTo ErrorenGrabacion
  
  'Abrimos una transacci�n para tratar toda la inserci�n de las facturas.
  ConD.BeginTrans
    
    'Nos posicionamos en la propuesta seleccionada dentro del objeto myFactura
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set paciente = varFact.Pacientes(CStr(Historia))
    Set Asistencia = paciente.Asistencias(1)
    Asistencia.Facturado = True
    intPos = InStr(1, Asistencia.Codigo, "-")
    lngProceso = CLng(Left(Asistencia.Codigo, intPos - 1))
    lngAsistencia = CLng(Right(Asistencia.Codigo, Len(Asistencia.Codigo) - intPos))
    Maximo = 0
    Precio = 0
    For x = 1 To 6
      Cont = 1
      For Each PropuestaFact In Asistencia.PropuestasFactura
        
        'Orden de selecci�n
        '1-> Forfait Quir �rgico
        '2-> Exploracione s Especiales
        '3-> Forfait Esta ncias
        '4-> Forfait Prue bas
        '5-> Forfait Ambu latorio
        '6-> Acto M�dico
        Cadena = Right(PropuestaFact.Name, Len(PropuestaFact.Name) - 10)
        If x = 1 And Left(Cadena, 12) = "Forfaits Qui" Then
          Maximo = Cont
          Exit For
        ElseIf x = 2 And Left(Cadena, 12) = "Exploracione" Then
          Maximo = Cont
          Exit For
        ElseIf x = 3 And Left(Cadena & Space(12), 12) = "Estancias   " Then
          Maximo = Cont
          Exit For
        ElseIf x = 4 And Left(Cadena, 12) = "Forfaits Pru" Then
          Maximo = Cont
          Exit For
        ElseIf x = 5 And Left(Cadena, 12) = "Forfait Ambu" Then
          Maximo = Cont
          Exit For
        End If
        'Maximo = 1
        Cont = Cont + 1
      Next
      If Cont <> 0 Then
        Exit For
      End If
    Next
    
    If Maximo = 0 Then Maximo = 1
    Set PropuestaFact = Asistencia.PropuestasFactura(Maximo)
    
    '--------------------------------------------------------------------------------------------
    'Seleccionar los Responsables econ�micos del grid de responsables econ�micos.
    'Comprobaremos que el responsable tiene l�neas de factura.
    'En caso afirmativo a�adiremos una nueva factura con ese responsable.
    '--------------------------------------------------------------------------------------------
    'Nos posicionamos en la primera linea del grid.
      'Inicializamos la varible que nos indica si la factura est� a�adida a la BD.
        Facturada = False
        IntContLineas = 1
        For Each NodoFACT In PropuestaFact.NodosFactura
          For Each Nodos In NodoFACT.Nodos
            'Comprobamos que la l�nea se va a facturar
            If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
              'Comprobamos si es el responsable principal, en ese caso le asignaremos tambi�n los que no tienen R.E.
              'Responsable Principal
              If Facturada = False Then
                'A�adiremos la factura
                CodFactura = fNextClave("FA04CODFACT", "FA0400")
                If NumFactura = "" Then
                  NumFactura = Me.txtCodTipEcon & Me.txtCodEntidad & IIf(Me.optAmbul.Value = True, "2", "1") & "/" & CodFactura
                End If
                qry(4).rdoParameters(0) = CodFactura 'Secuencia
                qry(4).rdoParameters(1) = NumFactura '"13/" & CodFactura
                qry(7).rdoParameters(0) = paciente.Codigo
                Set rsPaciente = qry(7).OpenResultset
                If Not rsPaciente.EOF Then
                  qry(4).rdoParameters(2) = rsPaciente(0) 'Paciente
                Else
                  qry(4).rdoParameters(2) = Null
                End If
                'If grdssResponsable.Columns(5).Text = "E" Then
                  qry(4).rdoParameters(3) = Me.txtCodEntidad.Text 'Entidad
                'Else
                '  qry(4).rdoParameters(3) = Null
                'End If
                'If Trim(Me.cboColetillas.Text) = "" Then
                  qry(4).rdoParameters(4) = Null
                'Else
                '  qry(4).rdoParameters(4) = Trim(Me.cboColetillas.Text) 'Descripcion
                'End If
                qry(4).rdoParameters(5) = 0 'txtTotal(grdssResponsable.Columns(3).Text) 'Cantidad Facturada
                qry(4).rdoParameters(6) = 0 '& txtDcto(grdssResponsable.Columns(3).Text) 'Dto aplicado
                qry(4).rdoParameters(7) = lngAsistencia 'N�mero de la asistencia
                'If Trim(txtObservaciones(grdssResponsable.Columns(3).Text)) = "" Then
                  qry(4).rdoParameters(8) = Null
                'Else
                '  qry(4).rdoParameters(8) = txtObservaciones(grdssResponsable.Columns(3).Text) 'Observaciones
                'End If
                qry(4).rdoParameters(9) = NodoFACT.codConcierto
                qry(4).rdoParameters(10) = lngProceso
                qry(4).rdoParameters(11) = Nodos.CodTipEcon
                qry(4).Execute
                Facturada = True
              End If
              qry(5).rdoParameters(0) = CodFactura 'C�digo de la factura
              qry(5).rdoParameters(1) = IntContLineas 'N� de L�nea
              If Not IsNull(Nodos.Codigo) Then
                qry(5).rdoParameters(2) = Nodos.Codigo 'C�digo de la categor�a (si tiene)
              Else
                qry(5).rdoParameters(2) = Null
              End If
              If Nodos.Descripcion <> "" Then
                qry(5).rdoParameters(3) = Nodos.Descripcion
              Else
                qry(5).rdoParameters(3) = Nodos.Name 'Descripci�n
              End If
              qry(5).rdoParameters(4) = Null '?????????????????????????????
              Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
              qry(5).rdoParameters(5) = Nodos.LineaFact.Precio + Nodos.LineaFact.PrecioNoDescontable
              qry(5).rdoParameters(6) = Dto 'Dto de la l�nea incluido redondeo
              qry(5).rdoParameters(7) = Format(Nodos.LineaFact.Total + Nodos.LineaFact.TotalNoDescontable, "#########.00")
              qry(5).rdoParameters(8) = Nodos.OrdImp
              qry(5).Execute
              For Each rnf In Nodos.RNFs
                'Asignamos a los rnfs de la FA0300, el n�mero de la factura y la l�nea
                qry(6).rdoParameters(0) = CodFactura
                qry(6).rdoParameters(1) = IntContLineas
                If rnf.Desglose = True Then
                  qry(6).rdoParameters(2) = 1
                Else
                  qry(6).rdoParameters(2) = 0
                End If
                qry(6).rdoParameters(3) = rnf.CodAtrib
                qry(6).rdoParameters(4) = rnf.CodCateg
                qry(6).rdoParameters(5) = lngAsistencia
                qry(6).rdoParameters(6) = lngProceso
                qry(6).Execute
              Next
              IntContLineas = IntContLineas + 1
            End If
          Next
        Next
    
  'Se ha grabado todo cerramos la transacci�n
  ConD.CommitTrans
Exit Sub

ErrorenGrabacion:
  'Se ha producido un error en la grabaci�n, anulamos la transacci�n
  ConD.RollbackTrans
  'Resume Next
  'MsgBox "Se ha producido un error en la grabaci�n.", vbOKOnly + vbCritical, "Atenci�n"
  Write #1, "Fallo en grabaci�nd de factura de: " & Historia
End Sub
Sub IniciarQRY()
Dim MiSqL As String
Dim x As Integer
  
  ''Queries relacionadas con la obtenci�n de una entidad a partir del n�mero de asistencia.
  'Seleccionar el c�digo de entidad a partir del n�mero de asistencia.
  MiSqL = "SELECT CI32CODTIPECON, CI13CODENTIDAD FROM AD1100 WHERE AD01CODASISTENCI = ? " & _
               "AND AD07CODPROCESO = ?"
  qry(1).SQL = MiSqL
  'Seleccionar la descripci�n de la entidad
  MiSqL = "SELECT CI13DESENTIDAD, CI13CODENTIDAD FROM CI1300 WHERE CI13CODENTIDAD = ? AND CI32CODTIPECON = ?"
  qry(2).SQL = MiSqL
  
  'Seleccionar la descripci�n de una categor�a.
  MiSqL = "Select FA05DESIG from FA0500 Where FA05CODCATEG = ?"
  qry(3).SQL = MiSqL
  
  'Insertar una cabecera de factura en FA0400
  MiSqL = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
              "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV," & _
              "FA09CODNODOCONC,AD07CODPROCESO,CI32CODTIPECON) Values (?,?,?,?,?,?,?,?,?,?,?,?)"
  qry(4).SQL = MiSqL
  'Isertar una l�nea de factura en FA1600
  MiSqL = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
              "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE,FA16ORDFACT) Values (?,?,?,?,?,?,?,?,?)"
  qry(5).SQL = MiSqL
  'Asignar a FA0300 el numero de la factura y el n�mero de l�nea
  MiSqL = "Update FA0300 set FA04NUMFACT = ?, FA16NUMLINEA = ?, FA03INDDESGLOSE = ?, FA15CODATRIB = ? " & _
              "Where FA05CODCATEG = ? AND AD01CODASISTENCI = ? AND AD07CODPROCESO = ?"
  qry(6).SQL = MiSqL
  
  MiSqL = "Select CI21CODPERSONA From CI2200 Where CI22NUMHISTORIA = ?"
  qry(7).SQL = MiSqL
  'Activamos las querys.
  For x = 1 To 7
    Set qry(x).ActiveConnection = ConD
    qry(x).Prepared = True
  Next
End Sub


Sub GenerarSqlRNF(Proceso As Long, Asistencia As Long, FechaInicio As String, FechaFin As String)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Cadena As String
  
    ' primero se borran los RNFs existentes en la FA0300 del mismo
    ' proceso-asistencia y que no esten facturados
    QrySelec(11).rdoParameters(0) = Proceso
    QrySelec(11).rdoParameters(1) = Asistencia
    QrySelec(11).Execute

  Cadena = ""
  'Seleccionamos todas las vistas de los grupos que se encuentren activos
  '*6
  'MiSqL = "SELECT FA08DESVISTA FROM FA0800 WHERE FA11CODESTGRUPO = 1"
  'Set MiRs = COND.OpenResultset(MiSqL, 3)
  Set MiRs = QrySelec(6).OpenResultset(rdOpenKeyset)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      'Creamos la SQL de selecci�n de los RNF
      'MiSqL = "INSERT INTO FA0300 " & _
                   "(FA03NUMRNF,FA05CODCATEG,FA03CANTIDAD,FA13CODESTRNF,AD01CODASISTENCI," & _
                   " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO) " & _
                   " SELECT FA03NUMRNF_SEQUENCE.NEXTVAL, " & _
                   " FA05CODCATEG, CANTIDAD, 1, AD01CODASISTENCI, " & _
                   " TO_DATE(FECHA,'DD/MM/YYYY'), CI22NUMHISTORIA, OBS, AD07CODPROCESO, ESTADO FROM " & MiRs(0) & _
                   " WHERE AD07CODPROCESO =  " & Proceso & " AND AD01CODASISTENCI =  " & Asistencia & _
                   " AND (AD07CODPROCESO,AD01CODASISTENCI,FA05CODCATEG,to_char(FECHA,'dd/mm/yyyy'),CANTIDAD) " & _
                   " NOT IN (SELECT  AD07CODPROCESO,AD01CODASISTENCI,FA05CODCATEG,FA03FECHA,FA03CANTIDAD " & _
                   "FROM FA0300 WHERE AD07CODPROCESO = " & Proceso & " AND AD01CODASISTENCI= " & Asistencia & ")"
      MiSqL = "INSERT INTO FA0300 " & _
                  "(FA03NUMRNF,FA05CODCATEG,FA03CANTIDAD,FA13CODESTRNF,AD01CODASISTENCI, " & _
                  " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO) " & _
                  " SELECT FA03NUMRNF_SEQUENCE.NEXTVAL,  FA05CODCATEG, CANTIDAD, 1, " & _
                  " AD01CODASISTENCI,  FECHA, CI22NUMHISTORIA, OBS, " & _
                  " AD07CODPROCESO, ESTADO FROM " & MiRs(0) & " WHERE AD07CODPROCESO = " & Proceso & _
                  " AND AD01CODASISTENCI =  " & Asistencia & " AND " & _
                  IIf(FechaInicio <> "", "FECHA > TO_DATE('" & FechaInicio & " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS') AND ", "") & _
                  IIf(FechaFin <> "", "FECHA <= TO_DATE('" & FechaFin & " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') AND ", "") & _
                  " (AD07CODPROCESO,AD01CODASISTENCI,FA05CODCATEG, to_char(FECHA,'dd/mm/yyyy')," & _
                  " CANTIDAD)  NOT IN " & _
                  " (SELECT " & _
                  " AD07CODPROCESO,AD01CODASISTENCI,FA05CODCATEG,to_char(FA03FECHA,'dd/mm/yyyy'),FA03CANTIDAD " & _
                  " From " & _
                  " FA0300 WHERE AD07CODPROCESO = " & Proceso & " AND AD01CODASISTENCI= " & Asistencia & ")"
  
      ConD.Execute MiSqL
      MiRs.MoveNext
      
    Wend
  End If
End Sub

Sub LlenarTypeRNF(Proceso As Long, Asist As Long)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim sqlFecha As String
Dim rsFecha As rdoResultset
Dim rsNombre As rdoResultset
Dim MiSqLConc As String
Dim MiRsConc As rdoResultset
Dim Cont As Long
Dim NumRNF As Long
Dim ContRNF As Long
Dim Fila As Integer
Dim RNFAnterior As String
Dim AsistAnterior As String
Dim ProcAnterior As String
Dim PacienteAnterior As String
Dim FechaInicio As String
Dim FechaFinal As String
Dim Fecha As String

  On Error GoTo ErrorenRNF
  
  RNFAnterior = ""
  AsistAnterior = ""
  PacienteAnterior = ""
  ProcAnterior = ""
  ContRNF = 0
  'Si la cadena de selecci�n de RNF no es nula.
  '*7
  QrySelec(7).rdoParameters(0) = Proceso
  QrySelec(7).rdoParameters(1) = Asist
  'MiSqL = "SELECT * FROM FA0300 WHERE AD07CODPROCESO = " & Proceso & " AND AD01CODASISTENCI = " & Asist & _
                " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
    'Set MiRs = COND.OpenResultset(MiSqL, 3)
    Set MiRs = QrySelec(7).OpenResultset(rdOpenKeyset)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      ReDim MatrizRNF(1 To MiRs.RowCount)
      Cont = 1
      ContRNF = 0
      ConD.BeginTrans
        While Not MiRs.EOF
          If MiRs("CI22NUMHISTORIA") <> PacienteAnterior Then
            'Se ha detectado un nuevo n�mero de historia, creamos el paciente.
            Set paciente = New paciente
            paciente.Codigo = MiRs("ci22numhistoria")
            'Fila = FilaDelGrid(MiRs("CI22NUMHISTORIA"))
            'paciente.Name = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
            '*8
            QrySelec(8).rdoParameters(0) = MiRs("ci22numhistoria")
            'MiSqL = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, " & _
                        "CI22FECNACIM, CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
                        " from CI2200, CI1000" & _
                        " where CI22NUMHISTORIA = " & IdPersona1.Historia & _
                        " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
                        " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)"
            'Set rsNombre = COND.OpenResultset(MiSqL, 3)
            Set rsNombre = QrySelec(8).OpenResultset(rdOpenKeyset)
            If Not rsNombre.EOF Then
              rsNombre.MoveFirst
              paciente.Direccion = rsNombre("CI10CALLE") & " " & rsNombre("CI10PORTAL") & " " & rsNombre("CI10RESTODIREC") & ", " & rsNombre("CI10DESLOCALID")
              paciente.FecNacimiento = rsNombre("CI22FECNACIM") & ""
            Else
              paciente.Direccion = ""
              paciente.FecNacimiento = ""
            End If
            varFact.Pacientes.Add paciente, MiRs("CI22NUMHISTORIA") & ""
            PacienteAnterior = MiRs("CI22NUMHISTORIA")
          End If
          If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
            'Se ha detectado un nuevo n�menro de asistencia, creamos la asistencia.
            Set Asistencia = New Asistencia
            'Esto es provisional, habr� que borrarlo despu�s
'            Asistencia.AddConcierto 97
            '*9
            QrySelec(9).rdoParameters(0) = MiRs("AD01CODASISTENCI")
            QrySelec(9).rdoParameters(1) = MiRs("AD07CODPROCESO")
            'MiSqLConc = " SELECT FA09CODNODOCONC FROM AD1100,CI1300 " & _
                                  " WHERE AD1100.CI32CODTIPECON=CI1300.CI32CODTIPECON " & _
                                  " AND AD1100.CI13CODENTIDAD=CI1300.CI13CODENTIDAD" & _
                                  " AND AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & _
                                  " AND AD07CODPROCESO = " & MiRs("AD07CODPROCESO")
            'Set MiRsConc = COND.OpenResultset(MiSqLConc, 3)
            Set MiRsConc = QrySelec(9).OpenResultset(rdOpenKeyset)
            If Not MiRsConc.EOF Then
              If Trim(MiRsConc(0)) <> "" Then
                If IsNumeric(MiRsConc(0)) Then
                  Asistencia.AddConcierto MiRsConc(0)
                End If
              End If
            Else
              MsgBox "Ante la no existencia de concierto se aplica el de Privado A", vbOKOnly + vbInformation, "Aviso"
              Asistencia.AddConcierto 97
            End If
            Asistencia.Codigo = MiRs("AD07CODPROCESO") & "-" & MiRs("AD01CODASISTENCI")
            sqlFecha = "Select AD01FECINICIO, AD01FECFIN From AD0100" & _
                            " Where AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI")
            Set rsFecha = ConD.OpenResultset(sqlFecha, 3)
            If Not rsFecha.EOF Then
              If Not IsNull(rsFecha("AD01FECINICIO")) Then
                Asistencia.FecInicio = Format(rsFecha("AD01FECINICIO"), "DD/MM/YYYY HH:MM:SS")
              End If
              If Not IsNull(rsFecha("AD01FECFIN")) Then
                If Format(rsFecha("AD01FECFIN"), "DD/MM/YYYY") > "31/12/1999" Then
                  Asistencia.FecFin = Format(Me.SDCFechaFin.Date, "DD/MM/YYYY HH:MM:SS")
                Else
                  Asistencia.FecFin = Format(rsFecha("AD01FECFIN"), "DD/MM/YYYY HH:MM:SS")
                End If
              End If
            End If
            Asistencia.Facturado = False
            paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
            AsistAnterior = MiRs("AD01CODASISTENCI")
            ProcAnterior = MiRs("AD07CODPROCESO")
            If Not IsNull(rsFecha("AD01FECINICIO")) Then
              FechaInicio = Format(rsFecha("AD01FECINICIO"), "DD/MM/YYYY HH:MM:SS")
            End If
            If Not IsNull(rsFecha("AD01FECFIN")) Then
              FechaFinal = Format(rsFecha("AD01FECFIN"), "DD/MM/YYYY HH:MM:SS")
            End If
          End If
          With Asistencia
            If MiRs("FA05CODCATEG") <> RNFAnterior Then
              Cont = 0
              RNFAnterior = MiRs("FA05CODCATEG")
            Else
              Cont = Cont + 1
            End If
            Set NuevoRNF = New rnf
            With NuevoRNF
              .CodCateg = MiRs("FA05CODCATEG")
              .Fecha = MiRs("FA03FECHA") & ""
              .Cantidad = MiRs("FA03CANTIDAD")
              .Asignado = False
              .Descripcion = MiRs("FA03OBSERV") & ""
              .Key = MiRs("FA05CODCATEG") & "-" & Format(Cont, "000")
            End With
           .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & Format(Cont, "000")
            MiRs.MoveNext
            If Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS") < FechaInicio Then
              FechaInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
              Asistencia.FecInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
            End If
            If Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS") > FechaFinal Then
              FechaFinal = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
              Asistencia.FecFin = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
            End If
          End With
        Wend
      ConD.CommitTrans
    Else
      Write #1, "Sin RNFs" & Proceso & " " & Asist
      'MsgBox "No hay ning�n registro que responda a las condiciones del filtro", vbOKOnly + vbInformation, "aviso"
      BotonSalir = True
    End If
  
Exit Sub
ErrorenRNF:
  MsgBox "Se ha producido un error en la inserci�n de los RNF", vbCritical + vbOKOnly, "Aviso"

  ConD.RollbackTrans


End Sub
Private Function ComprobarCampos() As Boolean
    
    'Comprobamos que haya un tipo econ�mico
    If Trim(Me.txtCodTipEcon.Text) = "" Then
      MsgBox "Se debe seleccionar un Tipo Econ�mico.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodTipEcon.Enabled = True Then
        Me.txtCodTipEcon.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que haya una entidad
    If Trim(Me.txtCodEntidad.Text) = "" Then
      MsgBox "Se debe seleccionar una entidad.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodEntidad.Enabled = True Then
        Me.txtCodEntidad.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que la fecha de inicio sea correcta
    If Not IsNull(Me.SDCFechaInicio.Date) Then
      If Not IsDate(Me.SDCFechaInicio.Date) Then
        MsgBox "La Fecha de Inicio no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaInicio.Enabled = True Then
          Me.SDCFechaInicio.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de finalizaci�n sea correcta
    If Not IsNull(Me.SDCFechaFin.Date) Then
      If Not IsDate(Me.SDCFechaFin.Date) Then
        MsgBox "La Fecha Fin no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaFin.Enabled = True Then
          Me.SDCFechaFin.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de inicio no sea mayor que la de finalizaci�n.
    If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
      MsgBox "La fecha de inicio no puede ser superior a la de finalizaci�n.", vbOKOnly + vbInformation, "Aviso"
      If Me.SDCFechaFin.Enabled = True Then
        Me.SDCFechaFin.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    ComprobarCampos = True
End Function

Private Sub grdConciertos_InitColumnProps()

End Sub

Private Sub SSDBGrid1_InitColumnProps()

End Sub

Private Sub cmdFacturar_Click()
Dim Cont As Integer
Dim TotLineas As Integer
Dim Proclinea As Long
Dim AsisLinea As Long
Dim FecIniLinea As String
Dim FecFinLinea As String
Dim HistLinea As Long

  '0 -> Valor
  '1 -> Nombre
  '2 -> Proceso
  '3 -> Asistencia
  '4 -> Historia
  Open "C:\ESTAD" & Me.txtCodTipEcon.Text & Me.txtCodEntidad.Text & ".LOG" For Append As #1
  grdPacientes.MoveFirst
  TotLineas = grdPacientes.Rows
  For Cont = 1 To TotLineas
    frm_FactMasiva.Caption = "Facturando Paciente n� " & Cont & " de " & TotalPacientes
    'If CDate(grdPacientes.Columns(5).Text) > CDate("01/01/2000") Then
    'Else
      If grdPacientes.Columns(0).Value = -1 Then
        Proclinea = grdPacientes.Columns(2).Text
        AsisLinea = grdPacientes.Columns(3).Text
        HistLinea = grdPacientes.Columns(4).Text
        FecIniLinea = grdPacientes.Columns(5).Text
        FecFinLinea = grdPacientes.Columns(6).Text
        If Proclinea <> 0 And AsisLinea <> 0 And HistLinea <> 0 Then
          Set varFact = New Factura
          Call GenerarSqlRNF(Proclinea, AsisLinea, FecIniLinea, FecFinLinea)
          'Call BuscarPruebas(Proclinea, AsisLinea)
          Call LlenarTypeRNF(Proclinea, AsisLinea)
          varFact.AsignarRNFs
          Call GuardarFactura(HistLinea)
        End If
      End If
    'End If
    grdPacientes.MoveNext
  Next
  Close #1
  MsgBox "A POR OTRO OE"
  
End Sub


Private Sub Form_Load()
  Set ConD = rdoEnvironments(0).OpenConnection("ORACLE73_EST")
  Call Me.IniciarQRY
  Call Me.InicQrySelec
End Sub

Private Sub txtCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodEntidad.Text = RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If


End Sub


Private Sub txtCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI32DESTIPECON From CI3200 Where CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodTipEcon.Text = RS("CI32DESTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodTipEcon.Text = ""
      Me.txtCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If


End Sub
Private Sub cboCodEntidad_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If
End Sub

Private Sub cboCodEntidad_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodEntidad.Clear
  
  If Trim(txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI32CODTIPECON = '" & Me.txtCodTipEcon & "'"
    Set RS = ConD.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        cboCodEntidad.AddItem RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    End If
  Else
    MsgBox "Debe seleccionar un tipo econ�mico", vbInformation + vbOKOnly, "Aviso"
  End If
End Sub

