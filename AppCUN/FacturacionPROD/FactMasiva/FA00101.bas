Attribute VB_Name = "modFunciones"
Option Explicit





Public Function fNextClave(campo$, tabla$) As String
'Devuelve el siguiente valor de la clave principal obtenido del sequence
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = ConD.OpenResultset(SQL)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
    
End Function
Public Function fValidarDecimales(Tecla As Integer, Texto As String) As Integer
Dim strComa As String
Dim intPos As Integer

  Select Case Tecla
    Case 48 To 57
      fValidarDecimales = Tecla
      Exit Function
    Case 8
      fValidarDecimales = Tecla
      Exit Function
    Case 44
      strComa = Trim(Texto)
      intPos = InStr(strComa, ",")
      If intPos <> 0 Then
        fValidarDecimales = 0
        Exit Function
      Else
        fValidarDecimales = Tecla
        Exit Function
      End If
    Case 46
      strComa = Trim(Texto)
      intPos = InStr(strComa, ",")
      If intPos <> 0 Then
        fValidarDecimales = 0
        Exit Function
      Else
        fValidarDecimales = 44
        Exit Function
      End If
    Case Else
      fValidarDecimales = 0
  End Select
End Function
Public Function fValidarNumFactura(Tecla As Integer, Texto As String) As Integer
Dim strComa As String
Dim intPos As Integer

  Select Case Tecla
    Case 48 To 57
      fValidarNumFactura = Tecla
      Exit Function
    Case 8
      fValidarNumFactura = Tecla
      Exit Function
    Case 47
      strComa = Trim(Texto)
      intPos = InStr(strComa, "/")
      If intPos <> 0 Then
        fValidarNumFactura = 0
        Exit Function
      Else
        fValidarNumFactura = Tecla
        Exit Function
      End If
    Case Else
      fValidarNumFactura = 0
  End Select
End Function

Public Function fValidarEnteros(Tecla As Integer) As Integer
Dim strComa As String
Dim intPos As Integer

  Select Case Tecla
    Case 48 To 57
      fValidarEnteros = Tecla
      Exit Function
    Case 8
      fValidarEnteros = Tecla
      Exit Function
    Case Else
      fValidarEnteros = 0
  End Select
End Function

