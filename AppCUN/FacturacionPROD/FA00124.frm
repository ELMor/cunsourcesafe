VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmSelConciertos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conciertos a los que aplicar la modificación."
   ClientHeight    =   4305
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7665
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   7665
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   555
      Left            =   6570
      TabIndex        =   1
      Top             =   90
      Width           =   1050
   End
   Begin SSDataWidgets_B.SSDBGrid grdConciertos 
      Height          =   4200
      Left            =   135
      TabIndex        =   0
      Top             =   45
      Width           =   6315
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   3
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1164
      Columns(0).Caption=   "Aplicar"
      Columns(0).Name =   "Aplicar"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   9049
      Columns(1).Caption=   "Nombre del Concierto"
      Columns(1).Name =   "Descripcion"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "CodConcierto"
      Columns(2).Name =   "CodConcierto"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   11139
      _ExtentY        =   7408
      _StockProps     =   79
      Caption         =   "Conciertos"
   End
End
Attribute VB_Name = "frmSelConciertos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cadena As String
Dim LineasGrid As Integer

Function ActualizarConciertos(Codigo As String, Fecha As String) As String
Dim MiSqL As String
Dim rsBusqueda As rdoResultset
Dim ArrayConciertos() As String
Dim PosicPunto As Integer
Dim StrCadena As String
Dim IntContArray As Integer
Dim X As Integer
Dim YaExiste As Boolean

  'Contador de las filas del array que contiene los conciertos
  IntContArray = 0
  ReDim ArrayConciertos(0 To 0)
  MiSqL = " Select FA15RUTA From FA1500 where FA15RUTA like '%/%." & Codigo & ".%'" & _
              " And (FA15FECINICIO <= TO_DATE('" & Fecha & "','DD/MM/YYYY HH24:MI:SS') " & _
              " And FA15FECFIN > TO_DATE('" & Fecha & "','DD/MM/YYYY HH24:MI:SS'))"
  Set rsBusqueda = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not rsBusqueda.EOF Then
    rsBusqueda.MoveLast
    rsBusqueda.MoveFirst
    While Not rsBusqueda.EOF
      'quitar punto buscar siguiente y coger
      StrCadena = rsBusqueda("FA15RUTA")
      StrCadena = Right(StrCadena, Len(StrCadena) - 1)
      PosicPunto = InStr(1, StrCadena, ".")
      StrCadena = Left(StrCadena, PosicPunto - 1)
      If StrCadena <> "" Then
        If IntContArray = 0 Then
          ReDim ArrayConciertos(1 To 1)
          ArrayConciertos(1) = StrCadena
          IntContArray = 1
        Else
          YaExiste = False
          For X = 1 To UBound(ArrayConciertos)
            If StrCadena = ArrayConciertos(X) Then
              YaExiste = True
              Exit For
            End If
          Next
          If YaExiste = False Then
            IntContArray = IntContArray + 1
            ReDim Preserve ArrayConciertos(1 To IntContArray)
            ArrayConciertos(IntContArray) = StrCadena
          End If
        End If
      End If
      rsBusqueda.MoveNext
    Wend
    If UBound(ArrayConciertos) > 0 Then
      For X = 1 To UBound(ArrayConciertos)
        MiSqL = "Select FA09DESIG From FA0900 Where FA09CODNODOCONC = " & ArrayConciertos(X)
        Set rsBusqueda = objApp.rdoConnect.OpenResultset(MiSqL, 3)
        If Not rsBusqueda.EOF Then
          grdConciertos.AddItem -1 & Chr(9) & rsBusqueda("FA09DESIG") & Chr(9) & ArrayConciertos(X)
        End If
      Next
      LineasGrid = UBound(ArrayConciertos)
    End If
  Else
  End If
  frmSelConciertos.Show 1
  ActualizarConciertos = Cadena
End Function


Private Sub cmdAceptar_Click()
Dim X As Integer
  For X = 1 To LineasGrid
    If grdConciertos.Columns(0).Value = -1 Then
      'añadimos el valor a la cadena que tiene los conciertos
      Cadena = Cadena & grdConciertos.Columns(2).Value & ","
    End If
  Next
  If Cadena <> "" Then
    MsgBox "concierto seleccionado"
  End If
  Unload Me
End Sub


Private Sub Form_Load()
  Cadena = ""
End Sub


