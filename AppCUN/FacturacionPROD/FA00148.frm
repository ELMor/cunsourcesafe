VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_TipoEconREco 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Petici�n de Tipo Econ�mico del Responsable"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10365
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4575
   ScaleWidth      =   10365
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBCombo sscboNodos 
      Height          =   330
      Left            =   3600
      TabIndex        =   1
      Top             =   3375
      Width           =   5295
      DataFieldList   =   "Column 1"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   9340
      Columns(0).Name =   "FA09DESIG"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "FA09CODNODOCONC"
      Columns(1).Name =   "FA09CODNODOCONC"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9349
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   510
      Left            =   9060
      TabIndex        =   2
      Top             =   3375
      Width           =   1230
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   510
      Left            =   9060
      TabIndex        =   3
      Top             =   3960
      Width           =   1230
   End
   Begin SSDataWidgets_B.SSDBGrid grdTipEcon 
      Height          =   2700
      Left            =   90
      TabIndex        =   0
      Top             =   585
      Width           =   10200
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   5
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   873
      Columns(0).Caption=   "Tipo"
      Columns(0).Name =   "CI32CODTIPECON"
      Columns(0).AllowSizing=   0   'False
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   1
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   873
      Columns(1).Caption=   "Entidad"
      Columns(1).Name =   "CI13CODENTIDAD"
      Columns(1).AllowSizing=   0   'False
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   2
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   11986
      Columns(2).Caption=   "Tipo Econ�mico"
      Columns(2).Name =   "CI13DESENTIDAD"
      Columns(2).AllowSizing=   0   'False
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "FA09CODNODOCONC"
      Columns(3).Name =   "FA09CODNODOCONC"
      Columns(3).AllowSizing=   0   'False
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   10
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3281
      Columns(4).Caption=   "Concierto"
      Columns(4).Name =   "FA09DESIG"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      TabNavigation   =   1
      _ExtentX        =   17992
      _ExtentY        =   4762
      _StockProps     =   79
   End
   Begin VB.Label Label1 
      Caption         =   "Facturar por :"
      Height          =   330
      Left            =   2520
      TabIndex        =   6
      Top             =   3465
      Width           =   1230
   End
   Begin VB.Label lblResultado 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resultado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   90
      TabIndex        =   5
      Top             =   3915
      Width           =   8790
   End
   Begin VB.Label lblREco 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Responsable Econ�mico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   330
      Left            =   90
      TabIndex        =   4
      Top             =   135
      Width           =   10185
   End
End
Attribute VB_Name = "frm_TipoEconREco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private objREco  As Persona

Public Sub pTipoEconReco(NuevoREco As Persona)
    
    Set objREco = NuevoREco
    lblREco.Caption = objREco.Name
    lblResultado.Caption = ""
    
    CargarTipECon
    
    Load Me
      'Set frmSeleccion.varFact = objFactura
    Me.Show (vbModal)
            
    'Set objFactura = varFact
    Unload Me
End Sub

Private Sub CargarTipECon()
    Dim rsTipEcon As rdoResultset
    Dim SQL As String
    
    SQL = "select CI32CODTIPECON, CI13CODENTIDAD, CI13DESENTIDAD, ci1300.FA09CODNODOCONC, FA09DESIG " & _
          "From ci1300, fa0900 " & _
          "Where ci1300.fa09codnodoconc = fa0900.fa09codnodoconc " & _
          "order by 1,2,3 "
          

    Set rsTipEcon = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
    
    Do While Not rsTipEcon.EOF
        grdTipEcon.AddItem rsTipEcon("CI32CODTIPECON") & Chr(9) & rsTipEcon("CI13CODENTIDAD") & Chr(9) & _
                              rsTipEcon("CI13DESENTIDAD") & Chr(9) & rsTipEcon("FA09CODNODOCONC") & Chr(9) & _
                              rsTipEcon("FA09DESIG")
        rsTipEcon.MoveNext
    Loop
    
    Set rsTipEcon = Nothing
End Sub

Sub CargarComboNodos()
    Dim MiSql As String
    Dim MiRs As rdoResultset
    Dim strDesig As String

    MiSql = "select fa09codnodoconc_h, fa09desig from fa1400, fa0900 " & _
            "where fa09codnodoconc_h = fa09codnodoconc and fa09codnodoconc_p = " & grdTipEcon.Columns("FA09CODNODOCONC").Text & _
            " order by fa09desig"
            
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, rdOpenForwardOnly)
    
    sscboNodos.RemoveAll
    ' coger la primera para ponerla en el combo
    If Not MiRs.EOF Then
        strDesig = MiRs("FA09DESIG")
    Else
        strDesig = ""
    End If
    
    ' cargar el combo
    While Not MiRs.EOF
        sscboNodos.AddItem MiRs("FA09DESIG") & Chr(9) & MiRs("fa09codnodoconc_h")
        MiRs.MoveNext
    Wend
    
    sscboNodos.Text = strDesig
End Sub

Private Sub cmdAceptar_Click()
    If MsgBox("� Es correcto ?", vbQuestion + vbYesNo, "Tipos Econ�micos") = vbYes Then
        objREco.CodTipEcon = "" & grdTipEcon.Columns("CI32CODTIPECON").Text
        objREco.EntidadResponsable = "" & grdTipEcon.Columns("CI13CODENTIDAD").Text
        objREco.DesEntidad = "" & grdTipEcon.Columns("CI13DESENTIDAD").Text
        objREco.Concierto.Concierto = "" & grdTipEcon.Columns("FA09CODNODOCONC").Text
        objREco.Concierto.NodoAFacturar = "" & sscboNodos.Columns("FA09CODNODOCONC").Text
        objREco.Concierto.Name = "" & grdTipEcon.Columns("FA09DESIG").Text
        
        Unload Me
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub grdTipEcon_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    CargarComboNodos
    lblResultado.Caption = "Concierto a aplicar : " & grdTipEcon.Columns("FA09DESIG").Text & _
                           "  ( " & sscboNodos.Text & " )"
    
End Sub

Private Sub sscboNodos_Click()
    lblResultado.Caption = "Concierto a aplicar : " & grdTipEcon.Columns("FA09DESIG").Text & _
                           "  ( " & sscboNodos.Text & " )"
End Sub
