VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmNuevoResponsable 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Busqueda de personas"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7710
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   7710
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Default         =   -1  'True
      Height          =   375
      Left            =   6300
      TabIndex        =   7
      Top             =   1485
      Width           =   1320
   End
   Begin VB.Frame Frame3 
      Caption         =   "Persona"
      Height          =   1185
      Left            =   6300
      TabIndex        =   15
      Top             =   45
      Width           =   1320
      Begin VB.OptionButton optTipPersona 
         Caption         =   "Jur�dica"
         Height          =   285
         Index           =   1
         Left            =   180
         TabIndex        =   6
         Top             =   675
         Width           =   915
      End
      Begin VB.OptionButton optTipPersona 
         Caption         =   "F�sica"
         Height          =   330
         Index           =   0
         Left            =   180
         TabIndex        =   5
         Top             =   270
         Value           =   -1  'True
         Width           =   1050
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Resultados de la b�squeda"
      Height          =   3390
      Left            =   90
      TabIndex        =   10
      Top             =   1890
      Width           =   7530
      Begin SSDataWidgets_B.SSDBGrid grdPersonas 
         Height          =   3075
         Left            =   90
         TabIndex        =   8
         Top             =   225
         Width           =   7350
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         GroupHeadLines  =   0
         Col.Count       =   3
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2037
         Columns(0).Caption=   "N� Historia"
         Columns(0).Name =   "Historia"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8705
         Columns(1).Caption=   "Nombre y Apellidos"
         Columns(1).Name =   "Paciente"
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1773
         Columns(2).Caption=   "CodPersona"
         Columns(2).Name =   "CodPersona"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   12965
         _ExtentY        =   5424
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de selecci�n"
      Height          =   1815
      Left            =   90
      TabIndex        =   9
      Top             =   45
      Width           =   6090
      Begin VB.TextBox txtNumHistoria 
         Height          =   285
         Left            =   4320
         TabIndex        =   1
         Top             =   270
         Width           =   1635
      End
      Begin VB.TextBox txtApellido2 
         Height          =   285
         Left            =   1485
         TabIndex        =   4
         Top             =   1350
         Width           =   4470
      End
      Begin VB.TextBox txtApellido1 
         Height          =   285
         Left            =   1485
         TabIndex        =   3
         Top             =   990
         Width           =   4470
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   1485
         TabIndex        =   2
         Top             =   630
         Width           =   4470
      End
      Begin VB.TextBox txtCodPersona 
         Height          =   285
         Left            =   1485
         TabIndex        =   0
         Top             =   270
         Width           =   1635
      End
      Begin VB.Label lblNumHistoria 
         Alignment       =   1  'Right Justify
         Caption         =   "N� Historia:"
         Height          =   240
         Left            =   3105
         TabIndex        =   16
         Top             =   315
         Width           =   1095
      End
      Begin VB.Label lblApellido2 
         Alignment       =   1  'Right Justify
         Caption         =   "2� Apellido:"
         Height          =   240
         Left            =   270
         TabIndex        =   14
         Top             =   1395
         Width           =   1095
      End
      Begin VB.Label lblApellido1 
         Alignment       =   1  'Right Justify
         Caption         =   "1er. Apellido:"
         Height          =   240
         Left            =   270
         TabIndex        =   13
         Top             =   1035
         Width           =   1095
      End
      Begin VB.Label lblNombre 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   240
         Left            =   270
         TabIndex        =   12
         Top             =   675
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cod. Pers.:"
         Height          =   240
         Left            =   270
         TabIndex        =   11
         Top             =   315
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmNuevoResponsable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Responsable As String
Private Sub cmdBuscar_Click()
   If optTipPersona(0).Value = True Then
      ' persona fisica
      BuscarPersFisic
   Else
      ' persona juridica
      BuscarPersJurid
   End If
End Sub

Private Sub BuscarPersFisic()

   Dim rsNombre As rdoResultset
   Dim MiSelect As String
   Dim MiWhere As String
   Dim MiSql As String
   Dim Linea As String

   If Trim(txtCodPersona) = "" And Trim(txtNumHistoria) = "" And _
      Trim(txtNombre) = "" And Trim(txtApellido1) = "" And Trim(txtApellido2) = "" Then
      
      MsgBox "Debe introducir alg�n dato para realizar la selecci�n", vbInformation + vbOKOnly, "Aviso"
      Exit Sub
   End If
   
   ' Construir la SQL de busqueda de personas fisicas
'   MiSelect = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22FECNACIM, CI22NUMDIRPRINC, " & _
              "CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID, CI22NUMHISTORIA" & _
              " from CI2200, CI1000"
   MiSelect = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22NUMHISTORIA" & _
              " from CI2200"
   ' Generar el filtro dependiendo de los datos tecleados en los controles
   MiWhere = " WHERE"
   
   If Trim(txtCodPersona) <> "" Then
      If IsNumeric(txtCodPersona) Then
         MiWhere = MiWhere & " CI2200.CI21CODPERSONA =" & txtCodPersona & " AND"
      Else
         Call MsgBox("El C�digo de persona debe ser num�rico", vbInformation + vbOKOnly, "Error")
         txtCodPersona.SetFocus
         Exit Sub
      End If
   End If
   
   If Trim(txtNumHistoria) <> "" Then
      If IsNumeric(txtNumHistoria) Then
         MiWhere = MiWhere & " CI2200.CI22NUMHISTORIA =" & txtNumHistoria & " AND"
      Else
         Call MsgBox("El N�mero de historia debe ser num�rico", vbInformation + vbOKOnly, "Error")
         txtNumHistoria.SetFocus
         Exit Sub
      End If
   End If
   If Trim(txtNombre) <> "" Then
      MiWhere = MiWhere & " CI2200.CI22NOMBREALF LIKE '%" & UCase(Trim(txtNombre)) & "%' AND"
   End If
   If Trim(txtApellido1) <> "" Then
      MiWhere = MiWhere & " CI2200.CI22PRIAPELALF LIKE '%" & UCase(Trim(txtApellido1)) & "%' AND"
   End If
   If Trim(txtApellido2) <> "" Then
      MiWhere = MiWhere & " CI2200.CI22SEGAPELALF LIKE '%" & UCase(Trim(txtApellido2)) & "%' AND"
   End If
'   MiWhere = MiWhere & " ((CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA(+)) " & _
                       " AND (CI1000.CI10INDDIRPRINC = -1 OR CI1000.CI10INDDIRPRINC IS NULL))"
   
   If Right(MiWhere, 3) = "AND" Then
      MiWhere = Left(MiWhere, Len(MiWhere) - 4)
   End If
  
   MiSql = MiSelect & MiWhere
   
   Screen.MousePointer = vbHourglass
   
   Set rsNombre = objApp.rdoConnect.OpenResultset(MiSql, 3)
   
   Me.grdPersonas.RemoveAll
   If Not rsNombre.EOF Then
      rsNombre.MoveFirst
      While Not rsNombre.EOF
         Linea = rsNombre("CI22NUMHISTORIA") & Chr(9) & rsNombre("CI22NOMBRE") & _
               " " & rsNombre("CI22PRIAPEL") & " " & rsNombre("CI22SEGAPEL") & _
               Chr(9) & rsNombre("CI21CODPERSONA")
         Me.grdPersonas.AddItem Linea
         rsNombre.MoveNext
      Wend
   Else
      MsgBox "No se han encontrado resultados para la b�squeda solicitada", vbExclamation + vbOKOnly, "Atenci�n"
   End If

   Screen.MousePointer = vbDefault
   
End Sub

Private Sub BuscarPersJurid()

   Dim rsNombre As rdoResultset
   Dim MiSelect As String
   Dim MiWhere As String
   Dim MiSql As String
   Dim Linea As String
   
   ' Aqui solo se filtra por CI21CODPERSONA y CI23RAZONSOCIAL
   If Trim(txtCodPersona) = "" And Trim(txtNombre) = "" Then
      MsgBox "Debe introducir alg�n dato para realizar la selecci�n", vbInformation + vbOKOnly, "Aviso"
      Exit Sub
   End If
   
   ' Construir la SQL de busqueda de personas JURIDICAS
   MiSelect = "Select CI23RAZONSOCIAL, CI21CODPERSONA from CI2300"
              
   ' Generar el filtro dependiendo de los datos tecleados en los controles
   MiWhere = " WHERE"
   
   If Trim(txtCodPersona) <> "" Then
      If IsNumeric(txtCodPersona) Then
         MiWhere = MiWhere & " CI21CODPERSONA =" & txtCodPersona & " AND"
      Else
         Call MsgBox("El C�digo de persona debe ser num�rico", vbInformation + vbOKOnly, "Error")
         txtCodPersona.SetFocus
         Exit Sub
      End If
   End If
'   If Trim(txtNumHistoria) <> "" Then
'      MiWhere = MiWhere & " CI2200.CI22NUMHISTORIA =" & txtNumHistoria & " AND"
'   End If
   If Trim(txtNombre) <> "" Then
      MiWhere = MiWhere & " UPPER(CI23RAZONSOCIAL) LIKE '%" & UCase(Trim(txtNombre)) & "%' AND"
   End If
   
   If Right(MiWhere, 3) = "AND" Then
      MiWhere = Left(MiWhere, Len(MiWhere) - 4)
   End If
   
   MiSql = MiSelect & MiWhere
   
   Screen.MousePointer = vbHourglass
   
   Set rsNombre = objApp.rdoConnect.OpenResultset(MiSql, 3)
   
   Me.grdPersonas.RemoveAll
   If Not rsNombre.EOF Then
      rsNombre.MoveFirst
      While Not rsNombre.EOF
         Linea = "" & Chr(9) & rsNombre("CI23RAZONSOCIAL") & _
               Chr(9) & rsNombre("CI21CODPERSONA")
         Me.grdPersonas.AddItem Linea
         rsNombre.MoveNext
      Wend
   Else
      MsgBox "No se han encontrado resultados para la b�squeda solicitada", vbExclamation + vbOKOnly, "Atenci�n"
   End If
   
   Screen.MousePointer = vbDefault
End Sub

Public Function pNuevoResponsable() As String
   Load frmNuevoResponsable
   
   frmNuevoResponsable.Show (vbModal)
   pNuevoResponsable = Responsable
   Unload frmNuevoResponsable
End Function

Private Sub grdPersonas_DblClick()
  'Asingamos a responsable todos los valores necesarios del nuevo responsable.
  Responsable = "" & grdPersonas.Columns(2).Value
  Unload Me
End Sub

Private Sub optTipPersona_Click(Index As Integer)
   Select Case Index
      Case 0   ' Persona fisica
         ' configurar los controles para pedir los datos de una persona fisica
         lblNumHistoria.Visible = True
         txtNumHistoria.Visible = True
         
         lblNombre.Caption = "Nombre"
         
         lblApellido1.Visible = True
         txtApellido1.Visible = True
         
         lblApellido2.Visible = True
         txtApellido2.Visible = True
         
      Case 1   ' Persona juridica
         ' configurar los controles para pedir los datos de una persona juridica
         lblNumHistoria.Visible = False
         txtNumHistoria.Visible = False
         txtNumHistoria.Text = ""
         
         lblNombre.Caption = "Raz�n Social"
         
         lblApellido1.Visible = False
         txtApellido1.Visible = False
         txtApellido1.Text = ""
         
         lblApellido2.Visible = False
         txtApellido2.Visible = False
         txtApellido2.Text = ""
      
   End Select
End Sub

Private Sub txtCodPersona_GotFocus()
    txtCodPersona.SelStart = 0
    txtCodPersona.SelLength = Len(txtCodPersona)
End Sub

Private Sub txtNumHistoria_GotFocus()
    txtNumHistoria.SelStart = 0
    txtNumHistoria.SelLength = Len(txtNumHistoria)
End Sub

Private Sub txtNombre_GotFocus()
    txtNombre.SelStart = 0
    txtNombre.SelLength = Len(txtNombre)
End Sub

Private Sub txtApellido1_GotFocus()
    txtApellido1.SelStart = 0
    txtApellido1.SelLength = Len(txtApellido1)
End Sub

Private Sub txtApellido2_GotFocus()
    txtApellido2.SelStart = 0
    txtApellido2.SelLength = Len(txtApellido2)
End Sub

