VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Begin VB.Form frm_A�adirOtrasFact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "A�adir facturas de otro paciente"
   ClientHeight    =   8100
   ClientLeft      =   45
   ClientTop       =   480
   ClientWidth     =   9585
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8100
   ScaleWidth      =   9585
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   420
      Left            =   8370
      TabIndex        =   27
      Top             =   7560
      Width           =   1140
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Continuar"
      Height          =   420
      Left            =   8370
      TabIndex        =   26
      Top             =   6975
      Width           =   1140
   End
   Begin VB.CommandButton cmdMarcar 
      Caption         =   "&Marcar"
      Height          =   375
      Left            =   8370
      TabIndex        =   25
      Top             =   2970
      Width           =   1140
   End
   Begin ComctlLib.ListView lvFact 
      Height          =   5100
      Left            =   45
      TabIndex        =   2
      Top             =   2925
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   8996
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      Icons           =   "ImageList"
      SmallIcons      =   "ImageList"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Facturas"
         Object.Width           =   13406
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Nuevo Paciente"
      Height          =   780
      Left            =   45
      TabIndex        =   1
      Top             =   2025
      Width           =   9465
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   420
         Left            =   8325
         TabIndex        =   24
         Top             =   225
         Width           =   1005
      End
      Begin VB.TextBox txtCodPersonaNuevo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   270
         Width           =   1140
      End
      Begin VB.TextBox txtNomPacNuevo 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2835
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   270
         Width           =   4875
      End
      Begin VB.Label Label1 
         Caption         =   "Paciente"
         Height          =   285
         Index           =   4
         Left            =   225
         TabIndex        =   23
         Top             =   315
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Factura a la que se a�adiran las elegidas"
      Height          =   1860
      Left            =   45
      TabIndex        =   0
      Top             =   90
      Width           =   9465
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   420
         Left            =   8325
         TabIndex        =   28
         Top             =   225
         Width           =   1005
      End
      Begin VB.TextBox txtImporte 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   3600
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   360
         Width           =   1545
      End
      Begin VB.TextBox txtNomPaciente 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   720
         Width           =   4875
      End
      Begin VB.TextBox txtNomReco 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   1080
         Width           =   4875
      End
      Begin VB.TextBox txtTecoEnti 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   1890
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1440
         Width           =   870
      End
      Begin VB.TextBox txtFecha 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   6525
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   360
         Width           =   1200
      End
      Begin VB.TextBox txtCodPersonaPac 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   720
         Width           =   1140
      End
      Begin VB.TextBox txtFecHasta 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   6525
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   1440
         Width           =   1200
      End
      Begin VB.TextBox txtFecDesde 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   3915
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1440
         Width           =   1200
      End
      Begin VB.TextBox txtNumFact 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   1260
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   1500
      End
      Begin VB.TextBox txtCodPersonaREco 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   1080
         Width           =   1140
      End
      Begin VB.Label Label1 
         Caption         =   "Importe"
         Height          =   285
         Index           =   3
         Left            =   2880
         TabIndex        =   20
         Top             =   405
         Width           =   1140
      End
      Begin VB.Label Label1 
         Caption         =   "Num. Factura"
         Height          =   285
         Index           =   12
         Left            =   225
         TabIndex        =   16
         Top             =   405
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Paciente"
         Height          =   285
         Index           =   10
         Left            =   225
         TabIndex        =   15
         Top             =   765
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Eco."
         Height          =   285
         Index           =   9
         Left            =   225
         TabIndex        =   14
         Top             =   1125
         Width           =   2040
      End
      Begin VB.Label Label1 
         Caption         =   "Fec. Desde"
         Height          =   285
         Index           =   6
         Left            =   2880
         TabIndex        =   13
         Top             =   1530
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Fec. Hasta"
         Height          =   285
         Index           =   2
         Left            =   5490
         TabIndex        =   12
         Top             =   1530
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo Eco. / Entidad"
         Height          =   285
         Index           =   1
         Left            =   225
         TabIndex        =   11
         Top             =   1485
         Width           =   1410
      End
      Begin VB.Label Label1 
         Caption         =   "Fec. Factura"
         Height          =   285
         Index           =   0
         Left            =   5490
         TabIndex        =   10
         Top             =   405
         Width           =   1140
      End
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   20
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":0000
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":031A
            Key             =   "AsistSoloFarmacia"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":0634
            Key             =   "RespMedico"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":094E
            Key             =   "AsistVacia"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":0C68
            Key             =   "AsistCerrada"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":0F82
            Key             =   "AsistAbierta"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":129C
            Key             =   "Asistencia"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":15B6
            Key             =   "TipoAsist"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":18D0
            Key             =   "RespEconomico"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":1BEA
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":1F04
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":221E
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":2538
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":2852
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":2B6C
            Key             =   "Factura"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":2E86
            Key             =   "FacturaGarantia"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":31A0
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":34BA
            Key             =   "Abonable"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":37D4
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00168.frx":3AEE
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frm_A�adirOtrasFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim qryFact(1 To 5) As New rdoQuery
Dim NumFact As String
Dim NumHistoria As String

Sub IniciarQRY()
   Dim MiSql As String
   Dim X As Integer
   
   ' seleccion de los datos de la factura a la que se va a a�adir el resto
   MiSql = "SELECT FA04NUMFACT, CI21CODPERSONA, FA04CANTFACT, FA04FECFACTURA,  " & _
           "       FA0400.CI32CODTIPECON, fa0400.CI13CODENTIDAD, ci13desentidad,  " & _
           "       FA04FECDESDE, FA04FECHASTA, FA04INDCONTABIL, FA04INDCOMPENSA " & _
           "From fa0400, ci1300 " & _
           "WHERE fa04numfact = ? " & _
           "  AND fa0400.CI32CODTIPECON = CI1300.CI32CODTIPECON " & _
           "  AND fa0400.ci13codentidad = CI1300.CI13CODENTIDAD " & _
           "  AND FA0400.FA04NUMFACREAL IS NULL " & _
           "  AND fa0400.fa04cantfact IS NOT NULL " & _
           "  AND fa0400.fa04cantfact <> 0"

   qryFact(1).SQL = MiSql
   
   
   ' seleccion de las facturas disponibles para el paciente y susceptibles de ser a�adidas.
   MiSql = "select DISTINCT AD0700.AD07CODPROCESO, FA0400.AD01CODASISTENCI, " & _
         "       FA04NUMFACT, FA04FECFACTURA, FA04FECDESDE, FA04FECHASTA, " & _
         "       FA04DESCRIP, FA04INDNMGC, FA04INDCONTABIL, FA04INDCOMPENSA, " & _
         "       CI32CODTIPECON, CI13CODENTIDAD, FA0400.CI21CODPERSONA as CI21CODPERSONA, " & _
         "       FA04CODFACT_GARAN, FA04FECFINGARANTIA " & _
         " From AD0700, FA0400  " & _
         " where AD0700.ci21codpersona = ? " & _
         "   AND AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') " & _
         "   AND AD0700.AD07CODPROCESO = FA0400.AD07CODPROCESO  " & _
         "   AND FA04NUMFACREAL IS NULL " & _
         "   AND FA04INDNMGC IS NULL " & _
         " order by FA04FECFACTURA desc, FA04NUMFACT DESC "
   qryFact(2).SQL = MiSql
   
   ' Recoger el importe total de una factura.
   MiSql = "Select SUM(FA04CANTFACT) AS FA04CANTFACT from FA0400 " & _
           "Where FA04NUMFACT = ? " & _
           "  AND FA04NUMFACREAL IS NULL " & _
           "  AND EXISTS (SELECT * FROM ad0700 " & _
           "              Where ad0700.CI22NUMHISTORIA = ? " & _
           "                AND ad0700.ad07codproceso = FA0400.AD07CODPROCESO)"
   qryFact(3).SQL = MiSql


   'Asignar el numero de factura definitivo a la factura provisional
   MiSql = "Update FA0400 set FA04NUMFACT = ? " & _
           "Where FA04NUMFACT = ? " & _
           "  AND FA04NUMFACREAL IS NULL "
   qryFact(4).SQL = MiSql


   For X = 1 To UBound(qryFact)
      Set qryFact(X).ActiveConnection = objApp.rdoConnect
      qryFact(X).Prepared = True
   Next

End Sub

Public Sub A�adirFact(nFact As String, CI21CODPERSONA As String)

   On Error GoTo error
   Load Me
   
   IniciarQRY
   
   ' cargar datos
   If Not CargarDatos(nFact, CI21CODPERSONA) Then
      ' error al cargar los datos del paciente o factura
      Exit Sub
      
   End If
   
   
   
   ' mostrar el formulario
   Me.Show vbModal
   
   
Exit Sub
error:
'   Resume

End Sub

Private Function CargarDatos(nFact As String, CI21CODPERSONA As String) As Boolean
   Dim rsFact As rdoResultset
   Dim RsImporte As rdoResultset
   Dim objPersona As New Persona
   
   ' CARGAR LOS DATOS DEL PACIENTE
   txtCodPersonaPac.Text = CI21CODPERSONA
   objPersona.Codigo = txtCodPersonaPac.Text
   txtCodPersonaPac.Tag = objPersona.NumHistoria
   txtNomPaciente.Text = objPersona.Name
   
   ' CARGAR LOS DATOS DE LA FACTURA
   txtNumFact.Text = nFact
   
   qryFact(1).rdoParameters(0) = nFact
   Set rsFact = qryFact(1).OpenResultset(rdOpenStatic)
   If Not rsFact.EOF Then
      If Not IsNull(rsFact("FA04INDCONTABIL")) Then
         ' la factura esta contabilizada no se puede modificar
         MsgBox "La factura " & nFact & " est� contabilizada," & Chr(10) & _
                "         NO se puede modificar.", vbInformation + vbOKOnly, "A�adir otra factura"
         CargarDatos = False
         
      ElseIf rsFact("FA04INDCOMPENSA") <> 0 Then
         ' la factura esta compensada no se puede modificar
         MsgBox "La factura " & nFact & " est� compensada," & Chr(10) & _
                "          NO se puede modificar.", vbInformation + vbOKOnly, "A�adir otra factura"
         CargarDatos = False
         
      Else
         ' es correcto
         
         'hay que recoger el importe, porque puede ser una agrupacion de varias asistencias
         qryFact(3).rdoParameters(0) = "" & rsFact("FA04NUMFACT")
         qryFact(3).rdoParameters(1) = txtCodPersonaPac.Tag
         Set RsImporte = qryFact(3).OpenResultset(rdOpenKeyset)
         If Not RsImporte.EOF Then
            txtImporte.Text = "" & Format(RsImporte("FA04CANTFACT"), "###,###,##0")
         Else
            txtImporte.Text = "0"
         End If

          
         txtFecha.Text = "" & rsFact("FA04FECFACTURA").Value
         
         txtCodPersonaREco.Text = "" & rsFact("CI21CODPERSONA").Value
         objPersona.Codigo = txtCodPersonaREco.Text
         txtNomReco.Text = objPersona.Name
         
         txtTecoEnti.Text = "" & rsFact("CI32CODTIPECON").Value & "-" & rsFact("CI13CODENTIDAD").Value
         
         txtFecDesde.Text = "" & rsFact("FA04FECDESDE").Value
         txtFecHasta.Text = "" & rsFact("FA04FECHASTA").Value
         CargarDatos = True
         
      End If
   
   Else
      ' no se ha encontrado la factura ����� ERROR ?????
      CargarDatos = False

   End If

Exit Function
error:
'   Resume
   
End Function

Private Sub cmdBuscar_Click()
   Dim CodPersona As String
   Dim objREco As Persona
   Dim rsFact As rdoResultset
   Dim RsImporte As rdoResultset
   Dim Fact As New ListItem
   Dim StrCadena As String
   Dim Texto As String
   
   'Llamamos a la pantalla de selecci�n de un nuevo responsable econ�mico.
   CodPersona = frmNuevoResponsable.pNuevoResponsable
   
   If CodPersona <> "" Then
      Set objREco = New Persona
      objREco.Codigo = CodPersona
      txtCodPersonaNuevo.Text = CodPersona
      txtCodPersonaNuevo.Tag = objREco.NumHistoria
      txtNomPacNuevo.Text = objREco.Name
      
      ' cargar sus facturas
      
      '*************************************************
      '   FACTURAS
      '*************************************************
      lvFact.ListItems.Clear
       
      qryFact(2).rdoParameters(0) = CodPersona
      Set rsFact = qryFact(2).OpenResultset(rdOpenStatic)
      Do While Not rsFact.EOF
         If txtNumFact.Text <> rsFact("FA04NUMFACT") Then
         
   '        'hay que recoger el importe, porque puede ser una agrupacion de varias asistencias
            qryFact(3).rdoParameters(0) = "" & rsFact("FA04NUMFACT")
            qryFact(3).rdoParameters(1) = txtCodPersonaNuevo.Tag
            Set RsImporte = qryFact(3).OpenResultset(rdOpenKeyset)
            If Not RsImporte.EOF Then
               StrCadena = Format(RsImporte("FA04CANTFACT"), "###,###,##0") & " Pts"
            Else
               StrCadena = "0 Pts"
            End If
   
            ' a�adir las facturas de la asistencia
            Texto = rsFact("FA04NUMFACT") & _
                    "   ( " & Format(StrCadena, "###,###,##0") & " )" & _
                    "   del  " & Format(rsFact("FA04FECFACTURA"), "dd/mm/yyyy") & _
                    "   entre   (" & Format(rsFact("FA04FECDESDE"), "dd/mm/yyyy") & _
                    " - " & Format(rsFact("FA04FECHASTA"), "dd/mm/yyyy") & ")" & _
                    "    (" & rsFact("CI32CODTIPECON") & rsFact("CI13CODENTIDAD") & ")"
                    
   
            Set Fact = lvFact.ListItems.Add(, , Texto, "Factura", "Factura")
            Fact.Tag = rsFact("FA04NUMFACT")
            
            If rsFact("FA04CODFACT_GARAN") <> "" Or rsFact("FA04FECFINGARANTIA") <> "" Then
               Fact.SmallIcon = "FacturaGarantia"
               
            ElseIf Not IsNull(rsFact("FA04INDCONTABIL")) Or rsFact("FA04INDCOMPENSA") <> 0 Then
               Fact.SmallIcon = "NodoCerrado"
               
            ElseIf txtTecoEnti.Text <> "" & rsFact("CI32CODTIPECON").Value & "-" & rsFact("CI13CODENTIDAD").Value _
                   Or txtCodPersonaREco.Text <> rsFact("CI21CODPERSONA").Value Then
               Fact.SmallIcon = "AsistCerrada"
               
            End If
         
         End If
         rsFact.MoveNext
       
      Loop
      
    End If
End Sub

Private Sub cmdImprimir_Click()
   Dim anumfact(1) As String
   anumfact(1) = txtNumFact.Text
   Call ImprimirFact(anumfact)
End Sub

Private Sub cmdMarcar_Click()
   On Error GoTo error
   
   If lvFact.SelectedItem.SmallIcon = "Factura" Then
      lvFact.SelectedItem.SmallIcon = "Facturable"
      cmdMarcar.Caption = "Des&marcar"
'      blnMarcado = False
      
   ElseIf lvFact.SelectedItem.SmallIcon = "Facturable" Then
      lvFact.SelectedItem.SmallIcon = "Factura"
      cmdMarcar.Caption = "&Marcar"
'      blnMarcado = False
      
   ElseIf lvFact.SelectedItem.SmallIcon = "AsistCerrada" Then
      MsgBox "La factura no es del mismo Tipo Economico o Responsable", vbOKOnly + vbExclamation, _
             "A�adir otra factura"
      
   ElseIf lvFact.SelectedItem.SmallIcon = "AsistAbierta" Then
      MsgBox "Esta factura ya ha sido a�adida", vbOKOnly + vbExclamation, _
             "A�adir otra factura"
      
   Else
      MsgBox "No se pueden modificar facturas compensadas o contabilizadas", vbOKOnly + vbExclamation, _
             "A�adir otra factura"
   End If
   
Exit Sub
error:
' MsgBox "error"

End Sub

Private Sub lvFact_DblClick()
   Dim anumfact(1) As String
   anumfact(1) = lvFact.SelectedItem.Tag
   Call ImprimirFact(anumfact)

End Sub

Private Sub lvFact_ItemClick(ByVal item As ComctlLib.ListItem)

   On Error GoTo error
   
   If item.SmallIcon = "Factura" Then
      cmdMarcar.Caption = "&Marcar"
      
   ElseIf item.SmallIcon = "Facturable" Then
      cmdMarcar.Caption = "Des&marcar"
      
   End If
   
Exit Sub
error:
' MsgBox "error"

End Sub

Private Sub CambiarNumero(nFactAnt As String, nFacNuevo As String)
   ' Asignar el numero de factura real definitivo a la factura anulada
   qryFact(4).rdoParameters(0) = nFacNuevo
   qryFact(4).rdoParameters(1) = nFactAnt
   qryFact(4).Execute
   
End Sub

Private Sub cmdContinuar_Click()
   Dim item As ListItem
   
   For Each item In lvFact.ListItems
      If item.SmallIcon = "Facturable" Then
         CambiarNumero item.Tag, txtNumFact.Text
         item.SmallIcon = "AsistAbierta"
      End If
   Next item
End Sub

Private Sub cmdSalir_Click()
   Unload Me
End Sub

