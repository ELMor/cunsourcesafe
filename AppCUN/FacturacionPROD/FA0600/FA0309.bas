Attribute VB_Name = "modFactInsalud"
Option Explicit

Global Const constRMN = "Resonancia Magn�tica Nuclear"
Global Const constRMNSimple = 13088
Global Const constRMNDoble = 13089
Global Const constRMNTriple = 13090
Global Const constResonancias = "(" & constRMNSimple & "," & constRMNDoble & "," & constRMNTriple & ")"
Global Const constRadioterapia = 391
Global Const constRadioterapia1 = 81
Global Const constRadiocirugia = 13267
Global Const constNeurofisiologia = 78
Global Const constPET = 99

Global Const strDescripProtesis = "PROTESIS"
Global Const strDescripMedicacion = "CITOSTATICOS Y FACTORES"

Global Const constTipoFactIndividual = 1
Global Const constTipoFactOsasunbidea = 2
Global Const constTipoFactJASS = 3
Global Const constTipoFactAgrupada = 4
Dim qryTipoAsist As rdoQuery
Dim qryInterv As rdoQuery
Dim qryCantidad As rdoQuery
Dim qryFechas As rdoQuery
Dim qryFechaFact As rdoQuery
Dim qryFactPET As rdoQuery
Dim qryFactRMN As rdoQuery
Dim qryFactNeurofisio As rdoQuery
Dim qryFactRadiocirugia As rdoQuery
Dim qryFactRevisiones As rdoQuery
Dim qryFecConsulta As rdoQuery
Dim qryFechaFact_Consulta As rdoQuery

Dim proProgreso As ProgressBar
Dim fraProgreso As Frame

Public Enum intNodo
    intNodoActoMedico = 454
    intNodoEstancias = 393
    intNodoForfAmbu = 389
    intNodoForfCataratas = 387
    intNodoForfInterv = 385
    intNodoForfPruebas = 388
End Enum

Public Enum intTipoFact
    intFactEstancias = 1
    intFactForfait = 2
    intFactAmbulatorio = 3
    intFactPET = 4
    intFactRMN = 5
    intFactRadiocirugia = 6
    intFactNeurofisiologia = 7
    intFactOtros = 8
End Enum

Public Enum intTipoAsist
    intTipoHospitalizado = 1
    intTipoAmbulatorio = 2
End Enum

Type typeFacturasLineas
    intNLinea As Integer
    strDescrip As String
    strDescripINSALUD As String
    lngCodNodo As Long
    dblImporte As Double
    blnPrecioDia As Boolean
    blnRadioterapia As Boolean
    blnPrincipal As Boolean
    blnProtesis As Boolean
    blnMedicacion As Boolean
    dblCantidad As Double
    strDiagINSALUD As String
    strProcINSALUD As String
End Type

Type typeFacturas
    lngCodFact As Long
    strNumFact As String
    dblImporte As Double
    lngNumHistoria As Long
    strNumSS As String
    strPaciente As String
    strFIngreso As String
    strFAlta As String
    strDiag As String
    lngNodoFact As Long
    strDptoResp As String
    intTipoAsistencia As intTipoAsist
    strFechaDesde As String
    strFechaHasta As String
    strFecFactura As String
    arLineas() As typeFacturasLineas
End Type

Type typeLineasFactEntidad
    strDescrip As String
    dblCantidad As Double
    dblCantidadPrimera As Double
    dblCantidadSucesiva As Double
    strFechas As String
    dblPrecio As Double
    dblPrecioPrimera As Double
    dblPrecioSucesiva As Double
    dblImporte As Double
    dblImporteSuplementos As Double
    blnPrincipal As Boolean
    blnPrecioDia As Boolean
    blnRadioterapia As Boolean
    blnMostrar As Boolean
End Type

Type typeFacturasEntidadPac
    lngCodFact As Long
    strNumSS As String
    lngNumHistoria As Long
    strPaciente As String
    strFechaIngreso As String
    strFechaIntervencion As String
    strFechaAlta As String
    strDpto As String
    strDiagnostico As String
    arFacturasEntidad_Lineas() As typeLineasFactEntidad
End Type

Type typeFacturasEntidadResumen
    dblCantidad As Double
    strTexto As String
    strTexto2 As String
    dblPrecio As Double
    dblImporte As Double
End Type

Type typeFacturasEntidad
    intTipo As intTipoFact
    strNumFact As String
    strFecha As String
    arResumen() As typeFacturasEntidadResumen
    arPacientes() As typeFacturasEntidadPac
    strDescrip As String
End Type

Type typeEntidad
    strTipoEcon As String
    strEntidad As String
    strDesig As String
    strDireccion As String
    lngCodReco As Long
    arFacturasEntidad() As typeFacturasEntidad
    arFacturas() As typeFacturas
End Type


Type typeResumenDatos
    dblCantidad As Double
    strDescrip1 As String
    strDescrip2 As String
    dblPrecio As Double
    dblImporte As Double
End Type
Type typeResumen
    intTipoResumen As intTipoAsist
    arDatos() As typeResumenDatos
End Type
Public Function fCargaFacturasINSALUD(strFecha$, _
                                      arEntidad() As typeEntidad, _
                                      proProgreso1 As ProgressBar, _
                                      fraProgreso1 As Frame, _
                                      arResumen() As typeResumen) As Boolean
    ' Carga los valores de las facturas en la estructura
    Dim intNumEnti As Integer
    Dim i As Integer
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim strEnti As String
    Dim blnError As Boolean
    Dim intNumUltimaEntidad As Integer
    Dim strUltimoTipoEcon As String
    
    On Error Resume Next
    
    Set proProgreso = proProgreso1
    Set fraProgreso = fraProgreso1
    
    intNumEnti = UBound(arEntidad)
    
    If intNumEnti > 0 Then
        For i = 1 To intNumEnti
            strEnti = strEnti & "('" & arEntidad(i).strTipoEcon & "'" _
                              & ",'" & arEntidad(i).strEntidad & "'" _
                              & "),"
        Next i
        If Right(strEnti, 1) = "," Then
            strEnti = Left(strEnti, Len(strEnti) - 1)
        End If
        
        SQL = "SELECT   FA0400.FA04CODFACT," _
                & "     FA0400.FA04NUMFACT," _
                & "     FA0400.FA04CANTFACT," _
                & "     CI2200.CI22NUMHISTORIA," _
                & "     CI2200.CI22NUMSEGSOC," _
                & "     CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE AS PACIENTE," _
                & "     AD0100.AD01FECINICIO," _
                & "     AD0100.AD01FECFIN, " _
                & "     AD0100.AD01DIAGNOSSALI," _
                & "     FA0400.FA09CODNODOCONC_FACT," _
                & "     FA1600.FA16NUMLINEA, " _
                & "     LTRIM(RTRIM(RPAD(FA1600.FA16DESCRIP,255))) AS FA16DESCRIP," _
                & "     FA1600.FA14CODNODOCONC," _
                & "     FA1600.FA16IMPORTE," _
                & "     NVL(FA1500.FA15PRECIOREF,0) AS FA15PRECIOREF," _
                & "     NVL(FA1500.FA15PRECIODIA,0) AS FA15PRECIODIA," _
                & "     NVL(FA1500.FA15INDSUFICIENTE,0) AS FA15INDSUFICIENTE," _
                & "     NVL(FA1500.FA15INDFACTOBLIG,0) AS FA15INDFACTOBLIG," _
                & "     AD0200.AD02DESDPTO, "
        SQL = SQL & "   FA0400.CI32CODTIPECON, " _
                & "     FA0400.CI13CODENTIDAD, " _
                & "     FA0400.FA04FECDESDE, " _
                & "     LAST_DAY(FA0400.FA04FECHASTA) AS FA04FECHASTA, " _
                & "     LAST_DAY(FA0400.FA04FECFACTURA) AS FA04FECFACTURA, " _
                & "     LTRIM(RTRIM(FA4600.FA46CODDIAG||' '||FA4600.FA46DESCRIP||' '||FA4600.FA46CODPROC||' '||FA4600.FA46DESPROC)) AS DESCRIPINSALUD, " _
                & "     LTRIM(RTRIM(FA4600.FA46CODDIAG||' '||FA4600.FA46DESCRIP)) AS DIAGINSALUD, " _
                & "     LTRIM(RTRIM(FA4600.FA46CODPROC||' '||FA4600.FA46DESPROC)) AS PROCINSALUD, " _
                & "     DECODE(FA0503J.FR73CODPRODUCTO,NULL,0,-1) AS INDPROTESIS, " _
                & "     DECODE(FA0500.FA08CODGRUPO,1, -1,0) AS INDFARMACIA "
        SQL = SQL & " FROM  FA0400, " _
                  & "       FA1600, " _
                  & "       FA1500, " _
                  & "       FA4600, " _
                  & "       AD0100, " _
                  & "       CI2200, " _
                  & "       AD0500, " _
                  & "       AD0200, " _
                  & "       FA0500, " _
                  & "       FA0503J, " _
                  & "       AD1100 "
                  
        SQL = SQL & " WHERE     FA1600.FA04CODFACT = FA0400.FA04CODFACT " _
                  & "       AND FA1500.FA15CODATRIB (+) = FA1600.FA15CODATRIB " _
                  & "       AND FA4600.FA46CODDESEXTERNO (+) =FA1500.FA46CODDESEXTERNO " _
                  & "       AND AD0100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
                  & "       AND CI2200.CI21CODPERSONA = AD0100.CI21CODPERSONA " _
                  & "       AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
                  & "       AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
                  & "       AND AD1100.AD11FECFIN IS NULL " _
                  & "       AND (AD1100.AD11INDVOLANTE IS NULL OR AD1100.AD11INDVOLANTE = 0) " _
                  & "       AND EXISTS " _
                  & "           (SELECT AD19CODTIPODOCUM " _
                  & "            FROM   AD1800 " _
                  & "            WHERE  AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
                  & "               AND AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
                  & "               AND AD1800.AD19CODTIPODOCUM = 1 " _
                  & "               AND AD1800.CI21CODPERSONA = AD0100.CI21CODPERSONA " _
                  & "           )"
       SQL = SQL & "       AND AD0500.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
                  & "       AND AD0500.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
                  & "       AND AD0200.AD02CODDPTO = AD0500.AD02CODDPTO " _
                  & "       AND AD0500.AD05FECFINRESPON IS NULL " _
                  & "       AND FA0400.FA04NUMFACREAL IS NULL " _
                  & "       AND FA0503J.FA05CODCATEG (+) = FA1600.FA14CODNODOCONC " _
                  & "       AND FA0500.FA05CODCATEG (+)= FA1600.FA14CODNODOCONC " _
                  & "       AND FA0400.FA04FECFACTURA " _
                  & "                   BETWEEN TO_DATE(?,'DD/MM/YYYY') " _
                  & "                   AND     LAST_DAY(TO_DATE(?,'DD/MM/YYYY'))" _
                  & "       AND (FA0400.CI32CODTIPECON, FA0400.CI13CODENTIDAD) " _
                  & "                   IN (" & strEnti & ")" _
                  & "       AND FA0400.FA04CANTFACT >0 " _
                  & "       AND FA1600.FA16IMPORTE >0 "
        SQL = SQL & " ORDER BY  FA0400.CI32CODTIPECON, " _
                  & "           FA0400.CI13CODENTIDAD, " _
                  & "           AD0200.AD02CODDPTO, " _
                  & "           CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE, " _
                  & "           FA0400.FA04CODFACT, " _
                  & "           FA1600.FA16NUMLINEA"
                  
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = Format(strFecha, "dd/mm/yyyy")
        qryDatos(1) = Format(strFecha, "dd/mm/yyyy")
        Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
        If Not rsDatos.EOF Then
            ' Se preparan los querys que se utilizar�n en el programa
            Call pPrepararQuerys
        End If
        
        Call pMsgProgress("Cargando facturas")
        
        Do While Not rsDatos.EOF
            blnError = Not fCargaFacturasINSALUD_AddArray(rsDatos, _
                                                      arEntidad(), _
                                                      intNumUltimaEntidad, _
                                                      strUltimoTipoEcon)
            If blnError Then Exit Do
            'rsDatos.MoveNext
        Loop
        rsDatos.Close
        qryDatos.Close
        
        If blnError Then
            Exit Function
        End If
        
        ' Se preparan los datos seg�n la estructura de los documentos
        ' Adem�s a medida que se van separando las facturas,
        ' se van actualizando los n�meros de factura
        
        Call pMsgProgress("Preparando formato facturas")
        blnError = Not fCargaFacturasINSALUD_PrepareDocs(arEntidad(), arResumen())
    
        If blnError Then
            fCargaFacturasINSALUD = False
            Exit Function
        Else
            ' Se actualizan los n�meros de factura
            Call pActualizarNFact(arEntidad(), strFecha)
            fCargaFacturasINSALUD = True
        End If
        
    End If
    
End Function


Private Function fCargaFacturasINSALUD_AddArray(rsDatos As rdoResultset, _
                                                arEntidad() As typeEntidad, _
                                                intNumUltimaEntidad%, _
                                                strUltimoTipoEcon$) As Boolean

    ' Se a�aden los registros al array
    
    Dim i As Integer
    Dim intNumFact As Integer
    Dim intNumLinea As Integer
    Dim rsTipoAsist As rdoResultset
    Dim rsFecFact As rdoResultset
    
    On Error Resume Next
    
    intNumLinea = 0
        
    If rsDatos!CI32CODTIPECON & "/" & rsDatos!CI13CODENTIDAD <> strUltimoTipoEcon Then
        For i = 1 To UBound(arEntidad)
            If rsDatos!CI32CODTIPECON = arEntidad(i).strTipoEcon _
            And rsDatos!CI13CODENTIDAD = arEntidad(i).strEntidad Then
                strUltimoTipoEcon = rsDatos!CI32CODTIPECON & "/" & rsDatos!CI13CODENTIDAD
                intNumUltimaEntidad = i
                Exit For
            End If
        Next i
    End If
    
    intNumFact = UBound(arEntidad(intNumUltimaEntidad).arFacturas)
    intNumFact = intNumFact + 1
    
    ReDim Preserve arEntidad(intNumUltimaEntidad).arFacturas(1 To intNumFact)
    
    Do While Not rsDatos.EOF
        With arEntidad(intNumUltimaEntidad).arFacturas(intNumFact)
            If .lngCodFact = 0 Then
                ' Se introducen los datos de la factura
                .lngCodFact = rsDatos!FA04CODFACT
                .strNumFact = rsDatos!FA04NUMFACT
                .dblImporte = rsDatos!FA04CANTFACT
                .lngNumHistoria = rsDatos!CI22NUMHISTORIA
                .strNumSS = rsDatos!CI22NUMSEGSOC
                .strPaciente = rsDatos!Paciente
                .strFIngreso = rsDatos!AD01FECINICIO
                .strFAlta = rsDatos!AD01FECFIN
                .strDiag = rsDatos!AD01DIAGNOSSALI
                .lngNodoFact = rsDatos!FA09CODNODOCONC_FACT
                .strDptoResp = rsDatos!AD02DESDPTO
                .strFechaDesde = rsDatos!FA04FECDESDE
                .strFechaHasta = rsDatos!FA04FECHASTA
                
                ' Tipo asistencia
                qryTipoAsist(0) = .lngCodFact
                Set rsTipoAsist = qryTipoAsist.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                If Not rsTipoAsist.EOF Then
                    .intTipoAsistencia = rsTipoAsist!TIPOASIST
                Else
                    .intTipoAsistencia = intTipoAmbulatorio
                End If
                rsTipoAsist.Close
                
                ' Fecha de factura, seg�n �ltimo movimiento, en todos los casos excepto Facturaci�n por
                ' Forfait Ambulatorio. En ese caso la fecha ser� la de la consulta (la �ltima consulta)
                If .lngNodoFact = intNodoForfAmbu Then
                    qryFechaFact_Consulta(0) = .lngCodFact
                    Set rsFecFact = qryFechaFact_Consulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    If rsFecFact.EOF Then
                        ' SI NO DEVUELVE NADA -> SE PONE LA FECHA DEL ULTIMO MOVIMIENTO
                        qryFechaFact(0) = .lngCodFact
                        Set rsFecFact = qryFechaFact.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    ElseIf IsNull(rsFecFact!Fecha) Then
                        ' SI LA FECHA ES NULA -> SE PONE LA FECHA DEL ULTIMO MOVIMIENTO
                        qryFechaFact(0) = .lngCodFact
                        Set rsFecFact = qryFechaFact.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    End If
                Else
                    qryFechaFact(0) = .lngCodFact
                    Set rsFecFact = qryFechaFact.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                End If
                
                If Not rsFecFact.EOF Then
                    If IsNull(rsFecFact!Fecha) Then
                        .strFecFactura = rsDatos!FA04FECFACTURA
                    ElseIf CVDate(rsDatos!FA04FECFACTURA) >= CVDate(rsFecFact!Fecha) Then
                        .strFecFactura = rsFecFact!Fecha
                    Else
                        .strFecFactura = rsDatos!FA04FECFACTURA
                    End If
                Else
                    .strFecFactura = rsDatos!FA04FECFACTURA
                End If
                rsFecFact.Close
                
            End If
            
            ' Se incorporan los datos de la l�nea de factura
            intNumLinea = intNumLinea + 1
            ReDim Preserve .arLineas(1 To intNumLinea)
            With .arLineas(intNumLinea)
                .intNLinea = rsDatos!FA16NUMLINEA
                .strDescrip = rsDatos!FA16DESCRIP
                .strDescripINSALUD = rsDatos!DESCRIPINSALUD
                .strDiagINSALUD = rsDatos!diagINSALUD
                .strProcINSALUD = rsDatos!procINSALUD
                .lngCodNodo = rsDatos!FA14CODNODOCONC
                ' Si es radioterapia se pone que se desglose por d�as
                If .lngCodNodo = constRadioterapia Or .lngCodNodo = constRadioterapia1 Then
                    .blnRadioterapia = True
                    .blnPrecioDia = True
                ElseIf rsDatos!FA15PRECIODIA > 0 And rsDatos!FA15PRECIOREF = 0 Then
                    .blnPrecioDia = True
                End If

                .dblImporte = rsDatos!FA16IMPORTE
                .blnMedicacion = rsDatos!INDFARMACIA
                .blnProtesis = rsDatos!INDPROTESIS
                
                If rsDatos!FA15INDSUFICIENTE <> 0 And rsDatos!FA15INDFACTOBLIG <> 0 Then
                    .blnPrincipal = True
                End If
            End With
            rsDatos.MoveNext
            
            If rsDatos.EOF Then Exit Do
            If .lngCodFact <> rsDatos!FA04CODFACT Then Exit Do
        End With
    Loop
    fCargaFacturasINSALUD_AddArray = True
End Function


Private Function fCargaFacturasINSALUD_PrepareDocs(arEntidad() As typeEntidad, _
                                                   arResumen() As typeResumen) As Boolean
    ' Se dejan los datos tal cual han de salir en los impresos
    
    Dim i As Integer, j As Integer, k As Integer
    Dim intTipoFactura As intTipoFact
    Dim intNumFactEntidad As Integer
    Dim intNumFact As Integer
    Dim rsDatos As rdoResultset
    
    On Error Resume Next
    For i = 1 To UBound(arEntidad)
        If arEntidad(i).strEntidad <> "M" Then
            ' Madrid es un caso especial
            ' Se toman las facturas y se van insertando en la nueva estructura
            Call pMsgProgress("Preparando formato facturas de " & arEntidad(i).strDesig)
            For j = 1 To UBound(arEntidad(i).arFacturas)
                With arEntidad(i).arFacturas(j)
                    ' 1� Se mira a qu� tipo de factura corresponde
                    Select Case .lngNodoFact
                        Case intNodoEstancias
                            intTipoFactura = intFactEstancias
                        Case intNodoForfCataratas
                            intTipoFactura = intFactForfait
                        Case intNodoForfInterv
                            ' Por si hay radiocirug�a de met�stasis
                            intTipoFactura = fCargaFacturasINSALUD_PrepareDocs_TipoFact(.lngCodFact)
                            If intTipoFactura = intFactOtros Then
                                intTipoFactura = intFactForfait
                            End If
                        Case intNodoForfAmbu, intNodoForfPruebas
                            intTipoFactura = intFactAmbulatorio
                        Case Else
                            intTipoFactura = fCargaFacturasINSALUD_PrepareDocs_TipoFact(.lngCodFact)
                    End Select
                    
                    '2� Se mira si para la fecha existe factura de ese tipo
                    intNumFactEntidad = 0
                    intNumFact = 0
                    intNumFact = UBound(arEntidad(i).arFacturasEntidad)
                    For k = 1 To intNumFact
                        If arEntidad(i).arFacturasEntidad(k).intTipo = intTipoFactura _
                        And arEntidad(i).arFacturasEntidad(k).strFecha = .strFecFactura Then
                            intNumFactEntidad = k
                            Exit For
                        End If
                    Next k
                    If intNumFactEntidad = 0 Then
                        'intNumFactEntidad = UBound(arEntidad(i).arFacturasEntidad)
                        intNumFactEntidad = intNumFact + 1
                        ReDim Preserve arEntidad(i).arFacturasEntidad(1 To intNumFactEntidad)
                        arEntidad(i).arFacturasEntidad(intNumFactEntidad).intTipo = intTipoFactura
                        arEntidad(i).arFacturasEntidad(intNumFactEntidad).strFecha = .strFecFactura
                    End If
                    ' Se pasan las facturas al formato de hoja
                    Call pCargaFacturasINSALUD_PrepareDocs_AsocFact(arEntidad(), arResumen(), i, j, intNumFactEntidad)
                End With
            Next j
        Else
            ' Facturaci�n de Madrid, por Conceptos
            ' Se toman las facturas y se van insertando en la nueva estructura
            Call pMsgProgress("Preparando formato facturas de " & arEntidad(i).strDesig)
            For j = 1 To UBound(arEntidad(i).arFacturas)
                ' Se toman cada una de la l�neas de Madrid
                Call pCargaFacturasINSALUD_PrepareDocs_AsocFactM(arEntidad(), arResumen(), i, j)
            Next j
        End If
    Next i
    
    fCargaFacturasINSALUD_PrepareDocs = True
End Function

Private Function fCargaFacturasINSALUD_PrepareDocs_TipoFact(lngCodFact As Long) As intTipoFact
    ' Devuelve el tipo de factura en el caso de que sean PET, Resonancias, etc.
    
    Dim rsDatos As rdoResultset
    
    ' Se mira si las l�neas tienen PET
    qryFactPET(0) = lngCodFact
    Set rsDatos = qryFactPET.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        If rsDatos!cuenta > 0 Then
            fCargaFacturasINSALUD_PrepareDocs_TipoFact = intFactPET
            Exit Function
        End If
    End If
    
    ' Si no es as� -> se mira si es de Resonancias
    qryFactRMN(0) = lngCodFact
    Set rsDatos = qryFactRMN.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        If rsDatos!cuenta > 0 Then
            fCargaFacturasINSALUD_PrepareDocs_TipoFact = intFactRMN
            Exit Function
        End If
    End If
    
    ' Se mira si las l�neas tienen Neurofisiolog�a
    qryFactNeurofisio(0) = lngCodFact
    Set rsDatos = qryFactNeurofisio.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        If rsDatos!cuenta > 0 Then
            fCargaFacturasINSALUD_PrepareDocs_TipoFact = intFactNeurofisiologia
            Exit Function
        End If
    End If
    
    ' Si no es as� -> se mira si es de Radiocirug�a
    qryFactRadiocirugia(0) = lngCodFact
    Set rsDatos = qryFactRadiocirugia.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        If rsDatos!cuenta > 0 Then
            fCargaFacturasINSALUD_PrepareDocs_TipoFact = intFactRadiocirugia
            Exit Function
        End If
    End If
    
    ' Si no -> se pone como uno indefinido
    
    fCargaFacturasINSALUD_PrepareDocs_TipoFact = intFactOtros
End Function


Private Function fTipoFact(intTipo As intTipoFact) As String
    Select Case intTipo
        Case intFactEstancias
            fTipoFact = "Estancias"
        Case intFactForfait
            fTipoFact = "Forfait Quir�rgico"
        Case intFactAmbulatorio
            fTipoFact = "Forfait Ambulatorio"
        Case intFactPET
            fTipoFact = "PET"
        Case intFactRMN
            fTipoFact = "Resonancia Magn�tica"
        Case intFactRadiocirugia
            fTipoFact = "Radiocirug�a"
        Case intFactNeurofisiologia
            fTipoFact = "Neurofisiolog�a"
        Case Else
            fTipoFact = "Otros"
    End Select

End Function

Private Sub pActualizarNFact(arEnti() As typeEntidad, strFecha$)
    ' Actualiza los n�meros de factura que no tengan el formato 13/
    ' y guarda en la tabla FA5900 los n�meros de factura asignados a cada provincia
    Dim SQL As String
    Dim qryNumFact As rdoQuery, rsNumFact As rdoResultset
    Dim qryCodFact As rdoQuery
    Dim intNumEnti As Integer
    Dim intNumFact As Integer
    Dim i As Integer
    Dim j As Integer
    Dim strNumFact_New As String
    Dim strEnti As String
    Dim qryFact As rdoQuery
    Dim strTipoFact As String
    Dim blnMadrid As Boolean
    Dim intMinFactMadrid As Integer
    
    On Error Resume Next
    intNumEnti = UBound(arEnti)
    If intNumEnti > 0 Then
        For i = 1 To intNumEnti
            If arEnti(i).strEntidad <> "M" Then
                strEnti = strEnti & "('" & arEnti(i).strTipoEcon & "'" _
                                  & ",'" & arEnti(i).strEntidad & "'" _
                                  & "),"
            Else
                blnMadrid = True
                ' Se recoge el primer n�mero de factura que le corresponde
                SQL = "SELECT    MIN(TO_NUMBER(FA5900.FA04NUMFACT)) as NUMFACT " _
                    & " FROM    FA5900 " _
                    & " WHERE   FA5900.FA59FECHA = LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " _
                    & "     AND CI32CODTIPECON = 'S' " _
                    & "     AND CI13CODENTIDAD = 'M' "
                Set qryNumFact = objApp.rdoConnect.CreateQuery("", SQL)
                qryNumFact(0) = Format$(strFecha, "dd/mm/yyyy")
                Set rsNumFact = qryNumFact.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                If Not rsNumFact.EOF Then
                    If Not IsNull(rsNumFact!numFact) Then
                        intMinFactMadrid = rsNumFact!numFact
                    End If
                End If
                rsNumFact.Close
                qryNumFact.Close
                If intMinFactMadrid = 0 Then
                    ' Se toma como primer n�mero de Madrid el m�ximo asignado este a�o.
                    SQL = "SELECT    MAX(TO_NUMBER(FA5900.FA04NUMFACT)) +1  as NUMFACT " _
                        & " FROM    FA5900 " _
                        & " WHERE   FA5900.FA59FECHA BETWEEN " _
                        & "                 TO_DATE(?,'DD/MM/YYYY') " _
                        & "                 AND  " _
                        & "                 TO_DATE(?,'DD/MM/YYYY') " _
                        & "     AND CI32CODTIPECON = 'S' " _
                        & "     AND CI13CODENTIDAD = 'M' "
                    Set qryNumFact = objApp.rdoConnect.CreateQuery("", SQL)
                    qryNumFact(0) = "1/1/" & Format$(strFecha, "yyyy")
                    qryNumFact(1) = "31/12/" & Format$(strFecha, "yyyy")
                    Set rsNumFact = qryNumFact.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    If Not rsNumFact.EOF Then
                        If Not IsNull(rsNumFact!numFact) Then
                            intMinFactMadrid = rsNumFact!numFact
                        End If
                    End If
                End If
                If intMinFactMadrid = 0 Then
                    ' Se asigna el 1, ya que todav�a no se ha asignado ninguno este a�o
                    intMinFactMadrid = 1
                End If
            End If
        Next i
        If Right(strEnti, 1) = "," Then
            strEnti = Left(strEnti, Len(strEnti) - 1)
        End If
    End If
    
    Call pMsgProgress("Generando N�meros facturas")
    
    ' Para actualizar los n�meros de factura
    SQL = "UPDATE FA0400 " _
        & " SET     FA04NUMFACT = ? " _
        & " WHERE   FA04NUMFACT = ? "
    Set qryNumFact = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Para actualizar los n�meros de factura
    SQL = "UPDATE FA0400 " _
        & " SET     FA04NUMFACT = ? " _
        & " WHERE   FA04CODFACT = ? "
    Set qryCodFact = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Para Actualizar la FA5900
    SQL = "INSERT INTO FA5900 " _
        & " (   CI32CODTIPECON , " _
        & "     CI13CODENTIDAD, " _
        & "     FA04NUMFACT, " _
        & "     FA59FECHA, " _
        & "     FA59OBSERV " _
        & " ) " _
        & " VALUES " _
        & " ( ?,?,?,LAST_DAY(TO_DATE(?,'DD/MM/YYYY')),?)"
    Set qryFact = objApp.rdoConnect.CreateQuery("", SQL)
    
    SQL = " DELETE FA5900 " _
        & " WHERE    (CI32CODTIPECON, CI13CODENTIDAD) IN (" & strEnti & ")" _
        & "     AND FA59FECHA = TRUNC(LAST_DAY(TO_DATE('" & Format$(strFecha, "dd/mm/yyyy") & "','DD/MM/YYYY')))"
    objApp.rdoConnect.Execute SQL
    
    For i = 1 To intNumEnti
        intNumFact = 0
        intNumFact = UBound(arEnti(i).arFacturasEntidad)
        For j = 1 To intNumFact
            With arEnti(i).arFacturasEntidad(j)
                If arEnti(i).strEntidad <> "M" Then
                    If .strNumFact = "" Then
                        SQL = "SELECT   FA04NUMFACT_SEQUENCE.NEXTVAL as NumFact " _
                            & " FROM    DUAL"
                        Set rsNumFact = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
                        strNumFact_New = "13/" & rsNumFact(0)
                        rsNumFact.Close
                        
                        qryCodFact(0) = strNumFact_New
                        qryCodFact(1) = .arPacientes(1).lngCodFact
                        qryCodFact.Execute
                        
                        .strNumFact = strNumFact_New
                        
                    ElseIf Left(.strNumFact, 3) <> "13/" Then
                        SQL = "SELECT   FA04NUMFACT_SEQUENCE.NEXTVAL " _
                            & " FROM    DUAL"
                        Set rsNumFact = objApp.rdoConnect.OpenResultset(SQL)
                        strNumFact_New = "13/" & rsNumFact(0)
                        rsNumFact.Close
                
                        ' Se actualiza el n�mero de factura
                        qryNumFact(0) = strNumFact_New
                        qryNumFact(1) = .strNumFact
                        qryNumFact.Execute
                        ' FacturaEstanc = FactEstancMala
                        .strNumFact = strNumFact_New
                    End If
                    ' Se incopora la factura a la tabla FA5900
                    qryFact(0) = arEnti(i).strTipoEcon
                    qryFact(1) = arEnti(i).strEntidad
                    qryFact(2) = .strNumFact
                    qryFact(3) = Format$(strFecha, "dd/mm/yyyy")
                    qryFact(4) = "Tipo: " & IIf(arEnti(i).strEntidad = "M", .strDescrip, fTipoFact(.intTipo)) & " Fecha: " & Format$(.strFecha, "dd/mm/yyyy")
                    qryFact.Execute
                Else
                    If blnMadrid Then
                        ' Hay que actualizar Madrid
                        strNumFact_New = Format$(intMinFactMadrid, "##") & "/" & Format$(strFecha, "yyyy")
                        
                        qryCodFact(0) = strNumFact_New
                        qryCodFact(1) = .arPacientes(1).lngCodFact
                        qryCodFact.Execute
                        
                        .strNumFact = strNumFact_New
                        
                        ' Se incopora la factura a la tabla FA5900
                        qryFact(0) = arEnti(i).strTipoEcon
                        qryFact(1) = arEnti(i).strEntidad
                        qryFact(2) = intMinFactMadrid
                        qryFact(3) = Format$(strFecha, "dd/mm/yyyy")
                        qryFact(4) = "Tipo: " & IIf(arEnti(i).strEntidad = "M", .strDescrip, fTipoFact(.intTipo)) & " Fecha: " & Format$(.strFecha, "dd/mm/yyyy")
                        qryFact.Execute
                        
                        intMinFactMadrid = intMinFactMadrid + 1
                    End If
                End If
            End With
        Next j
    Next i
    
    qryNumFact.Close
    
End Sub

Public Sub pActualizarNumerosDeFactura(arEnti() As typeEntidad, strFecha$)
    ' Actualiza los n�meros de factura de todas las facturas de las entidades
    Dim SQL As String
    Dim qryNumFact As rdoQuery
    Dim intNumEnti As Integer
    Dim intNumFact As Integer
    Dim intNumPac As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    
    On Error Resume Next
    ' Para actualizar los n�meros de factura
    SQL = "UPDATE FA0400 " _
        & " SET     FA04NUMFACT = ?, " _
        & "         FA04FECFACTURA = LAST_DAY(TRUNC(TO_DATE(?,'DD/MM/YYYY')))," _
        & "         FA61CODTIPOFACT = ? " _
        & " WHERE   FA04CODFACT = ? "
    Set qryNumFact = objApp.rdoConnect.CreateQuery("", SQL)
    
    Call pMsgProgress("Actualizando N�meros facturas")
    
    intNumEnti = UBound(arEnti)
    For i = 1 To intNumEnti
        intNumFact = 0
        intNumFact = UBound(arEnti(i).arFacturasEntidad)
        For j = 1 To intNumFact
            With arEnti(i).arFacturasEntidad(j)
                If .strNumFact <> "" Then
                    intNumPac = 0
                    intNumPac = UBound(.arPacientes)
                    For k = 1 To intNumPac
                        ' Se incorpora la factura a la tabla FA5900
                        qryNumFact(0) = .strNumFact
                        qryNumFact(1) = Format$(strFecha, "dd/mm/yyyy")
                        If arEnti(i).strEntidad = "M" Then
                            qryNumFact(2) = constTipoFactJASS
                        Else
                            qryNumFact(2) = constTipoFactIndividual
                        End If
                        qryNumFact(3) = .arPacientes(k).lngCodFact
                        qryNumFact.Execute
                    Next k
                End If
            End With
        Next j
    Next i
    
    qryNumFact.Close
    

End Sub


Private Sub pCargaFacturasINSALUD_PrepareDocs_AddResumen(arEntidad() As typeEntidad, _
                                                        arResumen() As typeResumen, _
                                                        intEnti%, _
                                                        intFact%, _
                                                        intNumLinea%, _
                                                        intNumFactEnti%)
    ' Se incorpora el dato de la linea especificada al resumen
    
    Dim strDescrip As String
    Dim strDescrip2 As String
    Dim intNumConceptos As Integer
    Dim intConcepto As Integer
    Dim i As Integer
    Dim dblCantidad As Double
    
    
    With arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea)
        If .blnProtesis Then
            strDescrip = strDescripProtesis
        ElseIf .blnMedicacion Then
            strDescrip = strDescripMedicacion
        ElseIf .strDescripINSALUD <> "" Then
            strDescrip = .strDiagINSALUD
            strDescrip2 = .strProcINSALUD
        Else
            strDescrip = .strDescrip
        End If
    End With
    
    On Error Resume Next
    
    '   Se busca la descripci�n en la estructura. Si no est� -> se incorpora
    With arEntidad(intEnti).arFacturasEntidad(intNumFactEnti)
        intNumConceptos = UBound(.arResumen)
        For i = 1 To intNumConceptos
            If UCase(.arResumen(i).strTexto) = UCase(strDescrip) And UCase(.arResumen(i).strTexto2) = UCase(strDescrip2) Then
                intConcepto = i
                Exit For
            End If
        Next i
        If intConcepto = 0 Then
            intConcepto = intNumConceptos + 1
            ReDim Preserve .arResumen(1 To intConcepto)
        End If
        With .arResumen(intConcepto)
            .dblImporte = .dblImporte + arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblImporte
            .strTexto = strDescrip
            .strTexto2 = strDescrip2
            If Not arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnProtesis _
            And Not arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnMedicacion Then
                dblCantidad = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblCantidad
                If dblCantidad = 0 Then
                    dblCantidad = 1
                    arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblCantidad = 1
                End If
                .dblCantidad = .dblCantidad + dblCantidad
                If .dblCantidad <> 0 And .dblPrecio = 0 Then
                    .dblPrecio = .dblImporte / .dblCantidad
'                Elseif .dblPrecio = 0
'                    .dblPrecio = 0
                End If
            End If
        End With
    End With
    
    With arResumen(arEntidad(intEnti).arFacturas(intFact).intTipoAsistencia)
        ' Se busca si el concepto existe en el resumen
        intNumConceptos = 0
        intNumConceptos = UBound(.arDatos)
        intConcepto = 0
        For i = 1 To intNumConceptos
            If .arDatos(i).strDescrip1 = strDescrip And .arDatos(i).strDescrip2 = strDescrip2 Then
                intConcepto = i
                Exit For
            End If
        Next i
        If intConcepto = 0 Then
            intConcepto = intNumConceptos + 1
            ReDim Preserve .arDatos(1 To intConcepto)
        End If
        With .arDatos(intConcepto)
            .dblImporte = .dblImporte + arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblImporte
            .strDescrip1 = strDescrip
            .strDescrip2 = strDescrip2
            If Not arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnProtesis _
            And Not arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnMedicacion Then
                .dblCantidad = .dblCantidad + arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblCantidad
                If .dblCantidad <> 0 And .dblPrecio = 0 Then
                    .dblPrecio = .dblImporte / .dblCantidad
                End If
            End If
        End With
    End With
End Sub

Private Sub pCargaFacturasINSALUD_PrepareDocs_AsocFact(arEntidad() As typeEntidad, _
                                                        arResumen() As typeResumen, _
                                                        intEnti%, _
                                                        intFact%, _
                                                        intNumFactEnti%)
    'Pasa los datos de una factura a la nueva estructura
    Dim intNumFacturas As Integer
    Dim intNumLineas As Integer
    Dim rsDatos As rdoResultset
    Dim blnPrincipal As Boolean
    Dim i As Integer
    
    ' Se inserta la nueva factura
    On Error Resume Next
    intNumFacturas = UBound(arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).arPacientes)
    intNumFacturas = intNumFacturas + 1
    
    If arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).strNumFact = "" Then
        arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).strNumFact = arEntidad(intEnti).arFacturas(intFact).strNumFact
    ElseIf Left(arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).strNumFact, 3) <> "13/" _
           And Left(arEntidad(intEnti).arFacturas(intFact).strNumFact, 3) = "13/" Then
        arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).strNumFact = arEntidad(intEnti).arFacturas(intFact).strNumFact
    End If
    
    ReDim Preserve arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).arPacientes(1 To intNumFacturas)
    With arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).arPacientes(intNumFacturas)
        .lngNumHistoria = arEntidad(intEnti).arFacturas(intFact).lngNumHistoria
        .lngCodFact = arEntidad(intEnti).arFacturas(intFact).lngCodFact
        .strNumSS = arEntidad(intEnti).arFacturas(intFact).strNumSS
        .strPaciente = arEntidad(intEnti).arFacturas(intFact).strPaciente
        .strFechaIngreso = arEntidad(intEnti).arFacturas(intFact).strFIngreso
        'If arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).intTipo = intFactEstancias Then
        '    .strFechaIngreso = arEntidad(intEnti).arFacturas(intFact).strFechaDesde
        'Else
        '    .strFechaIngreso = arEntidad(intEnti).arFacturas(intFact).strFIngreso
        'End If
        .strFechaAlta = arEntidad(intEnti).arFacturas(intFact).strFAlta
        'If arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).intTipo = intFactEstancias Then
        '    .strFechaAlta = arEntidad(intEnti).arFacturas(intFact).strFechaHasta
        'Else
        '    .strFechaAlta = arEntidad(intEnti).arFacturas(intFact).strFAlta
        'End If
        .strDpto = arEntidad(intEnti).arFacturas(intFact).strDptoResp
        .strDiagnostico = arEntidad(intEnti).arFacturas(intFact).strDiag
        If arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).intTipo = intFactEstancias Then
            ' Si es estancias -> se obtiene la fecha de intervenci�n, si la hay
            qryInterv(0) = arEntidad(intEnti).arFacturas(intFact).lngCodFact
            Set rsDatos = qryInterv.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If Not rsDatos.EOF Then
                .strFechaIntervencion = rsDatos!Fecha
            End If
            rsDatos.Close
        End If
        
        ' Ahora se incorporan las l�neas de factura
        ' Primero se incorporan las l�neas principales, y a continuaci�n el resto
        ' La identificaci�n de las principales se hace por el valor blnPrincipal
        

        intNumLineas = 0
        intNumLineas = UBound(arEntidad(intEnti).arFacturas(intFact).arLineas)
        
        ' Paso de datos de lineas principales
        For i = 1 To intNumLineas
            If arEntidad(intEnti).arFacturas(intFact).arLineas(i).blnPrincipal Then
                Call pCargaFacturasINSALUD_PrepareDocs_PasaLineas(arEntidad(), _
                                                        arResumen(), _
                                                        intEnti, _
                                                        intFact, _
                                                        intNumFactEnti, _
                                                        intNumFacturas, _
                                                        i, _
                                                        True)
            End If
        Next i
        
        ' A continuaci�n pasamos las que no son principales (complementos)
        For i = 1 To intNumLineas
            If Not arEntidad(intEnti).arFacturas(intFact).arLineas(i).blnPrincipal Then
                Call pCargaFacturasINSALUD_PrepareDocs_PasaLineas(arEntidad(), _
                                                        arResumen(), _
                                                        intEnti, _
                                                        intFact, _
                                                        intNumFactEnti, _
                                                        intNumFacturas, _
                                                        i, _
                                                        False)
            End If
        Next i
        
    End With
    
End Sub

Private Sub pCargaFacturasINSALUD_PrepareDocs_AsocFactM(arEntidad() As typeEntidad, _
                                                        arResumen() As typeResumen, _
                                                        i%, _
                                                        j%)
    ' Se formatean las facturas de Madrid
    
    Dim intNumLineas As Integer
    Dim k As Integer, l As Integer
    Dim intTipoFactura As intTipoFact
    Dim intTipoFactLinea As intTipoFact
    Dim intNumFactEnti As Integer
    Dim strDescrip As String
    Dim intNumFactSeleccionada As Integer
    Dim intNuevoPaciente As Integer
    Dim intPacientes As Integer
    Dim rsDatos As rdoResultset
    Dim blnPacLocalizado As Boolean
    
    On Error Resume Next
    With arEntidad(i).arFacturas(j)
        ' 1� Se mira a qu� tipo de factura corresponde
        Select Case .lngNodoFact
            Case intNodoEstancias
                intTipoFactura = intFactEstancias
            Case intNodoForfCataratas
                intTipoFactura = intFactForfait
            Case intNodoForfInterv
                intTipoFactura = intFactForfait
            Case intNodoForfAmbu, intNodoForfPruebas
                intTipoFactura = intFactAmbulatorio
            Case Else
                intTipoFactura = intFactOtros
        End Select
        
        intNumLineas = UBound(.arLineas)
            
        ' Si es nodo principal ser� del tipo especificado en la factura.
        ' El resto han de ser del tipo Otros.
        For k = 1 To intNumLineas
            With .arLineas(k)
                If .blnPrincipal Then
                    intTipoFactLinea = intTipoFactura
                Else
                    intTipoFactLinea = intFactOtros
                End If
                ' Se mira si existe una entrada para ese tipo en Madrid
                intNumFactEnti = UBound(arEntidad(i).arFacturasEntidad)
                intNumFactSeleccionada = 0
                If .strDescripINSALUD <> "" Then
                    strDescrip = .strDescripINSALUD
                Else
                    If .blnProtesis Then
                        strDescrip = strDescripProtesis
                    ElseIf .blnMedicacion Then
                        strDescrip = strDescripMedicacion
                    Else
                        strDescrip = .strDescrip
                    End If
                End If
                For l = 1 To intNumFactEnti
                    If arEntidad(i).arFacturasEntidad(l).intTipo = intTipoFactLinea _
                        And arEntidad(i).arFacturasEntidad(l).strFecha = arEntidad(i).arFacturas(j).strFecFactura _
                        And arEntidad(i).arFacturasEntidad(l).strDescrip = strDescrip Then
                            intNumFactSeleccionada = l
                            Exit For
                    End If
                Next l
                If intNumFactSeleccionada = 0 Then
                    ' Se crea nueva factura
                    intNumFactSeleccionada = intNumFactEnti + 1
                    ReDim Preserve arEntidad(i).arFacturasEntidad(1 To intNumFactSeleccionada)
                    arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).intTipo = intTipoFactLinea
                    arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).strFecha = arEntidad(i).arFacturas(j).strFecFactura
                    arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).strDescrip = strDescrip
                End If
                ' 1 � Se mira si existe esa factura en el grupo
                blnPacLocalizado = False
                intPacientes = 0
                intPacientes = UBound(arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).arPacientes)
                For l = 1 To intPacientes
                    If arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).arPacientes(l).lngCodFact = _
                       arEntidad(i).arFacturas(j).lngCodFact Then
                        blnPacLocalizado = True
                        intNuevoPaciente = l
                        Exit For
                    End If
                Next l
                ' Se carga 1� el paciente y despu�s la l�nea
                If Not blnPacLocalizado Then
                    intNuevoPaciente = intPacientes + 1
                    ReDim Preserve arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).arPacientes(1 To intNuevoPaciente)
                End If
                
                With arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).arPacientes(intNuevoPaciente)
                    .lngCodFact = arEntidad(i).arFacturas(j).lngCodFact
                    .strDiagnostico = arEntidad(i).arFacturas(j).strDiag
                    .strDpto = arEntidad(i).arFacturas(j).strDptoResp
                    .strFechaAlta = arEntidad(i).arFacturas(j).strFAlta
                    .strFechaIngreso = arEntidad(i).arFacturas(j).strFIngreso
                    If arEntidad(i).arFacturasEntidad(intNumFactSeleccionada).intTipo = intFactEstancias Then
                        ' Si es estancias -> se obtiene la fecha de intervenci�n, si la hay
                        qryInterv(0) = .lngCodFact
                        Set rsDatos = qryInterv.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                        If Not rsDatos.EOF Then
                            .strFechaIntervencion = rsDatos!Fecha
                        End If
                        rsDatos.Close
                    End If
                    .strNumSS = arEntidad(i).arFacturas(j).strNumSS
                    .strPaciente = arEntidad(i).arFacturas(j).strPaciente
                    .lngNumHistoria = arEntidad(i).arFacturas(j).lngNumHistoria
                    'ReDim Preserve .arFacturasEntidad_Lineas(1 To intNumLineas)
                    ' Primero se pasan las l�neas principales
                    'For l = 1 To intNumLineas
                        'If arEntidad(i).arFacturas(j).arLineas(l).blnPrincipal Then
                            Call pCargaFacturasINSALUD_PrepareDocs_PasaLineas(arEntidad(), _
                                                            arResumen(), _
                                                            i, _
                                                            j, _
                                                            intNumFactSeleccionada, _
                                                            intNuevoPaciente, _
                                                            k, _
                                                            arEntidad(i).arFacturas(j).arLineas(k).blnPrincipal)
                        'End If
                    'Next l
                    ' Se pasan las no principales
                    'For l = 1 To intNumLineas
'                        If Not arEntidad(i).arFacturas(j).arLineas(l).blnPrincipal Then
'                            Call pCargaFacturasINSALUD_PrepareDocs_PasaLineas(arEntidad(), _
'                                                            i, _
'                                                            j, _
'                                                            intNumFactSeleccionada, _
'                                                            intNuevoPaciente, _
'                                                            l, _
'                                                            False)
'                        End If
                    'Next l
                End With
            End With
        Next k
    End With
End Sub

Private Sub pCargaFacturasINSALUD_PrepareDocs_PasaLineas(arEntidad() As typeEntidad, _
                                                        arResumen() As typeResumen, _
                                                        intEnti%, _
                                                        intFact%, _
                                                        intNumFactEnti%, _
                                                        intNumFacturas%, _
                                                        intNumLinea%, _
                                                        blnPrincipal As Boolean)
                
    Dim intNuevaLinea As Integer
    Dim rsDatos As rdoResultset
    Dim dblPrecio As Double
    Dim blnRadioterapia As Boolean
    
    On Error Resume Next
    intNuevaLinea = 0
    
    With arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).arPacientes(intNumFacturas)
        intNuevaLinea = UBound(.arFacturasEntidad_Lineas)
        intNuevaLinea = intNuevaLinea + 1
        ReDim Preserve .arFacturasEntidad_Lineas(1 To intNuevaLinea)
        
        With .arFacturasEntidad_Lineas(intNuevaLinea)
            .blnPrincipal = blnPrincipal
            .blnMostrar = True ' La l�nea se muestra por defecto
                        
            If arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnProtesis _
               And (arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).intTipo = intFactEstancias _
                  Or arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).intTipo = intFactForfait) Then
                ' Si es pr�tesis -> se suma el importe a la columna de suplementos de la primera l�nea
                With arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).arPacientes(intNumFacturas).arFacturasEntidad_Lineas(1)
                    .dblImporteSuplementos = .dblImporteSuplementos + arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblImporte
                End With
                ' La l�nea individual de la pr�tesis no se muestra
                .blnMostrar = False
            Else
                .blnRadioterapia = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnRadioterapia
                blnRadioterapia = .blnRadioterapia
                .blnPrecioDia = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnPrecioDia
                .dblImporte = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblImporte
            End If
            If arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).strDescripINSALUD <> "" Then
                .strDescrip = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).strDescripINSALUD
            Else
                .strDescrip = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).strDescrip
            End If
            
            ' Se obtienen las cantidades
            qryCantidad(0) = arEntidad(intEnti).arFacturas(intFact).lngCodFact
            qryCantidad(1) = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).intNLinea
            Set rsDatos = qryCantidad.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        End With
        If Not .arFacturasEntidad_Lineas(intNuevaLinea).blnPrecioDia Then
            .arFacturasEntidad_Lineas(intNuevaLinea).dblCantidad = rsDatos!sumacantidad
            ' Se actualiza tambi�n el dato en el array de datos originales
            arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblCantidad = rsDatos!sumacantidad
            If Not arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnProtesis _
                And Not arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).blnMedicacion Then
                .arFacturasEntidad_Lineas(intNuevaLinea).strFechas = Format$(rsDatos!Fecha, "dd")
            End If
            
        Else
            If Not .arFacturasEntidad_Lineas(intNuevaLinea).blnRadioterapia Then
                .arFacturasEntidad_Lineas(intNuevaLinea).dblCantidad = rsDatos!cuentafechas
            Else
                .arFacturasEntidad_Lineas(intNuevaLinea).dblCantidad = rsDatos!sumacantidad
            End If
            .arFacturasEntidad_Lineas(intNuevaLinea).blnMostrar = False ' En el caso de que se desglose por fechas no se muetra la l�nea de factura
            
            If .arFacturasEntidad_Lineas(intNuevaLinea).dblCantidad <> 0 And .arFacturasEntidad_Lineas(intNuevaLinea).dblPrecio = 0 Then
                dblPrecio = .arFacturasEntidad_Lineas(intNuevaLinea).dblImporte / .arFacturasEntidad_Lineas(intNuevaLinea).dblCantidad
                .arFacturasEntidad_Lineas(intNuevaLinea).dblPrecio = dblPrecio
            End If
            
            ' Se actualiza tambi�n el dato en el array de datos originales
            arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).dblCantidad = .arFacturasEntidad_Lineas(intNuevaLinea).dblCantidad
            
            ' Se trata de facturaci�n por d�a ->
            ' Se listan en lineas sucesivas las diferentes fechas
            qryFechas(0) = arEntidad(intEnti).arFacturas(intFact).lngCodFact
            qryFechas(1) = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).intNLinea
            Set rsDatos = qryFechas.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            Do While Not rsDatos.EOF
                intNuevaLinea = intNuevaLinea + 1
                ReDim Preserve .arFacturasEntidad_Lineas(1 To intNuevaLinea)
                
                With .arFacturasEntidad_Lineas(intNuevaLinea)
                    If arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).strDescripINSALUD <> "" Then
                        .strDescrip = Chr$(9) & arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).strDescripINSALUD
                    Else
                        .strDescrip = Chr$(9) & arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).strDescrip
                    End If
                    .blnRadioterapia = blnRadioterapia
                    If .blnRadioterapia Then
                        .dblCantidad = rsDatos!cantidad
                    Else
                        .dblCantidad = 1
                    End If
                    .strFechas = Format$(rsDatos!Fecha, "dd")
                    .dblPrecio = dblPrecio
                    .dblImporte = .dblCantidad * dblPrecio
                    .blnMostrar = True
                End With
                rsDatos.MoveNext
            Loop
        End If
        rsDatos.Close
        
        With .arFacturasEntidad_Lineas(intNuevaLinea)
            ' Se calcula el precio
            If .dblCantidad <> 0 And .dblPrecio = 0 Then
                .dblPrecio = .dblImporte / .dblCantidad
            End If
            
            ' Si estamos cargando forfait ambulatorios y se trata de una l�nea principal ->
            ' se mira el n�mero de consultas y el de revisiones
            If .blnPrincipal And arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).intTipo = intFactAmbulatorio Then
                qryFactRevisiones(0) = arEntidad(intEnti).arFacturas(intFact).lngCodFact
                qryFactRevisiones(1) = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).intNLinea
                Set rsDatos = qryFactRevisiones.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                If Not rsDatos.EOF Then
                    Do While Not rsDatos.EOF
                        If rsDatos!tipo = "R" Then
                            ' Revisi�n
                            .dblCantidadSucesiva = .dblCantidadSucesiva + rsDatos!cuenta
                            If .dblPrecioSucesiva = 0 Then
                                .dblPrecioSucesiva = rsDatos!importe / rsDatos!cuenta
                            End If
                        ElseIf rsDatos!tipo = "C" Then
                            ' Consulta
                            .dblCantidadPrimera = .dblCantidadPrimera + rsDatos!cuenta
                            If .dblPrecioPrimera = 0 Then
                                .dblPrecioPrimera = rsDatos!importe / rsDatos!cuenta
                            End If
                        End If
                        rsDatos.MoveNext
                    Loop
                End If
                rsDatos.Close
                ' Se cargan tambi�n las diferentes fechas de consulta si son m�s de una
                If .dblCantidad > 1 Then
                    .strFechas = ""
                    qryFecConsulta(0) = arEntidad(intEnti).arFacturas(intFact).lngCodFact
                    qryFecConsulta(1) = arEntidad(intEnti).arFacturas(intFact).arLineas(intNumLinea).intNLinea
                    Set rsDatos = qryFecConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    Do While Not rsDatos.EOF
                        .strFechas = .strFechas & " " & rsDatos!Fecha
                        rsDatos.MoveNext
                    Loop
                    .strFechas = Trim(.strFechas)
                    rsDatos.Close
                End If
            End If
        
        End With
        
        ' Se incorporan los datos al resumen
        'arEntidad(intEnti).arFacturasEntidad(intNumFactEnti).arPacientes (intNumFacturas)
        Call pCargaFacturasINSALUD_PrepareDocs_AddResumen(arEntidad(), _
                                                arResumen(), _
                                                intEnti, _
                                                intFact, _
                                                intNumLinea, _
                                                intNumFactEnti _
                                                )
    End With

End Sub


Private Sub pMsgProgress(strTexto$, Optional intValor%)
    fraProgreso.Caption = strTexto
    If intValor > 0 Then
        proProgreso.Value = intValor
        'proProgreso.Refresh
    End If
    fraProgreso.Refresh
    fraProgreso.Parent.Refresh
End Sub

Private Sub pPrepararQuerys()
    Dim SQL As String
    
    ' Ambulatorio/Hospitalizado
    SQL = "SELECT    tipoAsist" _
        & " FROM    FA0419J" _
        & " WHERE   FA04CODFACT = ? "
    Set qryTipoAsist = objApp.rdoConnect.CreateQuery("", SQL)

    ' Fecha de Intervenci�n
    SQL = "SELECT    MIN(FA03FECHA) as fecha " _
        & " FROM    FA0300, FA0500 " _
        & " WHERE   FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
        & "     AND FA0300.FA04NUMFACT = ? " _
        & "     AND FA0500.FA08CODGRUPO = " & constIntervencion
    Set qryInterv = objApp.rdoConnect.CreateQuery("", SQL)
        
    ' Cantidades facturadas: por fechas o por cantidades
    SQL = "SELECT    count(DISTINCT TO_CHAR(FA0300.FA03FECHA,'DD/MM/YYYY')) as cuentaFechas, " _
        & "          sum(FA0300.FA03CANTFACT) as sumaCantidad, " _
        & "          TO_CHAR(min(FA0300.FA03FECHA),'DD/MM/YYYY') as fecha " _
        & " FROM    FA0300" _
        & " WHERE   FA0300.FA04NUMFACT = ? " _
        & "     AND FA0300.FA16NUMLINEA = ? " _
        & "     AND FA0300.FA03CANTFACT >0 "
    Set qryCantidad = objApp.rdoConnect.CreateQuery("", SQL)
    
         
    ' Diferentes fechas de cada concepto
    SQL = "SELECT    TRUNC(FA0300.FA03FECHA) AS fecha, " _
        & "          SUM(FA0300.FA03CANTFACT) as cantidad " _
        & " FROM    FA0300" _
        & " WHERE   FA0300.FA04NUMFACT = ? " _
        & "     AND FA0300.FA16NUMLINEA = ? " _
        & "     AND FA0300.FA03CANTFACT >0 " _
        & " GROUP BY TRUNC(FA0300.FA03FECHA) " _
        & " ORDER BY TRUNC(FA0300.FA03FECHA) "
    Set qryFechas = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Fecha m�xima. Utilizada para ver la fecha de factura, seg�n fecha del �ltimo movimiento
    SQL = "SELECT    TO_CHAR(LAST_DAY(MAX(FA0300.FA03FECHA)),'DD/MM/YYYY') AS fecha " _
        & " FROM    FA0300" _
        & " WHERE   FA0300.FA04NUMFACT = ? "
    Set qryFechaFact = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Fecha m�xima de consulta. Utilizada para ver la fecha de factura, seg�n fecha del �ltimo movimiento
    ' de consultas
    SQL = "SELECT    TO_CHAR(LAST_DAY(MAX(FA0300.FA03FECHA)),'DD/MM/YYYY') AS fecha " _
        & " FROM    FA0300, " _
        & "         FA0500 " _
        & " WHERE   FA0300.FA04NUMFACT = ? " _
        & "     AND FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
        & "     AND FA0500.FA05CODORIGC2 = 0 " _
        & "     AND FA0500.FA05CODORIGC3 in (1,2,4, 7)" _
        & "     AND FA0300.FA03CANTFACT >0 "
    Set qryFechaFact_Consulta = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Mira si la factura corresponde a PET
    SQL = "SELECT COUNT(*) as cuenta " _
        & " FROM    FA1600, " _
        & "         FA0500 " _
        & " WHERE   FA04CODFACT = ? " _
        & "     AND FA0500.FA05CODCATEG = FA1600. FA14CODNODOCONC " _
        & "     AND FA0500.FA05CODCATEG_P = " & constPET _
        & "     AND FA1600.FA16IMPORTE >0 "
    Set qryFactPET = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Mira si la factura corresponde a Neurofisiologia
    SQL = "SELECT COUNT(*) as cuenta " _
        & " FROM    FA1600, " _
        & "         FA0500 " _
        & " WHERE   FA04CODFACT = ? " _
        & "     AND FA0500.FA05CODCATEG = FA1600. FA14CODNODOCONC " _
        & "     AND FA0500.FA05CODCATEG_P = " & constNeurofisiologia _
        & "     AND FA1600.FA16IMPORTE >0 "
    Set qryFactNeurofisio = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Mira si la factura corresponde a RMN
    SQL = "SELECT COUNT(*) as cuenta " _
        & " FROM    FA1600 " _
        & " WHERE   FA04CODFACT = ? " _
        & "     AND FA14CODNODOCONC IN " & constResonancias _
        & "     AND FA16IMPORTE >0 "
    Set qryFactRMN = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Mira si la factura corresponde a Radiocirugia
    SQL = "SELECT COUNT(*) as cuenta " _
        & " FROM    FA1600 " _
        & " WHERE   FA04CODFACT = ? " _
        & "     AND FA14CODNODOCONC = " & constRadiocirugia _
        & "     AND FA16IMPORTE >0 "
    Set qryFactRadiocirugia = objApp.rdoConnect.CreateQuery("", SQL)
    
    
    
    ' Cantidades y precios de consultas y revisiones
    SQL = "SELECT   SUM(FA03PRECIOFACT) as importe , " _
        & "         DECODE(FA0500.FA05CODORIGC3, " _
        & "                         1, 'C' /* CONSULTA */, " _
        & "                         7, 'C' /* CONSULTA */, " _
        & "                         2, 'R' /* REVISION */, " _
        & "                         4, 'R' /* REVISION */ " _
        & "               )  AS Tipo, " _
        & "         sum(FA03CANTFACT) as cuenta " _
        & " FROM    FA0300, " _
        & "         FA0500 " _
        & " WHERE   FA0300.FA04NUMFACT = ? " _
        & "     AND FA0300.FA16NUMLINEA = ? " _
        & "     AND FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
        & "     AND FA0500.FA05CODORIGC2 = 0 " _
        & "     AND FA0500.FA05CODORIGC3 in (1,2,4, 7)" _
        & "     AND FA0300.FA03CANTFACT >0 " _
        & " GROUP BY " _
        & "         DECODE(FA0500.FA05CODORIGC3, " _
        & "                         1, 'C' /* CONSULTA */, " _
        & "                         7, 'C' /* CONSULTA */, " _
        & "                         2, 'R' /* REVISION */, " _
        & "                         4, 'R' /* REVISION */ " _
        & "               )"
    Set qryFactRevisiones = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Fechas diferentes de las consultas
    SQL = "SELECT DISTINCT TO_CHAR(FA0300.FA03FECHA, 'DD') as fecha " _
        & " FROM    FA0300 " _
        & " WHERE   FA0300.FA04NUMFACT = ? " _
        & "     AND FA0300.FA16NUMLINEA = ? " _
        & "     AND FA0300.FA03CANTFACT >0 "
    Set qryFecConsulta = objApp.rdoConnect.CreateQuery("", SQL)
        
End Sub


