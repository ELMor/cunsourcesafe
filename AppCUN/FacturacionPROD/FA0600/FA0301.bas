Attribute VB_Name = "modCW"
Option Explicit

Public objCW As clsCW        ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As clsCWPipeLine
Public objGen As clsCWGen
Public objEnv As clsCWEnvironment
Public objError As clsCWError
Public objMouse As clsCWMouse
Public objSecurity As clsCWSecurity

Type typeConcierto
    C�digo  As Long
    Ruta    As String
End Type

Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objSecurity = objCW.objSecurity
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  End
End Sub

