Attribute VB_Name = "modFactINSALUD_PrepareRegs"
Option Explicit

Dim qryFact As rdoQuery
Dim qryLinea As rdoQuery
Dim qryRNF As rdoQuery
Dim qryImporte As rdoQuery
Private Sub pCrearQuerys()
    Dim SQL As String
    
    ' Copia de facturas
    SQL = "INSERT INTO FA0400 " _
        & "(FA04CODFACT, FA04NUMFACT, CI21CODPERSONA, CI13CODENTIDAD, " _
        & " FA04DESCRIP, FA04DESCUENTO, FA04FECFACTURA, AD01CODASISTENCI, " _
        & " FA04OBSERV, FA09CODNODOCONC, FA04INDCOMPENSA, FA04INDRECORDAR, " _
        & " AD07CODPROCESO, CI32CODTIPECON, FA04FECDESDE, FA04FECHASTA, " _
        & " FA09CODNODOCONC_FACT, FA04INDFACTFINAL) "
    SQL = SQL & " SELECT  ?, " _
        & "         ?, " _
        & "         CI21CODPERSONA, " _
        & "         CI13CODENTIDAD, " _
        & "         FA04DESCRIP, FA04DESCUENTO, FA04FECFACTURA, AD01CODASISTENCI, " _
        & "         FA04OBSERV, FA09CODNODOCONC, FA04INDCOMPENSA, FA04INDRECORDAR, " _
        & "         AD07CODPROCESO, CI32CODTIPECON, FA04FECDESDE, FA04FECHASTA, " _
        & "         " & intNodoActoMedico & ", FA04INDFACTFINAL " _
        & " FROM    FA0400 " _
        & " WHERE   FA04CODFACT = ? "
    Set qryFact = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Movimiento de lineas
    SQL = "UPDATE   FA1600 " _
        & " SET     FA04CODFACT = ? " _
        & " WHERE   FA04CODFACT = ? " _
        & "     AND FA16NUMLINEA = ? "
    Set qryLinea = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Movimiento de RNFs
    SQL = "UPDATE   FA0300 " _
        & " SET     FA04NUMFACT = ? " _
        & " WHERE   FA04NUMFACT = ? " _
        & "     AND FA16NUMLINEA = ? "
    Set qryRNF = objApp.rdoConnect.CreateQuery("", SQL)
    
    ' Actualizaci�n de importes de factura
    SQL = "UPDATE   FA0400 " _
        & " SET     FA04CANTFACT = " _
        & "         (SELECT SUM(FA16IMPORTE) " _
        & "          FROM   FA1600 " _
        & "          WHERE  FA1600.FA04CODFACT = FA0400.FA04CODFACT " _
        & "         )" _
        & " WHERE   FA04CODFACT = ? "
    Set qryImporte = objApp.rdoConnect.CreateQuery("", SQL)
    
End Sub


' Se buscan facturas con RMN y PET, y otras lineas.
' Si se encuentran -> se meten en nuevas facturas


Public Function fPrepareFacturasINSALUD(strFecha$, arEntidad() As typeEntidad) As Boolean
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim strTexto As String
    Dim intNumEnti As Integer
    Dim strEnti As String
    Dim i As Integer
    Dim strCodFact As String
    Dim rsCodFact As rdoResultset
    Dim blnError As Boolean
    
    
    intNumEnti = UBound(arEntidad)
    
    If intNumEnti > 0 Then
        For i = 1 To intNumEnti
            strEnti = strEnti & "('" & arEntidad(i).strTipoEcon & "'" _
                              & ",'" & arEntidad(i).strEntidad & "'" _
                              & "),"
        Next i
        If Right(strEnti, 1) = "," Then
            strEnti = Left(strEnti, Len(strEnti) - 1)
        End If
    
    
        Call pCrearQuerys
        
        SQL = "SELECT   FA0400.FA04CODFACT," _
                & "     FA0400.FA04NUMFACT," _
                & "     FA0400.FA09CODNODOCONC_FACT," _
                & "     FA1600.FA16NUMLINEA, " _
                & "     LTRIM(RTRIM(RPAD(FA1600.FA16DESCRIP,255))) AS FA16DESCRIP," _
                & "     FA1600.FA14CODNODOCONC, " _
                & "     FA0400.CI32CODTIPECON, " _
                & "     FA0400.CI13CODENTIDAD "
        SQL = SQL & " FROM  FA0400, " _
                  & "       FA1600, " _
                  & "       FA0500 "
        SQL = SQL & " WHERE     FA1600.FA04CODFACT = FA0400.FA04CODFACT " _
                  & "       AND FA0500.FA05CODCATEG = FA1600.FA14CODNODOCONC " _
                  & "       AND FA0400.FA04NUMFACREAL IS NULL " _
                  & "       AND FA0400.FA04FECFACTURA " _
                  & "                   BETWEEN TO_DATE(?,'DD/MM/YYYY') " _
                  & "                   AND     LAST_DAY(TO_DATE(?,'DD/MM/YYYY'))" _
                  & "       AND (FA0400.CI32CODTIPECON, FA0400.CI13CODENTIDAD) " _
                  & "                   IN (" & strEnti & ")" _
                  & "       AND CI13CODENTIDAD NOT IN ('NA','M') "
        SQL = SQL & "       AND FA0400.FA04CANTFACT >0 " _
                  & "       AND FA1600.FA16IMPORTE >0 " _
                  & "       AND (FA1600.FA14CODNODOCONC IN " & constResonancias _
                  & "           OR FA1600.FA14CODNODOCONC = " & constRadiocirugia _
                  & "           OR FA0500.FA05CODCATEG_P IN (" & constPET & "," & constNeurofisiologia & ")" _
                  & "           ) " _
                  & "       AND EXISTS ( " _
                  & "                   SELECT  FA16NUMLINEA " _
                  & "                   FROM    FA1600 FA1600_1," _
                  & "                           FA0500 " _
                  & "                   WHERE   FA1600_1.FA04CODFACT = FA0400.FA04CODFACT " _
                  & "                       AND FA0500.FA05CODCATEG = FA1600_1.FA14CODNODOCONC " _
                  & "                       AND FA1600_1.FA14CODNODOCONC NOT IN " & constResonancias _
                  & "                       AND FA1600.FA14CODNODOCONC <> " & constRadiocirugia _
                  & "                       AND FA0500.FA05CODCATEG_P <> " & constNeurofisiologia _
                  & "                       AND FA0500.FA05CODCATEG_P <> " & constPET _
                  & "                       AND FA1600_1.FA16IMPORTE>0) " ' No son Resonancias, ni PET
        SQL = SQL & " ORDER BY  FA0400.CI32CODTIPECON, " _
                  & "           FA0400.CI13CODENTIDAD, " _
                  & "           FA0400.FA04CODFACT, " _
                  & "           FA1600.FA16NUMLINEA"
                  
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = Format(strFecha, "dd/mm/yyyy")
        qryDatos(1) = Format(strFecha, "dd/mm/yyyy")
        Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        ' Se toman las l�neas de facturas y se crean nuevas facturas
        Do While Not rsDatos.EOF
            'strTexto = strTexto & rsDatos!FA04CODFACT & " - " & rsDatos!FA16NUMLINEA _
                                & "      (" & rsDatos!CI32CODTIPECON _
                                & "/" & rsDatos!CI13CODENTIDAD & ")" _
                                & Chr$(13) & Chr$(10)
            
            ' La nueva factura creada ser� una copia de la que parte
            ' S�lo se cambia el c�digo factura, el n�mero factura (se pone uno temporal),
            ' el importe (tambi�n se actualiza el importe de la de origen)
            ' y el nodo facturado (siempre se pone que se factura como si fuese Acto M�dico)
                
            strCodFact = ""
            ' C�digo de Facturas
            SQL = "SELECT FA04CODFACT_SEQUENCE.NEXTVAL as CodFact FROM DUAL "
            Set rsCodFact = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
            If Not rsCodFact.EOF Then
                strCodFact = rsCodFact!CodFact
            
            End If
            rsCodFact.Close
            
            objApp.BeginTrans
            If strCodFact <> "" Then
                ' Creaci�n de la nueva factura
                qryFact(0) = strCodFact
                qryFact(1) = rsDatos!CI32CODTIPECON & rsDatos!CI13CODENTIDAD & "2/" & strCodFact
                qryFact(2) = rsDatos!FA04CODFACT
                qryFact.Execute
                If Err Then
                    objApp.RollBackTrans
                    blnError = True
                    Exit Do
                End If
                
                ' Movimiento de l�nea a la nueva factura
                qryLinea(0) = strCodFact
                qryLinea(1) = rsDatos!FA04CODFACT
                qryLinea(2) = rsDatos!FA16NUMLINEA
                qryLinea.Execute
                If Err Then
                    objApp.RollBackTrans
                    blnError = True
                    Exit Do
                End If
                
                ' Movimiento de RNFs a la nueva factura
                qryRNF(0) = strCodFact
                qryRNF(1) = rsDatos!FA04CODFACT
                qryRNF(2) = rsDatos!FA16NUMLINEA
                qryRNF.Execute
                If Err Then
                    objApp.RollBackTrans
                    blnError = True
                    Exit Do
                End If
                
                ' Actualizaci�n de precios de la nueva factura
                qryImporte(0) = rsDatos!FA04CODFACT
                qryImporte.Execute
                If Err Then
                    objApp.RollBackTrans
                    blnError = True
                    Exit Do
                End If
                
                ' Actualizaci�n de precios de la factura antigua
                qryImporte(0) = strCodFact
                Err = 0
                qryImporte.Execute
                If Err Then
                    objApp.RollBackTrans
                    blnError = True
                    Exit Function
                End If
                objApp.CommitTrans
            Else
                Exit Function
            End If
            rsDatos.MoveNext
        Loop
        rsDatos.Close
        qryDatos.Close
    End If
    
    If Not blnError Then
        fPrepareFacturasINSALUD = True
    End If
    
End Function


