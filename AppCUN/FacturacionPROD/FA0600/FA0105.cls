VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Asistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'Option Explicit

Public Codigo As String
Public RNFs As New Collection
Public PropuestasFactura As New Collection
Public CodConciertos As New Collection
Public FecInicio As String

' son las fechas reales de inicio y fin de la asistencia
Public FecFin As String
Public Facturado As Boolean

' *********************************************************************************
' *   variables para el tratamiento de las garantias
' *********************************************************************************
' esta es la fecha en la que finaliza la garant�a
Public FechaFinGarantia As String
' codigo de factura (FA04CODFACT) que origina la garantia
Public CodFactGarantia
' es el reco de la garantia
 Public REcoGarantia As String
' es el numero de dias acumulados ( se usa para los reingresos en los forfaits de estancias)
Public nDiasAcumulGaran As Integer
' *********************************************************************************



' estas fechas solo aparecen en el impreso de la factura
Public FecInicioFact As String
Public FecFinFact As String

Public FactAntigua As Boolean    ' hay una factura anterior (S/N)
Public NumFactAnt As String      ' Numero de factura anterior
Public FechaAnterior As String   ' Fecha de factura anterior


'' Se utiliza solo para contener los nodos de concierto
'' aplicables que se encuentren despues de hacer una primera
'' asignacion de RNFs. a partir de esta colecci�n se realizan
'' las diferentes combinaciones de conciertos que se puedan
'' aplicar simultaneamente.
'Public NodosAplicables As New Collection
Public FecIngreso As String

' strProcAsist  contiene una cadena del tipo : ((proceso1,asistencia1),(proceso2,asistencia2),...)
'               que se utiliza en las queries para los filtros
Public strProcAsist As String

' ProcAsist contiene los codigos de los diferentes procesos-asistencia a
'           facturar conjuntamente, los cuales se han agrupado en este
'           objeto asistencia
Public ProcAsist As New Collection

' Coleccion que contiene los RECOs de la Asistencia
Public REcos As New Collection
' se corresponde con el key del REco que es el paciente
Public DefaultREco As String


Dim qryCabecera As New rdoQuery
Dim qryGarantia As New rdoQuery
Dim qryLinea As New rdoQuery
Dim qryRNF As New rdoQuery
Dim qryPropuestas As New rdoQuery
Dim qryFactAnt As New rdoQuery

Public DptoResp As String
Public CodDptoResp As String


Public Sub QuitarAsignacion()
   Dim rnf As rnf
    For Each rnf In RNFs
        rnf.Asignado = False
        rnf.Ruta = ""
    Next
End Sub

Public Function BuscarRNF(ByVal Codigo As Long, ByVal FecInicio As String, ByVal FecFin As String, Ruta, KeyREcoFact) As Collection
   Dim rnf As rnf
   Dim RNFOrigen As rnf
   Dim fecha As String
   Dim cont As Integer
   Dim RNFsAsignados As New Collection
   
   
   FecInicio = Format(FecInicio, "yyyymmdd")
   FecFin = Format(FecFin, "yyyymmdd")
   
   On Error GoTo error
   
   cont = 0
   Do While True

Buscar:
   Set RNFOrigen = New rnf
   Set RNFOrigen = RNFs.item(Codigo & "-" & Format(cont, "000"))
   
   ' Crear una copia del RNForigen
   Set rnf = New rnf
   rnf.Copiar RNFOrigen
   
   fecha = Format(rnf.fecha, "yyyymmdd")
   ' Comprobar que est� entre las fechas del nodo del concierto
   
'   If (FecInicio <= Fecha) And (Fecha < FecFin) And _
      (rnf.KeyREco = KeyREcoFact Or KeyREcoFact = DefaultREco) Then
   If (FecInicio <= fecha) And (fecha < FecFin) And _
      (rnf.KeyREco = KeyREcoFact) Then
      
'      If Fecha <= FechaFinGarantia Or FechaFinGarantia = "" Then
         ' si todav�a no est� asignado a ning�n nodo se los asignamos
         ' a este
         If RNFOrigen.Asignado = False Then
            RNFOrigen.Asignado = True
            rnf.Ruta = Ruta
            RNFsAsignados.Add rnf, rnf.Key
         End If
'      End If
   End If
   cont = cont + 1
        
   Loop
    
Exit Function

error:
   Select Case Err.Number
      Case 5
         ' El Rnf no se encuentra en la colecci�n
         Set BuscarRNF = RNFsAsignados
          
      Case vbObjectError + 1
         ' El RNF no est� entre las fechas
         ' hay que buscar el siguiente
         cont = cont + 1
         Resume Buscar
          
      Case Else
         MsgBox "Error n� " & Err.Number & " " & Err.Description
'            Resume
   End Select
End Function

Public Function AsignacionCompleta() As Boolean
    Dim blnAsigCompleta As Boolean
    Dim rnf As rnf
    
    ' Por defecto consideramos que todos los RNFs est�n asignados
    
    blnAsigCompleta = True
    
    ' Revisamos la colecci�n de rnf's y en cuanto encontramos uno sin
    ' asignar nos salimos
    For Each rnf In RNFs
        If rnf.Asignado = False Then
            blnAsigCompleta = False
            Exit For
        End If
    Next
    
    AsignacionCompleta = blnAsigCompleta
End Function

Public Sub AddConcierto(codConcierto As Long, Optional CodNodoFact As Long)

    ' a�ade un nuevo codigo de concierto a la asistencia
    
    
    Dim Concierto As New CodigosConcierto
    
    Concierto.Concierto = codConcierto
    If IsMissing(CodNodoFact) Then
        Concierto.NodoAFacturar = 0
    Else
        Concierto.NodoAFacturar = CodNodoFact
    End If
    
    For Each objconcierto In CodConciertos
        If objconcierto.Concierto = Concierto.Concierto And objconcierto.NodoAFacturar = Concierto.NodoAFacturar Then
            ' no hace falta a�adirlo, ya est� en la coleccion
            Exit Sub
        End If
    Next
    
    CodConciertos.Add Concierto
    
End Sub

Public Sub AddREco(NuevoREco As Persona, FecInicio As String, FecFin As String)
   Dim objREco As Persona
   ' a�ade un nuevo REco a la asistencia
   
   For Each objREco In REcos
'      If objREco.Codigo = NuevoREco.Codigo And _
         objREco.Proceso = NuevoREco.Proceso And _
         objREco.Asistencia = NuevoREco.Asistencia And _
         objREco.CodTipEcon = NuevoREco.CodTipEcon And _
         objREco.EntidadResponsable = NuevoREco.EntidadResponsable Then
      If objREco.Codigo = NuevoREco.Codigo And _
         objREco.CodTipEcon = NuevoREco.CodTipEcon And _
         objREco.EntidadResponsable = NuevoREco.EntidadResponsable Then
         ' no hace falta a�adirlo, ya est� en la coleccion
         ' tan solo hay que a�adir las fechas
         objREco.TramosFechas.Add Array(FecInicio, FecFin, NuevoREco.proceso, NuevoREco.Asistencia)
'         objREco.TramosFechas.Add FecFin
         
         
         NuevoREco.Key = objREco.Key
         Exit Sub
      End If
   Next
   
   ' no existe en la coleccion, hay que a�adirlo
   NuevoREco.TramosFechas.Add Array(FecInicio, FecFin, NuevoREco.proceso, NuevoREco.Asistencia)
'   objREco.TramosFechas.Add FecFin
   NuevoREco.Key = REcos.Count + 1
   
   REcos.Add NuevoREco, NuevoREco.Key
    
End Sub

' devuelve el key del REco que le corresponda segun la fecha y el proceso-asistencia
Public Function BuscarREco(fecha As String, proceso As String, Asistencia As String) As String
   Dim reco As Persona
   Dim Tramo As Variant
   For Each reco In REcos
      ' comprobar que se correspondan los procesos-asistencia o que sea vacio (REco por defecto)
'      If (reco.Proceso = Proceso And reco.Asistencia = Asistencia) Or _
'         (reco.Proceso = "" And reco.Asistencia = "") Then
      
         ' revisar en que tramo de fechas est�
         For Each Tramo In reco.TramosFechas
            If Format(Tramo(0), "yyyymmdd") <= Format(fecha, "yyyymmdd") And _
                     (Tramo(1) = "" Or Format(fecha, "yyyymmdd") < Format(Tramo(1), "yyyymmdd")) And _
                     (Tramo(2) = proceso And Tramo(3) = Asistencia) Or _
                     (Tramo(2) = "" And Tramo(3) = "") Then
               ' encontrado
               BuscarREco = reco.Key
               Exit Function
            End If
         Next Tramo
'      End If
   Next reco
   
   If BuscarREco = "" Then
      BuscarREco = DefaultREco
   End If
End Function

Public Function Dias() As Integer
    ' Devuelve el n� de dias transcurridos entre las
    ' fechas de inicio y fin de la asistencia
    On Error GoTo error
    
    Dias = DateDiff("d", FecInicio, FecFin)
    Exit Function
    
error:
    Dias = 0
End Function

Public Sub Destruir()
   For Each obj In PropuestasFactura
      obj.Destruir
'      Set obj = Nothing
   Next
   Set PropuestasFactura = Nothing
     
   Set RNFs = Nothing
   Set CodConciertos = Nothing
   
   Set ProcAsist = Nothing
   Set REcos = Nothing

End Sub


' Mediante la funci�n GuardarFactura rellenaremos los ficheros FA0400 (cabeceras de factura) y
' FA1600 (L�neas de factura) y se modificar�n los RNFs FA0300
'
'  Parametros :
'     NombrePropuesta : nombre de la propuesta a facturar (es su key dentro de la coleccion)
'     Fecha de la factura
'     TipoFact : indica el tipo de numero de factura que se va a generar,
'                p.e. : "P" si es una factura provisional (valor por defecto)
'                       "SNA1" o "SNA2" para las de Osasunbidea Hospitalizados o Ambulatorios
'
Public Sub GrabarFactura(KeyPropuesta As String, FechaFact As String, Optional TipoFact As String = "P", Optional MostrarMensajes As Boolean = True)
    Dim CodFactura As Long
    Dim NumFactura As String
    Dim Dto As Double
    Dim IntContLineas As Integer
    Dim Facturada As Boolean
'    Dim SubImporte As Double
'    Dim IntCont As Integer
'    Dim rsPaciente As rdoResultset
'    Dim intPos As Integer
'    Dim Coletilla As String
    Dim ProcesoAsistencia As ProcAsist
'    Dim rsEntidad As rdoResultset
    
    Dim objPropuesta As New PropuestaFactura
    Dim objREco As Persona
    Dim objRNF As rnf
    Dim Linea As Nodo
    Dim PropuestaFact As PropuestaFactura
    
    Dim respuesta As Integer
    Dim PrioriAnt As Integer
    
    On Error GoTo ErrorenGrabacion
    
    IniciarQRY
    
    'Abrimos una transacci�n para tratar toda la inserci�n de las facturas.
    objApp.BeginTrans
      
    'Nos posicionamos en la propuesta seleccionada dentro del objeto myFactura
    Set PropuestaFact = PropuestasFactura(KeyPropuesta)
    
    
    
    '--------------------------------------------------------------------------------------------
    'Seleccionar los Responsables econ�micos
    'Comprobaremos que el responsable tiene l�neas de factura.
    'En caso afirmativo a�adiremos una nueva factura con ese responsable.
    '--------------------------------------------------------------------------------------------
    For Each objREco In REcos
        'Inicializamos la variable que nos indica si la factura est� a�adida a la BD.
        Facturada = False
        IntContLineas = 1
        
        '**********************************************************
        '*                guardar las lineas                      *
        '**********************************************************
        For Each NodoFACT In PropuestaFact.NodosFactura
            For Each Linea In NodoFACT.Nodos
                GoSub grabarlinea
            Next Linea
        Next NodoFACT
        
        '**********************************************************
        '*           guardar las lineas a�adidas                  *
        '**********************************************************
        For Each Linea In PropuestaFact.NuevasLineas
            GoSub grabarlinea
        Next Linea
        
            
         ' si es un INSALUD grabar las descripciones de las propuestas para
         ' despues poder realizar los listados de comprobacion
         If objREco.CodTipEcon = "S" And Facturada Then
             PrioriAnt = 32100
             For Each Propuesta In PropuestasFactura
         '      Cod36 = fNextClave("FA36NUMPROP", "FA3600")
'               qryPropuestas.rdoParameters(0) = CodFactura
               qryPropuestas.rdoParameters(2) = Propuesta.Name
               qryPropuestas.rdoParameters(3) = CLng(Propuesta.Importe)
               qryPropuestas.rdoParameters(1) = Propuesta.Prioridad
               qryPropuestas.rdoParameters(4) = IIf(Propuesta.Name = PropuestaFact.Name, 1, 0)
         '      qry(7).rdoParameters(5) = 0
               qryPropuestas.Execute
         '      If Propuesta.Prioridad < PrioriAnt Then
         '        intProp = Cont
         '        PrioriAnt = Propuesta.Prioridad
         '      End If
         '      Cont = Cont + 1
             Next
         '    If intProp = 0 Then intProp = 1
         '    Set PropuestaFact = Asistencia.PropuestasFactura(intProp)
         '    qry(8).rdoParameters(0) = CodFactura
         '    qry(8).rdoParameters(1) = PropuestaFact.Name
         '    qry(8).Execute
         End If
    

    'Pasamos al siguiente responsable.
    Next objREco
    
    'Se ha grabado todo cerramos la transacci�n
    objApp.CommitTrans
Exit Sub

'**************************************************************
' GRABAR LA LINEA
'**************************************************************
grabarlinea:
    'Comprobamos si es del responsable
    If Linea.KeyREco = objREco.Key Then
        If Facturada = False Then   'A�adiremos la factura

            '**********************************************************
            '*        Calcular el n�mero de factura a grabar          *
            '**********************************************************
            If Me.NumFactAnt <> "" And Me.FactAntigua = False Then
            
                NumFactura = ProxNumFact(TipoFact)
                qryCabecera.rdoParameters(1) = NumFactura
                
                NoFactura = Me.NumFactAnt
                
            ElseIf Trim(NoFactura) <> "" Then
               If IsNumeric(NoFactura) Then
                  NoFactura = "13/" & NoFactura
               End If

                NumFactura = ProxNumFact(TipoFact)
                qryCabecera.rdoParameters(1) = NumFactura

'                NoFactura = NoFactura
'
'                txtFactura.Text = ""
            Else
                NumFactura = ProxNumFact(TipoFact)
                qryCabecera.rdoParameters(1) = NumFactura
                If Me.FactAntigua = True Then
                    'Ponemos a la factura antigua el n�mero de la nueva
                    qryFactAnt.rdoParameters(0) = NumFactura
                    qryFactAnt.rdoParameters(1) = Me.NumFactAnt
                End If

                NoFactura = NumFactura
            End If
            
            objREco.NFactura = NumFactura
        
            '**********************************************************
            '*                 Grabar la cabecera                     *
            '**********************************************************
            qryCabecera.rdoParameters(2) = objREco.Codigo
            qryCabecera.rdoParameters(3) = objREco.EntidadResponsable
            
            If Trim(objREco.Coletilla) = "" Then ' coletilla
              qryCabecera.rdoParameters(4) = Null
            Else
              qryCabecera.rdoParameters(4) = objREco.Coletilla
            End If
            
            qryCabecera.rdoParameters(5) = NumToSql(RedondearMoneda(PropuestaFact.Importe(objREco.Key), tiTotalFact))
            
            qryCabecera.rdoParameters(6) = NumToSql(0 & objREco.Dto) 'Dto aplicado
            
            If Trim(objREco.Observaciones) = "" Then  'Observaciones
              qryCabecera.rdoParameters(8) = Null
            Else
              qryCabecera.rdoParameters(8) = objREco.Observaciones
            End If
            
            
            
            qryCabecera.rdoParameters(9) = ""
            qryCabecera.rdoParameters(17) = ""
            For Each objeto In PropuestaFact.NodosAplicados
               If TypeOf objeto Is Nodo Then
                  If objeto.codConcierto = objREco.Concierto.Concierto Then
                     ' concierto facturado (p.e. Osasunbidea, Privado A, etc)
                     qryCabecera.rdoParameters(9) = IIf(objeto.codConcierto = 0, "", objeto.codConcierto)
                     ' nodo facturado (p.e. Forfait estancias, Acto Medico, etc)
                     qryCabecera.rdoParameters(17) = IIf(objeto.Codigo = 0, "", objeto.Codigo)
                     Exit For
                  End If
               End If
            Next objeto
            
                               


            qryCabecera.rdoParameters(11) = objREco.CodTipEcon
            qryCabecera.rdoParameters(12) = IIf(FechaFact <> "", FechaFact, Null)
        
            qryCabecera.rdoParameters(14) = Format(Me.FecInicioFact, "dd/MM/YYYY")
            qryCabecera.rdoParameters(15) = Format(Me.FecFinFact, "dd/MM/YYYY")
            
            ' si la factura es de importe 0 se marca automaticamente como compensada
            If PropuestaFact.Importe(objREco.Key) = 0 Then
               qryCabecera.rdoParameters(16) = 2
            Else
               qryCabecera.rdoParameters(16) = 0
            End If
            
            ' codigo de la factura que origina la garantia
            qryCabecera.rdoParameters(18) = Null & Me.CodFactGarantia
            
            ' fecha de fin de la garantia solo se graba en la factura que la genera
            ' el numero de dias acumulados solo se graba en la factura que genera la garantia por estancias
            qryCabecera.rdoParameters(19) = Null
            qryCabecera.rdoParameters(20) = Null
            
            
            
            ' ESTO ES UN PARCHE PARA PODER FACTURAR JUNTOS LOS
            ' PROCESO-ASISTENCIA QUE SE HAN METIDO PARTIDOS POR ERROR
            ' En la cabecera de fact que contiene el importe se pone que
            ' se muestre en gesti�n de cobros (FA03INDNMGC = NULL)
            qryCabecera.rdoParameters(13) = Null
            ' -----------------------------
            
            For Each ProcesoAsistencia In Me.ProcAsist
                CodFactura = fNextClave("FA04CODFACT", "FA0400")
                qryCabecera.rdoParameters(0) = CodFactura 'Secuencia
                ' recoger el codfactura de esta primera parte de la factura que es la que
                ' contiene el importe y pasarla como parametro a la sql que graba los rnfs
                If IsNull(qryCabecera.rdoParameters(13)) Then
'                    qryGarantia.rdoParameters(2) = CodFactura 'C�digo de la factura
                    qryGarantia.rdoParameters(2) = NumFactura 'n�mero de la factura

                    qryLinea.rdoParameters(0) = CodFactura 'C�digo de la factura
                    qryRNF.rdoParameters(0) = CodFactura 'C�digo de la factura
                    qryPropuestas.rdoParameters(0) = CodFactura
                    
                End If
                '
                qryCabecera.rdoParameters(10) = ProcesoAsistencia.proceso
                qryCabecera.rdoParameters(7) = ProcesoAsistencia.Asistencia 'N�mero de la asistencia
                
                qryCabecera.Execute
                
                ' solo hay que grabar el importe, dto en la 1� linea de cabecera
                qryCabecera.rdoParameters(5) = 0
                qryCabecera.rdoParameters(6) = 0
                
                If IsNull(qryCabecera.rdoParameters(13)) Then
                  objREco.CodFactura = CodFactura
                End If
                
                qryCabecera.rdoParameters(13) = 1
            Next ProcesoAsistencia
        
            Facturada = True
        End If
        
        '**************************************
        '      Grabar la linea de factura
        '**************************************
        If Linea.FechaFinGarantia <> "" Then
'            qryCabecera.rdoParameters(18) = "" & Me.CodFactGarantia
            ' grabar las garantias si las hay
            qryGarantia.rdoParameters(0) = Linea.FechaFinGarantia 'C�digo de la factura
            
            If Linea.nDiasAcumulGaran > 0 Then
               qryGarantia.rdoParameters(1) = Linea.nDiasAcumulGaran
            End If
               
            qryGarantia.rdoParameters(3) = Linea.RNFs(1).CodProceso
            qryGarantia.Execute
            
'            MsgBox "Hay una garantia"
        End If
        
        qryLinea.rdoParameters(1) = IntContLineas 'N� de L�nea
        If Not IsNull(Linea.Codigo) Then
          qryLinea.rdoParameters(2) = Linea.Codigo 'C�digo de la categor�a (si tiene)
        Else
          qryLinea.rdoParameters(2) = Null
        End If
        If Linea.Descripcion <> "" Then
          qryLinea.rdoParameters(3) = Linea.Descripcion
        Else
          qryLinea.rdoParameters(3) = Linea.Name 'Descripci�n
        End If
        qryLinea.rdoParameters(4) = Null '?????????????????????????????
        Dto = 0 + Linea.LineaFact.Dto + Linea.LineaFact.DescuentoManual
        qryLinea.rdoParameters(5) = NumToSql(RedondearMoneda(Linea.LineaFact.Precio + Linea.LineaFact.PrecioNoDescontable, tiImporteLinea))
        qryLinea.rdoParameters(6) = NumToSql(Dto) 'Dto de la l�nea incluido redondeo
        qryLinea.rdoParameters(7) = NumToSql(RedondearMoneda(Linea.LineaFact.Total + Linea.LineaFact.TotalNoDescontable, tiImporteLinea))
        qryLinea.rdoParameters(8) = Linea.OrdImp
        
        ' si no hay franquicia se graban los datos realativos a ella como nulos
        If Linea.LimiteFranquicia <> -1 Then
            qryLinea.rdoParameters(9) = NumToSql(RedondearMoneda(Linea.LimiteFranquicia, tiImporteLinea))
            qryLinea.rdoParameters(10) = NumToSql(RedondearMoneda(Linea.ImporteAntesFranquicia, tiImporteLinea))
        Else
            qryLinea.rdoParameters(9) = Null
            qryLinea.rdoParameters(10) = Null
        End If
        qryLinea.rdoParameters(11) = IIf(Linea.CodAtrib = 0, Null, Linea.CodAtrib)
'        qryLinea.rdoParameters(11) = Linea.CodAtrib
        
        qryLinea.Execute
      
        '**************************************
        ' Actualizar los RNFs
        '**************************************
        For Each objRNF In Linea.RNFs
            'Asignamos a los rnfs de la FA0300, el n�mero de la factura y la l�nea
            qryRNF.rdoParameters(1) = IntContLineas
            If objRNF.Desglose = True Then
                qryRNF.rdoParameters(2) = 1
            Else
                qryRNF.rdoParameters(2) = 0
            End If
            qryRNF.rdoParameters(3) = objRNF.CodAtrib
            
            qryRNF.rdoParameters(4) = NumToSql(objRNF.Precio)
            qryRNF.rdoParameters(5) = NumToSql(objRNF.Cantidad)
            qryRNF.rdoParameters(6) = objRNF.Name
            qryRNF.rdoParameters(7) = IIf(objRNF.PeriodoGara > 0, objRNF.PeriodoGara, Null)
            qryRNF.rdoParameters(8) = objRNF.CodRNF
            
            qryRNF.Execute
        Next objRNF
        
        IntContLineas = IntContLineas + 1
    End If
Return




'**************************************************************
' CONTROL DE ERRORES
'**************************************************************
ErrorenGrabacion:
   'Se ha producido un error en la grabaci�n
'   If MostrarMensajes Then
'      Write #1, "Se ha producido un error en la grabaci�n:" & Historia
      respuesta = MsgBox("Se ha producido un error en la grabaci�n:" & Historia & Chr(10) & Chr(10) & Err.Description, vbRetryCancel + vbCritical, "Atenci�n")
'   Else

'      respuesta = vbAbort
'   End If
   
   If respuesta = vbRetry Then
      Resume
   Else
      objApp.RollbackTrans
   End If
End Sub

Private Sub IniciarQRY()
   Dim MiSqL As String
  
   'Insertar una cabecera de factura en FA0400
   MiSqL = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
               "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV," & _
               "FA09CODNODOCONC,AD07CODPROCESO,CI32CODTIPECON,FA04FECFACTURA, FA04INDNMGC, FA04FECDESDE, FA04FECHASTA, FA04INDCOMPENSA, " & _
               "FA09CODNODOCONC_FACT, FA04CODFACT_GARAN, FA04FECFINGARANTIA, FA04NUMACUMULGARAN ) " & _
               "Values (?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?,?,?,TO_DATE(?,'DD/MM/YYYY'),?)"
   qryCabecera.SQL = MiSqL
  'Activamos las querys.
   Set qryCabecera.ActiveConnection = objApp.rdoConnect
   qryCabecera.Prepared = True
  
  'Asignar a FA0400 los datos de la garantia
'   MiSqL = "Update FA0400 " & _
            " set FA04FECFINGARANTIA = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
            "     FA04NUMACUMULGARAN = ? " & _
            " where FA04CODFACT = ?"
   MiSqL = "Update FA0400 " & _
            " set FA04FECFINGARANTIA = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
            "     FA04NUMACUMULGARAN = ? " & _
            " where FA04NUMFACT = ? " & _
            "   AND AD07CODPROCESO = ? " & _
            "   AND FA04NUMFACREAL IS NULL " & _
            " "

   qryGarantia.SQL = MiSqL
  'Activamos las querys.
   Set qryGarantia.ActiveConnection = objApp.rdoConnect
   qryGarantia.Prepared = True
  
  
  'Insertar una l�nea de factura en FA1600
   MiSqL = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
               "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE,FA16ORDFACT, FA16FRANQUICIA," & _
               "FA16CANTSINFRAN, FA15CODATRIB) " & _
               "Values (?,?,?,?,?,?,?,?,?,?,?,?)"
   qryLinea.SQL = MiSqL
  'Activamos las querys.
   Set qryLinea.ActiveConnection = objApp.rdoConnect
   qryLinea.Prepared = True
  
  
  
  
  'Asignar a FA0300 el codigo de la factura y el n�mero de l�nea
   MiSqL = "Update FA0300 " & _
                 " set FA04NUMFACT = ?, FA16NUMLINEA = ?, FA03INDDESGLOSE = ?, FA15CODATRIB = ?, " & _
                 " FA03PRECIOFACT = ?, FA03CANTFACT = ?, FA03OBSIMP = ?, FA03PERIODOGARA = ? " & _
                 " Where FA03NUMRNF = ?"
   qryRNF.SQL = MiSqL
  'Activamos las querys.
   Set qryRNF.ActiveConnection = objApp.rdoConnect
   qryRNF.Prepared = True
   
   
   ' grabar las diferentes propuestas de factura (para los listados de INSALUD)
   MiSqL = "Insert Into FA3600 (FA04CODFACT, FA36NUMORDEN, FA36DESIG, FA36IMPORTE, FA36NUMPROP, FA36INDFACT) " & _
                 " Values (?,?,?,?,FA36NUMPROP_SEQUENCE.NEXTVAL,?)"
   qryPropuestas.SQL = MiSqL
  'Activamos las querys.
   Set qryPropuestas.ActiveConnection = objApp.rdoConnect
   qryPropuestas.Prepared = True
   

   
   
   'Asignamos al FA04NUMFACTREAL de la antigua el n�mero de factura creado
   MiSqL = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04NUMFACT = ?"
   qryFactAnt.SQL = MiSqL
  'Activamos las querys.
   Set qryFactAnt.ActiveConnection = objApp.rdoConnect
   qryFactAnt.Prepared = True

  
End Sub

Private Function ProxNumFact(Optional TipoFact As String = "P") As String
    ProxNumFact = Trim(TipoFact) & "/" & fNextClave("FA04CODFACT", "FA0400")
    
End Function


