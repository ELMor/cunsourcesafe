Attribute VB_Name = "modConstants"
Option Explicit

Global Const constRNFPendiente = 0 ' Pendiente de facturar
Global Const constRNFManual = 1    ' Introducción o modificación manual
Global Const constRNFFacturado = 2 ' Facturado
Global Const constRNFAnulado = 3   ' Se ha compensado (Anulado) con otra factura
Global Const constRNFCompensa = 4  ' Compensa otra factura

