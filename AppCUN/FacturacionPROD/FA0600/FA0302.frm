VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFacturaOsasunbidea 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Diskette de Osasunbidea"
   ClientHeight    =   4770
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10275
   ForeColor       =   &H8000000F&
   Icon            =   "FA0302.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4770
   ScaleWidth      =   10275
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   8940
      TabIndex        =   20
      Top             =   660
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de las facturas"
      Height          =   1215
      Left            =   120
      TabIndex        =   18
      Top             =   0
      Width           =   2415
      Begin SSCalendarWidgets_A.SSDateCombo ssdcFechaFactura 
         Height          =   375
         Left            =   120
         TabIndex        =   19
         Top             =   480
         Width           =   1815
         _Version        =   65537
         _ExtentX        =   3201
         _ExtentY        =   661
         _StockProps     =   93
         ForeColor       =   -2147483630
         AllowEdit       =   0   'False
      End
   End
   Begin TabDlg.SSTab sstDisk 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   10035
      _ExtentX        =   17701
      _ExtentY        =   5953
      _Version        =   327681
      Style           =   1
      Tabs            =   5
      Tab             =   3
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Creaci�n Ficheros"
      TabPicture(0)   =   "FA0302.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdExportar"
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(2)=   "cmdEjecutar"
      Tab(0).Control(3)=   "txtMsg"
      Tab(0).Control(4)=   "Line4"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Asignaci�n de N�meros de Factura"
      TabPicture(1)   =   "FA0302.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtMsg1"
      Tab(1).Control(1)=   "txtDatos(5)"
      Tab(1).Control(2)=   "txtDatos(4)"
      Tab(1).Control(3)=   "txtDatos(3)"
      Tab(1).Control(4)=   "txtDatos(2)"
      Tab(1).Control(5)=   "txtDatos(1)"
      Tab(1).Control(6)=   "txtDatos(0)"
      Tab(1).Control(7)=   "cmdAsignar"
      Tab(1).Control(8)=   "chkNumFactAmbu"
      Tab(1).Control(9)=   "chkNumFactHospi"
      Tab(1).Control(10)=   "Line1"
      Tab(1).Control(11)=   "Label2"
      Tab(1).Control(12)=   "Label1(1)"
      Tab(1).Control(13)=   "Label1(0)"
      Tab(1).ControlCount=   14
      TabCaption(2)   =   "Asignaci�n de fecha a Facturas del Mes"
      TabPicture(2)   =   "FA0302.frx":047A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Line2"
      Tab(2).Control(1)=   "Label3"
      Tab(2).Control(2)=   "Label1(2)"
      Tab(2).Control(3)=   "Label1(3)"
      Tab(2).Control(4)=   "txtMsg2"
      Tab(2).Control(5)=   "txtDatos1(5)"
      Tab(2).Control(6)=   "txtDatos1(4)"
      Tab(2).Control(7)=   "txtDatos1(3)"
      Tab(2).Control(8)=   "txtDatos1(2)"
      Tab(2).Control(9)=   "txtDatos1(1)"
      Tab(2).Control(10)=   "txtDatos1(0)"
      Tab(2).Control(11)=   "cmdAsigFecha"
      Tab(2).Control(12)=   "chkFechaFactAmbu"
      Tab(2).Control(13)=   "chkFechaFactHospi"
      Tab(2).ControlCount=   14
      TabCaption(3)   =   "Informes"
      TabPicture(3)   =   "FA0302.frx":0496
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "Frame3"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "Frame4"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Inserci�n de Registros"
      TabPicture(4)   =   "FA0302.frx":04B2
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "chkInsertRehabil"
      Tab(4).Control(1)=   "chkInsertHospi"
      Tab(4).Control(2)=   "chkInsertAmbu"
      Tab(4).Control(3)=   "cmdInsertar"
      Tab(4).Control(4)=   "txtMsg3"
      Tab(4).ControlCount=   5
      Begin VB.CheckBox chkInsertRehabil 
         Caption         =   "Hemodi�lisis y Rehabilitaci�n"
         Height          =   255
         Left            =   -74820
         TabIndex        =   62
         Top             =   1800
         Width           =   2715
      End
      Begin VB.CheckBox chkInsertHospi 
         Caption         =   "Hospitalizados"
         Height          =   255
         Left            =   -74820
         TabIndex        =   61
         Top             =   600
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CheckBox chkInsertAmbu 
         Caption         =   "Ambulatorios"
         Height          =   255
         Left            =   -74820
         TabIndex        =   60
         Top             =   1200
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CommandButton cmdInsertar 
         Caption         =   "Insertar"
         Height          =   495
         Left            =   -66300
         TabIndex        =   59
         Top             =   2700
         Width           =   1215
      End
      Begin VB.TextBox txtMsg3 
         BackColor       =   &H80000003&
         Height          =   2655
         Left            =   -71760
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   58
         Top             =   540
         Width           =   5355
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar"
         Height          =   495
         Left            =   -66300
         TabIndex        =   56
         Top             =   2700
         Width           =   1215
      End
      Begin VB.Frame Frame4 
         Caption         =   "Listados a Enviar"
         Height          =   2655
         Left            =   6180
         TabIndex        =   41
         Top             =   480
         Width           =   3735
         Begin VB.CommandButton cmdTarifas 
            Caption         =   "Tarifas Pr. No Concertadas"
            Height          =   495
            Left            =   2100
            TabIndex        =   45
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdNoConcert 
            Caption         =   "Pruebas No Concertadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2100
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   420
            Width           =   1215
         End
         Begin VB.CommandButton cmdMedicacion 
            Caption         =   "Medicaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdResumen 
            Caption         =   "Resumen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   420
            UseMaskColor    =   -1  'True
            Width           =   1215
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Comprobaci�n"
         Height          =   2655
         Left            =   120
         TabIndex        =   38
         Top             =   480
         Width           =   6015
         Begin VB.CommandButton cmdActoMedico 
            Caption         =   "Ambulatorios Acto M�dico"
            Height          =   495
            Left            =   240
            TabIndex        =   63
            Top             =   2040
            Width           =   1215
         End
         Begin VB.CommandButton cmdProtesis 
            Caption         =   "Pr�tesis"
            Height          =   495
            Left            =   3180
            TabIndex        =   57
            Top             =   300
            Width           =   1215
         End
         Begin VB.CommandButton cmdGarantias 
            Caption         =   "Garant�as"
            Height          =   495
            Left            =   1740
            TabIndex        =   51
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdPac 
            Caption         =   "Listado Pac. Ambulatorios"
            Height          =   495
            Left            =   1740
            TabIndex        =   50
            Top             =   300
            Width           =   1215
         End
         Begin VB.CommandButton cmdRadioterapia 
            Caption         =   "Radioterapia"
            Height          =   495
            Left            =   4620
            TabIndex        =   49
            Top             =   2040
            Width           =   1215
         End
         Begin VB.CommandButton cmdHemodialisis 
            Caption         =   "Hemodi�lisis"
            Height          =   495
            Left            =   4620
            TabIndex        =   48
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdRehabilitacion 
            Caption         =   "Rehabilitaci�n"
            Height          =   495
            Left            =   4620
            TabIndex        =   47
            Top             =   300
            Width           =   1215
         End
         Begin VB.CommandButton cmdSinFact 
            Caption         =   "Proc-Asist. Sin Facturar"
            Height          =   495
            Left            =   1740
            TabIndex        =   46
            Top             =   2040
            Width           =   1215
         End
         Begin VB.CommandButton cmdAmbulatorios 
            Caption         =   "Ambulatorios"
            Height          =   495
            Left            =   240
            TabIndex        =   40
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdHospitalizados 
            Caption         =   "Hospitalizados"
            Height          =   495
            Left            =   240
            TabIndex        =   39
            Top             =   300
            Width           =   1215
         End
      End
      Begin VB.CheckBox chkFechaFactHospi 
         Caption         =   "Hospitalizados"
         Height          =   255
         Left            =   -74580
         TabIndex        =   34
         Top             =   1260
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CheckBox chkFechaFactAmbu 
         Caption         =   "Ambulatorios"
         Height          =   255
         Left            =   -74580
         TabIndex        =   33
         Top             =   1860
         Value           =   1  'Checked
         Width           =   1275
      End
      Begin VB.CommandButton cmdAsigFecha 
         Caption         =   "Asignar"
         Enabled         =   0   'False
         Height          =   495
         Left            =   -66300
         TabIndex        =   32
         Top             =   2700
         Width           =   1215
      End
      Begin VB.TextBox txtDatos1 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   0
         Left            =   -73200
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   1260
         Width           =   1215
      End
      Begin VB.TextBox txtDatos1 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   1
         Left            =   -73200
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   1860
         Width           =   1215
      End
      Begin VB.TextBox txtDatos1 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   2
         Left            =   -71940
         Locked          =   -1  'True
         TabIndex        =   29
         Top             =   1260
         Width           =   1455
      End
      Begin VB.TextBox txtDatos1 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   3
         Left            =   -71940
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   1860
         Width           =   1455
      End
      Begin VB.TextBox txtDatos1 
         BackColor       =   &H80000003&
         ForeColor       =   &H00C00000&
         Height          =   285
         Index           =   4
         Left            =   -71940
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   2460
         Width           =   1455
      End
      Begin VB.TextBox txtDatos1 
         BackColor       =   &H80000003&
         ForeColor       =   &H00C00000&
         Height          =   285
         Index           =   5
         Left            =   -73200
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   2460
         Width           =   1215
      End
      Begin VB.TextBox txtMsg2 
         BackColor       =   &H80000003&
         Height          =   2655
         Left            =   -70260
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   25
         Top             =   540
         Width           =   3855
      End
      Begin VB.TextBox txtMsg1 
         BackColor       =   &H80000003&
         Height          =   2655
         Left            =   -70260
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   24
         Top             =   540
         Width           =   3855
      End
      Begin VB.TextBox txtDatos 
         BackColor       =   &H80000003&
         ForeColor       =   &H00C00000&
         Height          =   285
         Index           =   5
         Left            =   -73200
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   2460
         Width           =   1215
      End
      Begin VB.TextBox txtDatos 
         BackColor       =   &H80000003&
         ForeColor       =   &H00C00000&
         Height          =   285
         Index           =   4
         Left            =   -71940
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   2460
         Width           =   1455
      End
      Begin VB.TextBox txtDatos 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   3
         Left            =   -71940
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   1860
         Width           =   1455
      End
      Begin VB.TextBox txtDatos 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   2
         Left            =   -71940
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1260
         Width           =   1455
      End
      Begin VB.TextBox txtDatos 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   1
         Left            =   -73200
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   1860
         Width           =   1215
      End
      Begin VB.TextBox txtDatos 
         BackColor       =   &H80000003&
         Height          =   285
         Index           =   0
         Left            =   -73200
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   1260
         Width           =   1215
      End
      Begin VB.CommandButton cmdAsignar 
         Caption         =   "Asignar"
         Height          =   495
         Left            =   -66300
         TabIndex        =   11
         Top             =   2700
         Width           =   1215
      End
      Begin VB.CheckBox chkNumFactAmbu 
         Caption         =   "Ambulatorios"
         Height          =   255
         Left            =   -74580
         TabIndex        =   10
         Top             =   1860
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CheckBox chkNumFactHospi 
         Caption         =   "Hospitalizados"
         Height          =   255
         Left            =   -74580
         TabIndex        =   9
         Top             =   1260
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.Frame Frame2 
         Caption         =   "Opciones"
         Height          =   2775
         Left            =   -74880
         TabIndex        =   3
         Top             =   420
         Width           =   3015
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Exportar Fichero Hospitalizados"
            Height          =   255
            Index           =   8
            Left            =   120
            TabIndex        =   55
            Top             =   2460
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Exportar Fichero Conceptos"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   54
            Top             =   2220
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Exportar Fichero Ambulatorios"
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   53
            Top             =   1980
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Exportar Fichero Hospitalizados"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   52
            Top             =   1740
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "A�adir Diagn�sticos a Conceptos"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   8
            Top             =   960
            Value           =   1  'Checked
            Width           =   2775
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Crear Fichero de Diagn�sticos"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   7
            Top             =   1200
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Crear Fichero Conceptos"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   6
            Top             =   720
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Crear Fichero Ambulatorios"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   5
            Top             =   480
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.CheckBox chkOpciones 
            Caption         =   "Crear Fichero Hospitalizados"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.Line Line3 
            X1              =   60
            X2              =   2940
            Y1              =   1620
            Y2              =   1620
         End
      End
      Begin VB.CommandButton cmdEjecutar 
         Caption         =   "Crear"
         Height          =   495
         Left            =   -66300
         TabIndex        =   2
         Top             =   1500
         Width           =   1215
      End
      Begin VB.TextBox txtMsg 
         BackColor       =   &H80000003&
         Height          =   2655
         Left            =   -71760
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   540
         Width           =   5355
      End
      Begin VB.Line Line4 
         X1              =   -66360
         X2              =   -65040
         Y1              =   2040
         Y2              =   2040
      End
      Begin VB.Label Label1 
         Caption         =   "N� Facturas"
         Height          =   255
         Index           =   3
         Left            =   -73080
         TabIndex        =   37
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Importe"
         Height          =   255
         Index           =   2
         Left            =   -71640
         TabIndex        =   36
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "TOTAL"
         Height          =   255
         Left            =   -74580
         TabIndex        =   35
         Top             =   2520
         Width           =   1275
      End
      Begin VB.Line Line2 
         BorderWidth     =   3
         X1              =   -73320
         X2              =   -70380
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line Line1 
         BorderWidth     =   3
         X1              =   -73320
         X2              =   -70380
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Label Label2 
         Caption         =   "TOTAL"
         Height          =   255
         Left            =   -74580
         TabIndex        =   21
         Top             =   2520
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Importe"
         Height          =   255
         Index           =   1
         Left            =   -71640
         TabIndex        =   17
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "N� Facturas"
         Height          =   255
         Index           =   0
         Left            =   -73080
         TabIndex        =   16
         Top             =   900
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmFacturaOsasunbidea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim crReport As CrystalReport

Private Sub pLimpiarReportFormulas()
    Dim i As Integer
    
    On Error Resume Next
    For i = 0 To 5
        crReport.Formulas(i) = ""
        If Err <> 0 Then Exit For
    Next i

End Sub

Private Sub cmdActoMedico_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0104.rpt"
    crReport.WindowTitle = "Facturas Pacientes Ambulatorios"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0422J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")" _
                                & " and {FA0422J.FA09CODNODOCONC_FACT} = 328 "
                                '& " AND {FA0419J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")"
                                
    crReport.Formulas(0) = "TITULO = 'FACTURAS AMBULATORIAS POR ACTO M�DICO DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdAmbulatorios_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0104.rpt"
    crReport.WindowTitle = "Facturas Pacientes Ambulatorios"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0422J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")" _
                                '& " AND {FA0419J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")"
                                
    crReport.Formulas(0) = "TITULO = 'FACTURAS AMBULATORIAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdAsigFecha_Click()
    Dim strErrorMsg As String
    Dim blnRes As Boolean
    If IsDate(ssdcFechaFactura.Text) Then
        txtMsg2.Text = ""
        Call pMensaje("Comienzo ejecucion: " & Now, txtMsg2)
        blnRes = fAsignarFechaFactura(ssdcFechaFactura.Text, _
                                IIf(chkFechaFactHospi.Value = 1, True, False), _
                                IIf(chkFechaFactAmbu.Value = 1, True, False), _
                                strErrorMsg, _
                                txtMsg2)
                                
        Call pMensaje("Fin ejecucion: " & Now, txtMsg2)
        Beep
        Beep
        If blnRes Then
            MsgBox "Operaci�n Finalizada Correctamente", vbInformation, "Fin de Ejecuci�n"
        Else
        MsgBox "Operaci�n Abortada!!!!!!!!" & Chr$(13) & Chr$(10) & strErrorMsg, _
                vbInformation, "Fin de Ejecuci�n"
        End If
                                
    End If

End Sub

Private Sub cmdAsignar_Click()
    Dim strErrorMsg As String
    Dim blnRes As Boolean
    If IsDate(ssdcFechaFactura.Text) Then
        txtMsg1.Text = ""
        Call pMensaje("Comienzo ejecucion: " & Now, txtMsg1)
        blnRes = fAsignarNumFactura(ssdcFechaFactura.Text, _
                                IIf(chkNumFactHospi.Value = 1, True, False), _
                                IIf(chkNumFactAmbu.Value = 1, True, False), _
                                strErrorMsg, _
                                txtMsg1)
                                
        Call pMensaje("Fin ejecucion: " & Now, txtMsg1)
        Beep
        Beep
        If blnRes Then
            MsgBox "Operaci�n Finalizada Correctamente", vbInformation, "Fin de Ejecuci�n"
        Else
        MsgBox "Operaci�n Abortada!!!!!!!!" & Chr$(13) & Chr$(10) & strErrorMsg, _
                vbInformation, "Fin de Ejecuci�n"
        End If
                                
    End If
End Sub

Private Sub cmdEjecutar_Click()
    Dim strErrorMsg As String
    Dim blnRes As Boolean

    Screen.MousePointer = vbHourglass
    txtMsg.Text = ""
    Call pMensaje("Comienzo ejecucion: " & Now, txtMsg)
    
    objApp.rdoConnect.BeginTrans
    blnRes = fEjecutarFichOsasun(Format$(ssdcFechaFactura, "dd/mm/yyyy"), _
                                  IIf(chkOpciones(constCREAPACHOSPI) = 1, True, False), _
                                  IIf(chkOpciones(constCREAPACAMBU) = 1, True, False), _
                                  IIf(chkOpciones(constCREACONCEPT) = 1, True, False), _
                                  IIf(chkOpciones(constA�ADEDIAG) = 1, True, False), _
                                  IIf(chkOpciones(constCREADIAG) = 1, True, False), _
                                  strErrorMsg, _
                                  txtMsg _
                                )
    Beep
    Beep
    
    Call pMensaje("Fin ejecucion: " & Now, txtMsg)
    
    If blnRes Then
        objApp.rdoConnect.CommitTrans
        MsgBox "Operaci�n Finalizada Correctamente", vbInformation, "Fin de Ejecuci�n"
    Else
        objApp.rdoConnect.RollbackTrans
        MsgBox "Operaci�n Abortada!!!!!!!!" & Chr$(13) & Chr$(10) & strErrorMsg, _
                vbInformation, "Fin de Ejecuci�n"
    End If

    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdExportar_Click()
    Dim strErrorMsg As String
    Dim blnRes As Boolean

    Screen.MousePointer = vbHourglass
    txtMsg.Text = ""
    Call pMensaje("Comienzo ejecucion: " & Now, txtMsg)
    
    objApp.rdoConnect.BeginTrans
    blnRes = fExportFichOsasun(Format$(ssdcFechaFactura, "dd/mm/yyyy"), _
                                  IIf(chkOpciones(constEXPORTHOSPI) = 1, True, False), _
                                  IIf(chkOpciones(constEXPORTAMBU) = 1, True, False), _
                                  IIf(chkOpciones(constEXPORTCONCEPT) = 1, True, False), _
                                  IIf(chkOpciones(constEXPORTDIAG) = 1, True, False), _
                                  strErrorMsg, _
                                  txtMsg _
                                )
    Beep
    Beep
    
    Call pMensaje("Fin ejecucion: " & Now, txtMsg)
    
    If blnRes Then
        objApp.rdoConnect.CommitTrans
        MsgBox "Operaci�n Finalizada Correctamente", vbInformation, "Fin de Ejecuci�n"
    Else
        objApp.rdoConnect.RollbackTrans
        MsgBox "Operaci�n Abortada!!!!!!!!" & Chr$(13) & Chr$(10) & strErrorMsg, _
                vbInformation, "Fin de Ejecuci�n"
    End If

    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdGarantias_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    

    crReport.ReportFileName = rptPath & "\FA0107.rpt"
    crReport.WindowTitle = "Garant�as Pacientes Ambulatorios"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0421J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")"
    'crReport.SelectionFormula = "{FA0421J.FA04FECFACTURA}='" & Format(ssdcFechaFactura.Text, "dd/mm/yyyy") & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdHemodialisis_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0108.rpt"
    crReport.WindowTitle = "Hemodi�lisis"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA0306J.DPTOREA} = 212 " _
                                & " AND {FA0306J.DPTORESP}=212 " _
                                & " AND {FA0306J.PR01CODACTUACION}=3346 " _
                                & " AND {FA0306J.PR04FECFINACT}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,1") & ") " _
                                & " AND {FA0306J.PR04FECFINACT}< DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,1") & ")"
    
                    
    crReport.Formulas(0) = "TITULO = 'HEMODI�LISIS OSASUNBIDEA " & Format(ssdcFechaFactura.Text, "mm/yyyy") & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdHospitalizados_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0103.rpt"
    crReport.WindowTitle = "Facturas Pacientes Hospitalizados"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0414J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")" _
                                ' & " AND {FA0419J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")"
    crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdInsertar_Click()
    Dim strErrorMsg As String
    Dim blnRes As Boolean

    Screen.MousePointer = vbHourglass
    txtMsg.Text = ""
    Call pMensaje("Comienzo ejecucion: " & Now, txtMsg3)
    
    objApp.rdoConnect.BeginTrans
    blnRes = fInsertarReg(Format$(ssdcFechaFactura, "dd/mm/yyyy"), _
                                  IIf(chkInsertHospi.Value = 1, True, False), _
                                  IIf(chkInsertAmbu.Value = 1, True, False), _
                                  IIf(chkInsertRehabil.Value = 1, True, False), _
                                  strErrorMsg, _
                                  txtMsg3 _
                                )
    Beep
    Beep
    
    Call pMensaje("Fin ejecucion: " & Now, txtMsg3)
    
    If blnRes Then
        objApp.rdoConnect.CommitTrans
        MsgBox "Operaci�n Finalizada Correctamente", vbInformation, "Fin de Ejecuci�n"
    Else
        objApp.rdoConnect.RollbackTrans
        MsgBox "Operaci�n Abortada!!!!!!!!" & Chr$(13) & Chr$(10) & strErrorMsg, _
                vbInformation, "Fin de Ejecuci�n"
    End If

    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdMedicacion_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0302.rpt"
    crReport.WindowTitle = "Medicaci�n de Franquicia"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA5201J.FA52FECPROCESO}='" & Format(ssdcFechaFactura.Text, "yyyymm") & "'"
    
    crReport.Formulas(0) = "TITULO = '" & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdNoConcert_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0303.rpt"
    crReport.WindowTitle = "Pruebas No Concertadas"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA5300.FA52FECPROCESO}='" & Format(ssdcFechaFactura.Text, "yyyymm") & "'"
    
    crReport.Formulas(0) = "TITULO = '" & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdPac_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0105.rpt"
    crReport.WindowTitle = "Pacientes Ambulatorios con Factura"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0422J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")" _
                                & " AND {FA0419J.FA04FECFACTURA}=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,dd") & ")"
    crReport.Action = 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdProtesis_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0116.rpt"
    crReport.WindowTitle = "Pr�tesis"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA1503J.CI32CODTIPECON} = 'S' " _
                                & " AND {FA1503J.CI13CODENTIDAD}='NA' " _
                                & " AND {FA1503J.FR65FECHA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,1") & ") " _
                                & " AND {FA1503J.FR65FECHA}< DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,1") & ")"
    
    
    crReport.Formulas(0) = "TITULO = 'PR�TESIS OSASUNBIDEA " & Format(ssdcFechaFactura.Text, "mm/yyyy") & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdRadioterapia_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0108.rpt"
    crReport.WindowTitle = "Radioterapia"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA0306J.DPTOREA} = 210 " _
                                & " AND {FA0306J.DPTORESP}=210 " _
                                & "AND {FA0306J.PR04FECFINACT}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,1") & ") " _
                                & "AND {FA0306J.PR04FECFINACT}< DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,1") & ")"
    
    crReport.Formulas(0) = "TITULO = 'RADIOTERAPIA OSASUNBIDEA " & Format(ssdcFechaFactura.Text, "mm/yyyy") & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdRehabilitacion_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0108.rpt"
    crReport.WindowTitle = "Rehabilitaci�n"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA0306J.DPTOREA} = 211 " _
                                & " AND {FA0306J.DPTORESP}=211 " _
                                & " AND {FA0306J.PR04FECFINACT}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,1") & ") " _
                                & " AND {FA0306J.PR04FECFINACT}< DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,1") & ")"
    
    
    crReport.Formulas(0) = "TITULO = 'REHABILITACI�N OSASUNBIDEA " & Format(ssdcFechaFactura.Text, "mm/yyyy") & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdResumen_Click()
    Dim SQL As String
    Dim qryDatos As rdoQuery
    Dim rsDatos As rdoResultset
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
        
    SQL = "SELECT   FA52TIPOASISTENCIA," _
    & "             FA52FECPROCESO, " _
    & "             FA04NUMFACT, " _
    & "             IMPORTE, " _
    & "             FA04FECFACTURA " _
    & " FROM        FA5201 " _
    & " WHERE       FA52FECPROCESO = ? "
    
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    
    crReport.ReportFileName = rptPath & "\FA0301.rpt"
    crReport.WindowTitle = "Resumen de Factura"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA5200.FA52FECPROCESO}='" & Format(ssdcFechaFactura.Text, "yyyymm") & "'"
    
    crReport.Formulas(0) = "TITULO = '" & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"
    
    qryDatos(0) = Format(ssdcFechaFactura.Text, "yyyymm")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsDatos.EOF
        If i = 0 Then
            i = i + 1
            crReport.Formulas(i) = "FechaFactura = '" & rsDatos!FA04FECFACTURA & "'"
        
        End If
        
        Select Case rsDatos!FA52TIPOASISTENCIA
            Case "HO"
                i = i + 1
                crReport.Formulas(i) = "ImporteHospi = " & rsDatos!Importe
                i = i + 1
                crReport.Formulas(i) = "FacturaHospi = 'REFERENCIA N." & rsDatos!FA04NUMFACT & "'"
            Case "AM"
                i = i + 1
                crReport.Formulas(i) = "ImporteAmbu = " & rsDatos!Importe
                i = i + 1
                crReport.Formulas(i) = "FacturaAmbu = 'REFERENCIA N." & rsDatos!FA04NUMFACT & "'"
        End Select
        
        rsDatos.MoveNext
    Loop
    rsDatos.Close
    qryDatos.Close
    
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdSinFact_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0305.rpt"
    crReport.WindowTitle = "Procesos-Asistencias sin Factura"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0416J.AD01FECFIN}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,1") & ") " _
                                & "AND {FA0416J.AD01FECFIN}< DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,1") & ")"
    
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdTarifas_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0304.rpt"
    crReport.WindowTitle = "Tarifas de Pruebas No Concertadas"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = ""
    crReport.Formulas(0) = "TITULO = '" & UCase(Format(Now, "yyyy")) & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
     rptPath = App.Path
    Set crReport = frmFacturaINSALUD.crReport1
    crReport.WindowBorderStyle = crptFixedDouble
    crReport.WindowState = crptMaximized
    crReport.Connect = objApp.rdoConnect.Connect
End Sub

Private Sub ssdcFechaFactura_CloseUp()
    ' Se actualizan los datos de la factura de ese mes
    Dim strNumFacHospi$, strImporteFacHospi$
    Dim strNumFacAmbu$, strImporteFacAmbu$
    Dim strNumFacTot$, strImporteFacTot$
    
    Me.Refresh
    Screen.MousePointer = vbHourglass
    
    If IsDate(ssdcFechaFactura.Text) Then
        'Call pDatosFacturas(ssdcFechaFactura.Text, _
                        strNumFacHospi, strImporteFacHospi, _
                        strNumFacAmbu, strImporteFacAmbu, _
                        strNumFacTot, strImporteFacTot)
        txtDatos(constTXTNUMFACHOSPI).Text = strNumFacHospi
        txtDatos(constTXTIMPORTEFACHOSPI).Text = strImporteFacHospi
        txtDatos(constTXTNUMFACAMBU).Text = strNumFacAmbu
        txtDatos(constTXTIMPORTEFACAMBU).Text = strImporteFacAmbu
        txtDatos(constTXTNUMFACTOT).Text = strNumFacTot
        txtDatos(constTXTIMPORTEFACTOT).Text = strImporteFacTot
        
        'Call pDatosFacturasMes(ssdcFechaFactura.Text, _
                        strNumFacHospi, strImporteFacHospi, _
                        strNumFacAmbu, strImporteFacAmbu, _
                        strNumFacTot, strImporteFacTot)
                        
        txtDatos1(constTXTNUMFACHOSPI).Text = strNumFacHospi
        txtDatos1(constTXTIMPORTEFACHOSPI).Text = strImporteFacHospi
        txtDatos1(constTXTNUMFACAMBU).Text = strNumFacAmbu
        txtDatos1(constTXTIMPORTEFACAMBU).Text = strImporteFacAmbu
        txtDatos1(constTXTNUMFACTOT).Text = strNumFacTot
        txtDatos1(constTXTIMPORTEFACTOT).Text = strImporteFacTot
    Else
        txtDatos(constTXTNUMFACHOSPI).Text = "0"
        txtDatos(constTXTIMPORTEFACHOSPI).Text = "0"
        txtDatos(constTXTNUMFACAMBU).Text = "0"
        txtDatos(constTXTIMPORTEFACAMBU).Text = "0"
        txtDatos(constTXTNUMFACTOT).Text = "0"
        txtDatos(constTXTIMPORTEFACTOT).Text = "0"
        txtDatos1(constTXTNUMFACHOSPI).Text = "0"
        txtDatos1(constTXTIMPORTEFACHOSPI).Text = "0"
        txtDatos1(constTXTNUMFACAMBU).Text = "0"
        txtDatos1(constTXTIMPORTEFACAMBU).Text = "0"
        txtDatos1(constTXTNUMFACTOT).Text = "0"
        txtDatos1(constTXTIMPORTEFACTOT).Text = "0"
        
    End If
    Screen.MousePointer = vbDefault
End Sub


