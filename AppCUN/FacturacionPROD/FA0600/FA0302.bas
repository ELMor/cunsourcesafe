Attribute VB_Name = "modConstants"
Option Explicit


Global strFilePath As String

Global Const constCREAPACHOSPI = 0
Global Const constCREAPACAMBU = 1
Global Const constCREACONCEPT = 2
Global Const constCREADIAG = 3
Global Const constAŅADEDIAG = 4
Global Const constEXPORTHOSPI = 5
Global Const constEXPORTAMBU = 6
Global Const constEXPORTCONCEPT = 7
Global Const constEXPORTDIAG = 8


Global Const constTXTNUMFACHOSPI = 0
Global Const constTXTIMPORTEFACHOSPI = 2
Global Const constTXTNUMFACAMBU = 1
Global Const constTXTIMPORTEFACAMBU = 3
Global Const constTXTNUMFACTOT = 5
Global Const constTXTIMPORTEFACTOT = 4

Global Const constFarmacia = 1
Global Const constIntervencion = 6
Global Const constPrueba = 7
Global Const constBancoSangre = 9
