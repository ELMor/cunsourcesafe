Attribute VB_Name = "ModImpresionFact"
Option Explicit

Private NuevasFacts As Boolean

Public Const ImporteMinimoDesglose = 0

Private Type CabFact
  NFactura As String
  fecha As Date
  Observac As String
  Descrip As String
  NumCaso As Long
  proceso As Long
  TotFactura As Double
  Dto As Integer
  codConcierto As Integer
  Habitacion As String
  FechaInicio As String
  FechaFinal As String
  SG02COD_ADD As String
  Grabar As Boolean
  NumFactProvisional As String
  Paciente As Persona
  reco As Persona
  DescConcierto As String  ' descripcion del concierto (p.e.: Por su condicion de asegurado de ...)
  numPagInicial As Integer ' contiene el numero de pagina inicial de la factura en impresion
  CodFact As Long
  TipEcon As String
  CodEnt As String
End Type

Private Type LinFact
  NFactura As Long
  NLinea As Integer
  Descripcion As String
  Importe As Double
  mostrar As Boolean
  Precio As Double
End Type

Private Type RnfLinea
  NFactura As Long
  NLinea As Integer
  CodCateg As String
  Cantidad As Double    ' antes integer
  Descripcion As String
  Precio As Double
  Desglose As Boolean
End Type
       
Global ArrayCabecera() As CabFact
Global ArrayLineas() As LinFact
Global arrayRNFs() As RnfLinea

Dim qry(1 To 18) As New rdoQuery
Global NoFactura As String

'************************************************************************************
'   ImprimirFact :
'
'   Parametros : un array de numeros de factura, p.e. ("13/1600001","13/1600002",...)
'
'   Esta funci�n recoge los numeros de factura y se encarga de recoger los datos
'   de las facturas y llamar al formulario de impresi�n de facts.
'
'   recoge tambien un booleano opcional que indica si es una factura que se est�
'   creando para mostrar en el formulario los botones de grabar y cancelar
'
'   devuelve un booleano que indica si la factura nueva debe o no ser grabada
'************************************************************************************

Public Function ImprimirFact(nFact() As String, Optional NuevaFact As Boolean = False, Optional CI22NUMHISTORIA As String = "", Optional CodProceso As String = "", Optional CodAsistenci As String = "") As Boolean
    
    ImprimirFact = False
    
    NuevasFacts = NuevaFact
    
    On Error GoTo errorFact
    If UBound(nFact) > 0 Then
        ' inicializar las queries
        Call IniciarQRY
        
        'llenamos los tipos definidos por el usario correspondientes a las facturas
        Call LlenadoTiposImpresion(nFact, CI22NUMHISTORIA, CodProceso, CodAsistenci)
        
        ' llamar al formulario de presentaci�n preliminar
        If NuevasFacts = True Then
            frm_Impresion.cmdGrabar.Visible = True
            frm_Impresion.cmdModificar.Visible = False
        Else
            frm_Impresion.cmdGrabar.Visible = False
            frm_Impresion.cmdModificar.Visible = True
        End If
        
        frm_Impresion.Show vbModal
        
        ' si las facturas son nuevas gestionar la asignacion de un numero definitivo
        ' o el borrado de cabecera y lineas
        If NuevasFacts = True Then
            If ArrayCabecera(1).Grabar = True Then
                ImprimirFact = True
                
                Call GrabarFacts
                frm_Impresion.cmdGrabar.Visible = False
                frm_Impresion.Show vbModal
                
            Else
                MsgBox "Ha salido sin grabar la factura", vbInformation + vbOKOnly, "Edici�n de facturas"
                ImprimirFact = False
                
                Call AnularFactProvisionales
            End If
        End If
    End If
    
Exit Function

errorFact:
   If Err.Number = 321 Then
      MsgBox "Se ha producido un error en el formato de la factura" & Chr(10) & Chr(10) & _
             "No se mostrar� la factura ", vbCritical + vbOKOnly, "Facturaci�n"
             
      Call AnularFactProvisionales
   Else
      MsgBox "Se ha producido un error " & Err.Number & " en la impresi�n de facturas" & Chr(10) & Chr(10) & _
              Err.Description, vbCritical + vbOKOnly, "Facturaci�n"
'      Resume
   End If
'    Resume Next
End Function

'************************************************************************************
'   ImprimirFact :
'
'   Parametros : un array de numeros de factura, p.e. ("13/1600001","13/1600002",...)
'
'   Esta funci�n recoge los numeros de factura y se encarga de recoger los datos
'   de las facturas y llamar al formulario de impresi�n de facts.
'
'************************************************************************************

Public Function ImprimirFactMasiva(nFact() As String, Optional Impresora As String) As Boolean
    
'    ImprimirFact = False
    
   On Error GoTo errorFact
    If UBound(nFact) > 0 Then
        ' inicializar las queries
        Call IniciarQRY
        
        'llenamos los tipos definidos por el usario correspondientes a las facturas
        Call LlenadoTiposImpresion(nFact)
        
'        ' llamar al formulario de presentaci�n preliminar
'        If NuevasFacts = True Then
'            frm_Impresion.cmdGrabar.Visible = True
'            frm_Impresion.cmdModificar.Visible = False
'        Else
'            frm_Impresion.cmdGrabar.Visible = False
'            frm_Impresion.cmdModificar.Visible = True
'        End If
'        Load frm_Impresion
        frm_Impresion.ImpresionMasiva = True
        frm_Impresion.Impresora = Impresora
        frm_Impresion.Show vbModal
'        Load frm_Impresion
    End If
    
Exit Function

errorFact:
   If Err.Number = 321 Then
      MsgBox "Se ha producido un error en el formato de la factura" & Chr(10) & Chr(10) & _
             "No se mostrar� la factura ", vbCritical + vbOKOnly, "Facturaci�n"
             
      Call AnularFactProvisionales
   Else
      MsgBox "Se ha producido un error " & Err.Number & " en la impresi�n de facturas" & Chr(10) & Chr(10) & _
              Err.Description, vbCritical + vbOKOnly, "Facturaci�n"
'      Resume
   End If
'    Resume Next
End Function

Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSql = "Select * from FA0400 " & _
          "Where FA04NUMFACT = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "order by FA04INDNMGC, AD01CODASISTENCI"
          
  qry(1).SQL = MiSql
  
  ' Seleccionar los datos de una factura en base al n� de factura
  ' solo recoge la parte de la historia seleccionada
  ' solo se filtra por historia cuando el tipo de factura es 2 (osasunbidea)
  MiSql = "Select * from FA0400 " & _
          "Where FA04NUMFACT = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "  AND (" & _
          "          (FA61CODTIPOFACT  = 2 AND " & _
          "           EXISTS (SELECT * " & _
          "                   FROM ad0700 " & _
          "                   Where ad0700.CI22NUMHISTORIA = ? " & _
          "                     AND ad0700.ad07codproceso = FA0400.AD07CODPROCESO" & _
          "                   )" & _
          "           ) " & _
          "        OR FA61CODTIPOFACT IS NULL OR FA61CODTIPOFACT = 1 OR FA61CODTIPOFACT = 4 " & _
          "       )      " & _
          "order by FA04INDNMGC, AD01CODASISTENCI"
          
'  MiSql = "Select * from FA0400 " & _
          "Where FA04NUMFACT = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "  AND  EXISTS (SELECT * " & _
          "                   FROM ad0700 " & _
          "                   Where ad0700.CI22NUMHISTORIA = ? " & _
          "                     AND ad0700.ad07codproceso = FA0400.AD07CODPROCESO" & _
          "                   )" & _
          "order by FA04INDNMGC, AD01CODASISTENCI"
          
          
  qry(17).SQL = MiSql
  
  
'  MiSql = "SELECT t2.* " & _
          "FROM fa0400 t2, ad0700 " & _
          "Where t2.ad07codproceso = ad0700.ad07codproceso " & _
          "  AND ad0700.ci22numhistoria = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "   AND EXISTS " & _
          "               ( " & _
          "               SELECT CI32CODTIPECON, CI13CODENTIDAD, FA09CODNODOCONC, FA09CODNODOCONC_FACT, FA04FECDESDE, FA04FECHASTA " & _
          "               FROM fa0400 t1 " & _
          "               WHERE t1.fa04numfact = ? " & _
          "                   AND FA04NUMFACREAL IS NULL " & _
          "                  AND t1.ad07codproceso = ? " & _
          "                  AND t1.ad01codasistenci = ? " & _
          "                 AND t1.CI32CODTIPECON = t2.CI32CODTIPECON " & _
          "                 AND t1.CI13CODENTIDAD = t2.CI13CODENTIDAD " & _
          "                 AND t1.FA09CODNODOCONC = t2.FA09CODNODOCONC " & _
          "                 AND t1.FA09CODNODOCONC_FACT = t2.FA09CODNODOCONC_FACT " & _
          "                 AND t1.FA04FECDESDE = t2.FA04FECDESDE " & _
          "                 AND t1.FA04FECHASTA = t2.FA04FECHASTA " & _
          "               ) " & _
          "order by FA04INDNMGC, AD01CODASISTENCI"
          
  qry(18).SQL = MiSql
  
  
  
  
  
  'Seleccionamos las l�neas de una factura en base al c�digo de factura
  MiSql = "Select * from FA1600 Where FA04CODFACT = ? ORDER BY FA04CODFACT, FA16ORDFACT, FA16NUMLINEA"
  qry(2).SQL = MiSql
  
  'Seleccionamos los RNFs de una l�nea de factura en base al n� de factura y al n� de l�nea"
  MiSql = "Select * from FA0300 Where FA04NUMFACT = ? And FA16NUMLINEA = ?"
  qry(3).SQL = MiSql
  
  'Seleccionamos los datos de un responsable econ�mico
  
    'CI21CODPERSONA,CI23RAZONSOCIAL,CI23NUMDIRPRINC,CI10CALLE,CI10PORTAL,CI10RESTODIREC
    'CI07CODPOSTAL,CI10DESLOCALID DONDE EL CI21CODPERSONA ESTE RECOGIDO DE CI0900 EN BASE A CI13CODENTIDAD
    
'  'Seleccionamos la descripcion del concierto que aparece en el pie de factura
  MiSql = " select CI13COMENTARIOFAC from ci1300 where ci32codtipecon = ? and ci13codentidad = ?"
  qry(4).SQL = MiSql

  'Buscamos las designaci�n del RNF
  MiSql = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  qry(5).SQL = MiSql
  
  
  'Seleccionamos la �ltima cama en la que se ha encontrado el paciente.
  MiSql = "Select GCFN06(AD15CODCAMA) From AD1600 Where AD01CODASISTENCI = ? And AD07CODPROCESO = ? ORDER BY AD16FECFIN DESC"
  qry(8).SQL = MiSql
  
  'Buscamos el codigo del paciente para incluirlo en la factura.
  MiSql = "SELECT CI21CODPERSONA FROM AD0700 Where AD0700.AD07CODPROCESO = ?"
  qry(9).SQL = MiSql
  

  '***************************************************************************************************
  ' SQL para sustituir el numero provisional de la propuesta de factura por el definitivo
  '***************************************************************************************************
    'Asignar el numero de factura definitivo a la factura provisional
  MiSql = "Update FA0400 set FA04NUMFACT = ? Where FA04NUMFACT = ?"
  qry(10).SQL = MiSql
  
    'Asignar el numero de factura REAL definitivo a la factura ANULADA
  MiSql = "Update FA0400 set FA04NUMFACREAL = ? Where FA04NUMFACREAL = ?"
  qry(15).SQL = MiSql
  
    'Asignar a FA0300 el estado 2 (Facturado)
  MiSql = "update fa0300 set fa03indfact = 2 " & _
           "where fa04numfact in (select fa04codfact from fa0400 where fa04numfact = ? )"
  qry(11).SQL = MiSql
  
  '***************************************************************************************************
  ' SQL para el borrado de las facturas provisionales, se borra la cabecera y las lineas, los RNF NO
  ' se borran porque se continua con la edici�n de la factura
  ' El borrado se realiza en base al numero provisional de la factura
  '***************************************************************************************************
  
    'Borrar las lineas de una Factura
  MiSql = "UPDATE fa0300 SET FA04NUMFACT = NULL " & _
           "where FA04NUMFACT in (select fa04codfact from fa0400 where fa04numfact = ? )"
  qry(12).SQL = MiSql
    'Borrar las lineas de una Factura
  MiSql = "DELETE from fa1600 where fa04codfact in (select fa04codfact from fa0400 where fa04numfact = ? )"
  qry(13).SQL = MiSql
    'Borrar las cabeceras de una Factura
  MiSql = "DELETE from fa0400 where fa04numfact = ? "
  qry(14).SQL = MiSql
  
    'Asignar el numero de factura REAL 0 (marca de anulaci�n) a la factura ANULADA
  MiSql = "Update FA0400 set FA04NUMFACREAL = 0 Where FA04NUMFACREAL = ?"
  qry(16).SQL = MiSql
  
  'Activamos las querys.
  For x = 1 To 18
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub

' Mediante el procedimiento LlenadoTiposImpresi�n recogeremos los datos de determinadas facturas
' y los introduciremos en los tipos de impresi�n definidos para el efecto.
Private Sub LlenadoTiposImpresion(nFact() As String, Optional CI22NUMHISTORIA As String = "", Optional CodProceso As String = "", Optional CodAsistenci As String = "")

Dim rsCabecera As rdoResultset
Dim rsLineas As rdoResultset
Dim rsRNFs As rdoResultset
Dim rsDescripcion As rdoResultset
Dim rsPaciente As rdoResultset
Dim intContCabecera As Integer
Dim IntContLineas As Integer
Dim intContRnfs As Integer
Dim Acumulado As Boolean
Dim PrimerRNF As Boolean
Dim x As Integer
Dim Y As Integer
Dim SqlFechas As String
Dim RsFechas As rdoResultset
Dim NumFact As String
Dim rsCamas As rdoResultset
Dim intLinea As Integer
Dim sqlCamas As String
Dim rsCamas2 As rdoResultset

    Erase ArrayCabecera
    Erase ArrayLineas
    Erase arrayRNFs
    
    On Error GoTo error
    
    intContCabecera = 0
    IntContLineas = 0
    intContRnfs = 0
    For x = 1 To UBound(nFact)
        NumFact = nFact(x)
        'Se ha introducido un n�mero de factura
        If NumFact <> "" Then
            If CI22NUMHISTORIA = "" Then
               qry(1).rdoParameters(0) = NumFact   'N� de la factura introducido
               Set rsCabecera = qry(1).OpenResultset
            Else
               If CodProceso = "" And CodAsistenci = "" Then
                  qry(17).rdoParameters(0) = NumFact   'N� de la factura introducido
                  qry(17).rdoParameters(1) = CI22NUMHISTORIA ' n� de historia
                  Set rsCabecera = qry(17).OpenResultset
               Else
                  ' AQUI NO DEBERIA ENTRAR NUNCA
                  qry(18).rdoParameters(0) = CI22NUMHISTORIA   'N� de la factura introducido
                  qry(18).rdoParameters(1) = NumFact ' n� de historia
                  qry(18).rdoParameters(2) = CodProceso
                  qry(18).rdoParameters(3) = CodAsistenci
                  Set rsCabecera = qry(18).OpenResultset
               End If
            End If
            
            If Not rsCabecera.EOF Then
                '------------------------------------------------------------------------------------------
                ' DATOS DE LA CABECERA DE LA FACTURA
                '------------------------------------------------------------------------------------------
                'Llenamos los datos referidos a la cabecera
                intContCabecera = intContCabecera + 1
                ReDim Preserve ArrayCabecera(1 To intContCabecera)
                
                'Buscamos los datos relativos al responsable economico (si la hubiera)
                If Not IsNull(rsCabecera("CI21CODPERSONA")) Then
                    ' Cargamos el objeto REco
                    Set ArrayCabecera(intContCabecera).reco = New Persona
                    ArrayCabecera(intContCabecera).reco.Codigo = rsCabecera("CI21CODPERSONA")
                End If
                
                
                'Buscamos el codigo del paciente
                qry(9).rdoParameters(0) = rsCabecera("AD07CODPROCESO")
                Set rsPaciente = qry(9).OpenResultset
                If Not rsPaciente.EOF Then
                    Set ArrayCabecera(intContCabecera).Paciente = New Persona
                    ArrayCabecera(intContCabecera).Paciente.Codigo = rsPaciente("CI21CODPERSONA")
                End If
                
                ArrayCabecera(intContCabecera).Dto = "0" & rsCabecera("FA04DESCUENTO") 'Dto aplicado
                ArrayCabecera(intContCabecera).fecha = "" & Format(rsCabecera("FA04FECFACTURA"), "DD/MM/YYYY") 'Fecha
                ArrayCabecera(intContCabecera).NFactura = "" & rsCabecera("FA04NUMFACT")   'Numero de factura
                ArrayCabecera(intContCabecera).CodFact = "" & rsCabecera("FA04CODFACT")  'codigo de factura
                If NuevasFacts = True Then
                    ArrayCabecera(intContCabecera).NumFactProvisional = "" & rsCabecera("FA04NUMFACT")  'Numero de factura
                Else
                    ArrayCabecera(intContCabecera).NumFactProvisional = ""
                End If
                ArrayCabecera(intContCabecera).Observac = rsCabecera("FA04OBSERV") & "" 'Observaciones
                ArrayCabecera(intContCabecera).Descrip = rsCabecera("FA04DESCRIP") & ""
                ArrayCabecera(intContCabecera).NumCaso = rsCabecera("AD01CODASISTENCI")
                ArrayCabecera(intContCabecera).proceso = rsCabecera("AD07CODPROCESO")
'                ArrayCabecera(intContCabecera).TotFactura = rsCabecera("FA04CANTFACT") 'Total Facturado
                ArrayCabecera(intContCabecera).codConcierto = 0 & rsCabecera("FA09CODNODOCONC") 'C�digo de concierto
                ArrayCabecera(intContCabecera).TipEcon = rsCabecera("CI32CODTIPECON")
                ArrayCabecera(intContCabecera).CodEnt = rsCabecera("CI13CODENTIDAD")
                
                ' Recoger la descripcion del concierto que aparece en el pie de la factura
               qry(4).rdoParameters(0) = "" & rsCabecera("CI32CODTIPECON")   ' P
               qry(4).rdoParameters(1) = "" & rsCabecera("CI13CODENTIDAD")   ' J
               Set rsDescripcion = qry(4).OpenResultset
               If Not rsDescripcion.EOF Then
                  ArrayCabecera(intContCabecera).DescConcierto = "" & rsDescripcion("CI13COMENTARIOFAC")
               Else
                  ArrayCabecera(intContCabecera).DescConcierto = ""
               End If
                'Recuperamos de la base de datos los datos referentes a Fecha de Inicio y de final de Factura
                If Not IsNull(rsCabecera("AD01CODASISTENCI")) Then
                    SqlFechas = "Select AD01FECINICIO, AD01FECFIN From AD0100 Where AD01CODASISTENCI = " & rsCabecera("AD01CODASISTENCI")
                    Set RsFechas = objApp.rdoConnect.OpenResultset(SqlFechas, 3)
                    If Not RsFechas.EOF Then
                        If Not IsNull(RsFechas("AD01FECINICIO")) Then
                            ArrayCabecera(intContCabecera).FechaInicio = Format(RsFechas("AD01FECINICIO"), "DD/MM/YYYY")
                        Else
                            ArrayCabecera(intContCabecera).FechaInicio = ""
                        End If
                        If Not IsNull(RsFechas("AD01FECFIN")) Then
                            ArrayCabecera(intContCabecera).FechaFinal = Format(RsFechas("AD01FECFIN"), "DD/MM/YYYY")
                        Else
                            ArrayCabecera(intContCabecera).FechaFinal = ""
                        End If
                    Else
                        ArrayCabecera(intContCabecera).FechaInicio = ""
                        ArrayCabecera(intContCabecera).FechaFinal = ""
                    End If
                Else
                    ArrayCabecera(intContCabecera).FechaInicio = ""
                    ArrayCabecera(intContCabecera).FechaFinal = ""
                End If
                'Buscamos todos los procesos que tenga esa factura
                sqlCamas = "Select AD01CODASISTENCI, AD07CODPROCESO From FA0400 Where FA04NUMFACT = '" & nFact(x) & "'"
                Set rsCamas2 = objApp.rdoConnect.OpenResultset(sqlCamas, 3)
                If Not rsCamas2.EOF Then
                  rsCamas2.MoveLast
                  rsCamas2.MoveFirst
                  While Not rsCamas2.EOF
                    If Not IsNull(rsCamas2("AD01CODASISTENCI")) And Not IsNull(rsCamas2("AD07CODPROCESO")) Then
                      qry(8).rdoParameters(0) = rsCamas2("AD01CODASISTENCI")
                      qry(8).rdoParameters(1) = rsCamas2("AD07CODPROCESO")
                      Set rsCamas = qry(8).OpenResultset
                      If Not rsCamas.EOF Then
                        ArrayCabecera(intContCabecera).Habitacion = rsCamas(0)
                      Else
                        'ArrayCabecera(intContCabecera).Habitacion = ""
                      End If
                    End If
                    rsCamas2.MoveNext
                  Wend
                End If
                ' cargar el codigo del usuario que ha creado la factura
                ArrayCabecera(intContCabecera).SG02COD_ADD = "" & rsCabecera("SG02COD_ADD")
                
                Do While Not rsCabecera.EOF
                    ' hay que pasar por cada uno de los codigos de factura que
                    ' contenga este numero de factura para recoger sus lineas y
                    ' acumular el total
                    ArrayCabecera(intContCabecera).TotFactura = ArrayCabecera(intContCabecera).TotFactura + rsCabecera("FA04CANTFACT") 'Total Facturado

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Rellenamos los parametros que nos van a servir para buscar las lineas de esa factura
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    qry(2).rdoParameters(0) = rsCabecera("FA04CODFACT") 'C�digo de la factura
                    Set rsLineas = qry(2).OpenResultset
                    If Not rsLineas.EOF Then
                        '----------------------------------------------------------------------------------------
                        ' DATOS DE LAS L�NEAS DE LA FACTURA
                        '----------------------------------------------------------------------------------------
                        'rsLineas.MoveFirst
                        While Not rsLineas.EOF
                            'Llenamos los datos referidos a cada l�nea de la cabecera
                            IntContLineas = IntContLineas + 1
                            If IntContLineas = 1 Then
                                ReDim ArrayLineas(1 To IntContLineas)
                            Else
                                ReDim Preserve ArrayLineas(1 To IntContLineas)
                            End If
                            ArrayLineas(IntContLineas).NFactura = intContCabecera 'Contador L�neas de cabecera.
                            ArrayLineas(IntContLineas).NLinea = IntContLineas 'Contador l�neas de factura.
                            ArrayLineas(IntContLineas).Descripcion = rsLineas("FA16DESCRIP") 'Descripci�n de la l�nea
                            ArrayLineas(IntContLineas).Importe = rsLineas("FA16IMPORTE") 'Importe total de la l�nea (dto incluido)
                            ArrayLineas(IntContLineas).mostrar = True
                            ArrayLineas(IntContLineas).Precio = rsLineas("FA16PRECIO") 'Precio Unitario
                            'Rellenamos los par�metros que nos van a servir para buscar los RNFs de esa factura
                            qry(3).rdoParameters(0) = rsCabecera("FA04CODFACT")   'N� de la factura
                            qry(3).rdoParameters(1) = rsLineas("FA16NUMLINEA")       'N� de l�nea
                            PrimerRNF = True
                            Set rsRNFs = qry(3).OpenResultset
                
                            If Not rsRNFs.EOF Then
                            
                                ArrayLineas(IntContLineas).mostrar = False

                                '------------------------------------------------------------------------------------
                                ' DATOS DE LOS RNFS DE LAS L�NEAS DE FACTURA
                                '------------------------------------------------------------------------------------
                                'rsRnfs.MoveFirst
                                While Not rsRNFs.EOF
                                    Acumulado = False
                                    If PrimerRNF Then
                                        PrimerRNF = False
                                    Else
                                        For Y = 1 To UBound(arrayRNFs)
                                            'El rnf existe en el array, por lo cual lo agrupamos.
                                            If arrayRNFs(Y).NFactura = intContCabecera And arrayRNFs(Y).NLinea = IntContLineas _
                                                And arrayRNFs(Y).CodCateg = rsRNFs("FA05CODCATEG") _
                                                And arrayRNFs(Y).Descripcion = Trim(rsRNFs("FA03OBSIMP") & " " & rsRNFs("FA03OBSERV")) Then
                                                
                                                arrayRNFs(Y).Cantidad = arrayRNFs(Y).Cantidad + rsRNFs("FA03CANTFACT")
                                                Acumulado = True
                                            End If
                                        Next
                                    End If
                                    'No existe el RNF en el array y lo introducimos
                                    If Acumulado = False Then
                                        intContRnfs = intContRnfs + 1
                                        If intContRnfs = 1 Then
                                            ReDim arrayRNFs(1 To intContRnfs)
                                        Else
                                            ReDim Preserve arrayRNFs(1 To intContRnfs)
                                        End If
                                        'Llenamos los datos referidos a cada rnf de la l�nea
                                        arrayRNFs(intContRnfs).NFactura = intContCabecera 'Contador l�neas de cabecera.
                                        arrayRNFs(intContRnfs).CodCateg = rsRNFs("FA05CODCATEG")
                                        arrayRNFs(intContRnfs).NLinea = IntContLineas 'Contador l�neas de factura.
                                        arrayRNFs(intContRnfs).Cantidad = IIf(IsNumeric(rsRNFs("FA03CANTFACT")), rsRNFs("FA03CANTFACT"), 0) 'Cantidad
                                        arrayRNFs(intContRnfs).Precio = IIf(IsNumeric(rsRNFs("FA03PRECIOFACT")), rsRNFs("FA03PRECIOFACT"), 0) 'Precio del RNF.
                                        If rsRNFs("FA03INDDESGLOSE") = 1 Then 'Indicador del deglose del RNF en factura
                                            arrayRNFs(intContRnfs).Desglose = True
                                        Else
                                            arrayRNFs(intContRnfs).Desglose = False
                                        End If
                                        
                                        ' por defecto se imprime la descripcion del rnf que se haya grabado
                                        ' en la fa0300, si no tiene se recoge la de la categoria de la fa0500
                                        If Not IsNull(rsRNFs("FA03OBSIMP")) Then
                                            arrayRNFs(intContRnfs).Descripcion = rsRNFs("FA03OBSIMP")
                                        Else
                                            'Habr� que buscarla dentro de la tabla FA0500
                                            qry(5).rdoParameters(0) = rsRNFs("FA05CODCATEG")
                                            Set rsDescripcion = qry(5).OpenResultset
                                            If Not rsDescripcion.EOF Then
                                                arrayRNFs(intContRnfs).Descripcion = rsDescripcion("FA05DESIG")
                                            Else
                                                arrayRNFs(intContRnfs).Descripcion = ""
                                            End If
                                        End If
                                        
                                        If Not IsNull(rsRNFs("FA03OBSERV")) Then
                                            arrayRNFs(intContRnfs).Descripcion = arrayRNFs(intContRnfs).Descripcion & " " & rsRNFs("FA03OBSERV")
                                        End If
                                        arrayRNFs(intContRnfs).Descripcion = Trim(arrayRNFs(intContRnfs).Descripcion)
                                    End If
                                    rsRNFs.MoveNext
                                Wend
                            Else
'                                On Error Resume Next
'                                Erase arrayRNFs
'                                On Error GoTo 0
                                On Error Resume Next
                                ReDim Preserve arrayRNFs(0 To 0)
                                On Error GoTo error
                            End If
                            
                            For Y = 1 To UBound(arrayRNFs)
                                If arrayRNFs(Y).NFactura = intContCabecera And arrayRNFs(Y).NLinea = IntContLineas And arrayRNFs(Y).Cantidad <> 0 Then
                                    If (ArrayLineas(IntContLineas).Importe <> 0) Or (ArrayLineas(IntContLineas).Importe = 0 And arrayRNFs(Y).Desglose = True) Then
                                       ArrayLineas(IntContLineas).mostrar = True
                                       Exit For
                                    End If
                                End If
                            Next
                            rsLineas.MoveNext
                        Wend
                    Else
                '            ReDim Preserve ArrayLineas(0)
                    End If
                    
                    rsCabecera.MoveNext
            
                Loop
            Else
                'No existen l�neas en la factura.
                MsgBox "La factura " & NumFact & " no tiene l�neas.", vbExclamation + vbOKOnly, "Atenci�n"
            End If
        Else
        
          'No existe la factura en la BD.
          'MsgBox "El n�mero de factura " & NumFact & " no existe en la BD", vbExclamation + vbOKOnly, "Atenci�n"
        End If
    Next
Exit Sub
error:
    MsgBox "Error en el proceso de generaci�n de la factura" & Chr(13) & _
            Err.Description & Chr(13) & "La factura no ser� correcta", vbCritical + vbOKOnly, "ERROR"
    Resume Next
End Sub

Private Sub GrabarFacts()
    Dim i As Integer
    On Error GoTo error
    ' hay que sustituir los numeros de factura provisionales por los nuevos
    For i = 1 To UBound(ArrayCabecera)
        If NoFactura <> "" And NoFactura <> "0" And Left(NoFactura, 1) <> "P" Then
          ArrayCabecera(i).NFactura = NoFactura ' Se pone el antiguo numero de la factura
          ' se pone el numero anterior a cero para que la siguiente se le asigne uno nuevo
          NoFactura = ""
        Else
          ArrayCabecera(i).NFactura = "13/" & fNextClave("FA04NUMFACT", "FA0400")
          NoFactura = ""
        End If
        ' iniciar la transaccion
        objApp.BeginTrans
        
        ' Asignar el numero de factura definitivo a la factura
        qry(10).rdoParameters(0) = ArrayCabecera(i).NFactura
        qry(10).rdoParameters(1) = ArrayCabecera(i).NumFactProvisional
        qry(10).Execute
        
        ' Asignar el numero de factura real definitivo a la factura anulada
        qry(15).rdoParameters(0) = ArrayCabecera(i).NFactura
        qry(15).rdoParameters(1) = ArrayCabecera(i).NumFactProvisional
        qry(15).Execute
        
        ' Marcar sus RNFs como facturados (FA03INDFACT = 2)
        qry(11).rdoParameters(0) = ArrayCabecera(i).NFactura
        qry(11).Execute
        
        objApp.CommitTrans
        
        
        ArrayCabecera(i).NumFactProvisional = ""
ProximaFact:
        
    Next
    
    Exit Sub
error:
    MsgBox "Ha habido alg�n error en la grabaci�n de la factura " & Chr(13) & _
            ArrayCabecera(i).NFactura & Chr(13) & "Rev�sela", vbCritical + vbOKOnly, "Grabaci�n de facturas"
Resume
   ArrayCabecera(i).NFactura = ArrayCabecera(i).NumFactProvisional
    
    objApp.RollbackTrans
    objApp.BeginTrans
    
    Resume ProximaFact
End Sub

Private Sub AnularFactProvisionales()
    Dim i As Integer
    
    ' hay que sustituir los numeros de factura provisionales por los nuevos
    For i = 1 To UBound(ArrayCabecera)
    
        ' iniciar la transaccion
        objApp.BeginTrans
        
        ' Eliminar el codigo de la factura provisional de los RNFs en la FA0300
        qry(12).rdoParameters(0) = ArrayCabecera(i).NumFactProvisional
        qry(12).Execute
        ' Borrar las lineas de la factura provisional
        qry(13).rdoParameters(0) = ArrayCabecera(i).NumFactProvisional
        qry(13).Execute
        ' Borrar las cabeceras de la factura provisional
        qry(14).rdoParameters(0) = ArrayCabecera(i).NumFactProvisional
        qry(14).Execute
        
        ' Asignar el numero de factura real definitivo=0 (Anulada) a la factura anulada
        qry(16).rdoParameters(0) = ArrayCabecera(i).NumFactProvisional
        qry(16).Execute
        
        
        objApp.CommitTrans
ProximaFact:
        
    Next
    Exit Sub
    
error:
    MsgBox "Ha habido alg�n error en la anulaci�n de la factura provisional" & Chr(13) & _
            ArrayCabecera(i).NumFactProvisional & Chr(13) & "Revisela", vbCritical + vbOKOnly, "Presentaci�n de facturas"
            
    objApp.RollbackTrans
    objApp.BeginTrans
    
    Resume ProximaFact
End Sub
