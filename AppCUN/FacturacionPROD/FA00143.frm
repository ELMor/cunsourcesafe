VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_Remesas 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Cierre de Remesas"
   ClientHeight    =   3465
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3465
   ScaleWidth      =   4035
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtRemesa 
      Height          =   285
      Left            =   2070
      MaxLength       =   12
      TabIndex        =   7
      Top             =   540
      Width           =   1095
   End
   Begin VB.CommandButton cmdListado 
      Caption         =   "Listado de comprobaci�n previo al cierre de remesa."
      Height          =   555
      Left            =   120
      TabIndex        =   3
      Top             =   945
      Width           =   3840
   End
   Begin VB.CommandButton cmdCierre 
      Caption         =   "Cierre de remesa"
      Height          =   600
      Left            =   90
      TabIndex        =   2
      Top             =   1620
      Width           =   3840
   End
   Begin VB.CommandButton cmdListadoAnt 
      Caption         =   "Listado de cierre de remesa"
      Enabled         =   0   'False
      Height          =   555
      Left            =   90
      TabIndex        =   1
      Top             =   2340
      Width           =   3840
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   945
      TabIndex        =   0
      Top             =   3015
      Width           =   1950
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCierre 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2070
      TabIndex        =   4
      Tag             =   "Fecha Inicio"
      Top             =   135
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   420
      Left            =   270
      TabIndex        =   5
      Top             =   2430
      Visible         =   0   'False
      Width           =   465
      _Version        =   196608
      _ExtentX        =   820
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "N�mero de Remesa:"
      Height          =   240
      Index           =   0
      Left            =   90
      TabIndex        =   8
      Top             =   585
      Width           =   1905
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Cierre:"
      Height          =   240
      Index           =   4
      Left            =   180
      TabIndex        =   6
      Top             =   180
      Width           =   1815
   End
End
Attribute VB_Name = "frm_Remesas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ArrayCheques() As String


Dim TotalRemesa As Double
Dim TotalAcunsa As Double
Dim TotalInsalud As Double
Dim TotalMutuas As Double
Dim totalPrivado As Double
Dim TotalNoCompensado As Double
Dim TotalApuntes As Integer

Dim FechaListado As String

Private Sub cmdCierre_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Cantidad As String
Dim Texto As String
  MiSqL = "Select SUM(FA17CANTIDAD) AS SUMA from FA1700 Where FA17FECEFECTO Is Null AND FA17REMESA = '" & txtRemesa.Text & "'" & _
          " ORDER BY FA17REMESA, FA17APUNTE"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not IsNull(MiRs("SUMA")) Then
    Cantidad = Format(MiRs("SUMA"), "###,###,###,##0")
  Else
    Cantidad = 0
  End If
  'Tras pedir confirmacion asignamos a la fecha cierre de caja la que nos han introducido.
  Texto = "�Desea cerrar la remesa n�mero " & Me.txtRemesa.Text & Chr(10) & Chr(13) & _
          " de importe " & Cantidad & " a fecha " & Me.SDCFechaCierre.Date & "?"
  If MsgBox(Texto, vbQuestion + vbYesNo, "Aviso") = vbYes Then
    MiSqL = "UPDATE FA1700 SET FA17FECEFECTO = TO_DATE('" & SDCFechaCierre.Date & "','DD/MM/YYYY') " & _
            " Where FA17REMESA = '" & Me.txtRemesa.Text & "'" & _
            " AND FA17FECEFECTO = ''"
    objApp.rdoConnect.Execute MiSqL
  End If

End Sub

Private Sub cmdListado2_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim RsCant As rdoResultset
Dim sqlPers As String
Dim rsPers As rdoResultset
Dim Compensado As Double
Dim Cont As Integer

  'FechaCierre = Format(Me.SDCFechaCierre.Date, "DD/MM/YYYY")
  'Seleccionamos de la tabla de pago aquellos que tienen fecha de efecto menor que la fecha seleccionada
  'y la fecha de cierre de caja nula.
  MiSqL = "Select * from FA1700 Where FA17FECEFECTO Is Null AND FA17REMESA = " & txtRemesa.Text & _
          " ORDER BY FA17REMESA, FA17APUNTE"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Cont = 0
    While Not MiRs.EOF
      Cont = Cont + 1
      ReDim Preserve ArrayCheques(1 To 7, 1 To Cont)
      ArrayCheques(1, Cont) = MiRs("FA17CODPAGO")
      ArrayCheques(2, Cont) = MiRs("CI21CODPERSONA")
      ArrayCheques(3, Cont) = MiRs("FA17OBSERV")
      ArrayCheques(4, Cont) = Format(MiRs("FA17FECPAGO"), "DD/MM/YYYY")
      ArrayCheques(5, Cont) = MiRs("FA17REMESA")
      ArrayCheques(6, Cont) = "" & MiRs("FA17APUNTE")
      ArrayCheques(7, Cont) = Format(MiRs("FA17CANTIDAD"), "###,###,##0.##")
      MiRs.MoveNext
    Wend
    'MsgBox "La suma de los importes de los abonos es : " & Format(TotalCierreCaja, "###,###,###,##0.##")
    Call ImprimirListado
  Else
    MsgBox "No hay abonos pendientes de cierre a fecha " & Me.SDCFechaCierre.Date, vbInformation + vbOKOnly, "Aviso"
  End If
End Sub

Sub ImprimirListado()
Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer
Dim MiSqL As String
Dim rsTotales As rdoResultset

  'Calculamos el n�mero de pagos que vamos a listar
  NoAbonos = UBound(ArrayCheques, 2)
  Pagina = 1
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  'vsPrinter.Orientation = orLandscape
  vsPrinter.FontSize = 10
  vsPrinter.FontName = "Courier New"
  Texto = FechaListado & "                  PAG: " & Pagina 'N� de p�gina
  vsPrinter.Paragraph = Texto
  vsPrinter.Paragraph = ""
  Texto = "C.U.N.   REMESA..: " & Me.txtRemesa.Text 'Fecha en la que cerrramos la caja.
  vsPrinter.Paragraph = Texto
  vsPrinter.Paragraph = ""
  Texto = "APUNTE  ABONO  R.ECON    FECHA    TIPO    IMPORTE   C PDTE.COMPEN."
  vsPrinter.Paragraph = Texto
  Texto = "------ ------- ------- ---------- ---- ------------ - ------------"
  vsPrinter.Paragraph = Texto
  For Cont = 1 To NoAbonos
    Texto = Right(Space(6) & Trim(ArrayCheques(1, Cont)), 6) & " " 'N� de Abono
    Texto = Texto & Right(Space(7) & Trim(ArrayCheques(2, Cont)), 7) & " " 'Abono
    Texto = Texto & Right(Space(7) & Trim(ArrayCheques(3, Cont)), 7) & " "  'Resp. Econom.
    Texto = Texto & Format(ArrayCheques(4, Cont), "DD/MM/YYYY") & " "  'Fecha de Cobro
    Texto = Texto & Space(1) & Right(Space(2) & Trim(ArrayCheques(5, Cont)), 2) & "  " 'Tipo
    Texto = Texto & Right(Space(12) & Trim(ArrayCheques(6, Cont)), 12) & " " 'Apunte
    Texto = Texto & Right(Space(1) & Trim(ArrayCheques(7, Cont)), 1) & " " 'Compensado?
    Texto = Texto & Right(Space(12) & Trim(ArrayCheques(8, Cont)), 12) & " " 'Apunte
    vsPrinter.Paragraph = Texto
    If vsPrinter.CurrentY > 260 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      vsPrinter.NewPage
      vsPrinter.CurrentY = 10 * 54.6101086
      Pagina = Pagina + 1
      Texto = FechaListado & "                  PAG: " & Pagina 'N� de p�gina
      vsPrinter.Paragraph = Texto
      vsPrinter.Paragraph = ""
      Texto = "C.U.N.   REMESA..: " & Me.txtRemesa.Text 'Fecha en la que cerrramos la caja.
      vsPrinter.Paragraph = Texto
      vsPrinter.Paragraph = ""
      Texto = "APUNTE  ABONO  R.ECON    FECHA    TIPO    IMPORTE   C PDTE.COMPEN."
      vsPrinter.Paragraph = Texto
      Texto = "------ ------- ------- ---------- ---- ------------ - ------------"
      vsPrinter.Paragraph = Texto
    End If
  Next
  vsPrinter.Paragraph = ""
  Texto = "TOTAL PRIVADOS.. " & Right(Space(18) & Trim(Format(totalPrivado, "###,###,###,##0.##")), 18)
  vsPrinter.Paragraph = Texto
  Texto = "TOTAL MUTUAS.... " & Right(Space(18) & Trim(Format(TotalMutuas, "###,###,###,##0.##")), 18)
  vsPrinter.Paragraph = Texto
  Texto = "TOTAL ACUNSA.... " & Right(Space(18) & Trim(Format(TotalAcunsa, "###,###,###,##0.##")), 18)
  vsPrinter.Paragraph = Texto
  Texto = "TOTAL INSALUD... " & Right(Space(18) & Trim(Format(TotalInsalud, "###,###,###,##0.##")), 18)
  vsPrinter.Paragraph = Texto
  Texto = "TOTAL .......... " & Right(Space(18) & Trim(Format(TotalRemesa, "###,###,###,##0.##")), 18)
  vsPrinter.Paragraph = Texto
  Texto = "IMP. PDTE. COMP. " & Right(Space(18) & Trim(Format(TotalNoCompensado, "###,###,###,##0.##")), 18)
  vsPrinter.Paragraph = Texto
  Texto = "N� DE APUNTES... " & Right(Space(18) & NoAbonos, 18)
  vsPrinter.Paragraph = Texto
  vsPrinter.Action = 6
End Sub


Private Sub cmdListado_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim RsCant As rdoResultset
Dim SqLeNtI As String
Dim RsEnTi As rdoResultset
Dim Compensado As Double
Dim Cont As Integer
Dim SqL30 As String
Dim Rs30 As rdoResultset
  
  TotalRemesa = 0
  TotalAcunsa = 0
  TotalInsalud = 0
  TotalMutuas = 0
  totalPrivado = 0
  TotalNoCompensado = 0
  'Seleccionamos de la tabla de pago aquellos que tienen fecha de efecto menor que la fecha seleccionada
  'y la fecha de cierre de caja nula.
  MiSqL = "Select * from FA1700 Where FA17FECEFECTO Is Null AND FA17REMESA = '" & txtRemesa.Text & "'" & _
          " ORDER BY FA17REMESA, FA17APUNTE"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Cont = 0
    While Not MiRs.EOF
      'Seleccionamos el total compensado por cada uno de los pagos
      MiSqL = "Select NVL(SUM(FA18IMPCOMP),0) As Compensado From FA1800 Where FA17CODPAGO = " & MiRs("FA17CODPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        If Not IsNull(RsCant("COMPENSADO")) Then
          Compensado = CDbl(RsCant("Compensado"))
        Else
          Compensado = 0
        End If
      End If
      MiSqL = "Select NVL(SUM(FA51IMPCOMP),0) As Compensado From FA5100 Where FA17CODPAGO_POS = " & MiRs("FA17CODPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        If Not IsNull(RsCant("COMPENSADO")) Then
          Compensado = Compensado + CDbl(RsCant("Compensado"))
        End If
      End If
      MiSqL = "Select NVL(SUM(FA51IMPCOMP),0) As Compensado From FA5100 Where FA17CODPAGO_NEG = " & MiRs("FA17CODPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        If Not IsNull(RsCant("COMPENSADO")) Then
          Compensado = Compensado + CDbl(RsCant("Compensado"))
        End If
      End If
      Cont = Cont + 1
      If Not IsNull(MiRs("FA17CANTIDAD")) Then
        TotalRemesa = TotalRemesa + MiRs("FA17CANTIDAD")
      End If
      ReDim Preserve ArrayCheques(1 To 8, 1 To Cont)
      ArrayCheques(1, Cont) = "" & MiRs("FA17APUNTE")
      ArrayCheques(2, Cont) = MiRs("FA17CODPAGO")
      ArrayCheques(3, Cont) = MiRs("CI21CODPERSONA")
      ArrayCheques(4, Cont) = Format(MiRs("FA17FECPAGO"), "DD/MM/YYYY")
      MiSqL = "SELECT FA20DESIG FROM FA2000 WHERE FA20CODTIPPAGO = " & MiRs("FA20CODTIPPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        ArrayCheques(5, Cont) = RsCant("FA20DESIG") 'BUSCAR INICIALES
      Else
        ArrayCheques(5, Cont) = ""
      End If
      ArrayCheques(6, Cont) = MiRs("FA17CANTIDAD")
      If Compensado = 0 Then
        ArrayCheques(7, Cont) = "N"
      ElseIf CDbl(MiRs("FA17CANTIDAD")) > Compensado Then
        ArrayCheques(7, Cont) = "P"
      ElseIf CDbl(MiRs("FA17CANTIDAD")) = Compensado Then
        ArrayCheques(7, Cont) = "C"
      End If
      ArrayCheques(8, Cont) = CDbl(MiRs("FA17CANTIDAD")) - Compensado
      TotalNoCompensado = TotalNoCompensado + CDbl(MiRs("FA17CANTIDAD")) - Compensado
      If Not IsNull(MiRs("CI21CODPERSONA")) Then
        SqLeNtI = "Select * From CI2900 Where CI21CODPERSONA_REC = " & MiRs("CI21CODPERSONA")
        Set RsEnTi = objApp.rdoConnect.OpenResultset(SqLeNtI, 3)
        If Not RsEnTi.EOF Then
          If Not IsNull(RsEnTi("CI13CODENTIDAD")) Then
            'Comprobamos que no est� en la FA3000 (Conversi�n de tipos econ�micos
            SqL30 = "Select * From FA3000 Where CI13CODENTIDAD = '" & RsEnTi("CI13CODENTIDAD") & "'"
            Set Rs30 = objApp.rdoConnect.OpenResultset(SqL30, 3)
            If Not Rs30.EOF Then
              If Not IsNull(Rs30("CI32CODTIPECON_P")) Then
                If Rs30("CI32CODTIPECON_P") = "J" Then
                  TotalAcunsa = TotalAcunsa + CDbl(MiRs("FA17CANTIDAD"))
                ElseIf Rs30("CI32CODTIPECON_P") = "M" Then
                  TotalMutuas = TotalMutuas + CDbl(MiRs("FA17CANTIDAD"))
                ElseIf Rs30("CI32CODTIPECON_P") = "S" Then
                  TotalInsalud = TotalInsalud + CDbl(MiRs("FA17CANTIDAD"))
                End If
              End If
            Else
              If RsEnTi("CI32CODTIPECON") = "J" Then
                TotalAcunsa = TotalAcunsa + CDbl(MiRs("FA17CANTIDAD"))
              ElseIf RsEnTi("CI32CODTIPECON") = "M" Then
                TotalMutuas = TotalMutuas + CDbl(MiRs("FA17CANTIDAD"))
              ElseIf RsEnTi("CI32CODTIPECON") = "S" Then
                TotalInsalud = TotalInsalud + CDbl(MiRs("FA17CANTIDAD"))
              Else
                totalPrivado = totalPrivado + CDbl(MiRs("FA17CANTIDAD"))
              End If
            End If
          End If
        Else
          'No existe como responsable con lo cual lo metemos en privados
          totalPrivado = totalPrivado + CDbl(MiRs("FA17CANTIDAD"))
        End If
      End If
      MiRs.MoveNext
    Wend
    'MsgBox "La suma de los importes de los abonos es : " & Format(TotalRemesa, "###,###,###,##0.##")
    Call ImprimirListado
  Else
    MsgBox "No hay abonos pendientes de cierre a fecha " & Me.SDCFechaCierre.Date, vbInformation + vbOKOnly, "Aviso"
  End If
End Sub


Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Load()
Dim MiRsFecha
  
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    Me.SDCFechaCierre.Date = Format(MiRsFecha(0), "dd mmmm yyyy hh:mm:ss")
    FechaListado = Format(MiRsFecha(0), "dd mmmm yyyy hh:mm:ss")
  End If
End Sub


