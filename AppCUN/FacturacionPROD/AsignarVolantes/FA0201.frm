VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmPrincipal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Volantes de Osasunbidea"
   ClientHeight    =   2355
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7065
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   7065
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   465
      Left            =   5595
      TabIndex        =   1
      Top             =   1800
      Width           =   1290
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   465
      Left            =   5595
      TabIndex        =   0
      Top             =   120
      Width           =   1290
   End
   Begin ComctlLib.ListView lvwOpciones 
      Height          =   2295
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin ComctlLib.ListView lvwInformes 
      Height          =   2295
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   4048
      View            =   3
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
Dim item1 As ListItem
If lvwOpciones.Visible Then
  If lvwOpciones.SelectedItem.Selected Then
   Select Case lvwOpciones.SelectedItem.Tag
    Case 0
      Load frmCargaDisquetes
      frmCargaDisquetes.Show 1
    Case 1
      Load frmSeleccionProcesoAsistencia
      frmSeleccionProcesoAsistencia.Show 1
    Case 3
      Load frmProcesoAsistenciaExento
      frmProcesoAsistenciaExento.Show 1
    Case 4
      Load frmSeleccionEVol
      frmSeleccionEVol.Show 1
      
    Case 5
      lvwInformes.Visible = True
      lvwOpciones.Visible = False
     
  End Select
 Else
    MsgBox "Tiene que seleccionar un proceso", vbInformation + vbOKOnly
 End If
 
ElseIf lvwInformes.Visible Then
  If lvwInformes.SelectedItem.Selected Then
   Select Case lvwInformes.SelectedItem.Tag
    Case 6
       Load frminfProcesoAsistenciaPendienteVolante
       frminfProcesoAsistenciaPendienteVolante.Show 1
    Case 7
       Load frminfProcesoAsistenciaFacturaPendienteVolante
       frminfProcesoAsistenciaFacturaPendienteVolante.Show 1
    Case 8
       Load frminfVolantePendienteSerAsignado
       frminfVolantePendienteSerAsignado.Show 1
    Case 9
       Load frminfAsignacionVolanteProcesoAsistencia
       frminfAsignacionVolanteProcesoAsistencia.Show 1
       
    Case 10
       lvwInformes.Visible = False
       lvwOpciones.Visible = True
   End Select
  Else
   MsgBox "Tiene que seleccionar un proceso", vbInformation + vbOKOnly
  End If
End If

End Sub

Private Sub cmdCancelar_Click()

Unload Me
Set frmPrincipal = Nothing

End Sub

Private Sub Form_Load()
Dim item As ListItem

Dim i As Integer
lvwOpciones.ColumnHeaders.Add , , , 5000
lvwOpciones.ListItems.Clear
 
Set item = lvwOpciones.ListItems.Add(, , "Carga de volantes")
item.Tag = 0
Set item = lvwOpciones.ListItems.Add(, , "Asignaci�n de volantes pendientes")
item.Tag = 1
Set item = lvwOpciones.ListItems.Add(, , "Procesos-asistencia que no requieren volante")
item.Tag = 3
Set item = lvwOpciones.ListItems.Add(, , "Edici�n de volantes")
item.Tag = 4
Set item = lvwOpciones.ListItems.Add(, , "Informes")
item.Tag = 5
lvwInformes.ColumnHeaders.Add , , , 5000
Set item = lvwInformes.ListItems.Add(, , "Informe de procesos-asistencia pendientes de volante")
item.Tag = 6
Set item = lvwInformes.ListItems.Add(, , "Informe de procesos-asistencia de una factura pendientes de volante")
item.Tag = 7
Set item = lvwInformes.ListItems.Add(, , "Informe de volantes pendientes de ser asignados")
item.Tag = 8
Set item = lvwInformes.ListItems.Add(, , "Informe de volantes asignados entre dos fechas")
item.Tag = 9
Set item = lvwInformes.ListItems.Add(, , "Atr�s")
item.Tag = 10
End Sub
