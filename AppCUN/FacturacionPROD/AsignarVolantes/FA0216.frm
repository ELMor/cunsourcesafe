VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeleccionEVol 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Volantes de Osasunbidea"
   ClientHeight    =   8310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdActualizar 
      Caption         =   "&Actualizar"
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   7320
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2040
      TabIndex        =   19
      Top             =   7320
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Campos de b�squeda"
      ForeColor       =   &H00FF0000&
      Height          =   3135
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   11655
      Begin VB.Frame Frame2 
         Caption         =   "Por DNI"
         ForeColor       =   &H00FF0000&
         Height          =   1095
         Left            =   4680
         TabIndex        =   18
         Top             =   240
         Width           =   5055
         Begin VB.TextBox txtDNI 
            Height          =   285
            Left            =   1080
            TabIndex        =   5
            Top             =   480
            Width           =   2055
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "DNI:"
            Height          =   195
            Left            =   480
            TabIndex        =   22
            Top             =   480
            Width           =   330
         End
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   10560
         TabIndex        =   17
         Top             =   360
         Width           =   855
      End
      Begin VB.Frame Frame3 
         Caption         =   "Por Paciente"
         ForeColor       =   &H00FF0000&
         Height          =   2775
         Left            =   240
         TabIndex        =   11
         Top             =   240
         Width           =   4335
         Begin VB.TextBox txtBuscaApellido2 
            Height          =   285
            Left            =   1920
            TabIndex        =   2
            Top             =   960
            Width           =   2175
         End
         Begin VB.TextBox txtBuscaApellido1 
            Height          =   285
            Left            =   1920
            TabIndex        =   1
            Top             =   600
            Width           =   2175
         End
         Begin VB.TextBox txtBuscaNombre 
            Height          =   285
            Left            =   1920
            TabIndex        =   0
            Top             =   240
            Width           =   2175
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcBuscaFechaNacimiento 
            Height          =   255
            Left            =   1920
            TabIndex        =   3
            Top             =   1320
            Width           =   1815
            _Version        =   65543
            _ExtentX        =   3201
            _ExtentY        =   450
            _StockProps     =   93
            BackColor       =   -2147483634
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboBuscaSexo 
            Height          =   255
            Left            =   1920
            TabIndex        =   4
            Top             =   1680
            Width           =   1035
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196615
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Sexo"
            Columns(1).Name =   "Sexo"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   450
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Sexo"
            Height          =   195
            Left            =   360
            TabIndex        =   16
            Top             =   1680
            Width           =   360
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "F. de nacimiento"
            Height          =   195
            Left            =   360
            TabIndex        =   15
            Top             =   1320
            Width           =   1170
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Segundo Apellido"
            Height          =   195
            Left            =   360
            TabIndex        =   14
            Top             =   960
            Width           =   1245
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Primer apellido"
            Height          =   195
            Left            =   360
            TabIndex        =   13
            Top             =   600
            Width           =   1020
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            Height          =   195
            Left            =   360
            TabIndex        =   12
            Top             =   240
            Width           =   555
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Por n� de Seguridad Social"
         ForeColor       =   &H00FF0000&
         Height          =   1455
         Left            =   4680
         TabIndex        =   10
         Top             =   1560
         Width           =   5055
         Begin VB.TextBox txtCC 
            Height          =   285
            Left            =   1920
            TabIndex        =   7
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox txtSS 
            Height          =   285
            Left            =   1920
            TabIndex        =   6
            Top             =   360
            Width           =   1935
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Codigo de Control:"
            Height          =   195
            Left            =   240
            TabIndex        =   24
            Top             =   840
            Width           =   1305
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "N� Seguridad Social:"
            Height          =   195
            Left            =   240
            TabIndex        =   23
            Top             =   360
            Width           =   1470
         End
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "&Limpiar"
         Height          =   375
         Left            =   10560
         TabIndex        =   9
         Top             =   840
         Width           =   855
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdResultadoBus 
      Height          =   3615
      Left            =   120
      TabIndex        =   21
      Top             =   3600
      Width           =   11640
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20532
      _ExtentY        =   6376
      _StockProps     =   79
      Caption         =   "Resultado de la B�squeda"
      ForeColor       =   16711680
      BackColor       =   12632256
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboServicios 
      Height          =   1095
      Left            =   120
      TabIndex        =   25
      Top             =   4200
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Servicio"
      Columns(0).Name =   "Codigo Servicio"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Descripcion Servicio"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboTratamiento 
      Height          =   1095
      Left            =   2520
      TabIndex        =   26
      Top             =   4200
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Tratamiento"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboSexo 
      Height          =   1095
      Left            =   4920
      TabIndex        =   27
      Top             =   4200
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3201
      Columns(1).Name =   "Sexo"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboValido 
      Height          =   1095
      Left            =   7440
      TabIndex        =   28
      Top             =   4200
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Valido"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboFacturado 
      Height          =   1095
      Left            =   9360
      TabIndex        =   29
      Top             =   4200
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Facturado"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
End
Attribute VB_Name = "frmSeleccionEVol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Select2 As String

Private Sub cboBuscaSexo_InitColumnProps()
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""
End Sub

Private Sub cmdBuscar_Click()
  Dim strCondicion As String
  Dim strsql As String
  Dim rst1 As rdoResultset
  Dim formu As String
  
  Screen.MousePointer = vbHourglass
  
  'Si no se ha rellenado ningun campo de b�squeda
  If txtBuscaApellido1.Text = "" And _
    txtBuscaApellido2.Text = "" And _
    dtcBuscaFechaNacimiento.Text = "" And _
    txtDNI.Text = "" And txtSS.Text = "" And txtCC.Text = "" Then
    MsgBox "Debe rellenar al menos un campo de b�squeda.", vbOKOnly + vbInformation, "B�squeda nula"
    Screen.MousePointer = vbDefault
    
    Exit Sub
    
    
  Else
    strCondicion = "FA25CODREG<>0 "
    'Si se ha rellenado el campo Apellido1 se buscara apellidos1 que contengan
    'una transformacion (mediante la funcion TextoAlfa) del texto introducido en este campo.
    If txtBuscaApellido1.Text <> "" Then
      strCondicion = strCondicion & " AND FA25APEL1 LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaApellido1.Text) & "%'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If txtBuscaApellido2.Text <> "" Then
      strCondicion = strCondicion & " AND FA25APEL2 LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaApellido2.Text) & "%'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If txtBuscaNombre.Text <> "" Then
      strCondicion = strCondicion & " AND FA25NOMB LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaNombre.Text) & "%'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If dtcBuscaFechaNacimiento.Text <> "" Then
      strCondicion = strCondicion & " AND FA25FECHNAC = TO_DATE('"
      strCondicion = strCondicion & Format(dtcBuscaFechaNacimiento.Text, "DD/MM/YYYY") & "','DD/MM/YYYY') "
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If cboBuscaSexo.Text <> "" Then
      strCondicion = strCondicion & " AND FA25SEXO ='"
      strCondicion = strCondicion & cboBuscaSexo.Columns(1).Text & "'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
   If txtDNI.Text <> "" Then
     strCondicion = strCondicion & " AND FA25DNI=" & txtDNI.Text
     Call objPipe.PipeSet("DNI", txtDNI.Text)
   End If
   If txtSS.Text <> "" And txtCC.Text <> "" Then
     strCondicion = strCondicion & " AND FA25NUMSEGSOC" & txtSS.Text
     strCondicion = strCondicion & " AND FA25CODCTL=" & txtCC.Text
     Call objPipe.PipeSet("SS", txtSS.Text)
     Call objPipe.PipeSet("CC", txtCC.Text)
   End If
   
strsql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25DNI,FA25FECHNAC"
strsql = strsql & ",FA25NUMSEGSOC,FA25CODCTL,FA25SEXO,AD02CODDPTO"
strsql = strsql & ",FA33CODTRATAM,FA25FECRECEP,FA25EXPINF,FA25IDCLIENTE,FA25CODPOSTAL"
strsql = strsql & ",FA25VALID,FA25FACTUR,AD01CODASISTENCI,AD07CODPROCESO,FA25CODREG"
strsql = strsql & " FROM FA2500 WHERE " & strCondicion
Set rst1 = objApp.rdoConnect.OpenResultset(strsql, 2)
Do While Not rst1.EOF
   grdResultadoBus.AddItem rst1!FA25NOMB & Chr$(9) _
   & rst1!FA25APEL1 & Chr$(9) _
   & rst1!FA25APEL2 & Chr$(9) _
   & rst1!FA25DNI & Chr$(9) _
   & rst1!FA25FECHNAC & Chr$(9) _
   & rst1!FA25NUMSEGSOC & rst1!FA25CODCTL & Chr$(9) _
   & rst1!FA25SEXO & Chr$(9) _
   & rst1!AD02CODDPTO & Chr$(9) _
   & rst1!FA33CODTRATAM & Chr$(9) _
   & rst1!FA25FECRECEP & Chr$(9) _
   & rst1!FA25EXPINF & Chr$(9) _
   & rst1!FA25IDCLIENTE & Chr$(9) _
   & rst1!FA25CODPOSTAL & Chr$(9) _
   & rst1!FA25VALID & Chr$(9) _
   & rst1!FA25FACTUR & Chr$(9) _
   & rst1(16) & Chr$(9) _
   & rst1(17) & Chr$(9) _
   & rst1(18)
  rst1.MoveNext
 Loop
 
 
rst1.Close
End If

Screen.MousePointer = vbDefault
Select2 = strsql

End Sub

Private Sub cmdCancelar_Click()
Unload Me
Set frmSeleccionEVol = Nothing

End Sub


Private Sub cmdActualizar_Click()
Dim myupdate As String
Dim qry As rdoQuery
Dim i As Long
For i = 0 To grdResultadoBus.Rows - 1
 grdResultadoBus.Update
 
myupdate = "UPDATE FA2500 SET FA25NOMB=?,FA25APEL1=?,FA25APEL2=?,FA25FECHNAC=TO_DATE(?,'DD/MM/YYYY'),AD02CODDPTO=?,FA33CODTRATAM=?,FA25FECRECEP=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),FA25EXPINF=?,FA25IDCLIENTE=?,FA25CODPOSTAL=?,FA25VALID=?,FA25FACTUR=? WHERE FA25CODREG="
myupdate = myupdate & grdResultadoBus.Columns("Registro").CellValue(grdResultadoBus.RowBookmark(i))
Set qry = objApp.rdoConnect.CreateQuery("", myupdate)
qry(0) = grdResultadoBus.Columns("Nombre").CellValue(i)
qry(1) = grdResultadoBus.Columns("1� Apellido").CellValue(i)
qry(2) = grdResultadoBus.Columns("2� Apellido").CellValue(i)
qry(3) = grdResultadoBus.Columns("Fecha nac.").CellValue(i)
qry(4) = grdResultadoBus.Columns("Servicio").CellValue(i)
qry(5) = grdResultadoBus.Columns("Tratamiento").CellValue(i)
qry(6) = grdResultadoBus.Columns("F. recepcion").CellValue(i)
qry(7) = grdResultadoBus.Columns("N� informe").CellValue(i)
qry(8) = grdResultadoBus.Columns("Id. paciente").CellValue(i)
qry(9) = grdResultadoBus.Columns("CIP").CellValue(i)
qry(10) = grdResultadoBus.Columns("Valido").CellValue(i)
qry(11) = grdResultadoBus.Columns("Facturado").CellValue(i)
qry.Execute

qry.Close
 
Next i

End Sub



Private Sub cmdLimpiar_Click()
txtBuscaNombre = ""
txtBuscaApellido1 = ""
txtBuscaApellido2 = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""

txtBuscaNombre.SetFocus
grdResultadoBus.RemoveAll

End Sub



Private Sub dtcBuscaFechaNacimiento_Change()
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""
End Sub

Private Sub dtcBuscaFechaNacimiento_Click()
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""
End Sub

Private Sub Form_Load()
With grdResultadoBus
  .Columns(0).Caption = "Nombre"
  .Columns(0).Width = "2000"
  .Columns(1).Caption = "1� apellido"
  .Columns(1).Width = "1300"
  .Columns(2).Caption = "2� apellido"
  .Columns(2).Width = "1400"
  .Columns(3).Caption = "DNI"
  .Columns(3).Width = "1000"
  .Columns(4).Caption = "Fecha nac."
  .Columns(4).Width = "1300"
  .Columns(5).Caption = "Num. seg. soc"
  .Columns(5).Width = "1200"
  .Columns(5).BackColor = objApp.objUserColor.lngReadOnly
  .Columns(5).Locked = True
  .Columns(6).Caption = "Sexo"
  .Columns(6).Width = "900"
  .Columns(6).DropDownHwnd = dcboSexo.hWnd
  .Columns(7).Caption = "Servicio"
  .Columns(7).Width = "1400"
  .Columns(7).DropDownHwnd = dcboServicios.hWnd
  .Columns(8).Caption = "Tratamiento"
  .Columns(8).Width = "1200"
  .Columns(8).DropDownHwnd = dcboTratamiento.hWnd
  .Columns(9).Caption = "F. recepcion"
  .Columns(9).Width = "1300"
  .Columns(10).Caption = "N� Informe"
  .Columns(10).Width = "1000"
  .Columns(11).Caption = "Id. paciente"
  .Columns(11).Width = "1300"
  .Columns(12).Caption = "CIP"
  .Columns(12).Width = "1200"
  .Columns(13).Caption = "Valido"
  .Columns(13).Width = "600"
  .Columns(13).DropDownHwnd = dcboValido.hWnd
  .Columns(14).Caption = "Facturado"
  .Columns(14).Width = "600"
  .Columns(14).DropDownHwnd = dcboFacturado.hWnd
  .Columns(15).Caption = "Asistencia"
  .Columns(15).Visible = False
  .Columns(16).Caption = "Proceso"
  .Columns(16).Visible = False
  .Columns(17).Caption = "Registro"
  .Columns(17).Visible = True
  .Columns(17).Width = 1000
End With
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
Call Cargar_DropDowns

End Sub
Private Sub grdResultadoBus_Click()
 grdResultadoBus.SelBookmarks.RemoveAll
 grdResultadoBus.SelBookmarks.Add (grdResultadoBus.RowBookmark(grdResultadoBus.Row))
 
End Sub

Private Sub Cargar_DropDowns()
Dim sql1 As String
Dim rst1 As rdoResultset
sql1 = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200"
Set rst1 = objApp.rdoConnect.OpenResultset(sql1)
Do While Not rst1.EOF
  dcboServicios.AddItem rst1(0) & Chr$(9) & rst1(1)
  rst1.MoveNext
Loop
rst1.Close
sql1 = "SELECT FA33CODTRATAM,FA33DESCTRATAM FROM FA3300"
Set rst1 = objApp.rdoConnect.OpenResultset(sql1)
Do While Not rst1.EOF
  dcboTratamiento.AddItem rst1(0) & Chr$(9) & rst1(1)
  rst1.MoveNext
Loop
rst1.Close
dcboSexo.AddItem "1" & Chr$(9) & "Hombre"
dcboSexo.AddItem "2" & Chr$(9) & "Mujer"
cboBuscaSexo.AddItem "1" & Chr$(9) & "Hombre"
cboBuscaSexo.AddItem "2" & Chr$(9) & "Mujer"
dcboValido.AddItem "-1" & Chr$(9) & "Si"
dcboValido.AddItem "0" & Chr$(9) & "No"
dcboFacturado.AddItem "-1" & Chr$(9) & "Si"
dcboFacturado.AddItem "0" & Chr$(9) & "No"
End Sub

Private Sub txtBuscaApellido1_Change()
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""
End Sub

Private Sub txtBuscaApellido2_Change()
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""
End Sub

Private Sub txtBuscaNombre_Change()
txtDNI.Text = ""
txtSS.Text = ""
txtCC.Text = ""

End Sub

Private Sub txtCC_Change()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
txtDNI.Text = ""

End Sub

Private Sub txtDNI_Change()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
txtSS.Text = ""
txtCC.Text = ""

End Sub

Private Sub txtSS_Change()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
txtDNI.Text = ""

End Sub
