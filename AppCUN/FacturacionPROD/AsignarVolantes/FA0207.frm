VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVolante 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Volantes de Osasunbidea"
   ClientHeight    =   5115
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5115
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRefrescar 
      Caption         =   "&Refrescar"
      Height          =   375
      Left            =   4920
      TabIndex        =   11
      Top             =   3840
      Width           =   1695
   End
   Begin VB.CommandButton cmdFin 
      Caption         =   "&Fin"
      Height          =   375
      Left            =   3720
      TabIndex        =   10
      Top             =   3840
      Width           =   855
   End
   Begin VB.CommandButton cmdInicio 
      Caption         =   "&Principio"
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton cmdSiguiente 
      Caption         =   "&Siguiente"
      Height          =   375
      Left            =   2520
      TabIndex        =   8
      Top             =   3840
      Width           =   855
   End
   Begin VB.CommandButton cmdAnterior 
      Caption         =   "&Anterior"
      Height          =   375
      Left            =   1560
      TabIndex        =   7
      Top             =   3840
      Width           =   855
   End
   Begin SSDataWidgets_B.SSDBGrid grdVolantes 
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   11640
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20532
      _ExtentY        =   5106
      _StockProps     =   79
      ForeColor       =   8388608
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboServicios 
      Height          =   1095
      Left            =   360
      TabIndex        =   2
      Top             =   3840
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Servicio"
      Columns(0).Name =   "Codigo Servicio"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Descripcion Servicio"
      Columns(1).Name =   "Descripcion Servicio"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboTratamiento 
      Height          =   1095
      Left            =   2760
      TabIndex        =   3
      Top             =   3840
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Tratamiento"
      Columns(1).Name =   "Tratamiento"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboSexo 
      Height          =   1095
      Left            =   5160
      TabIndex        =   4
      Top             =   3840
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3201
      Columns(1).Name =   "Sexo"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboValido 
      Height          =   1095
      Left            =   7680
      TabIndex        =   5
      Top             =   3840
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Valido"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboFacturado 
      Height          =   1095
      Left            =   9600
      TabIndex        =   6
      Top             =   3840
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Facturado"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Edici�n de Volantes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   360
      TabIndex        =   1
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "frmVolante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As rdoResultset
Dim pos As Long


Private Sub cmdAnterior_Click()



If rst.BOF = True Then
  Exit Sub
Else
rst.MovePrevious
Screen.MousePointer = vbHourglass

 grdVolantes.RemoveAll
 If Not rst.BOF Then
   grdVolantes.AddItem rst!FA25NOMB & Chr$(9) _
   & rst!FA25APEL1 & Chr$(9) _
   & rst!FA25APEL2 & Chr$(9) _
   & rst!FA25DNI & Chr$(9) _
   & rst!FA25FECHNAC & Chr$(9) _
   & rst!FA25NUMSEGSOC & rst!FA25CODCTL & Chr$(9) _
   & rst!FA25SEXO & Chr$(9) _
   & rst!AD02CODDPTO & Chr$(9) _
   & rst!FA33CODTRATAM & Chr$(9) _
   & rst!FA25FECRECEP & Chr$(9) _
   & rst!FA25EXPINF & Chr$(9) _
   & rst!FA25IDCLIENTE & Chr$(9) _
   & rst!FA25CODPOSTAL & Chr$(9) _
   & rst!FA25VALID & Chr$(9) _
   & rst!FA25FACTUR & Chr$(9) _
   & rst(16) & Chr$(9) _
   & rst(17) & Chr$(9) _
   & rst(18)
   pos = pos - 1
End If
Screen.MousePointer = vbDefault
End If

End Sub

Private Sub cmdFin_Click()

If Not rst.EOF Then
Screen.MousePointer = vbHourglass
  rst.MoveLast
  grdVolantes.RemoveAll
  
  If Not rst.EOF Then
   grdVolantes.AddItem rst!FA25NOMB & Chr$(9) _
   & rst!FA25APEL1 & Chr$(9) _
   & rst!FA25APEL2 & Chr$(9) _
   & rst!FA25DNI & Chr$(9) _
   & rst!FA25FECHNAC & Chr$(9) _
   & rst!FA25NUMSEGSOC & rst!FA25CODCTL & Chr$(9) _
   & rst!FA25SEXO & Chr$(9) _
   & rst!AD02CODDPTO & Chr$(9) _
   & rst!FA33CODTRATAM & Chr$(9) _
   & rst!FA25FECRECEP & Chr$(9) _
   & rst!FA25EXPINF & Chr$(9) _
   & rst!FA25IDCLIENTE & Chr$(9) _
   & rst!FA25CODPOSTAL & Chr$(9) _
   & rst!FA25VALID & Chr$(9) _
   & rst!FA25FACTUR & Chr$(9) _
   & rst(16) & Chr$(9) _
   & rst(17) & Chr$(9) _
   & rst(18)
   pos = pos + 1
End If
Screen.MousePointer = vbDefault
Else
 Exit Sub
End If



End Sub

Private Sub cmdInicio_Click()

If Not rst.BOF Then
Screen.MousePointer = vbHourglass
  rst.MoveFirst
  grdVolantes.RemoveAll
  
  If Not rst.EOF Then
   grdVolantes.AddItem rst!FA25NOMB & Chr$(9) _
   & rst!FA25APEL1 & Chr$(9) _
   & rst!FA25APEL2 & Chr$(9) _
   & rst!FA25DNI & Chr$(9) _
   & rst!FA25FECHNAC & Chr$(9) _
   & rst!FA25NUMSEGSOC & rst!FA25CODCTL & Chr$(9) _
   & rst!FA25SEXO & Chr$(9) _
   & rst!AD02CODDPTO & Chr$(9) _
   & rst!FA33CODTRATAM & Chr$(9) _
   & rst!FA25FECRECEP & Chr$(9) _
   & rst!FA25EXPINF & Chr$(9) _
   & rst!FA25IDCLIENTE & Chr$(9) _
   & rst!FA25CODPOSTAL & Chr$(9) _
   & rst!FA25VALID & Chr$(9) _
   & rst!FA25FACTUR & Chr$(9) _
   & rst(16) & Chr$(9) _
   & rst(17) & Chr$(9) _
   & rst(18)
   pos = pos + 1
End If
Screen.MousePointer = vbDefault
Else
 Exit Sub
End If

End Sub

Private Sub cmdRefrescar_Click()
rst.Close

Call Cargar_Grid

End Sub

Private Sub cmdSiguiente_Click()

Dim myupdate As String
Dim qry As rdoQuery
If grdVolantes.Columns("Nombre").Text <> "" Then
myupdate = "UPDATE FA2500 SET FA25NOMB=?,FA25APEL1=?,FA25APEL2=?,FA25FECHNAC=TO_DATE(?,'DD/MM/YYYY'),AD02CODDPTO=?,FA33CODTRATAM=?,FA25FECRECEP=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),FA25EXPINF=?,FA25IDCLIENTE=?,FA25CODPOSTAL=?,FA25VALID=?,FA25FACTUR=? WHERE FA25CODREG="
myupdate = myupdate & grdVolantes.Columns("Registro").Text
Set qry = objApp.rdoConnect.CreateQuery("", myupdate)
qry(0) = grdVolantes.Columns("Nombre").Text
qry(1) = grdVolantes.Columns("1� Apellido").Text
qry(2) = grdVolantes.Columns("2� Apellido").Text
qry(3) = grdVolantes.Columns("Fecha nac.").Text
qry(4) = grdVolantes.Columns("Servicio").Text
qry(5) = grdVolantes.Columns("Tratamiento").Text
qry(6) = grdVolantes.Columns("F. recepcion").Text
qry(7) = grdVolantes.Columns("N� informe").Text
qry(8) = grdVolantes.Columns("Id. paciente").Text
qry(9) = grdVolantes.Columns("CIP").Text
qry(10) = grdVolantes.Columns("Valido").Text
qry(11) = grdVolantes.Columns("Facturado").Text
qry.Execute

qry.Close
End If

If rst.EOF Then
  Exit Sub
Else
Screen.MousePointer = vbHourglass
rst.MoveNext

 grdVolantes.RemoveAll
 If Not rst.EOF Then
   grdVolantes.AddItem rst!FA25NOMB & Chr$(9) _
   & rst!FA25APEL1 & Chr$(9) _
   & rst!FA25APEL2 & Chr$(9) _
   & rst!FA25DNI & Chr$(9) _
   & rst!FA25FECHNAC & Chr$(9) _
   & rst!FA25NUMSEGSOC & rst!FA25CODCTL & Chr$(9) _
   & rst!FA25SEXO & Chr$(9) _
   & rst!AD02CODDPTO & Chr$(9) _
   & rst!FA33CODTRATAM & Chr$(9) _
   & rst!FA25FECRECEP & Chr$(9) _
   & rst!FA25EXPINF & Chr$(9) _
   & rst!FA25IDCLIENTE & Chr$(9) _
   & rst!FA25CODPOSTAL & Chr$(9) _
   & rst!FA25VALID & Chr$(9) _
   & rst!FA25FACTUR & Chr$(9) _
   & rst(16) & Chr$(9) _
   & rst(17) & Chr$(9) _
   & rst(18)
   pos = pos + 1
End If
Screen.MousePointer = vbDefault
End If

End Sub


Private Sub Form_Load()
pos = 0
Call Formatear_Grid
Call Cargar_DropDowns
Call Cargar_Grid




End Sub
Private Sub Formatear_Grid()
With grdVolantes
  .Columns(0).Caption = "Nombre"
  .Columns(0).Width = "2000"
  .Columns(1).Caption = "1� apellido"
  .Columns(1).Width = "1300"
  .Columns(2).Caption = "2� apellido"
  .Columns(2).Width = "1400"
  .Columns(3).Caption = "DNI"
  .Columns(3).Width = "1000"
  .Columns(4).Caption = "Fecha nac."
  .Columns(4).Width = "1300"
  .Columns(5).Caption = "Num. seg. soc"
  .Columns(5).Width = "1200"
  .Columns(6).Caption = "Sexo"
  .Columns(6).Width = "900"
  .Columns(6).DropDownHwnd = dcboSexo.hWnd
  .Columns(7).Caption = "Servicio"
  .Columns(7).Width = "1400"
  .Columns(7).DropDownHwnd = dcboServicios.hWnd
  .Columns(8).Caption = "Tratamiento"
  .Columns(8).Width = "1200"
  .Columns(8).DropDownHwnd = dcboTratamiento.hWnd
  .Columns(9).Caption = "F. recepcion"
  .Columns(9).Width = "1300"
  .Columns(10).Caption = "N� Informe"
  .Columns(10).Width = "1000"
  .Columns(11).Caption = "Id. paciente"
  .Columns(11).Width = "1300"
  .Columns(12).Caption = "CIP"
  .Columns(12).Width = "1200"
  .Columns(13).Caption = "Valido"
  .Columns(13).Width = "600"
  .Columns(13).DropDownHwnd = dcboValido.hWnd
  .Columns(14).Caption = "Facturado"
  .Columns(14).Width = "600"
  .Columns(14).DropDownHwnd = dcboFacturado.hWnd
  .Columns(15).Caption = "Asistencia"
  .Columns(15).Visible = False
  .Columns(16).Caption = "Proceso"
  .Columns(16).Visible = False
  .Columns(17).Caption = "Registro"
  .Columns(17).Visible = False
End With

End Sub
Private Sub Cargar_DropDowns()
Dim sql1 As String
Dim rst1 As rdoResultset
sql1 = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200"
Set rst1 = objApp.rdoConnect.OpenResultset(sql1)
Do While Not rst1.EOF
  dcboServicios.AddItem rst1(0) & Chr$(9) & rst1(1)
  rst1.MoveNext
Loop
rst1.Close
sql1 = "SELECT FA33CODTRATAM,FA33DESCTRATAM FROM FA3300"
Set rst1 = objApp.rdoConnect.OpenResultset(sql1)
Do While Not rst1.EOF
  dcboTratamiento.AddItem rst1(0) & Chr$(9) & rst1(1)
  rst1.MoveNext
Loop
rst1.Close
dcboSexo.AddItem "1" & Chr$(9) & "Hombre"
dcboSexo.AddItem "2" & Chr$(9) & "Mujer"
dcboValido.AddItem "-1" & Chr$(9) & "Si"
dcboValido.AddItem "0" & Chr$(9) & "No"
dcboFacturado.AddItem "-1" & Chr$(9) & "Si"
dcboFacturado.AddItem "0" & Chr$(9) & "No"
End Sub
Private Sub Cargar_Grid()
Dim sql As String
grdVolantes.RemoveAll

sql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25DNI,FA25FECHNAC"
sql = sql & ",FA25NUMSEGSOC,FA25CODCTL,FA25SEXO,AD02CODDPTO"
sql = sql & ",FA33CODTRATAM,FA25FECRECEP,FA25EXPINF,FA25IDCLIENTE,FA25CODPOSTAL"
sql = sql & ",FA25VALID,FA25FACTUR,AD01CODASISTENCI,AD07CODPROCESO,FA25CODREG"
sql = sql & " FROM FA2500"
Set rst = objApp.rdoConnect.OpenResultset(sql, 2)
If Not rst.EOF Then
   grdVolantes.AddItem rst!FA25NOMB & Chr$(9) _
   & rst!FA25APEL1 & Chr$(9) _
   & rst!FA25APEL2 & Chr$(9) _
   & rst!FA25DNI & Chr$(9) _
   & rst!FA25FECHNAC & Chr$(9) _
   & rst!FA25NUMSEGSOC & rst!FA25CODCTL & Chr$(9) _
   & rst!FA25SEXO & Chr$(9) _
   & rst!AD02CODDPTO & Chr$(9) _
   & rst!FA33CODTRATAM & Chr$(9) _
   & rst!FA25FECRECEP & Chr$(9) _
   & rst!FA25EXPINF & Chr$(9) _
   & rst!FA25IDCLIENTE & Chr$(9) _
   & rst!FA25CODPOSTAL & Chr$(9) _
   & rst!FA25VALID & Chr$(9) _
   & rst!FA25FACTUR & Chr$(9) _
   & rst(16) & Chr$(9) _
   & rst(17) & Chr$(9) _
   & rst(18)
   
   
   pos = pos + 1
End If
 
 
End Sub
