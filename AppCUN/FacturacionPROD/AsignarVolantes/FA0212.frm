VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmProcesoAsistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asignaci�n de volantes de Osansubidea"
   ClientHeight    =   8190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8190
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBuscarProc 
      Caption         =   "&Busqueda Proceso-Asist"
      Height          =   855
      Left            =   10680
      TabIndex        =   10
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "B&uscar m�s Volantes"
      Height          =   615
      Left            =   10680
      TabIndex        =   8
      Top             =   4680
      Width           =   1095
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "&Asignar"
      Height          =   375
      Left            =   10680
      TabIndex        =   7
      Top             =   4200
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboServicios 
      Height          =   1095
      Left            =   2760
      TabIndex        =   4
      Top             =   4800
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Servicio"
      Columns(0).Name =   "Codigo Servicio"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Descripcion Servicio"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "column 1"
   End
   Begin VB.CommandButton cmdDesasignarVolante 
      Caption         =   "&Desasignar volante"
      Height          =   615
      Left            =   10680
      TabIndex        =   1
      Top             =   5880
      Width           =   1095
   End
   Begin VB.CommandButton cmdNoRequiereVolante 
      Caption         =   "&No requiere Volante"
      Height          =   735
      Left            =   10680
      TabIndex        =   0
      Top             =   960
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid grdVolCan 
      Height          =   1575
      Left            =   240
      TabIndex        =   2
      Top             =   4200
      Width           =   10320
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18203
      _ExtentY        =   2778
      _StockProps     =   79
      Caption         =   "Volantes candidatos"
      ForeColor       =   8388608
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdVolAsig 
      Height          =   2055
      Left            =   240
      TabIndex        =   3
      Top             =   5880
      Width           =   10320
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18203
      _ExtentY        =   3625
      _StockProps     =   79
      Caption         =   "Volantes asignado"
      ForeColor       =   8388608
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboTratamiento 
      Height          =   1095
      Left            =   5400
      TabIndex        =   5
      Top             =   4800
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Name =   "Tratamiento"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "column 1"
   End
   Begin SSDataWidgets_B.SSDBGrid grdProcAsist 
      Height          =   3135
      Left            =   240
      TabIndex        =   6
      Top             =   960
      Width           =   10320
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18203
      _ExtentY        =   5530
      _StockProps     =   79
      Caption         =   "Proceso-Asistencia"
      ForeColor       =   8388608
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   360
      TabIndex        =   9
      Top             =   240
      Width           =   90
   End
End
Attribute VB_Name = "frmProcesoAsistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAsignar_Click()
    Dim strsql As String
    Dim strsql3 As String
    Dim strsql2 As String
    Dim rst As rdoResultset
    Dim rst1 As rdoResultset
    Dim rst3 As rdoResultset
    Dim blnAsignar As Boolean
    Dim lngRespuesta As Long
    Dim strMsg As String
    Dim strTitulo As String
        
    
    
    blnAsignar = True
    
    strsql2 = "SELECT * FROM FA3200 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
    strsql2 = strsql2 & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
    
    Set rst = objApp.rdoConnect.OpenResultset(strsql2)
    
    strsql3 = "SELECT * FROM FA2500 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
    strsql3 = strsql3 & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
    
    Set rst3 = objApp.rdoConnect.OpenResultset(strsql3)
    
    If (grdProcAsist.Columns("Asistencia").Text = "" Or grdProcAsist.Columns("Proceso").Text = "") Or grdVolCan.Columns("Registro").Text = "" Then
        blnAsignar = False
        strMsg = "Imposible asignar volante con proceso-asistencia." & Chr$(13)
        strMsg = strMsg & "Debe elegir un proceso-asistencia y un volante."
        strTitulo = "Imposible asignaci�n"
        MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
    ElseIf Not rst.EOF Then
        blnAsignar = False
        strMsg = "Imposible asignar volante con proceso-asistencia." & Chr$(13)
        strMsg = strMsg & "El proceso-asistencia est� marcado que no requiere volante. Primero debe desmarcar el proceso-asistencia."
        strTitulo = "Imposible asignaci�n"
        MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
    ElseIf Not rst3.EOF Then
        blnAsignar = False
        strMsg = "Imposible asignar volante con proceso-asistencia." & Chr$(13)
        strMsg = strMsg & "El proceso-asistencia ya est� asignado a otro volante. Primero debe desasignar el proceso-asistencia y su volante."
        strTitulo = "Imposible asignaci�n"
        MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
    ElseIf grdVolCan.Columns("Asistencia").Text <> "" Then
        lngRespuesta = MsgBox("Este volante ya est� asignado a otro proceso-asistencia. �Est� seguro de que quiere reasignarlo?", vbYesNo + vbQuestion, "Reasignar un volante")
        If Not (vbYes = lngRespuesta) Then
            blnAsignar = False
        ElseIf grdVolCan.Columns("Facturado").Text = -1 Then
            lngRespuesta = MsgBox("Este volante ya est� facturado. �Est� seguro de que quiere reasignarlo?", vbYesNo + vbQuestion, "Reasignar un volante")
            If Not (vbYes = lngRespuesta) Then
                blnAsignar = False
            End If
        End If
    End If
    
    If blnAsignar Then
        strsql = "UPDATE FA2500 SET AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
        strsql = strsql & ", AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
        strsql = strsql & ", FA25FECASIGN = TO_DATE('" & Format(FechaHora(), "DD/MM/YYYY HH:MM:SS")
        strsql = strsql & "','DD/MM/YYYY HH24:MI:SS') WHERE FA25CODREG = " & grdVolCan.Columns("Registro").Text
        
        
        Set rst1 = objApp.rdoConnect.OpenResultset(strsql)
        Call grdProcAsist_Click
    End If
End Sub

Private Sub cmdBuscar_Click()

  Load frmSeleccionVolante
  frmSeleccionVolante.Show 1
  If Aceptar = True Then
    Aceptar = False
    Call pActualizar1
  End If
  
End Sub

Private Sub cmdBuscarProc_Click()
Unload Me
Set frmProcesoAsistencia = Nothing
Load frmSeleccionProcesoAsistencia
frmSeleccionProcesoAsistencia.Show 1
End Sub

Private Sub cmdDesasignarVolante_Click()
    Dim strsql As String
    Dim strsql2 As String
    Dim rst As rdoResultset

    Dim blnDesasignar As Boolean
    Dim lngRespuesta As Long
    Dim strMsg As String
    Dim strTitulo As String
        
   
    blnDesasignar = True
    
    strsql2 = "SELECT * FROM FA2500 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
    
    strsql2 = strsql2 & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
    Set rst = objApp.rdoConnect.OpenResultset(strsql2)
    
    If rst.EOF Then
        blnDesasignar = False
        strMsg = "Imposible desasignar el proceso-asistencia y su volante." & Chr$(13)
        strMsg = strMsg & "El proceso-asistencia no est� asignado a ning�n volante."
        strTitulo = "Imposible desasignar volante"
        MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
    Else
        rst.Close
    
        strsql2 = "SELECT * FROM FA2500 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
        strsql2 = strsql2 & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text & " AND FA25VALID = -1 AND FA25FACTUR = -1"
        Set rst = objApp.rdoConnect.OpenResultset(strsql2)
    
        If Not rst.EOF Then
            blnDesasignar = False
            strMsg = "Imposible desasignar el proceso-asistencia y su volante." & Chr$(13)
            strMsg = strMsg & "El proceso-asistencia ya est� facturado."
            strTitulo = "Imposible desasignar volante"
            MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
        End If
    End If
    rst.Close
        
    If blnDesasignar Then
        strsql = "UPDATE FA2500 SET AD01CODASISTENCI = Null, AD07CODPROCESO = Null, FA25FECASIGN = Null"
        strsql = strsql & " WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
        strsql = strsql & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        
        Call grdProcAsist_Click
    End If
End Sub


Private Sub cmdNoRequiereVolante_Click()
    Dim strsql As String
'    Dim qdf As QueryDef
    Dim strsql2 As String
    Dim rst As rdoResultset
'    Dim dbs As Database
    Dim blnRequiere As Boolean
    Dim lngRespuesta As Long
    Dim strMsg As String
    Dim strTitulo As String
        
      blnRequiere = True
    
    strsql2 = "SELECT * FROM FA2500 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
    strsql2 = strsql2 & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text & " AND FA25VALID = -1"
    Set rst = objApp.rdoConnect.OpenResultset(strsql2)
    
    If Not rst.EOF Then
        blnRequiere = False
        strMsg = "Imposible indicar que el proceso-asistencia no requiere volante." & Chr$(13)
        strMsg = strMsg & "El proceso-asistencia est� asignado a un volante. Primero debe deshacer la asignaci�n."
        strTitulo = "Imposible indicar no requiere volante"
        MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
    End If
    rst.Close
    
    strsql2 = "SELECT * FROM FA3200 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
    strsql2 = strsql2 & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
    Set rst = objApp.rdoConnect.OpenResultset(strsql2)
    
    If Not rst.EOF Then
        blnRequiere = False
        strMsg = "Imposible indicar que el proceso-asistencia no requiere volante." & Chr$(13)
        strMsg = strMsg & "El proceso-asistencia ya est� marcado que no requiere volante."
        strTitulo = "Imposible indicar no requiere volante"
        MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
    End If
    rst.Close
        
    If blnRequiere Then
      lngRespuesta = MsgBox("�Est� seguro de que quiere indicar que el proceso-asistencia no requiere volante?", vbYesNo + vbQuestion, "No requiere volante")
      If vbYes = lngRespuesta Then

        strsql = "INSERT INTO FA3200(AD01CODASISTENCI, AD07CODPROCESO) VALUES (" & grdProcAsist.Columns("Asistencia").Text
        strsql = strsql & ", " & grdProcAsist.Columns("Proceso").Text & ")"
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        grdProcAsist.DeleteSelected
        
      End If
    End If
End Sub











Private Sub Form_Load()
Call Formatear_Grids
If objPipe.PipeExist("Busqueda") Then
   lblTitulo.Caption = "B�squeda por Paciente"
   objPipe.PipeRemove ("Busqueda")
ElseIf objPipe.PipeExist("fecini") And objPipe.PipeExist("fecfin") Then
   lblTitulo.Caption = "B�squeda por fechas,Desde " & objPipe.PipeGet("fecini") & " y " & objPipe.PipeGet("fecfin")
   objPipe.PipeRemove ("fecini")
   objPipe.PipeRemove ("fecfin")
ElseIf objPipe.PipeExist("Nfactura") Then
   lblTitulo.Caption = "B�squeda por factura: " & objPipe.PipeGet("Nfactura")
   objPipe.PipeRemove ("Nfactura")
End If

Call Cargar_ProcAsist(-1)
Call Cargar_DropDowns


End Sub
Private Sub Cargar_DropDowns()
Dim sql1 As String
Dim sql2 As String
Dim rst2 As rdoResultset
Dim rst3 As rdoResultset
sql1 = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD32CODTIPODPTO IN(1,2)"
Set rst2 = objApp.rdoConnect.OpenResultset(sql1)
Do While Not rst2.EOF
   dcboServicios.AddItem rst2(0) & Chr$(9) & rst2(1)
   rst2.MoveNext
Loop
rst2.Close
sql2 = "SELECT FA33CODTRATAM,FA33DESCTRATAM FROM FA3300"
Set rst3 = objApp.rdoConnect.OpenResultset(sql2)
Do While Not rst3.EOF
   dcboTratamiento.AddItem rst3(0) & Chr$(9) & rst3(1)
   rst3.MoveNext
Loop

End Sub
Private Sub Formatear_Grids()
With grdProcAsist
  .Columns(0).Caption = "Nombre y Apellido"
  .Columns(0).Width = 2600
  .Columns(1).Caption = "DNI"
  .Columns(1).Width = 1000
  .Columns(2).Caption = "Fec. Nac."
  .Columns(2).Width = 1000
  .Columns(3).Caption = "N� Seg. Soc."
  .Columns(3).Width = 1200
  .Columns(4).Caption = "Servicio"
  .Columns(4).Width = 1500
  .Columns(5).Caption = "Fecha Inicio"
  .Columns(5).Width = 1000
  .Columns(6).Caption = "Tipo Asistencia"
  .Columns(6).Width = 1300
  .Columns(7).Caption = "nombre"
  .Columns(7).Visible = False
  .Columns(8).Caption = "ape1"
  .Columns(8).Visible = False
  .Columns(9).Caption = "ape2"
  .Columns(9).Visible = False
  .Columns(10).Caption = "Asistencia"
  .Columns(10).Visible = False
  .Columns(11).Caption = "Proceso"
  .Columns(11).Visible = False
 End With
 With grdVolCan
  .Columns(0).Caption = "Nombre y Apellido"
  .Columns(0).Width = 2600
  .Columns(1).Caption = "DNI"
  .Columns(1).Width = 1000
  .Columns(2).Caption = "Fec. Nac."
  .Columns(2).Width = 1000
  .Columns(3).Caption = "N� Seg. Soc."
  .Columns(3).Width = 1200
  .Columns(4).Caption = "Servicio"
  .Columns(4).DropDownHwnd = dcboServicios.hWnd
  .Columns(5).Caption = "Tratamiento"
  .Columns(5).DropDownHwnd = dcboTratamiento.hWnd
  .Columns(5).Width = 1200
  .Columns(6).Caption = "Fec. Recep."
  .Columns(6).Width = 1200
  .Columns(7).Caption = "nombre"
  .Columns(7).Visible = False
  .Columns(8).Caption = "ape1"
  .Columns(8).Visible = False
  .Columns(9).Caption = "ape2"
  .Columns(9).Visible = False
  .Columns(10).Caption = "Asistencia"
  .Columns(10).Visible = False
  .Columns(11).Caption = "Proceso"
  .Columns(11).Visible = False
  .Columns(12).Caption = "Registro"
  .Columns(12).Visible = False
  .Columns(13).Caption = "Facturado"
  .Columns(13).Visible = False
  
 End With
 With grdVolAsig
  .Columns(0).Caption = "Nombre y Apellido"
  .Columns(0).Width = 2600
  .Columns(1).Caption = "DNI"
  .Columns(1).Width = 1000
  .Columns(2).Caption = "Fec. Nac."
  .Columns(2).Width = 1000
  .Columns(3).Caption = "N� Seg. Soc."
  .Columns(3).Width = 1200
  .Columns(4).Caption = "Servicio"
  .Columns(4).DropDownHwnd = dcboServicios.hWnd
  .Columns(5).Caption = "Tratamiento"
  .Columns(5).DropDownHwnd = dcboTratamiento.hWnd
  .Columns(6).Caption = "Fec. Recep."
  .Columns(6).Width = 1000
  .Columns(7).Caption = "Nombre"
  .Columns(7).Visible = False
  .Columns(8).Caption = "Ape1"
  .Columns(8).Visible = False
  .Columns(9).Caption = "Ape2"
  .Columns(9).Visible = False
  .Columns(10).Caption = "N� Informe"
  .Columns(10).Width = 1000
  .Columns(11).Caption = "Id. Paciente."
  .Columns(11).Width = 1200
  .Columns(12).Caption = "CIP"
  .Columns(12).Width = 1200
  .Columns(13).Caption = "Valido"
  .Columns(13).Width = 1200
  .Columns(14).Caption = "Facturado"
  .Columns(14).Width = 1000
  .Columns(15).Caption = "Asistencia"
  .Columns(15).Visible = False
  .Columns(16).Caption = "Proceso"
  .Columns(16).Visible = False
 End With
 
End Sub
Private Sub Cargar_ProcAsist(intcol%)
Dim rst As rdoResultset
Dim nombre As String
Dim strsql As String
Dim fini As String
Dim ffin As String
Static sqlorder As String
grdProcAsist.RemoveAll

If intcol = -1 Then
      If sqlorder = "" Then sqlorder = " ORDER BY AD11FECINICIO"
    Else
        Select Case UCase(grdProcAsist.Columns(intcol).Caption)
        Case UCase("Nombre y Apellido"): sqlorder = " ORDER BY CI22NOMBRE"
        Case UCase("DNI"): sqlorder = " ORDER BY CI22DNI"
        Case UCase("Fec. Nac."): sqlorder = " ORDER BY CI22FECNACIM"
        Case UCase("N� Seg. Soc."): sqlorder = " ORDER BY CI22NUMSEGSOC"
        Case UCase("Servicio"): sqlorder = " ORDER BY AD02DESDPTO"
        Case UCase("Fec. Inicio"): sqlorder = " ORDER BY AD11FECINICIO"
        Case UCase("Tipo Asistencia"): sqlorder = " ORDER BY AD12DESTIPOASIST"
        Case Else: Exit Sub
        End Select
    End If
If Not objPipe.PipeExist("Select") Then
  Exit Sub
End If

strsql = objPipe.PipeGet("Select")
strsql = strsql & sqlorder
Set rst = objApp.rdoConnect.OpenResultset(strsql)
Do While Not rst.EOF
   grdProcAsist.AddItem rst!CI22NOMBRE & " " & rst!CI22PRIAPEL & " " & rst!CI22SEGAPEL & Chr$(9) _
   & rst!CI22DNI & Chr$(9) _
   & rst!CI22FECNACIM & Chr$(9) _
   & rst!CI22NUMSEGSOC & Chr$(9) _
   & rst!AD02DESDPTO & Chr$(9) _
   & rst!AD11FECINICIO & Chr$(9) _
   & rst!AD12DESTIPOASIST & Chr$(9) _
   & rst!CI22NOMBRE & Chr$(9) _
   & rst!CI22PRIAPEL & Chr$(9) _
   & rst!CI22SEGAPEL & Chr$(9) _
   & rst(7) & Chr$(9) _
   & rst(8)
   
   
   
 rst.MoveNext
Loop
objPipe.PipeRemove ("select")

End Sub

Private Sub pActualizar1()

Dim rst1 As rdoResultset
Dim nombre As String
Dim strsql1 As String

If objPipe.PipeExist("Select1") Then
  strsql1 = objPipe.PipeGet("Select1")
  objPipe.PipeRemove ("Select1")
  grdVolCan.RemoveAll
  grdVolAsig.RemoveAll
Else
 Exit Sub
End If



Set rst1 = objApp.rdoConnect.OpenResultset(strsql1)
 Do While Not rst1.EOF
       
      grdVolCan.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
      & rst1!FA25DNI & Chr$(9) _
      & rst1!FA25FECHNAC & Chr$(9) _
      & rst1!FA25NUMSEGSOC & rst1!FA25CODCTL & Chr$(9) _
      & rst1!AD02CODDPTO & Chr$(9) _
      & rst1!FA33CODTRATAM & Chr$(9) _
      & rst1!FA25FECRECEP & Chr$(9) _
      & rst1!FA25NOMB & Chr$(9) _
      & rst1!FA25APEL1 & Chr$(9) _
      & rst1!FA25APEL2 & Chr$(9) _
      & rst1(10) & Chr$(9) _
      & rst1(11) & Chr$(9) _
      & rst1(12) & Chr$(9) _
      & rst1(13)
      
      
     rst1.MoveNext
   Loop
   rst1.Close
   
End Sub

Private Sub grdProcAsist_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdProcAsist_Click()
Dim sql As String
Dim rst1 As rdoResultset
Dim i As Integer

   grdProcAsist.SelBookmarks.RemoveAll
   grdProcAsist.SelBookmarks.Add (grdProcAsist.RowBookmark(grdProcAsist.Row))
   
   
   grdVolCan.RemoveAll
   grdVolAsig.RemoveAll
   If grdProcAsist.SelBookmarks.Count = 0 Then
     Exit Sub
   End If
   
    sql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25DNI,FA25FECHNAC,FA25NUMSEGSOC,FA25CODCTL,AD02CODDPTO,FA33CODTRATAM,FA25FECRECEP,AD01CODASISTENCI,AD07CODPROCESO,FA25CODREG,FA25FACTUR"
    sql = sql & " FROM FA2500 WHERE ( "
    If grdProcAsist.Columns("DNI").Text <> "" Then
        sql = sql & "(FA25DNI = '" & grdProcAsist.Columns("DNI").Text & "' AND FA25DNI IS NOT NULL) OR "
    End If
    sql = sql & " (FA25FECHNAC = TO_DATE('" & Format(grdProcAsist.Columns("Fec. Nac.").Text, "DD/MM/YYYY") & "','DD/MM/YYYY') AND FA25APEL1 = '" & grdProcAsist.Columns("ape1").Text
    sql = sql & "' AND FA25APEL2 = '" & grdProcAsist.Columns("ape2").Text & "') OR "
    sql = sql & "(FA25NOMB = '" & grdProcAsist.Columns("nombre").Text & "' AND FA25APEL1 = '" & grdProcAsist.Columns("ape1").Text
    sql = sql & "' AND FA25APEL2 = '" & grdProcAsist.Columns("ape2").Text & "')) AND "
    sql = sql & "AD01CODASISTENCI IS NULL AND FA25VALID = -1 AND FA25FACTUR = 0 ORDER BY FA25FECRECEP"
    Set rst1 = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rst1.EOF
       
      grdVolCan.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
      & rst1!FA25DNI & Chr$(9) _
      & rst1!FA25FECHNAC & Chr$(9) _
      & rst1!FA25NUMSEGSOC & rst1!FA25CODCTL & Chr$(9) _
      & rst1!AD02CODDPTO & Chr$(9) _
      & rst1!FA33CODTRATAM & Chr$(9) _
      & rst1!FA25FECRECEP & Chr$(9) _
      & rst1!FA25NOMB & Chr$(9) _
      & rst1!FA25APEL1 & Chr$(9) _
      & rst1!FA25APEL2 & Chr$(9) _
      & rst1(10) & Chr$(9) _
      & rst1(11) & Chr$(9) _
      & rst1(12) & Chr$(9) _
      & rst1(13)
      
      
     rst1.MoveNext
   Loop
   rst1.Close
   
    sql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25DNI,FA25FECHNAC,FA25NUMSEGSOC,FA25CODCTL,AD02CODDPTO,FA33CODTRATAM,FA25FECRECEP,FA25EXPINF,FA25IDCLIENTE,FA25CODPOSTAL,FA25VALID,FA25FACTUR,AD01CODASISTENCI,AD07CODPROCESO"
    sql = sql & " FROM FA2500 WHERE AD01CODASISTENCI=" & grdProcAsist.Columns("Asistencia").Text & " AND AD07CODPROCESO=" & grdProcAsist.Columns("Proceso").Text
    Set rst1 = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rst1.EOF
       
      grdVolAsig.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
      & rst1!FA25DNI & Chr$(9) _
      & rst1!FA25FECHNAC & Chr$(9) _
      & rst1!FA25NUMSEGSOC & rst1!FA25CODCTL & Chr$(9) _
      & rst1!AD02CODDPTO & Chr$(9) _
      & rst1!FA33CODTRATAM & Chr$(9) _
      & rst1!FA25FECRECEP & Chr$(9) _
      & rst1!FA25NOMB & Chr$(9) _
      & rst1!FA25APEL1 & Chr$(9) _
      & rst1!FA25APEL2 & Chr$(9) _
      & rst1!FA25EXPINF & Chr$(9) _
      & rst1!FA25IDCLIENTE & Chr$(9) _
      & rst1!FA25CODPOSTAL & Chr$(9) _
      & rst1!FA25VALID & Chr$(9) _
      & rst1!FA25FACTUR & Chr$(9) _
      & rst1(15) & Chr$(9) _
      & rst1(16)
      
     rst1.MoveNext
   Loop
rst1.Close




End Sub

Private Sub grdProcAsist_HeadClick(ByVal ColIndex As Integer)
 If grdProcAsist.Rows > 0 Then Call Cargar_ProcAsist(ColIndex)
 
End Sub



