VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeleccionVolante 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de volantes"
   ClientHeight    =   6240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Campos de b�squeda"
      ForeColor       =   &H00FF0000&
      Height          =   2655
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   11655
      Begin VB.TextBox txtNumeroInforme 
         Height          =   285
         Left            =   6240
         TabIndex        =   25
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Frame Frame2 
         Caption         =   "Fecha del Disquete"
         Height          =   1095
         Left            =   4920
         TabIndex        =   9
         Top             =   1440
         Width           =   4935
         Begin SSCalendarWidgets_A.SSDateCombo dtcFechaDiscoInferior 
            Height          =   255
            Left            =   840
            TabIndex        =   10
            Top             =   480
            Width           =   1575
            _Version        =   65543
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   93
            BackColor       =   -2147483634
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFechaDiscoSuperior 
            Height          =   255
            Left            =   2760
            TabIndex        =   11
            Top             =   480
            Width           =   1575
            _Version        =   65543
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   93
            BackColor       =   -2147483634
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Entre"
            Height          =   195
            Left            =   360
            TabIndex        =   13
            Top             =   480
            Width           =   375
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "y"
            Height          =   195
            Left            =   2520
            TabIndex        =   12
            Top             =   480
            Width           =   75
         End
      End
      Begin VB.TextBox txtBuscaNombre 
         Height          =   285
         Left            =   1800
         TabIndex        =   8
         Top             =   360
         Width           =   2175
      End
      Begin VB.TextBox txtBuscaApellido1 
         Height          =   285
         Left            =   1800
         TabIndex        =   7
         Top             =   720
         Width           =   2175
      End
      Begin VB.TextBox txtBuscaApellido2 
         Height          =   285
         Left            =   1800
         TabIndex        =   6
         Top             =   1080
         Width           =   2175
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   10200
         TabIndex        =   4
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "&Limpiar"
         Height          =   375
         Left            =   10200
         TabIndex        =   3
         Top             =   1200
         Width           =   855
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcBuscaFechaNacimiento 
         Height          =   255
         Left            =   1800
         TabIndex        =   5
         Top             =   1440
         Width           =   2175
         _Version        =   65543
         _ExtentX        =   3836
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboBuscaSexo 
         Height          =   255
         Left            =   1800
         TabIndex        =   14
         Top             =   1800
         Width           =   1275
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196615
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Sexo"
         Columns(1).Name =   "Sexo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2249
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboBuscaDpto 
         Height          =   255
         Left            =   6240
         TabIndex        =   15
         Top             =   720
         Width           =   2235
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196615
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3942
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboAsignado 
         Height          =   255
         Left            =   6240
         TabIndex        =   28
         Top             =   360
         Width           =   915
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   196615
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "Asignado"
         Columns(0).Name =   "Asignado"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1614
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "N� Informe"
         Height          =   195
         Left            =   5040
         TabIndex        =   24
         Top             =   1080
         Width           =   750
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Left            =   240
         TabIndex        =   22
         Top             =   360
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Primer apellido"
         Height          =   195
         Left            =   240
         TabIndex        =   21
         Top             =   720
         Width           =   1020
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Segundo Apellido"
         Height          =   195
         Left            =   240
         TabIndex        =   20
         Top             =   1080
         Width           =   1245
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "F. de nacimiento"
         Height          =   195
         Left            =   240
         TabIndex        =   19
         Top             =   1440
         Width           =   1170
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         Height          =   195
         Left            =   240
         TabIndex        =   18
         Top             =   1800
         Width           =   360
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Asignado"
         Height          =   195
         Left            =   5040
         TabIndex        =   17
         Top             =   360
         Width           =   660
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         Height          =   195
         Left            =   5040
         TabIndex        =   16
         Top             =   720
         Width           =   1005
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   5640
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   1800
      TabIndex        =   0
      Top             =   5640
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid grdResultadoBus 
      Height          =   2655
      Left            =   120
      TabIndex        =   23
      Top             =   2880
      Width           =   11640
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20532
      _ExtentY        =   4683
      _StockProps     =   79
      Caption         =   "Resultado de la B�squeda"
      ForeColor       =   16711680
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboServicios 
      Height          =   1095
      Left            =   3720
      TabIndex        =   26
      Top             =   5400
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Servicio"
      Columns(0).Name =   "Codigo Servicio"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Descripcion Servicio"
      Columns(1).Name =   "Descripcion Servicio"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown dcboTratamiento 
      Height          =   1095
      Left            =   6000
      TabIndex        =   27
      Top             =   5520
      Width           =   2415
      DataFieldList   =   "column 0"
      _Version        =   196615
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo Tratamiento"
      Columns(0).Name =   "Codigo Tratamiento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Tratamiento"
      Columns(1).Name =   "Tratamiento"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "column 1"
   End
End
Attribute VB_Name = "frmSeleccionVolante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
  Dim strsql As String
    
  If grdResultadoBus.Columns("Registro").Text <> "" Then
        strsql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25DNI,FA25FECHNAC,FA25NUMSEGSOC,FA25CODCTL,AD02CODDPTO,FA33CODTRATAM,FA25FECRECEP,AD01CODASISTENCI,AD07CODPROCESO,FA25CODREG,FA25FACTUR"
        strsql = strsql & " FROM FA2500 WHERE FA25CODREG = " & grdResultadoBus.Columns("Registro").Text
        Call objPipe.PipeSet("Select1", strsql)
        Aceptar = True
        
  End If
    
  Unload Me
  
End Sub

Private Sub cmdBuscar_Click()
  Dim strCondicion As String
  Dim strsql As String
  Dim rst1 As rdoResultset
  Screen.MousePointer = vbHourglass
  
  
  'Si no se ha rellenado ningun campo de b�squeda
  If txtBuscaApellido1.Text = "" And _
    txtBuscaApellido2.Text = "" And _
    dtcBuscaFechaNacimiento.Text = "" And _
    dtcFechaDiscoInferior.Text = "" And _
    dtcFechaDiscoSuperior.Text = "" Then
    MsgBox "Debe rellenar al menos un campo de b�squeda.", vbOKOnly + vbInformation, "B�squeda nula"
    strCondicion = "FA25CODREG = 0"
    strsql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25FECHNAC,FA33CODTRATAM,AD02CODDPTO,FA25FECRECEP,AD01CODASISTENCI,AD07CODPROCESO,FA25CODREG"
    strsql = strsql & " FROM FA2500 WHERE " & strCondicion
    Set rst1 = objApp.rdoConnect.OpenResultset(strsql)
    Do While Not rst1.EOF
      grdResultadoBus.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
      & rst1!FA25FECHNAC & Chr$(9) _
      & rst1!FA33CODTRATAM & Chr$(9) _
      & rst1!AD02CODDPTO & Chr$(9) _
      & rst1!FA25FECRECEP & Chr$(9) _
      & rst1(7) & Chr$(9) _
      & rst1(8) & Chr$(9) _
      & rst1(9)
      rst1.MoveNext
    Loop
    rst1.Close
    
  Else
    strCondicion = "FA25CODREG <> 0 "
    'Si se ha rellenado el campo Apellido1 se buscara apellidos1 que contengan
    'una transformacion (mediante la funcion TextoAlfa) del texto introducido en este campo.
    If txtBuscaApellido1 <> "" Then
      strCondicion = strCondicion & " AND FA25APEL1 LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaApellido1) & "%'"
    End If
    If txtBuscaApellido2 <> "" Then
      strCondicion = strCondicion & " AND FA25APEL2 LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaApellido2) & "%' "
    End If
    If txtBuscaNombre <> "" Then
      strCondicion = strCondicion & " AND FA25NOMB LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaNombre) & "%' "
    End If
    If dtcBuscaFechaNacimiento.Text <> "" Then
      strCondicion = strCondicion & " AND FA25FECHNAC = TO_DATE('"
      strCondicion = strCondicion & Format(dtcBuscaFechaNacimiento, "DD/MM/YYYY") & "','DD/MM/YYYY') "
    End If
    If cboBuscaSexo.Text <> "" Then
      strCondicion = strCondicion & " AND FA25SEXO ="
      strCondicion = strCondicion & cboBuscaSexo
    End If
   
    
    If cboBuscaDpto.Text <> "" Then
      strCondicion = strCondicion & " AND AD02CODDPTO ="
      strCondicion = strCondicion & cboBuscaDpto.Text
    End If
    If txtNumeroInforme <> "" Then
      strCondicion = strCondicion & " AND FA25EXPINF LIKE '%"
      strCondicion = strCondicion & Me.txtNumeroInforme & "'"
    End If
    If dtcFechaDiscoInferior.Text <> "" Then
      strCondicion = strCondicion & " AND FA25FECRECEP >= TO_DATE('"
      strCondicion = strCondicion & Format(dtcFechaDiscoInferior.Text, "DD/MM/YYYY") & "','DD/MM/YYYY') "
    End If
    If dtcFechaDiscoSuperior.Text <> "" Then
      strCondicion = strCondicion & " AND FA25FECRECEP <= TO_DATE('"
      strCondicion = strCondicion & Format(dtcFechaDiscoSuperior.Text, "DD/MM/YYYY") & "','DD/MM/YYYY') "
    End If
    
    strsql = "SELECT FA25NOMB,FA25APEL1,FA25APEL2,FA25FECHNAC,FA33CODTRATAM,AD02CODDPTO,FA25FECRECEP,AD01CODASISTENCI,AD07CODPROCESO,FA25CODREG"
    strsql = strsql & " FROM FA2500 WHERE " & strCondicion & " AND FA25VALID = -1 AND FA25FACTUR = 0 ORDER BY FA25FECRECEP"
    Set rst1 = objApp.rdoConnect.OpenResultset(strsql)
    Do While Not rst1.EOF
    If cboAsignado.Text <> "" Then
      If cboAsignado.Text = "No" Then
        If IsNull(rst1(7)) Then
           grdResultadoBus.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
           & rst1!FA25FECHNAC & Chr$(9) _
           & rst1!FA33CODTRATAM & Chr$(9) _
           & rst1!AD02CODDPTO & Chr$(9) _
           & rst1!FA25FECRECEP & Chr$(9) _
           & rst1(7) & Chr$(9) _
           & rst1(8) & Chr$(9) _
           & rst1(9)
           rst1.MoveNext
       Else
         rst1.MoveNext
       End If
    ElseIf cboAsignado.Text = "S�" Then
          If Not (IsNull(rst1(7))) Then
            grdResultadoBus.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
           & rst1!FA25FECHNAC & Chr$(9) _
           & rst1!FA33CODTRATAM & Chr$(9) _
           & rst1!AD02CODDPTO & Chr$(9) _
           & rst1!FA25FECRECEP & Chr$(9) _
           & rst1(7) & Chr$(9) _
           & rst1(8) & Chr$(9) _
           & rst1(9)
           rst1.MoveNext
         Else
           rst1.MoveNext
         End If
   ElseIf cboAsignado.Text = "Todos" Then
            grdResultadoBus.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
           & rst1!FA25FECHNAC & Chr$(9) _
           & rst1!FA33CODTRATAM & Chr$(9) _
           & rst1!AD02CODDPTO & Chr$(9) _
           & rst1!FA25FECRECEP & Chr$(9) _
           & rst1(7) & Chr$(9) _
           & rst1(8) & Chr$(9) _
           & rst1(9)
           rst1.MoveNext
     End If
     
      
      Else
            grdResultadoBus.AddItem rst1!FA25NOMB & " " & rst1!FA25APEL1 & " " & rst1!FA25APEL2 & Chr$(9) _
           & rst1!FA25FECHNAC & Chr$(9) _
           & rst1!FA33CODTRATAM & Chr$(9) _
           & rst1!AD02CODDPTO & Chr$(9) _
           & rst1!FA25FECRECEP & Chr$(9) _
           & rst1(7) & Chr$(9) _
           & rst1(8) & Chr$(9) _
           & rst1(9)
           rst1.MoveNext
     End If
     
    Loop
    rst1.Close
  End If
  Screen.MousePointer = vbDefault
  
End Sub

Private Sub cmdCancelar_Click()
Unload Me

End Sub

Private Sub cmdLimpiar_Click()
txtBuscaNombre = ""
txtBuscaApellido1 = ""
txtBuscaApellido2 = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboAsignado.Text = ""
cboBuscaDpto.Text = ""
txtNumeroInforme.Text = ""
dtcFechaDiscoInferior.Text = ""
dtcFechaDiscoSuperior.Text = ""
txtBuscaNombre.SetFocus
grdResultadoBus.RemoveAll

End Sub

Private Sub Form_Load()
 With grdResultadoBus
    .Columns(0).Caption = "Nombre y apellidos"
    .Columns(0).Width = 3000
    .Columns(1).Caption = "F. nacimiento"
    .Columns(1).Width = 1400
    .Columns(2).Caption = "Tratamiento"
    .Columns(2).Width = 1600
    .Columns(2).DropDownHwnd = dcboTratamiento.hWnd
    .Columns(3).Caption = "Departamento"
    .Columns(3).Width = 1700
    .Columns(3).DropDownHwnd = dcboServicios.hWnd
    .Columns(4).Caption = "F. Disquete"
    .Columns(4).Width = 1400
    .Columns(5).Caption = "Asistencia"
    .Columns(5).Visible = False
    .Columns(6).Caption = "Proceso"
    .Columns(6).Visible = False
    .Columns(7).Caption = "Registro"
    .Columns(7).Visible = False
 End With
 Call Cargar_Combos
 Call Cargar_dropsdowns
dtcBuscaFechaNacimiento.Text = ""
dtcFechaDiscoInferior.Text = ""
dtcFechaDiscoSuperior.Text = ""
cboBuscaSexo.Text = ""
cboAsignado.Text = "No"
cboBuscaDpto.Text = ""


End Sub
Private Sub Cargar_Combos()
 Dim sql As String
 Dim rst As rdoResultset
 
 sql = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200"
 Set rst = objApp.rdoConnect.OpenResultset(sql)
 Do While Not rst.EOF
   cboBuscaDpto.AddItem rst(0) & Chr$(9) & rst(1)
   rst.MoveNext
 Loop
 rst.Close
 cboBuscaSexo.AddItem "1" & Chr$(9) & "Hombre"
 cboBuscaSexo.AddItem "2" & Chr$(9) & "Mujer"
 cboAsignado.AddItem "S�"
 cboAsignado.AddItem "No"
 cboAsignado.AddItem "Todos"
 cboAsignado.Text = "No"
 
End Sub
Private Sub Cargar_dropsdowns()
Dim sql1 As String
Dim sql2 As String
Dim rst2 As rdoResultset
Dim rst3 As rdoResultset
sql1 = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200"
Set rst2 = objApp.rdoConnect.OpenResultset(sql1)
Do While Not rst2.EOF
   dcboServicios.AddItem rst2(0) & Chr$(9) & rst2(1)
   rst2.MoveNext
Loop
rst2.Close
sql2 = "SELECT FA33CODTRATAM,FA33DESCTRATAM FROM FA3300"
Set rst3 = objApp.rdoConnect.OpenResultset(sql2)
Do While Not rst3.EOF
   dcboTratamiento.AddItem rst3(0) & Chr$(9) & rst3(1)
   rst3.MoveNext
Loop
End Sub
Private Sub grdResultadoBus_Click()
 grdResultadoBus.SelBookmarks.RemoveAll
 grdResultadoBus.SelBookmarks.Add (grdResultadoBus.RowBookmark(grdResultadoBus.Row))
End Sub

