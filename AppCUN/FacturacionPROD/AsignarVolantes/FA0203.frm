VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmCargaDisquetes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Disquetes de Osasunbidea"
   ClientHeight    =   1335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3825
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   3825
   StartUpPosition =   2  'CenterScreen
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   135
      Left            =   2640
      TabIndex        =   2
      Top             =   240
      Visible         =   0   'False
      Width           =   135
      _Version        =   196608
      _ExtentX        =   238
      _ExtentY        =   238
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   840
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCargar 
      Height          =   735
      Left            =   1800
      Picture         =   "FA0203.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   360
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Carga Disquetes"
      Height          =   195
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   1170
   End
End
Attribute VB_Name = "frmCargaDisquetes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim RUTA As String

Sub cargaDisquete()
  
  Dim lngficheroentrada As Long
  Dim lngFicheroLog As Long
  Dim strlineadisquete As String
  Dim rst As rdoResultset
  
  Dim rst1 As rdoResultset
  
  
  Dim strSqlComun As String
  Dim strsql As String
  Dim strSqlQdf As String
  Dim blnHayIncidencias As Boolean

 
  Dim strexpinf As String
  Dim strIdCliente As String
  Dim strNumSegSoc As String
  Dim strCodCtl As String
  Dim strDNI As String
  Dim strApel1 As String
  Dim strApel2 As String
  Dim strNomb As String
  Dim strFechNac As String
  Dim strSexo As String
  Dim strCP As String
  Dim strServ As String
  Dim strAsist As String
  Dim strNumVol As String
  Dim stroperac As String
  Dim qry1 As rdoQuery
  Dim numcopias As Long
  Dim i As Long
  
  Dim strMsg As String
  Dim strTitulo As String
  Dim sql As String
  Dim res As String
  
  On Error GoTo ErrorEnLaCarga
  vsPrinter1.StartDoc

  vsPrinter1.CurrentX = 5
  vsPrinter1.CurrentY = 5
  vsPrinter1.FontBold = True
  vsPrinter1.FontUnderline = True
  vsPrinter1.FontSize = 12
  vsPrinter1.Text = "FICHERO DE INCIDENCIAS" & Chr$(13) & Chr$(13)
  vsPrinter1.FontBold = False
  vsPrinter1.FontUnderline = False
  vsPrinter1.FontSize = 9
  Screen.MousePointer = vbHourglass
  blnHayIncidencias = False
  ' Se abre el archivo cun.txt (volantes) para lectura.
  lngficheroentrada = FreeFile
  Open RUTA For Input Lock Write As lngficheroentrada
  
  RUTA = Left(RUTA, Len(RUTA) - 7)
  
  Do While Not EOF(lngficheroentrada)
    Input #lngficheroentrada, strlineadisquete
    
    strexpinf = Trim(Mid(strlineadisquete, 1, 14))
    strIdCliente = Trim(Mid(strlineadisquete, 15, 23))
    strNumSegSoc = Trim(Mid(strlineadisquete, 38, 10))
    strCodCtl = Trim(Mid(strlineadisquete, 48, 2))
    strDNI = Trim(Mid(strlineadisquete, 50, 8))
    strApel1 = Trim(Mid(strlineadisquete, 58, 18))
    strApel2 = Trim(Mid(strlineadisquete, 76, 18))
    strNomb = Trim(Mid(strlineadisquete, 94, 14))
    strFechNac = Trim(Mid(strlineadisquete, 108, 10))
    strSexo = Trim(Mid(strlineadisquete, 118, 1))
    strCP = Trim(Mid(strlineadisquete, 119, 5))
    strServ = Trim(Mid(strlineadisquete, 124, 3))
    strAsist = Trim(Mid(strlineadisquete, 127, 7))
    strNumVol = Trim(Mid(strlineadisquete, 134, 2))
    stroperac = Trim(Mid(strlineadisquete, 136, 1))
    
    'Comprobaciones previas a la inserci�n del volante
    '1.- Seg�n el valor de strOperac
    If (stroperac = "B") Then
    'Si es un borrado, se comprueba que existe el registro, que no ha sido asignado, que no ha sido facturado y que es v�lido.
    'Si existe se borra. Si no se avisa en el log
        strSqlComun = " WHERE FA25EXPINF = '" & strexpinf
        strSqlComun = strSqlComun & "' AND FA25IDCLIENTE = '" & strIdCliente
        strSqlComun = strSqlComun & "' AND FA25NUMSEGSOC = '" & strNumSegSoc
        strSqlComun = strSqlComun & "' AND FA25CODCTL = '" & strCodCtl
        strSqlComun = strSqlComun & "' AND FA25DNI = '" & strDNI
        strSqlComun = strSqlComun & "' AND FA25APEL1 = '" & strApel1
        strSqlComun = strSqlComun & "' AND FA25APEL2 = '" & strApel2
        strSqlComun = strSqlComun & "' AND FA25NOMB  = '" & strNomb
        strSqlComun = strSqlComun & "' AND FA25FECHNAC = TO_DATE('" & Format(strFechNac, "DD/MM/YYYY")
        strSqlComun = strSqlComun & "','DD/MM/YYYY') AND FA25SEXO = " & strSexo
        strSqlComun = strSqlComun & " AND FA25CODPOSTAL = '" & strCP
        strSqlComun = strSqlComun & "' AND AD02CODDPTO = " & strServ
        strSqlComun = strSqlComun & " AND FA33CODTRATAM = '" & strAsist
        strSqlComun = strSqlComun & "' AND FA25NUMVOL = '" & strNumVol
        strSqlComun = strSqlComun & "' AND AD01CODASISTENCI IS NULL AND AD07CODPROCESO IS NULL"
        strSqlComun = strSqlComun & " AND FA25FACTUR = 0 AND FA25VALID = -1"
        
        strsql = "SELECT FA25CODREG FROM FA2500" & strSqlComun
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        If Not rst.EOF Then
            sql = "UPDATE FA2500 SET FA25VALID = 0, FA25FECUPD = TO_DATE('" & Format(FechaHora(), "DD/MM/YYYY HH:MM:SS") & "','DD/MM/YYYY HH24:MI:SS')" & strSqlComun
            Set rst1 = objApp.rdoConnect.OpenResultset(sql)
            rst1.Close
        Else
            If Not (blnHayIncidencias) Then
                blnHayIncidencias = True
                lngFicheroLog = FreeFile
                Open RUTA & "CARGA.LOG" For Output Lock Write As lngFicheroLog
            End If
            Print #lngFicheroLog, "No se ha podido borrar el volante:"
            Print #lngFicheroLog, strlineadisquete
            vsPrinter1.Text = "No se ha podido borrar el volante:" & Chr$(13)
            vsPrinter1.Text = strlineadisquete & Chr$(13)
            
            
        End If
        rst.Close
        
        
    ElseIf (stroperac = "M") Then
    'Si es una modificaci�n, se comprueba que existe el registro, que no ha sido facturado y que es v�lido.
    'Si existe se modifica. Si no se avisa en el log
        strSqlComun = " WHERE FA25EXPINF = '" & strexpinf
        strSqlComun = strSqlComun & "' AND FA25FACTUR = 0 AND FA25VALID = -1"
        
        strsql = "SELECT FA25CODREG FROM FA2500" & strSqlComun
        
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        If Not rst.EOF Then
            strSqlQdf = "UPDATE FA2500 SET FA25IDCLIENTE = '" & strIdCliente
            strSqlQdf = strSqlQdf & "', FA25NUMSEGSOC = '" & strNumSegSoc
            strSqlQdf = strSqlQdf & "', FA25CODCTL = '" & strCodCtl
            strSqlQdf = strSqlQdf & "', FA25DNI = '" & strDNI
            strSqlQdf = strSqlQdf & "', FA25APEL1 = '" & strApel1
            strSqlQdf = strSqlQdf & "', FA25APEL2 = '" & strApel2
            strSqlQdf = strSqlQdf & "', FA25NOMB  = '" & strNomb
            strSqlQdf = strSqlQdf & "', FA25FECHNAC = TO_DATE('" & Format(strFechNac, "DD/MM/YYYY")
            strSqlQdf = strSqlQdf & "','DD/MM/YYYY'), FA25SEXO = " & strSexo
            strSqlQdf = strSqlQdf & ", FA25CODPOSTAL = '" & strCP
            strSqlQdf = strSqlQdf & "', AD02CODDPTO = " & strServ
            strSqlQdf = strSqlQdf & ", FA33CODTRATAM = '" & strAsist
            strSqlQdf = strSqlQdf & "', FA25NUMVOL = '" & strNumVol
            strSqlQdf = strSqlQdf & "', FA25FECUPD = TO_DATE('" & Format(FechaHora(), "DD/MM/YYYY HH:MM:SS") & "','DD/MM/YYYY HH24:MI:SS')"
            strSqlQdf = strSqlQdf & ", FA25FECRECEP = TO_DATE('" & Format(FileDateTime("c:\Osasunbi\CargaVolantes\CUN.TXT"), "DD/MM/YYYY HH:MM:SS") & "','DD/MM/YYYY HH24:MI.SS')"
            
            sql = strSqlQdf & strSqlComun
            Set rst1 = objApp.rdoConnect.OpenResultset(sql)
        ElseIf rst.EOF Then
        'Caso de que no exista el registro facturado se inserta el volante
            rst.Close
            strsql = "SELECT FA25CODREG FROM FA2500 WHERE FA25EXPINF = '" & strexpinf
            strsql = strsql & "' AND FA25VALID = -1"
            Set rst = objApp.rdoConnect.OpenResultset(strsql)
            If rst.EOF Then
            'Se inserta el volante
                strSqlQdf = "INSERT INTO FA2500 ( FA25EXPINF, FA25IDCLIENTE, FA25NUMSEGSOC, FA25CODCTL,"
                strSqlQdf = strSqlQdf & "FA25DNI, FA25APEL1, FA25APEL2, FA25NOMB, FA25FECHNAC, FA25SEXO,"
                strSqlQdf = strSqlQdf & "FA25CODPOSTAL, AD02CODDPTO, FA33CODTRATAM, FA25NUMVOL, FA25FECRECEP, FA25FACTUR, FA25VALID)"
                strSqlQdf = strSqlQdf & " VALUES (?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),0,-1)"
                sql = strSqlQdf
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                qry1(0) = strexpinf
                qry1(1) = strIdCliente
                qry1(2) = strNumSegSoc
                qry1(3) = strCodCtl
                qry1(4) = strDNI
                qry1(5) = strApel1
                qry1(6) = strApel2
                qry1(7) = strNomb
                qry1(8) = strFechNac
                qry1(9) = strSexo
                qry1(10) = strCP
                qry1(11) = strServ
                qry1(12) = strAsist
                qry1(13) = strNumVol
                qry1(14) = Format(FileDateTime("C:\Osasunbi\CargaVolantes\Cun.txt"), "DD/MM/YYYY HH:MM:SS")
                qry1.Execute
                qry1.Close
                
                
                
                
            Else
            'Si existe el volante facturado se avisa en el log
                If Not (blnHayIncidencias) Then
                    blnHayIncidencias = True
                    lngFicheroLog = FreeFile
                    Open RUTA & "CARGA.LOG" For Output Lock Write As lngFicheroLog
                End If
                Print #lngFicheroLog, "No se ha podido actualizar el volante porque ya est� facturado:"
                Print #lngFicheroLog, strlineadisquete
                vsPrinter1.Text = "No se ha podido actualizar el volante porque ya est� facturado:" & Chr$(13)
                vsPrinter1.Text = strlineadisquete & Chr$(13)
                
                
            End If
        Else
            If Not (blnHayIncidencias) Then
                blnHayIncidencias = True
                lngFicheroLog = FreeFile
                Open RUTA & "CARGA.LOG" For Output Lock Write As lngFicheroLog
            End If
            Print #lngFicheroLog, "No se ha podido actualizar el volante porque hay m�s de uno v�lido:"
            Print #lngFicheroLog, strlineadisquete
            vsPrinter1.Text = "No se ha podido actualizar el volante porque hay m�s de uno v�lido:" & Chr$(13)
            vsPrinter1.Text = strlineadisquete & Chr$(13)
            
            
            
        End If
        rst.Close
    ElseIf (stroperac = "") Then
    'Si es una inserci�n se comprueba que no exista el registro en estado v�lido. Si no existe se inserta. Si existe se avisa en el log.
        strSqlComun = " WHERE FA25EXPINF = '" & strexpinf
        strSqlComun = strSqlComun & "' AND FA25IDCLIENTE = '" & strIdCliente
        strSqlComun = strSqlComun & "' AND FA25NUMSEGSOC = '" & strNumSegSoc
        strSqlComun = strSqlComun & "' AND FA25CODCTL = '" & strCodCtl
        strSqlComun = strSqlComun & "' AND FA25DNI = '" & strDNI
        strSqlComun = strSqlComun & "' AND FA25APEL1 = '" & strApel1
        strSqlComun = strSqlComun & "' AND FA25APEL2 = '" & strApel2
        strSqlComun = strSqlComun & "' AND FA25NOMB  = '" & strNomb
        strSqlComun = strSqlComun & "' AND FA25FECHNAC = TO_DATE('" & Format(strFechNac, "DD/MM/YYYY")
        strSqlComun = strSqlComun & "','DD/MM/YYYY') AND FA25SEXO = " & strSexo
        strSqlComun = strSqlComun & " AND FA25CODPOSTAL = '" & strCP
        strSqlComun = strSqlComun & "' AND AD02CODDPTO = " & strServ
        strSqlComun = strSqlComun & " AND FA33CODTRATAM = '" & strAsist
        strSqlComun = strSqlComun & "' AND FA25NUMVOL = '" & strNumVol
        strSqlComun = strSqlComun & "' AND FA25VALID = -1"
        
        strsql = "SELECT FA25CODREG FROM FA2500" & strSqlComun
        
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        If rst.EOF Then
            strSqlQdf = "INSERT INTO FA2500 ( FA25EXPINF, FA25IDCLIENTE, FA25NUMSEGSOC, FA25CODCTL,"
            strSqlQdf = strSqlQdf & "FA25DNI, FA25APEL1, FA25APEL2, FA25NOMB, FA25FECHNAC, FA25SEXO,"
            strSqlQdf = strSqlQdf & "FA25CODPOSTAL, AD02CODDPTO, FA33CODTRATAM, FA25NUMVOL, FA25FECRECEP, FA25FACTUR, FA25VALID)"
            strSqlQdf = strSqlQdf & " VALUES (?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),0,-1)"
                sql = strSqlQdf
                Set qry1 = objApp.rdoConnect.CreateQuery("P", sql)
                qry1(0) = strexpinf
                qry1(1) = strIdCliente
                qry1(2) = strNumSegSoc
                qry1(3) = strCodCtl
                qry1(4) = strDNI
                qry1(5) = strApel1
                qry1(6) = strApel2
                qry1(7) = strNomb
                qry1(8) = strFechNac
                qry1(9) = strSexo
                qry1(10) = strCP
                qry1(11) = strServ
                qry1(12) = strAsist
                qry1(13) = strNumVol
                qry1(14) = Format(FileDateTime("C:\Osasunbi\CargaVolantes\Cun.txt"), "DD/MM/YYYY HH:MM:SS")
                qry1.Execute
                qry1.Close
            
        Else
            If Not (blnHayIncidencias) Then
                blnHayIncidencias = True
                lngFicheroLog = FreeFile
                Open RUTA & "CARGA.LOG" For Output Lock Write As lngFicheroLog
            End If
            Print #lngFicheroLog, "No se ha podido insertar el volante:"
            Print #lngFicheroLog, strlineadisquete
            vsPrinter1.Text = "No se ha podido insertar el volante:" & Chr$(13)
            vsPrinter1.Text = strlineadisquete & Chr$(13)
            
        End If
        rst.Close
    Else
    'Se avisa en el log que el fichero es defectuoso
        If Not (blnHayIncidencias) Then
            blnHayIncidencias = True
            lngFicheroLog = FreeFile
            Open RUTA & "CARGA.LOG" For Output Lock Write As lngFicheroLog
        End If
        Print #lngFicheroLog, "No se ha podido interpretar el volante:"
        Print #lngFicheroLog, strlineadisquete
        vsPrinter1.Text = "No se ha podido interpretar el volante:" & Chr$(13)
        vsPrinter1.Text = strlineadisquete & Chr$(13)
    End If
  Loop
  vsPrinter1.EndDoc
  
  Close lngficheroentrada
  If blnHayIncidencias Then
    Close lngFicheroLog
    
    strMsg = "Se han producido incidencias en la carga del disquete." & Chr$(13)
    strMsg = strMsg & "Revise el archivo de log o p�ngase en contacto con su administrador." & Chr$(13)
    strMsg = strMsg & "�Desea Imprimirlo?"
    strTitulo = "Incidencias al realizar la carga del disquete"
    
    res = MsgBox(strMsg, vbYesNo + vbInformation, strTitulo)
    If res = vbYes Then
     vsPrinter1.PrintDialog pdPrinterSetup
     
     vsPrinter1.EndDoc
     vsPrinter1.PrintDoc
     
    
    
    End If
    Close lngFicheroLog
  Else
    strMsg = "La carga del disquete se ha realizado sin incidencias." & Chr$(13)
    strTitulo = "Fin de carga del disquete"
    MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
  End If
  Screen.MousePointer = vbDefault
  
  Exit Sub

ErrorEnLaCarga:

  strMsg = "Imposible realizar la carga del disquete." & Chr$(13)
  strMsg = strMsg & "P�ngase en contacto con su administrador."
  strTitulo = "Error al realizar la carga del disquete"
  MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
  Close lngficheroentrada
  If blnHayIncidencias Then
   
    strMsg = "Se han producido incidencias en la carga del disquete." & Chr$(13)
    strMsg = strMsg & "Revise el archivo de log o p�ngase en contacto con su administrador." & Chr$(13)
   strMsg = strMsg & "�Desea Imprimirlo?"
    strTitulo = "Incidencias al realizar la carga del disquete"
    
    res = MsgBox(strMsg, vbYesNo + vbInformation, strTitulo)
    If res = vbYes Then
      vsPrinter1.PrintDialog pdPrinterSetup
      vsPrinter1.EndDoc
      vsPrinter1.PrintDoc
    End If
    Close lngFicheroLog
  End If
  Screen.MousePointer = vbDefault
  
End Sub

Private Sub cmdCargar_Click()
  Dim lngRespuesta As Long
  Dim strMsg As String
  Dim strTitulo As String

    ' Set CancelError is True
    CommonDialog1.CancelError = True
    On Error GoTo ErrHandler
    
    ' Set flags
    CommonDialog1.Flags = cdlOFNExplorer
    ' Set filters
    CommonDialog1.Filter = "Text Files" & _
    "(*.txt)|*.txt"
    ' Specify default filter
    CommonDialog1.FilterIndex = 0
    ' Display the Open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file

    RUTA = CommonDialog1.filename
    Call cargaDisquete
    Exit Sub
    
ErrHandler:
    'User pressed the Cancel button
    Exit Sub

    
   
    
        
    
End Sub

'Private Sub Command1_Click()
'Dim lngficheroentrada As Long
'Dim strlineadisquete As String
'Dim strexpinf As String
'Dim rst As rdoResultset
'Dim sql As String
'Dim stroperac As String
'
'
'
'
'  lngficheroentrada = FreeFile
'  Open "c:\osasunbi\cargavolantes\cun.txt" For Input Lock Write As lngficheroentrada
'  Do While Not EOF(lngficheroentrada)
'    Input #lngficheroentrada, strlineadisquete
'    strexpinf = Trim(Mid(strlineadisquete, 1, 14))
'    stroperac = Trim(Mid(strlineadisquete, 136, 1))
'
'    If stroperac = "B" Or stroperac = "M" Then
'    Else
'
'    sql = "DELETE FROM FA2500 WHERE FA25EXPINF=" & strexpinf
'    Set rst = objApp.rdoConnect.OpenResultset(sql)
'    End If
'
'  Loop
'  Close lngficheroentrada
'
'End Sub

Private Sub Form_Unload(Cancel As Integer)
Unload Me


End Sub
