VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frm_AnotacionesCartas 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento del Estado de las Cartas de Recordatorio"
   ClientHeight    =   4935
   ClientLeft      =   150
   ClientTop       =   675
   ClientWidth     =   10500
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   10500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   " Estado de la Carta de Recordatorio "
      Height          =   2085
      Left            =   45
      TabIndex        =   11
      Top             =   2520
      Width           =   10410
      Begin VB.ComboBox cboDesDestCare 
         Enabled         =   0   'False
         Height          =   315
         Left            =   2745
         TabIndex        =   13
         Top             =   315
         Width           =   3480
      End
      Begin VB.TextBox txtCodDestCare 
         Height          =   285
         Left            =   2745
         TabIndex        =   15
         Top             =   315
         Width           =   285
      End
      Begin VB.TextBox txtObservaciones 
         Enabled         =   0   'False
         Height          =   1005
         Left            =   1260
         MaxLength       =   500
         MultiLine       =   -1  'True
         TabIndex        =   14
         Top             =   720
         Width           =   8970
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar Datos"
         Height          =   285
         Left            =   8640
         TabIndex        =   12
         Top             =   270
         Width           =   1590
      End
      Begin VB.Label Label2 
         Caption         =   "Observaciones"
         Height          =   240
         Left            =   90
         TabIndex        =   17
         Top             =   675
         Width           =   1140
      End
      Begin VB.Label Label1 
         Caption         =   "Estado de la Carta de Recordatorio"
         Height          =   240
         Left            =   135
         TabIndex        =   16
         Top             =   360
         Width           =   3030
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   " Responsable econ�mico "
      Height          =   2115
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   405
      Width           =   10485
      Begin TabDlg.SSTab tabPacientes 
         Height          =   1725
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   270
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   3043
         _Version        =   327681
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00167.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "IdPersona1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         Begin idperson.IdPersona IdPersona1 
            Height          =   1365
            Left            =   90
            TabIndex        =   3
            Top             =   315
            Width           =   10140
            _ExtentX        =   17886
            _ExtentY        =   2408
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            Top             =   450
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18865
            _ExtentY        =   2302
            _StockProps     =   79
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   4575
            TabIndex        =   8
            Tag             =   "Nombre Persona|Nombre Persona"
            Top             =   405
            Visible         =   0   'False
            Width           =   1950
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   6840
            TabIndex        =   5
            Tag             =   "D.N.I|D.N.I "
            Top             =   405
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   7140
            TabIndex        =   7
            Tag             =   "Primer Apellido|Primer Apellido"
            Top             =   855
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   7380
            TabIndex        =   6
            Tag             =   "Segundo Apellido|Segundo Apellido"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8790
            TabIndex        =   4
            Tag             =   "N�mero Historia"
            Top             =   420
            Visible         =   0   'False
            Width           =   1410
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   10
      Top             =   4665
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   476
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frm_AnotacionesCartas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public intIndGrid As Integer
Dim objMasterInfo     As New clsCWForm
Dim Responsable As Long
Const ConvX = 54.0809
Const ConvY = 54.6101086

Private Type CompFactura
  NoFactura As String
  FechaFact As String
  CantFact As String
  ImpComp As String
End Type

Dim CompFact() As CompFactura
Dim Cambiado As Boolean

Sub CargarDatos(CodPersona As Long)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim MiRs2 As rdoResultset
  
  MiSql = "Select NVL(FA21CODDESTCARE,0) AS CODIGO From CI2200 Where CI21CODPERSONA = " & CodPersona & _
          "Union All " & _
          "Select NVL(FA21CODDESTCARE,0) AS CODIGO From CI2300 Where CI21CODPERSONA = " & CodPersona
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If MiRs("Codigo") <> 0 Then
    Me.txtCodDestCare.Text = MiRs("Codigo")
    MiSql = "Select FA21DESDESTCARE From FA2100 Where FA21CODDESTCARE = " & MiRs("Codigo")
    Set MiRs2 = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs2.EOF Then
      Me.cboDesDestCare.Text = MiRs2("FA21DESDESTCARE")
    Else
      Me.cboDesDestCare.Text = ""
    End If
  Else
    Me.cboDesDestCare.Text = ""
  End If
  MiSql = "Select CI21OBSADMIN From CI2100 Where CI21CODPERSONA = " & CodPersona
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Me.txtObservaciones = IIf(Not IsNull(MiRs("CI21OBSADMIN")), MiRs("CI21OBSADMIN"), "")
  Else
    Me.txtObservaciones = ""
  End If

End Sub

Sub GuardarDatos(CodPersona As Long)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim SqlUpd21 As String
Dim SqlUpd22 As String

  If Trim(Me.txtCodDestCare) <> "" And IsNumeric(txtCodDestCare) = False Then
    MsgBox "Compruebe el estado de la carta, pues puede ser que no sea un dato correcto", vbExclamation + vbOKOnly, "Grabaci�n de los datos"
    Exit Sub
  End If
  
  MiSql = "Select CI33CODTIPPERS From CI2100 Where CI21CODPERSONA = " & CodPersona
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If MiRs("CI33CODTIPPERS") = 1 Then
    SqlUpd22 = "Update CI2200 Set FA21CODDESTCARE = " & Me.txtCodDestCare & " Where CI21CODPERSONA = " & CodPersona
    objApp.rdoConnect.Execute SqlUpd22
  ElseIf MiRs("CI33CODTIPPERS") = 2 Then
    SqlUpd22 = "Update CI2300 Set FA21CODDESTCARE = " & Me.txtCodDestCare & " Where CI21CODPERSONA = " & CodPersona
    objApp.rdoConnect.Execute SqlUpd22
  End If
  SqlUpd21 = "Update CI2100 Set CI21OBSADMIN = '" & Me.txtObservaciones & "'" & " Where CI21CODPERSONA = " & CodPersona
  objApp.rdoConnect.Execute SqlUpd21
  

End Sub

Sub BorrarDatos(CodPersona As Long)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim SqlUpd21 As String
Dim SqlUpd22 As String

  MiSql = "Select CI33CODTIPPERS From CI2100 Where CI21CODPERSONA = " & CodPersona
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If MiRs("CI33CODTIPPERS") = 1 Then
    SqlUpd22 = "Update CI2200 Set FA21CODDESTCARE = 0 Where CI21CODPERSONA = " & CodPersona
    objApp.rdoConnect.Execute SqlUpd22
  ElseIf MiRs("CI33CODTIPPERS") = 2 Then
    SqlUpd22 = "Update CI2300 Set FA21CODDESTCARE = 0 Where CI21CODPERSONA = " & CodPersona
    objApp.rdoConnect.Execute SqlUpd22
  End If
  SqlUpd21 = "Update CI2100 Set CI21OBSADMIN = '' Where CI21CODPERSONA = " & CodPersona
  objApp.rdoConnect.Execute SqlUpd21
  

End Sub


Public Sub pGestion(Respon As Long)

  Responsable = Respon
  frm_AnotacionesCartas.Show (vbModal)
  Unload Me
  
End Sub





Private Sub cboDesDestCare_Click()
Dim MiSql As String
Dim MiRs As rdoResultset

  If Trim(Me.cboDesDestCare) <> "" Then
    MiSql = "Select FA21CODDESTCARE From FA2100 Where FA21DESDESTCARE = '" & Me.cboDesDestCare.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.txtCodDestCare.Text = MiRs("FA21CODDESTCARE")
    Else
      MsgBox "No se ha encontrado el estado de la carta"
      Me.txtCodDestCare.Text = ""
      Me.cboDesDestCare.Text = ""
    End If
  Else
    Me.txtCodDestCare = ""
  End If
  Cambiado = True
End Sub

Private Sub cboDesDestCare_DropDown()
Dim MiSql As String
Dim MiRs As rdoResultset

  cboDesDestCare.Clear
  MiSql = "Select FA21DESDESTCARE From FA2100 Order by FA21CODDESTCARE"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      cboDesDestCare.AddItem MiRs("FA21DESDESTCARE")
      MiRs.MoveNext
    Wend
  End If
  
End Sub


Private Sub cboDesDestCare_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset
  
  If Trim(Me.cboDesDestCare) <> "" Then
    MiSql = "Select FA21CODDESTCARE From FA2100 Where FA21DESDESTCARE = '" & Me.cboDesDestCare.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.txtCodDestCare.Text = MiRs("FA21CODDESTCARE")
    Else
      MsgBox "No se ha encontrado el estado de la carta"
      Me.txtCodDestCare.Text = ""
      Me.cboDesDestCare.Text = ""
    End If
  Else
    Me.txtCodDestCare = ""
  End If
End Sub

Private Sub cmdModificar_Click()
  Me.cboDesDestCare.Enabled = True
  Me.txtObservaciones.Enabled = True
  Me.tlbToolbar1.Buttons(4).Enabled = True
  Me.tlbToolbar1.Buttons(8).Enabled = True
  Me.tlbToolbar1.Buttons(26).Enabled = True
  Cambiado = False
End Sub

Private Sub Form_Load()
  
  Dim intGridIndex      As Integer
  Call objApp.SplashOn
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleOpen, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
  
  
  With objMasterInfo
    '.strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPacientes
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    'If Responsable <> 0 Then
    '  .strWhere = "CI21CODPERSONA = " & Responsable
    'Else
      .strInitialWhere = "CI21CODPERSONA = 1000001"
   ' End If
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", False)
    .blnChanged = False
  End With
  
  
  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormCreateInfo(objMasterInfo)
    
    IdPersona1.ToolTipText = ""
    .CtrlGetInfo(IdPersona1).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
 Call .WinRegister
    Call .WinStabilize
    IdPersona1.BackColor = objApp.objUserColor.lngKey
'    blncommit = False
  End With
      
      

 Call objApp.SplashOff
 IdPersona1.Text = Responsable
    'Invisible bot�n de B�squeda
'  IdPersona1.blnAvisos = False
  'Activamos los botonses de Guardar y Eliminar.
  tlbToolbar1.Buttons(8).Enabled = True
End Sub

Private Sub grdDBGrid1_Change(Index As Integer)

  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, Y As Single)
'  If Button = 2 Then
'    PopupMenu mnuEstado
'  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub


Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
  If Trim(IdPersona1.Text) <> "" And IsNumeric(IdPersona1.Text) Then
    If CLng(IdPersona1.Text) >= 800000 Then
      Call Me.CargarDatos(IdPersona1.Text)
    End If
  End If
  Me.tlbToolbar1.Buttons(8).Enabled = True
End Sub

Private Sub IdPersona1_GotFocus()
'  frmCitasPaciente.MousePointer = vbHourglass
'  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.CtrlGotFocus
'  frmCitasPaciente.MousePointer = vbDefault
End Sub

Private Sub IdPersona1_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset
  
  If Trim(IdPersona1) <> "" Then
    If Left(IdPersona1.Text, 2) <> "80" Then
      MiSql = "Select CI23RAZONSOCIAL From CI2300 Where CI21CODPERSONA = " & IdPersona1.Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        GoTo Entidad
      Else
        Call objWinInfo.CtrlLostFocus
      End If
    Else
Entidad:
      'MiSqL = "select ci13desentidad from ci1300,ci2900 " & _
              "where ci1300.ci13codentidad = ci2900.ci13codentidad " & _
              "and ci1300.ci32codtipecon = ci2900.ci32codtipecon " & _
              "and ci2900.ci21codpersona_rec = " & IdPersona1.Text
      MiSql = "Select CI23RAZONSOCIAL From CI2300 Where CI21CODPERSONA = " & IdPersona1.Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
      If Not MiRs.EOF Then
        If Not IsNull(MiRs("CI23RAZONSOCIAL")) Then
          IdPersona1.Nombre = MiRs("CI23RAZONSOCIAL")
        End If
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  If strFormName = "fraFrame1(0)" And strCtrl = "IdPersona1" Then
     IdPersona1.SearchPersona
     objWinInfo.DataRefresh
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  'fraFrame1(9).ForeColor = fraFrame1(tabTab1(2).Tab + 1).ForeColor
  If strFormName = "fraFrame1(0)" Then
    IdPersona1.blnAvisos = True
    tlbToolbar1.Buttons(26).Enabled = True

  Else
    IdPersona1.blnAvisos = False
    tlbToolbar1.Buttons(26).Enabled = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
      objWinInfo.DataRefresh
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
    End If
  End If
  
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = "fraFrame1(0)" Then
'    Call IdPersona1.ReadPersona
'  End If

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabPacientes_Click(PreviousTab As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Me.MousePointer = vbDefault
End Sub

Private Sub tabPacientes_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)

End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim Respuesta As Integer

  objMasterInfo.blnChanged = False
  If btnButton.Index = 3 Then
    'Bot�n de nuevo
  ElseIf btnButton.Index = 4 Then
    'Boton de guardar
    Respuesta = MsgBox("�Desea guardar el estado de la carta y las observaciones de la historia  " & IdPersona1.Historia & _
                       "?", vbYesNo + vbQuestion, "Actualizaci�n de los datos")
    If Respuesta = vbYes Then
      Call GuardarDatos(IdPersona1.Text)
'      MsgBox "Guardando los datos"
      Me.cboDesDestCare.Enabled = False
      Me.txtObservaciones.Enabled = False
      Cambiado = False
    End If
  ElseIf btnButton.Index = 8 Then
    'Bot�n de borrado de datos
    Respuesta = MsgBox("�Desea borrar el estado de la carta y las observaciones de la historia " & IdPersona1.Historia & _
                       "?", vbYesNo + vbQuestion, "Borrado de datos")
    If Respuesta = vbYes Then
      Call Me.BorrarDatos(IdPersona1.Text)
      Me.cboDesDestCare.Text = ""
      Me.txtObservaciones.Text = ""
      Me.cboDesDestCare.Enabled = False
      Me.txtObservaciones.Enabled = False
    End If
  ElseIf btnButton.Index = 16 Then
    'Bot�n de Buscar
    Call IdPersona1.Buscar
    Me.tlbToolbar1.Buttons(8).Enabled = True
  ElseIf btnButton.Index = 26 Then
    'Bot�n de recargar datos
    Respuesta = MsgBox("�Desea recargar los datos de la persona actual?", vbYesNo + vbQuestion, "Recarga de datos")
    If Respuesta = vbYes Then
      Call Me.CargarDatos(IdPersona1.Text)
    End If
  ElseIf btnButton.Index >= 21 And btnButton.Index <= 24 Then
    Me.cboDesDestCare.Enabled = False
    Me.txtObservaciones.Enabled = False
    If Cambiado = True Then
      Respuesta = MsgBox("�Desea guardar el estado de la carta y las observaciones de la historia  " & IdPersona1.Historia & _
                       "?", vbYesNo + vbQuestion, "Actualizaci�n de los datos")
      If Respuesta = vbYes Then
        Call GuardarDatos(IdPersona1.Text)
      End If
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    tlbToolbar1.Buttons(8).Enabled = True
    Cambiado = False
  ElseIf btnButton.Index = 30 Then
    If Cambiado = True Then
      Respuesta = MsgBox("�Desea guardar el estado de la carta y las observaciones de la historia  " & IdPersona1.Historia & _
                       "?", vbYesNoCancel + vbQuestion, "Actualizaci�n de los datos")
      If Respuesta = vbYes Then
        Call GuardarDatos(IdPersona1.Text)
      ElseIf Respuesta = vbCancel Then
        Exit Sub
      End If
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub



Private Sub txtObservaciones_KeyPress(KeyAscii As Integer)
  Cambiado = True
End Sub


