VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_ImprimeHASS 
   Caption         =   "Form1"
   ClientHeight    =   6555
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7980
   LinkTopic       =   "Form1"
   ScaleHeight     =   6555
   ScaleWidth      =   7980
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   555
      Left            =   90
      TabIndex        =   1
      Top             =   90
      Width           =   645
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   5910
      Left            =   1485
      TabIndex        =   0
      Top             =   90
      Width           =   5415
      _Version        =   196608
      _ExtentX        =   9551
      _ExtentY        =   10425
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
End
Attribute VB_Name = "frm_ImprimeHASS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ConvX = 54.0809
Const ConvY = 54.6101086

Dim Tipo As String
Dim Historia As String
Dim Nombre As String
Dim SegSocial As String
Dim Afiliado As String
Dim Ingreso As String
Dim Departamento As String
Dim Direccion As String
Dim Poblacion As String
Dim Alta As String
Dim Hora As String
Dim Cama As String
Dim Fecha As String
Dim HistAsist As String
Dim Prov As String

Dim qry(1 To 10) As New rdoQuery

Private Type LinFactura
  Cantidad As Double
  Descripcion As String
  ImpUnit As Double
  ImpTotal As Double
End Type

Dim Lineas() As LinFactura
Sub ImprimirDatos()
Dim Texto As String
  With vsPrinter
    .MarginLeft = 0
    .MarginTop = 0
    .Preview = True
    .PenStyle = psSolid
    
    .DrawLine 26 * ConvX, 19 * ConvY, 26 * ConvX, 24 * ConvY
    .FontName = "Courier New"
    .FontSize = 10
    .TextAlign = taLeftBaseline
    .CurrentY = 24 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Tipo & "      " & HistAsist
    .CurrentY = 28 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Nombre
    .CurrentY = 32 * ConvY
    .CurrentX = 28 * ConvX
    .Text = SegSocial & " " & Left(Afiliado & Space(13), 13) & " " & Prov
    .CurrentY = 36 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Ingreso
    .CurrentY = 40 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Departamento
    .CurrentY = 44 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Direccion
    .CurrentY = 48 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Poblacion
    .CurrentY = 33 * ConvY
    .CurrentX = 112 * ConvX
    .Text = Right(Space(12) & Cama, 12)
    .CurrentY = 49 * ConvY
    .CurrentX = 112 * ConvX
    .Text = Right(Space(12) & Hora, 12)
    .CurrentY = 45 * ConvY
    .CurrentX = 112 * ConvX
    .Text = Right(Space(12) & Alta, 12)
    'dibujamos todo el rayado
    .DrawRectangle 7 * ConvX, 94 * ConvY, 141 * ConvX, 146 * ConvY
    .FontSize = 10
    '.TextAlign = taLeftBaseline
    '.CurrentY = 107 * ConvY
    '.CurrentX = 15 * ConvX
    '.Text = Lineas(1).Descripcion
    '.CurrentY = 107 * ConvY
    '.CurrentX = 84 * ConvX
    '.Text = Right(Space(12) & Lineas(1).ImpUnit, 12) & " "
    '.CurrentY = 107 * ConvY
    '.CurrentX = 110 * ConvX
    '.Text = Right(Space(14) & Lineas(1).ImpTotal, 14) & " "
    '.TextAlign = taLeftBaseline
    '.CurrentY = 111 * ConvY
    '.CurrentX = 15 * ConvX
    '.Text = Lineas(2).Descripcion
    '.CurrentY = 111 * ConvY
    '.CurrentX = 110 * ConvX
    '.Text = Right(Space(15) & Lineas(2).ImpTotal, 15) & " "
    .CurrentY = 149 * ConvY
    .CurrentX = 5 * ConvX
    '.Text = "2611991133"
    .FontSize = 9
    .CurrentY = 155 * ConvY
    .CurrentX = 57 * ConvX
    .Text = "Pamplona, " & Format(Alta, "d ""de ""MMMM"" de ""yyyy")
  End With

End Sub

Sub ImprimirHASSFactura(NumProc As Long, NumAsist As Long)
Dim i As Integer
Dim curdev%
  
  For i = 0 To vsPrinter.NDevices - 1
    If vsPrinter.Devices(i) = "\\CPU00605\ADMINISTRACION" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "ADMINISTRACION" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "\\CPU00613\HPFACT" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "HPFACT" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "\\CPU02223\HP" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "HP" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "\\CPU00609\HP DeskJet 600" Then
      'cmb_printers.AddItem vsPrinter.Devices(I)
    ElseIf vsPrinter.Devices(i) = "HP Deskjet 600" Then
      'cmb_printers.AddItem VSPRINTER.Devices(I)
    End If
    If vsPrinter.Devices(i) = vsPrinter.Device Then curdev = i
  Next
  If objSecurity.strMachine = "CPU00609" Then
   ' vsPrinter.Device = "HP DeskJet 600"
  Else
   ' vsPrinter.Device = "\\CPU00609\HP DeskJet 600"
  End If
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  Call Me.IniciarQRY
  Call ObtenerDatos(NumProc, NumAsist)
  Call ImprimirImpreso
  Call ImprimirDatos
  Call ImprimirLineas
  vsPrinter.Action = 6
  
End Sub

Sub ImprimirImpreso()
Dim Texto As String
  With vsPrinter
    .MarginLeft = 0
    .MarginTop = 0
    .Preview = False
    .PenStyle = psSolid
    .DrawLine 26 * ConvX, 19 * ConvY, 26 * ConvX, 24 * ConvY
    .DrawLine 111 * ConvX, 19 * ConvY, 111 * ConvX, 24 * ConvY
    .DrawLine 26 * ConvX, 19 * ConvY, 31 * ConvX, 19 * ConvY
    .DrawLine 106 * ConvX, 19 * ConvY, 111 * ConvX, 19 * ConvY
    .DrawLine 26 * ConvX, 47 * ConvY, 26 * ConvX, 52 * ConvY
    .DrawLine 111 * ConvX, 47 * ConvY, 111 * ConvX, 52 * ConvY
    .DrawLine 26 * ConvX, 52 * ConvY, 31 * ConvX, 52 * ConvY
    .DrawLine 106 * ConvX, 52 * ConvY, 111 * ConvX, 52 * ConvY
    .TextAlign = taCenterBaseline
    .FontSize = 13
    .FontName = "times New Roman"
    .CurrentY = 12 * ConvY
    .Text = "CLINICA UNIVERSITARIA"
    .Paragraph = ""
    .FontBold = False
    .FontSize = 7
    .TextAlign = taLeftBaseline
    .CurrentY = 26 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Concertado"
    .CurrentY = 30 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Asistencia a D."
    .CurrentY = 34 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "N.� Afiliado"
    .CurrentY = 38 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "INGRESO"
    .CurrentY = 42 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Dr. encargado"
    .CurrentY = 46 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Domicilio"
    .CurrentY = 37 * ConvY
    .CurrentX = 126 * ConvX
    .Text = "SALIDA"
    .CurrentY = 42 * ConvY
    .CurrentX = 125 * ConvX
    .Text = "D�a y hora"
    .CurrentY = 42 * ConvY
    .CurrentX = 124 * ConvX
    .FontSize = 8
    .CurrentY = 61 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Datos de la persona que firma la conformidad a esta factura:"
    .CurrentY = 65 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Nombre:"
    .CurrentY = 73 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Domicilio:"
    .CurrentY = 82 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Vinculaci�n con el enfermo:"
    .FontBold = True
    .FontSize = 10
    .CurrentY = 91 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "SERVICIOS PRESTADOS:"
    'dibujamos todo el rayado
    .BrushStyle = bsTransparent
    .PenStyle = psSolid
    .DrawRectangle 7 * ConvX, 94 * ConvY, 141 * ConvX, 146 * ConvY
    .DrawLine 7 * ConvX, 102 * ConvY, 141 * ConvX, 102 * ConvY
    .PenStyle = psDot
    .DrawLine 7 * ConvX, 108 * ConvY, 37 * ConvX, 108 * ConvY
    .DrawLine 47 * ConvX, 108 * ConvY, 141 * ConvX, 108 * ConvY
    .DrawLine 7 * ConvX, 112 * ConvY, 141 * ConvX, 112 * ConvY
    .DrawLine 7 * ConvX, 116 * ConvY, 141 * ConvX, 116 * ConvY
    .DrawLine 7 * ConvX, 120 * ConvY, 141 * ConvX, 120 * ConvY
    .DrawLine 7 * ConvX, 124 * ConvY, 141 * ConvX, 124 * ConvY
    .DrawLine 7 * ConvX, 128 * ConvY, 141 * ConvX, 128 * ConvY
    .DrawLine 7 * ConvX, 132 * ConvY, 141 * ConvX, 132 * ConvY
    .DrawLine 7 * ConvX, 137 * ConvY, 141 * ConvX, 137 * ConvY
    .PenStyle = psSolid
    .DrawLine 83 * ConvX, 138 * ConvY, 141 * ConvX, 138 * ConvY
    .DrawLine 83 * ConvX, 94 * ConvY, 83 * ConvX, 146 * ConvY
    .DrawLine 109 * ConvX, 94 * ConvY, 109 * ConvX, 146 * ConvY
    .FontBold = False
    .FontSize = 8
    .CurrentY = 99 * ConvY
    .CurrentX = 37 * ConvX
    .Text = "DETALLE"
    .CurrentY = 99 * ConvY
    .CurrentX = 88 * ConvX
    .Text = "Precio unitario"
    .CurrentY = 99 * ConvY
    .CurrentX = 120 * ConvX
    .Text = "IMPORTE"
    .Paragraph = ""
    .FontSize = 6
    .CurrentY = 108.5 * ConvY
    .CurrentX = 35 * ConvX
    .Text = "d�as estancia"
    .Paragraph = ""
    .Paragraph = ""
    .FontSize = 8
    .CurrentY = 161 * ConvY
    .CurrentX = 8 * ConvX
    .FontSize = 7
    .Text = "POR EL CENTRO CONCERTADO"
    .Paragraph = ""
    .CurrentY = 161 * ConvY
    .CurrentX = 99 * ConvX
    .Text = "CONFORME"
    .Paragraph = ""
    .FontSize = 6
    .CurrentY = 175 * ConvY
    .CurrentX = 88 * ConvX
    .Text = "(Firma del paciente o acompa�ante)"
    .PenStyle = psDot
    .DrawLine 67 * ConvX, 172 * ConvY, 143 * ConvX, 172 * ConvY
  End With
End Sub

Sub ImprimirLineas()

Dim Texto As String
Dim X As Integer
Dim Cont As Integer
Dim PosY

  Cont = 1
  PosY = 107
  For X = 1 To UBound(Lineas)
    If Lineas(X).ImpTotal <> 0 Then
      With vsPrinter
        .FontSize = 10
        .TextAlign = taLeftBaseline
        .CurrentY = PosY * ConvY
        If Lineas(X).Cantidad = 0 Then
          Lineas(X).Cantidad = 1
        End If
        .CurrentX = 15 * ConvX - .TextWidth(Lineas(X).Cantidad & " ")
        .Text = Lineas(X).Cantidad & " " & Lineas(X).Descripcion
        .CurrentY = PosY * ConvY
        .CurrentX = 83 * ConvX
        If Lineas(X).ImpUnit <> 0 And Lineas(X).Descripcion = "Estancias" Then
          .Text = Right(Space(12) & Format(Lineas(X).ImpUnit, "#,###,##0,##"), 12) & " "
        Else
          .Text = Space(12)
        End If
        .CurrentY = PosY * ConvY
        .CurrentX = 110 * ConvX
        .Text = Right(Space(14) & Format(Lineas(X).ImpTotal, "###,###,##0,##"), 14) & " "
        Cont = Cont + 1
        PosY = PosY + 4
        If Cont = 9 Then
          .FontSize = 12
          .CurrentY = 142 * ConvY
          .CurrentX = 59 * ConvX
          .Text = "SUMA Y SIGUE"
          .NewPage
          .Action = paNewPage
          Call Me.ImprimirImpreso
          Call Me.ImprimirDatos
          Cont = 1
        End If
      End With
    End If
  Next
  vsPrinter.FontSize = 12
  vsPrinter.CurrentY = 142 * ConvY
  vsPrinter.CurrentX = 59 * ConvX
  vsPrinter.Text = "TOTAL. . . . ."
End Sub

Sub IniciarQRY()
Dim MiSql As String
Dim i As Integer

  MiSql = "Select  AD01FECINICIO, AD01FECFIN, CI22NUMHISTORIA From AD0100 Where " & _
          "AD01CODASISTENCI = ?"
  qry(1).SQL = MiSql
  MiSql = "Select CI22PRIAPEL||', '||CI22SEGAPEL ||', '|| CI22NOMBRE As Nombre, CI22NUMSEGSOC,CI21CODPERSONA " & _
          "From CI2200 Where CI22NUMHISTORIA = ?"
  qry(2).SQL = MiSql
  
  MiSql = "Select GCFN06(AD15CODCAMA) as Cama From AD1600 Where AD01CODASISTENCI = ? And AD07CODPROCESO = ? ORDER BY AD16FECFIN DESC"
  qry(3).SQL = MiSql

  MiSql = "Select AD02DESDPTO From AD0200, AD0500 Where " & _
          "AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
          "And AD0500.AD01CODASISTENCI = ? " & _
          "And AD0500.AD07CODPROCESO = ?"
  qry(4).SQL = MiSql
  MiSql = "Select AD12DESTIPOASIST From AD1200,AD2500 " & _
          "Where AD1200.AD12CODTIPOASIST = AD2500.AD12CODTIPOASIST " & _
          "And AD2500.AD01CODASISTENCI = ?"
  qry(5).SQL = MiSql
  MiSql = "Select * From FA1600,FA0400 " & _
          "Where FA0400.FA04CODFACT = FA1600.FA04CODFACT " & _
          "And AD01CODASISTENCI = ? And AD07CODPROCESO = ? " & _
          "And FA16IMPORTE <> 0"
  qry(6).SQL = MiSql

  MiSql = "Select * From FA0300 Where FA04NUMFACT = ? AND FA16NUMLINEA = ?" ' AND FA03INDFACT <> 0"
  qry(7).SQL = MiSql
  
  For i = 1 To 7
    Set qry(i).ActiveConnection = objApp.rdoConnect
    qry(i).Prepared = True
  Next
                         
                
End Sub

Sub ObtenerDatos(proceso As Long, Asitencia As Long)

Dim rs01 As rdoResultset
Dim rs22 As rdoResultset
Dim rs10 As rdoResultset
Dim rs02 As rdoResultset
Dim rs12 As rdoResultset
Dim rs16 As rdoResultset
Dim rsA16 As rdoResultset
Dim rs03 As rdoResultset
Dim Cont As Integer
Dim Persona As New Persona
Dim CodPersona As Long
  
  'Seleccionamos la fecha de inicio, fecha fin y n�mero de historia de una asistencia
  qry(1).rdoParameters(0) = Asitencia
  Set rs01 = qry(1).OpenResultset
  If Not rs01.EOF Then
    If Not IsNull(rs01("AD01FECINICIO")) Then
      Ingreso = Format(rs01("AD01FECINICIO"), "DD/MM/YYYY     HH:MM")
    Else
      Ingreso = ""
    End If
    If Not IsNull(rs01("AD01FECFIN")) Then
      Alta = Format(rs01("AD01FECFIN"), "DD/MM/YYYY")
      Hora = Format(rs01("AD01FECFIN"), "HH:MM")
    Else
      Alta = ""
      Hora = ""
    End If
    If Not IsNull(rs01("CI22NUMHISTORIA")) Then
      Historia = rs01("CI22NUMHISTORIA")
      HistAsist = rs01("CI22NUMHISTORIA") & "/" & Asitencia
    End If
  End If

  'Selecconamos el nombre y el n� de la seg social de una persona por su historia
  qry(2).rdoParameters(0) = CLng(Historia)
  Set rs22 = qry(2).OpenResultset
  If Not rs22.EOF Then
    If Not IsNull(rs22("Nombre")) Then
      Nombre = rs22("Nombre")
    Else
      Nombre = ""
    End If
    If Not IsNull(rs22("CI22NUMSEGSOC")) Then
      SegSocial = rs22("CI22NUMSEGSOC")
    End If
    If Not IsNull(rs22("CI21CODPERSONA")) Then
      CodPersona = rs22("CI21CODPERSONA")
      Persona.Codigo = CodPersona
      Direccion = Persona.Direccion
      Poblacion = Persona.Poblacion & "(" & Persona.Provincia & ")"
      Prov = Persona.ProvCorta
    End If
  End If
  
  'Cogemos la �ltima cama en la que ha estado el paciente
  qry(3).rdoParameters(0) = Asitencia
  qry(3).rdoParameters(1) = proceso
  Set rsA16 = qry(3).OpenResultset
  If Not rsA16.EOF Then
    If Not IsNull(rsA16("Cama")) Then
      Cama = rsA16("cama")
    Else
      Cama = ""
    End If
  Else
    Cama = ""
  End If
  
  'Seleccionamos el departamento responsable de un proceso'asistencia
  qry(4).rdoParameters(0) = Asitencia
  qry(4).rdoParameters(1) = proceso
  Set rs02 = qry(4).OpenResultset
  If Not rs02.EOF Then
    If Not IsNull(rs02("AD02DESDPTO")) Then
      Departamento = "DPTO. " & UCase(rs02("AD02DESDPTO"))
    Else
      Departamento = ""
    End If
  End If
  
  'Seleccionamos si el proceso asistencia es Ambulatorio u Hospitalizado
  qry(5).rdoParameters(0) = Asitencia
  Set rs12 = qry(5).OpenResultset
  If Not rs12.EOF Then
    If Not IsNull(rs12("AD12DESTIPOASIST")) Then
      Tipo = UCase(rs12("AD12DESTIPOASIST"))
    Else
      Tipo = ""
    End If
  End If
  Cont = 1
  'Seleccionamos las l�neas de la factura para cada proceso-asistencia
  qry(6).rdoParameters(0) = Asitencia
  qry(6).rdoParameters(1) = proceso
  Set rs16 = qry(6).OpenResultset
  If Not rs16.EOF Then
    'rs16.MoveLast
    'rs16.MoveFirst
    ReDim Preserve Lineas(1 To Cont)
    Cont = 1
    While Not rs16.EOF
      ReDim Preserve Lineas(1 To Cont)
      'Seleccionamos la cantidad y el precio unitario de cada una de las l�neas
      qry(7).rdoParameters(0) = rs16("FA04CODFACT")
      qry(7).rdoParameters(1) = rs16("FA16NUMLINEA")
      Set rs03 = qry(7).OpenResultset
      If Not rs03.EOF Then
        Lineas(Cont).ImpUnit = 0 & rs03("FA03PRECIOFACT")
        Lineas(Cont).Cantidad = 0
        While Not rs03.EOF
          Lineas(Cont).Cantidad = Lineas(Cont).Cantidad + rs03("FA03CANTIDAD")
          rs03.MoveNext
        Wend
      Else
        Lineas(Cont).Cantidad = 0
        Lineas(Cont).ImpUnit = 0
      End If
      'If Not IsNull(rs16("FA16CANTIDAD")) Then
      '  Lineas(Cont).Descripcion = rs16("FA16CANTIDAD")
      'Else
      '  Lineas(Cont).Descripcion = ""
      'End If
      If Not IsNull(rs16("FA16DESCRIP")) Then
        Lineas(Cont).Descripcion = rs16("FA16DESCRIP")
      Else
        Lineas(Cont).Descripcion = ""
      End If
      If Not IsNull(rs16("FA16IMPORTE")) Then
        Lineas(Cont).ImpTotal = rs16("FA16IMPORTE")
      Else
        Lineas(Cont).ImpTotal = 0
      End If
      rs16.MoveNext
      Cont = Cont + 1
    Wend
  End If
End Sub


Private Sub Command1_Click()

  vsPrinter.Preview = False
  vsPrinter.Action = 3
  Call ObtenerDatos(1, 1)
  Call ImprimirImpreso
  Call ImprimirDatos
  Call ImprimirLineas
  vsPrinter.Action = 6
  
End Sub



Private Sub Form_Load()
Dim i As Integer
  Call IniciarQRY
End Sub


