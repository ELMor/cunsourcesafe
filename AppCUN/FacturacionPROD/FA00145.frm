VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Begin VB.Form frmDesglose 
   Caption         =   "Desglose de L�neas de factura"
   ClientHeight    =   6600
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   9600
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6600
   ScaleWidth      =   9600
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Orden"
      Height          =   1455
      Left            =   7380
      TabIndex        =   34
      Top             =   5100
      Width           =   915
      Begin VB.OptionButton optOrden 
         Caption         =   "Alfab."
         Height          =   420
         Index           =   1
         Left            =   45
         TabIndex        =   36
         Top             =   765
         Width           =   825
      End
      Begin VB.OptionButton optOrden 
         Caption         =   "Fecha"
         Height          =   285
         Index           =   0
         Left            =   45
         TabIndex        =   35
         Top             =   315
         Width           =   780
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Caracter�sticas de la categor�a seleccionada "
      Height          =   1455
      Left            =   60
      TabIndex        =   14
      Top             =   5100
      Width           =   7260
      Begin VB.TextBox txtKeyREco 
         Height          =   285
         Left            =   3825
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   360
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.CheckBox chkPermanente 
         Caption         =   "Permanente"
         Height          =   255
         Left            =   2565
         TabIndex        =   3
         Top             =   300
         Width           =   1275
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         Height          =   315
         Left            =   4425
         TabIndex        =   10
         Top             =   240
         Width           =   1275
      End
      Begin VB.CommandButton cmdRecargar 
         Caption         =   "&Recargar"
         Height          =   315
         Left            =   5820
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox txtPropuesta 
         Height          =   285
         Left            =   7785
         TabIndex        =   31
         Text            =   "Text1"
         Top             =   495
         Width           =   165
      End
      Begin VB.CheckBox chkQuitar 
         Caption         =   "Quitar de Factura?"
         Height          =   255
         Left            =   4770
         TabIndex        =   4
         Top             =   180
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.TextBox txtLinea 
         Height          =   285
         Left            =   8340
         TabIndex        =   30
         Top             =   1020
         Width           =   150
      End
      Begin VB.TextBox txtRNF 
         Height          =   285
         Left            =   8460
         TabIndex        =   29
         Top             =   1020
         Width           =   150
      End
      Begin VB.TextBox txtClaveAgrup 
         Height          =   285
         Left            =   8340
         TabIndex        =   24
         Text            =   "Text1"
         Top             =   180
         Width           =   195
      End
      Begin VB.TextBox txtNodoAnterior 
         Height          =   285
         Left            =   8160
         TabIndex        =   23
         Text            =   "Text1"
         Top             =   180
         Width           =   195
      End
      Begin VB.TextBox txtFecha 
         Height          =   285
         Left            =   7980
         TabIndex        =   22
         Text            =   "Text1"
         Top             =   180
         Width           =   195
      End
      Begin VB.TextBox txtRuta 
         Height          =   285
         Left            =   7800
         TabIndex        =   21
         Text            =   "Text1"
         Top             =   180
         Width           =   195
      End
      Begin VB.TextBox txtCategoria 
         Height          =   285
         Left            =   7560
         TabIndex        =   20
         Text            =   "Text1"
         Top             =   180
         Width           =   255
      End
      Begin VB.TextBox txtNodo 
         Height          =   285
         Left            =   7560
         TabIndex        =   19
         Text            =   "Text1"
         Top             =   480
         Width           =   255
      End
      Begin VB.TextBox txtImporte 
         Height          =   285
         Left            =   5640
         TabIndex        =   9
         Top             =   1020
         Width           =   1395
      End
      Begin VB.TextBox txtDto 
         Height          =   285
         Left            =   4140
         TabIndex        =   8
         Top             =   1020
         Width           =   495
      End
      Begin VB.TextBox txtPrecio 
         Height          =   285
         Left            =   2520
         TabIndex        =   7
         Top             =   1020
         Width           =   975
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   285
         Left            =   1140
         TabIndex        =   5
         Top             =   660
         Width           =   5895
      End
      Begin VB.CheckBox chkSoloRNF 
         Caption         =   "Solo RNF?"
         Height          =   255
         Left            =   7320
         TabIndex        =   18
         Top             =   1140
         Width           =   2175
      End
      Begin VB.CheckBox chkFactAnterior 
         Caption         =   "Facturar Anterior"
         Height          =   255
         Left            =   7320
         TabIndex        =   17
         Top             =   900
         Width           =   2175
      End
      Begin VB.CheckBox chkMostAnterior 
         Caption         =   "Mostrar Anterior"
         Height          =   255
         Left            =   7320
         TabIndex        =   16
         Top             =   660
         Width           =   1455
      End
      Begin VB.CheckBox chkFacturar 
         Caption         =   "Sacar a linea independiente"
         Height          =   255
         Left            =   135
         TabIndex        =   2
         Top             =   300
         Width           =   2715
      End
      Begin VB.TextBox txtCantidad 
         Height          =   285
         Left            =   1140
         TabIndex        =   6
         Top             =   1020
         Width           =   615
      End
      Begin VB.CheckBox chkMostrar 
         Caption         =   "Mostrar?"
         Height          =   255
         Left            =   4770
         TabIndex        =   1
         Top             =   405
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "REco"
         Height          =   255
         Index           =   5
         Left            =   3870
         TabIndex        =   33
         Top             =   180
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label Label1 
         Caption         =   "Importe:"
         Height          =   255
         Index           =   4
         Left            =   4965
         TabIndex        =   28
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Dto:"
         Height          =   255
         Index           =   3
         Left            =   3780
         TabIndex        =   27
         Top             =   1080
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Precio:"
         Height          =   255
         Index           =   2
         Left            =   1980
         TabIndex        =   26
         Top             =   1080
         Width           =   555
      End
      Begin VB.Label Label1 
         Caption         =   "Cantidad:"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   25
         Top             =   1080
         Width           =   795
      End
      Begin VB.Label Label1 
         Caption         =   "Descripci�n:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   555
      Left            =   8355
      TabIndex        =   12
      Top             =   5220
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   555
      Left            =   8355
      TabIndex        =   13
      Top             =   5985
      Width           =   1155
   End
   Begin ComctlLib.TreeView tvwDesglose 
      Height          =   4920
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   8678
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00145.frx":0000
            Key             =   "NotaPegada"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00145.frx":031A
            Key             =   "SinDesglose"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00145.frx":0634
            Key             =   "Reloj"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00145.frx":094E
            Key             =   "REco"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnu_Desglose 
      Caption         =   "Desglose"
      Visible         =   0   'False
      Begin VB.Menu mnu_CambiarREco 
         Caption         =   "Cambiar Responsable Econ�mico"
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 1"
            Index           =   1
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 2"
            Index           =   2
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 3"
            Index           =   3
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 4"
            Index           =   4
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 5"
            Index           =   5
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 6"
            Index           =   6
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 7"
            Index           =   7
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 8"
            Index           =   8
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 9"
            Index           =   9
            Visible         =   0   'False
         End
         Begin VB.Menu mnu_REco 
            Caption         =   "REco 10"
            Index           =   10
            Visible         =   0   'False
         End
      End
      Begin VB.Menu mnu_borrar 
         Caption         =   "Borrar Linea"
      End
   End
End
Attribute VB_Name = "frmDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit

Private Type DatosRNFs
  Clave As String
  Mostrar As Integer
  FactOblig As Integer
  Cantidad As Double
  Descripcion As String
  Precio As Double
  Dto As Double
  Importe As Double
  Nodo As String
  Categoria As String
  Ruta As String
  Fecha As Date
  MostrarAnt As Integer
  FactObligAnt As Integer
  NodoAnterior As Integer
  SoloRNF As Boolean
  grdRNF As String
  Quitar As Integer
  Key As String
  Modificado As Boolean
  Permanente As Boolean
  PermanenteAnt As Boolean
  KeyREco As String
End Type

Dim arrayRNFs() As DatosRNFs

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf
Dim Atributos As Nodo

Dim Propuesta As String
Dim Asist As String
Dim NodoFactura As String
Dim CodPaciente As String



Dim PacDesg As String
Dim AsistDesg As String
Dim PropFacDesg As String
Dim NodoDesg As Integer
Dim RespEconom As String

Dim aOrdenacion() As Integer
Dim IndiceRNFs() As Integer
Dim Cambiado As Boolean

' Funci�n para buscar la clave correspondiente a un determinado nodo o categor�a dentro del
' concierto aplicado
' Ruta -> Ruta v�lida para el nodo o cat anterior
'         Ej: ConciertosAplicables(74).Entidad.Nodos(254-000).Nodos(241-003).Categorias(2410-000)
' C�digo -> Clave del nodo o categor�a a buscar.
' Fecha -> Fecha del RNF
' Tipo -> Nos indica si es un (N)odo o una (C)ategor�a.
Function BuscarClave(nodoAnt As Nodo, Codigo As String, Fecha As String, Tipo As String) As String
Dim Nodo As Nodo
Dim blnSeguir As Boolean
Dim IntCont As Integer
Dim Clave As String

  On Error GoTo ErrorEnBuscarClave

  'Inicializamos las variablex que utilzaremos para...
  blnSeguir = True    'Continuar la b�squeda
  IntCont = 0         'Contador de nodos mirados
  
'  'Nos posicionamos en el nodo o categor�a padre.
'  Set Nodo = nodoAnt
  
  'Mientras no encontremos la fecha...
  While blnSeguir
    Clave = Codigo & "-" & Format(IntCont, "000")
    If Tipo = "N" Then
      'Si es nodo buscamos dentro de la colecci�n de los nodos
      Set Nodo = nodoAnt.Nodos(Clave)
    ElseIf Tipo = "C" Then
      'Si es categor�a buscamos dentro de la colecci�n de categor�as
      Set Nodo = nodoAnt.Categorias(Clave)
    End If
    If Format(Fecha, "yyyymmdd") >= Format(Nodo.FecInicio, "yyyymmdd") _
              And Format(Fecha, "yyyymmdd") < Format(Nodo.FecFin, "yyyymmdd") Then
      'La fecha est� entre ellas.
      BuscarClave = Clave
      blnSeguir = False
    End If
    IntCont = IntCont + 1
  Wend
  
Exit Function
ErrorEnBuscarClave:
  'No se ha encontrado la key
  If Err = 5 Then
    Clave = ""
  End If
End Function

Function ModificarNodo(Propuesta As Integer, Linea As Integer, Categ As Integer) As Nodo
Dim strRestante As String
Dim intPunto As Integer
Dim intBarra As Integer
Dim blnNodos As Boolean
Dim Categoria As String
Dim Entidad As Boolean
Dim CadenaBusqueda As Variant
Dim NodoAtrib As Nodo
Dim Ruta As String
  
  Ruta = arrayRNFs(Propuesta, Linea, Categ).Ruta
  If Ruta <> "" Then
    'Inicializamos la cadena de b�squeda y el string que analizaremos
    CadenaBusqueda = ""
    strRestante = Ruta
    blnNodos = True
    Entidad = True
    'Mientras quede una parte del string a analizar...
    While strRestante <> ""
      'Analizamos la parte correspondiente a los nodos...
      While blnNodos = True
        If Left(strRestante, 1) = "." Then
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        'Buscamos la posici�n del punto
        intPunto = InStr(1, strRestante, ".", 1)
        If intPunto <> 0 Then
          Categoria = Left(strRestante, intPunto - 1)
          If Entidad = True Then
            'Al ser la primera se trata de la entidad
            Set NodoAtrib = varFact.ConciertosAplicables(Trim(str(Categoria))).Entidad
            Entidad = False
          Else
            'Buscamos la clave correcta, indic�ndole que la busque en nodos
            Categoria = BuscarClave(NodoAtrib, Categoria, CStr(arrayRNFs(Propuesta, Linea, Categ).Fecha), "N")
          Debug.Print Categoria
            Set NodoAtrib = NodoAtrib.Nodos(CStr(Categoria))
          End If
          strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
        If Mid(strRestante, 1, 1) = "/" Then
          'Encontramos la barra que separa los nodos de las categor�as y la eliminamos. Ahora son cat.
          blnNodos = False
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        If strRestante = "" Then
          blnNodos = False
        End If
      Wend
      'Pasamos a analizar la parte correspondiente a las categor�as
      If Left(strRestante, 1) = "." Then
        strRestante = Right(strRestante, Len(strRestante) - 1)
      End If
      intPunto = InStr(1, strRestante, ".", 1)
      If intPunto <> 0 Then
        'Encontramos el punto
        Categoria = Left(strRestante, intPunto - 1)
        'Buscamos la clave correcta, indic�ndole que la busque en categor�as
        If Trim(Categoria) <> "" Then
          Categoria = BuscarClave(NodoAtrib, Categoria, CStr(arrayRNFs(Propuesta, Linea, Categ).Fecha), "C")
          Debug.Print Categoria
          Set NodoAtrib = NodoAtrib.Categorias(CStr(Categoria))
          strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
      End If
    Wend
  End If
  
  If arrayRNFs(Propuesta, Linea, Categ).Mostrar = 1 Then
    NodoAtrib.LinOblig = True
  Else
    NodoAtrib.LinOblig = False
  End If
  
  If arrayRNFs(Propuesta, Linea, Categ).FactOblig = 1 Then
    NodoAtrib.FactOblig = True
    ' se anula el desglose en esta linea para que no aparezca la
    ' descripcion dos veces
    NodoAtrib.Desglose = False
  Else
    NodoAtrib.FactOblig = False
  End If
  
    If Not arrayRNFs(Propuesta, Linea, Categ).Permanente Then
        Select Case NodoAtrib.TipoPrecio
          Case tpPrecioDia
            NodoAtrib.PrecioDia = arrayRNFs(Propuesta, Linea, Categ).Precio
          Case tpPrecioRef
            NodoAtrib.PrecioRef = arrayRNFs(Propuesta, Linea, Categ).Precio
          Case Else
            NodoAtrib.PrecioRef = arrayRNFs(Propuesta, Linea, Categ).Precio

        End Select
        
        NodoAtrib.Descuento = arrayRNFs(Propuesta, Linea, Categ).Dto
        NodoAtrib.Name = arrayRNFs(Propuesta, Linea, Categ).Descripcion
    End If
End Function


Function BuscarNodo(Ruta As String, FecRnf As String) As Nodo
Dim strRestante As String
Dim intPunto As Integer
Dim intBarra As Integer
Dim blnNodos As Boolean
Dim Categoria As String
Dim Entidad As Boolean
Dim CadenaBusqueda As Variant
Dim NodoAtrib As Nodo

  If Ruta <> "" Then
    'Inicializamos la cadena de b�squeda y el string que analizaremos
    CadenaBusqueda = ""
    strRestante = Ruta
    blnNodos = True
    Entidad = True
    'Mientras quede una parte del string a analizar...
    While strRestante <> ""
      'Analizamos la parte correspondiente a los nodos...
      While blnNodos = True
        If Left(strRestante, 1) = "." Then
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        'Buscamos la posici�n del punto
        intPunto = InStr(1, strRestante, ".", 1)
        If intPunto <> 0 Then
          Categoria = Left(strRestante, intPunto - 1)
          If Entidad = True Then
            'Al ser la primera se trata de la entidad
            Set NodoAtrib = varFact.ConciertosAplicables(Trim(str(Categoria))).Entidad
            Entidad = False
          Else
            'Buscamos la clave correcta, indic�ndole que la busque en nodos
            Categoria = BuscarClave(NodoAtrib, Categoria, FecRnf, "N")
            Set NodoAtrib = NodoAtrib.Nodos(CStr(Categoria))
          End If
          strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
        If Mid(strRestante, 1, 1) = "/" Then
          'Encontramos la barra que separa los nodos de las categor�as y la eliminamos. Ahora son cat.
          blnNodos = False
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        If strRestante = "" Then
          blnNodos = False
        End If
      Wend
      'Pasamos a analizar la parte correspondiente a las categor�as
      If Left(strRestante, 1) = "." Then
        strRestante = Right(strRestante, Len(strRestante) - 1)
      End If
      intPunto = InStr(1, strRestante, ".", 1)
      If intPunto <> 0 Then
        'Encontramos el punto
        Categoria = Left(strRestante, intPunto - 1)
        'Buscamos la clave correcta, indic�ndole que la busque en categor�as
        If Trim(Categoria) <> "" Then
          Categoria = BuscarClave(NodoAtrib, Categoria, FecRnf, "C")
          Set NodoAtrib = NodoAtrib.Categorias(CStr(Categoria))
          strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
      End If
    Wend
  End If
  Set BuscarNodo = NodoAtrib
End Function



Private Sub chkFacturar_Click()

  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
End Sub

Private Sub chkPermanente_Click()

  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
End Sub

Private Sub chkMostrar_Click()
  
  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
  
End Sub


Private Sub cmdAceptar_Click()
   Guardar
    
   Unload Me
End Sub

Private Sub Guardar()
    Dim Lineas As Integer
    Dim z As Integer
    Dim i As Integer
    Dim Y As Integer
    
    Propuesta = UBound(IndiceRNFs, 1)
    Lineas = UBound(IndiceRNFs, 2)
    
    For Y = 1 To Propuesta
      For z = 1 To Lineas
        If Not IsNull(IndiceRNFs(Y, z)) Then
          For i = 1 To IndiceRNFs(Y, z)
              If arrayRNFs(Y, z, i).Modificado = True Then
                  PropuestaFact.NecesitaRegeneracion = True
                  PropuestaFact.Modificada = True
                  Call ModificarNodo(Y, z, i)
                  Call ModificarCantidad(Y, z, i)
                  Call GrabarPermanente(Y, z, i)
              End If
          Next
        End If
      Next
    Next
End Sub

Private Sub cmdCancelar_Click()
    PropuestaFact.NecesitaRegeneracion = False

    Unload Me
End Sub

Private Sub cmdModificar_Click()
Dim respuesta As Integer

'  respuesta = MsgBox("�Est� seguro de que desea realizar los cambios?", vbQuestion + vbYesNo, "Modificaci�n de atributos")
'  If respuesta = vbYes Then
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Mostrar = chkMostrar.Value
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).FactOblig = chkFacturar.Value
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Descripcion = txtDescripcion.Text
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Cantidad = txtCantidad.Text
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Precio = txtPrecio.Text
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Dto = txtDto.Text
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Importe = txtImporte.Text
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Quitar = chkQuitar.Value
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Modificado = True
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).Permanente = chkPermanente.Value
    arrayRNFs(txtPropuesta, txtLinea, txtRNF).KeyREco = "" & txtKeyREco.Text
    Me.cmdModificar.Enabled = False
    Me.cmdRecargar.Enabled = False
'  End If
  
  If chkPermanente.Value = True Then
    ' hay que grabar esa modificaci�n permanente en la fa0300
    
  
  End If
  
End Sub

Private Sub cmdRecargar_Click()
Dim respuesta As Integer
  
  respuesta = MsgBox("�Desea recargar los datos y perder los cambios que se hayan producido?", vbQuestion + vbYesNo, "Modificaci�n de atributos")
  If respuesta = vbYes Then
    chkMostrar.Value = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Mostrar
    chkFacturar.Value = arrayRNFs(txtPropuesta, txtLinea, txtRNF).FactOblig
    txtDescripcion.Text = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Descripcion
    txtCantidad.Text = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Cantidad
    txtPrecio.Text = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Precio
    txtDto.Text = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Dto
    txtImporte.Text = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Importe
    chkQuitar.Value = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Quitar
    chkPermanente.Value = arrayRNFs(txtPropuesta, txtLinea, txtRNF).Permanente
    Me.cmdModificar.Enabled = False
    Me.cmdRecargar.Enabled = False
  End If
End Sub

Private Sub Form_Load()
    Dim objREco As Persona
    
    Cambiado = False
    optOrden(OrdenDesglose).Value = True
    
'    Call CargarTVW
    
   ' cargar el menu contextual con los REcos
   CargarMenuReco
   
   Call EvitarModif
End Sub


Private Sub Form_Resize()
    On Error Resume Next
    If Me.Width < 9750 Then Me.Width = 9750
    If Me.Height < 4000 Then Me.Height = 4000
    
    tvwDesglose.Width = Me.Width - 300
    tvwDesglose.Height = Me.Height - 2085
    
    Frame2.Top = Me.Height - 1900
    
    cmdAceptar.Left = tvwDesglose.Width - cmdAceptar.Width + 90
    cmdAceptar.Top = Me.Height - 1780
    
    cmdCancelar.Left = tvwDesglose.Width - cmdCancelar.Width + 90
    cmdCancelar.Top = Me.Height - 1015
    
    Frame1.Top = Me.Height - 1900
    Frame1.Left = tvwDesglose.Width - cmdAceptar.Width - Frame1.Width
End Sub

Private Sub CargarMenuReco()
   ' actualizar el menu de los REcos
   For Each objREco In Asistencia.REcos
      mnu_REco(objREco.Key).Caption = objREco.Name & "   ( " & objREco.CodTipEcon & " " & objREco.EntidadResponsable & " )"
      mnu_REco(objREco.Key).Visible = True
   Next
End Sub

Private Sub mnu_borrar_Click()
    Call BorrarLinea(tvwDesglose.SelectedItem.Key)
End Sub

Private Sub mnu_REco_Click(Index As Integer)
   Dim objconcierto As Concierto
   Dim blnEncontrado As Boolean
   
   If Asistencia.REcos(Index).Concierto.Concierto = 0 Then
      ' si el REco no tiene tipo economico ni concierto
      ' hay que pedirlo para asignarle un concierto
      Call frm_TipoEconREco.pTipoEconReco(Asistencia.REcos(Index))
   End If
   
   ' cargar el menu contextual con los REcos
   CargarMenuReco
   
   ' cargar su concierto si es necesario
   If Asistencia.REcos(Index).Concierto.Concierto <> 0 Then
      ' chequear si ya est� el concierto
      blnEncontrado = False
      For Each objconcierto In varFact.ConciertosAplicables
         If objconcierto.Codigo = Asistencia.REcos(Index).Concierto.Concierto Then
            blnEncontrado = True
            Exit For
         End If
      Next
      If blnEncontrado = False Then
         Set objconcierto = New Concierto
         objconcierto.Inicializar Asistencia.REcos(Index).Concierto.Concierto, _
                                 Asistencia.FecInicio, Asistencia.FecFin, Asistencia.strProcAsist

         ' A�adirlo a la coleccion
         varFact.ConciertosAplicables.Add objconcierto, CStr(objconcierto.Codigo)
      Else
      End If
      Call Asistencia.AddConcierto(Asistencia.REcos(Index).Concierto.Concierto, Asistencia.REcos(Index).Concierto.NodoAFacturar)
      
      
      ' chequear si el nodo aplicado ya est� en la propuesta, si no es as� a�adirlo
      blnEncontrado = False
      For Each obj In PropuestaFact.NodosAplicados
         If Not (TypeOf obj Is Nodo) Then
            If obj = CStr(Index) Then
               blnEncontrado = True
               Exit For
            End If
         End If
      Next
      If blnEncontrado = False Then
         PropuestaFact.NodosAplicados.Add CStr(Index)
         If Asistencia.REcos(Index).Concierto.NodoAFacturar = 0 Then
            objconcierto.Entidad.Nodos.item(1).NombreConcierto = Asistencia.REcos(Index).Concierto.Name
            PropuestaFact.NodosAplicados.Add objconcierto.Entidad.Nodos.item(1)
         Else
            objconcierto.Entidad.Nodos.item(Asistencia.REcos(Index).Concierto.NodoAFacturar & "-000").NombreConcierto = Asistencia.REcos(Index).Concierto.Name
            PropuestaFact.NodosAplicados.Add objconcierto.Entidad.Nodos.item(Asistencia.REcos(Index).Concierto.NodoAFacturar & "-000")
         End If
      End If
   End If
            
   If Asistencia.REcos(Index).Concierto.Concierto <> 0 Then
      ' si se le ha asignado un concierto o ya lo ten�a
      ' cambiar el reco de los RNFs
      txtKeyREco.Text = Index
      Call CambiarREco(tvwDesglose.SelectedItem.Key, Index)
   End If
End Sub

Private Sub optOrden_Click(Index As Integer)
   Dim Opcion As Integer
      
   If Not Cambiado Then
      If Index <> OrdenDesglose Then
         OrdenDesglose = Index
      End If
      Call CargarTVW
         
   Else
      If Index <> OrdenDesglose Then
      Opcion = MsgBox("Es necesario cerrar el desglose para reordenar" & Chr(13) & "� Desea grabar las modificaciones antes de salir ?", vbYesNoCancel + vbCritical, "Ordenaci�n del desglose")
      Select Case Opcion
         Case vbYes
            OrdenDesglose = Index
            cmdAceptar_Click
         Case vbNo
            OrdenDesglose = Index
            ' solo se reordena
            cmdCancelar_Click
            
         Case vbCancel
            optOrden(OrdenDesglose) = True
            
            ' no se hace nada
      End Select
      End If
   End If
End Sub

Private Sub tvwDesglose_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Integer
    On Error GoTo IsNothing
    
    
    If Button = vbRightButton And blnPermitirModifFact Then
        tvwDesglose.SelectedItem = tvwDesglose.HitTest(X, Y)
        Select Case TipoNodo(tvwDesglose.SelectedItem.Key)
            Case "E"    ' es un REco
            
            Case "L"    ' es una linea
                For i = 1 To 10
                    mnu_REco(i).Checked = False
                Next
                
                mnu_borrar.Visible = False
                PopupMenu mnu_Desglose
                
            Case "R"    ' es un RNF
                For i = 1 To 10
                    mnu_REco(i).Checked = False
                Next
                
                mnu_borrar.Visible = False
                
                PopupMenu mnu_Desglose
                
            Case Else
        End Select
    End If
    
    Exit Sub
  
IsNothing:

End Sub

Private Sub CambiarREco(NodoKey As String, nuevoKeyREco As Integer)
    Dim i As Integer
    Dim Propuesta As Integer
    Dim Linea As Integer
    Dim Registro As Integer
    Dim Cadena As String
    Dim intPos As Integer
    
    Cambiado = True
    
    
    Select Case TipoNodo(NodoKey)
        Case "E"    ' es un REco
        
        Case "L"    ' es una linea
            ' Calcular a que propuesta, linea y rnf corresponde
            Cadena = Right(NodoKey, Len(NodoKey) - 1)
            intPos = InStr(1, Cadena, ".")
            Propuesta = CInt(Left(Cadena, intPos - 1))
            Cadena = Right(Cadena, Len(Cadena) - intPos)
            intPos = InStr(1, Cadena, "-")
            Linea = CInt(Left(Cadena, intPos - 1))
            Registro = CInt(Right(Cadena, Len(Cadena) - intPos))
            
            For i = 1 To IndiceRNFs(Propuesta, Linea)
                arrayRNFs(Propuesta, Linea, i).KeyREco = nuevoKeyREco
                arrayRNFs(Propuesta, Linea, i).Modificado = True
            Next
            
        Case "R"    ' es un RNF
            ' Calcular a que propuesta, linea y rnf corresponde
            Cadena = Right(NodoKey, Len(NodoKey) - 1)
            intPos = InStr(1, Cadena, ".")
            Propuesta = CInt(Left(Cadena, intPos - 1))
            Cadena = Right(Cadena, Len(Cadena) - intPos)
            intPos = InStr(1, Cadena, "-")
            Linea = CInt(Left(Cadena, intPos - 1))
            Registro = CInt(Right(Cadena, Len(Cadena) - intPos))
            
            arrayRNFs(Propuesta, Linea, Registro).KeyREco = nuevoKeyREco
            arrayRNFs(Propuesta, Linea, Registro).Modificado = True
        Case Else
    End Select

End Sub

Private Sub BorrarLinea(NodoKey As String)
    Dim i As Integer
    Dim Propuesta As Integer
    Dim Linea As Integer
    Dim Registro As Integer
    Dim Cadena As String
    Dim intPos As Integer
    
    Cambiado = True
    
    
    Select Case TipoNodo(NodoKey)
        Case "E"    ' es un REco
        
        Case "L"    ' es una linea
            ' Calcular a que propuesta, linea y rnf corresponde
            Cadena = Right(NodoKey, Len(NodoKey) - 1)
            intPos = InStr(1, Cadena, ".")
            Propuesta = CInt(Left(Cadena, intPos - 1))
            Cadena = Right(Cadena, Len(Cadena) - intPos)
            intPos = InStr(1, Cadena, "-")
            Linea = CInt(Left(Cadena, intPos - 1))
            Registro = CInt(Right(Cadena, Len(Cadena) - intPos))
            
'            MsgBox "borrar " & PropuestaFact.NodosFactura(Linea), vbYesNo
'            For i = 1 To IndiceRNFs(Propuesta, Linea)
'                arrayRNFs(Propuesta, Linea, i).KeyREco = nuevoKeyREco
'                arrayRNFs(Propuesta, Linea, i).Modificado = True
'            Next
        
        Case Else
    End Select

End Sub

Private Sub tvwDesglose_NodeClick(ByVal Node As ComctlLib.Node)
    Dim Propuesta As Integer
    Dim Linea As Integer
    Dim Registro As Integer
    Dim Cadena As String
    Dim intPos As Integer

    Cadena = Node.Key
    If Left(Cadena, 1) = "R" Then
        Cadena = Right(Cadena, Len(Cadena) - 1)
        intPos = InStr(1, Cadena, ".")
        If intPos <> 0 Then
            Propuesta = CInt(Left(Cadena, intPos - 1))
            Cadena = Right(Cadena, Len(Cadena) - intPos)
        End If
        intPos = InStr(1, Cadena, "-")
        If intPos <> 0 Then
            Linea = CInt(Left(Cadena, intPos - 1))
            Registro = CInt(Right(Cadena, Len(Cadena) - intPos))
            Me.chkMostrar.Value = IIf(arrayRNFs(Propuesta, Linea, Registro).Mostrar = 0, 0, 1)
            Me.chkFacturar.Value = IIf(arrayRNFs(Propuesta, Linea, Registro).FactOblig = 0, 0, 1)
            Me.txtCantidad.Text = arrayRNFs(Propuesta, Linea, Registro).Cantidad
            Me.txtDescripcion = arrayRNFs(Propuesta, Linea, Registro).Descripcion
            Me.txtPrecio = arrayRNFs(Propuesta, Linea, Registro).Precio
            Me.txtDto = arrayRNFs(Propuesta, Linea, Registro).Dto
            Me.txtImporte = arrayRNFs(Propuesta, Linea, Registro).Importe
            Me.txtPropuesta = Propuesta
            Me.txtLinea = Linea
            Me.txtRNF = Registro
            chkQuitar.Value = IIf(arrayRNFs(Propuesta, Linea, Registro).Quitar = 0, 0, 1)
            chkPermanente.Value = IIf(arrayRNFs(Propuesta, Linea, Registro).Permanente = 0, 0, 1)
            txtKeyREco.Text = arrayRNFs(Propuesta, Linea, Registro).KeyREco
            If blnPermitirModifFact Then
               Frame2.Enabled = True
            Else
               Frame2.Enabled = False
            End If
        End If
    Else
          chkMostrar.Value = False
          chkFacturar.Value = False
          txtCantidad.Text = ""
          txtDescripcion.Text = ""
          txtPrecio.Text = ""
          txtDto.Text = ""
          txtImporte.Text = ""
          chkPermanente.Value = False
          txtKeyREco.Text = ""
          Frame2.Enabled = False
    End If
    Me.cmdModificar.Enabled = False
    Me.cmdRecargar.Enabled = False
End Sub

Private Sub txtCantidad_change()

  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
  
End Sub

Private Sub txtCantidad_GotFocus()
    txtCantidad.SelStart = 0
    txtCantidad.SelLength = Len(txtCantidad)
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtCantidad)
End Sub

Public Function pDesglose(objFactura As factura, strPaciente As String, strAsistencia As String, Prop As String, nodofactra As String, Responsable As String, aOrden() As Integer) As factura
    Dim i  As Integer
    Dim z As Integer
    
    Set varFact = objFactura
    PacDesg = strPaciente
    AsistDesg = strAsistencia
    PropFacDesg = Prop
    
    Set Paciente = varFact.Pacientes(PacDesg)
    Set Asistencia = Paciente.Asistencias(AsistDesg)
    Set PropuestaFact = Asistencia.PropuestasFactura(PropFacDesg)
    
    On Error GoTo SinLineas
    ReDim aOrdenacion(1 To 3, 1 To UBound(aOrden, 2))
    For i = 1 To 3
        For z = 1 To UBound(aOrden, 2)
            aOrdenacion(i, z) = aOrden(i, z)
        Next
    Next
    On Error GoTo 0
SinLineas:
    Load frmDesglose
    'Set frmSeleccion.varFact = objFactura
    frmDesglose.Show (vbModal)
            
    'Set objFactura = varFact
'    Unload frmDesglose
End Function


Private Sub CargarTVW()
   Dim strTexto As String
   Dim lngPrecio As Long
   Dim lngSuma As Long
   Dim n As Node
   Dim nFact As Node
   Dim nLineaFact As Node
   
   Dim NodoFACT As New Nodo
   Dim NodoLinea As New Nodo
   Dim i  As Integer
   Dim z As Integer
   Dim NodoFacturaCreado As Integer
   Dim intContRnfs As Integer
   Dim intLineaOblig As Integer
   Dim intFactOblig As Integer
   Dim SumaNodos As Integer
   Dim MaximoRNFS As Integer
   Dim MaximoLineas As Integer
   Dim SumaLineas As Integer
   Dim Importe As Double
   Dim Precio As Double
   
   On Error GoTo error
   
   Screen.MousePointer = vbHourglass
   Call LockWindowUpdate(Me.hWnd)
   
    tvwDesglose.Nodes.Clear
   '********************************
   ' Mostrar los REcos
   '********************************
   For Each reco In Asistencia.REcos
'       tvwDesglose.Nodes.Add , , "E" & reco.Key, reco.Name & "   ( " & reco.CodTipEcon & " " & reco.EntidadResponsable & " )  " & reco.FecInicio, "REco"
       tvwDesglose.Nodes.Add , , "E" & reco.Key, reco.Name & "   ( " & reco.CodTipEcon & " " & reco.EntidadResponsable & " )  ", "REco"
   Next
   
   
   '************************************************************************************************
   ' Calcularemos el numero de nodos y l�neas para crear el array de indices de RNFs
   '************************************************************************************************
   MaximoLineas = 0
   MaximoRNFS = 0
   For Each NodoFACT In PropuestaFact.NodosFactura
      If NodoFACT.Nodos.Count > MaximoLineas Then
         MaximoLineas = NodoFACT.Nodos.Count
      End If
   Next
   ReDim IndiceRNFs(1 To PropuestaFact.NodosFactura.Count, 1 To MaximoLineas)
    
   For Each NodoFACT In PropuestaFact.NodosFactura
      PropuestaFact.NodosFactura.Count
      SumaNodos = SumaNodos + 1
      SumaLineas = 0
      For Each NodoLinea In NodoFACT.Nodos
         SumaLineas = SumaLineas + 1
         If NodoLinea.RNFs.Count > MaximoRNFS Then
            MaximoRNFS = NodoLinea.RNFs.Count
         End If
         IndiceRNFs(SumaNodos, SumaLineas) = NodoLinea.RNFs.Count
      Next
   Next
   ReDim arrayRNFs(1 To SumaNodos, 1 To MaximoLineas, 1 To MaximoRNFS)
   
   
   '********************************
   ' Mostrar las lineas propuestas
   '********************************
   NodoFacturaCreado = 0
   ' Se crea el arbol del concierto ordenado
   For z = 1 To UBound(aOrdenacion, 2)
      ' si el nodo de factura (ej. Privado, Forfait..., etc.) no est� creado se crea
      If aOrdenacion(2, z) <> NodoFacturaCreado Then
          NodoFacturaCreado = aOrdenacion(2, z)
          Set NodoFACT = PropuestaFact.NodosFactura(NodoFacturaCreado)
          Set nFact = Me.tvwDesglose.Nodes.Add(tvwDesglose.Nodes("E" & NodoFACT.KeyREco), tvwChild, , NodoFACT.Name, "Reloj")
      End If
      ' se le cuelgan las lineas de factura ya ordenadas
      Set NodoLinea = NodoFACT.Nodos(aOrdenacion(3, z))
      Set nLineaFact = Me.tvwDesglose.Nodes.Add(nFact, tvwChild, "L" & aOrdenacion(2, z) & "." & aOrdenacion(3, z) & "-0", NodoLinea.Name & "       " & _
                                                                 IIf(NodoLinea.LimiteFranquicia >= 0, "(Franquicia :" & RedondearMoneda(NodoLinea.LimiteFranquicia, tiImporteLinea) & " Importe :" & RedondearMoneda(NodoLinea.ImporteAntesFranquicia, tiImporteLinea) & ") ", "") & _
                                                                 IIf(NodoLinea.LineaFact.Dto <> 0, "( " & RedondearMoneda((NodoLinea.LineaFact.Importe + NodoLinea.LineaFact.ImporteNoDescontable), tiImporteLinea) & " Pts" & _
                                                                  " - " & Format(NodoLinea.LineaFact.Dto, "##0.0#") & "%Dto ) =  ", "") & _
                                                                 RedondearMoneda((NodoLinea.LineaFact.Total + NodoLinea.LineaFact.TotalNoDescontable), tiImporteLinea) & " Pts", "Reloj")
      nLineaFact.Sorted = True
      nLineaFact.EnsureVisible
      
      
      '********************************
      ' Cargar sus RNFs
      '********************************
      intContRnfs = 0
      For Each rnf In NodoLinea.RNFs
         intContRnfs = intContRnfs + 1
         Select Case OrdenDesglose
         Case 0   ' Ordenar por fechas
            txtRNF = "" & _
                     "[ " & Format(rnf.Fecha, "yyyy/mm/dd") & " ]" & _
                     "     " & rnf.Name & IIf(rnf.Descripcion <> "", " " & rnf.Descripcion & "", "") & _
                     "     ...( " & rnf.Cantidad & " Ud x " & RedondearMoneda(rnf.Precio, tiPrecioUnit) & " Pts) "
         Case 1   ' Ordenar por fechas
            txtRNF = "" & _
                     rnf.Name & IIf(rnf.Descripcion <> "", " " & rnf.Descripcion & "", "") & _
                     "     [ " & Format(rnf.Fecha, "yyyy/mm/dd") & " ]" & _
                     "     ...( " & rnf.Cantidad & " Ud x " & RedondearMoneda(rnf.Precio, tiPrecioUnit) & " Pts) "
         End Select
         Set n = Me.tvwDesglose.Nodes.Add(nLineaFact, tvwChild, "R" & aOrdenacion(2, z) & "." & aOrdenacion(3, z) & "-" & intContRnfs, txtRNF, IIf(rnf.Desglose, "NotaPegada", "SinDesglose"))
         
         If rnf.Ruta <> "" Then
            Set Atributos = BuscarNodo(rnf.Ruta, rnf.Fecha)
         End If
         If rnf.NodoAnterior <> 0 Then
            If Nodos.LinOblig = True Then
               intLineaOblig = 1
            Else
               intLineaOblig = 0
            End If
            If Nodos.FactOblig = True Then
               intFactOblig = 1
            Else
               intFactOblig = 0
            End If
         Else
            If Atributos.LinOblig = True Then
               intLineaOblig = 1
            Else
               intLineaOblig = 0
            End If
            If Atributos.FactOblig = True Then
               intFactOblig = 1
            Else
               intFactOblig = 0
            End If
         End If
                   
         'Introducimos el rnf dentro del tipo que contiene los rnfs sin agrupar
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Clave = rnf.CodCateg
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Mostrar = intLineaOblig
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).FactOblig = intFactOblig
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Cantidad = rnf.Cantidad
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Descripcion = rnf.Name
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Precio = rnf.Precio
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Dto = Atributos.Descuento
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Importe = rnf.Cantidad * rnf.Precio
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Nodo = IntContNodo
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Categoria = intContCat
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Ruta = rnf.Ruta
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Fecha = rnf.Fecha
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).MostrarAnt = intLineaOblig
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).FactObligAnt = intFactOblig
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).NodoAnterior = rnf.NodoAnterior
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).SoloRNF = 0
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Key = rnf.Key
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Modificado = False
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).Permanente = rnf.Permanente
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).PermanenteAnt = rnf.Permanente
         arrayRNFs(aOrdenacion(2, z), aOrdenacion(3, z), intContRnfs).KeyREco = rnf.KeyREco
         intContCat = intContCat + 1
         intContador = intContador + 1
      Next rnf
           
   Next z
   
MostrarLineasA�adidas:
   '********************************
   ' Mostrar las lineas A�ADIDAS
   '********************************
   For Each NodoLinea In PropuestaFact.NuevasLineas
      Set nLineaFact = Me.tvwDesglose.Nodes.Add("E" & NodoLinea.KeyREco, tvwChild, "N" & NodoLinea.Key, NodoLinea.Name & "       " & _
                                                IIf(NodoLinea.LineaFact.Dto <> 0, "( " & RedondearMoneda((NodoLinea.LineaFact.Importe + NodoLinea.LineaFact.ImporteNoDescontable), tiImporteLinea) & " Pts" & _
                                                 " - " & Format(NodoLinea.LineaFact.Dto, "##0.0#") & "%Dto ) =  ", "") & _
                                                RedondearMoneda((NodoLinea.LineaFact.Total + NodoLinea.LineaFact.TotalNoDescontable), tiImporteLinea) & " Pts", "Reloj")
      nLineaFact.Sorted = True
      nLineaFact.EnsureVisible
   Next NodoLinea
      
   lngSuma = 0
   
   Call LockWindowUpdate(0&)
   Screen.MousePointer = vbDefault

   Exit Sub
   
error:
   Select Case Err.Number
      Case 9   ' Subscript out of range
'         Exit Sub
         Resume MostrarLineasA�adidas
      Case 457
         '
      Case Else
         MsgBox Err.Number & " " & Err.Description
'         Resume
   End Select
End Sub

Private Sub txtCantidad_LostFocus()

  If Trim(txtCantidad.Text) <> "" Then
    If Not IsNumeric(txtCantidad.Text) Then
      MsgBox "La cantidad es un dato de tipo num�rico", vbExclamation + vbOKOnly, "Aviso"
      If txtCantidad.Enabled = True Then
        txtCantidad.SetFocus
      End If
      Exit Sub
    End If
  Else
    MsgBox "El campo cantidad no se puede dejar en blanco", vbExclamation + vbOKOnly, "Aviso"
    If txtCantidad.Enabled = True Then
      txtCantidad.SetFocus
    End If
    Exit Sub
  End If
  Call RealizarCalculo

End Sub


Private Sub txtDescripcion_Change()
  
  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
  
End Sub

Private Sub txtDescripcion_GotFocus()
    txtDescripcion.SelStart = 0
    txtDescripcion.SelLength = Len(txtDescripcion)
End Sub

Private Sub txtDescripcion_LostFocus()

  If Trim(txtDescripcion) = "" Then
    MsgBox "La descripci�n no se puede dejar en blanco", vbOKOnly + vbExclamation, "Aviso"
    If txtDescripcion.Enabled = True Then
      txtDescripcion.SetFocus
    End If
  End If
  
End Sub


Private Sub txtDto_Change()
  
  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
  
End Sub


Private Sub txtDto_GotFocus()
    txtDto.SelStart = 0
    txtDto.SelLength = Len(txtDto)
End Sub

Private Sub txtDto_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtDto)
End Sub

Private Sub txtDto_LostFocus()
  If Trim(txtDto.Text) <> "" Then
    If Not IsNumeric(txtDto.Text) Then
      MsgBox "El campo descuento es un dato num�rico", vbExclamation + vbOKOnly, "Aviso"
      If txtDto.Enabled = True Then
        txtDto.SetFocus
      End If
    Else
      If txtDto.Text > 100 Then
        MsgBox "El descuento no puede ser superior al 100%", vbExclamation + vbOKOnly, "Aviso"
        If txtDto.Enabled = True Then
          txtDto.SetFocus
          Exit Sub
        End If
      End If
    End If
  Else
    MsgBox "El campo descuento no se puede dejar en blanco", vbExclamation + vbOKOnly, "Aviso"
    If txtDto.Enabled = True Then
      txtDto.SetFocus
    End If
    Exit Sub
  End If
  Call RealizarCalculo

End Sub


Private Sub txtImporte_Change()
  
  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
  
End Sub

Private Sub txtImporte_GotFocus()
    txtImporte.SelStart = 0
    txtImporte.SelLength = Len(txtImporte)
End Sub

Private Sub txtImporte_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtImporte)
End Sub


Private Sub txtImporte_LostFocus()
Dim ImpAnti As Double
Dim ImpDto As Double
Dim DtoNuevo As Double
Dim Cantidad As Double
Dim Precio As Double
Dim Dto As Double

  If Trim(txtImporte.Text) <> "" Then
    If Not IsNumeric(txtImporte.Text) Then
      MsgBox "El importe es un dato num�rico", vbExclamation + vbOKOnly, "Aviso"
      If txtImporte.Enabled = True Then
        txtImporte.SetFocus
      End If
    End If
    Cambiado = True
      
    'Comprobaremos que el importe no sea 0 ni negativo ya que no hay que hacer c�lculo en esos casos
    If CDbl(txtImporte.Text) < 0 Then
       MsgBox "El importe debe ser positivo", vbExclamation + vbOKOnly, "Aviso"
      If txtImporte.Enabled = True Then
        txtImporte.SetFocus
      End If
      Exit Sub
    ElseIf CDbl(txtImporte.Text) = 0 Then
      txtDto.Text = 100
      Exit Sub
    End If
  Else
    MsgBox "El campo importe no se puede dejar en blanco", vbExclamation + vbOKOnly, "Aviso"
    If txtImporte.Enabled = True Then
      txtImporte.SetFocus
    End If
    Exit Sub
  End If
  
  'Calcularemos el % a aplicar para conseguir el importe introducido
  If Trim(txtCantidad.Text) = "" Or Trim(txtPrecio.Text) = "" Then
    MsgBox "Para modificar el importe la cantidad o el precio no deber ser nulos", vbExclamation + vbOKOnly, "Aviso"
    If Trim(txtCantidad.Text) = "" Then
      txtCantidad = 0
      Cantidad = 0
      If txtCantidad.Enabled = True Then
        txtCantidad.SetFocus
      End If
    Else
      txtPrecio = 0
      Cantidad = 0
      If txtPrecio.Enabled = True Then
        txtPrecio.SetFocus
      End If
    End If
    Exit Sub
  End If
  
  Cantidad = CDbl(txtCantidad.Text)
  Precio = CDbl(txtPrecio.Text)
  If Trim(txtDto.Text) <> "" Then
    Dto = CDbl(txtDto.Text)
  Else
    Dto = 0
  End If
  
  ImpAnti = (Cantidad * Precio) - (((Cantidad * Precio) * Dto) / 100)
  ImpDto = ImpAnti - CDbl(txtImporte.Text)
  If ImpAnti <> 0 Then
   Dto = (ImpDto / ImpAnti) * 100
  Else
   Dto = 0
  End If
  If Dto <> 0 Then
    txtDto.Text = Format(Dto, "##0.##")
  Else
    txtDto.Text = 0
  End If
  Call RealizarCalculo
End Sub


Private Sub txtPrecio_change()
  
  If Me.cmdModificar.Enabled = False Then
    cmdModificar.Enabled = True
  End If
  If Me.cmdRecargar.Enabled = False Then
    Me.cmdRecargar.Enabled = True
  End If
  Cambiado = True
  
End Sub

Private Sub txtPrecio_GotFocus()
    txtPrecio.SelStart = 0
    txtPrecio.SelLength = Len(txtPrecio)
End Sub

Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtPrecio)
End Sub

Private Sub txtPrecio_LostFocus()
  
  If Trim(txtPrecio.Text) <> "" Then
    If Not IsNumeric(txtPrecio.Text) Then
      MsgBox "El precio debe ser un dato num�rico", vbInformation + vbOKOnly, "Aviso"
      If txtPrecio.Enabled = True Then
        txtPrecio.SetFocus
      End If
    End If
  Else
    MsgBox "El campo precio no se puede dejar en blanco", vbExclamation + vbOKOnly, "Aviso"
    If txtPrecio.Enabled = True Then
      txtPrecio.SetFocus
    End If
    Exit Sub
  End If
  Cambiado = True
  Call RealizarCalculo

End Sub

Sub RealizarCalculo()
Dim Importe As Double
Dim Cantidad As Double
Dim Precio As Double
Dim Dto As Double

  'Comprobamos que todos los campos tengan alg�n valor
  If Trim(Me.txtCantidad.Text) = "" Then
    Cantidad = 0
  Else
    Cantidad = CDbl(txtCantidad.Text)
  End If
  If Trim(txtPrecio.Text) = "" Then
    Precio = 0
  Else
    Precio = CDbl(txtPrecio.Text)
  End If
  If Trim(txtDto.Text) = "" Then
    Dto = 0
  Else
    Dto = CDbl(txtDto.Text)
  End If
  
  Importe = (Cantidad * Precio) - (((Cantidad * Precio) * Dto) / 100)
  txtImporte.Text = CLng(Importe)
End Sub
Sub ModificarCantidad(Propuesta As Integer, Linea As Integer, Categ As Integer)
    
'    Set Paciente = varFact.Pacientes(PacDesg)
'    Set Asistencia = Paciente.Asistencias(AsistDesg)
    Set rnf = Asistencia.RNFs(arrayRNFs(Propuesta, Linea, Categ).Key)
    
    rnf.Cantidad = arrayRNFs(Propuesta, Linea, Categ).Cantidad
    rnf.KeyREco = arrayRNFs(Propuesta, Linea, Categ).KeyREco
    If arrayRNFs(Propuesta, Linea, Categ).Permanente Then
        rnf.Precio = arrayRNFs(Propuesta, Linea, Categ).Precio
        rnf.Name = arrayRNFs(Propuesta, Linea, Categ).Descripcion
        rnf.Permanente = True
    Else
        rnf.Permanente = False
    End If
End Sub

Public Sub GrabarPermanente(Propuesta As Integer, Linea As Integer, Categ As Integer)
    
    If arrayRNFs(Propuesta, Linea, Categ).PermanenteAnt = True Or arrayRNFs(Propuesta, Linea, Categ).Permanente = True Then
        Dim qry As New rdoQuery

        Dim NumRNF As String
        Dim PrecioFact As Double
        Dim CantFact As Double
        Dim ObsImp As String
        Dim MiSqL As String
        
        NumRNF = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).RNFs(arrayRNFs(Propuesta, Linea, Categ).Key).CodRNF
        
        PrecioFact = arrayRNFs(Propuesta, Linea, Categ).Precio
        CantFact = arrayRNFs(Propuesta, Linea, Categ).Cantidad
        ObsImp = arrayRNFs(Propuesta, Linea, Categ).Descripcion
        
        MiSqL = "Update FA0300 set" & _
              " FA03PRECIOFACT = ?, FA03CANTFACT = ?, FA03OBSIMP = ?, FA03INDPERMANENTE = ? " & _
              " Where FA03NUMRNF = ?"
              
        qry.SQL = MiSqL
        Set qry.ActiveConnection = objApp.rdoConnect
        qry.Prepared = True
        
        qry.rdoParameters(0) = NumToSql(PrecioFact)
        qry.rdoParameters(1) = NumToSql(CantFact)
        qry.rdoParameters(2) = ObsImp
        qry.rdoParameters(3) = IIf(arrayRNFs(Propuesta, Linea, Categ).Permanente = True, 1, Null)
        qry.rdoParameters(4) = NumRNF
        
        qry.Execute

    End If
End Sub

Private Function TipoNodo(NodoKey) As String
    If Left(NodoKey, 1) = "E" Then ' REco
        TipoNodo = "E"
    ElseIf Left(NodoKey, 1) = "L" Then ' Linea
        TipoNodo = "L"
    ElseIf Left(NodoKey, 1) = "R" Then ' RNF
        TipoNodo = "R"
    ElseIf Left(NodoKey, 1) = "N" Then ' es un linea Nueva a�adida a mano
        TipoNodo = "N"
    Else
        TipoNodo = ""
    End If
End Function

Private Sub EvitarModif()
   If Not blnPermitirModifFact Then
      Frame2.Enabled = False
      
      cmdAceptar.Visible = False
      cmdModificar.Visible = False
      cmdRecargar.Visible = False
      
   End If
End Sub


