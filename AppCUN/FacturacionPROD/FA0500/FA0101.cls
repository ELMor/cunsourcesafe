VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************

' Las ventanas: numeraci�n a partir del DTM100

Const FAWinGestionCobros              As String = "FA0121"
Const FAWinEmisionAcuses              As String = "FA0131"
Const FAWinCierreCaja                 As String = "FA0134"
Const FAWinCartasRecordatorio         As String = "FA0135"
Const FAWinCierredeRemesas            As String = "FA0143"
Const FAWinSaldosPendientes           As String = "FA0146"
Const FAWinListadoCobrosCaja          As String = "FA0149"
Const FAWinAnotacionesRecordatorio    As String = "FA0166"

' Los listados: numeraci�n a partir del DT0101
Const FARepFA0201          As String = "FA0201"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

'  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
      
    Case FAWinSaldosPendientes
        frm_SaldosPdtes.Show vbModal
        Set frm_SaldosPdtes = Nothing

    Case FAWinGestionCobros
         If IsMissing(vntData) Then
           frm_Cobros.Show vbModal
         Else
            frm_Cobros.pGestion (vntData)
         End If
         Set frm_Cobros = Nothing

        
    Case FAWinEmisionAcuses
        frm_AcuseRecibos.Show vbModal
        Set frm_AcuseRecibos = Nothing
    
    Case FAWinCierreCaja
        frm_CierreCaja.Show vbModal
        Set frm_CierreCaja = Nothing
        
    Case FAWinCartasRecordatorio
        frm_Recordatorio.Show vbModal
        Set frm_Recordatorio = Nothing
        
    Case FAWinCierredeRemesas
      frm_Remesas.Show vbModal
      Set frm_Remesas = Nothing
    
    Case FAWinListadoCobrosCaja
      frm_ListadoCobros.Show vbModal
      Set frm_ListadoCobros = Nothing
      
    Case FAWinAnotacionesRecordatorio
        frm_AnotacionesCartas.Show vbModal
        Set frm_AnotacionesCartas = Nothing
    
    Case Else
        ' el c�digo de proceso no es v�lido y se devuelve falso
        LaunchProcess = False
  End Select
  
'  Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n� de procesos que se definan
  ReDim aProcess(1 To 1, 1 To 4) As Variant
 
  'aProcess(1, 1) = DTWinMantenimientos
  'aProcess(1, 2) = "Mantenimientos del Servicio de Dietas"
  'aProcess(1, 3) = True
  'aProcess(1, 4) = cwTypeWindow
  
  aProcess(1, 1) = FARepFA0201
  aProcess(1, 2) = "Listado de Conceptos Agrupantes"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeReport
  
  
End Sub

