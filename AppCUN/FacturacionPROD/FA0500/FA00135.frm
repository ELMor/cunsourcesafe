VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_Recordatorio 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Cartas de recordatorio"
   ClientHeight    =   4425
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7710
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   7710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame3 
      Caption         =   " Impresi�n de Cartas "
      Height          =   960
      Left            =   45
      TabIndex        =   31
      Top             =   3375
      Width           =   7575
      Begin VB.CommandButton cmdSeleccion 
         Caption         =   "Cartas de Resp. seleccionados"
         Height          =   465
         Left            =   5040
         TabIndex        =   34
         Top             =   360
         Width           =   2355
      End
      Begin VB.CommandButton cmdRevisar 
         Caption         =   "Cartas a revisar"
         Height          =   465
         Left            =   2610
         TabIndex        =   33
         Top             =   360
         Width           =   2355
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Imprimir Todas las cartas"
         Height          =   465
         Left            =   180
         TabIndex        =   32
         Top             =   360
         Width           =   2355
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Responsables econ�micos "
      Height          =   2220
      Left            =   45
      TabIndex        =   10
      Top             =   1125
      Width           =   7575
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   5
         Left            =   5490
         TabIndex        =   25
         Text            =   "0"
         Top             =   315
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   6
         Left            =   5490
         TabIndex        =   24
         Text            =   "0"
         Top             =   675
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   7
         Left            =   5490
         TabIndex        =   23
         Text            =   "0"
         Top             =   1035
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   8
         Left            =   5490
         TabIndex        =   22
         Text            =   "0"
         Top             =   1395
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   9
         Left            =   5490
         TabIndex        =   21
         Text            =   "0"
         Top             =   1755
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   0
         Left            =   1890
         TabIndex        =   15
         Text            =   "0"
         Top             =   315
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   1
         Left            =   1890
         TabIndex        =   14
         Text            =   "0"
         Top             =   675
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   2
         Left            =   1890
         TabIndex        =   13
         Text            =   "0"
         Top             =   1035
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   3
         Left            =   1890
         TabIndex        =   12
         Text            =   "0"
         Top             =   1395
         Width           =   1590
      End
      Begin VB.TextBox txtRespEcon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   4
         Left            =   1890
         TabIndex        =   11
         Text            =   "0"
         Top             =   1755
         Width           =   1590
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 6:"
         Height          =   255
         Index           =   6
         Left            =   3870
         TabIndex        =   30
         Top             =   360
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 7:"
         Height          =   255
         Index           =   7
         Left            =   3870
         TabIndex        =   29
         Top             =   720
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 8:"
         Height          =   255
         Index           =   8
         Left            =   3870
         TabIndex        =   28
         Top             =   1080
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 9:"
         Height          =   255
         Index           =   9
         Left            =   3870
         TabIndex        =   27
         Top             =   1440
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 10:"
         Height          =   255
         Index           =   10
         Left            =   3870
         TabIndex        =   26
         Top             =   1800
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 1:"
         Height          =   255
         Index           =   1
         Left            =   270
         TabIndex        =   20
         Top             =   360
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 2:"
         Height          =   255
         Index           =   2
         Left            =   270
         TabIndex        =   19
         Top             =   720
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 3:"
         Height          =   255
         Index           =   3
         Left            =   270
         TabIndex        =   18
         Top             =   1080
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 4:"
         Height          =   255
         Index           =   4
         Left            =   270
         TabIndex        =   17
         Top             =   1440
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "Resp. Econ�mico 5:"
         Height          =   255
         Index           =   5
         Left            =   270
         TabIndex        =   16
         Top             =   1800
         Width           =   1650
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Selecci�n de las cartas a enviar "
      Height          =   1050
      Left            =   45
      TabIndex        =   3
      Top             =   45
      Width           =   7575
      Begin VB.CommandButton cmdTodas 
         Caption         =   "Obtenci�n de las cartas"
         Height          =   330
         Left            =   5040
         TabIndex        =   35
         Top             =   630
         Width           =   2085
      End
      Begin VB.TextBox txtDias 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   4
         Text            =   "1"
         Top             =   630
         Width           =   420
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1800
         TabIndex        =   5
         Tag             =   "Fecha Inicio"
         Top             =   270
         Width           =   1635
         _Version        =   65537
         _ExtentX        =   2884
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCarta 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   5445
         TabIndex        =   6
         Tag             =   "Fecha Inicio"
         Top             =   270
         Width           =   1635
         _Version        =   65537
         _ExtentX        =   2884
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label lblFecInicio 
         Caption         =   "Fecha de Proceso:"
         Height          =   255
         Left            =   225
         TabIndex        =   9
         Top             =   315
         Width           =   1650
      End
      Begin VB.Label lblFecCarta 
         Caption         =   "Fecha de Carta:"
         Height          =   255
         Left            =   3870
         TabIndex        =   8
         Top             =   315
         Width           =   1650
      End
      Begin VB.Label Label1 
         Caption         =   "D�as de Proceso:"
         Height          =   255
         Index           =   0
         Left            =   225
         TabIndex        =   7
         Top             =   675
         Width           =   1650
      End
   End
   Begin VB.PictureBox Picture2 
      Height          =   180
      Left            =   315
      Picture         =   "FA00135.frx":0000
      ScaleHeight     =   120
      ScaleWidth      =   165
      TabIndex        =   2
      Top             =   0
      Visible         =   0   'False
      Width           =   225
   End
   Begin VB.PictureBox Picture1 
      Height          =   90
      Left            =   0
      Picture         =   "FA00135.frx":7AA2E
      ScaleHeight     =   30
      ScaleWidth      =   30
      TabIndex        =   1
      Top             =   45
      Visible         =   0   'False
      Width           =   90
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   60
      Left            =   180
      TabIndex        =   0
      Top             =   45
      Width           =   105
      _Version        =   196608
      _ExtentX        =   185
      _ExtentY        =   106
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
End
Attribute VB_Name = "frm_Recordatorio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ConvX = 54.0809
Const ConvY = 54.6101086

Private Type Pendiente
  Numero As String
  fecha As Date
  Cantidad As String
  Observ As String
End Type

  Dim ArrayPdtes() As Pendiente

Dim qry(1 To 10) As New rdoQuery
Dim Cantidad As Double
Dim Paciente As New Persona
    

Function ComprobarDatos() As Boolean
Dim Cont As Integer

  If Not IsNumeric(txtDias.Text) Then
    MsgBox "Los d�as de proceso deben ser un dato num�rico", vbInformation + vbOKOnly, "Aviso"
    If txtDias.Enabled = True Then
      txtDias.SetFocus
    End If
    ComprobarDatos = False
    Exit Function
  End If
  
  For Cont = 0 To 9
    If Not IsNumeric(txtRespEcon(Cont)) Then
      MsgBox "El responsable econ�mico " & Cont + 1 & "debe ser un dato num�rico", vbInformation + vbOKOnly, "aviso"
      If txtRespEcon(Cont).Enabled = True Then
        txtRespEcon(Cont).SetFocus
      End If
      ComprobarDatos = False
      Exit Function
    End If
  Next
  
  ComprobarDatos = True
  
End Function

Sub ImprimirCarta(CodPersona As Long, TipoPersona As Integer)
  
  Call Me.SeleccionarFacturasyPagos(CodPersona, Me.txtDias)
  Call Me.EncabezadoPagina(CodPersona)
  Call Me.ImprimirCuerpo(CodPersona, Me.SDCFechaInicio, Me.SDCFechaCarta, txtDias)
  Call Me.ImprimirFacturas(CodPersona)
  vsPrinter.NewPage
  
End Sub
Sub ImprimirCuerpo(CodPersona As Long, FechaInicio As Date, FechaCarta As Date, Dias As Integer)
Dim Texto As String
Dim MiSql As String
Dim MiRs As String
Dim rsBuscar As rdoResultset
Dim Total As Double
Dim Pagado As Double
Dim Abonos As Double
Dim Facturas As Double
Dim FechaLimite As Date
Dim Dia As Integer

  
  'Dia = 0 - Dias
  FechaLimite = DateAdd("d", Dia, FechaInicio)
  qry(4).rdoParameters(0) = CodPersona
  qry(4).rdoParameters(1) = Format(FechaLimite, "dd/mm/yyyy")
  Set rsBuscar = qry(4).OpenResultset
  If Not rsBuscar.EOF Then
    If Not IsNull(rsBuscar(0)) Then
      If rsBuscar(0) = "" Then
        Facturas = 0
      Else
        Facturas = CDbl(rsBuscar(0))
      End If
    Else
      Facturas = 0
    End If
  End If
  qry(5).rdoParameters(0) = CodPersona
  qry(5).rdoParameters(1) = Format(FechaLimite, "dd/mm/yyyy")
  Set rsBuscar = qry(5).OpenResultset
  If Not rsBuscar.EOF Then
    If IsNull(rsBuscar(0)) Then
      Abonos = 0
    Else
      Abonos = CDbl(rsBuscar(0))
    End If
  End If
  'Cantidad = Facturas - Abonos
  With vsPrinter
    .FontName = "Courier New"
    .FontSize = 11
    .TextAlign = taJustBaseline
    .MarginRight = 15 * ConvX
    .MarginLeft = 50 * ConvX
    If Paciente.Tratamiento = "Sr. D." Then
      .Paragraph = "Muy se�or nuestro:"
    ElseIf Trim(UCase(Paciente.Tratamiento)) = "SRA. D�A." Then
      .Paragraph = "Distinguida se�ora:"
    ElseIf Paciente.Tratamiento = "" Then
      .Paragraph = "Muy se�ores nuestros:"
    End If
    .CurrentY = .CurrentY + 100
    .IndentFirst = 15 * ConvX
    Texto = Space(4) & "Nos permitimos recordarle que, seg�n nuestras anotaciones, a fecha " & Format(FechaInicio, "dd/mm/yyyy") & ","
    Texto = Texto & " tenemos pendiente de cobro el importe de PESETAS, " & Format(Cantidad, "###,###,##0")
    Texto = Texto & " correspondiente a las facturas que detallamos a continuacion, el cual nos puede remitir"
    Texto = Texto & " por el medio que crea m�s conveniente."
    .Paragraph = Texto
    .CurrentY = .CurrentY + 100
    Texto = Space(4) & "Si ya lo ha enviado agradeceremos nos lo comunique, remiti�ndonos una copia"
    Texto = Texto & " del comprobante bancario."
    .Paragraph = Texto
    .CurrentY = .CurrentY + 100
    Texto = Space(4) & "Tan pronto como el mismo obre en nuestro poder, cursaremos el oportuno"
    Texto = Texto & " escrito confirmando la recepci�n del mismo."
    .Paragraph = Texto
    .CurrentY = .CurrentY + 100
    Texto = Space(4) & "Pendientes de sus noticias y ofreci�ndonos a su disposici�n, le saludamos atentamente."
    .Paragraph = Texto
    '.CurrentY = .CurrentY + 100
    .X1 = 117 * ConvX
    .Y1 = .CurrentY - 300
    .X2 = 160 * ConvX
    .Y2 = .CurrentY + 26 * ConvY
    .Picture = Me.Picture2.Picture
    .CurrentY = .CurrentY + 30 * ConvY
  End With

End Sub

Sub ImprimirFacturas(Persona As Long)
Dim Cont As Integer
Dim x As Integer
Dim Cadena As String
Dim Saldo As Double
Dim ContPag As Integer


  On Error GoTo Salir
  Cont = UBound(ArrayPdtes)
  On Error GoTo 0
  vsPrinter.CurrentX = 60 * ConvX
  vsPrinter.Paragraph = "N� FACTURA    FECHA      IMPORTE     OBSERVACIONES "
  vsPrinter.CurrentX = 60 * ConvX
  vsPrinter.Paragraph = "---------- ---------- ------------ ----------------"
  For x = 1 To Cont
    With Me.vsPrinter
      If Format(ArrayPdtes(x).Cantidad, "##,###,###,##0") <> 0 Then
        Cadena = Left(ArrayPdtes(x).Numero & Space(10), 10) & " "
        Cadena = Cadena & Left(ArrayPdtes(x).fecha & Space(10), 10) & " "
        'Cadena = Cadena & Right(Space(12) & Format(ArrayPdtes(x).Cantidad, "###,###,##0.##"), 12) & " "
        Cadena = Cadena & Right(Space(12) & Format(ArrayPdtes(x).Cantidad, "##,###,###,##0"), 12) & " "
        'Cadena = Cadena & Left(ArrayPdtes(x).Observ & Space(15), 15)
        .CurrentX = 60 * ConvX
        .Paragraph = Cadena
        Saldo = Saldo + ArrayPdtes(x).Cantidad
        If .CurrentY > 260 * ConvY Then
          ContPag = ContPag + 1
          .CurrentY = 270 * ConvY
          .CurrentX = 170 * ConvX
          .Paragraph = "P�gina " & ContPag
          .NewPage
          'Call Me.EncabezadoPagina(Persona)
          .CurrentY = 40 * ConvY
          vsPrinter.CurrentX = 60 * ConvX
          vsPrinter.Paragraph = "N� FACTURA    FECHA      IMPORTE     OBSERVACIONES "
          vsPrinter.CurrentX = 60 * ConvX
          vsPrinter.Paragraph = "---------- ---------- ------------ ----------------"
        End If
      End If
    End With
  Next
  Cadena = Space(22) & "------------"
  vsPrinter.CurrentX = 60 * ConvX
  vsPrinter.Paragraph = Cadena
  'Cadena = Space(22) & Right(Space(12) & Format(Saldo, "###,###,##0.##"), 12)
  Cadena = Space(22) & Right(Space(12) & Format(Saldo, "###,###,##0"), 12)
  vsPrinter.CurrentX = 60 * ConvX
  vsPrinter.Paragraph = Cadena
  If ContPag > 0 Then
    ContPag = ContPag + 1
    vsPrinter.CurrentY = 270 * ConvY
    vsPrinter.CurrentX = 170 * ConvX
    vsPrinter.Paragraph = "P�gina " & ContPag
  End If
Salir:
End Sub

Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer

  'Seleccionamos las facturas de una determinada persona y fecha que no est�n compensadas.
  
  MiSql = " Select /*+ INDEX(FA0400 FA0402) */ FA04CODFACT, FA04NUMFACT, FA04FECFACTURA, FA04CANTFACT " & _
              " From FA0400" & _
              " where CI21CODPERSONA = ?" & _
              " AND FA04INDCOMPENSA <> 2" '& _
              " AND FA04INDRECORDAR = 1"
  qry(1).SQL = MiSql
  
  'Seleccionamos las compensaciones que tiene cada una de las facturas
  MiSql = "Select SUM(FA18IMPCOMP) AS Suma From FA1800 Where FA04CODFACT = ?"
  qry(2).SQL = MiSql
  
  'Seleccionamos todos los pagos que tiene esa persona
  MiSql = "Select * From FA1700 Where CI21CODPERSONA = ?"
  qry(3).SQL = MiSql
  
  'Seleccionamos todas las compensaciones que tienen los pagos de esa persona
  MiSql = "Select SUM(FA18IMPCOMP) As Suma From FA1800 Where FA17CODPAGO = ?"
  qry(6).SQL = MiSql
  
  'Seleccionamos el total de las facturas que no est�n totalmente compensadas.
  MiSql = "Select /*+ INDEX(FA0400 FA0402) */ Sum(FA04CANTFACT) From FA0400 Where CI21CODPERSONA = ? " & _
              "and FA04FECFACTURA <= TO_DATE(?,'DD/MM/YYYY') And FA04INDCOMPENSA <> 2"
  qry(4).SQL = MiSql
  
  'Seleccionamos el impote compensado de las facturas que no est�n totalmente compensadas.
  MiSql = "Select NVL(Sum(FA18IMPCOMP),0) From FA1800 Where FA04CODFACT IN (" & _
              " SELECT /*+ INDEX(FA0400 FA0402) */ FA04CODFACT FROM FA0400 Where CI21CODPERSONA = ? " & _
              "and FA04FECFACTURA <= TO_DATE(?,'DD/MM/YYYY') And FA04INDCOMPENSA <> 2)"
  qry(5).SQL = MiSql
  
  'Seleccionamos todas las compensaciones que tienen los pagos de esa persona
  MiSql = "Select NVL(SUM(FA51IMPCOMP),0) As Suma From FA5100 Where FA17CODPAGO_POS = ?"
  qry(7).SQL = MiSql
  
  'Seleccionamos todas las compensaciones que tienen los pagos de esa persona
  MiSql = "Select NVL(SUM(FA51IMPCOMP),0) As Suma From FA5100 Where FA17CODPAGO_NEG = ?"
  qry(8).SQL = MiSql

  'Seleccionamos todas las compensaciones que tienen las facturas positivas de esa persona
  MiSql = "Select NVL(SUM(FA58IMPCOMP),0) As Suma From FA5800 Where FA04CODFACT_POS = ?"
  qry(9).SQL = MiSql
  
  'Seleccionamos todas las compensaciones que tienen las facturas negativas de esa persona
  MiSql = "Select NVL(SUM(FA58IMPCOMP),0) As Suma From FA5800 Where FA04CODFACT_NEG = ?"
  qry(10).SQL = MiSql

  
  For x = 1 To 10
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next

End Sub



'Mediante esta funci�n comprobaremos si se ha seleccionado alg�n responsable econ�mico.
'Si se ha introducido un responsable econ�mico, sacaremos las cartas de recordatorio de
'los responsables econ�micos seleccionados.
Function ResponsableEconomico() As String
Dim Cadena As String
Dim Cont As Integer

  Cadena = ""
  For Cont = 0 To 9
    'Comprobamos si se ha introducido un responsable econ�mico.
    If Me.txtRespEcon(Cont).Text <> 0 Then
      Cadena = Cadena & txtRespEcon(Cont).Text & ","
    End If
  Next
  
  If Right(Cadena, 1) = "," Then
    Cadena = Left(Cadena, Len(Cadena) - 1)
  End If
  
  ResponsableEconomico = Trim(Cadena)
  
End Function

Sub EncabezadoPagina(Persona As Long)
Dim MiSql As String
Dim rsBuscar As rdoResultset
Dim Nombre As String
Dim Direccion As String
Dim Poblacion As String
Dim Provincia As String
Dim fecha As String
    
    Paciente.Codigo = Persona
    fecha = Format(Me.SDCFechaCarta, "dd-mm-yyyy")
    If Paciente.Tratamiento <> "" Then
      Nombre = Paciente.Tratamiento & " " & Paciente.Name
    Else
      Nombre = Paciente.Name
    End If
    Direccion = Paciente.Direccion
    Poblacion = Trim(Paciente.CP & " " & Paciente.Poblacion)
    If Trim(UCase(Paciente.Pais)) <> "ESPA�A" Then
      Provincia = Trim(Paciente.Provincia & " " & UCase(Paciente.Pais))
    Else
      Provincia = Paciente.Provincia
    End If
    vsPrinter.Preview = False
    With vsPrinter
        .TextColor = QBColor(0)
        '-------------------------------------------------------
        ' REALIZAMOS LA IMPRESI�N DE LA CABECERA
        '-------------------------------------------------------
        'Imprimimos el escudo de la cl�nica.
        .X1 = 35 * ConvX
        .Y1 = 5 * ConvY
        .X2 = 59 * ConvX
        .Y2 = 35 * ConvY
        .Picture = Me.Picture1.Picture
                
        'Imprimimos el texto de debajo del escudo CLINICA...
        .FontName = "Arial"
        .CurrentX = 0 * ConvX
        .CurrentY = 35 * ConvY
        .MarginLeft = 0
        'Le asignamos un gran margen derecho y le decimos que lo centre
        .MarginRight = 120 * ConvX
        .FontSize = 14
        .TextAlign = taCenterBaseline
        .FontName = "Times New Roman"
        .Paragraph = "CLINICA UNIVERSITARIA"
        .SpaceAfter = 2 * ConvX
        .FontSize = 8
        .FontBold = False
        .CurrentY = 40 * ConvY
        .Paragraph = "FACULTAD DE MEDICINA"
        .CurrentY = 43 * ConvY
        .Paragraph = "UNIVERSIDAD DE NAVARRA"
        .DrawLine 39 * ConvX, 45 * ConvY, 49 * ConvX, 45 * ConvY
        .CurrentY = 49 * ConvY
        .FontName = "Arial"
        .FontBold = True
        .Paragraph = "ADMINISTRACION"
        .FontBold = False
        .TextAlign = taLeftBaseline
        .MarginRight = 0
        
        'Imprimimos la direcci�n y los tel�fonos (Parte derecha del encabezado)
        .CurrentX = 140 * ConvX
        .FontSize = 8
        .CurrentY = 16 * ConvY
        .Paragraph = "Avda. P�o XII, 36"
        .CurrentX = 140 * ConvX
        .CurrentY = 19 * ConvY
        .Paragraph = "Apartado, 4209"
        .CurrentX = 140 * ConvX
        .CurrentY = 25 * ConvY
        .Paragraph = "Tel�fonos:"
        .CurrentX = 140 * ConvX
        .CurrentY = 28 * ConvY
        .Paragraph = "Centralita 948.25.54.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 31 * ConvY
        .Paragraph = "Administraci�n 948.29.63.94"
        .CurrentX = 140 * ConvX
        .CurrentY = 37 * ConvY
        .Paragraph = "Fax: 948.29.65.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 40 * ConvY
        .Paragraph = "C.I.F.: Q.3168001 J"
        .CurrentX = 140 * ConvX
        .CurrentY = 43 * ConvY
        .Paragraph = "31080 PAMPLONA (ESPA�A)"
        
        .FontName = "Courier New"
        .FontSize = 11
        .CurrentX = 140 * ConvX
        .CurrentY = 51 * ConvY
        .Paragraph = fecha
        .CurrentX = 100 * ConvX
        .CurrentY = 63 * ConvY
        While .TextWidth(Nombre) > 90 * ConvX
          Nombre = Left(Nombre, Len(Nombre) - 1)
        Wend
        .Paragraph = Nombre
        .CurrentX = 100 * ConvX
        .CurrentY = 67 * ConvY
        .Paragraph = Direccion
        .CurrentX = 100 * ConvX
        .CurrentY = 71 * ConvY
        .Paragraph = Poblacion
        .CurrentX = 100 * ConvX
        .CurrentY = 75 * ConvY
        .Paragraph = Provincia
        
        .CurrentX = 50 * ConvX
        .CurrentY = 90 * ConvY

      End With
End Sub

Sub SeleccionarFacturasyPagos(CodPersona As Long, txtDias As Integer)
Dim RS1 As rdoResultset
Dim RS2 As rdoResultset
Dim RS3 As rdoResultset
Dim RS6 As rdoResultset
Dim RS7 As rdoResultset
Dim RS8 As rdoResultset
Dim RS9 As rdoResultset
Dim rs10 As rdoResultset
Dim ImpFact As Double
Dim ImpComp As Double
Dim ImpPago As Double
Dim ComPago As Double
Dim ImpPdte As Double
Dim Cont As Integer
Dim Cont1 As Integer
Dim Cont2 As Integer
Dim TmpNum As String
Dim TmpFecha As Date
Dim TmpObsv As String
Dim TmpCant As Double

  
  Cont = 0
  Cantidad = 0
  
  Erase ArrayPdtes()
  'Seleccionamos todas las facturas de la persona anteriores a una fecha y que no esten compensadas
  qry(1).rdoParameters(0) = CodPersona
  Set RS1 = qry(1).OpenResultset
  If Not RS1.EOF Then
    While Not RS1.EOF
      If Not IsNull(RS1("FA04CANTFACT")) Then
        ImpFact = CDbl(RS1("FA04CANTFACT"))
      Else
        ImpFact = 0
      End If
      'Seleccionamos todos los pagos que se corresponden con la factura seleccionada.
      qry(2).rdoParameters(0) = RS1("FA04CODFACT")
      Set RS2 = qry(2).OpenResultset
      If Not RS2.EOF Then
        If Not IsNull(RS2("SUMA")) Then
          ImpComp = CDbl(RS2("SUMA"))
        Else
          ImpComp = 0
        End If
      End If
      If ImpFact > 0 Then
        qry(9).rdoParameters(0) = RS1("FA04CODFACT")
        Set RS9 = qry(9).OpenResultset
        If Not RS9.EOF Then
          ImpComp = ImpComp + CDbl(RS9("SUMA"))
        End If
      Else
        qry(10).rdoParameters(0) = RS1("FA04CODFACT")
        Set rs10 = qry(10).OpenResultset
        If Not rs10.EOF Then
          ImpComp = ImpComp + CDbl(rs10("SUMA"))
        End If
      End If
      'Calcularemos el importe pendiente de pago de la factura.
      ImpPdte = ImpFact - ImpComp
      'Si el importe es mayor que 0 a�adiremos la factura al array que contiene las pendientes de cobro.
      If ImpPdte <> 0 Then
        Cont = Cont + 1
        ReDim Preserve ArrayPdtes(1 To Cont)
        ArrayPdtes(Cont).Numero = RS1("FA04NUMFACT")
        If Not IsNull(RS1("FA04FECFACTURA")) Then
          ArrayPdtes(Cont).fecha = CDate(RS1("FA04FECFACTURA"))
        End If
        ArrayPdtes(Cont).Cantidad = ImpPdte
        If ImpPdte < ImpFact Then
          ArrayPdtes(Cont).Observ = "DIFERENCIA PDTE"
        End If
        Cantidad = Cantidad + ImpPdte
      End If
      RS1.MoveNext
    Wend
  End If
  'Seleccionamos todos los pagos que nos ha realizado esa persona.
  qry(3).rdoParameters(0) = CodPersona
  Set RS3 = qry(3).OpenResultset
  If Not RS3.EOF Then
    While Not RS3.EOF
      If Not IsNull(RS3("FA17CANTIDAD")) Then
        ImpPago = CDbl(RS3("FA17CANTIDAD"))
      Else
        ImpPago = 0
      End If
      'Calculamos todas las compensaciones que ha realizado esa persona.
      qry(6).rdoParameters(0) = RS3("FA17CODPAGO")
      Set RS6 = qry(6).OpenResultset
      If Not RS6.EOF Then
        If Not IsNull(RS6("SUMA")) Then
          ComPago = CDbl(RS6("SUMA"))
        Else
          ComPago = 0
        End If
      End If
      
      If RS3("FA17CANTIDAD") > 0 Then
        'Comprobamos si ese pago es compensado por alg�n otro
        qry(7).rdoParameters(0) = RS3("FA17CODPAGO")
        Set RS7 = qry(7).OpenResultset
        If Not RS7.EOF Then
          If Not IsNull(RS7("SUMA")) Then
            ComPago = ComPago + CDbl(RS7("SUMA"))
          End If
        Else
          ComPago = 0
        End If
      ElseIf RS3("FA17CANTIDAD") < 0 Then
        'Comprobamosi si ese pago es compensado por alg�n otro
        qry(8).rdoParameters(0) = RS3("FA17CODPAGO")
        Set RS8 = qry(8).OpenResultset
        If Not RS8.EOF Then
          If Not IsNull(RS8("SUMA")) Then
            ComPago = ComPago - CDbl(RS8("SUMA"))
          End If
        Else
          ComPago = 0
        End If
      End If
      'Calculamos el importe pendiente de compensar
      ImpPdte = ImpPago - ComPago
      'Si queda algo pendiente de compensar lo a�adiremos al array que contiene los pendientes de compensaci�n
      If ImpPdte <> 0 Then
        Cont = Cont + 1
        ReDim Preserve ArrayPdtes(1 To Cont)
        ArrayPdtes(Cont).Numero = "A" & RS3("FA17CODPAGO")
        ArrayPdtes(Cont).fecha = CDate(RS3("FA17FECPAGO"))
        ArrayPdtes(Cont).Cantidad = 0 - ImpPdte
        ArrayPdtes(Cont).Observ = "SU ANTICIPO"
        Cantidad = Cantidad - ImpPdte
      End If
      RS3.MoveNext
    Wend
  End If
  'Ordenaremos las facturas y los abonos por fechas.
   For Cont1 = UBound(ArrayPdtes, 1) To 1 Step -1
    For Cont2 = Cont1 To UBound(ArrayPdtes, 1) - 1
      If ArrayPdtes(Cont2).fecha > ArrayPdtes(Cont2 + 1).fecha Then
        'Copiar a dos variables los valores del siguiente
        TmpFecha = ArrayPdtes(Cont2 + 1).fecha
        TmpNum = ArrayPdtes(Cont2 + 1).Numero
        TmpCant = ArrayPdtes(Cont2 + 1).Cantidad
        TmpObsv = ArrayPdtes(Cont2 + 1).Observ
        'Copiar los valores al siguiente
        ArrayPdtes(Cont2 + 1).fecha = ArrayPdtes(Cont2).fecha
        ArrayPdtes(Cont2 + 1).Numero = ArrayPdtes(Cont2).Numero
        ArrayPdtes(Cont2 + 1).Cantidad = ArrayPdtes(Cont2).Cantidad
        ArrayPdtes(Cont2 + 1).Observ = ArrayPdtes(Cont2).Observ
        'Copiar los valores al anterior
        ArrayPdtes(Cont2).fecha = TmpFecha
        ArrayPdtes(Cont2).Numero = TmpNum
        ArrayPdtes(Cont2).Cantidad = TmpCant
        ArrayPdtes(Cont2).Observ = TmpObsv
      ElseIf ArrayPdtes(Cont2).fecha < ArrayPdtes(Cont2 + 1).fecha Then
        Exit For
      End If
    Next
  Next
End Sub

Private Sub cmdImprimir_Click()
Dim MiSql As String
Dim MiUpdate As String
Dim MiRs As rdoResultset
Dim FechaProceso
Dim Dias As Integer
Dim Cont As Integer
Dim Imprimir As Boolean
Dim Anterior As Integer
Dim Impreso As Integer
Dim MiSelect1 As String
Dim MiUnion As String
Dim MiSelect2 As String
Dim MiOrder1 As String
Dim x As Integer


  Cont = 0
  Anterior = 0
    
    MiSelect1 = "Select FA6000.CI21CODPERSONA, CI33CODTIPPERS, FA21CODESTCARE, CI23RAZONSOCIAL AS NOMBRE " & _
                "From FA6000,CI2300 " & _
                "Where FA60INDENVIADA = 0 " & _
                "And FA6000.CI21CODPERSONA = CI2300.CI21CODPERSONA " & _
                "And CI33CODTIPPERS = 2 "
    
    MiUnion = "Union All "
    
    MiSelect2 = "Select FA6000.CI21CODPERSONA, CI33CODTIPPERS, FA21CODESTCARE, CI22PRIAPEL AS NOMBRE " & _
                "From FA6000,CI2200 " & _
                "Where FA60INDENVIADA = 0 " & _
                "And FA6000.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
                "And CI33CODTIPPERS = 1 "
        
    MiOrder1 = "Order by FA21CODESTCARE ASC, CI33CODTIPPERS DESC, NOMBRE ASC"
    
    MiSql = Trim(MiSelect1) & " " & _
            Trim(MiUnion) & " " & _
            Trim(MiSelect2) & " " & _
            Trim(MiOrder1)
  
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      vsPrinter.Preview = False
      vsPrinter.Action = 3
      While Not MiRs.EOF
        Cont = Cont + 1
        If Cont > 100 Then GoTo finalizar
        Me.Caption = Cont
        Imprimir = True
        If Not IsNull(MiRs("FA21CODESTCARE")) Then
          Impreso = CInt(MiRs("FA21CODESTCARE"))
          If MiRs("FA21CODESTCARE") = 5 Then
            Imprimir = False
          End If
        Else
          Impreso = 0
        End If
        If Impreso <> Anterior Then
          vsPrinter.NewPage
          Anterior = Impreso
          Cont = 0
        End If
        
        If Imprimir = True Then
          Call ImprimirCarta(MiRs("CI21CODPERSONA"), MiRs("CI33CODTIPPERS"))
        End If
        MiUpdate = "Update FA6000 Set FA60INDENVIADA = -1 Where CI21CODPERSONA = " & MiRs("CI21CODPERSONA")
        objApp.rdoConnect.Execute MiUpdate
        MiRs.MoveNext
      Wend
finalizar:
      vsPrinter.Action = 6
    End If
End Sub

Private Sub cmdRevisar_Click()
Dim MiSelect1 As String
Dim MiUpdate As String
Dim MiSql As String
Dim MiWhere As String
Dim MiRs As rdoResultset
Dim Cont As Integer

  Cont = 0
  
  MiSelect1 = "Select FA6000.CI21CODPERSONA, CI33CODTIPPERS, FA21CODESTCARE " & _
              "From FA6000 " & _
              "Where FA60INDENVIADA = 0 AND FA21CODESTCARE Is Null"

  If Me.ResponsableEconomico <> "" Then
    MiWhere = "AND CI2100.CI21CODPERSONA IN (" & Me.ResponsableEconomico & ")"
  Else
    MiWhere = ""
  End If
  If MiWhere <> "" Then
    MiSql = Trim(MiSelect1) & " " & Trim(MiWhere)
  Else
    MiSql = Trim(MiSelect1)
  End If
  Cont = 0
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    vsPrinter.Preview = False
    vsPrinter.Action = 3
    While Not MiRs.EOF
      Cont = Cont + 1
      If Cont > 100 Then GoTo finalizar
      Me.Caption = Cont
      Call ImprimirCarta(MiRs("CI21CODPERSONA"), MiRs("CI33CODTIPPERS"))
      MiUpdate = "Update FA6000 Set FA60INDENVIADA = -1 Where CI21CODPERSONA = " & MiRs("CI21CODPERSONA")
      objApp.rdoConnect.Execute MiUpdate
      MiRs.MoveNext
    Wend
finalizar:
    vsPrinter.Action = 6
  End If
End Sub

Private Sub cmdSeleccion_Click()
Dim MiSelect1 As String
Dim MiSelect2 As String
Dim MiUnion As String
Dim MiOrder As String
Dim MiOrder2 As String
Dim MiSql As String
Dim MiInsert As String
Dim MiWhere As String
Dim Cadena As String
Dim IntPosComa As Integer
Dim MiRs As rdoResultset
Dim FechaProceso
Dim Dias As Integer
Dim Cont As Integer

  
  Dias = 0 - CInt(txtDias)
  FechaProceso = DateAdd("D", Dias, Me.SDCFechaInicio.Date)
  
  'Seleccionaremos aquellas personas, cuyo saldo sea mayor que 2 (por los decimales que arrastra), y
  'que tengan alguna factura pendiente y no marcada en una fecha anterior a la fecha de proceso.
  MiSelect1 = "Select Distinct CI21CODPERSONA, CI33CODTIPPERS, FA21CODESTCARE " & _
              "From FA6000 " '& _
              "Where FA60INDENVIADA  = 0 "
  MiOrder = "Order by FA21CODESTCARE, CI21CODPERSONA"
     
  If Me.ResponsableEconomico <> "" Then
    MiWhere = "Where CI21CODPERSONA IN (" & Me.ResponsableEconomico & ")"
  Else
    MiWhere = ""
  End If
  If MiWhere <> "" Then
    MiSql = Trim(MiSelect1) & " " & Trim(MiWhere) & " " & Trim(MiOrder)
  End If
  Cont = 0
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    vsPrinter.Preview = False
    vsPrinter.Action = 3
    While Not MiRs.EOF
      Cont = Cont + 1
      Call ImprimirCarta(MiRs("CI21CODPERSONA"), MiRs("CI33CODTIPPERS"))
      MiRs.MoveNext
    Wend
    vsPrinter.Action = 6
  Else
    MsgBox "Las personas introducidas no est�n dentro de las cartas a emitir", vbOKOnly + vbInformation, "Cartas de Recordatorio"
  End If
Salir:
  

End Sub

Private Sub cmdTodas_Click()
Dim MiSelect1 As String
Dim MiSelect2 As String
Dim MiUnion As String
Dim MiOrder1 As String
Dim MiOrder2 As String
Dim MiSql As String
Dim MiInsert As String
Dim MiWhere As String
Dim Cadena As String
Dim IntPosComa As Integer
Dim MiRs As rdoResultset
Dim FechaProceso
Dim Dias As Integer
Dim Cont As Integer
Dim Respuesta As Integer

  Dias = 0 - CInt(txtDias)
  FechaProceso = DateAdd("D", Dias, Me.SDCFechaInicio.Date)
  
  Respuesta = MsgBox("Para insertar las nuevas cartas de recordatorio hay que borrar " & _
                     "las antiguas, �desea borrarlas?", vbQuestion + vbYesNo, "Inserci�n " & _
                     "de nuevas cartas")
  If Respuesta = vbNo Then
    Exit Sub
  End If
  Screen.MousePointer = vbHourglass
  MiSql = "Delete From FA6000"
  objApp.rdoConnect.Execute MiSql
  'Seleccionaremos aquellas personas, cuyo saldo sea mayor que 2 (por los decimales que arrastra), y
  'que tengan alguna factura pendiente y no marcada en una fecha anterior a la fecha de proceso.
  MiSelect1 = "Select Distinct CI2100.CI21CODPERSONA, CI2100.CI33CODTIPPERS,0,CI2200.FA21CODDESTCARE " & _
              "From CI2100, FA0400, CI2200 " & _
              "Where CI21SALDO >= 1000 AND FA04INDCOMPENSA <> 2 " & _
              "AND FA04INDRECORDAR = 1 AND FA04NUMFACREAL IS NULL " & _
              "AND (CI2100.CI21CODPERSONA = FA0400.CI21CODPERSONA) " & _
              "AND (CI2200.CI21CODPERSONA = CI2100.CI21CODPERSONA) " & _
              "AND FA04FECFACTURA <= TO_DATE('" & FechaProceso & "','DD/MM/YYYY')"

  
  MiUnion = "Union All "
  
  MiSelect2 = "Select Distinct CI2100.CI21CODPERSONA, CI2100.CI33CODTIPPERS,0,CI2300.FA21CODDESTCARE " & _
              "From CI2100, FA0400, CI2300 " & _
              "Where CI21SALDO >= 1000 AND FA04INDCOMPENSA <> 2  " & _
              "AND FA04INDRECORDAR = 1 AND FA04NUMFACREAL IS NULL " & _
              "AND (CI2100.CI21CODPERSONA = FA0400.CI21CODPERSONA) " & _
              "AND (CI2300.CI21CODPERSONA = CI2100.CI21CODPERSONA) " & _
              "AND FA04FECFACTURA <= TO_DATE('" & FechaProceso & "','DD/MM/YYYY')"
  MiOrder2 = "Order by CI2100.CI33CODTIPPERS, CI2200.FA21CODDESTCARE DESC"
  
  MiOrder1 = ""
  MiSql = Trim(MiSelect1) & " " & _
          Trim(MiUnion) & " " & _
          Trim(MiSelect2) & " " & _
          Trim(MiOrder1)
          
  MiInsert = "Insert Into FA6000 (CI21CODPERSONA,CI33CODTIPPERS,FA60INDENVIADA,FA21CODESTCARE) "
  MiSql = MiInsert & " " & MiSql
  
  Cont = 0
  objApp.rdoConnect.Execute MiSql
  Screen.MousePointer = vbDefault
Salir:
  
End Sub


Private Sub Form_Load()
Dim MiRsFecha
  
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    Me.SDCFechaInicio = Format(MiRsFecha(0), "dd/mm/yyyy")
    Me.SDCFechaCarta = Format(MiRsFecha(0), "dd/mm/YYYY")
  End If

  Call Me.IniciarQRY
End Sub


Private Sub txtDias_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtDias_LostFocus()
  
  If Not IsNumeric(txtDias.Text) Then
    MsgBox "Los d�as de proceso deben ser un dato num�rico", vbInformation + vbOKOnly, "Aviso"
    If txtDias.Enabled = True Then
      txtDias.SetFocus
    End If
  End If
  
End Sub


Private Sub txtRespEcon_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtRespEcon_LostFocus(Index As Integer)
  If Not IsNumeric(txtRespEcon(Index)) Then
    MsgBox "El responsable econ�mico " & Index + 1 & "debe ser un dato num�rico", vbInformation + vbOKOnly, "aviso"
    If txtRespEcon(Index).Enabled = True Then
      txtRespEcon(Index).SetFocus
    End If
  End If
End Sub


