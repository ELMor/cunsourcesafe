VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_CompensaAbonos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Compensaci�n de un abono con uno negativo."
   ClientHeight    =   5850
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtNotaAbono 
      Height          =   285
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   1170
      Width           =   420
   End
   Begin VB.TextBox txtCodFormaPago 
      Height          =   285
      Left            =   2250
      TabIndex        =   17
      Top             =   1170
      Width           =   420
   End
   Begin VB.TextBox txtResponsable 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   13
      Tag             =   "txtResponsable"
      Top             =   90
      Width           =   5235
   End
   Begin VB.TextBox txtCantidad 
      Height          =   285
      Left            =   6390
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   450
      Width           =   1275
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   1935
      TabIndex        =   11
      Top             =   5310
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   5220
      TabIndex        =   10
      Top             =   5310
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   " Facturas Pendientes de Compensar "
      Height          =   2490
      Left            =   135
      TabIndex        =   8
      Top             =   2700
      Width           =   8340
      Begin SSDataWidgets_B.SSDBGrid grdAbonos 
         Height          =   2175
         Left            =   135
         TabIndex        =   9
         Top             =   225
         Width           =   8115
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   6
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   1508
         Columns(0).Caption=   "Abono"
         Columns(0).Name =   "NoPago"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   6218
         Columns(1).Caption=   "Observaciones del abono negativo"
         Columns(1).Name =   "Observ"
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1852
         Columns(2).Caption=   "Cantidad"
         Columns(2).Name =   "Cantidad"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   2
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1852
         Columns(3).Caption=   "Pendiente"
         Columns(3).Name =   "Pendiente"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1852
         Columns(4).Caption=   "Can. Comp."
         Columns(4).Name =   "Parcial"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   556
         Columns(5).Name =   "Entera2"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         _ExtentX        =   14314
         _ExtentY        =   3836
         _StockProps     =   79
      End
   End
   Begin VB.TextBox txtRemesa 
      Height          =   285
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   1890
      Width           =   1275
   End
   Begin VB.TextBox txtObservaciones 
      Height          =   285
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1530
      Width           =   6225
   End
   Begin VB.TextBox txtApunte 
      Height          =   285
      Left            =   5445
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   1890
      Width           =   1275
   End
   Begin VB.TextBox txtInicPago 
      Height          =   285
      Left            =   2250
      TabIndex        =   4
      Top             =   1170
      Width           =   420
   End
   Begin VB.ComboBox cboFormaPago 
      Height          =   315
      Left            =   2745
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   1170
      Width           =   5730
   End
   Begin VB.CheckBox chkIndAcuse 
      Alignment       =   1  'Right Justify
      Caption         =   "�Sin Acuse?"
      Enabled         =   0   'False
      Height          =   195
      Left            =   6975
      TabIndex        =   2
      Top             =   1980
      Width           =   1275
   End
   Begin VB.TextBox txtCodPago 
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   -45
      Visible         =   0   'False
      Width           =   285
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   420
      Left            =   180
      TabIndex        =   1
      Top             =   5265
      Visible         =   0   'False
      Width           =   465
      _Version        =   196608
      _ExtentX        =   820
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFecha 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   14
      Tag             =   "Fecha Inicio"
      Top             =   450
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaEfecto 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   15
      Tag             =   "Fecha Inicio"
      Top             =   795
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCierre 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   6390
      TabIndex        =   16
      Tag             =   "Fecha Inicio"
      Top             =   810
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin MSComDlg.CommonDialog cmdDlg1 
      Left            =   0
      Top             =   -45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Responsable Econ�mico:"
      Height          =   240
      Index           =   0
      Left            =   360
      TabIndex        =   29
      Top             =   135
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Entregada:"
      Height          =   240
      Index           =   1
      Left            =   4500
      TabIndex        =   28
      Top             =   495
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Pago:"
      Height          =   240
      Index           =   2
      Left            =   360
      TabIndex        =   27
      Top             =   495
      Width           =   1815
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Pendiente de Asignar:"
      Height          =   240
      Left            =   360
      TabIndex        =   26
      Top             =   2295
      Width           =   2355
   End
   Begin VB.Label lblPendiente 
      Caption         =   "Pendiente"
      Height          =   240
      Left            =   2790
      TabIndex        =   25
      Top             =   2295
      Width           =   1230
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Efecto:"
      Height          =   240
      Index           =   3
      Left            =   360
      TabIndex        =   24
      Top             =   855
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Cierre:"
      Height          =   240
      Index           =   4
      Left            =   4500
      TabIndex        =   23
      Top             =   855
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Remesa:"
      Height          =   240
      Index           =   5
      Left            =   360
      TabIndex        =   22
      Top             =   1935
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Observaciones"
      Height          =   240
      Index           =   6
      Left            =   360
      TabIndex        =   21
      Top             =   1575
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Apunte:"
      Height          =   240
      Index           =   7
      Left            =   3735
      TabIndex        =   20
      Top             =   1935
      Width           =   1635
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Forma de Pago:"
      Height          =   240
      Index           =   8
      Left            =   360
      TabIndex        =   19
      Top             =   1215
      Width           =   1815
   End
End
Attribute VB_Name = "frm_CompensaAbonos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Nombre As String
Dim FecPago As String
Dim CodPersona As Long
Dim Conciertos As String
Dim PagoNuevo As Boolean
Dim ImportePago As Double
Dim CodPago As Long
Dim Pagado As Double
Dim Apuntes As Integer
Sub CargarAbonos(Persona As Long)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim MisqlSuma As String
Dim MiRsSuma As rdoResultset
Dim MiInsertar As String
Dim Importe As Double
Dim Obs As String
Dim Pdte As Double
  'Vaciamos el grid de los abonos.
  Me.grdAbonos.RemoveAll
  'Obtenemos las compensaciones negativas de esa persona
  MiSqL = "Select FA17CODPAGO, FA17CANTIDAD, FA17FECPAGO, FA17OBSERV From FA1700 " & _
          "Where FA17CANTIDAD < 0 And CI21CODPERSONA = " & Persona
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      MisqlSuma = "Select NVL(SUM(FA51IMPCOMP),0) AS IMPORTE FROM FA5100 " & _
                  "Where FA17CODPAGO_NEG = " & MiRs("FA17CODPAGO")
      Set MiRsSuma = objApp.rdoConnect.OpenResultset(MisqlSuma, 3)
      If Not MiRsSuma.EOF Then
        Importe = MiRsSuma(0)
      End If
      If MiRs("FA17CANTIDAD") + Importe <> 0 Then
        Pdte = MiRs("FA17CANTIDAD") + Importe
        
        If Not IsNull(MiRs("FA17OBSERV")) Then
          Obs = MiRs("FA17OBSERV")
        Else
          Obs = ""
        End If
        MiInsertar = MiRs("FA17CODPAGO") & Chr(9) & Obs & Chr(9) & _
                   Format(MiRs("FA17CANTIDAD"), "###,###,##0.##") & Chr(9) & Format(Pdte, "###,###,##0.##") & _
                   Chr(9) & 0 & Chr(9) & False
        grdAbonos.AddItem MiInsertar
      End If
      MiRs.MoveNext
    Wend
  End If
  
End Sub
 Sub ImprimirAbono(NoPago As Long, Responsable As String)
Dim Texto As String
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim Acumulado As Double
Dim Compensacion As String

  If Pagado = 0 Then
    Compensacion = "N NO COMPENSADO"
  ElseIf CDbl(txtCantidad) = Pagado Then
    Compensacion = "C COMPENSADO"
  ElseIf CDbl(txtCantidad) > Pagado Then
    Compensacion = "P PARCIALM. COMPENSADO"
  End If
  With vsPrinter1
    .Preview = False
    .Action = 3
    .FontSize = 11
    .FontName = "Courier New"
    .CurrentY = 660
    .FontBold = True
    '.FontUnderline = True
    .TextAlign = taCenterBaseline
    Texto = "A   B   O   N   O"
    .Paragraph = Texto
    Texto = "================="
    .Paragraph = Texto
    .Paragraph = "                                               " & objSecurity.strUser & " " & Me.SDCFecha.Date
    '.FontBold = False
    '.FontUnderline = False
    .TextAlign = taLeftBaseline
    Texto = "NUMERO DE ABONO.......: " & Space(7 - Len(str(NoPago))) & NoPago
    .Paragraph = Texto
    Texto = "RESPONSABLE ECONOMICO.: " & CodPersona & " " & Me.txtResponsable.Text
    .Paragraph = Texto
    Texto = "FORMA DE PAGO.........: " & Me.txtInicPago.Text & " " & Me.cboFormaPago.Text
    .Paragraph = Texto
    Texto = "CONCEPTO..............: " & Me.txtObservaciones
    .Paragraph = Texto
    Texto = "FECHA INTRODUCC. ABONO: " & Me.SDCFecha.Date
    .Paragraph = Texto
    Texto = "FECHA EFECTO CAJA.....: " & Me.SDCFechaEfecto.Date
    .Paragraph = Texto
    Texto = "IMPORTE...............: " & Space(12 - Len(Format(Me.txtCantidad.Text, "###,###,###"))) & Format(Me.txtCantidad.Text, "###,###,###")
    .Paragraph = Texto
    Texto = "COMPONSACION..........: " & Compensacion
    .Paragraph = Texto
    Texto = "N.APUNTES COMPENSADOS.: " & Space(12 - Len(Trim(str(Apuntes)))) & Apuntes
    .Paragraph = Texto
    .Paragraph = ""
    Texto = "TIPO FECHA          NUMERO NOMBRE                                  IMPORTE"
    .Paragraph = Texto
    Texto = "--------------------------------------------------------------------------"
    .Paragraph = Texto
    'Seleccionamos las facturas compensadas
    MiSqL = "Select * From FA1800 Where FA17CODPAGO = " & CodPago
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Acumulado = 0
      While Not MiRs.EOF
        sqlFact = "Select FA04FECFACTURA, FA04NUMFACT,CI22NUMHISTORIA From FA0400,AD0100 Where FA04CODFACT = " & MiRs("FA04CODFACT") & _
                  " And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI"
        Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact, 3)
        If Not rsFact.EOF Then
          Texto = "F   "
          Texto = Texto & " " & Format(rsFact("FA04FECFACTURA"), "DD/MM/YYYY")
          Texto = Texto & Right(Space(11) & rsFact("FA04NUMFACT"), 11) & " "
          Texto = Texto & Left(Trim(rsFact("CI22NUMHISTORIA")) & " " & Trim(Responsable) & Space(37), 37)
          Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        Else
          Texto = "F   "
          Texto = Texto & " " & Format(MiRs("FA18FECCOMP"), "DD/MM/YYYY")
          Texto = Texto & Space(8) & " "
          Texto = Texto & Left(Trim(Responsable) & Space(37), 37)
          Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        End If
        .Paragraph = Texto
        Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
        MiRs.MoveNext
      Wend
      Texto = "                                                              ------------"
      .Paragraph = Texto
      Texto = "                                          TOTAL COMPENSADO...."
      Texto = Texto & Space(12 - Len(Format(Acumulado, "###,###,##0"))) & Format(Acumulado, "###,###,##0")
      .Paragraph = Texto
      Texto = "                                                              ============"
      .Paragraph = Texto
    End If
    .Action = 6
  End With
End Sub

Sub CompensaAbono(idpersona As Long, Persona As String, Fecha As String, Anadir As Boolean, Importe As Double, Pago As Long)
  CodPersona = idpersona
  Nombre = UCase(Persona)
  FecPago = Fecha
  PagoNuevo = Anadir
  ImportePago = Importe
  CodPago = Pago
  frm_CompensaAbonos.Show vbModal
  Set frm_CompensaAbonos = Nothing
End Sub



Private Sub cmdAceptar_Click()
Dim NoPago As String
Dim MiSqL As String
Dim X As Integer
Dim Resultado As Integer

  On Error GoTo ErrorenGrabacion
  objApp.rdoConnect.BeginTrans
    Pagado = 0
    Apuntes = 0
    grdAbonos.MoveFirst
    For X = 1 To grdAbonos.Rows
      If grdAbonos.Columns(5).Value = True Or grdAbonos.Columns(4).Value <> 0 Then
        MiSqL = "INSERT INTO FA5100 (FA17CODPAGO_POS,FA17CODPAGO_NEG,FA51FECCOMP,FA51IMPCOMP) " & _
                      "VALUES (" & CodPago & "," & grdAbonos.Columns(0).Text & _
                       ", TO_DATE('" & Me.SDCFecha.Date & "','DD/MM/YYYY')," & Abs(CDbl(Me.grdAbonos.Columns(4).Text)) & ")"
        objApp.rdoConnect.Execute MiSqL
      End If
      grdAbonos.MoveNext
    Next
  objApp.rdoConnect.CommitTrans
  Unload Me
Exit Sub

ErrorenGrabacion:
  MsgBox "Se ha producido un error en la grabaci�n de los pagos", vbCritical + vbOKOnly, "Atenci�n"
  objApp.rdoConnect.RollbackTrans
End Sub




Private Sub cmdCancelar_Click()
  Unload Me
End Sub


Function CalcularPendiente(Pago As Long) As Double
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Acumulado As Double

  MiSqL = "Select FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & Pago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Acumulado = 0
    While Not MiRs.EOF
      Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
      MiRs.MoveNext
    Wend
  Else
    Acumulado = 0
  End If
  'Comprobamos sus compensaciones por saldos negativos
  MiSqL = "Select NVL(Sum(FA51IMPCOMP), 0) as Suma From FA5100 Where FA17CODPAGO_POS = " & Pago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    Acumulado = Acumulado + MiRs("Suma")
  End If
  CalcularPendiente = Acumulado
End Function

Private Sub Form_Load()
Dim MiSqL As String
Dim MiRs As rdoResultset

  Me.txtResponsable.Text = Nombre
  Me.SDCFecha.Date = FecPago
  txtCodPago = CodPago
  Me.Caption = "Compensaci�n de un abono con otro negativo"
  MiSqL = "Select * From FA1700 Where FA17CODPAGO = " & CodPago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    Me.SDCFecha = Format(MiRs("FA17FECPAGO"), "dd/mm/yyyy")
    If Not IsNull(MiRs("FA17FECCIERRE")) Then
      Me.SDCFechaCierre.Date = Format(MiRs("FA17FECCIERRE"), "DD/MM/YYYY")
    End If
    If Not IsNull(MiRs("FA17FECEFECTO")) Then
      Me.SDCFechaEfecto.Date = Format(MiRs("FA17FECEFECTO"), "DD/MM/YYYY")
    End If
    Me.txtApunte = MiRs("FA17APUNTE") & ""
    Me.txtRemesa = MiRs("FA17REMESA") & ""
    Me.txtObservaciones = MiRs("FA17OBSERV") & ""
    If Not IsNull(MiRs("FA20CODTIPPAGO")) Then
      Me.txtCodFormaPago = MiRs("FA20CODTIPPAGO")
      Call txtCodFormaPago_LostFocus
    End If
    If MiRs("FA17INDACUENVI") = 1 Then
      Me.chkIndAcuse.Value = 1
    Else
      Me.chkIndAcuse.Value = 0
    End If
  End If
  Me.txtCantidad = ImportePago
  If Not IsNull(MiRs("FA17FECEFECTO")) Then
    Me.txtCantidad.Locked = True
  End If
  lblPendiente = MiRs("FA17CANTIDAD") - Me.CalcularPendiente(CodPago)
  Call Me.CargarAbonos(CodPersona)
  
End Sub
Private Sub grdAbonos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim X As Integer
Dim Pendiente As Double
  If ColIndex = 4 Then
    If grdAbonos.Columns(4).Text = "" Then
      grdAbonos.Columns(4).Text = 0
    End If
    'La primera comprobaci�n ser� la de que la cantidad a compensar no es mayor que la cantidad facturada
    If CDbl(grdAbonos.Columns(4).Text) > CDbl(Abs(grdAbonos.Columns(3).Text)) Then
      MsgBox "La cantidad a compensar no puede ser mayor que la cantidad pendiente", vbOKOnly + vbCritical, "Atenci�n"
      grdAbonos.Columns(4).Text = OldValue
    Else
      ' Comprobaremos que la cantidad que sumamos no supera a la que queda pendiente
      Pendiente = CDbl(lblPendiente) + OldValue
      If Pendiente - grdAbonos.Columns(4).Text < 0 Then
        MsgBox "No se puede asignar esa cantidad ya que es mayor que la cantidad pendiente de asignaci�n", vbCritical + vbOKOnly, "Atenci�n"
        lblPendiente = Pendiente - OldValue
        grdAbonos.Columns(4).Text = OldValue
      Else
        lblPendiente = Pendiente - grdAbonos.Columns(4).Text
      End If
    End If
  ElseIf ColIndex = 5 Then
    
  End If
  
  
End Sub
Private Sub txtInicPago_LostFocus()
Dim MiSqL As String
Dim MiRs As rdoResultset

  If Trim(txtInicPago.Text) <> "" Then
    MiSqL = "Select * From FA2000 Where FA20DESIG = '" & Me.txtInicPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago = MiRs("FA20DESIG")
      Me.cboFormaPago.Text = MiRs("FA20DESC")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If txtInicPago.Enabled = True Then
        txtInicPago.SetFocus
      End If
    End If
  Else
    txtCodFormaPago.Text = ""
    cboFormaPago.Text = ""
  End If
End Sub


Private Sub grdAbonos_Click()
Dim Pendiente As Double

  If grdAbonos.Col = 5 Then
    If grdAbonos.Columns(5).Value = True Then
      Pendiente = CDbl(lblPendiente) + Abs(grdAbonos.Columns(3).Text)
      grdAbonos.Columns(4).Text = 0
      lblPendiente = Pendiente
    ElseIf grdAbonos.Columns(5).Value = False Then
      If CDbl(lblPendiente) >= Abs(grdAbonos.Columns(3).Text) Then
        lblPendiente = CDbl(lblPendiente) - Abs(grdAbonos.Columns(3).Text)
        grdAbonos.Columns(4).Text = Abs(grdAbonos.Columns(3).Text)
       Else
        MsgBox "No es posible compensar el abono, dado que la cantidad pendiente no es suficiente", vbCritical + vbOKOnly, "Atencion"
        grdAbonos.Columns(4).Value = False
      End If
    End If
  End If
End Sub

Private Sub txtCodFormaPago_LostFocus()
Dim MiSqL As String
Dim MiRs As rdoResultset

  If Trim(txtCodFormaPago.Text) <> "" Then
    MiSqL = "Select * From FA2000 Where FA20CODTIPPAGO = " & Me.txtCodFormaPago.Text
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago = MiRs("FA20DESIG")
      Me.cboFormaPago.Text = MiRs("FA20DESC")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If MiRs("FA20INDNOTABO") = 1 Then
        Me.chkIndAcuse.Value = 0
        Me.chkIndAcuse.Enabled = True
      Else
        Me.chkIndAcuse.Value = 1
        Me.chkIndAcuse.Enabled = False
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
    End If
  Else
    txtCodFormaPago.Text = ""
    cboFormaPago.Text = ""
  End If

End Sub



