VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Begin VB.Form frmConcSeleccionado 
   Caption         =   "Conciertos Aplicables"
   ClientHeight    =   5175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8310
   LinkTopic       =   "Form1"
   ScaleHeight     =   5175
   ScaleWidth      =   8310
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Salir"
      Height          =   420
      Left            =   6740
      TabIndex        =   1
      Top             =   4635
      Width           =   1500
   End
   Begin ComctlLib.TreeView tvwConc 
      Height          =   4425
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   8150
      _ExtentX        =   14367
      _ExtentY        =   7805
      _Version        =   327682
      Indentation     =   354
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList"
      Appearance      =   1
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   90
      Top             =   4635
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":0000
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":031A
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":0634
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":094E
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":0C68
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":0F82
            Key             =   "LineaFact"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":13D4
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":16EE
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":1A08
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":1D22
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConcSeleccionado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim varFact As factura

Private Sub tvwConcierto()
    Dim objconcierto As Concierto
    Dim nConcierto As Node
    Dim nNodo As Node
    Dim objREco As Persona
    Dim Tramo As Variant
    
    Dim NodoFACT As New Nodo
    Dim NodoLinea As New Nodo
    
    Screen.MousePointer = vbHourglass
    Call LockWindowUpdate(Me.hWnd)
    tvwConc.Nodes.Clear
    
    ' REcos
    Set nNodo = tvwConc.Nodes.Add(, , , "REcos")
    
    For Each objREco In varFact.Pacientes(1).Asistencias(1).REcos
    
      Set nConcierto = tvwConc.Nodes.Add(nNodo, tvwChild, , objREco.Name, "Editando")
      For Each Tramo In objREco.TramosFechas
         tvwConc.Nodes.Add nConcierto, tvwChild, , "(" & Tramo(0) & " - " & Tramo(1) & ")   -   (" & Tramo(2) & " - " & Tramo(3) & ")", "Hoja"
      Next Tramo

    Next
    
    ' Conciertos
    Set nNodo = tvwConc.Nodes.Add(, , , "Conciertos")
    
    For Each objconcierto In varFact.ConciertosAplicables
         Set nConcierto = tvwConc.Nodes.Add(nNodo, tvwChild, , "[" & objconcierto.Entidad.Codigo & "]    " & objconcierto.Entidad.Name, "Concierto")
         AņadirHijos objconcierto.Entidad, nConcierto
    Next
    nConcierto.EnsureVisible
   
   Call LockWindowUpdate(0&)
   Screen.MousePointer = vbDefault
    
End Sub

Sub AņadirHijos(NodoFACT As Nodo, NodoArbol As Node)
   ' se le pasa un nodo del objeto factura y su correpondiente nodo en el arbol
   ' y se encarga de crear sus hijos recursivamente
   Dim nRama As Node
   Dim objNodo As Nodo
   Dim strLinea  As String
   
   For Each objNodo In NodoFACT.Nodos
      strLinea = IIf(objNodo.Suficiente, "S", " ") & _
         IIf(objNodo.Necesario, "N", " ") & "  " & _
         "[" & objNodo.Codigo & "]    " & _
         IIf(objNodo.Descripcion <> "", objNodo.Descripcion, objNodo.Name) & "     " & _
         "  ..." & _
         IIf(objNodo.PrecioRef >= 0, " ( " & objNodo.PrecioRef & " Pts) ", "") & _
         IIf(objNodo.PrecioDia >= 0, " ( " & objNodo.PrecioDia & " Pts/Dia) ", "") & _
         " ( " & objNodo.FecInicio & " - " & objNodo.FecFin & " )"
         
                
        
      Set nRama = tvwConc.Nodes.Add(NodoArbol, tvwChild, , strLinea, IIf(objNodo.EsNodoFacturable Or objNodo.FactOblig, "LineaFact", "NodoCerrado"))
      
        
      AņadirHijos objNodo, nRama
   Next
   
   For Each objNodo In NodoFACT.Categorias
      strLinea = IIf(objNodo.Suficiente, "S", " ") & _
         IIf(objNodo.Necesario, "N", " ") & "  " & _
         "[" & objNodo.Codigo & "]    " & _
         IIf(objNodo.Descripcion <> "", objNodo.Descripcion, objNodo.Name) & "     " & _
         "  ..." & _
         IIf(objNodo.PrecioRef >= 0, " ( " & objNodo.PrecioRef & " Pts) ", "") & _
         IIf(objNodo.PrecioDia >= 0, " ( " & objNodo.PrecioDia & " Pts/Dia) ", "") & _
         " ( " & objNodo.FecInicio & " - " & objNodo.FecFin & " )"
        
      Set nRama = tvwConc.Nodes.Add(NodoArbol, tvwChild, , strLinea, IIf(objNodo.FactOblig, "LineaFact", "CatCerrado"))
        
      AņadirHijos objNodo, nRama
   Next
   


End Sub

Private Sub cmdSalir_Click()
  Me.Hide
End Sub

Private Sub Form_Load()
   Call tvwConcierto
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    If Me.Width < 4000 Then Me.Width = 4000
    If Me.Height < 4000 Then Me.Height = 4000
    
    tvwConc.Width = Me.Width - 300
    tvwConc.Height = Me.Height - 1080
    cmdSalir.Left = Me.Width - 1710
    cmdSalir.Top = Me.Height - 870
End Sub

Public Function pConc(objFactura As factura)
    Set varFact = objFactura
    
    Load frmConcSeleccionado
    
    frmConcSeleccionado.Show (vbModal)
            
End Function

