VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_AnulacionFacturas 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Anulaci�n de Facturas"
   ClientHeight    =   3345
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9885
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3345
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   510
      Left            =   8820
      TabIndex        =   1
      Top             =   2745
      Width           =   1005
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   510
      Left            =   8820
      TabIndex        =   0
      Top             =   135
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid grdFacturas 
      Height          =   3150
      Left            =   180
      TabIndex        =   2
      Top             =   90
      Width           =   8475
      ScrollBars      =   2
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   4
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowNavigation   =   1
      MaxSelectedRows =   10
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2302
      Columns(0).Caption=   "N� Factura"
      Columns(0).Name =   "NoFactura"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7646
      Columns(1).Caption=   "Responsable econ�mico"
      Columns(1).Name =   "Responsable"
      Columns(1).CaptionAlignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1984
      Columns(2).Caption=   "Fecha Fra."
      Columns(2).Name =   "Fecha"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1984
      Columns(3).Caption=   "Importe"
      Columns(3).Name =   "Cantidad"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      _ExtentX        =   14949
      _ExtentY        =   5556
      _StockProps     =   79
      Caption         =   "Facturas a anular"
   End
End
Attribute VB_Name = "frm_AnulacionFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAnular_Click()
    Dim x As Integer
    Dim Cadena As String
    Dim MiSql As String
    Dim RsAsistencias As rdoResultset
    Dim EnBlanco As Boolean
    Dim aNumFact() As String
    Dim contFact As Integer

    'Comprobamos que al menos una de las casillas tenga valor
    EnBlanco = True
    Me.grdFacturas.MoveFirst
    
    contFact = 0
    For x = 1 To 10
        If grdFacturas.Columns(0).Text <> "" Then
            contFact = contFact + 1
            ReDim Preserve aNumFact(1 To contFact) As String
            aNumFact(contFact) = grdFacturas.Columns(0).Text
            EnBlanco = False
        End If
        
        grdFacturas.MoveNext
    Next
    If EnBlanco Then
        MsgBox "Se debe introducir al menos un n�mero de factura", vbExclamation, "Atenci�n"
        If grdFacturas.Enabled = True Then grdFacturas.SetFocus
        Exit Sub
    End If
  
    'Una vez comprobado lo anterior llenamos los tipos definidos por el usario correspondientes a las facturas
    Call AnularFact(aNumFact)
End Sub


Private Sub cmdSalir_Click()
  Me.Hide
End Sub


Private Sub Form_Load()
Dim x As Integer
'  Call Me.IniciarQRY
  For x = 1 To 10
    Me.grdFacturas.AddItem Space(0)
  Next
End Sub

Private Sub grdFacturas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim strFactura As String
Dim rsFactura As rdoResultset
Dim rsNombre As rdoResultset
Dim MiSql As String

  If grdFacturas.Columns(0).Text <> "" Then
    strFactura = Me.grdFacturas.Columns(0).Text
    MiSql = "Select * from FA0400 Where FA04NUMFACT = '" & strFactura & "' AND FA04INDNMGC IS NULL"
    Set rsFactura = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not rsFactura.EOF Then
      MiSql = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22FECNACIM, CI22NUMDIRPRINC, " & _
              "CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
              " from CI2200, CI1000" & _
              " Where CI2200.CI21CODPERSONA = " & rsFactura("CI21CODPERSONA") & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " AND CI1000.CI10INDDIRPRINC = -1)"
      Set rsNombre = objApp.rdoConnect.OpenResultset(MiSql, 3)
    
      If Not rsNombre.EOF Then
        rsNombre.MoveFirst
        grdFacturas.Columns(1).Text = rsNombre("CI22NOMBRE") & " " & rsNombre("CI22PRIAPEL") & " " & rsNombre("CI22SEGAPEL")
        grdFacturas.Columns(2).Text = Format(rsFactura("FA04FECFACTURA"), "DD/MM/YYYY")
        grdFacturas.Columns(3).Text = FormatearMoneda(rsFactura("FA04CANTFACT"), tiTotalFact)
      Else
        MsgBox "No se puede anular una factura que no est� asociada a un paciente", vbExclamation + vbOKOnly, "Atenci�n"
        grdFacturas.Columns(0).Text = ""
        grdFacturas.Columns(1).Text = ""
        grdFacturas.Columns(2).Text = ""
        grdFacturas.Columns(3).Text = ""
        'GRDFACTURAS.Col = 0
      End If
    Else
      MsgBox "La factura no existe en la base de datos", vbExclamation + vbOKOnly, "Atenci�n"
      grdFacturas.Columns(0).Text = ""
      grdFacturas.Columns(1).Text = ""
      grdFacturas.Columns(2).Text = ""
      grdFacturas.Columns(3).Text = ""
    End If
  Else
    grdFacturas.Columns(1).Text = ""
    grdFacturas.Columns(2).Text = ""
    grdFacturas.Columns(3).Text = ""
  End If

End Sub
Private Sub grdFacturas_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 22 Then
      KeyAscii = fValidarNumFactura(KeyAscii, grdFacturas.Columns(0).Text)
    End If
End Sub








