
CREATE OR REPLACE VIEW FA2905J ( FECHA, 
CI43CODGRUCON, CI32CODTIPECON, AD02CODDPTO, FA29IMPORTE
 ) AS SELECT  
fa0400.fa04feccontabil										FECHA,  
ci3200.CI43CODGRUCON										CI43CODGRUCON,  
fa0400.ci32codtipecon										ci32codtipecon,  
fa2900.AD02CODDPTO											AD02CODDPTO,  
sum(-fa2900.fa29importe)									FA29IMPORTE  
FROM FA2900,fa0400,ci3200  
WHERE  
fa2900.AD02CODDPTO IS NOT NULL  
AND fa2900.AD02CODDPTO<>310  
and fa2900.fa04codfact=fa0400.fa04codfact  
and ci3200.ci32codtipecon=fa0400.ci32codtipecon  
and fa0400.fa04feccontabil is not null  
group by  
fa0400.fa04feccontabil,  
ci3200.CI43CODGRUCON,  
fa0400.ci32codtipecon,  
fa2900.AD02CODDPTO
