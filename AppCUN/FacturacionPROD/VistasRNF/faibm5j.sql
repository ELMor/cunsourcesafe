   CREATE OR REPLACE VIEW faibm5j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic,
      grupo,
      PVL,
      codExt
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci,
             b.fa05codcateg,
             TO_NUMBER (c.canti3),
             TO_DATE (
                c.fsoli3 ||
                   DECODE (c.hsoli3, 'FFFF', '0000', '2400', '0000', c.hsoli3),
                'YYYYMMDDHH24MI'
             ),
             TO_NUMBER (SUBSTR (c.nhist3, 1, 6)),
             0,
             '',
             215,
             '',
             NVL(B.FA05DESIG, FR7300.FR73DESPRODUCTO),
             1,
             DECODE (
                impss3, 0, TO_NUMBER (impor3) / TO_NUMBER (c.canti3),
                TO_NUMBER (impss3) / TO_NUMBER (c.canti3)
             ),
             DECODE(c.flag3,'0A',-1,0),
	     '0' || fr7300.FR00CODGRPTERAP,
             TO_NUMBER (NVL(impss3,'0')) / TO_NUMBER (c.canti3),
	     FR7300.FR73CODPRODUCTO
        FROM ad0800 a,
             fa0500 b,
	     FR7300,
             r3mov c
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen = c.medic3
         AND b.fa08codgrupo = 1
	 AND FR7300.FR73CODINTFAR = c.medic3

