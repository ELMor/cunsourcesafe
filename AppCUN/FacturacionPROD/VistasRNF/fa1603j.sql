

   CREATE OR REPLACE VIEW fa1603j (fa04numfact, importe) AS
      SELECT fa04numfact, SUM (NVL (importe, 0)) AS importe
        FROM fa1602j
       GROUP BY fa04numfact