   CREATE OR REPLACE VIEW fa0010j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic,
      grupo,
      PVL,
      codExt
   ) AS
      SELECT /*Vista que genera revision si es Exploraciones*/
             ad0500.ad07codproceso,
             ad0500.ad01codasistenci,
             fa05codcateg,
             1 cantidad,
             ad01fecinicio fecha,
             ci22numhistoria,
             0 dptosolicita,
             TO_CHAR (NULL) drsolicita,
             ad02coddpto dptorealiza,
             TO_CHAR (NULL) drrealiza,
             NULL obs,
             1 estado,
             TO_NUMBER (NULL) precio,
             0 ic,
	     NULL,
	     NULL,
	     NULL
        FROM ad0100,
             ad2500,
             ad0500,
             fa0500
       WHERE ad0100.ad01codasistenci = ad0500.ad01codasistenci
         AND ad2500.ad01codasistenci = ad0100.ad01codasistenci
         AND ad0500.ad05fecfinrespon IS NULL
         AND ad2500.ad12codtipoasist = 3
         AND ad2500.ad25fecfin IS NULL
         AND fa0500.fa05codorigc1 = ad0500.ad02coddpto
         AND fa0500.fa05codorigc2 = 0
         AND fa0500.fa05codorigc3 = 4
         AND fa0500.fa05codorigc4 = 1
         AND fa0500.fa08codgrupo = 7
