CREATE OR REPLACE VIEW fa1701j (
   orden,
   ci21codpersona,
   fecha,
   fac_abo,
   cod,
   debe,
   haber,
   saldo
) AS
   SELECT               /* Calcula el saldo por fechas de una cuenta corriente*/
       v1.ord,
       v1.cp,
       v1.fe,
       v1.tr,
       v1.cod,
       ROUND(DECODE (v1.tr, 1, v1.im, 0)),
       ROUND(DECODE (v1.tr, 2, -v1.im, 0)),
       SUM(ROUND(v2.im))
     FROM (SELECT/*+ INDEX(FA0400 FA0402)*/
               1 tr,
               ci21codpersona cp,
               fa04fecfactura fe,
               fa04cantfact im,
               (fa04fecfactura - TO_DATE ('01011900', 'DDMMYYYY')) * 100000000 +
                   fa04codfact ord,
               fa04numfact cod
             FROM fa0400
            WHERE fa04numfacreal IS NULL
           UNION ALL
           SELECT 2 tr,
                  ci21codpersona cp,
                  fa17fecpago fe,
                  -fa17cantidad im,
                  (fa17fecpago - TO_DATE ('01011900', 'DDMMYYYY')) * 100000000 +
                    90000000 +  fa17codpago ord,
                  to_char(fa17codpago) cod
             FROM fa1700) v1,
          (SELECT/*+ INDEX(FA0400 FA0402)*/
               1 tr,
               ci21codpersona cp,
               fa04fecfactura fe,
               fa04cantfact im,
               (fa04fecfactura - TO_DATE ('01011900', 'DDMMYYYY')) * 100000000 +
                   fa04codfact ord,
               fa04numfact cod
             FROM fa0400
            WHERE fa04numfacreal IS NULL
           UNION ALL
           SELECT 2 tr,
                  ci21codpersona cp,
                  fa17fecpago fe,
                  -fa17cantidad im,
                  (fa17fecpago - TO_DATE ('01011900', 'DDMMYYYY')) * 100000000 +
                     90000000 + fa17codpago ord,
                  to_char(fa17codpago) cod
             FROM fa1700) v2
    WHERE v1.cp = v2.cp
      AND v2.fe <= v1.fe
      AND v2.ord <= v1.ord
    GROUP BY v1.ord,
             v1.cp,
             v1.fe,
             v1.tr,
             v1.cod,
             DECODE (v1.tr, 1, v1.im, 0),
             DECODE (v1.tr, 2, -v1.im, 0)