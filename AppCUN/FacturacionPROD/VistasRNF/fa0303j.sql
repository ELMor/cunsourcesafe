

   CREATE OR REPLACE VIEW fa0303 (
      fa04codfact,
      fa16numlinea,
      cantidad,
      cuenta,
      importe,
      fa03fecha,
      fecha
   ) AS
      (SELECT/*+ RULE */
           fa04numfact AS fa04codfact,
           fa16numlinea,
           SUM (fa03cantfact) AS cantidad,
           COUNT (distinct TO_CHAR (fa03fecha, 'DD/MM/YYYY')) AS cuenta,
           SUM (NVL (NVL (fa03cantfact, fa03cantidad), 0) *
                   NVL (NVL (fa03preciofact, fa03precio), 0)) AS importe,
           TO_CHAR (MIN (fa03fecha), 'DD/MM/YY') AS fa03fecha,
           MIN (fa03fecha) AS fecha
         FROM fa0300
        GROUP BY fa04numfact, fa16numlinea)
