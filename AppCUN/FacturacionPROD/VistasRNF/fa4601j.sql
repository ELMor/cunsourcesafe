
   CREATE OR REPLACE VIEW fa4601j (
      fa25numsegsoc,
      ad01fecinicio,
      tipoasistencia,
      fa25apel1,
      fa25apel2,
      fa25nomb,
      fa25idcliente,
      fa25fechnac,
      fa25sexo,
      fa25codpostal,
      ad01fecingreso,
      circingreso,
      ad01fecfin,
      circalta,
      ad02coddpto,
      fa04cantfact,
      numreg,
      fecproceso,
      fa25expinf,
      fa04fecfactura
   ) AS
      (
   /* VISTA PARA OSASUNBIDEA DE PACIENTES HOSPITALIZADOS */
       SELECT/*+ RULE */
           RPAD (fa2500.fa25numsegsoc, 10) AS fa25numsegsoc,
           TO_CHAR (ad0100.ad01fecinicio, 'YYYYMMDD') AS ad01fecinicio,
           'HO' AS tipoasistencia,
           RPAD (fa2500.fa25apel1, 18) AS fa25apel1,
           RPAD (fa2500.fa25apel2, 18) AS fa25apel2,
           RPAD (fa2500.fa25nomb, 14) AS fa25nomb,
           RPAD (fa2500.fa25idcliente, 23) AS fa25idcliente,
           RPAD (TO_CHAR (fa2500.fa25fechnac, 'DDMMYYY'), 7) AS fa25fechnac,
           RPAD (fa2500.fa25sexo, 1) AS fa25sexo,
           RPAD (fa2500.fa25codpostal, 5) AS fa25codpostal,
           TO_CHAR (ad0100.ad01fecinicio, 'HH24DDMMYYY') AS ad01fecingreso,
           DECODE (ad0100.ad01indurgente, -1, 1, 2) AS circingreso,
           TO_CHAR (ad0100.ad01fecfin, 'HH24DDMMYYY') AS ad01fecfin,
           DECODE (
              ad0100.ad27codaltaasist, 1, 1, 2, 1, 3, 1, 4, 3, 5, 2, 6, 4, 7,
   1, 1
           ) AS circalta,
           RPAD (ad0500.ad02coddpto, 3) AS ad02coddpto,
           LPAD (fa0400_1.fa04cantfact, 10) AS fa04cantfact,
           LPAD (ROWNUM, 4, '0') AS numreg,
           '200001' AS fecproceso,
           RPAD (fa2500.fa25expinf, 14) AS fa25expinf,
           fa0400_1.fa04fecfactura
         FROM ad0100,
              ad0500,
              fa2500,
              fa0400 fa0400_1
        WHERE ad0100.ad01codasistenci = fa0400_1.ad01codasistenci
          AND ad0500.ad01codasistenci = fa0400_1.ad01codasistenci
          AND ad0500.ad07codproceso = fa0400_1.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND fa2500.ad01codasistenci = fa0400_1.ad01codasistenci
          AND fa2500.ad07codproceso = fa0400_1.ad07codproceso
          AND fa0400_1.fa04codfact IN
              (SELECT/*+ RULE */
                   fa04codfact
                 FROM ad2500,
                      fa0400
                WHERE ad2500.ad01codasistenci = fa0400.ad01codasistenci
                  AND fa0400.fa04fecfactura BETWEEN ad2500.ad25fecinicio
                          AND NVL (ad2500.ad25fecfin, SYSDATE)
                  AND fa0400.fa04numfacreal IS NULL
                  AND ad2500.ad12codtipoasist = 1
                  AND fa0400.ci32codtipecon = 'S'
                  AND fa0400.ci13codentidad = 'NA'
                  AND fa0400.fa04fecfactura = TO_DATE ('31/1/2000', 
   'DD/MM/YYYY')))
