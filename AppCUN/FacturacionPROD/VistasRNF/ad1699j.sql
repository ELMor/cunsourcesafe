   CREATE OR REPLACE VIEW ad1699j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT
             /*ESTANCIAS LOS QUE NO SON DE INSALUD NI LAGUN ARO*/
             a7.ad07codproceso,
             t1.ad01codasistenci,
             f5.fa05codcateg,
             DECODE (
                regnum, 0, DECODE (
                              TO_CHAR (fecha, 'HH24'), 12, 0.5, 13, 0.5, 14, 0.5,
                              15, 0.5, 16, 0.5, 17, 0.5, 18, 0.5, 19, 0.5, 20, 0.5,
                              21, 0, 22, 0, 23, 0, 1
                           ), DECODE (
                                 ultimo, -1, DECODE (
                                                TO_CHAR (fecha, 'HH24'), 18, 1, 19,
                                                1, 20, 1, 21, 1, 22, 1, 23, 1, 0.5
                                             ), 1
                              )
             ) cantidad,
             fecha,
             a7.ci22numhistoria,
             0 dptosolicita,
             TO_CHAR (0) drsolicita,
             a5.ad02coddpto dptorealiza,
             TO_CHAR (0) drrealiza,
             gcfn06 (t1.ad15codcama) obs,
             1 estado,
             0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND NOT EXISTS (SELECT ci32codtipecon
                           FROM ad1100 z1
                          WHERE z1.ad07codproceso = t1.ad07codproceso
                            AND z1.ad01codasistenci = t1.ad01codasistenci
                            AND (z1.ci32codtipecon = 'S' 
				OR (ci32codtipecon = 'M' AND ci13codentidad ='LA')
				)
			)
         AND a5.ad15codcama = t1.ad15codcama
      UNION ALL
      SELECT/*ESTANCIAS LOS QUE SI SON DE INSALUD PERO NO DE OSASUNBIDEA,QUE TIENE ESTANCIAS QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          f5.fa05codcateg,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND (	(z1.ci32codtipecon = 'S' AND z1.ci13codentidad <> 'NA')
			    )
		   )
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
      UNION ALL
      SELECT/*ESTANCIAS LOS QUE SI SON DE OSASUNBIDEA , ESTANCIAS NO QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          f5.fa05codcateg,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND (z1.ci32codtipecon = 'S'AND z1.ci13codentidad = 'NA')
		    )
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
         AND fecha <
              (SELECT TRUNC(NVL (
                         MIN (pr04feciniact), SYSDATE
                      ))/* MENOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci)
      UNION ALL
      SELECT/*ESTANCIAS LOS QUE SI SON DE OSASUNBIDEA,ESTANCIAS QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          42287,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND (z1.ci32codtipecon = 'S'AND z1.ci13codentidad = 'NA')
		   )
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
         AND fecha >=
              (SELECT TRUNC(NVL (
                         MIN (pr04feciniact), SYSDATE
                      ))/* MAYOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci)
      UNION ALL
      SELECT/*ASISTENCIA CLINICA EN PLANTA*/
          DISTINCT a7.ad07codproceso,
                   t1.ad01codasistenci,
                   f5.fa05codcateg,
                   1 cantidad,
                   fecha,
                   a7.ci22numhistoria,
                   0 dptosolicita,
                   TO_CHAR (0) drsolicita,
                   a5.ad02coddpto dptorealiza,
                   TO_CHAR (0) drrealiza,
                   '',
                   1 ,
                   0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad0500 a5
       WHERE f5.fa08codgrupo = 7
         AND
             /*PRUEBAS:Asistencia Clinica*/
             f5.fa05codorigc1 = a5.ad02coddpto
         AND f5.fa05codorigc2 = 0
         AND f5.fa05codorigc3 = 3
         AND f5.fa05codorigc4 = 2
         AND a7.ad07codproceso = t1.ad07codproceso
         AND a5.ad07codproceso = t1.ad07codproceso
         AND a5.ad01codasistenci = t1.ad01codasistenci
         AND fecha BETWEEN a5.ad05fecinirespon AND NVL (
                                                      a5.ad05fecfinrespon, SYSDATE
                                                   )
         AND fecha <
              (SELECT TRUNC(NVL (
                         MIN (pr04feciniact), SYSDATE
                      ))/* MENOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci
                  AND ad1697j.ad02coddpto = a5.ad02coddpto)
      UNION ALL
      SELECT/*ASISTENCIA CLINICA EN UCI*/
          DISTINCT a7.ad07codproceso,
                   t1.ad01codasistenci,
                   41980,
                   1 cantidad,
                   fecha,
                   a7.ci22numhistoria,
                   0 dptosolicita,
                   TO_CHAR (0) drsolicita,
                   318 dptorealiza,
                   TO_CHAR (0) drrealiza,
                   '',
                   1 ,
                   0
        FROM ad1698j t1,
             ad0700 a7,
             ad1500 a15
       WHERE a7.ad07codproceso = t1.ad07codproceso
         AND a15.ad15codcama = t1.ad15codcama
         AND a15.ad02coddpto = 318                                          /*UCI*/
      UNION ALL
      SELECT/*ESTANCIAS DE LAGUN ARO , ESTANCIAS NO QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          f5.fa05codcateg,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND ci32codtipecon = 'M' 
			AND ci13codentidad ='LA'
		    )
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
         AND (fecha <
              (SELECT TRUNC(NVL (
                         MIN (pr04feciniact), SYSDATE
                      ))/* MENOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci)
	    OR t1.ad13codtipocama IN (7,8,9) 	/* SI ES UCI -> no es quirúrgica */
	   )
      UNION ALL
      SELECT/*ESTANCIAS DE LAGUN ARO,ESTANCIAS QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          42287,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
			AND ci32codtipecon = 'M' 
			AND ci13codentidad ='LA'
		   )
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
         AND fecha >=
              (SELECT TRUNC(NVL (
                         MIN (pr04feciniact), SYSDATE
                      ))/* MAYOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci)
	AND t1.ad13codtipocama NOT IN (7,8,9)	/* Las Estancias UCI no son quirúrgicas */