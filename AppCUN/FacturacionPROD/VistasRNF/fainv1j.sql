
   CREATE OR REPLACE VIEW fainv1j (fecha, ad02coddpto, precio, 
   ad12codtipoasist) AS
      SELECT          /* Movimientos  de  investigación de la pr0400 con su 
   coste*/
            pr0400.pr04feciniact fecha,
            pr0400.ad02coddpto,
            NVL (
               mu.fa15precioref,
               pa.fa15precioref /
                  2
            )
                  precio,
            ad2500.ad12codtipoasist
        FROM pr0400,
             fa0500,
             fa1500 pa,
             fa1500 mu,
             ad2500
       WHERE pr0400.pr04indintcientif = -1
         AND pr0400.pr01codactuacion = fa0500.fa05codorigen
         AND pa.fa15codfin = fa0500.fa05codcateg
         AND pa.fa15escat = 1
         AND pa.fa15codconc = 97
         AND pa.fa15ruta LIKE '.97.328./.%'
         AND mu.fa15codfin = fa0500.fa05codcateg
         AND mu.fa15escat = 1
         AND mu.fa15codconc = 102
         AND mu.fa15ruta LIKE '.102.328./.%'
         AND pr0400.pr04feciniact BETWEEN mu.fa15fecinicio AND mu.fa15fecfin
         AND pr0400.pr04feciniact BETWEEN pa.fa15fecinicio AND pa.fa15fecfin
         AND pr0400.pr37codestado < 6
         AND fa0500.fa08codgrupo IN (6, 7)
         AND ad2500.ad01codasistenci = pr0400.ad01codasistenci
         AND ad2500.ad25fecfin IS NULL
      UNION ALL
      SELECT         /* moviemientos que no siendo de investigación su teco 
   es H */
          pr0400.pr04feciniact fecha,
          pr0400.ad02coddpto,
          NVL (mu.fa15precioref, pa.fa15precioref / 2) precio,
          ad2500.ad12codtipoasist
        FROM pr0400,
             fa0500,
             fa1500 pa,
             fa1500 mu,
             ad2500,
             ad1100
       WHERE pr0400.pr04indintcientif = 0
         AND pr0400.pr01codactuacion = fa0500.fa05codorigen
         AND pa.fa15codfin = fa0500.fa05codcateg
         AND pa.fa15escat = 1
         AND pa.fa15codconc = 97
         AND pa.fa15ruta LIKE '.97.328./.%'
         AND mu.fa15codfin = fa0500.fa05codcateg
         AND mu.fa15escat = 1
         AND mu.fa15codconc = 102
         AND mu.fa15ruta LIKE '.102.328./.%'
         AND pr0400.pr04feciniact BETWEEN mu.fa15fecinicio AND mu.fa15fecfin
         AND pr0400.pr04feciniact BETWEEN pa.fa15fecinicio AND pa.fa15fecfin
         AND pr0400.pr37codestado < 6
         AND fa0500.fa08codgrupo IN (6, 7)
         AND ad2500.ad01codasistenci = pr0400.ad01codasistenci
         AND ad2500.ad25fecfin IS NULL
         AND ad1100.ad01codasistenci = ad2500.ad01codasistenci
         AND ad1100.ci32codtipecon = 'H'
         AND ad1100.ad11fecfin IS NULL
