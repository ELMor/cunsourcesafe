
   CREATE OR REPLACE VIEW fa0304 (fecha, fa04codfact, fa16numlinea) AS
      (SELECT DISTINCT TO_CHAR (fa03fecha, 'DD/MM/YYYY') AS fecha,
                       fa04numfact AS fa04codfact,
                       fa16numlinea
         FROM fa0300)
