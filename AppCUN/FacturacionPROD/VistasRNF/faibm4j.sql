   CREATE OR REPLACE VIEW faibm4j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic,
      grupo,
      PVL,
      codExt
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci,
             b.fa05codcateg,
             TO_NUMBER (c.canti1),
             TO_DATE (
                c.fsoli1 ||
                   DECODE (c.hsoli1, 'FFFF', '0000', '2400', '0000', c.hsoli1),
                'YYYYMMDDHH24MI'
             ),
             TO_NUMBER (SUBSTR (c.nhist1, 1, 6)),
             0,
             '',
             215,
             '',
             NVL(B.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             DECODE (
                impss1, 0, TO_NUMBER (impor1) / TO_NUMBER (c.canti1),
                TO_NUMBER (impss1) / TO_NUMBER (c.canti1)
             ),
             DECODE(c.flag1,'0A',-1,0),
	     '0' || fr7300.FR00CODGRPTERAP,
             TO_NUMBER (NVL(impss1,'0')) / TO_NUMBER (c.canti1),
	     FR7300.FR73CODPRODUCTO
        FROM ad0800 a,
             fa0500 b,
	     FR7300,
             r1mov c
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen (+)= c.medic1
         AND b.fa08codgrupo (+)= 1
	 AND FR7300.FR73CODINTFAR = c.medic1
