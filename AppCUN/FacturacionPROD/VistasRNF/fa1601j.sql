
   CREATE OR REPLACE VIEW fa1601j (
      fa04codfact,
      orden,
      descrip_p,
      fa16precio,
      fa16importe,
      fa16ordfact,
      fa16franquicia,
      fa16cantsinfran,
      fa43concepto,
      codigo,
      descrip_d,
      descrip_r,
      cantidad,
      fecha,
      fa42codreg,
      fa42descripcion
   ) AS
      SELECT fa1600.fa04codfact,
             fa1600.fa16numlinea AS orden,
             fa1600.fa16descrip AS descrip_p,
             fa1600.fa16precio,
             fa1600.fa16importe,
             fa1600.fa16ordfact,
             fa1600.fa16franquicia,
             fa1600.fa16cantsinfran,
             fa4000.fa43concepto,
             fa4000.fa40codigo AS codigo,
             fa4000.fa40descripcion AS descrip_d,
             DECODE (
                fa4200.fa42mostrarcat, -1, fa4000.fa40descripcion,
                fa4300.fa43descripcion
             ) AS descrip_r,
             fafn07 (fa1600.fa04codfact, fa1600.fa16numlinea) AS cantidad,
             fafn08 (fa1600.fa04codfact, fa1600.fa16numlinea) AS fecha,
             fa4200.fa42codreg,
             fa4200.fa42descripcion
        FROM fa1600,
             fa4000,
             fa4100,
             fa4200,
             fa4300
       WHERE fa1600.fa16descrip = fa4100.fa41descripcion
         AND fa4100.fa40codreg = fa4000.fa40codreg
         AND fa4000.fa43concepto = fa4300.fa43concepto
         AND fa4300.fa42codreg = fa4200.fa42codreg
      UNION
      SELECT fa0400.fa04codfact,
             1000 + ad4000.ad40orden AS orden,
             fafn09 AS descrip_p,
             fafn10 AS fa16precio,
             fafn10 AS fa16importe,
             fafn10 AS fa16ordfact,
             fafn10 AS fa16franquicia,
             fafn10 AS fa16cantsinfran,
             fa4300.fa43concepto,
             ad3900.ad39codigo AS codigo,
             ad3900.ad39descodigo AS descrip_d,
             DECODE (
                fa4200.fa42mostrarcat, -1, ad3900.ad39descodigo,
                fa4300.fa43descripcion
             ) AS descrip_r,
             DECODE (ad4000.ad40tipodiagproc, 'P', 1, 0) AS cantidad,
             fafn11 AS fecha,
             fa4200.fa42codreg,
             fa4200.fa42descripcion
        FROM fa0400,
             ad3900,
             ad4000,
             fa4200,
             fa4300
       WHERE fa0400.ad01codasistenci = ad4000.ad01codasistenci
         AND fa0400.ad07codproceso = ad4000.ad07codproceso
         AND ad4000.ad39codigo = ad3900.ad39codigo
         AND ad3900.ad39tipocodigo = 'D'
         AND fa4300.fa42codreg = fa4200.fa42codreg
         AND fa4300.fa43concepto = 'DG'
