



   CREATE OR REPLACE VIEW fa0408j (fa48coddiskacunsa, fa1r90, fa04numfact, importe) AS
      SELECT DISTINCT fa4800.fa48coddiskacunsa,
                      LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                  -- Numero de Factura: Number(7)  
                         '90' ||                        -- Registros de Tipo '90'  
                         LPAD (fa04cantfact, 10, '0') ||
                                         -- Cantidad Facturada (Total) Number(10)  
                         LPAD (
                            SUM (DECODE (
                                    fa0500.fa08codgrupo, 5, fa03cantfact * 10, 0
                                 )), 6, 0
                         ) ||                              -- Numero de Estancias  
                         TO_CHAR (fa48fecha, 'YYYYMMDD') ||
                                             -- Ultimo dia del mes de facturacion  
                         LPAD (NVL (ad0500.sg02cod, ' '), 5, ' ') ||
                                                -- Doctor Responsable: Varchar(5)  
                         LPAD (fnfa02 (fa04observ), 8, '0')
                            fa1r90,                          -- Numero de Volante  
                      fa0400.fa04numfact,
                      fa0400.fa04cantfact importe
        FROM fa0400,
             fa0300,
             fa4800,
             fa4900,
             fa0500,
             ad0500
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa0400.fa04numfact = fa4900.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.fa04cantfact <> 0            -- SOLO LAS FACTURAS CON IMPORTE  
         AND fa0400.ad07codproceso = ad0500.ad07codproceso
         AND fa0400.ad01codasistenci = ad0500.ad01codasistenci
         AND ad0500.ad05fecfinrespon IS NULL
         AND fa04numfacreal IS NULL
       GROUP BY fa4800.fa48coddiskacunsa,
                fa0400.fa04numfact,
                fa0400.fa04cantfact,
                fa48fecha,
                ad0500.sg02cod,
                fa04observ
