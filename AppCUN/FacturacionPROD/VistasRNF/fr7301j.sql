

   CREATE OR REPLACE VIEW fr7301 (fr73codintfar, cp) AS
      SELECT fr73codintfar, MAX (fr73codproducto) cp
        FROM fr7300
       GROUP BY fr73codintfar
