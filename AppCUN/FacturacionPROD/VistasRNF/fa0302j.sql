
   CREATE OR REPLACE VIEW fa0302j (pr04numactplan, fa04numfact) AS
      (
   /* VISTA QUE DEVUELVE REGISTRO SI ESTA FACTURADA Y SINO NO */
       SELECT pr0400.pr04numactplan, fa0400.fa04numfact
         FROM fa0400,
              fa0500,
              fa0300,
              pr0100,
              pr0400
        WHERE fa0500.fa05codorigen = pr0400.pr01codactuacion
          AND pr0100.pr01codactuacion = pr0400.pr01codactuacion
          AND pr0100.pr12codactividad NOT IN (201, 207)
          AND fa0300.fa05codcateg = fa0500.fa05codcateg
          AND fa0300.fa03fecha = pr0400.pr04feciniact
          AND fa0300.ad01codasistenci = pr0400.ad01codasistenci
          AND fa0300.ad07codproceso = pr0400.ad07codproceso
          AND fa0300.fa13codestrnf > 0
          AND fa0400.fa04codfact (+) = fa0300.fa04numfact
       UNION ALL
   /* CASO DE INTERVENCIONES */
       SELECT pr0400.pr04numactplan, fa0400.fa04numfact
         FROM fa0400,
              fa0500,
              fa0300,
              pr0100,
              pr0400
        WHERE fa0500.fa05codorigen = pr0400.pr01codactuacion
          AND pr0100.pr01codactuacion = pr0400.pr01codactuacion
          AND pr0100.pr12codactividad = 207
          AND fa0300.fa05codcateg = fa0500.fa05codcateg
          AND fa0300.fa03fecha = pr0400.pr04fecfinact
          AND fa0300.ad01codasistenci = pr0400.ad01codasistenci
          AND fa0300.ad07codproceso = pr0400.ad07codproceso
          AND fa0300.fa13codestrnf > 0
          AND fa0400.fa04codfact (+) = fa0300.fa04numfact
       UNION ALL
   /* CASO DE CONSULTAS */
       SELECT pr0400.pr04numactplan, fa0400.fa04numfact
         FROM fa0400,
              fa0500,
              fa0300,
              pr0100,
              ad0800,
              ag1100,
              ad0300,
              pr1000,
              pr0400
        WHERE pr0100.pr01codactuacion = pr0400.pr01codactuacion
          AND pr0100.pr12codactividad = 201
          AND ad0800.ad07codproceso = pr0400.ad07codproceso
          AND ad0800.ad01codasistenci = pr0400.ad01codasistenci
          AND fa0300.fa05codcateg = fa0500.fa05codcateg
          AND fa0300.fa03fecha = pr0400.pr04fecfinact
          AND fa0300.ad01codasistenci = pr0400.ad01codasistenci
          AND fa0300.ad07codproceso = pr0400.ad07codproceso
          AND fa0500.fa05codorigc1 = pr0400.ad02coddpto
          AND fa0500.fa05codorigc2 = 0
          AND fa0500.fa05codorigc3 =
                 DECODE (
                    pr0400.pr01codactuacion, 4293/*URGENCIAS*/, 1, 3630/*ARV1*/, 8,
                    4039/*ARV2*/, 8, DECODE (
                                        ad0800.ad10codtippacien, 2, 7, 3, 2, 1
                                     )
                 )                                                  /* N-R, R, N */
          AND fa0500.fa05codorigc4 =
                 DECODE (
                    pr0400.pr01codactuacion, 4293/*URGENCIAS*/, 2,
                    6306/*ANESTESIA*/, 2, 3630/*ARV1*/, 1, 4039/*ARV2*/, 2,
                    3070/*PSICOTERAPIA*/, DECODE (
                                             ag1100.sg02cod, 'SCE', 4,
                                             DECODE (
                                                ad0300.ad31codpuesto, 6, 5, 5, 5,
                                                1, 6, 5
                                             )
                                          ),
                    DECODE (
                       ag1100.sg02cod, 'EML', 1, 'ABM', 1, 'ERH', 1, 'PRV', 1,
                       'MLM', 1, 'RGT', 1, 'SCE', 1, 'JMB', 1, 'JCM', 1,
                       DECODE (ad0300.ad31codpuesto, 6, 2, 5, 2, 1, 3, 2)
                    )
                 )
          AND pr1000.pr04numactplan = pr0400.pr04numactplan
          AND ag1100.ag11codrecurso = pr1000.ag11codrecurso
          AND ad0300.sg02cod (+) =
                 ag1100.sg02cod
                              /* AMPLIAR POR LOS RESIDENTES (T11.SG02COD IS NULL)*/
          AND ad0300.ad02coddpto (+) = ag1100.ad02coddpto
          AND ad0300.ad03fecfin (+) IS NULL
          AND pr1000.pr07numfase = 1
          AND fa0300.fa13codestrnf > 0
          AND fa0400.fa04codfact (+) = fa0300.fa04numfact)
