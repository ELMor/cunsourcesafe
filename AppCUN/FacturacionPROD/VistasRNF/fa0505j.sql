



   CREATE OR REPLACE VIEW fa0505j (
      pr04numactplan,
      ad07codproceso,
      ad01codasistenci,
      pr01codactuacion,
      pr04feciniact,
      ci22numhistoria,
      paciente,
      fechas,
      ci32codtipecon,
      ci13codentidad,
      fa05codcateg,
      fa05desig
   ) AS
      (SELECT/*+ RULE */
           /* VISTA PARA REHABILITACIÓN SIN FACTURAR */
           pr0400.pr04numactplan,
           pr0400.ad07codproceso,
           pr0400.ad01codasistenci,
           pr0400.pr01codactuacion,
           pr0400.pr04feciniact,
           ci2200.ci22numhistoria,
           (
              ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                 ci2200.ci22nombre
           ) AS paciente,
           (
              TO_CHAR (ad0100.ad01fecinicio, 'DD/MM/YYYY') || '-' ||
                 TO_CHAR (ad0100.ad01fecfin, 'DD/MM/YYYY')
           ) AS fechas,
           ad1100.ci32codtipecon,
           ad1100.ci13codentidad,
           fa0500.fa05codcateg,
           fa0500.fa05desig
         FROM fa0500,
              ad0100,
              ad1100,
              ci2200,
              pr0400
        WHERE ci2200.ci21codpersona = pr0400.ci21codpersona
          AND ad1100.ad01codasistenci = pr0400.ad01codasistenci
          AND ad1100.ad07codproceso = pr0400.ad07codproceso
          AND ad1100.ad11fecfin IS NULL
          AND ad0100.ad01codasistenci = pr0400.ad01codasistenci
          AND fa0500.fa05codorigen = pr0400.pr01codactuacion
          AND fa0500.fa08codgrupo = 7
          AND pr0400.pr37codestado < 6
          AND pr0400.ad02coddpto = 211)
