
CREATE OR REPLACE VIEW FA2903J ( FECHA, 
CUENTA, DPTO, PRODUCTO, PROYECTO, 
FILIACION, IMPORTE, CONCEPTO ) AS SELECT  /*Asiento Especial Farmacia*/  
LAST_DAY(TRUNC(FRK1FECMOV))		   							   FECHA,  
'9212' 	  		  	   			   							   CUENTA,  
'13'||LPAD(AD02CODDPTO,4,'0')								   DPTO,  
'1'															   PRODUCTO,  
NULL														   PROYECTO,  
NULL														   FILIACION,  
-SUM(round(nvl(frk1cantidad*frk1precio,frk1cantidad*fr73precioventa)))    IMPORTE,  
'FAC.FARMAC.'||TO_CHAR(LAST_DAY(TRUNC(FRK1FECMOV)),'MON-YYYY') CONCEPTO  
FROM  
FRK100,FR7300  
WHERE  
FRK100.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO  
GROUP BY  
LAST_DAY(TRUNC(FRK1FECMOV)),  
AD02CODDPTO  
UNION ALL  
SELECT  
LAST_DAY(TRUNC(FRK1FECMOV))	  				   				   FECHA,  
'0212'														   CUENTA,  
'130215'													   DPTO,  
'1'															   PRODUCTO,  
NULL														   PROYECTO,  
NULL														   FILIACION,  
SUM(round(nvl(frk1cantidad*frk1precio,frk1cantidad*fr73precioventa))) IMPORTE,  
'FAC.FARMAC.'||TO_CHAR(LAST_DAY(TRUNC(FRK1FECMOV)),'MON-YYYY') CONCEPTO  
FROM  
FRK100,FR7300  
WHERE  
FRK100.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO  
GROUP BY  
LAST_DAY(TRUNC(FRK1FECMOV))
