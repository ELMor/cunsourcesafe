

   CREATE OR REPLACE VIEW fa0305 (fa04codfact, fa03fecha, tipoconsulta) AS
      (SELECT/*+ RULE */
           fa0300_1.fa04numfact AS fa04codfact,
           fa0300_1.fa03fecha,
           DECODE (
              fa0500.fa05codorigc1, 119, 3,               /* Consulta Oncológica */
                                                                                   210,
              3, 154, 3, 216, 4,                        /* Consulta de Urgencias */
              DECODE (
                 fa0500.fa05codorigc3, 2, 2,                         /* Revisión */
                 1                                     /* El resto son Consultas */
              )
           )
                 AS tipoconsulta
         FROM fa0300 fa0300_1,
              fa0500
        WHERE fa0500.fa05codcateg = fa0300_1.fa05codcateg
          AND fa0500.fa08codgrupo = 7
          AND fa0500.fa05codorigc2 = 0
          AND fa0300_1.fa03fecha =
               (SELECT/*+ RULE */
                    MIN (fa0300.fa03fecha)
                  FROM fa0300,
                       fa0500
                 WHERE fa0300_1.fa04numfact = fa0300.fa04numfact
                   AND fa0500.fa05codcateg = fa0300.fa05codcateg
                   AND fa0500.fa08codgrupo = 7
                   AND fa0500.fa05codorigc2 = 0))
