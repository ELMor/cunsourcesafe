



   CREATE OR REPLACE VIEW fa0402j (ci21codpersona, cantidad) AS
      (SELECT ci21codpersona, SUM (fa04cantfact) cantidad
         FROM fa0400
        WHERE fa04numfacreal IS NULL
        GROUP BY ci21codpersona
       UNION
       SELECT ci21codpersona, -SUM (fa17cantidad) cantidad
         FROM fa1700
        GROUP BY ci21codpersona)
