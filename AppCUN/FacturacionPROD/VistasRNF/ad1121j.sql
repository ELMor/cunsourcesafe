

   CREATE OR REPLACE VIEW ad1121j (
      ci22nombre,
      ci22priapel,
      ci22segapel,
      ci22dni,
      ci22fecnacim,
      ci22numsegsoc,
      ci30codsexo,
      ci22nombrealf,
      ci22priapelalf,
      ci22segapelalf,
      ad01codasistenci,
      ad07codproceso,
      ad11fecinicio,
      ad02coddpto,
      ad02desdpto,
      ad12codtipoasist,
      ad12destipoasist
   ) AS
      SELECT ci2200.ci22nombre,
             ci2200.ci22priapel,
             ci2200.ci22segapel,
             ci2200.ci22dni,
             ci2200.ci22fecnacim,
             ci2200.ci22numsegsoc,
             ci2200.ci30codsexo,
             ci2200.ci22nombrealf,
             ci2200.ci22priapelalf,
             ci2200.ci22segapelalf,
             ad1100.ad01codasistenci,
             ad1100.ad07codproceso,
             ad1100.ad11fecinicio,
             ad0200.ad02coddpto,
             ad0200.ad02desdpto,
             ad1200.ad12codtipoasist,
             ad1200.ad12destipoasist
        FROM ad1100,
             ad0700,
             ci2200,
             ad0500,
             ad0200,
             ad2500,
             ad1200
       WHERE ad0700.ci21codpersona = ci2200.ci21codpersona
         AND ad0700.ad07codproceso = ad1100.ad07codproceso
         AND ad1100.ad07codproceso = ad0500.ad07codproceso
         AND ad1100.ad01codasistenci = ad0500.ad01codasistenci
         AND ad1100.ad11fecfin IS NULL
         AND ad0500.ad05fecfinrespon IS NULL
         AND ad0500.ad02coddpto = ad0200.ad02coddpto
         AND ad1100.ad01codasistenci = ad2500.ad01codasistenci
         AND ad2500.ad12codtipoasist = ad1200.ad12codtipoasist
         AND ad1100.ci32codtipecon = 'S'
         AND ad1100.ci13codentidad = 'NA'
