



   CREATE OR REPLACE VIEW fa0503j (
      fa05codcateg,
      fa05desig,
      fr73codproducto,
      fr73codintfar,
      fr73desproducto
   ) AS
      (
   /* PROTESIS */
       SELECT fa0500.fa05codcateg,
              fa0500.fa05desig,
              MAX (fr7300.fr73codproducto) AS fr73codproducto,
              fr7300.fr73codintfar,
              fr7300.fr73desproducto
         FROM fa0500,
              fr7300
        WHERE fr7300.fr73codintfar = fa0500.fa05codorigen
          AND (fr7300.fr00codgrpterap LIKE 'QI%'
		OR fr7300.fr00codgrpterap LIKE 'QTB1%'
	      )
        GROUP BY fa0500.fa05codcateg,
                 fa0500.fa05desig,
                 fr7300.fr73codintfar,
                 fr7300.fr73desproducto)
