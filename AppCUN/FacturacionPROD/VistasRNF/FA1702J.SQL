CREATE OR REPLACE VIEW FA1702J(CI21CODPERSONA,TR,CO,FECHA,DEBE,HABER) AS
SELECT
   PE       ,
   1        ,
   CF       ,
   FE       ,
   SUM(ROUND(IM))  ,
   0
FROM
   (
      SELECT /*+ INDEX(FA0400 FA0402)*/
         CI21CODPERSONA       PE,
         FA04NUMFACT          CF,
         FA04CANTFACT         IM,
         FA04FECFACTURA       FE
      FROM 
         FA0400
      WHERE
         FA04NUMFACREAL IS NULL
         AND FA04INDCOMPENSA<>2
   UNION ALL
      SELECT /*+ INDEX(FA0400 FA0402)*/
         CI21CODPERSONA       PE,
         FA0400.FA04NUMFACT   CF,
         -FA18IMPCOMP         IM,
         FA04FECFACTURA       FE
      FROM
         FA1800,FA0400
      WHERE
         FA1800.FA04CODFACT=FA0400.FA04CODFACT
         AND FA04INDCOMPENSA<>2
         AND FA04NUMFACREAL IS NULL
   UNION ALL
      SELECT /*+ INDEX(FA0400 FA0402)*/
         CI21CODPERSONA       PE,
         FA04NUMFACT          CF,
         -FA58IMPCOMP         IM,
         FA04FECFACTURA       FE
      FROM 
         FA5800,FA0400
      WHERE
         FA0400.FA04CODFACT=FA5800.FA04CODFACT_POS
         AND FA04INDCOMPENSA<>2
         AND FA04NUMFACREAL IS NULL
   )
GROUP BY PE,CF,FE
HAVING SUM(IM)<>0
UNION ALL
SELECT
   PE       ,
   2        ,
   CP       ,
   FE       ,
   0        ,
   SUM(ROUND(IM))  
FROM
   (
      SELECT
         CI21CODPERSONA    PE,
         TO_CHAR(FA17CODPAGO)       CP,
         FA17CANTIDAD      IM,
         FA17FECPAGO       FE
      FROM
         FA1700
    UNION ALL
      SELECT
         CI21CODPERSONA    PE,
         TO_CHAR(FA1700.FA17CODPAGO)       CP,
         -FA18IMPCOMP      IM,
         FA17FECPAGO       FE
      FROM
         FA1800,FA1700
      WHERE 
         FA1800.FA17CODPAGO=FA1700.FA17CODPAGO
    UNION ALL
      SELECT
         CI21CODPERSONA    PE,
         TO_CHAR(FA17CODPAGO_POS)   CP,
         -FA51IMPCOMP      IM,
         FA17FECPAGO       FE
      FROM
         FA5100,FA1700
      WHERE
         FA5100.FA17CODPAGO_POS=FA1700.FA17CODPAGO
   )
GROUP BY PE,CP,FE
HAVING SUM(IM) NOT BETWEEN -1 AND 1