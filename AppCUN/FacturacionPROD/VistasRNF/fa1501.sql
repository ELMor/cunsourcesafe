
   CREATE OR REPLACE VIEW fa1501 (
      fa15codatrib,
      fa15codconc,
      nivel,
      fa15escat,
      fa15codfin
   ) AS
      SELECT fa15codatrib, fa15codconc, LEVEL nivel, fa15escat, fa15codfin
        FROM fa1500
       WHERE SYSDATE BETWEEN fa15fecinicio AND fa15fecfin
       START WITH     fa15codfin IN (SELECT fa09codnodoconc
                                       FROM fa0900
                                      WHERE fa09indconcierto = -1)
                  AND fa15escat = 0
      CONNECT BY     fa15codconc = PRIOR fa15codconc
                 AND (
                           fa15ruta = PRIOR fa15ruta || '.' || fa15codfin || 
   '.'
                        OR fa15ruta = PRIOR fa15ruta || fa15codfin || '.'
                        OR fa15ruta =
                              SUBSTR (PRIOR fa15ruta, 1, LENGTH (PRIOR 
   fa15ruta) - 1) ||
                                 fa15codfin ||
                                 './'
                     )
