



   CREATE OR REPLACE VIEW fa0421j (
      indgarantia,
      fa04cantfact,
      fa04codfact,
      ci22numhistoria,
      ad01codasistenci,
      ad01fecinicio,
      ad01fecfin,
      ad02coddpto,
      nodo,
      fa04fecfactura
   ) AS
      (SELECT fafn02 (
                 ad0100.ci22numhistoria, ad0500.ad02coddpto, ad0100.ad01fecinicio
              ) AS indgarantia,
              fa0400.fa04cantfact,
              fa0400.fa04codfact,
              ad0100.ci22numhistoria,
              TO_CHAR (ad0100.ad01codasistenci) AS ad01codasistenci,
              ad0100.ad01fecinicio,
              ad0100.ad01fecfin,
              ad0500.ad02coddpto,
              SUBSTR (fa15ruta, 6, 3) AS nodo,
              fa0400.fa04fecfactura
         FROM fa1500,
              fa1600,
              ad0100,
              ad0500,
              fa0400
        WHERE ad0100.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad07codproceso = fa0400.ad07codproceso
          AND NOT EXISTS (SELECT fa04numfact
                            FROM fa0300,
                                 fa0500
                           WHERE fa0500.fa05codcateg = fa0300.fa05codcateg
                             AND fa0500.fa08codgrupo = 5
                             AND fa0300.fa04numfact = fa0400.fa04codfact)
          /* AND     fa0400.fa04fecfactura = to_date('30/4/2000','dd/mm/yyyy') */
          AND ad0500.ad05fecfinrespon IS NULL
          AND fa0400.fa04numfacreal IS NULL
          AND fa0400.ci32codtipecon = 'S'
          AND fa0400.ci13codentidad = 'NA'
          AND fa1600.fa04codfact = fa0400.fa04codfact
          AND fa1600.fa16numlinea = 1
          AND fa1500.fa15codatrib = fa1600.fa15codatrib)
