



   CREATE OR REPLACE VIEW fa0418j (
      ad01codasistenci,
      ad07codproceso,
      fa04numfact,
      fa04codfact,
      maxfecha,
      minfecha
   ) AS
      (SELECT/*+ RULE */
           /* VISTA PARA VER SI UN PROCESO ASISTENCIA ESTA FACTURADO ENTRE CIERTAS FECHAS */
           fa0400.ad01codasistenci,
           fa0400.ad07codproceso,
           fa0400.fa04numfact,
	   fa0400.fa04codfact,
           MAX (fa0300.fa03fecha) AS maxfecha,
           MIN (fa0300.fa03fecha) AS minfecha
         FROM fa0400,
              fa0300
        WHERE fa0300.fa04numfact = fa0400.fa04codfact
          AND fa0400.fa04numfacreal IS NULL
        GROUP BY fa0400.ad01codasistenci,
                 fa0400.ad07codproceso,
                 fa0400.fa04numfact)
