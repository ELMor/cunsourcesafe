
   CREATE OR REPLACE VIEW fa1604j (fa04codfact, fa16numlinea, fecha) AS
      (SELECT fa1600.fa04codfact, fa1600.fa16numlinea, fa0304.fecha
         FROM fa0304,
              fa1600
        WHERE fa0304.fa04codfact = fa1600.fa04codfact
          AND fa0304.fa16numlinea = fa1600.fa16numlinea
          AND UPPER (fa16descrip) IN (
                                        'HEMODIALISIS',
                                        'HEMODIÁLISIS',
                                        'RADIOTERAPIA',
                                        'REHABILITACIÓN',
                                        'SESION QUIMIOTERAPIA',
                                        'SESION DE QUIMIOTERAPIA',
                                        'SESIÓN QUIMIOTERAPIA',
                                        'SESIÓN DE QUIMIOTERAPIA'
                                     ))
