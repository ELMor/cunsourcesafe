



   CREATE OR REPLACE VIEW fa0504j (
      fa05codcateg,
      fa05desig,
      fr73codproducto,
      fr73codintfar,
      fr73desproducto,
      indhemoderivado
   ) AS
      (SELECT fa0500.fa05codcateg,
              fa0500.fa05desig,
              MAX (fr7300.fr73codproducto) AS fr73codproducto,
              fr7300.fr73codintfar,
              fr7300.fr73desproducto,
              DECODE (LPAD (fr7300.fr00codgrpterap, 4), 'B05A', 1, 0) AS 
   indhemoderivado
         FROM fa0500,
              fr7300
        WHERE fr7300.fr73codintfar = fa0500.fa05codorigen
        GROUP BY fa0500.fa05codcateg,
                 fa0500.fa05desig,
                 fr7300.fr73codintfar,
                 fr7300.fr73desproducto,
                 LPAD (fr7300.fr00codgrpterap, 4))
