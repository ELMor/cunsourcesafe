   CREATE OR REPLACE VIEW fa00062j (cuenta, pr04numactplan) AS
      (
   /* BIOPSIA */
       SELECT DISTINCT 1 AS cuenta, pr0400.pr04numactplan
         FROM pr0400
        WHERE pr01codactuacion = 3809
          AND pr37codestado BETWEEN 3 AND 5
       UNION ALL
   /* INTRA */
       SELECT DISTINCT 2 AS cuenta, pr0400.pr04numactplan
         FROM pr0400,
              prasist,
              ap2100,
              ap2500
        WHERE prasist.pr04numactplan = pr0400.pr04numactplan
          AND ap2100.ap21_codhist = prasist.nh
          AND ap2100.ap21_codcaso = prasist.nc
          AND ap2100.ap21_codsec = prasist.ns
          AND ap2500.ap21_codref = ap2100.ap21_codref
          AND ap2500.ap25_indintra = 1
       UNION ALL
   /* INMUNOFLUORESCENCIA */
       SELECT DISTINCT 4 AS cuenta, pr0400.pr04numactplan
         FROM pr0400,
              prasist,
              ap2100,
              ap3600,
              ap1200
        WHERE prasist.pr04numactplan = pr0400.pr04numactplan
          AND ap2100.ap21_codhist = prasist.nh
          AND ap2100.ap21_codcaso = prasist.nc
          AND ap2100.ap21_codsec = prasist.ns
          AND ap3600.ap21_codref = ap2100.ap21_codref
          AND ap1200.ap12_codtec = ap3600.ap12_codtec
          AND ap3600.ap36_indfact = -1
          AND ap1200.ap04_codtitec = 2
       UNION ALL
   /* MICROSCOPIA ELECTRONICA */
       SELECT DISTINCT 8 AS cuenta, pr0400.pr04numactplan
         FROM pr0400,
              prasist,
              ap2100,
              ap3600,
              ap1200
        WHERE prasist.pr04numactplan = pr0400.pr04numactplan
          AND ap2100.ap21_codhist = prasist.nh
          AND ap2100.ap21_codcaso = prasist.nc
          AND ap2100.ap21_codsec = prasist.ns
          AND ap3600.ap21_codref = ap2100.ap21_codref
          AND ap1200.ap12_codtec = ap3600.ap12_codtec
          AND ap3600.ap36_indfact = -1
          AND ap1200.ap04_codtitec = 5)
