   CREATE OR REPLACE VIEW fa0000j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT DISTINCT
   /* VISTA RNF DE INTERVENCIONES */
   /* Derechos de Quirófano */
                      t4.ad07codproceso,
                      t4.ad01codasistenci,
                      DECODE (c1.ag11codrecurso, 585, 35352, 586, 35352, 35354),
                                    /*QUIROFANOS AMBULATORIOS-RESTO DE QUIROFANOS*/
                      DECODE (
                         pr62fecini_anes, NULL,
                         GREATEST (1, FLOOR (pr62numminint / 15 + 0.5)),
                         DECODE (
                            pr62fecfin_anes, NULL,
                            GREATEST (1, FLOOR (pr62numminint / 15 + 0.5)),
                            1 +
                               FLOOR (
                                  (pr62fecfin_anes - pr62fecini_anes) * 1440 / 15
                               )
                         )
                      ),
                      NVL (t4.pr04fecfinact, t4.pr04fecadd),
                      t7.ci22numhistoria,
                      t9.ad02coddpto,
                      t9.sg02cod,
                      213,
                      NULL,
                      '',
                      DECODE (pr37codestado, 1, 0, 1),
                      DECODE (t4.pr04indintcientif,-1,-1,0)
        FROM pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             pr6200 t6,
             ci0100 c1
       WHERE t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado < 6/*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t6.pr62codhojaquir = t4.pr62codhojaquir
         AND pr62codestado > 0
         AND c1.pr04numactplan = t4.pr04numactplan
         AND c1.ci01sitcita = '1'
      UNION
   /* Derechos de Anestesia */
      SELECT t4.ad07codproceso,
             t4.ad01codasistenci,
             35353,
             1,
             NVL (t4.pr04fecfinact, t4.pr04fecadd),
             t7.ci22numhistoria,
             t9.ad02coddpto,
             t9.sg02cod,
             223,
             NULL,
             '',
             DECODE (pr37codestado, 1, 0, 1),
             DECODE (t4.pr04indintcientif,-1,-1,0)
        FROM pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             pr6200 t6
       WHERE t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado < 6/*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t6.pr62codhojaquir = t4.pr62codhojaquir
         AND pr62codestado > 0
         AND t6.sg02cod_ane IS NOT NULL
      UNION
      SELECT
   /* Vista RNF (Registro Normalizado de Facturación) de Intervenciones*/
             t4.ad07codproceso,
             t4.ad01codasistenci,
             f5.fa05codcateg,
             COUNT (*) cantidad,
             NVL (pr04fecfinact, pr04fecadd),
             t7.ci22numhistoria,
             t9.ad02coddpto,
             t9.sg02cod,
             t4.ad02coddpto,
             '0',
             '',
             DECODE (pr37codestado, 1, 0, 2, 0, 1),
             DECODE (t4.pr04indintcientif,-1,-1,0)
        FROM pr0100 t1,
             pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             fa0500 f5
       WHERE f5.fa08codgrupo = 6
         AND f5.fa05codorigen = t4.pr01codactuacion
         AND t1.pr01codactuacion = t4.pr01codactuacion
         AND t1.pr12codactividad = 207                           /*intervenciones*/
         AND t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado IN
                  (
                     1,
                     2,
                     3,
                     4,
                     5
                  )              /*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
       GROUP BY t4.ad07codproceso,
                t4.ad01codasistenci,
                f5.fa05codcateg,
                NVL (pr04fecfinact, pr04fecadd),
                t7.ci22numhistoria,
                t9.ad02coddpto,
                t9.sg02cod,
                t4.ad02coddpto,
                '0',
                '',
                DECODE (pr37codestado, 1, 0, 2, 0, 1),
                DECODE (t4.pr04indintcientif,-1,-1,0)
