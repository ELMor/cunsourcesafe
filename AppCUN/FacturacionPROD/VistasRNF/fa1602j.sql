



   CREATE OR REPLACE VIEW fa1602j (
      fa04numfact,
      fa42codreg,
      fa42descripcion,
      descrip_r,
      cantidad,
      importe
   ) AS
      SELECT fa0400.fa04numfact,
             fa42codreg,
             fa42descripcion,
             descrip_r,
             SUM (DECODE (NVL (cantidad, 0), 0, 1, cantidad)) AS cantidad,
             SUM (NVL (fa16importe, 0)) AS importe
        FROM fa1601j,
             fa0400
       WHERE fa0400.fa04codfact = fa1601j.fa04codfact
         AND fa0400.fa04numfacreal IS NULL
       GROUP BY fa0400.fa04numfact, fa42codreg, fa42descripcion, descrip_r
