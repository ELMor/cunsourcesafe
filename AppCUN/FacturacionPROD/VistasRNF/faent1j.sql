
   CREATE OR REPLACE VIEW faent1j (
      ci32codtipecon,
      ci13codentidad,
      fecha,
      dptorealiza,
      fa15precioref,
      ad12codtipoasist,
      ad07codproceso,
      ad01codasistenci
   ) AS
      SELECT          /* movimientos que no siendo de investigación su teco 
   es I */
          ad1100.ci32codtipecon,
          ad1100.ci13codentidad,
          fatmpj.fecha,
          fatmpj.dptorealiza,
          NVL (insalud.fa15precioref, 0.9 * mutuasg.fa15precioref) 
   fa15precioref,
          ad2500.ad12codtipoasist,
          ad1100.ad07codproceso,
          ad1100.ad01codasistenci
        FROM fatmpj,
             fa1500 insalud,
             fa1500 mutuasg,
             ad2500,
             ad1100
       WHERE ad1100.ci32codtipecon = 'I'
         AND ad1100.ad11fecfin IS NULL
         AND ad2500.ad01codasistenci = ad1100.ad01codasistenci
         AND ad2500.ad25fecfin IS NULL
         AND fatmpj.ad07codproceso = ad1100.ad07codproceso
         AND fatmpj.ad01codasistenci = ad1100.ad01codasistenci
         AND insalud.fa15codfin (+) = fatmpj.fa05codcateg
         AND insalud.fa15escat (+) = 1
         AND insalud.fa15codconc (+) = 126
         AND insalud.fa15ruta (+) LIKE '.126.328./.%'
         AND fatmpj.fecha >= insalud.fa15fecinicio (+)
         AND fatmpj.fecha < insalud.fa15fecfin (+)
         AND mutuasg.fa15codfin = fatmpj.fa05codcateg
         AND mutuasg.fa15escat = 1
         AND mutuasg.fa15codconc = 102
         AND mutuasg.fa15ruta LIKE '.102.328./.%'
         AND fatmpj.fecha >= mutuasg.fa15fecinicio
         AND fatmpj.fecha < mutuasg.fa15fecfin
