
   CREATE OR REPLACE VIEW fa1503j (
      fr65fecha,
      fr65cantidad,
      fr73desproducto,
      ci22numhistoria,
      paciente,
      ci32codtipecon,
      ci13codentidad,
      fa15precioref,
      FR73CODINTFAR
   ) AS
      (SELECT fr65fecha,
              fr65cantidad,
              fr73desproducto,
              ci2200.ci22numhistoria,
              ci22priapel || ' ' || ci22segapel || ', ' || ci22nombre AS 
   paciente,
              ad1100.ci32codtipecon,
              ad1100.ci13codentidad,
              (fa15precioref * fr65cantidad) AS fa15precioref,
	      FR73CODINTFAR
         FROM fr6500,
              fa0503j,
              ad0100,
              ci2200,
              ad1100,
              fa1500
        WHERE fa0503j.fr73codproducto = fr6500.fr73codproducto
          AND ad0100.ad01codasistenci = fr6500.ad01codasistenci
          AND ci2200.ci21codpersona = ad0100.ci21codpersona
          AND ad1100.ad01codasistenci = fr6500.ad01codasistenci
          AND ad1100.ad07codproceso = fr6500.ad07codproceso
          AND ad1100.ad11fecfin IS NULL
          AND fa1500.fa15escat = 1
          AND fa1500.fa15codfin = fa0503j.fa05codcateg
          AND fa1500.fa15codconc = 97
          AND fa1500.fa15ruta LIKE '.97.328.%'
          AND fr6500.fr65fecha BETWEEN fa1500.fa15fecinicio AND 
   fa1500.fa15fecfin)
