



   CREATE OR REPLACE VIEW fa0405j (
      ad01codasistenci,
      ad07codproceso,
      ad01fecinicio,
      ad01fecfin,
      ad08fecinicio,
      ad08fecfin,
      ad10codtippacien,
      ad10destippacien,
      ad12codtipoasist,
      ad12destipoasist,
      ci32codtipecon,
      ci13codentidad,
      fa04codfact,
      fa04numfact,
      ci32codtipecon_f,
      ci13codentidad_f,
      fa04cantfact,
      fa04fecfactura
   ) AS
      (SELECT/*+ RULE */
           ad0800.ad01codasistenci,
           ad0800.ad07codproceso,
           ad0100.ad01fecinicio,
           ad0100.ad01fecfin,
           ad0800.ad08fecinicio,
           ad0800.ad08fecfin,
           ad1000.ad10codtippacien,
           ad1000.ad10destippacien,
           ad1200.ad12codtipoasist,
           ad1200.ad12destipoasist,
           ad1100.ci32codtipecon,
           ad1100.ci13codentidad,
           fa0400.fa04codfact,
           fa0400.fa04numfact,
           fa0400.ci32codtipecon AS ci32codtipecon_f,
           fa0400.ci13codentidad AS ci13codentidad_f,
           fa0400.fa04cantfact,
           fa0400.fa04fecfactura
         FROM ad0800,
              ad0100,
              ad1000,
              ad1200,
              ad2500,
              ad1100,
              fa0400
        WHERE ad0800.ad01codasistenci = ad0100.ad01codasistenci
          AND ad1000.ad10codtippacien = ad0800.ad10codtippacien
          AND ad2500.ad01codasistenci = ad0100.ad01codasistenci
          AND ad2500.ad25fecfin IS NULL
          AND ad1100.ad01codasistenci = ad0800.ad01codasistenci
          AND ad1100.ad07codproceso = ad0800.ad07codproceso
          AND ad1100.ad11fecfin IS NULL
          AND ad1200.ad12codtipoasist = ad2500.ad12codtipoasist
          AND fa0400.ad01codasistenci (+) = ad0800.ad01codasistenci
          AND fa0400.ad07codproceso (+) = ad0800.ad07codproceso
          AND fa0400.fa04numfacreal IS NULL
          AND EXISTS (SELECT pr0400.ad01codasistenci, pr0400.ad07codproceso
                        FROM pr0400
                       WHERE pr0400.ad01codasistenci = ad0800.ad01codasistenci
                         AND pr0400.ad07codproceso = ad0800.ad07codproceso
                         AND pr0400.pr37codestado BETWEEN 3 AND 5)
          AND ad0100.ad01fecinicio BETWEEN TO_DATE ('1/1/2000', 'DD/MM/YYYY')
                  AND TO_DATE ('29/2/2000 23:59', 'DD/MM/YYYY HH24:MI'))

