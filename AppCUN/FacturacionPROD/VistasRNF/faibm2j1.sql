   CREATE OR REPLACE VIEW faibm2j1 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT/*+ INDEX(C R8MOV03) INDEX(B FA0502) USE_NL(B) */
          a.ad07codproceso,
          a.ad01codasistenci,
          b.fa05codcateg,
          TO_NUMBER (c.canti8),
          TO_DATE (c.freal8, 'YYYYMMDD'),
          TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
          TO_NUMBER (c.ssoli8),
          TO_CHAR (c.docto8),
          TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
          '0',
          '',
          1 ,
          to_number(NULL),
          DECODE(c.flag8,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r8mov c
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa08codgrupo = 7
         AND c.traba8 = b.fa05codorigen
