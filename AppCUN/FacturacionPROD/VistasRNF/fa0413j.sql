



   CREATE OR REPLACE VIEW fa0413 (
      ci32codtipecon,
      ci13codentidad,
      ad01codasistenci,
      ad07codproceso,
      fa04numfact,
      fa04fecfactura,
      ci21codpersona_resp
   ) AS
      (
       /* Vista que devuelve si un Proc-Asist est� facturado, y sus datos */
       SELECT ci32codtipecon,
              ci13codentidad,
              ad01codasistenci,
              ad07codproceso,
              fa04numfact,
              fa04fecfactura,
              ci21codpersona AS ci21codpersona_resp
         FROM fa0400
        WHERE fa04numfacreal IS NULL
          AND fa04indfactfinal = 1)
