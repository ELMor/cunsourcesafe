CREATE OR REPLACE VIEW AD1697J ( AD07CODPROCESO, AD01CODASISTENCI, PR04FECINIACT, AD02CODDPTO ) AS 
      SELECT /*Intervenciones*/
             pr0400.ad07codproceso, 
             pr0400.ad01codasistenci, 
             NVL (pr04feciniact, SYSDATE) pr04feciniact, 
             ad02coddpto 
        FROM pr0400, 
             pr0100 
       WHERE pr0100.pr01codactuacion = pr0400.pr01codactuacion 
         AND (
               ( pr0100.pr12codactividad = 207 )
                OR /*Algunas pruebas de Urologia se 
                     consideran intervenciones para Lagun-Aro*/
               (EXISTS (SELECT ci32codtipecon  
                          FROM ad1100 z1  
                         WHERE z1.ad07codproceso =   pr0400.ad07codproceso  
                           AND z1.ad01codasistenci = pr0400.ad01codasistenci  
                           AND z1.ci32codtipecon = 'M' 
                           AND z1.ci13codentidad ='LA'
                       )
                AND pr0100.pr01codactuacion in
                (5385 ,5386 ,5434 ,5437 ,5439 ,5440 ,5441 ,5442 ,5443 ,5444 ,5445 ,5446 ,5447 
                ,5448 ,5449 ,5450 ,5451 ,5452 ,5453 ,5454 ,5456 ,5464 ,7104 ,7115 ,7116 ,7197)

               )
     UNION ALL 
     SELECT  ad07codproceso, 
      ad01codasistenci, 
             fecha as pr04feciniact, 
      dptorealiza as ad02coddpto 
     FROM    FAIBM1J1

