   CREATE OR REPLACE VIEW faibm2j2 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci,
             b.fa05codcateg,
             TO_NUMBER (c.canti8),
             TO_DATE (c.freal8, 'YYYYMMDD'),
             TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
             TO_NUMBER (c.ssoli8),
             TO_CHAR (c.docto8),
             TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
             '0',
             '',
             1 ,
             to_number(null),
             DECODE(c.flag8,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r8mov c,
             pr0200 d,
             pr0100 e
       WHERE c.numcaso = a.ad08numcaso
         AND d.gc01fctreg_ibs = 7
         AND d.gc01fpclav_ibs = c.traba8
         AND d.pr01codactuacion = b.fa05codorigen
         AND b.fa08codgrupo = 7
         AND d.pr01codactuacion = e.pr01codactuacion
         AND e.pr12codactividad <> 201/* CONSULTAS SE DEVUELVEN EN LA FAIBM2J1*/
         AND (
                   (
                          b.fa08codgrupo = 7
                      AND e.pr01codactuacion >=
                                            2000/*NO DEVOLVER NADA DE LABORATORIO*/
                   )
                OR (b.fa08codgrupo = 9)                   /*NI DE BANCO DE SANGRE*/
             )
         AND NOT EXISTS (SELECT fa05codcateg
                           FROM fa2600
                          WHERE fa2600.fa05codcateg = b.fa05codcateg)
      UNION ALL
      SELECT                                               /*TACS Y RM MULTIPLES */
          a.ad07codproceso,
          a.ad01codasistenci,
          g.fa05codcateg,
          1,
          TO_DATE (c.freal8, 'YYYYMMDD'),
          TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
          MIN (TO_NUMBER (c.ssoli8)),
          MIN (TO_CHAR (c.docto8)),
          TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
          '0',
          MIN (fafn01 (
                  a.ad07codproceso, a.ad01codasistenci, TO_DATE (
                                                           c.freal8 || c.hreal8,
                                                           'YYYYMMDDHH24MI'
                                                        ), g.fa05codcateg
               )),
          1 ,
          to_number(null),
          DECODE(c.flag8,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r8mov c,
             pr0200 d,
             pr0100 e,
             fa2600 f,
             fa2800 g
       WHERE c.numcaso = a.ad08numcaso
         AND d.gc01fctreg_ibs = 7
         AND d.gc01fpclav_ibs = c.traba8
         AND d.pr01codactuacion = b.fa05codorigen
         AND b.fa08codgrupo = 7
         AND d.pr01codactuacion = e.pr01codactuacion
         AND b.fa05codcateg = f.fa05codcateg
         AND f.fa27codgrupofac = g.fa27codgrupofac
       GROUP BY a.ad07codproceso,
                a.ad01codasistenci,
                g.fa05codcateg,
                TO_DATE (c.freal8, 'YYYYMMDD'),
                FLOOR (TO_NUMBER (SUBSTR (hreal8, 1, 2)) / 14),
                TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
                TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
                g.fa28cantdesde,
                g.fa28canthasta,
                DECODE(c.flag8,'0A',-1,0)
      HAVING COUNT (b.fa05codcateg) BETWEEN g.fa28cantdesde AND g.fa28canthasta
