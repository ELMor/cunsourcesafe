   CREATE OR REPLACE VIEW faibmj (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             ic               
        FROM faibm1j1
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             ic               
        FROM faibm1j2
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             ic               
        FROM faibm1j3
