



   CREATE OR REPLACE VIEW fa4602j (
      fa25numsegsoc,
      ad01fecinicio,
      tipoasistencia,
      fa25apel1,
      fa25apel2,
      fa25nomb,
      fa25idcliente,
      fa25fechnac,
      fa25sexo,
      fa25codpostal,
      fechaconsulta,
      tipoconsulta,
      ad02coddpto,
      fa04cantfact,
      numreg,
      fecproceso,
      fa25expinf
   ) AS
      (
   /* VISTA PARA OSASUNBIDEA DE PACIENTES AMBULATORIOS */
       SELECT/*+ RULE */
           RPAD (fa2500.fa25numsegsoc, 10) AS fa25numsegsoc,
           TO_CHAR (ad0100.ad01fecinicio, 'YYYYMMDD') AS ad01fecinicio,
           'AM' AS tipoasistencia,
           RPAD (fa2500.fa25apel1, 18) AS fa25apel1,
           RPAD (fa2500.fa25apel2, 18) AS fa25apel2,
           RPAD (fa2500.fa25nomb, 14) AS fa25nomb,
           RPAD (fa2500.fa25idcliente, 23) AS fa25idcliente,
           RPAD (TO_CHAR (fa2500.fa25fechnac, 'DDMMYYY'), 7) AS fa25fechnac,
           RPAD (fa2500.fa25sexo, 1) AS fa25sexo,
           RPAD (fa2500.fa25codpostal, 5) AS fa25codpostal,
           RPAD (TO_CHAR (fa0305.fa03fecha, 'DDMMYYY'), 7) AS fechaconsulta,
           NVL (fa0305.tipoconsulta, 1) AS tipoconsulta,
           RPAD (ad0500.ad02coddpto, 3) AS ad02coddpto,
           LPAD (fa0400.fa04cantfact, 10) AS fa04cantfact,
           LPAD (ROWNUM, 4, '0') AS numreg,
           '200001' AS fecproceso,
           RPAD (fa2500.fa25expinf, 14) AS fa25expinf
         FROM fa0305,
              ad0100,
              ad0500,
              fa2500,
              fa0400
        WHERE ad0100.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad07codproceso = fa0400.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND fa2500.ad01codasistenci = fa0400.ad01codasistenci
          AND fa2500.ad07codproceso = fa0400.ad07codproceso
          AND fa0305.fa04codfact = fa0400.fa04codfact
          AND fa0400.fa04codfact IN
              (SELECT/*+ RULE */
                   fa04codfact
                 FROM ad2500,
                      fa0400
                WHERE ad2500.ad01codasistenci = fa0400.ad01codasistenci
                  AND fa0400.fa04fecfactura BETWEEN ad2500.ad25fecinicio
                          AND NVL (ad2500.ad25fecfin, SYSDATE)
                  AND fa0400.fa04numfacreal IS NULL
                  AND ad2500.ad12codtipoasist = 2
                  AND fa0400.ci32codtipecon = 'S'
                  AND fa0400.ci13codentidad = 'NA'
                  AND fa0400.fa04fecfactura = TO_DATE ('31/1/2000', 
   'DD/MM/YYYY'))
       UNION ALL
       SELECT/*+ RULE */ RPAD (fa2500.fa25numsegsoc, 10) AS fa25numsegsoc,
                         TO_CHAR (ad0100.ad01fecinicio, 'YYYYMMDD') AS 
   ad01fecinicio,
                         'AM' AS tipoasistencia,
                         RPAD (fa2500.fa25apel1, 18) AS fa25apel1,
                         RPAD (fa2500.fa25apel2, 18) AS fa25apel2,
                         RPAD (fa2500.fa25nomb, 14) AS fa25nomb,
                         RPAD (fa2500.fa25idcliente, 23) AS fa25idcliente,
                         RPAD (TO_CHAR (fa2500.fa25fechnac, 'DDMMYYY'), 7) AS
   fa25fechnac,
                         RPAD (fa2500.fa25sexo, 1) AS fa25sexo,
                         RPAD (fa2500.fa25codpostal, 5) AS fa25codpostal,
                         RPAD (TO_CHAR (ad0100.ad01fecinicio, 'DDMMYYY'), 7) 
   AS fechaconsulta,
                         1 AS tipoconsulta,
                         RPAD (ad0500.ad02coddpto, 3) AS ad02coddpto,
                         LPAD (fa0400.fa04cantfact, 10) AS fa04cantfact,
                         LPAD (ROWNUM, 4, '0') AS numreg,
                         '200001' AS fecproceso,
                         RPAD (fa2500.fa25expinf, 14) AS fa25expinf
         FROM ad0100,
              ad0500,
              fa2500,
              fa0400
        WHERE ad0100.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad07codproceso = fa0400.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND fa2500.ad01codasistenci = fa0400.ad01codasistenci
          AND fa2500.ad07codproceso = fa0400.ad07codproceso
          AND NOT EXISTS (SELECT/*+ RULE */
                              fa03numrnf
                            FROM fa0500,
                                 fa0300
                           WHERE fa0300.fa04numfact = fa0400.fa04codfact
                             AND fa0500.fa05codcateg = fa0300.fa05codcateg
                             AND fa0500.fa08codgrupo = 7
                             AND fa0500.fa05codorigc2 = 0)
          AND fa0400.fa04codfact IN
              (SELECT/*+ RULE */
                   fa04codfact
                 FROM ad2500,
                      fa0400
                WHERE ad2500.ad01codasistenci = fa0400.ad01codasistenci
                  AND fa0400.fa04fecfactura BETWEEN ad2500.ad25fecinicio
                          AND NVL (ad2500.ad25fecfin, SYSDATE)
                  AND fa0400.fa04numfacreal IS NULL
                  AND ad2500.ad12codtipoasist = 2
                  AND fa0400.ci32codtipecon = 'S'
                  AND fa0400.ci13codentidad = 'NA'
                  AND fa0400.fa04fecfactura = TO_DATE ('31/1/2000', 
   'DD/MM/YYYY')))
