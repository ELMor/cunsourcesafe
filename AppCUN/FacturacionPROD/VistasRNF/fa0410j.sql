



   CREATE OR REPLACE VIEW fa0410j (fa48coddiskacunsa, fa2rf) AS
      SELECT fa4800.fa48coddiskacunsa,
             'F' ||                                                        -- 'F'  
                    SUBSTR (fa0400.fa04numfact, 4) fa2rf     -- Numero de Factura  
        FROM fa0400,
             fa4800,
             fa4900
       WHERE fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa0400.fa04numfact = fa4900.fa04numfact
         AND fa04numfacreal IS NULL
         AND fa04cantfact <> 0
