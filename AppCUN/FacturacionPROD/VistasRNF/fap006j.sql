
   CREATE OR REPLACE VIEW fap006j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado
   ) AS
      (SELECT pr0400.ad07codproceso,
              pr0400.ad01codasistenci,
              fa0500.fa05codcateg,
              COUNT (*) AS cantidad,
              NVL (pr0400.pr04feciniact, pr04fecadd) AS fecha,
              ci2200.ci22numhistoria,
              pr0900.ad02coddpto AS dptosolicita,
              pr0900.sg02cod AS drsolicita,
              204 AS dptorealiza,
              NULL AS drrealiza,
              '' AS obs,
              1 AS estado
         FROM pr0400,
              fa9000,
              fa0500,
              ci2200,
              pr0300,
              pr0900
        WHERE fa9000.fa90cod =
               (SELECT MIN (fa9000.fa90cod)
                  FROM fa00061j,
                       fa9000
                 WHERE (
                             fa9000.fa90intra = fa00061j.intra
                          OR fa9000.fa90intra IS NULL
                       )
                   AND (
                             fa9000.fa90inmunofl = 
   fa00061j.inmunoflorescencia
                          OR fa9000.fa90inmunofl IS NULL
                       )
                   AND (
                             fa9000.fa90micel = fa00061j.microelect
                          OR fa9000.fa90micel IS NULL
                       )
                   AND fa00061j.pr04numactplan = pr0400.pr04numactplan)
          AND fa0500.fa05codorigen = fa9000.fa90codigo
          AND ci2200.ci21codpersona = pr0400.ci21codpersona
          AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
          AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
        GROUP BY pr0400.ad07codproceso,
                 pr0400.ad01codasistenci,
                 fa0500.fa05codcateg,
                 NVL (pr0400.pr04feciniact, pr04fecadd),
                 ci2200.ci22numhistoria,
                 pr0900.ad02coddpto,
                 pr0900.sg02cod)
