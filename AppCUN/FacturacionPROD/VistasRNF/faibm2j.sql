   CREATE OR REPLACE VIEW faibm2j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT 	/*+ rule */
		/*Pruebas del IBM pendientes de facturar*/
             ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM faibm2j1
      UNION ALL
      SELECT 	/*+ rule */
		ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM faibm2j2
