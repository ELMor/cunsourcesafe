
   CREATE OR REPLACE VIEW fa1605j (
      fa04numfact,
      fa16numlinea,
      importemedicacion,
      importehemoderivados,
      importetotal
   ) AS
      (SELECT fa0300.fa04numfact,
              fa0300.fa16numlinea,
              SUM (DECODE (fa0504j.indhemoderivado, 1, 0, 1) * fa03preciofact
   *
                      fa03cantfact) AS importemedicacion,
              SUM (fa0504j.indhemoderivado * fa03preciofact * fa03cantfact) 
   AS importehemoderivados,
              SUM (fa03preciofact * fa03cantfact) AS importetotal
         FROM fa0504j,
              fa0300
        WHERE fa0504j.fa05codcateg = fa0300.fa05codcateg
        GROUP BY fa0300.fa04numfact, fa0300.fa16numlinea)
