



   CREATE OR REPLACE VIEW fa0417j (
      ci22numhistoria,
      ad01codasistenci,
      ad07codproceso,
      fa04codfact,
      maxfecha
   ) AS
      (SELECT/*+ RULE */
           /* VISTA PARA VER REINGRESOS DE OSASUNBIDEA */
           ad0100.ci22numhistoria,
           fa0400.ad01codasistenci,
           fa0400.ad07codproceso,
           fa0400.fa04codfact,
           MAX (fa0300.fa03fecha) AS maxfecha
         FROM fa0500,
              fa0400,
              fa0300,
              ad0100
        WHERE ad0100.ad01codasistenci = fa0300.ad01codasistenci
          AND fa0300.fa04numfact = fa0400.fa04codfact
          AND fa0500.fa05codcateg = fa0300.fa05codcateg
          AND fa0500.fa08codgrupo = 5
          AND UPPER (fa0400.ci32codtipecon) = 'S'
          AND UPPER (fa0400.ci13codentidad) = 'NA'
          AND fa0400.fa04numfacreal IS NULL
        GROUP BY ad0100.ci22numhistoria,
                 fa0400.ad01codasistenci,
                 fa0400.ad07codproceso,
                 fa0400.fa04codfact)
