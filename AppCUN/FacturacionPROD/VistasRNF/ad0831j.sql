

   CREATE OR REPLACE VIEW ad0831j (ad07codproceso, ad01codasistenci) AS
      (
   /* Vista que dice informa si un proceso-asistencia tiene pruebas solicitadas o realizadas */
       SELECT ad0800.ad07codproceso, ad0800.ad01codasistenci
         FROM ad0800
        WHERE EXISTS (SELECT pr0400.pr04numactplan
                        FROM pr0400
                       WHERE pr0400.ad07codproceso = ad0800.ad07codproceso
                         AND pr0400.ad01codasistenci = ad0800.ad01codasistenci
                         AND pr37codestado BETWEEN 1 AND 5))
