   CREATE OR REPLACE VIEW fa0002j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT /*+ RULE */
   /* Vista RNF (Registro Normalizado de Facturación) de Pruebas (No consultas)*/
             t4.ad07codproceso,
             t4.ad01codasistenci,
             f5.fa05codcateg,
             SUM (NVL (pr04cantidad, 1)),
             NVL (pr04feciniact, pr04fecadd),
             t7.ci22numhistoria,
             t9.ad02coddpto,
             t9.sg02cod,
             t4.ad02coddpto,
             '0',
             DECODE (t4.pr01codactuacion, 3865, ad08observac, ''),
                                     /* Nombre del paciente si se venden isotopos*/
             DECODE (pr37codestado, 1, 0, 2, 0, 1),
             to_number(null),
             decode(pr04indintcientif,-1,-1,0)
        FROM pr0100 t1,
             pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             fa0500 f5,
             ad0800 a8
       WHERE f5.fa08codgrupo = 7
         AND f5.fa05codorigen = TO_CHAR(t4.pr01codactuacion)
         AND t1.pr01codactuacion = t4.pr01codactuacion
         AND t1.pr12codactividad <> 201                     /* EXCEPTO CONSULTAS */
         AND t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado < 6/*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t1.pr01codactuacion >= 2000        /*NO DEVOLVER NADA DE LABORATORIO*/
   /*NO DEVOLVER TACS Y RM, SE DEVUELVEN A CONTINUACION*/
         AND f5.fa05codcateg NOT IN (SELECT fa05codcateg
                                       FROM fa2600)
   /* BIOPSIAS POR VISTA FA0006J*/
         AND (   t1.pr01codactuacion <> 3809
              OR pr37codestado < 3)
         AND a8.ad01codasistenci = t4.ad01codasistenci
         AND a8.ad07codproceso = t4.ad07codproceso
       GROUP BY t4.ad07codproceso,
                t4.ad01codasistenci,
                f5.fa05codcateg,
                NVL (pr04feciniact, pr04fecadd),
                t7.ci22numhistoria,
                t9.ad02coddpto,
                t9.sg02cod,
                t4.ad02coddpto,
                '0',
                DECODE (t4.pr01codactuacion, 3865, ad08observac, ''),
                DECODE (pr37codestado, 1, 0, 2, 0, 1),
                decode(pr04indintcientif,-1,-1,0)
      UNION ALL
      SELECT	/*+ RULE */
                                      /* VISTA DE TACS Y RM MULTIPLES*/
          pr0400.ad07codproceso,
          pr0400.ad01codasistenci,
          fa2800.fa05codcateg,
          1 cantidad,
          TO_DATE (
             TO_CHAR (NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'DD/MM/YYYY'),
             'DD/MM/YYYY'
          ),
          ad0700.ci22numhistoria,
          MIN (pr0900.ad02coddpto),
          MIN (pr0900.sg02cod),
          pr0400.ad02coddpto,
          NULL,
          MIN (fafn01 (
                  pr0400.ad07codproceso, pr0400.ad01codasistenci,
                  NVL (pr0400.pr04fecfinact, pr0400.pr04fecadd),
                  fa2800.fa05codcateg
               )),
          DECODE (pr37codestado, 1, 0, 2, 0, 1),
          to_number(null),
          decode(pr04indintcientif,-1,-1,0)
        FROM pr0400,
             ad0700,
             pr0900,
             pr0300,
             fa2800,
             fa2600,
             fa0500
       WHERE fa0500.fa08codgrupo = 7
         AND fa0500.fa05codorigen = TO_CHAR(pr0400.pr01codactuacion)
         AND pr0400.pr01codactuacion = pr0400.pr01codactuacion
         AND ad0700.ad07codproceso = pr0400.ad07codproceso
         AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
         AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
         AND pr0400.pr37codestado IN (1, 2, 3, 4, 5)
         AND fa2600.fa05codcateg = fa0500.fa05codcateg
         AND fa2800.fa27codgrupofac = fa2600.fa27codgrupofac
       GROUP BY pr0400.ad07codproceso,
                pr0400.ad01codasistenci,
                fa2800.fa05codcateg,
                TO_DATE (
                   TO_CHAR (
                      NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'DD/MM/YYYY'
                   ), 'DD/MM/YYYY'
                ),
                FLOOR (
                   TO_NUMBER (
                      TO_CHAR (
                         NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'HH24'
                      )
                   ) /
                      14
                ),
                ad0700.ci22numhistoria,
                pr0400.ad02coddpto,
                fa2800.fa28cantdesde,
                fa2800.fa28canthasta,
                DECODE (pr37codestado, 1, 0, 2, 0, 1),
                decode(pr04indintcientif,-1,-1,0)
      HAVING COUNT (fa0500.fa05codcateg) BETWEEN fa2800.fa28cantdesde
                 AND fa2800.fa28canthasta
      UNION ALL
      SELECT pr0400.ad07codproceso,         /* PEROXIDASAS DE ANATOMIA PATOLOGICA*/
             pr0400.ad01codasistenci,
             fa2800.fa05codcateg AS fa05codcateg,
             1 AS cantidad,
             MIN (NVL (pr0400.pr04feciniact, pr04fecadd)) AS fecha,
             ad0100.ci22numhistoria,
             pr0900.ad02coddpto AS dptosolicita,
             pr0900.sg02cod AS drsolicita,
             204 AS dptorealiza,
             NULL AS drrealiza,
             '' AS obs,
             1 AS estado,
             to_number(null),
             decode(pr04indintcientif,-1,-1,0)
        FROM ap3600,
             ap1200,
             ap2100,
             prasist pa,
             pr0400,
             ad0100,
             pr0300,
             pr0900,
             fa2800,
             fa2600
       WHERE ap1200.ap12_codtec = ap3600.ap12_codtec
         AND ap2100.ap21_codref = ap3600.ap21_codref
         AND pa.nh = ap2100.ap21_codhist
         AND pa.nc = ap2100.ap21_codcaso
         AND pa.ns = ap2100.ap21_codsec
         AND pr0400.pr04numactplan = pa.pr04numactplan
         AND ad0100.ad01codasistenci = pr0400.ad01codasistenci
         AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
         AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
         AND fa2600.fa05codcateg = 12673
         AND fa2800.fa27codgrupofac = fa2600.fa27codgrupofac
         AND ap1200.ap04_codtitec = 4
         AND ap3600.ap36_indfact = -1
       GROUP BY pr0400.ad07codproceso,
                pr0400.ad01codasistenci,
                fa2800.fa05codcateg,
                ad0100.ci22numhistoria,
                pr0900.ad02coddpto,
                pr0900.sg02cod,
                fa2800.fa28cantdesde,
                fa2800.fa28canthasta,
                decode(pr04indintcientif,-1,-1,0)
      HAVING COUNT (*) BETWEEN fa2800.fa28cantdesde AND fa2800.fa28canthasta
      UNION ALL
      SELECT pr0400.ad07codproceso,               /* CITOMETRIAS Categoría 12676 */
             pr0400.ad01codasistenci,
             12676,
             COUNT (*) AS cantidad,
             TO_DATE (
                TO_CHAR (NVL (pr0400.pr04feciniact, pr04fecadd), 'DD/MM/YYYY'),
                'DD/MM/YYYY'
             ) AS fecha,
             ad0100.ci22numhistoria,
             pr0900.ad02coddpto AS dptosolicita,
             pr0900.sg02cod AS drsolicita,
             204 AS dptorealiza,
             NULL AS drrealiza,
             '' AS obs,
             1 AS estado,
             to_number(null),
             decode(pr04indintcientif,-1,-1,0)
        FROM ap3600,
             ap1200,
             ap2100,
             prasist pa,
             pr0400,
             ad0100,
             pr0300,
             pr0900
       WHERE ap1200.ap12_codtec = ap3600.ap12_codtec
         AND ap2100.ap21_codref = ap3600.ap21_codref
         AND pa.nh = ap2100.ap21_codhist
         AND pa.nc = ap2100.ap21_codcaso
         AND pa.ns = ap2100.ap21_codsec
         AND pr0400.pr04numactplan = pa.pr04numactplan
         AND ad0100.ad01codasistenci = pr0400.ad01codasistenci
         AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
         AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
         AND ap1200.ap04_codtitec = 7
         AND ap3600.ap36_indfact = -1
       GROUP BY pr0400.ad07codproceso,
                pr0400.ad01codasistenci,
                TO_DATE (
                   TO_CHAR (NVL (pr0400.pr04feciniact, pr04fecadd), 'DD/MM/YYYY'),
                   'DD/MM/YYYY'
                ),
                ad0100.ci22numhistoria,
                pr0900.ad02coddpto,
                pr0900.sg02cod,
                decode(pr04indintcientif,-1,-1,0)
