



   CREATE OR REPLACE VIEW fa0412j (fa48coddiskacunsa, fa3r1) AS
      SELECT fa4800.fa48coddiskacunsa,
             LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                  -- Numero de Factura: Number(7)  
                LPAD (fa16numlinea, 3, '0') ||      -- Numero de Linea: Number(3)  
                RPAD (fa16descrip, 50, ' ') || -- Texto de la linea: Varchar(150)  
                LPAD (fa16importe, 10, '0')
                   fa3r1                       -- Importe de la linea: Number(10)  
        FROM fa0400,
             fa1600,
             fa4800,
             fa4900
       WHERE fa0400.fa04codfact = fa1600.fa04codfact
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
         AND fa04cantfact <> 0
