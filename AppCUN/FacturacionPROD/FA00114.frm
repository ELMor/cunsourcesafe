VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmSeleccion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccione las historias a facturar"
   ClientHeight    =   3315
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7785
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3315
   ScaleWidth      =   7785
   StartUpPosition =   1  'CenterOwner
   Begin SSCalendarWidgets_A.SSDateCombo SDCInicio 
      Height          =   330
      Left            =   6525
      TabIndex        =   4
      Top             =   1125
      Width           =   1230
      _Version        =   65537
      _ExtentX        =   2170
      _ExtentY        =   582
      _StockProps     =   93
      DefaultDate     =   "01/01/1999"
   End
   Begin VB.TextBox txtConcierto 
      Height          =   285
      Left            =   7245
      TabIndex        =   3
      Text            =   "97"
      Top             =   765
      Width           =   375
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   510
      Left            =   6660
      TabIndex        =   2
      Top             =   2745
      Width           =   1005
   End
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "&Facturar"
      Height          =   510
      Left            =   6660
      TabIndex        =   1
      Top             =   135
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid grdHistoras 
      Height          =   3150
      Left            =   225
      TabIndex        =   0
      Top             =   90
      Width           =   6270
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      HeadLines       =   0
      Col.Count       =   5
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      RowNavigation   =   1
      MaxSelectedRows =   10
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   2328
      Columns(0).Caption=   "Historia"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7752
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Paciente"
      Columns(2).Name =   "Direccion"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "FecNacim"
      Columns(3).Name =   "FecNacim"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "FecIngreso"
      Columns(4).Name =   "FecIngreso"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   11060
      _ExtentY        =   5556
      _StockProps     =   79
      Caption         =   "Historias a facturar"
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFin 
      Height          =   330
      Left            =   6525
      TabIndex        =   5
      Top             =   1665
      Width           =   1230
      _Version        =   65537
      _ExtentX        =   2170
      _ExtentY        =   582
      _StockProps     =   93
      DefaultDate     =   "01/01/1999"
      MinDate         =   "1999/1/1"
      Format          =   "DD/MM/YYYY"
      Mask            =   2
   End
End
Attribute VB_Name = "frmSeleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type typeRNF
  Asistencia As Long
  CodCateg As String
  CodRNF As String
  Cantidad As Long
  Fecha As String
End Type
Dim MatrizRNF() As typeRNF
Dim CadenaConceptos As String

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Dim NumHisto As Long

Public Function pRNF(objFactura As Factura, Historia As Long) As Factura
Dim MiSqL
    If Historia = 0 Then
      NumHisto = 0
    Else
      NumHisto = Historia
    End If
    Set varFact = objFactura
    Load frmSeleccion
      'Set frmSeleccion.varFact = objFactura
      If NumHisto = 0 Then
        frmSeleccion.Show (vbModal)
      End If
    Set objFactura = varFact
    Unload frmSeleccion
End Function
Function FilaDelGrid(NumHistoria As String) As Integer
Dim Cont As Integer

  grdHistoras.MoveFirst
  FilaDelGrid = 0
  For Cont = 1 To 10
    If grdHistoras.Columns(0).Text = NumHistoria Then
      FilaDelGrid = Cont
      Exit Function
    End If
    grdHistoras.MoveNext
  Next
  
End Function


Private Sub cmdFacturar_Click()
Dim x As Integer
Dim Cadena As String
Dim MiSqL As String
Dim RsAsistencias As rdoResultset
Dim EnBlanco As Boolean
  
  Me.cmdSalir.Enabled = False
  'Comprobamos que al menos una de las casillas tenga valor
  EnBlanco = True
  Me.grdHistoras.MoveFirst
  For x = 1 To 10
    If grdHistoras.Columns(0).Text <> "" Then
      EnBlanco = False
      Exit For
    Else
      grdHistoras.MoveNext
    End If
  Next
  If EnBlanco Then
    MsgBox "Se debe introducir al menos una historia", vbExclamation, "Atenci�n"
    If grdHistoras.Enabled = True Then grdHistoras.SetFocus
    Me.cmdSalir.Enabled = True
    Exit Sub
  End If
  
  'Comprobamos que todas las historias sean num�ricas
  grdHistoras.MoveFirst
  For x = 1 To 10
    If grdHistoras.Columns(0).Text <> "" Then
      If Not IsNumeric(grdHistoras.Columns(0).Text) Then
        MsgBox "El n�mero de historia es un dato num�rico", vbExclamation, "Atenci�n"
        If grdHistoras.Enabled = True Then grdHistoras.SetFocus
        Me.cmdSalir.Enabled = True
        Exit Sub
      End If
    End If
    grdHistoras.MoveNext
  Next
  
  'Una vez comprobado lo anterior llenamos las colecciones correspondientes a pacientes y asistencias
    
  Me.grdHistoras.MoveFirst
  Cadena = ""
  For x = 1 To 10
    If grdHistoras.Columns(0).Text <> "" Then
      Cadena = Cadena & grdHistoras.Columns(0).Text & ","
    End If
    grdHistoras.MoveNext
  Next
  Cadena = Left(Cadena, Len(Cadena) - 1)
  'MiSql = "SELECT AD01CODASISTENCI From AD0100 WHERE CI22NUMHISTORIA IN (" & Cadena & ")"
'  MsgBox MiSql
  '  MsgBox CadenaConceptos
  MiSqL = GenerarSqlRNF(Cadena)
'  MsgBox MiSql
  Call LlenarTypeRNF(Cadena)
  'varfact.AsignarRNFs
  Unload Me
End Sub


Private Sub cmdSalir_Click()
  BotonSalir = True
  Me.Hide
End Sub


Private Sub Form_Load()
Dim x As Integer

  For x = 1 To 10
    Me.grdHistoras.AddItem Space(0)
  Next
  Me.grdHistoras.MoveFirst
  If NumHisto <> 0 Then
    Me.grdHistoras.Columns(0).Text = NumHisto
    Me.grdHistoras.MoveNext
    Call cmdFacturar_Click
  End If
End Sub


Function GenerarSqlRNF(ConcAgrup As String) As String
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Cadena As String
  
  MiSqL = "Delete from FA0300 where Ci22numhistoria in (" & ConcAgrup & ")"
  objApp.rdoConnect.Execute MiSqL
  Cadena = ""
  'Seleccionamos todas las vistas de los grupos que se encuentren activos
  MiSqL = "SELECT FA08DESVISTA FROM FA0800 WHERE FA11CODESTGRUPO = 1"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      'Creamos la SQL de selecci�n de los RNF
      MiSqL = "INSERT INTO FA0300 (FA03NUMRNF,FA05CODCATEG,FA03CANTIDAD,FA13CODESTRNF," & _
                  "AD01CODASISTENCI,FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO) SELECT FA03NUMRNF_SEQUENCE.NEXTVAL, " & _
                  "FA05CODCATEG, CANTIDAD,1, AD01CODASISTENCI,TO_DATE(FECHA,'DD/MM/YYYY'), " & _
                  "CI22NUMHISTORIA, OBS, AD07CODPROCESO FROM " & MiRs(0) & " WHERE CI22NUMHISTORIA IN (" & ConcAgrup & ") "
      objApp.rdoConnect.Execute MiSqL
      MiRs.MoveNext
    Wend
  End If
 GenerarSqlRNF = "A"
  
End Function


Sub LlenarTypeRNF(SqlRNF As String)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim MiSqLConc As String
Dim MiRsConc As rdoResultset
Dim Cont As Long
Dim NumRNF As Long
Dim ContRNF As Long
Dim Fila As Integer
Dim RNFAnterior As String
Dim AsistAnterior As String
Dim ProcAnterior As String
Dim PacienteAnterior As String
Dim FechaInicio As String
Dim FechaFinal As String
Dim Fecha As String

  On Error GoTo ErrorenRNF
  
  RNFAnterior = ""
  AsistAnterior = ""
  PacienteAnterior = ""
  ProcAnterior = ""
  ContRNF = 0
  'Si la cadena de selecci�n de RNF no es nula.
  If Trim(SqlRNF) <> "" Then
     MiSqL = "SELECT * FROM FA0300 WHERE CI22NUMHISTORIA IN (" & SqlRNF & ") " & _
                " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
  
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      ReDim MatrizRNF(1 To MiRs.RowCount)
      Cont = 1
      ContRNF = 0
      objApp.BeginTrans
        While Not MiRs.EOF
          If MiRs("CI22NUMHISTORIA") <> PacienteAnterior Then
            'Se ha detectado un nuevo n�mero de historia, creamos el paciente.
            Set paciente = New paciente
            paciente.Codigo = MiRs("CI22NUMHISTORIA")
            Fila = FilaDelGrid(MiRs("CI22NUMHISTORIA"))
            If Fila <> 0 Then
              grdHistoras.Row = Fila - 1
              paciente.Name = grdHistoras.Columns(1).Text
              paciente.Direccion = grdHistoras.Columns(2).Text
              paciente.FecNacimiento = grdHistoras.Columns(3).Text
            End If
            varFact.Pacientes.Add paciente, MiRs("CI22NUMHISTORIA") & ""
            PacienteAnterior = MiRs("CI22NUMHISTORIA")
          End If
          If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
            'Se ha detectado un nuevo n�menro de asistencia, creamos la asistencia.
            Set Asistencia = New Asistencia
            'Esto es provisional, habr� que borrarlo despu�s
'            Asistencia.AddConcierto 97
            MiSqLConc = " SELECT FA09CODNODOCONC FROM AD1100,CI1300 " & _
                                  " WHERE AD1100.CI32CODTIPECON=CI1300.CI32CODTIPECON " & _
                                  " AND AD1100.CI13CODENTIDAD=CI1300.CI13CODENTIDAD" & _
                                  " AND AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & _
                                  " AND AD07CODPROCESO = " & MiRs("AD07CODPROCESO")
            Set MiRsConc = objApp.rdoConnect.OpenResultset(MiSqLConc, 3)
            If Not MiRsConc.EOF Then
              If Trim(MiRsConc(0)) <> "" Then
                If IsNumeric(MiRsConc(0)) Then
                  Asistencia.AddConcierto MiRsConc(0)
                End If
              End If
            Else
              MsgBox "Ante la no existencia de concierto se aplica el de Privado A", vbOKOnly + vbInformation, "Aviso"
              Asistencia.AddConcierto 97
            End If
            Asistencia.Codigo = MiRs("AD07CODPROCESO") & "-" & MiRs("AD01CODASISTENCI")
            Asistencia.FecInicio = "01/01/1999 00:00:00"
            Asistencia.FecFin = "31/12/1999 00:00:00"
            Asistencia.Facturado = False
            paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
            AsistAnterior = MiRs("AD01CODASISTENCI")
            ProcAnterior = MiRs("AD07CODPROCESO")
            FechaInicio = "01/01/1900 00:00:00"
            FechaFinal = "31/12/3000 00:00:00"
          End If
          With Asistencia
            If MiRs("FA05CODCATEG") <> RNFAnterior Then
              Cont = 0
              RNFAnterior = MiRs("FA05CODCATEG")
            Else
              Cont = Cont + 1
            End If
            Set NuevoRNF = New rnf
            With NuevoRNF
              .CodCateg = MiRs("FA05CODCATEG")
              .Fecha = MiRs("FA03FECHA") & ""
              .Cantidad = MiRs("FA03CANTIDAD")
              .Asignado = False
              .Descripcion = MiRs("FA03OBSERV") & ""
              .Key = MiRs("FA05CODCATEG") & "-" & Format(Cont, "000")
            End With
           .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & Format(Cont, "000")
            MiRs.MoveNext
            If Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS") < FechaInicio Then
              FechaInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
              Asistencia.FecInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
            End If
            If Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS") > FechaFinal Then
              FechaFinal = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
              Asistencia.FecFin = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
            End If
          End With
        Wend
      objApp.CommitTrans
    Else
      MsgBox "No hay ning�n registro que responda a las condiciones del filtro", vbOKOnly + vbInformation, "aviso"
    End If
  End If
Exit Sub
ErrorenRNF:
  MsgBox "Se ha producido un error en la inserci�n de los RNF", vbCritical + vbOKOnly, "Aviso"
  Resume Next
  objApp.RollbackTrans


End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode = 0 Then
    BotonSalir = True
  End If
End Sub

Private Sub grdHistoras_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim lngHistoria As Long
Dim rsPersona As rdoResultset
Dim rsNombre As rdoResultset
Dim MiSqL As String

  If grdHistoras.Columns(0).Text <> "" Then
    lngHistoria = Me.grdHistoras.Columns(0).Text
    MiSqL = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22FECNACIM, CI22NUMDIRPRINC, " & _
            "CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
            " from CI2200, CI1000" & _
            " where CI22NUMHISTORIA = " & lngHistoria & _
            " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
            " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)"
    Set rsNombre = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    
    If Not rsNombre.EOF Then
      rsNombre.MoveFirst
      grdHistoras.Columns(1).Text = rsNombre("CI22NOMBRE") & " " & rsNombre("CI22PRIAPEL") & " " & rsNombre("CI22SEGAPEL")
      grdHistoras.Columns(2).Text = rsNombre("CI10CALLE") & " " & rsNombre("CI10PORTAL") & " " & rsNombre("CI10RESTODIREC") & ", " & rsNombre("CI10DESLOCALID")
      grdHistoras.Columns(3).Text = rsNombre("CI22FECNACIM") & ""
    Else
      MsgBox "No se puede facturar una historia que no est� asociada a un paciente", vbExclamation + vbOKOnly, "Atenci�n"
      grdHistoras.Columns(0).Text = ""
      grdHistoras.Columns(1).Text = ""
      grdHistoras.Columns(2).Text = ""
      grdHistoras.Columns(3).Text = ""
      'grdHistoras.Col = 0
    End If
  Else
    grdHistoras.Columns(1).Text = ""
    grdHistoras.Columns(2).Text = ""
    grdHistoras.Columns(3).Text = ""
  End If

End Sub

Private Sub grdHistoras_Change()
'  grdHistoras.Columns(1).Text = ""
End Sub


Private Sub grdHistoras_KeyPress(KeyAscii As Integer)
  If KeyAscii <> 22 Then
    KeyAscii = fValidarEnteros(KeyAscii)
  End If
End Sub




