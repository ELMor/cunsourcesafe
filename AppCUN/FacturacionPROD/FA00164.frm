VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frm_ListadoNoFacturados 
   Caption         =   "Listado de pacientes no facturados"
   ClientHeight    =   3000
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6870
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   6870
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cboCodEntidad 
      Height          =   315
      Left            =   2355
      TabIndex        =   8
      Top             =   840
      Width           =   3615
   End
   Begin VB.TextBox txtCodEntidad 
      Height          =   315
      Left            =   1755
      TabIndex        =   7
      Top             =   840
      Width           =   495
   End
   Begin VB.ComboBox cboCodTipEcon 
      Height          =   315
      Left            =   2355
      TabIndex        =   6
      Top             =   360
      Width           =   3615
   End
   Begin VB.TextBox txtCodTipEcon 
      Height          =   315
      Left            =   1755
      TabIndex        =   5
      Top             =   360
      Width           =   495
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Default         =   -1  'True
      Height          =   495
      Left            =   2400
      TabIndex        =   1
      Top             =   2160
      Width           =   1815
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSFecInicio 
      Height          =   255
      Left            =   1440
      TabIndex        =   0
      Top             =   1560
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483639
      DefaultDate     =   ""
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSFecFin 
      Height          =   255
      Left            =   4080
      TabIndex        =   2
      Top             =   1560
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483639
      DefaultDate     =   ""
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Entidad:"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   960
      Width           =   1440
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo Econ�mico:"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   480
      Width           =   1440
   End
   Begin VB.Label Label1 
      Caption         =   "Desde"
      Height          =   255
      Left            =   840
      TabIndex        =   4
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Hasta"
      Height          =   255
      Left            =   3480
      TabIndex        =   3
      Top             =   1560
      Width           =   615
   End
End
Attribute VB_Name = "frm_ListadoNoFacturados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdImprimir_Click()
Dim strWhere$
strWhere = "AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO AND " & _
    "AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI AND " & _
    "AD0800.AD01CODASISTENCI = AD0100.AD01CODASISTENCI AND " & _
    "AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI AND " & _
    "AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO AND " & _
    "AD1100.AD11FECFIN IS NULL AND " & _
    "AD0500.AD05FECFINRESPON IS NULL AND " & _
    "AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA AND " & _
    "AD0500.AD02CODDPTO = AD0200.AD02CODDPTO AND " & _
    "AD0800.AD08FECINICIO>=" & fFechaCrystal(SSFecInicio.Date) & _
    " AND AD0800.AD08FECINICIO <=" & fFechaCrystal(SSFecFin.Date) & _
    " AND NOT EXISTS (SELECT FA0400.AD01CODASISTENCI " & _
    "FROM FA0400 WHERE  " & _
    "AD0800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI AND " & _
    "AD0800.AD07CODPROCESO =FA0400.AD07CODPROCESO)"
If txtCodTipEcon.Text <> "" Then
  strWhere = strWhere & " AND AD1100.CI32CODTIPECON= '" & txtCodTipEcon.Text & "'"
  If txtCodEntidad.Text <> "" Then
    strWhere = strWhere & "AND AD1100.CI13CODENTIDAD= '" & txtCodEntidad.Text & "'"
  End If
End If
Call Imprimir_API(strWhere, "SinFacturarPorDepartamentos.rpt") ', formula)
End Sub
Private Sub cboCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If

End Sub

Private Sub cboCodTipEcon_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If
End Sub

Private Sub cboCodTipEcon_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodTipEcon.Clear
  
  SQL = "Select CI32DESTIPECON From CI3200 Where CI32INDRESPECO = -1"
  Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
  If Not RS.EOF Then
    RS.MoveLast
    RS.MoveFirst
    While Not RS.EOF
      cboCodTipEcon.AddItem RS("CI32DESTIPECON") & ""
      RS.MoveNext
    Wend
  End If
End Sub

Private Sub cboCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If

End Sub
Private Function ComprobarCampos() As Boolean
    
    'Comprobamos que haya un tipo econ�mico
    If Trim(Me.txtCodTipEcon.Text) = "" Then
      MsgBox "Se debe seleccionar un Tipo Econ�mico.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodTipEcon.Enabled = True Then
        Me.txtCodTipEcon.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que haya una entidad
    If Trim(Me.txtCodEntidad.Text) = "" Then
      MsgBox "Se debe seleccionar una entidad.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodEntidad.Enabled = True Then
        Me.txtCodEntidad.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que la fecha de inicio sea correcta
    If Not IsNull(Me.SDCFechaInicio.Date) Then
      If Not IsDate(Me.SDCFechaInicio.Date) Then
        MsgBox "La Fecha de Inicio no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaInicio.Enabled = True Then
          Me.SDCFechaInicio.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de finalizaci�n sea correcta
    If Not IsNull(Me.SDCFechaFin.Date) Then
      If Not IsDate(Me.SDCFechaFin.Date) Then
        MsgBox "La Fecha Fin no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaFin.Enabled = True Then
          Me.SDCFechaFin.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de inicio no sea mayor que la de finalizaci�n.
    If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
      MsgBox "La fecha de inicio no puede ser superior a la de finalizaci�n.", vbOKOnly + vbInformation, "Aviso"
      If Me.SDCFechaFin.Enabled = True Then
        Me.SDCFechaFin.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    ComprobarCampos = True
End Function


Private Sub Form_Load()
  objCW.blnAutoDisconnect = False
End Sub

Private Sub txtCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodEntidad.Text = RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If


End Sub


Private Sub txtCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI32DESTIPECON From CI3200 Where CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodTipEcon.Text = RS("CI32DESTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodTipEcon.Text = ""
      Me.txtCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If


End Sub
Private Sub cboCodEntidad_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If
End Sub

Private Sub cboCodEntidad_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodEntidad.Clear
  
  If Trim(txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI32CODTIPECON = '" & Me.txtCodTipEcon & "' " & _
          "And CI13FECFIVGENT IS NULL"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        cboCodEntidad.AddItem RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    End If
  Else
    MsgBox "Debe seleccionar un tipo econ�mico", vbInformation + vbOKOnly, "Aviso"
  End If
End Sub



