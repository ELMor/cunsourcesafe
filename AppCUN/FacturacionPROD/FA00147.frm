VERSION 5.00
Object = "{73571CB4-36E0-11D4-8538-00C04F9BFA08}#1.0#0"; "ac00100.ocx"
Begin VB.Form frm_VisionGlobal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Visi�n Global"
   ClientHeight    =   6795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11295
   ClipControls    =   0   'False
   Icon            =   "FA00147.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6795
   ScaleWidth      =   11295
   StartUpPosition =   2  'CenterScreen
   Begin ac00100.Procesos Procesos1 
      Height          =   6765
      Left            =   0
      TabIndex        =   1
      Top             =   45
      Width           =   4470
      _ExtentX        =   7885
      _ExtentY        =   11933
   End
   Begin ac00100.Actuaciones Actuaciones1 
      Height          =   6720
      Left            =   4455
      TabIndex        =   0
      Top             =   45
      Width           =   6825
      _ExtentX        =   12039
      _ExtentY        =   11853
   End
End
Attribute VB_Name = "frm_VisionGlobal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub Form_Resize()
    On Error Resume Next
    If Me.Width < 7000 Then
        Me.Width = 7000
    End If
    If Me.Height < 4000 Then
        Me.Height = 4000
    End If
    
    Actuaciones1.Height = Me.Height - 400
    Actuaciones1.Width = Me.Width - 4565
    Actuaciones1.Refresh
    
    Procesos1.Height = Me.Height - 345
End Sub

Public Sub Procesos1_Selected(strProceso As String, strAsistencia As String, strFecha As String, lngDpto As Long, lngActuacion As Long, lngGrActuacion As Long)
    Dim RS As rdoResultset
    Dim Texto As String
    On Error Resume Next
    ' cargar actuaciones
    Screen.MousePointer = vbHourglass
    
    Actuaciones1.strProceso = strProceso
    Actuaciones1.strAsistencia = strAsistencia
    Actuaciones1.strFecha = strFecha
    Actuaciones1.lngDpto = lngDpto
    Actuaciones1.lngCodActuacion = lngActuacion
    Actuaciones1.lngCodGrActuacion = lngGrActuacion
    
    Actuaciones1.Refresh
    Screen.MousePointer = vbDefault
    
End Sub

Private Sub Procesos1_SelectedItem(strProceso As String, strAsistencia As String, strFecha As String, lngDpto As Long, lngActuacion As Long, lngGrActuacion As Long)
    Dim RS As rdoResultset
    Dim Texto As String
    On Error Resume Next
    ' cargar actuaciones
    Screen.MousePointer = vbHourglass
    
    Actuaciones1.strProceso = strProceso
    Actuaciones1.strAsistencia = strAsistencia
    Actuaciones1.strFecha = strFecha
    Actuaciones1.lngDpto = lngDpto
    Actuaciones1.lngCodActuacion = lngActuacion
    Actuaciones1.lngCodGrActuacion = lngGrActuacion
    
    Actuaciones1.Refresh
    Screen.MousePointer = vbDefault

End Sub
