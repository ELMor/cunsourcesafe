VERSION 5.00
Begin VB.Form frmIdentificacionUsuario 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Identificación de Usuario"
   ClientHeight    =   2115
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4980
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2115
   ScaleWidth      =   4980
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   1440
      Width           =   2175
   End
   Begin VB.TextBox txtText1 
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Top             =   720
      Width           =   2175
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Usuario:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   840
      Width           =   855
   End
End
Attribute VB_Name = "frmIdentificacionUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdaceptar_Click()
  Dim strsql As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
  
  gintCodDpto = 0
  gstrNombrePerCUN = ""
  gstrPriApelPerCUN = ""
  gstrSegApelPerCUN = ""
  gintNumColegiado = 0
  
  If (IsNull(txtText1.Text)) Or (txtText1.Text = "") Then
    intResp = MsgBox("Usuario no autorizado", vbInformation, "Aviso")
    txtText1.SetFocus
  Else
    If IsNumeric(txtText1.Text) Then
      strsql = "SELECT COUNT(*) FROM AD0600 WHERE AD06CODPERSONA = " & txtText1.Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      If (rsta.rdoColumns(0).Value = 0) Then
        rsta.Close
        Set rsta = Nothing
        intResp = MsgBox("Usuario no autorizado", vbInformation, "Aviso")
        txtText1.SetFocus
      Else
        rsta.Close
        Set rsta = Nothing
        strsql = "SELECT AD02CODDPTO FROM AD0300 WHERE AD06CODPERSONA = " & txtText1.Text
        Set rsta = objApp.rdoConnect.OpenResultset(strsql)
        gintCodDpto = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
      
        strsql = "SELECT AD06NOMBRE,AD06PRIAPEL,AD06SEGAPEL,AD06NUMCOLEGIADO FROM AD0600 WHERE AD06CODPERSONA = " & txtText1.Text
        Set rsta = objApp.rdoConnect.OpenResultset(strsql)
        gstrNombrePerCUN = rsta.rdoColumns(0).Value
        gstrPriApelPerCUN = rsta.rdoColumns(1).Value
        gstrSegApelPerCUN = rsta.rdoColumns(2).Value
        gintNumColegiado = rsta.rdoColumns(3).Value
        rsta.Close
        Set rsta = Nothing
        Unload Me
      End If
    Else
      intResp = MsgBox("Introduzca su numero personal", vbInformation, "Aviso")
    End If
  End If
End Sub

