VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Object = "{A7E3152D-9E00-11D1-9146-00C04FBB52E1}#1.0#0"; "idperson.ocx"
Begin VB.Form frmpedir 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Alta de Petici�n"
   ClientHeight    =   7995
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11610
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0152.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7995
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   2280
      TabIndex        =   24
      Top             =   6840
      Width           =   7335
      Begin VB.CommandButton cmdLanzarPrograma 
         Caption         =   "Programa Paciente"
         Height          =   375
         Left            =   4800
         TabIndex        =   15
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton cmdInstruccionesPaciente 
         Caption         =   "Instrucciones Paciente"
         Height          =   375
         Left            =   2700
         TabIndex        =   14
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton cmdConsentimientoPaciente 
         Caption         =   "Consentimiento Paciente"
         Height          =   375
         Left            =   600
         TabIndex        =   13
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame frmBotonera 
      Height          =   735
      Left            =   1200
      TabIndex        =   23
      Top             =   6000
      Width           =   9495
      Begin VB.CommandButton cmdCambPlanific 
         Caption         =   "Cambiar Planificaci�n"
         Height          =   375
         Left            =   4800
         TabIndex        =   45
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton cmdCitar 
         Caption         =   "Citas"
         Height          =   375
         Left            =   6840
         TabIndex        =   12
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton cmdFormularPet 
         Caption         =   "Planificar"
         Height          =   375
         Left            =   2760
         TabIndex        =   11
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton cmdseleccionar 
         Caption         =   "Seleccionar Actuaciones"
         Height          =   375
         Left            =   480
         TabIndex        =   10
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   11460
      Begin TabDlg.SSTab tabTab1 
         Height          =   4860
         Index           =   0
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   8573
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0152.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(10)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "tabTab1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0152.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR09NUMPETICION"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   720
            TabIndex        =   1
            Tag             =   "N�mero de Petici�n"
            Top             =   480
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR09NUMGRUPO"
            Height          =   330
            Index           =   9
            Left            =   2880
            TabIndex        =   2
            Tag             =   "N�mero de Grupo"
            Top             =   480
            Width           =   1092
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            Caption         =   "PETICIONES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "PR09FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   6600
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Fecha de Petici�n|Fecha de la Petici�n"
            Top             =   480
            Width           =   1800
            _Version        =   65537
            _ExtentX        =   3175
            _ExtentY        =   582
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            StartofWeek     =   2
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   3660
            Index           =   1
            Left            =   120
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   960
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   6456
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Persona Paciente"
            TabPicture(0)   =   "PR0152.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Line1"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Line2"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "Line3"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "Line4"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(2)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(5)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(8)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(6)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(41)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "dtcfecha"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "UpDownhora"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "UpDownminuto"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(5)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(1)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(7)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "IdPersona1"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "Frame2"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtminuto"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txthora"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(23)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).ControlCount=   21
            TabCaption(1)   =   "Observaciones"
            TabPicture(1)   =   "PR0152.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(4)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(3)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "txtText1(4)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "txtText1(3)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).ControlCount=   4
            TabCaption(2)   =   "Contacto"
            TabPicture(2)   =   "PR0152.frx":007C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(12)"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).Control(1)=   "lblLabel1(13)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "txtText1(11)"
            Tab(2).Control(2).Enabled=   0   'False
            Tab(2).Control(3)=   "txtText1(12)"
            Tab(2).Control(3).Enabled=   0   'False
            Tab(2).ControlCount=   4
            TabCaption(3)   =   "Consecuencia Informe"
            TabPicture(3)   =   "PR0152.frx":0098
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(1)"
            Tab(3).Control(0).Enabled=   0   'False
            Tab(3).Control(1)=   "lblLabel1(9)"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).Control(2)=   "txtText1(6)"
            Tab(3).Control(2).Enabled=   0   'False
            Tab(3).Control(3)=   "txtText1(8)"
            Tab(3).Control(3).Enabled=   0   'False
            Tab(3).ControlCount=   4
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR04NUMACTPLAN"
               Height          =   330
               Index           =   8
               Left            =   -74520
               TabIndex        =   51
               Tag             =   "N� Actuaci�n Planificada|N�mero de Actuaci�n Planificada"
               Top             =   1080
               Width           =   1455
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   6
               Left            =   -74520
               TabIndex        =   50
               Tag             =   "Descripci�n Corta|Descripci�n Corta de la Actuaci�n"
               Top             =   2160
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09FECPLANIFIC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   23
               Left            =   7440
               Locked          =   -1  'True
               TabIndex        =   49
               TabStop         =   0   'False
               Tag             =   "Fecha y Hora de Preferencia"
               Top             =   2400
               Visible         =   0   'False
               Width           =   2775
            End
            Begin VB.TextBox txthora 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   8520
               MaxLength       =   2
               TabIndex        =   8
               Tag             =   "Hora de Preferencia"
               Top             =   3180
               Width           =   390
            End
            Begin VB.TextBox txtminuto 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   9120
               MaxLength       =   2
               TabIndex        =   9
               Tag             =   "Minutos de la Preferencia"
               Top             =   3180
               Width           =   375
            End
            Begin VB.Frame Frame2 
               BackColor       =   &H80000014&
               Height          =   1695
               Left            =   3480
               TabIndex        =   40
               Top             =   360
               Visible         =   0   'False
               Width           =   4215
               Begin VB.PictureBox Picture1 
                  BackColor       =   &H80000014&
                  BorderStyle     =   0  'None
                  Height          =   615
                  Left            =   360
                  Picture         =   "PR0152.frx":00B4
                  ScaleHeight     =   615
                  ScaleWidth      =   495
                  TabIndex        =   43
                  Top             =   720
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.Label Label2 
                  BackStyle       =   0  'Transparent
                  Caption         =   "Espere un momento por favor..."
                  Height          =   375
                  Left            =   1320
                  TabIndex        =   42
                  Top             =   840
                  Width           =   2415
               End
               Begin VB.Label Label1 
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Se est� generando la Petici�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   360
                  TabIndex        =   41
                  Top             =   240
                  Width           =   3495
               End
            End
            Begin IdPerson.IdPersona IdPersona1 
               Height          =   1335
               Left            =   240
               TabIndex        =   4
               Top             =   600
               Width           =   9975
               _ExtentX        =   17595
               _ExtentY        =   2355
               BackColor       =   12648384
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Datafield       =   "CI21CodPersona"
               MaxLength       =   7
               blnAvisos       =   0   'False
            End
            Begin VB.TextBox txtText1 
               DataField       =   "PR09PERSCONTAC"
               Height          =   690
               Index           =   12
               Left            =   -74640
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   37
               Tag             =   "Persona de Contacto"
               Top             =   1260
               Width           =   7965
            End
            Begin VB.TextBox txtText1 
               DataField       =   "PR09TFNOCONTAC"
               Height          =   330
               Index           =   11
               Left            =   -74640
               TabIndex        =   36
               Tag             =   "Tel�fono Contacto|N�mero deTel�fono de Contacto"
               Top             =   2340
               Width           =   2400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09DESOBSERVAC"
               Height          =   1170
               HelpContextID   =   30104
               Index           =   3
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   33
               Tag             =   "Observaciones de la Petici�n|Observaciones"
               Top             =   660
               Width           =   9825
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09DESINDICACIO"
               Height          =   1410
               HelpContextID   =   30104
               Index           =   4
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   32
               Tag             =   "Indicaciones de la Petici�n|Indicaciones"
               Top             =   2100
               Width           =   9825
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   7
               Left            =   1680
               TabIndex        =   30
               TabStop         =   0   'False
               Tag             =   "Apellido 1� del Solicitante"
               Top             =   3180
               Width           =   3495
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               HelpContextID   =   30101
               Index           =   1
               Left            =   120
               TabIndex        =   5
               Tag             =   "C�d.Dpto. Peticionario"
               Top             =   2460
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   120
               TabIndex        =   6
               Tag             =   "C�d.Persona Peticionaria"
               Top             =   3180
               Width           =   852
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   5
               Left            =   1680
               TabIndex        =   27
               TabStop         =   0   'False
               Tag             =   "Descripci�n Dpto. Peticionario"
               Top             =   2460
               Width           =   5400
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2865
               Index           =   1
               Left            =   -74910
               TabIndex        =   26
               TabStop         =   0   'False
               Top             =   90
               Width           =   8655
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15266
               _ExtentY        =   5054
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin ComCtl2.UpDown UpDownminuto 
               Height          =   375
               Left            =   9496
               TabIndex        =   46
               Top             =   3180
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   661
               _Version        =   327681
               BuddyControl    =   "txtminuto"
               BuddyDispid     =   196626
               OrigLeft        =   9240
               OrigTop         =   1080
               OrigRight       =   9480
               OrigBottom      =   1575
               Increment       =   5
               Max             =   55
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin ComCtl2.UpDown UpDownhora 
               Height          =   375
               Left            =   8896
               TabIndex        =   47
               Top             =   3180
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   661
               _Version        =   327681
               BuddyControl    =   "txthora"
               BuddyDispid     =   196625
               OrigLeft        =   5400
               OrigTop         =   1200
               OrigRight       =   5640
               OrigBottom      =   1695
               Max             =   23
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcfecha 
               Height          =   330
               Left            =   6360
               TabIndex        =   7
               Tag             =   "Fecha de Planificaci�n|Fecha de Planificaci�n"
               Top             =   3180
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�m Act. Planificada"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   -74520
               TabIndex        =   53
               Top             =   840
               Width           =   1800
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -74520
               TabIndex        =   52
               Top             =   1920
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   8520
               TabIndex        =   48
               Top             =   2940
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha de Planificaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   6360
               TabIndex        =   44
               Top             =   2940
               Width           =   1965
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tel�fono de Contacto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   -74640
               TabIndex        =   39
               Top             =   2100
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona de Contacto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74640
               TabIndex        =   38
               Top             =   1020
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones de la Petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   -74760
               TabIndex        =   35
               Top             =   420
               Width           =   2505
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Indicaciones de la Petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   -74760
               TabIndex        =   34
               Top             =   1860
               Width           =   2325
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   1680
               TabIndex        =   31
               Top             =   2940
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Departamento Solicitante"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   120
               TabIndex        =   29
               Top             =   2220
               Width           =   2160
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d.Doctor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   28
               Top             =   2940
               Width           =   975
            End
            Begin VB.Line Line4 
               X1              =   10440
               X2              =   10440
               Y1              =   480
               Y2              =   2040
            End
            Begin VB.Line Line3 
               X1              =   10440
               X2              =   120
               Y1              =   480
               Y2              =   480
            End
            Begin VB.Line Line2 
               X1              =   120
               X2              =   120
               Y1              =   480
               Y2              =   2040
            End
            Begin VB.Line Line1 
               X1              =   120
               X2              =   10440
               Y1              =   2040
               Y2              =   2040
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   720
            TabIndex        =   22
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de la Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   6600
            TabIndex        =   21
            Top             =   240
            Width           =   1770
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   2880
            TabIndex        =   20
            Top             =   240
            Width           =   975
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   18
      Top             =   7710
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmpedir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00124.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Pantalla de Petici�n de Actuaciones                     *                                                        *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

    Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
    Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
    
    Dim intcambioalgo As Integer
    Dim intaux As Integer
    Dim strfechaaux As String
    Dim mtblactsel()
    Dim mintindice As Integer
    Dim gintfinindice As Integer
    Dim blnSincroPaci As Boolean
    Dim mblnFechaModif As Boolean
    Dim mblnError As Boolean 'Determina si se ha producido un error al grabar
    Dim mblnfoco As Boolean
    Dim mintminuto As Integer

Private Sub cmdCambPlanific_Click()
  Dim strsqlact As String
  Dim rstact As rdoResultset
  
    cmdCambPlanific.Enabled = False
    strsqlact = "SELECT COUNT(*) FROM PR0600 WHERE PR03NUMACTPEDI IN (SELECT PR03NUMACTPEDI FROM PR0300 WHERE PR09NUMPETICION=" & txtText1(0).Text & ")"
    Set rstact = objApp.rdoConnect.OpenResultset(strsqlact)
    If rstact(0).Value > 0 Then
      'Load frmSeleccionarAct
      frmPlanifActuaciones.txtText1(0).Text = IdPersona1.Text
      frmPlanifActuaciones.txtText1(1).Text = IdPersona1.Historia
      frmPlanifActuaciones.txtText1(2).Text = IdPersona1.Dni
      frmPlanifActuaciones.txtText1(3).Text = IdPersona1.Nombre
      frmPlanifActuaciones.txtText1(4).Text = IdPersona1.Apellido1
      frmPlanifActuaciones.txtText1(5).Text = IdPersona1.Apellido2
      Call objsecurity.LaunchProcess("PR0199")
    Else
      Call MsgBox("Antes debe Planificar", vbExclamation)
    End If
    cmdCambPlanific.Enabled = True
End Sub

Private Sub cmdCitar_Click()
  Dim intResp As Integer
  Dim vntdatos(1 To 3) As Variant
  
  cmdCitar.Enabled = False
  If (txtText1(0).Text = "") Then
    intResp = MsgBox("No hay n�mero de Petici�n", vbInformation, "Aviso")
  Else
    If (txtText1(9).Text = "") Then
      intResp = MsgBox("No hay n�mero de Grupo", vbInformation, "Aviso")
    Else
      If (txtText1(1).Text = "") Then
        intResp = MsgBox("No hay Departamento Solicitante", vbInformation, "Aviso")
      Else
        If (txtText1(2).Text = "") Then
          intResp = MsgBox("No hay Persona Solicitante", vbInformation, "Aviso")
        Else
          If (dtcDateCombo1(0).Text = "") Then
            intResp = MsgBox("No hay Fecha de Petici�n", vbInformation, "Aviso")
          Else
            If (IdPersona1.Text = "") Then
              intResp = MsgBox("No hay Paciente", vbInformation, "Aviso")
            Else
              'Salvamos los datos de la petici�n
              Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
              objWinInfo.DataSave
              If gintEstadoPeticion <> 0 Then
                  Unload Me
              Else
                vntdatos(1) = txtText1(9).Text
                vntdatos(2) = txtText1(0).Text
                vntdatos(3) = IdPersona1.Text
                Call objsecurity.LaunchProcess("CI1025", vntdatos)
              End If
              'Se pasa el n� de petici�n a citas
              'Call objWinInfo.CtrlSet(frmPeticion3.txtText1(1), txtText1(0).Text)
            End If
          End If
        End If
      End If
    End If
  End If
  cmdCitar.Enabled = True
  
End Sub
Private Sub Crear_Recursos_Pedidos_Pet(ByVal mlngnumfase As Long)

    Dim rstAselrecursos As rdoResultset
    Dim sqlstrselrecursos As String
    Dim sqlstrinrecursos As String
    Dim mlngnumneces As Long
    Dim mlngnumuni As Long
    Dim mlngcodtiprec As Long
    Dim mblnindpref As Boolean
    Dim mlngcoddpto As Long
    
    sqlstrselrecursos = "SELECT PR13NUMNECESID,PR13NUMUNIREC,AD02CODDPTO,AG14CODTIPRECU,PR13INDPREFEREN,PR13INDPLANIF,PR13NUMTIEMPREC,PR13NUMMINDESF FROM PR1300 WHERE PR01CODACTUACION=" & _
                      mtblactsel(2, mintindice - 1) & " AND PR05NUMFASE=" & _
                      mlngnumfase
    Set rstAselrecursos = objApp.rdoConnect.OpenResultset(sqlstrselrecursos)
    
    Do Until rstAselrecursos.EOF
         If rstAselrecursos("pr13indplanif").Value <> 0 Then
            sqlstrinrecursos = "UPDATE PR0300 SET PR03indcitable=" & rstAselrecursos("pr13indplanif").Value _
             & ",PR03INDCITAANT=" & rstAselrecursos("pr13indplanif").Value & " WHERE pr03numactpedi=" & mtblactsel(6, mintindice - 1)
            objApp.rdoConnect.Execute sqlstrinrecursos, 64
         End If
        sqlstrinrecursos = "INSERT INTO PR1400 " _
                          & " (PR03NUMACTPEDI,PR06NUMFASE,PR14NUMNECESID,PR14NUMUNIREC,AD02CODDPTO,AG14CODTIPRECU,PR14INDRECPREFE,PR14NUMMINOCU,PR14NUMMINDESREC) VALUES " _
                          & " (" & mtblactsel(6, mintindice - 1) & "," & mlngnumfase
                          
        
        'PR13NUMNECESID
        If IsNull(rstAselrecursos.rdoColumns(0).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(0).Value
        End If
        
        'PR13NUMUNIREC
        If IsNull(rstAselrecursos.rdoColumns(3).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(1).Value
        End If
        
        'AD02CODDPTO
        If IsNull(rstAselrecursos.rdoColumns(2).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(2).Value
        End If
        
        'AG14CODTIPRECU
        If IsNull(rstAselrecursos.rdoColumns(1).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(3).Value
        End If
        'sqlstrinrecursos = sqlstrinrecursos & ",null,null"
        
        'PR13INDPREFEREN
        If IsNull(rstAselrecursos.rdoColumns(5).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(5).Value
        End If
        
        'PR14NUMMINOCU
        If IsNull(rstAselrecursos.rdoColumns(6).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(6).Value
        End If
        
        'PR14NUMMINDESREC
        
        If IsNull(rstAselrecursos.rdoColumns(7).Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns(7).Value
        End If
        
        sqlstrinrecursos = sqlstrinrecursos & ")"
        objApp.rdoConnect.Execute sqlstrinrecursos, 64
        rstAselrecursos.MoveNext
    Loop
    rstAselrecursos.Close
    Set rstAselrecursos = Nothing
End Sub
Private Sub crear_restricciones()
Dim rstact As rdoResultset
Dim stract As String
Dim rsttipo As rdoResultset
Dim strtipo As String
Dim strrest As String
Dim rstrest As rdoResultset
Dim stra As String
Dim rsta As rdoResultset
Dim strinsert As String

stract = "select pr03numactpedi from PR0300 where pr09numpeticion=" & frmpedir.txtText1(0).Text
Set rstact = objApp.rdoConnect.OpenResultset(stract)
While Not rstact.EOF
  strtipo = "select ag14codtiprecu from pr1400 where pr03numactpedi=" & _
           rstact.rdoColumns(0).Value
  Set rsttipo = objApp.rdoConnect.OpenResultset(strtipo)
  While Not rsttipo.EOF
    strrest = "select ag16codtiprest from AG1500 where ag14codtiprecu=" & rsttipo.rdoColumns(0).Value
    Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
    While Not rstrest.EOF
      stra = "select count(*) from PR4700 where pr09numpeticion=" & _
          frmpedir.txtText1(0).Text & " AND ag16codtiprest=" & rstrest.rdoColumns(0).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.rdoColumns(0).Value = 0 Then
         strinsert = "INSERT INTO PR4700(pr09numpeticion,ag16codtiprest,pr47valor) " & _
                   "VALUES(" & frmpedir.txtText1(0).Text & "," & _
                   rstrest.rdoColumns(0).Value & ",' ')"
         objApp.rdoConnect.Execute strinsert, 64
      End If
      rsta.Close
      Set rsta = Nothing
      rstrest.MoveNext
    Wend
    rstrest.Close
    Set rstrest = Nothing
    rsttipo.MoveNext
  Wend
  rsttipo.Close
  Set rsttipo = Nothing
  rstact.MoveNext
  Wend
  
  rstact.Close
  Set rstact = Nothing

End Sub
Private Sub Inicializar_Array()

    
    Dim sqlstrselact As String
    Dim rstaselact As rdoResultset
    Dim mblnactivado As Boolean
    Dim sqlstrselcond As String
    Dim rstaselcond As rdoResultset
    Dim mblncondiciones As Boolean
    Dim mblncumplircond As Boolean
    Dim rstaselcodact As rdoResultset
    Dim sqlstrselcodact As String
    Dim rstaseldesdpto As rdoResultset
    Dim sqlstrseldesdpto As String
    Dim rstdescripcion As rdoResultset
    Dim strdescripcion As String
    
    If txtText1(0).Text <> "" Then
      sqlstrselact = "SELECT PR03NUMACTPEDI, PR08NUMSECUENCIA, PR09NUMPETICION FROM PR0800 WHERE PR09NUMPETICION=" & _
                      txtText1(0).Text
  
      Set rstaselact = objApp.rdoConnect.OpenResultset(sqlstrselact)
      mintindice = 0
      ReDim mtblactsel(9, 1)
      Do Until rstaselact.EOF
  
          'sacar Selec
          mtblactsel(0, mintindice) = True
          'sacar Cond
          sqlstrselcond = "SELECT PR38INDRESPUESTA FROM PR3800 WHERE PR03NUMACTPEDI=" & _
                          rstaselact.rdoColumns(0).Value 'PR03NUMACTPEDI
      
          Set rstaselcond = objApp.rdoConnect.OpenResultset(sqlstrselcond)
          mblncumplircond = False
          mblncondiciones = False
          Do Until rstaselcond.EOF
              mblncumplircond = True
              mblncondiciones = True
              'PR38INDRESPUESTA
              If rstaselcond.rdoColumns(0).Value = False Then
                  mblncumplircond = False
                  Exit Do
              End If
              rstaselcond.MoveNext
          Loop
          If mblncumplircond = True Or mblncondiciones = False Then
              mtblactsel(1, mintindice) = True
              mblnactivado = True
          Else
              mtblactsel(1, mintindice) = False
              mblnactivado = False
          End If
          'sacar CodAct
          
          sqlstrselcodact = "SELECT AD02CODDPTO, PR01CODACTUACION, PR01CODACTUACION,PR03NUMACPEDI_PRI, PR09NUMPETICION " _
                          & " FROM PR0300 WHERE PR03NUMACTPEDI = " & _
                          rstaselact.rdoColumns(0).Value 'PR03NUMACTPEDI
          Set rstaselcodact = objApp.rdoConnect.OpenResultset(sqlstrselcodact)
          
          strdescripcion = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & _
                          rstaselcodact.rdoColumns(1).Value
          Set rstdescripcion = objApp.rdoConnect.OpenResultset(strdescripcion)
          
          mtblactsel(2, mintindice) = rstaselcodact.rdoColumns(1).Value 'PR01CODACTUACION
          'sacar DesAct
          'mtblactsel(3, mintindice) = rstaselcodact.rdoColumns(2).Value 'PR03DESCORTA
          mtblactsel(3, mintindice) = rstdescripcion.rdoColumns(0).Value 'PR01DESCORTA
          
          rstdescripcion.Close
          Set rstdescripcion = Nothing
          
          'sacar CodDpto
          mtblactsel(4, mintindice) = rstaselcodact.rdoColumns(0).Value 'AD02CODDPTO
          'sacar DesDpto
          sqlstrseldesdpto = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & _
                              rstaselcodact.rdoColumns(0).Value 'AD02CODDPTO
      
          Set rstaseldesdpto = objApp.rdoConnect.OpenResultset(sqlstrseldesdpto)
          mtblactsel(5, mintindice) = rstaseldesdpto.rdoColumns(0).Value 'AD02DESDPTO
          'sacar Pr0300
          If rstaselcodact.rdoColumns(4).Value = rstaselact.rdoColumns(2).Value Then
              mtblactsel(6, mintindice) = rstaselact.rdoColumns(0).Value 'PR03NUMACTPEDI
          End If
          'sacar Pr0800
          mtblactsel(7, mintindice) = rstaselact.rdoColumns(0).Value 'PR03NUMACTPEDI
          'sacar Pr0800Sec
          mtblactsel(8, mintindice) = rstaselact.rdoColumns(1).Value 'PR08NUMSECUENCIA
          'sacar Asociada a
          If IsNull(rstaselcodact.rdoColumns(3).Value) Then 'PR03NUMACPEDI_PRI
              mtblactsel(9, mintindice) = 0
          Else
              mtblactsel(9, mintindice) = rstaselcodact.rdoColumns(3).Value 'PR03NUMACPEDI_PRI
          End If
          mintindice = mintindice + 1
          ReDim Preserve mtblactsel(9, mintindice)
          rstaselact.MoveNext
          rstaselcond.Close
          rstaselcodact.Close
          rstaseldesdpto.Close
      Loop
      gintfinindice = mintindice
      rstaselact.Close
      Set rstaselact = Nothing
      Set rstaselcond = Nothing
      Set rstaselcodact = Nothing
      Set rstaseldesdpto = Nothing
   End If
End Sub
Private Sub Crear_Fases_Pedidas_Pet()
    
    Dim rstaselfases As rdoResultset
    Dim sqlstrselfases As String
    Dim sqlstrinfases As String
    Dim mlngnumfase As Long
    Dim mstrdesfase As String
    Dim mlngnumocupaci As Long
    Dim mlngnumfasepre As Long
    Dim mlngnumminpre As Long
    Dim mlngnummaxpre As Long
    Dim mblnindhabilnat As Boolean
    Dim mblnindinicfin As Boolean
    
    sqlstrselfases = "SELECT PR05NUMFASE, PR05DESFASE, PR05NUMOCUPACI, PR05NUMFASE_PRE, " _
                    & "PR05NUMTMINFPRE, PR05NUMTMAXFPRE, PR05INDHABILNATU, PR05INDINICIOFIN " _
                    & " FROM PR0500 " _
                    & "WHERE PR01CODACTUACION = " & mtblactsel(2, mintindice - 1) _
                    & " AND PR05NUMFASE IN  (SELECT PR05NUMFASE FROM PR1300 WHERE PR01CODACTUACION= " & mtblactsel(2, mintindice - 1) & ")"
                    
                    
    Set rstaselfases = objApp.rdoConnect.OpenResultset(sqlstrselfases)
    
    Do Until rstaselfases.EOF
        sqlstrinfases = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, " _
                        & " PR06NUMMINOCUPAC, PR06NUMFASE_PRE, PR06NUMMINFPRE, " _
                        & " PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN)" _
                        & " VALUES (" & mtblactsel(6, mintindice - 1)
              
        'PR05DESFASE
        If IsNull(rstaselfases.rdoColumns(0).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(0).Value
        End If
        
        'PR05DESFASE
        If IsNull(rstaselfases.rdoColumns(1).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & ",'" & rstaselfases.rdoColumns(1).Value & "'"
        End If
        
        'PR05NUMOCUPACI
        If IsNull(rstaselfases.rdoColumns(2).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(2).Value
        End If
        
        'PR05NUMFASE_PRE
        If IsNull(rstaselfases.rdoColumns(3).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(3).Value
        End If
        
        'PR05NUMTMINFPRE
        If IsNull(rstaselfases.rdoColumns(4).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(4).Value
        End If
        
        'PR05NUMTMAXFPRE
        If IsNull(rstaselfases.rdoColumns(5).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(5).Value
        End If
        
        'PR05INDHABILNATU
        If IsNull(rstaselfases.rdoColumns(6).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(6).Value
        End If
        
        'PR05INDINICIOFIN
        If IsNull(rstaselfases.rdoColumns(7).Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns(7).Value
        End If
        sqlstrinfases = sqlstrinfases & ")"
        
        objApp.rdoConnect.Execute sqlstrinfases, 64
        Call Crear_Recursos_Pedidos_Pet(rstaselfases.rdoColumns(0).Value)
        rstaselfases.MoveNext
    Loop
    rstaselfases.Close
    Set rstaselfases = Nothing
End Sub
Private Sub Crear_Cuestionario_Peticion_Pet()

    Dim rstAselcuest As rdoResultset
    Dim rstAselrepet As rdoResultset
    Dim sqlstrselcuest As String
    Dim sqlstrincuest As String
    Dim sqlstrselrepet As String
    Dim mlngcodpreg As Long
    Dim mlngcodtipres As Long
    Dim mblnindoblig As Boolean
    Dim mlngcodlistresp As Long
    
    sqlstrselcuest = "SELECT PR40CODPREGUNTA, PR27CODTIPRESPU, PR29INDROBLIG, PR46CODLISTRESP " _
                   & " FROM PR2900 WHERE PR01CODACTUACION=" & mtblactsel(2, mintindice - 1)

    Set rstAselcuest = objApp.rdoConnect.OpenResultset(sqlstrselcuest)
    
    Do Until rstAselcuest.EOF
        
        sqlstrselrepet = "SELECT * FROM PR4100 WHERE PR40CODPREGUNTA=" & _
                          rstAselcuest.rdoColumns(0).Value & " AND PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) ' IN (SELECT PR03NUMACTPEDI FROM PR0300 WHERE " & _
                          "PR09NUMPETICION = " & txtText1(0).Text & ")"
                          
                  
        Set rstAselrepet = objApp.rdoConnect.OpenResultset(sqlstrselrepet)
        If rstAselrepet.EOF = True Then
                    
            sqlstrincuest = "INSERT INTO PR4100 " _
                  & " (PR03NUMACTPEDI, PR40CODPREGUNTA, PR27CODTIPRESPU, PR41INDROBLIG, PR46CODLISTRESP )" _
                  & " VALUES (" & mtblactsel(6, mintindice - 1)
            If IsNull(rstAselcuest.rdoColumns(0).Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(0).Value
            End If
            If IsNull(rstAselcuest.rdoColumns(1).Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(1).Value
            End If
            If IsNull(rstAselcuest.rdoColumns(2).Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(2).Value
            End If
            If IsNull(rstAselcuest.rdoColumns(3).Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(3).Value
            End If
            sqlstrincuest = sqlstrincuest & ")"
            objApp.rdoConnect.Execute sqlstrincuest, 64
        End If
        rstAselrepet.Close
        rstAselcuest.MoveNext
    Loop
    rstAselcuest.Close
    Set rstAselcuest = Nothing
    Set rstAselrepet = Nothing
End Sub
Private Sub Crear_Cuestionario_Grupo_Peticion_Pet()
    Dim rstAselgrupo As rdoResultset
    Dim sqlstrselgrupo As String
    Dim rstAselcuest As rdoResultset
    Dim rstAselrepet As rdoResultset
    Dim sqlstrselcuest As String
    Dim sqlstrselrepet As String
    Dim sqlstrincuest As String
    Dim mlngcodpreg As Long
    Dim mlngcodtipres As Long
    Dim mblnindoblig As Boolean
    Dim mlngcodlistresp As Long
    
    sqlstrselgrupo = "SELECT PR16CODGRUPO FROM PR1700 WHERE PR01CODACTUACION=" & _
                      mtblactsel(2, mintindice - 1)

    Set rstAselgrupo = objApp.rdoConnect.OpenResultset(sqlstrselgrupo)
    Do Until rstAselgrupo.EOF
        
        sqlstrselcuest = "SELECT PR40CODPREGUNTA, PR30INDOBLIG, PR46CODLISTRESP, PR27CODTIPRESPU " _
                         & " FROM PR3000 WHERE PR16CODGRUPO=" & _
                          rstAselgrupo.rdoColumns(0).Value
                          
        Set rstAselcuest = objApp.rdoConnect.OpenResultset(sqlstrselcuest)
        
        Do Until rstAselcuest.EOF
            
            sqlstrselrepet = "SELECT * " _
                           & " FROM PR4100 WHERE PR40CODPREGUNTA=" & _
                              rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value & " AND PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
            Set rstAselrepet = objApp.rdoConnect.OpenResultset(sqlstrselrepet)
            If rstAselrepet.EOF = True Then
                sqlstrincuest = "INSERT INTO PR4100 (PR03NUMACTPEDI, PR40CODPREGUNTA, PR27CODTIPRESPU, PR41INDROBLIG, PR46CODLISTRESP)" & _
                                " VALUES (" & mtblactsel(6, mintindice - 1)
                
                If IsNull(rstAselcuest.rdoColumns(0).Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(0).Value
                End If
                If IsNull(rstAselcuest.rdoColumns(3).Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(3).Value
                End If
                If IsNull(rstAselcuest.rdoColumns(1).Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(1).Value
                End If
                If IsNull(rstAselcuest.rdoColumns(2).Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns(2).Value
                End If
                sqlstrincuest = sqlstrincuest & ")"
                objApp.rdoConnect.Execute sqlstrincuest, 64
            End If
            rstAselrepet.Close
            rstAselcuest.MoveNext
        Loop
        rstAselcuest.Close
        rstAselgrupo.MoveNext
    Loop
    rstAselgrupo.Close
    Set rstAselcuest = Nothing
    Set rstAselrepet = Nothing
    Set rstAselgrupo = Nothing

End Sub
Private Sub Crear_Muestras_Pedidas_Pet()
    Dim rstAselmuestras As rdoResultset
    Dim sqlstrselmuestras As String
    Dim sqlstrinmuestras As String
    Dim mlngcodmuestra As Long
    Dim mstrdesmuestra As String
    Dim mlngcodtipmuestra  As Long
    
    sqlstrselmuestras = "SELECT PR25CODMUESTRA, PR24CODTIPMUESTR, PR25DESMUESTRA FROM PR2500 WHERE PR01CODACTUACION=" & _
                      mtblactsel(2, mintindice - 1)
    Set rstAselmuestras = objApp.rdoConnect.OpenResultset(sqlstrselmuestras)
    
    Do Until rstAselmuestras.EOF
        sqlstrinmuestras = "INSERT INTO PR4200 (PR03NUMACTPEDI, PR42NUMMUESTRA, PR24CODTIPMUESTR, PR42DESMUESTRA) VALUES (" & mtblactsel(6, mintindice - 1)
        
        If IsNull(rstAselmuestras.rdoColumns(0).Value) Then
            sqlstrinmuestras = sqlstrinmuestras & ",null"
        Else
            sqlstrinmuestras = sqlstrinmuestras & "," & rstAselmuestras.rdoColumns(0).Value
        End If
        If IsNull(rstAselmuestras.rdoColumns(1).Value) Then
            sqlstrinmuestras = sqlstrinmuestras & ",null"
        Else
            sqlstrinmuestras = sqlstrinmuestras & "," & rstAselmuestras.rdoColumns(1).Value
        End If
        If IsNull(rstAselmuestras.rdoColumns(2).Value) Then
            sqlstrinmuestras = sqlstrinmuestras & ",null"
        Else
            sqlstrinmuestras = sqlstrinmuestras & ",'" & rstAselmuestras.rdoColumns(2).Value & "'"
        End If
        sqlstrinmuestras = sqlstrinmuestras & ")"
        objApp.rdoConnect.Execute sqlstrinmuestras, 64

        rstAselmuestras.MoveNext
    Loop
    rstAselmuestras.Close
    Set rstAselmuestras = Nothing
End Sub
Private Sub Crear_Interacciones_Pet()
    Dim mlngcodactorigen As Long
    Dim mlngcodactdestino As Long
    Dim sqlstrselinter As String
    Dim sqlstrininter As String
    Dim rstaselinter As rdoResultset
    Dim sqlstrselactdes As String
    Dim rstaselactdes As rdoResultset
    Dim sqlstrselcod As String
    'Dim rstaselcod As rdoResultset
    
    
    'Call Buscar_Actuaciones(mlngcodactorigen, mlngcodactdestino)
    
    sqlstrselactdes = "SELECT PR03NUMACTPEDI FROM PR0800 WHERE PR09NUMPETICION=" & _
                        txtText1(0).Text & " AND PR08NUMSECUENCIA=1"

    Set rstaselactdes = objApp.rdoConnect.OpenResultset(sqlstrselactdes)
    
    Do Until rstaselactdes.EOF
    
    
        mlngcodactorigen = mtblactsel(2, mintindice - 1)
        mlngcodactdestino = mtblactsel(6, mintindice - 1)
        
        sqlstrselinter = "SELECT PR19CODTIPINTERAC, PR18NUMDEFINTER FROM PR2000 WHERE PR01CODACTUACION=" & _
                            mlngcodactorigen & " AND PR01CODACTUACION_DES=" & _
                            mlngcodactdestino
        Set rstaselinter = objApp.rdoConnect.OpenResultset(sqlstrselinter)
    
        If rstaselinter.EOF = False Then
            sqlstrininter = "INSERT INTO PR3900 " _
                            & "(PR03NUMACTPEDI, PR03NUMACTPEDI_DES, PR19CODTIPINTERAC,PR39NUMMININTER) " _
                            & " VALUES ("
            
            If IsNull(mtblactsel(6, mintindice - 1)) Then
                sqlstrininter = sqlstrininter & "null"
            Else
                sqlstrininter = sqlstrininter & mtblactsel(6, mintindice - 1)
            End If
            
            If IsNull(rstaselactdes.rdoColumns(0).Value) Then
                sqlstrininter = sqlstrininter & ",null"
            Else
                sqlstrininter = sqlstrininter & "," & rstaselactdes.rdoColumns(0).Value
            End If
            
            If IsNull(rstaselinter.rdoColumns(2).Value) Then
                sqlstrininter = sqlstrininter & ",null"
            Else
                sqlstrininter = sqlstrininter & "," & rstaselinter.rdoColumns(2).Value
            End If
            
            If IsNull(rstaselinter.rdoColumns(4).Value) Then
                sqlstrininter = sqlstrininter & ",null"
            Else
                sqlstrininter = sqlstrininter & "," & rstaselinter.rdoColumns(4).Value
            End If
            
            sqlstrininter = sqlstrininter & ")"
            objApp.rdoConnect.Execute sqlstrininter, 64
        End If
        rstaselinter.Close
        rstaselactdes.MoveNext
    Loop
    rstaselactdes.Close
    Set rstaselinter = Nothing
    Set rstaselactdes = Nothing
    'Set rstaselcod = Nothing
End Sub
Private Sub Incrementar_Solicitadas()
    Dim sqlstrselactsol As String
    Dim rstAselactsol As rdoResultset
    Dim sqlstrinactsol As String
    Dim sqlstrupactsol As String
    
    sqlstrselactsol = "SELECT * FROM PR4400 WHERE AD02CODDPTO=" & txtText1(1).Text & _
                        " AND PR01CODACTUACION=" & mtblactsel(2, mintindice - 1) & _
                        " AND AD02CODDPTO_REA=" & mtblactsel(4, mintindice - 1)
    Set rstAselactsol = objApp.rdoConnect.OpenResultset(sqlstrselactsol)
    If rstAselactsol.EOF = True Then
        sqlstrinactsol = "INSERT INTO PR4400 " _
                          & " (AD02CODDPTO,PR01CODACTUACION,AD02CODDPTO_REA,PR44NUMCUENTA) VALUES " _
                          & " (" & txtText1(1).Text & "," & mtblactsel(2, mintindice - 1) & "," & mtblactsel(4, mintindice - 1) & ",1)"
        objApp.rdoConnect.Execute sqlstrinactsol, 64
    Else
        sqlstrupactsol = "UPDATE PR4400 SET PR44NUMCUENTA=" & rstAselactsol.rdoColumns("PR44NUMCUENTA") + 1 _
           & " WHERE AD02CODDPTO=" & txtText1(1).Text & _
             " AND PR01CODACTUACION=" & mtblactsel(2, mintindice - 1) & _
             " AND AD02CODDPTO_REA=" & mtblactsel(4, mintindice - 1)
        objApp.rdoConnect.Execute sqlstrupactsol, 64
    End If
    rstAselactsol.Close
    Set rstAselactsol = Nothing
End Sub
Private Sub cmdFormularPet_Click()
  Dim sqlstrselfases As String
  Dim rstaselfases As rdoResultset
  Dim strsql As String
  Dim rsta As rdoResultset
  Dim strsqlact As String
  Dim rstact As rdoResultset
  Dim intResp As Integer
  Dim sqlstrorigen As String
  Dim rstorigen As rdoResultset
  Dim ir_siguiente As Boolean
  Dim mintContes As Integer
  Dim mblnPlanif As Boolean
  Dim sqlCitadas As String
  Dim rstCitadas As rdoResultset
  Dim fecha As String
  Dim mensaje As Boolean
  
  Dim strsql900 As String
  Dim rsta900 As rdoResultset
  Dim sqlInstrReal As String
  Dim rstInstrReal As rdoResultset
  
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  mensaje = False
  strsqlact = "SELECT COUNT(*) FROM PR0300 WHERE PR09NUMPETICION=" & txtText1(0).Text
  Set rstact = objApp.rdoConnect.OpenResultset(strsqlact)
  If rstact(0).Value > 0 Then
      ir_siguiente = False
      cmdFormularPet.Enabled = False
      Picture1.Visible = True
      Frame2.Visible = True
      Call Form_Paint
      'On Error GoTo Err_Ejecutar
      If txtText1(0).Text <> "" Then
          strsql = "SELECT COUNT(*) FROM PR0400 WHERE PR03NUMACTPEDI IN " & _
                    "(SELECT PR03NUMACTPEDI FROM PR0300 WHERE PR09NUMPETICION=" & txtText1(0).Text & ")"
          Set rsta = objApp.rdoConnect.OpenResultset(strsql)
          If rsta.rdoColumns(0).Value > 0 Then
            mblnPlanif = True
            mintContes = MsgBox("La Petici�n tiene Actuaciones planificadas. �Desea replanificarlas?", vbExclamation + vbYesNo)
          Else
            mblnPlanif = False
          End If
          mintindice = 1
          Do While mintindice <= gintfinindice
            If mtblactsel(0, mintindice - 1) = True And mtblactsel(1, mintindice - 1) = True Then
                If mtblactsel(6, mintindice - 1) <> "" Then
                  sqlstrorigen = "SELECT * FROM PR0300 WHERE pr03numactpedi=" & mtblactsel(6, mintindice - 1)
                  Set rstorigen = objApp.rdoConnect.OpenResultset(sqlstrorigen)
                  If rstorigen.EOF = False Then
                    'mblnNueva = True
                  '***************************************************************jcr 3/8/98
                    sqlInstrReal = "SELECT * FROM PR0100 WHERE PR01CODACTUACION=" & mtblactsel(2, mintindice - 1)
                    Set rstInstrReal = objApp.rdoConnect.OpenResultset(sqlInstrReal)
                  '***************************************************************
                    sqlstrselfases = "SELECT * FROM PR0600 WHERE PR03NUMACTPEDI=" & _
                                      mtblactsel(6, mintindice - 1)
                
                    Set rstaselfases = objApp.rdoConnect.OpenResultset(sqlstrselfases)
                    
                    If rstaselfases.EOF Then
                        Call Crear_Fases_Pedidas_Pet
                        Call Crear_Cuestionario_Peticion_Pet
                        Call Crear_Cuestionario_Grupo_Peticion_Pet
                        'Crear las muestras de las actuaciones
                        'Call Crear_Muestras_Pedidas_Pet
                        'Crear las interacciones de las actuaciones
                        Call Crear_Interacciones_Pet
                        'Crear o incrementar las actuaciones m�s solicitadas por departamento
                        Call Incrementar_Solicitadas
                        Call crear_restricciones
                    End If
                    '**************************************************************jcr3/8/98
                    If rstInstrReal.EOF = False Then
                      If rstInstrReal("PR01INDINSTRREA").Value = -1 Then
                        sqlInstrReal = "UPDATE PR0300 SET PR03indcitable=0 WHERE pr03numactpedi=" & mtblactsel(6, mintindice - 1)
                        objApp.rdoConnect.Execute sqlInstrReal, 64
                      End If
                    End If
                    rstInstrReal.Close
                    Set rstInstrReal = Nothing
                    '***************************************************************
                    If Es_Posible Then
                      If mblnPlanif = True Then  'Alguna actuaci�n planificada
                        sqlCitadas = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
                        Set rstCitadas = objApp.rdoConnect.OpenResultset(sqlCitadas)
                        If rstCitadas.EOF = False Then 'Se ha Planificado
                            If mintContes = vbYes Then   'Se desea Replanificar
                              If rstCitadas("pr37codestado").Value = 1 Then   'No est� Citada
                                Call Borrar_Planificacion
                                Call Crear_Planificacion
                                mensaje = True
                              End If
                            End If
                        Else     'No se ha planificado
                            Call Crear_Planificacion
                            mensaje = True
                        End If
                      Else    'Ninguna Actuaci�n planificada
                        Call Crear_Planificacion
                        mensaje = True
                      End If
                    Else
                      strsql900 = "SELECT * FROM PR0900 WHERE PR09NUMPETICION=" & txtText1(0).Text
                      Set rsta900 = objApp.rdoConnect.OpenResultset(strsql900)
                      If IsNull(rsta900.rdoColumns("PR09FECPLANIFIC").Value) Then
                        fecha = "NULL"
                      Else
                        If Left(Right(rsta900.rdoColumns("PR09FECPLANIFIC").Value, 3), 1) = "." Then
                          fecha = Left(rsta900.rdoColumns("PR09FECPLANIFIC").Value, Len(rsta900.rdoColumns("PR09FECPLANIFIC")) - 3)
                          fecha = "to_date('" & fecha & "','dd/mm/yyyy hh24:mi')"
                        Else
                          fecha = rsta900.rdoColumns("PR09FECPLANIFIC").Value
                          fecha = "to_date('" & fecha & "','dd/mm/yyyy')"
                        End If
                      End If
                      strsqlact = "UPDATE PR0300 SET PR03FECPREFEREN=" & fecha & " WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
                      objApp.rdoConnect.Execute strsqlact, 64
                      rsta900.Close
                      Set rsta900 = Nothing
                      ir_siguiente = True
                    End If
    
                    rstaselfases.Close
                  Else
                      '*********+
                      sqlstrselfases = "SELECT * FROM PR0600 WHERE PR03NUMACTPEDI=" & _
                                          mtblactsel(6, mintindice - 1)
                        Set rstaselfases = objApp.rdoConnect.OpenResultset(sqlstrselfases)
                        If rstaselfases.EOF Then
                            'Crear o incrementar las actuaciones m�s solicitadas por departamento
                            Call Incrementar_Solicitadas
                        End If
                      '*********
                      rstaselfases.Close
                  End If
                  rstorigen.Close
                End If
            End If
            mintindice = mintindice + 1
          Loop
          
           Frame2.Visible = False
           Picture1.Visible = False
           
          If mintindice > 1 Then
            If ir_siguiente = True Then
              frmPlanifActuaciones.txtText1(0).Text = IdPersona1.Text
              frmPlanifActuaciones.txtText1(1).Text = IdPersona1.Historia
              frmPlanifActuaciones.txtText1(2).Text = IdPersona1.Dni
              frmPlanifActuaciones.txtText1(3).Text = IdPersona1.Nombre
              frmPlanifActuaciones.txtText1(4).Text = IdPersona1.Apellido1
              frmPlanifActuaciones.txtText1(5).Text = IdPersona1.Apellido2
              Call objsecurity.LaunchProcess("PR0199")
            Else
              If mensaje = True Then
                Call MsgBox("Todas las Actuaciones de la petici�n se han planificado correctamente", vbInformation)
              End If
            End If
          Else
            Call MsgBox("La petici�n no tiene actuaciones", vbInformation)
          End If
    
    
      End If
      rsta.Close
     Set rsta = Nothing
     Set rstaselfases = Nothing
     Set rstorigen = Nothing
     cmdFormularPet.Enabled = True
    Else
      Call MsgBox("La Petici�n no tiene Actuaciones Seleccionadas", vbExclamation)
    End If
     rstact.Close
     Set rstact = Nothing
    'Err_Ejecutar:
    '  cmdFormularPet.Enabled = True
    '  Exit Sub

End Sub

Private Sub cmdInstruccionesPaciente_Click()
  Dim strsql As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
 
  cmdInstruccionesPaciente.Enabled = False
  On Error GoTo Err_Ejecutar
  If txtText1(0).Text <> "" Then
    strsql = "SELECT COUNT(*) FROM PR0300 " _
           & "WHERE PR0300.PR09NUMPETICION = " & txtText1(0).Text _
           & "  AND PR0300.PR01CODACTUACION IN " _
           & "(SELECT PR01CODACTUACION FROM PR0100 " _
           & " WHERE (NOT (PR01DESINSTRPACI IS NULL)) )"
         
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.rdoColumns(0).Value > 0 Then
      frmlanzarfolletos.txtCabecera(0).Text = txtText1(9).Text 'N�mero de grupo
      frmlanzarfolletos.txtCabecera(1).Text = txtText1(0).Text 'N�mero de petici�n
      frmlanzarfolletos.txtCabecera(2).Text = IdPersona1.Dni 'D.N.I. del paciente
      frmlanzarfolletos.txtCabecera(3).Text = IdPersona1.Nombre 'Nombre del paciente
      frmlanzarfolletos.txtCabecera(4).Text = IdPersona1.Apellido1 'Apellido del paciente
      Call objsecurity.LaunchProcess("PR0140")
    Else
      intResp = MsgBox("La petici�n no tiene actuaciones con Instrucciones para el paciente", vbInformation)
    End If
    rsta.Close
    Set rsta = Nothing
  End If
 
Err_Ejecutar:
  cmdInstruccionesPaciente.Enabled = True
  Exit Sub
End Sub
Private Sub cmdConsentimientoPaciente_Click()
Dim strsql As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
 
  cmdConsentimientoPaciente.Enabled = False
  On Error GoTo Err_Ejecutar
  If txtText1(0).Text <> "" Then
    strsql = "SELECT COUNT(*) FROM PR0300 " _
           & "WHERE PR0300.PR09NUMPETICION = " & txtText1(0).Text _
           & " AND (PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR0100 WHERE (PR01INDCONSFDO = 1) OR (PR01INDCONSFDO = -1)))"
           
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.rdoColumns(0).Value > 0 Then
      frmlanzarconsentimiento.txtCabecera(0).Text = txtText1(9).Text
      frmlanzarconsentimiento.txtCabecera(1).Text = txtText1(0).Text
      frmlanzarconsentimiento.txtCabecera(2).Text = IdPersona1.Dni
      frmlanzarconsentimiento.txtCabecera(3).Text = IdPersona1.Nombre
      frmlanzarconsentimiento.txtCabecera(4).Text = IdPersona1.Apellido1
      Call objsecurity.LaunchProcess("PR0141")
    Else
      intResp = MsgBox("No es necesario el Consentimiento del paciente", vbInformation)
    End If
    rsta.Close
    Set rsta = Nothing
  End If
 
Err_Ejecutar:
  cmdConsentimientoPaciente.Enabled = True
  Exit Sub
End Sub

Private Sub cmdLanzarPrograma_Click()
   cmdLanzarPrograma.Enabled = False
   gblnDesdePedir = True
   frmlanzarprograma.txtText1(1).Text = IdPersona1.Text ' C�digo de persona
   frmlanzarprograma.txtText1(0).Text = IdPersona1.Historia
   frmlanzarprograma.txtText1(2).Text = IdPersona1.Dni
   frmlanzarprograma.txtText1(3).Text = IdPersona1.Nombre
   frmlanzarprograma.txtText1(4).Text = IdPersona1.Apellido1
   frmlanzarprograma.txtText1(5).Text = IdPersona1.Apellido2
   Call objsecurity.LaunchProcess("PR0142")
   cmdLanzarPrograma.Enabled = True
   gblnDesdePedir = False
End Sub

Private Sub cmdseleccionar_Click()
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataSave
  Call Comprobar_Medico
  If mblnError = False Then
    If campos_obligatorios Then
      cmdseleccionar.Enabled = False
      frmSeleccionarAct.txtText1(0).Text = IdPersona1.Text
      frmSeleccionarAct.txtText1(1).Text = IdPersona1.Historia
      frmSeleccionarAct.txtText1(2).Text = IdPersona1.Dni
      frmSeleccionarAct.txtText1(3).Text = IdPersona1.Nombre
      frmSeleccionarAct.txtText1(4).Text = IdPersona1.Apellido1
      frmSeleccionarAct.txtText1(5).Text = IdPersona1.Apellido2
      Call objsecurity.LaunchProcess("PR0153")
      Call Inicializar_Array
      cmdseleccionar.Enabled = True
    Else
      cmdseleccionar.Enabled = False
    End If
  Else
    cmdseleccionar.Enabled = False
  End If
    
End Sub

Private Sub Form_Activate()
  mblnError = False
  Select Case gintEstadoPeticion
  Case 1
       'se pasa el grupo. El foco debe posicionarse en c�d.departamento
       SendKeys "{TAB}", True
       SendKeys "{TAB}", True
       SendKeys "{TAB}", True
       SendKeys "{TAB}", True
       
       'txtText1(1).Locked = False
       'txtText1(2).Locked = False
       'txtText1(1).SetFocus
       'objWinInfo.objWinActiveForm.blnChanged = True
       'tlbToolbar1.Buttons(4).Enabled = True
  Case 2
       'no se pasa ni petici�n ni grupo, se generan autom�ticamente. El foco
       'se queda en c�d.departamento
       SendKeys "{TAB}", True
       SendKeys "{TAB}", True
       SendKeys "{TAB}", True
       SendKeys "{TAB}", True
       
       'txtText1(1).Locked = False
       'txtText1(2).Locked = False
       'txtText1(1).SetFocus
  Case 3
       'se pasa petici�n y grupo. Se refresca la pantalla para que active los
       'botones y cargue el registro
       If mblnfoco = True Then
          SendKeys "{TAB}", True
          
          'txtText1(9).SetFocus
       End If
  End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim blnDesdeCitas As Boolean
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  
  'Call objApp.AddCtrl(TypeName(IdPersona1))
  'Call IdPersona1.Init(objApp, objGen)
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Select Case gintEstadoPeticion
    Case 3:
      'Peticion y grupo ya creados
        Call objWinInfo.WinCreateInfo(cwModeSingleOpen, _
                                      Me, tlbToolbar1, stbStatusBar1, _
                                      cwWithAll)
        objDetailInfo.strWhere = "CI21CODPERSONA=" & glngcodigopersona
    Case 1:
      'Peticion no creada pero grupo si
        Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
        objDetailInfo.strWhere = "CI21CODPERSONA=" & glngcodigopersona
    Case 2:
      'Se quiere crear nueva peticion y nuevo grupo
        Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
        objDetailInfo.strWhere = "CI21CODPERSONA=" & glngcodigopersona
    Case Else
        Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                      Me, tlbToolbar1, stbStatusBar1, _
                                      cwWithAll)
  End Select
  With objDetailInfo
    .strName = "Peticiones"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0900" 'Petici�n
    '.strWhere = "CI21CODPERSONA=" & glngcodigopersona
    Call .FormAddOrderField("PR09NUMPETICION", cwAscending)
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones")
    Call .FormAddFilterWhere(strKey, "PR09NUMLOTE", "N�mero de Grupo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR09NUMPETICION", "N�mero de Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR09FECPETICION", "Fecha de Petici�n", cwDate)
    Call .FormAddFilterWhere(strKey, "PR09DESOBSERVAC", "Observaciones", cwString)
    Call .FormAddFilterWhere(strKey, "PR09DESINDICACIO", "Indicaciones", cwString)
    Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "Actuaci�n Planificada", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "PR09MOTPETCION", "Motivo", cwString)

    Call .FormAddFilterOrder(strKey, "PR09NUMLOTE", "N�mero de Grupo")
    Call .FormAddFilterOrder(strKey, "PR09NUMPETICION", "N�mero de Petici�n")
    Call .FormAddFilterOrder(strKey, "PR09FECPETICION", "Fecha de Petici�n")
    Call .FormAddFilterOrder(strKey, "PR09DESOBSERVAC", "Observaciones")
    Call .FormAddFilterOrder(strKey, "PR09DESINDICACIO", "Indicaciones")
    Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "Actuaci�n Planificada")
    'Call .FormAddFilterOrder(strKey, "PR09MOTPETCION", "Motivo")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    '.CtrlGetInfo(txtText1(6)).blnInFind = True
    
    .CtrlGetInfo(dtcfecha).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    '.CtrlGetInfo(txtcajadpto).blnNegotiated = False
    '.CtrlGetInfo(txtcajapersona).blnNegotiated = False
    
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "ad02coddpto", "SELECT ad02coddpto,ad02desdpto FROM AD0200 WHERE ad02coddpto = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "ad02desdpto")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "SG02COD", "SELECT SG02COD,SG02NOM,SG02APE1,SG02APE2 FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(6), "SG02NOM")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(7), "SG02APE1")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(8), "SG02APE2")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "PR04NUMACTPLAN", "SELECT PR01DESCORTA FROM PR0100,PR0300,PR0400 WHERE " & _
                "PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND PR0300.PR01CODACTUACION=PR0100.PR01CODACTUACION AND PR04NUMACTPLAN = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(6), "PR01DESCORTA")


    'IdPersona1.ToolTipText = ""
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Select Case gintEstadoPeticion
    Case 3:
      'Peticion y grupo ya creados
       Call objWinInfo.CtrlSet(txtText1(0), glngnumeropeticion)
       'Call objWinInfo.CtrlSet(txtText1(9), glngnumerogrupo)
    Case 1:
      'Peticion no creada pero grupo si
       Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
        sqlstr = "SELECT PR09NUMPETICION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
        Call objWinInfo.CtrlSet(txtText1(9), glngnumerogrupo)
        'IdPersona1.SetFocus
        Set rsta = Nothing
    Case 2:
  
      Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
      
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        sqlstr = "SELECT PR09NUMPETICION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
        'txtText1(0).Locked = True
        Call objWinInfo.CtrlSet(txtText1(9), rsta.rdoColumns(0).Value)
        'IdPersona1.SetFocus
        'Call objWinInfo.FormChangeStatus(cwModeSingleAddRest)
        'txtText1(9).SetFocus
        'IdPersona1.Text = ""
        rsta.Close
      Set rsta = Nothing
  End Select
  If gintEstadoPeticion <> 0 Then
    IdPersona1.Text = glngcodigopersona
  End If
  'txtText1(9).SetFocus
  'Call objApp.SplashOff
  
  cmdCitar.Enabled = False
  cmdConsentimientoPaciente.Enabled = False
  cmdFormularPet.Enabled = False
  cmdCambPlanific.Enabled = False
  cmdInstruccionesPaciente.Enabled = False
  cmdLanzarPrograma.Enabled = False
  cmdseleccionar.Enabled = False
  mblnfoco = True
  Exit Sub
     
Err_Ejecutar:
      Exit Sub
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_Paint()
  Frame2.Refresh
  Picture1.Refresh
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  If gintEstadoPeticion <> 0 Then
    glngnumerogrupo = txtText1(9).Text
    glngnumeropeticion = txtText1(0).Text
    End If
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Peticiones" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "where (AD02INDCAMA=-1 OR AD02INDRESPONPROC=-1) AND (AD02CODDPTO IN " & _
                "(SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "'))"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
     
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo del Departamento"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n del Departamento"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("pr01codactuacion"))
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
  If strFormName = "Peticiones" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "SG0200"
     If txtText1(1).Text <> "" Then
      .strWhere = "where SG02COD IN (SELECT SG02COD FROM AD0300 WHERE AD02CODDPTO=" & txtText1(1).Text & ")"
     End If
     .strOrder = "ORDER BY SG02COD ASC"
     
     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo del Doctor"
         
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Nombre del Doctor"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("pr01codactuacion"))
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
    If gintpadre = 1 Then
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(dtcDateCombo1(0)), Date)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), Date)
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(IdPersona1), frmpruebasadicionales.txtText1(25).Text)
        Call objWinInfo.CtrlSet(IdPersona1, frmpruebasadicionales.txtText1(25).Text)
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(1)), frmpruebasadicionales.txtText1(11).Text)
        Call objWinInfo.CtrlSet(txtText1(1), frmpruebasadicionales.txtText1(11).Text)
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), frmpruebasadicionales.txtText1(10).Text)
        Call objWinInfo.CtrlSet(txtText1(2), frmpruebasadicionales.txtText1(10).Text)
    End If
End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  intcambioalgo = 0
End Sub

Private Sub objWinInfo_cwPreChangeStatus(ByVal strFormName As String, ByVal intOldStatus As CodeWizard.cwFormStatus, ByVal intNewStatus As CodeWizard.cwFormStatus)


  If intNewStatus = cwModeSingleAddRest Then
    cmdseleccionar.Enabled = False
  Else
    If campos_obligatorios Then
      cmdseleccionar.Enabled = True
    End If
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Peticiones" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim strSelect
  Dim strmensaje As String

  Dim fecha As String
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
    Call Comprobar_Medico
    If btnButton.Index = 2 And gintEstadoPeticion = 0 Then
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
    End If
    If btnButton.Index = 8 Then
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
    End If

  'Localizar
  If btnButton.Index = 16 Then
    objWinInfo.DataRefresh
  End If
  If btnButton.Index <> 2 Then
    If btnButton.Index = 4 Then
        If Right(txtText1(23).Text, 1) = "." Then
            txtText1(23).Text = Left(txtText1(23).Text, Len(txtText1(23).Text) - 1)
        End If
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Else
    If gintEstadoPeticion = 0 Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Else
      Call MsgBox("Tiene que dar de alta una nueva Petici�n desde Citas", vbInformation, "Aviso")
    End If
  End If
  
  If btnButton.Index = 2 And gintEstadoPeticion = 0 Then
    On Error GoTo Err_Ejecutar
    ' generaci�n autom�tica del c�digo
    sqlstr = "SELECT PR09NUMPETICION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    txtText1(0).Locked = True
    txtText1(9).SetFocus
    txtText1(0).SetFocus
    Call objWinInfo.CtrlSet(txtText1(9), rsta.rdoColumns(0).Value)
    SendKeys "{TAB}", True
    'IdPersona1.SetFocus
    
    
    '*************
    'txtcajapersona.Text = 0
    'txtcajapersona.SetFocus
    'txtText1(0).SetFocus
    'txtcajapersona.Text = 0
    '**************
    rsta.Close
  End If
  Set rsta = Nothing
  
  If btnButton.Index = 16 Then
    Call txtText1_Change(23)
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
    If btnButton.Index <> 2 And btnButton.Index <> 30 Then
      If campos_obligatorios Then
        cmdCitar.Enabled = True
        cmdConsentimientoPaciente.Enabled = True
        cmdFormularPet.Enabled = True
        cmdCambPlanific.Enabled = True
        cmdInstruccionesPaciente.Enabled = True
        cmdLanzarPrograma.Enabled = True
        cmdseleccionar.Enabled = True
      Else
        cmdCitar.Enabled = False
        cmdConsentimientoPaciente.Enabled = False
        cmdFormularPet.Enabled = False
        cmdCambPlanific.Enabled = False
        cmdInstruccionesPaciente.Enabled = False
        cmdLanzarPrograma.Enabled = False
        cmdseleccionar.Enabled = False
      End If
    End If
    
 Exit Sub
 
Err_Ejecutar:
  Exit Sub
End Sub
Private Function campos_obligatorios() As Boolean

    If txtText1(0).Text = "" Or txtText1(1).Text = "" Or txtText1(2).Text = "" Or txtText1(9).Text = "" Or IdPersona1.Text = "" Then
        campos_obligatorios = False
    Else
        campos_obligatorios = True
    End If

End Function

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  If intIndex = 10 And gintEstadoPeticion = 0 Then
    cmdCitar.Enabled = False
    cmdConsentimientoPaciente.Enabled = False
    cmdFormularPet.Enabled = False
    cmdCambPlanific.Enabled = False
    cmdInstruccionesPaciente.Enabled = False
    cmdLanzarPrograma.Enabled = False
    cmdseleccionar.Enabled = False
  End If
  
  If intIndex <> 10 Then
    If intIndex = 40 Then
        If Right(txtText1(23).Text, 1) = "." Then
            txtText1(23).Text = Left(txtText1(23).Text, Len(txtText1(23).Text) - 1)
        End If
    End If
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  Else
    If gintEstadoPeticion = 0 Then
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Else
      Call MsgBox("Tiene que dar de alta una nueva Petici�n desde Citas", vbInformation, "Aviso")
    End If
  End If
  
  If intIndex = 10 And gintEstadoPeticion = 0 Then
    On Error GoTo Err_Ejecutar
    ' generaci�n autom�tica del c�digo
    sqlstr = "SELECT PR09NUMPETICION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    txtText1(0).Locked = True
    txtText1(9).SetFocus
    txtText1(0).SetFocus
    Call objWinInfo.CtrlSet(txtText1(9), rsta.rdoColumns(0).Value)
    SendKeys "{TAB}", True
    'IdPersona1.SetFocus
    rsta.Close
  End If
  Set rsta = Nothing
  
  If intIndex <> 10 And intIndex <> 100 Then
    If campos_obligatorios Then
        cmdCitar.Enabled = True
        cmdConsentimientoPaciente.Enabled = True
        cmdFormularPet.Enabled = True
        cmdCambPlanific.Enabled = True
        cmdInstruccionesPaciente.Enabled = True
        cmdLanzarPrograma.Enabled = True
        cmdseleccionar.Enabled = True
    Else
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
    End If
  End If
    
 Exit Sub
 
Err_Ejecutar:
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  If campos_obligatorios Then
    cmdCitar.Enabled = True
    cmdConsentimientoPaciente.Enabled = True
    cmdFormularPet.Enabled = True
    cmdCambPlanific.Enabled = True
    cmdInstruccionesPaciente.Enabled = True
    cmdLanzarPrograma.Enabled = True
    cmdseleccionar.Enabled = True
  Else
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
  End If
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  If campos_obligatorios Then
    cmdCitar.Enabled = True
    cmdConsentimientoPaciente.Enabled = True
    cmdFormularPet.Enabled = True
    cmdCambPlanific.Enabled = True
    cmdInstruccionesPaciente.Enabled = True
    cmdLanzarPrograma.Enabled = True
    cmdseleccionar.Enabled = True
  Else
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
  End If
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  'Localizar
  If intIndex = 10 Then
    objWinInfo.DataRefresh
  End If
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  If intIndex = 10 Then
    Call txtText1_Change(23)
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  If campos_obligatorios Then
    cmdCitar.Enabled = True
    cmdConsentimientoPaciente.Enabled = True
    cmdFormularPet.Enabled = True
    cmdCambPlanific.Enabled = True
    cmdInstruccionesPaciente.Enabled = True
    cmdLanzarPrograma.Enabled = True
    cmdseleccionar.Enabled = True
  Else
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  If campos_obligatorios Then
    cmdCitar.Enabled = True
    cmdConsentimientoPaciente.Enabled = True
    cmdFormularPet.Enabled = True
    cmdCambPlanific.Enabled = True
    cmdInstruccionesPaciente.Enabled = True
    cmdLanzarPrograma.Enabled = True
    cmdseleccionar.Enabled = True
  Else
      cmdCitar.Enabled = False
      cmdConsentimientoPaciente.Enabled = False
      cmdFormularPet.Enabled = False
      cmdCambPlanific.Enabled = False
      cmdInstruccionesPaciente.Enabled = False
      cmdLanzarPrograma.Enabled = False
      cmdseleccionar.Enabled = False
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub





Private Sub txthora_Change()
Dim fecha As String

  
  
If IsNumeric(txthora.Text) = True Then
  If txthora.Text > 23 Then
    Beep
    txthora.Text = ""
  End If
  If dtcfecha.Text <> "" Then
     If (txthora.Text <> "" And txtminuto <> "") Then
       fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
        objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     Else
       fecha = dtcfecha.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
        objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     End If
 End If
Else
 txthora.Text = ""
End If
'If txtminuto.Text = "" Then
'  txtminuto.Text = 0
'End If
End Sub


Private Sub txthora_KeyPress(KeyAscii As Integer)
intcambioalgo = 1
End Sub


Private Sub txtminuto_KeyPress(KeyAscii As Integer)
intcambioalgo = 1
mintminuto = 1
End Sub


Private Sub txtminuto_Change()
Dim fecha As String

  
If IsNumeric(txtminuto.Text) = True Then
  If txtminuto.Text > 59 Then
    Beep
    txtminuto.Text = ""
  End If
  If dtcfecha.Text <> "" Then
     If (txthora.Text <> "" And txtminuto <> "") Then
       fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
         objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     Else
       fecha = dtcfecha.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
          objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     End If
  End If
Else
  txtminuto.Text = ""
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  'Debug.Print ("Got_Focus:" & intIndex & "," & txtText1(intIndex).Tag)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 9 And gintEstadoPeticion = 3 And mblnfoco = True Then
    mblnfoco = False
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  End If
  'If intIndex = 0 Then
  '  If gintEstadoPeticion <> 0 Then
  '    IdPersona1.Text = glngcodigopersona
  '  End If
  '  Call objWinInfo.CtrlSet(txtText1(9), txtText1(0))
  '  txtText1(9).SetFocus
  'End If
End Sub

Private Sub Comprobar_Medico()
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim strMensage As String
  Dim intResp As Integer
  
    mblnError = False
    If (txtText1(1).Text <> "") And (txtText1(2).Text <> "") Then
      strSelect = "SELECT COUNT(*) FROM AD0300 " _
              & " WHERE AD02CODDPTO = " & txtText1(1).Text _
              & " AND SG02COD ='" & txtText1(2).Text & "'"
      Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
      If rsta.rdoColumns(0) = 0 Then
        Call MsgBox("Departamento y/o Doctor Err�neos.  ", vbInformation, "Aviso")
        Call objWinInfo.CtrlSet(txtText1(2), "")
        Call objWinInfo.CtrlSet(txtText1(1), "")
        mblnError = True
      End If
      rsta.Close
    Else
      mblnError = True
    End If
    Set rsta = Nothing
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim strMensage As String
  Dim intResp As Integer
  Dim hora As String
  
  If intIndex = 0 Then
     If txtText1(0).Text <> "" Then
        Call Inicializar_Array
     Else
      Dim mtblactsel()
     End If
  End If
   
  Call objWinInfo.CtrlDataChange
  
   If (intIndex = 6) And (txtText1(1).Text <> "") And (txtText1(2).Text <> "") Then
     strSelect = "SELECT COUNT(*) FROM AD0300 " _
              & " WHERE AD02CODDPTO = " & txtText1(1).Text _
              & " AND SG02COD ='" & txtText1(2).Text & "'"
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    If rsta.rdoColumns(0) = 0 Then
      'strMensage = txtText1(6).Text & " " & txtText1(7).Text & " " & txtText1(8).Text
      strMensage = txtText1(7).Text
      strMensage = "El doctor " & strMensage & " NO pertenece al departamento " & txtText1(5).Text
      'If (txtText1(6).Text = "") Or (txtText1(7).Text = "") Then
      If (txtText1(7).Text = "") Then
        strMensage = " El doctor NO pertenece al departamento"
      End If
      intResp = MsgBox(strMensage, vbInformation, "Importante")
    End If
    rsta.Close
    Set rsta = Nothing
  End If
 
  If intIndex = 9 Then
    strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    'MsgBox rstA.rdoColumns(0).Value
    dtcDateCombo1(0).Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
  'Sincronizar el IDUsuario, menos cuando sea nuevo
  'If (txtText1(10).Text <> "") And (intIndex = 10) And (blnSincroPaci = True) Then
  '  IdPersona1.Text = txtText1(10).Text
  'End If
  If intIndex = 0 Then
      intaux = 0
  End If
  If mintminuto = 1 Then
    mintminuto = 0
  Else
    If (intIndex = 23) Then 'fecha y hora de preferencia
      hora = txtText1(23).Text
      If hora <> "" Then
        If intaux = 0 Then
         strfechaaux = txtText1(23)
         intaux = 1
        End If
        dtcfecha.Text = Left(strfechaaux, 10)
        If Len(strfechaaux) > 10 Then
          txthora = Right(Left(strfechaaux, 13), 2)
          txtminuto = Right(Left(strfechaaux, 16), 2)
        Else
          txthora = ""
          txtminuto = ""
        End If
      Else
        dtcfecha.Text = ""
        txthora = ""
        txtminuto = ""
      End If
    End If
  End If
    
End Sub
Private Sub dtcfecha_Change()
 Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  grdDBGrid1(0).Enabled = False
  
      intcambioalgo = 1
      intaux = 0
      If dtcfecha.Text <> "" Then
        If (txthora.Text <> "" And txtminuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
End Sub

Private Sub dtcfecha_CloseUp()
  Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  grdDBGrid1(0).Enabled = False
  intcambioalgo = 1
  intaux = 0
  
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes sel systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txthora.Text <> "") Then
                If CDbl(txthora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txthora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtminuto.Text <> "") Then
                    If CDbl(txtminuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    If blnFechaValida = False Then
      intResp = MsgBox("La FECHA DE PLANIFICACI�N ha pasado", vbInformation, "Importante")
      dtcfecha.Text = ""
    Else
      If dtcfecha.Text <> "" Then
        If (txthora.Text <> "" And txtminuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing

End Sub


Private Sub dtcfecha_KeyPress(KeyAscii As Integer)
  mblnFechaModif = True
  intaux = 0
End Sub

Private Sub dtcfecha_Lostfocus()
  Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  
grdDBGrid1(0).Enabled = False

If mblnFechaModif = True Then
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes sel systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txthora.Text <> "") Then
                If CDbl(txthora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txthora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtminuto.Text <> "") Then
                    If CDbl(txtminuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    If blnFechaValida = False Then
      intResp = MsgBox("La FECHA DE PLANIFICACI�N ha pasado", vbInformation, "Importante")
    Else
      If dtcfecha.Text <> "" Then
        If (txthora.Text <> "" And txtminuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
              objWinInfo.objWinActiveForm.blnChanged = True
          End If
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
            objWinInfo.objWinActiveForm.blnChanged = True
          End If
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  
End If
mblnFechaModif = False
End Sub

Private Sub dtcfecha_Spin(OldDate As String, NewDate As String)
  mblnFechaModif = True
End Sub
'Private Function Es_Posible()
'  Dim strSelect As String
'  Dim rsta As rdoResultset
'  Dim strselect1 As String
'  Dim rstA1 As rdoResultset
'
'    strSelect = "SELECT COUNT(*) FROM PR2900 WHERE PR29INDROBLIG=-1 AND PR01CODACTUACION=" & mtblactsel(2, mintindice - 1)
'    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
'    If rsta(0).Value = 0 Then
'      strselect1 = "SELECT COUNT(*) FROM PR3000 WHERE PR30INDOBLIG=-1 AND PR16CODGRUPO IN (SELECT PR16CODGRUPO FROM PR1700 WHERE PR01CODACTUACION=" & mtblactsel(2, mintindice - 1) & ")"
'      Set rstA1 = objApp.rdoConnect.OpenResultset(strselect1)
'      If rstA1(0).Value = 0 Then
'        Es_Posible = True
'      Else
'        Es_Posible = False
'      End If
'    Else
'      Es_Posible = False
'    End If
'End Function

Private Function Es_Posible()
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim strSelect1 As String
  Dim rstA1 As rdoResultset

    strSelect = "SELECT count(*) FROM PR4100 WHERE PR41INDROBLIG=-1 AND PR41RESPUESTA IS NULL AND PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    If rsta(0).Value = 0 Then
      strSelect1 = "SELECT COUNT(*) FROM PR4700,AG1500,PR1400 WHERE " & _
                    "PR4700.PR09NUMPETICION=" & txtText1(0).Text & _
                    " AND PR4700.AG16CODTIPREST = AG1500.AG16CODTIPREST" & _
                    " AND AG1500.AG14CODTIPRECU = PR1400.AG14CODTIPRECU" & _
                    " AND PR1400.PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & _
                    " AND PR4700.PR47VALOR=' '"
      Set rstA1 = objApp.rdoConnect.OpenResultset(strSelect1)
      If rstA1(0).Value = 0 Then
        Es_Posible = True
      Else
        Es_Posible = False
      End If
    Else
      Es_Posible = False
    End If
End Function
Private Sub Crear_Planificacion()
Dim strinsert As String
'Dim strinsert07 As String
'Dim strRec As String
'Dim rstrec As rdoResultset
'Dim strinsert1000 As String
Dim strsql As String
Dim rsta As rdoResultset
'Dim strsql2 As String
'Dim rstA2 As rdoResultset
Dim strsql0300  As String
Dim rsta0300 As rdoResultset
Dim strsql0900  As String
Dim rsta0900 As rdoResultset
Dim fecha As String
Dim strsql2500 As String
Dim rstsql2500 As rdoResultset
Dim mintCodEstado As Integer
Dim rstasis As rdoResultset
Dim strasis As String
Dim rstdoc As rdoResultset
Dim strdoc As String

  strsql0300 = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
  Set rsta0300 = objApp.rdoConnect.OpenResultset(strsql0300)
  
  strdoc = "SELECT pr01indreqdoc FROM PR0100 WHERE PR01CODACTUACION=" & rsta0300.rdoColumns("PR01CODACTUACION").Value
  Set rstdoc = objApp.rdoConnect.OpenResultset(strdoc)
  
  strasis = "SELECT AD07CODPROCESO,AD01CODASISTENCI FROM PR0800 WHERE " & _
          "PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & " AND " & _
          "PR08NUMSECUENCIA=1"
  Set rstasis = objApp.rdoConnect.OpenResultset(strasis)
  
  strsql0900 = "SELECT TO_CHAR(PR09FECPLANIFIC,'DD/MM/YYYY HH24:MI') FROM PR0900 WHERE PR09NUMPETICION=" & txtText1(0).Text
  Set rsta0900 = objApp.rdoConnect.OpenResultset(strsql0900)
  If IsNull(rsta0900.rdoColumns(0).Value) Then
    fecha = "NULL"
  Else
    If Left(Right(rsta0900.rdoColumns(0).Value, 3), 1) = ":" Then
      fecha = rsta0900.rdoColumns(0).Value
      fecha = "to_date('" & fecha & "','dd/mm/yyyy hh24:mi')"
    Else
      fecha = rsta0900.rdoColumns(0).Value
      fecha = "to_date('" & fecha & "','dd/mm/yyyy')"
    End If
  End If
  strsql2500 = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION=" & mtblactsel(2, mintindice - 1)
  Set rstsql2500 = objApp.rdoConnect.OpenResultset(strsql2500)
  If rstsql2500(0).Value = 0 Then
    mintCodEstado = 0
  Else
    mintCodEstado = 1
  End If
  strsql = "SELECT PR04NUMACTPLAN_SEQUENCE.nextval FROM dual"
  Set rsta = objApp.rdoConnect.OpenResultset(strsql)
  strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
             "im01numdoc,pr04indpadre,pr56codestmues,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & rsta(0).Value & "," & mtblactsel(2, mintindice - 1) & "," & _
            mtblactsel(4, mintindice - 1) & "," & IdPersona1.Text & "," & "'" & rsta0300.rdoColumns("CI32CODTIPECON").Value & "'" & "," & "'" & rsta0300.rdoColumns("CI13CODENTIDAD").Value & "'" & ",1," & _
             mtblactsel(6, mintindice - 1) & "," & _
             fecha & ",null,null,null,null,null,null,null,null,null,null,null,null,"
   If mintCodEstado = 1 Then
    strinsert = strinsert & "1,"
   Else
    strinsert = strinsert & "null,"
   End If
   If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
   
    objApp.rdoConnect.Execute strinsert, 64
    
    strinsert = "UPDATE PR0300 SET PR03FECPREFEREN=" & fecha & " WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
    objApp.rdoConnect.Execute strinsert, 64
    
    '**************************************************************************jcr7/8/98
    'Esta estructura se crea al realizar
    '************************************************************************************
    
    'strsql2 = "SELECT * FROM PR0600 WHERE pr03numactpedi=" & mtblactsel(6, mintindice - 1)
    'Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)
    ''
    'While Not rstA2.EOF
    'strinsert07 = "INSERT INTO PR0700 " & _
    '                 "(pr04numactplan,pr07numfase,pr07desfase,pr07numminocupac," & _
    '                "pr07numfase_pre,pr07numminfpre,pr07nummaxfpre," & _
    '                "pr07indhabnatu,pr07indinifin) " & _
    '                " VALUES (" & rsta(0).Value & "," & rstA2.rdoColumns("pr06numfase").Value & "," & "'" & rstA2.rdoColumns("pr06desfase").Value & "'" & ","
   '
   ' If IsNull(rstA2.rdoColumns("pr06numminocupac").Value) Then
   '   strinsert07 = strinsert07 & "null" & ","
   ' Else
   '   strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminocupac").Value & ","
   ' End If
   ' If IsNull(rstA2.rdoColumns("pr06numfase_pre").Value) Then
   '   strinsert07 = strinsert07 & "null" & ","
   ' Else
   '   strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numfase_pre").Value & ","
   ' End If
   ' If IsNull(rstA2.rdoColumns("pr06numminfpre").Value) Then
   '   strinsert07 = strinsert07 & "null" & ","
   ' Else
   '   strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminfpre").Value & ","
   ' End If
   ' If IsNull(rstA2.rdoColumns("pr06nummaxfpre").Value) Then
   '   strinsert07 = strinsert07 & "null" & ","
   ' Else
   '   strinsert07 = strinsert07 & rstA2.rdoColumns("pr06nummaxfpre").Value & ","
   ' End If
   ' If IsNull(rstA2.rdoColumns("pr06indhabnatu").Value) Then
   '   strinsert07 = strinsert07 & "null" & ","
   ' Else
   '   strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indhabnatu").Value & ","
   ' End If
   ' If IsNull(rstA2.rdoColumns("pr06indinifin").Value) Then
   '   strinsert07 = strinsert07 & "null" & ")"
   ' Else
   '   strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indinifin").Value & ")"
   ' End If
   ' objApp.rdoConnect.Execute strinsert07, 64
   ' rstA2.MoveNext
   'Wend
  ''insertar en pr1000 RECURSO CONSUMIDO
  'strRec = "SELECT * FROM PR1400 WHERE pr03numactpedi=" & mtblactsel(6, mintindice - 1)
  'Set rstrec = objApp.rdoConnect.OpenResultset(strRec)
  'While Not rstrec.EOF
  'strinsert1000 = "INSERT INTO PR1000 " & _
  '              "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
  '              "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
  '              "VALUES (" & rsta(0).Value & "," & _
  '              rstrec.rdoColumns("pr06numfase").Value & "," & _
  '              rstrec.rdoColumns("pr14numnecesid").Value & ","
  'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ","
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ","
  'End If
  'If IsNull(rstrec.rdoColumns("pr14numunirec").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ","
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numunirec").Value & ","
  'End If
  'If IsNull(rstrec.rdoColumns("pr14numminocu").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ","
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numminocu").Value & ","
  'End If
  'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ")"
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ")"
  'End If
  'objApp.rdoConnect.Execute strinsert1000, 64
  'rstrec.MoveNext
  'Wend
  rstsql2500.Close
  rsta.Close
  'rstrec.Close
  'rstA2.Close
  rsta0300.Close
  Set rstsql2500 = Nothing
  Set rsta = Nothing
  'Set rstrec = Nothing
  'Set rstA2 = Nothing
  Set rsta0300 = Nothing
  rstasis.Close
  Set rstasis = Nothing
End Sub
Private Sub Borrar_Planificacion()
  Dim strdelete As String
  Dim sqlborrar As String
  Dim sqlstrsel As String
  Dim rstasel As rdoResultset
  
  sqlstrsel = "select pr52nummuestra from pr4200 where pr04numactplan=" & mtblactsel(6, mintindice - 1)
  Set rstasel = objApp.rdoConnect.OpenResultset(sqlstrsel)
  
  While rstasel.EOF = False
    sqlborrar = "DELETE FROM PR4300 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & ")"
    objApp.rdoConnect.Execute sqlborrar, 64
    'sqlborrar = "DELETE FROM PR4200 WHERE PR03NUMACTPEDI IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & ")"
    sqlborrar = "DELETE FROM PR4200 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & ")"
    objApp.rdoConnect.Execute sqlborrar, 64
    sqlborrar = "DELETE FROM PR5400 WHERE PR52NUMMUESTRA=" & rstasel(0).Value
    objApp.rdoConnect.Execute sqlborrar, 64
    sqlborrar = "DELETE FROM PR5200 WHERE PR52NUMMUESTRA=" & rstasel(0).Value
    objApp.rdoConnect.Execute sqlborrar, 64
    rstasel.MoveNext
  Wend
  
  
  'strdelete = "DELETE PR1000 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & ")"
  'objApp.rdoConnect.Execute strdelete, 64
  
  'strdelete = "DELETE PR0700 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1) & ")"
  'objApp.rdoConnect.Execute strdelete, 64
  
  strdelete = "DELETE PR0400 WHERE PR03NUMACTPEDI=" & mtblactsel(6, mintindice - 1)
  objApp.rdoConnect.Execute strdelete, 64
  
  rstasel.Close
  Set rstasel = Nothing
End Sub

Private Sub txtText1_LostFocus(Index As Integer)

  'Debug.Print ("Lost_Focus:" & Index & "," & txtText1(Index).Tag)
  Call objWinInfo.CtrlLostFocus
End Sub
