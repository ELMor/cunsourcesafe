VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{A7E3152D-9E00-11D1-9146-00C04FBB52E1}#1.0#0"; "idperson.ocx"
Begin VB.Form frmPeticPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Listar Actuaciones Pendientes"
   ClientHeight    =   8235
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11655
   ControlBox      =   0   'False
   Icon            =   "PR0137.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8235
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   0
      Top             =   840
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Width           =   11340
      Begin IdPerson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   2355
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7950
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   5535
      HelpContextID   =   90001
      Index           =   0
      Left            =   240
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2400
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   9763
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Actuaciones Pendientes"
      TabPicture(0)   =   "PR0137.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdConsEstPac"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdRecibirPaciente"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdCancelarPrueba"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdrecact(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "chksinfecha"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdinfadi(2)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Cola de Actuaciones"
      TabPicture(1)   =   "PR0137.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdrecact(1)"
      Tab(1).Control(1)=   "cmdEliminarCola"
      Tab(1).Control(2)=   "fraFrame1(2)"
      Tab(1).ControlCount=   3
      Begin VB.CommandButton cmdinfadi 
         Caption         =   "Informaci�n Actuaci�n"
         Height          =   375
         Index           =   2
         Left            =   8760
         TabIndex        =   17
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CheckBox chksinfecha 
         Caption         =   "Ver actuaciones sin fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7680
         TabIndex        =   16
         Top             =   480
         Width           =   2775
      End
      Begin VB.CommandButton cmdrecact 
         Caption         =   "Recursos Actuaci�n"
         Height          =   375
         Index           =   1
         Left            =   -68880
         TabIndex        =   15
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CommandButton cmdEliminarCola 
         Caption         =   "Deshacer Entrada"
         Height          =   375
         Left            =   -72360
         TabIndex        =   14
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CommandButton cmdrecact 
         Caption         =   "Recursos Actuaci�n"
         Height          =   375
         Index           =   0
         Left            =   6600
         TabIndex        =   13
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CommandButton cmdCancelarPrueba 
         Caption         =   "Cancelar Prueba"
         Height          =   375
         Left            =   4440
         TabIndex        =   12
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CommandButton cmdRecibirPaciente 
         Caption         =   "Recibir Paciente"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CommandButton cmdConsEstPac 
         Caption         =   "Estado Paciente"
         Height          =   375
         Left            =   2280
         TabIndex        =   10
         Top             =   4800
         Width           =   1935
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Cola de Actuaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4080
         Index           =   2
         Left            =   -74880
         TabIndex        =   8
         Top             =   480
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3585
            Index           =   1
            Left            =   120
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   360
            Width           =   10680
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18838
            _ExtentY        =   6324
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Actuaciones Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3960
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3465
            Index           =   0
            Left            =   120
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   360
            Width           =   10800
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19050
            _ExtentY        =   6112
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2130
         Index           =   2
         Left            =   -74880
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   90
         Width           =   10005
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17648
         _ExtentY        =   3757
         _StockProps     =   79
         Caption         =   "PACIENTES"
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPeticPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub chksinfecha_Click()
If chksinfecha.Value = 0 Then
  'objWinInfo.objWinActiveForm.strWhere = "AD02CODDPTO=" & glngdptologin & _
  '                                       " AND " & _
  '              "pr04numactplan in (select pr04numactplan from pr0400 where " & _
  '                              "pr03numactpedi in (select pr03numactpedi from pr0800 " & _
  '                                                  "where ad07codproceso is not null " & _
  '                                                  "and ad01codasistenci is not null))"
  objWinInfo.objWinActiveForm.strWhere = "AD07CODPROCESO IS NOT NULL " & _
                                         " AND AD01CODASISTENCI IS NOT NULL" & _
                                         " AND AD02CODDPTO=" & glngdptologin

  objWinInfo.DataRefresh
End If
If chksinfecha.Value = 1 Then
  'objWinInfo.objWinActiveForm.strWhere = "AD02CODDPTO=" & glngdptologin & _
  '                                       " AND FECHA is null AND " & _
  '                "pr04numactplan in (select pr04numactplan from pr0400 where " & _
  '                              "pr03numactpedi in (select pr03numactpedi from pr0800 " & _
  '                                                  "where ad07codproceso is not null " & _
  '                                                  "and ad01codasistenci is not null))"
  objWinInfo.objWinActiveForm.strWhere = "AD07CODPROCESO IS NOT NULL " & _
                                         " AND AD01CODASISTENCI IS NOT NULL" & _
                                         " AND AD02CODDPTO=" & glngdptologin & _
                                         " AND FECHA is null"
  objWinInfo.DataRefresh
End If
End Sub

Private Sub cmdCancelarPrueba_Click()
'Llama a la pantalla de cancelar actuaciones o anular las cancelaciones
    'Pasa el n�mero de actuaci�n planificada
    glngnumactplancan = grdDBGrid1(0).Columns(9).Value
    'Load frmcancelar
    'Call frmcancelar.Show(vbModal)
    Call objsecurity.LaunchProcess("PR0132")
    'Unload frmcancelar
    'Set frmcancelar = Nothing
    'Regresca la pantalla
    objWinInfo.DataRefresh
End Sub

Private Sub cmdConsEstPac_Click()
'Llama a la pantalla de consultar el estado del paciente

    'Pasa el c�digo del paciente
    'glngcodpaciente = IdPersona1.Text
    frmsituacionpaciente.IdPersona1.Text = IdPersona1.Text
    'Load frmsituacionpaciente
    'Call frmsituacionpaciente.Show(vbModal)
    Call objsecurity.LaunchProcess("PR0173")
    'Unload frmsituacionpaciente
    'Set frmsituacionpaciente = Nothing

End Sub

Private Sub cmdEliminarCola_Click()
    Dim strupdate As String
    'Introducimos la fecha de la entrada en la cola
    strupdate = "UPDATE PR0400 SET PR04FECENTRCOLA = null" & _
       " WHERE pr04numactplan=" & grdDBGrid1(1).Columns(10).Value
    objApp.rdoConnect.Execute strupdate, 64
    objWinInfo.DataRefresh
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End Sub

Private Sub cmdinfadi_Click(Index As Integer)
   
   Call objsecurity.LaunchProcess("PR0229")

End Sub

Private Sub cmdrecact_Click(Index As Integer)
   
   gstrLlamadorSel = ""
   If Index = 0 Then
    gstrLlamadorSel = "Peticiones Pendientes 0"
    'gstrLlamadorSel = grdDBGrid1(0).Columns(9).Value
    frmRecPacRecur.IdPersona1 = IdPersona1
    frmRecPacRecur.txtactText1(0).Text = grdDBGrid1(0).Columns(9).Value
    frmRecPacRecur.txtactText1(1).Text = grdDBGrid1(0).Columns(8).Value
   Else
    gstrLlamadorSel = "Peticiones Pendientes 1"
    'gstrLlamadorSel = grdDBGrid1(1).Columns(10).Value
    frmRecPacRecur.IdPersona1 = IdPersona1
    frmRecPacRecur.txtactText1(0).Text = grdDBGrid1(1).Columns(10).Value
    frmRecPacRecur.txtactText1(1).Text = grdDBGrid1(1).Columns(8).Value
   End If
   Call objsecurity.LaunchProcess("PR0197")
   gstrLlamadorSel = ""

End Sub

Private Sub Form_Activate()
  objWinInfo.DataRefresh
    
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim strKey As String
  
  If primerallamada = True Then
    Call Seleccionar_Dpto
    primerallamada = False
  End If
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
    
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0409J"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'El c�digo del departamento realizador y la actuaci�n tenga proceso y asistencia
    'en PR0800
    '.strWhere = "AD02CODDPTO=" & glngdptologin & " AND " & _
    '            "PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400,PR0800 " & _
    '                               " WHERE PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI " & _
    '                               "   AND PR0800.AD07CODPROCESO IS NOT NULL " & _
    '                               "   AND PR0800.AD01CODASISTENCI IS NOT NULL)"
     .strWhere = "AD07CODPROCESO IS NOT NULL " & _
                 " AND AD01CODASISTENCI IS NOT NULL" & _
                 " AND AD02CODDPTO=" & glngdptologin

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    
    'El grid estar� ordenado por la fecha de la consulta
    Call .FormAddOrderField("FECHA", cwAscending)
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
  
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado")
    
    
  End With
  
  
  With objMultiInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0418J"
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Se filtra por el departamento realizador
    .strWhere = "AD02CODDPTO=" & glngdptologin
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    
    'Se ordena el grid por la fecha de la citaci�n
    Call .FormAddOrderField("FECHA", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora Cita", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "HORACOLA", "Hora entrada cola", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora Cita")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "HORACOLA", "Hora entrada cola")
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo1, "Fecha", "FECHA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Hora", "HORA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo1, "Cod act pedi", "PR04numactplan", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Estado", "PR37DESESTADO", cwString, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado", "PR37CODESTADO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado Muestra", "PR56CODESTMUES", cwNumeric, 10)
    
    Call .FormCreateInfo(objMultiInfo1)
    
    'La primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    
    Call .FormChangeColor(objMultiInfo1)
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    
    '.CtrlGetInfo(chksinfecha).blnNegotiated = False
    
    'Los campos no estar�n visibles en el grid
    grdDBGrid1(0).Columns(9).Visible = False
    grdDBGrid1(0).Columns(10).Visible = False
    grdDBGrid1(0).Columns(12).Visible = False
    grdDBGrid1(0).Columns(13).Visible = False
    
    
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo2, "Fecha", "FECHA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo2, "Hora", "HORA", cwString, 8)
    Call .GridAddColumn(objMultiInfo2, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo2, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo2, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo2, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo2, "Hora entrada cola", "HORACOLA", cwString, 10)
    Call .GridAddColumn(objMultiInfo2, "Cod act pedi", "PR04numactplan", cwString, 10)
    Call .GridAddColumn(objMultiInfo2, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    
    Call .FormCreateInfo(objMultiInfo2)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnMandatory = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnMandatory = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnMandatory = True
    Call .FormChangeColor(objMultiInfo2)
  
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
    
    'Hacemos invisibles la columnas del grid
    grdDBGrid1(1).Columns(10).Visible = False
    grdDBGrid1(1).Columns(11).Visible = False
    
    
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  'Damos el ancho de las columnas del grid
  grdDBGrid1(0).Columns(3).Width = 1000
  grdDBGrid1(0).Columns(4).Width = 600
  grdDBGrid1(0).Columns(5).Width = 1400
  grdDBGrid1(0).Columns(6).Width = 1500
  grdDBGrid1(0).Columns(7).Width = 1500
  grdDBGrid1(0).Columns(8).Width = 2800
  grdDBGrid1(0).Columns(11).Width = 1400
  
    'Establecemos el ancho de las columnas del grid
    grdDBGrid1(1).Columns(3).Width = 1000
    grdDBGrid1(1).Columns(4).Width = 600
    grdDBGrid1(1).Columns(5).Width = 1600
    grdDBGrid1(1).Columns(6).Width = 1600
    grdDBGrid1(1).Columns(7).Width = 1600
    grdDBGrid1(1).Columns(8).Width = 2100
    grdDBGrid1(1).Columns(9).Width = 1600
  'Call objApp.SplashOff
    'Inicializamos el control Idpersona
    'Call objApp.AddCtrl(TypeName(IdPersona1))
    'Call IdPersona1.Init(objApp, objGen)
    'Call IdPersona1.BEGINCONTROL(objApp, objGen)
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.DataRefresh
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub cmdRecibirPaciente_Click()
'Tratamos la llegada de un paciente a la cola de espera
Dim strupdate As String
Dim sacarmensaje As String
Dim mensaje As String


cmdRecibirPaciente.Enabled = False
  If grdDBGrid1(0).Columns(13).Value = "" Then
    grdDBGrid1(0).Columns(13).Value = 0
  End If
  If grdDBGrid1(0).Columns(12).Value = 1 And grdDBGrid1(0).Columns(13).Value = 1 Then
        mensaje = MsgBox(" El paciente tiene muestras pendientes. " & Chr(13) & _
                       " �Desea recibirle?", vbYesNo + vbExclamation, "Aviso")
    If mensaje = vbNo Then
            cmdRecibirPaciente.Enabled = True
            Exit Sub
    Else
        'Introducimos la fecha de la entrada en la cola
        strupdate = "UPDATE PR0400 SET PR04FECENTRCOLA=(SELECT SYSDATE FROM DUAL)" & _
           " WHERE pr04numactplan=" & grdDBGrid1(0).Columns(9).Value
        objApp.rdoConnect.Execute strupdate, 64
        objWinInfo.DataRefresh
        'Activamos y desactivamos los botones
        If grdDBGrid1(0).Rows = 0 Then
            cmdCancelarPrueba.Enabled = False
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            If grdDBGrid1(0).Columns(12).Value = 6 Then
                cmdRecibirPaciente.Enabled = False
                cmdConsEstPac.Enabled = False
                cmdCancelarPrueba.Enabled = True
                cmdrecact(0).Enabled = False
                cmdinfadi(2).Enabled = False
            Else
                cmdRecibirPaciente.Enabled = True
                cmdConsEstPac.Enabled = True
                cmdCancelarPrueba.Enabled = True
                cmdrecact(0).Enabled = True
                cmdinfadi(2).Enabled = True
            End If
            IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
            IdPersona1.ReadPersona
        End If
        If grdDBGrid1(1).Rows = 0 Then
            cmdEliminarCola.Enabled = False
            cmdrecact(1).Enabled = False
        Else
            cmdEliminarCola.Enabled = True
            cmdrecact(1).Enabled = True
        End If
    End If
Else
 'Introducimos la fecha de la entrada en la cola
    strupdate = "UPDATE PR0400 SET PR04FECENTRCOLA=(SELECT SYSDATE FROM DUAL)" & _
       " WHERE pr04numactplan=" & grdDBGrid1(0).Columns(9).Value
    objApp.rdoConnect.Execute strupdate, 64
    objWinInfo.DataRefresh
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End If


End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  End If
  objWinInfo.DataRefresh
End Sub

Private Sub tabTab1_DblClick(Index As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  End If
  objWinInfo.DataRefresh
End Sub

Private Sub Timer1_Timer()
Dim cont As Integer

cont = cont + 1
If cont = 5 Then
    cont = 0
    objWinInfo.DataRefresh
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
If btnButton.Index = 30 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Exit Sub
End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
  
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    'Activamos y desactivamos los botones
    'If grdDBGrid1(0).Rows = 0 Or grdDBGrid1(0).Columns(12).Value = 5 Then
    '    cmdRecibirPaciente.Enabled = False
    '    cmdConsEstPac.Enabled = False
    'Else
    '    cmdRecibirPaciente.Enabled = True
    '    cmdConsEstPac.Enabled = True
    'End If
    'If grdDBGrid1(0).Rows = 0 Then
    '   cmdCancelarPrueba.Enabled = False
    'Else
    '    cmdCancelarPrueba.Enabled = True
    '    IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
    '    IdPersona1.ReadPersona
    'End If
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


