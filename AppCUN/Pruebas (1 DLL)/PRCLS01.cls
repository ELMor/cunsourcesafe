VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher2"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher2
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
'Const SGRepApps         As String = "SG0001"
'Const SGRepGroups       As String = "SG0002"
'Const SGRepRoles        As String = "SG0003"
'Const SGRepRolesGroup   As String = "SG0004"
'Const SGRepUsers        As String = "SG0005"
'Const SGRepUsersGroup   As String = "SG0006"
'Const SGRepUsersRol     As String = "SG0007"
'Const SGRepProcessRol   As String = "SG0008"
'Const SGRepProcessUser  As String = "SG0009"
'Const SGRepProcessApp   As String = "SG0010"
'Const SGRepTablesRol    As String = "SG0011"
'Const SGRepTablesUser   As String = "SG0012"
'Const SGRepColumnsRol   As String = "SG0013"
'Const SGRepColumnsUser  As String = "SG0014"
'Const SGRepAuditTables  As String = "SG0015"
'Const SGRepLogTable     As String = "SG0016"
'Const SGRepLogUser      As String = "SG0017"

' Ahora las ventanas. Continuan la numeraci�n a partir del SG1000

Const PRWinLanzarFolletos          As String = "PR00140"
Const PRWinLanzarConsentimiento    As String = "PR00141"
Const PRWinLanzarPrograma          As String = "PR00142"
Const PRWinBuscarDoctor            As String = "PR00143"
Const PRWinBuscarDptos             As String = "PR00144"
Const PRWinCuestionario1           As String = "PR00145"
Const PRWinCuestionario1Parte      As String = "PR00146"
Const PRWinCuestionario2           As String = "PR00147"
Const PRWinCuestionario2Parte      As String = "PR00148"
Const PRWinDatosActPedida          As String = "PR00149"
Const PRWinRecursos                As String = "PR00150"
'Const PRWinPedActPrincipal         As String = "PR00151"
Const PRWinPedir                   As String = "PR00152"
Const PRWinSeleccionarAct          As String = "PR00153"
Const PRWinSelPrincipales          As String = "PR00154"
Const PRWinSelPaquetesP             As String = "PR00155"
Const PRWinSelProtocolosP           As String = "PR00156"
Const PRWinSelDptosP                As String = "PR00157"
Const PRWinPedirCond               As String = "PR00158"
Const PRWinActnoSel                As String = "PR00159"
Const PRWinInterPlani              As String = "PR00161"
Const PRWinSelGruposP               As String = "PR00171"
Const PRWinDefDptosP                As String = "PR00172"
Const PRWinPruebasAdicionales As String = "PR00181"
' Ahora las ventanas. Continuan la numeraci�n a partir del SG1000

'Const SGWinApps         As String = "SG1001"
'Const SGWinGroups       As String = "SG1002"
'Const SGWinUsers        As String = "SG1003"
'Const SGWinRoles        As String = "SG1004"
'Const SGWinRolesGroup   As String = "SG1005"
'Const SGWinTypeProcess  As String = "SG1006"
'Const SGWinProcess      As String = "SG1007"
'Const SGWinFuncs        As String = "SG1008"
'Const SGWinSegTabCol    As String = "SG1009"
'Const SGWinChangePW     As String = "SG1010"
'Const SGWinForzePW      As String = "SG1011"
'Const SGWinProcessRol   As String = "SG1012"
'Const SGWinDelega       As String = "SG1013"
'Const SGWinPurgeDelega  As String = "SG1014"
'Const SGWinQueryDelega  As String = "SG1015"
'Const SGWinVenLis       As String = "SG1016"
'Const SGWinPrnTabCol    As String = "SG1017"
'Const SGWinEncript      As String = "SG1018"
'Const SGWinAuditTables  As String = "SG1019"
'Const SGWinViewAudit    As String = "SG1020"
'Const SGWinMaintAudit   As String = "SG1021"
'Const SGWinLoadProcess  As String = "SG1022"


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PRWinLanzarFolletos
      Load frmlanzarfolletos
      Call objsecurity.AddHelpContext(25)
      Call frmlanzarfolletos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmlanzarfolletos
      Set frmlanzarfolletos = Nothing
    Case PRWinLanzarConsentimiento
      Load frmlanzarconsentimiento
      Call frmlanzarconsentimiento.Show(vbModal)
      Unload frmlanzarconsentimiento
      Set frmlanzarconsentimiento = Nothing
    Case PRWinLanzarPrograma
      Load frmlanzarprograma
      Call frmlanzarprograma.Show(vbModal)
      Unload frmlanzarprograma
      Set frmlanzarprograma = Nothing
    Case PRWinBuscarDoctor
      Load frmbuscardoctor
      Call frmbuscardoctor.Show(vbModal)
      Unload frmbuscardoctor
      Set frmbuscardoctor = Nothing
    Case PRWinBuscarDptos
      Load frmbuscardptos
      Call frmbuscardptos.Show(vbModal)
      Unload frmbuscardptos
      Set frmbuscardptos = Nothing
    Case PRWinCuestionario1
      Load frmcuestionario1
      Call frmcuestionario1.Show(vbModal)
      Unload frmcuestionario1
      Set frmcuestionario1 = Nothing
    Case PRWinCuestionario1Parte
      Load frmcuestionario1parte
      Call frmcuestionario1parte.Show(vbModal)
      Unload frmcuestionario1parte
      Set frmcuestionario1parte = Nothing
    Case PRWinCuestionario2
      Load frmcuestionario2
      Call frmcuestionario2.Show(vbModal)
      Unload frmcuestionario2
      Set frmcuestionario2 = Nothing
    Case PRWinCuestionario2Parte
      Load frmcuestionario2parte
      Call frmcuestionario2parte.Show(vbModal)
      Unload frmcuestionario2parte
      Set frmcuestionario2parte = Nothing
    Case PRWinDatosActPedida
      Load frmdatosactpedida
      Call frmdatosactpedida.Show(vbModal)
      Unload frmdatosactpedida
      Set frmdatosactpedida = Nothing
    Case PRWinRecursos
      Load frmrecursos
      'If frmSelPaquetesP.Prepare(objsecurity.strUser, False) Then
        Call frmrecursos.Show(vbModal)
      'End If
      Unload frmrecursos
      Set frmrecursos = Nothing
    'Case PRWinPedActPrincipal
      'Load frmpedactprincipal
      'If frmSelProtocolosP.Prepare(vntData, True) Then
      '  Call frmpedactprincipal.Show(vbModal)
      'End If
      'Unload frmpedactprincipal
      'Set frmpedactprincipal = Nothing
    Case PRWinPedir
      gintPadre = 0
      Load frmpedir
      Call frmpedir.Show(vbModal)
      Unload frmpedir
      Set frmpedir = Nothing
    Case PRWinSeleccionarAct
      Load frmSeleccionarAct
      Call frmSeleccionarAct.Show(vbModal)
      Unload frmSeleccionarAct
      Set frmSeleccionarAct = Nothing
    Case PRWinSelPrincipales
      Load frmSelPrincipales
      Call frmSelPrincipales.Show(vbModal)
      Unload frmSelPrincipales
      Set frmSelPrincipales = Nothing
    Case PRWinSelPaquetesP
      Load frmSelPaquetesP
      Call frmSelPaquetesP.Show(vbModal)
      Unload frmSelPaquetesP
      Set frmSelPaquetesP = Nothing
    Case PRWinSelProtocolosP
      Load frmSelProtocolosP
      Call frmSelProtocolosP.Show(vbModal)
      Unload frmSelProtocolosP
      Set frmSelProtocolosP = Nothing
    Case PRWinSelDptosP
      Load frmSelDptosP
      Call frmSelDptosP.Show(vbModal)
      Unload frmSelDptosP
      Set frmSelDptosP = Nothing
    Case PRWinPedirCond
      Load frmpedircond
      Call frmpedircond.Show(vbModal)
      Unload frmpedircond
      Set frmpedircond = Nothing
    Case PRWinActnoSel
      Load frmactnosel
      Call frmactnosel.Show(vbModal)
      Unload frmactnosel
      Set frmactnosel = Nothing
    Case PRWinInterPlani
      Load frminterplani
      Call frminterplani.Show(vbModal)
      Unload frminterplani
      Set frminterplani = Nothing
    Case PRWinSelGruposP
      Load frmSelGruposP
      'If frmtiposinteraccion.Prepare(False) Then
        Call frmSelGruposP.Show(vbModal)
      'End If
      Unload frmSelGruposP
      Set frmSelGruposP = Nothing
    Case PRWinDefDptosP
      Load frmdefdptosP
      'If frmtiposdemuestra.Prepare(True) Then
        Call frmdefdptosP.Show(vbModal)
      'End If
      Unload frmdefdptosP
      Set frmdefdptosP = Nothing
    Case PRWinPruebasAdicionales
      Load frmpruebasadicionales
      Call frmpruebasadicionales.Show(vbModal)
      Unload frmpruebasadicionales
      Set frmpruebasadicionales = Nothing
    
    'Case SGRepProcessUser
    '  Call ReportProcessUser
    'Case SGRepLogTable
    '  Call ReportAudit(True)
    'Case SGRepLogUser
    '  Call ReportAudit(False)
    'Case Else
    '  ' el c�digo de proceso no es v�lido y se devuelve falso
    '  LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 39 procesos
  ReDim aProcess(1 To 24, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = PRWinLanzarFolletos
  aProcess(1, 2) = "Lanzar Folletos"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinLanzarConsentimiento
  aProcess(2, 2) = "Lanzar Consentimiento"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = PRWinLanzarPrograma
  aProcess(3, 2) = "Lanzar Programa"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = PRWinBuscarDoctor
  aProcess(4, 2) = "Buscar Doctor"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = PRWinBuscarDptos
  aProcess(5, 2) = "Buscar Departamentos"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = PRWinCuestionario1
  aProcess(6, 2) = "Cuestionario 1"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = PRWinCuestionario1Parte
  aProcess(7, 2) = "Cuestionario 1 Parte"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = PRWinCuestionario2
  aProcess(8, 2) = "Cuestionario 2"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = PRWinCuestionario2Parte
  aProcess(9, 2) = "Cuestionario 2 Parte"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = PRWinDatosActPedida
  aProcess(10, 2) = "Datos de la Actuacion Pedida"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
      
  aProcess(11, 1) = PRWinRecursos
  aProcess(11, 2) = "Recursos"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
      
  'aProcess(12, 1) = PRWinPedActPrincipal
  'aProcess(12, 2) = "Menu Principal"
  'aProcess(12, 3) = False
  'aProcess(12, 4) = cwTypeWindow
      
  aProcess(13, 1) = PRWinPedir
  aProcess(13, 2) = "Peticion de Pruebas"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
      
  aProcess(14, 1) = PRWinSeleccionarAct
  aProcess(14, 2) = "Seleccion de Actuaciones"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow
      
  aProcess(15, 1) = PRWinSelPrincipales
  aProcess(15, 2) = "Seleccion por Principales"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
      
  aProcess(16, 1) = PRWinSelPaquetesP
  aProcess(16, 2) = "Seleccion por Paquetes"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeWindow
      
  aProcess(17, 1) = PRWinSelProtocolosP
  aProcess(17, 2) = "Seleccion por Protocolos"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
      
  aProcess(18, 1) = PRWinSelDptosP
  aProcess(18, 2) = "Seleccion por Departamentos"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeWindow
      
  aProcess(19, 1) = PRWinPedirCond
  aProcess(19, 2) = "Pedir Condiciones"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeWindow
      
  aProcess(20, 1) = PRWinActnoSel
  aProcess(20, 2) = "Actuaciones no Seleccionadas"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
      
  aProcess(21, 1) = PRWinInterPlani
  aProcess(21, 2) = "Planificacion Interna"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
      
  aProcess(22, 1) = PRWinSelGruposP
  aProcess(22, 2) = "Seleccion por Grupos"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = PRWinDefDptosP
  aProcess(23, 2) = "Peticion de Departamentos"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeWindow
  
  aProcess(24, 1) = PRWinPruebasAdicionales
  aProcess(24, 2) = "Peticion de Pruebas Adicionales"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow
  
      
  ' LISTADOS
  'aProcess(23, 1) = SGRepApps
  'aProcess(23, 2) = "Relaci�n de Aplicaciones"
  'aProcess(23, 3) = False
  'aProcess(23, 4) = cwTypeReport
      
  'aProcess(24, 1) = SGRepGroups
  'aProcess(24, 2) = "Relaci�n de Grupos de Usuarios"
  'aProcess(24, 3) = False
  'aProcess(24, 4) = cwTypeReport
  
  'aProcess(25, 1) = SGRepRoles
  'aProcess(25, 2) = "Relaci�n de Roles"
  'aProcess(25, 3) = False
  'aProcess(25, 4) = cwTypeReport
  
  'aProcess(26, 1) = SGRepRolesGroup
  'aProcess(26, 2) = "Relaci�n de Roles por Grupo"
  'aProcess(26, 3) = False
  'aProcess(26, 4) = cwTypeReport

  'aProcess(27, 1) = SGRepUsers
  'aProcess(27, 2) = "Relaci�n de Usuarios"
  'aProcess(27, 3) = False
  'aProcess(27, 4) = cwTypeReport
  
  'aProcess(28, 1) = SGRepUsersGroup
  'aProcess(28, 2) = "Relaci�n de Usuarios por Grupo"
  'aProcess(28, 3) = False
  'aProcess(28, 4) = cwTypeReport

  'aProcess(29, 1) = SGRepUsersRol
  'aProcess(29, 2) = "Relaci�n de Usuarios por Rol"
  'aProcess(29, 3) = False
  'aProcess(29, 4) = cwTypeReport

  'aProcess(30, 1) = SGRepProcessRol
  'aProcess(30, 2) = "Relaci�n de Procesos por Rol"
  'aProcess(30, 3) = False
  'aProcess(30, 4) = cwTypeReport

  'aProcess(31, 1) = SGRepProcessUser
  'aProcess(31, 2) = "Relaci�n de Procesos por Usuarios"
  'aProcess(31, 3) = True
  'aProcess(31, 4) = cwTypeReport

  'aProcess(32, 1) = SGRepProcessApp
  'aProcess(32, 2) = "Relaci�n de Procesos por Aplicaci�n"
  'aProcess(32, 3) = False
  'aProcess(32, 4) = cwTypeReport

  'aProcess(33, 1) = SGRepTablesRol
  'aProcess(33, 2) = "Restricciones en Tablas por Rol"
  'aProcess(33, 3) = False
  'aProcess(33, 4) = cwTypeReport

  'aProcess(34, 1) = SGRepTablesUser
  'aProcess(34, 2) = "Restricciones en Tablas por Usuario"
  'aProcess(34, 3) = False
  'aProcess(34, 4) = cwTypeReport

  'aProcess(35, 1) = SGRepColumnsRol
  'aProcess(35, 2) = "Restricciones en Columnas por Rol"
  'aProcess(35, 3) = False
  'aProcess(35, 4) = cwTypeReport

  'aProcess(36, 1) = SGRepColumnsUser
  'aProcess(36, 2) = "Restricciones en Columnas por Usuario"
  'aProcess(36, 3) = False
  'aProcess(36, 4) = cwTypeReport

  'aProcess(37, 1) = SGRepAuditTables
  'aProcess(37, 2) = "Relaci�n de Tablas a auditar"
  'aProcess(37, 3) = False
  'aProcess(37, 4) = cwTypeReport

  'aProcess(38, 1) = SGRepLogTable
  'aProcess(38, 2) = "Relaci�n del Log de Auditor�a por Tabla"
  'aProcess(38, 3) = True
  'aProcess(38, 4) = cwTypeReport

  'aProcess(39, 1) = SGRepLogUser
  'aProcess(39, 2) = "Relaci�n del Log de Auditor�a por Usuario"
  'aProcess(39, 3) = True
  'aProcess(39, 4) = cwTypeReport
End Sub
