VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmrellenardatosmuestra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Extracciones. Rellenar datos de la muestra"
   ClientHeight    =   8340
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   1
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdasignar 
      Caption         =   "Asignar Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4800
      TabIndex        =   15
      Top             =   7560
      Width           =   2175
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   19
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Index           =   0
      Left            =   600
      TabIndex        =   0
      Top             =   600
      Width           =   10515
      Begin TabDlg.SSTab tabTab1 
         Height          =   6060
         Index           =   0
         Left            =   240
         TabIndex        =   16
         Top             =   480
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   10689
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0222.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(15)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(21)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(14)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cmdcuestionario"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(6)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "Frame1"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtcaja"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "Frame2"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).ControlCount=   17
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0222.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR52TIEMPO"
            Height          =   330
            Index           =   13
            Left            =   360
            TabIndex        =   36
            TabStop         =   0   'False
            Tag             =   "Tiempo en minutos respecto a la primera muestra"
            Top             =   2040
            Width           =   600
         End
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   7440
            TabIndex        =   34
            Top             =   1920
            Width           =   1335
         End
         Begin VB.TextBox txtcaja 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   7680
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "C�digo de la Muestra"
            Top             =   2040
            Width           =   735
         End
         Begin VB.Frame Frame1 
            Height          =   3375
            Left            =   240
            TabIndex        =   24
            Top             =   2520
            Width           =   9255
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   8
               Left            =   1440
               ScrollBars      =   2  'Vertical
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Apellido del realizador de la extracci�n"
               Top             =   720
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   7
               Left            =   240
               TabIndex        =   6
               Tag             =   "C�digo del realizador de la extracci�n"
               Top             =   720
               Width           =   600
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "�Muestra intraoperatoria?"
               DataField       =   "PR52INDINTRA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   0
               Left            =   1800
               TabIndex        =   9
               Tag             =   "Indicador de muestra operatoria(si/no)"
               Top             =   1560
               Width           =   2655
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR52DESMUESTRA"
               Height          =   930
               Index           =   3
               Left            =   240
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   13
               Tag             =   "Descripci�n de la Muestra"
               Top             =   2280
               Width           =   8760
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR52NUMIDMUESTRA"
               Height          =   330
               Index           =   4
               Left            =   240
               TabIndex        =   8
               Tag             =   "N� de identificaci�n de la muestra"
               Top             =   1560
               Width           =   855
            End
            Begin VB.TextBox txtminuto 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   7560
               MaxLength       =   2
               TabIndex        =   12
               Tag             =   "Minutos de la Extracci�n"
               Top             =   1560
               Width           =   375
            End
            Begin VB.TextBox txthora 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   375
               Left            =   6960
               MaxLength       =   2
               TabIndex        =   11
               Tag             =   "Hora de Extracci�n"
               Top             =   1560
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR52FECEXTRAC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   23
               Left            =   4920
               Locked          =   -1  'True
               TabIndex        =   25
               TabStop         =   0   'False
               Tag             =   "Fecha y Hora de Extracci�n"
               Top             =   240
               Visible         =   0   'False
               Width           =   3975
            End
            Begin ComCtl2.UpDown UpDownminuto 
               Height          =   375
               Left            =   7920
               TabIndex        =   26
               Top             =   1560
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   661
               _Version        =   327681
               BuddyControl    =   "txtminuto"
               BuddyDispid     =   196619
               OrigLeft        =   8040
               OrigTop         =   2760
               OrigRight       =   8280
               OrigBottom      =   3135
               Increment       =   5
               Max             =   55
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin ComCtl2.UpDown UpDownhora 
               Height          =   375
               Left            =   7320
               TabIndex        =   27
               Top             =   1560
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   661
               _Version        =   327681
               BuddyControl    =   "txthora"
               BuddyDispid     =   196620
               OrigLeft        =   5400
               OrigTop         =   1200
               OrigRight       =   5640
               OrigBottom      =   1695
               Max             =   23
               SyncBuddy       =   -1  'True
               BuddyProperty   =   0
               Enabled         =   -1  'True
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcfecha 
               Height          =   330
               Left            =   4680
               TabIndex        =   10
               Tag             =   "Fecha de Extracci�n"
               Top             =   1560
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16776960
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo del realizador de la extracci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   240
               TabIndex        =   32
               Top             =   480
               Width           =   3375
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   240
               TabIndex        =   31
               Top             =   2040
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Identificaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   240
               TabIndex        =   30
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha de Extracci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   4680
               TabIndex        =   29
               Top             =   1320
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora de Extracci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   6960
               TabIndex        =   28
               Top             =   1320
               Width           =   2175
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR55CODESTADO"
            Height          =   330
            Index           =   6
            Left            =   360
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Estado de la Muestra"
            Top             =   1320
            Width           =   600
         End
         Begin VB.CommandButton cmdcuestionario 
            BackColor       =   &H00C0FFFF&
            Caption         =   "Cuestionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   1320
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   5
            Left            =   1680
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Descripci�n del estado de la muestra"
            Top             =   1320
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR24CODMUESTRA"
            Height          =   330
            Index           =   2
            Left            =   1680
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "C�digo de la Muestra"
            Top             =   600
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   1
            Left            =   3480
            ScrollBars      =   3  'Both
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n corta de la Muestra"
            Top             =   600
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR52NUMMUESTRA"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   1
            Tag             =   "N�mero de la Muestra"
            Top             =   600
            Width           =   855
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5145
            Index           =   0
            Left            =   -74760
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   360
            Width           =   9375
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16536
            _ExtentY        =   9075
            _StockProps     =   79
            Caption         =   "MUESTRAS"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo respecto a la 1� muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   360
            TabIndex        =   38
            Top             =   1800
            Width           =   2895
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   1080
            TabIndex        =   37
            Top             =   2160
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   360
            TabIndex        =   23
            Top             =   1080
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   1680
            TabIndex        =   22
            Top             =   1080
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   1680
            TabIndex        =   21
            Top             =   360
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   3480
            TabIndex        =   18
            Top             =   360
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   17
            Top             =   360
            Width           =   1095
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cort&ar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener"
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo Valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner Filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar Filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero     CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior    Re P�g"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente  Av P�g"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&�ltimo       CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar Registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda   "
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmrellenardatosmuestra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: MANTENIMIENTOS PRUEBAS C.U.N. (PR002.VBP)                  *
'* NOMBRE: FRMRELLENARDATOSMUESTRA(PR00222.FRM)                         *
'* AUTOR: IRENE VAZQUEZ MARTINEZ                                        *
'* FECHA: 25 DE MAYO DE 1998                                            *
'* DESCRIPCION: rellenar datos de una muestra aportada/extra�da'        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim intcambioalgo As Integer
Dim intaux As Integer
Dim mblnFechaModif As Boolean
Dim strfechaaux As String
'intminuto detecta si los minutos se meten tecleando directamente sobre txtminuto para
'que si se mete 1 minuto no pase a la oculta 01 y de la oculta otra vez a txtminuto 01
Dim intminuto As Integer


Private Sub cboSSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmdAsignar_Click()
  cmdAsignar.Enabled = False
  'jcr frmAsigMuestras.Text1(0).Text = frmMuestrasPendientes.txtText1(2).Text 'nombre paciente
  'frmAsigMuestras.Text1(1).Text = frmMuestrasPendientes.txtText1(4).Text 'primer apellido
  'frmAsigMuestras.Text1(2).Text = frmMuestrasPendientes.txtText1(3).Text 'segundo apellido
  
  'frmAsigMuestras.Text1(3).Text = frmrellenardatosmuestra.txtText1(0).Text   'num.muestra
  'frmAsigMuestras.Text1(4).Text = frmrellenardatosmuestra.txtText1(2).Text   'c�d.muestra
  'frmAsigMuestras.Text1(5).Text = frmrellenardatosmuestra.txtText1(1).Text   'desc.muestra
  'frmAsigMuestras.Text1(6).Text = frmrellenardatosmuestra.txtText1(3).Text  'descripci�n
  'cmdasignar.Enabled = True
  'Call objsecurity.LaunchProcess("PR0224")
  ''cmdAsignar.Enabled = True
End Sub

Private Sub cmdcuestionario_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim cuestionariomuestra As Boolean
Dim cuestionariotipomuestra As Boolean
  
  cmdCuestionario.Enabled = False
  
  stra = "SELECT count(*) FROM PR5400 WHERE " & _
         "PR52NUMMUESTRA=" & txtText1(0).Text
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If rsta.rdoColumns(0).Value > 0 Then
     'cuestionariotipomuestra = True
     Call objsecurity.LaunchProcess("PR0216")
  End If
  rsta.Close
  Set rsta = Nothing
        
  '*******************************************************************************
  cmdCuestionario.Enabled = True
  cmdCuestionario.Visible = True
  'paso de par�metros a frmasigmuestras
  'jcr frmAsigMuestras.Text1(0).Text = frmMuestrasPendientes.txtText1(2).Text 'nombre paciente
  'frmAsigMuestras.Text1(1).Text = frmMuestrasPendientes.txtText1(4).Text 'primer apellido
  'frmAsigMuestras.Text1(2).Text = frmMuestrasPendientes.txtText1(3).Text 'segundo apellido
  '
  'frmAsigMuestras.Text1(3).Text = frmrellenardatosmuestra.txtText1(0).Text   'num.muestra
  'frmAsigMuestras.Text1(4).Text = frmrellenardatosmuestra.txtText1(2).Text   'c�d.muestra
  'frmAsigMuestras.Text1(5).Text = frmrellenardatosmuestra.txtText1(1).Text   'desc.muestra
  'frmAsigMuestras.Text1(6).Text = frmrellenardatosmuestra.txtText1(3).Text  'descripci�n
  ''se llama a frmasigmuestras
  'Call objsecurity.LaunchProcess("PR0224")
End Sub



Private Sub dtcfecha_Change()
Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  grdDBGrid1(0).Enabled = False
  
      intcambioalgo = 1
      intaux = 0
      If dtcfecha.Text <> "" Then
        If (txthora.Text <> "" And txtminuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
End Sub

Private Sub dtcfecha_CloseUp()
Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  grdDBGrid1(0).Enabled = False
  intcambioalgo = 1
  intaux = 0
  
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes sel systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txthora.Text <> "") Then
                If CDbl(txthora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txthora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtminuto.Text <> "") Then
                    If CDbl(txtminuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    If blnFechaValida = False Then
      intResp = MsgBox("La FECHA DE EXTRACCI�N ha pasado", vbInformation, "Importante")
      dtcfecha.Text = ""
    Else
      If dtcfecha.Text <> "" Then
        If (txthora.Text <> "" And txtminuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing
End Sub

Private Sub dtcfecha_KeyPress(KeyAscii As Integer)
mblnFechaModif = True
intaux = 0
End Sub

Private Sub dtcfecha_Lostfocus()
Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  
grdDBGrid1(0).Enabled = False

If mblnFechaModif = True Then
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes sel systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txthora.Text <> "") Then
                If CDbl(txthora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txthora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtminuto.Text <> "") Then
                    If CDbl(txtminuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    If blnFechaValida = False Or blnFechaValida = True Then
      If dtcfecha.Text <> "" Then
        If (txthora.Text <> "" And txtminuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
              objWinInfo.objWinActiveForm.blnChanged = True
          End If
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
            objWinInfo.objWinActiveForm.blnChanged = True
          End If
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  
End If
mblnFechaModif = False
End Sub

Private Sub dtcfecha_Spin(OldDate As String, NewDate As String)
mblnFechaModif = True
End Sub

Private Sub Form_Activate()
Dim rstasig As rdoResultset
Dim strasig As String

tlbToolbar1.Buttons(2).Enabled = False

'si al volver de la pantalla frmasigmuestras hay alguna muestra asignada
'este formulario debe descargarse
If gblnunloadrellenardatosmuestra = True Then
    Unload Me
Else
    If gblnactivarbotones = False Then
        cmdCuestionario.Visible = False
        cmdAsignar.Visible = False
   'jcr     'If frmMuestrasPendientes.chkCheck1(1).Value = 1 Then
        '    txthora.BackColor = &HFFFF00
        '    txtminuto.BackColor = &HFFFF00
        'End If
        'para que el cursor se posicione en txttext1(7)(c�digo del realizador de la muestra)
        txtcaja.Text = 0
        txtcaja.SetFocus
        txtText1(0).SetFocus
        txtcaja.Text = 0
    Else
        gblnactivarbotones = False
        strasig = "SELECT COUNT(*) FROM PR5400 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
        Set rstasig = objApp.rdoConnect.OpenResultset(strasig)
        If rstasig.rdoColumns(0).Value > 0 Then
            cmdCuestionario.Visible = True
        End If
        rstasig.Close
        Set rstasig = Nothing
        cmdAsignar.Visible = True
    End If
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  'Call objApp.SplashOn
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Datos de la muestra"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
   .strTable = "PR5200"

    
    .blnHasMaint = True
    

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Muestras")

  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(2)).blnReadOnly = True 'CODMUESTRA
    .CtrlGetInfo(txtText1(6)).blnReadOnly = True 'ESTADO
    .CtrlGetInfo(txtText1(13)).blnReadOnly = True 'TIEMPO
    
    .CtrlGetInfo(txtText1(7)).blnForeign = True  'CODIGO DEL REALIZADOR
    
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    .CtrlGetInfo(dtcfecha).blnNegotiated = False
    .CtrlGetInfo(txtcaja).blnNegotiated = False
     
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "PR24CODMUESTRA", "SELECT * FROM PR2400 WHERE PR24CODMUESTRA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(1), "PR24DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "PR55CODESTADO", "SELECT * FROM PR5500 WHERE PR55CODESTADO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(5), "PR55DESESTADO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(8), "SG02APE1")
    
    Call .WinRegister
    Call .WinStabilize
  End With
        'NUEVO
        Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
        sqlstr = "SELECT PR52NUMMUESTRA_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0) = rsta.rdoColumns(0).Value
        'pr24codmuestra
        'jcr Call objWinInfo.CtrlSet(txtText1(2), frmMuestrasPendientes.txtText1(22))
        'estado=extra�da
        Call objWinInfo.CtrlSet(txtText1(6), 2)
        'tiempo
        ' jcr Call objWinInfo.CtrlSet(txtText1(13), frmMuestrasPendientes.txtText1(13))
        rsta.Close
        Set rsta = Nothing
        
gblnactivarbotones = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim rsta As rdoResultset
Dim stra As String
Dim rstasig As rdoResultset
Dim strasig As String
Dim mensaje As String
Dim cajademensaje As String
Dim rstoblig As rdoResultset
Dim stroblig As String
Dim strdelete54 As String
Dim strdelete52 As String
Dim contestarcuestionario As Integer

contestarcuestionario = 0

On Error GoTo Err_Ejecutar

If gblnunloadrellenardatosmuestra = False Then
      objWinInfo.DataSave
      
      mensaje = "La muestra no ha sido asignada a ninguna actuaci�n. " & Chr(13) & _
              "Para hacer la asignaci�n debe "
      
      stra = "SELECT COUNT(*) FROM PR5200 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.rdoColumns(0).Value = 0 Then
            intCancel = objWinInfo.WinExit
      Else
            strasig = "SELECT COUNT(*) FROM PR5400 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
            Set rstasig = objApp.rdoConnect.OpenResultset(strasig)
            If rstasig.rdoColumns(0).Value = 0 Then  'no existe cuestionario asociado
              mensaje = mensaje & "pulsar el bot�n <Asignar Muestra>."
            Else
            'se mira si el cuestionario tiene preguntas obligatorias y no contestadas
              stroblig = "SELECT * FROM PR5400 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
              Set rstoblig = objApp.rdoConnect.OpenResultset(stroblig)
              Do While Not rstoblig.EOF
                If (rstoblig.rdoColumns("pr54indoblig").Value) = -1 And _
                   IsNull(rstoblig.rdoColumns("pr54respuesta").Value) = True Then
                        mensaje = mensaje & "contestar previamente el cuestionario asociado a la muestra."
                        contestarcuestionario = 1
                        Exit Do
                End If
                rstoblig.MoveNext
              Loop
              If contestarcuestionario = 0 Then
                mensaje = mensaje & "pulsar el bot�n <Asignar Muestra>."
              End If
              rstoblig.Close
              Set rstoblig = Nothing
            End If
            rstasig.Close
            Set rstasig = Nothing
            mensaje = mensaje & Chr(13) & Chr(13) & _
               "     �Desea salir sin guardar los datos referentes a la extracci�n?" & Chr(13)
            cajademensaje = MsgBox(mensaje, vbYesNo + vbExclamation, "Aviso")
            If cajademensaje = vbNo Then
               intCancel = 1
            End If
            If cajademensaje = vbYes Then
               strdelete54 = "DELETE PR5400 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
               objApp.rdoConnect.Execute strdelete54, 64
               objApp.rdoConnect.Execute "Commit", 64
               strdelete52 = "DELETE PR5200 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
               objApp.rdoConnect.Execute strdelete52, 64
               objApp.rdoConnect.Execute "Commit", 64
               intCancel = objWinInfo.WinExit
            End If
      End If
      rsta.Close
      Set rsta = Nothing
End If

If gblnunloadrellenardatosmuestra = True Then
    intCancel = objWinInfo.WinExit
    Exit Sub
End If

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  tlbToolbar1.Buttons(2).Enabled = False
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim strinsert As String
Dim strinsertcuest As String
Dim stra As String
Dim rsta As rdoResultset
Dim norespondido As Integer


intcambioalgo = 0

'se rellena el cuestionario si es que lo hay.
'si el cuestionario ya est� insertado en PR5400 entonces no se llena
stra = "SELECT COUNT(*) FROM PR5100 WHERE PR24CODMUESTRA=" & txtText1(2).Text
Set rsta = objApp.rdoConnect.OpenResultset(stra)
If rsta.rdoColumns(0).Value > 0 Then 'hay cuestionario
   cmdCuestionario.Visible = True
   stra = "SELECT COUNT(*) FROM PR5400 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(stra)
   If rsta.rdoColumns(0).Value = 0 Then 'el cuestionario no est� insertado en PR5400
          stra = "SELECT * FROM PR5100 WHERE PR24CODMUESTRA=" & txtText1(2).Text
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          While Not rsta.EOF
              If rsta.rdoColumns("pr51indroblig").Value = -1 Then
                  cmdAsignar.Visible = False
              End If
              strinsertcuest = "INSERT INTO PR5400(PR52NUMMUESTRA,PR40CODPREGUNTA," & _
                             "PR27CODTIPRESPU,PR46CODLISTRESP,PR54INDOBLIG,PR54RESPUESTA) " & _
                             "VALUES(" & txtText1(0).Text & "," & _
                             rsta.rdoColumns("pr40codpregunta").Value & "," & _
                             rsta.rdoColumns("pr27codtiprespu").Value & ","
              If IsNull(rsta.rdoColumns("pr46codlistresp").Value) = True Then
                strinsertcuest = strinsertcuest & "null" & ","
              Else
                strinsertcuest = strinsertcuest & rsta.rdoColumns("pr46codlistresp").Value & ","
              End If
              strinsertcuest = strinsertcuest & rsta.rdoColumns("pr51indroblig").Value & _
                            "," & "null)"
              objApp.rdoConnect.Execute strinsertcuest, 64
              rsta.MoveNext
          Wend
   Else 'el cuestionario est� insertado
      stra = "SELECT * FROM PR5400 WHERE PR52NUMMUESTRA=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      norespondido = 0
      While Not rsta.EOF
          If rsta.rdoColumns("pr54indoblig").Value = -1 And _
             IsNull(rsta.rdoColumns("pr54respuesta").Value) = True Then
             norespondido = 1
          End If
      rsta.MoveNext
      Wend
      If norespondido = 1 Then
        cmdAsignar.Visible = False
      Else
        cmdAsignar.Visible = True
      End If
   End If
Else 'no hay cuestionario
 cmdAsignar.Visible = True
End If

rsta.Close
Set rsta = Nothing

tlbToolbar1.Buttons(2).Enabled = False

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Muestras" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
 'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
 Dim objField As clsCWFieldSearch
  
  If strFormName = "Datos de la muestra" And strCtrl = "txtText1(7)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR5203J"
     .strWhere = "WHERE AD02CODDPTO=" & glngdptologin
     .strOrder = "ORDER BY SG02APE1 ASC"
     
     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo"
         
     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"
         
     Set objField = .AddField("SG02APE2")
     objField.strSmallDesc = "Segundo Apellido"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(7), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim mensaje As String

tlbToolbar1.Buttons(2).Enabled = False

If btnButton.Index = 4 Then
   If txthora.BackColor = &HFFFF00 Then
      If txthora.Text = "" And txtminuto = "" And _
          txtText1(4).Text <> "" And txtText1(7).Text <> "" And _
          txtText1(23).Text <> "" Then
         mensaje = MsgBox("El campo Hora de Extracci�n de la muestra es obligatorio", vbInformation, "Aviso")
         Exit Sub
      End If
   End If
End If
'Salir o Guardar
If btnButton.Index = 30 Or btnButton.Index = 4 Then
    If btnButton.Index = 4 Then
        If Right(txtText1(23).Text, 1) = "." Then
            txtText1(23).Text = Left(txtText1(23).Text, Len(txtText1(23).Text) - 1)
        End If
    End If

  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
If btnButton.Index <> 30 Then
  tlbToolbar1.Buttons(2).Enabled = False
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim mensaje As String

tlbToolbar1.Buttons(2).Enabled = False

If intIndex = 40 Then
   If txthora.BackColor = &HFFFF00 Then
      If txthora.Text = "" And txtminuto = "" And _
          txtText1(4).Text <> "" And txtText1(7).Text <> "" And _
          txtText1(23).Text <> "" Then
         mensaje = MsgBox("El campo Hora de Extracci�n de la muestra es obligatorio", vbInformation, "Aviso")
         Exit Sub
      End If
   End If
End If
'Salir o Guardar
If intIndex = 100 Or intIndex = 40 Then
    If intIndex = 40 Then
        If Right(txtText1(23).Text, 1) = "." Then
            txtText1(23).Text = Left(txtText1(23).Text, Len(txtText1(23).Text) - 1)
        End If
    End If
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
'salir
If intIndex <> 100 Then
  tlbToolbar1.Buttons(2).Enabled = False
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub




Private Sub txtcaja_GotFocus()
txtText1(0).SetFocus
txtText1(7).SetFocus
objWinInfo.intWinStatus = cwModeSingleAddRest
End Sub

Private Sub txthora_Change()
Dim fecha As String

grdDBGrid1(0).Enabled = False
  
  
If IsNumeric(txthora.Text) = True Then
 If txthora.Text > 23 Then
    Beep
    txthora.Text = ""
 End If
 
  If dtcfecha.Text <> "" Then
     If (txthora.Text <> "" And txtminuto <> "") Then
       fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
        objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     Else
       fecha = dtcfecha.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
        objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     End If
 End If
Else
 txthora.Text = ""
End If

End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
intcambioalgo = 1
End Sub

Private Sub txtminuto_Change()
Dim fecha As String

grdDBGrid1(0).Enabled = False
  
If IsNumeric(txtminuto.Text) = True Then
  If txtminuto.Text > 59 Then
    Beep
    txtminuto.Text = ""
  End If
  If dtcfecha.Text <> "" Then
     If (txthora.Text <> "" And txtminuto <> "") Then
       fecha = dtcfecha.Text & " " & txthora.Text & ":" & txtminuto.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
         objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     Else
       fecha = dtcfecha.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
          objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     End If
  End If
Else
  txtminuto.Text = ""
End If
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
intcambioalgo = 1
intminuto = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim mensaje As String

  tlbToolbar1.Buttons(2).Enabled = False
  Call objWinInfo.CtrlLostFocus
  'se controla que el c�digo del realizador de la extracci�n sea correcto
  If intIndex = 7 Then
  If txtText1(7).Text <> "" Then
      stra = "SELECT COUNT(*) FROM PR5203J WHERE AD02CODDPTO=" & glngdptologin & _
         " AND SG02COD='" & txtText1(7).Text & "'"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.rdoColumns(0).Value = 0 Then
          mensaje = MsgBox("El c�digo introducido no existe o no pertenece al departamento", vbExclamation, "Aviso")
          txtText1(7).Text = ""
          txtText1(7).SetFocus
      End If
  End If
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim strfec As String
  Dim fec As String
  Dim hora As String
  Dim stra As String
  Dim rsta As rdoResultset
  Dim cuestionariomuestra As Boolean
  Dim cuestionariotipomuestra As Boolean
  
  tlbToolbar1.Buttons(2).Enabled = False
  
 
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
      intaux = 0
      intcambioalgo = 0
  End If
  If intminuto = 1 Then
    intminuto = 0
  Else
        If (intIndex = 23) Then 'fecha y hora de preferencia
           hora = txtText1(23).Text
           If hora <> "" Then
              If intaux = 0 Then
               strfechaaux = txtText1(23)
               intaux = 1
              End If
              dtcfecha.Text = Left(strfechaaux, 10)
              If Len(strfechaaux) > 10 Then
                txthora = Right(Left(strfechaaux, 13), 2)
                txtminuto = Right(Left(strfechaaux, 16), 2)
              Else
                txthora = ""
                txtminuto = ""
              End If
           Else
            dtcfecha.Text = ""
            txthora = ""
            txtminuto = ""
          End If
        End If
  End If
End Sub



Private Sub UpDownhora_DownClick()
txthora.SetFocus
  txthora = UpDownhora.Value
  intcambioalgo = 1
End Sub

Private Sub UpDownhora_UpClick()
txthora.SetFocus
  txthora = UpDownhora.Value
  intcambioalgo = 1
End Sub



Private Sub UpDownminuto_DownClick()
txtminuto.SetFocus
txtminuto = UpDownminuto.Value
End Sub

Private Sub UpDownminuto_UpClick()
txtminuto.SetFocus
  txtminuto = UpDownminuto.Value
End Sub
