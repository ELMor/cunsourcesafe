VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmmuestra 
   BackColor       =   &H8000000A&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaci�n. Muestras"
   ClientHeight    =   6360
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   10830
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0102.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe 
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Index           =   1
      Left            =   240
      TabIndex        =   27
      Top             =   480
      Width           =   11355
      Begin VB.TextBox txtAct 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         ToolTipText     =   "C�digo de la Actuaci�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtAct 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   1
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         ToolTipText     =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.Label lblcodact 
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   7
         Left            =   240
         TabIndex        =   26
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lbldesact 
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   2160
         TabIndex        =   25
         Top             =   360
         Width           =   2655
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Muestras"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5655
      Index           =   0
      Left            =   240
      TabIndex        =   16
      Top             =   1800
      Width           =   11295
      Begin TabDlg.SSTab tabTab1 
         Height          =   5100
         Index           =   0
         Left            =   120
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   360
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   8996
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   -2147483638
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0102.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(9)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(10)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(11)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboSSDBCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "tabTab1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtAct(2)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(1)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(3)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(10)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(11)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(12)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "chkCheck1(5)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "chkCheck2(5)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "Check1"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(5)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).ControlCount=   26
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0102.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR24CODMUESTRA"
            Height          =   330
            Index           =   5
            Left            =   360
            TabIndex        =   1
            Tag             =   "C�digo Muestra"
            Top             =   600
            Width           =   1095
         End
         Begin VB.CheckBox Check1 
            BackColor       =   &H00FFFF00&
            Caption         =   "Check1"
            DataField       =   "PR25INDHORA"
            Height          =   255
            Left            =   4080
            TabIndex        =   6
            Top             =   4680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CheckBox chkCheck2 
            BackColor       =   &H00FFFF00&
            Caption         =   "NO"
            Height          =   255
            Index           =   5
            Left            =   840
            TabIndex        =   8
            Tag             =   "Indica si requiere hora exacta de extracci�n"
            Top             =   2040
            Width           =   615
         End
         Begin VB.CheckBox chkCheck1 
            BackColor       =   &H00FFFF00&
            Caption         =   "SI"
            Height          =   255
            Index           =   5
            Left            =   360
            TabIndex        =   7
            Tag             =   "Indica si requiere hora exacta de extracci�n"
            Top             =   2040
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR25GRUPOMUESTRA"
            Height          =   330
            Index           =   12
            Left            =   8520
            TabIndex        =   11
            Tag             =   "Grupos entubables"
            Top             =   2040
            Width           =   1935
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR25INDISTINTO"
            Height          =   330
            Index           =   11
            Left            =   6000
            TabIndex        =   10
            Tag             =   "Grupos de muestras necesarias"
            Top             =   2040
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR25ETIQADICIONAL"
            Height          =   330
            Index           =   10
            Left            =   3600
            TabIndex        =   9
            Tag             =   "N�mero de etiquetas adicionales a imprimir"
            Top             =   2040
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR25DILUCION"
            Height          =   330
            Index           =   4
            Left            =   8520
            TabIndex        =   5
            Tag             =   "Diluci�n de la muestra (tanto por 1)"
            Top             =   1320
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR25VOLUMENPREF"
            Height          =   330
            Index           =   3
            Left            =   6000
            TabIndex        =   4
            Tag             =   "Volumen preferible necesario en CC"
            Top             =   1320
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR25VOLUMENMIN"
            Height          =   330
            Index           =   1
            Left            =   3600
            TabIndex        =   3
            Tag             =   "Volumen m�nimo necesario en CC"
            Top             =   1320
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR25TIEMPO"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   2
            Tag             =   "Tiempo en minutos respecto a la primera"
            Top             =   1320
            Width           =   1095
         End
         Begin VB.TextBox txtAct 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Descripci�n de la Muestra"
            Top             =   600
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   2
            Left            =   1320
            TabIndex        =   0
            Tag             =   "C�digo Actuaci�n|C�d. Actuaci�n"
            Top             =   4680
            Visible         =   0   'False
            Width           =   1935
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5025
            Index           =   0
            Left            =   -74880
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   90
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   8864
            _StockProps     =   79
            Caption         =   "MUESTRAS"
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   1860
            Index           =   1
            Left            =   240
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   2640
            Width           =   9615
            _ExtentX        =   16960
            _ExtentY        =   3281
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Comentario Impresi�n"
            TabPicture(0)   =   "PR0102.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "txtText1(7)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Comentario Pantalla"
            TabPicture(1)   =   "PR0102.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(8)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR25COMENTINFORMA"
               Height          =   1170
               HelpContextID   =   30104
               Index           =   8
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   13
               Tag             =   "Comentarios a la muestra por pantalla"
               Top             =   480
               Width           =   9225
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR25COMENTIMPRI"
               Height          =   1170
               HelpContextID   =   30104
               Index           =   7
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   12
               Tag             =   "Comentarios en la impresi�n de la muestra"
               Top             =   480
               Width           =   9225
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2865
               Index           =   1
               Left            =   -74910
               TabIndex        =   33
               TabStop         =   0   'False
               Top             =   90
               Width           =   8655
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15266
               _ExtentY        =   5054
               _StockProps     =   79
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            Height          =   330
            Index           =   0
            Left            =   8640
            TabIndex        =   38
            Tag             =   "C�digo Muestra"
            Top             =   600
            Visible         =   0   'False
            Width           =   1095
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "C�DIGO"
            Columns(0).Name =   "C�DIGO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1940
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   65535
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   1560
            TabIndex        =   39
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Grupos Entubables"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   8520
            TabIndex        =   37
            Top             =   1800
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Grupos de muestras "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   6000
            TabIndex        =   36
            Top             =   1800
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Etiquetas a imprimir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   3600
            TabIndex        =   35
            Top             =   1800
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�Hora exacta de extracci�n?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   360
            TabIndex        =   34
            Top             =   1800
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Diluci�n (tanto por 1)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   8520
            TabIndex        =   31
            Top             =   1080
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Volumen Preferente (CC)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   6000
            TabIndex        =   30
            Top             =   1080
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Volumen M�nimo (CC)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   3600
            TabIndex        =   29
            Top             =   1080
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo respecto a la 1� muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   360
            TabIndex        =   28
            Top             =   1080
            Width           =   2775
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   20
            Top             =   360
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2400
            TabIndex        =   19
            Top             =   360
            Width           =   2415
         End
      End
   End
   Begin VB.CommandButton cmdcuestionario 
      Caption         =   "Cuestionario"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4680
      TabIndex        =   15
      Top             =   7680
      Width           =   2055
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   21
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmmuestra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00102.FRM                                                  *
'* AUTOR: JESUS MARIA RODILLA LARA                                      *
'* FECHA: 6 DE AGOSTO DE 1997                                           *
'* DESCRIPCION: permite definir nuestras para una actuaci�n. Tambi�n se *
'*              puede modificar muestras existentes                     *
'* ARGUMENTOS:  PR01CODACTUACION, PR01DESACTUACION                      *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim blnAltaMasiva As Boolean

Private Function CrearFiltroInforme(strEntrada As String, strPrefijo As String, strBuscar As String)
Dim strSalida As String
Dim strPalabra1 As String

    Do While Len(strEntrada) > 0
       strPalabra1 = Left(strEntrada, Len(strBuscar))
       If strPalabra1 = strBuscar Then
          strSalida = strSalida & strPrefijo & strPalabra1
          strEntrada = Right(strEntrada, Len(strEntrada) - Len(strBuscar))
       Else
          strSalida = strSalida & Left(strEntrada, 1)
          strEntrada = Right(strEntrada, Len(strEntrada) - 1)
       End If
    Loop
    CrearFiltroInforme = strSalida
End Function


Private Sub Check1_Click()
    
    If Check1.Value = 0 Then
        chkCheck2(5).Value = 1
        chkCheck1(5).Value = 0
    Else
        chkCheck2(5).Value = 0
        chkCheck1(5).Value = 1
    End If
  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus

End Sub

Private Sub Check1_GotFocus()

    Call objWinInfo.CtrlGotFocus

End Sub

Private Sub Check1_LostFocus()

    Call objWinInfo.CtrlLostFocus

End Sub

Private Sub chkCheck1_Click(Index As Integer)
  
  If chkCheck1(Index).Value = 1 Then
    chkCheck2(Index).Value = 0
    Call objWinInfo.CtrlSet(Check1, True)
  End If
  
End Sub


Private Sub chkCheck2_Click(Index As Integer)

  If chkCheck2(Index).Value = 1 Then
    chkCheck1(Index).Value = 0
    Call objWinInfo.CtrlSet(Check1, False)
  End If

End Sub

Private Sub cmdcuestionario_Click()
      
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
      Call objWinInfo.DataSave
      Call objsecurity.LaunchProcess("PR0103")

End Sub

Private Sub Form_Activate()
    txtAct(0).Text = frmdefactuacionesCUN.txtText1(0).Text 'C�d. de la actuaci�n
    txtAct(1).Text = frmdefactuacionesCUN.txtText1(2).Text   'Descripci�n de la actuaci�n
    blnAltaMasiva = False
    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If
    
    
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Muestra"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)

    .strTable = "PR2500"
    .strWhere = "PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0).Text
    
    ''Call .FormAddOrderField("PR25CODMUESTRA", cwAscending)
    Call .FormAddOrderField("PR24CODMUESTRA", cwAscending)
    
    Call .objPrinter.Add("PR1021", "Listado de Muestras")

    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "PR2500")
    Call .FormAddFilterWhere(strKey, "PR24CODMUESTRA", "C�d. Muestra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR25TIEMPO", "Tiempo en minutos respecto a la primera", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR25VOLUMENMIN", "Volumen m�nimo necesario en CC", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR25VOLUMENPREF", "Volumen preferible necesario en CC", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR25DILUCION", "Diluci�n de la muestra (tanto por 1)", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR25COMENTIMPRI", "Comentarios en la impresi�n de la muestra", cwString)
    Call .FormAddFilterWhere(strKey, "PR25INDHORA", "Indica si requiere hora exacta de extracci�n", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR25ETIQADICIONAL", "N�mero de etiquetas adicionales a imprimir", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR25INDISTINTO", "Grupos de muestras necesarias", cwString)
    Call .FormAddFilterWhere(strKey, "PR25COMENTINFORMA", "Comentarios a la muestra por pantalla", cwString)
    Call .FormAddFilterWhere(strKey, "PR25GRUPOMUESTRA", "Grupos entubables", cwString)

    
    'Se establecen los campos por los que se puede ordenar con el filtro
    ''Call .FormAddFilterOrder(strKey, "PR25CODMUESTRA", "C�d. Muestra")
    ''Call .FormAddFilterOrder(strKey, "PR25DESMUESTRA", "Descripci�n Muestra")
    ''Call .FormAddFilterOrder(strKey, "PR24CODTIPMUESTR", "C�d. Tipo Muestra")
    Call .FormAddFilterOrder(strKey, "PR24CODMUESTRA", "C�d. Muestra")
    Call .FormAddFilterOrder(strKey, "PR25TIEMPO", "Tiempo en minutos respecto a la primera")
    Call .FormAddFilterOrder(strKey, "PR25VOLUMENMIN", "Volumen m�nimo necesario en CC")
    Call .FormAddFilterOrder(strKey, "PR25VOLUMENPREF", "Volumen preferible necesario en CC")
    Call .FormAddFilterOrder(strKey, "PR25DILUCION", "Diluci�n de la muestra (tanto por 1)")
    Call .FormAddFilterOrder(strKey, "PR25COMENTIMPRI", "Comentarios en la impresi�n de la muestra")
    Call .FormAddFilterOrder(strKey, "PR25INDHORA", "Indica si requiere hora exacta de extracci�n")
    Call .FormAddFilterOrder(strKey, "PR25ETIQADICIONAL", "N�mero de etiquetas adicionales a imprimir")
    Call .FormAddFilterOrder(strKey, "PR25INDISTINTO", "Grupos de muestras necesarias")
    Call .FormAddFilterOrder(strKey, "PR25COMENTINFORMA", "Comentarios a la muestra por pantalla")
    Call .FormAddFilterOrder(strKey, "PR25GRUPOMUESTRA", "Grupos entubables")
    
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
        
    ''.CtrlGetInfo(txtText1(0)).blnInFind = True
    ''.CtrlGetInfo(txtText1(1)).blnInFind = True
    ''.CtrlGetInfo(cboSSDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtAct(2)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    '.CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(Check1).blnInFind = True

    .CtrlGetInfo(chkCheck1(5)).blnNegotiated = True
    .CtrlGetInfo(chkCheck2(5)).blnNegotiated = True
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    '.CtrlGetInfo(cboSSDBCombo1(0)).blnForeign = True
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    
    .CtrlGetInfo(txtAct(2)).blnNegotiated = False
    
    '.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR24CODMUESTRA,PR24DESCORTA FROM PR2400 ORDER BY PR24CODMUESTRA ASC"
    
    'Se muestra la descripci�n del tipo de muestra
    ''Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR24CODMUESTRA", "SELECT * FROM PR2400 WHERE PR24CODMUESTRA = ?")
    'Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR24CODMUESTRA", "SELECT * FROM PR2400 WHERE PR24CODMUESTRA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtAct(2), "PR24DESCORTA")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "PR24CODMUESTRA", "SELECT * FROM PR2400 WHERE PR24CODMUESTRA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtAct(2), "PR24DESCORTA")

    
    Call .WinRegister
    Call .WinStabilize
  End With

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Click(Index As Integer)
    
    'Activamos o desactivamos el bot�n del cuestionario de la muestra
    ''strMuestra = txtText1(0).Text

    If (grdDBGrid1(0).SelBookmarks.Count <> 0) Then
        cmdcuestionario.Enabled = True
    Else
        cmdcuestionario.Enabled = False
    End If

End Sub

Private Sub lblcodact_Click(Index As Integer)
   'cboSSDBCombo1(5).SetFocus
   txtText1(5).SetFocus
End Sub

Private Sub lbldesact_Click(Index As Integer)
   'cboSSDBCombo1(0).SetFocus
    txtText1(5).SetFocus
      
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
    ''txtText1(2).Text = txtAct(0).Text
    Call objWinInfo.CtrlSet(txtText1(2), txtAct(0).Text)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strFiltro As String
  Dim strOrden As String
  
  If strFormName = "Muestra" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      
      If blnHasFilter = False Then
          strFiltro = CrearFiltroInforme(objWinInfo.DataGetWhere(blnHasFilter), "PR2502J.", "PR01CODACTUACION")
      Else
          strFiltro = CrearFiltroInforme(objWinInfo.DataGetWhere(blnHasFilter), "PR2502J.", "PR01CODACTUACION")
          strFiltro = CrearFiltroInforme(strFiltro, "PR2502J.", "PR25CODMUESTRA")
          'strFiltro = CrearFiltroInforme(strFiltro, "PR2502J.", "PR25DESMUESTRA")
          'strFiltro = CrearFiltroInforme(strFiltro, "PR2502J.", "PR24CODTIPMUESTR")
      End If
       strOrden = CrearFiltroInforme(objWinInfo.DataGetOrder(blnHasFilter, True), "PR2502J.", "PR24CODMUESTRA")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25TIEMPO")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25VOLUMENMIN")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25VOLUMENPREF")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25DILUCION")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25COMENTIMPRI")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25INDHORA")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25ETIQADICIONAL")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25INDISTINTO")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25COMENTINFORMA")
       strOrden = CrearFiltroInforme(strOrden, "PR2502J.", "PR25GRUPOMUESTRA")

      Call objPrinter.ShowReport(strFiltro, strOrden)
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  
  If strFormName = "Muestra" Then
    frmtiposdemuestra.cmdAceptar.Visible = True
    'Call objsecurity.LaunchProcess("PR0230") 'Para multiples DLLs
    Call objsecurity.LaunchProcess("PR0206") 'Para una sola DLL
    'Call objWinInfo.CtrlSet(cboSSDBCombo1(0), guCodigo)
    If guCodigo <> "" Then
        Call objWinInfo.CtrlSet(txtText1(5), guCodigo)
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtAct(2)), gstrdescripcion)
        Call objWinInfo.CtrlSet(txtAct(2), gstrdescripcion)
        'Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
    End If
  End If
 guCodigo = ""
 gstrdescripcion = ""

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  'If strFormName = "Muestra" And strCtrl = "cboSSDBCombo1(0)" Then
  If strFormName = "Muestra" And strCtrl = "txtText1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "PR2400"
      .strOrder = "ORDER BY PR24CODMUESTRA ASC"
      
      Set objField = .AddField("PR24CODMUESTRA")
      objField.strSmallDesc = "C�d. Muestra"
      
      Set objField = .AddField("PR24DESCORTA")
      objField.strSmallDesc = "Descripci�n Muestra"
      
      If .Search Then
        'Call objWinInfo.CtrlSet(cboSSDBCombo1(0), .cllValues("PR24CODMUESTRA"))
        Call objWinInfo.CtrlSet(txtText1(5), .cllValues("PR24CODMUESTRA"))
        Call objWinInfo.CtrlSet(txtAct(2), .cllValues("PR24DESCORTA"))
      End If
      
    End With
    Set objSearch = Nothing

    'Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
  End If
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
    If tlbToolbar1.Buttons(4).Enabled Then
        If chkCheck1(5).Value + chkCheck2(5).Value = 0 Then
            Call MsgBox("El campo Hora Exacta es obligatorio", vbInformation)
            Exit Sub
        End If
    End If
  
    If btnButton.Index = 30 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Exit Sub
    End If
    
    If (btnButton.Index = 4) Then
        If chkCheck1(5).Value + chkCheck2(5).Value = 0 Then
            Call MsgBox("El campo Hora Exacta es obligatorio", vbInformation)
            Exit Sub
        End If
    End If
  
    If (btnButton.Index = 8) Then
        If (Tiene_Cuestionario = False) Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
    Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If

    'If cboSSDBCombo1(0).Text <> "" Then
    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If

    If btnButton.Index = 2 Then
        chkCheck1(5).Value = 0
        chkCheck2(5).Value = 0
        txtAct(2).SetFocus
    End If
  
  Exit Sub
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

    If tlbToolbar1.Buttons(4).Enabled Then
        If chkCheck1(5).Value + chkCheck2(5).Value = 0 Then
            Call MsgBox("El campo Hora Exacta es obligatorio", vbInformation)
            Exit Sub
        End If
    End If

    If intIndex = 100 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        Exit Sub
    End If
  
    If (intIndex = 40) Then
        If chkCheck1(5).Value + chkCheck2(5).Value = 0 Then
            Call MsgBox("El campo Hora Exacta es obligatorio", vbInformation)
            Exit Sub
        End If
    End If
  
    If (intIndex = 40) And (mnuOpcionesOpcion.Item(50).Checked = True) Then
        blnAltaMasiva = True
    End If
  
    If (intIndex = 60) Then
        If (Tiene_Cuestionario = False) Then
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        End If
    Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
  
    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If
  
 Exit Sub
 
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)

    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)

    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)

    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If

End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
    
    If txtText1(5).Text <> "" Then
        If Check1.Value = 0 Then
            chkCheck1(5).Value = 0
            chkCheck2(5).Value = 1
        Else
            chkCheck1(5).Value = 1
            chkCheck2(5).Value = 0
        End If
    End If
   
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
                              
                              
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtAct_GotFocus(Index As Integer)
   
   If txtText1(5).Text = "" Then
    txtText1(5).SetFocus
   Else
    txtText1(1).SetFocus
   End If

   
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim rstB As rdoResultset
  Dim strsqlA As String
  Dim strsqlB As String
  Dim strPregunta As String
  Dim intCont As Integer
  Dim strMensage As String
  Dim intRespuesta As String
  
  Call objWinInfo.CtrlLostFocus
  
  intCont = 0
  If (txtAct(2).Text = "") And (intIndex = 5) And (txtText1(5).Text <> "") Then
    strsqlB = "SELECT COUNT(*) " _
             & "FROM PR2400 " _
             & "WHERE PR24CODMUESTRA =" & txtText1(5).Text
    Set rstB = objApp.rdoConnect.OpenResultset(strsqlB)
    intCont = rstB.rdoColumns(0).Value
    rstB.Close
    Set rstB = Nothing
    If intCont > 0 Then
      strsqlA = "SELECT PR24DESCORTA " _
               & "FROM PR2400 " _
               & "WHERE PR24CODMUESTRA =" & txtText1(5).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
      txtAct(2).Text = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
    Else
      strMensage = "No existe ninguna Muestra con el c�digo " & txtText1(5).Text
      intRespuesta = MsgBox(strMensage, vbInformation, "Aviso")
      objWinInfo.objWinActiveForm.blnChanged = False
      objWinInfo.DataNew
    End If
  End If

End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim strmuestra As String
  
  If intIndex = 0 Then
     ''txtText1(2).Text = frmdefactuacionesCUN.txtText1(0)
     Call objWinInfo.CtrlSet(txtText1(2), frmdefactuacionesCUN.txtText1(0))
  End If
  Call objWinInfo.CtrlDataChange
  
  'Activamos o desactivamos el bot�n del cuestionario de la muestra
  'strmuestra = cboSSDBCombo1(0).Text
  strmuestra = txtText1(5).Text
  If (strmuestra = "") Then
        cmdcuestionario.Enabled = False
  Else
        cmdcuestionario.Enabled = True
  End If
End Sub


Public Function Tiene_Cuestionario() As Boolean
'JMRL 4/12/97
'Tiene_Cuestionario es una funci�n que devuelve TRUE si hay ning�n problema a la hora de
'borrar una muestra.

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "La muestra NO puede se borrada por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
  'Se comprueba que la muestras no tiene CUESTIONARIO
  strSelect = "SELECT COUNT(*) FROM PR2600" & _
              " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
              "   AND PR24CODMUESTRA=" & txtText1(5).Text & _
              "   AND PR25TIEMPO=" & txtText1(0).Text
  
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La muestra TIENE CUESTIONARIO." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
      
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Cuestionario = True
  Else
    Tiene_Cuestionario = False
  End If
  

End Function
