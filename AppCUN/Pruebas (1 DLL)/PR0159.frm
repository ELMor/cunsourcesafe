VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmactnosel 
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Actuaciones no Seleccionadas"
   ClientHeight    =   5970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7410
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5970
   ScaleWidth      =   7410
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      Caption         =   "Salir"
      Height          =   375
      Left            =   5520
      TabIndex        =   0
      Top             =   7920
      Width           =   1935
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   6015
      Index           =   0
      Left            =   1680
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1680
      Width           =   8520
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   0
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   15028
      _ExtentY        =   10610
      _StockProps     =   79
      Caption         =   "ACTUACIONES NO SELECCIONADAS"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Las siguientes actuaciones no formar�n parte de la petici�n pues no cumplen alguna de sus condiciones."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1680
      TabIndex        =   2
      Top             =   600
      Width           =   7335
   End
End
Attribute VB_Name = "frmactnosel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00130.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION:  Pantalla informativa de las interacciones no generadas *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Private Sub Command1_Click()
    grdDBGrid1(0).RemoveAll
    Unload Me
End Sub



Private Sub Form_Activate()
Dim i As Integer
Dim j As Integer

grdDBGrid1(0).Columns(0).Caption = "C�DIGO"
grdDBGrid1(0).Columns(1).Caption = "DESCRIPCI�N"
grdDBGrid1(0).Columns(1).Width = 6300

grdDBGrid1(0).Row = 0
grdDBGrid1(0).Col = 0
grdDBGrid1(0).AddItem "", grdDBGrid1(0).Row

'el Sub LlenarTabla es global(est� en un m�dulo)
'numelem indica el n� de actuaciones que no ser�n seleccionadas
'porque no se ha clicado alguna de sus condiciones
j = 1
For i = 1 To (numelem - 1)
    LlenarTabla tabla(i), tabla2(j)
    j = j + 1
Next i

End Sub




