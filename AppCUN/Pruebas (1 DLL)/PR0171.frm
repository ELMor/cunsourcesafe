VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmSelGruposP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Grupos"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0171.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdDescrAct 
      Caption         =   "Descripci�n Actuaci�n"
      Height          =   375
      Index           =   1
      Left            =   7320
      TabIndex        =   16
      Top             =   7680
      Width           =   2175
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones del Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4455
      Index           =   2
      Left            =   360
      TabIndex        =   5
      Top             =   3120
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3975
         Index           =   2
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         UseDefaults     =   0   'False
         _ExtentX        =   18785
         _ExtentY        =   7011
         _StockProps     =   79
         Caption         =   "ACTUACIONES DEL GRUPO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   0
      Left            =   360
      TabIndex        =   3
      Top             =   480
      Width           =   10935
      Begin TabDlg.SSTab tabTab1 
         Height          =   1935
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _ExtentX        =   18785
         _ExtentY        =   3413
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0171.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lbllabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0171.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR15CODTIPGRUPO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   3
            Left            =   1080
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "C�digo Grupo"
            Top             =   1320
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   2
            Left            =   3600
            MultiLine       =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Descripci�n Grupo"
            Top             =   1320
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR16CODGRUPO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   1080
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "C�digo Grupo"
            Top             =   600
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR16DESGRUPO"
            Height          =   330
            Index           =   1
            Left            =   3600
            MultiLine       =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Descripci�n Grupo"
            Top             =   600
            Width           =   5400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1650
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   120
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   2910
            _StockProps     =   79
            Caption         =   "GRUPOS"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Descripci�n Tipo Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   3600
            TabIndex        =   14
            Top             =   1080
            Width           =   2175
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Descripci�n Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   3600
            TabIndex        =   12
            Top             =   360
            Width           =   2175
         End
         Begin VB.Label lbllabel1 
            Caption         =   "C�digo Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1080
            TabIndex        =   11
            Top             =   360
            Width           =   2175
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Tipo Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   1080
            TabIndex        =   10
            Top             =   1080
            Width           =   2175
         End
      End
   End
   Begin VB.CommandButton cmdAceptarGrupos 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   2640
      TabIndex        =   0
      Top             =   7680
      Width           =   1935
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   8055
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   "Tipo Grupo"
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSelGruposP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00106.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 14 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite crear nuevos grupos de actuaciones. Tambi�n     *
'*              se puede modificar un grupo                             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim refrescar As Boolean
Dim gblnselec As Boolean

Private Sub Comprobar_Repetidas(ByVal lngcodact As Long, ByVal lngcoddpto As Long)
    Dim mintI As Integer
    Dim strrespuesta As String
    Dim cont As Integer
    
    
    frmSeleccionarAct.grdDBGrid1(0).MoveFirst
    cont = 0
    Do While cont < frmSeleccionarAct.grdDBGrid1(0).Rows
        If (lngcodact = frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value) And _
           (lngcoddpto = frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value) Then
           strrespuesta = MsgBox("La actuaci�n '" _
                        & frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value & "'" _
                        & " del Departamento '" & _
                        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value & "'" _
                        & Chr(13) & " ya ha sido seleccionada." & Chr(13) & Chr(13) & _
                        "                      �Desea seleccionarla?", vbYesNo, "SELECCION DE ACTUACIONES")
            If strrespuesta <> vbYes Then
                gblnselec = False
            End If
           Exit Do
        End If
        If cont = frmSeleccionarAct.grdDBGrid1(0).Rows - 1 Then
            Exit Do
        Else
            frmSeleccionarAct.grdDBGrid1(0).MoveNext
            cont = cont + 1
        End If
    Loop
   frmSeleccionarAct.grdDBGrid1(0).MoveFirst
End Sub
Private Sub Preguntar_Dpto(ByVal mvarBkmrk As Variant)
   gpasardpto = grdDBGrid1(2).Columns(3).CellValue(mvarBkmrk)
   frmdefdptosP.txtactText1(0).Text = grdDBGrid1(2).Columns(3).CellValue(mvarBkmrk)
   frmdefdptosP.txtactText1(1).Text = grdDBGrid1(2).Columns(4).CellValue(mvarBkmrk)
   'Load frmdefdptosP
   'frmdefdptosP.Show (vbModal)
   'Unload frmdefdptosP
   'Set frmdefdptosP = Nothing
   Call objsecurity.LaunchProcess("PR0172")
End Sub
Private Sub seleccionar_actuacion(ByVal codact As Long, ByVal codpadre As Long, ByVal mvarBkmrk As Variant, ByVal coddpto As Long)
    Dim rsta As rdoResultset
    Dim sqlstr As String
    Dim rstAcod As rdoResultset
    Dim sqlstrcodcond As String
    Dim rstAcodcond As rdoResultset
    Dim sqlstrcod As String
    Dim rstAcoddpto As rdoResultset
    Dim sqlstrcoddpto As String
    Dim rstAcoddpto2 As rdoResultset
    Dim sqlstrcoddpto2 As String
    Dim muactaso As Long
    Dim mudptoaso As Long
    
        sqlstrcod = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & codact
        Set rstAcod = objApp.rdoConnect.OpenResultset(sqlstrcod)
        
        If coddpto = 0 Then
            sqlstrcoddpto2 = "SELECT count(*) FROM PR0200 WHERE PR01CODACTUACION=" & codact
            Set rstAcoddpto2 = objApp.rdoConnect.OpenResultset(sqlstrcoddpto2)
            If rstAcoddpto2(0).Value > 1 Then
              Call Preguntar_Dpto(mvarBkmrk)
              coddpto = frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value
              Call Comprobar_Repetidas(codact, coddpto)
            Else
              sqlstrcoddpto2 = "SELECT ad02coddpto FROM PR0200 WHERE PR01CODACTUACION=" & codact
              Set rstAcoddpto2 = objApp.rdoConnect.OpenResultset(sqlstrcoddpto2)
              coddpto = rstAcoddpto2(0).Value
              Call Comprobar_Repetidas(codact, coddpto)
            End If
            rstAcoddpto2.Close
            Set rstAcoddpto2 = Nothing
        End If
                
    If gblnselec = True Then
        sqlstrcoddpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & coddpto
        Set rstAcoddpto = objApp.rdoConnect.OpenResultset(sqlstrcoddpto)
        
        frmSeleccionarAct.grdDBGrid1(0).AddNew
        frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value = codact
        frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value = rstAcod.rdoColumns("PR01DESCORTA").Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value = coddpto
        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value = rstAcoddpto.rdoColumns("AD02DESDPTO").Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(0).Value = True
        frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = False
        
        sqlstrcodcond = "SELECT * FROM PR2300 WHERE PR01CODACTUACION=" & codact
        Set rstAcodcond = objApp.rdoConnect.OpenResultset(sqlstrcodcond)
        
        If rstAcodcond.EOF = False Then
            glngSelCond = glngSelCond + 1
        End If
        frmSeleccionarAct.grdDBGrid1(0).Columns(9).Value = codpadre
        frmSeleccionarAct.grdDBGrid1(0).Update
        
        sqlstr = "SELECT * FROM PR3100 WHERE PR01CODACTUACION=" & codact
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        With rsta
            Do Until rsta.EOF
                muactaso = .rdoColumns("PR01CODACTUACION_ASO")
                mudptoaso = .rdoColumns("AD02CODDPTO")
                Call seleccionar_actuacion(muactaso, codact, mvarBkmrk, mudptoaso)
            rsta.MoveNext
            Loop
       End With
        rsta.Close
        Set rsta = Nothing
        rstAcod.Close
        Set rstAcod = Nothing
        rstAcodcond.Close
        Set rstAcodcond = Nothing
        rstAcoddpto.Close
        Set rstAcoddpto = Nothing
    Else
        gblnselec = True
    End If
End Sub

Private Sub cmdAceptarGrupos_Click()
    Dim mintisel As Integer
    Dim mintNTotalSelRows As Integer
    Dim mvarBkmrk As Variant
    
        'Guardamos el n�mero de filas seleccionadas
        mintNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
        'minti = 1
        gblnselec = True
        For mintisel = 0 To mintNTotalSelRows - 1
            'Guardamos el n�mero de fila que est� seleccionada
            mvarBkmrk = grdDBGrid1(2).SelBookmarks(mintisel)
            Call seleccionar_actuacion(grdDBGrid1(2).Columns(3).CellValue(mvarBkmrk), 0, mvarBkmrk, 0)
        Next mintisel
        If glngSelCond = 0 Then
            frmSeleccionarAct.cmdpeticion.Enabled = True
        Else
            frmSeleccionarAct.cmdpeticion.Enabled = False
        End If
    Unload Me
End Sub

Private Sub cmdDescrAct_Click(Index As Integer)

Dim sqlstrdesl As String
Dim rstdesl As rdoResultset
Dim codact As Long

'JRC 28/4/98
If grdDBGrid1(2).Rows > 0 Then
  codact = grdDBGrid1(2).Columns(3).Value
  If IsNull(codact) = False Then
    sqlstrdesl = "SELECT PR01DESCOMPLETA FROM PR0100 WHERE PR01CODACTUACION=" & codact
    Set rstdesl = objApp.rdoConnect.OpenResultset(sqlstrdesl)
    If Not rstdesl.EOF Then
        If IsNull(rstdesl.rdoColumns("PR01DESCOMPLETA").Value) = False Then
            Call MsgBox("Descripci�n completa actuaci�n " & codact & " : " & Chr(13) & rstdesl.rdoColumns("PR01DESCOMPLETA").Value, vbInformation)
        Else
            Call MsgBox("La actuaci�n " & codact & " no tiene descripci�n completa.", vbExclamation)
        End If
    End If
    rstdesl.Close
    Set rstdesl = Nothing
  Else
    Call MsgBox("No hay ninguna actuaci�n seleccionada.", vbExclamation)
  End If
Else
    Call MsgBox("No hay ninguna actuaci�n.", vbExclamation)
End If


End Sub

Private Sub Form_Activate()
    txtText1(0).SetFocus
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMasterInfo
        .strName = "Grupos"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(0)
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR1600"
        .intAllowance = cwAllowReadOnly
        'Call .objPrinter.Add("PR001061", "Listado de grupos y actuaciones")
        'Call .objPrinter.Add("PR001062", "Listado de cuestionario de un grupo")
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormAddOrderField("PR16codgrupo", cwAscending)
        Call .FormCreateFilterWhere(strKey, "Grupos")
        Call .FormAddFilterWhere(strKey, "PR16CODGRUPO", "C�digo Grupo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR16DESGRUPO", "Descripci�n Grupo", cwString)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR16CODGRUPO", "C�digo Grupo")
        Call .FormAddFilterOrder(strKey, "PR16DESGRUPO", "Descripci�n")
    End With
  
  
    With objMultiInfo
        .strName = "Actuaciones del grupo"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = fraFrame1(0)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(2)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .intAllowance = cwAllowReadOnly
        ' s�lo las actuaciones con alguna fase (y la fase con al menos un tipo de recurso)
        .strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion))) AND " _
            & "pr01codactuacion not in (select pr01codactuacion from PR0100 " _
            & " where pr01fecfin < (select sysdate from dual))"
  
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR1700"

        .intCursorSize = 0
    
        Call .FormAddOrderField("PR16codgrupo", cwAscending)
        Call .FormAddRelation("PR16codgrupo", txtText1(0))
 
        strKey = .strDataBase & .strTable
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Actuaciones del Grupo")
        Call .FormAddFilterWhere(strKey, "PR16CODGRUPO", "C�digo Grupo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR35CODGRUPO", "C�digo Grupo")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    End With

    With objWinInfo


        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
        
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del grupo
        Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d. Grupo", "PR16CODGRUPO", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Grupo", "", cwString, 30)

        Call .FormCreateInfo(objMasterInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True
        
        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del grupo
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnMandatory = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(5)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(6)).blnInFind = True
    

        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del grupo
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), grdDBGrid1(2).Columns(4), "PR01DESCORTA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), "PR16CODGRUPO", "SELECT * FROM PR1600 WHERE PR16CODGRUPO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(6), "PR16DESGRUPO")
     
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "PR15CODTIPGRUPO", "SELECT * FROM PR1500 WHERE PR15CODTIPGRUPO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(2), "PR15DESTIPGRUPO")

        Call .WinRegister
        Call .WinStabilize
    End With
    'Call objApp.SplashOff
    'Call objWinInfo.WinProcess(6, 24, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Grupos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)

    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
      
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



