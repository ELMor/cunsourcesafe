VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{A7E3152D-9E00-11D1-9146-00C04FBB52E1}#1.0#0"; "idperson.ocx"
Begin VB.Form frmRecibirPaciente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Recibir Paciente"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   Icon            =   "PR0139.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdanularconsen 
      Caption         =   "Anular Consentimiento"
      Height          =   375
      Left            =   8640
      TabIndex        =   9
      Top             =   7680
      Width           =   2295
   End
   Begin VB.CommandButton cmdConsentimiento 
      Caption         =   "Firmar Consentimiento"
      Height          =   375
      Left            =   6000
      TabIndex        =   8
      Top             =   7680
      Width           =   2295
   End
   Begin VB.CommandButton cmdrecact 
      Caption         =   "Recursos Actuaci�n"
      Height          =   375
      Left            =   3360
      TabIndex        =   7
      Top             =   7680
      Width           =   2295
   End
   Begin VB.CommandButton cmdIniciarPrueba 
      Caption         =   "Iniciar Prueba"
      Height          =   375
      Left            =   720
      TabIndex        =   0
      Top             =   7680
      Width           =   2295
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   1
      Left            =   0
      TabIndex        =   5
      Top             =   480
      Width           =   11820
      Begin IdPerson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   2355
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Cola de Actuaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5160
      Index           =   0
      Left            =   0
      TabIndex        =   2
      Top             =   2400
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4665
         Index           =   0
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   11640
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20532
         _ExtentY        =   8229
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRecibirPaciente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdanularconsen_Click()
 Dim strupdate As String
  If grdDBGrid1(0).Columns(12).Value = True Then
    If grdDBGrid1(0).Columns(13).Value = True Then
      strupdate = "UPDATE PR0300 SET PR03INDCONSFIRM=0 WHERE PR03NUMACTPEDI IN" & _
                  " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN=" & grdDBGrid1(0).Columns(10).Value & ")"
      objApp.rdoConnect.Execute strupdate, 64
      objWinInfo.DataRefresh
    Else
      Call MsgBox("El Paciente no ha firmado el Consentimiento")
    End If
  Else
    Call MsgBox("La Prueba no necesita Consentimiento Firmado")
  End If
End Sub

Private Sub cmdConsentimiento_Click()
  Dim strupdate As String
  If grdDBGrid1(0).Columns(12).Value = True Then
    If grdDBGrid1(0).Columns(13).Value = False Then
      strupdate = "UPDATE PR0300 SET PR03INDCONSFIRM=-1 WHERE PR03NUMACTPEDI IN" & _
                  " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN=" & grdDBGrid1(0).Columns(10).Value & ")"
      objApp.rdoConnect.Execute strupdate, 64
      objWinInfo.DataRefresh
    Else
      Call MsgBox("El Paciente ya ha firmado el Consentimiento")
    End If
  Else
    Call MsgBox("La Prueba no necesita Consentimiento Firmado")
  End If
End Sub

Private Sub cmdIniciarPrueba_Click()
'El paciente inicia la prueba
    Dim strupdate As String
    Dim mensaje As String
    Dim rsta As rdoResultset
    Dim stra As String
    Dim sacarmensaje As String
    
    
    
cmdIniciarPrueba.Enabled = False


If grdDBGrid1(0).Columns(12).Value = True And grdDBGrid1(0).Columns(13).Value = False Then
   mensaje = "El paciente no ha firmado el consentimiento necesario." & Chr(13)
End If
If grdDBGrid1(0).Columns(17).Value = "" Then
  grdDBGrid1(0).Columns(17).Value = 0
End If
If grdDBGrid1(0).Columns(16).Value = 1 And grdDBGrid1(0).Columns(17).Value = 1 Then
   mensaje = mensaje & "El paciente tiene muestras pendientes." & Chr(13)
End If
If mensaje <> "" Then
    sacarmensaje = MsgBox(mensaje & "� Desea Iniciar la actuaci�n ?", _
                          vbYesNo + vbExclamation, "Aviso")
   If sacarmensaje = vbNo Then
      cmdIniciarPrueba.Enabled = True
      Exit Sub
   Else
    'Se le pasa el n�mero de actuaci�n planificada
    glngnumactplan = grdDBGrid1(0).Columns(10).Value
    'Se introduce la fecha de inicio de la actuaci�n
    stra = "SELECT PR04FECINIACT FROM PR0400 WHERE pr04numactplan=" & _
         grdDBGrid1(0).Columns(10).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If IsNull(rsta.rdoColumns(0).Value) = True Then
        strupdate = "UPDATE PR0400 SET PR04FECINIACT=(SELECT SYSDATE FROM DUAL)" & _
           ",PR37CODESTADO=3 WHERE pr04numactplan=" & grdDBGrid1(0).Columns(10).Value
        objApp.rdoConnect.Execute strupdate, 64
        objWinInfo.DataRefresh
        If grdDBGrid1(0).Rows = 0 Then
            cmdIniciarPrueba.Enabled = False
        Else
            cmdIniciarPrueba.Enabled = True
            IdPersona1.Text = grdDBGrid1(0).Columns(11).Value
            IdPersona1.ReadPersona
        End If
    End If
    rsta.Close
    Set rsta = Nothing
    Call objsecurity.LaunchProcess("PR0138")
    cmdIniciarPrueba.Enabled = True
  End If
Else 'mensaje=""
'Se le pasa el n�mero de actuaci�n planificada
    glngnumactplan = grdDBGrid1(0).Columns(10).Value
    'Se introduce la fecha de inicio de la actuaci�n
    stra = "SELECT PR04FECINIACT FROM PR0400 WHERE pr04numactplan=" & grdDBGrid1(0).Columns(10).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If IsNull(rsta.rdoColumns(0).Value) = True Then
        strupdate = "UPDATE PR0400 SET PR04FECINIACT=(SELECT SYSDATE FROM DUAL)" & _
           ",PR37CODESTADO=3 WHERE pr04numactplan=" & grdDBGrid1(0).Columns(10).Value
        objApp.rdoConnect.Execute strupdate, 64
        objWinInfo.DataRefresh
        If grdDBGrid1(0).Rows = 0 Then
            cmdIniciarPrueba.Enabled = False
        Else
            cmdIniciarPrueba.Enabled = True
            IdPersona1.Text = grdDBGrid1(0).Columns(11).Value
            IdPersona1.ReadPersona
        End If
     End If
     rsta.Close
     Set rsta = Nothing
    'Load frmpruebasadicionales
    Call objsecurity.LaunchProcess("PR0138")
    
    cmdIniciarPrueba.Enabled = True
End If
End Sub

Private Sub cmdrecact_Click()
   
   cmdrecact.Enabled = False
   gstrLlamadorSel = ""
   gstrLlamadorSel = grdDBGrid1(0).Columns(10).Value
   frmRecPacRecur.IdPersona1 = IdPersona1
   frmRecPacRecur.txtactText1(0).Text = grdDBGrid1(0).Columns(10).Value
   frmRecPacRecur.txtactText1(1).Text = grdDBGrid1(0).Columns(8).Value
   Call objsecurity.LaunchProcess("PR0197")
   cmdrecact.Enabled = True
   gstrLlamadorSel = ""
End Sub

Private Sub Form_Activate()
  objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  If primerallamada = True Then
    Call Seleccionar_Dpto
    primerallamada = False
  End If
  
  'Call objApp.SplashOn
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0418J"
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Se filtra por el departamento realizador
    .strWhere = "AD02CODDPTO=" & glngdptologin
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    'Se ordena el grid por la fecha de la citaci�n
    Call .FormAddOrderField("FECHA", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha Cita", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora Cita", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "HORACOLA", "Hora entrada cola", cwString)
    Call .FormAddFilterWhere(strKey, "PR01INDCONSFDO", "Consentimiento?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDCONSFIRM", "Consentimiento Firmado?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FECHAINICIO", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "HORAINICIO", "Hora Inicio", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora Cita")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "HORACOLA", "Hora entrada cola")
    
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Fecha Cita", "FECHA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo, "Hora", "HORA", cwString, 8)
    Call .GridAddColumn(objMultiInfo, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Hora entrada cola", "HORACOLA", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Cod act pedi", "PR04numactplan", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo, "Consentimiento?", "PR01INDCONSFDO", cwBoolean, 10)
    Call .GridAddColumn(objMultiInfo, "Cons.Firmado?", "PR03INDCONSFIRM", cwBoolean, 10)
    Call .GridAddColumn(objMultiInfo, "Fecha Inicio", "FECHAINICIO", cwDate, 8)
    Call .GridAddColumn(objMultiInfo, "Hora Inicio", "HORAINICIO", cwString, 8)
    Call .GridAddColumn(objMultiInfo, "C�d.Estado", "PR37CODESTADO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo, "C�d.Estado Muestra", "PR56CODESTMUES", cwNumeric, 10)
    
    
    Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnMandatory = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnMandatory = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnMandatory = True
    Call .FormChangeColor(objMultiInfo)
  
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
    
    'Hacemos invisibles la columnas del grid
    grdDBGrid1(0).Columns(10).Visible = False
    grdDBGrid1(0).Columns(11).Visible = False
    grdDBGrid1(0).Columns(7).Visible = False
    grdDBGrid1(0).Columns(16).Visible = False
    grdDBGrid1(0).Columns(17).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
    'Call objApp.AddCtrl(TypeName(IdPersona1))
    'Call IdPersona1.Init(objApp, objGen)
    
    'Establecemos el ancho de las columnas del grid
    
    
    grdDBGrid1(0).Columns(3).Width = 1000
    grdDBGrid1(0).Columns(4).Width = 600
    grdDBGrid1(0).Columns(5).Width = 1300
    grdDBGrid1(0).Columns(6).Width = 1300
    grdDBGrid1(0).Columns(7).Width = 1400
    grdDBGrid1(0).Columns(8).Width = 1500
    grdDBGrid1(0).Columns(9).Width = 1100
    grdDBGrid1(0).Columns(12).Width = 1300
    grdDBGrid1(0).Columns(13).Width = 1200
    grdDBGrid1(0).Columns(14).Width = 1000
    grdDBGrid1(0).Columns(15).Width = 1300
    
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdIniciarPrueba.Enabled = False
        cmdrecact.Enabled = False
    Else
        cmdIniciarPrueba.Enabled = True
        cmdrecact.Enabled = True
        IdPersona1.Text = grdDBGrid1(0).Columns(11).Value
        IdPersona1.ReadPersona
    End If
  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                            intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
    If (btnButton.Index = 30) Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Exit Sub
    End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdIniciarPrueba.Enabled = False
        cmdrecact.Enabled = False
    Else
        cmdIniciarPrueba.Enabled = True
        cmdrecact.Enabled = True
        IdPersona1.Text = grdDBGrid1(0).Columns(11).Value
        IdPersona1.ReadPersona
    End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    IdPersona1.Text = grdDBGrid1(0).Columns(11).Value
    IdPersona1.ReadPersona

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


