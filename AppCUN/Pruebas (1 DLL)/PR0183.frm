VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmEstadoPeticion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Controlar Peticiones. Consultar Estado Peticiones"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones de la Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5160
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   2040
      Width           =   11415
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4665
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   11025
         ScrollBars      =   3
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   14
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483633
         BackColorOdd    =   -2147483633
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   14
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo Actuaci�n"
         Columns(0).Name =   "C�digo Actuaci�n"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4498
         Columns(1).Caption=   "Descripci�n Actuaci�n"
         Columns(1).Name =   "Descripci�n Actuaci�n"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1535
         Columns(2).Caption=   "Proceso"
         Columns(2).Name =   "Proceso"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1667
         Columns(3).Caption=   "Asistencia"
         Columns(3).Name =   "Asistencia"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2355
         Columns(4).Caption=   "Estado"
         Columns(4).Name =   "Estado"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2117
         Columns(5).Caption=   "Fecha Cita"
         Columns(5).Name =   "Fecha Cita"
         Columns(5).Alignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1588
         Columns(6).Caption=   "Hora Cita"
         Columns(6).Name =   "Hora Cita"
         Columns(6).Alignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   2117
         Columns(7).Caption=   "Entrada Cola"
         Columns(7).Name =   "Entrada Cola"
         Columns(7).Alignment=   2
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1588
         Columns(8).Caption=   "Hora Cola"
         Columns(8).Name =   "Hora Cola"
         Columns(8).Alignment=   2
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   2117
         Columns(9).Caption=   "Fecha Inicio"
         Columns(9).Name =   "Fecha Inicio"
         Columns(9).Alignment=   2
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   1588
         Columns(10).Caption=   "Hora Inicio"
         Columns(10).Name=   "Hora Inicio"
         Columns(10).Alignment=   2
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   2117
         Columns(11).Caption=   "Fecha Fin"
         Columns(11).Name=   "Fecha Fin"
         Columns(11).Alignment=   2
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   1588
         Columns(12).Caption=   "Hora Fin"
         Columns(12).Name=   "Hora Fin"
         Columns(12).Alignment=   2
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   2117
         Columns(13).Caption=   "Cancelada"
         Columns(13).Name=   "Cancelada"
         Columns(13).Alignment=   2
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         _ExtentX        =   19447
         _ExtentY        =   8229
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdMensajeCorreo 
      Caption         =   "Mensaje Correo"
      Height          =   375
      Left            =   4800
      TabIndex        =   5
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Index           =   1
      Left            =   240
      TabIndex        =   4
      Top             =   480
      Width           =   11460
      Begin VB.TextBox txtpetText1 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Tag             =   "N�mero de Grupo"
         Top             =   720
         Width           =   5000
      End
      Begin VB.TextBox txtpetText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "N�mero de Grupo"
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox txtpetText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "N�mero de Petici�n"
         Top             =   720
         Width           =   1092
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombopet1 
         Height          =   330
         Index           =   0
         Left            =   6120
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Fecha de Petici�n|Fecha de la Petici�n"
         Top             =   240
         Visible         =   0   'False
         Width           =   1800
         _Version        =   65537
         _ExtentX        =   3175
         _ExtentY        =   582
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "DD/MM/YY"
         AllowNullDate   =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Doctor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   3840
         TabIndex        =   13
         Top             =   480
         Width           =   585
      End
      Begin VB.Label lblLabel1 
         Caption         =   "N� Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   2160
         TabIndex        =   11
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   4560
         TabIndex        =   10
         Top             =   240
         Visible         =   0   'False
         Width           =   1290
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N� Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   9
         Top             =   480
         Width           =   975
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstadoPeticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    
    Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
    Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuRegistroOpcion(72).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub


Private Sub cmdMensajeCorreo_Click()
   cmdMensajeCorreo.Enabled = False
   'Load frmMensajeCorreo
   frmMensajeCorreo.txtText1(2).Text = frmControlarPet.txtText1(25).Text
   frmMensajeCorreo.txtText1(3).Text = frmControlarPet.txtText1(16).Text
   frmMensajeCorreo.txtText1(4).Text = frmControlarPet.txtText1(17).Text
   frmMensajeCorreo.txtText1(5).Text = frmControlarPet.txtText1(13).Text
   frmMensajeCorreo.txtText1(6).Text = frmControlarPet.txtText1(14).Text
   frmMensajeCorreo.txtText1(7).Text = frmControlarPet.txtText1(15).Text
   'frmMensajeCorreo.txtText1(8).Text = grdDBGrid1(0).Columns(4).Value
   frmMensajeCorreo.txtText1(8).Text = grdDBGrid1(0).Columns(1).Value
   frmMensajeCorreo.dtcDateCombo1(0).Text = dtcDateCombopet1(0).Text
   'frmMensajeCorreo.Show (vbModal)
   'Unload frmMensajeCorreo
   'Set frmMensajeCorreo = Nothing
   Call objsecurity.LaunchProcess("PR0184")
   cmdMensajeCorreo.Enabled = True

End Sub

Private Sub Form_Activate()
  Call Cargar_cabecera
  Call Cargar_grid
  Call Form_Paint
End Sub
Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

   Inicializar_Toolbar
End Sub

Private Sub Form_Paint()
  grdDBGrid1(0).Refresh
End Sub

Private Sub mnuDatosOpcion_Click(Index As Integer)

    If Index = 100 Then
      Unload Me
    End If

End Sub

Private Sub mnuOpcionesOpcion_Click(Index As Integer)

    If Index = 10 Then
        grdDBGrid1(0).Refresh
    End If

End Sub

Private Sub mnuRegistroOpcion_Click(Index As Integer)

  Select Case Index
    Case 40:
      grdDBGrid1(0).MoveFirst
    Case 50:
      grdDBGrid1(0).MovePrevious
    Case 60:
      grdDBGrid1(0).MoveNext
    Case 70:
      grdDBGrid1(0).MoveLast
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
  Select Case Button.Index
    Case 21:
      grdDBGrid1(0).MoveFirst
    Case 22:
      grdDBGrid1(0).MovePrevious
    Case 23:
      grdDBGrid1(0).MoveNext
    Case 24:
      grdDBGrid1(0).MoveLast
    Case 26:
      grdDBGrid1(0).Refresh
    Case 30:
      Unload Me
  End Select
End Sub
Private Sub Cargar_cabecera()
    txtpetText1(0).Text = frmControlarPet.grdDBGrid1(0).Columns(3).Value
    txtpetText1(1).Text = frmControlarPet.grdDBGrid1(0).Columns(4).Value
    txtpetText1(2).Text = frmControlarPet.grdDBGrid1(0).Columns(5).Value
    dtcDateCombopet1(0).Date = frmControlarPet.grdDBGrid1(0).Columns(6).Value
End Sub

Private Sub Cargar_grid()
  Dim strSelect As Variant
  Dim rsta As rdoResultset
  
 ' grdDBGrid1(0).Redraw = False
  grdDBGrid1(0).RemoveAll
  strSelect = "SELECT CI0100.PR04NUMACTPLAN,PR0100.PR01DESCORTA,"
  strSelect = strSelect & "PR0400.AD07CODPROCESO,PR0400.AD01CODASISTENCI,"
  strSelect = strSelect & "PR3700.PR37DESESTADO,CI0100.CI01FECCONCERT FECHA,"
  strSelect = strSelect & "TO_CHAR(CI0100.CI01FECCONCERT,'HH24:MI') HORA,"
  strSelect = strSelect & "PR0400.PR04FECENTRCOLA,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECENTRCOLA,'HH24:MI') HORACOLA,"
  strSelect = strSelect & "PR0400.PR04FECINIACT,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECINIACT,'HH24:MI') HORAINI,"
  strSelect = strSelect & "PR0400.PR04FECFINACT,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECFINACT,'HH24:MI') HORAFIN,"
  strSelect = strSelect & "PR0400.PR04FECCANCEL,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECCANCEL,'HH24:MI') HORACANCEL"
  strSelect = strSelect & " From PR0400, CI0100, PR0300, PR0100, PR3700 "
  strSelect = strSelect & " Where PR0300.PR09NUMPETICION = " & frmControlarPet.grdDBGrid1(0).Columns(3).Value
  strSelect = strSelect & " AND PR0400.CI21CODPERSONA= " & frmControlarPet.txtText1(25).Text
  strSelect = strSelect & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
  strSelect = strSelect & " AND PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI"
  strSelect = strSelect & " AND PR0100.PR01CODACTUACION=PR0400.PR01CODACTUACION"
  strSelect = strSelect & " AND PR0400.PR37CODESTADO=PR3700.PR37CODESTADO"
  strSelect = strSelect & " AND (CI01SITCITA='1' OR CI01SITCITA='3')"
  strSelect = strSelect & " Union "
  strSelect = strSelect & " SELECT PR0400.PR04NUMACTPLAN,PR0100.PR01DESCORTA,"
  strSelect = strSelect & "PR0400.AD07CODPROCESO,PR0400.AD01CODASISTENCI,"
  strSelect = strSelect & "PR3700.PR37DESESTADO,PR0400.PR04FECPLANIFIC FECHA,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECPLANIFIC,'HH24:MI') HORA,"
  strSelect = strSelect & "PR0400.PR04FECENTRCOLA,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECENTRCOLA,'HH24:MI') HORACOLA,"
  strSelect = strSelect & "PR0400.PR04FECINIACT,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECINIACT,'HH24:MI') HORAINI,"
  strSelect = strSelect & "PR0400.PR04FECFINACT,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECFINACT,'HH24:MI') HORAFIN,"
  strSelect = strSelect & "PR0400.PR04FECCANCEL,"
  strSelect = strSelect & "TO_CHAR(PR0400.PR04FECCANCEL,'HH24:MI') HORACANCEL"
  strSelect = strSelect & " From PR0400, PR0100, PR0300, PR3700"
  strSelect = strSelect & " Where PR0300.PR09NUMPETICION = " & frmControlarPet.grdDBGrid1(0).Columns(3).Value
  strSelect = strSelect & " AND PR0400.CI21CODPERSONA= " & frmControlarPet.txtText1(25).Text
  strSelect = strSelect & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
  strSelect = strSelect & " AND PR0100.PR01CODACTUACION=PR0400.PR01CODACTUACION"
  strSelect = strSelect & " AND PR0400.PR37CODESTADO=PR3700.PR37CODESTADO"
  strSelect = strSelect & " AND PR0400.PR37CODESTADO <> 2"
   
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
  While rsta.EOF = False
      grdDBGrid1(0).AddNew
      
      grdDBGrid1(0).Columns(0).Value = rsta("PR04NUMACTPLAN").Value
      grdDBGrid1(0).Columns(1).Value = rsta("PR01DESCORTA").Value
      grdDBGrid1(0).Columns(2).Value = rsta("AD07CODPROCESO").Value
      grdDBGrid1(0).Columns(3).Value = rsta("AD01CODASISTENCI").Value
      grdDBGrid1(0).Columns(4).Value = rsta("PR37DESESTADO").Value
      If IsNull(rsta("FECHA").Value) Then
          grdDBGrid1(0).Columns(5).Value = ""
      Else
          grdDBGrid1(0).Columns(5).Value = rsta("FECHA").Value
      End If
      If IsNull(rsta("HORA").Value) Then
          grdDBGrid1(0).Columns(6).Value = ""
      Else
          grdDBGrid1(0).Columns(6).Value = rsta("HORA").Value
      End If
      If IsNull(rsta("PR04FECENTRCOLA").Value) Then
          grdDBGrid1(0).Columns(7).Value = ""
      Else
          grdDBGrid1(0).Columns(7).Value = rsta("PR04FECENTRCOLA").Value
      End If
      If IsNull(rsta("HORACOLA").Value) Then
          grdDBGrid1(0).Columns(8).Value = ""
      Else
          grdDBGrid1(0).Columns(8).Value = rsta("HORACOLA").Value
      End If
      If IsNull(rsta("PR04FECINIACT").Value) Then
          grdDBGrid1(0).Columns(9).Value = ""
      Else
          grdDBGrid1(0).Columns(9).Value = rsta("PR04FECINIACT").Value
      End If
      If IsNull(rsta("HORAINI").Value) Then
          grdDBGrid1(0).Columns(10).Value = ""
      Else
          grdDBGrid1(0).Columns(10).Value = rsta("HORAINI").Value
      End If
      If IsNull(rsta("PR04FECFINACT").Value) Then
          grdDBGrid1(0).Columns(11).Value = ""
      Else
          grdDBGrid1(0).Columns(11).Value = rsta("PR04FECFINACT").Value
      End If
      If IsNull(rsta("HORAFIN").Value) Then
          grdDBGrid1(0).Columns(12).Value = ""
      Else
          grdDBGrid1(0).Columns(12).Value = rsta("HORAFIN").Value
      End If
      If IsNull(rsta("PR04FECCANCEL").Value) Then
          grdDBGrid1(0).Columns(13).Value = ""
      Else
          grdDBGrid1(0).Columns(13).Value = rsta("PR04FECCANCEL").Value
      End If
      'If IsNull(rsta("HORACANCEL").Value) Then
      '    grdDBGrid1(0).Columns(14).Value = ""
      'Else
      '    grdDBGrid1(0).Columns(14).Value = rsta("HORACANCEL").Value
      'End If
      
      grdDBGrid1(0).Update
      rsta.MoveNext
  Wend
  grdDBGrid1(0).MoveFirst
  rsta.Close
  Set rsta = Nothing
  'grdDBGrid1(0).Redraw = True
  grdDBGrid1(0).Refresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
'Private Sub Form_Load()
'  Dim objMultiInfo As New clsCWForm
'  Dim strKey As String
'
'  'Call objApp.SplashOn
'
'  Set objWinInfo = New clsCWWin
'
'  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
'                                Me, tlbToolbar1, stbStatusBar1, _
'                                cwWithAll)
'
'  With objMultiInfo
'    Set .objFormContainer = fraFrame1(0)
'    Set .objFatherContainer = Nothing
'    Set .tabMainTab = Nothing
'    Set .grdGrid = grdDBGrid1(0)
'    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
'
'    '.strDataBase = objEnv.GetValue("Main")
'    .strTable = "PR0426J"
'    .intAllowance = cwAllowReadOnly
'
'    '*************************************++
'    .strWhere = "CI21CODPERSONA=" & frmControlarPet.txtText1(25).Text & _
'                " AND PR09NUMPETICION=" & frmControlarPet.grdDBGrid1(0).Columns(3).Value
'
'    '*************************************+
'    .intCursorSize = 0
'
'    Call .FormAddOrderField("FECHA", cwAscending) 'Fecha planificada(o citada)
'    Call .FormAddOrderField("HORA", cwAscending)  'Hora citada
'
'    strKey = .strDataBase & .strTable
'    Call .FormCreateFilterWhere(strKey, "Consultar Estado de Actuaciones")
'    Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "C�digo Actuaci�n", cwNumeric)
'    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
'    Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
'    Call .FormAddFilterWhere(strKey, "AD07CODPROCESO", "Proceso", cwNumeric)
'    Call .FormAddFilterWhere(strKey, "AD01CODASISTENCI", "Asistencia", cwNumeric)
'    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha de Citaci�n", cwDate)
'    Call .FormAddFilterWhere(strKey, "HORA", "Hora de Planificaci�n", cwString)
'    'Call .FormAddFilterWhere(strKey, "PR04FECENTRCOLA", "Fecha de Entrada en Cola", cwDate)
'    'Call .FormAddFilterWhere(strKey, "HORACOLA", "Hora de Entrada en Cola", cwString)
'    'Call .FormAddFilterWhere(strKey, "PR04FECINIACT", "Fecha de Inicio", cwDate)
'    'Call .FormAddFilterWhere(strKey, "HORAINI", "Hora de Inicio", cwString)
'    Call .FormAddFilterWhere(strKey, "PR04FECFINACT", "Fecha de Fin", cwDate)
'    Call .FormAddFilterWhere(strKey, "HORAFIN", "Hora de Fin", cwString)
'    Call .FormAddFilterWhere(strKey, "PR04FECCANCEL", "Fecha de Cancelaci�n", cwDate)
'
'    Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "C�digo Actuaci�n")
'    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
'    Call .FormAddFilterOrder(strKey, "AD07CODPROCESO", "Proceso")
'    Call .FormAddFilterOrder(strKey, "AD01CODASISTENCI", "Asistencia")
'    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha de Citaci�n")
'    Call .FormAddFilterOrder(strKey, "HORA", "Hora de Planificaci�n")
'    'Call .FormAddFilterOrder("PR04FECENTRCOLA", "Fecha de Entrada en Cola")
'    'Call .FormAddFilterOrder("HORACOLA", "Hora de Entrada en Cola")
'    'Call .FormAddFilterOrder(strKey, "PR04FECINIACT", "Fecha de Inicio")
'    'Call .FormAddFilterOrder(strKey, "HORAINI", "Hora de Inicio")
'    Call .FormAddFilterOrder(strKey, "PR04FECFINACT", "Fecha de Fin")
'    Call .FormAddFilterOrder(strKey, "HORAFIN", "Hora de Fin")
'    Call .FormAddFilterOrder(strKey, "PR04FECCANCEL", "Fecha de Cancelaci�n")
'
'  End With
'
'  With objWinInfo
'
'
'    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
'
'    Call .GridAddColumn(objMultiInfo, "C�DIGO", "PR04NUMACTPLAN", cwNumeric, 9)
'    Call .GridAddColumn(objMultiInfo, "ACTUACI�N", "PR01DESCORTA", cwString, 30)
'    Call .GridAddColumn(objMultiInfo, "PROCESO", "AD07CODPROCESO", cwNumeric, 10)
'    Call .GridAddColumn(objMultiInfo, "ASISTENCIA", "AD01CODASISTENCI", cwNumeric, 10)
'    Call .GridAddColumn(objMultiInfo, "ESTADO", "PR37DESESTADO", cwString, 30)
'    Call .GridAddColumn(objMultiInfo, "FECHA CITA", "FECHA", cwDate, 75)
'    Call .GridAddColumn(objMultiInfo, "HORA CITA", "HORA", cwString, 75)
'    'Call .GridAddColumn(objMultiInfo, "COLA", "HORACOLA", cwString, 75)
'    'Call .GridAddColumn(objMultiInfo, "HORA INICIO", "HORAINI", cwString, 75)
'    Call .GridAddColumn(objMultiInfo, "FECHA FIN", "PR04FECFINACT", cwDate, 75)
'    Call .GridAddColumn(objMultiInfo, "HORA FIN", "HORAFIN", cwString, 75)
'    Call .GridAddColumn(objMultiInfo, "CANCELADA", "PR04FECCANCEL", cwDate, 75)
'
'    Call .FormCreateInfo(objMultiInfo)
'
'    Call .FormChangeColor(objMultiInfo)
'
'    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
'
'    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
'    '.CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True 'SE QUEDA COLGADO
'    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
'    '.CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
'    '.CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True  'SE QUEDA COLGADO
'    '.CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True 'SE QUEDA COLGADO
'    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
'    '.CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True  'SE QUEDA COLGADO
'
'
'    Call .WinRegister
'    Call .WinStabilize
'  End With
'  grdDBGrid1(0).Columns(3).Width = 0
'  grdDBGrid1(0).Columns(4).Width = 2000
'  grdDBGrid1(0).Columns(5).Width = 1100
'  grdDBGrid1(0).Columns(6).Width = 1100
'  grdDBGrid1(0).Columns(7).Width = 1200
'  grdDBGrid1(0).Columns(8).Width = 1200
'  grdDBGrid1(0).Columns(9).Width = 1050
'  'grdDBGrid1(0).Columns(8).Width = 1300
'  'grdDBGrid1(0).Columns(10).Width = 1150
'  grdDBGrid1(0).Columns(10).Width = 1000
'  grdDBGrid1(0).Columns(11).Width = 900
'  grdDBGrid1(0).Columns(12).Width = 1200
'
'  'grdDBGrid1(0).Columns(4).BackColor = &HC0FFFF
'  'Call objApp.SplashOff
'End Sub
