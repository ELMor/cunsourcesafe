VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmdeffases 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Fases de una Actuaci�n"
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0123.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdtiposrecurso 
      Caption         =   "Tipos de Recurso"
      Height          =   375
      Left            =   10200
      TabIndex        =   16
      Top             =   5160
      Width           =   1695
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   2
      Left            =   120
      TabIndex        =   28
      Top             =   600
      Width           =   9975
      Begin VB.TextBox txtfaseText1 
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01DESCORTA"
         Height          =   330
         Index           =   2
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.TextBox txtfaseText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01CODACTUACION"
         Height          =   330
         Index           =   0
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2880
         TabIndex        =   32
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   31
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fases"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   1920
      Width           =   10020
      Begin TabDlg.SSTab tabTab1 
         Height          =   5415
         Index           =   0
         Left            =   240
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   480
         Width           =   9615
         _ExtentX        =   16960
         _ExtentY        =   9551
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0123.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(7)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(4)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(11)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(12)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(13)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(14)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(15)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(16)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(17)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(6)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(9)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(18)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "cboSSDBCombo1(0)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(3)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(4)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(5)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(7)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(6)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(1)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "chkCheck1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "chkCheck1(1)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(0)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtminuto1"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txthora1"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtdia1"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtdia2"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txthora2"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtminuto2"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtdia3"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txthora3"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtminuto3"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).ControlCount=   38
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0123.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtminuto3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   7320
            MaxLength       =   2
            TabIndex        =   13
            ToolTipText     =   "Tiempo m�ximo en minutos"
            Top             =   3480
            Width           =   615
         End
         Begin VB.TextBox txthora3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   6000
            MaxLength       =   2
            TabIndex        =   12
            ToolTipText     =   "Tiempo m�ximo en horas"
            Top             =   3480
            Width           =   615
         End
         Begin VB.TextBox txtdia3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   4800
            MaxLength       =   2
            TabIndex        =   11
            ToolTipText     =   "Tiempo m�ximo en d�as"
            Top             =   3480
            Width           =   615
         End
         Begin VB.TextBox txtminuto2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   3000
            MaxLength       =   2
            TabIndex        =   10
            ToolTipText     =   "Tiempo m�nimo en minutos"
            Top             =   3480
            Width           =   615
         End
         Begin VB.TextBox txthora2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   1680
            MaxLength       =   2
            TabIndex        =   9
            ToolTipText     =   "Tiempo m�nimo en horas"
            Top             =   3480
            Width           =   615
         End
         Begin VB.TextBox txtdia2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   480
            MaxLength       =   2
            TabIndex        =   8
            ToolTipText     =   "Tiempo m�nimo en d�as"
            Top             =   3480
            Width           =   615
         End
         Begin VB.TextBox txtdia1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   480
            MaxLength       =   2
            TabIndex        =   5
            ToolTipText     =   "Tiempo de ocupaci�n del paciente en d�as"
            Top             =   2520
            Width           =   615
         End
         Begin VB.TextBox txthora1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   1800
            MaxLength       =   2
            TabIndex        =   6
            ToolTipText     =   "Tiempo de ocupaci�n del paciente en horas"
            Top             =   2520
            Width           =   615
         End
         Begin VB.TextBox txtminuto1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   3240
            MaxLength       =   2
            TabIndex        =   7
            ToolTipText     =   "Tiempo de ocupaci�n del paciente en minutos"
            Top             =   2520
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            Height          =   525
            Index           =   0
            Left            =   2040
            TabIndex        =   37
            Tag             =   "Descripci�n Fase Precedente"
            Top             =   1560
            Width           =   6975
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR05INDINICIOFIN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   600
            TabIndex        =   15
            Tag             =   "�Desfase con respecto al inicio o al final de la fase anterior?"
            Top             =   4680
            Width           =   255
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR05INDHABILNATU"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   600
            TabIndex        =   14
            Tag             =   "�El tiempo se mide en d�as h�biles o en d�as naturales?"
            Top             =   4200
            Width           =   255
         End
         Begin VB.TextBox txtText1 
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   1
            Left            =   6480
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   4920
            Visible         =   0   'False
            Width           =   2175
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR05NUMTMINFPRE"
            Height          =   330
            Index           =   6
            Left            =   6600
            TabIndex        =   18
            Tag             =   "Tiempo M�nimo desde Fase Previa"
            Top             =   2640
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR05NUMTMAXFPRE"
            Height          =   330
            Index           =   7
            Left            =   7440
            TabIndex        =   19
            Tag             =   "Tiempo M�ximo desde Fase Previa"
            Top             =   2640
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR05NUMOCUPACI"
            Height          =   330
            Index           =   5
            Left            =   5880
            TabIndex        =   17
            Tag             =   "Tiempo Ocupaci�n Paciente|Minutos en los que el paciente estar� ocupado"
            Top             =   2640
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR05DESFASE"
            Height          =   525
            Index           =   4
            Left            =   2040
            MultiLine       =   -1  'True
            TabIndex        =   3
            Tag             =   "Descripci�n|Descripci�n de la Fase"
            Top             =   600
            Width           =   7000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR05NUMFASE"
            Height          =   330
            Index           =   3
            Left            =   480
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "N�mero de Fase"
            Top             =   600
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR05NUMFASE_PRE"
            Height          =   330
            Index           =   0
            Left            =   480
            TabIndex        =   4
            Tag             =   "Fase Precedente|Fase Precedente a la Fase en curso"
            Top             =   1560
            Width           =   1095
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   820
            Columns(0).Caption=   "C�D"
            Columns(0).Name =   "N�MERO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9525
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1940
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5145
            Index           =   0
            Left            =   -74760
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   120
            Width           =   8895
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15690
            _ExtentY        =   9075
            _StockProps     =   79
            Caption         =   "FASES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   8040
            TabIndex        =   47
            Top             =   3600
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   6720
            TabIndex        =   46
            Top             =   3600
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   5520
            TabIndex        =   45
            Top             =   3600
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   3720
            TabIndex        =   44
            Top             =   3600
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   2400
            TabIndex        =   43
            Top             =   3600
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   1200
            TabIndex        =   42
            Top             =   3600
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   1200
            TabIndex        =   41
            Top             =   2640
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   2520
            TabIndex        =   40
            Top             =   2640
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   3960
            TabIndex        =   39
            Top             =   2640
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Fase Precedente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2040
            TabIndex        =   38
            Top             =   1320
            Width           =   3255
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Actuaci�n"
            Height          =   255
            Index           =   12
            Left            =   6480
            TabIndex        =   36
            Top             =   4680
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�El desfase es respecto al inicio de la fase anterior?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   960
            TabIndex        =   35
            Tag             =   "�Desfase con respecto al inicio o al final de la fase anterior?"
            Top             =   4680
            Width           =   4815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�El tiempo se mide en d�as h�biles?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   960
            TabIndex        =   34
            Tag             =   "�El tiempo se mide en d�as h�biles o en d�as naturales?"
            Top             =   4200
            Width           =   4095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n de la Fase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   2040
            TabIndex        =   22
            Top             =   360
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo M�ximo desde Fase Previa"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   4800
            TabIndex        =   25
            Top             =   3240
            Width           =   3135
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo M�nimo desde Fase Previa"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   480
            TabIndex        =   24
            Top             =   3240
            Width           =   3375
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fase Precedente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   480
            TabIndex        =   33
            Top             =   1320
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo Ocupaci�n Paciente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   480
            TabIndex        =   23
            Top             =   2280
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�mero Fase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   480
            TabIndex        =   20
            Top             =   360
            Width           =   1575
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   26
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmdeffases"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00123.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION:  Definici�n de las fases de una actuaci�n               *
'* ARGUMENTOS:  PR01CODACTUACION y PR01DESCORTA (por valor)             *                                 *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit


Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
'variable para controlar si hay fases o no
Dim ncontador As Integer
'variable que recoge la fase en curso
Dim intfasecurso As Variant
'intborrar controla si se quiere borrar una fase sin tipos de recurso
Dim intborrar As Integer
'mnabrir indica si se ha pulsado la opci�n Abrir
Dim mnabrir As Integer
'mncancelar controla los mensajes en la funci�n Borrar_Actuaciones
Dim mncancelar As Integer
' intdelete controla si se ha borrado una fase sin tipo de recurso
Dim intdelete As Integer
'intnumfase sirve de contador de fases a la hora de asignar el c�digo
Dim intnumfase As Integer
Dim TiempoTotal As Variant
Dim Tiempototal2 As Variant
Dim TiempoTotal1 As Variant
Dim tiempototal3 As Variant
Dim primera As Boolean
Dim intactivar As Integer
'intcambioalgo para detectar si se cambia algo en la pantalla
Dim intcambioalgo As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Activate()
Dim strsql As String
Dim rsta As rdoResultset

    ' activar el frame
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    ' bot�n de la barra de estado para llenar la tabla
    If primera = False Then
      Call objWinInfo.WinProcess(6, 21, 0)
      primera = True
    End If
    intfasecurso = txtText1(3).Text
    
    'el n� de fase debe ser consecutivo por tanto cada vez que se activa la
    'pantalla se genere autom�ticamente el n� de fase
    On Error GoTo Err_Ejecutar
    strsql = "select count(*) from PR0500 where pr01codactuacion=" & _
           frmdefactuacionesCUN.txtText1(0)
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If (rsta.rdoColumns(0).Value = 0) Then
       intnumfase = 0
    Else
       strsql = "select max(pr05numfase) from PR0500 where pr01codactuacion=" & _
           frmdefactuacionesCUN.txtText1(0)
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
       intnumfase = rsta.rdoColumns(0).Value
    End If
If txtText1(3).Text <> "" Then
  cmdtiposrecurso.Enabled = True
End If
rsta.Close
Set rsta = Nothing
'para que al entrar no pregunte si se desean guardar cambios cuando
'en realidad a�n no se ha cambiado nada
objWinInfo.objWinActiveForm.blnChanged = False
'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))

objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
    & frmdefactuacionesCUN.txtText1(0).Text & "and" & _
    " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")" & _
    " order by pr05numfase asc"
Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  
  Exit Sub
End Sub


Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Fases"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0500" 'Fases
    
    .strWhere = "PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
                " AND " & _
                " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")"
                
     .blnMasive = False
    
    Call .FormAddOrderField("PR05NUMFASE", cwAscending)
    
    Call .objPrinter.Add("PR1231", "Listado de Fases")
    
    .blnHasMaint = True
    
    strKey = .strDataBase & .strTable
     
    Call .FormCreateFilterWhere(strKey, "Fases de la Actuaci�n")
    Call .FormAddFilterWhere(strKey, "PR05NUMFASE", "N�mero Fase", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR05DESFASE", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR05NUMOCUPACI", "Tiempo de Ocupaci�n del Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR05NUMTMINFPRE", "Tiempo M�n desde Fase Previa", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR05NUMTMAXFPRE", "Tiempo M�x desde Fase Previa", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR05INDHABILNATU", "�El tiempo se mide en d�as h�biles?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR05INDINICIOFIN", "�El desfase es respecto al inicio de la fase?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "PR05NUMFASE", "N�mero Fase")
    Call .FormAddFilterOrder(strKey, "PR05DESFASE", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "PR05NUMOCUPACI", "Tiempo de Ocupaci�n del Paciente")
    Call .FormAddFilterOrder(strKey, "PR05NUMTMINFPRE", "Tiempo M�n desde Fase Previa")
    Call .FormAddFilterOrder(strKey, "PR05NUMTMAXFPRE", "Tiempo M�x desde Fase Previa")
   End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo)
    
    
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    
    .CtrlGetInfo(txtdia1).blnNegotiated = False
    .CtrlGetInfo(txthora1).blnNegotiated = False
    .CtrlGetInfo(txtminuto1).blnNegotiated = False
    
    .CtrlGetInfo(txtdia2).blnNegotiated = False
    .CtrlGetInfo(txthora2).blnNegotiated = False
    .CtrlGetInfo(txtminuto2).blnNegotiated = False
    
    .CtrlGetInfo(txtdia3).blnNegotiated = False
    .CtrlGetInfo(txthora3).blnNegotiated = False
    .CtrlGetInfo(txtminuto3).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(1)).blnInGrid = False
     
    '.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
    '& frmdefactuacionesCUN.txtText1(0).Text & " order by pr05numfase asc"
    
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
    & frmdefactuacionesCUN.txtText1(0).Text & "and" & _
    " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")" & _
    " order by pr05numfase asc"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR05NUMFASE", "SELECT * FROM PR0500 WHERE PR05NUMFASE=? and pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text)
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(0), "PR05DESFASE")
    
    Call .WinRegister
    Call .WinStabilize
  End With
  ' si al cargarse la pantalla no hay ninguna fase el bot�n Tipos de Recurso se deshabilita
  If txtText1(3).Text = "" Then
     cmdtiposrecurso.Enabled = False
  End If
  intfasecurso = txtText1(3).Text
  primera = True
  'Call objApp.SplashOff

End Sub



Private Sub grdDBGrid1_Click(Index As Integer)
grdDBGrid1(0).Enabled = True
End Sub

Private Sub lblactLabel1_Click(Index As Integer)
    txtText1(3).SetFocus
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
    txtText1(1).Text = txtfaseText1(0).Text
End Sub


Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)

End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)

End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim rstA1 As rdoResultset
  Dim sqlstr1 As String
    
    
  If txtText1(3).Text = "" Then
    intCancel = objWinInfo.WinExit
    frmdefactuacionesCUN.cmdcondiciones.Enabled = False
    frmdefactuacionesCUN.cmdinteracciones.Enabled = False
    frmdefactuacionesCUN.cmdmuestras.Enabled = False
    frmdefactuacionesCUN.cmdcuestionario.Enabled = False
    frmdefactuacionesCUN.cmdactprev.Enabled = False
    frmdefactuacionesCUN.cmdactasoc.Enabled = False
    frmdefactuacionesCUN.cmddptos.Enabled = False
  Else
   intborrar = Borrar_Fases
   
  'intborrar=1 dice que se quiere borrar la fase sin tipos de recurso
  If intborrar = 1 Then
    ' volver a la pantalla de actuaciones
    intCancel = objWinInfo.WinExit
    Else
    ' se mantiene en la misma pantalla de Fases
    intCancel = 1
  End If
  End If
        
 ' n� de fases de la actuaci�n
 sqlstr = "select count(*) from PR0500" & _
    " where PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
 Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
 ' ncontador contiene el n�mero de fases de la actuaci�n
 ncontador = rsta.rdoColumns(0).Value
 If (ncontador = 0) Then
         frmdefactuacionesCUN.cmdcondiciones.Enabled = False
         frmdefactuacionesCUN.cmdinteracciones.Enabled = False
         frmdefactuacionesCUN.cmdmuestras.Enabled = False
         frmdefactuacionesCUN.cmdcuestionario.Enabled = False
         frmdefactuacionesCUN.cmdactprev.Enabled = False
         frmdefactuacionesCUN.cmdactasoc.Enabled = False
         frmdefactuacionesCUN.cmddptos.Enabled = False
 Else
        frmdefactuacionesCUN.cmdcondiciones.Enabled = True
        frmdefactuacionesCUN.cmdmuestras.Enabled = True
        frmdefactuacionesCUN.cmdcuestionario.Enabled = True
        frmdefactuacionesCUN.cmdactprev.Enabled = True
        frmdefactuacionesCUN.cmdactasoc.Enabled = True
        frmdefactuacionesCUN.cmddptos.Enabled = True
        'mira si la actuaci�n tiene alg�n departamento realizador
        sqlstr1 = "SELECT count(*) " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0)
       Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
       If (rstA1.rdoColumns(0).Value = 0) Then
            frmdefactuacionesCUN.cmdinteracciones.Enabled = False
       Else
            frmdefactuacionesCUN.cmdinteracciones.Enabled = True
       End If
       rstA1.Close
       Set rstA1 = Nothing
End If
rsta.Close
Set rsta = Nothing
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
  'Si la fase borrada era la �ltima fase, hay que cambiar la clausula strWhere del frmdefactuacionesCUN
  Dim rstF As rdoResultset
  Dim strSelect As String
  
  strSelect = "SELECT COUNT(*) " & _
              "  FROM PR0500 " & _
              " WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value = 0 Then
    gstrWhereSinFases = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
            & "pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")"
  Else
    gstrWhereSinFases = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
  End If
  
  rstF.Close
  Set rstF = Nothing

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    'deshabilitar el bot�n Tipos de Recurso si se pulsa Abrir
    Dim sqlstring As String
    Dim rsta As rdoResultset
        
    If (txtText1(3).Text <> "") And (txtfaseText1(0).Text <> "") Then
        ' se cuentan los tipos de recurso
        sqlstring = "select count(*) from PR1300 " & _
             "where PR05NUMFASE=" & txtText1(3).Text & _
             " and PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        If rsta.rdoColumns(0).Value > 0 Then
            cmdtiposrecurso.Enabled = True
        End If
        rsta.Close
        Set rsta = Nothing
    End If
    If intdelete = 1 Then
        If (txtText1(3) <> "") Then
            sqlstring = "select count(*) from PR0500 " & _
                        "where PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
                        " and PR05NUMFASE=" & txtText1(3)
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
            If (rsta.rdoColumns(0).Value = 0) Then
                cmdtiposrecurso.Enabled = False
                intdelete = 0
            End If
            rsta.Close
            Set rsta = Nothing
        End If
    End If
  
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
     intcambioalgo = 0
     ' se habilita el bot�n <Tipos de Recurso> si los campos obligatorios est�n metidos
      If txtText1(4).Text <> "" Then
        cmdtiposrecurso.Enabled = True
      End If
      ' tras guardar una nueva fase el Combo de Fase precedente debe actualizarse
      'objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
      '& frmdefactuacionesCUN.txtText1(0).Text & " order by pr05numfase asc"
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
      & frmdefactuacionesCUN.txtText1(0).Text & "and" & _
      " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")" & _
                " order by pr05numfase asc"
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
      objWinInfo.objWinActiveForm.strWhere = "PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Fases" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If

End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

  Dim sqlstring As String
  Dim rsta As rdoResultset
  Dim sqlstr1 As String
  Dim rstA1 As rdoResultset
  Dim intactu As Integer
  Dim intCont As Integer
  Dim strmensaje As String
  Dim intResp As Integer
  '*************************************************************************
  Dim rstTiempo As rdoResultset
  Dim strTiempo As String
  Dim detectarcambios As Integer
  Dim strtiempototal As String
  Dim strtiempototal2 As String


  'JRC 28/4/98
  If btnButton.Index = 16 Or btnButton.Index = 18 Then
    Call MsgBox("Opci�n no disponible.", vbExclamation)
    Exit Sub
  End If
  
  Tiempototal2 = ""
  tiempototal3 = ""
  Call Calcular_Tiempo(txtdia1.Text, txthora1.Text, txtminuto1.Text)
  TiempoTotal1 = TiempoTotal
  Call Calcular_Tiempo(txtdia2.Text, txthora2.Text, txtminuto2.Text)
  Tiempototal2 = TiempoTotal
  Call Calcular_Tiempo(txtdia3.Text, txthora3.Text, txtminuto3.Text)
  tiempototal3 = TiempoTotal
  If txtfaseText1(0).Text <> "" And txtText1(3).Text <> "" And _
      btnButton.Index <> 2 Then
    If Tiempototal2 = "" Then
      strtiempototal = " is Null"
    Else
      strtiempototal = "= " & Tiempototal2
    End If
    If tiempototal3 = "" Then
      strtiempototal2 = " is Null"
    Else
      strtiempototal2 = "= " & tiempototal3
    End If
    strTiempo = "SELECT count(*) FROM PR0500 where pr01codactuacion=" & txtfaseText1(0).Text & _
           " AND pr05numfase=" & txtText1(3).Text & _
           " AND pr05numocupaci=" & TiempoTotal1 & _
           " AND pr05numtminfpre" & strtiempototal & _
           " And pr05numtmaxfpre" & strtiempototal2
    Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
    If (rstTiempo.rdoColumns(0).Value) = 0 Then
      detectarcambios = 0
      Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal1)
      Call objWinInfo.CtrlSet(txtText1(6), Tiempototal2)
      Call objWinInfo.CtrlSet(txtText1(7), tiempototal3)
      objWinInfo.objWinActiveForm.blnChanged = True
      detectarcambios = 1
    'Else
    '  objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rstTiempo.Close
    Set rstTiempo = Nothing
  End If
    If objWinInfo.objWinActiveForm.blnChanged = True Then
    If cboSSDBCombo1(0).Value <> "" Then
      If cboSSDBCombo1(0).Value <> 0 Then
        If txtText1(6).Text > txtText1(7).Text Then
          Call MsgBox("El Tiempo M�nimo debe ser menor que el Tiempo M�ximo", vbExclamation)
          If txtText1(7).Text <> 0 Then
            txtText1(6).Text = txtText1(7).Text
          Else
            txtText1(7).Text = txtText1(6).Text
          End If
        End If
      Else
        If txtText1(6).Text <> 0 Then
          Call MsgBox("No hay fase precedente", vbExclamation)
          txtText1(6).Text = 0
        End If
      End If
    Else
      If txtText1(6).Text <> "" Then
        If txtText1(6).Text <> 0 Then
        Call MsgBox("No hay fase precedente", vbExclamation)
        txtText1(6).Text = 0
        End If
      End If
    End If
    If cboSSDBCombo1(0) <> "" Then
      If cboSSDBCombo1(0).Value <> 0 Then
        If txtText1(6).Text > txtText1(7).Text Then
          Call MsgBox("El Tiempo M�ximo debe ser mayor que el Tiempo M�nimo", vbExclamation)
          txtText1(7).Text = txtText1(6).Text
        End If
      Else
        If txtText1(7).Text <> 0 Then
          Call MsgBox("No hay fase precedente", vbExclamation)
          txtText1(7).Text = 0
        End If
      End If
    Else
      If txtText1(7).Text <> "" Then
        If txtText1(7).Text <> 0 Then
        Call MsgBox("No hay fase precedente", vbExclamation)
        txtText1(7).Text = 0
        End If
      End If
    End If
  End If
  '*********************************************************************
'Salir
If (btnButton.Index = 30) Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
End If

'4:guardar, 6:imprimir, 8:borrar, 10:cortar, 11:copiar
'12:pegar, 14:deshacer, 26:refrescar, 28:mantenimiento
If (btnButton.Index <> 4 And btnButton.Index <> 6 And _
       btnButton.Index <> 8 And btnButton.Index <> 10 And _
       btnButton.Index <> 11 And btnButton.Index <> 12 And _
       btnButton.Index <> 14 And btnButton.Index <> 26 And _
       btnButton.Index <> 28) Then
    'si se pulsa Abrir Registro que no saque el mensaje de Guardar cambios
    If (btnButton.Index = 3) Then
        If intcambioalgo = 0 Then
          objWinInfo.objWinActiveForm.blnChanged = False
        End If
    End If
    If intcambioalgo = 0 Then
          objWinInfo.objWinActiveForm.blnChanged = False
    Else
          objWinInfo.objWinActiveForm.blnChanged = True
    End If
    intborrar = Borrar_Fases
    If intborrar = 1 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        If btnButton.Index = 16 Then
            Call txtText1_Change(5)
            Call txtText1_Change(6)
            Call txtText1_Change(7)
        End If
        If txtText1(3).Text = "" Then
            cmdtiposrecurso.Enabled = False
        End If
    End If
 Else
    If (btnButton.Index <> 8) Then 'distinto de Borrar
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
 End If

  'Borrar
  If (btnButton.Index = 8 And txtText1(3) <> "") Then
    sqlstr1 = "select count(PR05NUMFASE) from PR0500" & _
            " where PR05NUMFASE=" & txtText1(3).Text & _
            " And PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    intactu = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    If intactu = 1 Then
      strmensaje = Ver_Relaciones()
      If Len(strmensaje) = 0 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Else
        intResp = MsgBox(strmensaje, vbInformation, "Importante")
      End If
      
    End If
  End If
  
If (btnButton.Index = 2 And intborrar = 1) Then
      ' se deshabilita el bot�n <Tipos de Recurso>
      cmdtiposrecurso.Enabled = False
End If
  
'no hay fases
'jcr 27 / 3 / 98
If intfasecurso = "" Then
    If (intactu <> 1) Then
    ' para que no se repita la llamada
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    objWinInfo.objWinActiveForm.blnChanged = False
    End If
End If
 ' Guardar
 If btnButton.Index = 4 Then
     ' se habilita el bot�n <Tipos de Recurso>
      cmdtiposrecurso.Enabled = True
      ' tras guardar una nueva fase el Combo de Fase precedente debe actualizarse
      'objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
      '& frmdefactuacionesCUN.txtText1(0).Text & " order by pr05numfase asc"
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
       & frmdefactuacionesCUN.txtText1(0).Text & "and" & _
        " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")" & _
                " order by pr05numfase asc"
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
End If

If (btnButton.Index <> 3) Then
    'no se ha pulsado Abrir Registro
    mnabrir = 0
Else
    cmdtiposrecurso.Enabled = False
    If mncancelar = 0 Then
       mnabrir = 1
    Else
       mnabrir = 0
    End If
End If

  'Localizar
  If (btnButton.Index = 16 And txtText1(3).Text <> "") Then
        sqlstr1 = "select count(PR05NUMFASE) from PR0500" & _
                  " where PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
                  " and PR05NUMFASE=" & txtText1(3).Text
        Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
        intCont = rstA1.rdoColumns(0).Value
        If intCont = 1 Then
            cmdtiposrecurso.Enabled = True
        End If
        rstA1.Close
        Set rstA1 = Nothing
    End If
'Nuevo
If (btnButton.Index = 2) Then
  txtText1(3).Text = intnumfase + 1
  txtText1(3).Locked = True
  txtdia1.Text = 0
  txthora1.Text = 0
  txtminuto1.Text = 0
  txtdia2.Text = 0
  txthora2.Text = 0
  txtminuto2.Text = 0
  txtdia3.Text = 0
  txthora3.Text = 0
  txtminuto3.Text = 0
End If
objWinInfo.objWinActiveForm.blnChanged = False
'si no se han introducido campos obligatorios que no se active el bot�n Tipos de Recurso
If txtText1(4).Text = "" Then
  cmdtiposrecurso.Enabled = False
End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr1 As String
  Dim rstA1 As rdoResultset
  Dim intactu As Integer
  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  '*************************************************************************
  Dim rstTiempo As rdoResultset
  Dim strTiempo As String
  Dim detectarcambios As Integer
  Dim strtiempototal As String
  Dim strtiempototal2 As String
  
  Tiempototal2 = ""
  tiempototal3 = ""
  Call Calcular_Tiempo(txtdia1.Text, txthora1.Text, txtminuto1.Text)
  TiempoTotal1 = TiempoTotal
  Call Calcular_Tiempo(txtdia2.Text, txthora2.Text, txtminuto2.Text)
  Tiempototal2 = TiempoTotal
  Call Calcular_Tiempo(txtdia3.Text, txthora3.Text, txtminuto3.Text)
  tiempototal3 = TiempoTotal
  If txtfaseText1(0).Text <> "" And txtText1(3).Text <> "" Then
    If Tiempototal2 = "" Then
      strtiempototal = " is Null"
    Else
      strtiempototal = "= " & Tiempototal2
    End If
    If tiempototal3 = "" Then
      strtiempototal2 = " is Null"
    Else
      strtiempototal2 = "= " & tiempototal3
    End If
    strTiempo = "SELECT count(*) FROM PR0500 where pr01codactuacion=" & txtfaseText1(0).Text & _
           " AND pr05numfase=" & txtText1(3).Text & _
           " AND pr05numocupaci=" & TiempoTotal1 & _
           " AND pr05numtminfpre" & strtiempototal & _
           " And pr05numtmaxfpre" & strtiempototal2
    Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
    If (rstTiempo.rdoColumns(0).Value) = 0 Then
      detectarcambios = 0
      Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal1)
      Call objWinInfo.CtrlSet(txtText1(6), Tiempototal2)
      Call objWinInfo.CtrlSet(txtText1(7), tiempototal3)
      objWinInfo.objWinActiveForm.blnChanged = True
      detectarcambios = 1
    'Else
    '  objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rstTiempo.Close
    Set rstTiempo = Nothing
  End If
    If objWinInfo.objWinActiveForm.blnChanged = True Then
    If cboSSDBCombo1(0).Value <> "" Then
      If cboSSDBCombo1(0).Value <> 0 Then
        If txtText1(6).Text > txtText1(7).Text Then
          Call MsgBox("El Tiempo M�nimo debe ser menor que el Tiempo M�ximo", vbExclamation)
          If txtText1(7).Text <> 0 Then
            txtText1(6).Text = txtText1(7).Text
          Else
            txtText1(7).Text = txtText1(6).Text
          End If
        End If
      Else
        If txtText1(6).Text <> 0 Then
          Call MsgBox("No hay fase precedente", vbExclamation)
          txtText1(6).Text = 0
        End If
      End If
    Else
      If txtText1(6).Text <> "" Then
        If txtText1(6).Text <> 0 Then
        Call MsgBox("No hay fase precedente", vbExclamation)
        txtText1(6).Text = 0
        End If
      End If
    End If
    If cboSSDBCombo1(0) <> "" Then
      If cboSSDBCombo1(0).Value <> 0 Then
        If txtText1(6).Text > txtText1(7).Text Then
          Call MsgBox("El Tiempo M�ximo debe ser mayor que el Tiempo M�nimo", vbExclamation)
          txtText1(7).Text = txtText1(6).Text
        End If
      Else
        If txtText1(7).Text <> 0 Then
          Call MsgBox("No hay fase precedente", vbExclamation)
          txtText1(7).Text = 0
        End If
      End If
    Else
      If txtText1(7).Text <> "" Then
        If txtText1(7).Text <> 0 Then
        Call MsgBox("No hay fase precedente", vbExclamation)
        txtText1(7).Text = 0
        End If
      End If
    End If
  End If
  '*********************************************************************
  If intcambioalgo = 0 Then
    objWinInfo.objWinActiveForm.blnChanged = False
  Else
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  
  'Salir
  If (intIndex = 100) Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Exit Sub
  End If
  
  '40:guardar, 60:eliminar, 80:imprimir
  If (intIndex <> 40 And intIndex <> 60 And intIndex <> 80) Then
    intborrar = Borrar_Fases
    ' se quiere borrar la fase sin tipos de recurso
    If intborrar = 1 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        If txtText1(3).Text = "" Then
            cmdtiposrecurso.Enabled = False
        End If
    End If
  Else
    If (intIndex <> 60) Then 'distinto de Eliminar
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
  End If
  
  'Borrar
  If (intIndex = 60 And txtText1(3) <> "") Then
     sqlstr1 = "select count(PR05NUMFASE) from PR0500" & _
            " where PR05NUMFASE=" & txtText1(3).Text & _
            " And PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    intactu = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    If intactu = 1 Then
      strmensaje = Ver_Relaciones()
      If Len(strmensaje) > 0 Then
        intResp = MsgBox(strmensaje, vbInformation, "Importante")
      Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      End If
    End If
  End If
  
  'no hay fases
  If intfasecurso = "" Then
    If (intactu <> 1) Then
    ' para que no se repita la llamada
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
  End If
  
  If (intIndex = 10 And intborrar = 1) Then 'Nuevo
        ' se deshabilita el bot�n <Tipos de Recurso>
        cmdtiposrecurso.Enabled = False
  End If
  
 If intIndex = 40 Then   'Guardar
            ' se habilita el bot�n <Tipos de Recurso>
            cmdtiposrecurso.Enabled = True
            ' tras guardar una nueva fase el Combo de Fase precedente debe actualizarse
            'objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
            '& frmdefactuacionesCUN.txtText1(0).Text & " order by pr05numfase asc"
            objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT pr05numfase,pr05desfase FROM pr0500 Where pr01codactuacion = " _
              & frmdefactuacionesCUN.txtText1(0).Text & "and" & _
               " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")" & _
                 " order by pr05numfase asc"
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
 End If
 
 If (intIndex <> 20) Then
    'no se ha pulsado Abrir Registro
    mnabrir = 0
 Else
    cmdtiposrecurso.Enabled = False
    If mncancelar = 0 Then
       mnabrir = 1
    Else
       mnabrir = 0
    End If
End If
'Nuevo
If (intIndex = 10) Then
  txtText1(3).Text = intnumfase + 1
  txtText1(3).Locked = True
  txtdia1.Text = 0
  txthora1.Text = 0
  txtminuto1.Text = 0
  txtdia2.Text = 0
  txthora2.Text = 0
  txtminuto2.Text = 0
  txtdia3.Text = 0
  txthora3.Text = 0
  txtminuto3.Text = 0
End If
'si no se han introducido campos obligatorios que no se active el bot�n Tipos de Recurso
If txtText1(4).Text = "" Then
  cmdtiposrecurso.Enabled = False
End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)

End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    
'JRC 28/4/98
    Call MsgBox("Opci�n no disponible.", vbExclamation)
    Exit Sub
    
    intborrar = Borrar_Fases
    ' se quiere borrar la fase sin tipos de recurso
    If intborrar = 1 Then
        Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
        If txtText1(3).Text = "" Then
            cmdtiposrecurso.Enabled = False
        End If
    End If

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Dim strsql As String
Dim rstA1 As rdoResultset
Dim intCont As Integer
  '*************************************************************************
  Dim rstTiempo As rdoResultset
  Dim strTiempo As String
  Dim detectarcambios As Integer
  Dim strtiempototal As String
  Dim strtiempototal2 As String
  
'JRC 28/4/98
  If intIndex = 10 Then
    Call MsgBox("Opci�n no disponible.", vbExclamation)
    Exit Sub
  End If
  
  Tiempototal2 = ""
  tiempototal3 = ""
  Call Calcular_Tiempo(txtdia1.Text, txthora1.Text, txtminuto1.Text)
  TiempoTotal1 = TiempoTotal
  Call Calcular_Tiempo(txtdia2.Text, txthora2.Text, txtminuto2.Text)
  Tiempototal2 = TiempoTotal
  Call Calcular_Tiempo(txtdia3.Text, txthora3.Text, txtminuto3.Text)
  tiempototal3 = TiempoTotal
  If txtfaseText1(0).Text <> "" And txtText1(3).Text <> "" Then
    If Tiempototal2 = "" Then
      strtiempototal = " is Null"
    Else
      strtiempototal = "= " & Tiempototal2
    End If
    If tiempototal3 = "" Then
      strtiempototal2 = " is Null"
    Else
      strtiempototal2 = "= " & tiempototal3
    End If
    strTiempo = "SELECT count(*) FROM PR0500 where pr01codactuacion=" & txtfaseText1(0).Text & _
           " AND pr05numfase=" & txtText1(3).Text & _
           " AND pr05numocupaci=" & TiempoTotal1 & _
           " AND pr05numtminfpre" & strtiempototal & _
           " And pr05numtmaxfpre" & strtiempototal2
    Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
    If (rstTiempo.rdoColumns(0).Value) = 0 Then
      detectarcambios = 0
      Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal1)
      Call objWinInfo.CtrlSet(txtText1(6), Tiempototal2)
      Call objWinInfo.CtrlSet(txtText1(7), tiempototal3)
      objWinInfo.objWinActiveForm.blnChanged = True
      detectarcambios = 1
    'Else
    '  objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rstTiempo.Close
    Set rstTiempo = Nothing
  End If
    If objWinInfo.objWinActiveForm.blnChanged = True Then
    If cboSSDBCombo1(0).Value <> "" Then
      If cboSSDBCombo1(0).Value <> 0 Then
        If txtText1(6).Text > txtText1(7).Text Then
          Call MsgBox("El Tiempo M�nimo debe ser menor que el Tiempo M�ximo", vbExclamation)
          If txtText1(7).Text <> 0 Then
            txtText1(6).Text = txtText1(7).Text
          Else
            txtText1(7).Text = txtText1(6).Text
          End If
        End If
      Else
        If txtText1(6).Text <> 0 Then
          Call MsgBox("No hay fase precedente", vbExclamation)
          txtText1(6).Text = 0
        End If
      End If
    Else
      If txtText1(6).Text <> "" Then
        If txtText1(6).Text <> 0 Then
        Call MsgBox("No hay fase precedente", vbExclamation)
        txtText1(6).Text = 0
        End If
      End If
    End If
    If cboSSDBCombo1(0) <> "" Then
      If cboSSDBCombo1(0).Value <> 0 Then
        If txtText1(6).Text > txtText1(7).Text Then
          Call MsgBox("El Tiempo M�ximo debe ser mayor que el Tiempo M�nimo", vbExclamation)
          txtText1(7).Text = txtText1(6).Text
        End If
      Else
        If txtText1(7).Text <> 0 Then
          Call MsgBox("No hay fase precedente", vbExclamation)
          txtText1(7).Text = 0
        End If
      End If
    Else
      If txtText1(7).Text <> "" Then
        If txtText1(7).Text <> 0 Then
        Call MsgBox("No hay fase precedente", vbExclamation)
        txtText1(7).Text = 0
        End If
      End If
    End If
  End If
  '*********************************************************************
    intborrar = Borrar_Fases
    ' se quiere borrar la fase sin tipos de recurso
    If intborrar = 1 Then
        Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
        If intIndex = 10 Then
            Call txtText1_Change(5)
            Call txtText1_Change(6)
            Call txtText1_Change(7)
        End If
        If txtText1(3).Text = "" Then
            cmdtiposrecurso.Enabled = False
        End If
    End If
    
  'Localizar
  If (intIndex = 16 And txtText1(3).Text <> "") Then
        strsql = "select count(PR05NUMFASE) from PR0500" & _
                  " where PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
                  " and PR05NUMFASE=" & txtText1(3).Text
        Set rstA1 = objApp.rdoConnect.OpenResultset(strsql)
        intCont = rstA1.rdoColumns(0).Value
        If intCont = 1 Then
            cmdtiposrecurso.Enabled = True
        End If
        rstA1.Close
        Set rstA1 = Nothing
    End If

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)

End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   grdDBGrid1(0).Enabled = True
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  grdDBGrid1(0).Enabled = True
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Dim intResp As Integer
  
  If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) And (intIndex = 0) Then
    If (cboSSDBCombo1(0).Text >= txtText1(3).Text) Then
      intResp = MsgBox("La fase precedente debe tener un N� de Fase menor al N� de Fase actual", vbInformation, "Importante")
      cboSSDBCombo1(0).Text = ""
      cboSSDBCombo1(0).SetFocus
    Else
      Call objWinInfo.CtrlLostFocus
    End If
  Else
    Call objWinInfo.CtrlLostFocus
  End If
  
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Dim intResp As Integer
  
  If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) And (intIndex = 0) Then
    If (cboSSDBCombo1(0).Text >= txtText1(3).Text) Then
      intResp = MsgBox("La fase precedente debe tener un N� de Fase menor al N� de Fase actual", vbInformation, "Importante")
      cboSSDBCombo1(0).Text = ""
    Else
      Call objWinInfo.CtrlDataChange
    End If
  Else
    Call objWinInfo.CtrlDataChange
  End If
  intcambioalgo = 1
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  Dim intResp As Integer
  
  If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) And (intIndex = 0) Then
    If (cboSSDBCombo1(0).Text >= txtText1(3).Text) Then
      intResp = MsgBox("La fase precedente debe tener un N� de Fase menor al N� de Fase actual", vbInformation, "Importante")
      cboSSDBCombo1(0).Text = ""
    Else
      Call objWinInfo.CtrlDataChange
    End If
  Else
    Call objWinInfo.CtrlDataChange
  End If
  
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Dim intResp As Integer
  
  If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) And (Index = 0) Then
    If (cboSSDBCombo1(0).Text >= txtText1(3).Text) Then
      intResp = MsgBox("La fase precedente debe tener un N� de Fase menor al N� de Fase actual", vbInformation, "Importante")
      cboSSDBCombo1(0).Text = ""
    Else
      Call objWinInfo.CtrlDataChange
    End If
  Else
    Call objWinInfo.CtrlDataChange
  End If
End Sub



Private Sub txtdia1_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtdia2_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtdia3_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtfaseText1_GotFocus(Index As Integer)
txtText1(3).SetFocus
End Sub

Private Sub txthora1_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora2_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora3_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto1_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto2_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto3_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
End Sub
Private Sub txtdia1_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia1.Text) = False Then
    Beep
    txtdia1.Text = ""
    txtdia1.SetFocus
  End If
  If txtdia1.Text <> "" Then
     If txtdia1.Text > 65 Then
      txtdia1.Text = 65
     End If
  End If
End Sub

Private Sub txthora1_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora1.Text) = False Then
    Beep
    txthora1.Text = ""
    txthora1.SetFocus
  End If
  If txthora1.Text <> "" Then
     If txthora1.Text > 23 Then
      txthora1.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto1_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto1.Text) = False Then
    Beep
    txtminuto1.Text = ""
    txtminuto1.SetFocus
  End If
  If txtminuto1.Text <> "" Then
     If txtminuto1.Text > 59 Then
      txtminuto1.Text = 59
     End If
  End If
End Sub
Private Sub txtdia2_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia2.Text) = False Then
    Beep
    txtdia2.Text = ""
    txtdia2.SetFocus
  End If
  If txtdia2.Text <> "" Then
     If txtdia2.Text > 65 Then
      txtdia2.Text = 65
     End If
  End If
End Sub

Private Sub txthora2_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora2.Text) = False Then
    Beep
    txthora2.Text = ""
    txthora2.SetFocus
  End If
  If txthora2.Text <> "" Then
     If txthora2.Text > 23 Then
      txthora2.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto2_change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto2.Text) = False Then
    Beep
    txtminuto2.Text = ""
    txtminuto2.SetFocus
  End If
  If txtminuto2.Text <> "" Then
     If txtminuto2.Text > 59 Then
      txtminuto2.Text = 59
     End If
  End If
End Sub
Private Sub txtdia3_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia3.Text) = False Then
    Beep
    txtdia3.Text = ""
    txtdia3.SetFocus
  End If
  If txtdia3.Text <> "" Then
     If txtdia3.Text > 65 Then
      txtdia3.Text = 65
     End If
  End If
End Sub

Private Sub txthora3_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora3.Text) = False Then
    Beep
    txthora3.Text = ""
    txthora3.SetFocus
  End If
  If txthora3.Text <> "" Then
     If txthora3.Text > 23 Then
      txthora3.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto3_change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto3.Text) = False Then
    Beep
    txtminuto3.Text = ""
    txtminuto3.SetFocus
  End If
  If txtminuto3.Text <> "" Then
     If txtminuto3.Text > 59 Then
      txtminuto3.Text = 59
     End If
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)

  objWinInfo.objWinActiveForm.strWhere = "PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
                " AND " & _
                " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")"


  'si el c�digo es vac�o se deshabilita el bot�n Tipos de Recurso
  If (intIndex = 3) Then
    intcambioalgo = 0
    cmdtiposrecurso.Enabled = False
  End If
  'si se ha pulsado Abrir Registro se carga el c�digo de actuaci�n
  If (intIndex = 3) Then
    txtText1(1).Text = txtfaseText1(0).Text
  End If
  Call objWinInfo.CtrlDataChange
  If intIndex = 5 Then
    If txtText1(5).Text = "" Then
      txtdia1.Text = 0
      txthora1.Text = 0
      txtminuto1.Text = 0
    Else
      txtdia1.Text = txtText1(5).Text \ 1440                 'd�as
      txthora1.Text = (txtText1(5).Text Mod 1440) \ 60       'horas
      txtminuto1.Text = (txtText1(5).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  If intIndex = 6 Then
    If txtText1(6).Text = "" Then
      txtdia2.Text = 0
      txthora2.Text = 0
      txtminuto2.Text = 0
    Else
      txtdia2.Text = txtText1(6).Text \ 1440                 'd�as
      txthora2.Text = (txtText1(6).Text Mod 1440) \ 60       'horas
      txtminuto2.Text = (txtText1(6).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  If intIndex = 7 Then
    If txtText1(7).Text = "" Then
      txtdia3.Text = 0
      txthora3.Text = 0
      txtminuto3.Text = 0
    Else
      txtdia3.Text = txtText1(7).Text \ 1440                 'd�as
      txthora3.Text = (txtText1(7).Text Mod 1440) \ 60       'horas
      txtminuto3.Text = (txtText1(7).Text Mod 1440) Mod 60   'minutos
    End If
  End If
End Sub


'---------------------------------------------------------
'
' FUNCION Borrar_Fases
' borra las fases que no tienen ning�n tipo de recurso
'
'---------------------------------------------------------

Private Function Borrar_Fases() As Integer
  Dim sqlstring1 As String
  Dim rstA1 As rdoResultset
  Dim sqlstring3 As String
  Dim rstA3 As rdoResultset
  Dim strrespuesta As String
  Dim strrespuesta1 As String
  Dim blnrespuesta As Integer
  Dim sqlstr4 As String
  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim rstborrar As rdoResultset
  Dim strborrar As String
  
  On Error GoTo Err_Ejecutar
  Borrar_Fases = 0
  intfasecurso = txtText1(3).Text
  If intfasecurso <> "" Then
    sqlstring3 = "select count(*) from PR0500 " & _
             "where PR05NUMFASE=" & intfasecurso & _
             " and PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstA3 = objApp.rdoConnect.OpenResultset(sqlstring3)
    'la fase ya est� guardada
    sqlstring1 = "select count(*) from PR1300 " & _
             "where PR05NUMFASE=" & intfasecurso & _
             " and PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstring1)
    ' se mira el n� de Tipos de Recurso
    If (rstA1.rdoColumns(0).Value = 0 And mnabrir = 0) Then
             strrespuesta = MsgBox("La Fase con C�digo = " & intfasecurso & _
             " no tiene definida ning�n Tipo de Recurso." & Chr(13) & _
             "� Desea borrar dicha fase ?" & Chr(13) & _
             "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
             "asociarle al menos un Tipo de Recurso. ", 36, "Fases")
             If strrespuesta = vbYes Then
                 'eliminar la fase si est� guardada
                 If rstA3.rdoColumns(0).Value = 1 Then
                    ' se hace DELETE para que no saque el mensaje de
                    ' confirmaci�n de borrado
                    sqlstr4 = "DELETE FROM PR0500 " _
                    & "WHERE (pr05numfase = " & txtText1(3) _
                    & " and PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & ")"
                    On Error GoTo Err_Ejecutar
                    objApp.rdoConnect.Execute sqlstr4, 64
                    objApp.rdoConnect.Execute "Commit", 64
                    '*********************************************************************
                     'Si la fase borrada era la �ltima fase, hay que cambiar la clausula strWhere del frmdefactuacionesCUN
  
                      strborrar = "SELECT COUNT(*) " & _
                                  "  FROM PR0500 " & _
                                  " WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0).Text
                      Set rstborrar = objApp.rdoConnect.OpenResultset(strborrar)
                      If rstborrar.rdoColumns(0).Value > 0 Then
                          objWinInfo.objWinActiveForm.strWhere = "PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
                          " AND " & _
                          " pr05numfase IN (SELECT pr05numfase FROM PR1300 " & _
                          " WHERE pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")"
                      End If
                    objWinInfo.objWinActiveForm.blnChanged = False
                    intfasecurso = txtText1(3).Text
                    If rstborrar.rdoColumns(0).Value > 0 Then
                        objWinInfo.DataRefresh
                    End If
                    rstborrar.Close
                    Set rstborrar = Nothing
                 End If
                 intdelete = 1
                 objWinInfo.objWinActiveForm.blnChanged = False
                 mncancelar = 0
            Else
                'no se borra la fase
                blnrespuesta = 1
                mncancelar = 1
            End If
    Else
            mncancelar = 0
    End If
    If (blnrespuesta = 0) Then
        ' se borra la fase
        Borrar_Fases = 1
    End If
    intfasecurso = txtText1(3).Text
    rstA1.Close
    Set rstA1 = Nothing
    rstA3.Close
    Set rstA3 = Nothing
End If

intfasecurso = txtText1(3).Text

  
  'Si la fase borrada era la �ltima fase, hay que cambiar la clausula strWhere del frmdefactuacionesCUN
  
  strSelect = "SELECT COUNT(*) " & _
              "  FROM PR0500 " & _
              " WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value = 0 Then
    gstrWhereSinFases = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
            & "pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")"
  Else
    gstrWhereSinFases = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
  End If
  
  rstF.Close
  Set rstF = Nothing

Exit Function

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Function
End Function
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Button
' -----------------------------------------------

Private Sub cmdtiposrecurso_Click()
Dim rsta As rdoResultset
Dim sqlstr As String
Dim intCont As Integer
Dim strmensaje As String

    cmdtiposrecurso.Enabled = False
   
    ' se mira que la fase est� guardada antes de ir a sus tipos de recurso. Puede no ser as�
    ' si se est� en una fase guardada y a�n sin tipos de recurso y se pulsa Localizar,
    ' respondemos que queremos borrar la fase y en la ventana de Localizar
    ' inmediatamente se da a Cerrar y la fase sin tipos de recurso sigue en pantalla pero
    ' ya no est� guardada y el bot�n Tipos de Recurso sigue activo
     If (txtText1(3) <> "") Then
     sqlstr = "select count(PR05NUMFASE) from PR0500" & _
            " where PR05NUMFASE=" & txtText1(3).Text & _
            " And PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
     Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
     intCont = rsta.rdoColumns(0).Value
     ' fase guardada
     If intCont = 1 Then
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        frmdeftiposrecurso.txtacttext1(0).Text = txtfaseText1(0).Text
        frmdeftiposrecurso.txtacttext1(1).Text = txtfaseText1(2).Text
        frmdeftiposrecurso.txtacttext1(2).Text = txtText1(3).Text
        frmdeftiposrecurso.txtacttext1(3).Text = txtText1(4).Text
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        Call objsecurity.LaunchProcess("PR0124")
        'Load frmdeftiposrecurso
        'frmdeftiposrecurso!tabTab1(1).Tab = 0 'para mostrar el detalle del Tab
        'frmdeftiposrecurso.Show (vbModal)
        'Unload frmdeftiposrecurso
        'Set frmdeftiposrecurso = Nothing
        cmdtiposrecurso.Enabled = True
      Else
        strmensaje = MsgBox("No puede definir ning�n Tipo de Recurso para la fase : " _
                      & txtText1(3) & " puesto que no est� guardada.", vbCritical, _
                       "Definici�n de Fases")
        Call objWinInfo.WinProcess(6, 26, 0)
      End If
     rsta.Close
     Set rsta = Nothing
     End If
        
End Sub

Private Function Ver_Relaciones() As String
'JMRL 3/12/97
'Ver_Relaciones es una funci�n que devuelve "" si no hay ning�n problema a la hora de
'borrar una fase.

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  Dim blnEs_Ultima_fase As String
  
  strmensaje = "La fase NO puede se borrada por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
  blnEs_Ultima_fase = False
  
  'Se comprueba que la fase no tiene tipos de recursos
  strSelect = "SELECT COUNT(*) FROM PR1300" & _
              " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
              "   AND PR05NUMFASE=" & txtText1(3).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La fase TIENE TIPOS DE RECURSOS." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
      
  'Se compueba que la fase no pertenezca a una actuaci�n ya pedida
  strSelect = "SELECT COUNT(*) FROM PR0300" & _
              " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La fase PERTENECE A UNA ACTUACION PEDIDA." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
  
  'Se comprueba que si es la �nica fase que le queda a la actuaci�n
  strSelect = "SELECT COUNT(*) FROM PR0500" & _
              " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value = 1 Then
    blnEs_Ultima_fase = True
  End If
  rstF.Close
  Set rstF = Nothing
  
  If blnEs_Ultima_fase Then
    'Se compueba que la actuaci�n no tenga muestras
    strSelect = "SELECT COUNT(*) FROM PR2500" & _
                " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
    If rstF.rdoColumns(0).Value > 0 Then
      strmensaje = strmensaje & "� La fase PERTENECE A UNA ACTUACION CON MUESTRAS." & Chr(13)
      blnSacarMensaje = True
    End If
    rstF.Close
    Set rstF = Nothing
       
    'Se compueba que la actuaci�n no tenga condiciones
    strSelect = "SELECT COUNT(*) FROM PR2300" & _
                " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
    If rstF.rdoColumns(0).Value > 0 Then
      strmensaje = strmensaje & "� La fase PERTENECE A UNA ACTUACION CON CONDICIONES." & Chr(13)
      blnSacarMensaje = True
    End If
    rstF.Close
    Set rstF = Nothing
    
    'Se compueba que la actuaci�n no tenga cuestionario
    strSelect = "SELECT COUNT(*) FROM PR2900" & _
                " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
    If rstF.rdoColumns(0).Value > 0 Then
      strmensaje = strmensaje & "� La fase PERTENECE A UNA ACTUACION CON CUESTIONARIO." & Chr(13)
      blnSacarMensaje = True
    End If
    rstF.Close
    Set rstF = Nothing
       
    'Se compueba que la actuaci�n no tenga actuaciones asociadas
    strSelect = "SELECT COUNT(*) FROM PR3100" & _
                " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
    If rstF.rdoColumns(0).Value > 0 Then
      strmensaje = strmensaje & "� La fase PERTENECE A UNA ACTUACION CON ACTUACIONES ASOCIADAS." & Chr(13)
      blnSacarMensaje = True
    End If
    rstF.Close
    Set rstF = Nothing
    
    'Se compueba que la actuaci�n no tenga actuaciones previsibles
    strSelect = "SELECT COUNT(*) FROM PR3200" & _
                " WHERE PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
    If rstF.rdoColumns(0).Value > 0 Then
      strmensaje = strmensaje & "� La fase PERTENECE A UNA ACTUACION CON ACTUACIONES PREVISIBLES." & Chr(13)
      blnSacarMensaje = True
    End If
    rstF.Close
    Set rstF = Nothing
  End If
  
  If blnSacarMensaje = True Then
    Ver_Relaciones = strmensaje
  Else
    Ver_Relaciones = ""
  End If
  
End Function
Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 65 Then
    d = 65
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
End Sub


