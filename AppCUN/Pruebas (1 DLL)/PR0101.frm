VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmpaquetes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Paquetes"
   ClientHeight    =   7080
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11175
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7080
   ScaleWidth      =   11175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdSeleccAct 
      Caption         =   "Seleccionar"
      DragIcon        =   "PR0101.frx":0000
      Height          =   375
      Left            =   5400
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   7680
      Width           =   2175
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Paquete"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   11295
      Begin TabDlg.SSTab tabTab1 
         Height          =   2055
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3625
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0101.frx":0442
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblPaquete(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblPaquete(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblPaquete(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblPaquete(12)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtDesDpto"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0101.frx":045E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "C�d. Departamento|C�digo Departamento"
            Top             =   1320
            Width           =   600
         End
         Begin VB.TextBox txtDesDpto 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Left            =   1560
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n Departamento|Descripci�n Departamento"
            Top             =   1320
            Width           =   5400
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Selecci�n Autom�tica"
            DataField       =   "PR33INDSELAUTOMA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   7560
            TabIndex        =   4
            Tag             =   "Selecci�n Autom�tica"
            Top             =   1320
            Width           =   2265
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR33DESESTANDARD"
            Height          =   330
            Index           =   1
            Left            =   1560
            TabIndex        =   2
            Tag             =   "Descripci�n Paquete|Descripci�n del Paquete"
            Top             =   600
            Width           =   9000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR33CODESTANDARD"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   240
            TabIndex        =   1
            Tag             =   "C�d. Paquete|C�digo Paquete"
            Top             =   600
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1770
            Index           =   0
            Left            =   -74880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   120
            Width           =   10335
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18230
            _ExtentY        =   3122
            _StockProps     =   79
            Caption         =   "PAQUETES"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblPaquete 
            Caption         =   "C�d. Dpto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   240
            TabIndex        =   15
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblPaquete 
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   1560
            TabIndex        =   12
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblPaquete 
            Caption         =   "Descripci�n Paquete"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1560
            TabIndex        =   11
            Top             =   360
            Width           =   2055
         End
         Begin VB.Label lblPaquete 
            Caption         =   "C�d. Paquete"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   10
            Top             =   360
            Width           =   1215
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   6795
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones del Paquete"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Index           =   2
      Left            =   360
      TabIndex        =   5
      Top             =   3120
      Width           =   11295
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3855
         Index           =   2
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   11010
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19420
         _ExtentY        =   6800
         _StockProps     =   79
         Caption         =   "ACTUACIONES DEL PAQUETE"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmpaquetes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00101.FRM                                                  *
'* AUTOR: JESUS MARIA RODILLA LARA                                      *
'* FECHA: 4 DE AGOSTO DE 1997                                           *
'* DESCRIPCION: permite crear nuevos paquetes de actuaciones. Tambi�n se*
'*              puede modificar un paquete                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim mblnInicio As Integer
Dim mblnesta_lleno As Integer
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Function CrearFiltroInforme(strEntrada As String, strPrefijo As String, strBuscar As String)
'Toma como entrada una cadena de caracteres (strEntrada). En esta cadena de entrada busca la
'palabra (strBuscar) y cuando la encuentra le a�ade el prefijo (strPrefijo)

Dim strSalida As String
Dim strPalabra1 As String

    Do While Len(strEntrada) > 0
       strPalabra1 = Left(strEntrada, Len(strBuscar))
       If strPalabra1 = strBuscar Then
          strSalida = strSalida & strPrefijo & strPalabra1
          strEntrada = Right(strEntrada, Len(strEntrada) - Len(strBuscar))
       Else
          strSalida = strSalida & Left(strEntrada, 1)
          strEntrada = Right(strEntrada, Len(strEntrada) - 1)
       End If
    Loop
    CrearFiltroInforme = strSalida
End Function



Private Sub cmdSeleccAct_Click()
  Dim vResp As Variant
  
  If txtText1(0).Text <> "" Then
    objWinInfo.DataSave
    'Load frmSeleccionarActPaq
    'frmSeleccionarActPaq.Show (vbModal)
    'Unload frmSeleccionarActPaq
    'Set frmSeleccionarActPaq = Nothing
    Call objsecurity.LaunchProcess("PR0162")
    objWinInfo.DataRefresh
  Else
    vResp = MsgBox("Primero debe seleccionar un paquete", vbInformation)
  End If
  
End Sub

Private Sub Form_Activate()

  'Pasamos el foco al primer campo para llenar el drid de actuaciones
  txtText1(0).SetFocus
  'Inicializamos las valiables para cargar el grid
  mblnInicio = 0
  mblnesta_lleno = 0
   
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm

  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
  With objMasterInfo
    .strName = "Paquetes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR3300"
    strKey = .strTable
    Call .FormAddOrderField("PR33CODESTANDARD", cwAscending)
    
    Call .objPrinter.Add("PR1011", "Listado de paquetes")
    
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "Paquetes")
    Call .FormAddFilterWhere(strKey, "PR33CODESTANDARD", "C�digo Paquete", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR33DESESTANDARD", "Descripci�n Paquete", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
    
    'Se establecen los campos por los que se puede ordenar con el filtro
    Call .FormAddFilterOrder(strKey, "PR33CODESTANDARD", "C�digo Paquete")
    Call .FormAddFilterOrder(strKey, "PR33DESESTANDARD", "Descripci�n Paquete")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Departamento")
   
    
  End With
  
  
  With objMultiInfo
    .strName = "Actuaciones del Paquete"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR3400"
    .intAllowance = cwAllowDelete
    .intCursorSize = 0
    Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
    Call .FormAddRelation("PR33CODESTANDARD", txtText1(0))
 
    strKey = .strDataBase & .strTable
    
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "Actuaciones del Paquete")
    Call .FormAddFilterWhere(strKey, "PR33CODESTANDARD", "C�digo Paquete", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
    
    'Se establecen los campos por los que se puede ordenar con el filtro
    Call .FormAddFilterOrder(strKey, "PR33CODESTANDARD", "C�digo Paquete")
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
  End With

  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del paquete
    Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "C�d. Dpto. Realizador", "AD02CODDPTO_REA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Departamento Realizador", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "C�d.Dpto.", "AD02CODDPTO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "C�d. Paquete", "PR33CODESTANDARD", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Paquete", "", cwString, 50)
    
    
    Call .FormCreateInfo(objMasterInfo)
    
    'Llenamos la descripci�n del departamento del paquete en el frame Paquetes
     Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
     Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtDesDpto, "AD02DESDPTO")
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    'en el grid que contiene las actuaciones del paquete
    .CtrlGetInfo(grdDBGrid1(2).Columns(7)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(7)).blnMandatory = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(9)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(9)).blnMandatory = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnMandatory = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(5)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnMandatory = True
    
     
    'Se llena el combo, del grid que contiene los paquetes, que contiene los departamentos
    '.CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Main") & "AD0200 ORDER BY AD02CODDPTO ASC"

    'Se establecen los campos por los que se puede hacer una busqueda
    .CtrlGetInfo(txtText1(0)).blnInFind = True 'Codigo paquete
    .CtrlGetInfo(txtText1(1)).blnInFind = True 'Descipci�n del paquete
    
    'Instrucciones para que el dpto del paquete se pueda buscar por la lista
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    
    'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
    'contiene las actuaciones del paquete
    
    
    'Sacamos el nombre del departamento que define el paquete
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(7)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(7)), grdDBGrid1(2).Columns(8), "AD02DESDPTO")
    
    'Sacamos el nombre del paquete
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(9)), "PR02CODPESTANDARD", "SELECT * FROM PR3300 WHERE PR33CODESTANDARD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(9)), grdDBGrid1(2).Columns(10), "PR33DESESTANDARD")
    
    'Sacamos el nombre de la actuaci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), grdDBGrid1(2).Columns(4), "PR01DESCORTA")
    
    'Sacamos el nombre del departamento que realiza esa actuaci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), "AD02CODDPTO_REA", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(6), "AD02DESDPTO")
    
    Call .WinRegister
    Call .WinStabilize
  End With
    gstrLlamadorSel = "Paquetes"
    'Call objApp.SplashOff
   
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


'jcr 6/3/98
'Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  Dim rsta As rdoResultset
'  Dim strsqlA As String
'
'  If (strFormName = "Paquetes") And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
'    strsqlA = "SELECT AD02DESDPTO " _
'             & "FROM AD0200 " _
'             & "WHERE AD02CODDPTO =" & cboSSDBCombo1(0)
'    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
'    txtDesDpto.Text = rsta.rdoColumns(0).Value
'    rsta.close
'    Set rsta = Nothing
'  End If
'  txtDesDpto.Locked = True
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strFiltro As String
  Dim strOrden As String
  
  If strFormName = "Paquetes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      If blnHasFilter = False Then
          strFiltro = objWinInfo.DataGetWhere(blnHasFilter)
      Else
          strFiltro = CrearFiltroInforme(objWinInfo.DataGetWhere(blnHasFilter), "PR3302J.", "PR33CODESTANDARD")
          strFiltro = CrearFiltroInforme(strFiltro, "PR3302J.", "AD02CODDPTO")
          strFiltro = CrearFiltroInforme(strFiltro, "PR3302J.", "PR33DESESTANDARD")
      End If
      strOrden = CrearFiltroInforme(objWinInfo.DataGetOrder(blnHasFilter, True), "PR3302J.", "PR33CODESTANDARD")
      strOrden = CrearFiltroInforme(strOrden, "PR3302J.", "AD02CODDPTO")
      strOrden = CrearFiltroInforme(strOrden, "PR3302J.", "PR33DESESTANDARD")
      Call objPrinter.ShowReport(strFiltro, strOrden)
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub
Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Paquetes" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
      .strOrder = "ORDER BY AD02CODDPTO ASC"
      
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�d. Dpto."
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Desc. Dpto."
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
        Call objWinInfo.CtrlSet(txtDesDpto, .cllValues("AD02DESDPTO"))
      End If
      
    End With
    Set objSearch = Nothing
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


'jcr 6/3/98
'Private Sub tabTab1_GotFocus(intIndex As Integer)
'  Dim rsta As rdoResultset
'  Dim strsqlA As String
'
'  If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
'    strsqlA = "SELECT AD02DESDPTO " _
'             & "FROM AD0200 " _
'             & "WHERE AD02CODDPTO =" & cboSSDBCombo1(0)
'    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
'    txtDesDpto.Text = rsta.rdoColumns(0).Value
'    rsta.close
'    Set rsta = Nothing
'  End If
'  txtDesDpto.Locked = True
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr As String
  Dim rsta As rdoResultset

  If (btnButton.Index = 8) And (objWinInfo.objWinActiveForm.strName = "Paquetes") Then
    If (Tiene_Actuaciones = False) Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  
    
    If btnButton.Index = 2 Then
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        sqlstr = "SELECT PR33CODESTANDARD_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0) = rsta.rdoColumns(0).Value
        txtText1(0).Locked = True
        'txtText1(1).SetFocus
        txtText1(2).SetFocus
        rsta.Close
        Set rsta = Nothing
  End If
 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  If (intIndex = 60) And (objWinInfo.objWinActiveForm.strName = "Paquetes") Then
    If (Tiene_Actuaciones = False) Then
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
  
    If intIndex = 10 Then
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        sqlstr = "SELECT PR33CODESTANDARD_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0) = rsta.rdoColumns(0).Value
        txtText1(0).Locked = True
        'txtText1(1).SetFocus
        txtText1(2).SetFocus
        rsta.Close
        Set rsta = Nothing
  End If
 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
 
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Dim strDpto As String
  Dim strPaq As String
  
  strDpto = txtText1(2).Text
  strPaq = txtText1(0).Text
                            
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  
  'If intIndex = 1 Then
  '      If ((tabTab1(1).Tab = 1) And (grdDBGrid1(1).SelBookmarks.Count = 0)) Or ((strDpto = "") Or (strPaq = "")) Then
  '          'Deshabilitamos el bot�n de a�adir
  '          cmda�adir.Enabled = False
  '      Else
  '          'Habilitamos el bot�n de a�adir
  '          cmda�adir.Enabled = True
  '      End If
  'End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblPquete_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblPaquete(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
    'jcr 6/3/98
  'Dim rsta As rdoResultset
  'Dim strsqlA As String
  
  Call objWinInfo.CtrlDataChange
  
  'If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
  '  strsqlA = "SELECT AD02DESDPTO " _
  '           & "FROM AD0200 " _
  '           & "WHERE AD02CODDPTO =" & cboSSDBCombo1(0)
  '  Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
  '  txtDesDpto.Text = rsta.rdoColumns(0).Value
  '  rsta.close
  '  Set rsta = Nothing
  'End If
  'txtDesDpto.Locked = True
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
    'jcr 6/3/98
  'Dim rsta As rdoResultset
  'Dim strsqlA As String
  
  Call objWinInfo.CtrlDataChange
  
  'If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0))) And (cboSSDBCombo1(0) <> "") Then
  '  strsqlA = "SELECT AD02DESDPTO " _
  '           & "FROM AD0200 " _
  '           & "WHERE AD02CODDPTO =" & cboSSDBCombo1(0)
  '  Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
  '  txtDesDpto.Text = rsta.rdoColumns(0).Value
  '  rsta.close
  '  Set rsta = Nothing
  'End If
  'txtDesDpto.Locked = True
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
    'jcr 6/3/98
  'Dim rsta As rdoResultset
  'Dim strsqlA As String
  
  Call objWinInfo.CtrlDataChange
  
  'If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
  '  strsqlA = "SELECT AD02DESDPTO " _
  '           & "FROM AD0200 " _
  '           & "WHERE AD02CODDPTO =" & cboSSDBCombo1(0)
  '  Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
  '  txtDesDpto.Text = rsta.rdoColumns(0).Value
  '  rsta.close
  '  Set rsta = Nothing
  'End If
  'txtDesDpto.Locked = True
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
    'jcr 6/3/98
  'Dim rsta As rdoResultset
  'Dim strsqlA As String
 
   
   Call objWinInfo.CtrlGotFocus
   
  'Llenamos la descripci�n de departamento
  'If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
  '  strsqlA = "SELECT AD02DESDPTO " _
  '           & "FROM AD0200 " _
  '           & "WHERE AD02CODDPTO =" & cboSSDBCombo1(0)
  '  Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
  '  txtDesDpto.Text = rsta.rdoColumns(0).Value
  '  rsta.close
  '  Set rsta = Nothing
  'End If
  'txtDesDpto.Locked = True
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Public Function Tiene_Actuaciones() As Boolean
'JMRL 4/12/97
'Tiene_Actuaciones es una funci�n que devuelve TRUE si hay ning�n problema a la hora de
'borrar un paquete.

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "El paquete NO puede se borrado por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
      
  'Se comprueba que el paquete no tiene ACTUACIONES
  strSelect = "SELECT COUNT(*) FROM PR3400" & _
              " WHERE PR33CODESTANDARD=" & txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� El paquete TIENE ACTUACIONES." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Actuaciones = True
  Else
    Tiene_Actuaciones = False
  End If
  

End Function


