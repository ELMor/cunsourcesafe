VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmDefProtocolos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Protocolos"
   ClientHeight    =   7320
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11610
   ControlBox      =   0   'False
   HelpContextID   =   1
   Icon            =   "PR0105.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7320
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdSeleccAct 
      Caption         =   "Seleccionar"
      DragIcon        =   "PR0105.frx":000C
      Height          =   375
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   7680
      Width           =   2175
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones del Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   2
      Left            =   360
      TabIndex        =   3
      Top             =   2880
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4095
         Index           =   1
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   18785
         _ExtentY        =   7223
         _StockProps     =   79
         Caption         =   "ACTUACIONES DEL PROTOCOLO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      HelpContextID   =   2
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   10935
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         HelpContextID   =   90001
         Index           =   1
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _ExtentX        =   18785
         _ExtentY        =   3201
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0105.frx":044E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0105.frx":046A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR35DESPROTOCOLO"
            Height          =   330
            Index           =   1
            Left            =   480
            MultiLine       =   -1  'True
            TabIndex        =   2
            Tag             =   "Descripci�n Protocolo|Descripci�n del Protocolo"
            Top             =   1200
            Width           =   9000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR35CODPROTOCOLO"
            Height          =   330
            HelpContextID   =   1
            Index           =   0
            Left            =   480
            TabIndex        =   1
            Tag             =   "C�digo Protocolo"
            Top             =   480
            Width           =   852
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1530
            Index           =   2
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   120
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   2699
            _StockProps     =   79
            Caption         =   "PROTOCOLOS"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            Caption         =   "C�digo Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   6
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Descripci�n Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   480
            TabIndex        =   10
            Top             =   960
            Width           =   2295
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   7035
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefProtocolos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00105.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 14 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite crear nuevos protocolos de actuaciones. Tambi�n *
'*              se puede modificar un protocolo                         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim refrescar As Boolean
Dim mbolabrir As Boolean
Dim mbolcancelar As Boolean
Dim gblntransaccion As Boolean




Private Sub cmdSeleccAct_Click()
  Dim vResp As Variant
  
  If txtText1(0).Text <> "" Then
   objWinInfo.DataSave
   'Load frmSeleccionarActProt
   'frmSeleccionarActProt.Show (vbModal)
   'Unload frmSeleccionarActProt
   'Set frmSeleccionarActProt = Nothing
   Call objsecurity.LaunchProcess("PR0168")
   objWinInfo.DataRefresh
  Else
    vResp = MsgBox("Primero debe seleccionar un protocolo", vbInformation)
  End If
    
End Sub

Private Sub Form_Activate()
    txtText1(0).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strProtocolo As String
    Dim strRestoClausula As String
    Dim strKey As String
    
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMasterInfo
        .strName = "Protocolos"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(1)
        Set .grdGrid = grdDBGrid1(2)
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR3500"
        .blnMasive = False
        strKey = .strDataBase & .strTable
    '.strWhere = "(pr35codprotocolo in (select pr35codprotocolo from PR3600)) "
                           
        Call .FormAddOrderField("PR35CODPROTOCOLO", cwAscending)
        
        Call .objPrinter.Add("PR1051", "Listado de protocolos")
        'Call .objPrinter.Add("PR001052", "Listado de actuaciones de un protocolo")
        
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Protocolos")
        Call .FormAddFilterWhere(strKey, "PR35CODPROTOCOLO", "C�digo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR35DESPROTOCOLO", "Descripci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR35CODPROTOCOLO", "C�digo")
        Call .FormAddFilterOrder(strKey, "PR35DESPROTOCOLO", "Descripci�n")
  
    End With
  
  
    With objMultiInfo
        .strName = "Actuaciones del protocolo"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = fraFrame1(0)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .intAllowance = cwAllowDelete
        
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR3600"
        .intCursorSize = 0

        Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
        Call .FormAddRelation("PR35CODPROTOCOLO", txtText1(0))
 
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Actuaciones del Protocolo")
        Call .FormAddFilterWhere(strKey, "PR35CODPROTOCOLO", "C�digo Protocolo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR35CODPROTOCOLO", "C�digo Protocolo")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    End With

    With objWinInfo


    

        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del protocolo
        Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Dpto.", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d. Protocolo", "PR35CODPROTOCOLO", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Protocolo", "", cwString, 50)
    
    
        Call .FormCreateInfo(objMasterInfo)
    
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True

        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del protocolo
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
 
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
    

        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del protocolo
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "AD02DESDPTO")

        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(4), "PR01DESCORTA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), "PR35CODPROTOCOLO", "SELECT * FROM PR3500 WHERE PR35CODPROTOCOLO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), grdDBGrid1(1).Columns(8), "PR35DESPROTOCOLO")

        Call .WinRegister
        Call .WinStabilize
    End With
    gstrLlamadorSel = "Protocolos"
    'Call objApp.SplashOff
    refrescar = False
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    Dim strrespuesta As String
    Dim sqlstring As String
    Dim sqlstring2 As String
    Dim rsta As rdoResultset
    
    'Se controla que al salir no quede un protocolo sin sus actuaciones
    If txtText1(0).Text <> "" Then
        On Error GoTo Err_Ejecutar
        'se seleccionan todos los protocolos
        sqlstring = "select PR35CODPROTOCOLO,PR35DESPROTOCOLO from PR3500 " & _
                    "where PR35CODPROTOCOLO=" & txtText1(0).Text & " order by PR35CODPROTOCOLO asc "
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        If grdDBGrid1(1).Rows = 0 And mbolabrir = False Then
            strrespuesta = MsgBox("El protocolo no tiene ninguna actuaci�n asociada y ser� borrado", vbOKCancel, "Protocolos")
            If strrespuesta = vbOK Then
                'eliminar el protocolo
                    If rsta.EOF = False Then
                       sqlstring2 = "delete from PR3500 where " & _
                                     "PR35CODPROTOCOLO=" & rsta.rdoColumns(0).Value
                        objApp.rdoConnect.Execute sqlstring
                    Else
                        objWinInfo.objWinActiveForm.blnChanged = False
                    End If
                    If gblntransaccion = True Then
                        CommitTrans
                        gblntransaccion = False
                    End If
                    intCancel = objWinInfo.WinExit
            Else
                intCancel = 1
            End If
        Else
            intCancel = objWinInfo.WinExit
        End If
    Else
        intCancel = objWinInfo.WinExit
    
    rsta.Close
    Set rsta = Nothing
  End If
    Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Protocolos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim strrespuesta As String
    Dim rsta As rdoResultset
    Dim sqlstring As String
    Dim sqlstring2 As String
    Dim rstA1 As rdoResultset
    Dim sqlstr1 As String
    Dim cerrarcursor As Integer
          
    'Se controla que no quede ning�n protocolo sin actuaciones
    If (objWinInfo.objWinActiveForm.strName = "Protocolos") Then
        On Error GoTo Err_Ejecutar
        'Se seleccionan todos los protocolos
        If (txtText1(0).Text <> "") Then
          cerrarcursor = 1
          sqlstring = "select PR35CODPROTOCOLO,PR35DESPROTOCOLO from PR3500 " & _
                      "where PR35CODPROTOCOLO=" & txtText1(0).Text & " order by PR35CODPROTOCOLO asc "
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        End If
        'Controla si el usuario se mueve por la barra de herramientas
        Select Case btnButton.Index
        Case 2, 3, 16, 18, 21, 22, 23, 24
            'Se controla que no haya actuaciones del protocolo y que no se haya pulsado abrir
            If grdDBGrid1(1).Rows = 0 And mbolabrir = False Then
                If txtText1(0).Text <> "" Then
                  strrespuesta = MsgBox("El protocolo no tiene ninguna actuaci�n asociada y ser� borrado", vbOKCancel, "Protocolos")
                Else
                  strrespuesta = "1"
                End If
                If strrespuesta = vbOK Then
                    'eliminar el protocolo
                    If (txtText1(0).Text <> "") Then
                      If (rsta.EOF = False) Then
                        sqlstring2 = "delete from PR3500 where " & _
                                     "PR35CODPROTOCOLO=" & rsta.rdoColumns(0).Value
                        objApp.rdoConnect.Execute sqlstring2
                      Else
                        objWinInfo.objWinActiveForm.blnChanged = False
                      End If
                    End If
                    'Se hace el commit
                    If gblntransaccion = True Then
                        CommitTrans
                        gblntransaccion = False
                    End If
                    'grdDBgrid1(2).RemoveItem (grdDBgrid1(2).Row)
                    
                    mbolcancelar = False
                    'Si se pulsa nuevo se hace el begintrans
                    If btnButton.Index = 2 Then
                        BeginTrans
                        gblntransaccion = True
                    End If
                    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                    If btnButton.Index = 2 Then
                      On Error GoTo Err_Ejecutar
                      ' generaci�n autom�tica del c�digo
                      sqlstr1 = "SELECT PR35CODPROTOCOLO_SEQUENCE.nextval FROM dual"
                      Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
                      txtText1(0) = rstA1.rdoColumns(0).Value
                      rstA1.Close
                      Set rstA1 = Nothing
                      txtText1(0).Locked = True
                      'txtText1(1).SetFocus
                    End If
                Else
                    mbolcancelar = True
                End If
            Else
                'Si se pulsa nuevo se hace el begintrans
                If btnButton.Index = 2 Then
                    BeginTrans
                    gblntransaccion = True
                End If
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                mbolcancelar = False
                If btnButton.Index = 2 Then
                  On Error GoTo Err_Ejecutar
                  ' generaci�n autom�tica del c�digo
                  sqlstr1 = "SELECT PR35CODPROTOCOLO_SEQUENCE.nextval FROM dual"
                  Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
                  txtText1(0) = rstA1.rdoColumns(0).Value
                  rstA1.Close
                  Set rstA1 = Nothing
                  txtText1(0).Locked = True
                  'txtText1(1).SetFocus
                End If
            End If
        Case 4
            'Se hace el commit
            If grdDBGrid1(1).Rows <> 0 Then
                If gblntransaccion = True Then
                    CommitTrans
                    gblntransaccion = False
                End If
            End If
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Case Else
          If (btnButton.Index = 8) And (objWinInfo.objWinActiveForm.strName = "Protocolos") Then
            If (Tiene_Actuaciones = False) Then
              Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            End If
          Else
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          End If
        End Select
    'Se pulsa el bot�n abrir
    If btnButton.Index = 3 Then
        If mbolcancelar = False Then
            mbolabrir = True
        Else
            mbolabrir = False
        End If
    Else
        mbolabrir = False
    End If
    If cerrarcursor = 1 Then
      rsta.Close
      Set rsta = Nothing
      cerrarcursor = 0
    End If
    Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
  Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        'rsta.Close
        'Set rsta = Nothing
        'rstA1.Close
        'Set rstA1 = Nothing
  End If

'rsta.Close
'Set rsta = Nothing
'rstA1.Close
'Set rstA1 = Nothing
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Dim strrespuesta As String
    Dim rsta As rdoResultset
    Dim sqlstring As String
    Dim sqlstring2 As String
    Dim rstA1 As rdoResultset
    Dim sqlstr1 As String
    Dim cerrarcursor As Integer
    
    If objWinInfo.objWinActiveForm.strName = "Protocolos" Then
        On Error GoTo Err_Ejecutar
        ' se seleccionan todos las protocolos
        If txtText1(0).Text <> "" Then
          cerrarcursor = 1
          sqlstring = "select PR35CODPROTOCOLO,PR35DESPROTOCOLO from PR3500 " & _
                      "where PR35CODPROTOCOLO=" & txtText1(0).Text & " order by PR35CODPROTOCOLO asc "
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        End If
        Select Case intIndex
        Case 10, 20
            If grdDBGrid1(1).Rows = 0 And mbolabrir = False Then
                If txtText1(0).Text <> "" Then
                  strrespuesta = MsgBox("El protocolo no tiene ninguna actuaci�n asociada y ser� borrado", vbOKCancel, "Protocolos")
                Else
                  strrespuesta = "1"
                End If
                If strrespuesta = vbOK Then
                    'eliminar el protocolo
                    If txtText1(0).Text <> "" Then
                      If rsta.EOF = False Then
                        sqlstring2 = "delete from PR3500 where " & _
                                     "PR35CODPROTOCOLO=" & rsta.rdoColumns(0).Value
                        objApp.rdoConnect.Execute sqlstring2
                      Else
                        objWinInfo.objWinActiveForm.blnChanged = False
                      End If
                    End If
                    If gblntransaccion = True Then
                        CommitTrans
                        gblntransaccion = False
                    End If
                    grdDBGrid1(2).RemoveItem (grdDBGrid1(2).Row)
                    mbolcancelar = False
                    If intIndex = 10 Then
                        BeginTrans
                        gblntransaccion = True
                    End If
                    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
                    If intIndex = 10 Then
                      On Error GoTo Err_Ejecutar
                      ' generaci�n autom�tica del c�digo
                      sqlstr1 = "SELECT PR35CODPROTOCOLO_SEQUENCE.nextval FROM dual"
                      Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
                      txtText1(0) = rstA1.rdoColumns(0).Value
                      rstA1.Close
                      Set rstA1 = Nothing
                      txtText1(0).Locked = True
                      'txtText1(1).SetFocus
                    End If
                Else
                    mbolcancelar = True
                End If
            Else
                If intIndex = 10 Then
                    BeginTrans
                    gblntransaccion = True
                End If
                mbolcancelar = False
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
                If intIndex = 10 Then
                  On Error GoTo Err_Ejecutar
                  ' generaci�n autom�tica del c�digo
                  sqlstr1 = "SELECT PR35CODPROTOCOLO_SEQUENCE.nextval FROM dual"
                  Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
                  txtText1(0) = rstA1.rdoColumns(0).Value
                  rstA1.Close
                  Set rstA1 = Nothing
                  txtText1(0).Locked = True
                  'txtText1(1).SetFocus
                End If
            End If
        Case 40
            If grdDBGrid1(1).Rows <> 0 Then
                If gblntransaccion = True Then
                    CommitTrans
                    gblntransaccion = False
                End If
            End If
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        Case Else
          If (intIndex = 60) And (objWinInfo.objWinActiveForm.strName = "Protocolos") Then
            If (Tiene_Actuaciones = False) Then
              Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            End If
          Else
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
          End If
        End Select
        If intIndex = 20 Then
            If mbolcancelar = False Then
                mbolabrir = True
            Else
                mbolabrir = False
            End If
        Else
            mbolabrir = False
        End If
        If cerrarcursor = 1 Then
          rsta.Close
          Set rsta = Nothing
          cerrarcursor = 0
        End If
        Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
  Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        rsta.Close
        Set rsta = Nothing
        rstA1.Close
        Set rstA1 = Nothing
  End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Dim strrespuesta As String
    Dim rsta As rdoResultset
    Dim sqlstring As String
    Dim sqlstring2 As String
    Dim cerrarcursor As Integer
    
    If objWinInfo.objWinActiveForm.strName = "Protocolos" Then
        On Error GoTo Err_Ejecutar
        If txtText1(0).Text <> "" Then
          cerrarcursor = 1
          ' se seleccionan todos las protocolos
          sqlstring = "select PR35CODPROTOCOLO,PR35DESPROTOCOLO from PR3500 " & _
                      "where PR35CODPROTOCOLO=" & txtText1(0).Text & " order by PR35CODPROTOCOLO asc "
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        End If

        Select Case intIndex
        Case 10
            If grdDBGrid1(1).Rows = 0 And mbolabrir = False Then
                If txtText1(0).Text <> "" Then
                  strrespuesta = MsgBox("El protocolo no tiene ninguna actuaci�n asociada y ser� borrado", vbOKCancel, "Protocolos")
                Else
                  strrespuesta = "1"
                End If
                If strrespuesta = vbOK Then
                    'eliminar el protocolo
                    If txtText1(0).Text <> "" Then
                      If rsta.EOF = False Then
                        sqlstring2 = "delete from PR3500 where " & _
                                     "PR35CODPROTOCOLO=" & rsta.rdoColumns(0).Value
                        objApp.rdoConnect.Execute sqlstring2
                     Else
                        objWinInfo.objWinActiveForm.blnChanged = False
                     End If
                     End If
                     If gblntransaccion = True Then
                       CommitTrans
                       gblntransaccion = False
                     End If
                     grdDBGrid1(2).RemoveItem (grdDBGrid1(2).Row)
                     Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
                End If
            Else
                Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
            End If
        Case 40
            If grdDBGrid1(1).Rows <> 0 Then
                If gblntransaccion = True Then
                    CommitTrans
                    gblntransaccion = False
                End If
            End If
            Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
        Case Else
            Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
        End Select
        If cerrarcursor = 1 Then
          rsta.Close
          Set rsta = Nothing
          cerrarcursor = 0
        End If
        Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
  Else
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  End If

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Dim strrespuesta As String
    Dim rsta As rdoResultset
    Dim sqlstring As String
    Dim sqlstring2 As String
    Dim cerrarcursor As Integer
    
    If objWinInfo.objWinActiveForm.strName = "Protocolos" Then
        On Error GoTo Err_Ejecutar
        If txtText1(0).Text <> "" Then
          cerrarcursor = 1
          ' se seleccionan todos las protocolos
          sqlstring = "select PR35CODPROTOCOLO,PR35DESPROTOCOLO from PR3500 " & _
                      "where PR35CODPROTOCOLO=" & txtText1(0).Text & " order by PR35CODPROTOCOLO asc "
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        End If
        Select Case intIndex
        Case 10, 20, 40, 50, 60, 70
            If grdDBGrid1(1).Rows = 0 And mbolabrir = False Then
                If txtText1(0).Text <> "" Then
                  strrespuesta = MsgBox("El protocolo no tiene ninguna actuaci�n asociada ", vbOKCancel, "Protocolos")
                Else
                  strrespuesta = "1"
                End If
                If strrespuesta = vbOK Then
                    'eliminar el protocolo
                    If txtText1(0).Text <> "" Then
                      If rsta.EOF = False Then
                        sqlstring2 = "delete from PR3500 where " & _
                                     "PR35CODPROTOCOLO=" & rsta.rdoColumns(0).Value
                        objApp.rdoConnect.Execute sqlstring2
                      Else
                        objWinInfo.objWinActiveForm.blnChanged = False
                      End If
                    End If
                    If gblntransaccion = True Then
                        CommitTrans
                        gblntransaccion = False
                    End If
                    grdDBGrid1(2).RemoveItem (grdDBGrid1(2).Row)
                    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
                End If
            Else
                Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
            End If
        Case 40
            If grdDBGrid1(1).Rows <> 0 Then
                If gblntransaccion = True Then
                    CommitTrans
                    gblntransaccion = False
                End If
            End If
            Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
        Case Else
            Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
        End Select
        If cerrarcursor = 1 Then
          cerrarcursor = 0
          rsta.Close
          Set rsta = Nothing
        End If
        Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If

End Sub
Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange

End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
    'Se vac�a el grid en el caso de que se pulse abrir
    If intIndex = 0 And objWinInfo.intWinStatus = cwModeSingleOpen Then
        grdDBGrid1(1).RemoveAll
    End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    objWinInfo.CtrlDataChange
End Sub

Public Function Tiene_Actuaciones() As Boolean
'JMRL 4/12/97
'Tiene_Actuaciones es una funci�n que devuelve TRUE si hay ning�n problema a la hora de
'borrar un protocolo

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "El protocolo NO puede se borrado por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
      
  'Se comprueba que el paquete no tiene ACTUACIONES
  strSelect = "SELECT COUNT(*) FROM PR3600" & _
              " WHERE PR35CODPROTOCOLO=" & txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� El protocolo TIENE ACTUACIONES." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Actuaciones = True
  Else
    Tiene_Actuaciones = False
  End If
  

End Function




