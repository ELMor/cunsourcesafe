VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmFirmarActuaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Firmar Actuaciones"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11640
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0191.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11640
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11640
      _ExtentX        =   20532
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Realizadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7215
      Index           =   0
      Left            =   240
      TabIndex        =   6
      Top             =   600
      Width           =   10845
      Begin TabDlg.SSTab tabTab1 
         Height          =   6540
         Index           =   0
         Left            =   360
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   480
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   11536
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0191.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(6)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(7)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(10)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(6)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cmdFirmar"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0191.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   360
            TabIndex        =   15
            Tag             =   "C�digo Actuaci�n|C�d. Actuaci�n"
            Top             =   1200
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01DESCORTA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1680
            TabIndex        =   14
            Tag             =   "Nombre de la Actuaci�n|Actuaci�n"
            Top             =   1200
            Width           =   5400
         End
         Begin VB.CommandButton cmdFirmar 
            Caption         =   "Firmar"
            Height          =   375
            Left            =   7320
            TabIndex        =   13
            Top             =   1800
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR37CODESTADO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   360
            TabIndex        =   1
            Tag             =   "C�digo  Estado|C�d. Estado"
            Top             =   1800
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR04DESINFORME"
            Height          =   3930
            HelpContextID   =   30104
            Index           =   10
            Left            =   360
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "Informe"
            Top             =   2400
            Width           =   9105
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   1680
            TabIndex        =   2
            Tag             =   "Estado|Estado"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR04NUMACTPLAN"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   360
            TabIndex        =   0
            Tag             =   "N� Actuaci�n Planificada|N� Act. Planidicada"
            Top             =   585
            Width           =   1092
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5985
            Index           =   0
            Left            =   -74760
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   240
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16325
            _ExtentY        =   10557
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   360
            TabIndex        =   17
            Top             =   960
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1680
            TabIndex        =   16
            Top             =   960
            Width           =   870
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Informe de la Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   360
            TabIndex        =   12
            Top             =   2160
            Width           =   2040
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1680
            TabIndex        =   11
            Top             =   1560
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   360
            TabIndex        =   10
            Top             =   1560
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Act. Planif."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   8
            Top             =   360
            Width           =   1230
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   8055
      Width           =   11640
      _ExtentX        =   20532
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFirmarActuaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1




Private Sub cmdFirmar_Click()
'Se captura el usuario que se a conectado del login y se busca en la tabla
'de personal de la cl�nica
  'Dim strSql As String
  'Dim rstA As rdoResultset
  Dim strInforme As String
  Dim intResp As Integer
  Dim strFirmante As String
  Dim sqlstrper As String
  Dim rstper As rdoResultset
    
  sqlstrper = "SELECT * FROM SG0200 WHERE SG02COD ='" & objsecurity.strUser & "'"
  Set rstper = objApp.rdoConnect.OpenResultset(sqlstrper)
  If rstper.EOF = False Then
    gstrNombrePerCUN = rstper("SG02NOM").Value
    gstrPriApelPerCUN = rstper("SG02APE1").Value
    gstrSegApelPerCUN = rstper("SG02APE2").Value
  Else
      intResp = MsgBox("Usuario desconocido : " & objsecurity.strUser, vbCritical, "Aviso")
  End If
  rstper.Close
  Set rstper = Nothing
  
  
  'If (Not (IsNull(txtText1(10).Text))) And (txtText1(10).Text <> "") And (txtText1(0).Text <> "") Then
    'La actuaci�n est� informada
    strInforme = txtText1(10).Text
    If (Len(strInforme) < (2000 - 200)) Then
      'Hay espacio suficiente para escribir los datos de la persona que firma
      strFirmante = "Dr/a: " & gstrNombrePerCUN & " " & gstrPriApelPerCUN & " " & gstrSegApelPerCUN
      strFirmante = strFirmante & " N� Colegiado: " & gintNumColegiado
      strFirmante = strFirmante & "                                                         "
      strFirmante = strFirmante & "                                                         "
      strFirmante = strFirmante & "INFORME:                                                 "
      strFirmante = strFirmante & "                                                         "
      strFirmante = strFirmante & "                                                         "
      strInforme = strFirmante & strInforme
      Call objWinInfo.CtrlSet(txtText1(6), 7)
      Call objWinInfo.CtrlSet(txtText1(10), strInforme)
      objWinInfo.objWinActiveForm.blnChanged = True
      objWinInfo.DataSave
      cmdfirmar.Enabled = False
    Else
      intResp = MsgBox("Al firmar se perderan las dos �ltimas l�neas", vbInformation, "Aviso")
    End If
 ' Else
   ' intResp = MsgBox("Actuaci�n NO Informada", vbInformation, "Aviso")
 ' End If
    
End Sub

Private Sub Form_Activate()
  'Deshabilitamos el bot�n de abrir puesto que al estar los campos como solo lectura
  'no tiene sentido. Hacemos lo mismo con en men� abrir
 ' tlbToolbar1.Buttons(3).Enabled = False
 ' mnuDatosOpcion.Item(20).Enabled = False
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Actuaciones Realizadas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0403J"
    .strWhere = "(PR37CODESTADO = 4) AND (PR0403J.PR04NUMACTPLAN = " & gstrActPlan & ")"
    '.strWhere = "((PR37CODESTADO = 7) OR (PR37CODESTADO = 4)) AND (AD02CODDPTO = " & gintCodDpto & ")"
   ' .strTable = "PRINFORMAR"
    .intAllowance = cwAllowModify
    
    Call .FormAddOrderField("PR04NUMACTPLAN", cwAscending)
    
    'Call .objPrinter.Add("EMP1", "Listado 1 de empleados")
    'Call .objPrinter.Add("EMP2", "Listado 2 de empleados")
    'Call .objPrinter.Add("EMP3", "Listado 3 de empleados")
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Actuaciones Realizadas")
    Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "N�mero Actuaci�n Planificada", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "AD01CODASISTENCI", "N�mero de Asistencia   ", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "N�mero Actuaci�n Pedida    ", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo de Paciente     ", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n:      ", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n:             ", cwString)
    'Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento    ", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Departamento           ", cwString)
    Call .FormAddFilterWhere(strKey, "PR37CODESTADO", "C�digo de Estado       ", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado                 ", cwString)
    Call .FormAddFilterWhere(strKey, "PR04DESINFORME", "Informe                ", cwString)
    
    
    Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "N�mero Actuaci�n Planificada")
    'Call .FormAddFilterOrder("AD01CODASISTENCI", "N�mero de Asistencia   ")
    'Call .FormAddFilterOrder("PR03NUMACTPEDI", "N�mero Actuaci�n Pedida    ")
    'Call .FormAddFilterOrder("CI21CODPERSONA", "C�digo de Paciente     ")
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n:      ")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n:             ")
    'Call .FormAddFilterOrder("AD02CODDPTO", "C�digo Departamento    ")
    'Call .FormAddFilterOrder("AD02DESDPTO", "Departamento           ")
    Call .FormAddFilterOrder(strKey, "PR37CODESTADO", "C�digo de Estado       ")
    Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado                 ")
    Call .FormAddFilterOrder(strKey, "PR04DESINFORME", "Informe                ")
    
  

  End With
  
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    'Establecemos los campos por los que se puede buscar
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    '.CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(4)).blnInFind = True
    '.CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    '.CtrlGetInfo(txtText1(8)).blnInFind = True
    '.CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    
    'Ponemos los siguientes campos de solo lectura para que no los modifiquen
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(3)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(4)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(5)).blnReadOnly = True
    .CtrlGetInfo(txtText1(6)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(8)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(9)).blnReadOnly = True
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
   
   
    
    'Establecemos una lista en el campo c�digo de estado
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), _
          "PR37CODESTADO", "SELECT PR37CODESTADO,PR37DESESTADO FROM PR3700 WHERE PR37CODESTADO = ? ")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "PR37DESESTADO")
                                 
    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Empleados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Actuaciones Realizadas" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "PR3700"
      .strWhere = "WHERE PR37CODESTADO = 3 OR PR37CODESTADO= 4"
      .strOrder = "ORDER BY PR37CODESTADO ASC"
      
      Set objField = .AddField("PR37CODESTADO")
      objField.strSmallDesc = "Estado"
      
      Set objField = .AddField("PR37DESESTADO")
      
'      Call .SetZoom("Zoom", "Seleccione un estado")
      
      If .Search Then
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(6)), .cllValues("PR37CODESTADO"))
        Call objWinInfo.CtrlSet(txtText1(6), .cllValues("PR37CODESTADO"))
        'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(7)), .cllValues("PR37DESESTADO"))
        Call objWinInfo.CtrlSet(txtText1(7), .cllValues("PR37DESESTADO"))
      End If
      
'      If .ViewSelect Then
'        Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(6)), .cllValues("PR37CODESTADO"))
'        Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(7)), .cllValues("PR37DESESTADO"))
        'Call MsgBox("Estado: " & .cllValues("PR37CODESTADO") & " - " & .cllValues("PR37DESESTADO"))
'      End If
    End With
    Set objSearch = Nothing
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  If (btnButton.Index = 4) Or (btnButton.Index = 14) Or _
     (btnButton.Index = 26) Or (btnButton.Index = 30) Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  If (intIndex = 40) Or (intIndex = 100) Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  If (intIndex = 10) Then
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  End If
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  If (intIndex = 10) Then
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


