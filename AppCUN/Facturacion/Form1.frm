VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   270
      Top             =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Lanzar Proceso"
      Height          =   1050
      Left            =   1035
      TabIndex        =   0
      Top             =   405
      Width           =   2580
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type typeRNF
  Asistencia As Long
  CodCateg As String
  CodRNF As String
  Cantidad As Long
  Fecha As String
End Type
Dim MatrizRNF() As typeRNF
Dim CadenaConceptos As String

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf
Dim NumAsist As String
Dim Propuesta As String
Dim Asist As String
Dim NodoFactura As String
Dim CodPaciente As String
Dim UltimaLinea As Integer
Dim qry(1 To 10) As New rdoQuery

'Este tipo de usuario lo utilizaremos para agrupar rnfs de cara la impresi�n
Private Type TypeRNFs
  NumFact As Integer
  NumLinea As Integer
  Codigo As Long
  Descrip As String
  Cantidad As Integer
End Type

Dim ArrayContador() As TypeRNFs

Private Sub Command1_Click()
Dim misql As String
Dim rsBusqueda As rdoResultset

'  MiSqL = "Select * From * Where * = *"
'  Set rsBusqueda = objApp.rdoConnect.OpenResultset(MiSqL, 3)
'  If Not rsBusqueda.EOF Then
'    rsBusqueda.MoveLast
'    rsBusqueda.MoveFirst
'    While Not rsBusqueda.EOF
'      Set varFact = New Factura
''      'Call Me.GenerarSqlRNF(rsBusqueda("*"))
''      'Call Me.LlenarTypeRNF(rsBusqueda("*"))
'      Call Me.GenerarSqlRNF(402033)
'      Call Me.LlenarTypeRNF(402033)
'      varFact.AsignarRNFs
'      'Call Me.GuardarFactura(rsBusqueda("*"))
'      Call Me.GuardarFactura(402033)
''      rsBusqueda.MoveNext
''      Set varFact = Nothing
''    Wend
''  Else
''    MsgBox "No hay historias que se correspondan con ese concierto"
''  End If
 '
Timer1.Enabled = True
End Sub

' Mediante la funci�n GuardarFactura rellenaremos los ficheros FA0400 (cabeceras de factura) y
' FA1600 (L�neas de factura)
'
Sub GuardarFactura(Historia As String)
Dim CodFactura As Long
Dim Dto As Double
Dim IntContLineas As Integer
Dim Facturada As Boolean
Dim SubImporte As Double
Dim Importe As Double
Dim IntCont As Integer
Dim lngAsistencia As Long
Dim rsPaciente As rdoResultset

  On Error GoTo ErrorEnGrabacion
  
  'Abrimos una transacci�n para tratar toda la inserci�n de las facturas.
  objApp.BeginTrans
    
    'Nos posicionamos en la propuesta seleccionada dentro del objeto myFactura
    CodPaciente = CStr(Historia)
    Set paciente = varFact.Pacientes(CodPaciente)
    For Each Asistencia In paciente.Asistencias
      Asistencia.Facturado = True
      lngAsistencia = Asistencia.Codigo
      Set PropuestaFact = Asistencia.PropuestasFactura(1)
'      grdssResponsable.MoveFirst
      'Inicializamos la varible que nos indica si la factura est� a�adida a la BD.
       Facturada = False
       IntContLineas = 1
       For Each NodoFACT In PropuestaFact.NodosFactura
         For Each Nodos In NodoFACT.Nodos
           'Comprobamos que la l�nea se va a facturar
           If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
             'Comprobamos si es el responsable principal, en ese caso le asignaremos tambi�n los que no tienen R.E.
'             If grdssResponsable.Columns(2).Text = "T" Then
               'Responsable Principal
'               If Nodos.EntidadResponsable = grdssResponsable.Columns(1).Text Or Nodos.EntidadResponsable = "" Then
                 If Facturada = False Then
                   'A�adiremos la factura
                   CodFactura = fNextClave("FA04CODFACT", "FA0400")
                   qry(4).rdoParameters(0) = CodFactura 'Secuencia
                   qry(4).rdoParameters(1) = "13/" & CodFactura
'                   grdssResponsable.Columns(4).Text = "13/" & CodFactura
                   qry(7).rdoParameters(0) = paciente.Codigo
                   Set rsPaciente = qry(7).OpenResultset
                   If Not rsPaciente.EOF Then
                     qry(4).rdoParameters(2) = rsPaciente(0) 'Paciente
                   Else
                     qry(4).rdoParameters(2) = Null
                   End If
'                   If grdssResponsable.Columns(5).Text = "E" Then
'                     qry(4).rdoParameters(3) = Nodos.EntidadResponsable 'Entidad
'                   Else
                     qry(4).rdoParameters(3) = Null
'                   End If
'                   If Trim(txtObservaciones(grdssResponsable.Columns(3).Text)) = "" Then
                     qry(4).rdoParameters(4) = Null
'                   Else
'                     qry(4).rdoParameters(4) = txtObservaciones(grdssResponsable.Columns(3).Text) 'Descripcion
'                   End If
                   qry(4).rdoParameters(5) = 0 'txtTotal(grdssResponsable.Columns(3).Text) 'Cantidad Facturada
                   qry(4).rdoParameters(6) = 0 '& txtDcto(grdssResponsable.Columns(3).Text) 'Dto aplicado
                   qry(4).rdoParameters(7) = lngAsistencia 'N�mero de la asistencia
'                   If Trim(txtObservaciones(grdssResponsable.Columns(3).Text)) = "" Then
                     qry(4).rdoParameters(8) = Null
'                   Else
'                     qry(4).rdoParameters(8) = txtObservaciones(grdssResponsable.Columns(3).Text) 'Observaciones
'                   End If
                   qry(4).Execute
                   Facturada = True
                 End If
                 qry(5).rdoParameters(0) = CodFactura 'C�digo de la factura
                 qry(5).rdoParameters(1) = IntContLineas 'N� de L�nea
                 If Not IsNull(Nodos.Codigo) Then
                   qry(5).rdoParameters(2) = Nodos.Codigo 'C�digo de la categor�a (si tiene)
                 Else
                   qry(5).rdoParameters(2) = Null
                 End If
                 qry(5).rdoParameters(3) = Nodos.Name 'Descripci�n
                 qry(5).rdoParameters(4) = Null '?????????????????????????????
                 Dto = Nodos.Descuento + Nodos.DescuentoManual
                 If Nodos.PrecioDia <> -1 Then
                   qry(5).rdoParameters(5) = Nodos.PrecioDia 'Precio diario
                   qry(5).rdoParameters(6) = Dto 'Dto de la l�nea incluido redondeo
                   SubImporte = Nodos.PrecioDia * Nodos.Cantidad
                   qry(5).rdoParameters(7) = CLng(SubImporte - ((SubImporte * Dto) / 100)) 'Importe de la l�nea
                   Importe = Importe + CLng(SubImporte - ((SubImporte * Dto) / 100))
                 Else
                   qry(5).rdoParameters(5) = Nodos.PrecioRef 'Precio de referencia
                   qry(5).rdoParameters(6) = Dto 'Dto
                   SubImporte = Nodos.PrecioRef * Nodos.Cantidad
                   qry(5).rdoParameters(7) = CLng(SubImporte - ((SubImporte * Dto) / 100)) 'Importe de la linea
                   Importe = Importe + CLng(SubImporte - ((SubImporte * Dto) / 100))
                 End If
                 qry(5).Execute
                 For Each rnf In Nodos.RNFs
                   'Asignamos a los rnfs de la FA0300, el n�mero de la factura y la l�nea
                   qry(6).rdoParameters(0) = CodFactura
                   qry(6).rdoParameters(1) = IntContLineas
                   qry(6).rdoParameters(2) = rnf.CodCateg
                   qry(6).rdoParameters(3) = Asistencia.Codigo
                   qry(6).Execute
                 Next
                 IntContLineas = IntContLineas + 1
               End If
'           End If
         Next
       Next
       qry(8).rdoParameters(0) = CLng(Importe)
       qry(8).Execute
    Next
  'Se ha grabado todo cerramos la transacci�n
  objApp.CommitTrans
Exit Sub

ErrorEnGrabacion:
  'Se ha producido un error en la grabaci�n, anulamos la transacci�n
  objApp.RollbackTrans
  MsgBox "Se ha producido un error en la grabaci�n.", vbOKOnly + vbCritical, "Atenci�n"
End Sub



Sub IniciarQRY()
Dim misql As String
Dim x As Integer
  
  ''Queries relacionadas con la obtenci�n de una entidad a partir del n�mero de asistencia.
  'Seleccionar el c�digo de entidad a partir del n�mero de asistencia.
  misql = "SELECT CI13CODENTIDAD FROM AD1100 WHERE AD01CODASISTENCI = ? AND AD11FECFIN IS NULL"
  qry(1).SQL = misql
  'Seleccionar la descripci�n de la entidad
  misql = "SELECT CI13DESENTIDAD FROM CI1300 WHERE CI13CODENTIDAD = ?"
  qry(2).SQL = misql
  
  'Seleccionar la descripci�n de una categor�a.
  misql = "Select FA05DESIG from FA0500 Where FA05CODCATEG = ?"
  qry(3).SQL = misql
  
  'Insertar una cabecera de factura en FA0400
  misql = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
              "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV) Values (?,?,?,?,?,?,?,?,?)"
  qry(4).SQL = misql
  'Isertar una l�nea de factura en FA1600
  misql = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
              "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE) Values (?,?,?,?,?,?,?,?)"
  qry(5).SQL = misql
  'Asignar a FA0300 el numero de la factura y el n�mero de l�nea
  misql = "Update FA0300 set FA04NUMFACT = ?, FA16NUMLINEA = ? " & _
              "Where FA05CODCATEG = ? AND AD01CODASISTENCI = ?"
  qry(6).SQL = misql
  
  misql = "Select CI21CODPERSONA From CI2200 Where CI22NUMHISTORIA = ?"
  qry(7).SQL = misql
  
  'Asignamos a la tabla FA0400 el total de la factura
  misql = "Update FA0400 set FA04CANTFACT = ?"
  qry(8).SQL = misql
  
  'Activamos las querys.
  For x = 1 To 8
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub

Sub GenerarSqlRNF(ConcAgrup As String)
Dim misql As String
Dim MiRs As rdoResultset
Dim Cadena As String
  
  misql = "Delete from FA0300 where Ci22numhistoria in (" & ConcAgrup & ")"
  objApp.rdoConnect.Execute misql
  Cadena = ""
  'Seleccionamos todas las vistas de los grupos que se encuentren activos
  misql = "SELECT FA08DESVISTA FROM FA0800 WHERE FA11CODESTGRUPO = 1"
  Set MiRs = objApp.rdoConnect.OpenResultset(misql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      'Creamos la SQL de selecci�n de los RNF
      misql = "INSERT INTO FA0300 (FA03NUMRNF,FA05CODCATEG,FA03CANTIDAD,FA13CODESTRNF," & _
                  "AD01CODASISTENCI,FA03FECHA,CI22NUMHISTORIA) SELECT FA03NUMRNF_SEQUENCE.NEXTVAL, " & _
                  "FA05CODCATEG, CANTIDAD,1, AD01CODASISTENCI,TO_DATE(FECHA,'DD/MM/YYYY'), " & _
                  "CI22NUMHISTORIA FROM " & MiRs(0) & " WHERE CI22NUMHISTORIA IN (" & ConcAgrup & ") "
      objApp.rdoConnect.Execute misql
      MiRs.MoveNext
    Wend
  End If
  
End Sub

Sub LlenarTypeRNF(SqlRNF As String)
Dim misql As String
Dim MiRs As rdoResultset
Dim cont As Long
Dim NumRNF As Long
Dim ContRNF As Long
Dim Fila As Integer
Dim RNFAnterior As String
Dim AsistAnterior As String
Dim PacienteAnterior As String
Dim FechaInicio As String
Dim FechaFinal As String
Dim Fecha As String

  On Error GoTo ErrorenRNF
  
  RNFAnterior = ""
  AsistAnterior = ""
  PacienteAnterior = ""
  ContRNF = 0
  'Si la cadena de selecci�n de RNF no es nula.
  If Trim(SqlRNF) <> "" Then
     misql = "SELECT * FROM FA0300 WHERE CI22NUMHISTORIA IN (" & SqlRNF & ") " & _
                " order by CI22NUMHISTORIA, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
  
    Set MiRs = objApp.rdoConnect.OpenResultset(misql, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      ReDim MatrizRNF(1 To MiRs.RowCount)
      cont = 1
      ContRNF = 0
      objApp.BeginTrans
        While Not MiRs.EOF
          If MiRs("CI22NUMHISTORIA") <> PacienteAnterior Then
            'Se ha detectado un nuevo n�mero de historia, creamos el paciente.
            Set paciente = New paciente
            paciente.Codigo = MiRs("CI22NUMHISTORIA")
            'Fila = FilaDelGrid(MiRs("CI22NUMHISTORIA"))
            'If Fila <> 0 Then
            '  grdHistoras.Row = Fila - 1
            '  paciente.Name = grdHistoras.Columns(1).Text
            '  paciente.Direccion = grdHistoras.Columns(2).Text
            '  paciente.FecNacimiento = grdHistoras.Columns(3).Text
            'End If
            varFact.Pacientes.Add paciente, MiRs("CI22NUMHISTORIA") & ""
            PacienteAnterior = MiRs("CI22NUMHISTORIA")
          End If
          If MiRs("AD01CODASISTENCI") <> AsistAnterior Then
            'Se ha detectado un nuevo n�menro de asistencia, creamos la asistencia.
            Set Asistencia = New Asistencia
            'Esto es provisional, habr� que borrarlo despu�s
            Asistencia.AddConcierto 97
            'If Trim(txtConcierto.Text) <> "" Then
            '  If IsNumeric(txtConcierto.Text) Then
            '    Asistencia.AddConcierto txtConcierto.Text
            '  End If
            'End If
            Asistencia.Codigo = MiRs("AD01CODASISTENCI")
            Asistencia.FecInicio = "01/01/1999 00:00:00"
            Asistencia.FecFin = "31/12/1999 00:00:00"
            Asistencia.Facturado = False
            paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
            AsistAnterior = MiRs("AD01CODASISTENCI")
            FechaInicio = "01/01/1900 00:00:00"
            FechaFinal = "31/12/3000 00:00:00"
          End If
          With Asistencia
            If MiRs("FA05CODCATEG") <> RNFAnterior Then
              cont = 0
              RNFAnterior = MiRs("FA05CODCATEG")
            Else
              cont = cont + 1
            End If
            Set NuevoRNF = New rnf
            With NuevoRNF
              .CodCateg = MiRs("FA05CODCATEG")
              .Fecha = MiRs("FA03FECHA") & ""
              .Cantidad = MiRs("FA03CANTIDAD")
              .Asignado = False
              .Key = MiRs("FA05CODCATEG") & "-" & Format(cont, "000")
            End With
           .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & Format(cont, "000")
            MiRs.MoveNext
            If Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS") < FechaInicio Then
              FechaInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
              Asistencia.FecInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
            End If
            If Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS") > FechaFinal Then
              FechaFinal = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
              Asistencia.FecFin = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
            End If
          End With
        Wend
      objApp.CommitTrans
    Else
      MsgBox "No hay ning�n registro que responda a las condiciones del filtro", vbOKOnly + vbInformation, "aviso"
    End If
  End If
Exit Sub
ErrorenRNF:
  MsgBox "Se ha producido un error en la inserci�n de los RNF", vbCritical + vbOKOnly, "Aviso"
  objApp.RollbackTrans

End Sub



Private Sub Form_Load()
  Call Me.IniciarQRY
  objCW.blnAutoDisconnect = False
  objCW.SetClockEnable False
End Sub

Private Sub Form_Unload(Cancel As Integer)
  objCW.blnAutoDisconnect = True
  objCW.SetClockEnable True
End Sub


Private Sub Timer1_Timer()
Dim misql As String
Dim rs As rdoResultset
Dim cont As Double

    misql = "seLECT SYSDATE FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(misql)
    If Not rs.EOF Then
      Form1.Caption = rs(0) & " " & cont
      Timer1.Interval = 100
      cont = cont + 1
    End If
    
End Sub


