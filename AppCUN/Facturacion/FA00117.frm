VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmConcSeleccionado 
   Caption         =   "Concierto Aplicado"
   ClientHeight    =   5175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5955
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5175
   ScaleWidth      =   5955
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Salir"
      Height          =   420
      Left            =   4365
      TabIndex        =   1
      Top             =   4635
      Width           =   1500
   End
   Begin ComctlLib.TreeView tvwConcierto 
      Height          =   4425
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   7805
      _Version        =   327682
      Indentation     =   354
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   405
      Top             =   4500
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":0000
            Key             =   "NotaPegada"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":031A
            Key             =   "SinDesglose"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00117.frx":0634
            Key             =   "Reloj"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConcSeleccionado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private NuevoRNF As RNF
Private PropuestaFact As PropuestaFactura
Private NodoFact As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private RNF As RNF

Dim Propuesta As String
Dim Asist As String
Dim NodoFactura As String
Dim CodPaciente As String



Dim PacDesg As String
Dim AsistDesg As String
Dim PropFacDesg As String
Dim NodoDesg As Integer
Dim RespEconom As String

Dim aOrdenacion() As Integer

Public Function pConcSelec(objFactura As Factura, paciente As String, Asistencia As String, Prop As String, nodofactra As String, responsable As String, aOrden() As Integer) As Factura
    Dim i  As Integer
    Dim z As Integer
    
    Set varFact = objFactura
    PacDesg = paciente
    AsistDesg = Asistencia
    PropFacDesg = Prop
    ReDim aOrdenacion(1 To 3, 1 To UBound(aOrden, 2))
    For i = 1 To 3
        For z = 1 To UBound(aOrden, 2)
            aOrdenacion(i, z) = aOrden(i, z)
        Next
    Next
    Load frmConcSeleccionado
      'Set frmSeleccion.varFact = objFactura
      frmConcSeleccionado.Show (vbModal)
            
    'Set objFactura = varFact
      Unload frmConcSeleccionado
End Function

Private Sub CargarTVW()
    Dim strTexto As String
    Dim lngPrecio As Long
    Dim lngSuma As Long
    Dim n As Node
    Dim nFact As Node
    Dim nLineaFact As Node
    
    Dim NodoFact As New Nodo
    Dim NodoLinea As New Nodo
    Dim i  As Integer
    Dim z As Integer
    Dim NodoFacturaCreado As Integer
    
    Set paciente = varFact.Pacientes(PacDesg)
    Set Asistencia = paciente.Asistencias(AsistDesg)
    Set PropuestaFact = Asistencia.PropuestasFactura(PropFacDesg)
    
    NodoFacturaCreado = 0
    ' Se crea el arbol del concierto ordenado
    For z = 1 To UBound(aOrdenacion, 2)
        ' si el nodo de factura (ej. Privado, Forfait..., etc.) no est� creado se crea
        If aOrdenacion(2, z) <> NodoFacturaCreado Then
            NodoFacturaCreado = aOrdenacion(2, z)
            Set NodoFact = PropuestaFact.NodosFactura(NodoFacturaCreado)
            Set nFact = tvwConcierto.Nodes.Add(, , , NodoFact.Name, "Reloj")
        End If
        ' se le cuelgan las lineas de factura ya ordenadas
        Set NodoLinea = NodoFact.Nodos(aOrdenacion(3, z))
        Set nLineaFact = tvwConcierto.Nodes.Add(nFact, tvwChild, , NodoLinea.Name & "       " & Format((NodoLinea.LineaFact.Valor + NodoLinea.LineaFact.ValorNoDescontable), "###,###,##0") & " Pts", "Reloj")
        nLineaFact.Sorted = True
        For Each RNF In NodoLinea.RNFs
          Set n = tvwConcierto.Nodes.Add(nLineaFact, tvwChild, , RNF.Name & IIf(RNF.Descripcion <> "", " (" & RNF.Descripcion & ")", "") & "     ...(" & RNF.Cantidad & " Ud)", IIf(RNF.Desglose, "NotaPegada", "SinDesglose"))
        Next RNF
            
    Next
    
    lngSuma = 0
'    Set paciente = varFact.Pacientes(PacDesg)
'    Set Asistencia = paciente.Asistencias(AsistDesg)
'    Set PropuestaFact = Asistencia.PropuestasFactura(PropFacDesg)
'
'    For Each NodoFACT In PropuestaFact.NodosFactura
'
'      For Each Nodos In NodoFACT.Nodos
'        Set nLineaFact = tvwConcierto.Nodes.Add(nFact, tvwChild, , Nodos.Name & "     ...(" & (Nodos.LineaFact.Valor + Nodos.LineaFact.ValorNoDescontable) & " Pts)", "Reloj")
'        For Each RNF In Nodos.RNFs
'          Set n = tvwConcierto.Nodes.Add(nLineaFact, tvwChild, , RNF.Name & IIf(RNF.Descripcion <> "", " (" & RNF.Descripcion & ")", "") & "     ...(" & RNF.Cantidad & " Ud)", IIf(RNF.Desglose, "NotaPegada", "SinDesglose"))
'
'        Next RNF
'
'      Next Nodos
'
'    Next NodoFACT
'

End Sub

Private Sub cmdSalir_Click()
  Me.Hide
End Sub

Private Sub Form_Load()
   Call CargarTVW
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    If Me.Width < 4000 Then Me.Width = 4000
    If Me.Height < 4000 Then Me.Height = 4000
    
    tvwConcierto.Width = Me.Width - 300
    tvwConcierto.Height = Me.Height - 1080
    cmdSalir.Left = Me.Width - 1710
    cmdSalir.Top = Me.Height - 870
End Sub
