VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmNuevoResponsable 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "A�adir un nuevo responsable econ�mico"
   ClientHeight    =   5550
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5550
   ScaleWidth      =   7575
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   "Resultados de la b�squeda"
      Height          =   3570
      Left            =   45
      TabIndex        =   7
      Top             =   1890
      Width           =   7485
      Begin SSDataWidgets_B.SSDBGrid grdPersonas 
         Height          =   3075
         Left            =   45
         TabIndex        =   5
         Top             =   315
         Width           =   7305
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         GroupHeadLines  =   0
         Col.Count       =   3
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2487
         Columns(0).Caption=   "N� Historia"
         Columns(0).Name =   "Historia"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8096
         Columns(1).Caption=   "Nombre y Apellidos"
         Columns(1).Name =   "Paciente"
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1773
         Columns(2).Caption=   "CodPersona"
         Columns(2).Name =   "CodPersona"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   12885
         _ExtentY        =   5424
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   285
      Left            =   3330
      TabIndex        =   4
      Top             =   315
      Width           =   1590
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de selecci�n"
      Height          =   1815
      Left            =   45
      TabIndex        =   6
      Top             =   45
      Width           =   5280
      Begin VB.TextBox txtApellido2 
         Height          =   285
         Left            =   1485
         TabIndex        =   3
         Text            =   " "
         Top             =   1350
         Width           =   3615
      End
      Begin VB.TextBox txtApellido1 
         Height          =   285
         Left            =   1485
         TabIndex        =   2
         Text            =   " "
         Top             =   990
         Width           =   3615
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   1485
         TabIndex        =   1
         Top             =   630
         Width           =   3615
      End
      Begin VB.TextBox txtNumHistoria 
         Height          =   285
         Left            =   1485
         TabIndex        =   0
         Text            =   " "
         Top             =   270
         Width           =   1635
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "2� Apellido:"
         Height          =   240
         Index           =   3
         Left            =   270
         TabIndex        =   11
         Top             =   1395
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "1er. Apellido:"
         Height          =   240
         Index           =   2
         Left            =   270
         TabIndex        =   10
         Top             =   1035
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   240
         Index           =   1
         Left            =   270
         TabIndex        =   9
         Top             =   675
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "N� Historia:"
         Height          =   240
         Index           =   0
         Left            =   270
         TabIndex        =   8
         Top             =   315
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmNuevoResponsable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Responsable As String
Private Sub cmdBuscar_Click()

Dim rsNombre As rdoResultset
Dim MiSelect As String
Dim MiWhere As String
Dim MiSql As String
Dim Linea As String

  If Trim(txtNumHistoria) = "" And Trim(txtNombre) = "" And Trim(txtApellido1) = "" And Trim(txtApellido2) = "" Then
    MsgBox "Debe introducir alg�n dato para realizar la selecci�n", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  End If

  MiSelect = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22FECNACIM, CI22NUMDIRPRINC, " & _
          "CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID, CI22NUMHISTORIA" & _
          " from CI2200, CI1000"
  MiWhere = " WHERE"
  If Trim(txtNumHistoria) <> "" Then
    MiWhere = MiWhere & " CI2200.CI22NUMHISTORIA =" & txtNumHistoria & " AND"
  End If
  If Trim(txtNombre) <> "" Then
    MiWhere = MiWhere & " CI2200.CI22NOMBREALF LIKE '%" & UCase(Trim(txtNombre)) & "%' AND"
  End If
  If Trim(txtApellido1) <> "" Then
    MiWhere = MiWhere & " CI2200.CI22PRIAPELALF LIKE '%" & UCase(Trim(txtApellido1)) & "%' AND"
  End If
  If Trim(txtApellido2) <> "" Then
    MiWhere = MiWhere & " CI2200.CI22SEGAPELALF LIKE '%" & UCase(Trim(txtApellido2)) & "%' AND"
  End If
  MiWhere = MiWhere & " (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
          " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)"
  
  MiSql = MiSelect & MiWhere
  Set rsNombre = objApp.rdoConnect.OpenResultset(MiSql, 3)
  
  If Not rsNombre.EOF Then
      rsNombre.MoveFirst
      Me.grdPersonas.RemoveAll
      While Not rsNombre.EOF
      Linea = rsNombre("CI22NUMHISTORIA") & Chr(9) & rsNombre("CI22NOMBRE") & _
            " " & rsNombre("CI22PRIAPEL") & " " & rsNombre("CI22SEGAPEL") & _
            Chr(9) & rsNombre("CI21CODPERSONA")
      Me.grdPersonas.AddItem Linea
      rsNombre.MoveNext
    Wend
  Else
    grdPersonas.RemoveAll
    MsgBox "No se han encontrado resultados para la b�squeda solicitada", vbExclamation + vbOKOnly, "Atenci�n"
  End If



End Sub


Public Function pNuevoResponsable() As String
    Load frmNuevoResponsable
    'Set frmSeleccion.varFact = objFactura
      frmNuevoResponsable.Show (vbModal)
    pNuevoResponsable = Responsable
    Unload frmNuevoResponsable
End Function



Private Sub grdPersonas_DblClick()
  'Asingamos a responsable todos los valores necesarios del nuevo responsable.
  Responsable = grdPersonas.Columns(1).Value & Chr(9) & grdPersonas.Columns(2).Value & Chr(9) & "F"
  Unload Me
End Sub

