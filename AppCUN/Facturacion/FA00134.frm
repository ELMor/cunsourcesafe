VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_CierreCaja 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Proceso de cierre de caja"
   ClientHeight    =   3180
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   4035
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   990
      TabIndex        =   6
      Top             =   2700
      Width           =   1950
   End
   Begin VB.CommandButton cmdListadoAnt 
      Caption         =   "Listado de cierre de caja"
      Height          =   555
      Left            =   180
      TabIndex        =   5
      Top             =   2025
      Width           =   3750
   End
   Begin VB.CommandButton cmdCierre 
      Caption         =   "Cierre de caja"
      Height          =   600
      Left            =   180
      TabIndex        =   3
      Top             =   1305
      Width           =   3750
   End
   Begin VB.CommandButton cmdListado 
      Caption         =   "Listado de comprobaci�n previo al cierre de caja."
      Height          =   555
      Left            =   180
      TabIndex        =   2
      Top             =   630
      Width           =   3750
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCierre 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2115
      TabIndex        =   0
      Tag             =   "Fecha Inicio"
      Top             =   135
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   420
      Left            =   315
      TabIndex        =   4
      Top             =   2115
      Visible         =   0   'False
      Width           =   465
      _Version        =   196608
      _ExtentX        =   820
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Cierre:"
      Height          =   240
      Index           =   4
      Left            =   225
      TabIndex        =   1
      Top             =   180
      Width           =   1815
   End
End
Attribute VB_Name = "frm_CierreCaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ArrayAbonos() As String
Dim FechaListado As String
Dim FechaCierre As String

Sub ImprimirListado()
Dim cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer

  'Calculamos el n�mero de pagos que vamos a listar
  NoAbonos = UBound(ArrayAbonos, 2)
  Pagina = 1
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  vsPrinter.FontSize = 11
  vsPrinter.FontName = "Courier New"
  Texto = FechaListado & "                  PAG: " & Pagina 'N� de p�gina
  vsPrinter.Paragraph = Texto
  vsPrinter.Paragraph = ""
  Texto = "C.U.N.   ABONOS DIA..: " & FechaCierre 'Fecha en la que cerrramos la caja.
  vsPrinter.Paragraph = Texto
  vsPrinter.Paragraph = ""
  Texto = " ABONO  R.ECONO   FECHA    TIPO     IMPORTE     C IMP.P.COMPENSAR"
  vsPrinter.Paragraph = Texto
  Texto = "------- ------- ---------- ---- --------------- - ---------------"
  vsPrinter.Paragraph = Texto
  For cont = 1 To NoAbonos
    Texto = Right(Space(7) & Trim(ArrayAbonos(1, cont)), 7) & " " 'N� de Abono
    Texto = Texto & Right(Space(7) & Trim(ArrayAbonos(2, cont)), 7) & " " 'Responsable econ�mico
    Texto = Texto & Format(ArrayAbonos(3, cont), "DD/MM/YYYY") & " "  'Fecha de Cobro
    Texto = Texto & " " & ArrayAbonos(4, cont) & "  " 'Tipo de cobro.
    Texto = Texto & Right(Space(15) & Trim(ArrayAbonos(5, cont)), 15) & " " 'Importe cobrado
    Texto = Texto & Right(Space(1) & Trim(ArrayAbonos(6, cont)), 1) & " " 'Compensado (C/P/N)
    Texto = Texto & Right(Space(15) & Trim(ArrayAbonos(7, cont)), 15) 'Pendiente de compensasr
    vsPrinter.Paragraph = Texto
    If vsPrinter.CurrentY > 260 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      vsPrinter.NewPage
      vsPrinter.CurrentY = 10 * 54.6101086
      Pagina = Pagina + 1
      Texto = FechaListado & "                  PAG: " & Pagina
      vsPrinter.Paragraph = Texto
      Texto = "C.U.N.   ABONOS DIA..: " & FechaCierre
      vsPrinter.Paragraph = Texto
      Texto = " ABONO  R.ECON   FECHA    TIPO     IMPORTE     C IMP.P.COMPENSAR"
      vsPrinter.Paragraph = Texto
      Texto = "------- ------ ---------- ---- --------------- -----------------"
      vsPrinter.Paragraph = Texto
    End If
  Next
  vsPrinter.Action = 6
End Sub


Private Sub cmdCierre_Click()

Dim MiSqL As String
Dim MiRs As rdoResultset
  
  'Tras pedir confirmacion asignamos a la fecha cierre de caja la que nos han introducido.
  If MsgBox("�Desea cerrar la caja a fecha " & Me.SDCFechaCierre.Date & "?", vbQuestion + vbYesNo, "Aviso") = vbYes Then
    MiSqL = "UPDATE FA1700 SET FA17FECCIERRE = TO_DATE('" & SDCFechaCierre.Date & "','DD/MM/YYYY') " & _
            " Where FA17FECEFECTO < TO_DATE('" & SDCFechaCierre.Date & "','DD/MM/YYYY')" & _
            " AND FA17FECCIERRE = ''"
    objApp.rdoConnect.Execute MiSqL
  End If
End Sub

Private Sub cmdListado_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim RsCant As rdoResultset
Dim Compensado As Double
Dim cont As Integer

  FechaCierre = Format(Me.SDCFechaCierre.Date, "DD/MM/YYYY")
  'Seleccionamos de la tabla de pago aquellos que tienen fecha de efecto menor que la fecha seleccionada
  'y la fecha de cierre de caja nula.
  MiSqL = "Select * from FA1700 Where FA17FECEFECTO < TO_DATE('" & SDCFechaCierre.Date & "','DD/MM/YYYY')" & _
          " AND FA17FECCIERRE = ''"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    cont = 0
    While Not MiRs.EOF
      'Seleccionamos el total compensado por cada uno de los pagos
      MiSqL = "Select SUM(FA18IMPCOMP) As Compensado From FA1800 Where FA17CODPAGO = " & MiRs("FA17CODPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        If Not IsNull(RsCant("COMPENSADO")) Then
          Compensado = CDbl(RsCant("Compensado"))
        Else
          Compensado = 0
        End If
      End If
      cont = cont + 1
      ReDim Preserve ArrayAbonos(1 To 7, 1 To cont)
      ArrayAbonos(1, cont) = MiRs("FA17CODPAGO")
      ArrayAbonos(2, cont) = MiRs("CI21CODPERSONA")
      ArrayAbonos(3, cont) = Format(MiRs("FA17FECPAGO"), "DD/MM/YYYY")
      MiSqL = "SELECT FA20DESIG FROM FA2000 WHERE FA20CODTIPPAGO = " & MiRs("FA20CODTIPPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        ArrayAbonos(4, cont) = RsCant("FA20DESIG") 'BUSCAR INICIALES
      Else
        ArrayAbonos(4, cont) = ""
      End If
      ArrayAbonos(5, cont) = MiRs("FA17CANTIDAD")
      If Compensado = 0 Then
        ArrayAbonos(6, cont) = "N"
      ElseIf CDbl(MiRs("FA17CANTIDAD")) > Compensado Then
        ArrayAbonos(6, cont) = "P"
      ElseIf CDbl(MiRs("FA17CANTIDAD")) = Compensado Then
        ArrayAbonos(6, cont) = "C"
      End If
      ArrayAbonos(7, cont) = CDbl(MiRs("FA17CANTIDAD")) - Compensado
      MiRs.MoveNext
      
    Wend
    Call ImprimirListado
  Else
    MsgBox "No hay abonos pendientes de cierre a fecha " & Me.SDCFechaCierre.Date, vbInformation + vbOKOnly, "Aviso"
  End If
End Sub

Private Sub cmdListadoAnt_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim RsCant As rdoResultset
Dim Compensado As Double
Dim cont As Integer

  FechaCierre = Format(Me.SDCFechaCierre.Date, "DD/MM/YYYY")
  'Seleccionamos de la tabla de pago aquellos que tienen fecha de efecto menor que la fecha seleccionada
  'y la fecha de cierre de caja nula.
  MiSqL = "Select * from FA1700 Where FA17FECCIERRE = TO_DATE('" & SDCFechaCierre.Date & "','DD/MM/YYYY')"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    cont = 0
    While Not MiRs.EOF
      'Seleccionamos el total compensado por cada uno de los pagos
      MiSqL = "Select SUM(FA18IMPCOMP) As Compensado From FA1800 Where FA17CODPAGO = " & MiRs("FA17CODPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        If Not IsNull(RsCant("COMPENSADO")) Then
          Compensado = CDbl(RsCant("Compensado"))
        Else
          Compensado = 0
        End If
      End If
      cont = cont + 1
      ReDim Preserve ArrayAbonos(1 To 7, 1 To cont)
      ArrayAbonos(1, cont) = MiRs("FA17CODPAGO") 'C�digo del pago
      ArrayAbonos(2, cont) = MiRs("CI21CODPERSONA") 'C�digo del responsable econ�mico
      ArrayAbonos(3, cont) = Format(MiRs("FA17FECPAGO"), "DD/MM/YYYY") 'Fecha del pago
      MiSqL = "SELECT FA20DESIG FROM FA2000 WHERE FA20CODTIPPAGO = " & MiRs("FA20CODTIPPAGO")
      Set RsCant = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not RsCant.EOF Then
        ArrayAbonos(4, cont) = RsCant("FA20DESIG") 'Iniciales del tipo de pago
      Else
        ArrayAbonos(4, cont) = ""
      End If
      ArrayAbonos(5, cont) = MiRs("FA17CANTIDAD") 'Importe del cobro.
      If Compensado = 0 Then
        ArrayAbonos(6, cont) = "N" 'No compensado
      ElseIf CDbl(MiRs("FA17CANTIDAD")) > Compensado Then
        ArrayAbonos(6, cont) = "P" 'Parcialmente compensado
      ElseIf CDbl(MiRs("FA17CANTIDAD")) = Compensado Then
        ArrayAbonos(6, cont) = "C" 'Totalmente compensado
      End If
      ArrayAbonos(7, cont) = CDbl(MiRs("FA17CANTIDAD")) - Compensado 'Cantidad pendiente de compensaci�n
      MiRs.MoveNext
      
    Wend
    Call ImprimirListado
  End If

End Sub


Private Sub cmdSalir_Click()
  
  Unload Me
  
End Sub

Private Sub Form_Load()
Dim MiRsFecha
  
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    Me.SDCFechaCierre.Date = Format(DateAdd("d", -1, MiRsFecha(0)), "dd/mm/yyyy")
    FechaListado = Format(MiRsFecha(0), "dd mmmm yyyy hh:mm:ss")
  End If
End Sub



