VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Atributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public Codigo As Long
Public Suficiente As Boolean
Public Necesario As Boolean
Public Name As String
Public EsNodoFacturable As Boolean
Public FecInicio As String
Public FecFin As String
Public Opcional As Boolean
Public PrecioRef As Double
Public PrecioDia As Double
Public FranqUni As Boolean
Public FranqSuma As Boolean
Public PeriodoGara As Integer
Public Excluido As Boolean
Public LinOblig As Boolean
Public Descont As Boolean
Public FactOblig As Boolean
Public Descuento As Double
Public Ruta As String
Public EsCategoria As Boolean
Public EntidadResponsable As String
Public EsFinal As Boolean
Public Desglose As Boolean
Public Suplemento As Boolean
Public Descripcion As String
Public RutaRel As String
Public RelFijo As Double
Public RelPor As Double
Public OrdImp As Integer
Public CodAtrib As Long
Public TramAcum As Boolean
Public TramDias As Boolean
Public PlazoInter As Integer

