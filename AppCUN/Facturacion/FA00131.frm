VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_NuevoPago 
   Caption         =   "Introducci�n de un nuevo pago"
   ClientHeight    =   5865
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form2"
   ScaleHeight     =   5865
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   420
      Left            =   180
      TabIndex        =   29
      Top             =   5310
      Width           =   465
      _Version        =   196608
      _ExtentX        =   820
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin VB.CommandButton cmdAntiguo 
      Caption         =   "&Compensar por Antig�edad"
      Height          =   330
      Left            =   4185
      TabIndex        =   21
      Top             =   2295
      Visible         =   0   'False
      Width           =   2760
   End
   Begin VB.CheckBox chkIndAcuse 
      Alignment       =   1  'Right Justify
      Caption         =   "�Sin Acuse?"
      Height          =   195
      Left            =   6975
      TabIndex        =   10
      Top             =   2025
      Width           =   1275
   End
   Begin VB.ComboBox cboFormaPago 
      Height          =   315
      Left            =   2745
      TabIndex        =   6
      Top             =   1215
      Width           =   5730
   End
   Begin VB.TextBox txtInicPago 
      Height          =   285
      Left            =   2250
      TabIndex        =   5
      Top             =   1215
      Width           =   420
   End
   Begin VB.TextBox txtApunte 
      Height          =   285
      Left            =   5445
      TabIndex        =   9
      Top             =   1935
      Width           =   1275
   End
   Begin VB.TextBox txtObservaciones 
      Height          =   285
      Left            =   2250
      TabIndex        =   7
      Top             =   1575
      Width           =   6225
   End
   Begin VB.TextBox txtRemesa 
      Height          =   285
      Left            =   2250
      TabIndex        =   8
      Top             =   1935
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   " Facturas Pendientes de Compensar "
      Height          =   2490
      Left            =   135
      TabIndex        =   13
      Top             =   2745
      Width           =   8340
      Begin SSDataWidgets_B.SSDBGrid grdPagos 
         Height          =   2175
         Left            =   135
         TabIndex        =   14
         Top             =   225
         Width           =   8115
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   6
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "Compensar"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1879
         Columns(1).Caption=   "N� Factura"
         Columns(1).Name =   "NoFactura"
         Columns(1).Alignment=   1
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   7329
         Columns(2).Caption=   "Descripcion"
         Columns(2).Name =   "Descripcion"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1931
         Columns(3).Caption=   "Pdte. Comp."
         Columns(3).Name =   "PdteCompensar"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "Cantidad"
         Columns(4).Name =   "Cantidad"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   556
         Columns(5).Name =   "Entera"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   11
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         _ExtentX        =   14314
         _ExtentY        =   3836
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdCompensar 
      Caption         =   "C&ompensar"
      Height          =   330
      Left            =   4185
      TabIndex        =   11
      Top             =   2295
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   5220
      TabIndex        =   18
      Top             =   5355
      Width           =   1275
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   1620
      TabIndex        =   17
      Top             =   5355
      Width           =   1275
   End
   Begin VB.TextBox txtCantidad 
      Height          =   285
      Left            =   6390
      TabIndex        =   2
      Top             =   495
      Width           =   1275
   End
   Begin VB.TextBox txtResponsable 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2250
      TabIndex        =   0
      Tag             =   "txtResponsable"
      Top             =   135
      Width           =   5235
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFecha 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   1
      Tag             =   "Fecha Inicio"
      Top             =   495
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaEfecto 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   3
      Tag             =   "Fecha Inicio"
      Top             =   855
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCierre 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   6390
      TabIndex        =   4
      Tag             =   "Fecha Inicio"
      Top             =   855
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.TextBox txtCodFormaPago 
      Height          =   285
      Left            =   2250
      TabIndex        =   28
      Top             =   1215
      Width           =   420
   End
   Begin VB.TextBox txtNotaAbono 
      Height          =   285
      Left            =   2250
      TabIndex        =   30
      Top             =   1215
      Width           =   420
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Forma de Pago:"
      Height          =   240
      Index           =   8
      Left            =   360
      TabIndex        =   27
      Top             =   1260
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Apunte:"
      Height          =   240
      Index           =   7
      Left            =   3735
      TabIndex        =   26
      Top             =   1980
      Width           =   1635
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Observaciones"
      Height          =   240
      Index           =   6
      Left            =   360
      TabIndex        =   25
      Top             =   1620
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Remesa:"
      Height          =   240
      Index           =   5
      Left            =   360
      TabIndex        =   24
      Top             =   1980
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Cierre:"
      Height          =   240
      Index           =   4
      Left            =   4500
      TabIndex        =   23
      Top             =   900
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Efecto:"
      Height          =   240
      Index           =   3
      Left            =   360
      TabIndex        =   22
      Top             =   900
      Width           =   1815
   End
   Begin VB.Label lblPendiente 
      Caption         =   "Pendiente"
      Height          =   240
      Left            =   2790
      TabIndex        =   20
      Top             =   2340
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Pendiente de Asignar:"
      Height          =   240
      Left            =   360
      TabIndex        =   19
      Top             =   2340
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Pago:"
      Height          =   240
      Index           =   2
      Left            =   360
      TabIndex        =   16
      Top             =   540
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Entregada:"
      Height          =   240
      Index           =   1
      Left            =   4500
      TabIndex        =   15
      Top             =   540
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Responsable Econ�mico:"
      Height          =   240
      Index           =   0
      Left            =   360
      TabIndex        =   12
      Top             =   180
      Width           =   1815
   End
End
Attribute VB_Name = "frm_NuevoPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Nombre As String
Dim FecPago As String
Dim CodPersona As Long
Dim Conciertos As String
Dim PagoNuevo As Boolean
Dim ImportePago As Double
Dim CodPago As Long
Dim Pagado As Double
Dim Apuntes As Integer
Sub CargarFacturas(Persona As Long, Conciertos As String)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim MisqlSuma As String
Dim MiRsSuma As rdoResultset
Dim MiInsertar As String
Dim IntPospyc As Integer
Dim Conc As Integer
Dim Importe As Long

  Me.grdPagos.RemoveAll
  While Conciertos <> ""
    IntPospyc = InStr(1, Conciertos, ";")
    If IntPospyc <> 0 Then
      Conc = Left(Conciertos, IntPospyc - 1)
      Conciertos = Right(Conciertos, Len(Conciertos) - IntPospyc)
    ElseIf Conciertos <> "" Then
      Conc = Conciertos
      Conciertos = ""
    End If
    MiSqL = "Select FA04NUMFACT, FA04CODFACT,FA04CANTFACT,FA04DESCRIP, FA04INDCOMPENSA " & _
                   " From FA0400 Where FA09CODNODOCONC = " & Conc & " And CI21CODPERSONA = " & Persona & _
                   " And FA04INDCOMPENSA <> 2 ORDER BY FA04CODFACT"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      While Not MiRs.EOF
        MisqlSuma = "Select NVL(SUM(FA18IMPCOMP),0) AS IMPORTE FROM FA1800 Where FA04CODFACT = " & MiRs("FA04CODFACT")
        Set MiRsSuma = objApp.rdoConnect.OpenResultset(MisqlSuma, 3)
        If Not MiRsSuma.EOF Then
          Importe = MiRsSuma(0)
        End If
        MiInsertar = False & Chr(9) & MiRs("FA04NUMFACT") & Chr(9) & MiRs("FA04DESCRIP") & Chr(9) & _
                           Format(MiRs("FA04CANTFACT") - Importe, "###,###,##0.##") & Chr(9) & 0
        grdPagos.AddItem MiInsertar
        MiRs.MoveNext
      Wend
    End If
  Wend
  
End Sub

Sub ImprimirAbono(NoPago As Long)
Dim Texto As String
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Acumulado As Double
Dim Compensacion As String

  If Pagado = 0 Then
    Compensacion = "N NO COMPENSADO"
  ElseIf CDbl(txtCantidad) = Pagado Then
    Compensacion = "C COMPENSADO"
  ElseIf CDbl(txtCantidad) > Pagado Then
    Compensacion = "P PARCIALM. COMPENSADO"
  End If
  With vsPrinter1
    .Preview = False
    .Action = 3
    .FontSize = 11
    .FontName = "Courier New"
    .CurrentY = 660
    .FontBold = True
    '.FontUnderline = True
    .TextAlign = taCenterBaseline
    Texto = "A   B   0   N   O"
    .Paragraph = Texto
    Texto = "================="
    .Paragraph = Texto
    .Paragraph = ""
    '.FontBold = False
    '.FontUnderline = False
    .TextAlign = taLeftBaseline
    Texto = "NUMERO DE ABONO.......: " & Space(7 - Len(str(NoPago))) & NoPago
    .Paragraph = Texto
    Texto = "RESPONSABLE ECONOMICO.: " & Me.txtResponsable.Text
    .Paragraph = Texto
    Texto = "FORMA DE PAGO.........: " & Me.txtInicPago.Text & " " & Me.cboFormaPago.Text
    .Paragraph = Texto
    Texto = "CONCEPTO..............: " & Me.txtObservaciones
    .Paragraph = Texto
    Texto = "FECHA INTRODUCC. ABONO: " & Me.SDCFecha.Date
    .Paragraph = Texto
    Texto = "FECHA EFECTO CAJA.....: " & Me.SDCFechaEfecto.Date
    .Paragraph = Texto
    Texto = "IMPORTE...............: " & Space(12 - Len(Format(Me.txtCantidad.Text, "###,###,###"))) & Format(Me.txtCantidad.Text, "###,###,###")
    .Paragraph = Texto
    Texto = "COMPONSACION..........: " & Compensacion
    .Paragraph = Texto
    Texto = "N.APUNTES COMPENSADOS.: " & Space(12 - Len(Trim(str(Apuntes)))) & Apuntes
    .Paragraph = Texto
    .Paragraph = ""
    Texto = "TIPO FECHA       NUMERO NOMBRE                                  IMPORTE"
    .Paragraph = Texto
    Texto = "-----------------------------------------------------------------------"
    .Paragraph = Texto
    'Seleccionamos las facturas compensadas
    MiSqL = "Select * From FA1800 Where FA17CODPAGO = " & CodPago
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Acumulado = 0
      While Not MiRs.EOF
        Texto = "F   "
        Texto = Texto & " " & Format(MiRs("FA18FECCOMP"), "DD/MM/YYYY")
        Texto = Texto & Space(8 - Len(Trim(str(MiRs("FA04CODFACT"))))) & Trim(MiRs("FA04CODFACT")) & " "
        Texto = Texto & Left(txtResponsable & Space(37), 37)
        Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        .Paragraph = Texto
        Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
        MiRs.MoveNext
      Wend
      Texto = "                                                           ------------"
      .Paragraph = Texto
      Texto = "                                       TOTAL COMPENSADO...."
      Texto = Texto & Space(12 - Len(Format(Acumulado, "###,###,##0"))) & Format(Acumulado, "###,###,##0")
      .Paragraph = Texto
      Texto = "                                                           ============"
      .Paragraph = Texto
    End If
    .Action = 6
  End With
End Sub

Sub NuevoPago(idpersona As Long, Persona As String, fecha As String, Concierto As String, Anadir As Boolean, Importe As Double, Pago As Long)
  CodPersona = idpersona
  Nombre = UCase(Persona)
  FecPago = fecha
  Conciertos = Concierto
  PagoNuevo = Anadir
  ImportePago = Importe
  CodPago = Pago
  frm_NuevoPago.Show vbModal
  Set frm_NuevoPago = Nothing
End Sub


Private Sub cboFormaPago_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset

  If Trim(Me.cboFormaPago.Text) <> "" Then
    MiSqL = "Select * from FA2000 Where FA20DESC = '" & Me.cboFormaPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago.Text = MiRs("FA20DESIG")
      Me.txtCodFormaPago.Text = MiRs("FA20CODTIPPAGO")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    End If
  End If
End Sub

Private Sub cboFormaPago_DropDown()
Dim MiSqL As String
Dim MiRs As rdoResultset

  cboFormaPago.Clear
  MiSqL = "Select FA20DESC FROM FA2000"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      Me.cboFormaPago.AddItem MiRs(0)
      MiRs.MoveNext
    Wend
  End If
  
End Sub


Private Sub cboFormaPago_LostFocus()
Dim MiSqL As String
Dim MiRs As rdoResultset

  If Trim(Me.cboFormaPago.Text) <> "" Then
    MiSqL = "Select * from FA2000 Where FA20DESC = '" & Me.cboFormaPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago.Text = MiRs("FA20DESIG")
      Me.txtCodFormaPago.Text = MiRs("FA20CODTIPPAGO")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    End If
  End If
End Sub

Private Sub cmdAceptar_Click()
Dim NoPago As String
Dim MiSqL As String
Dim x As Integer

  On Error GoTo ErrorenGrabacion
  objApp.rdoConnect.BeginTrans
    If PagoNuevo = True Then
      NoPago = fNextClave("FA17CODPAGO", "FA1700")
      MiSqL = "INSERT INTO FA1700 VALUES("
      MiSqL = MiSqL & NoPago  'FA17CODPAGO
      MiSqL = MiSqL & "," & CodPersona 'CI21CODPERSONA
      MiSqL = MiSqL & ",TO_DATE('" & Me.SDCFecha & "','DD/MM/YYYY')" 'FA17FECPAGO
      MiSqL = MiSqL & "," & Me.txtCantidad 'FA17CANTIDAD
      If Not Me.SDCFechaCierre.Date = Null Then
        MiSqL = MiSqL & ",TO_DATE('" & Me.SDCFechaCierre & "','DD/MM/YYYY')" 'FA17FECCIERRE
      Else
        MiSqL = MiSqL & ",NULL"
      End If
      If Not Me.SDCFechaEfecto.Date = Null Then
        MiSqL = MiSqL & ",TO_DATE('" & Me.SDCFechaCierre & "','DD/MM/YYYY')" 'FA17FECEFECTO
      Else
        MiSqL = MiSqL & ",NULL"
      End If
      MiSqL = MiSqL & ",'" & Me.txtApunte & "'" 'FA17APUNTE
      MiSqL = MiSqL & ",'" & Me.txtRemesa & "'" 'FA17REMESA
      MiSqL = MiSqL & ",'" & Me.txtObservaciones & "'" 'FA17OBSERV
      MiSqL = MiSqL & "," & Me.txtCodFormaPago 'FA20CODTIPPAGO
      If Me.chkIndAcuse.Value = 1 Then
        MiSqL = MiSqL & ",1" 'FA17INDACUSE
      Else
        If Me.txtNotaAbono.Text = 1 Then
          MiSqL = MiSqL & ",1" 'Como se ha emitido la nota de abono no es necesario el acuse
        Else
          MiSqL = MiSqL & ",0"
        End If
      End If
      MiSqL = MiSqL & ",0"
      MiSqL = MiSqL & ")"
      CodPago = NoPago
      objApp.rdoConnect.Execute MiSqL
    End If
    Pagado = 0
    Apuntes = 0
    grdPagos.MoveFirst
    For x = 1 To grdPagos.Rows
      If grdPagos.Columns(4).Text <> 0 Then
        MiSqL = "INSERT INTO FA1800 VALUES (" & CodPago & "," & _
                       Right(grdPagos.Columns(1).Text, Len(grdPagos.Columns(1).Text) - 3) & _
                       ", TO_DATE('" & Me.SDCFecha.Date & "','DD/MM/YYYY')," & CDbl(Me.grdPagos.Columns(4).Text) & ")"
        objApp.rdoConnect.Execute MiSqL
        Pagado = Pagado + CDbl(grdPagos.Columns(4).Text)
        Apuntes = Apuntes + 1
        If grdPagos.Columns(4).Text = CDbl(grdPagos.Columns(3).Text) Then
          'Estado de la factura totalmente compensada
          MiSqL = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04NUMFACT = '" & grdPagos.Columns(1).Text & "'"
          objApp.rdoConnect.Execute MiSqL
        ElseIf grdPagos.Columns(4).Text < CDbl(grdPagos.Columns(3).Text) Then
          'Estado de la factura parcialmente compensada
          MiSqL = "Update FA0400 Set FA04INDCOMPENSA = 1 Where FA04NUMFACT = '" & grdPagos.Columns(1).Text & "'"
          objApp.rdoConnect.Execute MiSqL
        End If
      End If
      grdPagos.MoveNext
    Next
  objApp.rdoConnect.CommitTrans
  If PagoNuevo Then
    If Me.txtInicPago = "TR" And Me.txtRemesa = "" And Me.txtRemesa.Text = "" Then
    Else
      Call ImprimirAbono(CodPago)
    End If
  End If
  Unload Me
Exit Sub

ErrorenGrabacion:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en la grabaci�n de los pagos", vbCritical + vbOKOnly, "Atenci�n"
End Sub

Private Sub cmdAntiguo_Click()
Dim ContLineas As Integer
Dim Pendiente As Double
Dim x As Integer
  'Asignamos a contlineas el n�mero de l�neas que tiene el grid
  ContLineas = grdPagos.Rows
  'Asignamos a pendiente el valor que queda pendiente por asignar
  Pendiente = CDbl(lblPendiente)
  'Recorreremos cada una de las l�neas para poner el asignado a 0 e incrementar el pendiente
  grdPagos.MoveFirst
  For x = 1 To ContLineas
    If CDbl(grdPagos.Columns(4).Text) > 0 Then
      Pendiente = Pendiente + CDbl(grdPagos.Columns(4).Text)
      grdPagos.Columns(4).Text = 0
      grdPagos.Columns(5).Value = False
    End If
    grdPagos.MoveNext
  Next
  'Recorreremos las l�neas del grid e iremos asignando a las facturas la cantidad necesaria
  'para su compensaci�n hasta el momento de quedar una parcialmente compensada y el
  'pendiente sea 0
  grdPagos.MoveFirst
  For x = 1 To ContLineas
    If CDbl(grdPagos.Columns(3).Text) <= Pendiente Then
      grdPagos.Columns(4).Text = grdPagos.Columns(3).Text
      Pendiente = Pendiente - CDbl(grdPagos.Columns(3).Text)
      grdPagos.Columns(5).Value = True
    ElseIf CDbl(grdPagos.Columns(3).Text) > Pendiente Then
      grdPagos.Columns(4).Text = Format(Pendiente, "###,###,##0.##")
      Pendiente = 0
    End If
    If Pendiente = 0 Then
      Exit For
    End If
    grdPagos.MoveNext
  Next
  
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdCompensar_Click()
Dim respuesta As Integer

  If PagoNuevo = True Then
    If Trim(txtCantidad.Text) <> "" Then
      respuesta = MsgBox("El pago realizado es de " & Format(Me.txtCantidad.Text, "###,###,##0.###") & _
                                      Chr(10) & Chr(13) & "�Es esto correcto?", vbQuestion + vbYesNo, "Confirmaci�n del Pago")
      If respuesta = vbYes Then
        frm_NuevoPago.Height = 6300
        cmdCompensar.Visible = False
        Me.lblTitulo.Visible = True
        Me.lblPendiente.Visible = True
        Me.lblPendiente.Caption = Me.txtCantidad.Text
        Me.cmdAntiguo.Visible = True
        Call CargarFacturas(CodPersona, Conciertos)
      End If
    Else
      MsgBox "Debe introducir una cantidad", vbOKOnly + vbInformation, "Aviso"
      Me.txtCantidad.SetFocus
    End If
  Else
      frm_NuevoPago.Height = 6300
      cmdCompensar.Visible = False
      Me.lblTitulo.Visible = True
      Me.lblPendiente.Visible = True
      Me.lblPendiente.Caption = CDbl(txtCantidad.Text) - CalcularPendiente(CodPago)
      Me.cmdAntiguo.Visible = True
      Call CargarFacturas(CodPersona, Conciertos)
    End If
End Sub

Function CalcularPendiente(Pago As Long) As Double
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Acumulado As Double

  MiSqL = "Select FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & Pago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Acumulado = 0
    While Not MiRs.EOF
      Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
      MiRs.MoveNext
    Wend
  Else
    Acumulado = 0
  End If
  CalcularPendiente = Acumulado
End Function

Private Sub Form_Load()
Dim MiSqL As String
Dim MiRs As rdoResultset

  Me.txtResponsable.Text = Nombre
  Me.SDCFecha.Date = FecPago
  If PagoNuevo = True Then
    frm_NuevoPago.Height = 3100
    Me.Caption = "Introducci�n de un nuevo pago"
  Else
    Me.Caption = "Asignaci�n a facturas del remanente de un pago"
    MiSqL = "Select * From FA1700 Where FA17CODPAGO = " & CodPago
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.SDCFecha = Format(MiRs("FA17FECPAGO"), "dd/mm/yyyy")
      If Not IsNull(MiRs("FA17FECCIERRE")) Then
        Me.SDCFechaCierre.Date = Format(MiRs("FA17FECCIERRE"), "DD/MM/YYYY")
      End If
      If Not IsNull(MiRs("FA17FECEFECTO")) Then
        Me.SDCFechaEfecto.Date = Format(MiRs("FA17FECEFECTO"), "DD/MM/YYYY")
      End If
      Me.txtApunte = MiRs("FA17APUNTE") & ""
      Me.txtRemesa = MiRs("FA17REMESA") & ""
      Me.txtObservaciones = MiRs("FA17OBSERV") & ""
      Me.txtCodFormaPago = MiRs("FA20CODTIPPAGO")
      Call txtCodFormaPago_LostFocus
      If MiRs("FA17INDACUSE") = 1 Then
        Me.chkIndAcuse.Value = 0
      Else
        Me.chkIndAcuse.Value = 1
      End If
    End If
    Me.txtCantidad = ImportePago
    Me.txtCantidad.Locked = True
    Call cmdCompensar_Click
  End If
End Sub

Private Sub grdPagos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim x As Integer
Dim Pendiente As Double
  If ColIndex = 4 Then
    If grdPagos.Columns(4).Text = "" Then
      grdPagos.Columns(4).Text = 0
    End If
    'La primera comprobaci�n ser� la de que la cantidad a compensar no es mayor que la cantidad facturada
    If CDbl(grdPagos.Columns(4).Text) > CDbl(grdPagos.Columns(3).Text) Then
      MsgBox "La cantidad a compensar no puede ser mayor que la cantidad pendiente", vbOKOnly + vbCritical, "Atenci�n"
      grdPagos.Columns(4).Text = OldValue
    Else
      ' Comprobaremos que la cantidad que sumamos no supera a la que queda pendiente
      Pendiente = CDbl(lblPendiente) + OldValue
      If Pendiente - grdPagos.Columns(4).Text < 0 Then
        MsgBox "No se puede asignar esa cantidad ya que es mayor que la cantidad pendiente de asignaci�n", vbCritical + vbOKOnly, "Atenci�n"
        lblPendiente = Pendiente - OldValue
        grdPagos.Columns(4).Text = OldValue
      Else
        lblPendiente = Pendiente - grdPagos.Columns(4).Text
      End If
    End If
  ElseIf ColIndex = 5 Then
    
  End If
  
  
End Sub

Private Sub grdPagos_Click()
Dim Pendiente As Double

  If grdPagos.Col = 5 Then
    If grdPagos.Columns(5).Value = True Then
      Pendiente = CDbl(lblPendiente) + grdPagos.Columns(4).Text
      grdPagos.Columns(4).Text = 0
      lblPendiente = Pendiente
    ElseIf grdPagos.Columns(5).Value = False Then
      Pendiente = CDbl(lblPendiente) + grdPagos.Columns(4).Text
      If Pendiente >= grdPagos.Columns(3).Text Then
        grdPagos.Columns(4).Text = grdPagos.Columns(3).Text
        lblPendiente = Pendiente - grdPagos.Columns(4).Text
      Else
        MsgBox "No es posible compensar totalmente la factura, dado que la cantidad pendiente no es suficiente", vbCritical + vbOKOnly, "Atencion"
        grdPagos.Columns(5).Value = False
      End If
    End If
  End If
End Sub

Private Sub txtCodFormaPago_LostFocus()
Dim MiSqL As String
Dim MiRs As rdoResultset

  If Trim(txtCodFormaPago.Text) <> "" Then
    MiSqL = "Select * From FA2000 Where FA20CODTIPPAGO = " & Me.txtCodFormaPago.Text
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago = MiRs("FA20DESIG")
      Me.cboFormaPago.Text = MiRs("FA20DESC")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
    End If
  Else
    txtCodFormaPago.Text = ""
    cboFormaPago.Text = ""
  End If

End Sub


Private Sub txtInicPago_LostFocus()
Dim MiSqL As String
Dim MiRs As rdoResultset

  If Trim(txtInicPago.Text) <> "" Then
    MiSqL = "Select * From FA2000 Where FA20DESIG = '" & Me.txtInicPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago = MiRs("FA20DESIG")
      Me.cboFormaPago.Text = MiRs("FA20DESC")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If txtInicPago.Enabled = True Then
        txtInicPago.SetFocus
      End If
    End If
  Else
    txtCodFormaPago.Text = ""
    cboFormaPago.Text = ""
  End If
End Sub


