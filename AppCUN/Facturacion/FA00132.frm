VERSION 5.00
Begin VB.Form frm_ModCompensa 
   Caption         =   "Modificación de una compensación"
   ClientHeight    =   2250
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7170
   LinkTopic       =   "Form1"
   ScaleHeight     =   2250
   ScaleWidth      =   7170
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   2130
      Left            =   45
      TabIndex        =   2
      Top             =   45
      Width           =   5865
      Begin VB.TextBox txtNuevoImporte 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         MaxLength       =   12
         TabIndex        =   7
         Top             =   1665
         Width           =   1095
      End
      Begin VB.TextBox txtImpPendFactura 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   1170
         Width           =   1095
      End
      Begin VB.TextBox txtImpPenPago 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox txtPersona 
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   270
         Width           =   4425
      End
      Begin VB.TextBox txtCodFact 
         Height          =   285
         Left            =   4680
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   270
         Width           =   1095
      End
      Begin VB.TextBox txtCodPago 
         Height          =   285
         Left            =   3960
         MaxLength       =   5
         TabIndex        =   3
         Top             =   270
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Imp. compensar:"
         Height          =   195
         Index           =   4
         Left            =   90
         TabIndex        =   11
         Top             =   1710
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Pdte. Factura:"
         Height          =   195
         Index           =   3
         Left            =   90
         TabIndex        =   10
         Top             =   1260
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Pendiente pago:"
         Height          =   195
         Index           =   2
         Left            =   90
         TabIndex        =   9
         Top             =   810
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Persona:"
         Height          =   285
         Index           =   1
         Left            =   90
         TabIndex        =   8
         Top             =   315
         Width           =   1185
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   5985
      TabIndex        =   1
      Top             =   720
      Width           =   1140
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   5985
      TabIndex        =   0
      Top             =   180
      Width           =   1140
   End
End
Attribute VB_Name = "frm_ModCompensa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim CodigoPago As Long
Dim NombrePag As String
Dim CodFact As Long
Dim PdteFact As Double
Dim PtePago As Double
Sub pEditarCompensacion(CodPago As Long, CodFactura As Long, Nombre As String, PdtePago As Double, PdteFactura As Double)

    
    CodigoPago = CodPago
    NombrePag = Nombre
    PdteFact = PdteFactura
    PtePago = PdtePago
    NombrePag = Nombre
    CodFact = CodFactura
    
    Load frm_ModCompensa
    frm_ModCompensa.Show (vbModal)
    Unload frm_ModCompensa
End Sub

Private Sub cmdAceptar_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Resultado As Integer

  On Error GoTo ErrorenGrabacion
  'Confirmaremos que se quiere realizar la modificación
  Resultado = MsgBox("żDesea realizar el cambio en la compensación?", vbQuestion + vbYesNoCancel, "Confirmación del cambio")
  If Resultado = vbYes Then
    objApp.rdoConnect.BeginTrans
      'Grabaremos la modificación
      MiSqL = "Update FA1800 set FA18IMPCOMP = " & CDbl(Me.txtNuevoImporte.Text) & " " & _
                  "Where FA17CODPAGO = " & Me.txtCodPago.Text & " And FA04CODFACT = " & Me.txtCodFact
      objApp.rdoConnect.Execute MiSqL
      'Comprobaremos el nuevo estado de la factura
      If CDbl(Me.txtNuevoImporte) = CDbl(Me.txtImpPendFactura) Then
        MiSqL = "UPDATE FA0400 SET FA04INDCOMPENSA = 2 Where FA04CODFACT = " & Me.txtCodFact
        objApp.rdoConnect.Execute MiSqL
      ElseIf CDbl(Me.txtNuevoImporte) <> 0 Then
        MiSqL = "UPDATE FA0400 SET FA04INDCOMPENSA = 1 Where FA04CODFACT = " & Me.txtCodFact
        objApp.rdoConnect.Execute MiSqL
      Else
        MiSqL = "UPDATE FA0400 SET FA04INDCOMPENSA = 0 Where FA04CODFACT = " & Me.txtCodFact
        objApp.rdoConnect.Execute MiSqL
      End If
    objApp.rdoConnect.CommitTrans
    Unload Me
  ElseIf Resultado = vbNo Then
    'Seleccionamos el que no se quiere grabar y salimos de la pantalla sin hacer nada.
    Unload Me
  ElseIf Resultado = vbCancel Then
    'Al seleccionar cancelar no realizamos nada.
  End If

Exit Sub
ErrorenGrabacion:
  MsgBox "Se ha producido un error en la grabación. Compruebe el importe e inténtelo de nuevo", vbCritical + vbOKOnly, "Atención"
  objApp.rdoConnect.RollbackTrans
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()

  txtCodPago.Text = CodigoPago
  txtPersona = NombrePag
  txtImpPendFactura = PdteFact
  txtImpPenPago = PtePago
  txtNuevoImporte = 0
  txtCodFact = CodFact
End Sub

Private Sub txtNuevoImporte_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtNuevoImporte)
End Sub

Private Sub txtNuevoImporte_LostFocus()
  If Not IsNumeric(txtNuevoImporte.Text) Then
    MsgBox "El Importe a Compensar debe ser un dato numérico", vbOKOnly + vbCritical, "Atención"
    If txtNuevoImporte.Enabled = True Then
      txtNuevoImporte.SetFocus
    End If
    Exit Sub
  Else
    If CDbl(txtNuevoImporte) > CDbl(Me.txtImpPendFactura) Then
      MsgBox "El Importe a compensar no puede ser superior al importe pendiente de la factura", vbOKOnly + vbInformation, "Aviso"
      If txtNuevoImporte.Enabled = True Then
        txtNuevoImporte.SetFocus
      End If
      Exit Sub
    End If
    If CDbl(txtNuevoImporte) > CDbl(Me.txtImpPenPago) Then
      MsgBox "El Importe a Compensar no puede ser superior al importe pendiente de asignar del Pago", vbOKOnly + vbInformation, "Aviso"
      If txtNuevoImporte.Enabled = True Then
        txtNuevoImporte.SetFocus
      End If
      Exit Sub
    End If
  End If
End Sub


