VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_ImprimeHASS 
   Caption         =   "Form1"
   ClientHeight    =   6285
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8280
   LinkTopic       =   "Form1"
   ScaleHeight     =   6285
   ScaleWidth      =   8280
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   555
      Left            =   6660
      TabIndex        =   1
      Top             =   270
      Width           =   645
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   6090
      Left            =   585
      TabIndex        =   0
      Top             =   90
      Width           =   6000
      _Version        =   196608
      _ExtentX        =   10583
      _ExtentY        =   10742
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
End
Attribute VB_Name = "frm_ImprimeHASS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ConvX = 54.0809
Const ConvY = 54.6101086


Sub ImprimirDatos()
Dim Texto As String
  With vsPrinter
    .MarginLeft = 0
    .MarginTop = 0
    .Preview = True
    .PenStyle = psSolid
    
    .DrawLine 31 * ConvX, 19 * ConvY, 31 * ConvX, 24 * ConvY
    .FontName = "Courier New"
    .FontSize = 10
    .TextAlign = taLeftBaseline
    .CurrentY = 24 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "HOSPITALIZADO      422.427/2033"
    .CurrentY = 28 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "ABAD, SABANZA,ANGEL"
    .CurrentY = 32 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "20-13620784  AFILIADO    LO"
    .CurrentY = 36 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "17-11-1999   20:36"
    .CurrentY = 40 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "DPTO. CARDIOLOGIA"
    .CurrentY = 44 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "GONZALO DE BERCEO AVENIDA 54 1� C"
    .CurrentY = 48 * ConvY
    .CurrentX = 33 * ConvX
    .Text = "LOGRO�O (LA RIOJA)"
    .CurrentY = 33 * ConvY
    .CurrentX = 117 * ConvX
    .Text = Right(Space(12) & "350-2", 12)
    .CurrentY = 49 * ConvY
    .CurrentX = 117 * ConvX
    .Text = Right(Space(12) & "15:00", 12)
    .CurrentY = 45 * ConvY
    .CurrentX = 117 * ConvX
    .Text = Right(Space(12) & "22/11/1999", 12)
    'dibujamos todo el rayado
    .DrawRectangle 12 * ConvX, 94 * ConvY, 146 * ConvX, 146 * ConvY
    .FontSize = 10
    .TextAlign = taLeftBaseline
    .CurrentY = 107 * ConvY
    .CurrentX = 20 * ConvX
    .Text = "5 ESTANCIAS"
    .CurrentY = 107 * ConvY
    .CurrentX = 89 * ConvX
    .Text = Right(Space(12) & "25.667", 12) & " "
    .CurrentY = 107 * ConvY
    .CurrentX = 115 * ConvX
    .Text = Right(Space(15) & "128.385", 15) & " "
    .TextAlign = taLeftBaseline
    .CurrentY = 111 * ConvY
    .CurrentX = 20 * ConvX
    .Text = "1 STENT COR.NIR PRIMO 2.5X 14743-1625"
    .CurrentY = 111 * ConvY
    .CurrentX = 115 * ConvX
    .Text = Right(Space(15) & "224.700", 15) & " "
    .CurrentY = 149 * ConvY
    .CurrentX = 10 * ConvX
    .Text = "2611991133"
    .FontSize = 9
    .CurrentY = 155 * ConvY
    .CurrentX = 62 * ConvX
    .Text = "Pamplona, 22 de NOVIEMBRE de 1999"
  End With

End Sub

Sub ImprimirImpreso()
Dim Texto As String
  With vsPrinter
    .MarginLeft = 0
    .MarginTop = 0
    .Preview = False
    .PenStyle = psSolid
    .DrawLine 31 * ConvX, 19 * ConvY, 31 * ConvX, 24 * ConvY
    .DrawLine 116 * ConvX, 19 * ConvY, 116 * ConvX, 24 * ConvY
    .DrawLine 31 * ConvX, 19 * ConvY, 36 * ConvX, 19 * ConvY
    .DrawLine 111 * ConvX, 19 * ConvY, 116 * ConvX, 19 * ConvY
    .DrawLine 31 * ConvX, 47 * ConvY, 31 * ConvX, 52 * ConvY
    .DrawLine 116 * ConvX, 47 * ConvY, 116 * ConvX, 52 * ConvY
    .DrawLine 31 * ConvX, 52 * ConvY, 36 * ConvX, 52 * ConvY
    .DrawLine 111 * ConvX, 52 * ConvY, 116 * ConvX, 52 * ConvY
    .TextAlign = taCenterBaseline
    .FontSize = 13
    .FontName = "times New Roman"
    .CurrentY = 12 * ConvY
    .Text = "CLINICA UNIVERSITARIA"
    .Paragraph = ""
    .FontBold = False
    .FontSize = 7
    .TextAlign = taLeftBaseline
    .CurrentY = 26 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Concertado"
    .CurrentY = 30 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Asistencia a D."
    .CurrentY = 34 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "N.� Afiliado"
    .CurrentY = 38 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "INGRESO"
    .CurrentY = 42 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Dr. encargado"
    .CurrentY = 46 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Domicilio"
    .CurrentY = 37 * ConvY
    .CurrentX = 131 * ConvX
    .Text = "SALIDA"
    .CurrentY = 42 * ConvY
    .CurrentX = 130 * ConvX
    .Text = "D�a y hora"
    .CurrentY = 42 * ConvY
    .CurrentX = 129 * ConvX
    .FontSize = 8
    .CurrentY = 61 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Datos de la persona que firma la conformidad a esta factura:"
    .CurrentY = 65 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Nombre:"
    .CurrentY = 73 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Domicilio:"
    .CurrentY = 82 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "Vinculaci�n con el enfermo:"
    .FontBold = True
    .FontSize = 10
    .CurrentY = 91 * ConvY
    .CurrentX = 13 * ConvX
    .Text = "SERVICIOS PRESTADOS:"
    'dibujamos todo el rayado
    .BrushStyle = bsTransparent
    .PenStyle = psSolid
    .DrawRectangle 12 * ConvX, 94 * ConvY, 146 * ConvX, 146 * ConvY
    .DrawLine 12 * ConvX, 102 * ConvY, 146 * ConvX, 102 * ConvY
    .PenStyle = psDot
    .DrawLine 12 * ConvX, 108 * ConvY, 39 * ConvX, 108 * ConvY
    .DrawLine 52 * ConvX, 108 * ConvY, 146 * ConvX, 108 * ConvY
    .DrawLine 12 * ConvX, 112 * ConvY, 146 * ConvX, 112 * ConvY
    .DrawLine 12 * ConvX, 116 * ConvY, 146 * ConvX, 116 * ConvY
    .DrawLine 12 * ConvX, 120 * ConvY, 146 * ConvX, 120 * ConvY
    .DrawLine 12 * ConvX, 124 * ConvY, 146 * ConvX, 124 * ConvY
    .DrawLine 12 * ConvX, 128 * ConvY, 146 * ConvX, 128 * ConvY
    .DrawLine 12 * ConvX, 132 * ConvY, 146 * ConvX, 132 * ConvY
    .DrawLine 12 * ConvX, 137 * ConvY, 146 * ConvX, 137 * ConvY
    .PenStyle = psSolid
    .DrawLine 88 * ConvX, 138 * ConvY, 146 * ConvX, 138 * ConvY
    .DrawLine 88 * ConvX, 94 * ConvY, 88 * ConvX, 146 * ConvY
    .DrawLine 114 * ConvX, 94 * ConvY, 114 * ConvX, 146 * ConvY
    .FontBold = False
    .FontSize = 8
    .CurrentY = 99 * ConvY
    .CurrentX = 42 * ConvX
    .Text = "DETALLE"
    .CurrentY = 99 * ConvY
    .CurrentX = 93 * ConvX
    .Text = "Precio unitario"
    .CurrentY = 99 * ConvY
    .CurrentX = 125 * ConvX
    .Text = "IMPORTE"
    .Paragraph = ""
    .FontSize = 6
    .CurrentY = 108.5 * ConvY
    .CurrentX = 40 * ConvX
    .Text = "d�as estancia"
    .Paragraph = ""
    .FontSize = 12
    .CurrentY = 142 * ConvY
    .CurrentX = 64 * ConvX
    .Text = "TOTAL. . . . ."
    .Paragraph = ""
    .FontSize = 8
    .CurrentY = 161 * ConvY
    .CurrentX = 13 * ConvX
    .FontSize = 7
    .Text = "POR EL CENTRO CONCERTADO"
    .Paragraph = ""
    .CurrentY = 161 * ConvY
    .CurrentX = 104 * ConvX
    .Text = "CONFORME"
    .Paragraph = ""
    .FontSize = 6
    .CurrentY = 175 * ConvY
    .CurrentX = 93 * ConvX
    .Text = "(Firma del paciente o acompa�ante)"
    .PenStyle = psDot
    .DrawLine 72 * ConvX, 172 * ConvY, 148 * ConvX, 172 * ConvY
  End With
End Sub

Private Sub Command1_Click()
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  Call ImprimirImpreso
  Call ImprimirDatos
  vsPrinter.Action = 6
  
End Sub


