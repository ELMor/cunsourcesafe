VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_Impresion 
   Caption         =   "Vista Preliminar de la factura"
   ClientHeight    =   7905
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9825
   LinkTopic       =   "Form1"
   ScaleHeight     =   7905
   ScaleWidth      =   9825
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdPagAtras 
      BackColor       =   &H00C0C0C0&
      Caption         =   "P�gina &anterior"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   225
      TabIndex        =   11
      Top             =   2925
      Width           =   1095
   End
   Begin VB.CommandButton cmdPagAdelante 
      BackColor       =   &H00C0C0C0&
      Caption         =   "P�gina &siguiente"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1800
      TabIndex        =   10
      Top             =   2925
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2835
      TabIndex        =   8
      Top             =   45
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.ComboBox cmbZoom 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1650
      Width           =   1080
   End
   Begin VB.CommandButton cmdPrint 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Imprimir Facturas"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1800
      TabIndex        =   4
      Top             =   1305
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrientation 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   90
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1020
      Width           =   1275
   End
   Begin VB.ComboBox cmb_printers 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   90
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   345
      Width           =   2745
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   7800
      Left            =   3465
      TabIndex        =   0
      Top             =   90
      Visible         =   0   'False
      Width           =   6225
      _Version        =   196608
      _ExtentX        =   10980
      _ExtentY        =   13758
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      PreviewMode     =   1
   End
   Begin VB.PictureBox Picture1 
      Height          =   2025
      Left            =   45
      Picture         =   "FA00121.frx":0000
      ScaleHeight     =   1965
      ScaleWidth      =   2640
      TabIndex        =   9
      Top             =   4545
      Visible         =   0   'False
      Width           =   2700
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Zoom:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   1
      Left            =   105
      TabIndex        =   7
      Top             =   1395
      Width           =   555
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Orientaci�n de la p�gina:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   2
      Left            =   135
      TabIndex        =   6
      Top             =   765
      Width           =   2160
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Impresora a utilizar:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   0
      Left            =   135
      TabIndex        =   2
      Top             =   90
      Width           =   1935
   End
End
Attribute VB_Name = "frm_Impresion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Option Explicit

Dim MyPage%         'Keep the output view to be printed
Dim OldOrientation  'Don't mess with my printer settings



Const ConvX = 54.0809
Const ConvY = 54.6101086

Private Type LineaImpre
  Linea As String
  Cantidad As Double
End Type

'Variables que utilizaremos para gestionar pagina adelante y atr�s
Dim NumPaginas As Integer
Dim PagActual As Integer
Dim DescConcierto As String


Sub EncabezadoPagina(Factura As Integer)
Dim MiSqL As String
Dim rsBuscar As rdoResultset

    MiSqL = "Select FA09OBSFAC From FA0900 Where FA09CODNODOCONC = " & ArrayCabecera(Factura).codconcierto
    Set rsBuscar = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not rsBuscar.EOF Then
      DescConcierto = rsBuscar("FA09OBSFAC") & ""
    Else
      DescConcierto = ""
    End If
    With vsPrinter
        .TextColor = QBColor(0)
        '------------------------------------------------------------------------
        ' REALIZAMOS LA IMPRESI�N DE LA CABECERA Y EL LATERAL
        '------------------------------------------------------------------------
        'Imprimimos el escudo de la cl�nica.
        .X1 = 35 * ConvX
        .Y1 = 5 * ConvY
        .X2 = 59 * ConvX
        .Y2 = 35 * ConvY
        .Picture = Me.Picture1.Picture
                
        'Imprimimos el texto de debajo del escudo CLINICA...
        .FontName = "Arial"
        .CurrentX = 0 * ConvX
        .CurrentY = 35 * ConvY
        .MarginLeft = 0
        'Le asignamos un gran margen derecho y le decimos que lo centre
        .MarginRight = 120 * ConvX
        .FontSize = 14
        .TextAlign = taCenterBaseline
        .FontName = "Times New Roman"
        .Paragraph = "CLINICA UNIVERSITARIA"
        .SpaceAfter = 2 * ConvX
        .FontSize = 8
        .FontBold = False
        .CurrentY = 40 * ConvY
        .Paragraph = "FACULTAD DE MEDICINA"
        .CurrentY = 43 * ConvY
        .Paragraph = "UNIVERSIDAD DE NAVARRA"
        .DrawLine 39 * ConvX, 45 * ConvY, 49 * ConvX, 45 * ConvY
        .CurrentY = 49 * ConvY
        .FontName = "Arial"
        .FontBold = True
        .Paragraph = "ADMINISTRACION"
        .FontBold = False
        .TextAlign = taLeftBaseline
        .MarginRight = 0
        
        'Imprimimos la direcci�n y los tel�fonos (Parte derecha del encabezado)
        .CurrentX = 140 * ConvX
        .FontSize = 8
        .CurrentY = 20 * ConvY
        .Paragraph = "Avda. P�o XII, 36"
        .CurrentX = 140 * ConvX
        .CurrentY = 23 * ConvY
        .Paragraph = "Apartado, 4209"
        .CurrentX = 140 * ConvX
        .CurrentY = 29 * ConvY
        .Paragraph = "Tel�fonos:"
        .CurrentX = 140 * ConvX
        .CurrentY = 32 * ConvY
        .Paragraph = "Centralita 948.25.54.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 35 * ConvY
        .Paragraph = "Administraci�n 948.29.63.94"
        .CurrentX = 140 * ConvX
        .CurrentY = 41 * ConvY
        .Paragraph = "Fax: 948.29.65.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 44 * ConvY
        .Paragraph = "C.I.F.: Q.3168001 J"
        .CurrentX = 140 * ConvX
        .CurrentY = 47 * ConvY
        .Paragraph = "31080 PAMPLONA"
        .CurrentX = 140 * ConvX
        .CurrentY = 50 * ConvY
        .Paragraph = "(ESPA�A)"
        
        'Imprimimos el lateral de la p�gina
        .FontName = "Times New Roman"
        .TextAngle = 900 'Escribimos en vertical.
        .FontSize = 10
        .CurrentY = 180 * ConvY
        .CurrentX = 5 * ConvX
        .Paragraph = "Al abonar este importe rogamos haga referencia al n�mero de factura."
        .CurrentY = 180 * ConvY
        .CurrentX = 8 * ConvX + 30
        .Text = "Cuentas corrientes:"
        .FontBold = True
        .CurrentY = 152 * ConvY
        .CurrentX = 8 * ConvX + 30
        .Text = "Central Hispano N� 0049.2729.54.2214255346"
        .CurrentY = 152 * ConvY
        .CurrentX = 11 * ConvX + 60
        .Text = "BBV N� 0182.5912.71.0010288974"
        .BrushStyle = 1
        .DrawRectangle 2 * ConvX, 57 * 70.6101086, 13 * ConvX, 187 * ConvY 'Lo recuadramos.
        .FontBold = False
        .TextAngle = 0 'Anulamos la escritura vertical.
        
        'Imprimimos los datos del paciente y de la atenci�n.
        .CurrentY = 60 * ConvY
        .CurrentX = 35 * ConvX
        .MarginLeft = 35 * ConvX
        .MarginRight = 10 * ConvX
        .FontSize = 11
        '.FontBold = True
        .FontName = "Courier New"
        .Paragraph = UCase(ArrayCabecera(Factura).Tratamiento & " " & ArrayCabecera(Factura).NomPaciente)
        .Paragraph = UCase(ArrayCabecera(Factura).DNI)
        .Paragraph = ""
       ' .FontBold = False
        .Paragraph = UCase(ArrayCabecera(Factura).Descrip)
        .Paragraph = ""
        
      End With
      
End Sub


Sub NuevaPagina(Factura As Integer)
  Call PiePagina(Factura)
  vsPrinter.NewPage
  Call EncabezadoPagina(Factura)
  vsPrinter.MarginRight = 12 * ConvX
  vsPrinter.FontName = "Arial"
End Sub

Sub PiePagina(Factura As Integer)
    
    With vsPrinter
        .TextColor = QBColor(0)
        '--------------------------------------------------------
        ' IMPRIMIMOS LOS DATOS REFERENTES AL PIE
        '--------------------------------------------------------
        'Primera rejilla
        
        .FontSize = 12
        .CurrentX = 19 * ConvX
        .CurrentY = 250 * ConvY
        .Text = ArrayCabecera(Factura).NumHistoria & "-" & ArrayCabecera(Factura).NumCaso
        'N�mero de factura
        .CurrentX = 30 * ConvX
        .CurrentY = 263 * ConvY
        .Text = ArrayCabecera(Factura).NFactura
        'Fecha
        .CurrentX = 30 * ConvX
        .CurrentY = 275 * ConvY
        .Text = ArrayCabecera(Factura).Fecha
        
        'Datos del Paciente.
        .CurrentX = 100 * ConvX
        .CurrentY = 247 * ConvY
        .Paragraph = ArrayCabecera(Factura).Tratamiento
        .CurrentX = 100 * ConvX
        .Paragraph = ArrayCabecera(Factura).NomEntidad
        .CurrentX = 100 * ConvX
        .Paragraph = ArrayCabecera(Factura).Direccion
        .CurrentX = .CurrentX + 100
        .FontUnderline = True
        .CurrentX = 100 * ConvX
        .Paragraph = ArrayCabecera(Factura).CPPoblac
        .FontBold = False
        .FontUnderline = False
        'Imprimimos la rejilla que contiene los datos
        .BrushStyle = 1
        .FontSize = 8
        .CurrentX = 17 * ConvX
        .CurrentY = 256 * ConvY
        .Text = "N� FACTURA"
        .CurrentX = 17 * ConvX
        .CurrentY = 268 * ConvY
        .Text = "FECHA"
        'Definimos las l�neas
        .DrawLine 92 * ConvX, 241 * ConvY, 92 * ConvX, 277 * ConvY
        .DrawLine 16 * ConvX, 253 * ConvY, 92 * ConvX, 253 * ConvY
        .DrawLine 16 * ConvX, 265 * ConvY, 92 * ConvX, 265 * ConvY
        .DrawRectangle 16 * ConvX, 241 * ConvY, 196 * ConvX, 277 * ConvY, 200, 200
        .CurrentX = 16 * ConvX
        .CurrentY = 282 * ConvY
        .MarginLeft = 16 * ConvX
        .Paragraph = UCase(DescConcierto)
    End With
End Sub

Private Sub cmb_printers_Click()
        
        Dim s$
        
        vsPrinter.Device = cmb_printers.List(cmb_printers.ListIndex)
        
        '------------------------------------------------------
        ' Show selected printer attributes
        '------------------------------------------------------
        s = "DPI: " & str$(vsPrinter.DPI) & Chr(13)
        s = s & "Port: :" & vsPrinter.Port & Chr(13)
        s = s & "Driver: :" & vsPrinter.Driver

       ' lStatus = s

End Sub


Private Sub cmbOrientation_Click()
  
  MousePointer = 11
  
  vsPrinter.Orientation = cmbOrientation.ListIndex
  cmbZoom_click
  If MyPage >= 0 Then Command1_Click

  MousePointer = 0

End Sub

Private Sub cmbZoom_click()
  
  vsPrinter.Visible = False
  MousePointer = 11
    
  'Change the screen size to zoom value
  vsPrinter.Zoom = Val(cmbZoom)
  
  MousePointer = 0
  vsPrinter.Visible = True

End Sub

Private Sub cmdPagAdelante_Click()
  PagActual = PagActual + 1
  vsPrinter.PreviewPage = PagActual
  If PagActual = NumPaginas Then
    cmdPagAdelante.Enabled = False
  Else
    cmdPagAdelante.Enabled = True
  End If
  If PagActual = 1 Then
    cmdPagAtras.Enabled = False
  Else
    cmdPagAtras.Enabled = True
  End If
End Sub

Private Sub cmdPagAtras_Click()
  PagActual = PagActual - 1
  vsPrinter.PreviewPage = PagActual
  If PagActual = NumPaginas Then
    cmdPagAdelante.Enabled = False
  Else
    cmdPagAdelante.Enabled = True
  End If
  If PagActual = 1 Then
    cmdPagAtras.Enabled = False
  Else
    cmdPagAtras.Enabled = True
  End If
End Sub


Private Sub cmdPrint_Click()
  
  ' get ready to print
  cmdPrint.Enabled = False
  MousePointer = 11
  
  ' print to the printer
  vsPrinter.Preview = False
  Command1_Click
  vsPrinter.Preview = True

  ' all done
  cmdPrint.Enabled = True
  MousePointer = 0

End Sub

Private Sub Command1_Click()
Dim x As Integer
  ' remember page for use with Print command
  ' MyPage = Index%
  ' print pictures, stretch and align anywhere on the page
  ' we have a print job, so let's enable these guys
  cmbZoom.Enabled = True
  cmdPrint.Enabled = True
  cmbOrientation.Enabled = True

  ' start the print preview job
  vsPrinter.Action = 3 ' StartDoc
  If vsPrinter.error Then Beep: Exit Sub
  MousePointer = 12
  

  For x = 1 To UBound(ArrayCabecera)
    Call EncabezadoPagina(x)
    Call LineasFactura(x)
    Call PiePagina(x)
    If x <> UBound(ArrayCabecera) Then
      vsPrinter.NewPage
    End If
  Next
  ' all done
  vsPrinter.Action = 6 'End Document
  'vsPrinter.SaveDoc "c:\factura\" & ArrayCabecera(1).NumCaso & ".txt"
  MousePointer = 0
  
  NumPaginas = vsPrinter.PageCount
  If NumPaginas = 1 Then
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  ElseIf NumPaginas > 1 Then
    Me.cmdPagAdelante.Enabled = True
    Me.cmdPagAtras.Enabled = False
  Else
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  End If
  PagActual = 1
  
End Sub

Private Sub LineasFactura(Factura As Integer)
    Dim s$, fmt$
    Dim x, Y
    Dim LineaFact As String
    Dim ImporteFact As Double
    Dim ArrayFactura()  As LineaImpre
    Dim ContLineas
    Dim PosicionX As Integer
    Dim PosicionY As Integer
    Dim PosBlanco As Long
    Dim Cadena As String
    Dim Texto As String
    'Inicializo el array de las l�neas de factura y su contador
    ContLineas = 1
    
    With vsPrinter
        .MarginRight = 12 * ConvX
        '--------------------------------------------------------
        ' create table format
        '--------------------------------------------------------
        fmt = "=7085|>_1730;"                   '^Center > Right
        
        '--------------------------------------------------------
        ' create table string
        '--------------------------------------------------------
        .FontName = "Arial"
        .FontSize = 10
        .MarginLeft = 2000
        For x = 1 To UBound(ArrayLineas)
          If ArrayLineas(x).NFactura = Factura Then
            .CurrentX = 2000
            .TextAlign = taLeftBaseline
'            .Text = ArrayLineas(x).Descripcion & ": "
           Cadena = ArrayLineas(x).Descripcion & ": "
           PosicionX = .CurrentX
           While Cadena <> ""
             PosBlanco = InStr(1, Cadena, " ")
             If PosBlanco <> 0 Then
               Texto = Left(Cadena, PosBlanco)
               Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
               .TextColor = QBColor(15)
               .Text = Texto
               If .CurrentX <= 9085 Then
                 .CurrentX = PosicionX
                 .TextColor = QBColor(0)
                 .Text = Texto
                 PosicionX = .CurrentX
               Else
                 .CurrentX = 2000
                 .CurrentY = .CurrentY + 200
                 If vsPrinter.CurrentY > 220 * ConvY Then
                   Call NuevaPagina(Factura)
                   .MarginRight = 12 * ConvX
                   .FontSize = 10
                 End If
                 .TextColor = QBColor(0)
                 .Text = Texto
                 PosicionX = .CurrentX
               End If
             End If
           Wend
             
             PosicionX = .CurrentX
              For Y = 1 To UBound(arrayRNFs)
              If arrayRNFs(Y).NFactura = Factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And arrayRNFs(Y).Desglose = True Then
                'A�adimos la descripci�n de los distintos rnfs que corresponden a la l�nea
                'En el caso de que la cantidad sea superior a una unidad lo indicamos.
                'If arrayRNFs(Y).Cantidad > 1 Then
                '  Cadena = arrayRNFs(Y).Descripcion & "(" & arrayRNFs(Y).Cantidad & "), "
                'Else
                  Cadena = arrayRNFs(Y).Descripcion & ", "
                'End If
                'Cadena = Left(Cadena, Len(Cadena) - 2)
                Cadena = Cadena & " "
                While Cadena <> ""
                  PosBlanco = InStr(1, Cadena, " ")
                  If PosBlanco <> 0 Then
                    Texto = Left(Cadena, PosBlanco)
                    Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
                    .TextColor = QBColor(15)
                    .Text = Texto
                    If .CurrentX <= 9085 Then
                      .CurrentX = PosicionX
                      .TextColor = QBColor(0)
                      .Text = Texto
                      PosicionX = .CurrentX
                    Else
                      .CurrentX = 2000
                      .CurrentY = .CurrentY + 200
                      If vsPrinter.CurrentY > 220 * ConvY Then
                        Call NuevaPagina(Factura)
                        .MarginRight = 12 * ConvX
                        .FontSize = 10
                      End If
                      .TextColor = QBColor(0)
                      .Text = Texto
                      PosicionX = .CurrentX
                    End If
                  End If
                Wend
                'a�adimos los rnfs
              End If
            Next
          End If
          PosicionY = .CurrentY
          '.CurrentX = 217 * ConvX
          .CurrentY = .CurrentY - 200
          .TextAlign = taRightTop
          .Text = Format(ArrayLineas(x).Importe, "###,###,##0")
          .TextAlign = taLeftBaseline
          .CurrentY = .CurrentY + 200
          .CurrentX = PosicionX
          While .CurrentX <= 9085
            .Text = "."
          Wend
          .CurrentX = 2000
          .CurrentY = .CurrentY + 400
          If vsPrinter.CurrentY > 220 * ConvY Then
            Call NuevaPagina(Factura)
            .MarginRight = 12 * ConvX
            .FontSize = 10
          End If
        Next
                
 
        '.Paragraph = ""
        .SpaceAfter = 100
        .TextAlign = 0
   
        '-------------------------------------------------------
        ' Impresi�n desde la base de datos.
        '-------------------------------------------------------
        '-------------------------------------------------------
        ' Imprimimos el total.
        '-------------------------------------------------------
        x = .CurrentY
        x = x - 50
        'Imprimimos la l�nea que simboliza el final de la suma
        .DrawLine 170 * ConvX, x, 200 * ConvX, x
        .MarginRight = 12 * ConvX
        .CurrentX = 42 * ConvX
        .Text = " Exento de I.V.A."
        .CurrentX = 130 * ConvX
        .Text = "Total Pesetas"
        .TextAlign = taRightBaseline
        .CurrentX = 217 * ConvX
        .Paragraph = Format(ArrayCabecera(Factura).TotFactura, "###,###,###")
        x = .CurrentY
        x = x - 150
        'Imprimimos la doble l�nea que separa el total ptas de total euros
        .DrawLine 130 * ConvX, x, 200 * ConvX, x
        .DrawLine 130 * ConvX, x + 80, 200 * ConvX, x + 80
        .CurrentX = 203 * ConvX
        .Text = Format(ArrayCabecera(Factura).TotFactura / 166.386, "###,###.00")
        .TextAlign = taLeftBaseline
        .CurrentX = 130 * ConvX
        .Paragraph = "Total Euros"
        .FontBold = False
        .CurrentX = 16 * ConvX
        .CurrentY = 235 * ConvY
        .MarginLeft = 16 * ConvX
        .Paragraph = UCase(ArrayCabecera(Factura).Observac)
    End With

End Sub




Private Sub Form_Load()

    Dim i%, s$
    
    '------------------------------------------------------
    ' save orientation to clean up later
    '------------------------------------------------------
    OldOrientation = vsPrinter.Orientation
    MyPage = -1                         ' no current page
  
    '------------------------------------------------------
    ' preset zoom levels (you can choose your own)
    '------------------------------------------------------
    With cmbZoom
        .AddItem "30"
        .AddItem "50"
        .AddItem "75"
        .AddItem "100"
        .AddItem "120"
        .AddItem "200"
        .AddItem "300"
        .AddItem "400"
        
        .ListIndex = 0
    End With
    
    '------------------------------------------------------
    ' orientation (you cannot choose your own)
    '------------------------------------------------------
    With cmbOrientation
        .AddItem "Portrait"
        .AddItem "Landscape"
        .ListIndex = 0
    End With

    '------------------------------------------------------
    ' ready, set default page to 0
    '------------------------------------------------------
    MyPage = 0
  
    With vsPrinter
        
         .Preview = True         ' Show preview to screen
        .PreviewPage = 1        ' default preview page to first page
        .MarginBottom = 0
        '------------------------------------------------------
        ' show available devices
        ' and honor Windows default selection
        '------------------------------------------------------
        Dim curdev%
        For i = 0 To .NDevices - 1
          cmb_printers.AddItem .Devices(i)
          If .Devices(i) = .Device Then curdev = i
        Next
        cmb_printers.ListIndex = curdev
        
    End With
    
    Command1_Click
End Sub

Public Function pImpresion(objFactura As Factura, Prop As String) As Factura
Dim Posicion As Integer
    
    'Set varFact = objFactura
    'Buscamos el / para separar el n�mero de asistencia y la propuesta seleccionada
'    Posicion = InStr(1, Prop, "/")
'    If Posicion <> 0 Then
      'Propuesta seleccionada
'      Propuesta = Right(Prop, Len(Prop) - Posicion)
      'N�mero de asistencia.
'      NumAsist = Left(Prop, Posicion - 1)
'    End If
'    Load frm_Impresion
'      frm_Impresion.Show (vbModal)
            
    'Set objFactura = varFact
      Unload frm_Impresion


End Function


Private Sub Form_Unload(Cancel As Integer)
  
  '--------------------------------------------------------
  ' restore printer orientation
  '--------------------------------------------------------
  vsPrinter.Orientation = OldOrientation

End Sub

