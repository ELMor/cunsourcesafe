VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Begin VB.Form frmPrincipal 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   1620
      Top             =   585
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   2610
      Top             =   585
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   1
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA0100.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   2070
      Top             =   45
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mant 
      Caption         =   "Mantenimientos"
      Begin VB.Menu Conceptos 
         Caption         =   "Conceptos Agrupantes"
      End
      Begin VB.Menu Categorias 
         Caption         =   "Categorías"
      End
      Begin VB.Menu Nodos 
         Caption         =   "Nodos de Referencia"
      End
      Begin VB.Menu Conciertos 
         Caption         =   "Conciertos"
      End
      Begin VB.Menu mnuFacturación 
         Caption         =   "Proceso de Facturación"
      End
      Begin VB.Menu mnuAsignacionRNF 
         Caption         =   "Asignación RNF's"
      End
      Begin VB.Menu mnuFactura 
         Caption         =   "&Facturar"
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Private Sub Categorias_Click()
  
  frmA_Grupo.Show (vbModal)
  Set frmA_Grupo = Nothing
  
End Sub

Private Sub Conceptos_Click()
  
  frmA_Conceptos.Show (vbModal)
  Set frmA_Conceptos = Nothing
  
End Sub

Private Sub Conciertos_Click()
  frmA_Conciertos.Show (vbModal)
  Set frmA_Conciertos = Nothing
End Sub

Private Sub Form_Activate()
    Dim entrada As Boolean
    Screen.MousePointer = vbDefault
    entrada = objPipe.PipeGet("entrada")
    If entrada = True Then
        Call objPipe.PipeSet("entrada", False)
        'frmG_Seccion.Show vbModal
'        Set frmG_Seccion = Nothing
        'If frmG_Seccion.SelectSession = False Then
'        If objPipe.PipeGet("SelectSession") = False Then
'            Unload Me
'        End If
    End If
End Sub

Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  
  'QUITAR ESTAS LINEAS CUANDO SE FUNCIONE CON SEGURIDAD
  'If App.PrevInstance Then End
  Screen.MousePointer = vbHourglass
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    'Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
    .strUserName = "CUN"
    .strPassword = "TUY"
    .strDataSource = "Oracle73"
    .intUserEnv = 1
    .blnAskPassword = False
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
    .blnUseRegistry = True
    
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotación"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "FACTU.")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")
  
  End With
    
  If Not objApp.CreateInfo Then
    Screen.MousePointer = vbDefault
    Call ExitApp
  End If
  
  rptPath = App.Path
  If Right(rptPath, 1) <> "\" Then rptPath = rptPath & "\"
  'rptPath = rptPath & "..\..\inf"
  rptPath = rptPath & "rpt"
  
  'entrada = True
  Call objPipe.PipeSet("entrada", True)
  objSecurity.strUser = "PRUEBA"
  Call objSecurity.RegSession

End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  Call ExitApp
End Sub


Private Sub mnuAsignacionRNF_Click()
  frmFactura.Show (vbModal)
  Set frmFactura = Nothing
End Sub

Private Sub mnuFactura_Click()
'  frm_EdicFactura.Show (vbModal)
'  Set frm_EdicFactura = Nothing
End Sub

Private Sub mnuFacturación_Click()
  Call pFacturacion
'  frmSeleccion.Show (vbModal)
'  Set frmSeleccion = Nothing
End Sub

Private Sub Nodos_Click()
  
  'frmNodos.Show (vbModal)
  'Set frmNodos = Nothing
  frmCategorias.Show (vbModal)
  Set frmCategorias = Nothing
End Sub
