VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmConsumos 
   Caption         =   "Consumos de farmacia"
   ClientHeight    =   3075
   ClientLeft      =   3210
   ClientTop       =   3975
   ClientWidth     =   5385
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   5385
   Begin VB.CommandButton Command1 
      Caption         =   "&Imprimir Consumos "
      Height          =   495
      Left            =   1440
      TabIndex        =   10
      Top             =   2280
      Width           =   2295
   End
   Begin vsViewLib.vsPrinter vsPrin 
      Height          =   375
      Left            =   600
      TabIndex        =   11
      Top             =   2400
      Visible         =   0   'False
      Width           =   1335
      _Version        =   196608
      _ExtentX        =   2355
      _ExtentY        =   661
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      TableSep        =   "|#"
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin VB.Frame Frame1 
      Height          =   1695
      Left            =   480
      TabIndex        =   1
      Top             =   480
      Width           =   4335
      Begin VB.TextBox txtHoraFin 
         Height          =   285
         Left            =   3120
         TabIndex        =   6
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtHora 
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   480
         Width           =   375
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecInicio 
         Height          =   255
         Left            =   960
         TabIndex        =   3
         Top             =   480
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483639
         ShowCentury     =   -1  'True
         Mask            =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecFin 
         Height          =   255
         Left            =   960
         TabIndex        =   7
         Top             =   960
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483639
         ShowCentury     =   -1  'True
         Mask            =   2
      End
      Begin VB.Label Label4 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "h"
         Height          =   255
         Left            =   3600
         TabIndex        =   8
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "h"
         Height          =   255
         Left            =   3600
         TabIndex        =   4
         Top             =   480
         Width           =   255
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir Consumos"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   2400
      Visible         =   0   'False
      Width           =   255
   End
End
Attribute VB_Name = "frmConsumos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim con As rdoConnection
Dim strAhora As String

Private Sub cmdImprimir_Click()
Dim STR$, Picis$
Dim rd As rdoResultset
Dim qy As rdoQuery

Dim strWhere As String
'Dim formula(1, 2)
If dtcFecInicio.Text = "" Or dtcFecFin = "" Or txtHora = "" Or txtHoraFin = "" Then
  MsgBox "Rellene todos los campos", vbCritical, ""
  Exit Sub
End If
If Not IsNumeric(txtHora) Or Not IsNumeric(txtHoraFin) Then
  MsgBox "Introduzca un campo num�rico en las horas", vbCritical, ""
  Exit Sub
End If
If txtHora > 23 Or txtHoraFin > 23 Then
  MsgBox "Introduzca un valor entre 0 y 23 en las horas", vbCritical, ""
  Exit Sub
End If
'strWhere = " TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
'"TREATMENTS.TREATMENTDBOID=FRP200.TREATMENTDBOID AND " & _
'"ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
'"ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
'"PATIENTS.PATIENTDBOID = ADMISSIONS.PATIENTDBOID AND " & _
'"ADMISSIONS.ADMISSIONDBOID = PICISDATA.ADMISSIONDBOID AND " & _
'"PICISDATA.PICISDATADBOID = ORDERS.PICISDATADBOID AND " & _
'"((FAMILIES.FAMILYBEHAVDBOID=184000000000003000000) AND " & _
'"LOCATIONS.LOCATIONDBOID=ENVIRONMENTLOCATION.LOCATIONDBOID AND " & _
'"ENVIRONMENTLOCATION.ENVIRONMENTDBOID=ENVIRONMENTS.ENVIRONMENTDBOID AND " & _
'"ENVIRONMENTS.PICISDATADBOID=PICISDATA.PICISDATADBOID AND " & _

strWhere = "FRP201J.DEPTDBOID IN (SELECT DEPTDBOID FROM LOCATIONS WHERE LOCATIONS.COMPUTERNAME= '" & objSecurity.strMachine & "') AND " & _
"FRP201J.VALIDATEDDATE >= TO_DATE('" & dtcFecInicio.Text & " " & txtHora.Text & "', 'DD/MM/YYYY HH24') AND " & _
"FRP201J.VALIDATEDDATE <= TO_DATE('" & dtcFecFin.Text & " " & txtHoraFin.Text & "', 'DD/MM/YYYY HH24') AND " & _
"((FRP201J.TASKSTATUSDBOID=170000000000001000000 AND " & _
"FRP201J.ORDERTYPEDBOID=169000000000002000000) OR " & _
"(FRP201J.TASKSTATUSDBOID=170000000000004000000 AND " & _
"FRP201J.ORDERTYPEDBOID=169000000000001000000))"
'STR = "SELECT DISTINCT ENVIRONMENTS.PICISDATADBOID " & _
'"FROM LOCATIONS, ENVIRONMENTLOCATION, ENVIRONMENTS " & _
'"WHERE " & _
'"LOCATIONS.LOCATIONDBOID = ENVIRONMENTLOCATION.LOCATIONDBOID AND " & _
'"ENVIRONMENTLOCATION.ENVIRONMENTDBOID = ENVIRONMENTS.ENVIRONMENTDBOID AND " & _
'"LOCATIONS.DEPTDBOID IN (SELECT DEPTDBOID FROM LOCATIONS WHERE LOCATIONS.COMPUTERNAME= ?) "
'Set qy = objApp.rdoConnect.CreateQuery("", STR)
'qy(0) = objSecurity.strMachine
'Set rd = qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'
'Do While Not rd.EOF
'  Picis = Picis & rd!PICISDATADBOID & ","
'  rd.MoveNext
'Loop
'Picis = Left(Picis, Len(Picis) - 1)
'strWhere = "FRP201J.PICISDATADBOID IN (" & Picis & ") AND " & _
'"FRP201J.VALIDATEDDATE >= TO_DATE('" & dtcFecInicio.Text & " " & txtHora.Text & "', 'DD/MM/YYYY HH24') AND " & _
'"FRP201J.VALIDATEDDATE <= TO_DATE('" & dtcFecFin.Text & " " & txtHoraFin.Text & "', 'DD/MM/YYYY HH24') AND " & _
'"((FRP201J.TASKSTATUSDBOID=170000000000001000000 AND " & _
'"FRP201J.ORDERTYPEDBOID=169000000000002000000) OR " & _
'"(FRP201J.TASKSTATUSDBOID=170000000000004000000 AND " & _
'"FRP201J.ORDERTYPEDBOID=169000000000001000000))"

Call Imprimir_API(strWhere, "consumofluidos.rpt") ', formula)
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Command1_Click()
Dim STR$, cama$, Nombre$, order$
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim rd1 As rdoResultset
Dim qy1 As rdoQuery
Dim strWhere As String
Dim pos%, f$, S$
Me.MousePointer = vbHourglass
If dtcFecInicio.Text = "" Or dtcFecFin = "" Or txtHora = "" Or txtHoraFin = "" Then
  MsgBox "Rellene todos los campos", vbCritical, ""
  Exit Sub
End If
If Not IsNumeric(txtHora) Or Not IsNumeric(txtHoraFin) Then
  MsgBox "Introduzca un campo num�rico en las horas", vbCritical, ""
  Exit Sub
End If
If txtHora > 23 Or txtHoraFin > 23 Then
  MsgBox "Introduzca un valor entre 0 y 23 en las horas", vbCritical, ""
  Exit Sub
End If
STR = "SELECT ENVIRONMENTS.PICISDATADBOID, " & _
"ENVIRONMENTLOCATION.STARTED, ENVIRONMENTLOCATION.ENDED, gcfn06(LOCATIONS.LOCATIONDESC) CAMA " & _
"From LOCATIONS, ENVIRONMENTLOCATION, ENVIRONMENTS " & _
"WHERE LOCATIONS.LOCATIONDBOID = ENVIRONMENTLOCATION.LOCATIONDBOID AND " & _
"ENVIRONMENTLOCATION.ENVIRONMENTDBOID = ENVIRONMENTS.ENVIRONMENTDBOID AND " & _
"LOCATIONS.DEPTDBOID IN (SELECT DEPTDBOID FROM LOCATIONS WHERE COMPUTERNAME=?) and " & _
"(" & _
  "((ENVIRONMENTLOCATION.STARTED BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24') AND " & _
  "TO_DATE(?, 'DD/MM/YYYY HH24')) " & _
  "AND " & _
    "(" & _
    "(ENVIRONMENTLOCATION.ENDED BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24') AND " & _
    "TO_DATE(?, 'DD/MM/YYYY HH24')) " & _
    "OR " & _
    "ENVIRONMENTLOCATION.ENDED >= TO_DATE(?, 'DD/MM/YYYY HH24') " & _
    "OR " & _
    "ENVIRONMENTLOCATION.ENDED IS NULL " & _
    ")" & _
 ") "
STR = STR & "OR " & _
  "(ENVIRONMENTLOCATION.STARTED <= TO_DATE(?, 'DD/MM/YYYY HH24') " & _
  "AND " & _
    "(" & _
    "(ENVIRONMENTLOCATION.ENDED BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24') AND " & _
    "TO_DATE(?, 'DD/MM/YYYY HH24')) " & _
    "OR " & _
    "ENVIRONMENTLOCATION.ENDED >= TO_DATE(?, 'DD/MM/YYYY HH24') " & _
    "OR " & _
    "ENVIRONMENTLOCATION.ENDED IS NULL " & _
    ")" & _
  ")" & _
")"
Set qy = objApp.rdoConnect.CreateQuery("", STR)
qy(0) = objSecurity.strMachine
qy(1) = dtcFecInicio.Text & " " & txtHora.Text
qy(2) = dtcFecFin.Text & " " & txtHoraFin.Text
qy(3) = dtcFecInicio.Text & " " & txtHora.Text
qy(4) = dtcFecFin.Text & " " & txtHoraFin.Text
qy(5) = dtcFecFin.Text & " " & txtHoraFin.Text
qy(6) = dtcFecInicio.Text & " " & txtHora.Text
qy(7) = dtcFecInicio.Text & " " & txtHora.Text
qy(8) = dtcFecFin.Text & " " & txtHoraFin.Text
qy(9) = dtcFecFin.Text & " " & txtHoraFin.Text
Set rd = qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
With vsPrin
  .Action = 3
  .TextAlign = taCenterBaseline
  .FontBold = True
  .FontSize = 16
  .FontUnderline = True
  .Paragraph = "COMSUMO DE FLUIDOS"
  .Paragraph = ""
  
  .FontSize = 8
  .TextAlign = taLeftBaseline
  .FontUnderline = False
End With

Do While Not rd.EOF
  STR = "SELECT /*+ RULE */ ORDERS.ORDERDESC, TASKS.VALIDATEDDATE, " & _
      "PATIENTS.PTID1, PATIENTS.NAME ||' '|| PATIENTS.LASTNAME||' '|| PATIENTS.MIDDLENAME NOMBRE " & _
      "From " & _
      "PICISDATA, ORDERS,TASKS, TREATMENTS, FAMILIES, " & _
      "FRP200 , ADMISSIONS, PATIENTS " & _
      "Where " & _
      "PICISDATA.PICISDATADBOID= ? AND " & _
      "PICISDATA.PICISDATADBOID = ORDERS.PICISDATADBOID AND " & _
      "ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
      "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
      "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
      "TREATMENTS.TREATMENTDBOID = FRP200.TREATMENTDBOID AND " & _
      "FAMILIES.FAMILYBEHAVDBOID=184000000000003000000 AND " & _
      "PICISDATA.ADMISSIONDBOID = ADMISSIONS.ADMISSIONDBOID AND " & _
      "ADMISSIONS.PATIENTDBOID = PATIENTS.PATIENTDBOID AND " & _
      "TASKS.VALIDATEDDATE BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24') AND " & _
      "TO_DATE(?, 'DD/MM/YYYY HH24') AND " & _
      "TASKS.VALIDATEDDATE  BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') AND " & _
      "TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') AND " & _
      "((TASKS.TASKSTATUSDBOID=170000000000001000000 AND " & _
      "ORDERS.ORDERTYPEDBOID=169000000000002000000) OR " & _
      "(TASKS.TASKSTATUSDBOID=170000000000004000000 AND " & _
      "ORDERS.ORDERTYPEDBOID=169000000000001000000)) " & _
      "ORDER BY PATIENTS.PTID1, NOMBRE"
      Set qy1 = objApp.rdoConnect.CreateQuery("", STR)
      qy1(0) = rd!PICISDATADBOID
      qy1(1) = dtcFecInicio.Text & " " & txtHora.Text
      qy1(2) = dtcFecFin.Text & " " & txtHoraFin.Text
      qy1(3) = Format(rd!STARTED, "DD/MM/YYYY HH:MM:SS")
      If IsNull(rd!ENDED) Then
        qy1(4) = Format("01/01/3000 12", "DD/MM/YYYY HH:MM:SS")
      Else
        qy1(4) = Format(rd!ENDED, "DD/MM/YYYY HH:MM:SS")
      End If
      Set rd1 = qy1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      Do While Not rd1.EOF
        pos = InStr(1, rd1!ORDERDESC, ". Tiempo inf.")
        order = Left(rd1!ORDERDESC, Len(rd1!ORDERDESC) - (Len(rd1!ORDERDESC) - pos) - 1)
        If cama <> rd!cama And Nombre <> rd1!Nombre Then
          vsPrin.MarginLeft = 720
          vsPrin.FontBold = True
          vsPrin.Paragraph = rd!cama & "   " & rd1!Nombre
          vsPrin.Paragraph = ""
          
          vsPrin.MarginLeft = 1000
          vsPrin.FontBold = False
          vsPrin.Font = 8
          f$ = "<8000|<3000#"
          S$ = order & "|" & Format(rd1!VALIDATEDDATE, "DD/MM/YYYY HH:MM")
        Else
          vsPrin.MarginLeft = 1000
          vsPrin.FontBold = False
          vsPrin.Font = 8
          f$ = "<8000|<3000#"
          S$ = order & "|" & Format(rd1!VALIDATEDDATE, "DD/MM/YYYY HH:MM")
        End If
        vsPrin.Table = f & S
        vsPrin.Paragraph = ""
        cama = rd!cama
        Nombre = rd1!Nombre
        rd1.MoveNext
      Loop
  rd.MoveNext
Loop
vsPrin.Action = 6 'End Document
DoEvents
vsPrin.PrintDoc  ' Se imprime
DoEvents
vsPrin.KillDoc
Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
   Dim RS As rdoResultset
   Dim Dia, Mes, A�o As String
   Dim FechaAhora As String
   Set RS = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  strAhora = RS(0)
  FechaAhora = Format(strAhora, "dd/mm/yyyy")
  dtcFecInicio.Date = DateAdd("d", -1, FechaAhora)
  txtHora = Format(strHora_Sistema, "HH")
  dtcFecFin.Date = FechaAhora
  txtHoraFin = Format(strHora_Sistema, "HH")
End Sub





