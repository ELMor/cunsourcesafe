VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmValEnfermeras 
   Caption         =   "Resumen de Valoraci�n"
   ClientHeight    =   8355
   ClientLeft      =   1740
   ClientTop       =   2070
   ClientWidth     =   11880
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8355
   ScaleWidth      =   11880
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdProtocolos 
      Caption         =   "&Suspender Protocolos"
      Height          =   375
      Left            =   7800
      TabIndex        =   17
      Top             =   1080
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   6855
      Left            =   0
      TabIndex        =   0
      Top             =   1440
      Width           =   11775
      Begin VsOcxLib.VideoSoftAwk awk 
         Left            =   240
         Top             =   2280
         _Version        =   327680
         _ExtentX        =   847
         _ExtentY        =   847
         _StockProps     =   0
         FS              =   " "
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbValoracion 
         Height          =   6495
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   11535
         _Version        =   196614
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         FieldSeparator  =   ";"
         Col.Count       =   0
         stylesets.count =   3
         stylesets(0).Name=   "MasMemo"
         stylesets(0).Picture=   "OM101.frx":0000
         stylesets(0).AlignmentPicture=   2
         stylesets(1).Name=   "MAS"
         stylesets(1).Picture=   "OM101.frx":031A
         stylesets(1).AlignmentPicture=   2
         stylesets(2).Name=   "Memo"
         stylesets(2).Picture=   "OM101.frx":0634
         stylesets(2).AlignmentPicture=   2
         AllowUpdate     =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         SelectByCell    =   -1  'True
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   1085
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20346
         _ExtentY        =   11456
         _StockProps     =   79
         ForeColor       =   16711680
      End
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Default         =   -1  'True
      Height          =   375
      Left            =   5280
      TabIndex        =   11
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Frame Frame2 
      Height          =   495
      Left            =   0
      TabIndex        =   7
      Top             =   960
      Width           =   4935
      Begin Threed.SSOption Option1 
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "Hoy"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Value           =   -1  'True
      End
      Begin Threed.SSOption Option1 
         Height          =   255
         Index           =   1
         Left            =   840
         TabIndex        =   9
         Top             =   120
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "2 �ltimos d�as"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Threed.SSOption Option1 
         Height          =   255
         Index           =   2
         Left            =   2160
         TabIndex        =   10
         Top             =   120
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "4 �ltimos d�as"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Threed.SSOption Option1 
         Height          =   255
         Index           =   3
         Left            =   3480
         TabIndex        =   12
         Top             =   120
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "7 �ltimos d�as"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Height          =   615
      Left            =   10800
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Salir"
      Top             =   120
      Width           =   615
   End
   Begin VB.Frame fraPaciente 
      Caption         =   "Paciente"
      Height          =   855
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   10455
      Begin VB.TextBox txtHistoria 
         Alignment       =   2  'Center
         BackColor       =   &H8000000B&
         DataField       =   "CI22NUMHISTORIA"
         Height          =   285
         Left            =   9240
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox txtCama 
         Alignment       =   2  'Center
         BackColor       =   &H8000000B&
         DataField       =   "AD15CODCAMA"
         Height          =   285
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox txtAsistencia 
         Alignment       =   2  'Center
         BackColor       =   &H8000000B&
         DataField       =   "AD01CODASISTENCI"
         Height          =   285
         Left            =   10200
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   360
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Historia"
         Height          =   255
         Left            =   8640
         TabIndex        =   16
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "Cama"
         Height          =   255
         Left            =   7080
         TabIndex        =   15
         Top             =   360
         Width           =   615
      End
      Begin VB.Label lblNombre 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1200
         TabIndex        =   4
         Top             =   360
         Width           =   75
      End
      Begin VB.Label Label2 
         Caption         =   "Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   10200
         TabIndex        =   3
         Top             =   120
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Image Image1 
         Height          =   615
         Left            =   240
         Top             =   240
         Width           =   615
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   240
      Top             =   7800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   36
      ImageHeight     =   37
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM101.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM101.frx":0FB0
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM101.frx":12CA
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmValEnfermeras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim rdmemo As rdoResultset
'Dim GrupoMemo As Integer
'Dim ColumnaMemo As Integer

Private Sub cmdConsultar_Click()
Me.MousePointer = vbHourglass
ssdbValoracion.Visible = False
ssdbValoracion.RemoveAll
ssdbValoracion.Groups.RemoveAll
Dim DiasMostrados%
If Option1(0).Value Then
  DiasMostrados = 0
ElseIf Option1(1).Value Then
  DiasMostrados = 1
ElseIf Option1(2).Value Then
  DiasMostrados = 3
ElseIf Option1(3).Value Then
  DiasMostrados = 6
End If
Call RellenarGrid(DiasMostrados)
ssdbValoracion.Visible = True
Me.MousePointer = vbDefault
End Sub

Private Sub cmdProtocolos_Click()
Dim vntAsis(1) As Variant
vntAsis(1) = codAsistencia     'Codigo Paciente
Call objSecurity.LaunchProcess("OM102", vntAsis)
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim STR$, strNombre$, DiasMostrados%
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim rdNombre As rdoResultset
Dim qyNombre As rdoQuery

Set Me.Icon = ImageList1.ListImages.Item(3).Picture
Set Image1.Picture = ImageList1.ListImages.Item(1).Picture
Set cmdSalir.Picture = ImageList1.ListImages.Item(2).Picture
'FRAME DE PERSONA
'codAsistencia = objPipe.PipeGet("PI_CodAsistencia")

strNombre = "SELECT CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, " & _
"CI2200.CI22SEGAPEL, CI2200.CI22NUMHISTORIA, GCFN06(AD1500.AD15CODCAMA) AS AD15CODCAMA " & _
"FROM AD1500, CI2200, AD0100 WHERE AD0100.AD01CODASISTENCI= ? AND " & _
"AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA AND " & _
"AD1500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI"
Set qyNombre = objApp.rdoConnect.CreateQuery("", strNombre)
    qyNombre(0) = codAsistencia
Set rdNombre = qyNombre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
lblNombre.Caption = rdNombre!ci22nombre & " " & rdNombre!CI22PRIAPEL & _
" " & rdNombre!CI22SEGAPEL
txtHistoria = rdNombre!CI22NUMHISTORIA
txtAsistencia = codAsistencia
txtCama = rdNombre!AD15CODCAMA
rdNombre.Close
qyNombre.Close
End Sub

Private Sub ssDBValoracion_RowLoaded(ByVal Bookmark As Variant)
Dim i%, J%
On Error Resume Next
If ssdbValoracion.Groups.COUNT > 1 Then
For i = 1 To ssdbValoracion.Groups.COUNT
  For J = 0 To ssdbValoracion.Groups(i).Columns.COUNT
      awk = ssdbValoracion.Groups(i).Columns(J).Text
      If awk.NF > 4 Then 'And (rdmemo.RowCount = 0 Or IsNull(rdmemo!ADDITIONALINFO)) Then
           ssdbValoracion.Groups(i).Columns(J).CellStyleSet "MAS", ssdbValoracion.Row
      End If
      If Err = 0 Then
      If InStr(1, ssdbValoracion.Groups(i).Columns(J).Text, "Comentario:") > 0 Then 'existe comentario
        If awk.NF > 4 Then 'And (rdmemo.RowCount = 0 Or IsNull(rdmemo!ADDITIONALINFO)) Then
            ssdbValoracion.Groups(i).Columns(J).CellStyleSet "MasMemo", ssdbValoracion.Row
        Else
            ssdbValoracion.Groups(i).Columns(J).CellStyleSet "Memo", ssdbValoracion.Row
        End If
      End If
      End If
      Err = 0
  Next
Next
End If
End Sub

Private Sub RellenarGrid(DiasMostrados As Integer)
Dim fecha As String, STR$, fechaINICIO$, strNombre$, strmemo$
Dim i%, COUNT%, J%, PosGrid%
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim rdHORA As rdoResultset
Dim qyHORA As rdoQuery
Dim rdT As rdoResultset
Dim qyT As rdoQuery
Dim Tratamiento As New Collection
Dim HoraTratamiento As New Collection
Dim HoraPrevia As String
Dim Columna As String
Dim rdNombre As rdoResultset
Dim qyNombre As rdoQuery
Dim qymemo As rdoQuery
Dim rdmemo As rdoResultset
Dim FamiliaAnterior$
awk.FS = Chr$(13)

STR = "SELECT AD0100.AD01CODASISTENCI, AD0100.AD01FECINICIO " & _
"FROM AD0100 " & _
"Where AD0100.AD01CODASISTENCI = ?"
Set qy = objApp.rdoConnect.CreateQuery("", STR)
  qy(0) = txtAsistencia
Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
ssdbValoracion.Groups.Add (0)
ssdbValoracion.Groups.Item(0).Caption = "Valoraciones abiertas"
ssdbValoracion.Groups.Item(0).Width = 3000
ssdbValoracion.Groups(0).Columns.Add (0)
ssdbValoracion.Groups(0).Columns(0).Caption = "Familia"
ssdbValoracion.Groups(0).Columns.Add (1)
ssdbValoracion.Groups(0).Columns(1).Caption = "Tratamientos"
ssdbValoracion.Groups(0).Columns(0).BackColor = &H80FFFF    '&HFFC0C0
ssdbValoracion.Groups(0).Columns(1).BackColor = &H80FFFF    '&H80FF80
fecha = strFecha_Sistema
If DateDiff("d", rd!AD01FECINICIO, fecha) < DiasMostrados Then
  fechaINICIO = Format(rd!AD01FECINICIO, "DD/MM/YYYY")
Else: fechaINICIO = DateAdd("d", -DiasMostrados, fecha)
End If

STR = "SELECT  /*+ RULE */ DISTINCT TREATMENTS.BRANDNAME, FAMILIES.FAMILYDESC, " & _
  "PICISDATA.PICISDATADBOID " & _
  "FROM TASKS, FAMILIES, TREATMENTS, ORDERS, PICISDATA,ADMISSIONS " & _
  "WHERE " & _
  "ORDERS.PICISDATADBOID = PICISDATA.PICISDATADBOID AND " & _
  "PICISDATA.ADMISSIONDBOID = ADMISSIONS.ADMISSIONDBOID AND " & _
  "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
  "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
  "ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
  "ORDERS.ORDERTYPEDBOID=169000000000004000000 AND ADMISSIONS.ADMID1=? AND " & _
  "TASKS.TODODATE >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
  "TASKS.TODODATE <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
  "ORDERS.ORDERSTATUSDBOID<>170000000000008000000 AND (ORDERS.LASTDATE>=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') OR ORDERS.LASTDATE IS NULL) " & _
  "ORDER BY FAMILYDESC ASC"
  Set qyT = objApp.rdoConnect.CreateQuery("", STR)
    qyT(0) = txtAsistencia
    qyT(1) = Format(fechaINICIO & " 00:00:00", "DD/MM/YYYY HH:Mm:SS")
    qyT(2) = Format(fecha & " 23:59:00", "DD/MM/YYYY HH:Mm:SS")
    qyT(3) = Format(fechaINICIO & " 00:00:00", "DD/MM/YYYY HH:Mm:SS")  'CANCEL
  Set rdT = qyT.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

  While Not rdT.EOF
    If FamiliaAnterior <> rdT!FAMILYDESC Then
      ssdbValoracion.AddItem rdT!FAMILYDESC & ";" & rdT!BRANDNAME
    Else
      ssdbValoracion.AddItem "" & ";" & rdT!BRANDNAME
    End If
    Tratamiento.Add Item:=PosGrid, Key:=CStr(rdT!BRANDNAME)
    PosGrid = PosGrid + 1
    FamiliaAnterior = rdT!FAMILYDESC
    rdT.MoveNext
  Wend
  If rdT.RowCount = 0 Then
    MsgBox "No hay valoraciones para este paciente en este intervalo de fechas", vbInformation, ""
    Exit Sub
  End If
  i = 1
rdT.MoveFirst 'LO MOVEMOS AL PRIMER REGISTRO para leer Picisdatadboid
'  str = "SELECT /*+ RULE */ DISTINCT TO_CHAR(TASKS.TODODATE,'DD/MM/YYYY') FECHA, " & _
'  "TO_CHAR(TASKS.TODODATE,'HH24') HORA,TREATMENTS.BRANDNAME, FAMILIES.FAMILYDESC, " & _
'  "ASSESSMENTITEMS.ASSESSMENTITEMDESC FROM FAMILIES,TREATMENTS, " & _
'  "ASSESSMENTITEMS, TASKASSESSMENT, " & _
'  "TASKS, ORDERS, PICISDATA,ADMISSIONS " & _
'  "WHERE ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
'  "ORDERS.PICISDATADBOID = PICISDATA.PICISDATADBOID AND " & _
'  "PICISDATA.ADMISSIONDBOID = ADMISSIONS.ADMISSIONDBOID AND " & _
'  "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
'  "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
'  "TASKS.TASKDBOID = TASKASSESSMENT.TASKDBOID AND " & _
'  "TASKASSESSMENT.ASSESSMENTITEMDBOID = ASSESSMENTITEMS.ASSESSMENTITEMDBOID " & _
'  "AND ORDERS.ORDERTYPEDBOID=? AND ADMISSIONS.ADMID1 = ? " & _
'  "AND TASKS.TASKSTATUSDBOID=? AND " & _
'  "TASKS.TODODATE >= TO_DATE(?,'DD/MM/YYYY') AND " & _
'  "TASKS.TODODATE <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
'  "ORDERS.ORDERSTATUSDBOID<>?  AND (ORDERS.LASTDATE > TO_DATE(?,'DD/MM/YYYY') OR ORDERS.LASTDATE IS NULL) " & _
'  "ORDER BY  " & _
'  "FAMILIES.FAMILYDESC ASC, TREATMENTS.BRANDNAME ASC, " & _
'  "TO_CHAR(TASKS.TODODATE,'DD/MM/YYYY') DESC,TO_CHAR(TASKS.TODODATE,'HH24') DESC"
' Set qyHORA = objApp.rdoConnect.CreateQuery("", str)
'    qyHORA(0) = "169000000000004000000"
'    qyHORA(1) = txtAsistencia
'    qyHORA(2) = "170000000000001000000" 'Done
'    qyHORA(3) = fechaINICIO
'    qyHORA(4) = strFecha_Sistema & " 23:59:00"
'    qyHORA(5) = "170000000000008000000" 'CANCEL
'    qyHORA(6) = fechaINICIO
'  Set rdHORA = qyHORA.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
' COUNT = 0
' Dim pos%, posAnterior%, h%, Numdias%, contadorDia%, n%
' Dim Comparar As New Collection
' Dim HoraAnt As String
' Dim FechaAnt As String
' Dim FamiliaAnt As String
' Dim TrataAnt As String
' Dim blnBuscar As Boolean
' Dim col
' i = 1
'  While Not rdHORA.EOF
'    For Each col In Comparar
'      If col = rdHORA!BRANDNAME Then
'        blnBuscar = True
'      End If
'    Next
'    If blnBuscar Then
'        pos = Tratamiento.Item(rdHORA!BRANDNAME)
'        ssdbValoracion.MoveFirst
'        ssdbValoracion.MoveRecords (pos)
'    Else
'      Tratamiento.Add Item:=PosGrid, Key:=CStr(rdHORA!BRANDNAME)
'      Comparar.Add Item:=CStr(rdHORA!BRANDNAME), Key:=CStr(PosGrid)
'      ssdbValoracion.AddItem rdHORA!FAMILYDESC & ";" & rdHORA!BRANDNAME
'      ssdbValoracion.MoveFirst
'      ssdbValoracion.MoveRecords (PosGrid)
'      PosGrid = PosGrid + 1
'    End If
'      '    pos = Tratamiento.Item(rdHORA!BRANDNAME)
''    ssdbValoracion.MoveFirst
''    ssdbValoracion.MoveRecords (pos)
'
'    If rdHORA!fecha <> FechaAnt Then
''      contadorDia = contadorDia + 1
''      Dias.Add Item:=contadorDia, Key:=CStr(rdHORA!fecha)
''      i = Dias.Item(rdHORA!fecha)
'        ssdbValoracion.Groups.Add (i)
'        ssdbValoracion.Groups.Item(i).Caption = Format(rdHORA!fecha, "DD/MM/YYYY")
'        COUNT = 0
'    Else
'      i = i - 1
'    End If
''    pos = Tratamiento.Item(rdHORA!BRANDNAME)
''    ssdbValoracion.MoveFirst
''    ssdbValoracion.MoveRecords (pos)
'
'    If (rdHORA!Hora & " " & rdHORA!fecha) = HoraPrevia And blnBuscar Then  'si es la misma casilla anterior se suma
'      COUNT = COUNT - 1
''      If pos = posAnterior Then
'        ssdbValoracion.Groups(i).Columns(COUNT).Text = _
'        ssdbValoracion.Groups(i).Columns(COUNT).Text & Chr$(13) & rdHORA!ASSESSMENTITEMDESC
''      Else
''        ssdbValoracion.Groups(i).Columns(COUNT).Text = rdHORA!ASSESSMENTITEMDESC
''      End If
'    Else ' si es disitnta casilla se crea una nueva columna
'      ssdbValoracion.Groups(i).Columns.Add (COUNT)
'      'Se mira si esa hora ya existe para no repetirla
'      Columna = rdHORA!Hora & "-"
'      If Len(rdHORA!Hora + 1) = 1 Then
'        Columna = Columna & "0" & (rdHORA!Hora + 1)
'      Else: Columna = Columna & (rdHORA!Hora + 1)
'      End If
'      ssdbValoracion.Groups(i).Columns(COUNT).Caption = Columna
'      ssdbValoracion.Groups(i).Columns(COUNT).Text = rdHORA!ASSESSMENTITEMDESC
'    End If
'    ssdbValoracion.Update
''    posAnterior = pos
'    FechaAnt = rdHORA!fecha
'    HoraPrevia = rdHORA!Hora & " " & rdHORA!fecha
'    TrataAnt = rdHORA!BRANDNAME
'    i = i + 1
'    COUNT = COUNT + 1
'    rdHORA.MoveNext
'    blnBuscar = False
'  Wend
i = 1
COUNT = 0
  STR = "SELECT /*+ RULE */ DISTINCT 1, TO_CHAR(TASKS.TODODATE,'HH24') TODODATE, " & _
  "TREATMENTS.BRANDNAME BRANDNAME, FAMILIES.FAMILYDESC, " & _
  "ASSESSMENTITEMS.ASSESSMENTITEMDESC FROM FAMILIES,TREATMENTS, " & _
  "ASSESSMENTITEMS, TASKASSESSMENT, " & _
  "TASKS, ORDERS " & _
  "WHERE ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
  "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
  "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
  "TASKS.TASKDBOID = TASKASSESSMENT.TASKDBOID AND " & _
  "TASKASSESSMENT.ASSESSMENTITEMDBOID = ASSESSMENTITEMS.ASSESSMENTITEMDBOID " & _
  "AND ORDERS.ORDERTYPEDBOID=169000000000004000000 AND ORDERS.PICISDATADBOID = ? " & _
  "AND TASKS.TASKSTATUSDBOID=170000000000001000000 " & _
  "AND TO_CHAR(TASKS.TODODATE,'DD/MM/YYYY')=? " & _
  "UNION " & _
  "SELECT /*+ RULE */ DISTINCT 2, TO_CHAR(TASKS.TODODATE,'HH24') TODODATE, " & _
  "TREATMENTS.BRANDNAME BRANDNAME, FAMILIES.FAMILYDESC, " & _
  "TASKDATA.ADDITIONALINFO FROM FAMILIES,TREATMENTS, " & _
  "TASKDATA, " & _
  "TASKS, ORDERS WHERE ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
  "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
  "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
  "TASKS.TASKDBOID = TASKDATA.TASKDBOID AND TASKDATA.TASKDATAINDEX=0 AND " & _
  "ORDERS.ORDERTYPEDBOID=169000000000004000000 AND ORDERS.PICISDATADBOID = ? AND TASKS.TASKSTATUSDBOID=170000000000001000000 " & _
  "AND TO_CHAR(TASKS.TODODATE,'DD/MM/YYYY')=? " & _
 "ORDER BY TODODATE DESC,BRANDNAME ASC, 1 ASC"
  
  Set qyHORA = objApp.rdoConnect.CreateQuery("", STR)
    qyHORA(0) = rdT!PICISDATADBOID
While DateDiff("d", fecha, fechaINICIO) <= 0
    qyHORA(1) = fecha
    qyHORA(2) = rdT!PICISDATADBOID
    qyHORA(3) = fecha
  Set rdHORA = qyHORA.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'  ssdbValoracion.Groups.Add (i)
'  ssdbValoracion.Groups.Item(i).Caption = fecha
 COUNT = 0
 Dim pos%, posAnterior%
  While Not rdHORA.EOF
    pos = Tratamiento.Item(rdHORA!BRANDNAME)
    ssdbValoracion.MoveFirst
    ssdbValoracion.MoveRecords (pos)
    If (rdHORA!TODODATE & " " & fecha) = HoraPrevia Then
      COUNT = COUNT - 1
      '++++
      i = i - 1
      '+++++
      If pos = posAnterior Then
        If rdHORA.rdoColumns(0) = 2 And Not IsNull(rdHORA!ASSESSMENTITEMDESC) And Not IsNumeric(rdHORA!ASSESSMENTITEMDESC) Then
          ssdbValoracion.Groups(i).Columns(COUNT).Text = _
          ssdbValoracion.Groups(i).Columns(COUNT).Text & Chr$(13) & "Comentario:" & rdHORA!ASSESSMENTITEMDESC
        ElseIf Not IsNull(rdHORA!ASSESSMENTITEMDESC) And Not IsNumeric(rdHORA!ASSESSMENTITEMDESC) Then
        ssdbValoracion.Groups(i).Columns(COUNT).Text = _
        ssdbValoracion.Groups(i).Columns(COUNT).Text & Chr$(13) & rdHORA!ASSESSMENTITEMDESC
        End If
      Else
        If rdHORA.rdoColumns(0) = 2 And Not IsNull(rdHORA!ASSESSMENTITEMDESC) And Not IsNumeric(rdHORA!ASSESSMENTITEMDESC) Then
          ssdbValoracion.Groups(i).Columns(COUNT).Text = "Comentario:" & rdHORA!ASSESSMENTITEMDESC
        ElseIf Not IsNull(rdHORA!ASSESSMENTITEMDESC) And Not IsNumeric(rdHORA!ASSESSMENTITEMDESC) Then
          ssdbValoracion.Groups(i).Columns(COUNT).Text = rdHORA!ASSESSMENTITEMDESC
        End If
      End If
    Else
    '+++++++
      ssdbValoracion.Groups.Add (i)
      ssdbValoracion.Groups.Item(i).Caption = fecha
      COUNT = 0
    '+++++++++
      ssdbValoracion.Groups(i).Columns.Add (COUNT)
      'Se mira si esa hora ya existe para no repetirla
      Columna = rdHORA!TODODATE & "-"
      If Len(rdHORA!TODODATE + 1) = 1 Then
        Columna = Columna & "0" & (rdHORA!TODODATE + 1)
      Else: Columna = Columna & (rdHORA!TODODATE + 1)
      End If
      ssdbValoracion.Groups(i).Columns(COUNT).Caption = Columna
      If rdHORA.rdoColumns(0) = 2 Then
        ssdbValoracion.Groups(i).Columns(COUNT).Text = "Comentario:" & rdHORA!ASSESSMENTITEMDESC
      Else: ssdbValoracion.Groups(i).Columns(COUNT).Text = rdHORA!ASSESSMENTITEMDESC
      End If
    End If
    ssdbValoracion.Update
    posAnterior = pos
    HoraPrevia = rdHORA!TODODATE & " " & fecha
    COUNT = COUNT + 1
    i = i + 1
    rdHORA.MoveNext
    Wend
'  For j = 0 To COUNT - 1
'    ssdbValoracion.Groups(i).Columns(j).Width = 2200
'    ssdbValoracion.Groups(i).Columns(j).Alignment = ssCaptionAlignmentCenter
'  Next
'  ssdbValoracion.Groups.Item(i).Width = 2200 * (COUNT)
  fecha = DateAdd("d", -1, CDate(fecha))
'  i = i + 1
Wend
rdT.Close
qyT.Close
rdHORA.Close
qyHORA.Close
rd.Close
qy.Close
i = 1
For i = 1 To ssdbValoracion.Groups.COUNT - 1
  ssdbValoracion.Groups(i).Width = 2500
  ssdbValoracion.Groups(i).Columns(0).Alignment = ssCaptionAlignmentCenter
Next
If ssdbValoracion.Groups.COUNT > 1 Then
  If ssdbValoracion.Groups(1).Columns.COUNT <> 0 Then 'PONE VIOLETA LA PRIMERA COLUMNA
                                           'PARA DISTINGUIRLA
  ssdbValoracion.Groups(1).Columns(0).BackColor = &HFFC0C0
  '   Exit For
  End If
ssdbValoracion.MoveFirst
ssdbValoracion.SplitterPos = 1
End If
End Sub




