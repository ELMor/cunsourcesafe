VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmOMEnfermeras 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hoja de Enfermer�a"
   ClientHeight    =   8595
   ClientLeft      =   1755
   ClientTop       =   1275
   ClientWidth     =   11880
   ForeColor       =   &H00008000&
   Icon            =   "OM100.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdReload 
      Height          =   615
      Left            =   10440
      Style           =   1  'Graphical
      TabIndex        =   149
      TabStop         =   0   'False
      ToolTipText     =   "Refrescar"
      Top             =   480
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Height          =   6975
      Left            =   120
      TabIndex        =   27
      Top             =   1440
      Width           =   11655
      Begin VB.Frame fraLista 
         Caption         =   "Sueros del d�a"
         Height          =   4095
         Left            =   5400
         TabIndex        =   151
         Top             =   2520
         Visible         =   0   'False
         Width           =   5775
         Begin SSDataWidgets_B.SSDBGrid ssdbSueros 
            Height          =   2895
            Left            =   240
            Negotiate       =   -1  'True
            TabIndex        =   153
            Top             =   360
            Width           =   5295
            _Version        =   196614
            DataMode        =   2
            GroupHeaders    =   0   'False
            FieldSeparator  =   ";"
            Col.Count       =   4
            stylesets.count =   1
            stylesets(0).Name=   "Bueno"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "OM100.frx":000C
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            SelectTypeCol   =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   185
            Columns.Count   =   4
            Columns(0).Width=   926
            Columns(0).Caption=   " "
            Columns(0).Name =   "(2)"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6297
            Columns(1).Caption=   "Sueros y Aditivos"
            Columns(1).Name =   "Sueros y Aditivos"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1402
            Columns(2).Caption=   "ml/h"
            Columns(2).Name =   "ml/h"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   106
            Columns(3).Caption=   " "
            Columns(3).Name =   "Column 0"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   9340
            _ExtentY        =   5106
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.CommandButton cmdQuitar 
            Height          =   495
            Left            =   2040
            Style           =   1  'Graphical
            TabIndex        =   152
            TabStop         =   0   'False
            ToolTipText     =   "Borrar el Suero"
            Top             =   3480
            Width           =   1695
         End
      End
      Begin VB.CommandButton cmdNuevo 
         Caption         =   "Nueva Perfusi�n"
         Height          =   855
         Left            =   6960
         TabIndex        =   2
         Top             =   1200
         Width           =   2655
      End
      Begin ComctlLib.TreeView tvwOM 
         Height          =   6555
         Left            =   120
         TabIndex        =   1
         ToolTipText     =   "Orden M�dica del Paciente"
         Top             =   240
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   11562
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame fraPrincipal 
         BackColor       =   &H8000000B&
         Caption         =   "Fluidos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6735
         Index           =   2
         Left            =   5040
         TabIndex        =   83
         Top             =   120
         Visible         =   0   'False
         Width           =   6495
         Begin VB.Frame fraFAditivos 
            Caption         =   "Aditivo 1"
            Height          =   1095
            Index           =   0
            Left            =   240
            TabIndex        =   96
            Top             =   1680
            Width           =   6015
            Begin VB.TextBox txtDosis 
               BackColor       =   &H8000000B&
               Height          =   285
               Index           =   2
               Left            =   4200
               Locked          =   -1  'True
               TabIndex        =   46
               Top             =   480
               Width           =   615
            End
            Begin VB.TextBox txtVolumen 
               BackColor       =   &H8000000B&
               Height          =   285
               Index           =   2
               Left            =   5745
               Locked          =   -1  'True
               TabIndex        =   97
               TabStop         =   0   'False
               Top             =   480
               Visible         =   0   'False
               Width           =   150
            End
            Begin SSDataWidgets_B.SSDBCombo dbMedicamento 
               Height          =   300
               Index           =   2
               Left            =   120
               TabIndex        =   44
               Top             =   480
               Width           =   3135
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               ForeColorEven   =   0
               RowHeight       =   423
               Columns.Count   =   6
               Columns(0).Width=   5821
               Columns(0).Caption=   "Medicamento"
               Columns(0).Name =   "Medicamento"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1508
               Columns(1).Caption=   "Dosis"
               Columns(1).Name =   "Dosis"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   1508
               Columns(2).Caption=   "Unidad"
               Columns(2).Name =   "Unidad"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   1323
               Columns(3).Caption=   "Forma"
               Columns(3).Name =   "Forma"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   1349
               Columns(4).Caption=   "Via"
               Columns(4).Name =   "Via"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   1296
               Columns(5).Caption=   "Cod."
               Columns(5).Name =   "Cod."
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               _ExtentX        =   5530
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   128
            End
            Begin SSDataWidgets_B.SSDBCombo dbFF 
               Height          =   300
               Index           =   2
               Left            =   3360
               TabIndex        =   45
               Top             =   480
               Width           =   735
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               ForeColorEven   =   0
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1931
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1296
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   0
            End
            Begin SSDataWidgets_B.SSDBCombo dbUM 
               Height          =   300
               Index           =   2
               Left            =   4920
               TabIndex        =   47
               Top             =   480
               Width           =   855
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1640
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   0
            End
            Begin VB.Label lblMedicamento 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000080&
               Height          =   300
               Index           =   2
               Left            =   120
               TabIndex        =   102
               Top             =   360
               Width           =   2835
            End
            Begin VB.Label Label7 
               Caption         =   "Vol. ml"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   50
               Left            =   5760
               TabIndex        =   101
               Top             =   240
               Visible         =   0   'False
               Width           =   135
            End
            Begin VB.Label Label7 
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   53
               Left            =   4920
               TabIndex        =   100
               Top             =   240
               Width           =   375
            End
            Begin VB.Label Label7 
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   4200
               TabIndex        =   99
               Top             =   240
               Width           =   615
            End
            Begin VB.Label Label7 
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   3360
               TabIndex        =   98
               Top             =   240
               Width           =   495
            End
         End
         Begin VB.Frame fraFAditivos 
            Caption         =   "Aditivo 2"
            Height          =   1095
            Index           =   1
            Left            =   240
            TabIndex        =   103
            Top             =   2880
            Width           =   6015
            Begin VB.TextBox txtVolumen2 
               BackColor       =   &H8000000B&
               Height          =   285
               Index           =   2
               Left            =   5745
               Locked          =   -1  'True
               TabIndex        =   104
               TabStop         =   0   'False
               Top             =   480
               Visible         =   0   'False
               Width           =   150
            End
            Begin VB.TextBox txtDosis2 
               BackColor       =   &H8000000B&
               Height          =   285
               Index           =   2
               Left            =   4200
               Locked          =   -1  'True
               TabIndex        =   50
               Top             =   480
               Width           =   615
            End
            Begin SSDataWidgets_B.SSDBCombo dbMedicamento2 
               Height          =   300
               Index           =   2
               Left            =   120
               TabIndex        =   48
               Top             =   480
               Width           =   3135
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               ForeColorEven   =   0
               RowHeight       =   423
               Columns.Count   =   6
               Columns(0).Width=   5821
               Columns(0).Caption=   "Medicamento"
               Columns(0).Name =   "Medicamento"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1508
               Columns(1).Caption=   "Dosis"
               Columns(1).Name =   "Dosis"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   1508
               Columns(2).Caption=   "Unidad"
               Columns(2).Name =   "Unidad"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   1323
               Columns(3).Caption=   "Forma"
               Columns(3).Name =   "Forma"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   1349
               Columns(4).Caption=   "Via"
               Columns(4).Name =   "Via"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   1296
               Columns(5).Caption=   "Cod."
               Columns(5).Name =   "Cod."
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               _ExtentX        =   5530
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   128
            End
            Begin SSDataWidgets_B.SSDBCombo dbFF2 
               Height          =   300
               Index           =   2
               Left            =   3360
               TabIndex        =   49
               Top             =   480
               Width           =   735
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1931
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1296
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   0
            End
            Begin SSDataWidgets_B.SSDBCombo dbUM2 
               Height          =   300
               Index           =   2
               Left            =   4920
               TabIndex        =   51
               Top             =   480
               Width           =   855
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               ForeColorEven   =   0
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1640
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   0
            End
            Begin VB.Label lblMedicamento2 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000080&
               Height          =   615
               Index           =   2
               Left            =   120
               TabIndex        =   110
               Top             =   360
               Width           =   2835
            End
            Begin VB.Label Label7 
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   63
               Left            =   3360
               TabIndex        =   109
               Top             =   240
               Width           =   495
            End
            Begin VB.Label Label7 
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   62
               Left            =   4200
               TabIndex        =   108
               Top             =   240
               Width           =   615
            End
            Begin VB.Label Label7 
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   61
               Left            =   4920
               TabIndex        =   107
               Top             =   240
               Width           =   735
            End
            Begin VB.Label Label7 
               Caption         =   "Vol. ml"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   49
               Left            =   5760
               TabIndex        =   106
               Top             =   240
               Visible         =   0   'False
               Width           =   135
            End
            Begin VB.Label lblMedicamento 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000080&
               Height          =   240
               Index           =   3
               Left            =   240
               TabIndex        =   105
               Top             =   240
               Width           =   75
            End
         End
         Begin VB.Frame frasolucion 
            Caption         =   "Fluido"
            Height          =   1335
            Index           =   2
            Left            =   240
            TabIndex        =   86
            Top             =   240
            Width           =   6015
            Begin VB.CommandButton cmdVel 
               Caption         =   "..."
               Height          =   255
               Left            =   4920
               TabIndex        =   150
               Top             =   840
               Width           =   255
            End
            Begin VB.TextBox txtCantidad 
               BackColor       =   &H8000000B&
               Height          =   375
               Index           =   2
               Left            =   5640
               Locked          =   -1  'True
               TabIndex        =   43
               Top             =   840
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtVolumenDil 
               BackColor       =   &H8000000B&
               Height          =   375
               Index           =   2
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   39
               Top             =   840
               Width           =   855
            End
            Begin VB.TextBox txtMinInf 
               BackColor       =   &H8000000B&
               Height          =   375
               Index           =   2
               Left            =   2400
               Locked          =   -1  'True
               TabIndex        =   41
               Top             =   840
               Width           =   495
            End
            Begin VB.TextBox txtHorainf 
               BackColor       =   &H8000000B&
               Height          =   375
               Index           =   2
               Left            =   1560
               Locked          =   -1  'True
               TabIndex        =   40
               Top             =   840
               Width           =   495
            End
            Begin VB.TextBox txtvelocidad 
               BackColor       =   &H00C0C0C0&
               Height          =   375
               Index           =   2
               Left            =   3360
               Locked          =   -1  'True
               TabIndex        =   42
               Top             =   840
               Width           =   855
            End
            Begin SSDataWidgets_B.SSDBCombo dbSolucion 
               Height          =   300
               Index           =   2
               Left            =   120
               TabIndex        =   29
               Top             =   240
               Width           =   5535
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               ForeColorEven   =   0
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   7990
               Columns(0).Caption=   "Descripci�n"
               Columns(0).Name =   "Descripci�n"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Vol (ml)"
               Columns(1).Name =   "Vol (ml)"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   1217
               Columns(2).Caption=   "Cod."
               Columns(2).Name =   "Cod."
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   9763
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   16711680
            End
            Begin VB.Label Label7 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   5640
               TabIndex        =   95
               Top             =   600
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.Label lblSolucion 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00C00000&
               Height          =   240
               Index           =   2
               Left            =   120
               TabIndex        =   94
               Top             =   240
               Width           =   195
            End
            Begin VB.Label Label7 
               Caption         =   "Volumen"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   93
               Top             =   600
               Width           =   855
            End
            Begin VB.Label Label7 
               Caption         =   "ml"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   1080
               TabIndex        =   92
               Top             =   960
               Width           =   375
            End
            Begin VB.Label Label7 
               Caption         =   "Tiempo Infusi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   1560
               TabIndex        =   91
               Top             =   600
               Width           =   1455
            End
            Begin VB.Label Label7 
               Caption         =   "h."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   2160
               TabIndex        =   90
               Top             =   960
               Width           =   255
            End
            Begin VB.Label Label7 
               Caption         =   "m."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   58
               Left            =   3000
               TabIndex        =   89
               Top             =   960
               Width           =   255
            End
            Begin VB.Label Label7 
               Caption         =   "Vel. Perfusion"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   51
               Left            =   3360
               TabIndex        =   88
               Top             =   600
               Width           =   1335
            End
            Begin VB.Label Label7 
               Caption         =   "ml/h"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   52
               Left            =   4320
               TabIndex        =   87
               Top             =   960
               Width           =   375
            End
         End
         Begin VB.TextBox txtVolTotal 
            BackColor       =   &H8000000B&
            Height          =   285
            Index           =   2
            Left            =   4920
            Locked          =   -1  'True
            TabIndex        =   85
            Top             =   4320
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.TextBox txtHoraInicio 
            BackColor       =   &H8000000B&
            Height          =   375
            Index           =   2
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   56
            Top             =   4920
            Width           =   375
         End
         Begin VB.TextBox txtHoraFin 
            BackColor       =   &H8000000B&
            Height          =   375
            Index           =   2
            Left            =   4680
            Locked          =   -1  'True
            TabIndex        =   58
            Top             =   4920
            Width           =   375
         End
         Begin VB.TextBox txtObservaciones 
            Height          =   735
            Index           =   2
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   59
            ToolTipText     =   "Instrucciones de Administraci�n, Comentarios"
            Top             =   5760
            Width           =   4215
         End
         Begin VB.CommandButton cmdGuardar 
            Enabled         =   0   'False
            Height          =   615
            Index           =   2
            Left            =   5040
            Style           =   1  'Graphical
            TabIndex        =   60
            ToolTipText     =   "Guardar las modificaciones"
            Top             =   5760
            Width           =   615
         End
         Begin VB.CheckBox chkStat 
            Caption         =   "STAT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   5280
            TabIndex        =   84
            TabStop         =   0   'False
            ToolTipText     =   "Nueva Dosificaci�n"
            Top             =   4800
            Visible         =   0   'False
            Width           =   855
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFecInicio 
            Height          =   375
            Index           =   2
            Left            =   240
            TabIndex        =   55
            Top             =   4920
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   661
            _StockProps     =   93
            ForeColor       =   0
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "DD/MM/YYYY"
            AllowEdit       =   0   'False
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFecFin 
            Height          =   375
            Index           =   2
            Left            =   2760
            TabIndex        =   57
            Top             =   4920
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   661
            _StockProps     =   93
            ForeColor       =   0
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "DD/MM/YYYY"
            AllowEdit       =   0   'False
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo dbFrecuencia 
            Height          =   300
            Index           =   2
            Left            =   240
            TabIndex        =   52
            Top             =   4320
            Width           =   2055
            DataFieldList   =   "Column 0"
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3625
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo dbPeriodicidad 
            Height          =   300
            Index           =   2
            Left            =   3840
            TabIndex        =   54
            Top             =   4320
            Visible         =   0   'False
            Width           =   1815
            DataFieldList   =   "Column 0"
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3201
            _ExtentY        =   529
            _StockProps     =   93
         End
         Begin SSDataWidgets_B.SSDBCombo dbVia 
            Height          =   300
            Index           =   2
            Left            =   2520
            TabIndex        =   53
            Top             =   4320
            Width           =   1095
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1931
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin VB.Label Label7 
            Caption         =   "Periodicidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   54
            Left            =   3840
            TabIndex        =   120
            Top             =   4080
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label Label7 
            Caption         =   "Frecuencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   119
            Top             =   4080
            Width           =   1095
         End
         Begin VB.Label Label7 
            Caption         =   "Tipo Infusi�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   2520
            TabIndex        =   118
            Top             =   4080
            Width           =   1335
         End
         Begin VB.Label Label7 
            Caption         =   "ml"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   57
            Left            =   5760
            TabIndex        =   117
            Top             =   4320
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.Label Label7 
            Caption         =   "Vol. Total"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   4920
            TabIndex        =   116
            Top             =   4080
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label Label7 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   2160
            TabIndex        =   115
            Top             =   4680
            Width           =   615
         End
         Begin VB.Label Label7 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   56
            Left            =   4680
            TabIndex        =   114
            Top             =   4680
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   240
            TabIndex        =   113
            Top             =   4680
            Width           =   1335
         End
         Begin VB.Label Label7 
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   55
            Left            =   2760
            TabIndex        =   112
            Top             =   4680
            Width           =   1335
         End
         Begin VB.Label Label7 
            Caption         =   "Instrucciones de Administraci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   240
            TabIndex        =   111
            Top             =   5520
            Width           =   3015
         End
      End
      Begin VB.Frame fraPrincipal 
         BackColor       =   &H8000000B&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6735
         Index           =   0
         Left            =   5040
         TabIndex        =   28
         Top             =   120
         Visible         =   0   'False
         Width           =   6495
         Begin VB.TextBox txtHoraInicio 
            BackColor       =   &H8000000B&
            Height          =   375
            Index           =   0
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   148
            Top             =   4440
            Width           =   375
         End
         Begin VB.TextBox txtCantidad 
            BackColor       =   &H8000000B&
            Height          =   285
            Index           =   0
            Left            =   4680
            Locked          =   -1  'True
            TabIndex        =   7
            Top             =   1200
            Width           =   735
         End
         Begin VB.CheckBox chkStat 
            Caption         =   "STAT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   6000
            TabIndex        =   65
            TabStop         =   0   'False
            ToolTipText     =   "Nueva Dosificaci�n"
            Top             =   3960
            Visible         =   0   'False
            Width           =   135
         End
         Begin VB.CommandButton cmdGuardar 
            Enabled         =   0   'False
            Height          =   615
            Index           =   0
            Left            =   5160
            Style           =   1  'Graphical
            TabIndex        =   18
            ToolTipText     =   "Guardar las modificaciones"
            Top             =   5400
            Width           =   615
         End
         Begin VB.TextBox txtObservaciones 
            Height          =   855
            Index           =   0
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   17
            ToolTipText     =   "Instrucciones de Administraci�n, Comentarios"
            Top             =   5400
            Width           =   3975
         End
         Begin VB.TextBox txtHoraFin 
            BackColor       =   &H8000000B&
            Height          =   375
            Index           =   0
            Left            =   4800
            Locked          =   -1  'True
            TabIndex        =   16
            Top             =   4440
            Width           =   375
         End
         Begin VB.TextBox txtVolTotal 
            BackColor       =   &H8000000B&
            Height          =   285
            Index           =   0
            Left            =   5865
            Locked          =   -1  'True
            TabIndex        =   64
            TabStop         =   0   'False
            Top             =   3840
            Visible         =   0   'False
            Width           =   150
         End
         Begin VB.Frame frasolucion 
            Caption         =   "Soluci�n para Diluir"
            Height          =   1575
            Index           =   0
            Left            =   240
            TabIndex        =   31
            Top             =   1680
            Width           =   5775
            Begin VB.TextBox txtvelocidad 
               Height          =   375
               Index           =   0
               Left            =   1560
               TabIndex        =   10
               TabStop         =   0   'False
               Top             =   1080
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.TextBox txtHorainf 
               BackColor       =   &H8000000B&
               Height          =   375
               Index           =   0
               Left            =   3240
               Locked          =   -1  'True
               TabIndex        =   33
               TabStop         =   0   'False
               Top             =   1080
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.TextBox txtMinInf 
               BackColor       =   &H8000000B&
               Height          =   375
               Index           =   0
               Left            =   4080
               Locked          =   -1  'True
               TabIndex        =   32
               TabStop         =   0   'False
               Top             =   1080
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.TextBox txtVolumenDil 
               BackColor       =   &H00C0C0C0&
               Height          =   375
               Index           =   0
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   9
               Top             =   1080
               Width           =   855
            End
            Begin SSDataWidgets_B.SSDBCombo dbSolucion 
               Height          =   300
               Index           =   0
               Left            =   120
               TabIndex        =   8
               Top             =   360
               Width           =   5415
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196614
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               ForeColorEven   =   0
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   8916
               Columns(0).Caption=   "Descripci�n"
               Columns(0).Name =   "Descripci�n"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1746
               Columns(1).Caption=   "Vol (ml)"
               Columns(1).Name =   "Vol (ml)"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   1455
               Columns(2).Caption=   "Cod."
               Columns(2).Name =   "Cod."
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   9551
               _ExtentY        =   529
               _StockProps     =   93
               ForeColor       =   0
            End
            Begin VB.Label Label7 
               Caption         =   "ml/h"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   2520
               TabIndex        =   63
               Top             =   1200
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.Label Label7 
               Caption         =   "Vel. Perfusion"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   26
               Left            =   1560
               TabIndex        =   62
               Top             =   840
               Visible         =   0   'False
               Width           =   1335
            End
            Begin VB.Label Label7 
               Caption         =   "m."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   4680
               TabIndex        =   61
               Top             =   1200
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.Label Label7 
               Caption         =   "h."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   3840
               TabIndex        =   38
               Top             =   1200
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.Label Label7 
               Caption         =   "Tiempo Infusi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   3240
               TabIndex        =   37
               Top             =   840
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.Label Label7 
               Caption         =   "ml"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   1080
               TabIndex        =   36
               Top             =   1200
               Width           =   375
            End
            Begin VB.Label Label7 
               Caption         =   "Volumen"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   120
               TabIndex        =   35
               Top             =   840
               Width           =   1095
            End
            Begin VB.Label lblSolucion 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00C00000&
               Height          =   240
               Index           =   0
               Left            =   240
               TabIndex        =   34
               Top             =   360
               Width           =   75
            End
         End
         Begin VB.TextBox txtVolumen 
            BackColor       =   &H8000000B&
            Height          =   285
            Index           =   0
            Left            =   5385
            Locked          =   -1  'True
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   1320
            Visible         =   0   'False
            Width           =   150
         End
         Begin VB.TextBox txtDosis 
            BackColor       =   &H8000000B&
            Height          =   300
            Index           =   0
            Left            =   1800
            Locked          =   -1  'True
            TabIndex        =   5
            Top             =   1200
            Width           =   1080
         End
         Begin SSDataWidgets_B.SSDBCombo dbFF 
            Height          =   300
            Index           =   0
            Left            =   360
            TabIndex        =   4
            Top             =   1200
            Width           =   1095
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3122
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1931
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo dbMedicamento 
            Height          =   300
            Index           =   0
            Left            =   360
            TabIndex        =   3
            Top             =   480
            Width           =   5655
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   6
            Columns(0).Width=   5821
            Columns(0).Caption=   "Medicamento"
            Columns(0).Name =   "Medicamento"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1508
            Columns(1).Caption=   "Dosis"
            Columns(1).Name =   "Dosis"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1508
            Columns(2).Caption=   "Unidad"
            Columns(2).Name =   "Unidad"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   1323
            Columns(3).Caption=   "Forma"
            Columns(3).Name =   "Forma"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1085
            Columns(4).Caption=   "Via"
            Columns(4).Name =   "Via"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   1296
            Columns(5).Caption=   "Cod."
            Columns(5).Name =   "Cod."
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            _ExtentX        =   9975
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   128
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFecInicio 
            Height          =   375
            Index           =   0
            Left            =   240
            TabIndex        =   14
            Top             =   4440
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   661
            _StockProps     =   93
            ForeColor       =   0
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "DD/MM/YYYY"
            AllowEdit       =   0   'False
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFecFin 
            Height          =   375
            Index           =   0
            Left            =   2880
            TabIndex        =   15
            Top             =   4440
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   661
            _StockProps     =   93
            ForeColor       =   0
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "DD/MM/YYYY"
            AllowEdit       =   0   'False
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo dbUM 
            Height          =   300
            Index           =   0
            Left            =   3240
            TabIndex        =   6
            Top             =   1200
            Width           =   1095
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3122
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1931
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo dbFrecuencia 
            Height          =   300
            Index           =   0
            Left            =   240
            TabIndex        =   11
            Top             =   3720
            Width           =   1815
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3201
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo dbPeriodicidad 
            Height          =   300
            Index           =   0
            Left            =   3720
            TabIndex        =   13
            Top             =   3720
            Visible         =   0   'False
            Width           =   1815
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3201
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo dbVia 
            Height          =   300
            Index           =   0
            Left            =   2280
            TabIndex        =   12
            Top             =   3720
            Width           =   1095
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196614
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1931
            _ExtentY        =   529
            _StockProps     =   93
            ForeColor       =   0
         End
         Begin VB.Label Label7 
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   43
            Left            =   4680
            TabIndex        =   82
            Top             =   960
            Width           =   975
         End
         Begin VB.Label Label7 
            Caption         =   "Instrucciones de Administraci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   38
            Left            =   240
            TabIndex        =   81
            Top             =   5160
            Width           =   3015
         End
         Begin VB.Label Label7 
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   36
            Left            =   2880
            TabIndex        =   80
            Top             =   4200
            Width           =   1335
         End
         Begin VB.Label Label7 
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   44
            Left            =   240
            TabIndex        =   79
            Top             =   4200
            Width           =   1335
         End
         Begin VB.Label Label7 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   37
            Left            =   4800
            TabIndex        =   78
            Top             =   4200
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   35
            Left            =   2160
            TabIndex        =   77
            Top             =   4200
            Width           =   615
         End
         Begin VB.Label Label7 
            Caption         =   "Vol. Total"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   33
            Left            =   5880
            TabIndex        =   76
            Top             =   3600
            Visible         =   0   'False
            Width           =   135
         End
         Begin VB.Label Label7 
            Caption         =   "ml"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   34
            Left            =   6120
            TabIndex        =   75
            Top             =   3840
            Visible         =   0   'False
            Width           =   135
         End
         Begin VB.Label Label7 
            Caption         =   "Tipo Infusi�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   42
            Left            =   2280
            TabIndex        =   74
            Top             =   3480
            Width           =   1215
         End
         Begin VB.Label Label7 
            Caption         =   "FF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   39
            Left            =   360
            TabIndex        =   73
            Top             =   960
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   60
            Left            =   1800
            TabIndex        =   72
            Top             =   960
            Width           =   735
         End
         Begin VB.Label Label7 
            Caption         =   "UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   59
            Left            =   3240
            TabIndex        =   71
            Top             =   960
            Width           =   735
         End
         Begin VB.Label Label7 
            Caption         =   "Volumen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5280
            TabIndex        =   70
            Top             =   1080
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.Label Label7 
            Caption         =   "ml"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   25
            Left            =   5640
            TabIndex        =   69
            Top             =   1320
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.Label Label7 
            Caption         =   "Frecuencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   32
            Left            =   240
            TabIndex        =   68
            Top             =   3480
            Width           =   1095
         End
         Begin VB.Label Label7 
            Caption         =   "Periodicidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   31
            Left            =   3720
            TabIndex        =   67
            Top             =   3480
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblMedicamento 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   240
            Index           =   0
            Left            =   360
            TabIndex        =   66
            Top             =   480
            Width           =   75
         End
      End
      Begin VB.Frame frmHoras 
         Height          =   1815
         Left            =   6000
         TabIndex        =   121
         Top             =   2520
         Visible         =   0   'False
         Width           =   4815
         Begin VB.CommandButton cmdCancelar 
            Cancel          =   -1  'True
            Caption         =   "&Cancelar"
            Height          =   375
            Left            =   2880
            TabIndex        =   147
            Top             =   1320
            Width           =   855
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Default         =   -1  'True
            Height          =   375
            Left            =   720
            TabIndex        =   146
            Top             =   1320
            Width           =   855
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "23"
            Height          =   375
            Index           =   23
            Left            =   4200
            Style           =   1  'Graphical
            TabIndex        =   145
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "22"
            Height          =   375
            Index           =   22
            Left            =   3840
            Style           =   1  'Graphical
            TabIndex        =   144
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "21"
            Height          =   375
            Index           =   21
            Left            =   3480
            Style           =   1  'Graphical
            TabIndex        =   143
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "20"
            Height          =   375
            Index           =   20
            Left            =   3120
            Style           =   1  'Graphical
            TabIndex        =   142
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "19"
            Height          =   375
            Index           =   19
            Left            =   2760
            Style           =   1  'Graphical
            TabIndex        =   141
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "18"
            Height          =   375
            Index           =   18
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   140
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "17"
            Height          =   375
            Index           =   17
            Left            =   2040
            Style           =   1  'Graphical
            TabIndex        =   139
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "16"
            Height          =   375
            Index           =   16
            Left            =   1680
            Style           =   1  'Graphical
            TabIndex        =   138
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "15"
            Height          =   375
            Index           =   15
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   137
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "14"
            Height          =   375
            Index           =   14
            Left            =   960
            Style           =   1  'Graphical
            TabIndex        =   136
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "13"
            Height          =   375
            Index           =   13
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   135
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "12"
            Height          =   375
            Index           =   12
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   134
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "11"
            Height          =   375
            Index           =   11
            Left            =   4200
            Style           =   1  'Graphical
            TabIndex        =   133
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "10"
            Height          =   375
            Index           =   10
            Left            =   3840
            Style           =   1  'Graphical
            TabIndex        =   132
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "9"
            Height          =   375
            Index           =   9
            Left            =   3480
            Style           =   1  'Graphical
            TabIndex        =   131
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "8"
            Height          =   375
            Index           =   8
            Left            =   3120
            Style           =   1  'Graphical
            TabIndex        =   130
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "7"
            Height          =   375
            Index           =   7
            Left            =   2760
            Style           =   1  'Graphical
            TabIndex        =   129
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "6"
            Height          =   375
            Index           =   6
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   128
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "5"
            Height          =   375
            Index           =   5
            Left            =   2040
            Style           =   1  'Graphical
            TabIndex        =   127
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "4"
            Height          =   375
            Index           =   4
            Left            =   1680
            Style           =   1  'Graphical
            TabIndex        =   126
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "3"
            Height          =   375
            Index           =   3
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   125
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "2"
            Height          =   375
            Index           =   2
            Left            =   960
            Style           =   1  'Graphical
            TabIndex        =   124
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "1"
            Height          =   375
            Index           =   1
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   123
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras 
            Caption         =   "0"
            Height          =   375
            Index           =   0
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   122
            Top             =   360
            Width           =   375
         End
      End
   End
   Begin VB.CommandButton cmdSalir 
      Height          =   615
      Left            =   11040
      Style           =   1  'Graphical
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Salir"
      Top             =   480
      Width           =   615
   End
   Begin VB.Frame fraPaciente 
      Caption         =   "Paciente"
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10095
      Begin VB.TextBox txtCama 
         Alignment       =   2  'Center
         BackColor       =   &H8000000B&
         DataField       =   "AD15CODCAMA"
         Height          =   285
         Left            =   7200
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtHistoria 
         Alignment       =   2  'Center
         BackColor       =   &H8000000B&
         DataField       =   "CI22NUMHISTORIA"
         Height          =   285
         Left            =   8760
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtAsistencia 
         Alignment       =   2  'Center
         BackColor       =   &H8000000B&
         DataField       =   "AD01CODASISTENCI"
         Height          =   405
         Left            =   5640
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   600
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Image Image1 
         Height          =   735
         Left            =   240
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Cama"
         Height          =   255
         Left            =   6720
         TabIndex        =   26
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5520
         TabIndex        =   21
         Top             =   360
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Historia"
         Height          =   255
         Left            =   8160
         TabIndex        =   20
         Top             =   600
         Width           =   615
      End
      Begin VB.Label lblNombre 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   960
         TabIndex        =   19
         Top             =   600
         Width           =   75
      End
   End
   Begin ComctlLib.ImageList iml1 
      Left            =   0
      Top             =   8040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   18
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":0326
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":0640
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":095A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":0C74
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":0F8E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":12A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":15C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":18DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":1B56
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":21B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":24D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":27EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":2B06
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":2CE0
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":2FFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":3314
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":362E
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM100.frx":3948
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmOMEnfermeras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim blnCambio As Boolean
Dim volumenIni As Double
Dim DosisIni As Double
Dim VolumenDilIni As Double
Dim volumen2 As Double
Dim blnUnsegundo As Boolean
Dim blnPrincipio As Boolean
Private Sub cmdAceptar_Click()
Dim indice%
    frmHoras.Visible = False
    Select Case cmdNuevo.Caption
    Case "Nueva Perfusi�n"
        indice = cMED
        fraPrincipal(indice).Enabled = True
    Case "Nueva Mezcla"
        indice = cMEZ
        fraPrincipal(indice).Enabled = True
    Case "Nueva Fluidoterapia"
        indice = cFLU
        fraPrincipal(indice).Enabled = True
    Case "Nueva Nutrici�n Parenteral"
        indice = cFLU
        fraPrincipal(indice).Enabled = True
    Case "Nueva Nutrici�n Enteral"
        indice = cFLU
        fraPrincipal(indice).Enabled = True
    Case "PRN Fluidoterapia"
        indice = cFLU
        fraPrincipal(indice).Enabled = True
    Case "PRN Nutrici�n Enteral"
        indice = cFLU
        fraPrincipal(indice).Enabled = True
    End Select
End Sub

Private Sub cmdCancelar_Click()
Dim i%
Dim indice%
  For i = 0 To 23
      cmdHoras(i).BackColor = Gris
  Next i
  frmHoras.Visible = False
  Select Case cmdNuevo.Caption
  Case "Nueva Perfusi�n"
      indice = cMED
      fraPrincipal(indice).Enabled = True
  Case "Nueva Mezcla"
      indice = cMEZ
      fraPrincipal(indice).Enabled = True
  Case "Nueva Fluidoterapia"
      indice = cFLU
      fraPrincipal(indice).Enabled = True
  Case "Nueva Nutrici�n Parenteral"
      indice = cFLU
      fraPrincipal(indice).Enabled = True
  Case "Nueva Nutrici�n Enteral"
      indice = cFLU
      fraPrincipal(indice).Enabled = True
  Case "PRN Fluidoterapia"
      indice = cMED
      fraPrincipal(indice).Enabled = True
  Case "PRN Nutrici�n Enteral"
      indice = cMED
      fraPrincipal(indice).Enabled = True
  End Select
End Sub

Private Sub cmdGuardar_Click(Index As Integer)
Dim objOrdenMedica As New PI00100.clsFarmacia
Dim Node As Node
Dim lon As Integer
Dim PosL As Integer
Dim NumLinea As Integer
Dim CodPeticion As Long
Dim strMed As String
Dim rdMed As rdoResultset
Dim qyMed As rdoQuery
Dim codProducto2 As Long
Dim codProducto1 As Long
Dim codProducto3 As Long
Dim rdil As Long
Dim rprod As Long
Dim rprod2 As Long
Dim IntTipoOm As Integer
Dim Descripcion As String
Dim Key$
Dim blnfr28 As Boolean
Dim blnfrp4 As Boolean
Dim STR As String
Dim qy As rdoQuery
Dim rd As rdoResultset
Dim blnValida As Boolean
Dim horaInf%
Dim MinInf%, i%
Dim blnContinuar As Boolean
Dim strPims$
Dim J As Long

On Error Resume Next

Me.MousePointer = vbHourglass
Set Node = tvwOM.SelectedItem

'If Not LOG Then 'log-on de Picis
'    MsgBox "Error al conectarse a Picis"
'    Exit Sub
'End If

If Node.Parent <> "" Then 'Siempre entra en este if.
'Vale para que de el error posterior
End If
If Err = 0 And (Left(Node.Key, 1) = "P" And (Node.Key <> "PF" And Node.Key <> "PNE")) Then 'Se ha seleccionado un hijo
''''    lon = Len(node.Key)
''''    PosL = InStr(1, node.Key, "L")
''''    CodPeticion = Mid(node.Key, 2, lon - (lon - PosL + 1) - 1)
''''    NumLinea = Mid(node.Key, PosL + 1, lon - PosL)
''''    str = "SELECT ORDERSTATUSDBOID FROM ORDERS WHERE ORDERDBOID= " & _
''''    "(SELECT ORDERDBOID FROM FRP300 WHERE FR66CODPETICION=? " & _
''''    "AND FR28NUMLINEA=?)"
''''     Set qy = objApp.rdoConnect.CreateQuery("", str)
''''        qy(0) = codPeticion
''''        qy(1) = numlinea
''''     Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
''''    If RD(0) <> "170000000000006000000" Then ' Si no se esta haciendo
''''        MsgBox "Esta orden no puede ser actualizar porque" & Chr(13) & _
''''        "todavia no ha sido validada", vbCritical, "Aviso"
''''        Exit Sub
''''    End If
    lon = Len(Node.Key)
    CodPeticion = Right(Node.Key, lon - 1)
    STR = "SELECT ORDERSTATUSDBOID FROM ORDERS WHERE ORDERDBOID IN " & _
    "(SELECT ORDERDBOID FROM FRP500 WHERE FRP4CODPETICION=?) "
     Set qy = objApp.rdoConnect.CreateQuery("", STR)
        qy(0) = CodPeticion
     Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
     blnValida = False
    Do While Not rd.EOF
       If rd(0) = "170000000000006000000" Or rd(0) = "170000000000007000000" Then
       ' Si no se esta haciendo o es Reschedule
           blnValida = True 'funcion valida
       End If
       rd.MoveNext
    Loop
    rd.Close
    qy.Close
    If Not blnValida Then ' Sino es valida se sale de la funcion
           MsgBox "Esta orden no puede ser actualizada en Picis porque" & Chr(13) & _
           "todav�a no ha sido validada o est� ya finalizada.", vbCritical, "Aviso"
        If dtcFecFin(Index).DataChanged = True Or txtHoraFin(Index).DataChanged = True Then
            MsgBox "Solo se actualizar� la fecha u hora de finalizaci�n de la orden", vbInformation, ""
            Call UpdateFecFin(CodPeticion, txtHoraFin(Index), dtcFecFin(Index))
        End If
           Me.MousePointer = vbDefault
           Exit Sub
    End If

    Call UpdateFRP400(CodPeticion, txtvelocidad(Index), txtObservaciones(Index), _
    txtHoraFin(Index), dtcFecFin(Index))
    blnfrp4 = True
    If txtHorainf(Index) = "" Then horaInf = 0
    If txtMinInf(Index) = "" Then MinInf = 0
    lon = Len(Node.Key)
    CodPeticion = Right(Node.Key, lon - 1)
    Set objOrdenMedica.objClsCW = objCW
    objOrdenMedica.strUser = objSecurity.strUser
    
    strPims = "SELECT PATIENTDBOID FROM PATIENTS WHERE PTID1=?"
    Set qy = objApp.rdoConnect.CreateQuery("", strPims)
    qy(0) = txtHistoria.Text
    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    Call objOrdenMedica.CambiarProducto(CodPeticion, NumLinea, _
        txtvelocidad(Index), txtDosis(Index), txtVolumenDil(Index), _
        dbUM(Index), txtObservaciones(Index), MinInf, _
        horaInf, dbVia(Index), blnfr28, blnfrp4, rd)
    J = PcsVbPimsSendAllOrders(rd!PATIENTDBOID, 1)
    Dim t!
    t = Timer
    Do While Timer < t + 1
    
    Loop
    rd.Close
    qy.Close
Else
    Err = 0
    If dbFrecuencia(Index).Value = "Manual" Then
        For i = 0 To 23
            If cmdHoras(i).BackColor = Rojo Then
                blnContinuar = True
            End If
        Next i
        If Not blnContinuar Then
            MsgBox "Si la frecuencia es manual debe seleccionar alguna hora", vbCritical, "Frecuencia"
            Me.MousePointer = vbDefault
            Exit Sub
        End If
    End If
    If HayAlgoAzul(Index) Then
        Me.MousePointer = vbDefault
        MsgBox "No ha rellenado alg�n campo obligatorio" & Chr(13) & Chr(10) _
        , vbCritical, "Aviso"
        Exit Sub
    End If
    If Index = cMED Then
        If dbUM(Index) = "amp" Then
            MsgBox "La perfusi�n no pasar� a Picis. Debe cambiar la unidad de medida:" & Chr(13) & Chr(10) _
            & "No es v�lida la unidad de ampollas en una perfusi�n", vbCritical, "Orden no v�lida"
            Me.MousePointer = vbDefault
            Exit Sub
        End If
    End If
    CodPeticion = InsertarFRP400(Index)
    Set objOrdenMedica.objClsCW = objCW
    objOrdenMedica.strUser = objSecurity.strUser
    Call objOrdenMedica.OrdenMedica(CodPeticion, False, True)

    '''''Que OM estan firmadas en Farmacia
    ''''strMed = "SELECT FR2800.FR73CODPRODUCTO, " & _
    ''''"FR2800.FR73CODPRODUCTO_2, FR2800.FR73CODPRODUCTO_DIL, " & _
    ''''"FR2800.FR28OPERACION, FR2800.FR66CODPETICION, FR2800.FR28NUMLINEA " & _
    ''''"FROM FR2800, FR6600 WHERE " & _
    ''''"FR6600.FR66CODPETICION = FR2800.FR66CODPETICION " & _
    ''''"AND FR6600.AD01CODASISTENCI=? AND FR6600.FR26CODESTPETIC=? "
    ''''Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
    ''''    qyMed(0) = codAsistencia
    ''''    qyMed(1) = 2 'ESTADO FIRMADA
    ''''Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'    strPims = "SELECT PATIENTDBOID FROM PATIENTS WHERE PTID1=?"
'    Set Qy = objApp.rdoConnect.CreateQuery("", strPims)
'    Qy(0) = txtHistoria.Text
'    Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'    J = PcsVbPimsSendAllOrders(rd!PATIENTDBOID, 1)
'    rd.Close
'    Qy.Close

    'Qu� OM est�n en Picis
    strMed = "SELECT FR73CODPRODUCTO, " & _
    "FR73CODPRODUCTO_2, FR73CODPRODUCTO_DIL, " & _
    "FRP4OPERACION, FRP4CODPETICION " & _
    "FROM FRP400 WHERE " & _
    "FRP4CODPETICION=? "
    Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
        qyMed(0) = CodPeticion
    Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    '****
    'Set tvwOM.ImageList = iml1
    If Not IsNull(rdMed!FR73CODPRODUCTO_DIL) Then rdil = rdMed!FR73CODPRODUCTO_DIL Else rdil = 0
    If Not IsNull(rdMed!FR73CODPRODUCTO_2) Then rprod2 = rdMed!FR73CODPRODUCTO_2 Else rprod2 = 0
    If Not IsNull(rdMed!FR73CODPRODUCTO) Then rprod = rdMed!FR73CODPRODUCTO Else rprod = 0
    Call productos(rdMed!FRP4OPERACION, rdil, _
    rprod, rprod2, codProducto1, _
    codProducto2, codProducto3)
    
    IntTipoOm = ObtenerTipoMedicamento(rdMed!FRP4OPERACION, codProducto1, codProducto2, _
    codProducto3)

    Descripcion = ObtenerDescripcionOrden(codProducto1, codProducto2, _
    codProducto3, IntTipoOm)
        ''''key = "P" & rdMed!FR66CODPETICION & "L" & rdMed!FR28NUMLINEA
        Key = "P" & rdMed!FRP4CODPETICION
        ''''Select Case rdMed!FR28OPERACION
        Select Case rdMed!FRP4OPERACION
            Case "/"
                If IntTipoOm = 2 Then
                    Set Node = tvwOM.Nodes.Add("/", tvwChild, Key, Descripcion, constFLUIDOS)
                Else
                    Set Node = tvwOM.Nodes.Add("/", tvwChild, Key, Descripcion, constMEDICAMENTOS)
                End If
            Case "M"
                Set Node = tvwOM.Nodes.Add("M", tvwChild, Key, Descripcion, constFLUIDOS)
            Case "F"
              STR = "SELECT FR00CODGRPTERAP FROM FR7300 WHERE FR73CODPRODUCTO=?"
              Set qy = objApp.rdoConnect.CreateQuery("", STR)
                qy(0) = rdil
              Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
              Select Case rd!FR00CODGRPTERAP
              Case "V06C2A"
                  Set Node = tvwOM.Nodes.Add("FNE", tvwChild, Key, Descripcion, constFLUIDOS)
              Case "B05F"
                  Set Node = tvwOM.Nodes.Add("FNP", tvwChild, Key, Descripcion, constFLUIDOS)
              Case Else
                  Set Node = tvwOM.Nodes.Add("FF", tvwChild, Key, Descripcion, constFLUIDOS)
              End Select
              rd.Close
              qy.Close
            Case "P"
              STR = "SELECT FR00CODGRPTERAP FROM FR7300 WHERE FR73CODPRODUCTO=?"
              Set qy = objApp.rdoConnect.CreateQuery("", STR)
                qy(0) = rdil
              Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
              Select Case rd!FR00CODGRPTERAP
              Case "V06C2A"
                  Set Node = tvwOM.Nodes.Add("PNE", tvwChild, Key, Descripcion, constPRN)
              Case Else
                  Set Node = tvwOM.Nodes.Add("PF", tvwChild, Key, Descripcion, constPRN)
              End Select
              rd.Close
              qy.Close
            Case "E"
                If IntTipoOm = 6 Then
                    Set Node = tvwOM.Nodes.Add("E", tvwChild, Key, Descripcion, constFLUIDOS)
                Else
                    Set Node = tvwOM.Nodes.Add("E", tvwChild, Key, Descripcion, constFLUIDOS)
                End If
        End Select
        Node.EnsureVisible
        Set tvwOM.SelectedItem = Node
        tvwOM.Refresh
    rdMed.Close
    qyMed.Close
    fraPrincipal(Index).Enabled = False
End If
'J = PcsVbPimsFree()
Me.MousePointer = vbDefault
cmdSalir.SetFocus
End Sub

Private Sub cmdHoras_Click(Index As Integer)
If cmdHoras(Index).BackColor = Gris Then
    cmdHoras(Index).BackColor = Rojo
Else
    cmdHoras(Index).BackColor = Gris
End If
cmdAceptar.SetFocus
End Sub

Private Sub cmdNuevo_Click()
Dim indice As Integer
Me.MousePointer = vbHourglass
    Select Case cmdNuevo.Caption
    Case "Nueva Perfusi�n"
        indice = cMED
        fraPrincipal(indice).Caption = "Perfusiones"
        fraPrincipal(indice).ForeColor = &H8000&
        frasolucion(indice).Caption = "Fluido"
    Case "Nueva Mezcla"
        indice = cMEZ
        fraPrincipal(indice).ForeColor = &HFF00FF
        frasolucion(indice).Caption = "Fluido"
    Case "Nueva Fluidoterapia"
        indice = cFLU
        fraPrincipal(indice).Caption = "Fluidoterapia"
        fraPrincipal(indice).ForeColor = &H808000
        frasolucion(indice).Caption = "Fluido"
        fraLista.Visible = False
    Case "Nueva Nutrici�n Enteral"
        indice = cFLU
        fraPrincipal(indice).Caption = "Nutrici�n Enteral"
        fraPrincipal(indice).ForeColor = &H808000
        frasolucion(indice).Caption = ""
    Case "Nueva Nutrici�n Parenteral"
        indice = cFLU
        fraPrincipal(indice).Caption = "Nutrici�n Parenteral"
        fraPrincipal(indice).ForeColor = &H808000
        frasolucion(indice).Caption = ""
    Case "PRN Fluidoterapia"
        indice = cFLU
        fraPrincipal(indice).Caption = "PRN Fluidoterapia"
        fraPrincipal(indice).ForeColor = &H8080&
        frasolucion(indice).Caption = "Fluido"
    Case "PRN Nutrici�n Enteral"
        indice = cFLU
        fraPrincipal(indice).Caption = "PRN Nutrici�n Enteral"
        fraPrincipal(indice).ForeColor = &H8080&
        frasolucion(indice).Caption = ""
    End Select
Call BorrarCampos(indice)
Call ActivarCampos(indice)
CargarCombos (indice)
fraPrincipal(indice).Visible = True
Me.MousePointer = vbDefault
End Sub

Private Sub cmdQuitar_Click()
Dim i%, orden%, STR$
Dim qy As rdoQuery
If ssdbSueros.SelBookmarks.COUNT = 0 Then
  MsgBox "Seleccione una fila para borrarla", vbOKOnly, "Eliminar un suero"
  Exit Sub
End If
For i = 0 To ssdbSueros.SelBookmarks.COUNT - 1

  If ssdbSueros.Columns(3).Text = "" Then
'    MsgBox "Esta fila no est� grabada. No puede borrarla" & Chr(13) & _
'    "Posicione el cursos en otra l�nea del grid antes de borrar", vbInformation, ""
    Exit Sub
  End If
  ssdbSueros.MoveFirst
  ssdbSueros.MoveRecords (Val(ssdbSueros.SelBookmarks.Item(i)))

  orden = ssdbSueros.Columns(3).Text
  STR = "DELETE FROM FRP600 WHERE AD01CODASISTENCI=? AND FRP6ORDEN=?"
  Set qy = objApp.rdoConnect.CreateQuery("", STR)
  qy(0) = txtAsistencia
  qy(1) = orden
  qy.Execute
Next
Call RellenarGridSueros
End Sub

Private Sub cmdReload_Click()
tvwOM.Nodes.Clear
  Call Form_Load
fraPrincipal(0).Visible = False
fraPrincipal(2).Visible = False
frmHoras.Visible = False
cmdNuevo.Visible = True
cmdNuevo.Caption = "Nueva Perfusi�n"
End Sub

Private Sub cmdSalir_Click()
    Me.Visible = False
'    Unload Me
End Sub

Private Sub cmdVel_Click()
  If txtObservaciones(cFLU) = "" Then
    txtObservaciones(cFLU) = txtvelocidad(cFLU) & " ml/h"
  Else
    txtObservaciones(cFLU) = txtObservaciones(cFLU) & " " & txtvelocidad(cFLU) & " ml/h"
  End If
End Sub

Private Sub dbFrecuencia_CloseUp(Index As Integer)
    If dbFrecuencia(Index).Value = "Manual" Then
     frmHoras.Visible = True
     cmdAceptar.SetFocus
     fraPrincipal(Index).Enabled = False
    End If
End Sub

Private Sub dbFrecuencia_LostFocus(Index As Integer)
    If dbFrecuencia(Index) = "" Then
        dbFrecuencia(Index).Columns(0).Value = Null
    End If
End Sub

Private Sub dbMedicamento_LostFocus(Index As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim HorasInf As Integer
Dim MinInf As Integer
DosisIni = 0
volumenIni = 0
If dbMedicamento(Index).Columns(5).Value <> "" And dbMedicamento(Index).Columns(0).Value = dbMedicamento(Index).Text Then
    STR = "SELECT FR73VOLUMEN, FR73TIEMINF, FR34CODVIA, FRH7CODFORMFAR, " & _
    "FR73DOSIS, FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO= ? "
    Set qy = objApp.rdoConnect.CreateQuery("", STR)
        qy(0) = dbMedicamento(Index).Columns(5).Value
    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If Not IsNull(rd!FR73DOSIS) Then
            txtDosis(Index) = rd!FR73DOSIS
            DosisIni = rd!FR73DOSIS
        Else
            DosisIni = 0
        End If

        If Not IsNull(rd!FR73VOLUMEN) Then
            txtVolumen(Index) = rd!FR73VOLUMEN
            volumenIni = rd!FR73VOLUMEN
        Else
            txtVolumen(Index) = 0
            volumenIni = 0
        End If
'    If Not IsNull(RD!FR34CODVIA) Then
'        dbVia(Index).Columns(0).Value = RD!FR34CODVIA
'        dbVia(Index) = RD!FR34CODVIA
'    End If
    If Not IsNull(rd!FRH7CODFORMFAR) Then
        dbFF(Index).Columns(0).Value = rd!FRH7CODFORMFAR
        dbFF(Index) = rd!FRH7CODFORMFAR
    End If
    dbUM(Index) = rd!FR93CODUNIMEDIDA
    dbUM(Index).Columns(0).Value = rd!FR93CODUNIMEDIDA
    If Not IsNull(rd!FR73DOSIS) Then
        txtDosis(Index) = rd!FR73DOSIS
        DosisIni = rd!FR73DOSIS
    Else
        DosisIni = 0
    End If
    If Index <> cFLU Then 'se rellenan los siguientes campos si predomina el medicamento
        dbPeriodicidad(Index).Columns(0).Value = "Diario"
        dbPeriodicidad(Index) = "Diario"
        dbFrecuencia(Index).Columns(0).Value = Null
        dbFrecuencia(Index) = ""
        dtcFecInicio(Index).Date = strFecha_Sistema
        txtHoraInicio(Index) = Format(strHora_Sistema, "HH")
        txtCantidad(Index) = 1
        dbVia(Index).Columns(0).Value = "PC"
        dbVia(Index) = "PC"
    End If
    rd.Close
    qy.Close
Else
    dbMedicamento(Index).Columns(5).Value = Null
End If

End Sub

Private Sub dbMedicamento2_LostFocus(Index As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim HorasInf As Integer
Dim MinInf As Integer
87 If Index = cFLU And dbMedicamento(cFLU).Columns(5).Value = "" Then
    dbMedicamento2(Index).Value = ""
    dbMedicamento2(Index).Text = ""
    MsgBox "No ha rellenado los campos del Aditivo 1", vbCritical, ""
    Exit Sub
End If
    If dbMedicamento2(Index).Columns(5).Value <> "" And dbMedicamento2(Index).Columns(0).Value = dbMedicamento2(Index).Text Then
        STR = "SELECT FR73VOLUMEN, FR73TIEMINF, FR34CODVIA, FRH7CODFORMFAR, " & _
        "FR73DOSIS, FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO= ? "
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
            qy(0) = dbMedicamento2(Index).Columns(5).Value
        Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

        If Not IsNull(rd!FR73VOLUMEN) Then
            txtVolumen2(Index) = rd!FR73VOLUMEN
            volumen2 = rd!FR73VOLUMEN
        Else
            txtVolumen2(Index) = 0
            volumen2 = 0
        End If

''        If Not IsNull(RD!FR34CODVIA) Then
''            dbVia(cMEZ) = RD!FR34CODVIA
''            dbVia(cMEZ).Columns(0).Value = RD!FR34CODVIA
''        End If
        If Not IsNull(rd!FRH7CODFORMFAR) Then
            dbFF2(Index) = rd!FRH7CODFORMFAR
            dbFF2(Index).Columns(0).Value = rd!FRH7CODFORMFAR
        End If
        dbUM2(Index) = rd!FR93CODUNIMEDIDA
        dbUM2(Index).Columns(0).Value = rd!FR93CODUNIMEDIDA
        If Not IsNull(rd!FR73DOSIS) Then
            txtDosis2(Index) = rd!FR73DOSIS
        End If
        rd.Close
        qy.Close
    Else
        dbMedicamento2(Index).Columns(5).Value = Null
    End If
End Sub

Private Sub dbSolucion_LostFocus(Index As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim HorasInf As Integer
Dim MinInf As Integer
If dbSolucion(Index).Columns(2).Value <> "" And dbSolucion(Index).Columns(0).Value = dbSolucion(Index).Text Then
    STR = "SELECT FR73VOLUMEN, FR73TIEMINF, FR34CODVIA, FRH7CODFORMFAR " & _
    "FROM FR7300 WHERE FR73CODPRODUCTO= ? "
    Set qy = objApp.rdoConnect.CreateQuery("", STR)
        qy(0) = dbSolucion(Index).Columns(2).Value
    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If Not IsNull(rd!FR73VOLUMEN) Then txtVolumenDil(Index) = rd!FR73VOLUMEN
''    If Not IsNull(RD!FR34CODVIA) Then
''        dbVia(Index).Columns(0).Value = RD!FR34CODVIA
''        dbVia(Index) = RD!FR34CODVIA
''    End If
    If Not IsNull(rd!FRH7CODFORMFAR) And Index <> 2 Then
        dbFF(Index).Columns(0).Value = rd!FRH7CODFORMFAR
        dbFF(Index) = rd!FRH7CODFORMFAR
    End If
    If Index = cFLU Then 'Se rellenan si predomina el fluido
        dbPeriodicidad(Index).Columns(0).Value = "Diario"
        dbPeriodicidad(Index) = "Diario"
        dbFrecuencia(Index).Columns(0).Value = ""
        dbFrecuencia(Index) = ""
        dtcFecInicio(Index).Date = strFecha_Sistema
        txtHoraInicio(Index) = Format(strHora_Sistema, "HH")
        txtCantidad(Index) = 1
        VolumenDilIni = rd!FR73VOLUMEN
        dbVia(Index).Columns(0).Value = "PC"
        dbVia(Index) = "PC"
        dbMedicamento(Index).Columns(5).Value = ""
    End If
    If Not IsNull(rd!FR73TIEMINF) Then
        Call HorasMinutos(rd!FR73TIEMINF, HorasInf, MinInf)
        txtHorainf(Index) = HorasInf
        txtMinInf(Index) = MinInf
    End If
    rd.Close
    qy.Close
Else
    dbSolucion(Index).Columns(2).Value = ""
End If
Call CalcularVolumen(Index)
End Sub



Private Sub dtcFecFin_CloseUp(Index As Integer)
        cmdGuardar(Index).Enabled = True
End Sub

Private Sub dtcFecFin_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
        cmdGuardar(Index).Enabled = True
End Sub

Public Sub Form_Load()
Dim strNombre
Dim qyNombre As rdoQuery
Dim rdNombre As rdoResultset
Dim nodo As Node
Dim strMed As String
Dim rdMed As rdoResultset
Dim qyMed As rdoQuery
Dim IntTipoOm As Integer
Dim Operacion As String
Dim codProducto1 As Long
Dim codProducto2 As Long
Dim codProducto3 As Long
Dim Descripcion As String
Dim rdil As Long
Dim rprod2 As Long
Dim rprod As Long
Dim Key As String
'Dim codAsistencia As Long
Dim STR$
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim ierrcode%

Set cmdGuardar(0).Picture = iml1.ListImages.Item(10).Picture
'Set cmdGuardar(1).Picture = iml1.ListImages.Item(10).Picture
Set cmdGuardar(2).Picture = iml1.ListImages.Item(10).Picture
Set Me.Icon = iml1.ListImages.Item(constFLUIDOS).Picture
Set Image1.Picture = iml1.ListImages.Item(9).Picture
Set cmdSalir.Picture = iml1.ListImages.Item(constSALIR).Picture
Set cmdReload.Picture = iml1.ListImages.Item(constRELOAD).Picture
Set cmdQuitar.Picture = iml1.ListImages.Item(constTACHAR).Picture
'codAsistencia = objPipe.PipeGet("PI_CodAsistencia")

strNombre = "SELECT CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, " & _
"CI2200.CI22SEGAPEL, CI2200.CI22NUMHISTORIA, GCFN06(AD1500.AD15CODCAMA) AS AD15CODCAMA " & _
"FROM AD1500, CI2200, AD0100 WHERE AD0100.AD01CODASISTENCI= ? AND " & _
"AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA AND " & _
"AD1500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI"
Set qyNombre = objApp.rdoConnect.CreateQuery("", strNombre)
    qyNombre(0) = codAsistencia
Set rdNombre = qyNombre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

lblNombre.Caption = rdNombre!ci22nombre & " " & rdNombre!CI22PRIAPEL & _
" " & rdNombre!CI22SEGAPEL
txtHistoria = rdNombre!CI22NUMHISTORIA
txtAsistencia = codAsistencia
txtCama = rdNombre!AD15CODCAMA

rdNombre.Close
qyNombre.Close

'''''Que OM estan firmadas en Farmacia
''''strMed = "SELECT FR2800.FR73CODPRODUCTO, " & _
''''"FR2800.FR73CODPRODUCTO_2, FR2800.FR73CODPRODUCTO_DIL, " & _
''''"FR2800.FR28OPERACION, FR2800.FR66CODPETICION, FR2800.FR28NUMLINEA " & _
''''"FROM FR2800, FR6600 WHERE " & _
''''"FR6600.FR66CODPETICION = FR2800.FR66CODPETICION " & _
''''"AND FR6600.AD01CODASISTENCI=? AND FR6600.FR26CODESTPETIC=? "
''''Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
''''    qyMed(0) = codAsistencia
''''    qyMed(1) = 2 'ESTADO FIRMADA
''''Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'Qu� OM est�n en Picis
strMed = "SELECT FR73CODPRODUCTO, " & _
"FR73CODPRODUCTO_2, FR73CODPRODUCTO_DIL, " & _
"FRP4OPERACION, FRP4CODPETICION " & _
"FROM FRP400 WHERE " & _
"AD01CODASISTENCI=? AND " & _
"(TRUNC(FRP4FECFIN) > TRUNC(SYSDATE) OR " & _
"(FRP4HORAFIN > TO_CHAR(SYSDATE,'HH24') AND " & _
" TRUNC(FRP4FECFIN)  = TRUNC(SYSDATE)) OR FRP4FECFIN IS NULL)"
Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
    qyMed(0) = codAsistencia
Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'****
Set tvwOM.ImageList = iml1

Set nodo = tvwOM.Nodes.Add(, , "/", "Perfusiones", constNEUTRO)
'Set nodo = tvwOM.Nodes.Add(, , "M", "Mezclas", constNEUTRO)
Set nodo = tvwOM.Nodes.Add(, , "F", "Fluidos", constNEUTRO)
  Set nodo = tvwOM.Nodes.Add("F", tvwChild, "FF", "Fluidoterapia", constNEUTRO)
  Set nodo = tvwOM.Nodes.Add("F", tvwChild, "FNP", "Nutrici�n Parenteral", constNEUTRO)
  Set nodo = tvwOM.Nodes.Add("F", tvwChild, "FNE", "Nutrici�n Enteral", constNEUTRO)
nodo.EnsureVisible
Set nodo = tvwOM.Nodes.Add(, , "P", "PRN", constNEUTRO)
  Set nodo = tvwOM.Nodes.Add("P", tvwChild, "PF", "Fluidoterapia", constNEUTRO)
  Set nodo = tvwOM.Nodes.Add("P", tvwChild, "PNE", "Nutrici�n Enteral", constNEUTRO)
nodo.EnsureVisible
'Set nodo = tvwOM.Nodes.Add(, , "E", "Estupefacientes", constNEUTRO)

While Not rdMed.EOF
    ''''Operacion = rdMed!FR28OPERACION
    Operacion = rdMed!FRP4OPERACION
    If Not IsNull(rdMed!FR73CODPRODUCTO_DIL) Then rdil = rdMed!FR73CODPRODUCTO_DIL Else rdil = 0
    If Not IsNull(rdMed!FR73CODPRODUCTO_2) Then rprod2 = rdMed!FR73CODPRODUCTO_2 Else rprod2 = 0
    If Not IsNull(rdMed!FR73CODPRODUCTO) Then rprod = rdMed!FR73CODPRODUCTO Else rprod = 0
    Call productos(Operacion, rdil, _
    rprod, rprod2, codProducto1, _
    codProducto2, codProducto3)

    IntTipoOm = ObtenerTipoMedicamento(Operacion, codProducto1, codProducto2, _
    codProducto3)

    Descripcion = ObtenerDescripcionOrden(codProducto1, codProducto2, _
    codProducto3, IntTipoOm)

    ''''key = "P" & rdMed!FR66CODPETICION & "L" & rdMed!FR28NUMLINEA
    Key = "P" & rdMed!FRP4CODPETICION
    ''''Select Case rdMed!FR28OPERACION
    Select Case rdMed!FRP4OPERACION
        Case "/"
            If IntTipoOm = 2 Then
                Set nodo = tvwOM.Nodes.Add("/", tvwChild, Key, Descripcion, constFLUIDOS)
            Else
                Set nodo = tvwOM.Nodes.Add("/", tvwChild, Key, Descripcion, constMEDICAMENTOS)
            End If
'        Case "M"
'            Set nodo = tvwOM.Nodes.Add("M", tvwChild, Key, Descripcion, constFLUIDOS)
        Case "F"
          STR = "SELECT FR00CODGRPTERAP FROM FR7300 WHERE FR73CODPRODUCTO=?"
          Set qy = objApp.rdoConnect.CreateQuery("", STR)
            qy(0) = rdil
          Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          Select Case rd!FR00CODGRPTERAP
          Case "V06C2A"
              Set nodo = tvwOM.Nodes.Add("FNE", tvwChild, Key, Descripcion, constFLUIDOS)
          Case "B05F"
              Set nodo = tvwOM.Nodes.Add("FNP", tvwChild, Key, Descripcion, constFLUIDOS)
          Case Else
              Set nodo = tvwOM.Nodes.Add("FF", tvwChild, Key, Descripcion, constFLUIDOS)
          End Select
          rd.Close
          qy.Close
        Case "P"
          STR = "SELECT FR00CODGRPTERAP FROM FR7300 WHERE FR73CODPRODUCTO=?"
          Set qy = objApp.rdoConnect.CreateQuery("", STR)
            qy(0) = rdil
          Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          Select Case rd!FR00CODGRPTERAP
          Case "V06C2A"
              Set nodo = tvwOM.Nodes.Add("PNE", tvwChild, Key, Descripcion, constPRN)
          Case Else
              Set nodo = tvwOM.Nodes.Add("PF", tvwChild, Key, Descripcion, constPRN)
          End Select
          rd.Close
          qy.Close
'        Case "E"
'            If IntTipoOm = 6 Then
'                Set nodo = tvwOM.Nodes.Add("E", tvwChild, Key, Descripcion, constESTUPEFACIENTES)
'            Else
'                Set nodo = tvwOM.Nodes.Add("E", tvwChild, Key, Descripcion, constESTUPEFACIENTES)
'            End If
    End Select
    nodo.EnsureVisible
    rdMed.MoveNext
Wend
'Rellenar el grid de sueros

Set tvwOM.SelectedItem = nodo.Root
rdMed.Close
qyMed.Close

End Sub

Private Sub RellenarCampos(rdorder As rdoResultset, Key As String, indice As Integer)
Dim lon As Integer
Dim horaInf As Integer
Dim MinInf As Integer
Dim strgrupo$
Dim qygrupo As rdoQuery
Dim rdgrupo As rdoResultset

Select Case Key
Case "/", "E"
    indice = cMED
    Select Case Key
    Case "/"
        fraPrincipal(indice).Caption = "Perfusiones"
        fraPrincipal(indice).ForeColor = &H8000&
    Case "E"
        fraPrincipal(indice).Caption = "Estupefacientes"
        fraPrincipal(indice).ForeColor = &HFF&
    End Select
Case "M"
    indice = cMEZ
    fraPrincipal(indice).ForeColor = &HFF00FF
Case "FF"
    indice = cFLU
    fraPrincipal(indice).Caption = "Fluidoterapia"
    fraPrincipal(indice).ForeColor = &H808000
    Label7(7).Visible = True
    dbVia(2).Visible = True
Case "FNE"
    indice = cFLU
    fraPrincipal(indice).Caption = "Nutrici�n Enteral"
    fraPrincipal(indice).ForeColor = &H808000
    Label7(7).Visible = True
    dbVia(2).Visible = True
Case "FNP"
    indice = cFLU
    fraPrincipal(indice).Caption = "Nutrici�n Parenteral"
    fraPrincipal(indice).ForeColor = &H808000
    Label7(7).Visible = True
    dbVia(2).Visible = True
Case "PNE"
    indice = cFLU
    fraPrincipal(indice).Caption = "PRN Nutrici�n Enteral"
    fraPrincipal(indice).ForeColor = &H8080&
    Label7(7).Visible = False
    dbVia(2).Visible = False
Case "PF"
    indice = cFLU
    fraPrincipal(indice).Caption = "PRN Fluidoterapia"
    fraPrincipal(indice).ForeColor = &H8080&
    Label7(7).Visible = False
    dbVia(2).Visible = False
End Select
'medicamento o Aditivo
If Not IsNull(rdorder!FR73CODPRODUCTO) Then
    lblMedicamento(indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO)
''''    txtDosis(Indice) = rdorder!FR28DOSIS
    If Not IsNull(rdorder!FRP4DOSIS) Then txtDosis(indice) = rdorder!FRP4DOSIS Else txtDosis(indice) = ""
    dbUM(indice) = rdorder!FR93CODUNIMEDIDA
    dbFF(indice) = rdorder!FRH7CODFORMFAR
''''     If Not IsNull(rdorder!FR28VOLUMEN) Then txtVolumen(Indice) = rdorder!FR28VOLUMEN Else txtVolumen(Indice) = ""
     If Not IsNull(rdorder!FRP4VOLUMEN) Then txtVolumen(indice) = rdorder!FRP4VOLUMEN Else txtVolumen(indice) = ""
Else
    lblMedicamento(indice).Caption = ""
    txtDosis(indice) = ""
    dbUM(indice) = ""
    dbFF(indice) = ""
      txtVolumen(indice) = ""
End If

'Medicamento2
If Key = "M" Or (Key = "F" And Not IsNull(rdorder!FR73CODPRODUCTO_2)) Then
    lblMedicamento2(indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_2)
''''    txtDosis2(Indice) = rdorder!FR28DOSIS_2
    txtDosis2(indice) = rdorder!FRP4DOSIS_2
    dbUM2(indice) = rdorder!FR93CODUNIMEDIDA_2
    dbFF2(indice) = rdorder!FRH7CODFORMFAR_2
''''    If Not IsNull(rdorder!FR28VOLUMEN_2) Then txtVolumen2(Indice) = rdorder!FR28VOLUMEN_2 Else txtVolumen2 (Indice)= ""
    If Not IsNull(rdorder!FRP4VOLUMEN_2) Then txtVolumen2(indice) = rdorder!FRP4VOLUMEN_2 Else txtVolumen2(indice) = ""
ElseIf Key = "F" And Not IsNull(rdorder!FR73CODPRODUCTO_2) Then
    lblMedicamento2(indice).Caption = ""
''''    txtDosis2(Indice) = ""
    txtDosis2(indice) = ""
    dbUM2(indice) = ""
    dbFF2(indice) = ""
'''' txtVolumen2(Indice) = ""
    txtVolumen2(indice) = ""
End If

'General
''''txtCantidad(Indice) = rdorder!FR28CANTIDAD
txtCantidad(indice) = rdorder!FRP4CANTIDAD
If Not IsNull(rdorder!FRG4CODFRECUENCIA) Then
  If rdorder!FRG4CODFRECUENCIA = "Manual" Then
    dbFrecuencia(indice) = "Manual (" & rdorder!FRP4HORAS & ")"
  Else: dbFrecuencia(indice) = rdorder!FRG4CODFRECUENCIA
  End If
Else: dbFrecuencia(indice) = ""
End If
dbVia(indice) = rdorder!FR34CODVIA
dbPeriodicidad(indice) = rdorder!FRH5CODPERIODICIDAD
''''dtcFecInicio(Indice) = rdorder!FR28FECINICIO
''''txtHoraInicio(Indice) = rdorder!FR28HORAINICIO
''''If Not IsNull(rdorder!FR28INSTRADMIN) Then
''''    txtObservaciones(Indice) = rdorder!FR28INSTRADMIN
''''Else
''''    txtObservaciones(Indice) = ""
''''End If
''''If Not IsNull(rdorder!FR28HORAFIN) Then
''''    txtHoraFin(Indice) = rdorder!FR28HORAFIN
''''    dtcFecFin(Indice) = rdorder!FR28FECFIN
''''End If
''''If Not IsNull(rdorder!FR28VOLTOTAL) Then txtVolTotal(Indice) = rdorder!FR28VOLTOTAL Else txtVolTotal(Indice) = ""
dtcFecInicio(indice).Date = rdorder!FRP4FECINICIO
dtcFecInicio(indice).ShowCentury = True
txtHoraInicio(indice) = rdorder!FRP4HORAINICIO
If Not IsNull(rdorder!FRP4INSTRADMIN) Then
    txtObservaciones(indice) = rdorder!FRP4INSTRADMIN
Else
    txtObservaciones(indice) = ""
End If
If Not IsNull(rdorder!FRP4HORAFIN) And Not IsNull(rdorder!FRP4FECFIN) Then
    txtHoraFin(indice) = rdorder!FRP4HORAFIN
    dtcFecFin(indice).Date = rdorder!FRP4FECFIN
    dtcFecFin(indice).ShowCentury = True
Else
    txtHoraFin(indice) = ""
    dtcFecFin(indice) = ""
End If
If Not IsNull(rdorder!FRP4VOLTOTAL) Then txtVolTotal(indice) = rdorder!FRP4VOLTOTAL Else txtVolTotal(indice) = ""
'****
'Disolucion
If Not IsNull(rdorder!FR73CODPRODUCTO_DIL) Then
    lblSolucion(indice) = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_DIL)
''''    txtVolumenDil(Indice) = rdorder!FR28CANTIDADDIL
    txtVolumenDil(indice) = rdorder!FRP4CANTIDADDIL
''''    Call HorasMinutos(rdorder!FR28TIEMMININF, horaInf, MinInf)
    Call HorasMinutos(rdorder!FRP4TIEMMININF, horaInf, MinInf)
    'If Val(txtHorainf(indice)) <> 0 Then
    txtHorainf(indice) = horaInf
   ' Else: txtHorainf(indice) = ""
   ' End If
   ' If Val(txtMinInf(indice)) <> 0 Then
    txtMinInf(indice) = MinInf
   ' Else: txtMinInf(indice) = ""
   ' End If
''''    txtvelocidad(Indice) = rdorder!FR28VELPERFUSION
    If Not IsNull(rdorder!FRP4VELPERFUSION) Then
        txtvelocidad(indice) = rdorder!FRP4VELPERFUSION
    End If
    frasolucion(indice).Enabled = True
Else
    If Not IsNull(rdorder!FRP4VELPERFUSION) Then
        txtvelocidad(indice) = rdorder!FRP4VELPERFUSION
    Else: txtvelocidad(indice) = ""
    End If
    lblSolucion(indice) = ""
    txtVolumenDil(indice) = ""
    txtHorainf(indice) = ""
    txtMinInf(indice) = ""
    strgrupo = "SELECT FR7300.FR73CODPRODUCTO " & _
    "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '4000' AND " & _
    "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO and " & _
    "FR7300.FR73CODPRODUCTO= ? " 'grupo perfusiones sin suero
    Set qygrupo = objApp.rdoConnect.CreateQuery("", strgrupo)
        qygrupo(0) = rdorder!FR73CODPRODUCTO
    Set rdgrupo = qygrupo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rdgrupo.RowCount = 0 Then  'medicamento como una linea de medicamento
        frasolucion(indice).Enabled = False
    Else 'es un medicamento que pasa como suero
        frasolucion(indice).Enabled = True
    End If
End If
fraPrincipal(indice).Visible = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
   Cancel = True
End Sub

Private Sub ssdbSueros_AfterInsert(RtnDispErrMsg As Integer)
Dim STR$
Dim qy As rdoQuery
Dim rd As rdoResultset
Dim orden%
If Not blnPrincipio Then
' Por aqui no pasa cuando se carga el grid
  If Not IsNumeric(ssdbSueros.Columns(2).Text) And ssdbSueros.Columns(2).Text <> "" Then
    MsgBox "Introduzca un ritmo num�rico", vbInformation, ""
    Exit Sub
  End If
  If ssdbSueros.Columns(1).Text <> "" Then
    STR = "SELECT MAX(FRP6ORDEN) ORDEN FROM FRP600 WHERE AD01CODASISTENCI=?"
    Set qy = objApp.rdoConnect.CreateQuery("", STR)
    qy(0) = txtAsistencia
    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If IsNull(rd!orden) Then orden = 1 Else orden = (rd!orden + 1)
    rd.Close
    qy.Close
    
    STR = "INSERT INTO FRP600 (AD01CODASISTENCI, FRP6ORDEN, FRP6DESCRIPCION, " & _
    "FRP6VELOCIDAD,FRP6INDACTIVO) VALUES(?,?,?,?,?)"
    Set qy = objApp.rdoConnect.CreateQuery("", STR)
    qy(0) = txtAsistencia
    qy(1) = orden
    qy(2) = ssdbSueros.Columns(1).Text
    qy(3) = ssdbSueros.Columns(2).Text
    qy(4) = True
    qy.Execute
    ssdbSueros.Columns(3).Text = orden
    ssdbSueros.Columns(0).CellStyleSet "", ssdbSueros.Row
    ssdbSueros.ActiveRowStyleSet = ""
  Else
    MsgBox "El suero que ha introducido no tiene nombre." & Chr(13) & _
    "No se ha guardado en la base de datos", vbInformation, ""
  End If
End If
End Sub

Private Sub ssdbSueros_AfterUpdate(RtnDispErrMsg As Integer)
Dim STR$
Dim qy As rdoQuery
If Not blnPrincipio Then
' Por aqui no pasa cuando se carga el grid
  If Not IsNumeric(ssdbSueros.Columns(2).Text) And ssdbSueros.Columns(2).Text <> "" Then
    MsgBox "Introduzca un ritmo num�rico", vbInformation, ""
    ssdbSueros.Columns(2).Text = ""
    Exit Sub
  End If
  If ssdbSueros.Columns(1).Text <> "" Then
    STR = "UPDATE FRP600 SET FRP6DESCRIPCION=?, " & _
    "FRP6VELOCIDAD=?,FRP6INDACTIVO=? WHERE FRP6ORDEN=? AND AD01CODASISTENCI=?"
    Set qy = objApp.rdoConnect.CreateQuery("", STR)
    qy(0) = ssdbSueros.Columns(1).Text
    qy(1) = ssdbSueros.Columns(2).Text
    If ssdbSueros.ActiveRowStyleSet = "" Then
      qy(2) = True
    Else
      qy(2) = False
      ssdbSueros.Columns(0).CellStyleSet "Bueno", ssdbSueros.Row
    End If
    qy(3) = ssdbSueros.Columns(3).Text
    qy(4) = txtAsistencia
    qy.Execute
  Else
    MsgBox "El suero que ha introducido no tiene nombre." & Chr(13) & _
    "No se ha guardado en la base de datos", vbInformation, ""
  End If
End If
End Sub

Private Sub ssdbSueros_click()
Dim STR$
Dim qy As rdoQuery
If ssdbSueros.Col = 0 Then
  If ssdbSueros.ActiveRowStyleSet = "" Then
    ssdbSueros.Columns(0).CellStyleSet "Bueno", ssdbSueros.Row
    ssdbSueros.ActiveRowStyleSet = "Bueno"
  ElseIf ssdbSueros.ActiveRowStyleSet = "Bueno" Then
    ssdbSueros.Columns(0).CellStyleSet "", ssdbSueros.Row
    ssdbSueros.ActiveRowStyleSet = ""
  End If
  ssdbSueros.Col = 1
  STR = "UPDATE FRP600 SET FRP6DESCRIPCION=?, " & _
  "FRP6VELOCIDAD=?,FRP6INDACTIVO=? WHERE FRP6ORDEN=? AND AD01CODASISTENCI=?"
  Set qy = objApp.rdoConnect.CreateQuery("", STR)
  qy(0) = ssdbSueros.Columns(1).Text
  qy(1) = ssdbSueros.Columns(2).Text
  If ssdbSueros.ActiveRowStyleSet = "" Then
  qy(2) = True
  Else: qy(2) = False
  End If
  qy(3) = ssdbSueros.Columns(3).Text
  qy(4) = txtAsistencia
  qy.Execute
End If

End Sub

Private Sub ssdbSueros_LostFocus()
Dim STR$
Dim qy As rdoQuery
Dim rd As rdoResultset
Dim orden%
Dim y%
If ssdbSueros.Columns(3).Text = "" And Trim(ssdbSueros.Columns(1).Text) <> "" Then
    ssdbSueros.MoveNext
End If
End Sub

Private Sub ssdbSueros_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Call ssdbSueros_click
End Sub

Private Sub RellenarGridSueros()
Dim STR$
Dim qy As rdoQuery
Dim rd As rdoResultset
ssdbSueros.Columns(3).Visible = False
ssdbSueros.RemoveAll
STR = "SELECT * FROM FRP600 WHERE AD01CODASISTENCI=? ORDER BY FRP6ORDEN ASC"
Set qy = objApp.rdoConnect.CreateQuery("", STR)
qy(0) = txtAsistencia
Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Dim W$, i%
Dim c As Boolean
Dim Estado As New Collection

blnPrincipio = True
Do While Not rd.EOF
  W = Null & ";" & rd!FRP6DESCRIPCION & ";" & rd!FRP6VELOCIDAD & ";" & rd!FRP6ORDEN
  If rd!FRP6INDACTIVO Then
    Estado.Add Item:=True, Key:=CStr(rd!FRP6ORDEN)
  Else
    Estado.Add Item:=False, Key:=CStr(rd!FRP6ORDEN)
  End If
  ssdbSueros.AddItem W
  rd.MoveNext
Loop
blnPrincipio = False
ssdbSueros.MoveFirst
For i = 0 To ssdbSueros.Rows - 1
    c = Estado.Item(ssdbSueros.Columns(3).Text)
    If c Then
'      ssdbSueros.Columns(0).CellStyleSet "Bueno", i
      ssdbSueros.ActiveRowStyleSet = ""
    Else
      ssdbSueros.Columns(0).CellStyleSet "Bueno", i
      ssdbSueros.ActiveRowStyleSet = "Bueno"
    End If
ssdbSueros.MoveNext
Next
End Sub
Private Sub tvwOM_KeyUp(KeyCode As Integer, Shift As Integer)
Dim Node As Node
Dim i%
Dim CodPeticion&
Dim lon%
Set Node = tvwOM.SelectedItem
On Error Resume Next
    If Node.Parent <> "" Then 'Siempre entra en este if.
    'Vale para que de el error posterior
    End If
    If Err = 0 And KeyCode = 46 And IsNumeric(Mid(Node.Key, 2, 1)) Then 'Se ha seleccionado un hijo
        i = MsgBox("�Desea eliminar esta orden?", vbQuestion + vbYesNo + vbDefaultButton2, "Eliminar")
        If i = vbYes Then
            tvwOM.Nodes.Remove (Node.Index)
            lon = Len(Node.Key)
            CodPeticion = Mid(Node.Key, 2, lon - 1)
            Call UpdateFecFin(CodPeticion, Format(strHora_Sistema, "HH"), strFecha_Sistema)
            Set Node = tvwOM.SelectedItem
            Call tvwOM_NodeClick(Node)
        End If
    End If
End Sub

Private Sub tvwOM_NodeClick(ByVal Node As ComctlLib.Node)
Dim strOrder As String
Dim rdorder As rdoResultset
Dim qyOrder As rdoQuery
Dim lon As Integer
Dim PosL As Integer
Dim indice As Integer
Dim i%
On Error Resume Next
Err = 0
If Node.Parent <> "" Then 'Siempre entra en este if.
'Vale para que de el error posterior
    fraPrincipal(cMED).Visible = False
    fraPrincipal(cFLU).Visible = False
    'fraPrincipal(cMEZ).Visible = False
    fraPrincipal(cMED).Enabled = True
    fraPrincipal(cFLU).Enabled = True
    'fraPrincipal(cMEZ).Enabled = True
    cmdGuardar(cMED).Enabled = False
    cmdGuardar(cFLU).Enabled = False
    'cmdGuardar(cMEZ).Enabled = False
End If

If Err = 0 Then 'Se ha seleccionado un hijo
    Select Case Node.Key
    Case "FF"
        cmdNuevo.Caption = "Nueva Fluidoterapia"
        cmdNuevo.Visible = True
        fraLista.Visible = True
        Call RellenarGridSueros
        GoTo label
    Case "FNE"
        cmdNuevo.Caption = "Nueva Nutrici�n Enteral"
        cmdNuevo.Visible = True
        fraLista.Visible = False
        GoTo label
    Case "FNP"
        cmdNuevo.Caption = "Nueva Nutrici�n Parenteral"
        cmdNuevo.Visible = True
        fraLista.Visible = False
        GoTo label
    Case "PF"
        cmdNuevo.Caption = "PRN Fluidoterapia"
        cmdNuevo.Visible = True
        fraLista.Visible = False
       GoTo label
    Case "PNE"
        cmdNuevo.Caption = "PRN Nutrici�n Enteral"
        cmdNuevo.Visible = True
        fraLista.Visible = False
        GoTo label
    Case Else
        fraLista.Visible = False
    End Select
    cmdNuevo.Visible = False

''''    strOrder = "SELECT * FROM FR2800 WHERE FR66CODPETICION= ? " & _
''''    "AND FR28NUMLINEA= ?"
''''    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
''''        lon = Len(Node.key)
''''        PosL = InStr(1, Node.key, "L")
''''        qyOrder(0) = Mid(Node.key, 2, lon - (lon - PosL + 1) - 1)
''''        qyOrder(1) = Mid(Node.key, PosL + 1, lon - PosL)
    strOrder = "SELECT * FROM FRP400 WHERE FRP4CODPETICION= ? "
    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
        lon = Len(Node.Key)
        qyOrder(0) = Right(Node.Key, lon - 1)
'****
    Set rdorder = qyOrder.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    Call RellenarCampos(rdorder, Node.Parent.Key, indice)
    Call CamposInactivos(indice)
    rdorder.Close
    qyOrder.Close
Else 'Se ha seleccionado un padre
    Select Case Node.Key
    Case "/"
        cmdNuevo.Caption = "Nueva Perfusi�n"
        cmdNuevo.Visible = True
        fraLista.Visible = False
'    Case "M"
'        cmdNuevo.Caption = "Nueva Mezcla"
'        cmdNuevo.Visible = True
'        cmdNuevo.Visible = False
    Case "P"
'        cmdNuevo.Caption = "Nuevo PRN"
'        cmdNuevo.Visible = True
       cmdNuevo.Visible = False
        fraLista.Visible = False
    Case "F"
'        cmdNuevo.Caption = "Nuevo Fluido"
        cmdNuevo.Visible = False
        fraLista.Visible = False
'    Case "E"
'        cmdNuevo.Visible = False
    End Select
    'Inicializo las variables
End If
label:
volumenIni = 0
DosisIni = 0
VolumenDilIni = 0
volumen2 = 0
frmHoras.Visible = False
For i = 0 To 23
    cmdHoras(i).BackColor = Gris
Next i
Err = 0
End Sub

Private Sub txtCantidad_GotFocus(Index As Integer)
  txtCantidad(Index).SelLength = Len(txtCantidad(Index).Text)
End Sub

Private Sub txtCantidad_LostFocus(Index As Integer)
If txtCantidad(Index) = "" Then txtCantidad(Index) = 0
If MascaraNumero(txtCantidad(Index)) = True Then Exit Sub
    If Index <> cFLU Then
        txtDosis(Index) = txtCantidad(Index) * DosisIni
    Else
        txtVolumenDil(Index) = txtCantidad(Index) * VolumenDilIni
        Call CalcularVolumen(Index)
    End If
End Sub

Private Sub txtDosis_change(Index As Integer)
    If DosisIni <> 0 Then
        txtVolumen(Index) = (Val(txtDosis(Index)) * volumenIni) / DosisIni
    Else
        txtVolumen(Index) = 0
    End If
End Sub

Private Sub txtDosis_GotFocus(Index As Integer)
  txtDosis(Index).SelLength = Len(txtDosis(Index).Text)
End Sub

Private Sub txtDosis_LostFocus(Index As Integer)
If txtDosis(Index) = "" Then txtDosis(Index) = 0
If MascaraNumero(txtDosis(Index)) = True Then Exit Sub
If DosisIni <> 0 Then
        If Index <> cFLU Then
            txtCantidad(Index) = txtDosis(Index) / DosisIni
        End If
End If
End Sub

Private Sub txtDosis2_Change(Index As Integer)
    If dbMedicamento2(Index).Columns(1).Value <> "" Then
        txtVolumen2(Index) = (Val(txtDosis2(Index)) * volumen2) / dbMedicamento2(Index).Columns(1).Value
    Else
        txtVolumen2(Index) = 0
    End If
End Sub



Private Sub txtHoraFin_Change(Index As Integer)
    cmdGuardar(Index).Enabled = True
End Sub

Private Sub txtHoraFin_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    blnCambio = True
    If KeyCode = 13 Then
        cmdGuardar(Index).SetFocus
    End If
End Sub

Private Sub txtHoraFin_LostFocus(Index As Integer)
If txtHoraFin(Index) <> "" Then
  Call MascaraHora(txtHoraFin(Index))
End If
End Sub

Private Sub txtHorainf_GotFocus(Index As Integer)
  txtHorainf(Index).SelLength = Len(txtHorainf(Index).Text)
End Sub

Private Sub txtHorainf_LostFocus(Index As Integer)
Dim Min As Long
If txtHorainf(Index) = "" Then txtHorainf(Index) = 0
If txtMinInf(Index) = "" Then txtMinInf(Index) = 0
If txtVolumenDil(Index) = "" Then txtVolumenDil(Index) = 0
Min = txtMinInf(Index) + txtHorainf(Index) * 60
    If Min <> 0 Then
        txtvelocidad(Index) = Format(txtVolumenDil(Index) / (Min / 60), "#0.00")
    Else
        txtHorainf(Index) = ""
        txtMinInf(Index) = ""
        txtvelocidad(Index).SetFocus
    End If
End Sub



Private Sub txtHoraInicio_LostFocus(Index As Integer)
If txtHoraInicio(Index) <> "" Then
  Call MascaraHora(txtHoraInicio(Index))
End If
End Sub

Private Sub txtMinInf_GotFocus(Index As Integer)
  txtMinInf(Index).SelLength = Len(txtMinInf(Index).Text)
End Sub

Private Sub txtMinInf_LostFocus(Index As Integer)
Dim Min As Long
If txtHorainf(Index) = "" Then txtHorainf(Index) = 0
If txtMinInf(Index) = "" Then txtMinInf(Index) = 0
If txtVolumenDil(Index) = "" Then txtVolumenDil(Index) = 0
    Min = txtMinInf(Index) + txtHorainf(Index) * 60
    If Min <> 0 Then
        txtvelocidad(Index) = Format(txtVolumenDil(Index) / (Min / 60), "#0.00")
    Else
        txtHorainf(Index) = ""
        txtMinInf(Index) = ""
        txtvelocidad(Index).SetFocus
    End If
End Sub

Private Sub txtObservaciones_Change(Index As Integer)
    If blnCambio Then
        cmdGuardar(Index).Enabled = True
    End If
    blnCambio = False
End Sub

Private Sub txtObservaciones_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    blnCambio = True
    If KeyCode = 13 Then
        cmdGuardar(Index).SetFocus
    End If
End Sub

Private Sub txtvelocidad_Change(Index As Integer)
    If blnCambio Then
     cmdGuardar(Index).Enabled = True
    End If
    blnCambio = False

End Sub

Private Sub txtvelocidad_GotFocus(Index As Integer)
  txtvelocidad(Index).SelLength = Len(txtvelocidad(Index).Text)
End Sub

Private Sub txtvelocidad_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    blnCambio = True
    If KeyCode = 13 Then
        cmdGuardar(Index).SetFocus
    End If
End Sub

Private Sub txtVelocidad_LostFocus(i As Integer)
Dim Min As Long
Dim horaInf As Integer
Dim MinInf As Integer
  If txtvelocidad(i) = "" Then txtvelocidad(i) = 0
  If MascaraNumero(txtvelocidad(i)) = True Then Exit Sub
  txtvelocidad(i) = Format(txtvelocidad(i), "#0.00")
  'Tiene un link con tiempo de infusion si no es un pefusion
  If i <> cMED And fraPrincipal(i).Caption <> "Medicamento o Perfusion" Then
      If txtvelocidad(i) <> 0 And txtVolumenDil(i) <> "" Then
          Min = CInt((txtVolumenDil(i) / txtvelocidad(i)) * 60)
          Call HorasMinutos(Min, horaInf, MinInf)
          txtHorainf(i) = horaInf
          txtMinInf(i) = MinInf
      End If
  End If
End Sub

Private Sub CargarComboFF(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
  If dbFF(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
      STR = "SELECT FRH7CODFORMFAR, FRH7DESFORMFAR FROM FRH700 ORDER BY FRH7DESFORMFAR ASC"
      Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
      While Not rd.EOF
      WW = rd!FRH7CODFORMFAR & "; " & rd!FRH7DESFORMFAR
          dbFF(indice).AddItem WW
          rd.MoveNext
      Wend
      rd.Close
      dbFF(indice).Columns(0).Value = ""
  End If
End Sub

Private Sub CargarComboFF2(Index As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
  If dbFF2(Index).Rows = 0 Then 'Si no hay nada en el combo se carga
      STR = "SELECT FRH7CODFORMFAR, FRH7DESFORMFAR FROM FRH700 ORDER BY FRH7DESFORMFAR ASC"
      Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
      While Not rd.EOF
      WW = rd!FRH7CODFORMFAR & "; " & rd!FRH7DESFORMFAR
          dbFF2(Index).AddItem WW
          rd.MoveNext
      Wend
      rd.Close
      dbFF2(Index).Columns(0).Value = ""
  End If
End Sub

Private Sub CargarComboVIA(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
If dbVia(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
    STR = "SELECT FR34CODVIA, FR34DESVIA FROM FR3400 ORDER BY FR34DESVIA ASC"
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
'        If Indice <> cFLU Then
'            WW = RD!FR34CODVIA & "; " & RD!FR34DESVIA
'            dbVia(Indice).AddItem WW
'        Else
            If rd!FR34CODVIA = "PC" Then 'Or RD!FR34CODVIA = "PIN"
                WW = rd!FR34CODVIA & "; " & rd!FR34DESVIA
                dbVia(indice).AddItem WW
            End If
'        End If
        rd.MoveNext
    Wend
    dbVia(indice).Columns(0).Value = 0
    rd.Close
End If
End Sub

Private Sub CargarComboPeriodicidad(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
If dbPeriodicidad(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
    STR = "SELECT FRH5CODPERIODICIDAD, FRH5DESPERIODICIDAD FROM FRH500 ORDER BY FRH5CODPERIODICIDAD ASC"
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
        WW = rd!FRH5CODPERIODICIDAD & "; " & rd!FRH5DESPERIODICIDAD
          If rd!FRH5CODPERIODICIDAD <> "Agenda" Then
            dbPeriodicidad(indice).AddItem WW
          End If
            rd.MoveNext
    Wend
    dbPeriodicidad(indice).Columns(0).Value = 0
    rd.Close
End If
End Sub

Private Sub CargarComboFrecuencia(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
If dbFrecuencia(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
    STR = "SELECT FRG4CODFRECUENCIA, FRG4DESFRECUENCIA FROM FRG400 ORDER BY FRG4CODFRECUENCIA ASC"
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    dbFrecuencia(indice).AddItem "Manual; Frecuencia Manual"
    While Not rd.EOF
        WW = rd!FRG4CODFRECUENCIA & "; " & rd!FRG4DESFRECUENCIA
        dbFrecuencia(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbFrecuencia(indice).Columns(0).Value = 0
End If
End Sub

Private Sub CargarComboUM(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
If dbUM(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
    STR = "SELECT FR93CODUNIMEDIDA, FR93DESUNIMEDIDA FROM FR9300 ORDER BY FR93CODUNIMEDIDA ASC"
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
        If rd!FR93CODUNIMEDIDA = "mg" Or rd!FR93CODUNIMEDIDA = "mcg" Or _
        rd!FR93CODUNIMEDIDA = "g" Or rd!FR93CODUNIMEDIDA = "ui" Or _
        rd!FR93CODUNIMEDIDA = "ml" Or rd!FR93CODUNIMEDIDA = "amp" _
        Or rd!FR93CODUNIMEDIDA = "mEq" Then
            WW = rd!FR93CODUNIMEDIDA & "; " & rd!FR93DESUNIMEDIDA
                dbUM(indice).AddItem WW
        End If
        rd.MoveNext
    Wend
    rd.Close
    dbUM(indice).Columns(0).Value = ""
End If
End Sub

Private Sub CargarComboUM2(Index As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
If dbUM2(Index).Rows = 0 Then 'Si no hay nada en el combo se carga
    STR = "SELECT FR93CODUNIMEDIDA, FR93DESUNIMEDIDA FROM FR9300 ORDER BY FR93CODUNIMEDIDA ASC"
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
        If rd!FR93CODUNIMEDIDA = "mg" Or rd!FR93CODUNIMEDIDA = "mcg" Or _
        rd!FR93CODUNIMEDIDA = "g" Or rd!FR93CODUNIMEDIDA = "ui" Or _
        rd!FR93CODUNIMEDIDA = "ml" Or rd!FR93CODUNIMEDIDA = "amp" _
        Or rd!FR93CODUNIMEDIDA = "mEq" Then
            WW = rd!FR93CODUNIMEDIDA & "; " & rd!FR93DESUNIMEDIDA
                dbUM2(Index).AddItem WW
        End If
        rd.MoveNext
    Wend
    rd.Close
    dbUM2(Index).Columns(0).Value = ""
End If
End Sub

Private Sub CargarComboSolucion(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String

If dbSolucion(indice).Rows = 0 Then 'Si no hay nada en el combo se carga

       STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73VOLUMEN " & _
      "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '5000' AND " & _
      "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
      "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de Fluidoterapia Picis
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
    WW = rd!FR73DESPRODUCTO & "; " & rd!FR73VOLUMEN & "; " & rd!FR73CODPRODUCTO
        dbSolucion(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbSolucion(indice).Columns(2).Value = 0
End If
End Sub
Private Sub CargarComboSolucionPF(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String

If dbSolucion(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
       STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73VOLUMEN " & _
      "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '6000' AND " & _
      "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
      "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de Fluidoterapia PRN Picis
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
    WW = rd!FR73DESPRODUCTO & "; " & rd!FR73VOLUMEN & "; " & rd!FR73CODPRODUCTO
        dbSolucion(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbSolucion(indice).Columns(2).Value = 0
End If
End Sub
Private Sub CargarComboSolucionNE(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String

If dbSolucion(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
'    str = "SELECT FR73CODPRODUCTO, FR73DESPRODUCTO, FR73VOLUMEN " & _
'    "FROM FR7300 WHERE FR00CODGRPTERAP= ? " & _
'    "ORDER BY FR73DESPRODUCTO ASC"
'    Set qy = objApp.rdoConnect.CreateQuery("", str)
'    qy(0) = "V06C2A"
     STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73VOLUMEN " & _
    "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '7000' AND " & _
    "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
    "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de NE Picis
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
    WW = rd!FR73DESPRODUCTO & "; " & rd!FR73VOLUMEN & "; " & rd!FR73CODPRODUCTO
        dbSolucion(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbSolucion(indice).Columns(2).Value = 0
End If
End Sub
Private Sub CargarComboSolucionNEPRN(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String

If dbSolucion(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
     STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73VOLUMEN " & _
    "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '8000' AND " & _
    "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
    "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de NE PRN Picis
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
    WW = rd!FR73DESPRODUCTO & "; " & rd!FR73VOLUMEN & "; " & rd!FR73CODPRODUCTO
        dbSolucion(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbSolucion(indice).Columns(2).Value = 0
End If
End Sub

Private Sub CargarComboSolucionNP(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim WW As String

If dbSolucion(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
    STR = "SELECT FR73CODPRODUCTO, FR73DESPRODUCTO, FR73VOLUMEN " & _
    "FROM FR7300 WHERE FR00CODGRPTERAP= ? " & _
    "ORDER BY FR73DESPRODUCTO ASC"
    Set qy = objApp.rdoConnect.CreateQuery("", STR)
    qy(0) = "B05F"
    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
    WW = rd!FR73DESPRODUCTO & "; " & rd!FR73VOLUMEN & "; " & rd!FR73CODPRODUCTO
        dbSolucion(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbSolucion(indice).Columns(2).Value = 0
End If
End Sub
Private Sub CargarComboMedicamento(indice As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String
If dbMedicamento(indice).Rows = 0 Then 'Si no hay nada en el combo se carga
    If indice <> cFLU Then
'        STR = "SELECT FR73CODPRODUCTO, FR73DESPRODUCTO, FR73DOSIS,FR93CODUNIMEDIDA, " & _
'        "FRH7CODFORMFAR, FR34CODVIA FROM FR7300 WHERE FR73INDINFIV = 0  AND " & _
'        "FR73CODPRODUCTO IN (705,966,643,1116,771,12,13,357,1355,1353,1358,1552,4449, " & _
'        "961,1560,922,746,834,1515,948,329,435,1032,957,1342,4471,4571) " & _
'        "ORDER BY FR73DESPRODUCTO ASC"
         STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73DOSIS,FR7300.FR93CODUNIMEDIDA, " & _
        "FR7300.FRH7CODFORMFAR, FR7300.FR34CODVIA FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '2000' AND " & _
        "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
        "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de Perfusiones Picis
    Else 'SI SON ADITIVOS PASAN MENOS PRODUCTOS
'        STR = "SELECT FR73CODPRODUCTO, FR73DESPRODUCTO, FR73DOSIS,FR93CODUNIMEDIDA, " & _
'        "FRH7CODFORMFAR, FR34CODVIA FROM FR7300 WHERE FR73INDINFIV = 0  AND " & _
'        "FR73CODPRODUCTO IN (1424,4473,689,4471) " & _
'        "ORDER BY FR73DESPRODUCTO ASC"
         STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73DOSIS,FR7300.FR93CODUNIMEDIDA, " & _
        "FR7300.FRH7CODFORMFAR, FR7300.FR34CODVIA FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '3000' AND " & _
        "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
        "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de Aditivos Picis
    End If
    Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)

    While Not rd.EOF
    WW = rd!FR73DESPRODUCTO & "; " & rd!FR73DOSIS & "; " & rd!FR93CODUNIMEDIDA & _
    "; " & rd!FRH7CODFORMFAR & "; " & rd!FR34CODVIA & "; " & rd!FR73CODPRODUCTO
        dbMedicamento(indice).AddItem WW
        rd.MoveNext
    Wend
    rd.Close
    dbMedicamento(indice).Columns(5).Value = ""
End If
End Sub

Private Sub CargarComboMedicamento2(Index As Integer)
Dim STR As String
Dim rd As rdoResultset
Dim WW As String

If dbMedicamento2(Index).Rows = 0 Then 'Si no hay nada en el combo se carga
    Select Case Index
    Case cFLU
'        STR = "SELECT FR73CODPRODUCTO, FR73DESPRODUCTO, FR73DOSIS,FR93CODUNIMEDIDA, " & _
'        "FRH7CODFORMFAR, FR34CODVIA FROM FR7300 WHERE FR73INDINFIV = 0  AND " & _
'        "FR73CODPRODUCTO IN (1424,4473,689,4471) " & _
'        "ORDER BY FR73DESPRODUCTO ASC"
         STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73DOSIS,FR7300.FR93CODUNIMEDIDA, " & _
        "FR7300.FRH7CODFORMFAR, FR7300.FR34CODVIA FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '3000' AND " & _
        "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
        "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de Aditivos Picis

    Case Else
'        STR = "SELECT FR73CODPRODUCTO, FR73DESPRODUCTO, FR73DOSIS,FR93CODUNIMEDIDA, " & _
'        "FRH7CODFORMFAR, FR34CODVIA FROM FR7300 WHERE FR73INDINFIV = 0  AND " & _
'        "FR73CODPRODUCTO IN (705,966,643,1116,771,12,13,357,1355,1353,1358,1552,4449, " & _
'        "961,1560,922,746,834,1515,948,329,435,1032,957,1342,4471,4571) " & _
'        "ORDER BY FR73DESPRODUCTO ASC"
         STR = "SELECT FR7300.FR73CODPRODUCTO, FR7300.FR73DESPRODUCTO, FR7300.FR73DOSIS,FR7300.FR93CODUNIMEDIDA, " & _
        "FR7300.FRH7CODFORMFAR, FR7300.FR34CODVIA FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '2000' AND " & _
        "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO " & _
        "ORDER BY FR7300.FR73DESPRODUCTO ASC" 'Grupo de Perfusiones Picis

    End Select
Set rd = objApp.rdoConnect.OpenResultset(STR, rdOpenKeyset, rdConcurReadOnly)

While Not rd.EOF
WW = rd!FR73DESPRODUCTO & "; " & rd!FR73DOSIS & "; " & rd!FR93CODUNIMEDIDA & _
"; " & rd!FRH7CODFORMFAR & "; " & rd!FR34CODVIA & "; " & rd!FR73CODPRODUCTO
    dbMedicamento2(Index).AddItem WW
    rd.MoveNext
Wend
rd.Close
dbMedicamento2(Index).Columns(5).Value = ""
End If
End Sub

Private Sub CamposInactivos(indice As Integer)
'Pone en modo "inactivo" la pantalla, excepto algunos campos.
cmdGuardar(indice).Enabled = False
'medicamento o Aditivo
'lblMedicamento(Indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO)
dbMedicamento(indice).Visible = False
txtDosis(indice).Locked = True
txtDosis(indice).BackColor = Gris
dbUM(indice).Enabled = False
dbUM(indice).BackColor = Gris
dbFF(indice).Enabled = False
dbFF(indice).BackColor = Gris
txtVolumen(indice).Locked = True
txtVolumen(indice).BackColor = Gris

'Medicamento2
If indice = cMEZ Or indice = cFLU Then
    'lblMedicamento2(Indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_2)
    dbMedicamento2(indice).Visible = False
    txtDosis2(indice).Locked = True
    txtDosis2(indice).BackColor = Gris
    dbUM2(indice).Enabled = False
    dbUM2(indice).BackColor = Gris
    dbFF2(indice).Enabled = False
    dbFF2(indice).BackColor = Gris
    txtVolumen2(indice).Locked = True
    txtVolumen2(indice).BackColor = Gris
End If

'General
txtCantidad(indice).Locked = True
txtCantidad(indice).BackColor = Gris
dbFrecuencia(indice).Enabled = False
dbFrecuencia(indice).BackColor = Gris
dbVia(indice).Enabled = False
dbVia(indice).BackColor = Gris
dbPeriodicidad(indice).Enabled = False
dbPeriodicidad(indice).BackColor = Gris
dtcFecInicio(indice).AllowEdit = False
dtcFecInicio(indice).Enabled = False
dtcFecInicio(indice).BackColor = Gris
txtHoraInicio(indice).Locked = False
txtHoraInicio(indice).BackColor = Gris
txtObservaciones(indice).Locked = True
txtObservaciones(indice).BackColor = Gris
txtHoraFin(indice).Locked = True
txtHoraFin(indice).BackColor = Gris
dtcFecFin(indice).BackColor = Gris
dtcFecFin(indice).Enabled = False
dtcFecFin(indice).AllowEdit = False
txtVolTotal(indice).Locked = True
txtVolTotal(indice).BackColor = Gris

'Disolucion
'lblSolucion(Indice) = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_DIL)
dbSolucion(indice).Visible = False
txtVolumenDil(indice).Locked = True
txtVolumenDil(indice).BackColor = Gris
txtHorainf(indice).Locked = True
txtHorainf(indice).BackColor = Gris
txtMinInf(indice).Locked = False
txtMinInf(indice).BackColor = Gris
If indice <> 0 Then
    txtvelocidad(indice).Locked = True
    txtvelocidad(indice).BackColor = Gris
Else
    txtvelocidad(indice).Locked = False
    txtvelocidad(indice).BackColor = Blanco
End If

End Sub

Public Sub BorrarCampos(indice As Integer)
'medicamento o Aditivo
lblMedicamento(indice).Caption = ""
dbMedicamento(indice) = ""
dbMedicamento(indice).Columns(5).Value = Null
txtDosis(indice) = ""
dbUM(indice) = ""
dbFF(indice) = ""
txtVolumen(indice) = ""
'Medicamento2
If indice = cMEZ Or indice = cFLU Then
    lblMedicamento2(indice).Caption = ""
    dbMedicamento2(indice) = ""
    dbMedicamento2(indice).Columns(5).Value = Null
    txtDosis2(indice) = ""
    dbUM2(indice) = ""
    dbFF2(indice) = ""
    txtVolumen2(indice) = ""
End If

'General
txtCantidad(indice) = ""
dbFrecuencia(indice) = ""
dbFrecuencia(indice).Columns(0).Value = Null
dbVia(indice) = ""
dbVia(indice).Columns(0).Value = Null
dbPeriodicidad(indice) = ""
dbPeriodicidad(indice).Columns(0).Value = Null
dtcFecInicio(indice) = ""
txtHoraInicio(indice) = ""
txtObservaciones(indice) = ""
txtHoraFin(indice) = ""
dtcFecFin(indice) = ""
txtVolTotal(indice) = ""

'Disolucion
lblSolucion(indice) = ""
dbSolucion(indice) = ""
dbSolucion(indice).Columns(2).Value = Null
txtVolumenDil(indice) = ""
txtHorainf(indice) = ""
txtMinInf(indice) = ""
txtvelocidad(indice) = ""
End Sub

Private Sub ActivarCampos(indice As Integer)
'medicamento o Aditivo
If indice = cFLU Then
'lblMedicamento(Indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO)
    dbMedicamento(indice).Visible = True
    dbMedicamento(indice).BackColor = Blanco
    txtDosis(indice).Locked = False
    txtDosis(indice).BackColor = Blanco
    dbUM(indice).Enabled = True
    dbUM(indice).BackColor = Blanco
    dbFF(indice).Enabled = True
    dbFF(indice).BackColor = Blanco
    txtVolumen(indice).Locked = False 'true
    txtVolumen(indice).BackColor = Blanco 'gris
Else
    dbMedicamento(indice).Visible = True
    dbMedicamento(indice).BackColor = Azul
    txtDosis(indice).Locked = False
    txtDosis(indice).BackColor = Azul
    dbUM(indice).Enabled = True
    dbUM(indice).BackColor = Azul
    dbFF(indice).Enabled = True
    dbFF(indice).BackColor = Azul
    txtVolumen(indice).Locked = False 'true
    txtVolumen(indice).BackColor = Azul 'gris
End If

'Medicamento2
If indice = cMEZ Then
    'lblMedicamento2(Indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_2)
    dbMedicamento2(indice).Visible = True
    dbMedicamento2(indice).BackColor = Azul
    txtDosis2(indice).Locked = False
    txtDosis2(indice).BackColor = Azul
    dbUM2(indice).Enabled = True
    dbUM2(indice).BackColor = Azul
    dbFF2(indice).Enabled = True
    dbFF2(indice).BackColor = Azul
    txtVolumen2(indice).Locked = False
    txtVolumen2(indice).BackColor = Blanco
End If
If indice = cFLU Then
    'lblMedicamento2(Indice).Caption = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_2)
    dbMedicamento2(indice).Visible = True
    dbMedicamento2(indice).BackColor = Blanco
    txtDosis2(indice).Locked = False
    txtDosis2(indice).BackColor = Blanco
    dbUM2(indice).Enabled = True
    dbUM2(indice).BackColor = Blanco
    dbFF2(indice).Enabled = True
    dbFF2(indice).BackColor = Blanco
    txtVolumen2(indice).Locked = False
    txtVolumen2(indice).BackColor = Blanco
End If

'General
txtCantidad(indice).Locked = False
txtCantidad(indice).BackColor = Azul
dbFrecuencia(indice).Enabled = True
dbFrecuencia(indice).BackColor = Blanco
dbVia(indice).Enabled = True
dbVia(indice).BackColor = Azul
dbPeriodicidad(indice).Enabled = True
dbPeriodicidad(indice).BackColor = Azul
dtcFecInicio(indice).AllowEdit = True
dtcFecInicio(indice).BackColor = Azul
dtcFecInicio(indice).Enabled = True
txtHoraInicio(indice).Locked = False
txtHoraInicio(indice).BackColor = Azul
txtObservaciones(indice).Locked = False
txtObservaciones(indice).BackColor = Blanco
txtHoraFin(indice).Locked = False
txtHoraFin(indice).BackColor = Blanco
dtcFecFin(indice).BackColor = Blanco
dtcFecFin(indice).AllowEdit = True
dtcFecFin(indice).Enabled = True
txtVolTotal(indice).Locked = False
txtVolTotal(indice).BackColor = Gris

'Disolucion
Select Case indice
Case cMEZ, cFLU
    'lblSolucion(Indice) = ObtenerDesProducto(rdorder!FR73CODPRODUCTO_DIL)
    dbSolucion(indice).Visible = True
    dbSolucion(indice).BackColor = Azul
    txtVolumenDil(indice).Locked = False
    txtVolumenDil(indice).BackColor = Azul 'gris
    Select Case tvwOM.SelectedItem.Key
    Case "PNE", "PF"
      txtHorainf(indice).Locked = False
      txtHorainf(indice).BackColor = Blanco
      txtMinInf(indice).Locked = False
      txtMinInf(indice).BackColor = Blanco
      txtvelocidad(indice).Locked = False
      txtvelocidad(indice).BackColor = Blanco
    Case Else
      txtHorainf(indice).Locked = False
      txtHorainf(indice).BackColor = Azul
      txtMinInf(indice).Locked = False
      txtMinInf(indice).BackColor = Azul
      txtvelocidad(indice).Locked = False
      txtvelocidad(indice).BackColor = Azul
    End Select
Case cMED
    dbSolucion(indice).Visible = True
    dbSolucion(indice).BackColor = Blanco
    txtVolumenDil(indice).Locked = False ' True
    txtVolumenDil(indice).BackColor = Blanco 'Gris
    txtHorainf(indice).Locked = True
    txtHorainf(indice).BackColor = Gris
    txtMinInf(indice).Locked = True
    txtMinInf(indice).BackColor = Gris
    txtvelocidad(indice).Locked = False
    txtvelocidad(indice).BackColor = Blanco
End Select
cmdNuevo.Visible = False
cmdGuardar(indice).Enabled = True
frasolucion(indice).Enabled = True
  Select Case tvwOM.SelectedItem.Key
  Case "FNE", "FNP", "PNE"
      fraFAditivos(0).Enabled = False
      fraFAditivos(1).Enabled = False
  Case Else
    fraFAditivos(0).Enabled = True
    fraFAditivos(1).Enabled = True
  End Select
  If tvwOM.SelectedItem.Key = "PNE" Or tvwOM.SelectedItem.Key = "PF" Then
    Label7(7).Visible = False
    dbVia(2).Visible = False
  Else
    Label7(7).Visible = True
    dbVia(2).Visible = True
  End If
End Sub

Private Sub CargarCombos(indice As String)
     CargarComboVIA (indice)
     CargarComboFF (indice)
     CargarComboUM (indice)
     CargarComboPeriodicidad (indice)
     CargarComboFrecuencia (indice)
     CargarComboMedicamento (indice)
    If indice = cMEZ Or indice = cFLU Then
         CargarComboMedicamento2 (indice)
         CargarComboFF2 (indice)
         CargarComboUM2 (indice)
    End If
    dbSolucion(indice).RemoveAll
    Select Case tvwOM.SelectedItem.Key
    Case "FNE"
      CargarComboSolucionNE (indice)
    Case "PNE"
      CargarComboSolucionNEPRN (indice)
    Case "FNP"
      CargarComboSolucionNP (indice)
    Case "PF", "/"
      CargarComboSolucionPF (indice)
    Case Else
      CargarComboSolucion (indice)
    End Select
End Sub

Private Sub txtVolumen_Change(Index As Integer)
    Call CalcularVolumen(Index)
End Sub

Private Sub txtVolumen2_Change(Index As Integer)
    Call CalcularVolumen(Index)
End Sub

Private Sub txtVolumenDil_GotFocus(Index As Integer)
  txtVolumenDil(Index).SelLength = Len(txtVolumenDil(Index).Text)
End Sub

Private Sub txtVolumenDil_lostfocus(Index As Integer)
Dim Min As Long
    If txtVolumenDil(Index) = "" Then txtVolumenDil(Index) = 0
    If MascaraNumero(txtVolumenDil(Index)) = True Then Exit Sub
    Call CalcularVolumen(Index)
    If txtDosis(Index) = "" Then txtDosis(Index) = 0
    If Index = cFLU Then
        If txtHorainf(Index) = "" Then txtHorainf(Index) = 0
        If txtMinInf(Index) = "" Then txtMinInf(Index) = 0
'        If txtVolumenDil(Index) = "" Then txtVolumenDil(Index) = 0
        Min = txtMinInf(Index) + txtHorainf(Index) * 60

        If VolumenDilIni > 0 Then txtCantidad(Index) = txtDosis(Index) / VolumenDilIni
        If Min > 0 Then
            txtvelocidad(Index) = Format(txtVolumenDil(Index) / (Min / 60), "#0.00")
        End If
    End If
End Sub
Private Sub CalcularVolumen(Index As Integer)
Dim V As Long
Dim VD As Long
Dim V2 As Long
If txtVolumen(Index) <> "" Then
    V = CLng(txtVolumen(Index))
Else
    V = 0
End If
If txtVolumenDil(Index) <> "" Then
    VD = CLng(txtVolumenDil(Index))
Else
    VD = 0
End If
If Index = cMEZ Then
    If txtVolumen2(Index) <> "" Then
        V2 = CLng(txtVolumen2(Index))
    Else
        V2 = 0
    End If
End If
txtVolTotal(Index) = V + VD + V2
End Sub

Private Function HayAlgoAzul(indice As Integer) As Boolean
'Comprueba que todos los campos azules esten
'medicamento o Aditivo
    If dbMedicamento(indice).BackColor = Azul And dbMedicamento(indice).Columns(5).Value = "" Then
        HayAlgoAzul = True
    End If
    If txtDosis(indice).BackColor = Azul And txtDosis(indice) = "" Then
        HayAlgoAzul = True
    End If
    If dbUM(indice).BackColor = Azul And dbUM(indice) = "" Then
        HayAlgoAzul = True
    End If
    If dbFF(indice).BackColor = Azul And dbFF(indice) = "" Then
         HayAlgoAzul = True
    End If
   If txtVolumen(indice).BackColor = Azul And txtVolumen(indice) = "" Then
        HayAlgoAzul = True
    End If


'Medicamento2
If indice = cMEZ Or indice = cFLU Then
    If dbMedicamento2(indice).BackColor = Azul And dbMedicamento2(indice).Columns(5).Value = "" Then
        HayAlgoAzul = True
    End If
    If txtDosis2(indice).BackColor = Azul And txtDosis2(indice) = "" Then
        HayAlgoAzul = True
    End If
    If dbUM2(indice).BackColor = Azul And dbUM2(indice) = "" Then
        HayAlgoAzul = True
    End If
    If dbFF2(indice).BackColor = Azul And dbFF2(indice) = "" Then
        HayAlgoAzul = True
    End If
    If txtVolumen2(indice).BackColor = Azul And txtVolumen2(indice) = "" Then
         HayAlgoAzul = True
   End If
End If

'General
'If txtCantidad(Indice).BackColor = Azul And txtCantidad(Indice) = "" Then
'         HayAlgoAzul = True
'End If
If dbFrecuencia(indice).BackColor = Azul And dbFrecuencia(indice) = "" Then
         HayAlgoAzul = True
End If
If dbVia(indice).BackColor = Azul And dbVia(indice) = "" Then
         HayAlgoAzul = True
End If
If dbPeriodicidad(indice).BackColor = Azul And dbPeriodicidad(indice) = "" Then
        HayAlgoAzul = True
End If
If dtcFecInicio(indice).BackColor = Azul And dtcFecInicio(indice) = "" Then
        HayAlgoAzul = True
End If
If txtHoraInicio(indice).BackColor = Azul And txtHoraInicio(indice) = "" Then
        HayAlgoAzul = True
End If
If txtObservaciones(indice).BackColor = Azul And txtObservaciones(indice) = "" Then
        HayAlgoAzul = True
End If
If txtHoraFin(indice).BackColor = Azul And txtHoraFin(indice) = "" Then
        HayAlgoAzul = True
End If
If dtcFecFin(indice).BackColor = Azul And dtcFecFin(indice) = "" Then
        HayAlgoAzul = True
End If
If txtVolTotal(indice).BackColor = Gris And txtVolTotal(indice) = "" Then
        HayAlgoAzul = True
End If

'Disolucion

If dbSolucion(indice).BackColor = Azul And dbSolucion(indice).Columns(2).Value = "" Then
        HayAlgoAzul = True
End If
If txtVolumenDil(indice).BackColor = Azul And txtVolumenDil(indice) = "" Then
        HayAlgoAzul = True
End If
If txtHorainf(indice).BackColor = Azul And txtHorainf(indice) = "" Then
        HayAlgoAzul = True
End If
If txtMinInf(indice).BackColor = Azul And txtMinInf(indice) = "" Then
        HayAlgoAzul = True
End If
If txtvelocidad(indice).BackColor = Azul And txtvelocidad(indice) = "" Then
        HayAlgoAzul = True
End If
End Function

Private Function InsertarFRP400(Index As Integer) As Long
Dim strFRP400 As String
Dim qyFRP400 As rdoQuery
Dim Node As Node
Dim CodPeticion As Long
Dim strHoras As String
Dim lon%
Dim i%
Dim strgrupo$
Dim rdgrupo As rdoResultset
Dim qygrupo As rdoQuery
Dim blnSinSuero As Boolean
Set Node = tvwOM.SelectedItem

CodPeticion = CLng(fNextClaveSeq("FRP4CODPETICION"))
If dbFrecuencia(Index).Value = "Manual" Then
    For i = Val(txtHoraInicio(Index)) To 23 ' para que este en orden cronologico
        If cmdHoras(i).BackColor = Rojo Then
            strHoras = strHoras & cmdHoras(i).Caption & ";"
        End If
    Next i
    For i = 0 To Val(txtHoraInicio(Index)) - 1
        If cmdHoras(i).BackColor = Rojo Then
            strHoras = strHoras & cmdHoras(i).Caption & ";"
        End If
    Next i
    lon = Len(strHoras)
    strHoras = Left(strHoras, lon - 1)
End If
If Left(Node.Key, 1) = "/" Then
  strgrupo = "SELECT FR7300.FR73CODPRODUCTO " & _
  "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '4000' AND " & _
  "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO and " & _
  "FR7300.FR73CODPRODUCTO= ? " 'grupo perfusiones sin suero
  Set qygrupo = objApp.rdoConnect.CreateQuery("", strgrupo)
      qygrupo(0) = dbMedicamento(Index).Columns(5).Value
  Set rdgrupo = qygrupo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  If rdgrupo.RowCount > 0 Then blnSinSuero = True
  rdgrupo.Close
  qygrupo.Close
End If

strFRP400 = "INSERT INTO FRP400 (FRP4CODPETICION,AD01CODASISTENCI, " & _
 "FR73CODPRODUCTO, FRP4DOSIS, FR93CODUNIMEDIDA, FRP4VOLUMEN, " & _
 "FRP4TIEMINFMIN, FR34CODVIA, FRG4CODFRECUENCIA, FRP4INDDISPPRN, " & _
 "FRP4INDCOMIENINMED, FRH5CODPERIODICIDAD, FRP4INDBLOQUEADA, " & _
 "FRP4FECBLOQUEO, SG02CODPERSBLOQ, FRP4INDESTOM, FRH7CODFORMFAR, " & _
 "FRP4OPERACION, FRP4CANTIDAD, FRP4FECINICIO, FRP4HORAINICIO, " & _
 "FRP4FECFIN, FRP4HORAFIN, FRP4INDVIAOPC, FR73CODPRODUCTO_DIL, " & _
 "FRP4CANTIDADDIL, FRP4TIEMMININF, FRP4INDPERF, FRP4INSTRADMIN, " & _
 "FRP4VELPERFUSION, FRP4VOLTOTAL, FR73CODPRODUCTO_2, FRH7CODFORMFAR_2, " & _
 "FRP4DOSIS_2, FR93CODUNIMEDIDA_2, FRP4PATHFORMAG, FRP4VOLUMEN_2, SG02COD, FRP4HORAS)" & _
 "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, " & _
 "TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, ?, ?, " & _
 "TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), ?, " & _
 "TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, ?, ?, ?, ?," & _
 "?, ?, ?, ?, ?, ?, ?, ?, ?,?)"
Set qyFRP400 = objApp.rdoConnect.CreateQuery("", strFRP400)
    qyFRP400(0) = CodPeticion
    qyFRP400(1) = CLng(txtAsistencia)
    If dbMedicamento(Index).Value = "" Then
      qyFRP400(2) = Null
    Else: qyFRP400(2) = dbMedicamento(Index).Columns(5).Value
    End If
    qyFRP400(3) = txtDosis(Index)
    If dbUM(Index).Value = "" Then
      qyFRP400(4) = Null
    Else: qyFRP400(4) = dbUM(Index).Columns(0).Value
    End If
    qyFRP400(5) = txtVolumen(Index)
    qyFRP400(6) = Null
    qyFRP400(7) = dbVia(Index).Columns(0).Value
    
    If dbFrecuencia(Index) = "" Then
      qyFRP400(8) = Null
    Else
      qyFRP400(8) = dbFrecuencia(Index).Columns(0).Value
    End If
    If Node.Key = "P" Then
        qyFRP400(9) = -1
    Else
        qyFRP400(9) = 0
    End If
    qyFRP400(10) = chkStat(Index).Value
    qyFRP400(11) = dbPeriodicidad(Index).Columns(0).Value
    qyFRP400(12) = 0 'Indice bloquead
    qyFRP400(13) = Null 'Fecha bloqueo
    qyFRP400(14) = Null
    qyFRP400(15) = Null 'Indice estado OM
    If dbFF(Index) = "" Then
      qyFRP400(16) = Null
    Else: qyFRP400(16) = dbFF(Index).Columns(0).Value
    End If
    qyFRP400(17) = Left(Node.Key, 1) 'Operacion
    qyFRP400(18) = txtCantidad(Index)
    qyFRP400(19) = Format(dtcFecInicio(Index).Date, "DD/MM/YYYY HH:MM:SS")
    qyFRP400(20) = txtHoraInicio(Index)
    qyFRP400(21) = Format(dtcFecFin(Index).Date, "DD/MM/YYYY HH:MM:SS")
    qyFRP400(22) = txtHoraFin(Index)
    qyFRP400(23) = Null
    If dbSolucion(Index).Value = "" Then
      qyFRP400(24) = Null
    Else: qyFRP400(24) = dbSolucion(Index).Columns(2).Value
    End If
    If blnSinSuero = False Then
      qyFRP400(25) = txtVolumenDil(Index)
    Else
      qyFRP400(25) = txtVolumen(Index)
      blnSinSuero = True
    End If
    qyFRP400(26) = Val(txtHorainf(Index)) * 60 + Val(txtMinInf(Index))
    qyFRP400(27) = Null
    qyFRP400(28) = txtObservaciones(Index)
    qyFRP400(29) = txtvelocidad(Index)
    qyFRP400(30) = txtVolTotal(Index)
    If Index = cMEZ Or (Index = cFLU And dbMedicamento2(cFLU).Text <> "") Then
        If dbMedicamento2(Index).Value = "" Then
          qyFRP400(31) = Null
        Else: qyFRP400(31) = dbMedicamento2(Index).Columns(5).Value
        End If
        If dbFF2(Index).Value = "" Then
          qyFRP400(32) = Null
        Else: qyFRP400(32) = dbFF2(Index).Columns(0).Value
        End If
        qyFRP400(33) = txtDosis2(Index)
        If dbUM2(Index).Value = "" Then
          qyFRP400(34) = Null
        Else: qyFRP400(34) = dbUM2(Index).Columns(0).Value
        End If
        qyFRP400(35) = Null
        qyFRP400(36) = txtVolumen2(Index)
    Else
        qyFRP400(31) = Null
        qyFRP400(32) = Null
        qyFRP400(33) = Null
        qyFRP400(34) = Null
        qyFRP400(35) = Null
        qyFRP400(36) = Null
    End If
    qyFRP400(37) = objSecurity.strUser
    If Trim(strHoras) = "" Then
        qyFRP400(38) = Null
    Else
        qyFRP400(38) = strHoras
    End If
qyFRP400.Execute
InsertarFRP400 = CodPeticion
End Function

Private Sub MascaraHora(STR As TextBox)
  If Not IsNumeric(STR) Then
      MsgBox "Introduzca un valor v�lido en este campo." & Chr(13) & _
      "Recuerde que las horas est�n comprendidas entre las 0 y las 23", vbCritical, ""
     STR.Text = ""
     STR.SetFocus
      Exit Sub
  End If
  If CInt(STR.Text) <> STR.Text Or STR.Text > 23 Or STR.Text < 0 Then
    MsgBox "Introduzca un valor v�lido en este campo." & Chr(13) & _
    "Recuerde que las horas est�n comprendidas entre las 0 y las 23", vbCritical, ""
   STR.Text = ""
   STR.SetFocus
  End If
End Sub

Private Function MascaraNumero(STR As TextBox) As Boolean
If Not IsNumeric(STR) Or InStr(1, STR, ".") <> 0 Then
  MsgBox "Introduzca un n�mero y los decimales con coma.", vbCritical, ""
  STR = ""
  STR.SetFocus
  MascaraNumero = True
End If
End Function
'Private Sub llenarGridValoracion()
'
'Dim fecha As String, STR$, fechaINICIO$
'Dim i%, COUNT%, j%, PosGrid%
'Dim rd As rdoResultset
'Dim QY As rdoQuery
'Dim rdHORA As rdoResultset
'Dim qyHORA As rdoQuery
'Dim rdT As rdoResultset
'Dim qyT As rdoQuery
'Dim Tratamiento As New Collection
'Dim HoraTratamiento As New Collection
'Dim HoraPrevia As String
'Dim Columna As String
'
'awk.FS = Chr$(13)
'
'STR = "SELECT AD0100.AD01CODASISTENCI, AD0100.AD01FECINICIO " & _
'"FROM AD0100 " & _
'"Where AD0100.AD01CODASISTENCI = ?"
'Set QY = objApp.rdoConnect.CreateQuery("", STR)
'  QY(0) = txtAsistencia
'Set rd = QY.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'ssdbValoracion.Groups.Add (0)
'ssdbValoracion.Groups.Item(0).Caption = "Valoraciones"
'ssdbValoracion.Groups.Item(0).Width = 3000
'ssdbValoracion.Groups(0).Columns.Add (0)
'ssdbValoracion.Groups(0).Columns(0).Caption = "Familia"
'ssdbValoracion.Groups(0).Columns.Add (1)
'ssdbValoracion.Groups(0).Columns(1).Caption = "Tratamientos"
'ssdbValoracion.Groups(0).Columns(0).BackColor = &H80FFFF    '&HFFC0C0
'ssdbValoracion.Groups(0).Columns(1).BackColor = &H80FFFF    '&H80FF80
''
'fecha = strFecha_Sistema
'If DateDiff("d", rd!AD01FECINICIO, fecha) < 5 Then
'  fechaINICIO = rd!AD01FECINICIO
'Else: fechaINICIO = DateAdd("d", -5, fecha)
'End If
'
'STR = "SELECT  /*+ RULE */ DISTINCT TREATMENTS.BRANDNAME, FAMILIES.FAMILYDESC, " & _
'  "TREATMENTS.TREATMENTDBOID, PICISDATA.PICISDATADBOID " & _
'  "FROM TASKS, FAMILIES, TREATMENTS, ORDERS, PICISDATA,ADMISSIONS " & _
'  "WHERE " & _
'  "ORDERS.PICISDATADBOID = PICISDATA.PICISDATADBOID AND " & _
'  "PICISDATA.ADMISSIONDBOID = ADMISSIONS.ADMISSIONDBOID AND " & _
'  "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
'  "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
'  "ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
'  "ORDERS.ORDERTYPEDBOID=? AND ADMISSIONS.ADMID1=? AND " & _
'  "TASKS.TODODATE >= TO_DATE(?,'DD/MM/YYYY') AND " & _
'  "TASKS.TODODATE <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
'  "ORDER BY FAMILYDESC ASC"
'  Set qyT = objApp.rdoConnect.CreateQuery("", STR)
'    qyT(0) = "169000000000004000000"
'    qyT(1) = txtAsistencia
'    qyT(2) = fechaINICIO
'    qyT(3) = fecha & " 23:59:00"
'  Set rdT = qyT.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'  While Not rdT.EOF
'    ssdbValoracion.AddItem rdT!FAMILYDESC & Chr$(9) & rdT!BRANDNAME
'    Tratamiento.Add Item:=PosGrid, Key:=CStr(rdT!BRANDNAME)
'    PosGrid = PosGrid + 1
'    rdT.MoveNext
'  Wend
'  If rdT.RowCount = 0 Then Exit Sub
'rdT.MoveFirst 'LO MOVEMOS AL PRIMER REGISTRO para leer Picisdatadboid
'i = 1
'  STR = "SELECT /*+ RULE */ DISTINCT TO_CHAR(TASKS.TODODATE,'HH24'), " & _
'  "TREATMENTS.BRANDNAME, FAMILIES.FAMILYDESC, " & _
'  "ASSESSMENTITEMS.ASSESSMENTITEMDESC FROM FAMILIES,TREATMENTS, " & _
'  "ASSESSMENTITEMS, TASKASSESSMENT, " & _
'  "TASKS, ORDERS " & _
'  "WHERE ORDERS.ORDERDBOID = TASKS.ORDERDBOID AND " & _
'  "ORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
'  "TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID AND " & _
'  "TASKS.TASKDBOID = TASKASSESSMENT.TASKDBOID AND " & _
'  "TASKASSESSMENT.ASSESSMENTITEMDBOID = ASSESSMENTITEMS.ASSESSMENTITEMDBOID " & _
'  "AND ORDERS.ORDERTYPEDBOID=? AND ORDERS.PICISDATADBOID = ? " & _
'  "AND TO_CHAR(TASKS.TODODATE,'DD/MM/YYYY')=? " & _
'  "ORDER BY TO_CHAR(TASKS.TODODATE,'HH24') DESC"
'  Set qyHORA = objApp.rdoConnect.CreateQuery("", STR)
'    qyHORA(0) = "169000000000004000000"
'    qyHORA(1) = rdT!PICISDATADBOID
'While DateDiff("d", fecha, fechaINICIO) < 0
'    qyHORA(2) = fecha
'  Set rdHORA = qyHORA.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'  ssdbValoracion.Groups.Add (i)
'  ssdbValoracion.Groups.Item(i).Caption = fecha
' COUNT = 0
' Dim pos%, posAnterior%
'  While Not rdHORA.EOF
'    pos = Tratamiento.Item(rdHORA!BRANDNAME)
'    ssdbValoracion.MoveFirst
'    ssdbValoracion.MoveRecords (pos)
'    If (rdHORA.rdoColumns(0) & " " & fecha) = HoraPrevia Then
'      COUNT = COUNT - 1
'      If pos = posAnterior Then
'        ssdbValoracion.Groups(i).Columns(COUNT).Text = _
'        ssdbValoracion.Groups(i).Columns(COUNT).Text & Chr$(13) & rdHORA!ASSESSMENTITEMDESC
'      Else
'        ssdbValoracion.Groups(i).Columns(COUNT).Text = rdHORA!ASSESSMENTITEMDESC
'      End If
'    Else
'      ssdbValoracion.Groups(i).Columns.Add (COUNT)
'      'Se mira si esa hora ya existe para no repetirla
'      Columna = rdHORA.rdoColumns(0) & "-"
'      If Len(rdHORA.rdoColumns(0) + 1) = 1 Then
'        Columna = Columna & "0" & (rdHORA.rdoColumns(0) + 1)
'      Else: Columna = Columna & (rdHORA.rdoColumns(0) + 1)
'      End If
'      ssdbValoracion.Groups(i).Columns(COUNT).Caption = Columna
'      ssdbValoracion.Groups(i).Columns(COUNT).Text = rdHORA!ASSESSMENTITEMDESC
'    End If
'    ssdbValoracion.Update
'    posAnterior = pos
'    HoraPrevia = rdHORA.rdoColumns(0) & " " & fecha
'    COUNT = COUNT + 1
'    rdHORA.MoveNext
'  Wend
'  For j = 0 To COUNT - 1
'    ssdbValoracion.Groups(i).Columns(j).Width = 2200
'    ssdbValoracion.Groups(i).Columns(j).Alignment = ssCaptionAlignmentCenter
'  Next
'  ssdbValoracion.Groups.Item(i).Width = 2200 * (COUNT)
'  fecha = DateAdd("d", -1, CDate(fecha))
'  i = i + 1
'Wend
'rdT.Close
'qyT.Close
'rdHORA.Close
'qyHORA.Close
'rd.Close
'QY.Close
'i = 1
'For i = 1 To ssdbValoracion.Groups.COUNT - 1 'PONE VIOLETA LA PRIMERA COLUMNA
'                                         'PARA DISTINGUIRLA
'  If ssdbValoracion.Groups(i).Columns.COUNT <> 0 Then
'    ssdbValoracion.Groups(i).Columns(0).BackColor = &HFFC0C0
'    Exit For
'  End If
'Next
'ssdbValoracion.MoveFirst
'ssdbValoracion.SplitterPos = 1
'End Sub
