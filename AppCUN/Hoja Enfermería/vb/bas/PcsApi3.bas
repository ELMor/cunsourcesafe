Attribute VB_Name = "Module5"
#If DebugVersion Then

Public Declare Function PcsExternalLinkAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsExternalLinkFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pExtCode1_cond As String, ByVal in_pExtCode2_cond As String, _
ByVal in_pExtCode3_cond As String, ByVal in_pDescription_cond As String, ByVal in_pExtTableRef_cond As String, ByVal in_pPicisDBOID_cond As String) As Integer
Public Declare Function PcsExternalLinkFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsExternalLinkGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtCode1 Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtCode1 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtCode2 Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtCode2 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtCode3 Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtCode3 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtTableRef Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtTableRef As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetPicisDBOIDIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsExternalLinkSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsExternalLinkSetExtCode1 Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtCode1 As String) As Integer
Public Declare Function PcsExternalLinkSetExtCode2 Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtCode2 As String) As Integer
Public Declare Function PcsExternalLinkSetExtCode3 Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtCode3 As String) As Integer
Public Declare Function PcsExternalLinkSetExtTableRef Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtTableRef As String) As Integer
Public Declare Function PcsExternalLinkSetPicisDBOIDIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilyAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFamilyBehaviorAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFamilyBehaviorFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCoeff_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pCategory_cond As String) As Integer
Public Declare Function PcsFamilyBehaviorFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetCategory Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetCategoryIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetCoeff Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsFamilyBehaviorGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyBehaviorListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsFamilyBehaviorSetCategory Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFamilyBehaviorSetCategoryIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilyBehaviorSetCoeff Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsFamilyBehaviorSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFamilyBehaviorSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsFamilyFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pCategory_cond As String, ByVal in_pFamilyBehavior_cond As String) As Integer
Public Declare Function PcsFamilyFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFamilyGetCategory Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFamilyGetCategoryIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyGetFamilyBehavior Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFamilyGetFamilyBehaviorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsFamilySetCategory Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFamilySetCategoryIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilySetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFamilySetFamilyBehavior Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFamilySetFamilyBehaviorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilySetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsFormAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFormFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pFormType_cond As String) As Integer
Public Declare Function PcsFormFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFormGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormGetFormType Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFormGetFormTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsFormSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFormSetFormType Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFormSetFormTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFormSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFORM As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsFormTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFormTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsFormTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFormTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFORMTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormTypeGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFORMTYPE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsFormTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHFORMTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFormTypeSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHFORMTYPE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsGetBitmap Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsGetColor Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsGetDouble Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Double) As Integer
Public Declare Function PcsGetFont Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef in_pdefault As Long) As Integer
Public Declare Function PcsGetIcon Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsGroupAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsGroupFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pApplication_cond As String) As Integer
Public Declare Function PcsGroupFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsGroupGetApplication Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsGroupGetApplicationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsGroupGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsGroupGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsGroupListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupSetApplication Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsGroupSetApplicationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsGroupSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsGroupSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHGROUP As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsGroupPrescValListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupSystemListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupUserListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupPrescValListReadEx Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupSystemListReadEx Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupUserListReadEx Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupGetPrescValGroups Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_pGroupDboid As String, ByVal in_pIncludeDeletedGroups As String, ByVal in_pIncludeDeletedGroupRelationShips As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupGetSystemGroups Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_pGroupDboid As String, ByVal in_pIncludeDeletedGroups As String, ByVal in_pIncludeDeletedGroupRelationShips As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsHookActionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsHookActionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsHookActionGetCallbackCall Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByRef out_pcallbackfunc As Long) As Integer
Public Declare Function PcsHookActionGetDllCall Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal out_pdllname As String, ByVal in_num_max_dllname As Long, ByVal out_pfuncname As String, _
ByVal in_num_max_funcname As Long) As Integer
Public Declare Function PcsHookActionGetMaintainLoadedFlag Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByRef out_pmaintain_loaded As Long) As Integer
Public Declare Function PcsHookActionGetName Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsHookActionGetType Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByRef out_ptype As Long) As Integer
Public Declare Function PcsHookActionGetUserData Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByRef out_puserdata As Long) As Integer
Public Declare Function PcsHookActionLoadFromConfig Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_pworld As String, ByVal in_pzone As String, ByVal in_psection As String) As Integer
Public Declare Function PcsHookActionProcess Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_phookname As String, ByVal in_message As Long, ByVal in_wparam As Long, ByVal in_lparam As Long, _
ByRef out_presult As Long) As Integer
Public Declare Function PcsHookActionReset Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long) As Integer
Public Declare Function PcsHookActionSetCallbackCall Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_pcallbackfunc As Long) As Integer
Public Declare Function PcsHookActionSetDllCall Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_pdllname As String, ByVal in_pfuncname As String) As Integer
Public Declare Function PcsHookActionSetMaintainLoadedFlag Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_maintain_loaded As Long) As Integer
Public Declare Function PcsHookActionSetName Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_pName As String) As Integer
Public Declare Function PcsHookActionSetUserData Lib "PCSDB500D.DLL" (ByVal in_hhookaction As Long, ByVal in_userdata As Long) As Integer
Public Declare Function PcsHookControllerAddHook Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_hook As Long) As Integer
Public Declare Function PcsHookControllerAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsHookControllerFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsHookControllerGetDefault Lib "PCSDB500D.DLL" (ByRef out_phookcontroller As Long) As Integer
Public Declare Function PcsHookControllerLoadFromConfig Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long, ByVal in_pworld As String, ByVal in_pzone As String) As Integer
Public Declare Function PcsHookControllerProcessAllHooks Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_message As Long, ByVal in_wparam As Long, _
ByVal in_lparam As Long) As Integer
Public Declare Function PcsHookControllerProcessUntilTrue Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_message As Long, ByVal in_wparam As Long, _
ByVal in_lparam As Long) As Integer
Public Declare Function PcsHookControllerReloadFromConfig Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long) As Integer
Public Declare Function PcsHookControllerRemoveHook Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_hook As Long) As Integer
Public Declare Function PcsHookControllerReset Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long) As Integer
Public Declare Function PcsHookControllerSetDefault Lib "PCSDB500D.DLL" (ByVal in_hookcontroller As Long) As Integer
Public Declare Function PcsLabDataStatusAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabDataStatusFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsLabDataStatusFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabDataStatusGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHLABDATASTATUS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabDataStatusListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabDataStatusSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHLABDATASTATUS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsLabResultAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabResultFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTextValue_cond As String, _
ByVal in_pLabTest_cond As String, ByVal in_pPartComponent_cond As String, ByVal in_pLabDataStatus_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsLabResultFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabResultGetAudited Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pAudited As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultSetAudited Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pAudited As String) As Integer
Public Declare Function PcsLabResultGetLabDataStatus Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetLabDataStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetLabTest Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetLabTestIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetPartComponent Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetPartComponentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetTextValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pTextValue As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsLabResultListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabResultSetLabDataStatus Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetLabDataStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetLabTest Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetLabTestIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetPartComponent Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetPartComponentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetTextValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pTextValue As String) As Integer
Public Declare Function PcsLabResultSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsLabResultAuditedAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabResultAuditedFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabResultAuditedGetValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsLabResultAuditedSetValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsLabResultAuditedGetTextValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pTextValue As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetTextValue Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pTextValue As String) As Integer
Public Declare Function PcsLabResultAuditedGetInstant Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLabResultAuditedSetInstant Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsLabResultAuditedGetLabResult Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedSetLabResult Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedGetLabResultIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetLabResultIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultAuditedGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultAuditedGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultAuditedListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabResultAuditedFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTextValue_cond As String, _
ByVal in_pInstant_cond As String, ByVal in_pLabResults_cond As String, ByVal in_pUnit_cond As String, ByVal in_pStaff_cond As String) As Integer

Public Declare Function PcsLabTestAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabTestFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pIdentifier_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pEnded_cond As String, ByVal in_pAnalysis_cond As String, ByVal in_pStaff_cond As String, ByVal in_pPicisData_cond As String, ByVal in_pTask_cond As String) As Integer
Public Declare Function PcsLabTestFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabTestGetAnalysis Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabTestGetAnalysisIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLabTestGetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabTestGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabTestGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLabTestGetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabTestSetAnalysis Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabTestSetAnalysisIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabTestSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsLabTestSetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pidentifier As String) As Integer
Public Declare Function PcsLabTestSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabTestSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabTestSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabTestSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabTestSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsLabTestSetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsListObjectsAddHead Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsAddTail Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsAlloc Lib "PCSDB500D.DLL" (ByRef out_phslist As Long) As Integer
Public Declare Function PcsListObjectsFindFirstObjectPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsFindNextObjectPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsFree Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsGetAt Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_index As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGetCount Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_pcount As Long) As Integer
Public Declare Function PcsListObjectsGetHead Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGetMapClass Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal out_pclassname As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsListObjectsGetObjectFromPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGetTail Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGotoHeadPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsGotoIndexPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_index As Long) As Integer
Public Declare Function PcsListObjectsGotoNextPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGotoPrevPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGotoTailPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsInsertAfterPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsInsertBeforePosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsIsEmpty Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_pbool As Long) As Integer
Public Declare Function PcsListObjectsRemoveAll Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveAt Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_index As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveHead Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveObjectFromPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveTail Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsSetMapClass Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_pclassname As String) As Integer
Public Declare Function PcsListObjectsSetObjectFromPosition Lib "PCSDB500D.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListStringsAddHead Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsAddTail Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsAlloc Lib "PCSDB500D.DLL" (ByRef out_phslist As Long) As Integer
Public Declare Function PcsListStringsFindFirstStringPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsFindNextStringPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsFree Lib "PCSDB500D.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsGetAt Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_index As Long, ByVal out_pstr As String, ByVal out_num_max As Long) As Integer
Public Declare Function PcsListStringsGetCount Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByRef out_pcount As Long) As Integer
Public Declare Function PcsListStringsGetEnableRepeatFlag Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByRef out_penable_repeat As Long) As Integer
Public Declare Function PcsListStringsGetHead Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGetStringFromPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pstr As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsListStringsGetTail Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGotoHeadPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsGotoIndexPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_index As Long) As Integer
Public Declare Function PcsListStringsGotoNextPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGotoPrevPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGotoTailPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsInsertAfterPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsInsertBeforePosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsIsEmpty Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByRef out_pbool As Long) As Integer
Public Declare Function PcsListStringsRemoveAll Lib "PCSDB500D.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsRemoveHead Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsRemoveStringFromPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsRemoveTail Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsSetAt Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_index As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsListStringsSetEnableRepeatFlag Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_enable_repeat As Long) As Integer
Public Declare Function PcsListStringsSetStringFromPosition Lib "PCSDB500D.DLL" (ByVal in_hslist As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsLoadBitmap Lib "PCSDB500D.DLL" (ByVal in_bitmapname As Long) As Integer
Public Declare Function PcsLoadIcon Lib "PCSDB500D.DLL" (ByVal in_iconname As Long) As Integer
Public Declare Function PcsLoadSmallIcon Lib "PCSDB500D.DLL" (ByVal in_iconname As Long) As Integer
Public Declare Function PcsLocationAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLocationCreateFromScratch Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_cDescription As String, ByVal in_cComputerName As String, ByVal in_cInitials As String, _
ByVal in_hDepartment As Long, ByRef out_hLocation As Long) As Integer
Public Declare Function PcsLocationFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pLocationId_cond As String, ByVal in_pDescription_cond As String, _
ByVal in_pComputerName_cond As String, ByVal in_pInitials_cond As String, ByVal in_pDepartment_cond As String) As Integer
Public Declare Function PcsLocationFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLocationGetComputerName Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pComputerName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetDepartment Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLocationGetDepartmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetFromLocationId Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_LocationId As Long, ByRef out_phlocation As Long) As Integer
Public Declare Function PcsLocationGetInitials Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pInitials As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetLocationId Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsLocationGetPatient Lib "PCSDB500D.DLL" (ByVal in_hlocation As Long, ByRef out_phpat As Long) As Integer
Public Declare Function PcsLocationGetStatus Lib "PCSDB500D.DLL" (ByVal in_hlocation As Long, ByRef out_plStatus As Long) As Integer
Public Declare Function PcsLocationListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLocationSetComputerName Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pComputerName As String) As Integer
Public Declare Function PcsLocationSetDepartment Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLocationSetDepartmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLocationSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsLocationSetInitials Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pInitials As String) As Integer
Public Declare Function PcsLocationSetLocationId Lib "PCSDB500D.DLL" (ByVal in_hHLOCATION As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsLogAbortTransaction Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogAlloc Lib "PCSDB500D.DLL" (ByRef out_hlog As Long) As Integer
Public Declare Function PcsLogAutoLogOff Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogBeginTransaction Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogEndTransaction Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogFree Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogGetAllColumnsInTable Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_psourcename As String, ByVal in_pcreatorname As String, ByVal in_ptable_name As String, _
ByVal out_hslist As Long) As Integer
Public Declare Function PcsLogGetAllTables Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hslist As Long) As Integer
Public Declare Function PcsLogIsProcedureInDB Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_psourcename As String, ByRef out_pIsInDB As Long) As Integer
Public Declare Function PcsLogGetDatabaseType Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_pdatabasetype As Long) As Integer
Public Declare Function PcsLogGetCommonVirtualUserName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetDataSources Lib "PCSDB500D.DLL" (ByRef out_hslDataSources As Long) As Integer
Public Declare Function PcsLogGetDefault Lib "PCSDB500D.DLL" (ByRef out_phlog As Long) As Integer
Public Declare Function PcsLogGetPassword Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_ppassword As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetSourceName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_psourcename As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetUserName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetVirtualPassword Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_ppassword As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetVirtualUserName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogHasProperty Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_pgroup As String, ByRef out_plevel As Long) As Integer
Public Declare Function PcsLogIsLogged Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByRef out_pislogged As Long) As Integer
Public Declare Function PcsLogLogOff Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogLogOn Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogOffDo Lib "PCSDB500D.DLL" (ByVal in_logon As Long) As Integer
Public Declare Function PcsLogOnDo Lib "PCSDB500D.DLL" (ByRef out_phlog As Long, ByVal in_pUserName As String, ByVal in_ppassword As String) As Integer
Public Declare Function PcsLogSetDefault Lib "PCSDB500D.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogSetPassword Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_ppassword As String) As Integer
Public Declare Function PcsLogSetReconnectionStatus Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_AutomaticReconnection As Long, ByVal in_ReconnectionTimer As Long) As Integer
Public Declare Function PcsLogSetSourceName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_psourcename As String) As Integer
Public Declare Function PcsLogSetUserName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_pUserName As String) As Integer
Public Declare Function PcsLogSetVirtualPassword Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_ppassword As String) As Integer
Public Declare Function PcsLogSetVirtualUserName Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_pUserName As String) As Integer
Public Declare Function PcsLogStatementLock Lib "PCSDB500D.DLL" (ByVal in_Lock As Long) As Integer
Public Declare Function PcsLogUseDefaultUser Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_bUseDefaultUser As Long) As Integer
Public Declare Function PcsLogUtilExecuteQuery Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal in_pquery As String) As Integer
Public Declare Function PcsLogUtilGetAssociatedListsFromQuery Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByVal out_slist1 As Long, ByVal out_slist2 As Long) As Integer
Public Declare Function PcsLogUtilGetDoubleFromQuery Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsLogUtilGetStringFromQuery Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByVal out_pchar As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogUtilGetStringListFromQuery Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByVal out_slist1 As Long) As Integer
Public Declare Function PcsLogUtilGetStringListListFromQuery Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_query As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLogUtilGetTimeStampFromQuery Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLogUtilTranslateToDBJokerString Lib "PCSDB500D.DLL" (ByVal in_pstring As String) As Integer
Public Declare Function PcsMaritalAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMaritalFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsMaritalFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMaritalGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMARITAL As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMaritalListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsMaritalSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMARITAL As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsMedicalHistoryAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMedicalHistoryFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pPatient_cond As String) As Integer
Public Declare Function PcsMedicalHistoryFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMedicalHistoryGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMedicalHistoryGetPatient Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsMedicalHistoryGetPatientIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMedicalHistoryGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsMedicalHistoryListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsMedicalHistorySetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsMedicalHistorySetPatient Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsMedicalHistorySetPatientIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsMedicalHistorySetStarted Lib "PCSDB500D.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsMedProcedureAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMedProcedureFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsMedProcedureFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMedProcedureGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMEDPROCEDURE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMedProcedureListRead Lib "PCSDB500D.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsMedProcedureSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMEDPROCEDURE As Long, ByVal in_pDescription As String) As Integer

#Else

Public Declare Function PcsExternalLinkAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsExternalLinkFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pExtCode1_cond As String, ByVal in_pExtCode2_cond As String, _
ByVal in_pExtCode3_cond As String, ByVal in_pDescription_cond As String, ByVal in_pExtTableRef_cond As String, ByVal in_pPicisDBOID_cond As String) As Integer
Public Declare Function PcsExternalLinkFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsExternalLinkGetDescription Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtCode1 Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtCode1 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtCode2 Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtCode2 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtCode3 Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtCode3 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetExtTableRef Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pExtTableRef As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkGetPicisDBOIDIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsExternalLinkListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsExternalLinkSetDescription Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsExternalLinkSetExtCode1 Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtCode1 As String) As Integer
Public Declare Function PcsExternalLinkSetExtCode2 Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtCode2 As String) As Integer
Public Declare Function PcsExternalLinkSetExtCode3 Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtCode3 As String) As Integer
Public Declare Function PcsExternalLinkSetExtTableRef Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pExtTableRef As String) As Integer
Public Declare Function PcsExternalLinkSetPicisDBOIDIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEXTERNALLINK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilyAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFamilyBehaviorAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFamilyBehaviorFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCoeff_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pCategory_cond As String) As Integer
Public Declare Function PcsFamilyBehaviorFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetCategory Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetCategoryIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetCoeff Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsFamilyBehaviorGetDescription Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyBehaviorGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyBehaviorListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsFamilyBehaviorSetCategory Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFamilyBehaviorSetCategoryIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilyBehaviorSetCoeff Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsFamilyBehaviorSetDescription Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFamilyBehaviorSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFAMILYBEHAVIOR As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsFamilyFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pCategory_cond As String, ByVal in_pFamilyBehavior_cond As String) As Integer
Public Declare Function PcsFamilyFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFamilyGetCategory Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFamilyGetCategoryIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyGetDescription Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyGetFamilyBehavior Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFamilyGetFamilyBehaviorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFamilyListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsFamilySetCategory Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFamilySetCategoryIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilySetDescription Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFamilySetFamilyBehavior Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFamilySetFamilyBehaviorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFamilySetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFAMILY As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsFormAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFormFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pFormType_cond As String) As Integer
Public Declare Function PcsFormFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFormGetDescription Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormGetFormType Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsFormGetFormTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsFormSetDescription Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFormSetFormType Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsFormSetFormTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsFormSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFORM As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsFormTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsFormTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsFormTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsFormTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHFORMTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormTypeGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFORMTYPE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsFormTypeListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsFormTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHFORMTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsFormTypeSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHFORMTYPE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsGetBitmap Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsGetColor Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsGetDouble Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Double) As Integer
Public Declare Function PcsGetFont Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef in_pdefault As Long) As Integer
Public Declare Function PcsGetIcon Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsGroupAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsGroupFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pApplication_cond As String) As Integer
Public Declare Function PcsGroupFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsGroupGetApplication Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsGroupGetApplicationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsGroupGetDescription Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsGroupGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsGroupListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupSetApplication Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsGroupSetApplicationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsGroupSetDescription Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsGroupSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHGROUP As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsGroupPrescValListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupSystemListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupUserListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupPrescValListReadEx Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupSystemListReadEx Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupUserListReadEx Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupGetPrescValGroups Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_pGroupDboid As String, ByVal in_pIncludeDeletedGroups As String, ByVal in_pIncludeDeletedGroupRelationShips As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsGroupGetSystemGroups Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_pGroupDboid As String, ByVal in_pIncludeDeletedGroups As String, ByVal in_pIncludeDeletedGroupRelationShips As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsHookActionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsHookActionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsHookActionGetCallbackCall Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByRef out_pcallbackfunc As Long) As Integer
Public Declare Function PcsHookActionGetDllCall Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal out_pdllname As String, ByVal in_num_max_dllname As Long, ByVal out_pfuncname As String, _
ByVal in_num_max_funcname As Long) As Integer
Public Declare Function PcsHookActionGetMaintainLoadedFlag Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByRef out_pmaintain_loaded As Long) As Integer
Public Declare Function PcsHookActionGetName Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsHookActionGetType Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByRef out_ptype As Long) As Integer
Public Declare Function PcsHookActionGetUserData Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByRef out_puserdata As Long) As Integer
Public Declare Function PcsHookActionLoadFromConfig Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_pworld As String, ByVal in_pzone As String, ByVal in_psection As String) As Integer
Public Declare Function PcsHookActionProcess Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_phookname As String, ByVal in_message As Long, ByVal in_wparam As Long, ByVal in_lparam As Long, _
ByRef out_presult As Long) As Integer
Public Declare Function PcsHookActionReset Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long) As Integer
Public Declare Function PcsHookActionSetCallbackCall Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_pcallbackfunc As Long) As Integer
Public Declare Function PcsHookActionSetDllCall Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_pdllname As String, ByVal in_pfuncname As String) As Integer
Public Declare Function PcsHookActionSetMaintainLoadedFlag Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_maintain_loaded As Long) As Integer
Public Declare Function PcsHookActionSetName Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_pName As String) As Integer
Public Declare Function PcsHookActionSetUserData Lib "PCSDB500.DLL" (ByVal in_hhookaction As Long, ByVal in_userdata As Long) As Integer
Public Declare Function PcsHookControllerAddHook Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_hook As Long) As Integer
Public Declare Function PcsHookControllerAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsHookControllerFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsHookControllerGetDefault Lib "PCSDB500.DLL" (ByRef out_phookcontroller As Long) As Integer
Public Declare Function PcsHookControllerLoadFromConfig Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long, ByVal in_pworld As String, ByVal in_pzone As String) As Integer
Public Declare Function PcsHookControllerProcessAllHooks Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_message As Long, ByVal in_wparam As Long, _
ByVal in_lparam As Long) As Integer
Public Declare Function PcsHookControllerProcessUntilTrue Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_message As Long, ByVal in_wparam As Long, _
ByVal in_lparam As Long) As Integer
Public Declare Function PcsHookControllerReloadFromConfig Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long) As Integer
Public Declare Function PcsHookControllerRemoveHook Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long, ByVal in_phookname As String, ByVal in_hook As Long) As Integer
Public Declare Function PcsHookControllerReset Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long) As Integer
Public Declare Function PcsHookControllerSetDefault Lib "PCSDB500.DLL" (ByVal in_hookcontroller As Long) As Integer
Public Declare Function PcsLabDataStatusAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabDataStatusFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsLabDataStatusFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabDataStatusGetDescription Lib "PCSDB500.DLL" (ByVal in_hHLABDATASTATUS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabDataStatusListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabDataStatusSetDescription Lib "PCSDB500.DLL" (ByVal in_hHLABDATASTATUS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsLabResultAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabResultFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTextValue_cond As String, _
ByVal in_pLabTest_cond As String, ByVal in_pPartComponent_cond As String, ByVal in_pLabDataStatus_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsLabResultFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabResultGetAudited Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pAudited As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultSetAudited Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pAudited As String) As Integer
Public Declare Function PcsLabResultGetLabDataStatus Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetLabDataStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetLabTest Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetLabTestIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetPartComponent Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetPartComponentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetTextValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pTextValue As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetUnit Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultGetValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsLabResultListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabResultSetLabDataStatus Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetLabDataStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetLabTest Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetLabTestIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetPartComponent Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetPartComponentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetTextValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pTextValue As String) As Integer
Public Declare Function PcsLabResultSetUnit Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultSetValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULT As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsLabResultAuditedAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabResultAuditedFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabResultAuditedGetValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsLabResultAuditedSetValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsLabResultAuditedGetTextValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pTextValue As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetTextValue Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pTextValue As String) As Integer
Public Declare Function PcsLabResultAuditedGetInstant Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLabResultAuditedSetInstant Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsLabResultAuditedGetLabResults Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedSetLabResults Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedGetLabResultsIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetLabResultsIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultAuditedGetUnit Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedSetUnit Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultAuditedGetStaff Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedSetStaff Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabResultAuditedGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabResultAuditedSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABRESULTAUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabResultAuditedListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabResultAuditedFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTextValue_cond As String, _
ByVal in_pInstant_cond As String, ByVal in_pLabResults_cond As String, ByVal in_pUnit_cond As String, ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsLabTestAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLabTestFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pIdentifier_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pEnded_cond As String, ByVal in_pAnalysis_cond As String, ByVal in_pStaff_cond As String, ByVal in_pPicisData_cond As String, ByVal in_pTask_cond As String) As Integer
Public Declare Function PcsLabTestFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLabTestGetAnalysis Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabTestGetAnalysisIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetEnded Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLabTestGetIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabTestGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetStaff Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLabTestGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestGetStarted Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLabTestGetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLabTestListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLabTestSetAnalysis Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabTestSetAnalysisIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabTestSetEnded Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsLabTestSetIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pidentifier As String) As Integer
Public Declare Function PcsLabTestSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabTestSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabTestSetStaff Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLabTestSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLabTestSetStarted Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsLabTestSetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLABTEST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsListObjectsAddHead Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsAddTail Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsAlloc Lib "PCSDB500.DLL" (ByRef out_phslist As Long) As Integer
Public Declare Function PcsListObjectsFindFirstObjectPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsFindNextObjectPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsFree Lib "PCSDB500.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsGetAt Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_index As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGetCount Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_pcount As Long) As Integer
Public Declare Function PcsListObjectsGetHead Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGetMapClass Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal out_pclassname As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsListObjectsGetObjectFromPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGetTail Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGotoHeadPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsGotoIndexPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_index As Long) As Integer
Public Declare Function PcsListObjectsGotoNextPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGotoPrevPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsGotoTailPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsInsertAfterPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsInsertBeforePosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListObjectsIsEmpty Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_pbool As Long) As Integer
Public Declare Function PcsListObjectsRemoveAll Lib "PCSDB500.DLL" (ByVal in_listhandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveAt Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_index As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveHead Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveObjectFromPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsRemoveTail Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByRef out_phandle As Long) As Integer
Public Declare Function PcsListObjectsSetMapClass Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_pclassname As String) As Integer
Public Declare Function PcsListObjectsSetObjectFromPosition Lib "PCSDB500.DLL" (ByVal in_listhandle As Long, ByVal in_handle As Long) As Integer
Public Declare Function PcsListStringsAddHead Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsAddTail Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsAlloc Lib "PCSDB500.DLL" (ByRef out_phslist As Long) As Integer
Public Declare Function PcsListStringsFindFirstStringPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsFindNextStringPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsFree Lib "PCSDB500.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsGetAt Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_index As Long, ByVal out_pstr As String, ByVal out_num_max As Long) As Integer
Public Declare Function PcsListStringsGetCount Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByRef out_pcount As Long) As Integer
Public Declare Function PcsListStringsGetEnableRepeatFlag Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByRef out_penable_repeat As Long) As Integer
Public Declare Function PcsListStringsGetHead Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGetStringFromPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pstr As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsListStringsGetTail Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGotoHeadPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsGotoIndexPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_index As Long) As Integer
Public Declare Function PcsListStringsGotoNextPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGotoPrevPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsGotoTailPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsInsertAfterPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsInsertBeforePosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pchar As String) As Integer
Public Declare Function PcsListStringsIsEmpty Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByRef out_pbool As Long) As Integer
Public Declare Function PcsListStringsRemoveAll Lib "PCSDB500.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsRemoveHead Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsRemoveStringFromPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long) As Integer
Public Declare Function PcsListStringsRemoveTail Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal out_pchar As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsListStringsSetAt Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_index As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsListStringsSetEnableRepeatFlag Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_enable_repeat As Long) As Integer
Public Declare Function PcsListStringsSetStringFromPosition Lib "PCSDB500.DLL" (ByVal in_hslist As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsLoadBitmap Lib "PCSDB500.DLL" (ByVal in_bitmapname As Long) As Integer
Public Declare Function PcsLoadIcon Lib "PCSDB500.DLL" (ByVal in_iconname As Long) As Integer
Public Declare Function PcsLoadSmallIcon Lib "PCSDB500.DLL" (ByVal in_iconname As Long) As Integer
Public Declare Function PcsLocationAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsLocationCreateFromScratch Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_cDescription As String, ByVal in_cComputerName As String, ByVal in_cInitials As String, _
ByVal in_hDepartment As Long, ByRef out_hLocation As Long) As Integer
Public Declare Function PcsLocationFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pLocationId_cond As String, ByVal in_pDescription_cond As String, _
ByVal in_pComputerName_cond As String, ByVal in_pInitials_cond As String, ByVal in_pDepartment_cond As String) As Integer
Public Declare Function PcsLocationFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsLocationGetComputerName Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pComputerName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetDepartment Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsLocationGetDepartmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetDescription Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetFromLocationId Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_LocationId As Long, ByRef out_phlocation As Long) As Integer
Public Declare Function PcsLocationGetInitials Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal out_pInitials As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLocationGetLocationId Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsLocationGetPatient Lib "PCSDB500.DLL" (ByVal in_hlocation As Long, ByRef out_phpat As Long) As Integer
Public Declare Function PcsLocationGetStatus Lib "PCSDB500.DLL" (ByVal in_hlocation As Long, ByRef out_plStatus As Long) As Integer
Public Declare Function PcsLocationListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLocationSetComputerName Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pComputerName As String) As Integer
Public Declare Function PcsLocationSetDepartment Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsLocationSetDepartmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsLocationSetDescription Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsLocationSetInitials Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal in_pInitials As String) As Integer
Public Declare Function PcsLocationSetLocationId Lib "PCSDB500.DLL" (ByVal in_hHLOCATION As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsLogAbortTransaction Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogAlloc Lib "PCSDB500.DLL" (ByRef out_hlog As Long) As Integer
Public Declare Function PcsLogAutoLogOff Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogBeginTransaction Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogEndTransaction Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogFree Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogGetAllColumnsInTable Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_psourcename As String, ByVal in_pcreatorname As String, ByVal in_ptable_name As String, _
ByVal out_hslist As Long) As Integer
Public Declare Function PcsLogGetAllTables Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hslist As Long) As Integer
Public Declare Function PcsLogIsProcedureInDB Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_psourcename As String, ByRef out_pIsInDB As Long) As Integer
Public Declare Function PcsLogGetCommonVirtualUserName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetDatabaseType Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_pdatabasetype As Long) As Integer
Public Declare Function PcsLogGetDataSources Lib "PCSDB500.DLL" (ByRef out_hslDataSources As Long) As Integer
Public Declare Function PcsLogGetDefault Lib "PCSDB500.DLL" (ByRef out_phlog As Long) As Integer
Public Declare Function PcsLogGetPassword Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_ppassword As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetSourceName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_psourcename As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetUserName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetVirtualPassword Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_ppassword As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogGetVirtualUserName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogHasProperty Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_pgroup As String, ByRef out_plevel As Long) As Integer
Public Declare Function PcsLogIsLogged Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByRef out_pislogged As Long) As Integer
Public Declare Function PcsLogLogOff Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogLogOn Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogOffDo Lib "PCSDB500.DLL" (ByVal in_logon As Long) As Integer
Public Declare Function PcsLogOnDo Lib "PCSDB500.DLL" (ByRef out_phlog As Long, ByVal in_pUserName As String, ByVal in_ppassword As String) As Integer
Public Declare Function PcsLogSetDefault Lib "PCSDB500.DLL" (ByVal in_hLog As Long) As Integer
Public Declare Function PcsLogSetPassword Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_ppassword As String) As Integer
Public Declare Function PcsLogSetReconnectionStatus Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_AutomaticReconnection As Long, ByVal in_ReconnectionTimer As Long) As Integer
Public Declare Function PcsLogSetSourceName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_psourcename As String) As Integer
Public Declare Function PcsLogSetUserName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_pUserName As String) As Integer
Public Declare Function PcsLogSetVirtualPassword Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_ppassword As String) As Integer
Public Declare Function PcsLogSetVirtualUserName Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_pUserName As String) As Integer
Public Declare Function PcsLogStatementLock Lib "PCSDB500.DLL" (ByVal in_Lock As Long) As Integer
Public Declare Function PcsLogUseDefaultUser Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_bUseDefaultUser As Long) As Integer
Public Declare Function PcsLogUtilExecuteQuery Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal in_pquery As String) As Integer
Public Declare Function PcsLogUtilGetAssociatedListsFromQuery Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByVal out_slist1 As Long, ByVal out_slist2 As Long) As Integer
Public Declare Function PcsLogUtilGetDoubleFromQuery Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsLogUtilGetStringFromQuery Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByVal out_pchar As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsLogUtilGetStringListFromQuery Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByVal out_slist1 As Long) As Integer
Public Declare Function PcsLogUtilGetStringListListFromQuery Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_query As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsLogUtilGetTimeStampFromQuery Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal in_pquery As String, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsLogUtilTranslateToDBJokerString Lib "PCSDB500.DLL" (ByVal in_pstring As String) As Integer
Public Declare Function PcsLogAttachHDBC Lib "PCSDB500.DLL" (ByVal in_phlog As Long, ByVal in_HENV As Long, ByVal in_HDBC As Long) As Long
Public Declare Function PcsLogDetachHDBC Lib "PCSDB500.DLL" (ByVal in_phlog As Long, ByRef in_HENV As Long, ByRef in_HDBC As Long) As Long



Public Declare Function PcsMaritalAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMaritalFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsMaritalFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMaritalGetDescription Lib "PCSDB500.DLL" (ByVal in_hHMARITAL As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMaritalListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsMaritalSetDescription Lib "PCSDB500.DLL" (ByVal in_hHMARITAL As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsMedicalHistoryAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMedicalHistoryFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pPatient_cond As String) As Integer
Public Declare Function PcsMedicalHistoryFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMedicalHistoryGetDescription Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMedicalHistoryGetPatient Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsMedicalHistoryGetPatientIdentifier Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMedicalHistoryGetStarted Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsMedicalHistoryListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsMedicalHistorySetDescription Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsMedicalHistorySetPatient Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsMedicalHistorySetPatientIdentifier Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsMedicalHistorySetStarted Lib "PCSDB500.DLL" (ByVal in_hHMEDICALHISTORY As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsMedProcedureAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMedProcedureFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsMedProcedureFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMedProcedureGetDescription Lib "PCSDB500.DLL" (ByVal in_hHMEDPROCEDURE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMedProcedureListRead Lib "PCSDB500.DLL" (ByVal in_hLog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsMedProcedureSetDescription Lib "PCSDB500.DLL" (ByVal in_hHMEDPROCEDURE As Long, ByVal in_pDescription As String) As Integer

Public Declare Function PcsVbPimsSendAllOrders Lib "PCSVBPIMS500.DLL" (ByVal i_PatientDboid As String, ByVal i_Module As Long) As Long
Public Declare Function PcsVbPimsFree Lib "PCSVBPIMS500.DLL" () As Long
#End If
