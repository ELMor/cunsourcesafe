VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Const PIWinOMEnfermeras As String = "OM100"
Const PIWinValEnfermeras As String = "OM101"
Const PIWinProtocolos As String = "OM102"
Const PIWinConsumos As String = "OM103"
Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
  Set objCW = mobjCW
End Sub
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  ' comienza la selecci�n del proceso
  codAsistencia = vntData(1)
  Select Case strProcess
    Case PIWinOMEnfermeras
        Dim frm As Form
        For Each frm In Forms
            If frm.Name = "frmOMEnfermeras" Then
                If CStr(vntData(1)) = frm.txtAsistencia Then 'El mismo paciente
                    frm.tvwOM.Nodes.Clear 'para que si hay alguna orden cancelada, desaparezca
                    Call frm.BorrarCampos(0)
                    Call frm.Form_Load
                    frm.fraPrincipal(0).Visible = False
                    frm.fraPrincipal(2).Visible = False
                    frm.frmHoras.Visible = False
                    frm.cmdNuevo.Visible = True
                    frm.cmdNuevo.Caption = "Nueva Perfusi�n"
                    frm.fraLista.Visible = False
                    frm.Show vbModal
                    Exit Function

'                    frm.Show vbModal
'                    Exit Function
                Else 'distinto paciente
                    frm.tvwOM.Nodes.Clear
                    Call frm.BorrarCampos(0)
                    Call frm.Form_Load
                    frm.fraPrincipal(0).Visible = False
                    frm.fraPrincipal(2).Visible = False
                    frm.frmHoras.Visible = False
                    frm.cmdNuevo.Visible = True
                    frm.cmdNuevo.Caption = "Nueva Perfusi�n"
                    frm.fraLista.Visible = False
                    frm.ssdbSueros.RemoveAll
                    frm.Show vbModal
                    Exit Function
                End If
            End If
        Next frm
        frmOMEnfermeras.Show vbModal
        Set frmOMEnfermeras = Nothing
    Case PIWinValEnfermeras
        frmValEnfermeras.Show vbModal
        Set frmValEnfermeras = Nothing
    Case PIWinProtocolos
'        frmProtocolos.Show vbModal
'        Set frmProtocolos = Nothing
    Case PIWinConsumos
        frmConsumos.Show vbModal
        Set frmConsumos = Nothing
    Case Else
        LaunchProcess = False
    End Select
  End Function
  Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a procesos
  ReDim aProcess(1 To 4, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = PIWinOMEnfermeras
  aProcess(1, 2) = "Hoja de Enfermer�a"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = PIWinValEnfermeras
  aProcess(2, 2) = "Resumen de Valoraci�n"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PIWinProtocolos
  aProcess(3, 2) = "Suspender Protocolos"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow

  aProcess(4, 1) = PIWinConsumos
  aProcess(4, 2) = "Consumos de Fluidos"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
End Sub







