VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsHojaEnfermeria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Public Property Set objClsCW(new_objCW As Object)
    Set objCW = new_objCW
    Set objApp = objCW.objApp
    Set objPipe = objCW.objPipe
    Set objGen = objCW.objGen
    Set objEnv = objCW.objEnv
    Set objError = objCW.objError
    Set objSecurity = objCW.objSecurity
    Set objMRes = CreateObject("CodeWizard.clsCWMRES")
End Property
Public Property Get strUser() As String
    strUser = strCodUser
End Property
Public Property Let strUser(new_strUser As String)
    strCodUser = new_strUser
End Property



Public Function HojaEnfermeria(codAsistencia As Long)
Dim ierrcode As Long

If Not LOG Then Exit Function 'log-on de Picis

Call objPipe.PipeSet("PI_CodAsistencia", codAsistencia)
frmOMEnfermeras.Show (vbModal)
Call objPipe.PipeRemove("PI_CodAsistencia")

'log off de Picis
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)


End Function

Public Function ValoracionEnfermeria(codAsistencia As Long)
Dim ierrcode As Long

If Not LOG Then Exit Function 'log-on de Picis

Call objPipe.PipeSet("PI_CodAsistencia", codAsistencia)
frmValEnfermeras.Show (vbModal)
Call objPipe.PipeRemove("PI_CodAsistencia")

'log off de Picis
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)


End Function
