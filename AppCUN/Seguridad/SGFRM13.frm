VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCWSegTabCol 
   Caption         =   "SEGURIDAD. Seguridad en Tablas y Columnas"
   ClientHeight    =   6510
   ClientLeft      =   735
   ClientTop       =   1650
   ClientWidth     =   10140
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   19
   Icon            =   "SGFRM13.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6510
   ScaleWidth      =   10140
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   10140
      _ExtentX        =   17886
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   327682
   End
   Begin SSDataWidgets_B.SSDBGrid grdGrid 
      Height          =   4515
      Left            =   4410
      TabIndex        =   1
      Top             =   630
      Width           =   6765
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      _ExtentX        =   11933
      _ExtentY        =   7964
      _StockProps     =   79
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picSplitter 
      BorderStyle     =   0  'None
      Height          =   4290
      Left            =   4005
      MousePointer    =   9  'Size W E
      ScaleHeight     =   4290
      ScaleWidth      =   45
      TabIndex        =   2
      Top             =   675
      Width           =   50
   End
   Begin ComctlLib.TreeView tvwTables 
      Height          =   4485
      Left            =   45
      TabIndex        =   0
      Top             =   675
      Width           =   3660
      _ExtentX        =   6456
      _ExtentY        =   7911
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   6225
      Width           =   10140
      _ExtentX        =   17886
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Datos"
      Begin VB.Menu optData 
         Caption         =   "&Guardar"
         Index           =   10
         Shortcut        =   ^G
      End
      Begin VB.Menu optData 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optData 
         Caption         =   "&Imprimir"
         Index           =   30
         Shortcut        =   ^P
      End
      Begin VB.Menu optData 
         Caption         =   "-"
         Index           =   40
      End
      Begin VB.Menu optData 
         Caption         =   "&Salir"
         Index           =   50
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edici�n"
      Begin VB.Menu optEdit 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu optEdit 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optEdit 
         Caption         =   "&Marcar todos"
         Index           =   30
         Shortcut        =   ^E
      End
      Begin VB.Menu optEdit 
         Caption         =   "&Desmarcar todos"
         Index           =   40
         Shortcut        =   ^D
      End
   End
   Begin VB.Menu mnuRegister 
      Caption         =   "&Registro"
      Begin VB.Menu optRegister 
         Caption         =   "&Primero"
         Index           =   10
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Anterior"
         Index           =   20
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Siguiente"
         Index           =   30
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Ultimo"
         Index           =   40
      End
      Begin VB.Menu optRegister 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu optRegister 
         Caption         =   "Lista de &Valores"
         Index           =   60
      End
      Begin VB.Menu optRegister 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Refrescar Registros"
         Index           =   80
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&?"
      Begin VB.Menu optHelp 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu optHelp 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmCWSegTabCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWSegTabCol
' Coded by SYSECA Bilbao
' **********************************************************************************


Dim WithEvents objTree As clsCWTree
Attribute objTree.VB_VarHelpID = -1


Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
 ' KM: this approach needed a lot more modifications than expected:
 ' intKeyCode = objTree.TreeProcess(cwTreeProcessKeys, intKeyCode, intShift)
  If intKeyCode = vbKeyF12 And intShift = vbCtrlMask + vbAltMask + vbShiftMask Then
    Call objTree.TreeProcess(cwTreeProcessDoc, 0)
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objSource As New clsCWTreeSource
  Dim objLevel  As New clsCWTreeLevel
  Dim strRol    As String
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objTree = New clsCWTree
  
  With objTree
    Set .frmForm = Me
    Set .grdGrid = grdGrid
    Set .picSplitter = picSplitter
    Set .tvwTables = tvwTables
    Set .tlbToolbar = tlbToolbar
    Set .stbStatusBar = stbStatusBar
  
    ' datos del nivel 1
    With objLevel
      .intType = cwTreeLevelTypeNothing
      .strText = "TABLAS Y COLUMNAS"
      Set .objSource = Nothing
    End With
    Call .TreeAddLevel(objLevel)
    Set objLevel = Nothing
    
    ' datos del nivel 2
    With objLevel
      .intType = cwTreeLevelTypeSQL
      .strText = "SELECT sg00id, sg00des FROM sg0000 ORDER BY sg00des"
      Set .objSource = Nothing
    End With
    Call .TreeAddLevel(objLevel)
    Set objLevel = Nothing

    ' datos del nivel 3
    With objSource
      .strCrossSQL = "SELECT sg03cod, sg09cod, sg08indacceso FROM sg0800 WHERE sg09cod = ?"
      .strCrossTable = "SG0800"
      .strText = "sg0300"
      .intType = cwTreeSourceTypeSQL
      Call .AddField("sg03cod", "", cwVariant, False, True, True, 0, "")
      Call .AddField("sg03des", "", cwVariant, False, True, True, 0, "")
      Call .AddField("", "Consultar", cwBoolean, False, False, True, 0, "")
      Call .AddField("", "Insertar", cwBoolean, False, False, True, 0, "")
      Call .AddField("", "Modificar", cwBoolean, False, False, True, 0, "")
      Call .AddField("", "Eliminar", cwBoolean, False, False, True, 0, "")
      Call .CheckPositions(3, 4, 5, 6)
      Call .LoadDictionary
    End With
    With objLevel
      .intType = cwTreeLevelTypeTables
      .strText = ""
      Set .objSource = objSource
    End With
    Call .TreeAddLevel(objLevel)
    Set objSource = Nothing
    Set objLevel = Nothing
    
    ' datos del nivel 4
    With objSource
      .strCrossSQL = "SELECT sg03cod, sg09cod, sg10cod, sg11indacceso FROM sg1100 WHERE sg09cod = ? AND sg10cod = ?"
      .strCrossTable = "SG1100"
      .strText = "sg0300"
      .intType = cwTreeSourceTypeSQL
      Call .AddField("sg03cod", "", cwVariant, False, True, True, 0, "")
      Call .AddField("sg03des", "", cwVariant, False, True, True, 0, "")
      Call .AddField("", "Visualizar", cwBoolean, False, False, True, 0, "")
      Call .AddField("", "Modificar", cwBoolean, False, False, True, 0, "")
      Call .CheckPositions(3, 4)
      Call .LoadDictionary
    End With
    With objLevel
      .intType = cwTreeLevelTypeColumns
      .strText = ""
      Set .objSource = objSource
    End With
    Call .TreeAddLevel(objLevel)
    Set objSource = Nothing
    Set objLevel = Nothing
    
    strRol = "SG0300"
    Call .TreeCreateFilterWhere(strRol, "Roles")
    Call .TreeAddFilterWhere(strRol, "SG03COD", "C�digo de Rol", cwString, objGen.ReplaceStr("SELECT SG03COD #C�digo de Rol#, SG03DES #Descripci�n del Rol# FROM SG0300 ORDER BY SG03COD", "#", Chr(34), 0))
    Call .TreeAddFilterWhere(strRol, "SG03DES", "Descripci�n del Rol", cwString)

    Call .TreeAddFilterOrder(strRol, "SG03COD", "C�digo de Rol")
    Call .TreeAddFilterOrder(strRol, "SG03DES", "Descripci�n del Rol")

    With .objPrinter
      Call .Add("sg0011", "Restricciones de Acceso a Tablas por Rol")
      Call .Add("sg0013", "Restricciones de Acceso a Columnas por Rol")
    End With
    
    Call .TreeCreateInfo
  End With
  
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Cancel = objTree.TreeExit
End Sub

Private Sub Form_Resize()
  Call objTree.TreeResize
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de clsCWTree
' -----------------------------------------------
Private Sub objTree_cwQueryUpdateRow(ByVal intLevel As Integer, ByVal cllGridValues As Collection, blnSave As Boolean, blnCancel As Boolean)
  If intLevel = 3 Then
    blnSave = (cllGridValues(3) <> 0) Or _
              (cllGridValues(4) <> 0) Or _
              (cllGridValues(5) <> 0) Or _
              (cllGridValues(6) <> 0)
  ElseIf intLevel = 4 Then
    blnSave = (cllGridValues(3) <> 0) Or _
              (cllGridValues(4) <> 0)
  End If
End Sub

Private Sub objTree_cwReadCross(ByVal intLevel As Integer, aParameters() As String, ByVal cllParents As Collection)
  If intLevel = 3 Then
    aParameters(1) = cllParents(1)
  ElseIf intLevel = 4 Then
    aParameters(1) = cllParents(2)
    aParameters(2) = cllParents(1)
  End If
End Sub

Private Sub objTree_cwReadRow(ByVal intLevel As Integer, aGridValues() As String, ByVal cllCross As Collection, ByVal cllCursor As Collection, ByVal blnExist As Boolean)
  If intLevel = 3 Then
    aGridValues(1) = cllCursor(1)
    aGridValues(2) = cllCursor(2)
    If Not blnExist Then
      aGridValues(3) = 0
      aGridValues(4) = 0
      aGridValues(5) = 0
      aGridValues(6) = 0
    Else
      aGridValues(3) = IIf(objGen.IsAnd(cllCross(3), cwRestrictTableSelect), -1, 0)
      aGridValues(4) = IIf(objGen.IsAnd(cllCross(3), cwRestrictTableInsert), -1, 0)
      aGridValues(5) = IIf(objGen.IsAnd(cllCross(3), cwRestrictTableUpdate), -1, 0)
      aGridValues(6) = IIf(objGen.IsAnd(cllCross(3), cwRestrictTableDelete), -1, 0)
    End If
  ElseIf intLevel = 4 Then
    aGridValues(1) = cllCursor(1)
    aGridValues(2) = cllCursor(2)
    If Not blnExist Then
      aGridValues(3) = 0
      aGridValues(4) = 0
    Else
      aGridValues(3) = IIf(objGen.IsAnd(cllCross(4), cwRestrictColumnView), -1, 0)
      aGridValues(4) = IIf(objGen.IsAnd(cllCross(4), cwRestrictColumnEdit), -1, 0)
    End If
  End If
End Sub

Private Sub objTree_cwUpdateRow(ByVal intLevel As Integer, ByVal cllGridValues As Collection, ByVal cllParents As Collection, rdoCursor As RDO.rdoResultset)
  If intLevel = 3 Then
    rdoCursor(0) = cllGridValues(1)
    rdoCursor(1) = cllParents(1)
    rdoCursor(2) = Abs(cllGridValues(3)) * cwRestrictTableSelect + _
                   Abs(cllGridValues(4)) * cwRestrictTableInsert + _
                   Abs(cllGridValues(5)) * cwRestrictTableUpdate + _
                   Abs(cllGridValues(6)) * cwRestrictTableDelete
  ElseIf intLevel = 4 Then
    rdoCursor(0) = cllGridValues(1)
    rdoCursor(1) = cllParents(2)
    rdoCursor(2) = cllParents(1)
    rdoCursor(3) = Abs(cllGridValues(3)) * cwRestrictColumnView + _
                   Abs(cllGridValues(4)) * cwRestrictColumnEdit
  End If
End Sub

Private Sub objTree_cwQueryReadLevel(ByVal intLevel As Integer, ByVal strValue As String, ByVal cllParents As Collection, blnCancel As Boolean)
  If intLevel = 3 Then
    If Not (strValue Like (cllParents(1) & "####")) Then
      blnCancel = True
    End If
  End If
End Sub

Private Sub objTree_cwPrint(ByVal intLevel As Integer, ByVal cllParents As Collection)
  Dim strWhere  As String
  Dim strOrder  As String
  
  With objTree.TreePrinterDialog(True, "")
    If .Selected > 0 Then
      If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
        strWhere = "WHERE " & .objFilter.strWhere
      End If
      If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
        strOrder = "ORDER BY " & .objFilter.strOrderBy
      End If
      Call .ShowReport(strWhere, strOrder)
    End If
  End With
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Grid
' -----------------------------------------------
Private Sub grdGrid_Change()
  Call objTree.TreeDataChanged
End Sub

Private Sub grdGrid_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objTree.TreeRowColChange(LastRow, LastCol)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del men�
' -----------------------------------------------
Private Sub optData_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessData, intIndex)
End Sub

Private Sub optEdit_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessEdit, intIndex)
End Sub

Private Sub optHelp_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessHelp, intIndex)
End Sub

Private Sub optRegister_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessRegister, intIndex)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Splitter
' -----------------------------------------------
Private Sub picSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.TreeMoving(cwTreeMouseDown, Button, Shift, X, Y)
End Sub

Private Sub picSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.TreeMoving(cwTreeMouseUp, Button, Shift, X, Y)
End Sub

Private Sub picSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.TreeMoving(cwTreeMouseMove, Button, Shift, X, Y)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la ToolBar
' -----------------------------------------------
Private Sub tlbtoolbar_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Call objTree.TreeProcess(cwTreeProcessToolBar, btnButton.Index)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la StatusBar
' -----------------------------------------------
Private Sub stbStatusBar_PanelDblClick(ByVal panPanel As ComctlLib.Panel)
  Call objTree.TreeProcess(cwTreeProcessStatusBar, panPanel.Index)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del TreeView
' -----------------------------------------------
Private Sub tvwTables_Collapse(ByVal nodNode As ComctlLib.Node)
  Call objTree.TreeSelected(cwTreeCollapse, nodNode)
End Sub

Private Sub tvwTables_Expand(ByVal nodNode As ComctlLib.Node)
  Call objTree.TreeSelected(cwTreeExpand, nodNode)
End Sub

Private Sub tvwTables_NodeClick(ByVal nodNode As ComctlLib.Node)
  Call objTree.TreeSelected(cwTreeSelect, nodNode)
End Sub

