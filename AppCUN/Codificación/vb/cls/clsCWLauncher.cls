VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Const PIWinCODiagPacientes As String = "CO0101"
Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
  Set objCW = mobjCW
End Sub
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean
  On Error Resume Next

   'fija el valor de retorno a verdadero
  LaunchProcess = True
  Select Case strProcess
    Case PIWinCODiagPacientes
        frmCODiagPacientes.Show vbModal
        Set frmCODiagPacientes = Nothing
    Case Else
        LaunchProcess = False
   End Select

'  Call Err.Clear
  End Function
  Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente

  ' Redimensionar la matriz a procesos
  ReDim aProcess(1 To 1, 1 To 4) As Variant

  ' VENTANAS
  aProcess(1, 1) = PIWinCODiagPacientes
  aProcess(1, 2) = "Diagn�sticos del Paciente"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

End Sub











