VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCODiagPacientes 
   Caption         =   "Diagn�sticos del Paciente"
   ClientHeight    =   8430
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   8430
   ScaleWidth      =   11880
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4320
      TabIndex        =   27
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Frame frmOpciones 
      Caption         =   "Opciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   0
      TabIndex        =   8
      Top             =   1800
      Width           =   5535
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2760
         TabIndex        =   13
         Top             =   240
         Width           =   2655
         Begin VB.OptionButton optPrincipal 
            Caption         =   "Princ."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   16
            Top             =   240
            Width           =   735
         End
         Begin VB.OptionButton optSecundario 
            Caption         =   "Secun."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   840
            TabIndex        =   15
            Top             =   240
            Width           =   855
         End
         Begin VB.OptionButton optPS 
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1680
            TabIndex        =   14
            Top             =   240
            Value           =   -1  'True
            Width           =   855
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   2535
         Begin VB.OptionButton optTodosDP 
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1560
            TabIndex        =   12
            Top             =   240
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton optProcedimientos 
            Caption         =   "Proce."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   840
            TabIndex        =   11
            Top             =   240
            Width           =   855
         End
         Begin VB.OptionButton optDiagnosticos 
            Caption         =   "Diag."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   10
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   17
         Top             =   840
         Width           =   4095
         Begin VB.OptionButton optTodos123 
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3120
            TabIndex        =   26
            Top             =   240
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton opt3 
            Caption         =   "Rep. Proc."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2040
            TabIndex        =   20
            Top             =   240
            Width           =   1095
         End
         Begin VB.OptionButton opt2 
            Caption         =   "Revisiones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   960
            TabIndex        =   19
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton opt1 
            Caption         =   "1� vez"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   240
            Width           =   1095
         End
      End
   End
   Begin VB.Frame frmDiagnosticos 
      Caption         =   "Diagn�sticos / Procedimientos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4815
      Left            =   0
      TabIndex        =   7
      Top             =   3480
      Width           =   11775
      Begin TabDlg.SSTab sstab 
         Height          =   4455
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   7858
         _Version        =   327681
         TabHeight       =   520
         TabCaption(0)   =   "Ver"
         TabPicture(0)   =   "CO0101frm.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tvwDiag"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Modificar"
         TabPicture(1)   =   "CO0101frm.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label1(6)"
         Tab(1).Control(1)=   "Label1(8)"
         Tab(1).Control(2)=   "Label1(9)"
         Tab(1).Control(3)=   "Label1(10)"
         Tab(1).Control(4)=   "txtCodigo(0)"
         Tab(1).Control(5)=   "tvwModificar"
         Tab(1).Control(6)=   "Frame4"
         Tab(1).Control(7)=   "txtOrden(0)"
         Tab(1).Control(8)=   "cmdGuardar(0)"
         Tab(1).Control(9)=   "txtObservaciones(0)"
         Tab(1).Control(10)=   "txtFecha"
         Tab(1).Control(11)=   "cmdBorrar"
         Tab(1).ControlCount=   12
         TabCaption(2)   =   "Crear"
         TabPicture(2)   =   "CO0101frm.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Label1(11)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "Label1(13)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "tvwCrear"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "txtObservaciones(1)"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "Frame5"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "cmdGuardar(1)"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "Frame6"
         Tab(2).Control(6).Enabled=   0   'False
         Tab(2).Control(7)=   "Frame7"
         Tab(2).Control(7).Enabled=   0   'False
         Tab(2).Control(8)=   "cmdNuevo"
         Tab(2).Control(8).Enabled=   0   'False
         Tab(2).Control(9)=   "txtCodigo(1)"
         Tab(2).Control(9).Enabled=   0   'False
         Tab(2).ControlCount=   10
         Begin VB.TextBox txtCodigo 
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   -73440
            TabIndex        =   47
            Top             =   600
            Width           =   855
         End
         Begin VB.CommandButton cmdNuevo 
            Caption         =   "&Nuevo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -74880
            TabIndex        =   57
            Top             =   480
            Width           =   1215
         End
         Begin VB.Frame Frame7 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -74880
            TabIndex        =   58
            Top             =   960
            Width           =   2175
            Begin VB.OptionButton optND 
               Caption         =   "Diag."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   60
               Top             =   120
               Width           =   855
            End
            Begin VB.OptionButton optNP 
               Caption         =   "Proced."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   960
               TabIndex        =   59
               Top             =   120
               Width           =   975
            End
         End
         Begin VB.Frame Frame6 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -72480
            TabIndex        =   52
            Top             =   960
            Width           =   3015
            Begin VB.OptionButton optN3 
               Caption         =   "Rev. Proce."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   1680
               TabIndex        =   55
               Top             =   120
               Width           =   1215
            End
            Begin VB.OptionButton optN2 
               Caption         =   "Rev."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   960
               TabIndex        =   54
               Top             =   120
               Width           =   975
            End
            Begin VB.OptionButton optN1 
               Caption         =   "1� vez"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   53
               Top             =   120
               Width           =   855
            End
         End
         Begin VB.CommandButton cmdGuardar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   735
            Index           =   1
            Left            =   -64440
            Style           =   1  'Graphical
            TabIndex        =   56
            Top             =   720
            Width           =   735
         End
         Begin VB.Frame Frame5 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -72240
            TabIndex        =   46
            Top             =   480
            Width           =   2415
            Begin VB.OptionButton optNPrincipal 
               Caption         =   "Principal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   49
               Top             =   120
               Width           =   975
            End
            Begin VB.OptionButton optNSecundario 
               Caption         =   "Secundario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   1080
               TabIndex        =   51
               Top             =   120
               Width           =   1215
            End
         End
         Begin VB.TextBox txtObservaciones 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   855
            Index           =   1
            Left            =   -69240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   45
            Top             =   600
            Width           =   4575
         End
         Begin ComctlLib.TreeView tvwCrear 
            Height          =   2775
            Left            =   -74880
            TabIndex        =   44
            Top             =   1560
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   4895
            _Version        =   327682
            HideSelection   =   0   'False
            Indentation     =   265
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.CommandButton cmdBorrar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   -64440
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   360
            Width           =   615
         End
         Begin VB.TextBox txtFecha 
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   -70320
            Locked          =   -1  'True
            TabIndex        =   41
            Top             =   600
            Width           =   1335
         End
         Begin VB.TextBox txtObservaciones 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   -68880
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   39
            Top             =   600
            Width           =   3495
         End
         Begin VB.CommandButton cmdGuardar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   0
            Left            =   -65280
            Style           =   1  'Graphical
            TabIndex        =   38
            Top             =   360
            Width           =   615
         End
         Begin VB.TextBox txtOrden 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   -71160
            TabIndex        =   36
            Top             =   600
            Width           =   735
         End
         Begin VB.Frame Frame4 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -73800
            TabIndex        =   33
            Top             =   480
            Width           =   2535
            Begin VB.OptionButton optS 
               Caption         =   "Secundario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   1200
               TabIndex        =   35
               Top             =   120
               Width           =   1215
            End
            Begin VB.OptionButton optP 
               Caption         =   "Principal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   34
               Top             =   120
               Width           =   1095
            End
         End
         Begin ComctlLib.TreeView tvwModificar 
            Height          =   3135
            Left            =   -74880
            TabIndex        =   32
            Top             =   1200
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   5530
            _Version        =   327682
            Indentation     =   353
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.TextBox txtCodigo 
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   -74880
            Locked          =   -1  'True
            TabIndex        =   30
            Top             =   600
            Width           =   975
         End
         Begin ComctlLib.TreeView tvwDiag 
            Height          =   3975
            Left            =   120
            TabIndex        =   29
            Top             =   360
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   7011
            _Version        =   327682
            HideSelection   =   0   'False
            Indentation     =   353
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   13
            Left            =   -73440
            TabIndex        =   50
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Observaciones"
            Height          =   255
            Index           =   11
            Left            =   -69240
            TabIndex        =   48
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha"
            Height          =   255
            Index           =   10
            Left            =   -70320
            TabIndex        =   42
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Observaciones"
            Height          =   255
            Index           =   9
            Left            =   -68880
            TabIndex        =   40
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Orden"
            Height          =   255
            Index           =   8
            Left            =   -71160
            TabIndex        =   37
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   6
            Left            =   -74880
            TabIndex        =   31
            Top             =   360
            Width           =   735
         End
      End
   End
   Begin VB.Frame frmProcAsis 
      Caption         =   "Proceso / Asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Left            =   5640
      TabIndex        =   5
      Top             =   0
      Width           =   6135
      Begin VB.CheckBox chkProcAsis 
         Caption         =   "Todos los Procesos / Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Value           =   1  'Checked
         Width           =   2775
      End
      Begin ComctlLib.ListView lstvAsistencia 
         Height          =   2655
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   4683
         SortKey         =   1
         View            =   3
         LabelEdit       =   1
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   8
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Fec. Ingreso"
            Object.Width           =   1499
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Fec. Alta"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   2
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Dep. Responsable"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   3
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Dr. Responsable"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   4
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Tipo Eco."
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(6) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   5
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Entidad"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(7) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   6
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Cod. Asistencia"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(8) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   7
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Cod. Proceso"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.Frame frmPaciente 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5535
      Begin VB.TextBox txtFecNacimiento 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   1200
         Width           =   1695
      End
      Begin VB.TextBox txtSexo 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox txtNombre 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   480
         Width           =   4095
      End
      Begin VB.TextBox txtHistoria 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Fecha nacimiento"
         Height          =   255
         Index           =   5
         Left            =   1320
         TabIndex        =   24
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Sexo"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   23
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "1� Apellido, 2� Apellido, Nombre"
         Height          =   255
         Index           =   2
         Left            =   1320
         TabIndex        =   4
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label Label1 
         Caption         =   "Historia"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   855
      End
   End
   Begin ComctlLib.ImageList iml1 
      Left            =   0
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":0054
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":036E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":0688
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":09A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":0CBC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":0FD6
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":12F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":160A
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CO0101frm.frx":1924
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCODiagPacientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBorrar_Click()
Dim str$
Dim qy As rdoQuery
Dim qyUP As rdoQuery
Dim RD As rdoResultset
Dim node As node
Dim i%, OrdenBorrada%
  Set node = tvwModificar.Nodes.Item(1)
  'Seleccionamos el orden que tiene
  OrdenBorrada = txtOrden(0)

  str = "DELETE AD4000 " & _
  "WHERE AD01CODASISTENCI=? AND AD07CODPROCESO=? AND " & _
  "AD39CODIGO=?"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
  qy(0) = Mid(node.Root.Key, 2, 10)
  qy(1) = Mid(node.Root.Key, 13, 10)
  qy(2) = txtCodigo(0)
  qy.Execute
  qy.Close
  
  str = "SELECT AD40ORDEN,AD39CODIGO FROM AD4000 " & _
  "WHERE AD01CODASISTENCI=? AND AD07CODPROCESO=? AND " & _
  "AD40ORDEN > ? ORDER BY AD40ORDEN ASC"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
  qy(0) = Mid(node.Root.Key, 2, 10)
  qy(1) = Mid(node.Root.Key, 13, 10)
  qy(2) = OrdenBorrada
  Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  
  Do While Not RD.EOF
    str = "UPDATE AD4000 SET AD40ORDEN=? " & _
    "WHERE AD01CODASISTENCI=? AND AD07CODPROCESO=? AND " & _
    "AD39CODIGO = ? "
    Set qyUP = objApp.rdoConnect.CreateQuery("", str)
    qyUP(0) = Val(RD!AD40ORDEN - 1)
    qyUP(1) = Mid(node.Root.Key, 2, 10)
    qyUP(2) = Mid(node.Root.Key, 13, 10)
    qyUP(3) = RD!AD39CODIGO
    qyUP.Execute
    RD.MoveNext
  Loop
  
  tvwModificar.Nodes.Clear
  Call cmdConsultar_Click
  sstab.Tab = 0

End Sub

Private Sub cmdConsultar_Click()
Dim i%
Dim node As node
tvwDiag.Nodes.Clear
tvwModificar.Nodes.Clear
tvwCrear.Nodes.Clear
If chkProcAsis.Value = 1 Then
    For i = 1 To lstvAsistencia.ListItems.Count
        lstvAsistencia.SelectedItem = lstvAsistencia.ListItems.Item(i)
        Call EscribirTreeView(i)
    Next i
Else
    i = lstvAsistencia.SelectedItem.Index
    Call EscribirTreeView(i)
End If
DoEvents
Set lstvAsistencia.SelectedItem = lstvAsistencia.ListItems.Item(1)
Set tvwDiag.SelectedItem = tvwDiag.Nodes.Item(1)
End Sub

Private Sub cmdGuardar_Click(Index As Integer)
Dim str$
Dim qy As rdoQuery
Dim RD As rdoResultset
Dim qyUP As rdoQuery
Dim node As node
Dim i%, Ord%
If Index = 0 Then
  Set node = tvwModificar.SelectedItem
  str = "UPDATE AD4000 SET " & _
   "AD40ORDEN=?, AD40TIPODIAGPROC=?," & _
   "AD40OBSERVACIONES=? WHERE " & _
   "AD39CODIGO= ? AND AD01CODASISTENCI=? AND AD07CODPROCESO=?"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
      qy(0) = txtOrden(Index)
      If node.Image = constDIAGNOSTICO Then
        qy(1) = "P"
      Else
        qy(2) = "S"
      End If
      If Trim(txtObservaciones(Index)) = "" Then
          qy(2) = Null
      Else
          qy(2) = txtObservaciones(Index)
      End If
      qy(3) = txtCodigo(Index)
      
      qy(4) = Mid(node.Root.Key, 2, 10)
      qy(5) = Mid(node.Root.Key, 13, 10)
  qy.Execute
  qy.Close
  str = "SELECT AD39CODIGO FROM AD4000 WHERE AD01CODASISTENCI=? " & _
  "AND AD07CODPROCESO=? AND AD40ORDEN=?"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
      qy(0) = Mid(node.Root.Key, 2, 10)
      qy(1) = Mid(node.Root.Key, 13, 10)
      qy(2) = txtOrden(0)
  Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  If RD.RowCount > 1 Then
    RD.Close
    qy.Close
    str = "SELECT AD39CODIGO,AD40ORDEN FROM AD4000 WHERE AD01CODASISTENCI=? " & _
    "AND AD07CODPROCESO=? AND AD39CODIGO <> ? ORDER BY AD40ORDEN"
    Set qy = objApp.rdoConnect.CreateQuery("", str)
        qy(0) = Mid(node.Root.Key, 2, 10)
        qy(1) = Mid(node.Root.Key, 13, 10)
        qy(2) = txtCodigo(Index)
   Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    Ord = 1
    Do While Not RD.EOF
      If Ord = txtOrden(Index) Then
        Ord = Ord + 1
      End If
      str = "UPDATE AD4000 SET AD40ORDEN=? WHERE AD01CODASISTENCI=? " & _
      "AND AD07CODPROCESO=? AND AD39CODIGO=?"
      Set qyUP = objApp.rdoConnect.CreateQuery("", str)
          qyUP(0) = Ord
          qyUP(1) = Mid(node.Root.Key, 2, 10)
          qyUP(2) = Mid(node.Root.Key, 13, 10)
          qyUP(3) = RD!AD39CODIGO
      qyUP.Execute
      qyUP.Close
      Ord = Ord + 1
      RD.MoveNext
    Loop
    RD.Close
    qy.Close
  End If
  tvwModificar.Nodes.Clear
Else 'tab Crear
Ord = 1
    For i = 1 To tvwCrear.Nodes.Count Step 1
      Set node = tvwCrear.Nodes.Item(i)
      If node.Image = constDIAGNOSTICO Or node.Image = constDIAGSECUNDARIO Then
        str = "INSERT INTO AD4000 (AD01CODASISTENCI, AD07CODPROCESO, " & _
        "AD39CODIGO, AD40TIPODIAGPROC, AD40TIPOCOD, AD40OBSERVACIONES, AD40ORDEN)" & _
        " VALUES (?,?,?,?,?,?,?)"
        Set qy = objApp.rdoConnect.CreateQuery("", str)
          qy(0) = Mid(node.Root.Key, 2, 10)
          qy(1) = Mid(node.Root.Key, 13, 10)
          qy(2) = Mid(node.Key, 2, Len(node.Key) - 1)
          If tvwCrear.Nodes.Item(i).Image = constDIAGNOSTICO Then
            qy(3) = "P"
          Else
            qy(3) = "S"
          End If
          qy(4) = EsTipo123(node)
          If txtObservaciones(Index) = "" Then
            qy(5) = Null
          Else
            qy(5) = txtObservaciones(Index)
          End If
          qy(6) = Ord
          Ord = Ord + 1
          On Error GoTo label
        qy.Execute
        qy.Close
      End If
    Next
End If
Call cmdConsultar_Click

sstab.Tab = 0
Exit Sub
label:
If Err = 40002 Then
  Resume Next
Else
  MsgBox "Ha habido un error guardando los registros", vbCritical, ""
End If
End Sub

Private Sub cmdNuevo_Click()
  txtCodigo(1) = ""
  txtCodigo(1).SetFocus
  optNPrincipal = False
  optNSecundario = False
  optN1 = False
  optN2 = False
  optN3 = False
  optND = False
  optNP = False
  txtObservaciones(1) = ""

End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
Set cmdGuardar(0).Picture = iml1.ListImages.Item(constGUARDAR).Picture
Set cmdGuardar(1).Picture = iml1.ListImages.Item(constGUARDAR).Picture
Set cmdBorrar.Picture = iml1.ListImages.Item(constBORRAR).Picture
txtHistoria.SetFocus
End Sub



Private Sub lstvAsistencia_ItemClick(ByVal Item As ComctlLib.ListItem)
'  sstab.Tab = 0
'  tvwCrear.Nodes.Clear
'  tvwDiag.Nodes.Clear
'  tvwModificar.Nodes.Clear
  tvwCrear.Nodes.Clear
  Call sstab_Click(0)
End Sub

Private Sub optNPrincipal_Click()
  Dim node As node
  Set node = tvwCrear.SelectedItem
  node.Image = constDIAGNOSTICO
  txtObservaciones(1).Locked = False
End Sub

Private Sub optNSecundario_Click()
  Dim node As node
  Set node = tvwCrear.SelectedItem
  node.Image = constDIAGSECUNDARIO
  txtObservaciones(1) = ""
  txtObservaciones(1).Locked = True
End Sub

Private Sub optP_Click(Index As Integer)
  Dim node As node
  Set node = tvwModificar.SelectedItem
  node.Image = constDIAGNOSTICO
    txtObservaciones(Index).Locked = False
End Sub

Private Sub optS_Click(Index As Integer)
  Dim node As node
  Set node = tvwModificar.SelectedItem
  node.Image = constDIAGSECUNDARIO
    txtObservaciones(Index) = ""
    txtObservaciones(Index).Locked = True
End Sub

Private Sub sstab_Click(PreviousTab As Integer)
Dim node As node
Dim nodediag As node
Dim nodeProv As node
Dim Index%
Dim Key$, Descripcion$, key3$, key5$, key2$, key4$
Dim str$, CodigoPadre$, DesPadre$
Dim RD As rdoResultset
Dim qy As rdoQuery
Dim rdASIS As rdoResultset
Dim qyASIS As rdoQuery
Dim rdPadre As rdoResultset
Dim qyPadre As rdoQuery
Dim DesCod$, j%, Pos%
Dim Codigo$, tipocodigo$
On Error GoTo label
Err = 0
If PreviousTab = 0 And sstab.Tab = 1 Then
  j = 0
    If tvwDiag.SelectedItem Is Nothing Then
      sstab.Tab = 0
      MsgBox "Debe seleccionar un Diagn�stico o Procedimiento", vbOKOnly, ""
      Exit Sub
    End If
    Set nodediag = tvwDiag.SelectedItem
    If tvwDiag.SelectedItem.Image <> constCHECKROJO And _
    tvwDiag.SelectedItem.Image <> constCHECKAZUL And _
    tvwDiag.SelectedItem.Image <> constDIAGNOSTICO And _
    tvwDiag.SelectedItem.Image <> constDIAGSECUNDARIO Then
        sstab.Tab = 0
        MsgBox "Elija un Diagn�stico o un Procedimiento, no un elemento superior", vbCritical, ""
        Exit Sub
    End If
    Pos = InStr(1, nodediag.Key, "D")
    If Pos = 0 Then Pos = InStr(14, nodediag.Key, "P")
    txtCodigo(j) = Mid(nodediag.Key, Pos + 1, Len(nodediag.Key) - (Pos))
    
    tvwModificar.Nodes.Clear
    'Encontramos el padre
    Set nodeProv = tvwDiag.SelectedItem
    Do While Left(nodeProv.Key, 1) <> "A"
        Set nodeProv = nodeProv.Parent
    Loop
    'A�adimos el padre al treeview Modificar
        Set tvwModificar.ImageList = iml1
        Key = "A" + Mid(nodeProv.Key, 2, 10) + "P" + Mid(nodeProv.Key, 13, 10)
        str = "SELECT AD0500.AD01CODASISTENCI, AD0500.AD07CODPROCESO, " & _
        "AD0800.AD08FECINICIO, AD0800.AD08FECFIN, SG0200.SG02TXTFIRMA, " & _
        "AD0200.AD02DESDPTO, AD1100.CI32CODTIPECON, AD1100.CI13CODENTIDAD " & _
        "FROM AD0200, AD0500, AD1100, " & _
        "AD0800, SG0200 WHERE " & _
        "AD0500.AD01CODASISTENCI = AD0800.AD01CODASISTENCI " & _
        "AND AD0500.AD07CODPROCESO = AD0800.AD07CODPROCESO " & _
        "AND AD0500.SG02COD = SG0200.SG02COD AND " & _
        "AD0500.AD02CODDPTO = AD0200.AD02CODDPTO AND " & _
        "AD1100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI " & _
        "AND AD1100.AD07CODPROCESO = AD0800.AD07CODPROCESO " & _
        "AND AD1100.AD01CODASISTENCI=? AND AD1100.AD07CODPROCESO=?"
        Set qyASIS = objApp.rdoConnect.CreateQuery("", str)
        qyASIS(0) = Mid(nodeProv.Key, 2, 10)
        qyASIS(1) = Mid(nodeProv.Key, 13, 10)
        Set rdASIS = qyASIS.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        
        Descripcion = Format(rdASIS!AD08FECINICIO, "DD/MM/YYYY")
        If Not IsNull(rdASIS!AD08FECFIN) Then
          Descripcion = Descripcion + " " + Format(rdASIS!AD08FECFIN, "DD/MM/YYYY")
        End If
        If Not IsNull(rdASIS!AD02DESDPTO) Then
          Descripcion = Descripcion + " " + rdASIS!AD02DESDPTO
        End If
        If Not IsNull(rdASIS!SG02TXTFIRMA) Then
          Descripcion = Descripcion + " " + rdASIS!SG02TXTFIRMA
        End If
          Descripcion = Descripcion + " " + rdASIS!CI32CODTIPECON + " " + rdASIS!CI13CODENTIDAD
        Set node = tvwModificar.Nodes.Add(, , Key, Descripcion, constASISTENCIA)
     'A�adimos los datos a la pesta�a Modificar
        str = "SELECT AD3900.AD39CODIGO, AD3900.AD39DESCODIGO, AD4000.AD40TIPODIAGPROC,AD4000.AD40OBSERVACIONES, " & _
        "AD4000.AD40FECADD, AD4000.AD40FECUPD, " & _
        "AD4000.AD40ORDEN, AD3900.AD39TIPOCODIGO, AD4000.AD40TIPOCOD FROM AD4000, AD3900 " & _
        "WHERE AD3900.AD39CODIGO=AD4000.AD39CODIGO AND " & _
        "AD4000.AD01CODASISTENCI=? AND AD4000.AD07CODPROCESO=? AND " & _
        "AD4000.AD39CODIGO= ? "
        Set qy = objApp.rdoConnect.CreateQuery("", str)
            qy(0) = Mid(nodeProv.Key, 2, 10)
            qy(1) = Mid(nodeProv.Key, 13, 10)
            qy(2) = txtCodigo(j).Text
        Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            
            If RD!AD39TIPOCODIGO = "D" Then
              If Left(RD!AD39CODIGO, 1) = "M" Then 'Si el diagnostico empieza por E
                If RD!AD40TIPODIAGPROC = "P" Then
                  Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                Else
                  Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                End If
              ElseIf Left(RD!AD39CODIGO, 1) = "E" Then 'Si el diagnostico empieza por E
                Select Case Len(RD!AD39CODIGO)
                Case 4
                    If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                    Else
                      Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    End If
                Case 6 'Si es un hijo
                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                      If RD!AD40TIPODIAGPROC = "P" Then
                        Set node = tvwModificar.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                      Else
                        Set node = tvwModificar.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                      End If
                End Select
              Else
                Select Case Len(RD!AD39CODIGO)
                Case 3
                    If RD!AD40TIPODIAGPROC = "P" Then
                        Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                    Else
                        Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    End If
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwModificar.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwModificar.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                Case 6
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    
                    CodigoPadre = Left(RD!AD39CODIGO, 5)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key5 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(key3, tvwChild, key5, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwModificar.Nodes.Add(key5, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwModificar.Nodes.Add(key5, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                End Select
              End If
            Else 'procedimiento
                Select Case Len(RD!AD39CODIGO)
                Case 2
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwModificar.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                Case 4
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwModificar.Nodes.Add(key2, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwModificar.Nodes.Add(key2, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    
                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key4 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwModificar.Nodes.Add(key2, tvwChild, key4, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwModificar.Nodes.Add(key4, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwModificar.Nodes.Add(key4, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                End Select
            End If
            tvwModificar.SelectedItem = node
            '*
            Select Case RD!AD40TIPOCOD
                Case 1
                    DesCod = "(1� vez)"
                Case 2
                    DesCod = "(Rev.)"
                Case 3
                    DesCod = "(Repet. Proc.)"
            End Select
            If Not IsNull(RD!AD40OBSERVACIONES) Then txtObservaciones(j) = RD!AD40OBSERVACIONES
            If RD!AD40TIPODIAGPROC = "P" Then
                optP(j) = True
                txtObservaciones(j).Locked = False
            Else
                optS(j) = True
                txtObservaciones(j).Locked = True
            End If
            txtOrden(j) = RD!AD40ORDEN
            If Not IsNull(RD!AD40FECUPD) Then
                txtFecha = Format(RD!AD40FECUPD, "DD/MM/YYYY")
            Else
                txtFecha = Format(RD!AD40FECADD, "DD/MM/YYYY")
            End If
            '*
            
            RD.MoveNext
            node.EnsureVisible
        RD.Close
        qy.Close
ElseIf PreviousTab = 0 And sstab.Tab = 2 Then
  Call cmdNuevo_Click 'Limpiamos los campos antes de cambiar de tab
  Set tvwCrear.ImageList = iml1
  Call EscribirDaignosticosCrear
    'tvwCrear.Nodes.Clear
        Key = "A" + lstvAsistencia.SelectedItem.SubItems(6) + "P" + lstvAsistencia.SelectedItem.SubItems(7)
        Descripcion = Format(lstvAsistencia.SelectedItem.Text, "DD/MM/YYYY") & _
        " " + Format(lstvAsistencia.SelectedItem.SubItems(1), "DD/MM/YYYY") & _
        " " + lstvAsistencia.SelectedItem.SubItems(2) + " " + lstvAsistencia.SelectedItem.SubItems(3) & _
        " " + lstvAsistencia.SelectedItem.SubItems(4) + " " + lstvAsistencia.SelectedItem.SubItems(5)
        Set node = tvwCrear.Nodes.Add(, , Key, Descripcion, 3)
     'A�adimos los datos a la pesta�a Crear
    Dim i%
    'nodediag = nodediag.Root
    For i = 1 To tvwDiag.Nodes.Count Step 1
        If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Or tvwDiag.Nodes.Item(i).Image = constCHECKAZUL Then
          Set nodediag = tvwDiag.Nodes.Item(i)
        '  Codigo = Mid(tvwDiag.Nodes.Item(i).Key, 2, Len(tvwDiag.Nodes.Item(i).Key) - 1)
          Pos = InStr(1, nodediag.Key, "D")
          If Pos = 0 Then Pos = InStr(14, nodediag.Key, "P")
          Codigo = Mid(nodediag.Key, Pos + 1, Len(nodediag.Key) - (Pos))
          tipocodigo = Mid(tvwDiag.Nodes.Item(i).Key, Pos, 1)
          str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
          Set qy = objApp.rdoConnect.CreateQuery("", str)
              qy(0) = Codigo
          Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          If tipocodigo = "D" Then
              If Left(Codigo, 1) = "M" Then 'Si el diagnostico empieza por E
                If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGNOSTICO)
                    tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                Else
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGSECUNDARIO)
                    tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                End If
              ElseIf Left(Codigo, 1) = "E" Then 'Si el diagnostico empieza por E
                Select Case Len(Codigo)
                Case 4
                  If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGNOSTICO)
                      tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                  Else
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGSECUNDARIO)
                      tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                  End If
                Case 6 'Si es un hijo
                    CodigoPadre = Left(Codigo, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = Key + "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                      If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                          Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGNOSTICO)
                          tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                      Else
                          Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGSECUNDARIO)
                          tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                      End If
                End Select
              Else
                Select Case Len(Codigo)
                  Case 3
                      If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                          Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGNOSTICO)
                          tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                      Else
                          Set node = tvwCrear.Nodes.Add(Key, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, constDIAGSECUNDARIO)
                          tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                      End If
                  Case 5
                      CodigoPadre = Left(Codigo, 3)
                      str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                      Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                          qyPadre(0) = CodigoPadre
                      Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                      DesPadre = rdPadre!AD39DESCODIGO
                      key3 = "D" & CodigoPadre
                      rdPadre.Close
                      qyPadre.Close
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, tipocodigo + CodigoPadre + " " + DesPadre)
                      If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                          Set node = tvwCrear.Nodes.Add(key3, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 1)
                          tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                      Else
                          Set node = tvwCrear.Nodes.Add(key3, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 4)
                          tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                      End If
                  Case 6
                      CodigoPadre = Left(Codigo, 3)
                      str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                      Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                          qyPadre(0) = CodigoPadre
                      Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                      DesPadre = rdPadre!AD39DESCODIGO
                      key3 = "D" & CodigoPadre
                      rdPadre.Close
                      qyPadre.Close
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, tipocodigo + CodigoPadre + " " + DesPadre)
  
                      CodigoPadre = Left(Codigo, 5)
                      str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                      Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                          qyPadre(0) = CodigoPadre
                      Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                      DesPadre = rdPadre!AD39DESCODIGO
                      key5 = "D" & CodigoPadre
                      rdPadre.Close
                      qyPadre.Close
                      Set node = tvwCrear.Nodes.Add(key3, tvwChild, key5, tipocodigo + CodigoPadre + " " + DesPadre)
                          If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                              Set node = tvwCrear.Nodes.Add(key5, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 1)
                              tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                         Else
                              Set node = tvwCrear.Nodes.Add(key5, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 4)
                              tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                          End If
                  End Select
                End If
              Else 'procedimiento
                  Select Case Len(Codigo)
                  Case 2
                          If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                              Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 1)
                              tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                          Else
                              Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 4)
                              tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                          End If
                  Case 4
                      CodigoPadre = Left(Codigo, 2)
                      str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                      Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                          qyPadre(0) = CodigoPadre
                      Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                      DesPadre = rdPadre!AD39DESCODIGO
                      key2 = "P" & CodigoPadre
                      rdPadre.Close
                      qyPadre.Close
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, key2, RD!AD39DESCODIGO)
                          If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                              Set node = tvwCrear.Nodes.Add(key2, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 1)
                              tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                          Else
                              Set node = tvwCrear.Nodes.Add(key2, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 4)
                              tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                          End If
                  Case 5
                      CodigoPadre = Left(Codigo, 2)
                      str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                      Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                          qyPadre(0) = CodigoPadre
                      Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                      DesPadre = rdPadre!AD39DESCODIGO
                      key2 = "P" & CodigoPadre
                      rdPadre.Close
                      qyPadre.Close
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, key2, tipocodigo + CodigoPadre + " " + DesPadre)
  
                      CodigoPadre = Left(Codigo, 4)
                      str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                      Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                          qyPadre(0) = CodigoPadre
                      Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                      DesPadre = rdPadre!AD39DESCODIGO
                      key4 = "P" & CodigoPadre
                      rdPadre.Close
                      qyPadre.Close
                      Set node = tvwCrear.Nodes.Add(key2, tvwChild, key4, tipocodigo + CodigoPadre + " " + DesPadre)
                          If tvwDiag.Nodes.Item(i).Image = constCHECKROJO Then
                              Set node = tvwCrear.Nodes.Add(key4, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 1)
                              tvwDiag.Nodes.Item(i).Image = constDIAGNOSTICO
                          Else
                              Set node = tvwCrear.Nodes.Add(key4, tvwChild, tipocodigo + Codigo, tipocodigo + Codigo + " " + RD!AD39DESCODIGO, 4)
                              tvwDiag.Nodes.Item(i).Image = constDIAGSECUNDARIO
                         End If
                  End Select
            End If
            Set tvwCrear.SelectedItem = node
            Call tvwCrear_Click
            node.EnsureVisible
            RD.Close
            qy.Close
        End If
    Next
ElseIf PreviousTab = 2 And sstab.Tab = 1 Then
  sstab.Tab = 2
End If
Exit Sub
label:
If Err = 35602 Then
    Resume Next
ElseIf Err = 91 And sstab.Tab = 2 Then
  MsgBox "Un Diagnostico est� ya seleccionado en la carpeta Crear", vbCritical, ""
Else
  MsgBox "Error", vbCritical, ""
End If
End Sub


Private Sub tvwCrear_Click()
Dim str$
Dim RD As rdoResultset
Dim qy As rdoQuery

  Dim node As node
  Set node = tvwCrear.SelectedItem
  If node.Children = 0 Then
    If node.Image = constDIAGNOSTICO Or node.Image = constDIAGSECUNDARIO Then
      txtCodigo(1) = Mid(node.Key, 2, Len(node.Key) - 1)
      If Left(node.Key, 1) = "P" Then
        optNP = True
      ElseIf Left(node.Key, 1) = "D" Then
        optND = True
      End If
      If node.Image = constDIAGNOSTICO Then
        optNPrincipal = True
      ElseIf node.Image = constDIAGSECUNDARIO Then
        optNSecundario = True
      End If
      Call EsTipo123(node)
    Else
      txtCodigo(1) = ""
    End If
    str = "SELECT AD40OBSERVACIONES FROM AD4000 WHERE " & _
    "AD01CODASISTENCI=? AND AD07CODPROCESO=? AND " & _
    "AD39CODIGO=?"
    Set qy = objApp.rdoConnect.CreateQuery("", str)
    qy(0) = Mid(node.Root.Key, 2, 10)
    qy(1) = Mid(node.Root.Key, 13, 10)
    qy(2) = txtCodigo(1)
    Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If Not (IsNull(RD!AD40OBSERVACIONES) Or RD.RowCount = 0) Then
      txtObservaciones(1) = RD!AD40OBSERVACIONES
    End If
    RD.Close
    qy.Close
  Else
    Call cmdNuevo_Click
  End If
End Sub


Private Sub tvwCrear_KeyUp(KeyCode As Integer, Shift As Integer)
Dim node As node
Dim i%
Dim Res%
  If KeyCode = 46 Then
    Res = MsgBox("�Desea borrar esta codificaci�n?", vbYesNo + vbQuestion + vbDefaultButton2, "")
      If Res = vbYes Then
        i = tvwCrear.SelectedItem.Index
        tvwCrear.Nodes.Remove (i)
        Call cmdNuevo_Click
      End If
  End If
End Sub

Private Sub tvwDiag_KeyUp(KeyCode As Integer, Shift As Integer)
Dim node As node
Dim i%
Dim Res%
Dim str$
Dim qy As rdoQuery
  Set node = tvwDiag.SelectedItem
  If KeyCode = 46 And node.Parent Is Nothing Then
    Res = MsgBox("�Desea borrar toda la codificaci�n de este Proceso/Asistencia?", vbYesNo + vbQuestion + vbDefaultButton2, "")
      If Res = vbYes Then
            str = "DELETE AD4000 " & _
            "WHERE AD01CODASISTENCI=? AND AD07CODPROCESO=? "
            Set qy = objApp.rdoConnect.CreateQuery("", str)
            qy(0) = Mid(node.Key, 2, 10)
            qy(1) = Mid(node.Key, 13, 10)
            qy.Execute
            qy.Close
          Call cmdConsultar_Click
      End If
  End If
End Sub

Private Sub tvwDiag_NodeClick(ByVal node As ComctlLib.node)
Set node = tvwDiag.SelectedItem
Select Case node.Image
Case constDIAGNOSTICO
   node.Image = constCHECKROJO
Case constDIAGSECUNDARIO
   node.Image = constCHECKAZUL
Case constCHECKROJO
   node.Image = constDIAGNOSTICO
Case constCHECKAZUL
    node.Image = constDIAGSECUNDARIO
End Select
End Sub



Private Sub txtCodigo_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
Dim Tipo%, str$, ms%
Dim Codigo$
Dim RD As rdoResultset
Dim qy As rdoQuery
Dim node As node

If KeyCode = 13 Then
Me.MousePointer = vbHourglass
  Set node = tvwCrear.Nodes.Item(1)
    If Len(txtCodigo(1)) < 2 Then
      optNP = True
    ElseIf Mid(txtCodigo(1), 3, 1) = "." Then
      optNP = True
    Else
      optND = True
    End If
    Tipo = EsTipo123(node)
    str = "SELECT AD39CODIGO FROM AD3900 WHERE AD39TIPOCODIGO=? AND " & _
    "AD39CODIGO LIKE ? AND AD39CODIGO<>?"
    Set qy = objApp.rdoConnect.CreateQuery("", str)
      If optNP Then
        qy(0) = "P"
      Else
        qy(0) = "D"
      End If
      qy(1) = txtCodigo(1) + "%"
      qy(2) = txtCodigo(1)
    Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If RD.RowCount > 0 Then
      ms = MsgBox("Esta Codificaci�n tiene un elemento inferior.�Desea continuar?", vbYesNo + vbQuestion + vbDefaultButton2, "")
        If ms = vbNo Then
          Call cmdNuevo_Click
          Exit Sub
        End If
    End If
    txtObservaciones(1).Locked = True
    Call EscribirtvwCrear
    optNSecundario = False
  cmdNuevo.SetFocus
  Me.MousePointer = vbDefault
End If
End Sub

Private Sub txtHistoria_Change()
  txtNombre = ""
  txtSexo = ""
  txtFecNacimiento = ""
  lstvAsistencia.ListItems.Clear
  tvwDiag.Nodes.Clear
End Sub

Private Sub txtHistoria_Keydown(KeyCode As Integer, Shift As Integer)
  If KeyCode = 13 Then lstvAsistencia.SetFocus
End Sub

Private Sub txtHistoria_LostFocus()
Dim itmX As ListItem
Dim str$, Nombre
Dim RD As rdoResultset
Dim qy As rdoQuery
Me.MousePointer = vbHourglass
lstvAsistencia.ListItems.Clear
tvwDiag.Nodes.Clear
tvwCrear.Nodes.Clear
tvwModificar.Nodes.Clear
If txtHistoria <> "" Then
str = "SELECT CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, " & _
"CI3000.CI30DESSEXO, CI2200.CI22FECNACIM " & _
"FROM CI2200,CI3000 WHERE CI2200.CI22NUMHISTORIA=? and " & _
"CI3000.CI30CODSEXO=CI2200.CI30CODSEXO"
Set qy = objApp.rdoConnect.CreateQuery("", str)
qy(0) = txtHistoria
Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    If RD.RowCount <> 0 Then
            Nombre = RD!CI22PRIAPEL + ", "
        If Not IsNull(RD!CI22SEGAPEL) Then
            Nombre = Nombre + RD!CI22SEGAPEL
        End If
        txtNombre = Nombre + ", " + RD!CI22NOMBRE
        If Not IsNull(RD!CI30DESSEXO) Then
            txtSexo = RD!CI30DESSEXO
        Else: txtSexo = ""
        End If
        If Not IsNull(RD!CI22FECNACIM) Then
            txtFecNacimiento = RD!CI22FECNACIM
        Else: txtFecNacimiento = ""
        End If
    Else
        txtNombre = ""
        txtSexo = ""
        txtFecNacimiento = ""
    End If
RD.Close
qy.Close

    str = "SELECT AD0800.AD01CODASISTENCI, AD0800.AD07CODPROCESO, " & _
    "AD0800.AD08FECINICIO, AD0800.AD08FECFIN, AD0200.AD02DESDPTO, " & _
    "SG0200.SG02TXTFIRMA, AD1100.CI32CODTIPECON, AD1100.CI13CODENTIDAD " & _
    "FROM AD0100, AD0200, AD0500, " & _
    " AD0800, AD1100, SG0200, AD2500 " & _
    "Where AD0100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI " & _
    "And AD0100.AD01CODASISTENCI = AD0500.AD01CODASISTENCI And " & _
    "AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO And " & _
    "AD0500.AD02CODDPTO = AD0200.AD02CODDPTO And " & _
    "AD0500.SG02COD = SG0200.SG02COD And " & _
    "AD1100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI " & _
    "And AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO And " & _
    "AD0100.AD01CODASISTENCI=AD2500.AD01CODASISTENCI AND " & _
    "AD2500.AD12CODTIPOASIST= 1 AND " & _
    "AD1100.AD11FECFIN Is Null AND " & _
    "AD0500.AD05FECFINRESPON Is Null AND " & _
    "AD0100.CI22NUMHISTORIA = ? order by AD0800.AD08FECFIN DESC"
    Set qy = objApp.rdoConnect.CreateQuery("", str)
        qy(0) = txtHistoria
    Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If RD.RowCount = 0 Then
    MsgBox "Este paciente no ha tenido nunca asistencias abiertas", vbInformation, ""
    Else
        Do While Not RD.EOF
            Set itmX = lstvAsistencia.ListItems.Add(, , RD!AD08FECINICIO)
            If Not IsNull(RD!AD08FECFIN) Then
                itmX.SubItems(1) = CStr(RD!AD08FECFIN)
            End If
            itmX.SubItems(2) = CStr(RD!AD02DESDPTO)
            If Not IsNull(RD!SG02TXTFIRMA) Then
                itmX.SubItems(3) = CStr(RD!SG02TXTFIRMA)
            End If
            itmX.SubItems(4) = CStr(RD!CI32CODTIPECON)
            itmX.SubItems(5) = CStr(RD!CI13CODENTIDAD)
            itmX.SubItems(6) = CStr(RD!AD01CODASISTENCI)
            itmX.SubItems(7) = CStr(RD!AD07CODPROCESO)
            RD.MoveNext
        Loop
        Set lstvAsistencia.SelectedItem = lstvAsistencia.ListItems.Item(1)
    End If
    RD.Close
    qy.Close
End If
Me.MousePointer = vbDefault
End Sub

Private Sub EscribirTreeView(i%)
Dim node As node
Dim Key$, Descripcion$, key3$, key5$, key2$, key4$
Dim str$, CodigoPadre$, DesPadre$
Dim RD As rdoResultset
Dim qy As rdoQuery
Dim rdPadre As rdoResultset
Dim qyPadre As rdoQuery
Dim blnDP As Boolean
Dim blnPS As Boolean, bln123 As Boolean
Dim Tipo123%
Dim TipoDP$, TipoPS$, DesCod$
On Error GoTo label
Me.MousePointer = vbHourglass
        Set node = tvwDiag.SelectedItem
        Set tvwDiag.ImageList = iml1
        Key = "A" + lstvAsistencia.SelectedItem.SubItems(6) + "P" + lstvAsistencia.SelectedItem.SubItems(7)
        Descripcion = Format(lstvAsistencia.SelectedItem.Text, "DD/MM/YYYY") & _
        " " + Format(lstvAsistencia.SelectedItem.SubItems(1), "DD/MM/YYYY") & _
        " " + lstvAsistencia.SelectedItem.SubItems(2) + " " + lstvAsistencia.SelectedItem.SubItems(3) & _
        " " + lstvAsistencia.SelectedItem.SubItems(4) + " " + lstvAsistencia.SelectedItem.SubItems(5)
        Set node = tvwDiag.Nodes.Add(, , Key, Descripcion, 3)
        
        str = "SELECT AD3900.AD39CODIGO, AD3900.AD39DESCODIGO, AD4000.AD40TIPODIAGPROC, " & _
        "AD4000.AD40ORDEN, AD3900.AD39TIPOCODIGO, AD4000.AD40TIPOCOD FROM AD4000, AD3900 " & _
        "WHERE AD3900.AD39CODIGO=AD4000.AD39CODIGO AND " & _
        "AD4000.AD01CODASISTENCI=? AND AD4000.AD07CODPROCESO=? "
        If Not optTodosDP Then
            str = str + "AND AD3900.AD39TIPOCODIGO=? "
            blnDP = True
            If optDiagnosticos Then TipoDP = "D" Else TipoDP = "P"
        End If
        If Not optPS Then
            str = str + "AND AD4000.AD40TIPODIAGPROC=? "
            blnPS = True
            If optPrincipal Then TipoPS = "P" Else TipoPS = "S"
        End If
        
        If Not optTodos123 Then
            str = str + "AND AD4000.AD40TIPOCOD=? "
            bln123 = True
            If opt1 = True Then Tipo123 = 1
            If opt2 = True Then Tipo123 = 2
            If opt3 = True Then Tipo123 = 3
        End If
        str = str + "order by AD4000.AD40ORDEN"
        Set qy = objApp.rdoConnect.CreateQuery("", str)
            qy(0) = lstvAsistencia.SelectedItem.SubItems(6)
            qy(1) = lstvAsistencia.SelectedItem.SubItems(7)
            If blnDP Then
                qy(2) = TipoDP
                If blnPS Then
                    qy(3) = TipoPS
                        If bln123 Then
                            qy(4) = Tipo123
                        End If
                Else
                    If bln123 Then
                        qy(3) = Tipo123
                    End If
                End If
            Else
                If blnPS Then
                    qy(2) = TipoPS
                        If bln123 Then
                            qy(3) = Tipo123
                        End If
                Else
                    If bln123 Then
                        qy(2) = Tipo123
                    End If
                End If
            End If
                    
        Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        blnDP = False
        blnPS = False
        Tipo123 = 0
        
        Do While Not RD.EOF
            Select Case RD!AD40TIPOCOD
                Case 1
                    DesCod = "(1� vez)"
                Case 2
                    DesCod = "(Rev.)"
                Case 3
                    DesCod = "(Repet. Proc.)"
            End Select
            If RD!AD39TIPOCODIGO = "D" Then
              If Left(RD!AD39CODIGO, 1) = "M" Then 'Si el diagnostico empieza por E
                  If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                  Else
                      Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                  End If
              ElseIf Left(RD!AD39CODIGO, 1) = "E" Then 'Si el diagnostico empieza por E
                Select Case Len(RD!AD39CODIGO)
                Case 4
                  If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                  Else
                      Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                  End If
                Case 6 'Si es un hijo
                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = Key + "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwDiag.Nodes.Add(key3, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwDiag.Nodes.Add(key3, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                End Select
              Else
                Select Case Len(RD!AD39CODIGO)
                Case 3
                    If RD!AD40TIPODIAGPROC = "P" Then
                        Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                    Else
                        Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    End If
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = Key + "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwDiag.Nodes.Add(key3, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwDiag.Nodes.Add(key3, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                Case 6
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = Key + "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    
                    CodigoPadre = Left(RD!AD39CODIGO, 5)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key5 = Key + "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(key3, tvwChild, key5, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwDiag.Nodes.Add(key5, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwDiag.Nodes.Add(key5, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                End Select
              End If
            Else 'procedimiento
                Select Case Len(RD!AD39CODIGO)
                Case 2
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwDiag.Nodes.Add(Key, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                Case 4
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = Key + "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwDiag.Nodes.Add(key2, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwDiag.Nodes.Add(key2, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = Key + "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    
                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key4 = Key + "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwDiag.Nodes.Add(key2, tvwChild, key4, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                        If RD!AD40TIPODIAGPROC = "P" Then
                            Set node = tvwDiag.Nodes.Add(key4, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                        Else
                            Set node = tvwDiag.Nodes.Add(key4, tvwChild, Key + RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                        End If
                End Select
            End If
            RD.MoveNext
            node.EnsureVisible
        Loop
        RD.Close
        qy.Close
      Me.MousePointer = vbDefault
    Exit Sub
label:
    If Err = 35602 Then
        Resume Next
    Else: MsgBox "Error", vbCritical, "Error"
    End If
    Me.MousePointer = vbDefault
End Sub
Private Sub EscribirtvwCrear()
Dim node As node
Dim Key$, Descripcion$, key3$, key5$, key2$, key4$
Dim str$, CodigoPadre$, DesPadre$
Dim RD As rdoResultset
Dim qy As rdoQuery
Dim rdPadre As rdoResultset
Dim qyPadre As rdoQuery
Dim blnDP As Boolean
Dim blnPS As Boolean, bln123 As Boolean
Dim Tipo123%
Dim TipoDP$, TipoPS$, DesCod$, Codigo$
On Error GoTo label
If txtCodigo(1) <> "" Then
        Set node = tvwCrear.SelectedItem
        Set tvwCrear.ImageList = iml1
        Key = "A" + lstvAsistencia.SelectedItem.SubItems(6) + "P" + lstvAsistencia.SelectedItem.SubItems(7)
        Descripcion = Format(lstvAsistencia.SelectedItem.Text, "DD/MM/YYYY") & _
        " " + Format(lstvAsistencia.SelectedItem.SubItems(1), "DD/MM/YYYY") & _
        " " + lstvAsistencia.SelectedItem.SubItems(2) + " " + lstvAsistencia.SelectedItem.SubItems(3) & _
        " " + lstvAsistencia.SelectedItem.SubItems(4) + " " + lstvAsistencia.SelectedItem.SubItems(5)
        Set node = tvwCrear.Nodes.Add(, , Key, Descripcion, 3)

        str = "SELECT AD3900.AD39CODIGO, AD3900.AD39DESCODIGO, " & _
        " AD3900.AD39TIPOCODIGO FROM AD3900 " & _
        "WHERE AD39CODIGO=? "
        Set qy = objApp.rdoConnect.CreateQuery("", str)
            qy(0) = txtCodigo(1)
        Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            If RD!AD39TIPOCODIGO = "D" Then
              If Left(RD!AD39CODIGO, 1) = "M" Then 'Si el diagnostico empieza por E
                Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                Set tvwCrear.SelectedItem = node
              ElseIf Left(RD!AD39CODIGO, 1) = "E" Then 'Si el diagnostico empieza por E
                Select Case Len(RD!AD39CODIGO)
                Case 4
                  Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                  Set tvwCrear.SelectedItem = node
                Case 6 'Si es un hijo
                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    Set node = tvwCrear.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                    Set tvwCrear.SelectedItem = node
                End Select
              Else
                Select Case Len(RD!AD39CODIGO)
                Case 3
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                    Set tvwCrear.SelectedItem = node
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    Set node = tvwCrear.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    Set tvwCrear.SelectedItem = node
                Case 6
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)

                    CodigoPadre = Left(RD!AD39CODIGO, 5)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key5 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(key3, tvwChild, key5, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    Set node = tvwCrear.Nodes.Add(key5, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    Set tvwCrear.SelectedItem = node
                End Select
              End If
            Else 'procedimiento
                Select Case Len(RD!AD39CODIGO)
                Case 2
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    Set tvwCrear.SelectedItem = node
                Case 4
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    Set node = tvwCrear.Nodes.Add(key2, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO, 4)
                    Set tvwCrear.SelectedItem = node
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)

                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key4 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(key2, tvwChild, key4, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    Set node = tvwCrear.Nodes.Add(key4, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    Set tvwCrear.SelectedItem = node
                End Select
            End If
            node.EnsureVisible
        RD.Close
        qy.Close
End If
    Exit Sub
label:
    If Err = 35602 Then
        Resume Next
    ElseIf Err = 40009 Then
      MsgBox "No existen datos para este c�digo", vbCritical, ""
    Else: MsgBox "Error", vbCritical, "Error"
    End If
End Sub
Private Function EsTipo123(node As node) As Integer
Dim str$
Dim qy As rdoQuery
Dim RD As rdoResultset

'txtCodigo(1) = Mid(node.Key, 2, Len(node.Key) - 1)

  str = "SELECT COUNT(*) " & _
  "FROM AD0100, AD4000 " & _
  "Where AD0100.AD01CODASISTENCI = AD4000.AD01CODASISTENCI " & _
  "AND AD4000.AD39CODIGO=? " & _
  "AND AD0100.CI22NUMHISTORIA = ?"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
      qy(0) = Mid(node.Key, 2, Len(node.Key) - 1)
      qy(1) = txtHistoria
  Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

  If RD.rdoColumns(0) = 0 Then
    optN1 = True
    EsTipo123 = 1
  Else
    If Left(node.Key, 1) = "D" Then
      optN2 = True
      EsTipo123 = 2
    Else
      optN3 = True
      EsTipo123 = 3
    End If
  End If
End Function

Private Sub EscribirDaignosticosCrear()
Dim node As node
Dim Key$, Descripcion$, key3$, key5$, key2$, key4$
Dim str$, CodigoPadre$, DesPadre$
Dim RD As rdoResultset
Dim qy As rdoQuery
Dim rdPadre As rdoResultset
Dim qyPadre As rdoQuery
Dim blnDP As Boolean
Dim blnPS As Boolean, bln123 As Boolean
Dim Tipo123%
Dim TipoDP$, TipoPS$, DesCod$
On Error GoTo label
        Set node = tvwCrear.SelectedItem
        Set tvwCrear.ImageList = iml1
        Key = "A" + lstvAsistencia.SelectedItem.SubItems(6) + "P" + lstvAsistencia.SelectedItem.SubItems(7)
        Descripcion = Format(lstvAsistencia.SelectedItem.Text, "DD/MM/YYYY") & _
        " " + Format(lstvAsistencia.SelectedItem.SubItems(1), "DD/MM/YYYY") & _
        " " + lstvAsistencia.SelectedItem.SubItems(2) + " " + lstvAsistencia.SelectedItem.SubItems(3) & _
        " " + lstvAsistencia.SelectedItem.SubItems(4) + " " + lstvAsistencia.SelectedItem.SubItems(5)
        Set node = tvwCrear.Nodes.Add(, , Key, Descripcion, 3)

        str = "SELECT AD3900.AD39CODIGO, AD3900.AD39DESCODIGO, AD4000.AD40TIPODIAGPROC, " & _
        "AD4000.AD40ORDEN, AD3900.AD39TIPOCODIGO, AD4000.AD40TIPOCOD FROM AD4000, AD3900 " & _
        "WHERE AD3900.AD39CODIGO=AD4000.AD39CODIGO AND " & _
        "AD4000.AD01CODASISTENCI=? AND AD4000.AD07CODPROCESO=? ORDER BY AD40ORDEN"

        Set qy = objApp.rdoConnect.CreateQuery("", str)
            qy(0) = lstvAsistencia.SelectedItem.SubItems(6)
            qy(1) = lstvAsistencia.SelectedItem.SubItems(7)
       Set RD = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
       Do While Not RD.EOF
            If RD!AD39TIPOCODIGO = "D" Then
              If Left(RD!AD39CODIGO, 1) = "M" Then 'Si el diagnostico empieza por M
                If RD!AD40TIPODIAGPROC = "P" Then
                  Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGNOSTICO)
                Else
                  Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                End If
              ElseIf Left(RD!AD39CODIGO, 1) = "E" Then 'Si el diagnostico empieza por E
                Select Case Len(RD!AD39CODIGO)
                Case 4
                  If RD!AD40TIPODIAGPROC = "P" Then
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGNOSTICO)
                  Else
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                  End If
                Case 6 'Si es un hijo
                  CodigoPadre = Left(RD!AD39CODIGO, 4)
                  str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                  Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                      qyPadre(0) = CodigoPadre
                  Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                  DesPadre = rdPadre!AD39DESCODIGO
                  key3 = "D" & CodigoPadre
                  rdPadre.Close
                  qyPadre.Close
                  Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                      If RD!AD40TIPODIAGPROC = "P" Then
                          Set node = tvwCrear.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 1)
                      Else
                          Set node = tvwCrear.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                      End If
                End Select
              Else
                Select Case Len(RD!AD39CODIGO)
                Case 3
                    If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGNOSTICO)
                    Else
                      Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                    End If
                    Set tvwCrear.SelectedItem = node
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwCrear.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGNOSTICO)
                    Else
                      Set node = tvwCrear.Nodes.Add(key3, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                    End If
                    Set tvwCrear.SelectedItem = node
                Case 6
                    CodigoPadre = Left(RD!AD39CODIGO, 3)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key3 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key3, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)

                    CodigoPadre = Left(RD!AD39CODIGO, 5)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key5 = "D" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(key3, tvwChild, key5, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwCrear.Nodes.Add(key5, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGNOSTICO)
                    Else
                      Set node = tvwCrear.Nodes.Add(key5, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                    End If
                    Set tvwCrear.SelectedItem = node
                End Select
              End If
            Else 'procedimiento
                Select Case Len(RD!AD39CODIGO)
                Case 2
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, 4)
                    Set tvwCrear.SelectedItem = node
                Case 4
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwCrear.Nodes.Add(key2, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO, constDIAGNOSTICO)
                    Else
                      Set node = tvwCrear.Nodes.Add(key2, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO, constDIAGSECUNDARIO)
                    End If
                    Set tvwCrear.SelectedItem = node
                Case 5
                    CodigoPadre = Left(RD!AD39CODIGO, 2)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key2 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(Key, tvwChild, key2, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)

                    CodigoPadre = Left(RD!AD39CODIGO, 4)
                    str = "SELECT AD39DESCODIGO FROM AD3900 WHERE AD39CODIGO=?"
                    Set qyPadre = objApp.rdoConnect.CreateQuery("", str)
                        qyPadre(0) = CodigoPadre
                    Set rdPadre = qyPadre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                    DesPadre = rdPadre!AD39DESCODIGO
                    key4 = "P" & CodigoPadre
                    rdPadre.Close
                    qyPadre.Close
                    Set node = tvwCrear.Nodes.Add(key2, tvwChild, key4, RD!AD39TIPOCODIGO + CodigoPadre + " " + DesPadre)
                    If RD!AD40TIPODIAGPROC = "P" Then
                      Set node = tvwCrear.Nodes.Add(key4, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGNOSTICO)
                    Else
                      Set node = tvwCrear.Nodes.Add(key4, tvwChild, RD!AD39TIPOCODIGO + RD!AD39CODIGO, RD!AD39TIPOCODIGO + RD!AD39CODIGO + " " + RD!AD39DESCODIGO + " " + DesCod, constDIAGSECUNDARIO)
                    End If
                    Set tvwCrear.SelectedItem = node
                End Select
            End If
            node.EnsureVisible
            RD.MoveNext
          Loop
        RD.Close
        qy.Close
    Exit Sub
label:
    If Err = 35602 Then
        Resume Next
    Else: MsgBox "Error", vbCritical, "Error"
    End If

End Sub

