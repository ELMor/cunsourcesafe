VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form frmContrPresuFarm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Seguimiento del Gasto."
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   5520
      TabIndex        =   16
      Top             =   480
      Width           =   6255
      Begin VB.Label Label4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   840
         TabIndex        =   20
         Top             =   420
         Width           =   5055
      End
      Begin VB.Label Label3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   840
         TabIndex        =   19
         Top             =   180
         Width           =   5055
      End
      Begin VB.Label Label1 
         BackColor       =   &H0000FF00&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   105
         Left            =   240
         TabIndex        =   18
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label2 
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   105
         Left            =   240
         TabIndex        =   17
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Frame Frame2 
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   2655
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         DataField       =   "frh5codperiodicidad"
         Height          =   330
         Index           =   0
         Left            =   1560
         TabIndex        =   11
         Tag             =   "Cod. Periodicidad"
         Top             =   120
         Width           =   1005
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0050.frx":0000
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0050.frx":001C
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "Ejercicio"
         Columns(0).Name =   "Ejercicio"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1773
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Ejercicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   12
         Top             =   150
         Width           =   855
      End
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   1095
      Left            =   4920
      TabIndex        =   9
      Top             =   6840
      Width           =   6735
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   12
      AllowUpdate     =   0   'False
      RowHeight       =   423
      Columns.Count   =   12
      Columns(0).Width=   926
      Columns(0).Caption=   "1"
      Columns(0).Name =   "1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   926
      Columns(1).Caption=   "2"
      Columns(1).Name =   "2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   926
      Columns(2).Caption=   "3"
      Columns(2).Name =   "3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   926
      Columns(3).Caption=   "4"
      Columns(3).Name =   "4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   926
      Columns(4).Caption=   "5"
      Columns(4).Name =   "5"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   926
      Columns(5).Caption=   "6"
      Columns(5).Name =   "6"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   926
      Columns(6).Caption=   "7"
      Columns(6).Name =   "7"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   926
      Columns(7).Caption=   "8"
      Columns(7).Name =   "8"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   926
      Columns(8).Caption=   "9"
      Columns(8).Name =   "9"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   926
      Columns(9).Caption=   "10"
      Columns(9).Name =   "10"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   926
      Columns(10).Caption=   "11"
      Columns(10).Name=   "11"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   926
      Columns(11).Caption=   "12"
      Columns(11).Name=   "12"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   11880
      _ExtentY        =   1931
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Left            =   2880
      TabIndex        =   6
      Top             =   480
      Width           =   2535
      Begin VB.OptionButton optPtas 
         Caption         =   "Euros"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   1
         Left            =   1320
         TabIndex        =   8
         Top             =   200
         Width           =   855
      End
      Begin VB.OptionButton optPtas 
         Caption         =   "Ptas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ejercicios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6720
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6075
         Index           =   0
         Left            =   360
         TabIndex        =   4
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   10716
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Gr�fica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5280
      Index           =   0
      Left            =   4920
      TabIndex        =   2
      Top             =   1440
      Width           =   6780
      Begin MSChartLib.MSChart MSChart1 
         Height          =   4935
         Left            =   240
         OleObjectBlob   =   "FR0050.frx":0038
         TabIndex        =   5
         Top             =   240
         Width           =   6375
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label Label8 
      BackColor       =   &H000000FF&
      Height          =   105
      Left            =   0
      TabIndex        =   15
      Top             =   75
      Width           =   375
   End
   Begin VB.Label Label7 
      BackColor       =   &H0000FF00&
      Height          =   105
      Left            =   0
      TabIndex        =   14
      Top             =   315
      Width           =   375
   End
   Begin VB.Label Label6 
      Height          =   255
      Left            =   480
      TabIndex        =   13
      Top             =   0
      Width           =   4695
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":24D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2534
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2592
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":25F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":264E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":26AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":270A
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2768
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":27C6
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2824
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2882
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":28E0
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":293E
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":299C
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":29FA
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2A58
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2AB6
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2B14
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2B72
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmContrPresuFarm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmContrPresuFarm (FR0050.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Controlar Presupuestos                                  *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
Dim gblnabrir As Boolean
Dim gintMoneda As Integer
Dim gintMarca As Integer
Dim gGrafico1(1 To 2, 1 To 12)
Dim gGrafico2(1 To 2, 1 To 12)
Dim gPrimeraVez As Boolean

Dim mvarEuro As Variant
Dim mintMes As Integer
Dim mstrFechas As String

'Private Sub rellenar_TreeView_GrupServ()
'    Dim a�o As Variant
'    Dim strejercicio As String
'    Dim rstejercicio As rdoResultset
'    'Dim strMes As String
'    'Dim rstMes As rdoResultset
'    'Dim mes As Variant
'    Dim strgrupo As String
'    Dim rstgrupo As rdoResultset
'    Dim strdesGrupo As String
'    Dim rstdesGrupo As rdoResultset
'    Dim grupo As Variant
'    Dim desGrupo As Variant
'    Dim strServicio As String
'    Dim rstServicio As rdoResultset
'    Dim desServicio As Variant
'    Dim strdesServicio As String
'    Dim rstdesServicio As rdoResultset
'    Dim Servicio As Variant
'    Dim gNode As Node
'    Dim Gvacio As Boolean
'
'    Dim strTipGrupo As String
'    Dim rstTipGrupo As rdoResultset
'    Dim TipGrupo As Integer
'    Dim strdesTipGrupo As String
'    Dim rstdesTipGrupo As rdoResultset
'    Dim desTipGrupo As Variant
'
'    tvwItems(0).Nodes.Clear
'    Gvacio = True
'
'    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
'    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
'
'
'    While rstejercicio.EOF = False
'      a�o = rstejercicio("frg9ejercicio").Value
'      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
'      strTipGrupo = "SELECT DISTINCT FRI7CODTIPGRP FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRI7CODTIPGRP"
'      Set rstTipGrupo = objApp.rdoConnect.OpenResultset(strTipGrupo)
'      While rstTipGrupo.EOF = False
'        TipGrupo = rstTipGrupo("FRI7CODTIPGRP").Value
'        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
'        strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                     " AND FRI7CODTIPGRP=" & TipGrupo & " ORDER BY FR41CODGRUPPROD"
'        Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
'        strdesTipGrupo = "SELECT FRI7DESTIPGRP FROM FRI700 WHERE FRI7CODTIPGRP=" & rstTipGrupo("FRI7CODTIPGRP").Value
'        Set rstdesTipGrupo = objApp.rdoConnect.OpenResultset(strdesTipGrupo)
'        desTipGrupo = rstdesTipGrupo("FRI7DESTIPGRP").Value
'        Set gNode = tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "tipgr" & a�o & "/" & TipGrupo, TipGrupo & ".-" & desTipGrupo)
'        While rstgrupo.EOF = False
'            grupo = rstgrupo("FR41CODGRUPPROD").Value
'            strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                          " AND FR41CODGRUPPROD=" & grupo & " AND FRI7CODTIPGRP=" & TipGrupo & " ORDER BY AD02CODDPTO"
'            Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
'            strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
'            Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
'            desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
'            Set gNode = tvwItems(0).Nodes.Add("tipgr" & a�o & "/" & TipGrupo, tvwChild, "grupo" & a�o & "/" & TipGrupo & "/" & grupo, grupo & ".-" & desGrupo)
'            Gvacio = False
'            While rstServicio.EOF = False
'                Servicio = rstServicio("AD02CODDPTO").Value
'                strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
'                Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
'                desServicio = rstdesServicio("AD02DESDPTO").Value
'                Call tvwItems(0).Nodes.Add("grupo" & a�o & "/" & TipGrupo & "/" & grupo, tvwChild, "servicio" & a�o & "/" & TipGrupo & "/" & grupo & "/" & Servicio, Servicio & ".-" & desServicio)
'                rstdesServicio.Close
'                Set rstdesServicio = Nothing
'                rstServicio.MoveNext
'            Wend
'            rstServicio.Close
'            Set rstServicio = Nothing
'            rstdesGrupo.Close
'            Set rstdesGrupo = Nothing
'            rstgrupo.MoveNext
'        Wend
'        rstgrupo.Close
'        Set rstgrupo = Nothing
'        rstTipGrupo.MoveNext
'      Wend
'      rstTipGrupo.Close
'      Set rstTipGrupo = Nothing
'      rstejercicio.MoveNext
'    Wend
'    rstejercicio.Close
'    Set rstejercicio = Nothing
'    If Gvacio = False Then
'        Call tvwItems_NodeClick(0, gNode)
'    End If
'
'End Sub

'Private Sub rellenar_TreeView_ServGrup()
'    Dim a�o As Variant
'    Dim strejercicio As String
'    Dim rstejercicio As rdoResultset
'    'Dim strMes As String
'    'Dim rstMes As rdoResultset
'    'Dim mes As Variant
'    Dim strgrupo As String
'    Dim rstgrupo As rdoResultset
'    Dim strdesGrupo As String
'    Dim rstdesGrupo As rdoResultset
'    Dim grupo As Variant
'    Dim desGrupo As Variant
'    Dim strServicio As String
'    Dim rstServicio As rdoResultset
'    Dim desServicio As Variant
'    Dim strdesServicio As String
'    Dim rstdesServicio As rdoResultset
'    Dim Servicio As Variant
'
'    Dim strTipGrupo As String
'    Dim rstTipGrupo As rdoResultset
'    Dim TipGrupo As Integer
'    Dim strdesTipGrupo As String
'    Dim rstdesTipGrupo As rdoResultset
'    Dim desTipGrupo As Variant
'
'    tvwItems(0).Nodes.Clear
'
'    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
'    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
'
'
'    While rstejercicio.EOF = False
'      a�o = rstejercicio("frg9ejercicio").Value
'      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
'      'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
'      'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
'      'While rstMes.EOF = False
'        'mes = rstMes("FRH1MES").Value
'        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
'        strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                     " ORDER BY AD02CODDPTO"
'        Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
'        While rstServicio.EOF = False
'            Servicio = rstServicio("AD02CODDPTO").Value
'            strTipGrupo = "SELECT DISTINCT FRI7CODTIPGRP FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                            " AND AD02CODDPTO=" & Servicio & " ORDER BY FRI7CODTIPGRP"
'            Set rstTipGrupo = objApp.rdoConnect.OpenResultset(strTipGrupo)
'            strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
'            Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
'            desServicio = rstdesServicio("AD02DESDPTO").Value
'            Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "servicio" & a�o & "/" & Servicio, Servicio & ".-" & desServicio)
'            While rstTipGrupo.EOF = False
'                TipGrupo = rstTipGrupo("FRI7CODTIPGRP").Value
'                strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                              " AND AD02CODDPTO=" & Servicio & " AND FRI7CODTIPGRP=" & TipGrupo & " ORDER BY FR41CODGRUPPROD"
'                Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
'                strdesTipGrupo = "SELECT FRI7DESTIPGRP FROM FRI700 WHERE FRI7CODTIPGRP=" & rstTipGrupo("FRI7CODTIPGRP").Value
'                Set rstdesTipGrupo = objApp.rdoConnect.OpenResultset(strdesTipGrupo)
'                desGrupo = rstdesTipGrupo("FRI7DESTIPGRP").Value
'                Call tvwItems(0).Nodes.Add("servicio" & a�o & "/" & Servicio, tvwChild, "tipgr" & a�o & "/" & Servicio & "/" & TipGrupo, TipGrupo & ".-" & desTipGrupo)
'
'            'strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'            '              " AND AD02CODDPTO=" & Servicio & "ORDER BY FR41CODGRUPPROD"
'            'Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
'            'strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
'            'Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
'            'desServicio = rstdesServicio("AD02DESDPTO").Value
'            'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "servicio" & a�o & "/" & Servicio, Servicio & ".-" & desServicio)
'                While rstgrupo.EOF = False
'                    grupo = rstgrupo("FR41CODGRUPPROD").Value
'                    strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
'                    Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
'                    desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
'                    Call tvwItems(0).Nodes.Add("tipgr" & a�o & "/" & Servicio & "/" & TipGrupo, tvwChild, "grupo" & a�o & "/" & Servicio & "/" & TipGrupo & "/" & grupo, grupo & ".-" & desGrupo)
'                    rstdesGrupo.Close
'                    Set rstdesGrupo = Nothing
'                    rstgrupo.MoveNext
'                Wend
'                rstgrupo.Close
'                Set rstgrupo = Nothing
'                rstdesTipGrupo.Close
'                Set rstdesTipGrupo = Nothing
'                rstTipGrupo.MoveNext
'            Wend
'            rstTipGrupo.Close
'            Set rstTipGrupo = Nothing
'            rstdesServicio.Close
'            Set rstdesServicio = Nothing
'            rstServicio.MoveNext
'        Wend
'        rstServicio.Close
'        Set rstServicio = Nothing
'        'rstMes.MoveNext
'      'Wend
'      'rstMes.Close
'      'Set rstMes = Nothing
'      rstejercicio.MoveNext
'    Wend
'    rstejercicio.Close
'    Set rstejercicio = Nothing
'
'
'
'End Sub

Private Sub rellenar_TreeView()
    Dim a�o As Variant
    Dim strejercicio As String
    Dim rstejercicio As rdoResultset
    'Dim strMes As String
    'Dim rstMes As rdoResultset
    'Dim mes As Variant
    Dim strgrupo As String
    Dim rstgrupo As rdoResultset
    Dim strdesGrupo As String
    Dim rstdesGrupo As rdoResultset
    Dim grupo As Variant
    Dim desGrupo As Variant
    Dim strServicio As String
    Dim rstServicio As rdoResultset
    Dim desServicio As Variant
    Dim strdesServicio As String
    Dim rstdesServicio As rdoResultset
    Dim Servicio As Variant
    
    Dim strTipGrupo As String
    Dim rstTipGrupo As rdoResultset
    Dim TipGrupo As Integer
    Dim strdesTipGrupo As String
    Dim rstdesTipGrupo As rdoResultset
    Dim desTipGrupo As Variant
    
    frmContrPresuFarm.MousePointer = 11
    tvwItems(0).Nodes.Clear
    
    'strejercicio = "select * from frG900 order by frg9ejercicio desc"
    'Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
    
    
    'While rstejercicio.EOF = False
    If cboDBCombo1(0).Text <> "" Then
      a�o = cboDBCombo1(0).Text
      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
      'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
      'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
      'While rstMes.EOF = False
        'mes = rstMes("FRH1MES").Value
        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
        strServicio = "SELECT * FROM AD0200" & _
                      " where (AD02FECINICIO < (SELECT SYSDATE FROM DUAL)) OR" & _
                      " (NOT(AD02FECINICIO IS NULL))"
                       ' " ' WHERE FRG9EJERCICIO=" & a�o & _
                     " ORDER BY AD02CODDPTO"
        Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
        While rstServicio.EOF = False
            Servicio = rstServicio("AD02CODDPTO").Value
            strTipGrupo = "SELECT * FROM FRI700 ORDER BY FRI7CODTIPGRP"
            Set rstTipGrupo = objApp.rdoConnect.OpenResultset(strTipGrupo)
            'strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
            'Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
            desServicio = rstServicio("AD02DESDPTO").Value
            Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "servicio" & a�o & "/" & Servicio, Servicio & ".-" & desServicio)
            While rstTipGrupo.EOF = False
                TipGrupo = rstTipGrupo("FRI7CODTIPGRP").Value
                strgrupo = "SELECT * FROM FR4100 WHERE  FRI7CODTIPGRP=" & TipGrupo & " ORDER BY FR41CODGRUPPROD"
                Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
                strdesTipGrupo = "SELECT FRI7DESTIPGRP FROM FRI700 WHERE FRI7CODTIPGRP=" & rstTipGrupo("FRI7CODTIPGRP").Value
                Set rstdesTipGrupo = objApp.rdoConnect.OpenResultset(strdesTipGrupo)
                desTipGrupo = rstdesTipGrupo("FRI7DESTIPGRP").Value
                Call tvwItems(0).Nodes.Add("servicio" & a�o & "/" & Servicio, tvwChild, "tipgr" & a�o & "/" & Servicio & "/" & TipGrupo, TipGrupo & ".-" & desTipGrupo)
                
            'strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
            '              " AND AD02CODDPTO=" & Servicio & "ORDER BY FR41CODGRUPPROD"
            'Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
            'strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
            'Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
            'desServicio = rstdesServicio("AD02DESDPTO").Value
            'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "servicio" & a�o & "/" & Servicio, Servicio & ".-" & desServicio)
                While rstgrupo.EOF = False
                    grupo = rstgrupo("FR41CODGRUPPROD").Value
                    'strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
                    'Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
                    desGrupo = rstgrupo("FR41DESGRUPPROD").Value
                    Call tvwItems(0).Nodes.Add("tipgr" & a�o & "/" & Servicio & "/" & TipGrupo, tvwChild, "grupo" & a�o & "/" & Servicio & "/" & TipGrupo & "/" & grupo, grupo & ".-" & desGrupo)
                    'rstdesGrupo.Close
                    'Set rstdesGrupo = Nothing
                    rstgrupo.MoveNext
                Wend
                rstgrupo.Close
                Set rstgrupo = Nothing
                rstdesTipGrupo.Close
                Set rstdesTipGrupo = Nothing
                rstTipGrupo.MoveNext
            Wend
            rstTipGrupo.Close
            Set rstTipGrupo = Nothing
            'rstdesServicio.Close
            'Set rstdesServicio = Nothing
            rstServicio.MoveNext
        Wend
        rstServicio.Close
        Set rstServicio = Nothing
        'rstMes.MoveNext
      'Wend
      'rstMes.Close
      'Set rstMes = Nothing
   '   rstejercicio.MoveNext
   ' Wend
   ' rstejercicio.Close
   ' Set rstejercicio = Nothing
   End If
    frmContrPresuFarm.MousePointer = 0
End Sub

Private Sub cboDBCombo1_CloseUp(Index As Integer)
    Call rellenar_TreeView
    mstrFechas = "and fr80fecmovimiento >= to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) & "','dd/mm/yyyy')" & _
                " and fr80fecmovimiento < to_date('1/" & mintMes - 1 & "/" & Val(cboDBCombo1(0).Text) + 1 & "','dd/mm/yyyy')"

End Sub

Private Sub Form_Activate()
    
    Dim rowLabelCount As Integer
    Dim columnLabelCount As Integer
    Dim rowCount As Integer
    Dim columnCount As Integer
    Dim datagrid As datagrid
    Dim labelindex As Integer
    Dim column As Integer
    Dim row As Integer
    Dim i As Integer
    
    Dim strSelect As String
    Dim rstSelect As rdoResultset
    
    Set datagrid = MSChart1.datagrid
    MSChart1.ShowLegend = True
    With MSChart1.datagrid
    ' Establece los par�metros del gr�fico con
    ' los m�todos.
    
        strSelect = "select * from frg900 order by FRG9EJERCICIO"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    While rstSelect.EOF = False
        cboDBCombo1(0).AddItem rstSelect("FRG9EJERCICIO").Value
        rstSelect.MoveNext
    Wend
    strSelect = "select frh2paramgen from frh200 where frh2codparamgen=1"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    mvarEuro = objGen.ReplaceStr(rstSelect(0).Value, ",", ".", 1)
    strSelect = "select frh2paramgen from frh200 where frh2codparamgen=13"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    mintMes = rstSelect(0).Value

    
    'SSDBGrid1.AddNew
    'SSDBGrid1.AddNew
    'SSDBGrid1.Update
        SSDBGrid1.RowHeight = 275
        SSDBGrid1.AddNew
        For i = 0 To 11
            SSDBGrid1.Columns(i).Value = 0
        Next i
        SSDBGrid1.Update
        SSDBGrid1.AddNew
        For i = 0 To 11
            SSDBGrid1.Columns(i).Value = 0
        Next i
        SSDBGrid1.Update
        rowLabelCount = 12
        columnLabelCount = 2
        rowCount = 12
        columnCount = 2
        .SetSize rowLabelCount, columnLabelCount, rowCount, columnCount
        labelindex = 1
        labelindex = 1
        row = 1
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Ene"
        .RowLabel(row, labelindex) = "Enero"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 2
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Feb"
        .RowLabel(row, labelindex) = "Febrero"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 3
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Mar"
        .RowLabel(row, labelindex) = "Marzo"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 4
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Abr"
        .RowLabel(row, labelindex) = "Abril"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 5
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "May"
        .RowLabel(row, labelindex) = "Mayo"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 6
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Jun"
        .RowLabel(row, labelindex) = "Junio"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 7
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Jul"
        .RowLabel(row, labelindex) = "Julio"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 8
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Ago"
        .RowLabel(row, labelindex) = "Agosto"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 9
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Sep"
        .RowLabel(row, labelindex) = "Septiembre"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 10
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Oct"
        .RowLabel(row, labelindex) = "Octubre"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 11
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Nov"
        .RowLabel(row, labelindex) = "Noviembre"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
        row = 12
        row = ((12 - mintMes + row) Mod 12) + 1
        SSDBGrid1.Columns(row - 1).Caption = "Dic"
        .RowLabel(row, labelindex) = "Diciembre"
        Call .SetData(row, 1, 0, 0)
        Call .SetData(row, 2, 0, 0)
    End With
    
    gPrimeraVez = False
    
    Call controlar_menues
    Call Label3.ZOrder(0)
    Call Label4.ZOrder(0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  
  gintMarca = 1
  tvwItems(0).Visible = True
  gPrimeraVez = True
  'optGrupos(1).Value = True
  optPtas(0).Value = True
  'opt2d(0).Value = True
  
End Sub

Private Sub MSChart1_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
    gintMarca = Series
End Sub

'Private Sub opt2d_Click(Index As Integer)
'    If Index = 0 Then
'        MSChart1.chartType = VtChChartType2dLine
'    Else
'        MSChart1.chartType = VtChChartType3dLine
'    End If
'End Sub

Private Sub optGrupos_Click(Index As Integer)
    'If Index = 1 Then
     '   Call rellenar_TreeView_GrupServ
    'Else
    '    Call rellenar_TreeView_ServGrup
    'End If
End Sub

Private Sub optPtas_Click(Index As Integer)
Dim i As Integer

    If Index = 0 Then
        gintMoneda = 0
    Else
        gintMoneda = 1
    End If
    If gPrimeraVez = False Then
        For i = 1 To 12
            Call MSChart1.datagrid.SetData(i, 1, gGrafico1(gintMoneda + 1, i), 0)
            Call MSChart1.datagrid.SetData(i, 2, gGrafico2(gintMoneda + 1, i), 0)
        Next i
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
Dim rsta As rdoResultset
Dim sqlstr As String
Dim strSelect As String
Dim rstSelect As rdoResultset
Dim vntclave As Variant
Dim strupdate As String
Dim vntPtas As Variant
Dim vntEuro As Variant
  
  Select Case btnButton.Index
  Case 11
      MSChart1.EditCopy
  Case 30 'salir
    Unload Me
  Case 21 'Primero
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case 22 'Anterior
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 23 'Siguiente
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 24 'Ultimo
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case Else 'Otro boton
    Exit Sub
  End Select
  

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub


Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub


Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)


  Node.EnsureVisible
  Node.Selected = True
  Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim ejercicio As Variant
Dim mes As Variant
Dim grupo As Variant
Dim Servicio As Variant
Dim primeraparte As Variant
Dim segundaparte As Variant
Dim terceraparte As Variant
Dim resto As Variant
Dim strWhere As String
Dim rstwhere As rdoResultset
Dim intmes As Integer
Dim i As Integer
Dim TipGrupo As Variant
    
    'Select Case optGrupos(1).Value
    '    Case True:
        
            Select Case Left(Node.Key, 3)
                Case "eje":
                  ejercicio = Right(Node.Key, Len(Node.Key) - 5)
                  strWhere = "select sum(fr8000.fr80unisalen * fr7300.fr73pvl),sum(fr8000.fr80unisalen * fr7300.fr73pvl) / " & mvarEuro & _
                               ",to_char(fr8000.fr80fecmovimiento,'mm') from fr8000,fr7300" & _
                               " where fr8000.fr73codproducto=fr7300.fr73codproducto " & _
                               mstrFechas & _
                               " group by to_char(fr8000.fr80fecmovimiento,'mm')"
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                            "from frh100 where frg9ejercicio=" & ejercicio & _
                            " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  If gintMarca = 1 Then
                    Label3.Caption = Node.Text
                  Else
                    Label4.Caption = Node.Text
                  End If
                  For i = 1 To 12
                    If gintMarca = 1 Then
                        gGrafico1(1, i) = 0
                        gGrafico1(2, i) = 0
                    Else
                        gGrafico2(1, i) = 0
                        gGrafico2(2, i) = 0
                    End If
                    SSDBGrid1.MoveFirst
                    SSDBGrid1.Columns(i - 1).Value = 0
                    'SSDBGrid1.Update
                    SSDBGrid1.MoveLast
                    SSDBGrid1.Columns(i - 1).Value = 0
                    SSDBGrid1.Update
                  Next i
                  While rstwhere.EOF = False
                    intmes = rstwhere(2).Value
                    intmes = ((12 - mintMes + intmes) Mod 12) + 1
                    If gintMarca = 1 Then
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico1(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico1(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    Else
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico2(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico2(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    End If
                    'intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                Case "tip":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 5)
                  Servicio = Left(segundaparte, Len(segundaparte) - 1)
                  TipGrupo = resto
                  
                  
                  strWhere = "select sum(fr8000.fr80unisalen * fr7300.fr73pvl),sum(fr8000.fr80unisalen * fr7300.fr73pvl) / " & mvarEuro & _
                               ",to_char(fr8000.fr80fecmovimiento,'mm') from fr8000,fr7300" & _
                               " where fr8000.fr73codproducto=fr7300.fr73codproducto " & _
                               " and fr8000.fr73codproducto in (select fr73codproducto from fr0900,fr4100" & _
                               " where fr0900.fr41codgrupprod=fr4100.fr41codgrupprod" & _
                               " and fr4100.fri7codtipgrp=" & TipGrupo & ") " & _
                               " and fr8000.fr04codalmacen_des in (select fr04codalmacen from fr0400" & _
                               " where ad02coddpto=" & Servicio & ")" & _
                               mstrFechas & _
                               " group by to_char(fr8000.fr80fecmovimiento,'mm')"
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                            "from frh100 where frg9ejercicio=" & ejercicio & _
                            " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  If gintMarca = 1 Then
                    Label3.Caption = Node.Text
                  Else
                    Label4.Caption = Node.Text
                  End If
                  For i = 1 To 12
                    If gintMarca = 1 Then
                        gGrafico1(1, i) = 0
                        gGrafico1(2, i) = 0
                    Else
                        gGrafico2(1, i) = 0
                        gGrafico2(2, i) = 0
                    End If
                    SSDBGrid1.MoveFirst
                    SSDBGrid1.Columns(i - 1).Value = 0
                    'SSDBGrid1.Update
                    SSDBGrid1.MoveLast
                    SSDBGrid1.Columns(i - 1).Value = 0
                    SSDBGrid1.Update
                  Next i
                  While rstwhere.EOF = False
                    intmes = rstwhere(2).Value
                    intmes = ((12 - mintMes + intmes) Mod 12) + 1
                    If gintMarca = 1 Then
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico1(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico1(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    Else
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico2(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico2(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    End If
                    'intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                  
                  
                  
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                  '           "frg9ejercicio=" & ejercicio & _
                  '           " and FRI7CODTIPGRP=" & TipGrupo & _
                  '           " group by frh1mes"
                  'Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  'intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  'While rstwhere.EOF = False
                  '  If gintMarca = 1 Then
                  '      gGrafico1(1, intmes + 1) = rstwhere(0).Value
                  '      gGrafico1(2, intmes + 1) = rstwhere(1).Value
                  '  Else
                  '      gGrafico2(1, intmes + 1) = rstwhere(0).Value
                  '      gGrafico2(2, intmes + 1) = rstwhere(1).Value
                  '  End If
                  '  intmes = intmes + 1
                  '  rstwhere.MoveNext
                  'Wend
                Case "gru":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  terceraparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
                  Servicio = Left(segundaparte, Len(segundaparte) - 1)
                  TipGrupo = Left(terceraparte, Len(terceraparte) - 1)
                  grupo = resto
                  
                  
                  strWhere = "select sum(fr8000.fr80unisalen * fr7300.fr73pvl),sum(fr8000.fr80unisalen * fr7300.fr73pvl) / " & mvarEuro & _
                               ",to_char(fr8000.fr80fecmovimiento,'mm') from fr8000,fr7300" & _
                               " where fr8000.fr73codproducto=fr7300.fr73codproducto " & _
                               " and fr8000.fr73codproducto in (select fr73codproducto from fr0900,fr4100" & _
                               " where fr0900.fr41codgrupprod=fr4100.fr41codgrupprod" & _
                               " and fr4100.fri7codtipgrp=" & TipGrupo & _
                               " and fr4100.fr41codgrupprod=" & grupo & " ) " & _
                               " and fr8000.fr04codalmacen_des in (select fr04codalmacen from fr0400" & _
                               " where ad02coddpto=" & Servicio & ")" & _
                               mstrFechas & _
                               " group by to_char(fr8000.fr80fecmovimiento,'mm')"
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                            "from frh100 where frg9ejercicio=" & ejercicio & _
                            " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  If gintMarca = 1 Then
                    Label3.Caption = Node.Text
                  Else
                    Label4.Caption = Node.Text
                  End If
                  For i = 1 To 12
                    If gintMarca = 1 Then
                        gGrafico1(1, i) = 0
                        gGrafico1(2, i) = 0
                    Else
                        gGrafico2(1, i) = 0
                        gGrafico2(2, i) = 0
                    End If
                    SSDBGrid1.MoveFirst
                    SSDBGrid1.Columns(i - 1).Value = 0
                    'SSDBGrid1.Update
                    SSDBGrid1.MoveLast
                    SSDBGrid1.Columns(i - 1).Value = 0
                    SSDBGrid1.Update
                  Next i
                  While rstwhere.EOF = False
                    intmes = rstwhere(2).Value
                    intmes = ((12 - mintMes + intmes) Mod 12) + 1
                    If gintMarca = 1 Then
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico1(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico1(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    Else
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico2(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico2(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    End If
                    'intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                  '           "frg9ejercicio=" & ejercicio & _
                  '           " and FRI7CODTIPGRP=" & TipGrupo & _
                  '           " and fr41codgrupprod=" & grupo & _
                  '           " group by frh1mes"
                  'Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  'intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  'While rstwhere.EOF = False
                  '  If gintMarca = 1 Then
                  '      gGrafico1(1, intmes + 1) = rstwhere(0).Value
                  '      gGrafico1(2, intmes + 1) = rstwhere(1).Value
                  '  Else
                  '      gGrafico2(1, intmes + 1) = rstwhere(0).Value
                  '      gGrafico2(2, intmes + 1) = rstwhere(1).Value
                  '  End If
                  '  intmes = intmes + 1
                  '  rstwhere.MoveNext
                  'Wend
                Case "ser":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
                  Servicio = resto
                  
                  
                  strWhere = "select sum(fr8000.fr80unisalen * fr7300.fr73pvl),sum(fr8000.fr80unisalen * fr7300.fr73pvl) / " & mvarEuro & _
                               ",to_char(fr8000.fr80fecmovimiento,'mm') from fr8000,fr7300" & _
                               " where fr8000.fr73codproducto=fr7300.fr73codproducto " & _
                               " and fr8000.fr04codalmacen_des in (select fr04codalmacen from fr0400" & _
                               " where ad02coddpto=" & Servicio & ")" & _
                               mstrFechas & _
                               " group by to_char(fr8000.fr80fecmovimiento,'mm')"
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                            "from frh100 where frg9ejercicio=" & ejercicio & _
                            " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  If gintMarca = 1 Then
                    Label3.Caption = Node.Text
                  Else
                    Label4.Caption = Node.Text
                  End If
                  For i = 1 To 12
                    If gintMarca = 1 Then
                        gGrafico1(1, i) = 0
                        gGrafico1(2, i) = 0
                    Else
                        gGrafico2(1, i) = 0
                        gGrafico2(2, i) = 0
                    End If
                    SSDBGrid1.MoveFirst
                    SSDBGrid1.Columns(i - 1).Value = 0
                    'SSDBGrid1.Update
                    SSDBGrid1.MoveLast
                    SSDBGrid1.Columns(i - 1).Value = 0
                    SSDBGrid1.Update
                  Next i
                  While rstwhere.EOF = False
                    intmes = rstwhere(2).Value
                    intmes = ((12 - mintMes + intmes) Mod 12) + 1
                    If gintMarca = 1 Then
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico1(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico1(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico1(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    Else
                        SSDBGrid1.MoveFirst
                        If IsNull(rstwhere(0).Value) Then
                            gGrafico2(1, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(1, intmes) = rstwhere(0).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(0).Value
                        End If
                        SSDBGrid1.MoveLast
                        If IsNull(rstwhere(1).Value) Then
                            gGrafico2(2, intmes) = 0
                            SSDBGrid1.Columns(intmes - 1).Value = 0
                        Else
                            gGrafico2(2, intmes) = rstwhere(1).Value
                            SSDBGrid1.Columns(intmes - 1).Value = rstwhere(1).Value
                        End If
                    End If
                    'intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                  
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                  '           "frg9ejercicio=" & ejercicio & _
                  '           " and FRI7CODTIPGRP=" & TipGrupo & _
                  '           " and fr41codgrupprod=" & grupo & _
                  '           " and ad02coddpto=" & Servicio & _
                  '           " group by frh1mes"
                  'Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  'intmes = 0
                  'MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  'While rstwhere.EOF = False
                  '  If gintMarca = 1 Then
                  '      gGrafico1(1, intmes + 1) = rstwhere(0).Value
                  '      gGrafico1(2, intmes + 1) = rstwhere(1).Value
                  '  Else
                  '      gGrafico2(1, intmes + 1) = rstwhere(0).Value
                  '      gGrafico2(2, intmes + 1) = rstwhere(1).Value
                  '  End If
                  '  intmes = intmes + 1
                  '  rstwhere.MoveNext
                  'Wend
            End Select
     '   Case False:
     '       Select Case Left(Node.Key, 3)
     '           Case "eje":
     '             ejercicio = Right(Node.Key, Len(Node.Key) - 5)
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio & _
     '                        " group by frh1mes"
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             intmes = 0
     '             MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
     '             While rstwhere.EOF = False
     '               If gintMarca = 1 Then
     '                   gGrafico1(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico1(2, intmes + 1) = rstwhere(1).Value
     '               Else
     '                   gGrafico2(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico2(2, intmes + 1) = rstwhere(1).Value
     '               End If
     '               intmes = intmes + 1
     '               rstwhere.MoveNext
     '             Wend
     '           Case "ser":
     '             primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
     '             resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
     '             ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
     '             Servicio = resto
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio & _
     '                        " and ad02coddpto=" & Servicio & _
     '                        " group by frh1mes"
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             intmes = 0
     '             MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
     '             While rstwhere.EOF = False
     '               If gintMarca = 1 Then
     '                   gGrafico1(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico1(2, intmes + 1) = rstwhere(1).Value
     '               Else
     '                   gGrafico2(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico2(2, intmes + 1) = rstwhere(1).Value
     '               End If
     '               intmes = intmes + 1
     '               rstwhere.MoveNext
     '             Wend
     '           Case "tip":
     '             primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
     '             resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
     '             segundaparte = Left(resto, InStr(resto, "/"))
     '             resto = Right(resto, Len(resto) - InStr(resto, "/"))
     '             ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 5)
     '             Servicio = Left(segundaparte, Len(segundaparte) - 1)
     '             TipGrupo = resto
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio & _
     '                        " and FRI7CODTIPGRP=" & TipGrupo & _
     '                        " and ad02coddpto=" & Servicio & _
     '                        " group by frh1mes"
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             intmes = 0
     '             MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
     '             While rstwhere.EOF = False
     '               If gintMarca = 1 Then
     '                   gGrafico1(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico1(2, intmes + 1) = rstwhere(1).Value
     '               Else
     '                   gGrafico2(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico2(2, intmes + 1) = rstwhere(1).Value
     '               End If
     '               intmes = intmes + 1
     '               rstwhere.MoveNext
     '             Wend
     '           Case "gru":
     '             primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
     '             resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
     '             segundaparte = Left(resto, InStr(resto, "/"))
     '             resto = Right(resto, Len(resto) - InStr(resto, "/"))
     '             terceraparte = Left(resto, InStr(resto, "/"))
     '             resto = Right(resto, Len(resto) - InStr(resto, "/"))
     '             ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
     '             Servicio = Left(segundaparte, Len(segundaparte) - 1)
     '             TipGrupo = Left(terceraparte, Len(terceraparte) - 1)
     '             grupo = resto
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio & _
     '                        " and fr41codgrupprod=" & grupo & _
     '                        " and ad02coddpto=" & Servicio & _
     '                        " and FRI7CODTIPGRP=" & TipGrupo & _
     '                        " group by frh1mes"
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             intmes = 0
     '             MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
     '             While rstwhere.EOF = False
     '               If gintMarca = 1 Then
     '                   gGrafico1(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico1(2, intmes + 1) = rstwhere(1).Value
     '               Else
     '                   gGrafico2(1, intmes + 1) = rstwhere(0).Value
     '                   gGrafico2(2, intmes + 1) = rstwhere(1).Value
     '               End If
     '               intmes = intmes + 1
     '               rstwhere.MoveNext
     '             Wend
     '           End Select
      '  End Select
    If gPrimeraVez = False Then
      For i = 1 To 12
          Call MSChart1.datagrid.SetData(i, 1, gGrafico1(gintMoneda + 1, i), 0)
          Call MSChart1.datagrid.SetData(i, 2, gGrafico2(gintMoneda + 1, i), 0)
      Next i
    End If
    SSDBGrid1.Update
End Sub


Private Sub controlar_menues()
    
    tlbToolbar1.Buttons(2).Enabled = False
    tlbToolbar1.Buttons(3).Enabled = False
    tlbToolbar1.Buttons(4).Enabled = False
    tlbToolbar1.Buttons(6).Enabled = False
    tlbToolbar1.Buttons(8).Enabled = False
    tlbToolbar1.Buttons(14).Enabled = False
    tlbToolbar1.Buttons(10).Enabled = False
    tlbToolbar1.Buttons(11).Enabled = False
    tlbToolbar1.Buttons(12).Enabled = False
    tlbToolbar1.Buttons(18).Enabled = False
    tlbToolbar1.Buttons(19).Enabled = False
    tlbToolbar1.Buttons(16).Enabled = False
    tlbToolbar1.Buttons(21).Enabled = True
    tlbToolbar1.Buttons(22).Enabled = True
    tlbToolbar1.Buttons(23).Enabled = True
    tlbToolbar1.Buttons(24).Enabled = True
    tlbToolbar1.Buttons(26).Enabled = False
    tlbToolbar1.Buttons(28).Enabled = False
    mnuDatosOpcion(10).Enabled = False
    mnuDatosOpcion(20).Enabled = False
    mnuDatosOpcion(40).Enabled = False
    mnuDatosOpcion(60).Enabled = False
    mnuDatosOpcion(80).Enabled = False
    mnuEdicionOpcion(10).Enabled = False
    mnuEdicionOpcion(30).Enabled = False
    mnuEdicionOpcion(40).Enabled = False
    mnuEdicionOpcion(50).Enabled = False
    mnuEdicionOpcion(60).Enabled = False
    mnuEdicionOpcion(62).Enabled = False
    mnuEdicionOpcion(80).Enabled = False
    mnuEdicionOpcion(90).Enabled = False
    mnuFiltroOpcion(10).Enabled = False
    mnuFiltroOpcion(20).Enabled = False
    mnuRegistroOpcion(10).Enabled = False
    mnuRegistroOpcion(20).Enabled = False
    mnuRegistroOpcion(40).Enabled = True
    mnuRegistroOpcion(50).Enabled = True
    mnuRegistroOpcion(60).Enabled = True
    mnuRegistroOpcion(70).Enabled = True
    mnuRegistroOpcion(72).Enabled = False
    mnuOpcionesOpcion(10).Enabled = False
    mnuOpcionesOpcion(20).Enabled = False
    mnuOpcionesOpcion(40).Enabled = False
    mnuOpcionesOpcion(50).Enabled = False

End Sub


