VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBusGrpTera 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Grupo Terape�tico."
   ClientHeight    =   8220
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11805
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0072.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8220
   ScaleWidth      =   11805
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   7200
      TabIndex        =   6
      Top             =   7320
      Width           =   2295
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Index           =   2
      Left            =   4560
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   7215
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6135
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   6930
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         MaxSelectedRows =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   12224
         _ExtentY        =   10821
         _StockProps     =   79
         Caption         =   "PRODUCTOS"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Grupos Terape�ticos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7200
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   4335
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6495
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   11456
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         OLEDragMode     =   1
         OLEDropMode     =   1
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7935
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusGrpTera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusGrpTera(FR0072.FRM)                                    *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: FEBRERO  1999                                                 *
'* DESCRIPCION: Buscar  Grupos Terapeuticos                             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
'* NOTAS :                                                              *
'* Se utiliza en definir protocolos,                                    *
'* los grids solo deja escoger 1                                        *
'* pues solo se puede coger un producto a la vez                        *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
' Declara variables globales.
Dim Enarbol As Boolean
Dim EnPostWrite As Boolean

Dim indrag As Boolean 'Indicador de operaci�n de arrastrar y colocar.
Dim nodX As Object ' Elemento que se arrastra.
Dim arbolvacio As Boolean

Private Sub Rellenar_Componentes(ByVal vntpadre As Variant, _
                                 ByVal vntgrupo As Variant, _
                                 ByVal vntdesgrupo As Variant)
  Dim vntcompHijo As Variant
  Dim vntdesCompHijo As Variant
  Dim strgrupo As String
  Dim rstgrupo As rdoResultset
  
    strgrupo = "SELECT * FROM FR0000 WHERE fr00codgrpterap_pad='" & vntgrupo & "' order by fr00codgrpterap"
  
    Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
    
    If vntpadre = "grup" Then
        vntpadre = vntpadre & vntgrupo
    Else
        vntpadre = vntpadre & "/" & vntgrupo
    End If
    If rstgrupo.EOF = False Then
        While rstgrupo.EOF = False
            vntcompHijo = rstgrupo("fr00codgrpterap").Value
            vntdesCompHijo = rstgrupo("fr00desgrpterap").Value
            Call tvwItems(0).Nodes.Add(vntpadre, tvwChild, vntpadre & "/" & vntcompHijo, vntcompHijo & ".-" & vntdesCompHijo)
            Call Rellenar_Componentes(vntpadre, vntcompHijo, vntdesCompHijo)
            rstgrupo.MoveNext
        Wend
    End If

End Sub
Private Sub rellenar_TreeView(ByVal CampoEleccion As String)
  Dim strRelGrupo As String
  Dim rstRelGrupo As rdoResultset
  Dim rstesProd As rdoResultset
  Dim vntprotocolo As Variant
  Dim vntdesProtocolo As Variant
  Dim vntgrupo As Variant
  Dim vntdesgrupo As Variant
  Dim vntpadre As Variant
  
    tvwItems(0).Nodes.Clear
    If CampoEleccion = "GrupoQ" Then
      strRelGrupo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='Q' "
    Else
      strRelGrupo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP<>'Q' AND FR00CODGRPTERAP_PAD is null order by FR00CODGRPTERAP"
    End If
    Set rstRelGrupo = objApp.rdoConnect.OpenResultset(strRelGrupo)
    While rstRelGrupo.EOF = False
      arbolvacio = False
        vntgrupo = rstRelGrupo("FR00CODGRPTERAP").Value
        vntdesgrupo = rstRelGrupo("FR00DESGRPTERAP").Value
        Call tvwItems(0).Nodes.Add(, , "grup" & vntgrupo, vntgrupo & ".-" & vntdesgrupo)
        tvwItems(0).Nodes("grup" & vntgrupo).Selected = True
        vntpadre = "grup"
        Call Rellenar_Componentes(vntpadre, vntgrupo, vntdesgrupo)
        rstRelGrupo.MoveNext
    Wend

End Sub



Private Sub cmdaceptar_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
  
    
  'Guardamos el n�mero de filas seleccionadas
  mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  ReDim gintprodbuscado(mintNTotalSelRows, 8)
  gintprodtotal = mintNTotalSelRows
  
  If gstrllamador = "GrupoCGCF" Then
    For mintisel = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
         mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
         gintprodbuscado(mintisel, 0) = grdDBGrid1(1).Columns("C�digo Producto").CellValue(mvarBkmrk) 'c�digo
         gintprodbuscado(mintisel, 1) = grdDBGrid1(1).Columns("C�d.Interno").CellValue(mvarBkmrk) 'c�digo interno
         gintprodbuscado(mintisel, 2) = grdDBGrid1(1).Columns("Descripci�n Medicamento").CellValue(mvarBkmrk) 'descripci�n
         gintprodbuscado(mintisel, 3) = grdDBGrid1(1).Columns("Dosis").CellValue(mvarBkmrk) 'Dosis
         gintprodbuscado(mintisel, 4) = grdDBGrid1(1).Columns("Unid.").CellValue(mvarBkmrk) 'Uni.Medida
         gintprodbuscado(mintisel, 5) = grdDBGrid1(1).Columns("F.F.").CellValue(mvarBkmrk) 'Forma
         gintprodbuscado(mintisel, 6) = grdDBGrid1(1).Columns("Volumen").CellValue(mvarBkmrk) 'Volumen
         gintprodbuscado(mintisel, 7) = grdDBGrid1(1).Columns("V�a").CellValue(mvarBkmrk) 'Via
    Next mintisel
  Else
    For mintisel = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
         mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
         gintprodbuscado(mintisel, 0) = grdDBGrid1(1).Columns("C�digo Producto").CellValue(mvarBkmrk) 'c�digo
         gintprodbuscado(mintisel, 1) = grdDBGrid1(1).Columns("C�d.Interno").CellValue(mvarBkmrk) 'c�digo interno
         gintprodbuscado(mintisel, 2) = grdDBGrid1(1).Columns("Descripci�n Producto").CellValue(mvarBkmrk) 'descripci�n
         gintprodbuscado(mintisel, 3) = grdDBGrid1(1).Columns("Dosis").CellValue(mvarBkmrk) 'Dosis
         gintprodbuscado(mintisel, 4) = grdDBGrid1(1).Columns("Unid.").CellValue(mvarBkmrk) 'Uni.Medida
         gintprodbuscado(mintisel, 5) = grdDBGrid1(1).Columns("F.F.").CellValue(mvarBkmrk) 'Forma
         gintprodbuscado(mintisel, 6) = grdDBGrid1(1).Columns("Volumen").CellValue(mvarBkmrk) 'Volumen
         gintprodbuscado(mintisel, 7) = grdDBGrid1(1).Columns("V�a").CellValue(mvarBkmrk) 'Via
    Next mintisel
  End If
  
  gstrllamador = ""
  
  Unload Me

End Sub

Private Sub Form_Activate()
    grdDBGrid1(1).Columns(0).Visible = False
    If arbolvacio = False Then
      tvwItems(0).Nodes(1).Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Productos"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR7300"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        If gstrllamador = "GrupoCGCF" Then
          Call .FormCreateFilterWhere(strKey, "Medicamentos")
          Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno", cwNumeric)
          Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento", cwString)
          Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmaceutica", cwString)
          Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwDecimal)
          Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Uni.Medida", cwString)
        Else
          Call .FormCreateFilterWhere(strKey, "Productos Sanitarios")
          Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno", cwNumeric)
          Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Producto", cwString)
          Call .FormAddFilterWhere(strKey, "FR73REFERENCIA", "Referencia", cwString)
        End If
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Producto")
    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        If gstrllamador = "GrupoCGCF" Then
          Call .GridAddColumn(objMultiInfo, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
          Call .GridAddColumn(objMultiInfo, "Descripci�n Medicamento", "FR73DESPRODUCTO", cwString, 50)
          Call .GridAddColumn(objMultiInfo, "F.F.", "FRH7CODFORMFAR", cwString, 3)
          Call .GridAddColumn(objMultiInfo, "Dosis", "FR73DOSIS", cwDecimal, 2)
          Call .GridAddColumn(objMultiInfo, "Unid.", "FR93CODUNIMEDIDA", cwString, 5)
          Call .GridAddColumn(objMultiInfo, "C�d.Interno", "FR73CODINTFAR", cwNumeric, 7)
          Call .GridAddColumn(objMultiInfo, "Volumen", "FR73VOLUMEN", cwDecimal, 2)
          Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
        Else
          Call .GridAddColumn(objMultiInfo, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
          Call .GridAddColumn(objMultiInfo, "Descripci�n Producto", "FR73DESPRODUCTO", cwString, 50)
          Call .GridAddColumn(objMultiInfo, "F.F.", "FRH7CODFORMFAR", cwString, 3)
          Call .GridAddColumn(objMultiInfo, "Dosis", "FR73DOSIS", cwDecimal, 2)
          Call .GridAddColumn(objMultiInfo, "Unid.", "FR93CODUNIMEDIDA", cwString, 5)
          Call .GridAddColumn(objMultiInfo, "Referencia", "FR73REFERENCIA", cwString, 15)
          Call .GridAddColumn(objMultiInfo, "C�d.Interno", "FR73CODINTFAR", cwNumeric, 7)
          Call .GridAddColumn(objMultiInfo, "Volumen", "FR73VOLUMEN", cwDecimal, 2)
          Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
        End If

        Call .FormCreateInfo(objMultiInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        If gstrllamador = "GrupoCGCF" Then
          .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
          .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
          .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
          .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
          .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        Else
          .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
          .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
          .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        End If
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
  arbolvacio = True
        
  If gstrllamador = "GrupoCGCF" Then
    Me.Caption = "MANTENIMIENTO FARMACIA. Definici�n Protocolos. B�squeda de Medicamentos por Grupos Terapeuticos"
    fraframe1(2).Caption = "Medicamentos"
    grdDBGrid1(1).Caption = "MEDICAMENTOS"
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(9).Visible = False
    grdDBGrid1(1).Columns(10).Visible = False
    Call rellenar_TreeView("GrupoCGCF")
  Else
    Me.Caption = "MANTENIMIENTO FARMACIA. Definici�n Protocolos. B�squeda de Productos Sanitarios por Grupos Terapeuticos"
    fraframe1(2).Caption = "Productos Sanitarios"
    grdDBGrid1(1).Caption = "PRODUCTOS SANITARIOS"
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(5).Visible = False
    grdDBGrid1(1).Columns(6).Visible = False
    grdDBGrid1(1).Columns(7).Visible = False
    grdDBGrid1(1).Columns(10).Visible = False
    grdDBGrid1(1).Columns(11).Visible = False
    Call rellenar_TreeView("GrupoQ")
  End If
  
  grdDBGrid1(1).Columns(4).Width = 3800

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  Select Case btnButton.Index
  Case 21 'Primero
    If arbolvacio = False Then
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
    Exit Sub
  Case 22 'Anterior
    If arbolvacio = False Then
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    End If
    Exit Sub
  Case 23 'Siguiente
    If arbolvacio = False Then
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    End If
    Exit Sub
  Case 24 'Ultimo
    If arbolvacio = False Then
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
    Exit Sub
  Case Else 'Otro boton
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)

  Select Case intIndex
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_DragDrop(Index As Integer, Source As Control, X As Single, Y As Single)
    If tvwItems(0).DropHighlight Is Nothing Then
        Set tvwItems(0).DropHighlight = Nothing
        indrag = False
        Exit Sub
    Else
        If nodX = tvwItems(0).DropHighlight Then Exit Sub

Cls
        Print nodX.Text & " colocado en " & tvwItems(0).DropHighlight.Text
        Set tvwItems(0).DropHighlight = Nothing
        indrag = False
    End If
End Sub



Private Sub tvwItems_DragOver(Index As Integer, Source As Control, X As Single, Y As Single, State As Integer)
    If indrag = True Then
        ' Establece las coordenadas del mouse en
        ' DropHighlight.
        Set tvwItems(0).DropHighlight = tvwItems(0).HitTest(X, Y)
    End If
End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  If EnPostWrite = True Then
    EnPostWrite = False
    Exit Sub
  End If
        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)
End Sub


Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim aux As Variant
Dim aux2 As Variant

On Error GoTo ERR:
    Enarbol = True
    
    aux2 = Me.MousePointer
    Me.MousePointer = vbHourglass
    aux = Right(Node.Key, Len(Node.Key) - 4)
    While InStr(aux, "/") <> 0
        aux = Right(aux, Len(aux) - InStr(aux, "/"))
    Wend
    
    objWinInfo.objWinActiveForm.strWhere = "FR00CODGRPTERAP='" & aux & "'"
    objWinInfo.DataRefresh
ERR:
    Me.MousePointer = aux2
    Enarbol = False

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


