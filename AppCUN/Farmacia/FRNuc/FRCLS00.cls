VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

'MANTENIMIENTO
'Const PRWinMantActProv                As String = "FR0001"
Const PRWinMantAlergia                As String = "FR0002"
Const PRWinMantAlmacen                As String = "FR0004"
Const PRWinMantCarro                  As String = "FR0005"
Const PRWinMantCentroCoste            As String = "FR0006"
Const PRWinMantEstaPeticion           As String = "FR0007"
Const PRWinMantTipoObserv             As String = "FR0008"
Const PRWinMantTipoVia                As String = "FR0009"
'Const PRWinMantPresentacion           As String = "FR0010"
Const PRWinMantTipoInter              As String = "FR0011"
Const PRWinMantTipoABCProd            As String = "FR0013"
'Const PRWinMantFabricacion            As String = "FR0014"
Const PRWinMantEstadoCarro            As String = "FR0015"
Const PRWinMantABCProveedor           As String = "FR0016"
Const PRWinMantIVA                    As String = "FR0017"
Const PRWinMantMovimiento             As String = "FR0018"
Const PRWinMantUrgencias              As String = "FR0019"
Const PRWinMantUnidadesMedida         As String = "FR0020"
Const PRWinMantPedidoCompra           As String = "FR0021"
Const PRWinMantAlerta                 As String = "FR0022"
Const PRWinMantEstadoOferta           As String = "FR0023"
Const PRWinMantProgramacionCarro      As String = "FR0024"
Const PRWinMantProveedor              As String = "FR0026"
Const PRWinBusGrp                     As String = "FR0156"
Const PRWinMantObservFarma            As String = "FR0027"
'Const PRWinMantProtocolos             As String = "FR0028"
Const PRWinBuscarProductos            As String = "FR0029"
'Const PRWinMantGrupoProt              As String = "FR0030"
Const PRWinBuscarProtocolos           As String = "FR0031"
Const PRWinMantGrupoProducto          As String = "FR0032"
Const PRWinMantEnvase                 As String = "FR0033"
Const PRWinMantExpediente             As String = "FR0034"
Const PRWinMantPrincAct               As String = "FR0054"
Const PRWinMantEstOF                  As String = "FR0055"
Const PRWinMantConvUnidad             As String = "FR0056"
Const PRWinMantFrecuencias            As String = "FR0057"
Const PRWinDefGrpTerap                As String = "FR0058"
Const PRWinMantQuirofano              As String = "FR0059"
Const PRWinMantOrigDest               As String = "FR0060"
'Const PRWinMantTipPaciente            As String = "FR0061"
Const PRWinMantCGCF                   As String = "FR0062"
Const PRWinMantInventario             As String = "FR0064"
Const PRWinBuscarActividades          As String = "FR0065"
Const PRWinMantPeriod                 As String = "FR0068"
Const PRWinMantMonedas                As String = "FR0069"
Const PRWinMantFormFarma              As String = "FR0070"
Const PRWinMantUbicaciones            As String = "FR0071"
Const PRWinMantAcciones               As String = "FR0074"
Const PRWinMantNaturaleza             As String = "FR0075"
Const PRWinMantTipoGrp                As String = "FR0077"
Const PRWinMantTipoProtQuirof         As String = "FR0078"


'DEFINIR
Const PRWinDefCarro                   As String = "FR0040"
Const PRWinBuscarCamas                As String = "FR0051"
Const PRWinDefProgCarro               As String = "FR0052"
Const PRWinDefProducto                As String = "FR0035"
Const PRWinBuscarPrincipiosActivos    As String = "FR0053"
Const PRWinBuscadorPrincipiosActivos  As String = "FR0066"
Const PRWinDefSinonimo                As String = "FR0038"
Const PRWinDefSustituto               As String = "FR0036"
Const PRWinDefSustAlergia             As String = "FR0037"
Const PRWinDefProcFab                 As String = "FR0041"
Const PRWinActCGCF                    As String = "FR0039"
Const PRWinDefProtocolo               As String = "FR0067"
Const PRWinBusGrpTera                 As String = "FR0072"
Const PRWinBusProdProt                As String = "FR0073"
Const PRWinBusGrupos                  As String = "FR0076"

'INVENTARIO F�SICO
Const PRWinRealInvent                 As String = "FR0042"
'CADUCIDADES
Const PRWinConsCaducidad              As String = "FR0044"

'DEFINIR PAR�METROS
Const PRWinDefParamGen                As String = "FR0045"

'DEFINIR PRESUPUESTOS
Const PRWinDefGrpPresup               As String = "FR0047"
Const PRWinEstPresuMes                As String = "FR0049"
Const PRWinContrPresuFarm             As String = "FR0050"
Const PRWinEstPresuAnu                As String = "FR0063"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    'MANTENIMIENTO
'    Case PRWinMantActProv
'      Load frmMantActProv
'      'Call objsecurity.AddHelpContext(528)
'      Call frmMantActProv.Show(vbModal)
'      Call objsecurity.RemoveHelpContext
'      Unload frmMantActProv
'      Set frmMantActProv = Nothing
    Case PRWinMantAlergia
      Load frmMantAlergia
      'Call objsecurity.AddHelpContext(528)
      Call frmMantAlergia.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAlergia
      Set frmMantAlergia = Nothing
    Case PRWinMantAlmacen
      Load frmMantAlmacen
      'Call objsecurity.AddHelpContext(528)
      Call frmMantAlmacen.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAlmacen
      Set frmMantAlmacen = Nothing
    Case PRWinMantCarro
      Load frmMantCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmMantCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantCarro
      Set frmMantCarro = Nothing
    Case PRWinMantCentroCoste
      Load frmMantCentroCoste
      'Call objsecurity.AddHelpContext(528)
      Call frmMantCentroCoste.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantCentroCoste
      Set frmMantCentroCoste = Nothing
    Case PRWinMantEstaPeticion
      Load frmMantEstaPeticion
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstaPeticion.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstaPeticion
      Set frmMantEstaPeticion = Nothing
    Case PRWinMantTipoObserv
      Load frmMantTipoObserv
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoObserv.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoObserv
      Set frmMantTipoObserv = Nothing
    Case PRWinMantTipoVia
      Load frmMantTipoVia
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoVia.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoVia
      Set frmMantTipoVia = Nothing
    'Case PRWinMantPresentacion
    '  Load frmMantPresentacion
    ' 'Call objsecurity.AddHelpContext(528)
    '  Call frmMantPresentacion.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmMantPresentacion
    '  Set frmMantPresentacion = Nothing
    Case PRWinMantTipoInter
      Load frmMantTipoInter
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoInter.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoInter
      Set frmMantTipoInter = Nothing
    Case PRWinMantTipoABCProd
      Load frmMantTipoABCProd
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoABCProd.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoABCProd
      Set frmMantTipoABCProd = Nothing
    'Case PRWinMantFabricacion
    '  Load frmMantFabricacion
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmMantFabricacion.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmMantFabricacion
    '  Set frmMantFabricacion = Nothing
    Case PRWinMantEstadoCarro
      Load frmMantEstadoCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstadoCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstadoCarro
      Set frmMantEstadoCarro = Nothing
    Case PRWinMantABCProveedor
      Load frmMantABCProveedor
      'Call objsecurity.AddHelpContext(528)
      Call frmMantABCProveedor.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantABCProveedor
      Set frmMantABCProveedor = Nothing
    Case PRWinMantIVA
      Load frmMantIVA
      'Call objsecurity.AddHelpContext(528)
      Call frmMantIVA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantIVA
      Set frmMantIVA = Nothing
    Case PRWinMantMovimiento
      Load frmMantMovimiento
      'Call objsecurity.AddHelpContext(528)
      Call frmMantMovimiento.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantMovimiento
      Set frmMantMovimiento = Nothing
    Case PRWinMantUrgencias
      Load frmMantUrgencias
      'Call objsecurity.AddHelpContext(528)
      Call frmMantUrgencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantUrgencias
      Set frmMantUrgencias = Nothing
    Case PRWinMantUnidadesMedida
      Load frmMantUnidadesMedida
      'Call objsecurity.AddHelpContext(528)
      Call frmMantUnidadesMedida.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantUnidadesMedida
      Set frmMantUnidadesMedida = Nothing
    Case PRWinMantPedidoCompra
      Load frmMantPedidoCompra
      'Call objsecurity.AddHelpContext(528)
      Call frmMantPedidoCompra.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantPedidoCompra
      Set frmMantPedidoCompra = Nothing
    Case PRWinMantAlerta
      Load frmMantAlerta
      'Call objsecurity.AddHelpContext(528)
      Call frmMantAlerta.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAlerta
      Set frmMantAlerta = Nothing
    Case PRWinMantEstadoOferta
      Load frmMantEstadoOferta
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstadoOferta.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstadoOferta
      Set frmMantEstadoOferta = Nothing
    Case PRWinMantProgramacionCarro
      Load frmMantProgramacionCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmMantProgramacionCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantProgramacionCarro
      Set frmMantProgramacionCarro = Nothing
    Case PRWinMantProveedor
      Load frmMantProveedor
      'Call objsecurity.AddHelpContext(528)
      Call frmMantProveedor.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantProveedor
      Set frmMantProveedor = Nothing
    Case PRWinMantObservFarma
      Load frmMantObservFarma
      'Call objsecurity.AddHelpContext(528)
      Call frmMantObservFarma.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantObservFarma
      Set frmMantObservFarma = Nothing
'    Case PRWinMantProtocolos
'      Load frmMantProtocolos
'      'Call objsecurity.AddHelpContext(528)
'      Call frmMantProtocolos.Show(vbModal)
'      Call objsecurity.RemoveHelpContext
'      Unload frmMantProtocolos
'      Set frmMantProtocolos = Nothing
    Case PRWinBuscarProductos
      Load frmBuscarProductos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarProductos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarProductos
      Set frmBuscarProductos = Nothing
    'Case PRWinMantGrupoProt
    '  Load frmMantGrupoProt
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmMantGrupoProt.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmMantGrupoProt
    '  Set frmMantGrupoProt = Nothing
    Case PRWinBuscarProtocolos
      Load frmBuscarProtocolos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarProtocolos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarProtocolos
      Set frmBuscarProtocolos = Nothing
    Case PRWinMantGrupoProducto
      Load frmMantGrupoProducto
      'Call objsecurity.AddHelpContext(528)
      Call frmMantGrupoProducto.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantGrupoProducto
      Set frmMantGrupoProducto = Nothing
    Case PRWinMantEnvase
      Load frmMantEnvase
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEnvase.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEnvase
      Set frmMantEnvase = Nothing
    Case PRWinMantExpediente
      Load frmMantExpediente
      'Call objsecurity.AddHelpContext(528)
      Call frmMantExpediente.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantExpediente
      Set frmMantExpediente = Nothing
    Case PRWinMantPrincAct
      Load frmMantPrincAct
      'Call objsecurity.AddHelpContext(528)
      Call frmMantPrincAct.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantPrincAct
      Set frmMantPrincAct = Nothing
    Case PRWinMantEstOF
       Load frmMantEstOF
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstOF.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstOF
      Set frmMantEstOF = Nothing
    Case PRWinMantQuirofano
       Load frmMantQuirofano
      'Call objsecurity.AddHelpContext(528)
      Call frmMantQuirofano.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantQuirofano
      Set frmMantQuirofano = Nothing
    Case PRWinDefGrpTerap
       Load frmDefGrpTerap
      'Call objsecurity.AddHelpContext(528)
      Call frmDefGrpTerap.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefGrpTerap
      Set frmDefGrpTerap = Nothing
    Case PRWinMantFrecuencias
       Load frmMantFrecuencias
      'Call objsecurity.AddHelpContext(528)
      Call frmMantFrecuencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantFrecuencias
      Set frmMantFrecuencias = Nothing
    Case PRWinMantConvUnidad
       Load frmMantConvUnidad
      'Call objsecurity.AddHelpContext(528)
      Call frmMantConvUnidad.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantConvUnidad
      Set frmMantConvUnidad = Nothing
    Case PRWinMantOrigDest
       Load frmMantOrigDest
      'Call objsecurity.AddHelpContext(528)
      Call frmMantOrigDest.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantOrigDest
      Set frmMantOrigDest = Nothing
    'Case PRWinMantTipPaciente
    '   Load frmMantTipPaciente
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmMantTipPaciente.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmMantTipPaciente
    '  Set frmMantTipPaciente = Nothing
     Case PRWinMantCGCF
       Load frmMantCGCF
      'Call objsecurity.AddHelpContext(528)
      Call frmMantCGCF.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantCGCF
      Set frmMantCGCF = Nothing
    Case PRWinBuscarActividades
       Load frmBuscarActividades
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarActividades.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBuscarActividades
      Set frmBuscarActividades = Nothing
    Case PRWinMantInventario
       Load frmMantInventario
      'Call objsecurity.AddHelpContext(528)
      Call frmMantInventario.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantInventario
      Set frmMantInventario = Nothing
    Case PRWinMantPeriod
       Load frmMantPeriod
      'Call objsecurity.AddHelpContext(528)
      Call frmMantPeriod.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantPeriod
      Set frmMantPeriod = Nothing
    Case PRWinMantMonedas
       Load frmMantMonedas
      'Call objsecurity.AddHelpContext(528)
      Call frmMantMonedas.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantMonedas
      Set frmMantMonedas = Nothing
    Case PRWinMantFormFarma
       Load frmMantFormFarma
      'Call objsecurity.AddHelpContext(528)
      Call frmMantFormFarma.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantFormFarma
      Set frmMantFormFarma = Nothing
    Case PRWinMantUbicaciones
       Load frmMantUbicaciones
      'Call objsecurity.AddHelpContext(528)
      Call frmMantUbicaciones.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantUbicaciones
      Set frmMantUbicaciones = Nothing
    Case PRWinMantAcciones
      Load frmMantAcciones
      Call frmMantAcciones.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAcciones
      Set frmMantAcciones = Nothing
    Case PRWinMantNaturaleza
      Load frmMantNaturaleza
      Call frmMantNaturaleza.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantNaturaleza
      Set frmMantNaturaleza = Nothing
    Case PRWinMantTipoGrp
      Load frmMantTipoGrp
      Call frmMantTipoGrp.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoGrp
      Set frmMantTipoGrp = Nothing
    Case PRWinMantTipoProtQuirof
      Load frmMantTipoProtQuirof
      Call frmMantTipoProtQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoProtQuirof
      Set frmMantTipoProtQuirof = Nothing
      
      
    'DEFINIR
    Case PRWinDefCarro
      Load frmDefCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmDefCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefCarro
      Set frmDefCarro = Nothing
    Case PRWinBuscarCamas
      Load frmBuscarCamas
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarCamas.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarCamas
      Set frmBuscarCamas = Nothing
    Case PRWinDefProgCarro
      Load frmDefProgCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProgCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProgCarro
      Set frmDefProgCarro = Nothing
    Case PRWinDefProducto
      Load frmDefProducto
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProducto.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProducto
      Set frmDefProducto = Nothing
    Case PRWinBuscarPrincipiosActivos
      Load frmBuscarPrincipiosActivos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarPrincipiosActivos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarPrincipiosActivos
      Set frmBuscarPrincipiosActivos = Nothing
    Case PRWinBuscadorPrincipiosActivos
      Load frmBuscadorPrincipiosActivos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscadorPrincipiosActivos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscadorPrincipiosActivos
      Set frmBuscadorPrincipiosActivos = Nothing
      
    Case PRWinDefSinonimo
      Load frmDefSinonimo
      'Call objsecurity.AddHelpContext(528)
      Call frmDefSinonimo.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefSinonimo
      Set frmDefSinonimo = Nothing
    Case PRWinDefSustituto
      Load frmDefSustituto
      'Call objsecurity.AddHelpContext(528)
      Call frmDefSustituto.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefSustituto
      Set frmDefSustituto = Nothing
    Case PRWinDefSustAlergia
      Load frmDefSustAlergia
      'Call objsecurity.AddHelpContext(528)
      Call frmDefSustAlergia.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefSustAlergia
      Set frmDefSustAlergia = Nothing
    Case PRWinDefProcFab
      Load frmDefProcFab
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProcFab.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProcFab
      Set frmDefProcFab = Nothing
    Case PRWinActCGCF
      Load frmActCGCF
      'Call objsecurity.AddHelpContext(528)
      Call frmActCGCF.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmActCGCF
      Set frmActCGCF = Nothing
    Case PRWinDefProtocolo
      Load frmDefProtocolos
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProtocolos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProtocolos
      Set frmDefProtocolos = Nothing
    
    
    'INVENTARIO F�SICO
    Case PRWinRealInvent
      Load frmRealInvent
      'Call objsecurity.AddHelpContext(528)
      Call frmRealInvent.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRealInvent
      Set frmRealInvent = Nothing
    'CADUCIDAD
    Case PRWinConsCaducidad
      Load frmConsCaducidad
      'Call objsecurity.AddHelpContext(528)
      Call frmConsCaducidad.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConsCaducidad
      Set frmConsCaducidad = Nothing
    
    'DEFINIR PAR�METROS
    Case PRWinDefParamGen
      Load frmDefParamGen
      'Call objsecurity.AddHelpContext(528)
      Call frmDefParamGen.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefParamGen
      Set frmDefParamGen = Nothing
    
    Case PRWinBusGrp
      Load frmBuscarGrTerapeuticos
      Call frmBuscarGrTerapeuticos.Show(vbModal)
      Unload frmBuscarGrTerapeuticos
      Set frmBuscarGrTerapeuticos = Nothing
      
      
    'DEFINIR PRESUPUESTOS
    Case PRWinDefGrpPresup
      Load frmDefGrpPresup
      'Call objsecurity.AddHelpContext(528)
      Call frmDefGrpPresup.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefGrpPresup
      Set frmDefGrpPresup = Nothing
    
    Case PRWinEstPresuMes
      Load frmEstPresuMes
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPresuMes.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmEstPresuMes
      Set frmEstPresuMes = Nothing
    
    Case PRWinEstPresuAnu
      Load frmEstPresuAnu
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPresuAnu.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmEstPresuAnu
      Set frmEstPresuAnu = Nothing
    
    Case PRWinContrPresuFarm
      Load frmContrPresuFarm
      'Call objsecurity.AddHelpContext(528)
      Call frmContrPresuFarm.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmContrPresuFarm
      Set frmContrPresuFarm = Nothing
    
    Case PRWinBusGrpTera
      Load frmBusGrpTera
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTera.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpTera
      Set frmBusGrpTera = Nothing
    
    Case PRWinBusProdProt
      Load frmBusProdProt
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProdProt.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProdProt
      Set frmBusProdProt = Nothing
      
    Case PRWinBusGrupos
      Load frmBuscarGrupos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarGrupos.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBuscarGrupos
      Set frmBuscarGrupos = Nothing
  End Select
  Call ERR.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 73, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
'  aProcess(1, 1) = PRWinMantActProv
'  aProcess(1, 2) = "Mantenimiento Actividad Proveedor"
'  aProcess(1, 3) = True
'  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = PRWinMantAlergia
  aProcess(2, 2) = "Mantenimiento Alergia"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow

  aProcess(3, 1) = PRWinMantAlmacen
  aProcess(3, 2) = "Mantenimiento Almac�n"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow

  aProcess(4, 1) = PRWinMantCarro
  aProcess(4, 2) = "Mantenimiento Carro"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow

  aProcess(5, 1) = PRWinMantCentroCoste
  aProcess(5, 2) = "Mantenimiento Centro Coste"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow

  aProcess(6, 1) = PRWinMantEstaPeticion
  aProcess(6, 2) = "Mantenimiento Estado Petici�n"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow

  aProcess(7, 1) = PRWinMantTipoObserv
  aProcess(7, 2) = "Mantenimiento Tipo Observaci�n"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow

  aProcess(8, 1) = PRWinMantTipoVia
  aProcess(8, 2) = "Mantenimiento Tipo V�a"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow

  'aProcess(9, 1) = PRWinMantPresentacion
  'aProcess(9, 2) = "Mantenimiento Presentaci�n"
  'aProcess(9, 3) = True
  'aProcess(9, 4) = cwTypeWindow

  aProcess(10, 1) = PRWinMantTipoInter
  aProcess(10, 2) = "Mantenimiento Tipo Interacci�n"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = PRWinMantTipoABCProd
  aProcess(11, 2) = "Mantenimiento Tipo ABC Producto"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
  
  'aProcess(12, 1) = PRWinMantFabricacion
  'aProcess(12, 2) = "Mantenimiento Tipo de Fabricaci�n"
  'aProcess(12, 3) = True
  'aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinMantEstadoCarro
  aProcess(13, 2) = "Mantenimiento Estado Carro"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWinMantABCProveedor
  aProcess(14, 2) = "Mantenimiento Tipo ABC Proveedor"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWinMantIVA
  aProcess(15, 2) = "Mantenimiento Tipo IVA"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinMantMovimiento
  aProcess(16, 2) = "Mantenimiento Tipo Movimiento"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = PRWinMantUrgencias
  aProcess(17, 2) = "Mantenimiento Tipo Urgencias"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinMantUnidadesMedida
  aProcess(18, 2) = "Mantenimiento Unidades de Medida "
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinMantPedidoCompra
  aProcess(19, 2) = "Mantenimiento Estado Pedido de Compra"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinMantAlerta
  aProcess(20, 2) = "Mantenimiento Tipo de Alerta"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinMantEstadoOferta
  aProcess(21, 2) = "Mantenimiento Estado Oferta"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinMantProgramacionCarro
  aProcess(22, 2) = "Mantenimiento Programaci�n Carro"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow

  aProcess(23, 1) = PRWinMantProveedor
  aProcess(23, 2) = "Mantenimiento Proveedor"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeWindow

  aProcess(24, 1) = PRWinMantObservFarma
  aProcess(24, 2) = "Mantenimiento Observaciones Farmacia"
  aProcess(24, 3) = True
  aProcess(24, 4) = cwTypeWindow
  
'  aProcess(25, 1) = PRWinMantProtocolos
'  aProcess(25, 2) = "Definir Protocolos"
'  aProcess(25, 3) = True
'  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinBuscarProductos
  aProcess(26, 2) = "Buscar Productos"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow
   
  'aProcess(27, 1) = PRWinMantGrupoProt
  'aProcess(27, 2) = "Definir Grupos de Protocolos"
  'aProcess(27, 3) = True
  'aProcess(27, 4) = cwTypeWindow
  
  aProcess(28, 1) = PRWinBuscarProtocolos
  aProcess(28, 2) = "Buscar Protocolos"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow
  
  aProcess(29, 1) = PRWinMantGrupoProducto
  aProcess(29, 2) = "Definir Grupos de Productos"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow
  
  aProcess(30, 1) = PRWinMantEnvase
  aProcess(30, 2) = "Mantenimiento Envases"
  aProcess(30, 3) = True
  aProcess(30, 4) = cwTypeWindow
  
  aProcess(31, 1) = PRWinMantExpediente
  aProcess(31, 2) = "Mantenimiento Expedientes"
  aProcess(31, 3) = True
  aProcess(31, 4) = cwTypeWindow
  
  aProcess(32, 1) = PRWinMantPrincAct
  aProcess(32, 2) = "Mantenimiento Principios Activos"
  aProcess(32, 3) = True
  aProcess(32, 4) = cwTypeWindow
  
  aProcess(33, 1) = PRWinMantEstOF
  aProcess(33, 2) = "Mantenimiento Estado Orden Fabricaci�n"
  aProcess(33, 3) = True
  aProcess(33, 4) = cwTypeWindow
  
  aProcess(34, 1) = PRWinMantQuirofano
  aProcess(34, 2) = "Mantenimiento Quir�fanos"
  aProcess(34, 3) = True
  aProcess(34, 4) = cwTypeWindow
  
  aProcess(35, 1) = PRWinMantConvUnidad
  aProcess(35, 2) = "Mantenimiento Conversi�n de Unidad"
  aProcess(35, 3) = True
  aProcess(35, 4) = cwTypeWindow
  
  aProcess(36, 1) = PRWinMantFrecuencias
  aProcess(36, 2) = "Mantenimiento Frecuencias"
  aProcess(36, 3) = True
  aProcess(36, 4) = cwTypeWindow
  
  aProcess(37, 1) = PRWinMantOrigDest
  aProcess(37, 2) = "Mantenimiento Origen Destino"
  aProcess(37, 3) = True
  aProcess(37, 4) = cwTypeWindow
  
  'aProcess(38, 1) = PRWinMantTipPaciente
  'aProcess(38, 2) = "Mantenimiento Tipo Paciente"
  'aProcess(38, 3) = True
  'aProcess(38, 4) = cwTypeWindow
    
  
  'DEFINIR
  aProcess(39, 1) = PRWinDefCarro
  aProcess(39, 2) = "Definici�n de Carro"
  aProcess(39, 3) = True
  aProcess(39, 4) = cwTypeWindow

  aProcess(40, 1) = PRWinDefProcFab
  aProcess(40, 2) = "Definici�n de Proceso de Fabricaci�n"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeWindow
  
  aProcess(41, 1) = PRWinDefGrpTerap
  aProcess(41, 2) = "Definir Grupos Terape�ticos"
  aProcess(41, 3) = True
  aProcess(41, 4) = cwTypeWindow
  
  aProcess(42, 1) = PRWinBuscarCamas
  aProcess(42, 2) = "Buscar Camas"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeWindow

  aProcess(43, 1) = PRWinDefProgCarro
  aProcess(43, 2) = "Definici�n Programaci�n Carro"
  aProcess(43, 3) = False
  aProcess(43, 4) = cwTypeWindow
  
  aProcess(44, 1) = PRWinDefProducto
  aProcess(44, 2) = "Definir Productos"
  aProcess(44, 3) = True
  aProcess(44, 4) = cwTypeWindow
  
  aProcess(45, 1) = PRWinBuscarPrincipiosActivos
  aProcess(45, 2) = "Buscar Principios Activos"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeWindow
 
  aProcess(46, 1) = PRWinDefSinonimo
  aProcess(46, 2) = "Definici�n Sin�nimos"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeWindow
  
  aProcess(47, 1) = PRWinDefSustituto
  aProcess(47, 2) = "Definici�n Sustitutos"
  aProcess(47, 3) = False
  aProcess(47, 4) = cwTypeWindow
  
  aProcess(48, 1) = PRWinDefSustAlergia
  aProcess(48, 2) = "Definici�n Sustituci�n Alergias"
  aProcess(48, 3) = False
  aProcess(48, 4) = cwTypeWindow
  
  aProcess(49, 1) = PRWinActCGCF
  aProcess(49, 2) = "Actualizar CGCF"
  aProcess(49, 3) = True
  aProcess(49, 4) = cwTypeWindow
  
  'INVENTARIO F�SICO
  aProcess(50, 1) = PRWinRealInvent
  aProcess(50, 2) = "Realizar Inventario"
  aProcess(50, 3) = True
  aProcess(50, 4) = cwTypeWindow
  
  'CADUCIDAD
  aProcess(51, 1) = PRWinConsCaducidad
  aProcess(51, 2) = "Consultar Caducidades"
  aProcess(51, 3) = True
  aProcess(51, 4) = cwTypeWindow
    
  aProcess(52, 1) = PRWinMantCGCF
  aProcess(52, 2) = "Mantenimiento CGCF"
  aProcess(52, 3) = True
  aProcess(52, 4) = cwTypeWindow
  
  'DEFINIR PAR�METROS
  aProcess(53, 1) = PRWinDefParamGen
  aProcess(53, 2) = "Definir Par�metros Generales"
  aProcess(53, 3) = True
  aProcess(53, 4) = cwTypeWindow
  
  'DEFINIR PRESUPUESTOS
  aProcess(54, 1) = PRWinDefGrpPresup
  aProcess(54, 2) = "Definir Grupos Presupuestarios"
  aProcess(54, 3) = True
  aProcess(54, 4) = cwTypeWindow
   
  aProcess(55, 1) = PRWinEstPresuMes
  aProcess(55, 2) = "Establecer Presupuestos Mensuales"
  aProcess(55, 3) = False
  aProcess(55, 4) = cwTypeWindow
  
  aProcess(56, 1) = PRWinEstPresuAnu
  aProcess(56, 2) = "Establecer Presupuestos Anuales"
  aProcess(56, 3) = True
  aProcess(56, 4) = cwTypeWindow
  
  aProcess(57, 1) = PRWinContrPresuFarm
  aProcess(57, 2) = "Controlar Presupuestos"
  aProcess(57, 3) = True
  aProcess(57, 4) = cwTypeWindow
  
  
  aProcess(58, 1) = PRWinBuscarActividades
  aProcess(58, 2) = "Buscar Actividades Proveedor"
  aProcess(58, 3) = False
  aProcess(58, 4) = cwTypeWindow
  
  aProcess(59, 1) = PRWinMantInventario
  aProcess(59, 2) = "Mantenimiento Inventario"
  aProcess(59, 3) = True
  aProcess(59, 4) = cwTypeWindow

  aProcess(60, 1) = PRWinMantPeriod
  aProcess(60, 2) = "Mantenimiento Periodicidades"
  aProcess(60, 3) = True
  aProcess(60, 4) = cwTypeWindow

  aProcess(61, 1) = PRWinDefProtocolo
  aProcess(61, 2) = "Definici�n Protocolos"
  aProcess(61, 3) = True
  aProcess(61, 4) = cwTypeWindow
  
  aProcess(62, 1) = PRWinBuscadorPrincipiosActivos
  aProcess(62, 2) = "Buscador Principios Activos"
  aProcess(62, 3) = False
  aProcess(62, 4) = cwTypeWindow
  
  'MANTENIMIENTO
  
  aProcess(63, 1) = PRWinMantMonedas
  aProcess(63, 2) = "Mantenimiento Monedas"
  aProcess(63, 3) = True
  aProcess(63, 4) = cwTypeWindow
  
  aProcess(64, 1) = PRWinMantFormFarma
  aProcess(64, 2) = "Mantenimiento Formas Farmace�ticas"
  aProcess(64, 3) = True
  aProcess(64, 4) = cwTypeWindow
  
  aProcess(65, 1) = PRWinMantUbicaciones
  aProcess(65, 2) = "Mantenimiento Ubicaciones"
  aProcess(65, 3) = True
  aProcess(65, 4) = cwTypeWindow
  
  aProcess(66, 1) = PRWinBusGrpTera
  aProcess(66, 2) = "Buscador Grupos Terape�ticos"
  aProcess(66, 3) = False
  aProcess(66, 4) = cwTypeWindow
  
  aProcess(67, 1) = PRWinBusProdProt
  aProcess(67, 2) = "Buscador Productos Grupos Terape�ticos"
  aProcess(67, 3) = False
  aProcess(67, 4) = cwTypeWindow

  aProcess(68, 1) = PRWinMantAcciones
  aProcess(68, 2) = "Mantenimiento Acciones"
  aProcess(68, 3) = True
  aProcess(68, 4) = cwTypeWindow
  
  aProcess(69, 1) = PRWinMantNaturaleza
  aProcess(69, 2) = "Mantenimiento Naturaleza Interacciones"
  aProcess(69, 3) = True
  aProcess(69, 4) = cwTypeWindow
  
  aProcess(70, 1) = PRWinBusGrupos
  aProcess(70, 2) = "Buscador de Grupos"
  aProcess(70, 3) = False
  aProcess(70, 4) = cwTypeWindow

  aProcess(71, 1) = PRWinMantTipoGrp
  aProcess(71, 2) = "Mantenimiento Tipos de Grupos"
  aProcess(71, 3) = True
  aProcess(71, 4) = cwTypeWindow

  aProcess(72, 1) = PRWinBusGrp
  aProcess(72, 2) = "Buscar Grupos Terap�uticos"
  aProcess(72, 3) = False
  aProcess(72, 4) = cwTypeWindow
  
  aProcess(73, 1) = PRWinMantTipoProtQuirof
  aProcess(73, 2) = "Mantenimiento Tipos de Protocolos de Quir�fano"
  aProcess(73, 3) = True
  aProcess(73, 4) = cwTypeWindow
  
End Sub
