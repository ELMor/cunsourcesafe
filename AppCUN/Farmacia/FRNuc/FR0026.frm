VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantProveedor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Proveedor."
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   39
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Proveedores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Index           =   1
      Left            =   120
      TabIndex        =   40
      Top             =   480
      Width           =   11700
      Begin TabDlg.SSTab tabTab1 
         Height          =   3855
         Index           =   0
         Left            =   120
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   6800
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0026.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "SSTab1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0026.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab SSTab1 
            Height          =   2895
            Left            =   120
            TabIndex        =   46
            Top             =   840
            Width           =   10815
            _ExtentX        =   19076
            _ExtentY        =   5106
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0026.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(6)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(5)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(4)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(3)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(13)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(14)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(15)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(16)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(30)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(2)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(31)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(32)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(33)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(7)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(6)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(5)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(4)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(14)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(15)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(16)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(17)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(30)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(3)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(31)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(32)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(33)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).ControlCount=   26
            TabCaption(1)   =   "Delegado 1"
            TabPicture(1)   =   "FR0026.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(7)"
            Tab(1).Control(1)=   "lblLabel1(8)"
            Tab(1).Control(2)=   "lblLabel1(9)"
            Tab(1).Control(3)=   "lblLabel1(10)"
            Tab(1).Control(4)=   "lblLabel1(11)"
            Tab(1).Control(5)=   "lblLabel1(12)"
            Tab(1).Control(6)=   "lblLabel1(17)"
            Tab(1).Control(7)=   "lblLabel1(18)"
            Tab(1).Control(8)=   "lblLabel1(19)"
            Tab(1).Control(9)=   "lblLabel1(34)"
            Tab(1).Control(10)=   "txtText1(8)"
            Tab(1).Control(11)=   "txtText1(9)"
            Tab(1).Control(12)=   "txtText1(10)"
            Tab(1).Control(13)=   "txtText1(11)"
            Tab(1).Control(14)=   "txtText1(12)"
            Tab(1).Control(15)=   "txtText1(13)"
            Tab(1).Control(16)=   "txtText1(18)"
            Tab(1).Control(17)=   "txtText1(19)"
            Tab(1).Control(18)=   "txtText1(20)"
            Tab(1).Control(19)=   "txtText1(34)"
            Tab(1).ControlCount=   20
            TabCaption(2)   =   "Delegado 2"
            TabPicture(2)   =   "FR0026.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(20)"
            Tab(2).Control(1)=   "lblLabel1(21)"
            Tab(2).Control(2)=   "lblLabel1(22)"
            Tab(2).Control(3)=   "lblLabel1(23)"
            Tab(2).Control(4)=   "lblLabel1(24)"
            Tab(2).Control(5)=   "lblLabel1(25)"
            Tab(2).Control(6)=   "lblLabel1(26)"
            Tab(2).Control(7)=   "lblLabel1(27)"
            Tab(2).Control(8)=   "lblLabel1(29)"
            Tab(2).Control(9)=   "lblLabel1(35)"
            Tab(2).Control(10)=   "txtText1(21)"
            Tab(2).Control(11)=   "txtText1(22)"
            Tab(2).Control(12)=   "txtText1(23)"
            Tab(2).Control(13)=   "txtText1(24)"
            Tab(2).Control(14)=   "txtText1(25)"
            Tab(2).Control(15)=   "txtText1(26)"
            Tab(2).Control(16)=   "txtText1(27)"
            Tab(2).Control(17)=   "txtText1(28)"
            Tab(2).Control(18)=   "txtText1(29)"
            Tab(2).Control(19)=   "txtText1(35)"
            Tab(2).ControlCount=   20
            TabCaption(3)   =   "Comentarios"
            TabPicture(3)   =   "FR0026.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(1)"
            Tab(3).Control(1)=   "txtText1(2)"
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Par�metros"
            TabPicture(4)   =   "FR0026.frx":00A8
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(36)"
            Tab(4).Control(0).Enabled=   0   'False
            Tab(4).Control(1)=   "lblLabel1(38)"
            Tab(4).Control(1).Enabled=   0   'False
            Tab(4).Control(2)=   "txtText1(38)"
            Tab(4).Control(2).Enabled=   0   'False
            Tab(4).Control(3)=   "chkCheck1(0)"
            Tab(4).Control(3).Enabled=   0   'False
            Tab(4).Control(4)=   "chkCheck1(1)"
            Tab(4).Control(4).Enabled=   0   'False
            Tab(4).Control(5)=   "chkCheck1(2)"
            Tab(4).Control(5).Enabled=   0   'False
            Tab(4).ControlCount=   6
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Facturaci�n Mensual"
               DataField       =   "FR79INDFACTMES"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   2
               Left            =   -74760
               TabIndex        =   87
               Tag             =   "F.M.|Factuarci�n Mensual"
               Top             =   1680
               Width           =   4215
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Tiene Algo Pendiente de Bonificar"
               DataField       =   "FR79INDPTEBONIF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   86
               Tag             =   "Pte.Bon.|Pendiente de Bonificar"
               Top             =   1440
               Width           =   4215
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Trabaja por Pronto Pago"
               DataField       =   "FR79INDPRONTOPAG"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   -74760
               TabIndex        =   85
               Tag             =   "P.P.|Pronto Pago"
               Top             =   1200
               Width           =   4215
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR79CTRLFIAB"
               Height          =   330
               Index           =   38
               Left            =   -74760
               TabIndex        =   82
               Tag             =   "C.F.|Control de Fiabilidad"
               Top             =   720
               Width           =   885
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79TFNO33"
               Height          =   330
               Index           =   35
               Left            =   -72960
               TabIndex        =   32
               Tag             =   "Tfno2. D2|Segundo Tel�fono del Segundo Delegado"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79TFNO22"
               Height          =   330
               Index           =   34
               Left            =   -72960
               TabIndex        =   22
               Tag             =   "Tfno2. D1|Segundo Tel�fono del Primer Delegado"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DINABC"
               Height          =   330
               Index           =   33
               Left            =   6600
               TabIndex        =   14
               Tag             =   "ABC(CE)|Clasificaci�n ABC Cantidad de Pesetas"
               Top             =   2400
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79CANTABC"
               Height          =   330
               Index           =   32
               Left            =   2280
               TabIndex        =   13
               Tag             =   "ABC(C)|Clasificaci�n ABC Cantidad de Productos"
               Top             =   2400
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79CODCONT"
               Height          =   330
               Index           =   31
               Left            =   120
               TabIndex        =   12
               Tag             =   "C.C.|C�digo Contable"
               Top             =   2400
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79TFNO11"
               Height          =   330
               Index           =   3
               Left            =   2280
               TabIndex        =   9
               Tag             =   "2�Tfno|Segundo Tel�fono"
               Top             =   1800
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79NIF"
               Height          =   330
               Index           =   30
               Left            =   120
               TabIndex        =   2
               Tag             =   "NIF"
               Top             =   600
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79EMAIL3"
               Height          =   330
               Index           =   29
               Left            =   -69120
               TabIndex        =   34
               Tag             =   "E-Mail D2|E-mail Segundo Delegado"
               Top             =   2400
               Width           =   4500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DISTRITO3"
               Height          =   330
               Index           =   28
               Left            =   -66480
               TabIndex        =   27
               Tag             =   "Dist. D2|Distrito Segundo Delegado"
               Top             =   1200
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79TFNO3"
               Height          =   330
               Index           =   27
               Left            =   -74880
               TabIndex        =   31
               Tag             =   "Tfno. D2|Tel�fono Segundo Delegado"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79FAX3"
               Height          =   330
               Index           =   26
               Left            =   -71040
               TabIndex        =   33
               Tag             =   "Fax D2|Fax del Segundo Delegado"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79PAIS3"
               Height          =   330
               Index           =   25
               Left            =   -67440
               TabIndex        =   30
               Tag             =   "Pa�s D2|Pa�s Segundo Delegado"
               Top             =   1800
               Width           =   2780
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79PROVINCIA3"
               Height          =   330
               Index           =   24
               Left            =   -70560
               TabIndex        =   29
               Tag             =   "Prov. D2|Provincia Segundo Delegado"
               Top             =   1800
               Width           =   2800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79LOCALIDAD3"
               Height          =   330
               Index           =   23
               Left            =   -74880
               TabIndex        =   28
               Tag             =   "Localidad D2|Localidad Segundo Delegado"
               Top             =   1800
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DIRECCION3"
               Height          =   330
               Index           =   22
               Left            =   -74880
               TabIndex        =   26
               Tag             =   "Direcci�n D2|Direcci�n Segundo Delegado"
               Top             =   1200
               Width           =   8055
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79REPRESENT3"
               Height          =   330
               Index           =   21
               Left            =   -74880
               TabIndex        =   25
               Tag             =   "Nombre D2|Nombre Segundo Delegado"
               Top             =   600
               Width           =   10245
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79EMAIL2"
               Height          =   330
               Index           =   20
               Left            =   -69120
               TabIndex        =   24
               Tag             =   "E-Mail D1|E-mail Primer Delegado"
               Top             =   2400
               Width           =   4500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DISTRITO2"
               Height          =   330
               Index           =   19
               Left            =   -66480
               TabIndex        =   17
               Tag             =   "Dist. D1|Distrito Primer Delegado"
               Top             =   1200
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79TFNO2"
               Height          =   330
               Index           =   18
               Left            =   -74880
               TabIndex        =   21
               Tag             =   "Tfno1. D1|Primer Tel�fono del Primer Delegado"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79FAX2"
               Height          =   330
               Index           =   13
               Left            =   -71040
               TabIndex        =   23
               Tag             =   "Fax. D1|Fax del Primer Delegado"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79PAIS2"
               Height          =   330
               Index           =   12
               Left            =   -67440
               TabIndex        =   20
               Tag             =   "Pa�s D1|Pa�s Primer Delegado"
               Top             =   1800
               Width           =   2780
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79PROVINCIA2"
               Height          =   330
               Index           =   11
               Left            =   -70560
               TabIndex        =   19
               Tag             =   "Prov. D1|Provincia Primer Delegado"
               Top             =   1800
               Width           =   2800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79LOCALIDAD2"
               Height          =   330
               Index           =   10
               Left            =   -74880
               TabIndex        =   18
               Tag             =   "Loc. D1|Localidad Primer Delegado"
               Top             =   1800
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DIRECCION2"
               Height          =   330
               Index           =   9
               Left            =   -74880
               TabIndex        =   16
               Tag             =   "Dirrecci�n D1|Direcci�n Primer Delegado"
               Top             =   1200
               Width           =   8055
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79REPRESENT2"
               Height          =   330
               Index           =   8
               Left            =   -74880
               TabIndex        =   15
               Tag             =   "Nombre D1|Nombre Primer Delegado"
               Top             =   600
               Width           =   10245
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79EMAIL1"
               Height          =   330
               Index           =   17
               Left            =   6600
               TabIndex        =   11
               Tag             =   "E-mail"
               Top             =   1800
               Width           =   3840
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DISTRITO1"
               Height          =   330
               Index           =   16
               Left            =   8880
               TabIndex        =   4
               Tag             =   "Distrito"
               Top             =   600
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79TFNO1"
               Height          =   330
               Index           =   15
               Left            =   120
               TabIndex        =   8
               Tag             =   "1er Tel�fono"
               Top             =   1800
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79FAX1"
               Height          =   330
               Index           =   14
               Left            =   4440
               TabIndex        =   10
               Tag             =   "Fax"
               Top             =   1800
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79COMENTARIOS"
               Height          =   1770
               Index           =   2
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   35
               Tag             =   "Comentarios"
               Top             =   840
               Width           =   10200
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79DIRECCION1"
               Height          =   330
               Index           =   4
               Left            =   2040
               TabIndex        =   3
               Tag             =   "Direcci�n"
               Top             =   600
               Width           =   6615
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79LOCALIDAD1"
               Height          =   330
               Index           =   5
               Left            =   120
               TabIndex        =   5
               Tag             =   "Localidad"
               Top             =   1200
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79PROVINCIA1"
               Height          =   330
               Index           =   6
               Left            =   4440
               TabIndex        =   6
               Tag             =   "Provincia"
               Top             =   1200
               Width           =   2800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79PAIS1"
               Height          =   330
               Index           =   7
               Left            =   7680
               TabIndex        =   7
               Tag             =   "Pa�s"
               Top             =   1200
               Width           =   2780
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Control de Fiabilidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   38
               Left            =   -74760
               TabIndex        =   84
               Top             =   480
               Width           =   1755
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   36
               Left            =   -74760
               TabIndex        =   83
               Top             =   1080
               Width           =   75
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "2� Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   -72960
               TabIndex        =   81
               Top             =   2160
               Width           =   1005
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "2� Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   34
               Left            =   -72960
               TabIndex        =   80
               Top             =   2160
               Width           =   1005
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Clasificaci�n ABC Cantidad de Pesetas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   33
               Left            =   6600
               TabIndex        =   79
               Top             =   2160
               Width           =   3330
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Clasificaci�n ABC Cantidad de Productos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   32
               Left            =   2280
               TabIndex        =   78
               Top             =   2160
               Width           =   3495
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Contable"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   31
               Left            =   120
               TabIndex        =   77
               Top             =   2160
               Width           =   1410
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "2� Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   2280
               TabIndex        =   76
               Top             =   1560
               Width           =   1005
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "NIF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   30
               Left            =   120
               TabIndex        =   75
               Top             =   360
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "E-mail"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   29
               Left            =   -69120
               TabIndex        =   74
               Top             =   2160
               Width           =   525
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   27
               Left            =   -74880
               TabIndex        =   73
               Top             =   960
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   26
               Left            =   -71040
               TabIndex        =   72
               Top             =   2160
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   25
               Left            =   -74880
               TabIndex        =   71
               Top             =   2160
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   24
               Left            =   -67440
               TabIndex        =   70
               Top             =   1560
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   23
               Left            =   -70560
               TabIndex        =   69
               Top             =   1560
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   -74880
               TabIndex        =   68
               Top             =   1560
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Distrito"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   -66480
               TabIndex        =   67
               Top             =   960
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   -74880
               TabIndex        =   66
               Top             =   360
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "E-mail"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   -69120
               TabIndex        =   65
               Top             =   2160
               Width           =   525
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   18
               Left            =   -74880
               TabIndex        =   64
               Top             =   960
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   17
               Left            =   -71040
               TabIndex        =   63
               Top             =   2160
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   -74880
               TabIndex        =   62
               Top             =   2160
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   -67440
               TabIndex        =   61
               Top             =   1560
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   -70560
               TabIndex        =   60
               Top             =   1560
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   -74880
               TabIndex        =   59
               Top             =   1560
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Distrito"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   -66480
               TabIndex        =   58
               Top             =   960
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   -74880
               TabIndex        =   57
               Top             =   360
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "E-mail"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   6600
               TabIndex        =   56
               Top             =   1560
               Width           =   525
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   2040
               TabIndex        =   55
               Top             =   360
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   4440
               TabIndex        =   54
               Top             =   1560
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   120
               TabIndex        =   53
               Top             =   1560
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Comentarios"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -74760
               TabIndex        =   52
               Top             =   600
               Width           =   1050
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Distrito"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   8880
               TabIndex        =   51
               Top             =   360
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   120
               TabIndex        =   50
               Top             =   960
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   4440
               TabIndex        =   49
               Top             =   960
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   7680
               TabIndex        =   48
               Top             =   960
               Width           =   405
            End
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR79PROVEEDOR"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   1
            Tag             =   "Proveedor|Nombre Proveedor"
            Top             =   360
            Width           =   8400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Proveedor"
            Top             =   360
            Width           =   840
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3465
            Index           =   2
            Left            =   -74880
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   120
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19076
            _ExtentY        =   6112
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1320
            TabIndex        =   45
            Top             =   120
            Width           =   1590
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Prov."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   120
            TabIndex        =   44
            Tag             =   "C�d.|C�digo de Proveedor"
            Top             =   120
            Width           =   855
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Grupos Terap�uticos del Proveedor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3240
      Index           =   0
      Left            =   120
      TabIndex        =   37
      Top             =   4800
      Width           =   11655
      Begin VB.CommandButton cmdSelActivi 
         Caption         =   "Seleccionar Grupos Terap�uticos"
         Height          =   615
         Left            =   9840
         TabIndex        =   47
         Top             =   1320
         Width           =   1575
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2745
         Index           =   0
         Left            =   120
         TabIndex        =   38
         Top             =   360
         Width           =   9600
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16933
         _ExtentY        =   4842
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   36
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantProveedor (FR0026.FRM)                                *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: ENERO DE 1999                                                 *
'* DESCRIPCION: Mantenimiento de Proveedores                            *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdSelActivi_Click()
Dim mensaje As String

cmdSelActivi.Enabled = False

  If txtText1(0).Text <> "" Then
    Call objsecurity.LaunchProcess("FR0156")
    objWinInfo.DataRefresh
  Else
    mensaje = MsgBox("No hay ning�n Proveedor seleccionado", vbInformation, "Aviso")
  End If

cmdSelActivi.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Proveedor"
      
    .strTable = "FR7900"
    
    Call .FormAddOrderField("FR79CODPROVEEDOR", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Proveedor")

    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric, 3)
    Call .FormAddFilterWhere(strKey, "FR79PROVEEDOR", "Nombre Proveedor", cwString, 50)
    
    Call .FormAddFilterWhere(strKey, "FR79NIF", "NIF", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79DIRECCION1", "Direcci�n1", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DISTRITO1", "Distrito1", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79LOCALIDAD1", "Localidad1", cwString, 50)
    Call .FormAddFilterWhere(strKey, "FR79PROVINCIA1", "Provincia1", cwString, 20)
    Call .FormAddFilterWhere(strKey, "FR79PAIS1", "Pa�s1", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79TFNO1", "Tel�fono1", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79TFNO11", "2� Tel�fono1", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79FAX1", "Fax1", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79EMAIL1", "E-mail1", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79CODCONT", "C�digo Contable", cwNumeric, 4)
    Call .FormAddFilterWhere(strKey, "FR79CANTABC", "Clasificaci�n ABC por cantidad de productos", cwString, 1)
    Call .FormAddFilterWhere(strKey, "FR79DINABC", "Clasificaci�n ABC por cantidad de pesetas", cwString, 1)
    
    Call .FormAddFilterWhere(strKey, "FR79REPRESENT2", "Nombre Representante2", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DIRECCION2", "Direcci�n2", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DISTRITO2", "Distrito2", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79LOCALIDAD2", "Localidad2", cwString, 50)
    Call .FormAddFilterWhere(strKey, "FR79PROVINCIA2", "Provincia2", cwString, 20)
    Call .FormAddFilterWhere(strKey, "FR79PAIS2", "Pa�s2", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79TFNO2", "Tel�fono2", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79TFNO22", "2� Tel�fono2", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79FAX2", "Fax2", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79EMAIL2", "E-mail2", cwString, 30)
    
    Call .FormAddFilterWhere(strKey, "FR79REPRESENT3", "Nombre Representante3", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DIRECCION3", "Direcci�n3", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DISTRITO3", "Distrito3", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79LOCALIDAD3", "Localidad3", cwString, 50)
    Call .FormAddFilterWhere(strKey, "FR79PROVINCIA3", "Provincia3", cwString, 20)
    Call .FormAddFilterWhere(strKey, "FR79PAIS3", "Pa�s3", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79TFNO3", "Tel�fono3", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79TFNO33", "2� Tel�fono3", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79FAX3", "Fax3", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79EMAIL3", "E-mail3", cwString, 30)

    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Grupos Terap�uticos del Proveedor"
    .intAllowance = cwAllowDelete
    
    .strTable = "FRH300"
    
    Call .FormAddOrderField("FR00CODGRPTERAP", cwAscending)
    Call .FormAddRelation("FR79CODPROVEEDOR", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Grupos Terap�uticos del Proveedor")
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric, 3)
    Call .FormAddFilterWhere(strKey, "FR00CODGRPTERAP", "C�digo de Grupo Terap�utico", cwString, 10)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d.Proveedor", "FR79CODPROVEEDOR", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d. Grupo", "FR00CODGRPTERAP", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Desc. Grupo", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Padre", "", cwString, 10)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(31)).blnInFind = True
    .CtrlGetInfo(txtText1(32)).blnInFind = True
    .CtrlGetInfo(txtText1(33)).blnInFind = True
    
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(18)).blnInFind = True
    .CtrlGetInfo(txtText1(34)).blnInFind = True
    .CtrlGetInfo(txtText1(19)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(23)).blnInFind = True
    .CtrlGetInfo(txtText1(24)).blnInFind = True
    .CtrlGetInfo(txtText1(27)).blnInFind = True
    .CtrlGetInfo(txtText1(35)).blnInFind = True
    .CtrlGetInfo(txtText1(26)).blnInFind = True
    .CtrlGetInfo(txtText1(29)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR00CODGRPTERAP", "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(5), "FR00DESGRPTERAP")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(6), "FR00CODGRPTERAP_PAD")
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  grdDBGrid1(0).Columns(3).Width = 1200
  grdDBGrid1(0).Columns(4).Width = 1200
  grdDBGrid1(0).Columns(5).Width = 6000
  grdDBGrid1(0).Columns(6).Width = 1200
  grdDBGrid1(0).Columns(3).Visible = False

  grdDBGrid1(0).Columns(0).Visible = False

'Par que no salga con datos el grid de actividades
Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim dblPuntos As Double 'Para guardar los punto del proveedor (valoraci�n)
  Dim vntA As Variant
  
  'Se controla la puntuaci�n del proveedor no sea mayor que 10
  If txtText1(38).Text = "" Then
    dblPuntos = 0
  Else
    dblPuntos = txtText1(38).Text
  End If
  If (dblPuntos > 10) Or (dblPuntos < 0) Then
    Call objError.SetError(cwCodeMsg, "La puntuaci�n del proveedor debe estar entre 0 y 10", vntA)
    vntA = objError.Raise
    blnCancel = True
    Exit Sub
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim mensaje As String

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Actividad Proveedor" Then
    If txtText1(0).Text <> "" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Else
      mensaje = MsgBox("No hay ning�n Proveedor seleccionado", vbInformation, "Aviso")
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim mensaje As String

  If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Actividad Proveedor" Then
    If txtText1(0).Text <> "" Then
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Else
      mensaje = MsgBox("No hay ning�n Proveedor seleccionado", vbInformation, "Aviso")
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


