VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmRedHojAnest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Hoja de Anestesia"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   25
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   7455
      Index           =   1
      Left            =   240
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   480
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   13150
      _Version        =   327681
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DATOS GENERALES"
      TabPicture(0)   =   "FR0127.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "SUEROS"
      TabPicture(1)   =   "FR0127.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "MEDICAMENTOS"
      TabPicture(2)   =   "FR0127.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraFrame1(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "ESPECIFICOS"
      TabPicture(3)   =   "FR0127.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraFrame1(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "OTROS CONSUMOS"
      TabPicture(4)   =   "FR0127.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraFrame1(4)"
      Tab(4).Control(1)=   "cmdbuscargruprod"
      Tab(4).Control(2)=   "cmdbuscarprod"
      Tab(4).Control(3)=   "cmdinter"
      Tab(4).ControlCount=   4
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   4
         Left            =   -74880
         TabIndex        =   59
         Top             =   360
         Width           =   9975
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   4
            Left            =   120
            TabIndex        =   60
            Top             =   360
            Width           =   9705
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17119
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   495
         Left            =   -64800
         TabIndex        =   58
         Top             =   2400
         Width           =   1215
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Productos"
         Height          =   495
         Left            =   -64800
         TabIndex        =   57
         Top             =   3240
         Width           =   1215
      End
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   495
         Left            =   -64800
         TabIndex        =   56
         Top             =   4080
         Width           =   1215
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   3
         Left            =   -74880
         TabIndex        =   49
         Top             =   360
         Width           =   11175
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   3
            Left            =   120
            TabIndex        =   50
            Top             =   360
            Width           =   10905
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19235
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   1
         Left            =   -74760
         TabIndex        =   45
         Top             =   360
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   1
            Left            =   120
            TabIndex        =   46
            Top             =   360
            Width           =   10785
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19024
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   2
         Left            =   -74760
         TabIndex        =   36
         Top             =   360
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   2
            Left            =   120
            TabIndex        =   37
            Top             =   360
            Width           =   10785
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19024
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6975
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   360
         Width           =   11220
         Begin TabDlg.SSTab tabTab1 
            Height          =   6495
            Index           =   0
            Left            =   120
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   360
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   11456
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0127.frx":008C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(16)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(15)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(28)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(22)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(23)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(29)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(2)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "dtcDateCombo1(0)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "tab1"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(1)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(0)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(2)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(3)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(4)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(5)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cmdFirmar"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).ControlCount=   16
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0127.frx":00A8
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.CommandButton cmdFirmar 
               Caption         =   "Firmar"
               Height          =   495
               Left            =   6600
               TabIndex        =   52
               Top             =   1920
               Width           =   1215
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   7200
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Persona"
               Top             =   1440
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   4
               Left            =   6000
               TabIndex        =   5
               Tag             =   "Firma Digital"
               Top             =   1440
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR43DESINTERVENCION"
               Height          =   810
               Index           =   3
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   1  'Horizontal
               TabIndex        =   4
               Tag             =   "Descripci�n Intervenci�n"
               Top             =   2280
               Width           =   4815
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR43DURACCIRUGIA"
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   3
               Tag             =   "Duraci�n Cirug�a"
               Top             =   1680
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR43CODHOJAANEST"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   0
               Tag             =   "C�d. Hoja Anestesia"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR43HORAINTERVEN"
               Height          =   330
               Index           =   1
               Left            =   2400
               TabIndex        =   2
               Tag             =   "Hora Intervenci�n"
               Top             =   1080
               Width           =   612
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5865
               Index           =   0
               Left            =   -74760
               TabIndex        =   30
               TabStop         =   0   'False
               Top             =   240
               Width           =   10215
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18018
               _ExtentY        =   10345
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin TabDlg.SSTab tab1 
               Height          =   3015
               Left            =   120
               TabIndex        =   31
               TabStop         =   0   'False
               Top             =   3360
               Width           =   10215
               _ExtentX        =   18018
               _ExtentY        =   5318
               _Version        =   327681
               Tabs            =   4
               TabsPerRow      =   4
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Paciente"
               TabPicture(0)   =   "FR0127.frx":00C4
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(11)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(4)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(7)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(8)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(9)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(5)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "cboDBCombo1(1)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "txtText1(7)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "txtText1(8)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtText1(6)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtText1(9)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtText1(10)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "cmdHistClin"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "cmdPerfFTP"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).ControlCount=   14
               TabCaption(1)   =   "Equipo Anestesia"
               TabPicture(1)   =   "FR0127.frx":00E0
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "lblLabel1(20)"
               Tab(1).Control(1)=   "lblLabel1(0)"
               Tab(1).Control(2)=   "lblLabel1(3)"
               Tab(1).Control(3)=   "txtText1(11)"
               Tab(1).Control(4)=   "txtText1(12)"
               Tab(1).Control(5)=   "txtText1(13)"
               Tab(1).Control(6)=   "txtText1(14)"
               Tab(1).Control(7)=   "txtText1(15)"
               Tab(1).Control(8)=   "txtText1(16)"
               Tab(1).ControlCount=   9
               TabCaption(2)   =   "Servicio"
               TabPicture(2)   =   "FR0127.frx":00FC
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "lblLabel1(1)"
               Tab(2).Control(0).Enabled=   0   'False
               Tab(2).Control(1)=   "txtText1(18)"
               Tab(2).Control(1).Enabled=   0   'False
               Tab(2).Control(2)=   "txtText1(17)"
               Tab(2).Control(2).Enabled=   0   'False
               Tab(2).ControlCount=   3
               TabCaption(3)   =   "Indicadores Anestesia"
               TabPicture(3)   =   "FR0127.frx":0118
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "chkCheck1(0)"
               Tab(3).Control(1)=   "chkCheck1(1)"
               Tab(3).Control(2)=   "chkCheck1(2)"
               Tab(3).Control(3)=   "chkCheck1(3)"
               Tab(3).Control(4)=   "chkCheck1(4)"
               Tab(3).ControlCount=   5
               Begin VB.CommandButton cmdPerfFTP 
                  Caption         =   "Perfil FTP"
                  Height          =   495
                  Left            =   4800
                  TabIndex        =   55
                  Top             =   2280
                  Width           =   1215
               End
               Begin VB.CommandButton cmdHistClin 
                  Caption         =   "Historia Cl�nica"
                  Height          =   495
                  Left            =   2280
                  TabIndex        =   54
                  Top             =   2280
                  Width           =   1215
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Anestesia para Trasplante Hepatico"
                  DataField       =   "FR43INDTRANSPHEPA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   -74520
                  TabIndex        =   24
                  Tag             =   "Anestesia para Trasplante Hepatico"
                  Top             =   2100
                  Width           =   4215
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Anestesia para CEC"
                  DataField       =   "FR43INDCEC"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -74520
                  TabIndex        =   23
                  Tag             =   "Anestesia para CEC"
                  Top             =   1740
                  Width           =   4215
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Analgosedaci�n y Anestesia Locorregional"
                  DataField       =   "FR43INDANALGLOCOR"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   -74520
                  TabIndex        =   22
                  Tag             =   "Analgosedaci�n y Anestesia Locorregional"
                  Top             =   1380
                  Width           =   4215
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Anestesia General Pedi�trica"
                  DataField       =   "FR43INDANESTGENPED"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   -74520
                  TabIndex        =   21
                  Tag             =   "Anestesia General Pedi�trica"
                  Top             =   1020
                  Width           =   4215
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Anestesia General Adulto"
                  DataField       =   "FR43INDANESTGENADUL"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   -74520
                  TabIndex        =   20
                  Tag             =   "Anestesia General Adulto"
                  Top             =   660
                  Width           =   4215
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "AD02CODDPTO"
                  Height          =   330
                  Index           =   17
                  Left            =   -74280
                  TabIndex        =   18
                  Tag             =   "C�d.Servicio"
                  Top             =   1320
                  Width           =   1035
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   18
                  Left            =   -72960
                  TabIndex        =   19
                  TabStop         =   0   'False
                  Tag             =   "Desc.Servicio"
                  Top             =   1320
                  Width           =   5445
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   16
                  Left            =   -73440
                  TabIndex        =   17
                  Tag             =   "Apellido Cirujano"
                  Top             =   2220
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_CIR"
                  Height          =   330
                  Index           =   15
                  Left            =   -74640
                  TabIndex        =   16
                  Tag             =   "C�digo Cirujano"
                  Top             =   2220
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   14
                  Left            =   -73440
                  TabIndex        =   15
                  Tag             =   "Apellido Enfermera"
                  Top             =   1500
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ENF"
                  Height          =   330
                  Index           =   13
                  Left            =   -74640
                  TabIndex        =   14
                  Tag             =   "C�digo Enfermera"
                  Top             =   1500
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   10
                  Left            =   6000
                  TabIndex        =   11
                  TabStop         =   0   'False
                  Tag             =   "Apellido 2� Paciente"
                  Top             =   1620
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   9
                  Left            =   3000
                  TabIndex        =   10
                  TabStop         =   0   'False
                  Tag             =   "Apellido 1� Paciente"
                  Top             =   1620
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "CI21CODPERSONA"
                  Height          =   330
                  Index           =   6
                  Left            =   360
                  TabIndex        =   7
                  Tag             =   "C�digo Paciente"
                  Top             =   900
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   8
                  Left            =   360
                  TabIndex        =   9
                  TabStop         =   0   'False
                  Tag             =   "Nombre Paciente"
                  Top             =   1620
                  Width           =   2565
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   7
                  Left            =   2400
                  TabIndex        =   8
                  TabStop         =   0   'False
                  Tag             =   "C�d. Historia Paciente"
                  Top             =   900
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   12
                  Left            =   -73440
                  TabIndex        =   13
                  Tag             =   "Apellido Anestesista"
                  Top             =   780
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ANE"
                  Height          =   330
                  Index           =   11
                  Left            =   -74640
                  TabIndex        =   12
                  Tag             =   "C�digo Anestesista"
                  Top             =   780
                  Width           =   1155
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "FRG6CODTIPPERS"
                  Height          =   330
                  Index           =   1
                  Left            =   5310
                  TabIndex        =   62
                  Tag             =   "C�digo Tipo Paciente"
                  Top             =   840
                  Width           =   1005
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0127.frx":0134
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0127.frx":0150
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1773
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 0"
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tipo Paciente"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   5
                  Left            =   5280
                  TabIndex        =   63
                  Top             =   600
                  Width           =   1200
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Servicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   -74280
                  TabIndex        =   53
                  Top             =   1080
                  Width           =   2055
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cirujano"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -74640
                  TabIndex        =   44
                  Top             =   1980
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Enfermera"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   -74640
                  TabIndex        =   43
                  Top             =   1260
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 2�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   6000
                  TabIndex        =   42
                  Top             =   1380
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 1�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   3000
                  TabIndex        =   41
                  Top             =   1380
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   360
                  TabIndex        =   40
                  Top             =   1380
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Persona"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   360
                  TabIndex        =   39
                  Top             =   660
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Historia "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   2400
                  TabIndex        =   38
                  Top             =   660
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Anestesista"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   20
                  Left            =   -74640
                  TabIndex        =   32
                  Top             =   540
                  Width           =   1095
               End
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR43FECINTERVEN"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   1
               Tag             =   "Fecha Intervenci�n"
               Top             =   1080
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "HORAS"
               Height          =   195
               Index           =   2
               Left            =   1320
               TabIndex        =   61
               Top             =   1735
               Width           =   570
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Firma Digital"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   6000
               TabIndex        =   51
               Top             =   1200
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Duraci�n Cirug�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   120
               TabIndex        =   48
               Top             =   1440
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   120
               TabIndex        =   47
               Top             =   2040
               Width           =   2145
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hoja Anestesia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   120
               TabIndex        =   35
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   120
               TabIndex        =   34
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   2400
               TabIndex        =   33
               Top             =   840
               Width           =   1575
            End
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRedHojAnest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedHojAnest (FR0127.FRM)                                  *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Redactar Hoja Anestesia                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscargruprod.Enabled = False

  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    Call objsecurity.LaunchProcess("FR0120")
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(4).Rows > 0 Then
          grdDBGrid1(4).MoveFirst
          For i = 0 To grdDBGrid1(4).Rows - 1
            If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(4).MoveNext
          Next i
        End If
        If grdDBGrid1(1).Rows > 0 And noinsertar = True Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(2).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If grdDBGrid1(2).Rows > 0 And noinsertar = True Then
          grdDBGrid1(2).MoveFirst
          For i = 0 To grdDBGrid1(2).Rows - 1
            If grdDBGrid1(2).Columns(5).Value = gintprodbuscado(v, 0) Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(2).MoveNext
          Next i
        End If
        If grdDBGrid1(3).Rows > 0 And noinsertar = True Then
          grdDBGrid1(3).MoveFirst
          For i = 0 To grdDBGrid1(3).Rows - 1
            If grdDBGrid1(3).Columns(5).Value = gintprodbuscado(v, 0) Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(3).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(4).Columns(7).Value = gintprodbuscado(v, 2)
          stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta(0).Value)
          rsta.Close
          Set rsta = Nothing
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If

cmdbuscargruprod.Enabled = True

End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscarprod.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  Call objsecurity.LaunchProcess("FR0119")
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(4).Rows > 0 Then
        grdDBGrid1(4).MoveFirst
        For i = 0 To grdDBGrid1(3).Rows - 1
          If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
             And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(4).MoveNext
        Next i
      End If
      If grdDBGrid1(1).Rows > 0 And noinsertar = True Then
        grdDBGrid1(1).MoveFirst
        For i = 0 To grdDBGrid1(1).Rows - 1
          If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(1).MoveNext
        Next i
      End If
      If grdDBGrid1(2).Rows > 0 And noinsertar = True Then
        grdDBGrid1(2).MoveFirst
        For i = 0 To grdDBGrid1(2).Rows - 1
          If grdDBGrid1(2).Columns(5).Value = gintprodbuscado(v, 0) Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(2).MoveNext
        Next i
      End If
      If grdDBGrid1(3).Rows > 0 And noinsertar = True Then
        grdDBGrid1(3).MoveFirst
        For i = 0 To grdDBGrid1(3).Rows - 1
          If grdDBGrid1(3).Columns(5).Value = gintprodbuscado(v, 0) Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(3).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(7).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta(0).Value)
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdbuscarprod.Enabled = True

End Sub

Private Sub cmdfirmar_Click()
Dim rstcantidad As rdoResultset
Dim strcantidad As String
Dim strInsert As String
Dim rstSelect As rdoResultset
Dim strSelect As String
Dim persona, producto, fecha, hora, servicio, cantreceta, indquirofano

  If txtText1(4).Text = "" Then
    
    strcantidad = "SELECT MAX(FR17CANTIDAD) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
               txtText1(0).Text
    Set rstcantidad = objApp.rdoConnect.OpenResultset(strcantidad)
    If IsNull(rstcantidad(0).Value) Then
      MsgBox "No se puede firmar, ya que no ha rellenado la lista de productos."
    Else
      If rstcantidad(0).Value = 0 Then
        MsgBox "No se puede firmar, ya que no ha rellenado la lista de productos."
      Else
        strSelect = "SELECT FR73CODPRODUCTO,FR17CANTIDAD FROM FR1700 WHERE FR43CODHOJAANEST=" & _
               txtText1(0).Text & " AND FR17CANTIDAD>0 "
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        While Not rstSelect.EOF
          persona = txtText1(6).Text
          producto = rstSelect("FR73CODPRODUCTO").Value
          fecha = "TO_DATE('" & dtcDateCombo1(0) & "','DD/MM/YYYY')"
          If InStr(txtText1(1).Text, ",") > 0 Then
            hora = Left(txtText1(1).Text, InStr(txtText1(1).Text, ",") - 1) & "." & Right(txtText1(1).Text, Len(txtText1(1).Text) - InStr(txtText1(1).Text, ","))
          Else
            hora = txtText1(1).Text
          End If
          servicio = txtText1(17).Text
          cantreceta = rstSelect("FR17CANTIDAD").Value
          indquirofano = -1
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)" & _
                      " VALUES (" & persona & "," & producto & "," & fecha & "," & hora & "," & servicio & "," & cantreceta & "," & indquirofano & ")"
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          rstSelect.MoveNext
        Wend
        rstSelect.Close
        Set rstSelect = Nothing
        
        strInsert = ""
        If chkCheck1(0).Value = True Then
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
          strInsert = strInsert & " SELECT " & persona & "," _
                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
                      " FROM FR4300,FR5200 WHERE FR4300.FRG6CODTIPPERS=FR5200.FRG6CODTIPPERS AND FR43INDANESTGENADUL<>0 "
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        If chkCheck1(1).Value = True Then
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
          strInsert = strInsert & " SELECT " & persona & "," _
                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
                      " FROM FR4300,FR5200 WHERE FR4300.FRG6CODTIPPERS=FR5200.FRG6CODTIPPERS AND FR43INDANESTGENPED<>0 "
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        If chkCheck1(2).Value = True Then
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
          strInsert = strInsert & " SELECT " & persona & "," _
                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
                      " FROM FR4300,FR5200 WHERE FR4300.FRG6CODTIPPERS=FR5200.FRG6CODTIPPERS AND FR43INDANALGLOCOR<>0 "
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        If chkCheck1(3).Value = True Then
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
          strInsert = strInsert & " SELECT " & persona & "," _
                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
                      " FROM FR4300,FR5200 WHERE FR4300.FRG6CODTIPPERS=FR5200.FRG6CODTIPPERS AND FR43INDCEC<>0 "
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        If chkCheck1(4).Value = True Then
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
          strInsert = strInsert & " SELECT " & persona & "," _
                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
                      " FROM FR4300,FR5200 WHERE FR4300.FRG6CODTIPPERS=FR5200.FRG6CODTIPPERS AND FR43INDTRANSPHEPA<>0 "
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        
        txtText1(4).SetFocus
        Call objWinInfo.CtrlSet(txtText1(4), objsecurity.strUser)
        Call objWinInfo.DataSave
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Call objWinInfo.DataRefresh
      End If
    End If
    rstcantidad.Close
    Set rstcantidad = Nothing
  Else
    MsgBox "La Hoja de Quir�fano ya esta firmada."
  End If

End Sub

Private Sub cmdHistClin_Click()

cmdHistClin.Enabled = False

cmdHistClin.Enabled = True

End Sub

Private Sub cmdinter_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim rstprin As rdoResultset
Dim strprin As String
Dim i As Integer

cmdinter.Enabled = False
        
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(4).MoveFirst
  For i = 0 To grdDBGrid1(4).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(4).Columns(5).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(4).Columns(5).Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(4).MoveNext
  Next i
  
  If gstrLista <> "" Then
    gstrLista = "(" & gstrLista & ")"
    Call objsecurity.LaunchProcess("FR0116")
  End If

cmdinter.Enabled = True

End Sub

Private Sub cmdPerfFTP_Click()
Dim mensaje As String

cmdPerfFTP.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(6).Text <> "" Then
      glngPaciente = txtText1(6).Text
      Call objsecurity.LaunchProcess("FR0106")
    Else
      mensaje = MsgBox("No hay ning�n paciente.", vbInformation, "Aviso")
    End If
  End If

cmdPerfFTP.Enabled = True


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim objMultiInfo3 As New clsCWForm
  Dim objMultiInfo4 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Hoja Anestesia"
    
    .strTable = "FR4300"
    
    Call .FormAddOrderField("FR43CODHOJAANEST", cwAscending)
   
'vntdatos(1) = "frmFirmarHojAnest" 'gstrllamador
'vntdatos(2) = frmFirmarHojAnest.grdDBGrid1(0).Columns(3).Value 'gintcodhojaanest
  
    If gstrllamador = "frmFirmarHojAnest" Then
      .strWhere = "FR43CODHOJAANEST=" & gintcodhojaanest & " AND SG02COD IS NULL " 'y NO FIRMADA
      .intAllowance = cwAllowModify
    Else
      .strWhere = "SG02COD IS NULL "
    End If
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Hoja Anestesia")
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43FECINTERVEN", "Fecha Intervenci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR43HORAINTERVEN", "Hora Intervenci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR43DESINTERVENCION", "Descripci�n Intervenci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR43DURACCIRUGIA", "Duraci�n Cirug�a", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ANE", "Anestesista", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_CIR", "Cirujano", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD", "Firma Digital", cwString)
    Call .FormAddFilterWhere(strKey, "FR43INDANESTGENADUL", "Ind. Anest. General Adulto", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDANESTGENPED", "Ind. Anest. General Pedi�trica", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDANALGLOCOR", "Ind. Analgosedaci�n y Anest. Locorregional", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDCEC", "Ind. Anest. para CEC", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDTRANSPHEPA", "Ind. Anest. para Transplante Hep�tico", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia")

  End With
  
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Sueros"
    .strTable = "FR1700"
    .intAllowance = cwAllowModify

    Call .FormAddOrderField("FR17NUMLINEA", cwAscending)

    Call .FormAddRelation("FR43CODHOJAANEST", txtText1(0))

    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300 WHERE " & _
                " FR73INDANESTGENADU=0 " & _
                " AND FR73INDANESTGENPED=0" & _
                " AND FR73INDANESTCEC=0" & _
                " AND FR73INDANESTTRASHEP=0" & _
                " AND FR73INDANALGSEDACION=0" & _
                " AND FR73INDANESTSUERO<>0" & _
                " AND FR73INDANESTMED=0" & _
                " AND FR73INDANESTESPEC=0 )"

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Sueros")
    Call .FormAddFilterWhere(strKey, "FR17NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR17CANTIDAD", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR17INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDESTHA", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR17NUMLINEA", "N�mero de Linea")

  End With
  
  With objMultiInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Medicamento"
    .strTable = "FR1700"
    .intAllowance = cwAllowModify

    Call .FormAddOrderField("FR17NUMLINEA", cwAscending)

    Call .FormAddRelation("FR43CODHOJAANEST", txtText1(0))

    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300 WHERE " & _
                " FR73INDANESTGENADU=0 " & _
                " AND FR73INDANESTGENPED=0" & _
                " AND FR73INDANESTCEC=0" & _
                " AND FR73INDANESTTRASHEP=0" & _
                " AND FR73INDANALGSEDACION=0" & _
                " AND FR73INDANESTSUERO=0" & _
                " AND FR73INDANESTMED<>0" & _
                " AND FR73INDANESTESPEC=0 )"

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamento")
    Call .FormAddFilterWhere(strKey, "FR17NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR17CANTIDAD", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR17INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDESTHA", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR17NUMLINEA", "N�mero de Linea")

  End With
  
  With objMultiInfo3
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Especifico"
    .strTable = "FR1700"
    .intAllowance = cwAllowModify

    Call .FormAddOrderField("FR17NUMLINEA", cwAscending)

    Call .FormAddRelation("FR43CODHOJAANEST", txtText1(0))

    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300 WHERE " & _
                " FR73INDANESTGENADU=0 " & _
                " AND FR73INDANESTGENPED=0" & _
                " AND FR73INDANESTCEC=0" & _
                " AND FR73INDANESTTRASHEP=0" & _
                " AND FR73INDANALGSEDACION=0" & _
                " AND FR73INDANESTSUERO=0" & _
                " AND FR73INDANESTMED=0" & _
                " AND FR73INDANESTESPEC<>0 )"

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Especifico")
    Call .FormAddFilterWhere(strKey, "FR17NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR17CANTIDAD", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR17INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDESTHA", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR17NUMLINEA", "N�mero de Linea")

  End With
  
  With objMultiInfo4
    Set .objFormContainer = fraFrame1(4)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(4)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Otros Productos"

    .strTable = "FR1700"
    .intAllowance = cwAllowModify + cwAllowDelete

    Call .FormAddOrderField("FR17NUMLINEA", cwAscending)

    Call .FormAddRelation("FR43CODHOJAANEST", txtText1(0))

    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300 WHERE " & _
                " FR73INDANESTGENADU=0 " & _
                " AND FR73INDANESTGENPED=0" & _
                " AND FR73INDANESTCEC=0" & _
                " AND FR73INDANESTTRASHEP=0" & _
                " AND FR73INDANALGSEDACION=0" & _
                " AND FR73INDANESTSUERO=0" & _
                " AND FR73INDANESTMED=0" & _
                " AND FR73INDANESTESPEC=0 )"

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Otros Productos")
    Call .FormAddFilterWhere(strKey, "FR17NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR17CANTIDAD", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR17INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDESTHA", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR17NUMLINEA", "N�mero de Linea")

  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo3, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo4, cwFormMultiLine)

    Call .GridAddColumn(objMultiInfo1, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo1, "Hoja Quir�fano", "FR43CODHOJAANEST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "Prod.", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo1, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo1, "Cant.Consumida", "FR17CANTIDAD", cwDecimal)
    Call .GridAddColumn(objMultiInfo1, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo1, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo1, "Leida?", "FR17INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo1, "Bloqueada?", "FR17INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo1, "Estado Linea?", "FR17INDESTHA", cwBoolean)
    
    Call .GridAddColumn(objMultiInfo2, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo2, "Hoja Quir�fano", "FR43CODHOJAANEST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo2, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo2, "Prod.", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo2, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo2, "Cant.Consumida", "FR17CANTIDAD", cwDecimal)
    Call .GridAddColumn(objMultiInfo2, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo2, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo2, "Leida?", "FR17INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Bloqueada?", "FR17INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Estado Linea?", "FR17INDESTHA", cwBoolean)

    Call .GridAddColumn(objMultiInfo3, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo3, "Hoja Quir�fano", "FR43CODHOJAANEST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo3, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo3, "Prod.", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo3, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo3, "Cant.Consumida", "FR17CANTIDAD", cwDecimal)
    Call .GridAddColumn(objMultiInfo3, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo3, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo3, "Leida?", "FR17INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Bloqueada?", "FR17INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Estado Linea?", "FR17INDESTHA", cwBoolean)

    Call .GridAddColumn(objMultiInfo4, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo4, "Hoja Quir�fano", "FR43CODHOJAANEST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo4, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo4, "Prod.", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo4, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo4, "Cant.Consumida", "FR17CANTIDAD", cwDecimal)
    Call .GridAddColumn(objMultiInfo4, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo4, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo4, "Leida?", "FR17INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Bloqueada?", "FR17INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Estado Linea?", "FR17INDESTHA", cwBoolean)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(3).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(3).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(4).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(4).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo1)
    Call .FormChangeColor(objMultiInfo2)
    Call .FormChangeColor(objMultiInfo3)
    Call .FormChangeColor(objMultiInfo4)
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(8), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(9), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(10), "CI22SEGAPEL")
    .CtrlGetInfo(txtText1(6)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "SG02COD_ANE", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(12), "SG02APE1")
    .CtrlGetInfo(txtText1(11)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "AD02CODDPTO_ENF", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(14), "AD02DESDPTO")
    .CtrlGetInfo(txtText1(13)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "SG02COD_CIR", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(16), "SG02APE1")
    .CtrlGetInfo(txtText1(15)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(17)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(17)), txtText1(18), "AD02DESDPTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(17)), txtText1(19), "AD02CODDPTO")
    .CtrlGetInfo(txtText1(17)).blnForeign = True
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(20), "AD02DESDPTO")
    '.CtrlGetInfo(txtText1(19)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "SG02COD", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "SG02APE1")
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73DESPRODUCTO")
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(7), "FR73DESPRODUCTO")
    .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(9)), grdDBGrid1(2).Columns(10), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(2).Columns(9)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(7), "FR73DESPRODUCTO")
    .CtrlGetInfo(grdDBGrid1(3).Columns(5)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(9)), grdDBGrid1(3).Columns(10), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(3).Columns(9)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(7), "FR73DESPRODUCTO")
    .CtrlGetInfo(grdDBGrid1(4).Columns(5)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(9)), grdDBGrid1(4).Columns(10), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(4).Columns(9)).blnReadOnly = True
     
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(18)).blnInFind = True
    '.CtrlGetInfo(txtText1(19)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT FRG6CODTIPPERS,FRG6DESTIPPERS FROM FRG600 ORDER BY FRG6CODTIPPERS"
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

grdDBGrid1(1).Columns(7).Width = 4545
grdDBGrid1(1).Columns(8).Width = 1335
grdDBGrid1(1).Columns(9).Width = 1335
grdDBGrid1(1).Columns(10).Width = 2685

grdDBGrid1(2).Columns(7).Width = 4545
grdDBGrid1(2).Columns(8).Width = 1335
grdDBGrid1(2).Columns(9).Width = 1335
grdDBGrid1(2).Columns(10).Width = 2685

grdDBGrid1(3).Columns(7).Width = 4545
grdDBGrid1(3).Columns(8).Width = 1335
grdDBGrid1(3).Columns(9).Width = 1335
grdDBGrid1(3).Columns(10).Width = 2685

grdDBGrid1(4).Columns(7).Width = 4545
grdDBGrid1(4).Columns(8).Width = 1335
grdDBGrid1(4).Columns(9).Width = 1335
grdDBGrid1(4).Columns(10).Width = 2685

grdDBGrid1(1).Columns(4).Visible = False
grdDBGrid1(2).Columns(4).Visible = False
grdDBGrid1(3).Columns(4).Visible = False
grdDBGrid1(4).Columns(4).Visible = False

grdDBGrid1(1).Columns(11).Visible = False
grdDBGrid1(1).Columns(12).Visible = False
grdDBGrid1(1).Columns(13).Visible = False
grdDBGrid1(2).Columns(11).Visible = False
grdDBGrid1(2).Columns(12).Visible = False
grdDBGrid1(2).Columns(13).Visible = False
grdDBGrid1(3).Columns(11).Visible = False
grdDBGrid1(3).Columns(12).Visible = False
grdDBGrid1(3).Columns(13).Visible = False
grdDBGrid1(4).Columns(11).Visible = False
grdDBGrid1(4).Columns(12).Visible = False
grdDBGrid1(4).Columns(13).Visible = False

grdDBGrid1(1).RemoveAll
grdDBGrid1(1).Refresh
grdDBGrid1(2).RemoveAll
grdDBGrid1(2).Refresh
grdDBGrid1(3).RemoveAll
grdDBGrid1(3).Refresh
grdDBGrid1(4).RemoveAll
grdDBGrid1(4).Refresh

    If gstrllamador = "frmFirmarHojAnest" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    End If

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "CI2200"
      
      Set objField = .AddField("CI21CODPERSONA")
      objField.strSmallDesc = "C�digo Paciente"
      
      Set objField = .AddField("CI22NUMHISTORIA")
      objField.strSmallDesc = "Historia"
      
      Set objField = .AddField("CI22NOMBRE")
      objField.strSmallDesc = "Nombre"
      
      Set objField = .AddField("CI22PRIAPEL")
      objField.strSmallDesc = "Apellido 1�"
      
      Set objField = .AddField("CI22SEGAPEL")
      objField.strSmallDesc = "Apellido 2�"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(6), .cllValues("CI21CODPERSONA"))
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(11)" Or strCtrl = "txtText1(13)" Or _
     strCtrl = "txtText1(15)" Or strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "SG0200"
      
      Set objField = .AddField("SG02COD")
      objField.strSmallDesc = "C�digo Usuario"
      
      Set objField = .AddField("SG02APE1")
      objField.strSmallDesc = "Primer Apellido"
      
      If .Search Then
        Select Case strCtrl
          Case "txtText1(11)"
            Call objWinInfo.CtrlSet(txtText1(11), .cllValues("SG02COD"))
          Case "txtText1(13)"
            Call objWinInfo.CtrlSet(txtText1(13), .cllValues("SG02COD"))
          Case "txtText1(15)"
            Call objWinInfo.CtrlSet(txtText1(15), .cllValues("SG02COD"))
          Case "txtText1(4)"
            Call objWinInfo.CtrlSet(txtText1(4), .cllValues("SG02COD"))
        End Select
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(17)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
     .strWhere = " WHERE AD32CODTIPODPTO = 3" & _
                 " AND AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
                 
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo Servicio"
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n Servicio"
      
      'Set objField = .AddField("AD02CODDPTO")
      'objField.strSmallDesc = "C�digo Departamento"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(17), .cllValues("AD02CODDPTO"))
      End If
    End With
    Set objSearch = Nothing
  End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

  If txtText1(4).Text <> "" Then
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        cmdbuscargruprod.Enabled = False
        cmdbuscarprod.Enabled = False
        cmdinter.Enabled = False
        
        Select Case strFormName
        Case "Hoja Anestesia"
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Case "Sueros"
          Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
        Case "Medicamento"
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        Case "Especifico"
          Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        Case "Otros Productos"
          Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
        End Select
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    If gstrllamador = "frmFirmarHojAnest" Then
      objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Else
      objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
    cmdbuscargruprod.Enabled = True
    cmdbuscarprod.Enabled = True
    cmdinter.Enabled = True
    
    Select Case strFormName
    Case "Hoja Anestesia"
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    Case "Sueros"
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
    Case "Medicamento"
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
    Case "Especifico"
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
    Case "Otros Productos"
      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
    End Select
  End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta As rdoResultset
Dim sqlstr, strInsert As String
Dim cuenta, numlinea As Integer

  If blnError = False And strFormName = "Hoja Anestesia" Then
    sqlstr = "SELECT COUNT(FR43CODHOJAANEST) FROM FR1700 WHERE FR43CODHOJAANEST=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    cuenta = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    
    If cuenta = 0 Then
      'A�adir Sueros
      numlinea = 1
      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDANESTSUERO<>0"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        
        'insertar linea
        strInsert = "INSERT INTO FR1700 (FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD)" & _
                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & rsta.rdoColumns(1).Value & ",0)"
        objApp.rdoConnect.Execute strInsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        numlinea = numlinea + 1
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      
      'A�adir Medicamentos
      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDANESTMED<>0"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        
        'insertar linea
        strInsert = "INSERT INTO FR1700 (FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD)" & _
                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & rsta.rdoColumns(1).Value & ",0)"
        objApp.rdoConnect.Execute strInsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        numlinea = numlinea + 1
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      
      'A�adir Especificos
      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDANESTESPEC<>0"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        
        'insertar linea
        strInsert = "INSERT INTO FR1700 (FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD)" & _
                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & rsta.rdoColumns(1).Value & ",0)"
        objApp.rdoConnect.Execute strInsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        numlinea = numlinea + 1
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    End If
  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
'
'
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabGeneral1_Click(Index As Integer, PreviousTab As Integer)
    
  Select Case tabGeneral1(1).Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 1
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 2
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 3
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 4
      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
  End Select

End Sub

Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
   tab1.Tab = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Hoja Anestesia" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR43CODHOJAANEST_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    txtText1(0).SetFocus
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
    rsta.Close
    Set rsta = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Otros Productos" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(4).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(4).Rows
        End If
        grdDBGrid1(4).Columns(3).Value = linea
        SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de anestesia", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  If intIndex = 10 Then 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   tab1.Tab = 1
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


