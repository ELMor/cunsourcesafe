VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmActCGCF 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Actualizar CGCF"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   ForeColor       =   &H00808080&
   HelpContextID   =   30001
   Icon            =   "FR0039.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdActPrinActi 
      Caption         =   "ACTUALIZAR PRINCIPIOS ACTIVOS"
      Height          =   855
      Left            =   7200
      TabIndex        =   9
      Top             =   4920
      Width           =   3255
   End
   Begin VB.CommandButton cmdActGrpTer 
      Caption         =   "ACTUALIZAR GRUPOS TERAPEUTICOS"
      Height          =   855
      Left            =   7200
      TabIndex        =   8
      Top             =   3840
      Width           =   3255
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   6960
      TabIndex        =   7
      Top             =   7560
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.Frame Frame1 
      Caption         =   "TABLAS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7935
      Left            =   480
      TabIndex        =   5
      Top             =   360
      Width           =   6255
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   7455
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   6030
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   3
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         BalloonHelp     =   0   'False
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3281
         Columns(0).Caption=   "Tabla FoxPro"
         Columns(0).Name =   "Tabla FoxPro"
         Columns(0).Alignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(1).Width=   3281
         Columns(1).Caption=   "Tabla Oracle"
         Columns(1).Name =   "Tabla Oracle"
         Columns(1).Alignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(2).Width=   2275
         Columns(2).Caption=   "Actualizada"
         Columns(2).Name =   "Actualizada"
         Columns(2).Alignment=   2
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   11
         Columns(2).FieldLen=   256
         Columns(2).Style=   2
         Columns(2).HasHeadForeColor=   -1  'True
         _ExtentX        =   10636
         _ExtentY        =   13150
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdElegir 
      Caption         =   "ELEGIR ODBC FOXPRO"
      Height          =   855
      Left            =   7200
      TabIndex        =   4
      Top             =   600
      Width           =   3255
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "SALIR"
      Height          =   855
      Left            =   7200
      TabIndex        =   3
      Top             =   6000
      Width           =   3255
   End
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "ACTUALIZAR FOXPRO DESDE ORACLE"
      Height          =   855
      Left            =   7200
      TabIndex        =   2
      Top             =   2760
      Width           =   3255
   End
   Begin VB.Data DataFoxPro1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   495
      Left            =   8160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   8040
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   6960
      TabIndex        =   1
      Top             =   6960
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.CommandButton cdmActualizar 
      Caption         =   "ACTUALIZAR ORACLE DESDE FOXPRO"
      Height          =   855
      Left            =   7200
      TabIndex        =   0
      Top             =   1680
      Width           =   3255
   End
End
Attribute VB_Name = "frmActCGCF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmActCGCF (FR0039.FRM)                                      *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Actualizar el CGCF                                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************
'*                                                                      *
'* NOTA:                                                                *
'* =====                                                                *
'* Esta probado con el ODBC : Microsoft FoxPro Driver (*.dbf)           *
'* Ademas he utilizado el control DATA porque es el unico               *
'* que funciona con los campos Memos                                    *
'*                                                                      *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim Env1 As rdoEnvironment
Dim Con1 As rdoConnection
Dim env2 As rdoEnvironment
Dim Con2 As rdoConnection



Private Function campoStr(ByVal cadena As String, ByVal posicion As Long, ByVal separador As String) As String
Dim pos As Integer
Dim posicionaux As Long
Dim aux1 As String
Dim aux2 As String

  posicionaux = posicion
  aux1 = cadena
  pos = InStr(aux1, separador)
  If pos = 0 Then
    campoStr = ""
  Else
    aux2 = Left(aux1, pos - 1)
    aux1 = Right(aux1, (Len(aux1) - pos))
    While posicionaux <> 0 And pos <> 0
      
      pos = InStr(aux1, separador)
      If pos <> 0 Then
        aux2 = Left(aux1, pos - 1)
        aux1 = Right(aux1, (Len(aux1) - pos))
        posicionaux = posicionaux - 1
      End If
    Wend
    If posicionaux = 0 Then
      campoStr = aux2
    Else
      If posicionaux = 1 Then
        campoStr = aux1
      Else
        campoStr = ""
      End If
    End If
  
  End If

End Function

Private Sub cdmActualizar_Click()
Dim rstOrclTab As rdoResultset
Dim rstOrclCol As rdoResultset
Dim sqlstrOrcl As String
Dim strInsertOrcl As String
Dim strDeleteOrcl As String
Dim NomColsOrcl As String
Dim NomColsFox As String
Dim TipoColsOrcl As String
Dim TipoColsFox As String
Dim numColumnas As Long
Dim FechaAux As String
Dim i As Long
Dim auxDirBDFox As String
Dim FilaGrid As Integer
Dim Filaux As Integer
Dim erreninsert As Boolean
Dim Nfil1 As Integer
Dim FileOut As String
Dim strLongRaw As String


  If Text1.Text = "" Then
    MsgBox "Debe elegir el ODBC de acceso de la BD FOXPRO 2.6"
    Exit Sub
  End If
  
  If MsgBox("Se van a actualizar sus Tablas Oracle." & Chr$(13) _
   & "�Desea continuar?", vbExclamation + vbYesNo) = vbNo Then
    Exit Sub
  End If
  
FileOut = "C:\ERRCGCF.LOG"
Nfil1 = FreeFile
Open FileOut$ For Output As #Nfil1
  
cdmActualizar.Enabled = False
  
  FilaGrid = 0
  
  SSDBGrid1.Redraw = False
  SSDBGrid1.MoveFirst
  For Filaux = 0 To SSDBGrid1.Rows
    SSDBGrid1.Columns(2).Value = False
    SSDBGrid1.MoveNext
  Next Filaux
  SSDBGrid1.MoveFirst
  SSDBGrid1.Redraw = True
    
  auxDirBDFox = Con1.Connect
  i = InStr(auxDirBDFox, "DBQ=")
  auxDirBDFox = Right(auxDirBDFox, Len(auxDirBDFox) - i - 3)
  i = InStr(auxDirBDFox, ";")
  auxDirBDFox = Left(auxDirBDFox, i - 1)
    
  On Error GoTo Err_Ejecutar
    
  sqlstrOrcl = "SELECT FRG7TABCGCF,FRG7TABORAC FROM FRG700 ORDER BY FRG7TABCGCF ASC"
  Set rstOrclTab = objApp.rdoConnect.OpenResultset(sqlstrOrcl)
  While Not rstOrclTab.EOF
    strDeleteOrcl = "DELETE FROM " & rstOrclTab("FRG7TABORAC").Value
    
    On Error GoTo Err_Ejecutar
    'objApp.rdoConnect.Execute strDeleteOrcl, 64
    Con2.Execute strDeleteOrcl, 64
    'objApp.rdoConnect.Execute "commit", 64

    sqlstrOrcl = "SELECT * FROM FRG800 WHERE FRG7TABCGCF='" & rstOrclTab("FRG7TABCGCF").Value & "' ORDER BY FRG8ORDEN ASC"
    On Error GoTo Err_Ejecutar
    Set rstOrclCol = objApp.rdoConnect.OpenResultset(sqlstrOrcl)
    NomColsOrcl = ""
    TipoColsFox = ""
    NomColsFox = ""
    TipoColsOrcl = ""
    numColumnas = 0
    While Not rstOrclCol.EOF
      numColumnas = numColumnas + 1
      NomColsFox = NomColsFox & rstOrclCol("FRG8COLCGCF").Value & ","
      TipoColsFox = TipoColsFox & rstOrclCol("FRG8TYPCGCF").Value & ","
      NomColsOrcl = NomColsOrcl & rstOrclCol("FRG8COLORAC").Value & ","
      TipoColsOrcl = TipoColsOrcl & rstOrclCol("FRG8TYPORAC").Value & ","
      rstOrclCol.MoveNext
    Wend
    NomColsFox = Left(NomColsFox, Len(NomColsFox) - 1)
    TipoColsFox = Left(TipoColsFox, Len(TipoColsFox) - 1)
    NomColsOrcl = Left(NomColsOrcl, Len(NomColsOrcl) - 1)
    TipoColsOrcl = Left(TipoColsOrcl, Len(TipoColsOrcl) - 1)
    rstOrclCol.Close
    Set rstOrclCol = Nothing

    On Error GoTo Err_Ejecutar
    
    DataFoxPro1.Connect = "FoxPro 2.6;"
    'DataFoxPro1.DatabaseName = Text1.Text
    DataFoxPro1.DatabaseName = auxDirBDFox
    'DataFoxPro1.RecordSource = "SELECT " & NomColsFox & " FROM " & rstOrclTab("FRG7TABCGCF").Value
    DataFoxPro1.RecordSource = rstOrclTab("FRG7TABCGCF").Value
    DataFoxPro1.Refresh
    
    While Not DataFoxPro1.Recordset.EOF
      strInsertOrcl = "INSERT INTO " & rstOrclTab("FRG7TABORAC").Value
      strInsertOrcl = strInsertOrcl & "(" & NomColsOrcl & ")"
      strInsertOrcl = strInsertOrcl & " VALUES ("

      'tipos Oracle:char,varchar2,long,date,number,raw,longraw,rowid,mlslabel
      For i = 0 To numColumnas - 1
        Select Case campoStr(TipoColsOrcl, i, ",")
        Case "CHAR", "VARCHAR2", "LONG"
          If i < numColumnas - 1 Then
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL,"
            Else
              If InStr(DataFoxPro1.Recordset(i).Value, "'") > 0 Then
                strLongRaw = DataFoxPro1.Recordset(i).Value
                strLongRaw = objGen.ReplaceStr(strLongRaw, "'", "`", Len(strLongRaw))
                strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "',"
              Else
                strInsertOrcl = strInsertOrcl & "'" & DataFoxPro1.Recordset(i).Value & "',"
              End If
            End If
          Else
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL)"
            Else
              If InStr(DataFoxPro1.Recordset(i).Value, "'") > 0 Then
                strLongRaw = DataFoxPro1.Recordset(i).Value
                strLongRaw = objGen.ReplaceStr(strLongRaw, "'", "`", Len(strLongRaw))
                strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "')"
              Else
                strInsertOrcl = strInsertOrcl & "'" & DataFoxPro1.Recordset(i).Value & "')"
              End If
            End If
          End If
        Case "LONG RAW"
          If i < numColumnas - 1 Then
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL,"
            Else
              strLongRaw = Left(DataFoxPro1.Recordset(i).Value, 2000)
              If InStr(strLongRaw, "'") > 0 Then
                strLongRaw = objGen.ReplaceStr(strLongRaw, "'", "`", Len(strLongRaw))
                strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "',"
              Else
                strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "',"
              End If
              'strInsertOrcl = strInsertOrcl & "'" & DataFoxPro1.Recordset(i).Value & "',"
              'strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "',"
            End If
          Else
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL)"
            Else
              strLongRaw = Left(DataFoxPro1.Recordset(i).Value, 2000)
              If InStr(strLongRaw, "'") > 0 Then
                strLongRaw = objGen.ReplaceStr(strLongRaw, "'", "`", Len(strLongRaw))
                strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "')"
              Else
                strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "')"
              End If
              'strLongRaw = Left(DataFoxPro1.Recordset(i).Value, 2000)
              'strInsertOrcl = strInsertOrcl & "'" & DataFoxPro1.Recordset(i).Value & "')"
              'strInsertOrcl = strInsertOrcl & "'" & strLongRaw & "')"
            End If
          End If
        Case "DATE"
          If i < numColumnas - 1 Then
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL,"
            Else
              FechaAux = Format(DataFoxPro1.Recordset(i).Value, "dd/mm/yyyy")
              FechaAux = "TO_DATE('" & FechaAux & "','dd/mm/yyyy')"
              strInsertOrcl = strInsertOrcl & FechaAux & ","
            End If
          Else
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL)"
            Else
              FechaAux = Format(DataFoxPro1.Recordset(i).Value, "dd/mm/yyyy")
              FechaAux = "TO_DATE('" & FechaAux & "','dd/mm/yyyy')"
              strInsertOrcl = strInsertOrcl & FechaAux & ")"
            End If
          End If
        Case Else
          If i < numColumnas - 1 Then
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL,"
            Else
              If InStr(DataFoxPro1.Recordset(i).Value, ",") > 0 Then
                strInsertOrcl = strInsertOrcl & objGen.ReplaceStr(DataFoxPro1.Recordset(i).Value, ",", ".", 1) & ","
              Else
                strInsertOrcl = strInsertOrcl & DataFoxPro1.Recordset(i).Value & ","
              End If
            End If
          Else
            If IsNull(DataFoxPro1.Recordset(i).Value) Then
              strInsertOrcl = strInsertOrcl & "NULL)"
            Else
              If InStr(DataFoxPro1.Recordset(i).Value, ",") > 0 Then
                strInsertOrcl = strInsertOrcl & objGen.ReplaceStr(DataFoxPro1.Recordset(i).Value, ",", ".", 1) & ","
              Else
                strInsertOrcl = strInsertOrcl & DataFoxPro1.Recordset(i).Value & ")"
              End If
            End If
          End If
        End Select
      Next i
      
      On Error GoTo Err_Ejecutar

      'On Error Resume Next

      erreninsert = True
      'objApp.rdoConnect.Execute strInsertOrcl, 64
      Con2.Execute strInsertOrcl, 64
      'objApp.rdoConnect.Execute "commit", 64
      erreninsert = False
      
      'SSDBGrid1.row = FilaGrid
      'SSDBGrid1.Columns(2).Value = True

      DataFoxPro1.Recordset.MoveNext
    Wend
    DataFoxPro1.Recordset.Close
    
    SSDBGrid1.Redraw = False
    SSDBGrid1.MoveFirst
    For Filaux = 0 To FilaGrid
      SSDBGrid1.MoveNext
    Next Filaux
    SSDBGrid1.Columns(2).Value = True
    SSDBGrid1.MoveFirst
    SSDBGrid1.Redraw = True
    
    FilaGrid = FilaGrid + 1

    rstOrclTab.MoveNext
  Wend
  rstOrclTab.Close
  Set rstOrclTab = Nothing

  Close #Nfil1

cdmActualizar.Enabled = True

Err_Ejecutar:
  If ERR.Number <> 0 Then
    If erreninsert = True Then
      Write #Nfil1, strInsertOrcl
      Resume Next
    Else
      MsgBox "Error: " & ERR.Number & " " & ERR.Description
    End If
  End If
  cdmActualizar.Enabled = True
  
  Close #Nfil1

  Exit Sub

End Sub

Private Sub cmdActGrpTer_Click()
Dim sqlstr As String
Dim rstSQL As rdoResultset
Dim strupdate As String
Dim rstUpdate As rdoResultset
Dim strClave As String
Dim strPadre As String
Dim strsqlPadre As String
Dim rstsqlPadre As rdoResultset
Dim sqlstrGT As String
Dim rstSQLGT As rdoResultset
  
  If MsgBox("Se van a actualizar sus Tablas Oracle de Grupos Terape�ticos." & Chr$(13) _
   & "�Desea continuar?", vbExclamation + vbYesNo) = vbNo Then
    Exit Sub
  End If
  
  cmdActGrpTer.Enabled = False
  
  
  sqlstrGT = "SELECT count(*) FROM FRC400 ORDER BY FRC4GTECONCOD ASC"
  Set rstSQLGT = objApp.rdoConnect.OpenResultset(sqlstrGT)
'FRC4GTECONCOD,FRC4GTECONDES
'FRC4GTECONFALT
'FRC4GTECONFMOD
'FRC4GTECONFBAJ
  If rstSQLGT(0).Value = 0 Then
    MsgBox "No hay datos para actualizar sus Tablas Oracle de Grupos Terape�ticos."
    cmdActGrpTer.Enabled = True
    Exit Sub
  End If
  
  objApp.rdoConnect.Execute "UPDATE FR7300 SET FR00CODGRPTERAP=NULL WHERE FR00CODGRPTERAP IS NOT NULL", 64
  objApp.rdoConnect.Execute "commit", 64
  
  objApp.rdoConnect.Execute "DELETE FROM FR0000", 64
  objApp.rdoConnect.Execute "commit", 64

  objApp.rdoConnect.Execute "DELETE FROM FRC400 WHERE FRC4GTECONCOD0='  0'", 64
  objApp.rdoConnect.Execute "commit", 64

  objApp.rdoConnect.Execute "DELETE FROM FRC400 WHERE FRC4GTECONCOD IS NULL", 64
  objApp.rdoConnect.Execute "commit", 64
  
  objApp.rdoConnect.Execute "DELETE FROM FRC400 WHERE FRC4GTECONDES IS NULL", 64
  objApp.rdoConnect.Execute "commit", 64
  
  objApp.rdoConnect.Execute "INSERT INTO FR0000 (FR00CODGRPTERAP,FR00DESGRPTERAP) SELECT FRC4GTECONCOD,FRC4GTECONDES FROM FRC400", 64
  objApp.rdoConnect.Execute "commit", 64
  
  sqlstr = "SELECT * FROM FR0000 ORDER BY FR00CODGRPTERAP ASC"
  Set rstSQL = objApp.rdoConnect.OpenResultset(sqlstr)
  While Not rstSQL.EOF
    strClave = rstSQL("FR00CODGRPTERAP").Value
    Select Case Len(strClave)
    Case 1
    
    Case 2
      strPadre = Left(strClave, 1)
      strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
      objApp.rdoConnect.Execute strupdate, 64
      objApp.rdoConnect.Execute "commit", 64
    
    Case 3
      strPadre = Left(strClave, 1)
      strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
      objApp.rdoConnect.Execute strupdate, 64
      objApp.rdoConnect.Execute "commit", 64
    
    Case 4
      strPadre = Left(strClave, 3)
      strsqlPadre = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
      Set rstsqlPadre = objApp.rdoConnect.OpenResultset(strsqlPadre)
      If rstsqlPadre.EOF Then
        rstsqlPadre.Close
        Set rstsqlPadre = Nothing
        strPadre = Left(strClave, 1)
        strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "commit", 64
      Else
        rstsqlPadre.Close
        Set rstsqlPadre = Nothing
        strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "commit", 64
      End If
    
    Case 5
      strPadre = Left(strClave, 4)
      strsqlPadre = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
      Set rstsqlPadre = objApp.rdoConnect.OpenResultset(strsqlPadre)
      If rstsqlPadre.EOF Then
        rstsqlPadre.Close
        Set rstsqlPadre = Nothing
        strPadre = Left(strClave, 3)
        strsqlPadre = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
        Set rstsqlPadre = objApp.rdoConnect.OpenResultset(strsqlPadre)
        If rstsqlPadre.EOF Then
          rstsqlPadre.Close
          Set rstsqlPadre = Nothing
          strPadre = Left(strClave, 1)
          strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "commit", 64
        Else
          rstsqlPadre.Close
          Set rstsqlPadre = Nothing
          strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "commit", 64
        End If
      Else
        rstsqlPadre.Close
        Set rstsqlPadre = Nothing
        strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "commit", 64
      End If
      
    Case 6
      strPadre = Left(strClave, 5)
      strsqlPadre = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
      Set rstsqlPadre = objApp.rdoConnect.OpenResultset(strsqlPadre)
      If rstsqlPadre.EOF Then
        rstsqlPadre.Close
        Set rstsqlPadre = Nothing
        strPadre = Left(strClave, 4)
        strsqlPadre = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
        Set rstsqlPadre = objApp.rdoConnect.OpenResultset(strsqlPadre)
        If rstsqlPadre.EOF Then
          rstsqlPadre.Close
          Set rstsqlPadre = Nothing
          strPadre = Left(strClave, 3)
          strsqlPadre = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
          Set rstsqlPadre = objApp.rdoConnect.OpenResultset(strsqlPadre)
          If rstsqlPadre.EOF Then
            rstsqlPadre.Close
            Set rstsqlPadre = Nothing
            strPadre = Left(strClave, 1)
            strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "commit", 64
          Else
            rstsqlPadre.Close
            Set rstsqlPadre = Nothing
            strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "commit", 64
          End If
        Else
          rstsqlPadre.Close
          Set rstsqlPadre = Nothing
          strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "commit", 64
        End If
      Else
        rstsqlPadre.Close
        Set rstsqlPadre = Nothing
        strupdate = "UPDATE FR0000 SET FR00CODGRPTERAP_PAD='" & strPadre & "' WHERE FR00CODGRPTERAP='" & strClave & "'"
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "commit", 64
      End If
    End Select
    rstSQL.MoveNext
  Wend
  rstSQL.Close
  Set rstSQL = Nothing

  cmdActGrpTer.Enabled = True

End Sub

Private Sub cmdActPrinActi_Click()
Dim sqlstrPA As String
Dim rstSQLPA As rdoResultset
Dim strInsertPA As String
  
  If MsgBox("Se van a actualizar sus Tablas Oracle de Principios Activos." & Chr$(13) _
   & "�Desea continuar?", vbExclamation + vbYesNo) = vbNo Then
    Exit Sub
  End If
  
  cmdActPrinActi.Enabled = False
  
  sqlstrPA = "SELECT count(*) FROM FRE200 WHERE FRE2CLASE='2'"
  Set rstSQLPA = objApp.rdoConnect.OpenResultset(sqlstrPA)
  
  If rstSQLPA(0).Value = 0 Then
    MsgBox "No hay datos para actualizar sus Tablas Oracle de Principios Activos."
    cmdActPrinActi.Enabled = True
    Exit Sub
  End If
  
  objApp.rdoConnect.Execute "DELETE FROM FR6400 WHERE FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR>990000 AND FR73INDPRINACT=-1)", 64
  objApp.rdoConnect.Execute "commit", 64
  
  objApp.rdoConnect.Execute "DELETE FROM FR7300 WHERE FR73CODINTFAR>990000 AND FR73INDPRINACT=-1", 64
  objApp.rdoConnect.Execute "commit", 64

  strInsertPA = "INSERT INTO FR7300 "
  strInsertPA = strInsertPA & "(FR73CODPRODUCTO,FR73CODINTFAR,FR73CODINTFARSEG,FR73DESPRODUCTO,"
  strInsertPA = strInsertPA & "FR93CODUNIMEDIDA,FR88CODTIPIVA,FR73PRECIONETCOMPRA,FR73FECINIVIG,FR73INDPRINACT) "
  strInsertPA = strInsertPA & "SELECT FR73CODPRODUCTO_SEQUENCE.NEXTVAL,FRE2CODIGO+990000,0,FRE2NOMBRE,"
  strInsertPA = strInsertPA & "'NE',1,0,TO_DATE(SYSDATE),-1 FROM FRE200 WHERE FRE2CLASE='2'"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64
  
  objApp.rdoConnect.Execute "DELETE FROM FR4600 WHERE FR68CODPRINCACTIV_ORI<10000", 64
  objApp.rdoConnect.Execute "commit", 64
  objApp.rdoConnect.Execute "DELETE FROM FR4600 WHERE FR68CODPRINCACTIV_DES<10000", 64
  objApp.rdoConnect.Execute "commit", 64
  objApp.rdoConnect.Execute "DELETE FROM FR6400 WHERE FR68CODPRINCACTIV<10000", 64
  objApp.rdoConnect.Execute "commit", 64
  objApp.rdoConnect.Execute "DELETE FROM FR6800 WHERE FR68CODPRINCACTIV<10000", 64
  objApp.rdoConnect.Execute "commit", 64

  strInsertPA = "INSERT INTO FR6800 "
  strInsertPA = strInsertPA & "(FR68CODPRINCACTIV,FR68DESPRINCACTIV,"
  strInsertPA = strInsertPA & "FR68FECINIVIG) "
  strInsertPA = strInsertPA & "SELECT FRE2CODIGO,FRE2NOMBRE,"
  strInsertPA = strInsertPA & "TO_DATE(SYSDATE) FROM FRE200 WHERE FRE2CLASE='2'"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64

  strInsertPA = "INSERT INTO FR6400 "
  strInsertPA = strInsertPA & "(FR73CODPRODUCTO,FR68CODPRINCACTIV,"
  strInsertPA = strInsertPA & "FR64DOSIS,FR93CODUNIMEDIDA) "
  strInsertPA = strInsertPA & "SELECT FR73CODPRODUCTO,FR73CODINTFAR-990000,"
  strInsertPA = strInsertPA & "0,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODINTFAR>990000 AND FR73INDPRINACT=-1"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64
  
  strInsertPA = "INSERT INTO FR4600 "
  strInsertPA = strInsertPA & "(FR46CODINTERACCIO,"
  strInsertPA = strInsertPA & "FR68CODPRINCACTIV_ORI,FR68CODPRINCACTIV_DES,"
  strInsertPA = strInsertPA & "FR78CODTIPINTER,FR46TIEMPINTERACC,"
  strInsertPA = strInsertPA & "FR46DESLARGAINTER,"
  strInsertPA = strInsertPA & "FRI5CODACCION,FRI4CODNATURALEZA) "
  strInsertPA = strInsertPA & "SELECT FR46CODINTERACCIO_SEQUENCE.NEXTVAL,"
  strInsertPA = strInsertPA & "FRC5COD1A,FRC5COD2A,FRC5INTSIG,"
  strInsertPA = strInsertPA & "0,FRC6INTTXI,FRC5INTMED,FRC5INTNAT FROM FRC500,FRC600 "
  strInsertPA = strInsertPA & "Where FRC500.FRC5INTTEX = FRC600.FRC6CODTXI And FRC5INTSEN = 0"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64
  
  strInsertPA = "INSERT INTO FR4600 "
  strInsertPA = strInsertPA & "(FR46CODINTERACCIO,"
  strInsertPA = strInsertPA & "FR68CODPRINCACTIV_ORI,FR68CODPRINCACTIV_DES,"
  strInsertPA = strInsertPA & "FR78CODTIPINTER,FR46TIEMPINTERACC,"
  strInsertPA = strInsertPA & "FR46DESLARGAINTER,"
  strInsertPA = strInsertPA & "FRI5CODACCION,FRI4CODNATURALEZA) "
  strInsertPA = strInsertPA & "SELECT FR46CODINTERACCIO_SEQUENCE.NEXTVAL,"
  strInsertPA = strInsertPA & "FRC5COD2A,FRC5COD1A,FRC5INTSIG,"
  strInsertPA = strInsertPA & "0,FRC6INTTXI,FRC5INTMED,FRC5INTNAT FROM FRC500,FRC600 "
  strInsertPA = strInsertPA & "Where FRC500.FRC5INTTEX = FRC600.FRC6CODTXI And FRC5INTSEN = 1"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64

  strInsertPA = "INSERT INTO FR4600 "
  strInsertPA = strInsertPA & "(FR46CODINTERACCIO,"
  strInsertPA = strInsertPA & "FR68CODPRINCACTIV_ORI,FR68CODPRINCACTIV_DES,"
  strInsertPA = strInsertPA & "FR78CODTIPINTER,FR46TIEMPINTERACC,"
  strInsertPA = strInsertPA & "FR46DESLARGAINTER,"
  strInsertPA = strInsertPA & "FRI5CODACCION,FRI4CODNATURALEZA) "
  strInsertPA = strInsertPA & "SELECT FR46CODINTERACCIO_SEQUENCE.NEXTVAL,"
  strInsertPA = strInsertPA & "FRC5COD2A,FRC5COD1A,FRC5INTSIG,"
  strInsertPA = strInsertPA & "0,FRC6INTTXI,FRC5INTMED,FRC5INTNAT FROM FRC500,FRC600 "
  strInsertPA = strInsertPA & "Where FRC500.FRC5INTTEX = FRC600.FRC6CODTXI And FRC5INTSEN = 2"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64

  strInsertPA = "INSERT INTO FR4600 "
  strInsertPA = strInsertPA & "(FR46CODINTERACCIO,"
  strInsertPA = strInsertPA & "FR68CODPRINCACTIV_ORI,FR68CODPRINCACTIV_DES,"
  strInsertPA = strInsertPA & "FR78CODTIPINTER,FR46TIEMPINTERACC,"
  strInsertPA = strInsertPA & "FR46DESLARGAINTER,"
  strInsertPA = strInsertPA & "FRI5CODACCION,FRI4CODNATURALEZA) "
  strInsertPA = strInsertPA & "SELECT FR46CODINTERACCIO_SEQUENCE.NEXTVAL,"
  strInsertPA = strInsertPA & "FRC5COD1A,FRC5COD2A,FRC5INTSIG,"
  strInsertPA = strInsertPA & "0,FRC6INTTXI,FRC5INTMED,FRC5INTNAT FROM FRC500,FRC600 "
  strInsertPA = strInsertPA & "Where FRC500.FRC5INTTEX = FRC600.FRC6CODTXI And FRC5INTSEN = 2"
  
  objApp.rdoConnect.Execute strInsertPA, 64
  objApp.rdoConnect.Execute "commit", 64

  cmdActPrinActi.Enabled = True

End Sub

Private Sub cmdRestaurar_Click()
Dim rstOrclTab As rdoResultset
Dim rstOrclCol As rdoResultset
Dim sqlstrOrcl As String
Dim rstOrcl As rdoResultset
Dim sqlstrFox As String
Dim rstFox As rdoResultset
Dim strInsertFox As String
Dim strDeleteFox As String
Dim NomColsOrcl As String
Dim NomColsFox As String
Dim TipoColsOrcl As String
Dim TipoColsFox As String
Dim numColumnas As Long
Dim FechaAux As String
Dim i As Long
Dim FilaGrid As Integer
Dim Filaux As Integer
    
  If Text1.Text = "" Then
    MsgBox "Debe elegir el ODBC de acceso de la BD FOXPRO 2.6"
    Exit Sub
  End If
            
  If MsgBox("Se van a actualizar sus Tablas FoxPro locales." & Chr$(13) _
   & "�Desea continuar?", vbExclamation + vbYesNo) = vbNo Then
    Exit Sub
  End If
  
  FilaGrid = 0
  
  SSDBGrid1.Redraw = False
  SSDBGrid1.MoveFirst
  For Filaux = 0 To SSDBGrid1.Rows
    SSDBGrid1.Columns(2).Value = False
    SSDBGrid1.MoveNext
  Next Filaux
  SSDBGrid1.MoveFirst
  SSDBGrid1.Redraw = True
  
  On Error GoTo Err_Ejecutar
  sqlstrOrcl = "SELECT FRG7TABCGCF,FRG7TABORAC FROM FRG700 ORDER BY FRG7TABCGCF ASC"
  Set rstOrclTab = objApp.rdoConnect.OpenResultset(sqlstrOrcl)
  While Not rstOrclTab.EOF
    strDeleteFox = "DELETE FROM " & rstOrclTab("FRG7TABCGCF").Value

    On Error GoTo Err_Ejecutar
    Con1.Execute strDeleteFox, 64

    sqlstrOrcl = "SELECT * FROM FRG800 WHERE FRG7TABCGCF='" & rstOrclTab("FRG7TABCGCF").Value & "' ORDER BY FRG8ORDEN ASC"
    On Error GoTo Err_Ejecutar
    Set rstOrclCol = objApp.rdoConnect.OpenResultset(sqlstrOrcl)
    NomColsOrcl = ""
    TipoColsFox = ""
    NomColsFox = ""
    TipoColsOrcl = ""
    numColumnas = 0
    While Not rstOrclCol.EOF
      numColumnas = numColumnas + 1
      NomColsFox = NomColsFox & rstOrclCol("FRG8COLCGCF").Value & ","
      TipoColsFox = TipoColsFox & rstOrclCol("FRG8TYPCGCF").Value & ","
      If rstOrclCol("FRG8TYPORAC").Value <> "DATE" Then
        NomColsOrcl = NomColsOrcl & rstOrclCol("FRG8COLORAC").Value & ","
      Else
        NomColsOrcl = NomColsOrcl & "TO_CHAR(" & rstOrclCol("FRG8COLORAC").Value & ",'DD/MM/YY')" & ","
      End If
      TipoColsOrcl = TipoColsOrcl & rstOrclCol("FRG8TYPORAC").Value & ","
      rstOrclCol.MoveNext
    Wend
    NomColsFox = Left(NomColsFox, Len(NomColsFox) - 1)
    TipoColsFox = Left(TipoColsFox, Len(TipoColsFox) - 1)
    NomColsOrcl = Left(NomColsOrcl, Len(NomColsOrcl) - 1)
    TipoColsOrcl = Left(TipoColsOrcl, Len(TipoColsOrcl) - 1)
    rstOrclCol.Close
    Set rstOrclCol = Nothing

    On Error GoTo Err_Ejecutar
    sqlstrOrcl = "SELECT " & NomColsOrcl & " FROM " & rstOrclTab("FRG7TABORAC").Value
    Set rstOrcl = objApp.rdoConnect.OpenResultset(sqlstrOrcl)
    While Not rstOrcl.EOF
      strInsertFox = "INSERT INTO " & rstOrclTab("FRG7TABCGCF").Value
      'strInsertFox = strInsertFox & "(" & NomColsFox & ")"
      strInsertFox = strInsertFox & " VALUES ("

      'tipos Oracle:char,varchar2,long,date,number,raw,longraw,rowid,mlslabel
      For i = 0 To numColumnas - 1
        Select Case campoStr(TipoColsOrcl, i, ",")
        Case "CHAR", "VARCHAR2", "LONG", "LONG RAW"
          If i < numColumnas - 1 Then
            If IsNull(rstOrcl(i).Value) Then
              strInsertFox = strInsertFox & "NULL,"
            Else
              strInsertFox = strInsertFox & "'" & rstOrcl(i).Value & "',"
            End If
          Else
            If IsNull(rstOrcl(i).Value) Then
              strInsertFox = strInsertFox & "NULL)"
            Else
              strInsertFox = strInsertFox & "'" & rstOrcl(i).Value & "')"
            End If
          End If
        Case "DATE"
          If i < numColumnas - 1 Then
            If IsNull(rstOrcl(i).Value) Then
              strInsertFox = strInsertFox & "NULL,"
            Else
              FechaAux = rstOrcl(i).Value
              strInsertFox = strInsertFox & "'" & FechaAux & "',"
            End If
          Else
            If IsNull(rstOrcl(i).Value) Then
              strInsertFox = strInsertFox & "NULL)"
            Else
              FechaAux = rstOrcl(i).Value
              strInsertFox = strInsertFox & "'" & FechaAux & "')"
            End If
          End If
        Case Else
          If i < numColumnas - 1 Then
            If IsNull(rstOrcl(i).Value) Then
              strInsertFox = strInsertFox & "NULL,"
            Else
              strInsertFox = strInsertFox & rstOrcl(i).Value & ","
            End If
          Else
            If IsNull(rstOrcl(i).Value) Then
              strInsertFox = strInsertFox & "NULL)"
            Else
              strInsertFox = strInsertFox & rstOrcl(i).Value & ")"
            End If
          End If
        End Select
      Next i
      
      On Error GoTo Err_Ejecutar
      Con1.Execute strInsertFox, 64
      
      'SSDBGrid1.row = FilaGrid
      'SSDBGrid1.Columns(2).Value = True
  
      rstOrcl.MoveNext
    Wend
    rstOrcl.Close
    Set rstOrcl = Nothing
    
    SSDBGrid1.Redraw = False
    SSDBGrid1.MoveFirst
    For Filaux = 0 To FilaGrid
      SSDBGrid1.MoveNext
    Next Filaux
    SSDBGrid1.Columns(2).Value = True
    SSDBGrid1.MoveFirst
    SSDBGrid1.Redraw = True
    
    FilaGrid = FilaGrid + 1

    rstOrclTab.MoveNext
  Wend
  rstOrclTab.Close
  Set rstOrclTab = Nothing
  
  Exit Sub

Err_Ejecutar:
  If ERR.Number <> 0 Then
    MsgBox "Error: " & ERR.Number & " " & ERR.Description
  End If
  Exit Sub

End Sub

Private Sub cmdSalir_Click()

  Unload Me

End Sub

Private Sub cmdElegir_Click()
Dim Filaux As Integer
  
'Dim i, j As Integer
'Dim aux As String
'  CommonDialog1.ShowOpen
'  i = InStr(CommonDialog1.filename, "\")
'  aux = CommonDialog1.filename
'  j = i
'  While i <> 0
'    aux = Right(aux, Len(aux) - i)
'    i = InStr(aux, "\")
'    j = j + i
'  Wend
'  Text1.Text = Left(CommonDialog1.filename, j - 1)
  
  cdmActualizar.Enabled = False
  cmdRestaurar.Enabled = False
  
  MsgBox "Debe elegir el ODBC de acceso de la BD FOXPRO 2.6"
  On Error GoTo Err_Ejecutar
  Set Env1 = rdoEnvironments(0)
  On Error GoTo Err_Ejecutar
  Set Con1 = Env1.OpenConnection("conexion1", rdDriverPrompt, False, "")
  Text1.Text = Con1.Connect

  MsgBox "Debe elegir el ODBC de acceso a ORACLE"
  On Error GoTo Err_Ejecutar
  Set env2 = rdoEnvironments(0)
  On Error GoTo Err_Ejecutar
  Set Con2 = Env1.OpenConnection("conexion2", rdDriverPrompt, False, "")
  Text2.Text = Con2.Connect

  SSDBGrid1.Redraw = False
  SSDBGrid1.MoveFirst
  For Filaux = 0 To SSDBGrid1.Rows
    SSDBGrid1.Columns(2).Value = False
    SSDBGrid1.MoveNext
  Next Filaux
  SSDBGrid1.MoveFirst
  SSDBGrid1.Redraw = True

  cdmActualizar.Enabled = True
  cmdRestaurar.Enabled = True

  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & ERR.Number & " " & ERR.Description
  Exit Sub

End Sub

Private Sub Form_Load()

Dim rsta As rdoResultset
Dim sqlstr As String

  cdmActualizar.Enabled = False
  cmdRestaurar.Enabled = False

  sqlstr = "SELECT FRG7TABCGCF,FRG7TABORAC FROM FRG700 ORDER BY FRG7TABCGCF ASC"
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)

  SSDBGrid1.AddNew

  While Not rsta.EOF

    SSDBGrid1.AddNew
    SSDBGrid1.Columns(0).Value = rsta("FRG7TABCGCF").Value
    SSDBGrid1.Columns(1).Value = rsta("FRG7TABORAC").Value
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  SSDBGrid1.Refresh
  SSDBGrid1.MoveFirst

End Sub

