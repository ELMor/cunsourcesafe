VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmConsOMAnte 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. �rdenes M�dicas anteriores"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   32
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdtraer 
      Caption         =   "Traer Productos"
      Height          =   375
      Left            =   4800
      TabIndex        =   59
      Top             =   7680
      Width           =   2055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   42
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "OM / PRN anteriores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   1
      Left            =   120
      TabIndex        =   33
      Top             =   480
      Width           =   11100
      Begin TabDlg.SSTab tabTab1 
         Height          =   3375
         Index           =   0
         Left            =   120
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   360
         Width           =   10695
         _ExtentX        =   18865
         _ExtentY        =   5953
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0115.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "tab1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkCheck1(12)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(15)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0115.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   -66960
            TabIndex        =   4
            Tag             =   "Inter�s Cient�fico?"
            Top             =   360
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   -66960
            TabIndex        =   3
            Tag             =   "Restricci�n de Volumen?"
            Top             =   120
            Width           =   2175
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Medicaci�n Infantil"
            DataField       =   "FR66INDMEDINF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -69240
            TabIndex        =   2
            Tag             =   "Medicaci�n Infantil?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -69240
            TabIndex        =   1
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   -71520
            TabIndex        =   0
            Tag             =   "Orden M�dica?"
            Top             =   360
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   1
            Left            =   -73320
            TabIndex        =   19
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   -74880
            TabIndex        =   18
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3345
            Index           =   2
            Left            =   0
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   0
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   5900
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tab1 
            Height          =   2415
            Left            =   -74880
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   840
            Width           =   10095
            _ExtentX        =   17806
            _ExtentY        =   4260
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            Tab             =   1
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0115.frx":0038
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(8)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(7)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(4)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(5)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(4)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(2)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(3)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(23)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(24)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "M�dico / Enfermera"
            TabPicture(1)   =   "FR0115.frx":0054
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "lblLabel1(6)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(10)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "lblLabel1(0)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "lblLabel1(22)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "lblLabel1(2)"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "lblLabel1(18)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "lblLabel1(5)"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "lblLabel1(20)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "lblLabel1(27)"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "dtcDateCombo1(2)"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "dtcDateCombo1(0)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "dtcDateCombo1(1)"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).Control(12)=   "txtText1(22)"
            Tab(1).Control(12).Enabled=   0   'False
            Tab(1).Control(13)=   "txtText1(19)"
            Tab(1).Control(13).Enabled=   0   'False
            Tab(1).Control(14)=   "txtText1(11)"
            Tab(1).Control(14).Enabled=   0   'False
            Tab(1).Control(15)=   "txtText1(10)"
            Tab(1).Control(15).Enabled=   0   'False
            Tab(1).Control(16)=   "txtText1(21)"
            Tab(1).Control(16).Enabled=   0   'False
            Tab(1).Control(17)=   "txtText1(20)"
            Tab(1).Control(17).Enabled=   0   'False
            Tab(1).Control(18)=   "txtText1(9)"
            Tab(1).Control(18).Enabled=   0   'False
            Tab(1).Control(19)=   "txtText1(8)"
            Tab(1).Control(19).Enabled=   0   'False
            Tab(1).Control(20)=   "txtText1(7)"
            Tab(1).Control(20).Enabled=   0   'False
            Tab(1).Control(21)=   "txtText1(6)"
            Tab(1).Control(21).Enabled=   0   'False
            Tab(1).ControlCount=   22
            TabCaption(2)   =   "Servicio "
            TabPicture(2)   =   "FR0115.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(3)"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).Control(1)=   "lblLabel1(1)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "txtText1(17)"
            Tab(2).Control(2).Enabled=   0   'False
            Tab(2).Control(3)=   "txtText1(16)"
            Tab(2).Control(3).Enabled=   0   'False
            Tab(2).Control(4)=   "txtText1(14)"
            Tab(2).Control(4).Enabled=   0   'False
            Tab(2).Control(5)=   "txtText1(15)"
            Tab(2).Control(5).Enabled=   0   'False
            Tab(2).ControlCount=   6
            TabCaption(3)   =   "Observaciones"
            TabPicture(3)   =   "FR0115.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(18)"
            Tab(3).Control(1)=   "lblLabel1(24)"
            Tab(3).ControlCount=   2
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   24
               Left            =   -72960
               TabIndex        =   16
               Tag             =   "C�digo Paciente"
               Top             =   1440
               Width           =   1395
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   23
               Left            =   -74760
               TabIndex        =   14
               Tag             =   "C�digo Paciente"
               Top             =   1440
               Width           =   1395
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   -72960
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   960
               Width           =   2205
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   12
               Tag             =   "C�digo Paciente"
               Top             =   960
               Width           =   1395
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   -70320
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   960
               Width           =   2445
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   -67680
               TabIndex        =   17
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   960
               Width           =   2445
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_ENF"
               Height          =   330
               Index           =   6
               Left            =   120
               TabIndex        =   20
               Tag             =   "C�digo Enfermera"
               Top             =   600
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   2280
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Nombre Enfermera"
               Top             =   600
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   120
               TabIndex        =   5
               Tag             =   "C�digo Doctor"
               Top             =   1200
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   2280
               TabIndex        =   6
               Tag             =   "Apellido Doctor"
               Top             =   1200
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   8040
               TabIndex        =   23
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   21
               Left            =   8040
               TabIndex        =   24
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   2040
               TabIndex        =   25
               Tag             =   "Hora Redacci�n"
               Top             =   1920
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   4920
               TabIndex        =   27
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   1920
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   3960
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   600
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   3840
               TabIndex        =   26
               Tag             =   "C�digo Urgencia"
               Top             =   1920
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   -72600
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Desc.Servicio Cargo"
               Top             =   1440
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   14
               Left            =   -74760
               TabIndex        =   8
               Tag             =   "C�d.Servicio Cargo"
               Top             =   1440
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   10
               Tag             =   "C�d.Servicio"
               Top             =   720
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -72600
               TabIndex        =   11
               Tag             =   "Desc.Servicio"
               Top             =   720
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1650
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   28
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   9525
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   39
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   40
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   5880
               TabIndex        =   60
               Tag             =   "Fecha Firma M�dico"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   0
               Left            =   5880
               TabIndex        =   7
               Tag             =   "Fecha Firma Enfermera"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   61
               Tag             =   "Fecha Redacci�n"
               Top             =   1920
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Paciente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -74760
               TabIndex        =   58
               Top             =   720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -72960
               TabIndex        =   57
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "1� Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   -70320
               TabIndex        =   56
               Top             =   720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "2� Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -67680
               TabIndex        =   55
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   120
               TabIndex        =   54
               Top             =   360
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   120
               TabIndex        =   53
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   5880
               TabIndex        =   52
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   8040
               TabIndex        =   51
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   5880
               TabIndex        =   50
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   8040
               TabIndex        =   49
               Top             =   960
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   48
               Top             =   1680
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   2040
               TabIndex        =   47
               Top             =   1680
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   3840
               TabIndex        =   46
               Top             =   1680
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   45
               Top             =   480
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -74760
               TabIndex        =   44
               Top             =   1200
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   43
               Top             =   360
               Width           =   2655
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   -73320
            TabIndex        =   41
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   -74880
            TabIndex        =   37
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3105
      Index           =   0
      Left            =   120
      TabIndex        =   30
      Top             =   4440
      Width           =   11055
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2595
         Index           =   0
         Left            =   120
         TabIndex        =   31
         Top             =   360
         Width           =   10785
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   19024
         _ExtentY        =   4577
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   29
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmConsOMAnte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedactarOMPRN (FR0111.FRM)                                           *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: redactar Orden M�dica                                   *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdtraer_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String

cmdtraer.Enabled = False
If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
'Guardamos el n�mero de filas seleccionadas
mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
ReDim gintprodbuscado(mintNTotalSelRows, 3)
gintprodtotal = mintNTotalSelRows
For mintisel = 0 To mintNTotalSelRows - 1
    'Guardamos el n�mero de fila que est� seleccionada
     mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
     gintprodbuscado(mintisel, 0) = "" & grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk) & "" 'c�digo
     gintprodbuscado(mintisel, 1) = grdDBGrid1(0).Columns(6).CellValue(mvarBkmrk) 'c�d.interno
     gintprodbuscado(mintisel, 2) = grdDBGrid1(0).Columns(7).CellValue(mvarBkmrk) 'descripci�n
Next mintisel
cmdtraer.Enabled = True
Unload Me
Else
mensaje = MsgBox("No ha seleccionado ning�n producto", vbInformation, "Aviso")
cmdtraer.Enabled = True
End If
End Sub

Private Sub Form_Activate()
'se va al primer registro para refrescar el grid
Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "OM"
      
    .strTable = "FR6600"
    
    .strWhere = "CI21CODPERSONA=" & frmRedactarOMPRN.txtText1(2).Text & " AND FR66CODPETICION<>" & frmRedactarOMPRN.txtText1(0).Text
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    
   
    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")



  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle OM"
    
    .strTable = "FR2800"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�digo", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Desc.V�a", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Volumen", "FR28VOLUMEN", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Tiempo Infusi�n", "FR28TIEMINFMIN", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "C�d.Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "", cwString, 60)
    Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "C.I", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "S.N", "FR28INDSN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR28CANTCOMPU", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Grupo", "FR28GRUPO", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "1�d�a", "FR28FECADMPRIMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "2�d�a", "FR28FECADMSEGMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "3�d�a", "FR28FECADMTERMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "4�d�a", "FR28FECADMCUAMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "5�d�a", "FR28FECADMQUIMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "6�d�a", "FR28FECADMSEXMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "7�d�a", "FR28FECADMSEPMDIA", cwDate)
    Call .GridAddColumn(objMultiInfo, "Notas", "FR28NOTASCOMP", cwString, 2000)
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(23)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(23)), txtText1(4), "CI22PRIAPEL")
   
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(5), "CI22SEGAPEL")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "SG02NOM")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(19), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR34DESVIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(18)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(18)), grdDBGrid1(0).Columns(19), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(13), "FRG4DESFRECUENCIA")
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    .CtrlGetInfo(txtText1(23)).blnForeign = True
    .CtrlGetInfo(txtText1(24)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnForeign = True
     
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
grdDBGrid1(0).Columns(3).Visible = False
grdDBGrid1(0).Columns(4).Visible = False

grdDBGrid1(2).Columns(3).Width = 0 'c�digo repetido
grdDBGrid1(2).Columns(5).Width = 0 'c�digo repetido

grdDBGrid1(0).Columns(5).Width = 1000   'c�d.producto
grdDBGrid1(0).Columns(6).Width = 800   'c�d interno
grdDBGrid1(0).Columns(7).Width = 1500   'desc.producto
grdDBGrid1(0).Columns(8).Width = 400   'c�d.v�a
grdDBGrid1(0).Columns(9).Width = 1500   'desc.v�a
grdDBGrid1(0).Columns(10).Width = 1100  'volumen
grdDBGrid1(0).Columns(11).Width = 1100  'tiempo infusi�n
grdDBGrid1(0).Columns(12).Width = 1500  'c�d.frecuencia
grdDBGrid1(0).Columns(13).Width = 1500  'frecuencia
grdDBGrid1(0).Columns(14).Width = 500  'prn
grdDBGrid1(0).Columns(15).Width = 500  'comienzo inmediato
grdDBGrid1(0).Columns(16).Width = 500  'seg�n niveles
grdDBGrid1(0).Columns(17).Width = 1000  'cantidad compuesto
grdDBGrid1(0).Columns(18).Width = 700  'c�d.medida
grdDBGrid1(0).Columns(19).Width = 1000  'desc. medida
grdDBGrid1(0).Columns(20).Width = 700  'grupo
grdDBGrid1(0).Columns(21).Width = 1000  'dia 1
grdDBGrid1(0).Columns(22).Width = 1000  'dia 2
grdDBGrid1(0).Columns(23).Width = 1000  'dia 3
grdDBGrid1(0).Columns(24).Width = 1000  'dia 4
grdDBGrid1(0).Columns(25).Width = 1000  'dia 5
grdDBGrid1(0).Columns(26).Width = 1000  'dia 6
grdDBGrid1(0).Columns(27).Width = 1000  'dia 7

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "CI2200"

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Primer Apellido"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Segundo Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Enfermera"

     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(14)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).C�d.Prod" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"

     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(5), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).V�a" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR3400"

     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "C�digo V�a"
     
     Set objField = .AddField("FR34DESVIA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(8), .cllValues("FR34CODVIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Medida" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(18), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer


If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "OM" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
                If txtText1(0).Text <> "" Then
                    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                    strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                              txtText1(0).Text
                    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
                    If IsNull(rstlinea.rdoColumns(0).Value) Then
                       linea = 1 + grdDBGrid1(0).row
                    Else
                       linea = rstlinea.rdoColumns(0).Value + 1 + grdDBGrid1(0).row
                    End If
                    grdDBGrid1(0).Columns(4).Value = linea
                    SendKeys ("{TAB}")
                    rstlinea.Close
                    Set rstlinea = Nothing
                Else
                    MsgBox "No hay ninguna petici�n", vbInformation
                    Exit Sub
                End If
        Else
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer

If intIndex = 2 And objWinInfo.objWinActiveForm.strName = "OM" Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        sqlstr = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        grdDBGrid1(0).Columns(3).Value = txtText1(0).Text
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        If intIndex = 2 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
                If txtText1(0).Text <> "" Then
                    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
                    strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                              txtText1(0).Text
                    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
                    If IsNull(rstlinea.rdoColumns(0).Value) Then
                       linea = 1 + grdDBGrid1(0).row
                    Else
                       linea = rstlinea.rdoColumns(0).Value + 1 + grdDBGrid1(0).row
                    End If
                    grdDBGrid1(0).Columns(4).Value = linea
                    SendKeys ("{TAB}")
                    rstlinea.Close
                    Set rstlinea = Nothing
                Else
                    MsgBox "No hay ninguna petici�n", vbInformation
                    Exit Sub
                End If
        Else
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        End If
End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


