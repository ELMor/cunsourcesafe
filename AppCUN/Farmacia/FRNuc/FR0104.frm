VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmVerOriPeticion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Controlar Estupefacientes. Ver OM/PRN "
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdBloquear 
      Caption         =   "Boquear/Desbloq"
      Height          =   375
      Left            =   10320
      TabIndex        =   232
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton cmdHistoriaclinica 
      Caption         =   "Historia Cl�nica"
      Height          =   375
      Left            =   10320
      TabIndex        =   231
      Top             =   2880
      Width           =   1455
   End
   Begin VB.CommandButton cmdFtp 
      Caption         =   "Perfil FTP"
      Height          =   375
      Left            =   10320
      TabIndex        =   230
      Top             =   2280
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Hoja Quir�fano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   3
      Left            =   120
      TabIndex        =   135
      Top             =   600
      Width           =   10020
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   90
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   211
         Tag             =   "N�mero Orden D�a"
         Top             =   600
         Width           =   372
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Urgente"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   4800
         TabIndex        =   210
         Tag             =   "Urgente?"
         Top             =   960
         Width           =   2295
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Microscop�a"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   7440
         TabIndex        =   209
         Tag             =   "Microscop�a?"
         Top             =   720
         Width           =   2295
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   58
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   141
         Tag             =   "C�digo Hoja"
         Top             =   600
         Width           =   1100
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Anatom�a Patol�gica"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   13
         Left            =   4800
         TabIndex        =   140
         Tag             =   "Anatom�a Patol�gica?"
         Top             =   720
         Width           =   2655
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Endoscopia"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   4800
         TabIndex        =   139
         Tag             =   "Endoscopia?"
         Top             =   240
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Radioterapia"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   4800
         TabIndex        =   138
         Tag             =   "Radioterapia?"
         Top             =   480
         Width           =   2055
      End
      Begin VB.CheckBox Check1 
         Caption         =   "CEC"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   7440
         TabIndex        =   137
         Tag             =   "CEC?"
         Top             =   480
         Width           =   2175
      End
      Begin VB.CheckBox Check1 
         Caption         =   "RX"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   7440
         TabIndex        =   136
         Tag             =   "RX?"
         Top             =   240
         Width           =   2295
      End
      Begin TabDlg.SSTab tab1 
         Height          =   2415
         Index           =   2
         Left            =   240
         TabIndex        =   142
         TabStop         =   0   'False
         Top             =   1200
         Width           =   9615
         _ExtentX        =   16960
         _ExtentY        =   4260
         _Version        =   327681
         Style           =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Paciente"
         TabPicture(0)   =   "FR0104.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(60)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(61)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(62)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(63)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(64)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Text1(68)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Text1(69)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Text1(70)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Text1(71)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "Text1(72)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Doctores"
         TabPicture(1)   =   "FR0104.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblLabel1(80)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "lblLabel1(81)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "lblLabel1(82)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "lblLabel1(83)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "Text1(91)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "Text1(92)"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).Control(6)=   "Text1(93)"
         Tab(1).Control(6).Enabled=   0   'False
         Tab(1).Control(7)=   "Text1(94)"
         Tab(1).Control(7).Enabled=   0   'False
         Tab(1).ControlCount=   8
         TabCaption(2)   =   "Intervenci�n"
         TabPicture(2)   =   "FR0104.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lblLabel1(84)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "lblLabel1(85)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "lblLabel1(86)"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "lblLabel1(87)"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "DateCombo1(11)"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "Text1(95)"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "Text1(96)"
         Tab(2).Control(6).Enabled=   0   'False
         Tab(2).Control(7)=   "Text1(97)"
         Tab(2).Control(7).Enabled=   0   'False
         Tab(2).Control(8)=   "Text1(98)"
         Tab(2).Control(8).Enabled=   0   'False
         Tab(2).ControlCount=   9
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   98
            Left            =   -70920
            Locked          =   -1  'True
            TabIndex        =   228
            Tag             =   "Hora Fin Anestesia"
            Top             =   1680
            Width           =   612
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   97
            Left            =   -74160
            Locked          =   -1  'True
            TabIndex        =   227
            Tag             =   "Quir�fano"
            Top             =   840
            Width           =   8280
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   96
            Left            =   -74760
            Locked          =   -1  'True
            TabIndex        =   225
            Tag             =   "C�digo Quir�fano"
            Top             =   840
            Width           =   372
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   95
            Left            =   -72480
            Locked          =   -1  'True
            TabIndex        =   221
            Tag             =   "Hora Intervenci�n"
            Top             =   1680
            Width           =   612
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   94
            Left            =   -70680
            Locked          =   -1  'True
            TabIndex        =   216
            Tag             =   "Apellido Doctor"
            Top             =   960
            Width           =   3285
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   93
            Left            =   -74280
            Locked          =   -1  'True
            TabIndex        =   215
            Tag             =   "C�digo Cirujano"
            Top             =   960
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   92
            Left            =   -70680
            Locked          =   -1  'True
            TabIndex        =   214
            Tag             =   "Apellido Doctor"
            Top             =   1680
            Width           =   3285
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   91
            Left            =   -74280
            Locked          =   -1  'True
            TabIndex        =   213
            Tag             =   "C�digo Firma Doctor"
            Top             =   1680
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            Height          =   1650
            Index           =   89
            Left            =   -74880
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   173
            Tag             =   "Observaciones"
            Top             =   600
            Width           =   9525
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   88
            Left            =   -72600
            TabIndex        =   172
            Tag             =   "Desc.Departamento"
            Top             =   1920
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   87
            Left            =   -74760
            TabIndex        =   171
            Tag             =   "C�d.Departamento"
            Top             =   1920
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   86
            Left            =   -74760
            TabIndex        =   170
            Tag             =   "C�d.Servicio Cargo"
            Top             =   1320
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   85
            Left            =   -72600
            TabIndex        =   169
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio Cargo"
            Top             =   1320
            Width           =   5445
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   84
            Left            =   -74760
            TabIndex        =   168
            Tag             =   "C�d.Servicio"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   83
            Left            =   -72600
            TabIndex        =   167
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio"
            Top             =   600
            Width           =   5445
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   82
            Left            =   -71160
            TabIndex        =   166
            Tag             =   "C�digo Urgencia"
            Top             =   1920
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   81
            Left            =   -71040
            TabIndex        =   165
            TabStop         =   0   'False
            Tag             =   "Apellido 1� Enfermera"
            Top             =   1200
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   80
            Left            =   -70080
            TabIndex        =   164
            TabStop         =   0   'False
            Tag             =   "Desc. Urgencia"
            Top             =   1920
            Width           =   4125
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   79
            Left            =   -72840
            TabIndex        =   163
            Tag             =   "Hora Redacci�n"
            Top             =   1920
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   78
            Left            =   -66960
            TabIndex        =   162
            Tag             =   "Hora Firma Enfermera"
            Top             =   1200
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   77
            Left            =   -66960
            TabIndex        =   161
            Tag             =   "hora Firma M�dico"
            Top             =   600
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   76
            Left            =   -72720
            TabIndex        =   160
            Tag             =   "Apellido Doctor"
            Top             =   600
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   75
            Left            =   -74880
            TabIndex        =   159
            Tag             =   "C�digo Doctor"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   74
            Left            =   -72720
            TabIndex        =   158
            TabStop         =   0   'False
            Tag             =   "Nombre Enfermera"
            Top             =   1200
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   73
            Left            =   -74880
            TabIndex        =   157
            Tag             =   "C�digo Enfermera"
            Top             =   1200
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   72
            Left            =   6480
            Locked          =   -1  'True
            TabIndex        =   156
            TabStop         =   0   'False
            Tag             =   "Apellido 2� Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   71
            Left            =   3360
            Locked          =   -1  'True
            TabIndex        =   155
            TabStop         =   0   'False
            Tag             =   "Apellido 1� Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   70
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   154
            Tag             =   "C�digo Paciente"
            Top             =   960
            Width           =   1500
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   69
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   153
            TabStop         =   0   'False
            Tag             =   "Nombre Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   68
            Left            =   2280
            Locked          =   -1  'True
            TabIndex        =   152
            TabStop         =   0   'False
            Tag             =   "Historia"
            Top             =   960
            Width           =   1500
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   67
            Left            =   -70560
            TabIndex        =   151
            Tag             =   "Apellido Doctor"
            Top             =   720
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   66
            Left            =   -74160
            TabIndex        =   150
            Tag             =   "C�digo Doctor"
            Top             =   720
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   65
            Left            =   -70560
            TabIndex        =   149
            Tag             =   "Apellido Doctor"
            Top             =   1320
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   64
            Left            =   -74160
            TabIndex        =   148
            Tag             =   "C�digo Doctor"
            Top             =   1320
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   63
            Left            =   -70560
            TabIndex        =   147
            Tag             =   "Apellido Doctor"
            Top             =   1920
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   62
            Left            =   -74160
            TabIndex        =   146
            Tag             =   "C�digo Doctor"
            Top             =   1920
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   61
            Left            =   -72240
            TabIndex        =   145
            Tag             =   "hora Firma M�dico"
            Top             =   720
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   60
            Left            =   -70200
            TabIndex        =   144
            Tag             =   "C�digo Petici�n"
            Top             =   720
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            Height          =   810
            Index           =   59
            Left            =   -74760
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   143
            Tag             =   "Observaciones"
            Top             =   1440
            Width           =   9525
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   5
            Left            =   -74880
            TabIndex        =   174
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   6
            Left            =   -74880
            TabIndex        =   175
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   7
            Left            =   -69120
            TabIndex        =   176
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   600
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   8
            Left            =   -69120
            TabIndex        =   177
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1200
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   9
            Left            =   -74880
            TabIndex        =   178
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1920
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   10
            Left            =   -74760
            TabIndex        =   179
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   720
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo DateCombo1 
            Height          =   330
            Index           =   11
            Left            =   -74760
            TabIndex        =   222
            Tag             =   "Fecha Intervenci�n"
            Top             =   1680
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Fin Anestesia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   87
            Left            =   -70920
            TabIndex        =   229
            Top             =   1440
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Quir�fano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   86
            Left            =   -74760
            TabIndex        =   226
            Top             =   600
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Intervenci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   85
            Left            =   -74760
            TabIndex        =   224
            Top             =   1440
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   84
            Left            =   -72480
            TabIndex        =   223
            Top             =   1440
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   83
            Left            =   -70680
            TabIndex        =   220
            Top             =   720
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   82
            Left            =   -70680
            TabIndex        =   219
            Top             =   1440
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cirujano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   81
            Left            =   -74280
            TabIndex        =   218
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Firma"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   80
            Left            =   -74280
            TabIndex        =   217
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   77
            Left            =   -74880
            TabIndex        =   207
            Top             =   360
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   76
            Left            =   -74760
            TabIndex        =   206
            Top             =   1680
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio Cargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   75
            Left            =   -74760
            TabIndex        =   205
            Top             =   1080
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   74
            Left            =   -74760
            TabIndex        =   204
            Top             =   360
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   73
            Left            =   -71160
            TabIndex        =   203
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Redacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   72
            Left            =   -72840
            TabIndex        =   202
            Top             =   1680
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Redacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   71
            Left            =   -74880
            TabIndex        =   201
            Top             =   1680
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Firma Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   70
            Left            =   -66960
            TabIndex        =   200
            Top             =   960
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   69
            Left            =   -69120
            TabIndex        =   199
            Top             =   960
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Firma M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   68
            Left            =   -66960
            TabIndex        =   198
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   67
            Left            =   -69120
            TabIndex        =   197
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   66
            Left            =   -74880
            TabIndex        =   196
            Top             =   360
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   65
            Left            =   -74880
            TabIndex        =   195
            Top             =   960
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   64
            Left            =   6480
            TabIndex        =   194
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   63
            Left            =   3360
            TabIndex        =   193
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   62
            Left            =   240
            TabIndex        =   192
            Top             =   1560
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   61
            Left            =   240
            TabIndex        =   191
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   60
            Left            =   2280
            TabIndex        =   190
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   59
            Left            =   -70560
            TabIndex        =   189
            Top             =   480
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   58
            Left            =   -70560
            TabIndex        =   188
            Top             =   1080
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   57
            Left            =   -70560
            TabIndex        =   187
            Top             =   1680
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cirujano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   56
            Left            =   -74160
            TabIndex        =   186
            Top             =   480
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Anestesista"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   55
            Left            =   -74160
            TabIndex        =   185
            Top             =   1080
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Firmado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   54
            Left            =   -74160
            TabIndex        =   184
            Top             =   1680
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Intervenci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   53
            Left            =   -74760
            TabIndex        =   183
            Top             =   480
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   52
            Left            =   -72240
            TabIndex        =   182
            Top             =   480
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Duraci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   51
            Left            =   -70200
            TabIndex        =   181
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   50
            Left            =   -74760
            TabIndex        =   180
            Top             =   1200
            Width           =   1215
         End
      End
      Begin VB.Label lblLabel1 
         Caption         =   "N�m. Orden D�a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   79
         Left            =   2160
         TabIndex        =   212
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d. Hoja"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   78
         Left            =   360
         TabIndex        =   208
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Hoja Anestesia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   2
      Left            =   120
      TabIndex        =   61
      Top             =   720
      Width           =   10020
      Begin VB.CheckBox Check1 
         Caption         =   "Trasplante Hep�tico"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   7440
         TabIndex        =   67
         Tag             =   "Trasplante Hep�tico?"
         Top             =   600
         Width           =   2295
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Anestesia G Adulto"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   7440
         TabIndex        =   66
         Tag             =   "Anestesia G. Adulto?"
         Top             =   360
         Width           =   2175
      End
      Begin VB.CheckBox Check1 
         Caption         =   "CEC HA"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   4800
         TabIndex        =   65
         Tag             =   "CEC HA?"
         Top             =   600
         Width           =   2055
      End
      Begin VB.CheckBox Check1 
         Caption         =   "A.G. Pedi�trica"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   4800
         TabIndex        =   64
         Tag             =   "A.G. Pedi�trica?"
         Top             =   360
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Analgosedacci�n y Locor"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   4800
         TabIndex        =   63
         Tag             =   "Analgosedacci�n y Locor?"
         Top             =   840
         Width           =   2655
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   24
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   62
         Tag             =   "C�digo Hoja"
         Top             =   600
         Width           =   1100
      End
      Begin TabDlg.SSTab tab1 
         Height          =   2415
         Index           =   1
         Left            =   120
         TabIndex        =   68
         TabStop         =   0   'False
         Top             =   1200
         Width           =   9735
         _ExtentX        =   17171
         _ExtentY        =   4260
         _Version        =   327681
         Style           =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Paciente"
         TabPicture(0)   =   "FR0104.frx":0054
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(34)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(35)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(36)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(37)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(38)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Text1(47)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Text1(48)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Text1(49)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Text1(50)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "Text1(51)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Doctores"
         TabPicture(1)   =   "FR0104.frx":0070
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblLabel1(40)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "lblLabel1(41)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "lblLabel1(42)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "lblLabel1(43)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "lblLabel1(44)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "lblLabel1(45)"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).Control(6)=   "Text1(27)"
         Tab(1).Control(6).Enabled=   0   'False
         Tab(1).Control(7)=   "Text1(28)"
         Tab(1).Control(7).Enabled=   0   'False
         Tab(1).Control(8)=   "Text1(29)"
         Tab(1).Control(8).Enabled=   0   'False
         Tab(1).Control(9)=   "Text1(52)"
         Tab(1).Control(9).Enabled=   0   'False
         Tab(1).Control(10)=   "Text1(53)"
         Tab(1).Control(10).Enabled=   0   'False
         Tab(1).Control(11)=   "Text1(54)"
         Tab(1).Control(11).Enabled=   0   'False
         Tab(1).ControlCount=   12
         TabCaption(2)   =   "Intervenci�n"
         TabPicture(2)   =   "FR0104.frx":008C
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lblLabel1(46)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "lblLabel1(47)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "lblLabel1(48)"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "lblLabel1(49)"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "DateCombo1(6)"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "Text1(55)"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "Text1(56)"
         Tab(2).Control(6).Enabled=   0   'False
         Tab(2).Control(7)=   "Text1(57)"
         Tab(2).Control(7).Enabled=   0   'False
         Tab(2).ControlCount=   8
         Begin VB.TextBox Text1 
            Height          =   810
            Index           =   57
            Left            =   -74760
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   133
            Tag             =   "Descripci�n"
            Top             =   1440
            Width           =   8925
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   56
            Left            =   -70200
            Locked          =   -1  'True
            TabIndex        =   131
            Tag             =   "Duraci�n"
            Top             =   720
            Width           =   1100
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   55
            Left            =   -72240
            Locked          =   -1  'True
            TabIndex        =   129
            Tag             =   "Hora Intervenci�n"
            Top             =   720
            Width           =   612
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   54
            Left            =   -74160
            Locked          =   -1  'True
            TabIndex        =   122
            Tag             =   "C�digo Firmado"
            Top             =   1920
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   53
            Left            =   -70560
            Locked          =   -1  'True
            TabIndex        =   121
            Tag             =   "Apellido Doctor"
            Top             =   1920
            Width           =   3285
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   52
            Left            =   -74160
            Locked          =   -1  'True
            TabIndex        =   119
            Tag             =   "C�digo Anestesista"
            Top             =   1320
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   29
            Left            =   -70560
            Locked          =   -1  'True
            TabIndex        =   118
            Tag             =   "Apellido Doctor"
            Top             =   1320
            Width           =   3285
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   28
            Left            =   -74160
            Locked          =   -1  'True
            TabIndex        =   116
            Tag             =   "C�digo Cirujano"
            Top             =   720
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   27
            Left            =   -70560
            Locked          =   -1  'True
            TabIndex        =   115
            Tag             =   "Apellido Doctor"
            Top             =   720
            Width           =   3285
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   51
            Left            =   2280
            Locked          =   -1  'True
            TabIndex        =   90
            TabStop         =   0   'False
            Tag             =   "Historia"
            Top             =   960
            Width           =   1500
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   50
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   89
            TabStop         =   0   'False
            Tag             =   "Nombre Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   49
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   88
            Tag             =   "C�digo Paciente"
            Top             =   960
            Width           =   1500
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   48
            Left            =   3360
            Locked          =   -1  'True
            TabIndex        =   87
            TabStop         =   0   'False
            Tag             =   "Apellido 1� Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   47
            Left            =   6480
            Locked          =   -1  'True
            TabIndex        =   86
            TabStop         =   0   'False
            Tag             =   "Apellido 2� Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   46
            Left            =   -74880
            TabIndex        =   85
            Tag             =   "C�digo Enfermera"
            Top             =   1200
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   45
            Left            =   -72720
            TabIndex        =   84
            TabStop         =   0   'False
            Tag             =   "Nombre Enfermera"
            Top             =   1200
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   44
            Left            =   -74880
            TabIndex        =   83
            Tag             =   "C�digo Doctor"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   43
            Left            =   -72720
            TabIndex        =   82
            Tag             =   "Apellido Doctor"
            Top             =   600
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   42
            Left            =   -66960
            TabIndex        =   81
            Tag             =   "hora Firma M�dico"
            Top             =   600
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   41
            Left            =   -66960
            TabIndex        =   80
            Tag             =   "Hora Firma Enfermera"
            Top             =   1200
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   40
            Left            =   -72840
            TabIndex        =   79
            Tag             =   "Hora Redacci�n"
            Top             =   1920
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   39
            Left            =   -70080
            TabIndex        =   78
            TabStop         =   0   'False
            Tag             =   "Desc. Urgencia"
            Top             =   1920
            Width           =   4125
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   38
            Left            =   -71040
            TabIndex        =   77
            TabStop         =   0   'False
            Tag             =   "Apellido 1� Enfermera"
            Top             =   1200
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   37
            Left            =   -71160
            TabIndex        =   76
            Tag             =   "C�digo Urgencia"
            Top             =   1920
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   36
            Left            =   -72600
            TabIndex        =   75
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio"
            Top             =   600
            Width           =   5445
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   35
            Left            =   -74760
            TabIndex        =   74
            Tag             =   "C�d.Servicio"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   34
            Left            =   -72600
            TabIndex        =   73
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio Cargo"
            Top             =   1320
            Width           =   5445
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   33
            Left            =   -74760
            TabIndex        =   72
            Tag             =   "C�d.Servicio Cargo"
            Top             =   1320
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   32
            Left            =   -74760
            TabIndex        =   71
            Tag             =   "C�d.Departamento"
            Top             =   1920
            Width           =   2000
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   31
            Left            =   -72600
            TabIndex        =   70
            Tag             =   "Desc.Departamento"
            Top             =   1920
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            Height          =   1650
            Index           =   30
            Left            =   -74880
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   69
            Tag             =   "Observaciones"
            Top             =   600
            Width           =   9525
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   91
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   2
            Left            =   -74880
            TabIndex        =   92
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   3
            Left            =   -69120
            TabIndex        =   93
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   600
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   4
            Left            =   -69120
            TabIndex        =   94
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1200
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   5
            Left            =   -74880
            TabIndex        =   95
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1920
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo DateCombo1 
            Height          =   330
            Index           =   6
            Left            =   -74760
            TabIndex        =   127
            Tag             =   "Fecha Intervenci�n"
            Top             =   720
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   49
            Left            =   -74760
            TabIndex        =   134
            Top             =   1200
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Duraci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   48
            Left            =   -70200
            TabIndex        =   132
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   47
            Left            =   -72240
            TabIndex        =   130
            Top             =   480
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Intervenci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   46
            Left            =   -74760
            TabIndex        =   128
            Top             =   480
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Firmado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   45
            Left            =   -74160
            TabIndex        =   126
            Top             =   1680
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Anestesista"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   44
            Left            =   -74160
            TabIndex        =   125
            Top             =   1080
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cirujano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   43
            Left            =   -74160
            TabIndex        =   124
            Top             =   480
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   42
            Left            =   -70560
            TabIndex        =   123
            Top             =   1680
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   41
            Left            =   -70560
            TabIndex        =   120
            Top             =   1080
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   40
            Left            =   -70560
            TabIndex        =   117
            Top             =   480
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   38
            Left            =   2280
            TabIndex        =   113
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   37
            Left            =   240
            TabIndex        =   112
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   36
            Left            =   240
            TabIndex        =   111
            Top             =   1560
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   35
            Left            =   3360
            TabIndex        =   110
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   34
            Left            =   6480
            TabIndex        =   109
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   33
            Left            =   -74880
            TabIndex        =   108
            Top             =   960
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   32
            Left            =   -74880
            TabIndex        =   107
            Top             =   360
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   31
            Left            =   -69120
            TabIndex        =   106
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Firma M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   30
            Left            =   -66960
            TabIndex        =   105
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   29
            Left            =   -69120
            TabIndex        =   104
            Top             =   960
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Firma Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   26
            Left            =   -66960
            TabIndex        =   103
            Top             =   960
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Redacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   25
            Left            =   -74880
            TabIndex        =   102
            Top             =   1680
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Redacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   -72840
            TabIndex        =   101
            Top             =   1680
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   -71160
            TabIndex        =   100
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   -74760
            TabIndex        =   99
            Top             =   360
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio Cargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   -74760
            TabIndex        =   98
            Top             =   1080
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   -74760
            TabIndex        =   97
            Top             =   1680
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   -74880
            TabIndex        =   96
            Top             =   360
            Width           =   2655
         End
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d. Hoja"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   39
         Left            =   360
         TabIndex        =   114
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   10020
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   0
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   14
         Tag             =   "C�digo Petici�n"
         Top             =   600
         Width           =   1100
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   1
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   13
         Tag             =   "C�digo OM origen PRN"
         Top             =   600
         Width           =   1100
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Orden M�dica"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   5760
         TabIndex        =   12
         Tag             =   "Orden M�dica?"
         Top             =   840
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Diab�tico"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   5760
         TabIndex        =   11
         Tag             =   "Diab�tico?"
         Top             =   360
         Width           =   1575
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Medicaci�n Infantil"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   5760
         TabIndex        =   10
         Tag             =   "Medicaci�n Infantil?"
         Top             =   600
         Width           =   2055
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Restricci�n volumen"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   7800
         TabIndex        =   9
         Tag             =   "Restricci�n de Volumen?"
         Top             =   360
         Width           =   2175
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Inter�s Cient�fico"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   7800
         TabIndex        =   8
         Tag             =   "Inter�s Cient�fico?"
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   25
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   7
         Tag             =   "Estado Petici�n"
         Top             =   600
         Width           =   315
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   26
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "Estado"
         Top             =   600
         Width           =   1605
      End
      Begin TabDlg.SSTab tab1 
         Height          =   2415
         Index           =   0
         Left            =   120
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   1320
         Width           =   9735
         _ExtentX        =   17171
         _ExtentY        =   4260
         _Version        =   327681
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Paciente"
         TabPicture(0)   =   "FR0104.frx":00A8
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(11)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(7)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(9)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Text1(23)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Text1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Text1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Text1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "Text1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "M�dico / Enfermera"
         TabPicture(1)   =   "FR0104.frx":00C4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblLabel1(27)"
         Tab(1).Control(1)=   "lblLabel1(20)"
         Tab(1).Control(2)=   "lblLabel1(5)"
         Tab(1).Control(3)=   "lblLabel1(18)"
         Tab(1).Control(4)=   "lblLabel1(2)"
         Tab(1).Control(5)=   "lblLabel1(22)"
         Tab(1).Control(6)=   "lblLabel1(0)"
         Tab(1).Control(7)=   "lblLabel1(10)"
         Tab(1).Control(8)=   "lblLabel1(6)"
         Tab(1).Control(9)=   "DateCombo1(2)"
         Tab(1).Control(10)=   "DateCombo1(0)"
         Tab(1).Control(11)=   "DateCombo1(1)"
         Tab(1).Control(12)=   "Text1(6)"
         Tab(1).Control(13)=   "Text1(7)"
         Tab(1).Control(13).Enabled=   0   'False
         Tab(1).Control(14)=   "Text1(8)"
         Tab(1).Control(15)=   "Text1(9)"
         Tab(1).Control(16)=   "Text1(20)"
         Tab(1).Control(17)=   "Text1(21)"
         Tab(1).Control(18)=   "Text1(10)"
         Tab(1).Control(19)=   "Text1(11)"
         Tab(1).Control(19).Enabled=   0   'False
         Tab(1).Control(20)=   "Text1(19)"
         Tab(1).Control(20).Enabled=   0   'False
         Tab(1).Control(21)=   "Text1(22)"
         Tab(1).ControlCount=   22
         TabCaption(2)   =   "Servicio"
         TabPicture(2)   =   "FR0104.frx":00E0
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Text1(14)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "Text1(15)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "Text1(12)"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "Text1(13)"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "lblLabel1(3)"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "lblLabel1(1)"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).ControlCount=   6
         TabCaption(3)   =   "Observaciones"
         TabPicture(3)   =   "FR0104.frx":00FC
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lblLabel1(24)"
         Tab(3).Control(1)=   "Text1(18)"
         Tab(3).ControlCount=   2
         Begin VB.TextBox Text1 
            Height          =   1650
            Index           =   18
            Left            =   -74880
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   35
            Tag             =   "Observaciones"
            Top             =   600
            Width           =   9285
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   14
            Left            =   -74760
            Locked          =   -1  'True
            TabIndex        =   34
            Tag             =   "C�d.Servicio Cargo"
            Top             =   1560
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   15
            Left            =   -72600
            Locked          =   -1  'True
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio Cargo"
            Top             =   1560
            Width           =   5445
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   12
            Left            =   -74760
            Locked          =   -1  'True
            TabIndex        =   32
            Tag             =   "Cod. Servicio"
            Top             =   840
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   13
            Left            =   -72600
            Locked          =   -1  'True
            TabIndex        =   31
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio"
            Top             =   840
            Width           =   5445
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   22
            Left            =   -71160
            Locked          =   -1  'True
            TabIndex        =   30
            Tag             =   "C�digo Urgencia"
            Top             =   1920
            Width           =   735
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   19
            Left            =   -71160
            Locked          =   -1  'True
            TabIndex        =   29
            TabStop         =   0   'False
            Tag             =   "Apellido 1� Enfermera"
            Top             =   1200
            Width           =   1605
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   11
            Left            =   -70080
            Locked          =   -1  'True
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Desc. Urgencia"
            Top             =   1920
            Width           =   4125
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   10
            Left            =   -72840
            Locked          =   -1  'True
            TabIndex        =   27
            Tag             =   "Hora Redacci�n"
            Top             =   1920
            Width           =   612
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   21
            Left            =   -67320
            TabIndex        =   26
            Tag             =   "Hora Firma Enfermera"
            Top             =   1200
            Width           =   612
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   20
            Left            =   -67320
            TabIndex        =   25
            Tag             =   "Hora Firma M�dico"
            Top             =   600
            Width           =   612
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   9
            Left            =   -72840
            Locked          =   -1  'True
            TabIndex        =   24
            Tag             =   "Apellido Doctor"
            Top             =   600
            Width           =   3285
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   8
            Left            =   -74880
            Locked          =   -1  'True
            TabIndex        =   23
            Tag             =   "C�digo Doctor"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   7
            Left            =   -72840
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Nombre Enfermera"
            Top             =   1200
            Width           =   1605
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   6
            Left            =   -74880
            Locked          =   -1  'True
            TabIndex        =   21
            Tag             =   "C�digo Enfermera"
            Top             =   1200
            Width           =   2000
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   5
            Left            =   6480
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Apellido 2� Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   4
            Left            =   3360
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Apellido 1� Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            Height          =   330
            Index           =   2
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   18
            Tag             =   "C�digo Paciente"
            Top             =   960
            Width           =   1500
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   3
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Nombre Paciente"
            Top             =   1800
            Width           =   2925
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   23
            Left            =   2280
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Historia"
            Top             =   960
            Width           =   1500
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   3
            Left            =   -74880
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   4
            Left            =   -74880
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo DateCombo1 
            Height          =   330
            Index           =   1
            Left            =   -69360
            TabIndex        =   38
            Tag             =   "Fecha Firma M�dico"
            Top             =   600
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo DateCombo1 
            Height          =   330
            Index           =   0
            Left            =   -69360
            TabIndex        =   39
            Tag             =   "Fecha Firma Enfermera"
            Top             =   1200
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo DateCombo1 
            Height          =   330
            Index           =   2
            Left            =   -74880
            TabIndex        =   40
            Tag             =   "Fecha Redacci�n"
            Top             =   1920
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   24
            Left            =   -74880
            TabIndex        =   57
            Top             =   360
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio Cargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74760
            TabIndex        =   56
            Top             =   1320
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74760
            TabIndex        =   55
            Tag             =   "Servicio"
            Top             =   600
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -71160
            TabIndex        =   54
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Redacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   -72840
            TabIndex        =   53
            Top             =   1680
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Redacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74880
            TabIndex        =   52
            Top             =   1680
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Firma Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   -67320
            TabIndex        =   51
            Top             =   960
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -69360
            TabIndex        =   50
            Top             =   960
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Firma M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   -67320
            TabIndex        =   49
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   -69360
            TabIndex        =   48
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   -74880
            TabIndex        =   47
            Top             =   360
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Enfermera"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   27
            Left            =   -74880
            TabIndex        =   46
            Top             =   960
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   6480
            TabIndex        =   45
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   3360
            TabIndex        =   44
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   43
            Top             =   1560
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   42
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   2280
            TabIndex        =   41
            Top             =   720
            Width           =   975
         End
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d. Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   28
         Left            =   120
         TabIndex        =   60
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d.OM origen PRN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   1440
         TabIndex        =   59
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   13
         Left            =   3360
         TabIndex        =   58
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3360
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   4560
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2835
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   11385
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16776960
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0104.frx":0118
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20082
         _ExtentY        =   5001
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVerOriPeticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmVerOriPeticion (FR0104.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: 5 OCTUBRE DE 1998                                             *
'* DESCRIPCION: Ver OM/PRN                                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


Private Sub Rellenar_HojaQuirofano()
    Dim rst1 As rdoResultset
    Dim str1 As String
    Dim rst2 As rdoResultset
    Dim str2 As String
    Dim rst3 As rdoResultset
    Dim str3 As String
    Dim rst4 As rdoResultset
    Dim str4 As String
    Dim rst5 As rdoResultset
    Dim str5 As String
    
    str1 = "select fr44codhojaquiro,sg02cod_cir,ci21codpersona,fr44horafinanest,fr44horainterv," & _
            "fr97codquirofano,fr44numordendia,sg02cod,fr44indendoscopia," & _
            "fr44indradioterap,fr44indanatpato,fr44indurgente,fr44indcec," & _
            "fr44indrx,fr44indmicroscopia,TO_DATE(TO_CHAR(fr44fecinterv,'DD/MM/YYYY'),'DD/MM/YYYY') " & _
            "from fr4400 where fr44CODHOJAQUIRO=" & frmMonPetEstup.grdDBGrid1(1).Columns(17).Value
    Set rst1 = objApp.rdoConnect.OpenResultset(str1)
    
    str2 = "select * from ci2200 where ci21codpersona=" & rst1("ci21codpersona").Value
    Set rst2 = objApp.rdoConnect.OpenResultset(str2)
    
    
    Frame1(1).Visible = False
    Frame1(2).Visible = False
    Frame1(3).Visible = True
    
    Text1(58).Text = rst1("fr44codhojaquiro").Value
    If IsNull(rst1("sg02cod_cir").Value) = False Then
        Text1(93).Text = rst1("sg02cod_cir").Value
        str3 = "select * from sg0200 where sg02cod='" & rst1("sg02cod_cir").Value & "'"
        Set rst3 = objApp.rdoConnect.OpenResultset(str3)
        If rst3.EOF = False Then
            Text1(94).Text = rst3("sg02ape1").Value
        End If
        rst3.Close
        Set rst3 = Nothing
    End If
    If IsNull(rst1("ci21codpersona").Value) = False Then
        Text1(70).Text = rst1("ci21codpersona").Value
    End If
    If IsNull(rst2("ci22numhistoria").Value) = False Then
        Text1(68).Text = rst2("ci22numhistoria").Value
    End If
    If IsNull(rst2("ci22nombre").Value) = False Then
        Text1(69).Text = rst2("ci22nombre").Value
    End If
    If IsNull(rst2("ci22priapel").Value) = False Then
        Text1(71).Text = rst2("ci22priapel").Value
    End If
    If IsNull(rst2("ci22segapel").Value) = False Then
        Text1(72).Text = rst2("ci22segapel").Value
    End If
    If IsNull(rst1("fr44horafinanest").Value) = False Then
        Text1(98).Text = rst1("fr44horafinanest").Value
    End If
    If IsNull(rst1("fr44horainterv").Value) = False Then
        Text1(95).Text = rst1("fr44horainterv").Value
    End If
    If IsNull(rst1("fr97codquirofano").Value) = False Then
        Text1(96).Text = rst1("fr97codquirofano").Value
        str4 = "select * from fr9700 where fr97codquirofano=" & rst1("fr97codquirofano").Value
        Set rst4 = objApp.rdoConnect.OpenResultset(str4)
        If rst4.EOF = False Then
            Text1(97).Text = rst4("fr97desquirofano").Value
        End If
        rst4.Close
        Set rst4 = Nothing
    End If
    If IsNull(rst1("fr44numordendia").Value) = False Then
        Text1(90).Text = rst1("fr44numordendia").Value
    End If
    If IsNull(rst1("sg02cod").Value) = False Then
        Text1(91).Text = rst1("sg02cod").Value
        str5 = "select * from sg0200 where sg02cod='" & rst1("sg02cod").Value & "'"
        Set rst5 = objApp.rdoConnect.OpenResultset(str5)
        If rst5.EOF = False Then
            Text1(92).Text = rst5("sg02ape1").Value
        End If
        rst5.Close
        Set rst5 = Nothing
    End If
    If rst1("fr44indendoscopia").Value = -1 Then
        Check1(11).Value = 1
    Else
        Check1(11).Value = 0
    End If
    If rst1("fr44indradioterap").Value = -1 Then
        Check1(10).Value = 1
    Else
        Check1(10).Value = 0
    End If
    If rst1("fr44indanatpato").Value = -1 Then
        Check1(13).Value = 1
    Else
        Check1(13).Value = 0
    End If
    If rst1("fr44indurgente").Value = -1 Then
        Check1(16).Value = 1
    Else
        Check1(16).Value = 0
    End If
    If rst1("fr44indcec").Value = -1 Then
        Check1(9).Value = 1
    Else
        Check1(9).Value = 0
    End If
    If rst1("fr44indrx").Value = -1 Then
        Check1(8).Value = 1
    Else
        Check1(8).Value = 0
    End If
    If rst1("fr44indmicroscopia").Value = -1 Then
        Check1(14).Value = 1
    Else
        Check1(14).Value = 0
    End If
    If IsNull(rst1(15).Value) = False Then
        DateCombo1(11).Date = rst1(15).Value
    End If
    rst1.Close
    rst2.Close
    Set rst1 = Nothing
    Set rst2 = Nothing
End Sub
Private Sub Rellenar_HojaAnestesia()
    Dim rst1 As rdoResultset
    Dim str1 As String
    Dim rst2 As rdoResultset
    Dim str2 As String
    Dim rst3 As rdoResultset
    Dim str3 As String
    Dim rst4 As rdoResultset
    Dim str4 As String
    Dim rst5 As rdoResultset
    Dim str5 As String
    
    str1 = "select fr43codhojaanest,fr43horainterven,ci21codpersona,fr43duraccirugia," & _
            "sg02cod_cir,sg02cod_ane,sg02cod,fr43desintervencion,fr43indanestgenped," & _
            "fr43indcec,fr43indanalglocor,fr43indanestgenadul,fr43indtransphepa," & _
            "TO_DATE(TO_CHAR(fr43fecinterven,'DD/MM/YYYY'),'DD/MM/YYYY') " & _
            "from fr4300 where fr43CODHOJAANEST=" & frmMonPetEstup.grdDBGrid1(1).Columns(17).Value
    Set rst1 = objApp.rdoConnect.OpenResultset(str1)
    
    str2 = "select * from ci2200 where ci21codpersona=" & rst1("ci21codpersona").Value
    Set rst2 = objApp.rdoConnect.OpenResultset(str2)
    
    
    Frame1(1).Visible = False
    Frame1(2).Visible = True
    Frame1(3).Visible = False
    
    Text1(24).Text = rst1("fr43codhojaanest").Value
    If IsNull(rst1("fr43horainterven").Value) = False Then
        Text1(55).Text = rst1("fr43horainterven").Value
    End If
    If IsNull(rst1("ci21codpersona").Value) = False Then
        Text1(49).Text = rst1("ci21codpersona").Value
    End If
    If IsNull(rst2("ci22numhistoria").Value) = False Then
        Text1(51).Text = rst2("ci22numhistoria").Value
    End If
    If IsNull(rst2("ci22nombre").Value) = False Then
        Text1(50).Text = rst2("ci22nombre").Value
    End If
    If IsNull(rst2("ci22priapel").Value) = False Then
        Text1(48).Text = rst2("ci22priapel").Value
    End If
    If IsNull(rst2("ci22segapel").Value) = False Then
        Text1(47).Text = rst2("ci22segapel").Value
    End If
    If IsNull(rst1("fr43duraccirugia").Value) = False Then
        Text1(56).Text = rst1("fr43duraccirugia").Value
    End If
    If IsNull(rst1("sg02cod_cir").Value) = False Then
        Text1(28).Text = rst1("sg02cod_cir").Value
        str3 = "select * from sg0200 where sg02cod='" & rst1("sg02cod_cir").Value & "'"
        Set rst3 = objApp.rdoConnect.OpenResultset(str3)
        If rst3.EOF = False Then
            Text1(27).Text = rst3("sg02ape1").Value
        End If
        rst3.Close
        Set rst3 = Nothing
    End If
    If IsNull(rst1("sg02cod_ane").Value) = False Then
        Text1(52).Text = rst1("sg02cod_ane").Value
        str4 = "select * from sg0200 where sg02cod='" & rst1("sg02cod_ane").Value & "'"
        Set rst4 = objApp.rdoConnect.OpenResultset(str4)
        If rst4.EOF = False Then
            Text1(29).Text = rst4("sg02ape1").Value
        End If
        rst4.Close
        Set rst4 = Nothing
    End If
    If IsNull(rst1("sg02cod").Value) = False Then
        Text1(54).Text = rst1("sg02cod").Value
        str5 = "select * from sg0200 where sg02cod='" & rst1("sg02cod").Value & "'"
        Set rst5 = objApp.rdoConnect.OpenResultset(str5)
        If rst5.EOF = False Then
            Text1(53).Text = rst5("sg02ape1").Value
        End If
        rst5.Close
        Set rst5 = Nothing
    End If
    If IsNull(rst1("fr43desintervencion").Value) = False Then
        Text1(57).Text = rst1("fr43desintervencion").Value
    End If
    If rst1("fr43indanestgenped").Value = -1 Then
        Check1(3).Value = 1
    Else
        Check1(3).Value = 0
    End If
    If rst1("fr43indcec").Value = -1 Then
        Check1(4).Value = 1
    Else
        Check1(4).Value = 0
    End If
    If rst1("fr43indanalglocor").Value = -1 Then
        Check1(1).Value = 1
    Else
        Check1(1).Value = 0
    End If
    If rst1("fr43indanestgenadul").Value = -1 Then
        Check1(5).Value = 1
    Else
        Check1(5).Value = 0
    End If
    If rst1("fr43indtransphepa").Value = -1 Then
        Check1(6).Value = 1
    Else
        Check1(6).Value = 0
    End If
    If IsNull(rst1(13).Value) = False Then
        DateCombo1(6).Date = rst1(13).Value
    End If
    rst1.Close
    rst2.Close
    Set rst1 = Nothing
    Set rst2 = Nothing
End Sub
Private Sub Rellenar_Petici�nFarmacia()
    Dim rst1 As rdoResultset
    Dim str1 As String
    Dim rst2 As rdoResultset
    Dim str2 As String
    Dim rst3 As rdoResultset
    Dim str3 As String
    Dim rst4 As rdoResultset
    Dim str4 As String
    Dim rst5 As rdoResultset
    Dim str5 As String
    Dim rst6 As rdoResultset
    Dim str6 As String
    Dim rst7 As rdoResultset
    Dim str7 As String
    
    str1 = "select fr66codpeticion,fr66codomoriprn,fr26codestpetic,ci21codpersona,sg02cod_med," & _
                    "sg02cod_fen,fr66horafirmmedi,fr66horaredacci,fr91codurgencia," & _
                    "ad02coddpto,ad02coddpto_crg,fr66observ,fr66observ," & _
                    "fr66indpacidiabet,fr66indmedinf,fr66indom,fr66indrestvolum,fr66indintercient,fr66horafirmenf," & _
                    "TO_DATE(TO_CHAR(fr66fecfirmmedi,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE(TO_CHAR(fr66fecfirmenf,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE(TO_CHAR(fr66fecredaccion,'DD/MM/YYYY'),'DD/MM/YYYY') from fr6600 where fr66codpeticion=" & _
                    frmMonPetEstup.grdDBGrid1(1).Columns(17).Value
    Set rst1 = objApp.rdoConnect.OpenResultset(str1)
    
    str2 = "select * from fr2600 where fr26codestpetic=" & rst1("fr26codestpetic").Value
    Set rst2 = objApp.rdoConnect.OpenResultset(str2)
    
    str3 = "select * from ci2200 where ci21codpersona=" & rst1("ci21codpersona").Value
    Set rst3 = objApp.rdoConnect.OpenResultset(str3)
    
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    Frame1(3).Visible = False
    
    If IsNull(rst1("fr66codpeticion").Value) = False Then
        Text1(0).Text = rst1("fr66codpeticion").Value
    End If
    If IsNull(rst1("fr66codomoriprn").Value) = False Then
        Text1(1).Text = rst1("fr66codomoriprn").Value
    End If
    If IsNull(rst1("fr26codestpetic").Value) = False Then
        Text1(25).Text = rst1("fr26codestpetic").Value
    End If
    If IsNull(rst2("fr26desestadopet").Value) = False Then
        Text1(26).Text = rst2("fr26desestadopet").Value
    End If
    If IsNull(rst1("ci21codpersona").Value) = False Then
        Text1(2).Text = rst1("ci21codpersona").Value
    End If
    If IsNull(rst3("ci22numhistoria").Value) = False Then
        Text1(23).Text = rst3("ci22numhistoria").Value
    End If
    If IsNull(rst3("ci22nombre").Value) = False Then
        Text1(3).Text = rst3("ci22nombre").Value
    End If
    If IsNull(rst3("ci22priapel").Value) = False Then
        Text1(4).Text = rst3("ci22priapel").Value
    End If
    If IsNull(rst3("ci22segapel").Value) = False Then
        Text1(5).Text = rst3("ci22segapel").Value
    End If
    If IsNull(rst1("sg02cod_med").Value) = False Then
        Text1(8).Text = rst1("sg02cod_med").Value
    End If
    Text1(9).Text = frmMonPetEstup.grdDBGrid1(1).Columns(14).Value
    If IsNull(rst1("sg02cod_fen").Value) = False Then
        Text1(6).Text = rst1("sg02cod_fen").Value
        str4 = "select * from sg0200 where sg02cod='" & rst1("sg02cod_fen").Value & "'"
        Set rst4 = objApp.rdoConnect.OpenResultset(str4)
        If rst4.EOF = False Then
            Text1(7).Text = rst4("sg02nombre").Value
            Text1(19).Text = rst4("sg02ape1").Value
        End If
        rst4.Close
        Set rst4 = Nothing
    End If
    If IsNull(rst1("fr66horafirmmedi").Value) = False Then
        Text1(20).Text = rst1("fr66horafirmmedi").Value
    End If
    If IsNull(rst1("fr66horafirmenf").Value) = False Then
        Text1(21).Text = rst1("fr66horafirmenf").Value
    End If
    If IsNull(rst1("fr66horaredacci").Value) = False Then
        Text1(10).Text = rst1("fr66horaredacci").Value
    End If
    If IsNull(rst1("fr91codurgencia").Value) = False Then
        Text1(22).Text = rst1("fr91codurgencia").Value
        str5 = "select * from fr9100 where fr91codurgencia=" & rst1("fr91codurgencia").Value
        Set rst5 = objApp.rdoConnect.OpenResultset(str5)
        If rst5.EOF = False Then
            Text1(11).Text = rst5("fr91desurgencia").Value
        End If
        rst5.Close
        Set rst5 = Nothing
    End If
    If IsNull(rst1("ad02coddpto").Value) = False Then
        Text1(12).Text = rst1("ad02coddpto").Value
    End If
    Text1(13).Text = frmMonPetEstup.grdDBGrid1(1).Columns(11).Value
    If IsNull(rst1("ad02coddpto_crg").Value) = False Then
        Text1(14).Text = rst1("ad02coddpto_crg").Value
        str6 = "select * from ad0200 where ad02coddpto=" & rst1("ad02coddpto_crg").Value
        Set rst6 = objApp.rdoConnect.OpenResultset(str6)
        If rst6.EOF = False Then
            Text1(15).Text = rst6("ad02desdpto").Value
        End If
        rst6.Close
        Set rst6 = Nothing
    End If
    If IsNull(rst1("fr66observ").Value) = False Then
        Text1(18).Text = rst1("fr66observ").Value
    End If
    If rst1("fr66indpacidiabet").Value = -1 Then
        Check1(0).Value = 1
    Else
        Check1(0).Value = 0
    End If
    If rst1("fr66indmedinf").Value = -1 Then
        Check1(2).Value = 1
    Else
        Check1(2).Value = 0
    End If
    If rst1("fr66indom").Value = -1 Then
        Check1(12).Value = 1
    Else
        Check1(12).Value = 0
    End If
    If rst1("fr66indrestvolum").Value = -1 Then
        Check1(7).Value = 1
    Else
        Check1(7).Value = 0
    End If
    If rst1("fr66indintercient").Value = -1 Then
        Check1(15).Value = 1
    Else
        Check1(15).Value = 0
    End If
    If IsNull(rst1(19).Value) = False Then
        DateCombo1(1).Date = rst1(19).Value
    End If
    If IsNull(rst1(20).Value) = False Then
        DateCombo1(0).Date = rst1(20).Value
    End If
    If IsNull(rst1(21).Value) = False Then
        DateCombo1(2).Date = rst1(21).Value
    End If
    rst1.Close
    rst2.Close
    rst3.Close
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set rst3 = Nothing
End Sub
Private Sub cmdbloquear_Click()
    Dim strupdate As String
    Dim rsta As rdoResultset
    Dim stra As String
    
    cmdBloquear.Enabled = False
    Select Case frmMonPetEstup.grdDBGrid1(1).Columns(18).Value
        Case "Peticion Farmacia":
            If grdDBGrid1(0).Columns(29).Value <> 1 Then
                stra = "SELECT FR28INDBLOQUEADA FROM FR2800 WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
                        " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta(0) = -1 Then
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0" & _
                                " WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
                                " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
                    objApp.rdoConnect.Execute strupdate, 64
                Else
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1" & _
                                " WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
                                " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
                    objApp.rdoConnect.Execute strupdate, 64
                End If
                objWinInfo.DataRefresh
                
                rsta.Close
                Set rsta = Nothing
            Else
                Call MsgBox("El Producto no es un estupefaciente", vbExclamation)
            End If
        Case "Hoja Anestesia":
            If grdDBGrid1(0).Columns(11).Value <> 1 Then
                stra = "SELECT FR28INDBLOQUEADA FROM FR1700 WHERE FR43CODHOJAANEST=" & grdDBGrid1(0).Columns(4).Value & _
                        " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta(0) = -1 Then
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0" & _
                                " WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
                                " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
                    objApp.rdoConnect.Execute strupdate, 64
                Else
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1" & _
                                " WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
                                " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
                    objApp.rdoConnect.Execute strupdate, 64
                End If
                objWinInfo.DataRefresh
                
                rsta.Close
                Set rsta = Nothing
            Else
                Call MsgBox("El Producto no es un estupefaciente", vbExclamation)
            End If
        Case "Hoja Quirofano":
            glngPaciente = Text1(70).Text
        Case Else
            Exit Sub
    End Select
    cmdBloquear.Enabled = True
End Sub

Private Sub cmdFtp_Click()
    cmdFtp.Enabled = False
    Select Case frmMonPetEstup.grdDBGrid1(1).Columns(18).Value
        Case "Peticion Farmacia":
            glngPaciente = Text1(2).Text
        Case "Hoja Anestesia":
            glngPaciente = Text1(49).Text
        Case "Hoja Quirofano":
            glngPaciente = Text1(70).Text
        Case Else
            Exit Sub
    End Select
    Call objsecurity.LaunchProcess("FR0106")
    cmdFtp.Enabled = True
End Sub

Private Sub cmdHistoriaclinica_Click()
 Call MsgBox("Historia Cl�nica de Paciente")
End Sub

Private Sub Form_Activate()
    Select Case frmMonPetEstup.grdDBGrid1(1).Columns(18).Value
        Case "Peticion Farmacia":
            Call Rellenar_Petici�nFarmacia
        Case "Hoja Anestesia":
            Call Rellenar_HojaAnestesia
        Case "Hoja Quirofano":
            Call Rellenar_HojaQuirofano
        Case Else
            Exit Sub
    End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Ver OM"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        Select Case frmMonPetEstup.grdDBGrid1(1).Columns(18).Value
            Case "Peticion Farmacia":
                .strTable = "fr2800"
                .strWhere = "fr66codpeticion=" & frmMonPetEstup.grdDBGrid1(1).Columns(17).Value
                Call .FormAddOrderField("fr66codpeticion", cwAscending)
                Call .FormAddOrderField("fr28numlinea", cwAscending)
                
            Case "Hoja Anestesia":
                .strTable = "fr1700"
                .strWhere = "fr43codhojaanest=" & frmMonPetEstup.grdDBGrid1(1).Columns(17).Value
                Call .FormAddOrderField("fr17numlinea", cwAscending)
            Case "Hoja Quirofano":
                .strTable = "fr1800"
                .strWhere = "fr44codhojaquiro=" & frmMonPetEstup.grdDBGrid1(1).Columns(17).Value
                Call .FormAddOrderField("fr18numlinea", cwAscending)
        End Select
        strKey = .strDataBase & .strTable
        .intAllowance = cwAllowReadOnly
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
        Select Case frmMonPetEstup.grdDBGrid1(1).Columns(18).Value
            Case "Peticion Farmacia":
                Call .GridAddColumn(objMultiInfo, "Bloqueada", "FR28INDBLOQUEADA", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "C�digo", "FR66CODPETICION", cwNumeric, 9)
                Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
                Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
                Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
                Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwNumeric, 2)
                Call .GridAddColumn(objMultiInfo, "Desc.V�a", "", cwString, 30)
                Call .GridAddColumn(objMultiInfo, "Volumen", "FR28VOLUMEN", cwDecimal, 2)
                Call .GridAddColumn(objMultiInfo, "Tiempo Infusi�n", "FR28TIEMINFMIN", cwDecimal, 2)
                Call .GridAddColumn(objMultiInfo, "Cod Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
                Call .GridAddColumn(objMultiInfo, "Frecuencia", "", cwString, 60)
                Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "C.I", "FR28INDCOMIENINMED", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "S.N", "FR28INDSN", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "Cantidad", "FR28CANTCOMPU", cwDecimal, 2)
                Call .GridAddColumn(objMultiInfo, "Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
                Call .GridAddColumn(objMultiInfo, "Desc.Medida", "", cwString, 30)
                Call .GridAddColumn(objMultiInfo, "Grupo", "FR28GRUPO", cwNumeric, 5)
                Call .GridAddColumn(objMultiInfo, "1�d�a", "FR28FECADMPRIMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "2�d�a", "FR28FECADMSEGMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "3�d�a", "FR28FECADMTERMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "4�d�a", "FR28FECADMCUAMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "5�d�a", "FR28FECADMQUIMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "6�d�a", "FR28FECADMSEXMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "7�d�a", "FR28FECADMSEPMDIA", cwDate)
                Call .GridAddColumn(objMultiInfo, "Notas", "FR28NOTASCOMP", cwString, 2000)
                Call .GridAddColumn(objMultiInfo, "Estupefaciente", "", cwBoolean)
                
                Call .FormCreateInfo(objMultiInfo)
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(7), "FR73DESPRODUCTO")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR34DESVIA")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(13), "FRG4DESFRECUENCIA")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(18)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(18)), grdDBGrid1(0).Columns(19), "FR93DESUNIMEDIDA")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(29), "FR73INDESTUPEFACI")
                
                'grdDBGrid1(0).Columns(29).Visible = False
            Case "Hoja Anestesia":
                Call .GridAddColumn(objMultiInfo, "Bloqueada", "FR17INDBLOQUEO", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
                Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
                Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
                Call .GridAddColumn(objMultiInfo, "Cantidad", "FR17CANTIDAD", cwNumeric, 9)
                Call .GridAddColumn(objMultiInfo, "Cod Unidad Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
                Call .GridAddColumn(objMultiInfo, "Unidad Medida", "", cwString, 30)
                Call .GridAddColumn(objMultiInfo, "Leido HA", "FR17INDLEIDO", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "Estupefaciente", "", cwBoolean)
                
                Call .FormCreateInfo(objMultiInfo)
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73DESPRODUCTO")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR93DESUNIMEDIDA")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(11), "FR73INDESTUPEFAC")
                
                grdDBGrid1(0).Columns(11).Visible = False
                
            Case "Hoja Quirofano":
                Call .GridAddColumn(objMultiInfo, "Bloqueada", "FR18INDBLOQUEO", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "L�nea", "FR18NUMLINEA", cwNumeric, 3)
                Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
                Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
                Call .GridAddColumn(objMultiInfo, "Cantidad", "FR18CANTCONSUMIDA", cwNumeric, 9)
                Call .GridAddColumn(objMultiInfo, "Cod Unidad Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
                Call .GridAddColumn(objMultiInfo, "Unidad Medida", "", cwString, 30)
                Call .GridAddColumn(objMultiInfo, "Leido", "FR18INDLEIDO", cwBoolean)
                Call .GridAddColumn(objMultiInfo, "Estupefaciente", "", cwBoolean)
                
                Call .FormCreateInfo(objMultiInfo)
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73DESPRODUCTO")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR93DESUNIMEDIDA")
                
                Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
                Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(11), "FR73INDESTUPEFAC")
                
                grdDBGrid1(0).Columns(11).Visible = False
        End Select
        
  
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnMandatory = True
        
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
   
        Call .WinRegister
        Call .WinStabilize
    End With

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub
Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
Dim j As Integer

    Select Case frmMonPetEstup.grdDBGrid1(1).Columns(18).Value
        Case "Peticion Farmacia":
            j = 28
        Case "Hoja Anestesia":
            j = 10
        Case "Hoja Quirofano":
            j = 10
        Case Else
            Exit Sub
    End Select
    'If grdDBGrid1(0).Columns(j + 1).Value = 0 Then
    '    For i = 3 To j
    '        grdDBGrid1(0).Columns(i).CellStyleSet "Estupefaciente"
    '    Next i
    'Else
        If grdDBGrid1(0).Columns(3).Value <> 0 Then
            For i = 3 To j
                grdDBGrid1(0).Columns(i).CellStyleSet "Bloqueada"
            Next i
        End If
    'End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
End Sub



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


