VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmRealInvent 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Realizar Inventario"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   ForeColor       =   &H00808080&
   HelpContextID   =   30001
   Icon            =   "FR0042.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Inventario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   2
      Left            =   0
      TabIndex        =   15
      Tag             =   "Actuaciones Asociadas"
      Top             =   4200
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3375
         Index           =   2
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   11730
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20690
         _ExtentY        =   5953
         _StockProps     =   79
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdactinvent 
      Caption         =   "Actualizar Inventario"
      Height          =   375
      Left            =   9480
      TabIndex        =   14
      Top             =   3240
      Width           =   1815
   End
   Begin VB.CommandButton cmdejecutarrec 
      Caption         =   "Ejecutar Recuento"
      Height          =   375
      Left            =   9480
      TabIndex        =   13
      Top             =   2640
      Width           =   1815
   End
   Begin VB.Frame frame1 
      Caption         =   "Tipo de Recuento "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1335
      Index           =   0
      Left            =   9000
      TabIndex        =   10
      Top             =   840
      Width           =   2775
      Begin VB.TextBox txtdecimales 
         Height          =   285
         Left            =   2160
         MaxLength       =   2
         TabIndex        =   19
         Top             =   840
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtporcentaje 
         Height          =   285
         Left            =   1560
         MaxLength       =   4
         TabIndex        =   18
         Top             =   840
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Porcentaje "
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   17
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   600
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Aleatorio"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   360
         Width           =   975
      End
      Begin VB.Label lblcoma 
         Caption         =   ","
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2040
         TabIndex        =   20
         Top             =   880
         Visible         =   0   'False
         Width           =   135
      End
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   4080
      TabIndex        =   9
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   4080
      TabIndex        =   8
      Top             =   2280
      Width           =   615
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   4080
      TabIndex        =   7
      Top             =   1800
      Width           =   615
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   4080
      TabIndex        =   6
      Top             =   1320
      Width           =   615
   End
   Begin VB.Frame frame1 
      Caption         =   "Almacenes Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   1
      Left            =   4800
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   3975
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   3015
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   3690
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   2
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   5106
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         _ExtentX        =   6509
         _ExtentY        =   5318
         _StockProps     =   79
         Caption         =   "ALMACENES SELECCIONADOS"
         ForeColor       =   -2147483630
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Almacenes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   3975
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3015
         Index           =   0
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   3690
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   6509
         _ExtentY        =   5318
         _StockProps     =   79
         Caption         =   "ALMACENES"
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRealInvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRealInvent (FR0042.FRM)                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: realizar inventario                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim almacenes As String
Dim lista_productos As String


Private Sub Recuento_Aleatorio(almacen, tp, numproductos)
'funci�n que coge el % de productos a examinar y el n� de productos a examinar
'y devuelve una lista con los c�digos de los productos
Dim porcentaje As Variant
Dim i As Integer
Dim stra As String
Dim rsta As rdoResultset

If IsNull(tp) Then
    tp = 99
End If
porcentaje = (numproductos * tp) / 100
If porcentaje = 0 Then
    Exit Sub
End If
porcentaje = Int(porcentaje) + 1
stra = "SELECT FR73CODPRODUCTO FROM FR4700 WHERE " & _
         "FR04CODALMACEN=" & almacen
Set rsta = objApp.rdoConnect.OpenResultset(stra)
For i = 1 To porcentaje
  If Not rsta.EOF Then
    If lista_productos <> "" Then
        lista_productos = lista_productos & ","
    End If
    If Not rsta.EOF Then
        lista_productos = lista_productos & rsta.rdoColumns(0).Value
        rsta.MoveNext
    End If
  End If
Next i
rsta.Close
Set rsta = Nothing
End Sub

Private Sub cmdactinvent_Click()
Dim strinsertar As String
Dim strinsert1030 As String
Dim mensaje As String
Dim faltan As Boolean
Dim continuar As Boolean
Dim resultado As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim i As Integer
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim strsel As String
Dim rstsel As rdoResultset

continuar = False
faltan = False
objWinInfo.objWinActiveForm.blnChanged = False
'se mira en la variable <faltan> si alguna existencia real no se ha metido
grdDBGrid1(2).MoveFirst
For i = 0 To grdDBGrid1(2).Rows - 1
  If grdDBGrid1(2).Columns(10).Value = "" Then
    faltan = True
    Exit For
  End If
  grdDBGrid1(2).MoveNext
Next i
If faltan = True Then
    mensaje = MsgBox("Todas las existencias reales no est�n introducidas." & Chr(13) & _
                     "�Desea continuar?", vbYesNo)
    If mensaje = vbNo Then
       Exit Sub
    Else
       continuar = True
    End If
End If
    
If continuar = True Or faltan = False Then
    grdDBGrid1(2).MoveFirst
    For i = 0 To grdDBGrid1(2).Rows - 1
        If grdDBGrid1(2).Columns(10).Value <> "" And grdDBGrid1(2).Columns(8).Value <> "" Then
            resultado = grdDBGrid1(2).Columns(8).Value - grdDBGrid1(2).Columns(10).Value
               strinsertar = "UPDATE FR4700 SET " & _
                            "FR47ACUSALIDA=FR47ACUSALIDA+" & resultado & _
                            " WHERE FR04CODALMACEN=" & grdDBGrid1(2).Columns(3).Value & _
                            " AND FR73CODPRODUCTO=" & grdDBGrid1(2).Columns(5).Value
            objApp.rdoConnect.Execute strinsertar, 64
            objApp.rdoConnect.Execute "Commit", 64
        End If
        If grdDBGrid1(2).Columns(10).Value <> "" And grdDBGrid1(2).Columns(8).Value <> "" Then
            'fecha actual
            strfecha = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
            Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
            'se mira si ya existe tupla en FRA500 para hacer insert o update
            strsel = "SELECT COUNT(*) FROM FRA500 WHERE " & _
                     "FR04CODALMACEN=" & grdDBGrid1(2).Columns(3).Value & " AND " & _
                     "SG02COD='" & objsecurity.strUser & "' AND " & _
                     "FR73CODPRODUCTO=" & grdDBGrid1(2).Columns(5).Value & " AND " & _
                     "FRA5FECRECALE=" & "TO_DATE('" & rstfecha.rdoColumns(0).Value & "','DD/MM/YYYY HH24:MI')"
            Set rstsel = objApp.rdoConnect.OpenResultset(strsel)
            If rstsel.rdoColumns(0).Value = 0 Then
                strinsert1030 = "INSERT INTO FRA500 (FR04CODALMACEN,SG02COD,FR73CODPRODUCTO," & _
                      "FRA5FECRECALE,FRA5EXISTBD,FRA5EXISTREAL)" & _
                      " VALUES (" & _
                      grdDBGrid1(2).Columns(3).Value & "," & _
                      "'" & objsecurity.strUser & "'" & "," & _
                      grdDBGrid1(2).Columns(5).Value & "," & _
                      "TO_DATE('" & rstfecha.rdoColumns(0).Value & "','DD/MM/YYYY HH24:MI')" & "," & _
                      grdDBGrid1(2).Columns(8).Value & "," & _
                      grdDBGrid1(2).Columns(10).Value & ")"
            Else
                strinsert1030 = "UPDATE FRA500 SET " & _
                                "FRA5EXISTBD=" & grdDBGrid1(2).Columns(8).Value & "," & _
                                "FRA5EXISTREAL=" & grdDBGrid1(2).Columns(10).Value
                strinsert1030 = strinsert1030 & " WHERE " & _
                     "FR04CODALMACEN=" & grdDBGrid1(2).Columns(3).Value & " AND " & _
                     "SG02COD='" & objsecurity.strUser & "' AND " & _
                     "FR73CODPRODUCTO=" & grdDBGrid1(2).Columns(5).Value & " AND " & _
                     "FRA5FECRECALE=" & "TO_DATE('" & rstfecha.rdoColumns(0).Value & "','DD/MM/YYYY HH24:MI') AND FR04CODALMACEN=" & grdDBGrid1(2).Columns(3).Value
            End If
            rstsel.Close
            Set rstsel = Nothing
            rstfecha.Close
            Set rstfecha = Nothing
            objApp.rdoConnect.Execute strinsert1030, 64
            objApp.rdoConnect.Execute "Commit", 64
       End If
     grdDBGrid1(2).MoveNext
     Next i
End If
'RECUENTO TOTAL
If Option2.Value = True Then
 If almacenes <> "" Then
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IN " & "(" & almacenes & ")"
 End If
End If
'RECUENTO ALEATORIO
If Option1.Value = True Then
 If almacenes <> "" And lista_productos <> "" Then
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IN " & "(" & almacenes & ")" & " AND FR73CODPRODUCTO IN " & "(" & lista_productos & ")"
 End If
End If
'PORCENTAJE PUNTUAL
If Option3.Value = True And txtporcentaje.Text <> "" Then
 If almacenes <> "" And lista_productos <> "" Then
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IN " & "(" & almacenes & ")" & " AND FR73CODPRODUCTO IN " & "(" & lista_productos & ")"
 End If
End If

objWinInfo.DataRefresh
End Sub

Private Sub cmdejecutarrec_Click()
Dim resultado As Variant
Dim i As Integer
'se calcula el % de error
objWinInfo.objWinActiveForm.blnChanged = False
grdDBGrid1(2).MoveFirst
For i = 0 To grdDBGrid1(2).Rows - 1
  If grdDBGrid1(2).Columns(10).Value <> "" And grdDBGrid1(2).Columns(8).Value <> "" Then
        resultado = (grdDBGrid1(2).Columns(10).Value - grdDBGrid1(2).Columns(8).Value)
        resultado = (resultado / grdDBGrid1(2).Columns(8).Value) * 100
        grdDBGrid1(2).Columns(11).Value = Format(resultado, "###0.00") 's�lo 2 decimales

  End If
  grdDBGrid1(2).MoveNext
Next i
End Sub

Private Sub cmdrellenar_Click(Index As Integer)
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mvarfilatotal As Variant
Dim filas() As Integer
Dim filasordenadas() As Integer
Dim i As Integer
Dim j As Integer
Dim v As Integer
Dim max As Integer
Dim insertar As Integer


Select Case Index
Case 0
  insertar = 0
  'se mira que no se meta el mismo almac�n 2 veces
  Grid1(1).MoveFirst
  If Grid1(1).Rows > 0 Then
        For mintisel = 0 To Grid1(1).Rows - 1
            If Grid1(1).Columns(0).Value <> grdDBGrid1(0).Columns(3).Value Then
              insertar = 1
            Else
              insertar = 0
              Exit For
            End If
        Grid1(1).MoveNext
        Next mintisel
        If insertar = 1 Then
           insertar = 0
           Grid1(1).AddNew
           Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).Value
           Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).Value
           Grid1(1).Update
       End If
  Else
        Grid1(1).AddNew
        Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).Value
        Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).Value
        Grid1(1).Update
  End If
Case 1
  'se mira que no se meta el mismo almac�n 2 veces
  insertar = 0
  mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
  For mintisel = 0 To mintNTotalSelRows - 1
    Grid1(1).MoveFirst
    If Grid1(1).Rows > 0 Then
        For i = 0 To Grid1(1).Rows - 1
            mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
            If Grid1(1).Columns(0).Value <> grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk) Then
                insertar = 1
            Else
                insertar = 0
                Exit For
            End If
        Grid1(1).MoveNext
        Next i
        If insertar = 1 Then
                insertar = 0
                Grid1(1).AddNew
                Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk)
                Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
                Grid1(1).Update
        End If
    Else
        mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
        Grid1(1).AddNew
        Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk)
        Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
        Grid1(1).Update
    End If
  Next mintisel
Case 2
  If Grid1(1).Rows = 0 Or Grid1(1).Rows = 1 Then
    Grid1(1).RemoveAll
  Else
    mvarBkmrk = Grid1(1).Bookmark
    mvarfilatotal = Grid1(1).AddItemRowIndex(mvarBkmrk)
    Call Grid1(1).RemoveItem(mvarfilatotal)
  End If
Case 3
        mintNTotalSelRows = Grid1(1).SelBookmarks.Count
        ReDim filas(mintNTotalSelRows)
        ReDim filasordenadas(mintNTotalSelRows)
        For mintisel = 0 To mintNTotalSelRows - 1
          mvarBkmrk = Grid1(1).SelBookmarks(mintisel)
          mvarfilatotal = Grid1(1).AddItemRowIndex(mvarBkmrk)
          filas(mintisel) = mvarfilatotal
        Next mintisel
        'se ordena el array <filas> de mayor a menor en el array <filasordenadas>
        For j = 0 To mintNTotalSelRows - 1
            max = filas(0)
            For i = 0 To mintNTotalSelRows - 1
                If filas(i) >= max Then
                    max = filas(i)
                    v = i
                End If
            Next i
            filas(v) = 0
            filasordenadas(j) = max
        Next j
        For mintisel = 0 To mintNTotalSelRows - 1
          Call Grid1(1).RemoveItem(filasordenadas(mintisel))
        Next mintisel
End Select

'se meten en una lista los c�digos de los almacenes seleccionados para refrescar
'el grid de inventario
almacenes = ""
Grid1(1).MoveFirst
If Grid1(1).Rows = 0 Then
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN is null"
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Exit Sub
End If

If Grid1(1).Rows = 1 Then
    almacenes = almacenes & Grid1(1).Columns(0).Value
Else
    almacenes = almacenes & Grid1(1).Columns(0).Value
    Grid1(1).MoveNext
    For i = 1 To Grid1(1).Rows - 1
        almacenes = almacenes & "," & Grid1(1).Columns(0).Value
        Grid1(1).MoveNext
    Next i
End If


'RECUENTO TOTAL
If Option2.Value = True Then
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IN " & "(" & almacenes & ")"
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
'RECUENTO ALEATORIO
Dim indice As Integer
Dim rsta As rdoResultset
Dim stra As String
Dim rstprod As rdoResultset
Dim strprod As String

If Option1.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 Then
    Grid1(1).MoveFirst
    For indice = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira el % de productos a examinar
      stra = "SELECT FR04TPEXAMRECALE FROM FR0400 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      'para cada almac�n se mira cu�ntos productos tiene
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, rsta.rdoColumns(0).Value, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next indice
 
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If lista_productos <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR73CODPRODUCTO IN (" & lista_productos & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If

'PORCENTAJE PUNTUAL
Dim k As Integer
'Dim rstprod As rdoResultset
'Dim strprod As String
Dim strporcentaje As String

If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For k = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next k
 
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If lista_productos <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR73CODPRODUCTO IN (" & lista_productos & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
'si no hay ning�n porcentaje introducido que se limpie el grid
If txtporcentaje.Text = "" Then
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If

End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm
    Dim objMultiInfo1 As New clsCWForm
    

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Almacenes"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR0400"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR04CODALMACEN", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Almacenes")
        Call .FormAddFilterWhere(strKey, "FR04CODALMACEN", "C�digo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR04DESALMACEN", "Descripci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR04CODALMACEN", "C�digo")
        Call .FormAddFilterOrder(strKey, "FR04DESALMACEN", "Descripci�n")
    
    End With
    
    With objMultiInfo1
        .strName = "Log de Recuento Aleatorio"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(2)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR4701J"
        .intAllowance = cwAllowModify
        .strWhere = "FR04CODALMACEN IS NULL"
        
        Call .FormAddOrderField("FR04CODALMACEN", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        'Call .FormCreateFilterWhere(strKey, "Almacenes")
        'Call .FormAddFilterWhere(strKey, "FR04CODALMACEN", "C�digo", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "FR04DESALMACEN", "Descripci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        'Call .FormAddFilterOrder(strKey, "FR04CODALMACEN", "C�digo")
        'Call .FormAddFilterOrder(strKey, "FR04DESALMACEN", "Descripci�n")
    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
        Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo", "FR04CODALMACEN", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Descripci�n", "FR04DESALMACEN", cwString, 30)
        
        Call .GridAddColumn(objMultiInfo1, "C�digo Almac�n", "FR04CODALMACEN", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo1, "Almac�n", "FR04DESALMACEN", cwString, 30)
        Call .GridAddColumn(objMultiInfo1, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo1, "Producto", "FR73DESPRODUCTO", cwString, 50)
        'como sobran las columnas Ubicaci�n y %Error m�x se ponen sin campo de la BD
        'y despu�s se ponen invisibles
        'Call .GridAddColumn(objMultiInfo1, "Ubicaci�n", "FR47UBICACION", cwString, 30)
        Call .GridAddColumn(objMultiInfo1, "Ubicaci�n", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo1, "Exist.BD", "EXISTENCIAS_BD", cwDecimal, 14)
        'Call .GridAddColumn(objMultiInfo1, "%Error m�x", "FR47TPMAXERROR", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo1, "%Error m�x", "", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo1, "Exist.Reales", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo1, "%Error Real", "", cwDecimal, 2)
  
        Call .FormCreateInfo(objMultiInfo)
        Call .FormCreateInfo(objMultiInfo1)
    
       
        Call .FormChangeColor(objMultiInfo)
        Call .FormChangeColor(objMultiInfo1)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
        
        '.CtrlGetInfo(txtporcentaje).blnNegotiated = False
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
grdDBGrid1(0).Columns(3).Visible = False

grdDBGrid1(2).Columns(4).Width = 2900
grdDBGrid1(2).Columns(6).Width = 4000
grdDBGrid1(2).Columns(8).Width = 1100
grdDBGrid1(2).Columns(10).Width = 1100
grdDBGrid1(2).Columns(11).Width = 1100

grdDBGrid1(2).Columns(10).BackColor = &HFFC0FF
grdDBGrid1(2).Columns(3).Visible = False
grdDBGrid1(2).Columns(5).Visible = False
grdDBGrid1(2).Columns(7).Visible = False 'ubicaci�n
grdDBGrid1(2).Columns(9).Visible = False '%error m�x
    
'se refresca el grid de almacenes
Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
objWinInfo.DataRefresh

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

Private Sub Option1_Click()
'Recuento Aleatorio
Dim i As Integer
Dim rsta As rdoResultset
Dim stra As String
Dim rstprod As rdoResultset
Dim strprod As String

txtporcentaje.Visible = False
txtdecimales.Visible = False
lblcoma.Visible = False
If Option1.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 Then
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira el % de productos a examinar
      stra = "SELECT FR04TPEXAMRECALE FROM FR0400 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      'para cada almac�n se mira cu�ntos productos tiene
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, rsta.rdoColumns(0).Value, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If lista_productos <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR73CODPRODUCTO IN (" & lista_productos & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If
End Sub

Private Sub Option2_Click()
'Recuento total
txtporcentaje.Visible = False
txtdecimales.Visible = False
lblcoma.Visible = False
If Option2.Value = True Then
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If almacenes <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End Sub

Private Sub Option3_Click()
'Porcentaje Puntual. El usuario introduce el porcentaje que quiera en txtporcentaje y
'txtdecimales y as� no se coge por defecto el de la definici�n.
Dim i As Integer
Dim rstprod As rdoResultset
Dim strprod As String
Dim strporcentaje As String

If Option3.Value = True Then
    txtporcentaje.Visible = True
    txtdecimales.Visible = True
    lblcoma.Visible = True
Else
    txtporcentaje.Visible = False
    txtdecimales.Visible = False
    lblcoma.Visible = False
End If

If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If lista_productos <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR73CODPRODUCTO IN (" & lista_productos & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
'si no hay ning�n porcentaje introducido que se limpie el grid
If txtporcentaje.Text = "" Then
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If

End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
objWinInfo.objWinActiveForm.blnChanged = False
'<>Guardar
If btnButton.Index <> 4 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
Else
    MsgBox "Para guardar el inventario pulse <Actualizar Inventario>", vbInformation
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
objWinInfo.objWinActiveForm.blnChanged = False
'<>Guardar
If intIndex <> 40 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
Else
    MsgBox "Para guardar el inventario pulse <Actualizar Inventario>", vbInformation
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.CtrlGotFocus
If intIndex = 2 Then
    grdDBGrid1(2).Columns(3).Locked = True
    grdDBGrid1(2).Columns(4).Locked = True
    grdDBGrid1(2).Columns(5).Locked = True
    grdDBGrid1(2).Columns(6).Locked = True
    grdDBGrid1(2).Columns(7).Locked = True
    grdDBGrid1(2).Columns(8).Locked = True
    grdDBGrid1(2).Columns(9).Locked = True
    grdDBGrid1(2).Columns(11).Locked = True
End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.GridDblClick
If intIndex = 2 Then
    grdDBGrid1(2).Columns(3).Locked = True
    grdDBGrid1(2).Columns(4).Locked = True
    grdDBGrid1(2).Columns(5).Locked = True
    grdDBGrid1(2).Columns(6).Locked = True
    grdDBGrid1(2).Columns(7).Locked = True
    grdDBGrid1(2).Columns(8).Locked = True
    grdDBGrid1(2).Columns(9).Locked = True
    grdDBGrid1(2).Columns(11).Locked = True
End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
If intIndex = 2 Then
    grdDBGrid1(2).Columns(3).Locked = True
    grdDBGrid1(2).Columns(4).Locked = True
    grdDBGrid1(2).Columns(5).Locked = True
    grdDBGrid1(2).Columns(6).Locked = True
    grdDBGrid1(2).Columns(7).Locked = True
    grdDBGrid1(2).Columns(8).Locked = True
    grdDBGrid1(2).Columns(9).Locked = True
    grdDBGrid1(2).Columns(11).Locked = True
End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    objWinInfo.objWinActiveForm.blnChanged = False
    Call objWinInfo.CtrlDataChange
If intIndex = 2 Then
    grdDBGrid1(2).Columns(3).Locked = True
    grdDBGrid1(2).Columns(4).Locked = True
    grdDBGrid1(2).Columns(5).Locked = True
    grdDBGrid1(2).Columns(6).Locked = True
    grdDBGrid1(2).Columns(7).Locked = True
    grdDBGrid1(2).Columns(8).Locked = True
    grdDBGrid1(2).Columns(9).Locked = True
    grdDBGrid1(2).Columns(11).Locked = True
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtdecimales_Change()
If txtdecimales.Text <> "" Then
    If Not IsNumeric(txtdecimales.Text) Then
        Call MsgBox("Datos no num�ricos", vbCritical, "Aviso")
        txtdecimales.Text = ""
    End If
End If
End Sub

Private Sub txtdecimales_KeyPress(KeyAscii As Integer)
'Porcentaje Puntual. El usuario introduce el porcentaje que quiera en txtporcentaje y
'txtdecimales y as� no se coge por defecto el de la definici�n.
Dim i As Integer
Dim rstprod As rdoResultset
Dim strprod As String
Dim strporcentaje As String

If KeyAscii = 13 Then   'si se pulsa Return
If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If lista_productos <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR73CODPRODUCTO IN (" & lista_productos & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If
End If
End Sub

Private Sub txtporcentaje_Change()
If txtporcentaje.Text <> "" Then
    If Not IsNumeric(txtporcentaje.Text) Then
        Call MsgBox("Datos no num�ricos", vbCritical, "Aviso")
        txtporcentaje.Text = ""
    End If
End If
End Sub



Private Sub txtporcentaje_KeyPress(KeyAscii As Integer)

'Porcentaje Puntual. El usuario introduce el porcentaje que quiera en txtporcentaje y
'txtdecimales y as� no se coge por defecto el de la definici�n.
Dim i As Integer
Dim rstprod As rdoResultset
Dim strprod As String
Dim strporcentaje As String

If KeyAscii = 13 Then   'si se pulsa Return
If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
           Grid1(1).Columns(0).Value
      Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If lista_productos <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR73CODPRODUCTO IN (" & lista_productos & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")"
    Else
        objWinInfo.objWinActiveForm.strWhere = "FR04CODALMACEN IS NULL"
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If
End If
End Sub
