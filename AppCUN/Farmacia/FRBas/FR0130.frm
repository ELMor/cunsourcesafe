VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmValNecQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Validar Necesidades de Quir�fano"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdValid 
      Caption         =   "Generar Necesidades Validadas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   6
      Top             =   1200
      Width           =   1095
   End
   Begin VB.CommandButton cmdbuscarprod 
      Caption         =   "Sustituir Producto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   5
      Top             =   4800
      Width           =   1095
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Usuario"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   10380
      Begin VB.TextBox txtApellido 
         Height          =   330
         Index           =   2
         Left            =   4920
         TabIndex        =   10
         Tag             =   "Apellido"
         Top             =   720
         Width           =   3285
      End
      Begin VB.TextBox txtPersona 
         Height          =   330
         Index           =   0
         Left            =   240
         TabIndex        =   8
         Tag             =   "C�digo Persona"
         Top             =   720
         Width           =   1155
      End
      Begin VB.TextBox txtNombre 
         Height          =   330
         Index           =   1
         Left            =   1560
         TabIndex        =   7
         Tag             =   "Nombre"
         Top             =   720
         Width           =   3285
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecha 
         Height          =   330
         Index           =   0
         Left            =   240
         TabIndex        =   13
         Tag             =   "Fecha Validaci�n"
         Top             =   1320
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fecha Validaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   240
         TabIndex        =   14
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Apellido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   4920
         TabIndex        =   12
         Top             =   480
         Width           =   690
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1560
         TabIndex        =   11
         Top             =   480
         Width           =   660
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Persona"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Necesidades Validadas Quir�fano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5400
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   2520
      Width           =   10335
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4875
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   10065
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17754
         _ExtentY        =   8599
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmValNecQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmValNecQuirof (FR0130.FRM)                                 *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Validar Necesidades Quir�fano                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim fila As Integer

  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar una linea."
    Exit Sub
  End If
  fila = grdDBGrid1(0).Row
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call objWinInfo.DataSave
  End If
  
  cmdbuscarprod.Enabled = False
  
'  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
'    MsgBox "Debe seleccionar una linea."
'  Else
    
    noinsertar = True
    Call objsecurity.LaunchProcess("FR0119")
    If gintprodtotal > 0 Then
      If gintprodtotal > 1 Then
        MsgBox "Debe traer s�lo 1 producto."
      Else
          'For v = 0 To gintprodtotal - 1
          'se mira que no se inserte 2 veces el mismo producto
          'If grdDBGrid1(0).Rows > 0 Then
          '  grdDBGrid1(0).MoveFirst
          '  For i = 0 To grdDBGrid1(0).Rows - 1
          '    If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
          '       And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
          '      'no se inserta el producto pues ya est� insertado en el grid
          '      noinsertar = False
          '      Exit For
          '    Else
          '      noinsertar = True
          '    End If
          '    grdDBGrid1(0).MoveNext
          '  Next i
          'End If
          'If noinsertar = True Then
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Row = fila
            grdDBGrid1(0).Col = 5
            grdDBGrid1(0).Columns(5).Value = ""
            SendKeys gintprodbuscado(v, 0)
            Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(6), gintprodbuscado(v, 1))
            Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(7), gintprodbuscado(v, 2))
          'Else
          '  mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          '  Chr(13), vbInformation)
          'End If
          'noinsertar = True
        'Next v
      End If
    End If
    gintprodtotal = 0
'  End If
  cmdbuscarprod.Enabled = True

End Sub

Private Sub cmdValid_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strb As String
Dim rstb As rdoResultset
Dim strc As String
Dim rstc As rdoResultset
Dim strInsert As String
Dim strupdate As String
Dim CodNecesVal As Variant

  cmdValid.Enabled = False
  
  stra = "SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=1"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    strb = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & rsta(0).Value
    Set rstb = objApp.rdoConnect.OpenResultset(strb)
    While Not rstb.EOF
      strc = "SELECT FR19CODNECESVAL_SEQUENCE.nextval FROM dual"
      Set rstc = objApp.rdoConnect.OpenResultset(strc)
      CodNecesVal = rstc(0).Value
      rstc.Close
      Set rstc = Nothing
      
      strInsert = "INSERT INTO FR1900 "
      strInsert = strInsert & "(FR20NUMLINEA,FR55CODNECESUNID"
      strInsert = strInsert & ",FR19CODNECESVAL,FR73CODPRODUCTO"
      strInsert = strInsert & ",FR93CODUNIMEDIDA,FR19CANTNECESQUIR"
      strInsert = strInsert & ",SG02COD_VAL,FR19FECVALIDACION"
      strInsert = strInsert & ",FR93CODUNIMEDIDA_SUM,FR19CANTSUMIFARM)"
      strInsert = strInsert & " VALUES (" & rstb("FR20NUMLINEA").Value
      strInsert = strInsert & "," & rstb("FR55CODNECESUNID").Value
      strInsert = strInsert & "," & CodNecesVal
      strInsert = strInsert & "," & rstb("FR73CODPRODUCTO").Value
      strInsert = strInsert & "," & "'" & rstb("FR93CODUNIMEDIDA").Value & "'"
      strInsert = strInsert & "," & rstb("FR20CANTNECESQUIR").Value
      strInsert = strInsert & ",'" & objsecurity.strUser & "'"
      strInsert = strInsert & ",TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
      strInsert = strInsert & "," & "'" & rstb("FR93CODUNIMEDIDA").Value & "'"
      strInsert = strInsert & ",0"
      strInsert = strInsert & ")"
      objApp.rdoConnect.Execute strInsert, 64
      
      strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=4 WHERE FR55CODNECESUNID=" & rsta(0).Value 'VALIDADA
      objApp.rdoConnect.Execute strupdate, 64
      
      rstb.MoveNext
    Wend
    rstb.Close
    Set rstb = Nothing
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  
  Call objWinInfo.DataRefresh
  
  cmdValid.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim stra As String
  Dim rsta As rdoResultset
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Validadas Quirofano"
    
    .intAllowance = cwAllowDelete + cwAllowModify
    .strTable = "FR1900"
    
    Call .FormAddOrderField("FR19CODNECESVAL", cwAscending)
    
    .strWhere = " FR19FECVALIDACION >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') " & _
                " AND FR55CODNECESUNID IN (SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC in (1,4))" 'Redactada,Validada
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "N�m.L�nea", "FR20NUMLINEA", cwNumeric, 3) '3
    Call .GridAddColumn(objMultiInfo, "C�d.Nec.Val.", "FR19CODNECESVAL", cwNumeric, 9) '4
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9) '5
    Call .GridAddColumn(objMultiInfo, "Prod.", "", cwNumeric, 7) '6
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50) '7
    Call .GridAddColumn(objMultiInfo, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwString, 5) '8
    Call .GridAddColumn(objMultiInfo, "Desc.Medida", "", cwString, 30) '9
    Call .GridAddColumn(objMultiInfo, "Cant.Neces.Quir�.", "FR19CANTNECESQUIR", cwDecimal, 11) '10
    Call .GridAddColumn(objMultiInfo, "Cant.Suminis.Farma.", "FR19CANTSUMIFARM", cwDecimal, 11) '11
    Call .GridAddColumn(objMultiInfo, "C�d.Uni.Medi.Sumi.", "FR93CODUNIMEDIDA_SUM", cwNumeric, 5) '12
    Call .GridAddColumn(objMultiInfo, "Desc.Medida Sumi.", "", cwString, 30) '13
    Call .GridAddColumn(objMultiInfo, "C�d.Nec.Quiro.", "FR55CODNECESUNID", cwNumeric, 9) '14
    Call .GridAddColumn(objMultiInfo, "C�d. Persona Valida", "SG02COD_VAL", cwString, 6) '15
    Call .GridAddColumn(objMultiInfo, "Fecha Validaci�n", "FR19FECVALIDACION", cwDate) '16
  
    Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria

    Call .FormChangeColor(objMultiInfo)

    txtPersona(0).Text = objsecurity.strUser
    stra = "SELECT SG02NOM,SG02APE1 FROM SG0200 WHERE SG02COD ='" & objsecurity.strUser & "'"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    txtNombre(1).Text = rsta(0).Value
    txtApellido(2).Text = rsta(1).Value
    rsta.Close
    Set rsta = Nothing
    
    stra = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    dtcFecha(0).Date = rsta(0).Value
    rsta.Close
    Set rsta = Nothing
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "FR93CODUNIMEDIDA_SUM", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(13), "FR93DESUNIMEDIDA")
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnReadOnly = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

grdDBGrid1(0).Columns(3).Width = 990
grdDBGrid1(0).Columns(4).Width = 1100
grdDBGrid1(0).Columns(5).Width = 1230
grdDBGrid1(0).Columns(6).Width = 915
grdDBGrid1(0).Columns(7).Width = 6495
grdDBGrid1(0).Columns(8).Width = 1305
grdDBGrid1(0).Columns(9).Width = 3900
grdDBGrid1(0).Columns(10).Width = 1425
grdDBGrid1(0).Columns(11).Width = 1575
grdDBGrid1(0).Columns(12).Width = 1575
grdDBGrid1(0).Columns(13).Width = 3900
grdDBGrid1(0).Columns(14).Width = 1260
grdDBGrid1(0).Columns(15).Width = 1575
grdDBGrid1(0).Columns(16).Width = 1395

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim stra As String
Dim rsta As rdoResultset
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 0 And grdDBGrid1(0).Columns(5).Value <> "" Then
      stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta(0).Value <> grdDBGrid1(0).Columns(8).Value Then
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(8), rsta(0).Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(12), rsta(0).Value)
      End If
      rsta.Close
      Set rsta = Nothing
    End If

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


