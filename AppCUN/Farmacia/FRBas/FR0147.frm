VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmConfNecQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Confeccionar Necesidades de Quir�fano"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdbuscarprod 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10680
      TabIndex        =   22
      Top             =   6000
      Width           =   975
   End
   Begin VB.CommandButton cmdbuscargruprod 
      Caption         =   "Grupo Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10680
      TabIndex        =   21
      Top             =   5280
      Width           =   975
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n Necesidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   600
      Width           =   10860
      Begin TabDlg.SSTab tabTab1 
         Height          =   2655
         Index           =   0
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   4683
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0147.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(13)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(20)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0147.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   5
            Left            =   120
            TabIndex        =   3
            Tag             =   "C�d.Servicio"
            Top             =   1560
            Width           =   555
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   720
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio"
            Top             =   1560
            Width           =   5445
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   1080
            TabIndex        =   2
            Tag             =   "Apellido Peticionario"
            Top             =   960
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   3
            Left            =   120
            TabIndex        =   1
            Tag             =   "C�digo Peticionario"
            Top             =   960
            Width           =   915
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   3720
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   3360
            TabIndex        =   6
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Necesidad Unidad"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2385
            Index           =   2
            Left            =   -74880
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   120
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   4207
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   5
            Tag             =   "Fecha Petici�n"
            Top             =   2160
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   20
            Top             =   1320
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   19
            Top             =   1920
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   18
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3360
            TabIndex        =   17
            Top             =   120
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   16
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4200
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   3720
      Width           =   10335
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3675
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   10065
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17754
         _ExtentY        =   6482
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmConfNecQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmConfNecQuirof (FR0147.FRM)                                *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Confeccionar Necesidades Quir�fano                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

  cmdbuscargruprod.Enabled = False
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    If grdDBGrid1(2).Rows = 0 Then
      cmdbuscargruprod.Enabled = True
      Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
      Exit Sub
    End If
    Call objsecurity.LaunchProcess("FR0120")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 2)
          stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(8).Value = rsta(0).Value
          rsta.Close
          Set rsta = Nothing
          stra = "SELECT FR93DESUNIMEDIDA FROM FR9300 WHERE FR93CODUNIMEDIDA = " & grdDBGrid1(0).Columns(8).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(9).Value = rsta(0).Value
          rsta.Close
          Set rsta = Nothing
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
  cmdbuscargruprod.Enabled = True
  
End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

  cmdbuscargruprod.SetFocus
  cmdbuscarprod.Enabled = False
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    If grdDBGrid1(2).Rows = 0 Then
      cmdbuscarprod.Enabled = True
      Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
      Exit Sub
    End If
    Call objsecurity.LaunchProcess("FR0119")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 2)
          stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(8).Value = rsta(0).Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(8).Value = "" Then
            grdDBGrid1(0).Columns(8).Value = "NE"
          End If
          stra = "SELECT FR93DESUNIMEDIDA FROM FR9300 WHERE FR93CODUNIMEDIDA = '" & grdDBGrid1(0).Columns(8).Value & "'"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(9).Value = rsta(0).Value
          rsta.Close
          Set rsta = Nothing
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
  cmdbuscarprod.Enabled = True

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Necesidad Unidad"
    .blnAskPrimary = False
    .strTable = "FR5500"
    .strWhere = "FR26CODESTPETIC=1" 'REDACTADA
    
    Call .FormAddOrderField("FR55CODNECESUNID", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
    Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
    Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
    Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle Necesidad"
    
    .intAllowance = cwAllowDelete + cwAllowModify
    
    .strTable = "FR2000"
    
    Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
    Call .FormAddRelation("FR55CODNECESUNID", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d. Nec. Quir�fano", "FR55CODNECESUNID", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "N�mero L�nea", "FR20NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "C�d. Unidad Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Cant. Necesidad Quir�fano", "FR20CANTNECESQUIR", cwNumeric, 11)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR93DESUNIMEDIDA")
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnReadOnly = True
    .CtrlGetInfo(txtText1(3)).blnForeign = True
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  grdDBGrid1(0).Columns(3).Width = 9 * 180
  grdDBGrid1(0).Columns(4).Width = 7 * 180
  grdDBGrid1(0).Columns(5).Width = 8 * 180
  grdDBGrid1(0).Columns(8).Width = 9 * 180
  grdDBGrid1(0).Columns(10).Width = 12 * 180
  grdDBGrid1(0).RemoveAll
  grdDBGrid1(0).Refresh


End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset

  If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
    sqlstrPreg = "SELECT * FROM FR2000 WHERE "
    sqlstrPreg = sqlstrPreg & " FR55CODNECESUNID=" & txtText1(0).Text
    Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
    If rstPreg.EOF Then
      strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
      " no tiene asociado ningun Producto." & Chr(13) & _
      "� Desea borrar dicha Necesidad ?" & Chr(13) & _
      "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
      "asociarle al menos un Producto. ", 36, "Necesidades")
      If strPregunta = vbYes Then
        strdelete = "DELETE FROM FR5500 WHERE FR55CODNECESUNID = " & txtText1(0).Text
        objApp.rdoConnect.Execute strdelete, 64
        objWinInfo.objWinActiveForm.blnChanged = False
      Else
        rstPreg.Close
        Set rstPreg = Nothing
        intCancel = 1
        Exit Sub
      End If
    End If
    rstPreg.Close
    Set rstPreg = Nothing
    intCancel = objWinInfo.WinExit
  Else
    intCancel = objWinInfo.WinExit
  End If
  
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txtText1(3)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Peticionario"

     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(3), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txtText1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = " WHERE AD32CODTIPODPTO=3 AND " & _
         "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
         "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
  'If strFormName = "Detalle Necesidad" And strCtrl = "grdDBGrid1(0).C�digo Producto" Then
  '  Set objSearch = New clsCWSearch
  '  With objSearch
  '    .strTable = "FR7300"
  '
  '    Set objField = .AddField("FR73CODPRODUCTO")
  '    objField.strSmallDesc = "C�digo Producto"
  '
  '    Set objField = .AddField("FR73CODINTFAR")
  '    objField.strSmallDesc = "C�digo Interno Producto"
  '
  '    Set objField = .AddField("FR73DESPRODUCTO")
  '    objField.strSmallDesc = "Descripci�n Producto"
  '
  '    If .Search Then
  '      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), .cllValues("FR73CODPRODUCTO"))
  '    End If
  '  End With
  '  Set objSearch = Nothing
  'End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta, rstb, rstc As rdoResultset
Dim sqlstr, strInsert, strupdate As String
Dim numlinea As Integer

  If blnError = False And objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    sqlstr = "SELECT COUNT(*) FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta(0).Value = 0 Then
      'Buscar hojas de quir�fano y anestesia fr4300,fr4400
      sqlstr = "SELECT FR44CODHOJAQUIRO FROM FR4400 WHERE SG02COD IS NOT NULL AND (FR44INDESTHQ IS NULL OR FR44INDESTHQ=0)"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      numlinea = 1
      While Not rsta.EOF
        'sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR18CANTCONSUMIDA FROM FR1800 WHERE FR44CODHOJAQUIRO=" & rsta(0).Value & _
        '         " AND FR18CANTCONSUMIDA>0"
        sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA,SUM(FR18CANTCONSUMIDA) FROM FR1800 WHERE "
        sqlstr = sqlstr & " FR44CODHOJAQUIRO=" & rsta(0).Value & " AND FR18CANTCONSUMIDA>0 GROUP BY FR73CODPRODUCTO,FR93CODUNIMEDIDA"
        
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        While Not rstb.EOF
          strInsert = "INSERT INTO FR2000 VALUES (" & _
                      txtText1(0).Text & "," & _
                      numlinea & "," & _
                      rstb(0).Value & "," & _
                      "'" & rstb(1).Value & "'" & "," & _
                      rstb(2).Value & ")"
          objApp.rdoConnect.Execute strInsert, 64
          strupdate = "UPDATE FR4400 SET FR44INDESTHQ=-1 WHERE FR44CODHOJAQUIRO=" & rsta(0).Value
          objApp.rdoConnect.Execute strupdate, 64
          numlinea = numlinea + 1
          rstb.MoveNext
        Wend
        rstb.Close
        Set rstb = Nothing
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      
      sqlstr = "SELECT FR43CODHOJAANEST FROM FR4300 WHERE SG02COD IS NOT NULL AND (FR43INDESTHA IS NULL OR FR43INDESTHA=0)"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        'sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD FROM FR1700 WHERE FR43CODHOJAANEST=" & rsta(0).Value & _
        '         " AND FR17CANTIDAD>0"
        sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA,SUM(FR17CANTIDAD) FROM FR1700 WHERE FR43CODHOJAANEST=" & rsta(0).Value
        sqlstr = sqlstr & " AND FR17CANTIDAD>0 GROUP BY FR73CODPRODUCTO,FR93CODUNIMEDIDA;"
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        
        While Not rstb.EOF
          sqlstr = "SELECT * FROM FR2000 WHERE "
          sqlstr = sqlstr & " FR55CODNECESUNID=" & txtText1(0).Text
          sqlstr = sqlstr & " AND FR73CODPRODUCTO=" & rstb(0).Value
          Set rstc = objApp.rdoConnect.OpenResultset(sqlstr)
          If rstc.EOF Then
            strInsert = "INSERT INTO FR2000 VALUES (" & _
                        txtText1(0).Text & "," & _
                        numlinea & "," & _
                        rstb(0).Value & "," & _
                        "'" & rstb(1).Value & "'" & "," & _
                        rstb(2).Value & ")"
            objApp.rdoConnect.Execute strInsert, 64
            numlinea = numlinea + 1
          Else
            strupdate = "UPDATE FR2000 SET FR20CANTNECESQUIR=FR20CANTNECESQUIR+" & rstb(2).Value
            strupdate = strupdate & " WHERE "
            strupdate = strupdate & " FR55CODNECESUNID=" & txtText1(0).Text
            strupdate = strupdate & " AND FR73CODPRODUCTO=" & rstb(0).Value
            objApp.rdoConnect.Execute strupdate, 64
          End If
          rstc.Close
          Set rstc = Nothing
          strupdate = "UPDATE FR4300 SET FR43INDESTHA=-1 WHERE FR43CODHOJAANEST=" & rsta(0).Value
          objApp.rdoConnect.Execute strupdate, 64
          rstb.MoveNext
        Wend
        rstb.Close
        Set rstb = Nothing
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
      Exit Sub
    End If
    rsta.Close
    Set rsta = Nothing
  End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim mensaje As String

If txtText1(5).Text <> "" Then
    stra = "SELECT * FROM AD0200 " & _
           "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
           " AND AD02CODDPTO=" & txtText1(5).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then
        mensaje = MsgBox("El servicio es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset

  '2 Nuevo,3 Abrir,4 Guardar,6 Imprimir,8 Borrar,10 Cortar,11 Copiar
  '12 Pegar,14 Deshacer,16 Localizar,18 Filtro,19 No Filtro,21 Primer Registro
  '22 Anterior,23 Siguiente,24 Ultimo,26 Refrescar,28 Mantenimiento,30 Salir
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case btnButton.Index
    Case 2, 3, 16, 18, 19, 21, 22, 23, 24, 26, 28, 30
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FR2000 WHERE "
        sqlstrPreg = sqlstrPreg & " FR55CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FR5500 WHERE FR55CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR55CODNECESUNID_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 1) 'FR26CODESTPETIC, REDACTADA
    rsta.Close
    Set rsta = Nothing
    txtText1(3).SetFocus
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle Necesidad" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR2000 WHERE FR55CODNECESUNID=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = grdDBGrid1(0).Rows
        Else
          linea = rstlinea.rdoColumns(0).Value + grdDBGrid1(0).Rows
        End If
        grdDBGrid1(0).Columns(4).Value = linea
        grdDBGrid1(0).Col = 5
        SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
'Menu Datos                                Botones
'10 Nuevo ................................ 2 Nuevo
'20 Abrir ................................ 3 Abrir
'40 Guardar .............................. 4 Guardar
'60 Eliminar ............................. 8 Borrar
'80 Imprimir ............................. 6 Imprimir
'100 Salir ............................... 30 Salir
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset

  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 20, 100
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FR2000 WHERE "
        sqlstrPreg = sqlstrPreg & " FR55CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FR5500 WHERE FR55CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If

  If intIndex = 10 Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)

End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
'Menu Filtro                               Botones
'10 Poner Filtro ......................... 18 Filtro
'20 Quitar Filtro ........................ 19 No Filtro
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 10, 20
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FR2000 WHERE "
        sqlstrPreg = sqlstrPreg & " FR55CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FR5500 WHERE FR55CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If

  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
'Menu registro                             Botones
'10 Localizar ............................ 16 Localizar
'20 Restaurar ............................
'40 Primero .............................. 21 Primer Registro
'50 Anterior ............................. 22 Anterior
'60 Siguiente ............................ 23 Siguiente
'70 Ultimo ............................... 24 Ultimo
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 10, 40, 50, 60, 70
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FR2000 WHERE "
        sqlstrPreg = sqlstrPreg & " FR55CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FR5500 WHERE FR55CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If
  
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
'Menu Opciones                             Botones
'10 Refrescar ............................ 26 Refrescar
'20 Mantenimiento ........................ 28 Mantenimiento
'40 Colores ..............................
'50 Alta Masiva ..........................
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 10, 20
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FR2000 WHERE "
        sqlstrPreg = sqlstrPreg & " FR55CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FR5500 WHERE FR55CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If
  
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)

End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim stra As String
Dim rsta As rdoResultset
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 0 And grdDBGrid1(0).Columns(5).Value <> "" And grdDBGrid1(0).Columns(8).Value = "" Then
      stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(8), rsta(0).Value)
      rsta.Close
      Set rsta = Nothing
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


