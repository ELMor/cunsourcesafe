VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDefProducto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DEFINIR FARMACIA. Definir Productos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   49
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      ForeColor       =   &H00000000&
      Height          =   2415
      Left            =   9360
      TabIndex        =   79
      Top             =   5640
      Width           =   2295
      Begin VB.CommandButton cmdsinonimos 
         Caption         =   "Sin�nimos"
         Height          =   375
         Left            =   240
         TabIndex        =   83
         Top             =   360
         Width           =   1815
      End
      Begin VB.CommandButton cmdsustitutos 
         Caption         =   "Sustitutos"
         Height          =   375
         Left            =   240
         TabIndex        =   82
         Top             =   840
         Width           =   1815
      End
      Begin VB.CommandButton cmdsustalergia 
         Caption         =   "Sustituci�n Alergias"
         Height          =   375
         Left            =   240
         TabIndex        =   81
         Top             =   1320
         Width           =   1815
      End
      Begin VB.CommandButton cmdprocfab 
         Caption         =   "Proceso de Fabricaci�n"
         Height          =   375
         Left            =   240
         TabIndex        =   80
         Top             =   1800
         Width           =   1815
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Index           =   1
      Left            =   120
      TabIndex        =   50
      Top             =   480
      Width           =   11820
      Begin TabDlg.SSTab tabTab1 
         Height          =   4815
         Index           =   0
         Left            =   120
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   360
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   8493
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0035.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(14)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(26)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(22)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "tabTab1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(34)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(22)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0035.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "FR73CODINTFAR"
            Height          =   330
            Index           =   22
            Left            =   1440
            TabIndex        =   1
            Tag             =   "C�digo Producto"
            Top             =   360
            Width           =   860
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73ESPECIALIDAD"
            Height          =   330
            Index           =   34
            Left            =   120
            TabIndex        =   4
            Tag             =   "Especialidad"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73NOMGENERICO"
            Height          =   330
            Index           =   2
            Left            =   120
            TabIndex        =   3
            Tag             =   "Nombre Gen�rico"
            Top             =   1200
            Width           =   4935
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73DESPRODUCTO"
            Height          =   690
            Index           =   1
            Left            =   2880
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   2
            Tag             =   "Descripci�n Producto"
            Top             =   360
            Width           =   6420
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Producto"
            Top             =   360
            Width           =   1020
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4305
            Index           =   2
            Left            =   -74880
            TabIndex        =   52
            TabStop         =   0   'False
            Top             =   120
            Width           =   10935
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19288
            _ExtentY        =   7594
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR73FECINIVIG"
            Height          =   330
            Index           =   0
            Left            =   9480
            TabIndex        =   5
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   480
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR73FECFINVIG"
            Height          =   330
            Index           =   1
            Left            =   9480
            TabIndex        =   6
            Tag             =   "Fecha Fin Vigencia"
            Top             =   1200
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   2415
            Index           =   1
            Left            =   120
            TabIndex        =   58
            TabStop         =   0   'False
            Top             =   2280
            Width           =   10815
            _ExtentX        =   19076
            _ExtentY        =   4260
            _Version        =   327681
            Style           =   1
            Tabs            =   8
            TabsPerRow      =   8
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Grupo Terape�tico"
            TabPicture(0)   =   "FR0035.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(27)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(20)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(21)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "txtText1(36)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(35)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(19)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(20)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(21)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).ControlCount=   8
            TabCaption(1)   =   "Indicadores"
            TabPicture(1)   =   "FR0035.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "chkCheck1(16)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "chkCheck1(0)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "chkCheck1(1)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "chkCheck1(2)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "chkCheck1(3)"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "chkCheck1(4)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "chkCheck1(5)"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "chkCheck1(6)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "chkCheck1(7)"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "chkCheck1(8)"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "chkCheck1(9)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "chkCheck1(10)"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).Control(12)=   "chkCheck1(11)"
            Tab(1).Control(12).Enabled=   0   'False
            Tab(1).Control(13)=   "chkCheck1(12)"
            Tab(1).Control(13).Enabled=   0   'False
            Tab(1).Control(14)=   "chkCheck1(13)"
            Tab(1).Control(14).Enabled=   0   'False
            Tab(1).Control(15)=   "chkCheck1(14)"
            Tab(1).Control(15).Enabled=   0   'False
            Tab(1).Control(16)=   "chkCheck1(15)"
            Tab(1).Control(16).Enabled=   0   'False
            Tab(1).ControlCount=   17
            TabCaption(2)   =   "Costes"
            TabPicture(2)   =   "FR0035.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(3)"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).Control(1)=   "txtText1(4)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "txtText1(5)"
            Tab(2).Control(2).Enabled=   0   'False
            Tab(2).Control(3)=   "txtText1(6)"
            Tab(2).Control(3).Enabled=   0   'False
            Tab(2).Control(4)=   "txtText1(7)"
            Tab(2).Control(4).Enabled=   0   'False
            Tab(2).Control(5)=   "txtText1(8)"
            Tab(2).Control(5).Enabled=   0   'False
            Tab(2).Control(6)=   "txtText1(9)"
            Tab(2).Control(6).Enabled=   0   'False
            Tab(2).Control(7)=   "txtText1(37)"
            Tab(2).Control(7).Enabled=   0   'False
            Tab(2).Control(8)=   "lblLabel1(1)"
            Tab(2).Control(8).Enabled=   0   'False
            Tab(2).Control(9)=   "lblLabel1(3)"
            Tab(2).Control(9).Enabled=   0   'False
            Tab(2).Control(10)=   "lblLabel1(4)"
            Tab(2).Control(10).Enabled=   0   'False
            Tab(2).Control(11)=   "lblLabel1(6)"
            Tab(2).Control(11).Enabled=   0   'False
            Tab(2).Control(12)=   "lblLabel1(7)"
            Tab(2).Control(12).Enabled=   0   'False
            Tab(2).Control(13)=   "lblLabel1(8)"
            Tab(2).Control(13).Enabled=   0   'False
            Tab(2).Control(14)=   "lblLabel1(9)"
            Tab(2).Control(14).Enabled=   0   'False
            Tab(2).Control(15)=   "lblLabel1(29)"
            Tab(2).Control(15).Enabled=   0   'False
            Tab(2).ControlCount=   16
            TabCaption(3)   =   "Lote / Proveedor"
            TabPicture(3)   =   "FR0035.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(11)"
            Tab(3).Control(0).Enabled=   0   'False
            Tab(3).Control(1)=   "txtText1(12)"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).Control(2)=   "txtText1(13)"
            Tab(3).Control(2).Enabled=   0   'False
            Tab(3).Control(3)=   "txtText1(14)"
            Tab(3).Control(3).Enabled=   0   'False
            Tab(3).Control(4)=   "txtText1(27)"
            Tab(3).Control(4).Enabled=   0   'False
            Tab(3).Control(5)=   "txtText1(28)"
            Tab(3).Control(5).Enabled=   0   'False
            Tab(3).Control(6)=   "txtText1(29)"
            Tab(3).Control(6).Enabled=   0   'False
            Tab(3).Control(7)=   "lblLabel1(11)"
            Tab(3).Control(7).Enabled=   0   'False
            Tab(3).Control(8)=   "lblLabel1(12)"
            Tab(3).Control(8).Enabled=   0   'False
            Tab(3).Control(9)=   "lblLabel1(13)"
            Tab(3).Control(9).Enabled=   0   'False
            Tab(3).Control(10)=   "lblLabel1(15)"
            Tab(3).Control(10).Enabled=   0   'False
            Tab(3).ControlCount=   11
            TabCaption(4)   =   "Stock / Demanda-consumo"
            TabPicture(4)   =   "FR0035.frx":00A8
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "txtText1(15)"
            Tab(4).Control(0).Enabled=   0   'False
            Tab(4).Control(1)=   "txtText1(16)"
            Tab(4).Control(1).Enabled=   0   'False
            Tab(4).Control(2)=   "txtText1(18)"
            Tab(4).Control(2).Enabled=   0   'False
            Tab(4).Control(3)=   "lblLabel1(16)"
            Tab(4).Control(3).Enabled=   0   'False
            Tab(4).Control(4)=   "lblLabel1(17)"
            Tab(4).Control(4).Enabled=   0   'False
            Tab(4).Control(5)=   "lblLabel1(19)"
            Tab(4).Control(5).Enabled=   0   'False
            Tab(4).ControlCount=   6
            TabCaption(5)   =   "Imagen"
            TabPicture(5)   =   "FR0035.frx":00C4
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "txtText1(17)"
            Tab(5).Control(0).Enabled=   0   'False
            Tab(5).Control(1)=   "lblLabel1(18)"
            Tab(5).Control(1).Enabled=   0   'False
            Tab(5).ControlCount=   2
            TabCaption(6)   =   "Otros"
            TabPicture(6)   =   "FR0035.frx":00E0
            Tab(6).ControlEnabled=   0   'False
            Tab(6).Control(0)=   "txtText1(39)"
            Tab(6).Control(0).Enabled=   0   'False
            Tab(6).Control(1)=   "txtText1(40)"
            Tab(6).Control(1).Enabled=   0   'False
            Tab(6).Control(2)=   "txtText1(41)"
            Tab(6).Control(2).Enabled=   0   'False
            Tab(6).Control(3)=   "txtText1(43)"
            Tab(6).Control(3).Enabled=   0   'False
            Tab(6).Control(4)=   "txtText1(44)"
            Tab(6).Control(4).Enabled=   0   'False
            Tab(6).Control(5)=   "txtText1(45)"
            Tab(6).Control(5).Enabled=   0   'False
            Tab(6).Control(6)=   "txtText1(46)"
            Tab(6).Control(6).Enabled=   0   'False
            Tab(6).Control(7)=   "txtText1(47)"
            Tab(6).Control(7).Enabled=   0   'False
            Tab(6).Control(8)=   "txtText1(48)"
            Tab(6).Control(8).Enabled=   0   'False
            Tab(6).Control(9)=   "txtText1(49)"
            Tab(6).Control(9).Enabled=   0   'False
            Tab(6).Control(10)=   "txtText1(10)"
            Tab(6).Control(10).Enabled=   0   'False
            Tab(6).Control(11)=   "lblLabel1(31)"
            Tab(6).Control(11).Enabled=   0   'False
            Tab(6).Control(12)=   "lblLabel1(32)"
            Tab(6).Control(12).Enabled=   0   'False
            Tab(6).Control(13)=   "lblLabel1(33)"
            Tab(6).Control(13).Enabled=   0   'False
            Tab(6).Control(14)=   "lblLabel1(34)"
            Tab(6).Control(14).Enabled=   0   'False
            Tab(6).Control(15)=   "lblLabel1(35)"
            Tab(6).Control(15).Enabled=   0   'False
            Tab(6).Control(16)=   "lblLabel1(36)"
            Tab(6).Control(16).Enabled=   0   'False
            Tab(6).Control(17)=   "lblLabel1(10)"
            Tab(6).Control(17).Enabled=   0   'False
            Tab(6).ControlCount=   18
            TabCaption(7)   =   "Presentaci�n"
            TabPicture(7)   =   "FR0035.frx":00FC
            Tab(7).ControlEnabled=   0   'False
            Tab(7).Control(0)=   "txtText1(23)"
            Tab(7).Control(0).Enabled=   0   'False
            Tab(7).Control(1)=   "txtText1(24)"
            Tab(7).Control(1).Enabled=   0   'False
            Tab(7).Control(2)=   "lblLabel1(23)"
            Tab(7).Control(2).Enabled=   0   'False
            Tab(7).Control(3)=   "lblLabel1(25)"
            Tab(7).Control(3).Enabled=   0   'False
            Tab(7).ControlCount=   4
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PRESENTACION"
               Height          =   330
               Index           =   23
               Left            =   -74760
               TabIndex        =   107
               Tag             =   "Presentaci�n"
               Top             =   660
               Width           =   9000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73INDICPRESENT"
               Height          =   810
               Index           =   24
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   106
               Tag             =   "Indicaciones Presentaci�n"
               Top             =   1380
               Width           =   9015
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73TPDESVMAXRECALE"
               Height          =   330
               Index           =   39
               Left            =   -69720
               TabIndex        =   98
               Tag             =   "Desviaci�n permitida"
               Top             =   1920
               Width           =   1000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR89CODTIPFABRI"
               Height          =   330
               Index           =   40
               Left            =   -74880
               TabIndex        =   97
               Tag             =   "C�d.Tipo Fabricaci�n"
               Top             =   1920
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   41
               Left            =   -74160
               TabIndex        =   96
               TabStop         =   0   'False
               Tag             =   "Desc.Tipo Fabricaci�n"
               Top             =   1920
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR86codabcproduct"
               Height          =   330
               Index           =   43
               Left            =   -69720
               TabIndex        =   95
               Tag             =   "ABC Producto"
               Top             =   1260
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   44
               Left            =   -68400
               TabIndex        =   94
               TabStop         =   0   'False
               Tag             =   "Desc.Proceso Fabricaci�n"
               Top             =   660
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR69codprocfabric"
               Height          =   330
               Index           =   45
               Left            =   -69720
               TabIndex        =   93
               Tag             =   "C�d.Proceso Fabricaci�n"
               Top             =   660
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   -74160
               TabIndex        =   92
               TabStop         =   0   'False
               Tag             =   "Desc.Tipo IVA"
               Top             =   1260
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR88CODTIPIVA"
               Height          =   330
               Index           =   47
               Left            =   -74880
               TabIndex        =   91
               Tag             =   "C�d.Tipo IVA"
               Top             =   1260
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   48
               Left            =   -74160
               TabIndex        =   90
               TabStop         =   0   'False
               Tag             =   "Desc.Unidad de Medida"
               Top             =   660
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   49
               Left            =   -74880
               TabIndex        =   89
               Tag             =   "C�d.Unidad de Medida"
               Top             =   660
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73TPMAXERROR"
               Height          =   330
               Index           =   10
               Left            =   -67440
               TabIndex        =   88
               Tag             =   "% M�x.Error"
               Top             =   1260
               Width           =   1000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PATHIMAGEN"
               Height          =   1530
               Index           =   17
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   86
               Tag             =   "Imagen"
               Top             =   660
               Width           =   9885
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Estupefaciente"
               DataField       =   "FR73INDESTUPEFACI"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   -71160
               TabIndex        =   84
               Tag             =   "Estupefaciente?"
               Top             =   1800
               Width           =   2055
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73SIMBOLOS"
               Height          =   330
               Index           =   21
               Left            =   240
               TabIndex        =   11
               Tag             =   "S�mbolo"
               Top             =   1860
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   20
               Left            =   2400
               TabIndex        =   10
               TabStop         =   0   'False
               Tag             =   "Descripci�n Subgrupo Terape�tico"
               Top             =   1260
               Width           =   8205
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRA4SUBGRPTERAPE"
               Height          =   330
               Index           =   19
               Left            =   240
               TabIndex        =   9
               Tag             =   "C�digo Subgrupo Terape�tico"
               Top             =   1260
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   35
               Left            =   2400
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Desc.Grupo Terape�tico"
               Top             =   660
               Width           =   8205
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR00CODGRPTERAP"
               Height          =   330
               Index           =   36
               Left            =   240
               TabIndex        =   7
               Tag             =   "Grupo Terape�tico"
               Top             =   660
               Width           =   2000
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Unidosis"
               DataField       =   "FR73INDUNIDOSIS"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74880
               TabIndex        =   12
               Tag             =   "Unidosis?"
               Top             =   720
               Width           =   1095
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anestesia Suero"
               DataField       =   "fr73indanestsuero"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -71160
               TabIndex        =   21
               Tag             =   "Anestesia Suero?"
               Top             =   1440
               Width           =   1815
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Analgosedaci�n"
               DataField       =   "FR73INDANALGSEDACION"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   -73200
               TabIndex        =   16
               Tag             =   "Analgosedaci�n?"
               Top             =   720
               Width           =   1695
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Fabricable"
               DataField       =   "FR73INDFABRICABLE"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -73200
               TabIndex        =   17
               Tag             =   "Fabricable?"
               Top             =   1080
               Width           =   1455
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Materia Prima"
               DataField       =   "FR73INDMATPRIMA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -71160
               TabIndex        =   19
               Tag             =   "Materia Prima?"
               Top             =   720
               Width           =   1575
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Dep�sito"
               DataField       =   "FR73INDDEPOSITO"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -71160
               TabIndex        =   20
               Tag             =   "Dep�sito?"
               Top             =   1080
               Width           =   1095
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anestesia Gen.Pedi�trica"
               DataField       =   "FR73INDANESTGENPED"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -69360
               TabIndex        =   23
               Tag             =   "Anest.Gen.Pedi�trica?"
               Top             =   1080
               Width           =   2535
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Medicamento"
               DataField       =   "FR73INDMEDICAMENTO"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -74880
               TabIndex        =   13
               Tag             =   "Medicamento?"
               Top             =   1080
               Width           =   1695
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anestesia CEC"
               DataField       =   "FR73INDANESTCEC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   -66840
               TabIndex        =   26
               Tag             =   "Anestesia CEC?"
               Top             =   1080
               Width           =   1695
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anest.Trasplante Hep�tico"
               DataField       =   "FR73INDANESTTRASHEP"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -66840
               TabIndex        =   25
               Tag             =   "Anest.Trasplante Hep�tico?"
               Top             =   720
               Width           =   2655
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anest. Espec�fico"
               DataField       =   "FR73INDANESTESPEC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -69360
               TabIndex        =   22
               Tag             =   "Anest.Espec�fico?"
               Top             =   720
               Width           =   2415
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Mat/Med Quir�fano"
               DataField       =   "FR73INDMATMEDQUIRO"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   -73200
               TabIndex        =   18
               Tag             =   "Material/Medicam.Quir�fano?"
               Top             =   1440
               Width           =   2055
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Medicam. RUM"
               DataField       =   "FR73INDMEDRUM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74880
               TabIndex        =   14
               Tag             =   "Medicamento RUM?"
               Top             =   1440
               Width           =   1695
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anest. General Adulto"
               DataField       =   "FR73INDANESTGENADU"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   -69360
               TabIndex        =   24
               Tag             =   "Anest.General Adulto?"
               Top             =   1440
               Width           =   2295
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Anestesia Medicamento"
               DataField       =   "FR73INDANESTMED"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   -66840
               TabIndex        =   27
               Tag             =   "Anestesia Medicamento?"
               Top             =   1440
               Width           =   2415
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Sujeto a control de Lote"
               DataField       =   "FR73INDCTRLLOTE"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -74880
               TabIndex        =   15
               Tag             =   "Sujeto a control de lote?"
               Top             =   1800
               Width           =   2535
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73COSTEALMAANUAL"
               Height          =   330
               Index           =   3
               Left            =   -74760
               TabIndex        =   28
               Tag             =   "Coste Almacenamiento Anual"
               Top             =   1020
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73COSTELANZPEDIDO"
               Height          =   330
               Index           =   4
               Left            =   -74760
               TabIndex        =   29
               Tag             =   "Coste Lanzamiento Pedido"
               Top             =   1740
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73COSTEUNITARIO"
               Height          =   330
               Index           =   5
               Left            =   -71520
               TabIndex        =   30
               Tag             =   "Coste Unitario"
               Top             =   1020
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PRECIOVENTA"
               Height          =   330
               Index           =   6
               Left            =   -71520
               TabIndex        =   31
               Tag             =   "Precio de Venta"
               Top             =   1740
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PRECIONETCOMPRA"
               Height          =   330
               Index           =   7
               Left            =   -69000
               TabIndex        =   32
               Tag             =   "Precio Neto de Compra"
               Top             =   1020
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PRECIOASS"
               Height          =   330
               Index           =   8
               Left            =   -69000
               TabIndex        =   33
               Tag             =   "Precio de Venta S.S"
               Top             =   1740
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PRECIOENVASE"
               Height          =   330
               Index           =   9
               Left            =   -66360
               TabIndex        =   44
               Tag             =   "Precio Envase"
               Top             =   1020
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73TPRAPPEL"
               Height          =   330
               Index           =   37
               Left            =   -66360
               TabIndex        =   45
               Tag             =   "Procentaje Rappel"
               Top             =   1800
               Width           =   1000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73LOTEECONPEDIDO"
               Height          =   330
               Index           =   11
               Left            =   -74760
               TabIndex        =   34
               Tag             =   "Lote Econ�mico Pedido"
               Top             =   840
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79CODPROVEEDOR_A"
               Height          =   330
               Index           =   12
               Left            =   -74760
               TabIndex        =   35
               Tag             =   "C�digo Proveedor A"
               Top             =   1620
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79CODPROVEEDOR_B"
               Height          =   330
               Index           =   13
               Left            =   -69600
               TabIndex        =   37
               Tag             =   "C�digo Proveedor B"
               Top             =   780
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79CODPROVEEDOR_C"
               Height          =   330
               Index           =   14
               Left            =   -69600
               TabIndex        =   39
               Tag             =   "C�digo Proveedor C"
               Top             =   1620
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   27
               Left            =   -68280
               TabIndex        =   40
               TabStop         =   0   'False
               Tag             =   "Descripci�n Proveedor C"
               Top             =   1620
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   28
               Left            =   -68280
               TabIndex        =   38
               TabStop         =   0   'False
               Tag             =   "Descripci�n Proveedor B"
               Top             =   780
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   29
               Left            =   -73440
               TabIndex        =   36
               TabStop         =   0   'False
               Tag             =   "Descripci�n Proveedor A"
               Top             =   1620
               Width           =   3765
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73STOCKPREFEREN"
               Height          =   330
               Index           =   15
               Left            =   -74640
               TabIndex        =   41
               Tag             =   "Stock Preferente"
               Top             =   900
               Width           =   1250
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73STOCKMIN"
               Height          =   330
               Index           =   16
               Left            =   -74640
               TabIndex        =   42
               Tag             =   "Stock M�nimo"
               Top             =   1740
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73DEMANDIVCONSUMO"
               Height          =   330
               Index           =   18
               Left            =   -71880
               TabIndex        =   43
               Tag             =   "Demanda/Consumo Anual"
               Top             =   900
               Width           =   2160
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Presentaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   -74760
               TabIndex        =   109
               Top             =   420
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Indicaciones de la Presentaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   -74760
               TabIndex        =   108
               Top             =   1140
               Width           =   3255
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Desviaci�n permitida"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   31
               Left            =   -69720
               TabIndex        =   105
               Top             =   1680
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tipo Fabricaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   32
               Left            =   -74880
               TabIndex        =   104
               Top             =   1680
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "ABC Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   33
               Left            =   -69720
               TabIndex        =   103
               Top             =   1020
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Proceso Fabricaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   34
               Left            =   -69720
               TabIndex        =   102
               Top             =   420
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tipo IVA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   35
               Left            =   -74880
               TabIndex        =   101
               Top             =   1020
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Unidad Medida"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   36
               Left            =   -74880
               TabIndex        =   100
               Top             =   420
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "% M�x.Error"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -67440
               TabIndex        =   99
               Top             =   1020
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Archivo Imagen Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -74880
               TabIndex        =   87
               Top             =   420
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "S�mbolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   21
               Left            =   240
               TabIndex        =   77
               Top             =   1620
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Subgrupo Terape�tico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   240
               TabIndex        =   76
               Top             =   1020
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Grupo Terape�tico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   240
               TabIndex        =   75
               Top             =   420
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Coste Almacenamiento Anual"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   74
               Top             =   780
               Width           =   2775
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Coste Lanzamiento Pedido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -74760
               TabIndex        =   73
               Top             =   1500
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Coste Unitario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -71520
               TabIndex        =   72
               Top             =   780
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Precio de Venta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -71520
               TabIndex        =   71
               Top             =   1500
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Precio Neto Compra"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -69000
               TabIndex        =   70
               Top             =   780
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Precio de Venta S.S."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   -69000
               TabIndex        =   69
               Top             =   1500
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Precio Envase"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -66360
               TabIndex        =   68
               Top             =   780
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Porcentaje Rappel"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   -66360
               TabIndex        =   67
               Top             =   1560
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Lote Econ�mico Pedido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   -74760
               TabIndex        =   66
               Top             =   600
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.proveedor A"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74760
               TabIndex        =   65
               Top             =   1380
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.proveedor B"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   -69600
               TabIndex        =   64
               Top             =   540
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.proveedor C"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -69600
               TabIndex        =   63
               Top             =   1380
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Stock Preferente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   -74640
               TabIndex        =   62
               Top             =   660
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Stock M�nimo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   -74640
               TabIndex        =   61
               Top             =   1500
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Demanda/Consumo Anual"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   -71880
               TabIndex        =   60
               Top             =   660
               Width           =   2295
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Interno"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   1440
            TabIndex        =   85
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Especialidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   26
            Left            =   120
            TabIndex        =   59
            Top             =   1560
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre Gen�rico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   57
            Top             =   960
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   9480
            TabIndex        =   56
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   9480
            TabIndex        =   55
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   54
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   2880
            TabIndex        =   53
            Top             =   120
            Width           =   2535
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Principios Activos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2280
      Index           =   0
      Left            =   120
      TabIndex        =   47
      Top             =   5760
      Width           =   9015
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "Seleccionar Principios Activos"
         Height          =   615
         Left            =   7440
         TabIndex        =   78
         Top             =   960
         Width           =   1455
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1785
         Index           =   0
         Left            =   120
         TabIndex        =   48
         Top             =   360
         Width           =   7185
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   12674
         _ExtentY        =   3149
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   46
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantGrupoProducto (FR0032.FRM)                            *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: definici�n de productos                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim intmantenimiento As Integer



Private Sub cmdbuscar_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String

cmdbuscar.Enabled = False
noinsertar = True
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
Call objsecurity.LaunchProcess("FR0053")
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
For v = 0 To gintpricipioactivototal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(0).Rows > 0 Then
        grdDBGrid1(0).MoveFirst
        For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(3).Value = gintpricipioactivobuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
               'no se inserta el protocolo pues ya est� insertado en el grid
                noinsertar = False
                Exit For
            Else
                noinsertar = True
            End If
        grdDBGrid1(0).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
        grdDBGrid1(0).Columns(3).Value = gintpricipioactivobuscado(v, 0)
        grdDBGrid1(0).Columns(4).Value = gintpricipioactivobuscado(v, 1)
        grdDBGrid1(0).Columns(5).Value = txtText1(0).Text
      Else
        mensaje = MsgBox("El principio: " & gintpricipioactivobuscado(v, 1) & " ya est� guardado." & _
                       Chr(13), vbInformation)
      End If
      noinsertar = True
Next v

cmdbuscar.Enabled = True
End Sub

Private Sub cmdprocfab_Click()
Dim mensaje As String
cmdprocfab.Enabled = False
If txtText1(0).Text <> "" Then
    If chkCheck1(3).Value = 1 Then
        Call objsecurity.LaunchProcess("FR0041")
    Else
        mensaje = MsgBox("El producto no es fabricable.", vbInformation, "Aviso")
    End If
End If
cmdprocfab.Enabled = True
End Sub

Private Sub cmdsinonimos_Click()
cmdsinonimos.Enabled = False
frmDefSinonimo.txtProd(0).Text = txtText1(0).Text
frmDefSinonimo.txtProd(1).Text = txtText1(1).Text
frmDefSinonimo.txtProd(2).Text = txtText1(22).Text
Call objsecurity.LaunchProcess("FR0038")
cmdsinonimos.Enabled = True
End Sub

Private Sub cmdsustalergia_Click()
cmdsustalergia.Enabled = False
frmDefSustAlergia.txtProd(0).Text = txtText1(0).Text
frmDefSustAlergia.txtProd(1).Text = txtText1(1).Text
frmDefSustAlergia.txtProd(2).Text = txtText1(22).Text
Call objsecurity.LaunchProcess("FR0037")
cmdsustalergia.Enabled = True
End Sub

Private Sub cmdsustitutos_Click()
cmdsustitutos.Enabled = False
frmDefSustituto.txtProd(0).Text = txtText1(0).Text
frmDefSustituto.txtProd(1).Text = txtText1(1).Text
frmDefSustituto.txtProd(2).Text = txtText1(22).Text
Call objsecurity.LaunchProcess("FR0036")
cmdsustitutos.Enabled = True
End Sub

Private Sub Form_Activate()
If txtText1(0).Text = "" Then
    cmdsinonimos.Enabled = False
    cmdsustitutos.Enabled = False
    cmdsustalergia.Enabled = False
    cmdprocfab.Enabled = False
Else
    cmdsinonimos.Enabled = True
    cmdsustitutos.Enabled = True
    cmdsustalergia.Enabled = True
    cmdprocfab.Enabled = True
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Productos"
      
    .strTable = "FR7300"
    .blnHasMaint = True
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Producto", cwString)
    Call .FormAddFilterWhere(strKey, "FR73FECINIVIG", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR73FECFINVIG", "Fecha Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR00CODGRPTERAP", "C�d.Grupo Terape�tico", cwString)
    Call .FormAddFilterWhere(strKey, "FR73SUBGRPTERAPE", "C�d.Subgrupo Terape�tico", cwString)
    Call .FormAddFilterWhere(strKey, "FR73NOMGENERICO", "Nombre Gen�rico", cwString)
    Call .FormAddFilterWhere(strKey, "FR73PRECIOENVASE", "Precio Envase", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73PRECIONETCOMPRA", "Precio Neto Compra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73PRECIOVENTA", "Precio Venta", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73PRECIOASS", "Precio Venta S.S.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73STOCKMIN", "Stock M�nimo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73STOCKPREFEREN", "Stock Preferente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73COSTELANZPEDIDO", "Coste Lanzamiento Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73DEMANDIVCONSUMO", "Demanda/Consumo Anual", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73COSTEALMAANUAL", "Coste Almacenamiento Anual", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73ESPECIALIDAD", "Especialidad", cwString)
    Call .FormAddFilterWhere(strKey, "FR73INDMATPRIMA", "Materia Prima?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDUNIDOSIS", "Unidosis?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDINDMEDICAMENTO", "Medicamento?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDFABRICABLE", "Fabricable?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDMATMEDQUIRO", "Mat/Med Quir�fano?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANESTGENADU", "Anestesia General Adulto?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANESTGENPED", "Anestesia Gen.Pedi�trica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANESTCEC", "Anestesia CEC?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANALGSEDACION", "Analgosedaci�n?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANESTSUERO", "Anestesia Suero", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANESTMED", "Anestesia Medicamento?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDANESTESPEC", "Anestesia Espec�fico", cwBoolean)

    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")



  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Principios Activos"
    
    .strTable = "FR6400"
    
    Call .FormAddOrderField("FR68CODPRINCACTIV", cwAscending)
    Call .FormAddRelation("FR73CODPRODUCTO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�digo", "FR68CODPRINCACTIV", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Porcentaje", "FR64TPPRINCACTIV", cwDecimal, 2)
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True
    

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(34)).blnInFind = True
    .CtrlGetInfo(txtText1(36)).blnInFind = True
    .CtrlGetInfo(txtText1(19)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(37)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(18)).blnInFind = True
    .CtrlGetInfo(txtText1(49)).blnInFind = True
    .CtrlGetInfo(txtText1(47)).blnInFind = True
    .CtrlGetInfo(txtText1(40)).blnInFind = True
    .CtrlGetInfo(txtText1(45)).blnInFind = True
    .CtrlGetInfo(txtText1(43)).blnInFind = True
    .CtrlGetInfo(txtText1(39)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(chkCheck1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(6)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(8)).blnInFind = True
    .CtrlGetInfo(chkCheck1(9)).blnInFind = True
    .CtrlGetInfo(chkCheck1(10)).blnInFind = True
    .CtrlGetInfo(chkCheck1(11)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(13)).blnInFind = True
    .CtrlGetInfo(chkCheck1(14)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    
    
  
    .CtrlGetInfo(txtText1(17)).blnInGrid = False

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), "FR68CODPRINCACTIV", "SELECT * FROM FR6800 WHERE FR68CODPRINCACTIV = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(4), "FR68DESPRINCACTIV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(49)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(49)), txtText1(48), "FR93DESUNIMEDIDA")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(45)), "FR69CODPROCFABRIC", "SELECT * FROM FR6900 WHERE FR69CODPROCFABRIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(45)), txtText1(44), "FR69DESPROCESO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(47)), "FR88CODTIPIVA", "SELECT * FROM FR8800 WHERE FR88CODTIPIVA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(47)), txtText1(46), "FR88DESCIVA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(36)), "FR00CODGRPTERAP", "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(36)), txtText1(35), "FR00DESGRPTERAP")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(12)), "FR79CODPROVEEDOR_A", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(12)), txtText1(29), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "FR79CODPROVEEDOR_B", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(28), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "FR79CODPROVEEDOR_C", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(27), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(40)), "FR89CODTIPFABRI", "SELECT * FROM FR8900 WHERE FR89CODTIPFABRI = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(40)), txtText1(41), "FR89DESTIPFABRI")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "FRA4SUBGRPTERAPE", "SELECT * FROM FRA400 WHERE FRA4SUBGRPTERAPE = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(20), "FRA4DESSUBGRPTERA")
    
    .CtrlGetInfo(txtText1(49)).blnForeign = True
    .CtrlGetInfo(txtText1(45)).blnForeign = True
    .CtrlGetInfo(txtText1(47)).blnForeign = True
    .CtrlGetInfo(txtText1(36)).blnForeign = True
    .CtrlGetInfo(txtText1(12)).blnForeign = True
    .CtrlGetInfo(txtText1(13)).blnForeign = True
    .CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(40)).blnForeign = True
    .CtrlGetInfo(txtText1(19)).blnForeign = True
    
    .CtrlGetInfo(txtText1(43)).blnReadOnly = True 'ABC Producto
    
    grdDBGrid1(0).Columns(5).Visible = False
    grdDBGrid1(0).Columns(6).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim mensaje As String
  
  If strFormName = "Productos" And strCtrl = "txtText1(49)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad Medida"

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n Unidad Medida"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(49), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(45)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR6900"
     .strWhere = "WHERE FR73CODPRODUCTO=" & txtText1(0).Text

     Set objField = .AddField("FR69CODPROCFABRIC")
     objField.strSmallDesc = "C�digo Proceso Fabricaci�n"

     Set objField = .AddField("FR69DESPROCESO")
     objField.strSmallDesc = "Descripci�n Proceso Fabricaci�n"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(45), .cllValues("FR69CODPROCFABRIC"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(47)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR8800"

     Set objField = .AddField("FR88CODTIPIVA")
     objField.strSmallDesc = "C�digo Tipo IVA"

     Set objField = .AddField("FR88DESCIVA")
     objField.strSmallDesc = "Descripci�n tipo IVA"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(47), .cllValues("FR88CODTIPIVA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(36)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR0000"

     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�digo Grupo Terape�tico"

     Set objField = .AddField("FR00DESGRPTERAP")
     objField.strSmallDesc = "Descripci�n Grupo Terape�tico"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(36), .cllValues("FR00CODGRPTERAP"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(12)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"

     Set objField = .AddField("FR79CODPROVEEDOR_A")
     objField.strSmallDesc = "C�digo Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n Proveedor"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(12), .cllValues("FR79CODPROVEEDOR_A"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(13)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"

     Set objField = .AddField("FR79CODPROVEEDOR_B")
     objField.strSmallDesc = "C�digo Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n Proveedor"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(13), .cllValues("FR79CODPROVEEDOR_B"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(14)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"

     Set objField = .AddField("FR79CODPROVEEDOR_C")
     objField.strSmallDesc = "C�digo Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n Proveedor"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("FR79CODPROVEEDOR_C"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 'para modificar el tipo de fabricaci�n la check de fabricable debe estar activada
 If strFormName = "Productos" And strCtrl = "txtText1(40)" Then
  If chkCheck1(3).Value = 1 Then
        Set objSearch = New clsCWSearch
        With objSearch
         .strTable = "FR8900"
    
         Set objField = .AddField("FR89CODTIPFABRI")
         objField.strSmallDesc = "C�digo Tipo Fabricaci�n"
    
         Set objField = .AddField("FR89DESTIPFABRI")
         objField.strSmallDesc = "Descripci�n Tipo Fabricaci�n"
    
         If .Search Then
          Call objWinInfo.CtrlSet(txtText1(40), .cllValues("FR89CODTIPFABRI"))
         End If
       End With
       Set objSearch = Nothing
  Else
       mensaje = MsgBox("El indicador de Fabricable est� desactivado." & Chr(13) & _
                        "No puede modificar el Tipo de Fabricaci�n", vbInformation, "Aviso")
  End If
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(19)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRA400"
     If txtText1(36).Text <> "" Then
        .strWhere = "WHERE FR00CODGRPTERAP=" & "'" & txtText1(36).Text & "'"
     Else
        .strWhere = "WHERE FR00CODGRPTERAP IS NULL"
     End If

     Set objField = .AddField("FRA4SUBGRPTERAPE")
     objField.strSmallDesc = "C�digo Subgrupo Terape�tico"

     Set objField = .AddField("FRA4DESSUBGRPTERA")
     objField.strSmallDesc = "Descripci�n Subgrupo Terape�tico"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(19), .cllValues("FRA4SUBGRPTERAPE"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
'si me posiciono en grupo terape�tico o subgrupo terape�tico que se active el mantenimiento
If intmantenimiento = 19 Or intmantenimiento = 36 Then
    Call objsecurity.LaunchProcess("FR0058")
End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim rstgrupo As rdoResultset
Dim strgrupo As String
Dim intnoexiste As String
Dim mensaje As String
Dim rstServicio As rdoResultset
Dim strServicio As String
Dim Servicio As Integer

'si se ha pinchado la check de sujeto a control de lote se debe mirar si existe alg�n
'grupo de productos que contenga a ese producto cuyo c�digo de servicio sea
'Farmacia(c�digo 1) y tenga los campos <Tiempo de estabilidad> y <Tipo de medida>
'rellenados.
'Esto influye para que luego el producto tenga caducidad o no.
intnoexiste = True
If chkCheck1(15).Value = 1 Then
    strServicio = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=3"
    Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
    Servicio = rstServicio("FRH2PARAMGEN").Value
    rstServicio.Close
    Set rstServicio = Nothing
    
    stra = "SELECT * FROM FR0900 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    Do While Not rsta.EOF
        strgrupo = "SELECT count(*) FROM FR4100 WHERE " & _
                 "FR41CODGRUPPROD=" & rsta.rdoColumns("FR41CODGRUPPROD").Value & " AND " & _
                 "AD02CODDPTO=" & Servicio & " AND " & _
                 "FR41NUMESTAB IS NOT NULL AND FR41TIPMEDIDA IS NOT NULL"
        Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
        If rstgrupo.rdoColumns(0).Value > 0 Then
            intnoexiste = False
            Exit Do
        Else
            intnoexiste = True
        End If
        rstgrupo.Close
        Set rstgrupo = Nothing
    rsta.MoveNext
    Loop
    rsta.Close
    Set rsta = Nothing
    If intnoexiste = True Then
        mensaje = MsgBox("Es necesario definir un Grupo de Productos ya que el producto" & _
        Chr(13) & " tiene activado el indicador de Sujeto a Control de Lote.", vbInformation)
    End If
End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub





Private Sub tabTab1_DblClick(Index As Integer)

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Principios Activos" Then
    MsgBox "S�lo puede a�adir principios activos mediante el buscador", vbExclamation
   Exit Sub
End If

If btnButton.Index = 2 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR73CODPRODUCTO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String

If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Principios Activos" Then
    MsgBox "S�lo puede a�adir principios activos mediante el buscador", vbExclamation
   Exit Sub
End If

If intIndex = 2 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        sqlstr = "SELECT FR73CODPRODUCTO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  'si la check de fabricable est� activada(chkcheck1(13)) no dejar� meter ning�n tipo
  'de fabricaci�n(txttext1(40))
  If intIndex = 3 Then
     If chkCheck1(3).Value = 0 Then
        txtText1(40).Text = ""
        txtText1(41).Text = ""
     End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  intmantenimiento = intIndex
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim mensaje As String
If txtText1(0).Text = "" Then
        cmdsinonimos.Enabled = False
        cmdsustitutos.Enabled = False
        cmdsustalergia.Enabled = False
        cmdprocfab.Enabled = False
  Else
        cmdsinonimos.Enabled = True
        cmdsustitutos.Enabled = True
        cmdsustalergia.Enabled = True
        cmdprocfab.Enabled = True
End If
'tipo de fabricaci�n
If intIndex = 40 Then
    If chkCheck1(3).Value = 0 Then
         txtText1(40).Text = ""
         txtText1(41).Text = ""
         Exit Sub
    Else
        Call objWinInfo.CtrlDataChange
    End If
Else
    Call objWinInfo.CtrlDataChange
End If

End Sub


