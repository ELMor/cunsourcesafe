VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "MSRDC20.OCX"
Begin VB.Form frmUbicacionHHCC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Ubicaci�n de Historias Cl�nicas"
   ClientHeight    =   7335
   ClientLeft      =   2040
   ClientTop       =   450
   ClientWidth     =   8250
   ClipControls    =   0   'False
   HelpContextID   =   12
   Icon            =   "AR1005.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7335
   ScaleWidth      =   8250
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   741
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   330
      Left            =   4560
      Top             =   2160
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      _Version        =   393216
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   7080
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   450
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Historias Cl�nicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4665
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   2280
      Width           =   8015
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4110
         Index           =   0
         Left            =   150
         TabIndex        =   3
         Top             =   360
         Width           =   7680
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   13547
         _ExtentY        =   7250
         _StockProps     =   79
         Enabled         =   0   'False
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ubicaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   8015
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "AR1005.frx":0442
         Left            =   240
         List            =   "AR1005.frx":0444
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   600
         Width           =   5875
      End
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   1
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   6440
         MaxLength       =   5
         TabIndex        =   2
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label Label4 
         Caption         =   "L�nea"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Pto. Archivo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label8 
         Caption         =   "Pto. Ubicaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6440
         TabIndex        =   8
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label7 
         BackColor       =   &H8000000C&
         BorderStyle     =   1  'Fixed Single
         Enabled         =   0   'False
         Height          =   315
         Left            =   1660
         TabIndex        =   4
         Top             =   1200
         Width           =   4455
      End
      Begin VB.Label Label6 
         Caption         =   "Tipolog�a"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1660
         TabIndex        =   7
         Top             =   960
         Width           =   1215
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmUbicacionHHCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public objHHCC As New clsCWForm
Dim WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1

Public filtrar As Boolean
Dim num_puntos As Integer
Dim puntos As New Collection
Dim tipologias As New Collection
Dim cod_tipologia As String
Dim filtro As String

Sub comprobar()
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  'comprobar linea
  If Text3.Text <> "" Then
    'comprobar que es un numero correcto
    If Not IsNumeric(Text3.Text) Then 'no es correcto
      MsgBox "La entrada """ & Text3.Text & """ no es un n�mero de L�nea v�lida." & Chr(13) & "Introduzca un n�mero por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
      'reintroducir linea
      Text3.Text = ""
      Text3.SetFocus
    Else  'es correcto
      'comprobar si la linea existe
      Set c = objApp.rdoConnect
      Set q = c.CreateQuery("", "SELECT AR00CODTIPOLOGIA FROM AR0300 WHERE AR02CODPTOARCHIVO = ? AND AR03CODLINEA = ?")
      'q(0) = cod_ptoarchivo
      q(0) = puntos(Combo1.ListIndex + 1)
      q(1) = Text3.Text
      q.Execute
      Set r = q.OpenResultset
      If r.EOF Then  'la linea no existe
        MsgBox "No existe la L�nea " & Text3.Text & "." & Chr(13) & "Introduzca otra L�nea por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
        'reintroducir linea
        Text3.Text = ""
        Text3.SetFocus
      Else  'la linea existe
        'mostrar la descripcion de la tipologia
        If IsNull(r("AR00CODTIPOLOGIA")) Then
          Label7.Caption = "<Cualquiera>"
          cod_tipologia = "<Cualquiera>"
        Else
          Label7.Caption = tipologias(r("AR00CODTIPOLOGIA"))
          cod_tipologia = r("AR00CODTIPOLOGIA")
        End If
      End If
      r.Close
      q.Close
    End If
  End If
  'comprobar hueco
  If Text1.Text <> "" And Text3.Text <> "" Then
    If Not IsNumeric(Text1.Text) Then 'no es correcto
      MsgBox "La entrada """ & Text1.Text & """ no es un Punto de Ubicaci�n v�lido." & Chr(13) & "Introduzca un n�mero por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
      'reintroducir hueco
      Text1.Text = ""
      Text1.SetFocus
    Else  'es correcto
      If Val(Text1.Text) < 1 Then 'hueco < 1
        MsgBox "La entrada """ & Text1.Text & """ no es un Punto de Ubicaci�n v�lido." & Chr(13) & "Introduzca un n�mero mayor que 1 por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
        'reintroducir hueco
        Text1.Text = ""
        Text1.SetFocus
      Else  'hueco >= 1
        'comprobar si el hueco esta dentro del rango
        Set c = objApp.rdoConnect
        Set q = c.CreateQuery("", "SELECT AR03NUMHUECOS FROM AR0300 WHERE AR02CODPTOARCHIVO = ? AND AR03CODLINEA = ?")
        'q(0) = cod_ptoarchivo
        q(0) = puntos(Combo1.ListIndex + 1)
        q(1) = Text3.Text
        q.Execute
        Set r = q.OpenResultset
        If Val(Text1.Text) > r("AR03NUMHUECOS") Then 'fuera del rango
          MsgBox "Esta L�nea tiene " & Str(r("AR03NUMHUECOS")) & " Puntos de Ubicaci�n." & Chr(13) & "Introduzca un Punto de Ubicaci�n menor por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
          'reintroducir hueco
          Text1.Text = ""
          Text1.SetFocus
        End If
        r.Close
        q.Close
      End If
    End If
  End If
  If Text3.Text = "" Or Text1.Text = "" Then  'faltan datos
    objWin.CtrlGetInfo(grdDBGrid1(0).Columns("Ubicar")).blnReadOnly = True
    grdDBGrid1(0).Columns("Ubicar").BackColor = &H8000000F
  Else  'datos introducidos
    objWin.CtrlGetInfo(grdDBGrid1(0).Columns("Ubicar")).blnReadOnly = False
    grdDBGrid1(0).Columns("Ubicar").BackColor = &H80000005
  End If
End Sub

Sub filtro_on()
  Dim i As Integer
  dlgFiltroUbicacion.Show vbModal
  If filtrar Then
    filtro = ""
    If dlgFiltroUbicacion.desde <> -1 Then
      filtro = filtro & "CI22NUMHISTORIA >= " & Str(dlgFiltroUbicacion.desde) & " AND "
    End If
    If dlgFiltroUbicacion.hasta <> -1 Then
      filtro = filtro & "CI22NUMHISTORIA <= " & Str(dlgFiltroUbicacion.hasta) & " AND "
    End If
    If dlgFiltroUbicacion.tipologias.Count > 0 Then
      filtro = filtro & "AR00CODTIPOLOGIA IN ("
      For i = 1 To dlgFiltroUbicacion.tipologias.Count
        filtro = filtro & "'" & dlgFiltroUbicacion.tipologias(i) & "'"
        If i < dlgFiltroUbicacion.tipologias.Count Then
          filtro = filtro & ", "
        End If
      Next i
      filtro = filtro & ") AND "
    End If
  End If
  objHHCC.strWhere = "(" & filtro & " AR06CODESTADO = 'R'AND AR04FECINICIO >= TO_DATE('13/03/2000', 'DD/MM/YYYY') AND AR04FECFIN IS NULL AND AR02CODPTOARCHIVO = '" & puntos(Combo1.ListIndex + 1) & "')"
  objWin.DataRefresh
  Call enable_disable
End Sub
Sub filtro_off()
  filtrar = False
  filtro = ""
  objHHCC.strWhere = "(" & filtro & "AR06CODESTADO = 'R' AND AR04FECINICIO >= TO_DATE('13/03/2000', 'DD/MM/YYYY')AND AR04FECFIN IS NULL AND AR02CODPTOARCHIVO = '" & puntos(Combo1.ListIndex + 1) & "')"
  objWin.DataRefresh
  Call enable_disable
End Sub
Sub enable_disable()
  'nuevo
  tlbToolbar1.Buttons(2).Enabled = False
  mnuDatosOpcion(10).Enabled = False
  'borrar
  tlbToolbar1.Buttons(8).Enabled = False
  mnuDatosOpcion(60).Enabled = False
  'buscar
  tlbToolbar1.Buttons(16).Enabled = False
  mnuRegistroOpcion(10).Enabled = False
  'filtro
  If grdDBGrid1(0).Enabled Then
    tlbToolbar1.Buttons(18).Enabled = True
    mnuFiltroOpcion(10).Enabled = True
    If filtrar Then
      tlbToolbar1.Buttons(19).Enabled = True
      mnuFiltroOpcion(20).Enabled = True
    Else
      tlbToolbar1.Buttons(19).Enabled = False
      mnuFiltroOpcion(20).Enabled = False
    End If
  Else
    tlbToolbar1.Buttons(18).Enabled = False
    mnuFiltroOpcion(10).Enabled = False
    tlbToolbar1.Buttons(19).Enabled = False
    mnuFiltroOpcion(20).Enabled = False
  End If
End Sub

Private Sub Form_GotFocus()
  grdDBGrid1(0).Refresh
End Sub

Private Sub grdDBGrid1_Change(Index As Integer)
  Call objWin.CtrlDataChange
  Call enable_disable
End Sub

Private Sub objWin_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim sql As String
  Dim c As rdoConnection
  Dim q As rdoQuery
  If grdDBGrid1(0).Columns("Fecha Fin").Value <> "" Then
    Set c = objApp.rdoConnect
    sql = "INSERT INTO AR0400"
    sql = sql & "  (CI22NUMHISTORIA, AR00CODTIPOLOGIA, AR04FECINICIO,"
    sql = sql & "    AR06CODESTADO, AR02CODPTOARCHIVO, AR03CODLINEA,"
    sql = sql & "    AR04NUMHUECO, AR04FECFIN)"
    sql = sql & "  VALUES (?, ?, TO_DATE (?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, ?, ?)"
    Set q = c.CreateQuery("", sql)
    q(0) = grdDBGrid1(0).Columns("N�mero H.C.").Value
    q(1) = grdDBGrid1(0).Columns("Tipolog�a").Value
    q(2) = grdDBGrid1(0).Columns("Fecha Fin").Value
    q(3) = "U"
    q(4) = puntos(Combo1.ListIndex + 1)
    q(5) = grdDBGrid1(0).Columns("L�nea").Value
    q(6) = grdDBGrid1(0).Columns("Pto. Ubicaci�n").Value
    q(7) = Null
    q.Execute
    q.Close
  End If
End Sub

Private Sub Combo1_Click()
  If Combo1.ListIndex <> -1 Then
    Screen.MousePointer = vbHourglass
    'vaciar linea y hueco
    Text3.Text = ""
    Text1.Text = ""
    Label7.Caption = ""
    'habilitar linea y hueco
    Label4.Enabled = True
    Text3.Enabled = True
    Label6.Enabled = True
    Label7.Enabled = True
    Label8.Enabled = True
    Text1.Enabled = True
    'cargar grid
    grdDBGrid1(0).Enabled = True
    objHHCC.strWhere = "( AR06CODESTADO = 'R' AND AR04FECINICIO >= TO_DATE('13/03/2000', 'DD/MM/YYYY') AND AR02CODPTOARCHIVO = '" & puntos(Combo1.ListIndex + 1) & "' AND AR04FECFIN IS NULL)"
    objWin.DataRefresh
    Call enable_disable
    If grdDBGrid1(0).Rows = 0 Then 'el grid esta vacio
      'desabilitar linea y hueco
      Label4.Enabled = False
      Text3.Enabled = False
      Label6.Enabled = False
      Label7.Enabled = False
      Label8.Enabled = False
      Text1.Enabled = False
      grdDBGrid1(0).Enabled = False
      Screen.MousePointer = vbHourglass
    Else  'el grid tiene datos
      'habilitar linea
      Label4.Enabled = True
      Text3.Enabled = True
      Label6.Enabled = True
      Label7.Enabled = True
      Text3.SetFocus
    End If
  End If
  Screen.MousePointer = vbNormal
End Sub
Private Sub Combo1_KeyPress(KeyAscii As Integer)
  If Combo1.Text = "" Then
    'desabilitar linea y hueco
    Label4.Enabled = False
    Text3.Enabled = False
    Label6.Enabled = False
    Label7.Enabled = False
    Label8.Enabled = False
    Text1.Enabled = False
  End If
End Sub
Private Sub Form_Load()
  'declaraciones
  Dim cod As String
  Dim des As String
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  'inicializaciones
  MSRDC1.Connect = objApp.rdoConnect.Connect
  filtrar = False
  Set c = objApp.rdoConnect
  'cargar coleccion tipologias
  Set c = objApp.rdoConnect
  Set q = c.CreateQuery("", "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000")
  q.Execute
  Set r = q.OpenResultset
  Do Until r.EOF
    des = r(1)
    cod = r(0)
    tipologias.Add des, cod
    r.MoveNext
  Loop
  r.Close
  q.Close
  'cargar combo con puntos de archivo
  Set q = c.CreateQuery("", "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO")
  q.Execute
  Set r = q.OpenResultset
  Do Until r.EOF
    cod = r(0)
    des = r(1)
    'rellenar combo
    Combo1.AddItem des
    'rellenar coleccion
    puntos.Add cod
    'pasar a la siguiente linea
    r.MoveNext
  Loop
  r.Close
  q.Close
  
  Dim strKey   As String
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objWin = New clsCWWin
  Call objWin.WinCreateInfo(cwModeSingleEmpty, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  Call enable_disable
  With objWin.objDoc
    .cwPRJ = "CWSpy"
    .cwMOD = "M�dulo Roles"
    .cwAUT = "DocuTEX Bilbao"
    .cwDAT = "22-07-97"
    .cwDES = "Este formulario permite ubicar las historias clinicas"
    .cwUPD = "22-07-97 - DocuTEX Bilbao - Creaci�n del m�dulo"
    .cwEVT = ""
  End With
  
  With objHHCC
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  
    .strTable = "AR0400"

    Call .FormAddOrderField("CI22NUMHISTORIA", cwAscending)
  
    strKey = .strDataBase & .strTable
    
    .intCursorSize = -1
    .strName = "HHCC"
  End With
  
  With objWin
    Call .FormAddInfo(objHHCC, cwFormMultiLine)

    Call .GridAddColumn(objHHCC, "N�mero H.C.", "CI22NUMHISTORIA")             '3
    Call .GridAddColumn(objHHCC, "Tipolog�a", "AR00CODTIPOLOGIA")     '4
    Call .GridAddColumn(objHHCC, "Descripci�n Tipolog�a", "")         '5
    Call .GridAddColumn(objHHCC, "L�nea", "")                         '6
    Call .GridAddColumn(objHHCC, "Pto. Ubicaci�n", "")                '7
    Call .GridAddColumn(objHHCC, "Ubicar", "", cwBoolean)             '8
    Call .GridAddColumn(objHHCC, "Estado", "AR06CODESTADO")           '9
    Call .GridAddColumn(objHHCC, "BDL�nea", "AR03CODLINEA")           '10
    Call .GridAddColumn(objHHCC, "BDPto. Ubicaci�n", "AR04NUMHUECO")  '11
    Call .GridAddColumn(objHHCC, "Fecha Fin", "AR04FECFIN")           '12
    Call .FormCreateInfo(objHHCC)
    
    'asociar el campo descrip tipol a tipol
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grdDBGrid1(0).Tipolog�a")), "AR00CODTIPOLOGIA", "SELECT AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grdDBGrid1(0).Tipolog�a")), grdDBGrid1(0).Columns("grdDBGrid1(0).Descripci�n Tipolog�a"), "AR00DESTIPOLOGIA")

    'columnas de solo lectura
    .CtrlGetInfo(grdDBGrid1(0).Columns("L�nea")).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns("Pto. Ubicaci�n")).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns("Ubicar")).blnReadOnly = False

    'ocultar columnas
    '.CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInGrid = True  'no funcona!
    grdDBGrid1(0).Columns("Estado").Visible = False
    grdDBGrid1(0).Columns("BDL�nea").Visible = False
    grdDBGrid1(0).Columns("BDPto. Ubicaci�n").Visible = False
    grdDBGrid1(0).Columns("Fecha Fin").Visible = False
    
    'fijar tama�o de los campos del grid
    grdDBGrid1(0).Columns("N�mero H.C.").Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns("N�mero H.C.").Width = 1100
    grdDBGrid1(0).Columns("Tipolog�a").Width = 900
    grdDBGrid1(0).Columns("Tipolog�a").Alignment = ssCaptionAlignmentCenter
    grdDBGrid1(0).Columns("Descripci�n Tipolog�a").Width = 1725
    grdDBGrid1(0).Columns("L�nea").Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns("Pto. Ubicaci�n").Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns("Pto. Ubicaci�n").Width = 1200
    
    'lock columnas linea y hueco
    'grdDBGrid1(0).Columns(5).Locked = False  'no funciona!
    'grdDBGrid1(0).Columns(6).Locked = True   'no funciona!
    
    Call .WinRegister
    Call .WinStabilize
    Call enable_disable
 End With
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWin.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWin.WinProcess(cwProcessKeys, intKeyCode, intShift)
If intKeyCode = vbKeyF1 Then
  Call objApp.HelpContext
End If
End Sub
Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWin.WinExit
End Sub
Private Sub Form_Unload(intCancel As Integer)
  Call objWin.WinDeRegister
  Call objWin.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
  Dim fila As Integer
  Dim i As Integer
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  With grdDBGrid1(Index)
    If .col = 8 Then
      If objWin.CtrlGetInfo(.Columns("Ubicar")).blnReadOnly Then 'linea y hueco incorrectos
        MsgBox "Debe especificar la L�nea y el Punto de Ubicaci�n donde desea Ubicar la H.C." & Chr(13) & Chr(13) & "Introduzca esos datos primero." & Chr(13), vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
      Else  'linea y hueco correctos
        If .Columns(8).Value <> False Then    'check activo, desactivando
          'hacer cambios en la fila
          .Columns(8).Value = Unchecked
          .Columns("L�nea").Value = ""
          .Columns("Pto. Ubicaci�n").Value = ""
          .Columns("Fecha Fin").Value = ""
          'hacer que los cambios permanezcan
          fila = .row
          .row = 0
          .row = fila
          .RowChanged = False
          Call objWin.CtrlDataChange
          Call enable_disable
        Else  'check inactivo, activando
          If cod_tipologia <> "<Cualquiera>" And .Columns("Tipolog�a").Value <> cod_tipologia Then 'tipologias incompatibles
            MsgBox "La L�nea seleccionada s�lo soporta la Tipolog�a """ & Label7.Caption & """," & Chr(13) & "y la H.C. que desea Ubicar es de Tipolog�a """ & grdDBGrid1(0).Columns("Descripci�n Tipolog�a").Value & """.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
            Call objWin.CtrlDataChange
            .RowChanged = False
          Else
            .Columns(8).Value = True
            .Columns("L�nea").Value = Text3.Text
            .Columns("Pto. Ubicaci�n").Value = Text1.Text
            'calcular valor de fecha fin
            Set c = objApp.rdoConnect
            Set q = c.CreateQuery("", "SELECT SYSDATE FROM DUAL")
            q.Execute
            Set r = q.OpenResultset
            .Columns("Fecha Fin").Value = r("SYSDATE")
            Call objWin.CtrlDataChange
            Call enable_disable
            r.Close
            q.Close
          End If
        End If  'actualizacion auto al modificar col seleccion
      End If
    End If
  End With
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessData, intIndex, 0)
End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessEdit, intIndex, 0)
End Sub
Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessToolBar, 4, 0)  'grabar
  objHHCC.blnChanged = False
  objWin.DataRefresh
  Select Case intIndex
    Case 10
      Call filtro_on
    Case 20
      Call filtro_off
  End Select
End Sub
Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessRegister, intIndex, 0)
End Sub
Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessOptions, intIndex, 0)
End Sub
Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
   Call objWin.CtrlGotFocus
   Call enable_disable
End Sub
Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWin.GridDblClick
   Call grdDBGrid1_Click(intIndex)
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWin.GridChangeRowCol(vntLastRow, intLastCol)
  Call enable_disable
End Sub
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWin.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
  End If
End Sub

Private Sub Text1_KeyUp(KeyCode As Integer, Shift As Integer)
  objWin.CtrlGetInfo(grdDBGrid1(0).Columns("Ubicar")).blnReadOnly = True
  grdDBGrid1(0).Columns("Ubicar").BackColor = &H8000000F
End Sub

Private Sub Text1_LostFocus()
  Call comprobar
End Sub

Private Sub Text3_Change()
  objWin.CtrlGetInfo(grdDBGrid1(0).Columns("Ubicar")).blnReadOnly = True
  grdDBGrid1(0).Columns("Ubicar").BackColor = &H8000000F
End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
  End If
End Sub

Private Sub Text3_LostFocus()
  Call comprobar
End Sub

Private Sub tlbToolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
  Select Case Button.Index
    Case 18 'filtro on
      mnuFiltroOpcion_Click (10)
    Case 19 'filtro off
      mnuFiltroOpcion_Click (20)
    Case Else
      Call objWin.WinProcess(cwProcessToolBar, Button.Index, 0)
  End Select
  If Button.Index <> 30 Then  'no ha pulsado en boton salir
    Call enable_disable
  End If
End Sub
