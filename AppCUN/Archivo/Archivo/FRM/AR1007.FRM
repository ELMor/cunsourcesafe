VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form dlgFiltroUbicacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Filtro"
   ClientHeight    =   2415
   ClientLeft      =   2850
   ClientTop       =   1620
   ClientWidth     =   6975
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2415
   ScaleWidth      =   6975
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlMainSmall"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   4
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Lanzar"
            Object.Tag             =   ""
            ImageKey        =   "execute"
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Salir"
            Object.Tag             =   ""
            ImageKey        =   "exit"
         EndProperty
      EndProperty
      BorderStyle     =   1
      MouseIcon       =   "AR1007.frx":0000
   End
   Begin VB.Frame fraHHCC 
      Caption         =   "Historias Cl�nicas"
      Height          =   1815
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   6735
      Begin VB.ListBox lstTipologia 
         Height          =   1035
         ItemData        =   "AR1007.frx":031A
         Left            =   3600
         List            =   "AR1007.frx":031C
         MultiSelect     =   1  'Simple
         TabIndex        =   2
         Top             =   480
         Width           =   2895
      End
      Begin VB.TextBox txtHasta 
         Height          =   315
         Left            =   1920
         MaxLength       =   7
         TabIndex        =   1
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox txtDesde 
         Height          =   315
         Left            =   360
         MaxLength       =   7
         TabIndex        =   0
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblTipologia 
         Caption         =   "Tipolog�a"
         Height          =   255
         Left            =   3600
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblHasta 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblDesde 
         Caption         =   "Desde"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   240
         Width           =   615
      End
   End
   Begin ComctlLib.ImageList imlMainBig 
      Left            =   2520
      Top             =   2550
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin ComctlLib.ImageList imlMainSmall 
      Left            =   1560
      Top             =   2550
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   28
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":031E
            Key             =   "main"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":0638
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":0952
            Key             =   "function"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":0C6C
            Key             =   "window"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":0F86
            Key             =   "report"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":12A0
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":15BA
            Key             =   "process"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":18D4
            Key             =   "new"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":1BEE
            Key             =   "big"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":1F08
            Key             =   "small"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":2222
            Key             =   "list"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":253C
            Key             =   "details"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":2856
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":2B70
            Key             =   "refresh"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":2E8A
            Key             =   "save"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":31A4
            Key             =   "print"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":34BE
            Key             =   "CheckOff"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":37D8
            Key             =   "CheckOn"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":3AF2
            Key             =   "first"
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":3E0C
            Key             =   "last"
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":4126
            Key             =   "next"
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":4440
            Key             =   "previous"
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":475A
            Key             =   "undo"
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":4A74
            Key             =   "delete"
         EndProperty
         BeginProperty ListImage25 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":4D8E
            Key             =   "filoff"
         EndProperty
         BeginProperty ListImage26 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":50A8
            Key             =   "New"
         EndProperty
         BeginProperty ListImage27 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":53C2
            Key             =   "filon"
         EndProperty
         BeginProperty ListImage28 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1007.frx":56DC
            Key             =   "execute"
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList imlImageList 
      Left            =   3600
      Top             =   2520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuSalir 
         Caption         =   "&Salir"
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuLanzar 
         Caption         =   "&Lanzar"
      End
   End
End
Attribute VB_Name = "dlgFiltroUbicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public desde As Long
Public hasta As Long
Public tipologias As New Collection

Dim c As rdoConnection
Dim q As rdoQuery
Dim r As rdoResultset
Dim tipologias_lst As New Collection
Dim tipologias_ind As New Collection
Function verificar_desde() As Boolean
  verificar_desde = False
  If txtDesde.Text <> "" Then
    If Not IsNumeric(txtDesde.Text) Then
      MsgBox "No es un n�mero v�lido.", vbOKOnly + vbExclamation + vbApplicationModal, "Valor incorrecto"
      txtDesde.Text = ""
      txtDesde.SetFocus
    Else
      If Val(txtDesde.Text) < 0 Then
        MsgBox "El n�mero de H.C. debe mayor que 0.", vbOKOnly + vbExclamation + vbApplicationModal, "Valor incorrecto"
        txtDesde.Text = ""
        txtDesde.SetFocus
      Else
        If IsNumeric(txtHasta.Text) And Val(txtHasta.Text) >= 0 And Val(txtDesde.Text) > Val(txtHasta.Text) Then
          MsgBox "El valor del campo Desde debe ser menor o igual al del campo Hasta.", vbOKOnly + vbExclamation + vbApplicationModal, "Valor incorrecto"
          txtDesde.Text = ""
          txtDesde.SetFocus
        Else
          txtHasta.SetFocus
          verificar_desde = True
        End If
      End If
    End If
  Else
    verificar_desde = True
  End If
End Function
Function verificar_hasta() As Boolean
  verificar_hasta = False
  If txtHasta.Text <> "" Then
    If Not IsNumeric(txtHasta.Text) Then
      MsgBox "No es un n�mero v�lido.", vbOKOnly + vbExclamation + vbApplicationModal, "Valor incorrecto"
      txtHasta.Text = ""
      txtHasta.SetFocus
    Else
      If Val(txtHasta.Text) < 0 Then
        MsgBox "El n�mero de H.C. debe ser mayor que 0.", vbOKOnly + vbExclamation + vbApplicationModal, "Valor incorrecto"
        txtHasta.Text = ""
        txtHasta.SetFocus
      Else
        If IsNumeric(txtDesde.Text) And Val(txtDesde.Text) >= 0 And Val(txtHasta.Text) < Val(txtDesde.Text) Then
          MsgBox "El valor del campo Hasta debe ser mayor o igual al del campo Desde.", vbOKOnly + vbExclamation + vbApplicationModal, "Valor incorrecto"
          txtHasta.Text = ""
          txtHasta.SetFocus
        Else
          lstTipologia.SetFocus
          verificar_hasta = True
        End If
      End If
    End If
  Else
    verificar_hasta = True
  End If
End Function
Private Sub Form_Load()
  Dim i As Integer
  'inicializaciones
  Set c = objApp.rdoConnect
  'salir = True
  'vaciar colecciones
  For i = tipologias_lst.Count To 1 Step -1
    tipologias_lst.Remove (i)
    tipologias_ind.Remove (i)
  Next i
  'cargar lstTipologia
  Set q = c.CreateQuery("", "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA")
  Set r = q.OpenResultset
  i = 0
  While Not r.EOF
    lstTipologia.AddItem r(1)
    tipologias_lst.Add CStr(r(0))
    tipologias_ind.Add i, CStr(r(0))
    i = i + 1
    r.MoveNext
  Wend
  r.Close
  q.Close
  'mostrar filtro anterior
  If frmUbicacionHHCC.filtrar Then
    If desde = -1 Then
      txtDesde.Text = ""
    Else
      txtDesde.Text = desde
    End If
    If hasta = -1 Then
      txtHasta.Text = ""
    Else
      txtHasta.Text = hasta
    End If
    For i = 1 To tipologias.Count
      lstTipologia.Selected(tipologias_ind(tipologias(i))) = True
    Next i
  End If
End Sub
Private Sub mnuLanzar_Click()
  Dim correcto As Integer
  Dim selec As Boolean
  Dim i As Integer
  'verificar datos
  correcto = verificar_desde
  If correcto Then
    correcto = verificar_hasta
    If correcto Then
      'comprobar si se ha seleccionado alguna tipologia
      selec = False
      For i = 0 To lstTipologia.ListCount - 1
        If lstTipologia.Selected(i) Then
          selec = True
        End If
      Next i
      'comprobar si se ha seleccionado algo
      If txtDesde.Text <> "" Or txtHasta.Text <> "" Or selec Then
        'actualizar variables publicas locales
        If txtDesde.Text = "" Then
          desde = -1
        Else
          desde = Val(txtDesde.Text)
        End If
        If txtHasta.Text = "" Then
          hasta = -1
        Else
          hasta = Val(txtHasta.Text)
        End If
        For i = 0 To lstTipologia.ListCount - 1
          If lstTipologia.Selected(i) Then
            tipologias.Add (tipologias_lst(i + 1))
          End If
        Next i
        'actualizar las variables publicas externas
        frmUbicacionHHCC.filtrar = True
      End If
      Unload Me
    End If
  End If
End Sub
Private Sub mnuSalir_Click()
  Unload Me
End Sub
Private Sub tlbtoolbar_ButtonClick(ByVal Button As ComctlLib.Button)
  Dim correcto As Integer
  Dim i As Integer
  Dim selec As Boolean
  Select Case Button.Index
    Case 2
      mnuLanzar_Click
    Case 4
      mnuSalir_Click
  End Select
End Sub
Private Sub txtDesde_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    Call verificar_desde
  End If
  If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
  End If
End Sub
Private Sub txtDesde_LostFocus()
  Call verificar_desde
End Sub
Private Sub txtHasta_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    Call verificar_hasta
  End If
  If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
  End If
End Sub
Private Sub txtHasta_LostFocus()
  Call verificar_hasta
End Sub
