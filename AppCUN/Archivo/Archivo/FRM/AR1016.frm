VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form dlgSelCtaPtosUbicacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Seleccionar Consulta de Entradas/Salidas en Puntos de Ubicaci�n"
   ClientHeight    =   5265
   ClientLeft      =   2085
   ClientTop       =   1335
   ClientWidth     =   6825
   ControlBox      =   0   'False
   HelpContextID   =   24
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5265
   ScaleWidth      =   6825
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   24
      Left            =   5040
      TabIndex        =   27
      Top             =   2760
      Width           =   1575
   End
   Begin VB.Frame Frame6 
      Caption         =   "Puntos de Ubicaci�n"
      Height          =   975
      Index           =   2
      Left            =   180
      TabIndex        =   24
      Top             =   4080
      Width           =   4695
      Begin VB.TextBox txtNum 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   3
         Left            =   3000
         MaxLength       =   5
         TabIndex        =   8
         Top             =   390
         Width           =   1395
      End
      Begin VB.TextBox txtNum 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   2
         Left            =   840
         MaxLength       =   5
         TabIndex        =   7
         Top             =   390
         Width           =   1395
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Index           =   2
         Left            =   2400
         TabIndex        =   26
         Top             =   390
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   25
         Top             =   390
         Width           =   855
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "L�neas"
      Height          =   975
      Index           =   1
      Left            =   180
      TabIndex        =   21
      Top             =   3000
      Width           =   4695
      Begin VB.TextBox txtNum 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   0
         Left            =   840
         MaxLength       =   4
         TabIndex        =   5
         Top             =   390
         Width           =   1395
      End
      Begin VB.TextBox txtNum 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   1
         Left            =   3000
         MaxLength       =   4
         TabIndex        =   6
         Top             =   390
         Width           =   1395
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   23
         Top             =   390
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   22
         Top             =   390
         Width           =   615
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Hora de Proceso"
      Height          =   975
      Index           =   0
      Left            =   180
      TabIndex        =   17
      Top             =   1920
      Width           =   4695
      Begin VB.TextBox txtHora 
         Height          =   285
         Index           =   1
         Left            =   3000
         MaxLength       =   5
         TabIndex        =   4
         Top             =   390
         Width           =   1035
      End
      Begin VB.TextBox txtHora 
         Height          =   285
         Index           =   0
         Left            =   840
         MaxLength       =   5
         TabIndex        =   3
         Top             =   390
         Width           =   1035
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Index           =   0
         Left            =   2400
         TabIndex        =   20
         Top             =   390
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   19
         Top             =   390
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo Proceso"
      Height          =   1695
      Left            =   5040
      TabIndex        =   16
      Top             =   3360
      Width           =   1575
      Begin VB.CheckBox chkTipo 
         Caption         =   "Salida"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   10
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CheckBox chkTipo 
         Caption         =   "Entrada"
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   9
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Proceso"
      Height          =   975
      Left            =   180
      TabIndex        =   13
      Top             =   840
      Width           =   4695
      Begin SSCalendarWidgets_A.SSDateCombo datFecha 
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   1
         Top             =   360
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin SSCalendarWidgets_A.SSDateCombo datFecha 
         Height          =   375
         Index           =   1
         Left            =   3000
         TabIndex        =   2
         Top             =   360
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin VB.Label Label6 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2400
         TabIndex        =   14
         Top             =   480
         Width           =   495
      End
   End
   Begin VB.ComboBox cboArchivo 
      BackColor       =   &H00FFFF00&
      Height          =   315
      ItemData        =   "AR1016.frx":0000
      Left            =   180
      List            =   "AR1016.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4695
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5040
      TabIndex        =   12
      Top             =   1920
      Width           =   1575
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   5040
      TabIndex        =   11
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Punto de Archivo"
      Height          =   255
      Left            =   180
      TabIndex        =   18
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "dlgSelCtaPtosUbicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim c As rdoConnection
Dim q As rdoQuery
Dim r As rdoResultset
Dim puntos As New Collection

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 24)
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdConsultar_Click()
  'declaraciones
  Dim CORRECTO As Boolean
  Dim sql_e As String
  Dim sql_f1 As String
  Dim sql_f2 As String
  Dim sql_s As String
  Dim sSelectTabla As String
  Dim sSelectRPT As String
  Dim sql As String
  'comprobaciones
  CORRECTO = True
  'pto archivo
  If CORRECTO And cboArchivo.ListIndex = -1 Then
    MsgBox "Debe especificar un Punto de Archivo", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    cboArchivo.SetFocus
    CORRECTO = False
  End If
  'fecha movimiento
  If CORRECTO And datFecha(0).FormattedText <> "" And Not IsDate(datFecha(0).FormattedText) Then
    MsgBox "El contenido del campo Fecha de Movimiento Desde no es una fecha correcta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    datFecha(0).SetFocus
    CORRECTO = False
  End If
  If CORRECTO And datFecha(1).FormattedText <> "" And Not IsDate(datFecha(1).FormattedText) Then
    MsgBox "El contenido del campo Fecha de Movimiento Hasta no es una fecha correcta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    datFecha(1).SetFocus
    CORRECTO = False
  End If
  If CORRECTO And datFecha(0).FormattedText <> "" And datFecha(1).FormattedText <> "" Then
    If DateValue(datFecha(0).Date) > DateValue(datFecha(1).Date) Then
      MsgBox "Fecha Desde debe ser menor que Fecha Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
      datFecha(0).SetFocus
      CORRECTO = False
    End If
  End If
  'hora movimiento
  If CORRECTO And txtHora(0).Text <> "" And txtHora(1).Text <> "" Then
    If CDate(txtHora(0).Text) > CDate(txtHora(1).Text) Then
      MsgBox "La Hora Desde debe ser menor que la Hora Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
      txtHora(0).SetFocus
      CORRECTO = False
    End If
  End If
  'lineas movimiento
  If CORRECTO And txtNum(0).Text <> "" And txtNum(1).Text <> "" And Val(txtNum(0).Text) > Val(txtNum(1).Text) Then
    MsgBox "La L�nea Desde debe ser menor que la L�nea Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    txtNum(0).SetFocus
    CORRECTO = False
  End If
  'puntos ubicacion mivimiento
  If CORRECTO And txtNum(2).Text <> "" And txtNum(3).Text <> "" And Val(txtNum(2).Text) > Val(txtNum(3).Text) Then
    MsgBox "El Punto de Ubicaci�n Desde debe ser menor que el Punto de Ubicaci�n Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    txtNum(2).SetFocus
    CORRECTO = False
  End If
  If CORRECTO Then 'comprobaciones superadas
      sSelectTabla = "SELECT "
      sSelectTabla = sSelectTabla & "H.AR03CODLINEA AS ""L�nea"", "
      sSelectTabla = sSelectTabla & "H.AR04NUMHUECO AS ""Pto. Ubicaci�n"", "
      sSelectTabla = sSelectTabla & "H.AR04FECINICIO AS ""Fecha Entrada"", "
      sSelectTabla = sSelectTabla & "H.AR04FECFIN AS ""Fecha Salida"", "
      sSelectTabla = sSelectTabla & "H.CI22NUMHISTORIA AS ""N�mero H. C."", "
      sSelectTabla = sSelectTabla & "T.AR00DESTIPOLOGIA AS ""Tipolog�a"", "
      sSelectTabla = sSelectTabla & "P.AR02DESPTOARCHIVO AS ""Pto Archivo"", "
      ' Select para Report
      sSelectRPT = sSelectTabla & "C.CI22NOMBRE,C.CI22PRIAPEL,C.CI22SEGAPEL "
      'Select para Tabla
      sSelectTabla = sSelectTabla & "C.CI22NOMBRE||' '||C.CI22PRIAPEL||' '||C.CI22SEGAPEL AS ""Paciente"" "
      ' sql_e
      sql_e = "FROM "
      sql_e = sql_e & "CI2200 C, "
      sql_e = sql_e & "AR0200 P, "
      sql_e = sql_e & "AR0000 T, "
      sql_e = sql_e & "AR0400 H "
      sql_e = sql_e & "WHERE "
      sql_e = sql_e & "H.AR06CODESTADO = 'U' AND "
      ' sql_e = sql_e & "H.AR06CODESTADO = 'U' AND "
      sql_f1 = ""
      If chkTipo(0).Value = Checked Or chkTipo(0).Value = Unchecked And chkTipo(1).Value = Unchecked Then 'entrada
       If datFecha(0).FormattedText <> "" Then
        sql_f1 = " TRUNC(H.AR04FECINICIO) >= TO_DATE ('" & datFecha(0).Date & "', 'DD/MM/YYYY') "
       End If
       If datFecha(1).FormattedText <> "" Then
         If sql_f1 <> "" Then
          sql_f1 = sql_f1 & " AND "
         End If
         sql_f1 = sql_f1 & " TRUNC(H.AR04FECINICIO) <= TO_DATE ('" & datFecha(1).Date & "', 'DD/MM/YYYY') "
       End If
       If txtHora(0).Text <> "" Then
         If sql_f1 <> "" Then
          sql_f1 = sql_f1 & " AND "
         End If
         sql_f1 = sql_f1 & " TO_CHAR(H.AR04FECINICIO, 'HH24:MI') >= '" & txtHora(0).Text & "' "
       End If
       If txtHora(1).Text <> "" Then
         If sql_f1 <> "" Then
          sql_f1 = sql_f1 & " AND "
         End If
         sql_f1 = sql_f1 & " TO_CHAR(H.AR04FECINICIO, 'HH24:MI') <= '" & txtHora(1).Text & "' "
       End If
       If sql_f1 <> "" Then
          sql_f1 = "( " & sql_f1 & " )"
       End If
      End If
      sql_f2 = ""
      If chkTipo(1).Value = Checked Or chkTipo(0).Value = Unchecked And chkTipo(1).Value = Unchecked Then 'entrada
       If datFecha(0).FormattedText <> "" Then
        sql_f2 = "TRUNC (H.AR04FECFIN) >= TO_DATE ('" & datFecha(0).Date & "', 'DD/MM/YYYY') "
       End If
       If datFecha(1).FormattedText <> "" Then
         If sql_f2 <> "" Then
          sql_f2 = sql_f2 & " AND "
         End If
         sql_f2 = sql_f2 & "TRUNC (H.AR04FECFIN) <= TO_DATE ('" & datFecha(1).Date & "', 'DD/MM/YYYY') "
       End If
       If txtHora(0).Text <> "" Then
         If sql_f2 <> "" Then
          sql_f2 = sql_f2 & " AND "
         End If
         sql_f2 = sql_f2 & "TO_CHAR(H.AR04FECFIN, 'HH24:MI') >= '" & txtHora(0).Text & "' "
       End If
       If txtHora(1).Text <> "" Then
         If sql_f2 <> "" Then
          sql_f2 = sql_f2 & " AND "
         End If
         sql_f2 = sql_f2 & "TO_CHAR(H.AR04FECFIN, 'HH24:MI') <= '" & txtHora(1).Text & "'  "
       End If
       
       If sql_f2 <> "" Then
          sql_f2 = "( " & sql_f2 & " )"
       End If
      End If
      If sql_f1 <> "" And sql_f2 <> "" Then
        sql_e = sql_e & " ( " & sql_f1 & " OR " & sql_f2 & " ) AND "
      Else
        If chkTipo(1).Value = Checked And sql_f2 = "" Then
          If chkTipo(0).Value = Unchecked Then
            If sql_f1 <> "" Then
              sql_f2 = " AND H.AR04FECFIN IS NOT NULL "
            Else
              sql_f2 = " H.AR04FECFIN IS NOT NULL "
            End If
          End If
        End If
        sql_e = sql_e & sql_f1 & sql_f2
        If sql_f1 <> "" Or sql_f2 <> "" Then
          sql_e = sql_e & " AND "
        End If
      End If
      ' PUNTO DE ARCHIVO Y HUECOS
      sql_e = sql_e & "H.AR02CODPTOARCHIVO ='" & puntos(cboArchivo.ListIndex + 1) & "' AND "
      If txtNum(0).Text = "" And txtNum(1).Text = "" Then
        sql_e = sql_e & "H.AR03CODLINEA IS NOT NULL AND "
      Else
        If txtNum(0).Text <> "" Then
          sql_e = sql_e & "H.AR03CODLINEA >= " & txtNum(0).Text & " AND "
        End If
        If txtNum(1).Text <> "" Then
          sql_e = sql_e & "H.AR03CODLINEA <= " & txtNum(1).Text & " AND "
        End If
      End If
      If txtNum(2).Text = "" And txtNum(3).Text = "" Then
        sql_e = sql_e & "H.AR04NUMHUECO IS NOT NULL AND "
      Else
        If txtNum(2).Text <> "" Then
          sql_e = sql_e & "H.AR04NUMHUECO >= " & txtNum(2).Text & " AND "
        End If
        If txtNum(3).Text <> "" Then
          sql_e = sql_e & "H.AR04NUMHUECO <= " & txtNum(3).Text & " AND "
        End If
      End If
      sql_e = sql_e & "T.AR00CODTIPOLOGIA = H.AR00CODTIPOLOGIA AND "
      sql_e = sql_e & "P.AR02CODPTOARCHIVO = H.AR02CODPTOARCHIVO AND "
      sql_e = sql_e & "C.CI22NUMHISTORIA(+) = H.CI22NUMHISTORIA "
      sql = sql_e
      sql = sql & " ORDER BY 1, 2 "
      'mostrar resultados consulta
      Screen.MousePointer = vbHourglass
      frmCtaPtosUbicacion.sSelectTabla = sSelectTabla
      frmCtaPtosUbicacion.sSelectRPT = sSelectRPT
      frmCtaPtosUbicacion.sql = sql
      frmCtaPtosUbicacion.Show vbModal
      Set frmCtaPtosUbicacion = Nothing
    End If
End Sub

Private Sub Form_Load()
  'declaraciones
  Dim i As Integer
  'inicializaciones
  datFecha(0).Date = Date
  datFecha(1).Date = Date
  'cargar combos con puntos de archivo
  Set c = objApp.rdoConnect
  Set q = c.CreateQuery("", "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO")
  Set r = q.OpenResultset
  Set puntos = Nothing
  Do Until r.EOF
    cboArchivo.AddItem CStr(r(1))
    puntos.Add CStr(r(0))
    r.MoveNext
  Loop
  r.Close
  q.Close
End Sub


Private Sub txtHora_LostFocus(Index As Integer)
  If txtHora(Index).Text <> "" Then
    If Not Horavalida(txtHora(Index).Text) Then
      MsgBox "Hora no v�lida", vbOKOnly + vbExclamation + vbApplicationModal, "Hora incorrecta"
      txtHora(Index).SetFocus
    End If
  End If
End Sub

Private Sub txtNum_KeyPress(Index As Integer, KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtNum_LostFocus(Index As Integer)
  If txtNum(Index).Text <> "" Then
    If Not IsNumeric(txtNum(Index).Text) Then
      MsgBox "Debe introducir un valor num�rico", vbOKOnly + vbExclamation + vbApplicationModal, "Dato incorrecto"
      txtNum(Index).SetFocus
    End If
  End If
End Sub
