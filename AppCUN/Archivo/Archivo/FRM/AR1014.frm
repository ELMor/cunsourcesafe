VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form dlgSelCtaProcesos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Seleccionar Consulta de Procesos"
   ClientHeight    =   4680
   ClientLeft      =   2085
   ClientTop       =   1335
   ClientWidth     =   8475
   ControlBox      =   0   'False
   HelpContextID   =   27
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4680
   ScaleWidth      =   8475
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Caption         =   "Orden"
      Height          =   1095
      Left            =   6720
      TabIndex        =   31
      Top             =   360
      Width           =   1455
      Begin VB.OptionButton OptHora 
         Caption         =   "Fecha/Hora"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton OptNumero 
         Caption         =   "N�mero HC"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   27
      Left            =   6720
      TabIndex        =   30
      Top             =   4080
      Width           =   1575
   End
   Begin VB.Frame Frame6 
      Caption         =   "HHCC"
      Height          =   1815
      Left            =   180
      TabIndex        =   25
      Top             =   2640
      Width           =   4575
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   3
         Left            =   2880
         MaxLength       =   7
         TabIndex        =   6
         Top             =   270
         Width           =   800
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   2
         Left            =   840
         MaxLength       =   7
         TabIndex        =   5
         Top             =   270
         Width           =   800
      End
      Begin VB.ListBox List2 
         Height          =   1035
         ItemData        =   "AR1014.frx":0000
         Left            =   840
         List            =   "AR1014.frx":0002
         MultiSelect     =   1  'Simple
         TabIndex        =   7
         Top             =   600
         Width           =   3495
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2400
         TabIndex        =   29
         Top             =   270
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   270
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Tipolog�a"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo de Proceso"
      Height          =   4095
      Left            =   4920
      TabIndex        =   24
      Top             =   360
      Width           =   1635
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   15
         Top             =   3720
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2187;450"
         Value           =   "0"
         Caption         =   "Traslado"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   14
         Top             =   3240
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2187;450"
         Value           =   "0"
         Caption         =   "Pr�stamo"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   13
         Top             =   2760
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2187;450"
         Value           =   "0"
         Caption         =   "Reubicaci�n"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   12
         Top             =   2280
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2187;450"
         Value           =   "0"
         Caption         =   "Ubicaci�n"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   11
         Top             =   1800
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2187;450"
         Value           =   "0"
         Caption         =   "Devoluci�n"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   10
         Top             =   1260
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2196;661"
         Value           =   "0"
         Caption         =   "Entrega"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   840
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2187;450"
         Value           =   "0"
         Caption         =   "Revisi�n"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin MSForms.OptionButton optProceso 
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   1245
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         DisplayStyle    =   5
         Size            =   "2196;450"
         Value           =   "0"
         Caption         =   "Alta"
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Proceso"
      Height          =   735
      Left            =   180
      TabIndex        =   21
      Top             =   840
      Width           =   4575
      Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   1
         Top             =   240
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
         Height          =   375
         Index           =   1
         Left            =   2880
         TabIndex        =   2
         Top             =   240
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin VB.Label Label6 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2400
         TabIndex        =   22
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFF00&
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.Frame Frame3 
      Caption         =   "Hora de Proceso"
      Height          =   855
      Left            =   180
      TabIndex        =   18
      Top             =   1680
      Width           =   4575
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   1
         Left            =   2880
         MaxLength       =   5
         TabIndex        =   4
         Top             =   345
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   0
         Left            =   840
         MaxLength       =   5
         TabIndex        =   3
         Top             =   345
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2400
         TabIndex        =   19
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   6720
      TabIndex        =   17
      Top             =   2520
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   6720
      TabIndex        =   16
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Punto de Archivo"
      Height          =   255
      Left            =   180
      TabIndex        =   27
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "dlgSelCtaProcesos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim rdopuntos As rdoResultset
Dim puntos As New Collection
Dim tipologias As New Collection
Dim PUNTOUBICACION As String
Dim PUNTOUBICACION2 As String
Dim DEPARTAMENTOSQL As String
Dim SELDEPART As String
Dim TABLASSQL As String
Dim sql As String
Dim tipoprosql As String
Dim sSent1 As String
Dim sSent2 As String
Dim sSent3 As String
Dim sSent4 As String
Dim S_IN As String
Dim estado As String
Dim selec As String

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 27)
End Sub

Private Sub Combo1_Click()
Dim indcoleccion As Integer
If Combo1.ListIndex > -1 Then
    indcoleccion = Combo1.ItemData(Combo1.ListIndex)
    PUNTOUBICACION = puntos.Item(indcoleccion)
End If
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
  'declaraciones
  Dim i As Integer
  Dim sql
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  'inicializaciones
  OptNumero = True
  Set c = objApp.rdoConnect
  SSDateCombo1(0).Date = Date
  SSDateCombo1(1).Date = Date
  'cargar puntos de carchivo
  sql = "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO"
  Set q = c.CreateQuery("", sql)
  q.Execute
  Set r = q.OpenResultset
  If Not r.EOF Then
    Combo1.Clear
    i = 0
    Set puntos = New Collection
    While Not r.EOF
      Combo1.AddItem (r("AR02DESPTOARCHIVO"))
      Combo1.ItemData(i) = i + 1
      puntos.Add (r("AR02CODPTOARCHIVO"))
      r.MoveNext
      i = i + 1
    Wend
  End If
  r.Close
  q.Close
  'cargar tipologias
  sql = "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA"
  Set q = c.CreateQuery("", sql)
  q.Execute
  Set r = q.OpenResultset
  If Not r.EOF Then
    List2.Clear
    i = 0
    Set tipologias = New Collection
    While Not r.EOF
      List2.AddItem (r("AR00DESTIPOLOGIA"))
      List2.ItemData(i) = i + 1
      tipologias.Add (r("AR00CODTIPOLOGIA"))
      r.MoveNext
      i = i + 1
    Wend
  End If
  r.Close
  q.Close
End Sub



Private Sub SSDateCombo1_Click(Index As Integer)
Select Case Index
' Combo fecha proceso
Case 0  'desde
Case 1  'hasta

End Select

End Sub

Private Sub Text1_Change(Index As Integer)
Select Case Index
'HOORA
Case 0  'desde
Case 1  'hasta
'HHCC
Case 2  'desde
Case 3  'hasta
End Select
End Sub


Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 2 Or Index = 3 Then
    If (KeyAscii < 48 Or KeyAscii > 57) Then
        If KeyAscii <> 8 Then
            KeyAscii = 13
        End If
    End If
End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
If Index = 0 Or Index = 1 Then
    If Text1(Index).Text <> "" Then
        If Not Horavalida(Text1(Index).Text) Then
            MsgBox "Hora no v�lida", vbOKOnly + vbExclamation + vbApplicationModal, "Hora incorrecta"
            Text1(Index).SetFocus
        End If
    End If
End If

If Index = 2 Or Index = 3 Then
    If Text1(Index).Text <> "" Then
        If Not IsNumeric(Text1(Index).Text) Then
            MsgBox "Debe introducir un valor num�rico", vbOKOnly + vbExclamation + vbApplicationModal, "Dato incorrecto"
            Text1(Index).SetFocus
        End If
    End If
 
End If
End Sub

Private Sub Command1_Click()
Dim sSent As String
Dim fechassql As String
Dim horassql As String
Dim historiasql As String
Dim tiposql As String
Dim i As Integer
Dim cond1 As Boolean
Dim cond2 As Boolean
Dim cond3 As Boolean
Dim cond4 As Boolean
    
    ' Puntos de Archivo

If Combo1.Text = "" Then
    MsgBox "Debe introducir el Punto de Archivo", vbInformation + vbOKOnly, "Atenci�n"
    Combo1.SetFocus
    Exit Sub
ElseIf Not IsDate(SSDateCombo1(0).FormattedText) Or Not IsDate(SSDateCombo1(1).FormattedText) Then
    MsgBox "Debe Introducir un Rango de Fechas", vbOKOnly + vbInformation, "Atenci�n"
    Exit Sub
Else
    For i = 0 To 7
        If optProceso(i).Value = True Then
            Exit For
        End If
    Next i
    If i = 8 Then
        MsgBox "Debe seleccionar el Tipo de Proceso", vbInformation + vbOKOnly, "Atenci�n"
        optProceso(0).SetFocus
        Exit Sub
    Else
      sql = ""
      ' SQL = "Select * From AR0400 Where AR02CODPTOARCHIVO = '" & puntos.Item(Combo1.ItemData(Combo1.ListIndex)) & "'"
    End If
End If

    'Fechas de Proceso
' Compruebo que las segunda fecha sea mayor
If IsDate(SSDateCombo1(0).FormattedText) And IsDate(SSDateCombo1(1).FormattedText) Then
   If DateValue(SSDateCombo1(0).Date) > DateValue(SSDateCombo1(1).Date) Then
   'If SSDateCombo1(1).Date < SSDateCombo1(0).Date Then
    Call MsgBox("Hasta debe ser mayor que Desde", vbOKOnly + vbInformation, "Atenci�n")
       Exit Sub
   End If
End If
If IsDate(SSDateCombo1(0).FormattedText) Then 'SSDateCombo1(0).IsDateValid Then
    'fechassql = " AND to_date(V.AR04FECINICIO,'DD/MM/YYYY') >= '" & SSDateCombo1(0).Date & "' "
    fechassql = " V.AR04FECINICIO >= TO_DATE('" & SSDateCombo1(0).Date & " 00:00', 'DD/MM/YYYY HH24:MI') AND "
   ' SQL = SQL & fechassql
    
 '   where to_date(ar04fecinicio,'DD/MM/YYYY') = to_date('6/10/99','DD/MM/YYYY')
End If
If IsDate(SSDateCombo1(1).FormattedText) Then 'If SSDateCombo1(1).IsDateValid Then
    'fechassql = fechassql & " AND to_date(V.AR04FECINICIO,'DD/MM/YYYY') <= '" & SSDateCombo1(1).Date & "' "
    fechassql = fechassql & " V.AR04FECINICIO <= TO_DATE('" & SSDateCombo1(1).Date & " 23:59', 'DD/MM/YYYY HH24:MI') AND "
    'SQL = SQL & fechassql
End If

' Horas
  ' Compruebo que las segunda hora sea mayor
If Text1(0).Text <> "" And Text1(1).Text <> "" Then
    If CDate(Text1(1).Text) < CDate(Text1(0).Text) Then
       Call MsgBox("Hasta debe ser mayor que Desde", vbOKOnly + vbInformation, "Atenci�n")
       Exit Sub
    End If
End If

If Text1(0).Text <> "" Then
   horassql = " to_char(V.ar04fecinicio,'HH24:MI') >= '" & Text1(0).Text & "' AND "
   'SQL = SQL & horassql
End If
If Text1(1).Text <> "" Then
   horassql = horassql & " to_char(V.ar04fecinicio,'HH24:MI')  <= '" & Text1(1).Text & "' AND "
   'SQL = SQL & horassql
End If

' HHCC
If Text1(2).Text <> "" And Text1(3).Text <> "" Then
    If CLng(Text1(3).Text) < CLng(Text1(2).Text) Then
       Call MsgBox("Hasta debe ser mayor que Desde", vbOKOnly + vbInformation, "Atenci�n")
       Exit Sub
    End If
End If
    If Text1(2).Text <> "" Then
       historiasql = " V.CI22NUMHISTORIA >= " & CLng(Text1(2).Text) & " AND "
       'SQL = SQL & historiasql
    End If
    If Text1(3).Text <> "" Then
       historiasql = historiasql & " V.CI22NUMHISTORIA <= " & CLng(Text1(3).Text) & " AND "
       'SQL = SQL & historiasql
    End If

' TIPOLOGIAS

' Motivos de Solicitud
If List2.SelCount <> 0 Then ' hay alguna tipologia seleccionada
    tiposql = " V.AR00CODTIPOLOGIA IN ("
    For i = 0 To List2.ListCount - 1
        If List2.Selected(i) = True Then
            tiposql = tiposql & "'" & tipologias.Item(List2.ItemData(i)) & "',"
        End If
    Next
    tiposql = Left(tiposql, Len(tiposql) - 1)
    tiposql = tiposql & ") AND "
    'SQL = SQL & tiposql
End If
' Punto de Archivo
PUNTOUBICACION2 = " ( V.AR02CODPTOARCHIVO = '" & PUNTOUBICACION & "'"
PUNTOUBICACION2 = PUNTOUBICACION2 & " OR V.AR02CODPTOARCHIVO IS NULL) AND "
' Procesos
S_IN = ""
If optProceso(0).Value = True Then
  S_IN = "'A'"
End If
If optProceso(1).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'R'"
End If
If optProceso(2).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'E'"
End If
If optProceso(3).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'D'"
End If
If optProceso(4).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'U'"
End If
If optProceso(5).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'C'"
End If
If optProceso(6).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'P'"
End If
If optProceso(7).Value = True Then
  If S_IN <> "" Then
    S_IN = S_IN & ","
  End If
  S_IN = S_IN & "'T'"
End If
If S_IN <> "" Then
  estado = " V.AR06CODESTADO IN ( " & S_IN & " ) AND "
End If

' SELECT Y FROM
selec = "SELECT V.CI22NUMHISTORIA AS ""N�mero H.C."", "
selec = selec & "T.AR00DESTIPOLOGIA AS ""Tipolog�a"", "
selec = selec & "V.AR04FECINICIO AS ""Fecha Proceso"", "
selec = selec & "V.AR04HORAINICIO AS ""Hora Proceso"", "
selec = selec & "E.AR06DESESTADO AS ""Tipo Proceso"", "
selec = selec & "V.AR03CODLINEA AS ""L�nea"", "
selec = selec & "V.AR04NUMHUECO AS ""Pto. Ubicaci�n"", "
selec = selec & "D.AD02DESDPTO AS ""Devuelto al Departamento"", "
selec = selec & "D2.AD02DESDPTO AS ""Prestado al Departamento"", "
selec = selec & "V.SG02DOCTORPRES AS ""Prestado al Destinatario"", "
selec = selec & "M.AR01DESMOTIVOSOL AS ""Tipo de Pr�stamo"", "
selec = selec & "A2.AR02DESPTOARCHIVO AS ""Archivo Destino"", "
selec = selec & "V.AR04OBSERVACIO AS ""Observaciones"", "
selec = selec & "A.AR02DESPTOARCHIVO AS ""Pto. Archivo"" "
selec = selec & "FROM AR0200 A2, AR0200 A, AR0100 M, AD0200 D2, AD0200 D, AR0600 E, AR0000 T, CI2200 C, AR0401J V "

' WHERE
sql = " WHERE "
sql = sql & historiasql & tiposql & fechassql & horassql & estado & PUNTOUBICACION2
sql = sql & " C.CI22NUMHISTORIA = V.CI22NUMHISTORIA"
sql = sql & " AND D.AD02CODDPTO(+) = V.AD02CODDPTODEV"
sql = sql & " AND D2.AD02CODDPTO(+) = V.AD02CODDPTOPRES"
sql = sql & " AND M.AR01CODMOTIVOSOL(+) = V.AR05CODMOTIVOSOL"
sql = sql & " AND A2.AR02CODPTOARCHIVO(+) = V.AR02CODPTOARDES"
sql = sql & " AND A.AR02CODPTOARCHIVO(+) = V.AR02CODPTOARCHIVO"
sql = sql & " AND T.AR00CODTIPOLOGIA = V.AR00CODTIPOLOGIA"
sql = sql & " AND E.AR06CODESTADO = V.AR06CODESTADO "
If OptNumero.Value = True Then
 sql = sql & "  ORDER BY 1, 3"
Else
 sql = sql & "  ORDER BY 3"
End If
' SQL

sql = selec & sql

    'mostrar resultados consulta
    Screen.MousePointer = vbHourglass
    frmCtaProcesos.sql = sql
    frmCtaProcesos.Show vbModal
    Set frmCtaProcesos = Nothing
End Sub
