VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmEtiqueSecuen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Archivo. Etiquetas Secuenciales"
   ClientHeight    =   1740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4695
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   4695
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtText1 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Index           =   0
      Left            =   3480
      MaxLength       =   2
      TabIndex        =   2
      Text            =   "1"
      Top             =   720
      Width           =   255
   End
   Begin VB.TextBox txtText1 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Index           =   1
      Left            =   4320
      MaxLength       =   1
      TabIndex        =   3
      Text            =   "1"
      Top             =   720
      Width           =   255
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3600
      TabIndex        =   5
      Top             =   1200
      Width           =   975
   End
   Begin VB.TextBox txtFin 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Left            =   3480
      MaxLength       =   6
      TabIndex        =   1
      Top             =   240
      Width           =   1095
   End
   Begin VB.TextBox txtInicio 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Left            =   1200
      MaxLength       =   6
      TabIndex        =   0
      Top             =   240
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBTipo 
      Height          =   285
      HelpContextID   =   30110
      Left            =   1200
      TabIndex        =   11
      Tag             =   "Motivo fin Asistencia|Motivo fin Asistencia"
      Top             =   720
      Width           =   1545
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldDelimiter  =   """"
      FieldSeparator  =   ";"
      stylesets.count =   2
      stylesets(0).Name=   "Activo"
      stylesets(0).Picture=   "AR1040.frx":0000
      stylesets(1).Name=   "Inactivo"
      stylesets(1).BackColor=   255
      stylesets(1).Picture=   "AR1040.frx":001C
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1296
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5556
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "FechaFin"
      Columns(2).Name =   "FechaFin"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   2725
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin vsViewLib.vsPrinter vs 
      Height          =   255
      Left            =   1560
      TabIndex        =   12
      Top             =   1200
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   450
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Tipo"
      Height          =   255
      Index           =   4
      Left            =   600
      TabIndex        =   10
      Top             =   720
      Width           =   495
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Fila"
      Height          =   375
      Index           =   3
      Left            =   3000
      TabIndex        =   9
      Top             =   720
      Width           =   255
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Col."
      Height          =   375
      Index           =   2
      Left            =   3960
      TabIndex        =   8
      Top             =   720
      Width           =   375
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Historia Fin:"
      Height          =   255
      Index           =   1
      Left            =   2520
      TabIndex        =   7
      Top             =   240
      Width           =   855
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Historia Inicio:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "frmEtiqueSecuen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strHistoria As String
Dim intTipo As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim TG(9) As String
Dim BNS As Integer
Dim BBS As Integer
Dim BAL As Integer

Private Sub cmdImprimir_Click()
If Trim(txtInicio) = "" Or Trim(txtFin) = "" _
Or Trim(txtText1(0)) = "" Or Trim(txtText1(1)) = "" _
Or Trim(SSDBTipo.Text) = "" Then
   MsgBox "Introduzca todos los datos", vbExclamation, Me.Caption
Else
    pImprimir
End If
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433

TG(0) = "00110"
TG(1) = "10001"
TG(2) = "01001"
TG(3) = "11000"
TG(4) = "00101"
TG(5) = "10100"
TG(6) = "01100"
TG(7) = "00011"
TG(8) = "10010"
TG(9) = "01010"

BNS = 15
BBS = 45
BAL = 700
'llenamos el combo
SSDBTipo.AddItem "1" & ";" & "Papel"
SSDBTipo.AddItem "2" & ";" & "Radiografia"
End Sub

Private Sub txtFin_KeyPress(KeyAscii As Integer)
If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
End If
End Sub

Private Sub txtInicio_KeyPress(KeyAscii As Integer)
If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
End If
End Sub
Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
End If
End Sub

Private Sub pImprimir()
Dim lngHistoria As Long
Dim strHistoria As String
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim intPX As Integer
Dim intPY As Integer
vs.PaperBin = 1
vs.StartDoc
intPX = intPosX + (intIncX * (Int(txtText1(1).Text) - 1))
intPY = intPosY + (intIncY * (Int(txtText1(0).Text) - 1))
For lngHistoria = Val(txtInicio) To Val(txtFin)

    SQL = "SELECT COUNT(*) FROM AR0400 WHERE AR00CODTIPOLOGIA = ? AND "
    SQL = SQL & " CI22NUMHISTORIA = ? "
    
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = "0" & SSDBTipo.Columns(0).Value
        qry(1) = lngHistoria
    Set rs = qry.OpenResultset()
    If rs(0) <> 0 Then
        strHistoria = Trim(Str(lngHistoria))
        If Len(strHistoria) < 6 Then strHistoria = fstrPonerCeros(strHistoria)
        Call pEtiquetas(strHistoria & "0" & SSDBTipo.Columns(0).Value, intPX, intPY)
        Call pNombreFecha(strHistoria, SSDBTipo.Columns(0).Value, intPX, intPY)
        intPX = intPX + intIncX
        If intPX > 10000 Then
            intPX = intPosX
            intPY = intPY + intIncY
        End If
        If intPY > 16000 Then
            vs.EndDoc
            vs.PrintDoc
            vs.StartDoc
            intPY = intPosY
        End If
    End If
    rs.Close
    qry.Close
Next lngHistoria
vs.EndDoc
vs.PrintDoc
End Sub

Private Sub pEtiquetas(Num As String, ByVal POSX As Integer, POSY As Integer)
Dim i As Integer
Dim resto As Integer
Dim j As String
Dim str1 As String
Dim str2 As String
''Dim POSX As String
''Dim POSY As String


resto = Len(Num) - Int(Len(Num) / 2) * 2
If resto > 0 Then Num = "0" & Num
str1 = "00"
str2 = "00"

For i = 0 To (Len(Num) / 2) - 1
    j = Mid(Num, i * 2 + 1, 1)
    str1 = str1 & TG(Int(j))
    j = Mid(Num, i * 2 + 2, 1)
    str2 = str2 & TG(Int(j))
Next i
str1 = str1 & "10"
str2 = str2 & "00"
For i = 0 To Len(str1) - 1
    If Mid(str1, i + 1, 1) = 0 Then
       Call vs.DrawRectangle(POSX, POSY, POSX + BNS, POSY + BAL)
       POSX = POSX + BNS
    Else
        Call vs.DrawRectangle(POSX, POSY, POSX + BBS, POSY + BAL)
        POSX = POSX + BBS
    End If
    If Mid(str2, i + 1, 1) = 0 Then
       POSX = POSX + BNS
    Else
       POSX = POSX + BBS
    End If
Next i
vs.CurrentX = POSX + 80
vs.CurrentY = POSY + 200
End Sub
Private Sub pNombreFecha(strHistoria As String, inttip As Integer, px As Integer, py As Integer)
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim SQL As String
Dim strSegApel As String
vs.Font = "Courier"
vs.FontSize = 25
vs.FontBold = True
vs.Text = Format(strHistoria, "##,##000000")
'vs.FontBold = False
vs.Font = "Courier New"
vs.FontSize = 10
vs.CurrentX = vs.CurrentX - 100
If inttip = 1 Then vs.Text = " H"
If inttip = 2 Then vs.Text = " R"
If inttip = 3 Then vs.Text = " G"
If inttip = 4 Then vs.Text = " P"
 
vs.FontSize = 10
SQL = "SELECT CI22PRIAPEL,CI22SEGAPEL,CI22NOMBRE, CI22FECNACIM FROM "
SQL = SQL & "CI2200 WHERE CI22NUMHISTORIA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Trim(strHistoria)
Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        vs.CurrentX = px
        vs.CurrentY = py + 730
        If IsNull(rs!CI22SEGAPEL) Then strSegApel = "" Else strSegApel = rs!CI22SEGAPEL
        If Len(rs!CI22PRIAPEL & strSegApel & rs!CI22NOMBRE) > 24 Then
            If Len(rs!CI22PRIAPEL & strSegApel) > 24 Then
                vs.Text = rs!CI22PRIAPEL
'                vs.Text = Chr$(13)
                vs.CurrentY = vs.CurrentY + 190
                vs.CurrentX = px
                If Len(rs!CI22NOMBRE & strSegApel) > 24 Then
                    vs.Text = Left(strSegApel & "," & rs!CI22NOMBRE, 26)
                Else
                    vs.Text = strSegApel & "," & rs!CI22NOMBRE
                End If
                If Not IsNull(rs!CI22FECNACIM) Then
'                    vs.Text = Chr$(13)
                    vs.CurrentY = vs.CurrentY + 190
                    vs.CurrentX = px
                    vs.Text = " " & rs!CI22FECNACIM
                End If
            Else
               vs.Text = rs!CI22PRIAPEL & "," & strSegApel & ","
'               vs.Text = Chr$(13)
                vs.CurrentY = vs.CurrentY + 190
               vs.CurrentX = px
               vs.Text = rs!CI22NOMBRE
               If Not IsNull(rs!CI22FECNACIM) Then
'                    vs.Text = Chr$(13)
                    vs.CurrentY = vs.CurrentY + 190
                    vs.CurrentX = px
                    vs.Text = " " & rs!CI22FECNACIM
                End If
            End If
        Else
            vs.Text = rs!CI22PRIAPEL & "," & strSegApel & "," & rs!CI22NOMBRE
'            vs.Text = Chr$(13)
            vs.CurrentY = vs.CurrentY + 190
            vs.CurrentX = px
            If Not IsNull(rs!CI22FECNACIM) Then
                vs.Text = " " & rs!CI22FECNACIM
            End If
        End If
    End If
rs.Close
qry.Close

End Sub

Private Function fstrPonerCeros(strH As String) As String
Dim l As Integer
Dim i As Integer
fstrPonerCeros = strH
l = 6 - Len(strH)
For i = 0 To l - 1
    fstrPonerCeros = "0" & fstrPonerCeros
Next i
End Function
