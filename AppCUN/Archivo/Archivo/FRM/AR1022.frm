VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSolicitudInvestiga 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Solicitud de Pr�stamo para Investigaci�n"
   ClientHeight    =   7335
   ClientLeft      =   1215
   ClientTop       =   2025
   ClientWidth     =   10905
   HelpContextID   =   18
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7335
   ScaleWidth      =   10905
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "&Solicitar"
      Height          =   375
      Index           =   2
      Left            =   8880
      TabIndex        =   15
      Top             =   3720
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Sa&lir"
      Height          =   375
      Index           =   3
      Left            =   8880
      TabIndex        =   14
      Top             =   4260
      Width           =   1575
   End
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   18
      Left            =   8880
      TabIndex        =   13
      Top             =   6480
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "<<&Eliminar"
      Height          =   375
      Index           =   1
      Left            =   360
      TabIndex        =   4
      Top             =   3960
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "A�a&dir >>"
      Height          =   375
      Index           =   0
      Left            =   360
      TabIndex        =   3
      Top             =   3480
      Width           =   1215
   End
   Begin VB.ComboBox cboDpto 
      BackColor       =   &H00FFFF00&
      Height          =   315
      Left            =   4620
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   360
      Width           =   4335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selecci�n de Historia Cl�nica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6375
      Left            =   120
      TabIndex        =   8
      Top             =   780
      Width           =   10575
      Begin VB.Frame Frame2 
         Caption         =   "Solicitud de Pr�stamo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3915
         Left            =   1620
         TabIndex        =   12
         Top             =   2340
         Width           =   3135
         Begin MSFlexGridLib.MSFlexGrid FGrid1 
            Height          =   3435
            Left            =   120
            TabIndex        =   7
            Top             =   300
            Width           =   2895
            _ExtentX        =   5106
            _ExtentY        =   6059
            _Version        =   65541
            Rows            =   1
            Cols            =   1
            FixedRows       =   0
            FixedCols       =   0
            BackColor       =   -2147483633
            BackColorBkg    =   -2147483637
            GridColor       =   0
            AllowBigSelection=   0   'False
            ScrollBars      =   2
            SelectionMode   =   1
         End
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Index           =   0
         Left            =   180
         MaxLength       =   7
         TabIndex        =   2
         Top             =   600
         Width           =   1095
      End
      Begin MSFlexGridLib.MSFlexGrid msgTipologias 
         Height          =   1035
         Left            =   180
         TabIndex        =   5
         Top             =   1200
         Width           =   10155
         _ExtentX        =   17912
         _ExtentY        =   1826
         _Version        =   65541
         FixedCols       =   0
         BackColorBkg    =   -2147483633
         FocusRect       =   0
         ScrollBars      =   2
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero H.C."
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   10
         Top             =   360
         Width           =   915
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipolog�as"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   9
         Top             =   960
         Width           =   750
      End
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H8000000F&
      Height          =   285
      Index           =   1
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   360
      Width           =   4335
   End
   Begin VB.Label lblDpto 
      AutoSize        =   -1  'True
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4620
      TabIndex        =   11
      Top             =   60
      Width           =   1200
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Destinatario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "frmSolicitudInvestiga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public u                    As String
Dim usuario                 As String
Dim rstipologia             As rdoResultset
Dim rsusuario               As rdoResultset
Dim rsdepartamento          As rdoResultset
Dim rshctipsol              As rdoResultset
Dim rshctipmov              As rdoResultset
Dim rshcsel                 As rdoResultset
Dim QyConsulta              As rdoQuery
Dim QyInsert                As rdoQuery
Dim colecHC                 As New Collection ' Historias clinicas !=
Dim colecHCBD               As New Collection ' Historias recuperadas de BD
Dim numhcsel                As Integer        ' numero de HC-tipologias
Dim colecHC2                As New Collection ' Hist., tantas como numhcsel
Dim coleccodmen             As New Collection
Dim coleccmen               As New Collection
Dim mensa                   As String
Dim berror                  As Boolean
Dim blnSolicitada           As Boolean
Dim blnPsiquiatria          As Boolean
Dim colDpto                 As New Collection 'contiene los codigos de departamento
Const correcto = "C"   'INDICA QUE LA TIPOLOG�A ES CORRECTA
Const CAZUL = &HC00000
Const CNEGRO = &H80000008
Const CROJO = &HFF&

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 18)
End Sub

Private Sub Command1_Click(Index As Integer)
Dim sql As String
Dim sqlfecha As String
Dim rsfecha As rdoResultset
Dim r As rdoResultset
Dim faltandatos As Integer
Dim existe, anadirhist, anadircolec As Boolean
Dim i, j, k As Integer
Dim numhctbl  As Integer
Dim resp As Integer
Dim men_aceptar As String
mensa = ""
Select Case Index
      
Case 0      ' Boton A�adir
  
    If cboDpto.ListIndex = -1 Then
        mensa = mensa & Chr(13) & "El campo Departamento es Obligatorio"
        faltandatos = True
    End If
    If Text1(0).Text = "" Then
        mensa = mensa & Chr(13) & "El campo Historia es Obligatorio"
        faltandatos = True
    End If
    If msgTipologias.ColSel <> 5 Then
        mensa = mensa & Chr(13) & "El campo Tipolog�a es Obligatorio"
        faltandatos = True
    End If
    If faltandatos = True Then
        Call MsgBox(mensa, vbInformation + vbOKOnly, "Aviso")
        Exit Sub
    End If
    If msgTipologias.Rows > 1 Then
      If msgTipologias.TextMatrix(msgTipologias.row, 0) <> correcto And msgTipologias.ColSel = 5 Then
        Call MsgBox("La Tipolog�a " & msgTipologias.TextMatrix(msgTipologias.row, 2) & " no puede ser solicitada", vbOKOnly, "A�adir")
        Exit Sub
      End If
    End If
     
    'comprobar si la hc-tipol ya existe
    For i = 2 To FGrid1.Rows - 1
        FGrid1.row = i
        FGrid1.col = 0
        If FGrid1.Text = Text1(0).Text Then
            FGrid1.col = 2
                If FGrid1.Text = msgTipologias.TextMatrix(msgTipologias.row, 1) Then
                    MsgBox "La Historia Cl�nica-Tipolog�a ya ha sido seleccionada", vbOKOnly + vbInformation, "Aviso"
                    Exit Sub
                End If
        End If
    Next i
         
         
    anadirhist = True
    anadircolec = True
    FGrid1.col = 0
    For j = 1 To FGrid1.Rows - 1
        FGrid1.row = j
        If FGrid1.Text = Text1(0).Text Then
            anadirhist = False
            anadircolec = False
            Exit For
        End If
    Next
    
    If anadirhist = True Then
        anadircolec = True
        For i = 1 To colecHCBD.Count
             If Text1(0).Text = colecHCBD.Item(i) Then
                anadircolec = False
                Exit For
             End If
        Next
    End If
     
    FGrid1.col = 3
    FGrid1.AddItem Text1(0).Text & vbTab & msgTipologias.TextMatrix(msgTipologias.row, 2) & vbTab & msgTipologias.TextMatrix(msgTipologias.row, 1) & vbTab & Text1(0).Text & msgTipologias.TextMatrix(msgTipologias.row, 1)
    colecHC2.Add (Text1(0).Text)
            
    If anadircolec = True Then
      colecHC.Add (Text1(0).Text)
      numhcsel = colecHC.Count
      anadircolec = False
    End If
        

    
        
   

Case 1      ' Eliminar
        
    If FGrid1.Rows <= 2 Then
        Exit Sub
    End If
     If FGrid1.row = 1 Then
        Exit Sub
    End If
    For i = 1 To colecHC2.Count
        FGrid1.col = 0
        If FGrid1.Text = colecHC2.Item(i) Then
            numhctbl = numhctbl + 1
        End If
               
        If numhctbl > 1 Then
        ' Hay m�s de una tipolog�a de esta HC por lo que no
        ' puede quitarse de la colecci�n colecHC que es la que contiene
        ' las DIFERENTES HC , pero hay que quitar de colecHC2 ya que
        ' en esta hay tantas iguales (HC) como HC_tipo
            colecHC2.Remove (i)
            FGrid1.col = 3
            j = 1
            Do While j <= coleccodmen.Count
               If FGrid1.Text = coleccodmen.Item(j) Then
                   coleccodmen.Remove (j)
                   coleccmen.Remove (j)
                   Exit Do
               End If
               j = j + 1
            Loop
            FGrid1.col = 0
            FGrid1.RemoveItem (FGrid1.row)
            FGrid1.col = 3
            Exit Sub
        End If
    Next i
        
        
    If numhctbl = 1 Then
        FGrid1.col = 0                'Puede que haya sido
        For i = 1 To colecHCBD.Count  'recuperada de BD por lo
                                      ' que lo comprobamos
                                      
            If FGrid1.Text = colecHCBD.Item(i) Then
            ' Ha sido recuperado de BD, El n�mero de HC
            ' NO puede eliminarse de la colecci�n
            ' Pero al quitar la HC-Tipo de la selecci�n
            ' se quita el mensaje y tambi�n de colecHC2
                 k = 1
                 Do While k <= colecHC2.Count
                    If FGrid1.Text = colecHC2.Item(k) Then
                       colecHC2.Remove (k)
                    End If
                    k = k + 1
                 Loop
                 
                 FGrid1.col = 3
                 j = 1
                 Do While j <= coleccodmen.Count
                    If FGrid1.Text = coleccodmen.Item(j) Then
                        coleccodmen.Remove (j)
                        coleccmen.Remove (j)
                        Exit Do
                    End If
                    j = j + 1
                 Loop
                 FGrid1.col = 0
                 FGrid1.RemoveItem (FGrid1.row)
                 FGrid1.col = 3
                 Exit Sub
                  
            End If
        Next
    End If
    
 ' No Existe otro registro HC adem�s del seleccionado
 ' por lo que  puede ser eliminado
        
    FGrid1.col = 0
    i = 1
    Do While i <= colecHC.Count
        If FGrid1.Text = colecHC.Item(i) Then
            numhcsel = numhcsel - 1
            colecHC.Remove (i)
            Exit Do
        End If
       i = i + 1
    Loop
    i = 1
    Do While i <= colecHC2.Count
        If FGrid1.Text = colecHC2.Item(i) Then
            colecHC2.Remove (i)
            Exit Do
        End If
       i = i + 1
    Loop

   ' Tambi�n se elimina el mensaje
    FGrid1.col = 3
    j = 1
    Do While j <= coleccodmen.Count
       If FGrid1.Text = coleccodmen.Item(j) Then
           coleccodmen.Remove (j)
           coleccmen.Remove (j)
           Exit Do
       End If
       j = j + 1
    Loop

    FGrid1.col = 0
    FGrid1.RemoveItem (FGrid1.row)
    

Case 2      ' Solicitar

    If cboDpto.ListIndex = -1 Then
        MsgBox "Debe seleccionar el Departamento Solicitante", vbOKOnly + vbInformation, "Aviso"
        Exit Sub
    End If
    
    If FGrid1.Rows < 3 Then
        mensa = " Debe Seleccionar un Historia Cl�nica-Tipolog�a"
        Call MsgBox(mensa, vbOKOnly + vbInformation, "Aviso")
        Exit Sub
    Else
        men_aceptar = ""
        
        For i = 2 To FGrid1.Rows - 1
              FGrid1.row = i
              FGrid1.col = 0
              men_aceptar = men_aceptar & Chr(13) & FGrid1.Text & " - "
              FGrid1.col = 1
              men_aceptar = men_aceptar & FGrid1.Text
        Next
        If FGrid1.Rows < 4 Then
            men_aceptar = "�Esta seguro que desea Solicitar la Historia Clinica Tipologia " & men_aceptar
        Else
            men_aceptar = "�Esta seguro que desea Solicitar las Historias Clinicas Tipologia " & men_aceptar
        End If
        resp = MsgBox(men_aceptar, vbYesNoCancel + vbInformation, "Aviso")
        If resp = vbNo Then
           Exit Sub
        ElseIf resp = vbYes Then
        
           sqlfecha = " Select to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS') from dual"
           Set rsfecha = objApp.rdoConnect.OpenResultset(sqlfecha, rdOpenKeyset, rdConcurValues)
    
           FGrid1.row = 2
           For i = FGrid1.row To FGrid1.Rows - 1
              'obtenemos el cod pto archivo de la hh-tipologia actual
              sql = "SELECT AR02CODPTOARCHIVO FROM AR0400 "
              sql = sql & "WHERE CI22NUMHISTORIA = ? "
              sql = sql & "AND AR00CODTIPOLOGIA = ? "
              sql = sql & "AND AR04FECFIN IS NULL"
              Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
              FGrid1.col = 0
              QyConsulta(0) = FGrid1.Text
              FGrid1.col = 2
              QyConsulta(1) = FGrid1.Text
              Set r = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
               'insertar solicitud
               sql = "INSERT INTO AR0500 "
               sql = sql & "(AR05FECSOLICITUD, "
               sql = sql & "CI22NUMHISTORIA, "
               sql = sql & "AR00CODTIPOLOGIA, "
               sql = sql & "AD02CODDPTO, "
               sql = sql & "AR01CODMOTIVOSOL, "
               sql = sql & "SG02COD, AR02CODPTOARCHIVO) "
               sql = sql & "VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), "
               sql = sql & "?, ?, ?, 'I', ?, ?)"
               Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
               
               QyInsert(0) = CStr(rsfecha(0))
               FGrid1.col = 0
               QyInsert(1) = FGrid1.Text
               FGrid1.col = 2
               QyInsert(2) = FGrid1.Text
               QyInsert(3) = colDpto(cboDpto.ListIndex + 1)
               QyInsert(4) = usuario
               QyInsert(5) = r(0)
               r.Close
               QyInsert.Execute
               QyInsert.Close
      
               If FGrid1.row < FGrid1.Rows - 1 Then
                   FGrid1.row = FGrid1.row + 1
               End If
           Next
           rsfecha.Close
           blnSolicitada = True
           Unload Me
           
        End If
    End If
Case 3      ' Cancelar
    ' blnSolicitada = False
    Unload Me
End Select

End Sub

Private Sub limpiar()
msgTipologias.Rows = 1
Call msgTipologias_SelChange
End Sub

Private Sub FGrid1_Click()
FGrid1.FocusRect = flexFocusNone
FGrid1.rowsel = FGrid1.row
End Sub

Private Sub Form_Activate()

FGrid1.Rows = 2
FGrid1.FixedRows = 1
FGrid1.RowHeight(1) = 0

FGrid1.row = 0
FGrid1.cols = 4
FGrid1.col = 0
FGrid1.Text = "Historia "

FGrid1.col = 1
FGrid1.Text = "Tipolog�a "

FGrid1.col = 2
FGrid1.ColWidth(3) = 0
FGrid1.Text = "CODTIPOLOGIA"

FGrid1.col = 3
FGrid1.ColWidth(3) = 0
FGrid1.Text = "HC_TIPOLOGIA"

FGrid1.ColWidth(1) = 2000

If berror Then
          Call MsgBox(mensa, vbOKOnly + vbInformation, "Aviso")
    Unload Me
End If

End Sub

Private Sub Form_Load()
Dim i As Integer
Dim sql As String
blnSolicitada = False
berror = False
usuario = objSecurity.strUser

   'cargar dptos a los que pertenece el usuario conectado
   sql = " SELECT  D.AD02CODDPTO, D.AD02DESDPTO "
   sql = sql & " From AD0300 U, AD0200 D "
   sql = sql & " Where SG02COD = ?"
   sql = sql & " AND AD03FECFIN IS NULL "
   sql = sql & " AND U.AD02CODDPTO = D.AD02CODDPTO"
   Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
   QyConsulta(0) = usuario
   Set rsdepartamento = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
   Do While Not rsdepartamento.EOF
     colDpto.Add Trim(rsdepartamento("AD02CODDPTO"))
     cboDpto.AddItem Trim(rsdepartamento("AD02DESDPTO"))
     If rsdepartamento("AD02CODDPTO") = DPTO_PSIQUIATRIA Then blnPsiquiatria = True
     rsdepartamento.MoveNext
   Loop
   rsdepartamento.Close
    
   '2.-
        ' Obtengo los datos del usuario conectado
        
    sql = " SELECT SG02APE1, SG02APE2, SG02NOM"
    sql = sql & " From sg0200 "
    sql = sql & " Where SG02COD = ?"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = usuario
    Set rsusuario = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    Text1(1).Text = rsusuario(0) & " " & rsusuario(1) & ", " & rsusuario(2)
    rsusuario.Close
    
  Call Construir_Grid
  Call compro_inicial
 
End Sub



Private Sub compro_inicial()
' Comprobaci�n de Posibilidad de petici�n para investigaci�n
   Dim sql As String
    sql = "SELECT 1"
    sql = sql & " From AR0500 "
    sql = sql & " Where SG02COD = ?" 'usuario
    sql = sql & " AND AR01CODMOTIVOSOL    = 'I' "
    sql = sql & " AND AR05FECFINSOLICITUD     IS NULL"
    sql = sql & " AND ( AR05FECSOLICITUD <  (select TRUNC(to_date(sysdate)) from dual)"
    sql = sql & " OR AR05FECSOLICITUD >= (select TRUNC(to_date(sysdate + 1)) from dual) )"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = usuario
    Set rshctipsol = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
   
    If rshctipsol.RowCount > 0 Then
      
        mensa = "�No puede Solicitar HHCC para Investigaci�n mientras no haya Devuelto su Pedido anterior completo!"
        berror = True
        rshctipsol.Close
        Exit Sub
    Else
        sql = " SELECT  P.CI22NUMHISTORIA, P.AR00CODTIPOLOGIA "
        sql = sql & " FROM   AR0400 H, AR0500 P"
        sql = sql & " Where P.SG02COD = ?"
        sql = sql & " AND P.AR01CODMOTIVOSOL  = 'I'"
        sql = sql & " AND H.CI22NUMHISTORIA   = P.CI22NUMHISTORIA"
        sql = sql & " AND H.AR00CODTIPOLOGIA  = P.AR00CODTIPOLOGIA"
        sql = sql & " AND     H.AR04FECINICIO = P.AR05FECFINSOLICITUD"
        sql = sql & " AND     H.AR06CODESTADO = 'P'"
        sql = sql & " AND H.AR04FECFIN IS NULL"
        sql = sql & " AND NOT EXISTS ("
        sql = sql & " SELECT 1 FROM AR0700 M"
        sql = sql & " Where M.CI22NUMHISTORIA = H.CI22NUMHISTORIA"
        sql = sql & " AND M.AR00CODTIPOLOGIA      = H.AR00CODTIPOLOGIA"
        sql = sql & " AND M.AR04FECINICIO         = H.AR04FECINICIO"
        sql = sql & " AND M.AR07FECFINMVTO IS NULL)"
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = usuario
        Set rshctipmov = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        
        If rshctipmov.RowCount > 0 Then ' ha encontrado, debe devolver las historias
            mensa = "�Para poder realizar la Solicitud antes deber� Devolver las siguientes Historias Cl�nicas:"
            rshctipmov.MoveFirst
            While Not rshctipmov.EOF
                sql = "select ar00destipologia from ar0000"
                sql = sql & " Where ar00codtipologia = ?"
                Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
                QyConsulta(0) = rshctipmov(1)
                Set rstipologia = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                mensa = mensa & Chr(13) & rshctipmov(0) & " " & rstipologia(0)
                rshctipmov.MoveNext
            Wend
            rstipologia.Close
            rshctipmov.Close
            berror = True
            Exit Sub
        End If
    End If ' de rshctipsol.RowCount > 0 Then
rshctipsol.Close

' Miramos si el usuario ha seleccionado HC el mismo dia y contamos cuantas

    sql = "SELECT DISTINCT (CI22NUMHISTORIA) "
    sql = sql & " From AR0500"
    sql = sql & " WHERE SG02COD = ?"
    sql = sql & " AND AR01CODMOTIVOSOL = 'I'"
    sql = sql & " AND AR05FECSOLICITUD >=(select TRUNC(to_date(sysdate)) from dual)"
    sql = sql & " AND AR05FECSOLICITUD < (select TRUNC(to_date(sysdate + 1)) from dual)"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = usuario
    Set rshcsel = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rshcsel.RowCount > 0 Then
       Set colecHC = New Collection
       Set colecHCBD = New Collection
       numhcsel = rshcsel.RowCount
       rshcsel.MoveFirst
       While Not rshcsel.EOF
              colecHC.Add (rshcsel(0))
              colecHCBD.Add (rshcsel(0))
              rshcsel.MoveNext
       Wend
    Else
       numhcsel = 0
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim vntResp As Variant
If berror = False And blnSolicitada = False And FGrid1.Rows > 2 Then
vntResp = MsgBox("�Desea salir sin solicitar ninguna Historia Cl�nica?", vbYesNo + vbQuestion, "Salir")
    If vntResp = vbYes Then
        Cancel = 0
    Else
        Cancel = 1
    End If
Else
Cancel = 0
End If
End Sub

Private Sub msgTipologias_Click()
Dim lngHc As Long
With msgTipologias
  .col = 0
  .rowsel = .row
  .ColSel = .cols - 1
End With
If IsNumeric(Text1(0).Text) Then
    lngHc = Text1(0).Text
End If
End Sub

Private Sub msgTipologias_SelChange()
With msgTipologias
  If .Rows = 1 Then
    .BackColorSel = &H8000000F
    .ForeColorSel = &H80000012
  Else
    .BackColorSel = &H8000000D
    .ForeColorSel = &H8000000E
  End If
End With
End Sub

Private Sub Text1_Change(Index As Integer)
Call limpiar
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim i As Integer
Select Case Index
Case 0
    If (KeyAscii < 48 Or KeyAscii > 57) Then
            If KeyAscii <> 8 Then
                KeyAscii = 13
            End If
    End If
End Select
End Sub
Private Sub Text1_LostFocus(Index As Integer)
Dim i As Integer
Dim existe As Boolean
Select Case Index
Case 0
    If numhcsel >= 15 Then
        For i = 1 To colecHC.Count
            If Text1(0).Text = colecHC.Item(i) Then
                existe = True
            End If
        Next
        ' Si no ha sido seleccionada esta HC anteriormente ...
        If existe = False Then
            mensa = " �No pueden ser Solicitadas para Investigaci�n "
            mensa = mensa & "m�s de 15 Historias Cl�nicas !."
            Call MsgBox(mensa, vbOKOnly + vbInformation, "Aviso")
            Text1(0).Text = ""
        End If
    End If
    If Text1(0).Text <> "" Then
        Call Cargar_Grid
    End If
End Select
End Sub
Private Sub Cargar_Estados(strCodTip As String, strDesTip As String)
Dim sql  As String
Dim rshctipologia As rdoResultset
Dim rs As rdoResultset
Dim estado As String
Dim tiposel As String
Dim tiposeldes As String
Dim mensa As String

tiposel = strCodTip
tiposeldes = strDesTip


    sql = "SELECT AR06CODESTADO,TO_CHAR(AR04FECINICIO,'DD/MM/YYYY HH24:MI:SS'), AD02CODDPTO, AR02CODPTOARCHIVO "
    sql = sql & " From AR0400 "
    sql = sql & " Where CI22NUMHISTORIA = ? "
    sql = sql & " AND AR00CODTIPOLOGIA = ? "
    sql = sql & " AND AR04FECFIN IS NULL "
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = Text1(0).Text
    QyConsulta(1) = tiposel
    Set rshctipologia = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
    If rshctipologia.RowCount Then
    
       ' La Historia esta en ALTA
       
       If rshctipologia("AR06CODESTADO") = "A" Then
        sql = " SELECT D.AD02DESDPTO, TO_CHAR(M.AR07FECMVTO,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
        sql = sql & " From AD0200 D, SG0200 U, AR0700 M "
        sql = sql & " Where M.CI22NUMHISTORIA = ? "
        sql = sql & " AND M.AR00CODTIPOLOGIA = ? " 'to_date(?,'dd/mm/yy hh24:mi:ss')
        sql = sql & " AND to_char(M.AR04FECINICIO,'dd/mm/yyyy hh24:mi:ss') = ?"
        sql = sql & " AND M.AR07FECFINMVTO IS NULL "
        sql = sql & " AND D.AD02CODDPTO = M.AD02CODDPTO"
        sql = sql & " AND U.SG02COD = M.SG02COD "
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = Text1(0).Text
        QyConsulta(1) = tiposel
        QyConsulta(2) = rshctipologia(1)
        Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rs.RowCount >= 1 Then
          msgTipologias.TextMatrix(msgTipologias.row, 3) = "En el Departamento " & rs(0)
          msgTipologias.TextMatrix(msgTipologias.row, 4) = rs(1)
          msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(2) & " " & rs(3) & ", " & rs(4)
        Else
          msgTipologias.TextMatrix(msgTipologias.row, 3) = "En Alta, todav�a no ha llegado al Archivo"
        End If
        rs.Close
             
'         La Historia esta DEVUELTA
       
      ElseIf rshctipologia("AR06CODESTADO") = "D" Then
        sql = " SELECT D.AD02DESDPTO, TO_CHAR(M.AR07FECMVTO,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
        sql = sql & " From AD0200 D, SG0200 U, AR0700 M "
        sql = sql & " Where M.CI22NUMHISTORIA = ? "
        sql = sql & " AND M.AR00CODTIPOLOGIA = ? " 'to_date(?,'dd/mm/yy hh24:mi:ss')
        sql = sql & " AND to_char(M.AR04FECINICIO,'dd/mm/yyyy hh24:mi:ss') = ?"
        sql = sql & " AND M.AR07FECFINMVTO IS NULL "
        sql = sql & " AND D.AD02CODDPTO = M.AD02CODDPTO"
        sql = sql & " AND U.SG02COD = M.SG02COD "
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = Text1(0).Text
        QyConsulta(1) = tiposel
        QyConsulta(2) = rshctipologia(1)
        Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rs.RowCount >= 1 Then
          msgTipologias.TextMatrix(msgTipologias.row, 3) = "En el Departamento " & rs(0)
          msgTipologias.TextMatrix(msgTipologias.row, 4) = rs(1)
          msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(2) & " " & rs(3) & ", " & rs(4)
          rs.Close
        Else
          rs.Close
          sql = " SELECT AD02DESDPTO "
          sql = sql & " From AD0200"
          sql = sql & " Where AD02CODDPTO = ? "
          Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
          QyConsulta(0) = rshctipologia("AD02CODDPTO")
          Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          msgTipologias.TextMatrix(msgTipologias.row, 3) = "Devuelta al Departamento " & rs(0) & "!"
          msgTipologias.TextMatrix(msgTipologias.row, 4) = rshctipologia(1)
          rs.Close
        End If
       
       ' La Historia esta TRASLADADA
       
       ElseIf rshctipologia("AR06CODESTADO") = "T" Then
       
            sql = " SELECT AR02DESPTOARCHIVO "
            sql = sql & " From AR0200 "
            sql = sql & " Where AR02CODPTOARCHIVO = ? "
            Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
                                          
            QyConsulta(0) = rshctipologia("AR02CODPTOARCHIVO")
            Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            msgTipologias.TextMatrix(msgTipologias.row, 3) = "Trasladada al Archivo " & rs(0)
            msgTipologias.TextMatrix(msgTipologias.row, 4) = rshctipologia(1)
            rs.Close
            
       ' La Historia esta ENTREGADA
       
       ElseIf rshctipologia("AR06CODESTADO") = "E" Then
            msgTipologias.TextMatrix(msgTipologias.row, 3) = "Entregada al Paciente"
            msgTipologias.TextMatrix(msgTipologias.row, 4) = rshctipologia(1)
       
       ' La Historia esta PRESTADA
       ElseIf rshctipologia("AR06CODESTADO") = "P" Then
       
            sql = " SELECT D.AD02DESDPTO, TO_CHAR(M.AR07FECMVTO,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
            sql = sql & " From AD0200 D, AR0700 M, SG0200 U"
            sql = sql & " Where M.CI22NUMHISTORIA = ? "
            sql = sql & " AND M.AR00CODTIPOLOGIA = ? " 'to_date(?,'dd/mm/yy hh24:mi:ss')
            sql = sql & " AND to_char(M.AR04FECINICIO,'dd/mm/yyyy hh24:mi:ss') = ?"
            sql = sql & " AND D.AD02CODDPTO = M.AD02CODDPTO"
            sql = sql & " AND AR07FECFINMVTO IS NULL "
            sql = sql & " AND U.SG02COD = M.SG02COD "
            Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
            QyConsulta(0) = Text1(0).Text
            QyConsulta(1) = tiposel
            QyConsulta(2) = rshctipologia(1)
            Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            
            If rs.RowCount < 1 Then
            
               sql = " SELECT D.AD02DESDPTO, TO_CHAR(P.AR05FECSOLICITUD,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
               sql = sql & " FROM AD0200 D, AR0500 P , SG0200 U"
               sql = sql & " WHERE P.CI22NUMHISTORIA = ? "
               sql = sql & " AND P.AR00CODTIPOLOGIA = ? "
               sql = sql & " AND to_char(P.AR05FECFINSOLICITUD,'dd/mm/yyyy hh24:mi:ss') = ? "
               sql = sql & " AND D.AD02CODDPTO = P.AD02CODDPTO"
               sql = sql & " AND U.SG02COD = P.SG02COD "
               Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
               QyConsulta(0) = Text1(0).Text
               QyConsulta(1) = tiposel
               QyConsulta(2) = rshctipologia(1)
               Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
           End If
            msgTipologias.TextMatrix(msgTipologias.row, 3) = "En el Departamento " & rs(0)
            msgTipologias.TextMatrix(msgTipologias.row, 4) = rs(1)
              msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(2) & " " & rs(3) & ", " & rs(4)
           rs.Close
      
      ' Historia ha sido dada de BAJA
       
       ElseIf rshctipologia("AR06CODESTADO") = "B" Then
            msgTipologias.TextMatrix(msgTipologias.row, 3) = "No existe"
   
      ' La Historia esta Solicitada
       
       Else
            sql = " SELECT D.AD02DESDPTO, TO_CHAR(P.AR05FECSOLICITUD,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
            sql = sql & " FROM AD0200 D, AR0500 P, SG0200 U"
            sql = sql & " WHERE P.CI22NUMHISTORIA = ?"
            sql = sql & " AND P.AR00CODTIPOLOGIA = ?"
            sql = sql & " AND P.AR05FECFINSOLICITUD IS NULL "
            sql = sql & " AND D.AD02CODDPTO = P.AD02CODDPTO "
            sql = sql & " AND U.SG02COD = P.SG02COD "
            Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
            QyConsulta(0) = Text1(0).Text
            QyConsulta(1) = tiposel
            Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            If rs.RowCount Then
                mensa = "Solicitada por el Departamento " & rs(0)
                msgTipologias.TextMatrix(msgTipologias.row, 3) = mensa
                msgTipologias.TextMatrix(msgTipologias.row, 4) = rs(1)
                msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(2) & " " & rs(3) & ", " & rs(4)
                rs.Close
            Else
                ' esta todo correcto
                msgTipologias.TextMatrix(msgTipologias.row, 3) = "En el Archivo"
                msgTipologias.TextMatrix(msgTipologias.row, 0) = correcto
            End If
       End If
Else
        msgTipologias.TextMatrix(msgTipologias.row, 3) = "No existe"
End If   ' DEL ROWCOUNT

End Sub

Private Sub Cargar_Grid()
Dim sql As String
Dim i As Integer
Dim strCodTipologia As String
Dim strDesTipologia As String
 ' Cargo el Grid  con las tipologias
        
        
     'Carga de las Tipologias
    
    
    sql = "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    Set rstipologia = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
     If rstipologia.RowCount Then
        rstipologia.MoveFirst
        i = 1
        
        While Not rstipologia.EOF
            With msgTipologias
              strCodTipologia = rstipologia("ar00codtipologia")
              strDesTipologia = rstipologia("ar00destipologia")
              If strCodTipologia = TIPOLOGIA_PSIQUIATRIA Then
                If blnPsiquiatria Then
                    .Redraw = False
                    .Rows = .Rows + 1
                    .row = i
                    .TextMatrix(i, 1) = strCodTipologia
                    .TextMatrix(i, 2) = strDesTipologia
                    Call Cargar_Estados(strCodTipologia, strDesTipologia)
                    i = i + 1
                End If
              Else
                .Redraw = False
                .Rows = .Rows + 1
                .row = i
                .TextMatrix(i, 1) = strCodTipologia
                .TextMatrix(i, 2) = strDesTipologia
                Call Cargar_Estados(strCodTipologia, strDesTipologia)
                i = i + 1
              End If
            End With
            rstipologia.MoveNext
        Wend
        rstipologia.Close
        msgTipologias.col = 0
        With msgTipologias
          For i = 1 To .Rows - 1
          .row = i
            If .TextMatrix(i, 0) <> correcto Then
                .col = 0
                .CellForeColor = CROJO
                .col = 1
                .CellForeColor = CROJO
                .col = 2
                .CellForeColor = CROJO
                .col = 3
                .CellForeColor = CROJO
                .col = 4
                .CellForeColor = CROJO
                .col = 5
                .CellForeColor = CROJO
                .col = 0
            End If
          Next
        End With
    Else
      Call msgTipologias_SelChange
    End If
msgTipologias.Redraw = True
End Sub
Private Sub Construir_Grid()
'Contruir la Grid
    With msgTipologias
    .Clear
    .Redraw = False
    .Rows = 1
    .cols = 6
    .row = 0
    .col = 0
    .ColWidth(0) = 0 'Indicador de correcto o no correcto de la tipolog�a
    .col = 1
    .ColWidth(1) = 0 'Codigo de la tipolog�a
    .col = 2
    .ColWidth(2) = "1000"
    .CellFontBold = True
    .TextMatrix(0, 2) = "Tipolog�a"
    .col = 3
    .ColWidth(3) = "4500"
    .CellFontBold = True
    .TextMatrix(0, 3) = "Situaci�n"
    .col = 4
    .ColWidth(4) = "2000"
    .CellFontBold = True
    .TextMatrix(0, 4) = "Fecha Situaci�n"
    .col = 5
    .ColWidth(5) = "2500"
    .CellFontBold = True
    .TextMatrix(0, 5) = "Responsable"
    .col = 0
    .Redraw = True
    End With

End Sub

