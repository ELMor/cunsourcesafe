VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "MSRDC20.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCitas 
   Caption         =   "ARCHIVO. Pr�stamo de HHCC para Citas"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   11880
   HelpContextID   =   17
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   11880
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlMainSmall"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Guardar"
            Object.Tag             =   ""
            ImageIndex      =   15
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Primero"
            Object.Tag             =   ""
            ImageIndex      =   19
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Anterior"
            Object.Tag             =   ""
            ImageIndex      =   22
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Siguiente"
            Object.Tag             =   ""
            ImageIndex      =   29
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Ultimo"
            Object.Tag             =   ""
            ImageIndex      =   28
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Salir"
            Object.Tag             =   ""
            ImageIndex      =   13
         EndProperty
      EndProperty
      BorderStyle     =   1
      MouseIcon       =   "AR1031.frx":0000
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   330
      Left            =   6360
      Top             =   360
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      _Version        =   393216
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   "oracle73"
      LogMessages     =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdListar 
      Caption         =   "&Listar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   7920
      TabIndex        =   4
      Top             =   840
      Width           =   855
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   5160
      TabIndex        =   2
      Top             =   840
      Width           =   1695
      _Version        =   65537
      _ExtentX        =   2990
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   3300
      TabIndex        =   1
      Top             =   840
      Width           =   1695
      _Version        =   65537
      _ExtentX        =   2990
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
   End
   Begin VB.CommandButton cmdVolcar 
      Caption         =   "&Volcar"
      Height          =   375
      Left            =   6960
      TabIndex        =   3
      Top             =   840
      Width           =   855
   End
   Begin VB.ComboBox cboDepartamento 
      Height          =   315
      Left            =   6600
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1680
      Width           =   3855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Citas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7020
      Left            =   0
      TabIndex        =   8
      Top             =   1200
      Width           =   11865
      Begin VB.Frame Frame2 
         Caption         =   "Manual"
         ForeColor       =   &H80000008&
         Height          =   825
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   2355
         Begin VB.CommandButton cmdTodos 
            Caption         =   "P.Todos"
            Height          =   495
            Left            =   1560
            Picture         =   "AR1031.frx":031A
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   240
            Width           =   735
         End
         Begin VB.CommandButton cmdSeleccionar 
            Caption         =   "Prestar"
            Height          =   495
            Left            =   840
            Picture         =   "AR1031.frx":0464
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   240
            Width           =   615
         End
         Begin VB.CommandButton cmdAnular 
            Caption         =   "Anular"
            Height          =   495
            Left            =   120
            Picture         =   "AR1031.frx":05B2
            Style           =   1  'Graphical
            TabIndex        =   17
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Ordenar por"
         Height          =   825
         Left            =   3240
         TabIndex        =   12
         Top             =   240
         Width           =   2535
         Begin VB.OptionButton optOrdenar 
            Caption         =   "N� HC."
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   15
            Top             =   300
            Width           =   855
         End
         Begin VB.OptionButton optOrdenar 
            Caption         =   "Dpto."
            Height          =   255
            Index           =   1
            Left            =   960
            TabIndex        =   14
            Top             =   300
            Width           =   735
         End
         Begin VB.OptionButton optOrdenar 
            Caption         =   "Ruta"
            Height          =   255
            Index           =   2
            Left            =   1680
            TabIndex        =   13
            Top             =   300
            Width           =   735
         End
      End
      Begin MSFlexGridLib.MSFlexGrid msgCitas 
         Bindings        =   "AR1031.frx":08BC
         Height          =   5415
         Left            =   180
         TabIndex        =   6
         Top             =   1080
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   9551
         _Version        =   327680
         FixedCols       =   0
         BackColorBkg    =   -2147483633
         FocusRect       =   0
         ScrollBars      =   2
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label Label1 
         Caption         =   "Departamento"
         Height          =   255
         Index           =   3
         Left            =   6600
         TabIndex        =   20
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.ComboBox cboPunto 
      BackColor       =   &H00FFFF00&
      Height          =   315
      ItemData        =   "AR1031.frx":08CD
      Left            =   120
      List            =   "AR1031.frx":08CF
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   840
      Width           =   3135
   End
   Begin ComctlLib.ImageList imlMainSmall 
      Left            =   3360
      Top             =   180
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   29
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":08D1
            Key             =   "main"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":0BEB
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":0F05
            Key             =   "function"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":121F
            Key             =   "window"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":1539
            Key             =   "report"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":1853
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":1B6D
            Key             =   "process"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":1E87
            Key             =   "new"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":21A1
            Key             =   "big"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":24BB
            Key             =   "small"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":27D5
            Key             =   "list"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":2AEF
            Key             =   "details"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":2E09
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":3123
            Key             =   "refresh"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":343D
            Key             =   "save"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":3757
            Key             =   "print"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":3A71
            Key             =   "CheckOff"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":3D8B
            Key             =   "CheckOn"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":40A5
            Key             =   "first"
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":43BF
            Key             =   "uno"
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":46D9
            Key             =   "dos"
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":4737
            Key             =   "previous"
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":4A51
            Key             =   "undo"
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":4D6B
            Key             =   "delete"
         EndProperty
         BeginProperty ListImage25 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":5085
            Key             =   "filoff"
         EndProperty
         BeginProperty ListImage26 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":539F
            Key             =   "New"
         EndProperty
         BeginProperty ListImage27 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":56B9
            Key             =   "filon"
         EndProperty
         BeginProperty ListImage28 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":59D3
            Key             =   "last"
         EndProperty
         BeginProperty ListImage29 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1031.frx":5CED
            Key             =   "next"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   3300
      TabIndex        =   10
      Top             =   540
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   5160
      TabIndex        =   9
      Top             =   540
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "Punto de Archivo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   2175
   End
   Begin VB.Menu MnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu optDatos 
         Caption         =   "&Guardar"
         Index           =   0
         Shortcut        =   ^G
      End
      Begin VB.Menu optDatos 
         Caption         =   "-"
         Index           =   1
      End
      Begin VB.Menu optDatos 
         Caption         =   "&Salir"
         Index           =   2
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu optRegistro 
         Caption         =   "&Primero        CTRL+Inicio"
         Enabled         =   0   'False
         Index           =   0
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Anterior        Re Pag."
         Enabled         =   0   'False
         Index           =   1
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Siguiente     Av Pag."
         Enabled         =   0   'False
         Index           =   2
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&�ltimo          CTRL+Fin"
         Enabled         =   0   'False
         Index           =   3
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu optAyuda 
         Caption         =   "&Ayuda"
         Index           =   0
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmCitas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ANULAR = "<Anular>"
Const ANULAR_P = "<AnularP>"
Const SELECCIONAR = "<Prestar>"
Const SELECCIONAR_P = "<PrestarP>"
Const VOLCARDESDE = "<Desde>"
Const VOLCARHASTA = "<Hasta>"
Const VOLCAR = "<Volcar>"
Const LOCALIZANDO = "L"

'Declaracion de variables

Dim rsDepartamentos As rdoResultset
Dim rsPuntos As rdoResultset
Dim rsHistorias As rdoResultset
Dim rsCitas As rdoResultset
Dim rsTipologias As rdoResultset
Dim rsPreferencias As rdoResultset


Dim QyConsulta As rdoQuery
Dim QyBorrar As rdoQuery
Dim QyInsertar As rdoQuery

Dim colTipologias As New Collection
Dim colContador As New Collection

Dim lngColorAnt As Long
Dim intFilaDesde As Integer
Dim intFilaHasta As Integer
Dim NumHC As String
Dim strFechaCita As String
Dim strCodTipologia As String
Dim strCodMedico As String
Dim strCodDepartamento As String
Dim blnVolcarHistorias2 As Boolean
Dim blnLoad As Boolean
Dim intCambios1 As Integer


Private Sub cboDepartamento_Click()
  If blnLoad <> True Then
Screen.MousePointer = vbHourglass
If blnVolcarHistorias2 = False Then
  Call Volcar_Historias
Else
  Call Volcar_Historias2
End If
If optOrdenar(0).Value = True Then
  Call optOrdenar_Click(0)
ElseIf optOrdenar(1).Value = True Then
  Call optOrdenar_Click(1)
End If
Screen.MousePointer = vbNormal
End If
End Sub

Private Sub cboDepartamento_DropDown()
Dim blnCambios As Boolean
Dim vntResp As Variant
Dim inti As Integer
'Preguntar si desea guardar
With msgCitas
    .Redraw = False
    blnCambios = False
    For inti = 1 To .Rows - 1
        If Trim(.TextMatrix(inti, 0)) <> "" And Trim(.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(.TextMatrix(inti, 0)) <> VOLCAR Then
            vntResp = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
            blnCambios = True
            Exit For
        End If
    Next
    If blnCambios = True Then
        If vntResp = vbYes Then
            Call GUARDAR
        ElseIf vntResp = vbCancel Then
            msgCitas.SetFocus
        End If
    End If
    .col = 12
    .Redraw = True
End With
Call Limpiar_Grid_DH
End Sub

Private Sub cboPunto_Click()
If intCambios1 = 1 Then
    Screen.MousePointer = vbHourglass
    If blnVolcarHistorias2 = False Then
        Call Volcar_Historias
    Else
        Call Volcar_Historias2
    End If
    If optOrdenar(0).Value = True Then
        Call optOrdenar_Click(0)
    ElseIf optOrdenar(1).Value = True Then
        Call optOrdenar_Click(1)
    End If
    Screen.MousePointer = vbNormal
End If
intCambios1 = 0
End Sub

Private Sub cboPunto_DropDown()
Dim blnCambios As Boolean
Dim vntResp As Variant
Dim inti As Integer

intCambios1 = 0
'Preguntar si desea guardar
With msgCitas
    .Redraw = False
    blnCambios = False
    For inti = 1 To .Rows - 1
        If Trim(.TextMatrix(inti, 0)) <> "" And Trim(msgCitas.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(.TextMatrix(inti, 0)) <> VOLCAR Then
            vntResp = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
            blnCambios = True
            Exit For
        End If
    Next
    If blnCambios = True Then
        If vntResp = vbYes Then
            Call GUARDAR
            intCambios1 = 1
        ElseIf vntResp = vbCancel Then
            intCambios1 = 0
            msgCitas.SetFocus
        Else
            intCambios1 = 1
        End If
    End If
    .col = 12
    .Redraw = True
End With
End Sub

Private Sub cmdAnular_Click()
With msgCitas
    If .col = 0 Then
        If Trim(.TextMatrix(.row, 0)) <> ANULAR And Trim(.TextMatrix(.row, 0)) <> ANULAR_P Then
            If Trim(.TextMatrix(.row, 0)) = LOCALIZANDO Then
            .TextMatrix(.row, 0) = ANULAR_P
            Else
            .TextMatrix(.row, 0) = ANULAR
            End If
        Else
            If Trim(.TextMatrix(.row, 0)) = ANULAR_P Then
            .TextMatrix(.row, 0) = LOCALIZANDO
            Else
            .TextMatrix(.row, 0) = ""
            End If
        End If
        .col = 0
        .rowsel = .row
        .ColSel = .cols - 1
    End If
End With
End Sub

Private Sub cmdListar_Click()
If cboDepartamento.ListIndex < cboDepartamento.ListCount - 1 Then
    dlgCitas.dlgCitasstrCodDepartamento = Trim(Right(cboDepartamento.List(cboDepartamento.ListIndex), 3))
Else
    dlgCitas.dlgCitasstrCodDepartamento = ""
End If
dlgCitas.dlgCitasstrPuntoArchivo = Trim(Right(cboPunto.List(cboPunto.ListIndex), 2))
dlgCitas.dlgCitasstrFechaDesde = SSDateCombo1(0).Date
dlgCitas.dlgCitasstrFechaHasta = SSDateCombo1(1).Date
dlgCitas.Show vbModal
Set dlgCitas = Nothing
End Sub

Private Sub cmdSeleccionar_Click()
With msgCitas
    If .col = 0 Then
        If Trim(.TextMatrix(.row, 0)) <> SELECCIONAR And Trim(.TextMatrix(.row, 0)) <> SELECCIONAR_P Then
            If Trim(.TextMatrix(.row, 0)) = LOCALIZANDO Then
            .TextMatrix(.row, 0) = SELECCIONAR_P
            Else
            .TextMatrix(.row, 0) = SELECCIONAR
            End If
        Else
            If Trim(.TextMatrix(.row, 0)) = SELECCIONAR_P Then
            .TextMatrix(.row, 0) = LOCALIZANDO
            Else
            .TextMatrix(.row, 0) = ""
            End If
        End If
        .col = 0
        .rowsel = .row
        .ColSel = .cols - 1
    End If
End With
End Sub

Private Sub cmdTodos_Click()
'Declaracion de variables
Dim inti As Integer
Dim blnBlancos As Boolean
With msgCitas
    blnBlancos = False
    For inti = 1 To .Rows - 1
        If Trim(.TextMatrix(inti, 0)) = "" Then
            .TextMatrix(inti, 0) = SELECCIONAR
            blnBlancos = True
        End If
        'Distinguimos si se est� localizando_p
        If Trim(.TextMatrix(inti, 0)) = LOCALIZANDO Then
            .TextMatrix(inti, 0) = SELECCIONAR_P
            blnBlancos = True
        End If
    Next
    If blnBlancos = False Then
        For inti = 1 To .Rows - 1
            If Trim(.TextMatrix(inti, 0)) = SELECCIONAR_P Then
               .TextMatrix(inti, 0) = LOCALIZANDO
            End If
            If Trim(.TextMatrix(inti, 0)) = SELECCIONAR Then
                .TextMatrix(inti, 0) = ""
            End If
        Next
    End If
.col = 12
End With
End Sub

Private Sub cmdVolcar_Click()

'Declaraci�n de variables
Dim strFechasSql As String
Dim SQLHistorias As String
Dim blnCambios As Boolean
Dim vntResp As Variant
Dim inti As Integer
'Preguntar si desea guardar
With msgCitas
    .Redraw = False
    blnCambios = False
    For inti = 1 To .Rows - 1
        If Trim(.TextMatrix(inti, 0)) <> "" And Trim(.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(.TextMatrix(inti, 0)) <> VOLCAR Then
            vntResp = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
            blnCambios = True
            Exit For
        End If
    Next

    If blnCambios = True Then
        If vntResp = vbYes Then
            Call GUARDAR
            msgCitas.col = 12
            msgCitas.Redraw = True
        ElseIf vntResp = vbCancel Then
            msgCitas.col = 12
            msgCitas.Redraw = True
            Exit Sub
        End If
    End If
End With

blnVolcarHistorias2 = False
optOrdenar(0).Value = True
msgCitas.Redraw = False
msgCitas.Rows = 1
'msgCitas.Width = 9750
msgCitas.Width = 11400
Call msgCitas_CambioSeleccion
msgCitas.Redraw = True
cboDepartamento.ListIndex = cboDepartamento.ListCount - 1

'Validaci�n de datos obligatorios
If cboPunto.ListIndex = -1 Then
    Call MsgBox("Debe indicar el punto de archivo", vbOKOnly + vbInformation, "Aviso")
    cboPunto.SetFocus
Exit Sub
End If
If Not IsDate(SSDateCombo1(0).FormattedText) Or Not IsDate(SSDateCombo1(1).FormattedText) Then
    Call MsgBox("Debe Introducir un Rango de Fechas", vbOKOnly + vbInformation, "Aviso")
    SSDateCombo1(0).SetFocus
    Exit Sub
End If
' Compruebo que las segunda fecha sea mayor
If IsDate(SSDateCombo1(0).FormattedText) And IsDate(SSDateCombo1(1).FormattedText) Then
    If DateValue(SSDateCombo1(1).Date) < DateValue(SSDateCombo1(0).Date) Then
        Call MsgBox("El campo Fecha Cita Hasta debe ser mayor que el campo Fecha Cita Desde", vbOKOnly + vbInformation, "Aviso")
        SSDateCombo1(1).SetFocus
       Exit Sub
    End If
End If

'PROCESO VOLCAR **********************************************************************************************

Screen.MousePointer = vbHourglass

'Obtener  cada n� de HC, Fecha de Cita, Departamento y M�dico ordenado por
'N� de HC y Fecha Cita

'strFechasSql = " AND to_char(AC.CI01FECCONCERT,'DD/MM/YYYY') >= '" & SSDateCombo1(0).Date & "' "
'strFechasSql = strFechasSql & " AND to_char(AC.CI01FECCONCERT,'DD/MM/YYYY') <= '" & SSDateCombo1(1).Date & "' "
strFechasSql = " AND AC.CI01FECCONCERT >= TO_DATE('" & SSDateCombo1(0).Date & " 00:00:00','DD/MM/YYYY HH24:MI:SS') "
strFechasSql = strFechasSql & " AND AC.CI01FECCONCERT <= TO_DATE('" & SSDateCombo1(1).Date & " 23:59:59','DD/MM/YYYY HH24:MI:SS') "
SQLHistorias = "SELECT P.CI22NUMHISTORIA, to_char(AC.CI01FECCONCERT, 'DD/MM/YYYY HH24:MI:SS'),"
SQLHistorias = SQLHistorias & " AP.AD02CODDPTO, AG.SG02COD"
SQLHistorias = SQLHistorias & " FROM CI0100 AC, PR0400 AP, PR0100 A,  AG1100 AG, CI2200 P"
SQLHistorias = SQLHistorias & " WHERE AC.CI01SITCITA = 1 " & strFechasSql
SQLHistorias = SQLHistorias & " AND AP.PR04NUMACTPLAN   =   AC.PR04NUMACTPLAN "
SQLHistorias = SQLHistorias & " AND AP.PR37CODESTADO    =   2"
SQLHistorias = SQLHistorias & " AND A.PR01CODACTUACION = AP.PR01CODACTUACION "
SQLHistorias = SQLHistorias & " AND A.PR12CODACTIVIDAD IN (201,214) "
SQLHistorias = SQLHistorias & " AND AG.AG11CODRECURSO=AC.AG11CODRECURSO"
SQLHistorias = SQLHistorias & " AND P.CI21CODPERSONA    =   AP.CI21CODPERSONA "
SQLHistorias = SQLHistorias & " AND P.CI22NUMHISTORIA IS NOT NULL "
SQLHistorias = SQLHistorias & " ORDER BY P.CI22NUMHISTORIA, AC.CI01FECCONCERT"

' SE RECOGEN LOS REGISTROS

Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLHistorias)
Set rsHistorias = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsHistorias.RowCount > 0 Then
   rsHistorias.MoveFirst
   NumHC = ""
   While Not rsHistorias.EOF
        strFechaCita = Trim(rsHistorias(1))
        If IsNull(rsHistorias(2)) = False Then
          strCodDepartamento = Trim(CStr(rsHistorias(2)))
        Else
          strCodDepartamento = ""
        End If
        If IsNull(rsHistorias(3)) = False Then
          strCodMedico = Trim(rsHistorias(3))
        Else
          strCodMedico = ""
        End If
        If NumHC <> Trim(CStr(rsHistorias(0))) Then
            NumHC = Trim(CStr(rsHistorias(0)))
            Set colTipologias = New Collection
            Call Volcar_Tipologias
        ElseIf NumHC = Trim(CStr(rsHistorias(0))) And colTipologias.Count > 0 Then
            Call Evaluar_Preferencias
        End If
        rsHistorias.MoveNext
   Wend
    rsHistorias.Close

    '*************************************************************************************************************
End If
'Una vez finalizado el proceso volcamos las HC que se han insertado en AR0500
Call Volcar_Historias
Screen.MousePointer = vbNormal
'ORDENAMOS POR HUECO
optOrdenar.Item(2) = True
End Sub


Private Sub Form_Load()

MSRDC1.Connect = objApp.rdoConnect.Connect
blnLoad = True
SSDateCombo1(0).Date = Date + 2
SSDateCombo1(1).Date = Date + 2

'Cargar los puntos de archivo
Call Cargar_Puntos

'Cargar los Departamentos
Call Cargar_Departamentos

'Construir la Grid
Call Construir_Grid
optOrdenar(0).Value = True
blnLoad = False
End Sub

Private Sub Cargar_Puntos()

'Declaracion de variables
Dim SQLpuntos As String
Dim inti As Integer

cboPunto.Clear
SQLpuntos = "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200" ' ORDER BY AR02CODARCHIVO"
Set rsPuntos = objApp.rdoConnect.OpenResultset(SQLpuntos, rdOpenKeyset, rdConcurReadOnly)
If rsPuntos.RowCount > 0 Then
    rsPuntos.MoveFirst
    inti = 0
    While Not rsPuntos.EOF
        cboPunto.AddItem Trim(rsPuntos("AR02DESPTOARCHIVO")) & Space(200) & Trim(rsPuntos("AR02CODPTOARCHIVO"))
        cboPunto.ItemData(cboPunto.NewIndex) = inti
        inti = inti + 1
        rsPuntos.MoveNext
    Wend
End If
rsPuntos.Close
End Sub
Private Sub Cargar_Departamentos()

'Declaracion de variables
Dim SQLDepartamentos As String
Dim inti As Integer

cboDepartamento.Clear

'Seleccionar los departamentos
SQLDepartamentos = "SELECT AD02CODDPTO, AD02DESDPTO "
SQLDepartamentos = SQLDepartamentos & " FROM AD0200  "
SQLDepartamentos = SQLDepartamentos & " WHERE AD02FECFIN IS NULL "
SQLDepartamentos = SQLDepartamentos & " ORDER BY AD02DESDPTO"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLDepartamentos)
Set rsDepartamentos = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsDepartamentos.RowCount > 0 Then
rsDepartamentos.MoveFirst
inti = 0
    While Not rsDepartamentos.EOF
        cboDepartamento.AddItem Trim(rsDepartamentos("AD02DESDPTO")) & Space(200) & Trim(rsDepartamentos("AD02CODDPTO"))
        inti = inti + 1
        rsDepartamentos.MoveNext
    Wend
End If
cboDepartamento.AddItem ("<ninguno>")
cboDepartamento.ListIndex = cboDepartamento.ListCount - 1
rsDepartamentos.Close
End Sub

Private Sub Volcar_Tipologias()

'Declaraci�n de variables
Dim SQLTipologias As String
Dim inti As Integer

'Volcar las Tipolog�as disponibles en el archivo
SQLTipologias = "SELECT H.AR00CODTIPOLOGIA"
SQLTipologias = SQLTipologias & " FROM AR0400 H  "
SQLTipologias = SQLTipologias & " WHERE H.CI22NUMHISTORIA=?"
SQLTipologias = SQLTipologias & " AND H.AR06CODESTADO IN ('R','U')"
SQLTipologias = SQLTipologias & " AND H.AR04FECFIN IS NULL"
SQLTipologias = SQLTipologias & " AND H.AR02CODPTOARCHIVO=?"
SQLTipologias = SQLTipologias & " AND NOT EXISTS (SELECT 1 FROM AR0500 P"
SQLTipologias = SQLTipologias & " WHERE P.CI22NUMHISTORIA=H.CI22NUMHISTORIA"
SQLTipologias = SQLTipologias & " AND P.AR00CODTIPOLOGIA=H.AR00CODTIPOLOGIA"
SQLTipologias = SQLTipologias & " AND P.AR05FECFINSOLICITUD IS NULL) "
SQLTipologias = SQLTipologias & " AND NOT EXISTS (SELECT 1 FROM AR0500 P"
SQLTipologias = SQLTipologias & " WHERE P.CI22NUMHISTORIA=H.CI22NUMHISTORIA"
SQLTipologias = SQLTipologias & " AND P.AR00CODTIPOLOGIA=H.AR00CODTIPOLOGIA"
SQLTipologias = SQLTipologias & " AND TRUNC(P.AR05FECSOLICITUD)=TO_DATE(?,'DD/MM/YYYY'))"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLTipologias)
QyConsulta(0) = NumHC
QyConsulta(1) = Trim(Right(cboPunto.List(cboPunto.ListIndex), 2))
QyConsulta(2) = Trim(Left(strFechaCita, 10))
Set rsTipologias = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsTipologias.RowCount > 0 Then
    rsTipologias.MoveFirst
    Set colTipologias = New Collection
    inti = 1
    While Not rsTipologias.EOF
        colTipologias.Add Trim(rsTipologias(0)), CStr(Trim(rsTipologias(0)))
        rsTipologias.MoveNext
        inti = inti + 1
    Wend
    'Evaluar las preferencias de los m�dicos
    Call Evaluar_Preferencias
End If
rsTipologias.Close
End Sub

Private Sub Evaluar_Preferencias()

'Si se ha recuperado alguna Tipolog�a, esta funci�n se encarga
'de evaluar las preferencias de los m�dicos.

'Declaraci�n de variables
Dim SQLPreferencias As String
Dim inti As Integer

SQLPreferencias = "SELECT P.AR00CODTIPOLOGIA"
SQLPreferencias = SQLPreferencias & " FROM AR0900 P  "
SQLPreferencias = SQLPreferencias & " WHERE P.SG02COD=?"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLPreferencias)
QyConsulta(0) = strCodMedico
Set rsPreferencias = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsPreferencias.RowCount > 0 Then
'Si la select devuelve registros, habr� que coger las Tipolog�as por las que tiene
'preferencias este m�dico y por cada una de ellas se realizar� una Insert en
'AR0500 ( siempre y cuando sea una tipolog�a disponible para la HC)
    Set colContador = New Collection
    rsPreferencias.MoveFirst
    While Not rsPreferencias.EOF
        If colTipologias.Count > 1 Then
            For inti = 1 To colTipologias.Count
                If colTipologias(inti) = Trim(rsPreferencias(0)) Then
                    'Si la preferencia del m�dico es una Tipologia disponible la insertamos
                    colContador.Add (CStr(Trim(rsPreferencias(0))))
                    strCodTipologia = Trim(rsPreferencias(0))
                    Call Insertar_UnRegistro(strCodTipologia)
                End If
            Next
        ElseIf colTipologias.Count = 1 Then
            If colTipologias(1) = Trim(rsPreferencias(0)) Then
                'Si la preferencia del m�dico es una Tipologia disponible la insertamos
                colContador.Add (CStr(Trim(rsPreferencias(0))))
                strCodTipologia = Trim(rsPreferencias(0))
                Call Insertar_UnRegistro(strCodTipologia)
            End If
        End If
    rsPreferencias.MoveNext
    Wend
    'Una vez insertado el registro se borra de la coleccion la
    'Tipolog�a ya que dejar� de estar disponible para otro m�dico
    If colContador.Count > 1 Then
        For inti = 1 To colContador.Count
            colTipologias.Remove colContador(inti)
        Next
    ElseIf colContador.Count = 1 Then
        colTipologias.Remove CStr(colContador(1))
    End If
End If
rsPreferencias.Close
End Sub

Private Sub Insertar_UnRegistro(strCodTipologia As String)

'Declaracion de variables
Dim SQLInsertar As String
Dim cn As rdoConnection

Set cn = objApp.rdoConnect

SQLInsertar = "INSERT INTO AR0500"
SQLInsertar = SQLInsertar & " (CI22NUMHISTORIA,AR00CODTIPOLOGIA,AR05FECSOLICITUD, "
SQLInsertar = SQLInsertar & " SG02COD,AD02CODDPTO,AR01CODMOTIVOSOL,AR02CODPTOARCHIVO) "
SQLInsertar = SQLInsertar & "  VALUES (?, ?, to_date(?,'dd/mm/yyyy hh24:mi:ss'), ?, ?, ?, ?)"
Set QyInsertar = cn.CreateQuery("", SQLInsertar)
QyInsertar(0) = NumHC
QyInsertar(1) = strCodTipologia
QyInsertar(2) = strFechaCita
QyInsertar(3) = strCodMedico
QyInsertar(4) = strCodDepartamento
QyInsertar(5) = "C"
QyInsertar(6) = Trim(Right(cboPunto.List(cboPunto.ListIndex), 2))
QyInsertar.Execute
QyInsertar.Close
End Sub

'Private Sub Insertar_Registros()
'
''Declaracion de variables
'Dim SQLInsertar As String
'Dim cn As rdoConnection
'Dim inti As Integer
'
'Set cn = objApp.rdoConnect
'
'For inti = 1 To colTipologias.Count
'    SQLInsertar = "INSERT INTO AR0500"
'    SQLInsertar = SQLInsertar & " (CI22NUMHISTORIA,AR00CODTIPOLOGIA,AR05FECSOLICITUD, "
'    SQLInsertar = SQLInsertar & " SG02COD,AD02CODDPTO,AR01CODMOTIVOSOL,AR02CODPTOARCHIVO) "
'    SQLInsertar = SQLInsertar & "  VALUES (?, ?, to_date(?,'dd/mm/yyyy hh24:mi:ss'), ?, ?, ?, ?)"
'    Set QyInsertar = cn.CreateQuery("", SQLInsertar)
'    QyInsertar(0) = NumHC
'    QyInsertar(1) = colTipologias(inti)
'    QyInsertar(2) = strFechaCita
'    QyInsertar(3) = strCodMedico
'    QyInsertar(4) = strCodDepartamento
'    QyInsertar(5) = "C"
'    QyInsertar(6) = Trim(Right(cboPunto.List(cboPunto.ListIndex), 2))
'    QyInsertar.Execute
'    QyInsertar.Close
'Next
'End Sub

Private Sub Volcar_Historias()

'Declaracion de variables
Dim inti As Integer
Dim SQLCitas As String

SQLCitas = "SELECT P.AR06CODESTADO,' ',' ',D.AD02DESDPTO, P.CI22NUMHISTORIA, P.AR00CODTIPOLOGIA,C.CI22PRIAPEL || ' ' || C.CI22SEGAPEL || ', ' ||C.CI22NOMBRE, "
SQLCitas = SQLCitas & " D.AD02DESDPTO, S.SG02APE1 || ' ' || S.SG02APE2 || ', ' || S.SG02NOM, to_char(P.AR05FECSOLICITUD ,'DD/MM/YYYY HH24:MI:SS'),' ',' ',' '"
SQLCitas = SQLCitas & " FROM AD0200 D, SG0200 S, CI2200 C, AR0500 P "
SQLCitas = SQLCitas & " WHERE P.AR02CODPTOARCHIVO= '" & Trim(Right(cboPunto.List(cboPunto.ListIndex), 2)) & "' "
SQLCitas = SQLCitas & " AND P.AR01CODMOTIVOSOL='C' "
SQLCitas = SQLCitas & " AND P.AR05FECFINSOLICITUD IS NULL "
SQLCitas = SQLCitas & " AND TRUNC(P.AR05FECSOLICITUD) >= TO_DATE('" & SSDateCombo1(0).Date & "','DD/MM/YYYY') "
SQLCitas = SQLCitas & " AND TRUNC(P.AR05FECSOLICITUD) <= TO_DATE('" & SSDateCombo1(1).Date & "','DD/MM/YYYY') "
'SQLCitas =SQLCitas & " AND to_char(P.AR05FECSOLICITUD,'DD/MM/YYYY') >= '" & SSDateCombo1(0).Date & "' "
'SQLCitas = SQLCitas & " AND to_char(P.AR05FECSOLICITUD,'DD/MM/YYYY') <= '" & SSDateCombo1(1).Date & "' "
If cboDepartamento.ListIndex < cboDepartamento.ListCount - 1 Then
    SQLCitas = SQLCitas & " AND P.AD02CODDPTO= '" & Trim(Right(cboDepartamento.List(cboDepartamento.ListIndex), 3)) & "'"
End If
SQLCitas = SQLCitas & " AND C.CI22NUMHISTORIA=P.CI22NUMHISTORIA "
SQLCitas = SQLCitas & " AND D.AD02CODDPTO=P.AD02CODDPTO "
SQLCitas = SQLCitas & " AND S.SG02COD=P.SG02COD "
'SQLCitas = SQLCitas & " ORDER BY P.CI22NUMHISTORIA,P.AR00CODTIPOLOGIA"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLCitas)
Set rsCitas = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Set MSRDC1.Resultset = rsCitas
Call Construir_Grid2
Call msgCitas_CambioSeleccion
Call Control_Objetos
End Sub

Private Sub Construir_Grid()

With msgCitas
    .Redraw = False
    '.Width = 9750
    .Width = 11400
    .Rows = 1
    .cols = 13
    .row = 0
    .col = 0
    .ColWidth(0) = 900
    .TextMatrix(.row, .col) = "Estado"
    .col = 1
    .ColWidth(1) = 0
    .TextMatrix(.row, .col) = "L�nea"
    .col = 2
    .ColWidth(2) = 0
    .TextMatrix(.row, .col) = "Pto. Ubic."
    .col = 3
    .ColWidth(3) = 0
    .TextMatrix(.row, .col) = "Departamento"
    .col = 4
    .ColWidth(4) = 750
    .TextMatrix(.row, .col) = "N�mero"
    .col = 5
    .ColWidth(5) = 1000
    .TextMatrix(.row, .col) = "Tipolog�a"
    .col = 6
    .ColWidth(6) = 2000
    .TextMatrix(.row, .col) = "Paciente"
    .col = 7
    .ColWidth(7) = 1300
    .TextMatrix(.row, .col) = "Departamento"
    .col = 8
    .ColWidth(8) = 2000
    .TextMatrix(.row, .col) = "M�dico"
    .col = 9
    .ColWidth(9) = 1700
    .TextMatrix(.row, .col) = "Fecha Cita"
    .col = 10
    .ColWidth(10) = 0
    .TextMatrix(.row, .col) = "L�nea"
    .col = 11
    .ColWidth(11) = 0
    .TextMatrix(.row, .col) = "Pto. Ubic"
    .col = 12
    .ColWidth(12) = 0
    .Redraw = True
    lngColorAnt = .CellForeColor
End With
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim blnCambios As Boolean
Dim respuesta As Variant
Dim inti As Integer
 'Si se han efectuado cambios
    blnCambios = False
    For inti = 1 To msgCitas.Rows - 1
        If Trim(msgCitas.TextMatrix(inti, 0)) <> "" And Trim(msgCitas.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCAR Then
           respuesta = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
            blnCambios = True
            Exit For
        End If
    Next
    If blnCambios = True Then
        If respuesta = vbYes Then
            Call GUARDAR
            Cancel = 0
        ElseIf respuesta = vbNo Then
            Cancel = 0
        Else
            Cancel = 1
        End If
    Else
        Cancel = 0
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Screen.MousePointer = vbNormal
End Sub


Private Sub msgCitas_Click()
If msgCitas.row <> 0 Then
With msgCitas
    If .TextMatrix(.row, 0) = ANULAR Then
        cmdSeleccionar.Enabled = False
    Else
        cmdSeleccionar.Enabled = True
    End If
  .col = 0
  .rowsel = .row
  .ColSel = .cols - 1
End With
End If
End Sub

Private Sub msgCitas_CambioSeleccion()
With msgCitas
If .Rows = 1 Then
    .BackColorSel = &H8000000F
    .ForeColorSel = &H80000012
Else
    .BackColorSel = &H8000000D
    .ForeColorSel = &H8000000E
End If
End With
End Sub

Private Sub msgCitas_DblClick()
With msgCitas
    If .col = 0 Then
        If Trim(.TextMatrix(.row, 0)) = "" Or Trim(.TextMatrix(.row, 0)) = LOCALIZANDO Then
            If Trim(.TextMatrix(.row, 0)) = LOCALIZANDO Then
             .TextMatrix(.row, 0) = SELECCIONAR_P
            Else
             .TextMatrix(.row, 0) = SELECCIONAR
            End If
        ElseIf Trim(.TextMatrix(.row, 0)) = SELECCIONAR Or Trim(.TextMatrix(.row, 0)) = SELECCIONAR_P Then
            If Trim(.TextMatrix(.row, 0)) = SELECCIONAR_P Then
             .TextMatrix(.row, 0) = LOCALIZANDO
            Else
             .TextMatrix(.row, 0) = ""
            End If
        End If
        .col = 0
        .rowsel = .row
        .ColSel = .cols - 1
    End If
End With
End Sub

Private Sub optAyuda_Click(Index As Integer)
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 17)
End Sub

Private Sub optDatos_Click(Index As Integer)
Dim respuesta2 As Variant
Select Case Index
Case 0
  respuesta2 = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
  If respuesta2 = vbYes Then
    Call GUARDAR
  End If
Case 2
  Unload Me
End Select
End Sub

Private Sub optOrdenar_Click(Index As Integer)
'Declaracion de variables
Dim inti As Integer
Dim vntResp As Variant
Dim blnCambios As Boolean
Dim blnCambios2 As Boolean

Screen.MousePointer = vbHourglass

Select Case Index
    Case 0 'NHC
    With msgCitas
        .Redraw = False
        .col = 1
        .ColWidth(1) = 0
        .col = 2
        .ColWidth(2) = 0
        .col = 3
        .ColWidth(3) = 0
        .col = 7
        .ColWidth(7) = 1300
        If blnVolcarHistorias2 = True Then
            .col = 10
            .ColWidth(10) = 700
            .col = 11
            .ColWidth(11) = 1000
        End If
        .col = 5
        .ColSel = 4
        .Sort = 1
        .col = 12
        .Redraw = True
    End With
   
    Case 1 'Departamento
    With msgCitas
        .Redraw = False
        .col = 1
        .ColWidth(1) = 0
        .col = 2
        .ColWidth(2) = 0
        .col = 3
        .ColWidth(3) = 1300
        .col = 7
        .ColWidth(7) = 0
        If blnVolcarHistorias2 = True Then
            .col = 10
            .ColWidth(10) = 700
            .col = 11
            .ColWidth(11) = 1000
        End If
        .col = 5
        .ColSel = 3
        .Sort = 1
        .col = 12
        .Redraw = True
     End With
    
    Case 2 'Ruta
    blnCambios2 = False
    For inti = 1 To msgCitas.Rows - 1
    '
    If Trim(msgCitas.TextMatrix(inti, 0)) <> "" And Trim(msgCitas.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(msgCitas.TextMatrix(inti, 0)) <> VOLCAR Then
        blnCambios = True
        Exit For
    End If
    Next
    If blnVolcarHistorias2 = False And blnCambios = True Then
        'Preguntar si desea guardar
        With msgCitas
            .Redraw = False
            blnCambios = False
            For inti = 1 To .Rows - 1
                If Trim(.TextMatrix(inti, 0)) <> "" And Trim(.TextMatrix(inti, 0)) <> LOCALIZANDO _
                And Trim(.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(.TextMatrix(inti, 0)) <> VOLCARHASTA _
                And Trim(.TextMatrix(inti, 0)) <> VOLCAR Then
                    vntResp = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
                    blnCambios = True
                    Exit For
                End If
            Next
            If blnCambios = True Then
                If vntResp = vbYes Then
                    Call GUARDAR
                    Call Volcar_Historias2
                ElseIf vntResp = vbNo Then
                    Call Volcar_Historias2
                End If
            End If
            .col = 12
            .Redraw = True
        End With
    ElseIf blnVolcarHistorias2 = False And blnCambios = False Then
      With msgCitas
        .Redraw = False
        Call Volcar_Historias2
        .col = 12
        .Redraw = True
      End With
    ElseIf blnVolcarHistorias2 = True Then
        With msgCitas
            .Redraw = False
            .col = 1
            .ColWidth(1) = 700
            .col = 2
            .ColWidth(2) = 1000
            .col = 3
            .ColWidth(3) = 0
            .col = 7
            .ColWidth(7) = 1300
            .col = 10
            .ColWidth(10) = 0
            .col = 11
            .ColWidth(11) = 0
            .col = 2
            .ColSel = 1
            .Sort = 1
            .col = 12
            .Redraw = True
        End With
    End If
   
End Select
Limpiar_Grid_DH
Screen.MousePointer = vbNormal
End Sub

Private Sub GUARDAR()

'Declaracion de variables
Dim c As rdoConnection
Dim cn As rdoConnection
Dim q As rdoQuery
Dim r As rdoResultset
Dim SYSDATE As String   'contiene la fecha-hora del servidor
Dim SQLBorrar As String
Dim SQLInsertar As String
Dim inti As Integer

Screen.MousePointer = vbHourglass

'Guardar
'coger la hora del servidor
Set c = objApp.rdoConnect
Set cn = objApp.rdoConnect

Set q = c.CreateQuery("", "SELECT TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') FROM DUAL")
q.Execute
Set r = q.OpenResultset
SYSDATE = r(0)
r.Close
q.Close
'ANULAR
For inti = 1 To msgCitas.Rows - 1
  If msgCitas.TextMatrix(inti, 0) = ANULAR Or msgCitas.TextMatrix(inti, 0) = ANULAR_P Then
      SQLBorrar = "DELETE FROM AR0500"
      SQLBorrar = SQLBorrar & " WHERE CI22NUMHISTORIA=? "
      SQLBorrar = SQLBorrar & " AND AR00CODTIPOLOGIA=? "
      SQLBorrar = SQLBorrar & " AND AR01CODMOTIVOSOL=? "
      SQLBorrar = SQLBorrar & " AND AR05FECFINSOLICITUD IS NULL "
      Set QyBorrar = cn.CreateQuery("", SQLBorrar)
      QyBorrar(0) = Trim(msgCitas.TextMatrix(inti, 4))
      QyBorrar(1) = Trim(msgCitas.TextMatrix(inti, 5))
      QyBorrar(2) = "C"
      QyBorrar.Execute
      QyBorrar.Close
  End If
    'PRESTAR
    If msgCitas.TextMatrix(inti, 0) = SELECCIONAR Or msgCitas.TextMatrix(inti, 0) = SELECCIONAR_P Then
      'CERRAR LA SOLICITUD
      SQLInsertar = "UPDATE AR0500"
      SQLInsertar = SQLInsertar & " SET AR05FECFINSOLICITUD=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')"
      SQLInsertar = SQLInsertar & " WHERE CI22NUMHISTORIA=? "
      SQLInsertar = SQLInsertar & " AND AR00CODTIPOLOGIA=? "
      SQLInsertar = SQLInsertar & " AND AR01CODMOTIVOSOL=? "
      SQLInsertar = SQLInsertar & " AND AR05FECFINSOLICITUD IS NULL "
      Set QyInsertar = cn.CreateQuery("", SQLInsertar)
      QyInsertar(0) = SYSDATE
      QyInsertar(1) = Trim(msgCitas.TextMatrix(inti, 4))
      QyInsertar(2) = Trim(msgCitas.TextMatrix(inti, 5))
      QyInsertar(3) = "C"
      QyInsertar.Execute
      QyInsertar.Close
      
      'CERRAR EL ESTADO ACTUAL DE LA HC-TIPOLOGIA
      SQLInsertar = "UPDATE AR0400"
      SQLInsertar = SQLInsertar & " SET AR04FECFIN=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')"
      SQLInsertar = SQLInsertar & " WHERE CI22NUMHISTORIA=? "
      SQLInsertar = SQLInsertar & " AND AR00CODTIPOLOGIA=? "
      SQLInsertar = SQLInsertar & " AND AR04FECFIN IS NULL "
      Set QyInsertar = cn.CreateQuery("", SQLInsertar)
      QyInsertar(0) = SYSDATE
      QyInsertar(1) = Trim(msgCitas.TextMatrix(inti, 4))
      QyInsertar(2) = Trim(msgCitas.TextMatrix(inti, 5))
      QyInsertar.Execute
      QyInsertar.Close
      
      
      'INSERTAR EL NUEVO ESTADO DE LA HC-TIPOLOGIA
      SQLInsertar = "INSERT INTO AR0400"
      SQLInsertar = SQLInsertar & " (CI22NUMHISTORIA, AR00CODTIPOLOGIA, AR04FECINICIO, "
      SQLInsertar = SQLInsertar & " AR06CODESTADO, AR02CODPTOARCHIVO) "
      SQLInsertar = SQLInsertar & " VALUES (?, ?, TO_DATE (?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?)"
      Set QyInsertar = cn.CreateQuery("", SQLInsertar)
      QyInsertar(0) = Trim(msgCitas.TextMatrix(inti, 4))
      QyInsertar(1) = Trim(msgCitas.TextMatrix(inti, 5))
      QyInsertar(2) = SYSDATE
      QyInsertar(3) = "P"
      QyInsertar(4) = Trim(Right(cboPunto.List(cboPunto.ListIndex), 2))
      QyInsertar.Execute
      QyInsertar.Close
    End If
  Next
      
'Refrescar la Tabla
If optOrdenar(0).Value = True Then
    Call Volcar_Historias
ElseIf optOrdenar(1).Value = True Then
    Call Volcar_Historias
ElseIf optOrdenar(2).Value = True Then
    Call Volcar_Historias2
End If
Screen.MousePointer = vbNormal
End Sub

Private Sub Volcar_Historias2()

'Declaracion de variables
Dim inti As Integer
Dim SQLCitas As String

SQLCitas = "SELECT P.AR06CODESTADO,H.AR03CODLINEA, H.AR04NUMHUECO,D.AD02DESDPTO,P.CI22NUMHISTORIA, P.AR00CODTIPOLOGIA,C.CI22PRIAPEL || ' ' || C.CI22SEGAPEL || ', ' ||C.CI22NOMBRE, "
SQLCitas = SQLCitas & " D.AD02DESDPTO, S.SG02APE1 || ' ' || S.SG02APE2 || ', ' || S.SG02NOM, to_char(P.AR05FECSOLICITUD ,'DD/MM/YYYY HH24:MI:SS'), "
SQLCitas = SQLCitas & " H.AR03CODLINEA, H.AR04NUMHUECO,' ' "
SQLCitas = SQLCitas & " FROM AD0200 D, SG0200 S, CI2200 C, AR0500 P, AR0400 H "
SQLCitas = SQLCitas & " WHERE P.AR02CODPTOARCHIVO='" & Trim(Right(cboPunto.List(cboPunto.ListIndex), 2)) & "' "
SQLCitas = SQLCitas & " AND P.AR01CODMOTIVOSOL='C' "
SQLCitas = SQLCitas & " AND P.AR05FECFINSOLICITUD IS NULL "
SQLCitas = SQLCitas & " AND TRUNC(P.AR05FECSOLICITUD) >= TO_DATE('" & SSDateCombo1(0).Date & "','DD/MM/YYYY') "
SQLCitas = SQLCitas & " AND TRUNC(P.AR05FECSOLICITUD) <= TO_DATE('" & SSDateCombo1(1).Date & "','DD/MM/YYYY') "
If cboDepartamento.ListIndex < cboDepartamento.ListCount - 1 Then
    SQLCitas = SQLCitas & " AND P.AD02CODDPTO='" & Trim(Right(cboDepartamento.List(cboDepartamento.ListIndex), 3)) & "' "
End If
SQLCitas = SQLCitas & " AND C.CI22NUMHISTORIA=P.CI22NUMHISTORIA "
SQLCitas = SQLCitas & " AND D.AD02CODDPTO=P.AD02CODDPTO "
SQLCitas = SQLCitas & " AND S.SG02COD=P.SG02COD "
SQLCitas = SQLCitas & " AND H.CI22NUMHISTORIA=P.CI22NUMHISTORIA "
SQLCitas = SQLCitas & " AND H.AR00CODTIPOLOGIA=P.AR00CODTIPOLOGIA "
SQLCitas = SQLCitas & " AND H.AR04FECFIN IS NULL "
SQLCitas = SQLCitas & " ORDER BY H.AR03CODLINEA, H.AR04NUMHUECO "

Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLCitas)
Set rsCitas = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Set MSRDC1.Resultset = rsCitas

blnVolcarHistorias2 = True
Call Construir_Grid3
Call msgCitas_CambioSeleccion
Call Control_Objetos
End Sub

Private Sub Control_Objetos()
If msgCitas.Rows = 1 Then
  cmdListar.Enabled = False
  tlbToolBar.Buttons(5).Enabled = False
  optRegistro(0).Enabled = False
  tlbToolBar.Buttons(6).Enabled = False
  optRegistro(1).Enabled = False
  tlbToolBar.Buttons(7).Enabled = False
  optRegistro(2).Enabled = False
  tlbToolBar.Buttons(8).Enabled = False
  optRegistro(3).Enabled = False
Else
  cmdListar.Enabled = True
  tlbToolBar.Buttons(5).Enabled = True
  optRegistro(0).Enabled = True
  tlbToolBar.Buttons(6).Enabled = True
  optRegistro(1).Enabled = True
  tlbToolBar.Buttons(7).Enabled = True
  optRegistro(2).Enabled = True
  tlbToolBar.Buttons(8).Enabled = True
  optRegistro(3).Enabled = True
End If
End Sub

Private Sub optRegistro_Click(Index As Integer)
Select Case Index
  Case 0
    msgCitas.row = 1
  Case 1
    If msgCitas.row > 1 Then
      msgCitas.row = msgCitas.row - 1
    End If
  Case 2
    If msgCitas.row < msgCitas.Rows - 1 Then
      msgCitas.row = msgCitas.row + 1
    End If
  Case 3
    msgCitas.row = msgCitas.Rows - 1
End Select
End Sub

Private Sub SSDateCombo1_DropDown(Index As Integer)
Dim blnCambios As Boolean
Dim vntResp As Variant
Dim inti As Integer
'Preguntar si desea guardar
With msgCitas
    .Redraw = False
    blnCambios = False
    For inti = 1 To .Rows - 1
        If Trim(.TextMatrix(inti, 0)) <> "" And Trim(.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(.TextMatrix(inti, 0)) <> VOLCAR Then
            vntResp = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
            blnCambios = True
            Exit For
        End If
    Next
End With
If blnCambios = True Then
    If vntResp = vbYes Then
        Call GUARDAR
    ElseIf vntResp = vbCancel Then
        msgCitas.SetFocus
        msgCitas.col = 12
        msgCitas.Redraw = True
        Exit Sub
    End If
    msgCitas.col = 12
    msgCitas.Redraw = True
    Screen.MousePointer = vbHourglass
    If blnVolcarHistorias2 = False Then
        Call Volcar_Historias
    Else
        Call Volcar_Historias2
    End If
    If optOrdenar(0).Value = True Then
        Call optOrdenar_Click(0)
    ElseIf optOrdenar(1).Value = True Then
        Call optOrdenar_Click(1)
    End If
    Screen.MousePointer = vbNormal
Else
    msgCitas.Redraw = True
End If
End Sub

Private Sub SSDateCombo1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
Dim blnCambios As Boolean
Dim vntResp As Variant
Dim inti As Integer
'Preguntar si desea guardar
With msgCitas
    .Redraw = False
    blnCambios = False
    For inti = 1 To .Rows - 1
        If Trim(.TextMatrix(inti, 0)) <> "" And Trim(.TextMatrix(inti, 0)) <> LOCALIZANDO _
        And Trim(.TextMatrix(inti, 0)) <> VOLCARDESDE And Trim(.TextMatrix(inti, 0)) <> VOLCARHASTA _
        And Trim(.TextMatrix(inti, 0)) <> VOLCAR Then
            vntResp = MsgBox("�Desea guardar los cambios realizados?", vbYesNoCancel + vbQuestion, "Guardar")
            blnCambios = True
            Exit For
        End If
    Next
End With
If blnCambios = True Then
    KeyCode = 0
    If vntResp = vbYes Then
        Call GUARDAR
    ElseIf vntResp = vbCancel Then
        msgCitas.col = 12
        msgCitas.Redraw = True
        Exit Sub
    End If
    msgCitas.col = 12
    msgCitas.Redraw = True
    Screen.MousePointer = vbHourglass
    If blnVolcarHistorias2 = False Then
        Call Volcar_Historias
    Else
        Call Volcar_Historias2
    End If
    If optOrdenar(0).Value = True Then
        Call optOrdenar_Click(0)
    ElseIf optOrdenar(1).Value = True Then
        Call optOrdenar_Click(1)
    End If
    Screen.MousePointer = vbNormal
Else
    msgCitas.Redraw = True
End If
End Sub

Private Sub tlbToolBar_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Index
  Case 2  'Grabar
    Call optDatos_Click(0)
  Case 5 'Primero
    msgCitas.row = 1
    Call msgCitas_Click
  Case 6 'Anterior
    If msgCitas.row > 1 Then
      msgCitas.row = msgCitas.row - 1
      Call msgCitas_Click
    End If
  Case 7 'Siguiente
    If msgCitas.row < msgCitas.Rows - 1 Then
      msgCitas.row = msgCitas.row + 1
      Call msgCitas_Click
    End If
  Case 8 'ultimo
    msgCitas.row = msgCitas.Rows - 1
    Call msgCitas_Click
  Case 10 'Salir
    Call optDatos_Click(2)
End Select
End Sub
Private Sub Construir_Grid2()
With msgCitas
    .Redraw = False
    '.Width = 9750
    .Width = 11400
    .row = 0
    .col = 0
    .ColWidth(0) = 900
    .TextMatrix(.row, .col) = "Estado"
    .col = 1
    .ColWidth(1) = 0
    .TextMatrix(.row, .col) = "L�nea"
    .col = 2
    .ColWidth(2) = 0
    .TextMatrix(.row, .col) = "Pto. Ubic."
    .col = 3
    .ColWidth(3) = 0
    .TextMatrix(.row, .col) = "Departamento"
    .col = 4
    .ColWidth(4) = 750
    .TextMatrix(.row, .col) = "N�mero"
    .col = 5
    .ColWidth(5) = 1000
    .TextMatrix(.row, .col) = "Tipolog�a"
    .col = 6
    .ColWidth(6) = 2000
    .TextMatrix(.row, .col) = "Paciente"
    .col = 7
    .ColWidth(7) = 1300
    .TextMatrix(.row, .col) = "Departamento"
    .col = 8
    .ColWidth(8) = 2000
    .TextMatrix(.row, .col) = "M�dico"
    .col = 9
    .ColWidth(9) = 1700
    .TextMatrix(.row, .col) = "Fecha Cita"
    .col = 10
    .ColWidth(10) = 0
    .TextMatrix(.row, .col) = "L�nea"
    .col = 11
    .ColWidth(11) = 0
    .TextMatrix(.row, .col) = "Pto. Ubic"
    .col = 12
    .ColWidth(12) = 0
    .Redraw = True
End With
End Sub
Private Sub Construir_Grid3()
  With msgCitas
    .Redraw = False
    .Width = 11400
    .row = 0
    .col = 0
    .ColWidth(0) = 900
    .TextMatrix(.row, .col) = "Estado"
    .col = 1
    .ColWidth(1) = 700
    .TextMatrix(.row, .col) = "L�nea"
    .col = 2
    .ColWidth(2) = 1000
    .TextMatrix(.row, .col) = "Pto. Ubic."
    .col = 3
    .ColWidth(3) = 0
    .TextMatrix(.row, .col) = "Departamento"
    .col = 4
    .ColWidth(4) = 750
    .TextMatrix(.row, .col) = "N�mero"
    .col = 5
    .ColWidth(5) = 1000
    .TextMatrix(.row, .col) = "Tipolog�a"
    .col = 6
    .ColWidth(6) = 2000
    .TextMatrix(.row, .col) = "Paciente"
    .col = 7
    .ColWidth(7) = 1300
    .TextMatrix(.row, .col) = "Departamento"
    .col = 8
    .ColWidth(8) = 2000
    .TextMatrix(.row, .col) = "M�dico"
    .col = 9
    .ColWidth(9) = 1700
    .TextMatrix(.row, .col) = "Fecha Cita"
    .col = 10
    .ColWidth(10) = 0
    .TextMatrix(.row, .col) = "L�nea"
    .col = 11
    .ColWidth(11) = 0
    .TextMatrix(.row, .col) = "Pto. Ubic"
    .col = 12
    .ColWidth(12) = 0
    .Redraw = True
  End With
End Sub
' LIMPIA LOS ESTADOS DE DESDE Y HASTA CUANDO ORDENEMOS
Public Sub Limpiar_Grid_DH()
With msgCitas
    If .Rows > 1 Then
        .row = 1
        While .row < .Rows - 1
            If Trim(.TextMatrix(.row, 0)) = VOLCARDESDE Or Trim(.TextMatrix(.row, 0)) = VOLCARHASTA Or Trim(.TextMatrix(.row, 0)) = VOLCAR Then
               .TextMatrix(.row, 0) = ""
            End If
            .row = .row + 1
        Wend
         'ULTIMA FILA
        If .row = .Rows - 1 Then
            If Trim(.TextMatrix(.row, 0)) = VOLCARDESDE Or Trim(.TextMatrix(.row, 0)) = VOLCARHASTA Or Trim(.TextMatrix(.row, 0)) = VOLCAR Then
               .TextMatrix(.row, 0) = ""
            End If
        End If
    End If
End With
End Sub
