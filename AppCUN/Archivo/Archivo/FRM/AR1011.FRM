VERSION 5.00
Begin VB.Form dlgReubicacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Reubicaci�n de Historia Cl�nica"
   ClientHeight    =   2130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4455
   ControlBox      =   0   'False
   Icon            =   "AR1011.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   4455
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2520
      TabIndex        =   3
      Top             =   1560
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   720
      TabIndex        =   2
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nueva Ubicaci�n"
      Height          =   1215
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   4215
      Begin VB.TextBox txtHueco 
         Height          =   315
         Left            =   2400
         MaxLength       =   5
         TabIndex        =   1
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtLinea 
         Height          =   315
         Left            =   600
         MaxLength       =   4
         TabIndex        =   0
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblHueco 
         Caption         =   "Pto.Ubicaci�n"
         Height          =   255
         Left            =   2400
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label lblLinea 
         Caption         =   "L�nea"
         Height          =   255
         Left            =   600
         TabIndex        =   5
         Top             =   360
         Width           =   735
      End
   End
End
Attribute VB_Name = "dlgReubicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'variables de entrada
Public cod_ptoarchivo As String 'pto de archivo de la hc
Public hc As String             'numero de la hc
Public cod_tipologia As String  'tipologia de la hc
'variables de entrada/salida
Public linea As String          'linea de destino
Public hueco As String          'hueco de destino
'variables de salida
Public reubicar As Boolean        'indica si se debe realizar la reubicacion o no
Private Sub cmdAceptar_Click()
  If txtLinea.Text = "" Then
    MsgBox "Debe especificar la L�nea donde est� Ubicada la H.C.", vbOKOnly + vbExclamation + vbApplicationModal, "L�nea incorrecta"
    txtLinea.SetFocus
  Else
    If txtHueco.Text = "" Then
      MsgBox "Debe especificar el Pto. de Ubicaci�n donde est� Ubicada la H.C.", vbOKOnly + vbExclamation + vbApplicationModal, "Punto de Ubicaci�n incorrecto"
      txtHueco.SetFocus
    Else
      If txtLinea.Text <> "" And txtHueco.Text <> "" Then
        reubicar = True
        linea = txtLinea.Text
        hueco = txtHueco.Text
        Unload Me
      End If
    End If
  End If
End Sub
Private Sub cmdCancelar_Click()
  Unload Me
End Sub
Private Sub Form_Load()
  reubicar = False
End Sub

Private Sub txtHueco_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
  End If
End Sub

Private Sub txtHueco_Lostfocus()
  Call comprobar
End Sub
Sub comprobar()
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  'comprobar linea
  If txtLinea.Text <> "" Then
    'comprobar que es un numero correcto
    If Not IsNumeric(txtLinea.Text) Then 'no es correcto
      MsgBox "La entrada """ & txtLinea.Text & """ no es un n�mero de L�nea v�lida." & Chr(13) & "Introduzca un n�mero por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "L�nea incorrecta"
      'reintroducir linea
      txtLinea.Text = ""
      txtLinea.SetFocus
    Else  'es correcto
      'comprobar si la linea existe
      Set c = objApp.rdoConnect
      Set q = c.CreateQuery("", "SELECT AR00CODTIPOLOGIA FROM AR0300 WHERE AR02CODPTOARCHIVO = ? AND AR03CODLINEA = ?")
      q(0) = cod_ptoarchivo
      q(1) = txtLinea.Text
      q.Execute
      Set r = q.OpenResultset
      If r.EOF Then  'la linea no existe
        MsgBox "No existe la L�nea " & txtLinea.Text & "." & Chr(13) & "Introduzca otra l�nea por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "L�nea incorrecta"
        'reintroducir linea
        txtLinea.Text = ""
        txtLinea.SetFocus
      Else  'la linea existe
        'comprobar que la linea donde ubicar admite la tipologia de la hc
        If Not IsNull(r("AR00CODTIPOLOGIA")) And r("AR00CODTIPOLOGIA") <> cod_tipologia Then  'no admite
          Set q = c.CreateQuery("", "SELECT AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
          q(0) = cod_tipologia
          q.Execute
          Set r = q.OpenResultset
          MsgBox "La L�nea seleccionada no admite H.C. de Tipolog�a " & r("AR00DESTIPOLOGIA") & ".", vbOKOnly + vbExclamation + vbApplicationModal, "L�nea incorrecta"
          'reintroducir linea
          txtLinea.Text = ""
          txtLinea.SetFocus
        End If
      End If
      r.Close
      q.Close
    End If
  End If
  'comprobar hueco
  If txtHueco.Text <> "" And txtLinea.Text <> "" Then
    If Not IsNumeric(txtHueco.Text) Then 'no es correcto
      MsgBox "La entrada """ & txtHueco.Text & """ no es un Punto de Ubicaci�n v�lido." & Chr(13) & "Introduzca un n�mero por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Punto de Ubicaci�n incorrecto"
      'reintroducir hueco
      txtHueco.Text = ""
      txtHueco.SetFocus
    Else  'es correcto
      If Val(txtHueco.Text) < 1 Then 'hueco < 1
        MsgBox "La entrada """ & txtHueco.Text & """ no es un Punto de Ubicaci�n v�lido." & Chr(13) & "Introduzca un n�mero mayor que 1 por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Aviso"
        'reintroducir hueco
        txtHueco.Text = ""
        txtHueco.SetFocus
      Else  'hueco >= 1
        'comprobar si el hueco esta dentro del rango
        Set c = objApp.rdoConnect
        Set q = c.CreateQuery("", "SELECT AR03NUMHUECOS FROM AR0300 WHERE AR02CODPTOARCHIVO = ? AND AR03CODLINEA = ?")
        q(0) = cod_ptoarchivo
        q(1) = txtLinea.Text
        q.Execute
        Set r = q.OpenResultset
        If Val(txtHueco.Text) > r("AR03NUMHUECOS") Then 'fuera del rango
          MsgBox "Esta L�nea tiene " & Str(r("AR03NUMHUECOS")) & " Puntos de Ubicaci�n." & Chr(13) & "Introduzca un Punto de Ubicaci�n menor por favor.", vbOKOnly + vbExclamation + vbApplicationModal, "Punto de Ubicaci�n incorrecto"
          'reintroducir hueco
          txtHueco.Text = ""
          txtHueco.SetFocus
        End If
        r.Close
        q.Close
      End If
    End If
  End If
End Sub

Private Sub txtLinea_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
  End If
End Sub

Private Sub txtLinea_LostFocus()
  Call comprobar
End Sub

