VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form dlgSelCtaTraslados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Seleccionar Consulta de Traslados de Historias Cl�nicas"
   ClientHeight    =   5370
   ClientLeft      =   2085
   ClientTop       =   1335
   ClientWidth     =   6690
   ControlBox      =   0   'False
   HelpContextID   =   23
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   6690
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   23
      Left            =   4920
      TabIndex        =   18
      Top             =   4800
      Width           =   1575
   End
   Begin VB.ComboBox cboDestino 
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1080
      Width           =   4575
   End
   Begin VB.Frame Frame6 
      Caption         =   "Historias Cl�nicas"
      Height          =   2535
      Left            =   180
      TabIndex        =   12
      Top             =   2640
      Width           =   4575
      Begin VB.TextBox txtHasta 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3060
         MaxLength       =   7
         TabIndex        =   5
         Top             =   360
         Width           =   915
      End
      Begin VB.TextBox txtDesde 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   900
         MaxLength       =   7
         TabIndex        =   4
         Top             =   360
         Width           =   915
      End
      Begin VB.ListBox lstTipologia 
         Height          =   1230
         ItemData        =   "AR1018.frx":0000
         Left            =   600
         List            =   "AR1018.frx":0002
         MultiSelect     =   1  'Simple
         TabIndex        =   6
         Top             =   1080
         Width           =   3375
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2400
         TabIndex        =   16
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   390
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Tipolog�as"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   840
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Traslado"
      Height          =   975
      Left            =   180
      TabIndex        =   9
      Top             =   1560
      Width           =   4575
      Begin SSCalendarWidgets_A.SSDateCombo datDesde 
         Height          =   375
         Left            =   840
         TabIndex        =   2
         Top             =   360
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin SSCalendarWidgets_A.SSDateCombo datHasta 
         Height          =   375
         Left            =   2880
         TabIndex        =   3
         Top             =   360
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin VB.Label Label6 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2400
         TabIndex        =   10
         Top             =   480
         Width           =   495
      End
   End
   Begin VB.ComboBox cboOrigen 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4920
      TabIndex        =   8
      Top             =   2640
      Width           =   1575
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   4920
      TabIndex        =   7
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "Punto de Archivo Destino"
      Height          =   255
      Left            =   180
      TabIndex        =   17
      Top             =   840
      Width           =   2415
   End
   Begin VB.Label Label8 
      Caption         =   "Punto de Archivo Origen"
      Height          =   255
      Left            =   180
      TabIndex        =   14
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "dlgSelCtaTraslados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim c As rdoConnection
Dim q As rdoQuery
Dim r As rdoResultset
Dim puntos As New Collection
Dim tipologias As New Collection

Private Sub cmdAyuda_Click()
  Dim h As Integer
  h = WinHelp(hwnd, (App.HelpFile), &H1, 23)
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdConsultar_Click()
  'declaraciones
  Dim CORRECTO As Boolean
  Dim sql As String
  Dim sSelectTabla As String
  Dim sSelectRPT As String
  Dim haytipologias As Boolean
  Dim i As Integer
  Dim primero As Boolean
  'comprobaciones
  CORRECTO = True
  If datDesde.FormattedText <> "" And Not IsDate(datDesde.FormattedText) Then
    MsgBox "El contenido del campo Fecha Desde no es una fecha correcta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    CORRECTO = False
    datDesde.SetFocus
  End If
  If CORRECTO And datHasta.FormattedText <> "" And Not IsDate(datHasta.FormattedText) Then
    MsgBox "El contenido del campo Fecha Hasta no es una fecha correcta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    CORRECTO = False
    datHasta.SetFocus
  End If
  If CORRECTO And datDesde.FormattedText <> "" And datHasta.FormattedText <> "" Then
    If DateValue(datDesde.Date) > DateValue(datHasta.Date) Then
      MsgBox "Fecha Desde debe ser menor que Fecha Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
      CORRECTO = False
      datDesde.SetFocus
    End If
  End If
  If CORRECTO And txtDesde.Text <> "" And Not IsNumeric(txtDesde.Text) Then
    MsgBox "Desde no es un n�mero correcto", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    CORRECTO = False
    txtDesde.SetFocus
  End If
  If CORRECTO And txtHasta.Text <> "" And Not IsNumeric(txtHasta.Text) Then
    MsgBox "Hasta no es un n�mero correcto", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    CORRECTO = False
    txtHasta.SetFocus
  End If
  If CORRECTO And txtDesde.Text <> "" And txtHasta.Text <> "" And Val(txtDesde.Text) > Val(txtHasta.Text) Then
    MsgBox "El contenido del campo Desde debe ser menor que el del campo Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    CORRECTO = False
    txtDesde.SetFocus
  End If
  If CORRECTO Then  'comprobaciones superadas
    sSelectTabla = "SELECT "
    sSelectTabla = sSelectTabla & "TO_CHAR (H1.AR04FECINICIO, 'DD/MM/YYYY HH24:MI:SS') AS ""Fecha de Traslado"", "
    sSelectTabla = sSelectTabla & "P2.AR02DESPTOARCHIVO AS ""Archivo Origen"", "
    sSelectTabla = sSelectTabla & "P1.AR02DESPTOARCHIVO AS ""Archivo Destino"", "
    sSelectTabla = sSelectTabla & "H1.CI22NUMHISTORIA AS ""N�mero H.C."", "
    sSelectTabla = sSelectTabla & "T.AR00DESTIPOLOGIA AS ""Tipolog�a"" , "
    'Para el Report
    sSelectRPT = sSelectTabla & "C.CI22NOMBRE,C.CI22PRIAPEL,C.CI22SEGAPEL "
    'Para la Tabla
    sSelectTabla = sSelectTabla & "C.CI22NOMBRE||' '||C.CI22PRIAPEL||' '||C.CI22SEGAPEL AS ""Paciente"" "
    'SQL
    sql = "FROM "
    sql = sql & "CI2200 C, "
    sql = sql & "AR0000 T, "
    sql = sql & "AR0200 P2, "
    sql = sql & "AR0200 P1, "
    sql = sql & "AR0400 H2, "
    sql = sql & "AR0400 H1 "
    sql = sql & "WHERE "
    If txtDesde.Text <> "" Then
      sql = sql & "H1.CI22NUMHISTORIA >= " & txtDesde.Text & " AND "
    End If
    If txtHasta.Text <> "" Then
      sql = sql & "H1.CI22NUMHISTORIA <= " & txtHasta.Text & " AND "
    End If
    haytipologias = False
    For i = 0 To lstTipologia.ListCount - 1
      If lstTipologia.Selected(i) Then
        haytipologias = True
      End If
    Next i
    primero = True
    If haytipologias Then
      sql = sql & "H1.AR00CODTIPOLOGIA IN ("
      For i = 0 To lstTipologia.ListCount - 1
        If lstTipologia.Selected(i) Then
          If Not primero Then
            sql = sql & ", "
          End If
          primero = False
          sql = sql & "'" & tipologias(i + 1) & "'"
        End If
      Next i
      sql = sql & ") AND "
    End If
    sql = sql & "H1.AR06CODESTADO = 'T' AND "
    If datDesde.FormattedText <> "" Then
      sql = sql & "TRUNC(H1.AR04FECINICIO) >= TO_DATE ('" & datDesde.Date & "', 'DD/MM/YYYY') AND "
    End If
    If datHasta.FormattedText <> "" Then
      sql = sql & "TRUNC(H1.AR04FECINICIO) <= TO_DATE ('" & datHasta.Date & "', 'DD/MM/YYYY') AND "
    End If
    If cboDestino.ListIndex <> cboDestino.ListCount - 1 Then
      sql = sql & "H1.AR02CODPTOARCHIVO = '" & puntos(cboDestino.ListIndex + 1) & "' AND "
    End If
    sql = sql & "H2.CI22NUMHISTORIA = H1.CI22NUMHISTORIA AND "
    sql = sql & "H2.AR00CODTIPOLOGIA = H1.AR00CODTIPOLOGIA AND "
    sql = sql & "H2.AR04FECFIN = H1.AR04FECINICIO AND "
    If cboOrigen.ListIndex <> cboOrigen.ListCount - 1 Then
      sql = sql & "H2.AR02CODPTOARCHIVO = '" & puntos(cboOrigen.ListIndex + 1) & "' AND "
    End If
    sql = sql & "P1.AR02CODPTOARCHIVO = H1.AR02CODPTOARCHIVO AND "
    sql = sql & "P2.AR02CODPTOARCHIVO = H2.AR02CODPTOARCHIVO AND "
    sql = sql & "T.AR00CODTIPOLOGIA = H1.AR00CODTIPOLOGIA AND "
    sql = sql & "C.CI22NUMHISTORIA(+) = H1.CI22NUMHISTORIA "
    sql = sql & " ORDER BY H1.AR04FECINICIO"
    'mostrar resultados consulta
    Screen.MousePointer = vbHourglass
    frmCtaTraslados.sSelectTabla = sSelectTabla
    frmCtaTraslados.sSelectRPT = sSelectRPT
    frmCtaTraslados.sql = sql
    frmCtaTraslados.Show vbModal
    Set frmCtaTraslados = Nothing
  End If
End Sub

Private Sub Form_Load()
  'declaraciones
  Dim i As Integer
  'inicializaciones
  datDesde.Date = Date
  datHasta.Date = Date
  'cargar combos con puntos de archivo
  Set c = objApp.rdoConnect
  Set q = c.CreateQuery("", "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO")
  Set r = q.OpenResultset
  Set puntos = Nothing
  Do Until r.EOF
    cboOrigen.AddItem CStr(r(1))
    cboDestino.AddItem CStr(r(1))
    puntos.Add CStr(r(0))
    r.MoveNext
  Loop
  r.Close
  q.Close
  cboOrigen.AddItem "<ninguno>"
  cboDestino.AddItem "<ninguno>"
  cboOrigen.ListIndex = cboOrigen.ListCount - 1
  cboDestino.ListIndex = cboDestino.ListCount - 1
  'cargar tipologias
  Set q = c.CreateQuery("", "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA")
  Set r = q.OpenResultset
  Set tipologias = Nothing
  While Not r.EOF
    lstTipologia.AddItem r(1)
    tipologias.Add CStr(r(0))
    i = i + 1
    r.MoveNext
  Wend
  r.Close
  q.Close
End Sub

Private Sub txtDesde_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtHasta_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub
