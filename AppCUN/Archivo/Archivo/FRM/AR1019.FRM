VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "MSRDC20.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmCtaBajas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Consulta de Baja de Historias Cl�nicas"
   ClientHeight    =   6720
   ClientLeft      =   705
   ClientTop       =   1290
   ClientWidth     =   11865
   HelpContextID   =   14
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   11865
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   5520
      Top             =   720
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      Destination     =   1
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Historias Cl�nicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5040
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   1320
      Width           =   11730
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4545
         Index           =   0
         Left            =   180
         TabIndex        =   2
         Top             =   360
         Width           =   11415
         ScrollBars      =   3
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20135
         _ExtentY        =   8017
         _StockProps     =   79
      End
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   330
      Left            =   6840
      Top             =   720
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      _Version        =   393216
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   840
      Width           =   4935
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   6435
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Punto de Archivo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   480
      Width           =   2055
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Enabled         =   0   'False
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Enabled         =   0   'False
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Enabled         =   0   'False
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Enabled         =   0   'False
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmCtaBajas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim indcoleccion As Integer
Dim PUNTOUBI As String
Dim WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1
Dim objMotivo_Baja As New clsCWForm
Dim puntosubicacion As New Collection
Dim cambio  As Boolean
Sub enable_disable()
  'deshabilitar nuevo
  tlbToolbar1.Buttons(2).Enabled = False
  mnuDatosOpcion(10).Enabled = False
  'deshabilitar eliminar
  tlbToolbar1.Buttons(8).Enabled = False
  mnuDatosOpcion(60).Enabled = False
  'des/habilitar imprimir
  If grdDBGrid1(0).Rows = 0 Then
    tlbToolbar1.Buttons(6).Enabled = False
    mnuDatosOpcion(80).Enabled = False
  Else
    tlbToolbar1.Buttons(6).Enabled = True
    mnuDatosOpcion(80).Enabled = True
  End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
 
  Dim strKey   As String
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objWin = New clsCWWin
  
  Call objWin.WinCreateInfo(cwModeMultiLineEdit, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  
  With objWin.objDoc
    .cwPRJ = "CWSpy"
    .cwEVT = ""
  End With
  
  With objMotivo_Baja
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AR0400"
    PUNTOUBI = -1
    '.strWhere = "( AR02CODPTOARCHIVO = '" & PUNTOUBI & "' AND AR06CODESTADO = 'B' AND AR04FECFIN IS NULL)"
    .strWhere = "( AR06CODESTADO = 'B' AND AR04FECINICIO >= TO_DATE('13/03/2000','DD/MM/YYYY') AND AR02CODPTOARCHIVO = '" & PUNTOUBI & "' AND AR04FECFIN IS NULL)"
    Call .FormAddOrderField("AR04FECINICIO", cwAscending)
    .intCursorSize = -1
    .strName = "Bajas"
  End With
  With objWin
    
    Call .FormAddInfo(objMotivo_Baja, cwFormMultiLine)
    Call .GridAddColumn(objMotivo_Baja, "Fecha de Baja", "AR04FECINICIO")
    Call .GridAddColumn(objMotivo_Baja, "N�mero H.C.", "CI22NUMHISTORIA")
    Call .GridAddColumn(objMotivo_Baja, "CODTIPO", "AR00CODTIPOLOGIA")
    Call .GridAddColumn(objMotivo_Baja, "Tipolog�a", "")
    Call .GridAddColumn(objMotivo_Baja, "CODPAC", "CI22NUMHISTORIA")
    Call .GridAddColumn(objMotivo_Baja, "Paciente", "")
    Call .GridAddColumn(objMotivo_Baja, "Motivo de Baja", "AR04OBSERVACIO")
    
    Call .FormCreateInfo(objMotivo_Baja)
    grdDBGrid1(0).Columns("N�mero H.C.").Locked = True
    grdDBGrid1(0).Columns("CODTIPO").Locked = True
    grdDBGrid1(0).Columns("Tipolog�a").Locked = True
    grdDBGrid1(0).Columns("CODTIPO").Visible = False
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).CODTIPO")), "AR00CODTIPOLOGIA", "SELECT AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).CODTIPO")), grdDBGrid1(0).Columns("grddbgrid1(0).Tipolog�a"), "AR00DESTIPOLOGIA")
    ' Paciente
    grdDBGrid1(0).Columns("CODPAC").Locked = True
    grdDBGrid1(0).Columns("Paciente").Locked = True
    grdDBGrid1(0).Columns("CODPAC").Visible = False
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).CODPAC")), "CI22NUMHISTORIA", "SELECT CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL AS CODPAC FROM CI2200 WHERE CI22NUMHISTORIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).CODPAC")), grdDBGrid1(0).Columns("grddbgrid1(0).Paciente"), "CODPAC")
    
    
    'Tama�os
    grdDBGrid1(0).Columns("Fecha de Baja").Alignment = ssCaptionAlignmentCenter
    grdDBGrid1(0).Columns("Fecha de Baja").Width = 1850
    grdDBGrid1(0).Columns("N�mero H.C.").Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns("N�mero H.C.").Width = 1100
    grdDBGrid1(0).Columns("Tipolog�a").Width = 1300
    grdDBGrid1(0).Columns("Paciente").Width = 3300
    grdDBGrid1(0).Columns("Motivo de Baja").Width = 3300
    
    Call .WinRegister
    Call .WinStabilize
  End With
  Call cargaCombo
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWin.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWin.WinProcess(cwProcessKeys, intKeyCode, intShift)
  If intKeyCode = vbKeyF1 Then
    Call objApp.HelpContext
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  intCancel = objWin.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWin.WinDeRegister
  Call objWin.WinRemoveInfo
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWin_cwPrint(ByVal strFormName As String)
  Dim strWhere  As String
  Dim strOrder  As String
  
  If strFormName = "Bajas" Then
    With objWin.FormPrinterDialog(True, "")
      If .Selected > 0 Then
        strWhere = objWin.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
          strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), "WHERE ", " AND ")
          strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = "ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWin.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_ButtonClick(ByVal btnButton As Button)
  If btnButton.Index = 6 Then 'imprimir
    Call mnuDatosOpcion_Click(80)
  Else
    Call objWin.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  If btnButton.Index <> 30 Then  'no salir
    Call enable_disable
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sql As String
  Dim camino As String
  If intIndex = 80 Then
    objWin.DataRefresh
    sql = "SELECT H.AR04FECINICIO, "
    sql = sql & "H.CI22NUMHISTORIA, T.AR00DESTIPOLOGIA, "
    sql = sql & "H.AR04OBSERVACIO , P.AR02DESPTOARCHIVO "
    sql = sql & "FROM CI2200 C, AR0000 T, AR0200 P, AR0400 H "
    sql = sql & "WHERE H.AR02CODPTOARCHIVO = '" & PUNTOUBI & "' AND "
    sql = sql & "H.AR06CODESTADO = 'B' AND "
    sql = sql & "H.AR04FECFIN IS NULL AND "
    sql = sql & "T.AR00CODTIPOLOGIA = H.AR00CODTIPOLOGIA AND "
    sql = sql & "P.AR02CODPTOARCHIVO = H.AR02CODPTOARCHIVO AND "
    sql = sql & "C.CI22NUMHISTORIA(+) = H.CI22NUMHISTORIA "
    sql = sql & " ORDER BY H.AR04FECINICIO"
    camino = Trim(App.Path)
    If Right(camino, 1) = "\" Then
      camino = Left(camino, Len(camino) - 1)
    End If
    CrystalReport1.ReportFileName = camino & PATH_REPORTS & "\CTABAJA.RPT"
    CrystalReport1.Connect = objApp.rdoConnect.Connect
    CrystalReport1.SQLQuery = sql
    CrystalReport1.Action = 1
    Call enable_disable
  Else
    Call objWin.WinProcess(cwProcessData, intIndex, 0)
  End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessRegister, intIndex, 0)
  Call enable_disable
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessOptions, intIndex, 0)
  Call enable_disable
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWin.CtrlGotFocus
  Call enable_disable
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWin.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWin.GridChangeRowCol(vntLastRow, intLastCol)
  Call enable_disable
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWin.CtrlDataChange
  Call enable_disable
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWin.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub Combo1_Click()
 Dim indcoleccion As Integer
 Dim a As String
    
If Combo1.ListIndex > -1 Then
    Me.MousePointer = vbHourglass
    indcoleccion = Combo1.ItemData(Combo1.ListIndex)
    PUNTOUBI = puntosubicacion.Item(indcoleccion)
    objMotivo_Baja.strWhere = "( AR06CODESTADO = 'B' AND AR04FECINICIO >= TO_DATE('13/03/2000','DD/MM/YYYY') AND AR02CODPTOARCHIVO = " & "'" & PUNTOUBI & "'" & " AND AR04FECFIN IS NULL)"
    Call objWin.DataRefresh
    Me.MousePointer = vbDefault
    Call enable_disable
End If
End Sub


Public Sub cargaCombo()

Dim i As Integer
Dim SQLpuntos
  
MSRDC1.Connect = objApp.rdoConnect.Connect
SQLpuntos = "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO"
MSRDC1.sql = SQLpuntos
MSRDC1.Refresh
If MSRDC1.Resultset.RowCount > 0 Then
    MSRDC1.Resultset.MoveFirst
    Combo1.Clear
     i = 0
     Set puntosubicacion = New Collection
     While Not MSRDC1.Resultset.EOF
        Combo1.AddItem (MSRDC1.Resultset("AR02DESPTOARCHIVO"))
        Combo1.ItemData(i) = i + 1
        puntosubicacion.Add (MSRDC1.Resultset("AR02CODPTOARCHIVO"))
        i = i + 1
        MSRDC1.Resultset.MoveNext
    Wend
End If
End Sub
