VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmInicial 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gesti�n de Archivo"
   ClientHeight    =   3690
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11250
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3690
   ScaleWidth      =   11250
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11250
      _ExtentX        =   19844
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlIconos"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   2
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            ImageKey        =   "salir"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Ubicaci�n Palm Pilot"
      Height          =   375
      Index           =   20
      Left            =   8520
      TabIndex        =   21
      Top             =   3120
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Revisi�n HHCC"
      Height          =   375
      Index           =   19
      Left            =   5760
      TabIndex        =   20
      Top             =   3120
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Tipos de Entrega"
      Height          =   375
      Index           =   18
      Left            =   240
      TabIndex        =   19
      Top             =   3120
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Citas"
      Height          =   375
      Index           =   17
      Left            =   3000
      TabIndex        =   18
      Top             =   3120
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Etiquetado de Ptos. Ubicacion"
      Height          =   375
      Index           =   16
      Left            =   8520
      TabIndex        =   17
      Top             =   1080
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Etiquetado de HHCC"
      Height          =   375
      Index           =   15
      Left            =   8520
      TabIndex        =   16
      Top             =   600
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Mto de HHCC"
      Height          =   375
      Index           =   14
      Left            =   240
      TabIndex        =   15
      Top             =   2040
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Mto de Ptos Archivo"
      Height          =   375
      Index           =   13
      Left            =   240
      TabIndex        =   14
      Top             =   1560
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Cta de Procesos de HHCC"
      Height          =   375
      Index           =   12
      Left            =   5760
      TabIndex        =   13
      Top             =   2040
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Cta de Mtos en Ptos. Ubicacion"
      Height          =   375
      Index           =   11
      Left            =   5760
      TabIndex        =   12
      Top             =   1560
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Cta de HHCC  Prestadas"
      Height          =   375
      Index           =   10
      Left            =   5760
      TabIndex        =   11
      Top             =   1080
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Cta de Traslados"
      Height          =   375
      Index           =   9
      Left            =   5760
      TabIndex        =   10
      Top             =   600
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Solicitudes"
      Height          =   375
      Index           =   8
      Left            =   3000
      TabIndex        =   9
      Top             =   2520
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Cta de Bajas"
      Height          =   375
      Index           =   7
      Left            =   5760
      TabIndex        =   8
      Top             =   2520
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Baja de HHCC"
      Height          =   375
      Index           =   6
      Left            =   3000
      TabIndex        =   7
      Top             =   600
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Solicitudes para Investigaci�n"
      Height          =   375
      Index           =   5
      Left            =   240
      TabIndex        =   6
      Top             =   2520
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Pr�stamos"
      Height          =   375
      Index           =   4
      Left            =   3000
      TabIndex        =   5
      Top             =   2040
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Solicitud de Pr�stamo"
      Height          =   375
      Index           =   3
      Left            =   3000
      TabIndex        =   4
      Top             =   1560
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Mto de Motivos de Solicitud"
      Height          =   375
      Index           =   2
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Mto de Tipolog�as"
      Height          =   375
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   2535
   End
   Begin VB.CommandButton cmdBoton 
      Caption         =   "Ubicaci�n de HHCC"
      Height          =   375
      Index           =   0
      Left            =   3000
      TabIndex        =   0
      Top             =   1080
      Width           =   2535
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   120
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin ComctlLib.ImageList imlIconos 
      Left            =   720
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   2
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Inicial.frx":0000
            Key             =   "ejecutar"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Inicial.frx":031A
            Key             =   "salir"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmInicial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Sub salir()
  End
End Sub

Private Sub cmdBoton_Click(Index As Integer)
4 Select Case Index
  Case 0
    frmUbicacionHHCC.Show vbModal
    Set frmUbicacionHHCC = Nothing
  Case 1
    frmTipologias.Show vbModal
    Set frmTipologias = Nothing
  Case 2
    frmMotivosSolicitud.Show vbModal
    Set frmMotivosSolicitud = Nothing
  Case 3
    frmSolicitudInvestiga.Show vbModal
    Set frmSolicitudInvestiga = Nothing
  Case 4
    frmPrestamos.Show vbModal
    Set frmPrestamos = Nothing
  Case 5
  frmSolicitudInvestiga.Show vbModal
  Set frmSolicitudInvestiga = Nothing
  Case 6
    frmBajaHHCC.Show vbModal
    Set frmBajaHHCC = Nothing
  Case 7
    frmCtaBajas.Show vbModal
    Set frmCtaBajas = Nothing
  Case 8
    frmSolicitud.Show vbModal
    Set frmSolicitud = Nothing
  Case 9
    dlgSelCtaTraslados.Show vbModal
    Set dlgSelCtaTraslados = Nothing
  Case 10
    dlgSelCtaPendientesDevol.Show vbModal
    Set dlgSelCtaPendientesDevol = Nothing
  Case 11
    dlgSelCtaPtosUbicacion.Show vbModal
    Set dlgSelCtaPtosUbicacion = Nothing
  Case 12
    dlgSelCtaProcesos.Show vbModal
    Set dlgSelCtaProcesos = Nothing
  Case 13
    frmPuntosArchivo.Show vbModal
    Set frmPuntosArchivo = Nothing
  Case 14
    frmProcesosHHCC.Show vbModal
    Set frmProcesosHHCC = Nothing
  Case 15
    dlgSelEtiquetadoHHCC.Show vbModal
    Set dlgSelEtiquetadoHHCC = Nothing
  Case 16
    dlgSelEtiquetadoPtosUbi.Show vbModal
    Set dlgSelEtiquetadoPtosUbi = Nothing
  Case 17
    frmCitas.Show vbModal
    Set frmCitas = Nothing
  Case 18
    frmTiposEntrega.Show vbModal
    Set frmTiposEntrega = Nothing
End Select
End Sub

Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  Dim strEntorno As String
  Dim res%
  Dim strDir As String
  On Error Resume Next
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    Set .imlImageList = imlImageList1
    Set .frmFormMain = Me
    .strUserName = "CUN"
    .strPassword = "TUY"
    .strDataSource = "Oracle73"
    .intUserEnv = 1
    .blnAskPassword = False
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
    .blnUseRegistry = True
    .blnReg = False
  End With
  
  'strEntorno1 = "Entorno de Desarrollo"
  'strEntorno2 = "Entorno de Explotaci�n"
  
    strEntorno = "Entorno de Explotaci�n"
    Call objEnv.AddEnv(strEntorno)
    'Call objEnv.AddValue(strEntorno, "DataBase", "")
    Call objEnv.AddValue(strEntorno, "DataBase", "")
  
  'With objEnv
    'Call .AddEnv(strEntorno1)
    'Call .AddValue(strEntorno1, "Main", "")
    'Call .AddEnv(strEntorno2)
    'Call .AddValue(strEntorno2, "Main", "")
  'End With
    
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If
    
 objSecurity.strUser = "PRUEBA"
 objSecurity.RegSession
    
End Sub


Private Sub mnuAyuda_Click()

End Sub


Private Sub mnuSalir_Click()
  salir
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
  Select Case Button.Index
    Case 2
      salir
  End Select
End Sub
