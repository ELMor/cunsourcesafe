VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSolicitud 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Solicitud de Pr�stamo"
   ClientHeight    =   5085
   ClientLeft      =   2850
   ClientTop       =   1080
   ClientWidth     =   11910
   HelpContextID   =   21
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5085
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Height          =   2295
      Left            =   9660
      TabIndex        =   16
      Top             =   2580
      Width           =   2115
      Begin VB.CommandButton Command2 
         Caption         =   "Sa&lir"
         Height          =   375
         Left            =   300
         TabIndex        =   7
         Top             =   840
         Width           =   1575
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Solicitar"
         Height          =   375
         Left            =   300
         TabIndex        =   6
         Top             =   300
         Width           =   1575
      End
      Begin VB.CommandButton cmdAyuda 
         Caption         =   "&Ayuda"
         Height          =   375
         HelpContextID   =   21
         Left            =   300
         TabIndex        =   8
         Top             =   1680
         Width           =   1575
      End
   End
   Begin VB.ComboBox cboDpto 
      BackColor       =   &H00FFFF00&
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   2760
      Width           =   4575
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   1
      Left            =   120
      MaxLength       =   50
      TabIndex        =   5
      Top             =   4560
      Width           =   8475
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFF00&
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Text            =   "Combo1"
      Top             =   3360
      Width           =   4575
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFF00&
      Enabled         =   0   'False
      Height          =   315
      Index           =   1
      ItemData        =   "AR1021.frx":0000
      Left            =   120
      List            =   "AR1021.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   3960
      Width           =   4575
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H8000000F&
      Height          =   285
      Index           =   0
      Left            =   120
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   0
      Top             =   300
      Width           =   1150
   End
   Begin VB.CommandButton cmdMarcar 
      Height          =   375
      Left            =   180
      Picture         =   "AR1021.frx":0004
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   900
      Width           =   375
   End
   Begin MSFlexGridLib.MSFlexGrid msgTipologias 
      Height          =   1035
      Left            =   180
      TabIndex        =   9
      Top             =   1320
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1826
      _Version        =   65541
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selecci�n de Tipolog�as"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Left            =   60
      TabIndex        =   15
      Top             =   660
      Width           =   11715
   End
   Begin VB.Label lblDpto 
      AutoSize        =   -1  'True
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   14
      Top             =   2520
      Width           =   1200
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Observaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   13
      Top             =   4320
      Width           =   1275
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Destinatario"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   12
      Top             =   3120
      Width           =   1035
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Motivo de Solicitud"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   3720
      Width           =   1650
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "N�mero H.C."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   10
      Top             =   60
      Width           =   1095
   End
End
Attribute VB_Name = "frmSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public usuario              As String
Public departamento         As String
Dim lngHc                   As Long
Dim rdomotivo               As rdoResultset
Dim sql                     As String

Dim colecusuario            As New Collection
Dim colecmotivo             As New Collection
Dim coleccodtipologia       As New Collection
Dim colectipologia          As New Collection
Dim QyConsulta              As rdoQuery
Dim QyInsert                As rdoQuery
Dim rsdepartamento          As rdoResultset
Dim rsdatos                 As rdoResultset
Dim rsmedicos               As rdoResultset
Dim rsTipologias            As rdoResultset
Dim rsmotivo                As rdoResultset
Dim rsfecha                 As rdoResultset
Dim rsDptoUsu               As rdoResultset
Dim i                       As Integer
Dim j                       As Integer
Dim salir                   As Boolean
Dim pos                     As Integer

Dim gblcontrol              As Boolean
Dim men_aceptar             As String

Dim responsable As String
Dim blnSolicitada As Boolean

Dim colDpto                 As New Collection 'contiene los codigos de los departamentos
Const correcto = "C"   'INDICA QUE LA TIPOLOG�A ES CORRECTA
Const CROJO = &HFF&
Const SOLICITAR = "S�"

Private Sub cboDpto_Click()
If cboDpto.ListIndex <> -1 Then
    'des/habilitar combo motivo solicitud
    departamento = colDpto(cboDpto.ListIndex + 1)
    If departamento = DPTO_URGENCIAS Then
        lblLabel1(1).Enabled = False
        Combo1(1).ListIndex = -1
        Combo1(1).Enabled = False
    Else
        lblLabel1(1).Enabled = True
        Combo1(1).Enabled = True
    End If
    'Carga del combo de Usuarios y correspondiente colecci�n de codigos
    sql = "SELECT U.SG02APE1, U.SG02APE2, U.SG02NOM, U.SG02COD "
    sql = sql & " FROM SG0200 U, AD0300 D "
    sql = sql & " Where D.AD02CODDPTO = ?" ' rsdepartamento("AD02CODDPTO")
    sql = sql & " AND  D.AD03FECFIN IS NULL "
    sql = sql & " AND  D.AD31CODPUESTO <> 21 "
    sql = sql & " AND D.SG02COD <> '" & usuario & "' "
    sql = sql & " AND  U.SG02COD = D.SG02COD"
    
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = departamento
    Set rsmedicos = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
    If rsmedicos.RowCount Then
         Combo1(0).Enabled = True
         lblLabel1(0).Enabled = True
         i = 1

        While Not rsmedicos.EOF
            responsable = rsmedicos("SG02APE1") & " "
            responsable = responsable & rsmedicos("SG02APE2") & ", "
            responsable = responsable & rsmedicos("SG02NOM")
           Combo1(0).AddItem responsable
            Combo1(0).ItemData(i) = i + 1
            colecusuario.Add (rsmedicos("SG02COD"))
            i = i + 1
            rsmedicos.MoveNext
        Wend
        Combo1(0).ListIndex = 0
        rsmedicos.Close
    End If
End If
End Sub

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 21)
End Sub

Private Sub cmdMarcar_Click()
msgTipologias.col = 0
With msgTipologias
    If Trim(.TextMatrix(.row, 0)) = correcto Then
        If Trim(.TextMatrix(.row, 1)) = "" Then
            .TextMatrix(.row, 1) = SOLICITAR
            coleccodtipologia.Add .TextMatrix(.row, 2), CStr(.row)
            colectipologia.Add .TextMatrix(.row, 3), CStr(.row)
        Else
            .TextMatrix(.row, 1) = ""
            coleccodtipologia.Remove CStr(.row)
            colectipologia.Remove CStr(.row)
        End If
  ElseIf .TextMatrix(.row, 0) <> correcto And .TextMatrix(.row, 1) <> SOLICITAR Then
    Call MsgBox("La Tipolog�a " & .TextMatrix(.row, 3) & " no puede ser solicitada", vbOKOnly, "Marcar Tipolog�a")
  End If
End With
Call msgTipologias_Click
End Sub

Private Sub Combo1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 0 Then
   Combo1(0).Text = ""
End If
End Sub

Private Sub Command1_Click()
Dim sqlfecha As String
Dim mensa As String
Dim faltandatos As Boolean
Dim resp As Integer
Dim i As Integer
Dim r As rdoResultset
faltandatos = False
mensa = ""


    If Combo1(0).Text = "" Then
        mensa = mensa & Chr(13) & "El campo Destinatario es Obligatorio"
        faltandatos = True
    End If

If departamento = "" Then
        mensa = mensa & Chr(13) & "El campo Departamento es Obligatorio"
        faltandatos = True
ElseIf departamento <> DPTO_URGENCIAS Then
        If Combo1(1).Text = "" Then
            mensa = mensa & Chr(13) & "El campo Motivo de Solicitud es Obligatorio"
            faltandatos = True
        End If
End If

If colectipologia.Count <= 0 Then
   mensa = mensa & Chr(13) & "Debe seleccionar la Tipologia/s que desea solicitar al Archivo"
   faltandatos = True
Else
    men_aceptar = ""
    For i = 1 To colectipologia.Count
        men_aceptar = men_aceptar & Chr(13) & colectipologia(i)
    Next
   If colectipologia.Count = 1 Then
    men_aceptar = "�Esta seguro que desea Solicitar la Historia Cl�nica Tipolog�a " & men_aceptar
   Else
    men_aceptar = "�Esta seguro que desea Solicitar la Historia Clinica Tipolog�as " & men_aceptar
   End If
End If
If faltandatos = True Then
   Call MsgBox(mensa, vbInformation + vbOKOnly, "Confirmar Solicitud")
   Exit Sub
End If
''''''''''''''''''''''''''''''''''''
resp = MsgBox(men_aceptar & "?", vbYesNo + vbInformation, "Confirmar Solicitud")
If resp = vbNo Then
    Exit Sub
End If

'''''''''''''''''''''''''''''
   sqlfecha = " Select to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS') from dual"
   Set rsfecha = objApp.rdoConnect.OpenResultset(sqlfecha, rdOpenKeyset, rdConcurValues)
   
For i = 1 To coleccodtipologia.Count
        'obtenemos el cod pto archivo de la hh-tipologia actual
        sql = "SELECT AR02CODPTOARCHIVO FROM AR0400 "
        sql = sql & "WHERE CI22NUMHISTORIA = ? "
        sql = sql & "AND AR00CODTIPOLOGIA = ? "
        sql = sql & "AND AR04FECFIN IS NULL"
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = Text1(0).Text
        QyConsulta(1) = coleccodtipologia(i)
        Set r = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        'insertamos el pedido
        sql = "INSERT INTO AR0500 ( CI22NUMHISTORIA, AR00CODTIPOLOGIA, "
        sql = sql & " AR05FECSOLICITUD, "
        sql = sql & " AD02CODDPTO, AR01CODMOTIVOSOL, "
        sql = sql & " SG02COD, AR05OBSERVACIO, AR02CODPTOARCHIVO) "
        sql = sql & " VALUES (?, ?, to_date(?,'dd/mm/yyyy hh24:mi:ss'), ?, ?, ?, ?, ?)"
        Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
        QyInsert(0) = Text1(0).Text
        QyInsert(1) = coleccodtipologia(i)
        QyInsert(2) = CStr(rsfecha(0))
        QyInsert(3) = departamento
        If departamento = DPTO_URGENCIAS Then
            QyInsert(4) = "U"
           ' QyInsert(6) = Null
            QyInsert(6) = Text1(1).Text
        Else
            QyInsert(4) = colecmotivo.Item(Combo1(1).ItemData(Combo1(1).ListIndex))
            QyInsert(6) = Text1(1).Text
        End If
        
        QyInsert(5) = colecusuario.Item(Combo1(0).ItemData(Combo1(0).ListIndex))
        QyInsert(7) = r(0)
        r.Close
        QyInsert.Execute
        QyInsert.Close
   
        'If Err > 0 Then Exit For
   
Next
rsfecha.Close
'
'If Err > 0 Then
'    objApp.rdoConnect.RollBackTrans
'    MsgBox "Rollback"
'End If
blnSolicitada = True
Unload Me

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim responsable As String
Dim i As Integer

Set coleccodtipologia = New Collection
Set colectipologia = New Collection
Set colecusuario = New Collection

Text1(0).Locked = True
usuario = objSecurity.strUser
lngHc = glblngNumHC
Text1(0).Text = glblngNumHC
blnSolicitada = False
' Consulto el departamento del usuario
Call cargaCombo
'''''''''''''''''''
   'cargar dptos a los que pertenece el usuario conectado
   sql = " SELECT  D.AD02CODDPTO, D.AD02DESDPTO "
   sql = sql & " From AD0300 U, AD0200 D "
   sql = sql & " Where SG02COD = ?"
   sql = sql & " AND AD03FECFIN IS NULL "
   sql = sql & " AND U.AD02CODDPTO = D.AD02CODDPTO"
   Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
   QyConsulta(0) = usuario
   Set rsdepartamento = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
   Do While Not rsdepartamento.EOF
     colDpto.Add Trim(rsdepartamento("AD02CODDPTO"))
     cboDpto.AddItem Trim(rsdepartamento("AD02DESDPTO"))
     rsdepartamento.MoveNext
   Loop
   rsdepartamento.Close

  ' Consulto cuales son ape1, ape2, nom del usuario

sql = " Select SG02APE1, SG02APE2, SG02NOM ,SG02COD "
sql = sql & " FROM SG0200 "
sql = sql & " WHERE SG02COD = ?"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = usuario
Set rsdatos = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
i = 0
responsable = rsdatos("SG02APE1") & " "
responsable = responsable & rsdatos("SG02APE2") & ", "
responsable = responsable & rsdatos("SG02NOM")
Combo1(0).AddItem (responsable)
Combo1(0).ItemData(i) = i + 1
colecusuario.Add (rsdatos("SG02COD"))
Combo1(0).ListIndex = i
Combo1(0).Enabled = False
lblLabel1(0).Enabled = False
rsdatos.Close
End Sub

Public Sub cargaCombo()
Dim i As Integer
'Dim responsable As String
Dim SQLTipologias As String
Dim SQLmotivos  As String
Dim SQLdptoUsuario As String
Dim strCodTipologia As String
Dim strDesTipologia As String
Dim blnPsiquiatria As Boolean


'determino si el usuario es de psiquiatria
SQLdptoUsuario = "SELECT COUNT(*) FROM AD0300 WHERE SG02COD = ? "
SQLdptoUsuario = SQLdptoUsuario & " AND AD02CODDPTO = ? "
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLdptoUsuario)
    QyConsulta(0) = objSecurity.strUser
    QyConsulta(1) = DPTO_PSIQUIATRIA
Set rsDptoUsu = QyConsulta.OpenResultset()
If rsDptoUsu(0) = 0 Then blnPsiquiatria = False Else blnPsiquiatria = True
rsDptoUsu.Close
QyConsulta.Close
    '           CON COLECCION
    'Carga de las Tipologias
    'Contruir la Grid
    With msgTipologias
    .Clear
    .Redraw = False
    .Rows = 1
    .cols = 7
    .row = 0
    .col = 0
    .ColWidth(0) = 0 'Indicador de correcto o no correcto de la tipolog�a
    .col = 1
    .ColWidth(1) = 800
    .CellFontBold = True
    .CellAlignment = 4
    .TextMatrix(0, 1) = "Solicitar"
    .col = 2
    .ColWidth(2) = 0 'Codigo de la tipolog�a
    .col = 3
    .ColWidth(3) = "1000"
    .CellFontBold = True
    .TextMatrix(0, 3) = "Tipolog�a"
    .col = 4
    .ColWidth(4) = "6000"
    .CellFontBold = True
    .TextMatrix(0, 4) = "Situaci�n"
    .col = 5
    .ColWidth(5) = "2000"
    .CellFontBold = True
    .TextMatrix(0, 5) = "Fecha Situaci�n"
    .col = 6
    .ColWidth(6) = "2500"
    .CellFontBold = True
    .TextMatrix(0, 6) = "Responsable"
    End With
'EFS: MODIFICADO PARA QUE LA TIPOLOGIA 04 'PSIQUIATRIA' SOLO APAREZCA CUANDO
'EL USUARIO ES DEL DPTO DE  PSIQUIATRIA
    SQLTipologias = "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLTipologias)
    Set rsTipologias = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rsTipologias.RowCount Then
        rsTipologias.MoveFirst
        i = 1
        
        While Not rsTipologias.EOF
            With msgTipologias
              strCodTipologia = rsTipologias("ar00codtipologia")
              strDesTipologia = rsTipologias("ar00destipologia")
              If strCodTipologia = TIPOLOGIA_PSIQUIATRIA Then
               If blnPsiquiatria Then
                .Rows = .Rows + 1
                .row = i
                .TextMatrix(i, 2) = strCodTipologia
                .TextMatrix(i, 3) = strDesTipologia
                Call Cargar_Estados(strCodTipologia, strDesTipologia)
                i = i + 1
               End If
              Else
                .Rows = .Rows + 1
                .row = i
                .TextMatrix(i, 2) = strCodTipologia
                .TextMatrix(i, 3) = strDesTipologia
                Call Cargar_Estados(strCodTipologia, strDesTipologia)
                i = i + 1
              End If
            End With
            rsTipologias.MoveNext
        Wend
        rsTipologias.Close
        msgTipologias.col = 0
        With msgTipologias
        For i = 1 To .Rows - 1
        .row = i
            If .TextMatrix(i, 0) <> correcto Then
                .col = 0
                .CellForeColor = CROJO
                .col = 1
                .CellForeColor = CROJO
                .col = 2
                .CellForeColor = CROJO
                .col = 3
                .CellForeColor = CROJO
                .col = 4
                .CellForeColor = CROJO
                .col = 5
                .CellForeColor = CROJO
                .col = 6
                .CellForeColor = CROJO
                .col = 0
            End If
          Next
        End With
    Else
      Call msgTipologias_SelChange
  End If
  msgTipologias.Redraw = True


' Carga de la lista de Motivos
    
    
    SQLmotivos = "SELECT AR01CODMOTIVOSOL, AR01DESMOTIVOSOL "
    SQLmotivos = SQLmotivos & " FROM AR0100  "
    SQLmotivos = SQLmotivos & " WHERE AR01CODMOTIVOSOL NOT IN ('C','U','I') AND AR01FECFIN IS NULL "
    SQLmotivos = SQLmotivos & " ORDER BY AR01DESMOTIVOSOL"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLmotivos)
    Set rsmotivo = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rsmotivo.RowCount Then
        rsmotivo.MoveFirst
        Combo1(1).Clear
        i = 0
        Set colecmotivo = New Collection
        While Not rsmotivo.EOF
        Combo1(1).AddItem (rsmotivo("AR01DESMOTIVOSOL"))
        Combo1(1).ItemData(i) = i + 1
        colecmotivo.Add (rsmotivo("AR01CODMOTIVOSOL"))
        rsmotivo.MoveNext
        i = i + 1
    Wend
    rsmotivo.Close
End If
 End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim blnSolicitar As Boolean
Dim i As Integer
Dim vntResp As Variant
If blnSolicitada = False Then
With msgTipologias
    .Redraw = False
    blnSolicitar = False
    For i = 1 To .Rows - 1
        .row = i
        If .TextMatrix(i, 1) = SOLICITAR Then
            blnSolicitar = True
            Exit For
        End If
    Next
    .col = 0
    .Redraw = True
End With
If blnSolicitar = True Then
    vntResp = MsgBox("�Desea salir sin solicitar la Historia Cl�nica " & Text1(0).Text & "?", vbYesNo + vbQuestion, "Salir")
    If vntResp = vbYes Then
        Cancel = 0
    Else
        Cancel = 1
    End If
End If
Else
  Cancel = 0
End If
End Sub

Private Sub msgTipologias_Click()
With msgTipologias
  .col = 0
  .rowsel = .row
  .ColSel = .cols - 1
End With
End Sub
Private Sub Cargar_Estados(strCodTip As String, strDesTip As String)
Dim sql  As String
Dim rshctipologia As rdoResultset
Dim rs As rdoResultset
Dim estado As String
Dim tiposel As String
Dim tiposeldes As String

tiposel = strCodTip
tiposeldes = strDesTip


    sql = "SELECT AR06CODESTADO,TO_CHAR(AR04FECINICIO,'DD/MM/YYYY HH24:MI:SS'), AD02CODDPTO, AR02CODPTOARCHIVO "
    sql = sql & " From AR0400 "
    sql = sql & " Where CI22NUMHISTORIA = ? "
    sql = sql & " AND AR00CODTIPOLOGIA = ? "
    sql = sql & " AND AR04FECFIN IS NULL "
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = Text1(0).Text
    QyConsulta(1) = tiposel
    Set rshctipologia = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
    If rshctipologia.RowCount Then
    
       ' La Historia esta en ALTA
       
       If rshctipologia("AR06CODESTADO") = "A" Then
        sql = " SELECT D.AD02DESDPTO, TO_CHAR(M.AR07FECMVTO,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
        sql = sql & " From AD0200 D, SG0200 U, AR0700 M "
        sql = sql & " Where M.CI22NUMHISTORIA = ? "
        sql = sql & " AND M.AR00CODTIPOLOGIA = ? " 'to_date(?,'dd/mm/yy hh24:mi:ss')
        sql = sql & " AND to_char(M.AR04FECINICIO,'dd/mm/yyyy hh24:mi:ss') = ?"
        sql = sql & " AND M.AR07FECFINMVTO IS NULL "
        sql = sql & " AND D.AD02CODDPTO = M.AD02CODDPTO"
        sql = sql & " AND U.SG02COD = M.SG02COD "
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = Text1(0).Text
        QyConsulta(1) = tiposel
        QyConsulta(2) = rshctipologia(1)
        Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rs.RowCount >= 1 Then
          msgTipologias.TextMatrix(msgTipologias.row, 4) = "En el Departamento " & rs(0)
          msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(1)
          msgTipologias.TextMatrix(msgTipologias.row, 6) = rs(2) & " " & rs(3) & ", " & rs(4)
        Else
          msgTipologias.TextMatrix(msgTipologias.row, 4) = "En Alta, todav�a no ha llegado al Archivo"
        End If
        rs.Close
            
             
'         La Historia esta DEVUELTA
       
      ElseIf rshctipologia("AR06CODESTADO") = "D" Then
        sql = " SELECT D.AD02DESDPTO, TO_CHAR(M.AR07FECMVTO,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
        sql = sql & " From AD0200 D, SG0200 U, AR0700 M "
        sql = sql & " Where M.CI22NUMHISTORIA = ? "
        sql = sql & " AND M.AR00CODTIPOLOGIA = ? " 'to_date(?,'dd/mm/yy hh24:mi:ss')
        sql = sql & " AND to_char(M.AR04FECINICIO,'dd/mm/yyyy hh24:mi:ss') = ?"
        sql = sql & " AND M.AR07FECFINMVTO IS NULL "
        sql = sql & " AND D.AD02CODDPTO = M.AD02CODDPTO"
        sql = sql & " AND U.SG02COD = M.SG02COD "
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = Text1(0).Text
        QyConsulta(1) = tiposel
        QyConsulta(2) = rshctipologia(1)
        Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rs.RowCount >= 1 Then
          msgTipologias.TextMatrix(msgTipologias.row, 4) = "En el Departamento " & rs(0)
          msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(1)
          msgTipologias.TextMatrix(msgTipologias.row, 6) = rs(2) & " " & rs(3) & ", " & rs(4)
          rs.Close
        Else
          rs.Close
          sql = " SELECT AD02DESDPTO "
          sql = sql & " From AD0200"
          sql = sql & " Where AD02CODDPTO = ? "
          Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
          QyConsulta(0) = rshctipologia("AD02CODDPTO")
          Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          msgTipologias.TextMatrix(msgTipologias.row, 4) = "Devuelta al Departamento " & rs(0)
          msgTipologias.TextMatrix(msgTipologias.row, 5) = rshctipologia(1)
          rs.Close
        End If
       
       ' La Historia esta TRASLADADA
       
       ElseIf rshctipologia("AR06CODESTADO") = "T" Then
       
            sql = " SELECT AR02DESPTOARCHIVO "
            sql = sql & " From AR0200 "
            sql = sql & " Where AR02CODPTOARCHIVO = ? "
            Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
                                          
            QyConsulta(0) = rshctipologia("AR02CODPTOARCHIVO")
            Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            msgTipologias.TextMatrix(msgTipologias.row, 4) = "Trasladada al Archivo " & rs(0)
            msgTipologias.TextMatrix(msgTipologias.row, 5) = rshctipologia(1)
            rs.Close
            
       ' La Historia esta ENTREGADA
       
       ElseIf rshctipologia("AR06CODESTADO") = "E" Then
            msgTipologias.TextMatrix(msgTipologias.row, 4) = "Entregada al Paciente"
            msgTipologias.TextMatrix(msgTipologias.row, 5) = rshctipologia(1)
       
       ' La Historia esta PRESTADA
       ElseIf rshctipologia("AR06CODESTADO") = "P" Then
       
            sql = " SELECT D.AD02DESDPTO, TO_CHAR(M.AR07FECMVTO,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
            sql = sql & " From AD0200 D, AR0700 M, SG0200 U"
            sql = sql & " Where M.CI22NUMHISTORIA = ? "
            sql = sql & " AND M.AR00CODTIPOLOGIA = ? " 'to_date(?,'dd/mm/yy hh24:mi:ss')
            sql = sql & " AND to_char(M.AR04FECINICIO,'dd/mm/yyyy hh24:mi:ss') = ?"
            sql = sql & " AND D.AD02CODDPTO = M.AD02CODDPTO"
            sql = sql & " AND AR07FECFINMVTO IS NULL "
            sql = sql & " AND U.SG02COD = M.SG02COD "
            Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
            QyConsulta(0) = Text1(0).Text
            QyConsulta(1) = tiposel
            QyConsulta(2) = rshctipologia(1)
            Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            
            If rs.RowCount < 1 Then
            
               sql = " SELECT D.AD02DESDPTO, TO_CHAR(P.AR05FECSOLICITUD,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
               sql = sql & " FROM AD0200 D, AR0500 P, SG0200 U"
               sql = sql & " WHERE P.CI22NUMHISTORIA = ? "
               sql = sql & " AND P.AR00CODTIPOLOGIA = ? "
               sql = sql & " AND to_char(P.AR05FECFINSOLICITUD,'dd/mm/yyyy hh24:mi:ss') = ? "
               sql = sql & " AND D.AD02CODDPTO = P.AD02CODDPTO"
               sql = sql & " AND U.SG02COD = P.SG02COD "
               Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
               QyConsulta(0) = Text1(0).Text
               QyConsulta(1) = tiposel
               QyConsulta(2) = rshctipologia(1)
               Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
           End If
            msgTipologias.TextMatrix(msgTipologias.row, 4) = "En el Departamento " & rs(0)
            msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(1)
            msgTipologias.TextMatrix(msgTipologias.row, 6) = rs(2) & " " & rs(3) & ", " & rs(4)
            rs.Close
       
      ' Historia ha sido dada de BAJA
       
       ElseIf rshctipologia("AR06CODESTADO") = "B" Then
            msgTipologias.TextMatrix(msgTipologias.row, 4) = "No existe"
            
       
   
      ' La Historia esta Solicitada
       
       Else
            sql = " SELECT D.AD02DESDPTO, TO_CHAR(P.AR05FECSOLICITUD,'DD/MM/YYYY HH24:MI:SS'), U.SG02APE1, U.SG02APE2, U.SG02NOM "
            sql = sql & " FROM AD0200 D, AR0500 P, SG0200 U"
            sql = sql & " WHERE P.CI22NUMHISTORIA = ?"
            sql = sql & " AND P.AR00CODTIPOLOGIA = ?"
            sql = sql & " AND P.AR05FECFINSOLICITUD IS NULL "
            sql = sql & " AND D.AD02CODDPTO = P.AD02CODDPTO "
            sql = sql & " AND U.SG02COD = P.SG02COD "
            Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
            QyConsulta(0) = Text1(0).Text
            QyConsulta(1) = tiposel
            Set rs = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            If rs.RowCount Then
                msgTipologias.TextMatrix(msgTipologias.row, 4) = "�Solicitada por el Departamento " & rs(0) & "!"
                msgTipologias.TextMatrix(msgTipologias.row, 5) = rs(1)
                msgTipologias.TextMatrix(msgTipologias.row, 6) = rs(2) & " " & rs(3) & ", " & rs(4)
                rs.Close
            Else
                ' esta todo correcto
                msgTipologias.TextMatrix(msgTipologias.row, 4) = "En el Archivo"
                msgTipologias.TextMatrix(msgTipologias.row, 0) = correcto
            End If
       End If
Else
        msgTipologias.TextMatrix(msgTipologias.row, 4) = "No existe"
End If   ' DEL ROWCOUNT

End Sub

Private Sub msgTipologias_DblClick()
Call cmdMarcar_Click
End Sub

Private Sub msgTipologias_SelChange()
With msgTipologias
  If .Rows = 1 Then
    .BackColorSel = &H8000000F
    .ForeColorSel = &H80000012
  Else
    .BackColorSel = &H8000000D
    .ForeColorSel = &H8000000E
  End If
End With
End Sub




