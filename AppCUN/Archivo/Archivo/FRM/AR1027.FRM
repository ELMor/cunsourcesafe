VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form dlgSelEtiquetadoPtosUbi 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Etiquetado de Puntos de Ubicaci�n"
   ClientHeight    =   3270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6840
   ControlBox      =   0   'False
   HelpContextID   =   7
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   6840
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   7
      Left            =   5160
      TabIndex        =   14
      Top             =   2640
      Width           =   1455
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   5160
      TabIndex        =   5
      Top             =   1140
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5160
      TabIndex        =   6
      Top             =   1740
      Width           =   1455
   End
   Begin VB.Frame Frame6 
      Caption         =   "L�neas"
      Height          =   975
      Index           =   1
      Left            =   180
      TabIndex        =   7
      Top             =   840
      Width           =   4695
      Begin VB.TextBox txtLineaH 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   3000
         MaxLength       =   4
         TabIndex        =   2
         Top             =   390
         Width           =   1395
      End
      Begin VB.TextBox txtLineaD 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   840
         MaxLength       =   4
         TabIndex        =   1
         Top             =   390
         Width           =   1395
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   11
         Top             =   390
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   10
         Top             =   390
         Width           =   855
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Puntos de Ubicaci�n"
      Height          =   975
      Index           =   2
      Left            =   180
      TabIndex        =   8
      Top             =   2040
      Width           =   4695
      Begin VB.TextBox txtHuecoD 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   840
         MaxLength       =   5
         TabIndex        =   3
         Top             =   390
         Width           =   1395
      End
      Begin VB.TextBox txtHuecoH 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   3000
         MaxLength       =   5
         TabIndex        =   4
         Top             =   390
         Width           =   1395
      End
      Begin VB.Label Label4 
         Caption         =   "Desde"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   12
         Top             =   390
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Hasta"
         Height          =   255
         Index           =   2
         Left            =   2400
         TabIndex        =   13
         Top             =   390
         Width           =   615
      End
   End
   Begin VB.ComboBox cboArchivo 
      BackColor       =   &H00FFFF00&
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4695
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Bindings        =   "AR1027.frx":0000
      Left            =   5640
      Top             =   360
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      Destination     =   1
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label8 
      Caption         =   "Punto de Archivo"
      Height          =   255
      Left            =   180
      TabIndex        =   9
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "dlgSelEtiquetadoPtosUbi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim puntos As New Collection

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 7)
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdImprimir_Click()
  'declaraciones
  Dim correcto As Boolean
  Dim sql As String
  Dim camino As String
  'comprobaciones
  correcto = True
  'pto archivo
  If correcto And cboArchivo.ListIndex = -1 Then
    MsgBox "Debe especificar un Punto de Archivo", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
    cboArchivo.SetFocus
    correcto = False
  End If
  'lineas
  If correcto And txtLineaD.Text <> "" And txtLineaH.Text <> "" Then
    If Val(txtLineaD.Text) > Val(txtLineaH.Text) Then
      MsgBox "La L�nea Desde debe ser menor que la L�nea Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
      txtLineaD.SetFocus
      correcto = False
    End If
  ElseIf correcto Then
    MsgBox "Debe especificar el rango de L�neas para poder imprimir", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
    txtLineaD.SetFocus
    correcto = False
  End If
  'ptos ubicacion
  If correcto And txtHuecoD.Text <> "" And txtHuecoH.Text <> "" Then
    If Val(txtHuecoD.Text) > Val(txtHuecoH.Text) Then
      MsgBox "El Punto de Ubicaci�n Desde debe ser menor que el Punto de Ubicaci�n Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
      txtHuecoD.SetFocus
      correcto = False
    End If
  ElseIf correcto Then
    MsgBox "Debe especificar el rango de Puntos de Ubicaci�n para poder imprimir", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
    txtHuecoD.SetFocus
    correcto = False
  End If
  If correcto Then 'comprobaciones superadas
    sql = "SELECT AR03CODLINEA, AR08NUMHUECO "
    sql = sql & "FROM AR0800 "
    sql = sql & "WHERE AR02CODPTOARCHIVO = '" & puntos(cboArchivo.ListIndex + 1) & "' AND "
    sql = sql & "AR03CODLINEA >= " & txtLineaD & " AND AR03CODLINEA <= " & txtLineaH & " AND "
    sql = sql & "AR08NUMHUECO >= " & txtHuecoD & " AND AR08NUMHUECO <= " & txtHuecoH & " "
    sql = sql & " ORDER BY AR03CODLINEA, AR08NUMHUECO"
    camino = Trim(App.Path)
    If Right(camino, 1) = "\" Then
      camino = Left(camino, Len(camino) - 1)
    End If
    CrystalReport1.ReportFileName = camino & PATH_REPORTS & "\ETIQ_UBICA.RPT"
    CrystalReport1.Connect = objApp.rdoConnect.Connect
    CrystalReport1.SQLQuery = sql
    CrystalReport1.Action = 1
  End If
End Sub

Private Sub Form_Load()
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  Dim i As Integer
  'cargar combo con puntos de archivo
  Set c = objApp.rdoConnect
  Set q = c.CreateQuery("", "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO")
  Set r = q.OpenResultset
  Set puntos = Nothing
  Do Until r.EOF
    cboArchivo.AddItem CStr(r(1))
    puntos.Add CStr(r(0))
    r.MoveNext
  Loop
  r.Close
  q.Close
End Sub

Private Sub txtHuecoD_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtHuecoH_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtLineaD_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtLineaH_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

