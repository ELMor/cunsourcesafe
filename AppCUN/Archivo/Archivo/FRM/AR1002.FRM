VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmTipologias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Mantenimiento de Tipolog�as"
   ClientHeight    =   4470
   ClientLeft      =   1575
   ClientTop       =   1620
   ClientWidth     =   8445
   HelpContextID   =   3
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4470
   ScaleWidth      =   8445
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipolog�as"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3520
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   8175
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2985
         Index           =   0
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   7890
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   13917
         _ExtentY        =   5265
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   4185
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmTipologias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF1 Then
  Call objApp.HelpContext
End If
End Sub

Private Sub Form_Load()
  Dim objTipologia As New clsCWForm
  Dim strKey   As String
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objWin = New clsCWWin
  
  Call objWin.WinCreateInfo(cwModeMultiLineEdit, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  
  With objWin.objDoc
    .cwPRJ = "CWSpy"
    .cwEVT = ""
  End With
  With objTipologia
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
    .strTable = "AR0000"
    
    Call .FormAddOrderField("AR00CODTIPOLOGIA", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Roles de Usuario")
    Call .FormAddFilterWhere(strKey, "AR00CODTIPOLOGIA", "C�digo Tipologia", cwString, objGen.ReplaceStr("SELECT AR00CODTIPOLOGIA #C�digo de Tipologia#, AR00DESTIPOLOGIA #Descripci�n de la Tipologia# FROM AR0000 ORDER BY AR00CODTIPOLOGIA", "#", Chr(34), 0))
    Call .FormAddFilterWhere(strKey, "AR00DESTIPOLOGIA", "Descripci�n Tipologia", cwString)
  
    Call .FormAddFilterOrder(strKey, "AR00CODTIPOLOGIA", "C�digo Tipologia")
    Call .FormAddFilterOrder(strKey, "AR00DESTIPOLOGIA", "Descripci�n Tipologia")
    
    .intCursorSize = -1
    .strName = "Tipologias"
  End With
  
  With objWin
    Call .FormAddInfo(objTipologia, cwFormMultiLine)
    Call .GridAddColumn(objTipologia, "C�digo Tipolog�a", "AR00CODTIPOLOGIA")
    Call .GridAddColumn(objTipologia, "Descripci�n Tipolog�a", "AR00DESTIPOLOGIA")
    Call .GridAddColumn(objTipologia, "Letra Etiqueta", "AR00LETRAETIQUETA")
  
    Call .FormCreateInfo(objTipologia)
    
    With .CtrlGetInfo(grdDBGrid1(0).Columns(3))
      .intMask = cwMaskUpper
      .blnInFind = True
    End With
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
  
    grdDBGrid1(0).Columns("C�digo Tipolog�a").Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns("C�digo Tipolog�a").Width = 1350
    grdDBGrid1(0).Columns("Descripci�n Tipolog�a").Width = 4000
    grdDBGrid1(0).Columns("Letra Etiqueta").Alignment = ssCaptionAlignmentCenter
    grdDBGrid1(0).Columns("Letra Etiqueta").Width = 1200
  
    Call .WinRegister
    Call .WinStabilize
  End With
  
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWin.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub



Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWin.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWin.WinDeRegister
  Call objWin.WinRemoveInfo
End Sub


Private Sub objWin_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
    'grdDBGrid1(0).MoveFirst
    If grdDBGrid1(0).Columns("C�digo Tipolog�a").Text = TIPOLOGIA_PAPEL Then
        Call MsgBox("�No se puede eliminar la Tipolog�a Papel!", vbOKOnly, "Error")
        blnCancel = True
    End If

End Sub

Private Sub objWin_cwPrint(ByVal strFormName As String)
  Dim strWhere  As String
  Dim strOrder  As String
  
  If strFormName = "Tipologias" Then
    With objWin.FormPrinterDialog(True, "")
      If .Selected > 0 Then
        strWhere = objWin.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
          strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), "WHERE ", " AND ")
          strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = "ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWin.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbToolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWin.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWin.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWin.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWin.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWin.CtrlDataChange
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWin.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


