VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmPrestamos 
   Caption         =   "ARCHIVO. Pr�stamos de Historias Cl�nicas"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   11880
   ClipControls    =   0   'False
   HelpContextID   =   19
   Icon            =   "AR1023.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   ScaleHeight     =   8310
   ScaleWidth      =   11880
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   741
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Ruta de Localizaci�n"
      Enabled         =   0   'False
      Height          =   375
      Left            =   9480
      TabIndex        =   10
      Top             =   720
      Width           =   2175
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   8880
      Top             =   720
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "AR1023.frx":0442
      Left            =   240
      List            =   "AR1023.frx":0444
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   840
      Width           =   4215
   End
   Begin VB.Frame fraFrame1 
      BorderStyle     =   0  'None
      Height          =   6375
      Index           =   9
      Left            =   120
      TabIndex        =   2
      Tag             =   "Frame1"
      Top             =   1360
      Width           =   11775
      Begin TabDlg.SSTab SSTab1 
         Height          =   6240
         Left            =   0
         TabIndex        =   3
         Top             =   0
         Width           =   11685
         _ExtentX        =   20611
         _ExtentY        =   11007
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         Enabled         =   0   'False
         TabCaption(0)   =   "&Urgentes"
         TabPicture(0)   =   "AR1023.frx":0446
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFrame1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Investigaci�n"
         TabPicture(1)   =   "AR1023.frx":0462
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraFrame1(2)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "&Otros"
         TabPicture(2)   =   "AR1023.frx":047E
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraFrame1(3)"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "&Pr�stamos"
         TabPicture(3)   =   "AR1023.frx":049A
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "fraFrame1(4)"
         Tab(3).ControlCount=   1
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5760
            Index           =   4
            Left            =   -74880
            TabIndex        =   18
            Tag             =   "Prestamos"
            Top             =   420
            Width           =   11460
            Begin VB.CommandButton cmdTodo 
               Height          =   375
               Index           =   3
               Left            =   11080
               Picture         =   "AR1023.frx":04B6
               Style           =   1  'Graphical
               TabIndex        =   19
               ToolTipText     =   "Seleccionar todos"
               Top             =   0
               Width           =   375
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5685
               Index           =   4
               Left            =   0
               TabIndex        =   20
               Top             =   0
               Width           =   10995
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   -2147483643
               BackColorOdd    =   -2147483643
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   19394
               _ExtentY        =   10028
               _StockProps     =   79
            End
         End
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5760
            Index           =   3
            Left            =   -74880
            TabIndex        =   15
            Tag             =   "Otros"
            Top             =   420
            Width           =   11460
            Begin VB.CommandButton cmdEliminar 
               Height          =   375
               Index           =   2
               Left            =   11080
               Picture         =   "AR1023.frx":06CC
               Style           =   1  'Graphical
               TabIndex        =   23
               Top             =   480
               Width           =   375
            End
            Begin VB.CommandButton cmdTodo 
               Height          =   375
               Index           =   2
               Left            =   11080
               Picture         =   "AR1023.frx":0C1E
               Style           =   1  'Graphical
               TabIndex        =   16
               ToolTipText     =   "Seleccionar todos"
               Top             =   0
               Width           =   375
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5685
               Index           =   3
               Left            =   0
               TabIndex        =   17
               Top             =   0
               Width           =   10995
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   -2147483643
               BackColorOdd    =   -2147483643
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   19394
               _ExtentY        =   10028
               _StockProps     =   79
            End
         End
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5760
            Index           =   2
            Left            =   -74880
            TabIndex        =   12
            Tag             =   "Investigaci�n"
            Top             =   420
            Width           =   11460
            Begin VB.CommandButton cmdEliminar 
               Height          =   375
               Index           =   1
               Left            =   11080
               Picture         =   "AR1023.frx":0E34
               Style           =   1  'Graphical
               TabIndex        =   22
               Top             =   480
               Width           =   375
            End
            Begin VB.CommandButton cmdTodo 
               Height          =   375
               Index           =   1
               Left            =   11080
               Picture         =   "AR1023.frx":1386
               Style           =   1  'Graphical
               TabIndex        =   13
               ToolTipText     =   "Seleccionar todos"
               Top             =   0
               Width           =   375
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5685
               Index           =   2
               Left            =   0
               TabIndex        =   14
               Top             =   0
               Width           =   10995
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   -2147483643
               BackColorOdd    =   -2147483643
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   19394
               _ExtentY        =   10028
               _StockProps     =   79
            End
         End
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5760
            Index           =   1
            Left            =   120
            TabIndex        =   4
            Tag             =   "Urgentes"
            Top             =   420
            Width           =   11460
            Begin VB.CommandButton cmdEliminar 
               Height          =   375
               Index           =   0
               Left            =   11080
               Picture         =   "AR1023.frx":159C
               Style           =   1  'Graphical
               TabIndex        =   21
               Top             =   480
               Width           =   375
            End
            Begin VB.CommandButton cmdTodo 
               Height          =   375
               Index           =   0
               Left            =   11080
               Picture         =   "AR1023.frx":1AEE
               Style           =   1  'Graphical
               TabIndex        =   11
               ToolTipText     =   "Seleccionar todos"
               Top             =   0
               Width           =   375
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5685
               Index           =   0
               Left            =   0
               TabIndex        =   5
               Top             =   0
               Width           =   10995
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowColumnSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   -2147483643
               BackColorOdd    =   -2147483643
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   19394
               _ExtentY        =   10028
               _StockProps     =   79
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   8055
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   450
      Style           =   1
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picReloj 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   5160
      Picture         =   "AR1023.frx":1D04
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   8
      ToolTipText     =   "Configurar Temporizador"
      Top             =   660
      Width           =   495
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "�Hay nuevas Peticiones Urgentes!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Index           =   3
      Left            =   5640
      TabIndex        =   9
      Top             =   780
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.Label Label3 
      Caption         =   "Punto de Archivo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   480
      Width           =   2055
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPrestamos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public objHHCCPRINCI                        As New clsCWForm
Public objHHCC                              As New clsCWForm
Public objHHCC2                             As New clsCWForm
Public objHHCC3                             As New clsCWForm
Public objHHCC4                             As New clsCWForm
Dim strWhere                                As String
Dim num_puntos                              As Integer
Dim puntos                                  As New Collection
Dim departamento                            As New Collection
Dim PUNTOSQL                                As String
Dim PUNTOSQL2                               As String
Dim sql                                     As String
Dim coddpto                                 As String
Dim valorcombo1                             As Integer
Dim rdopuntos                               As rdoResultset
Dim rsdepartamento                          As rdoResultset
Dim rscompro                                As rdoResultset
Dim QyConsulta                              As rdoQuery
Dim QyInsert                                As rdoQuery
Public SOLI                                 As String
Dim dtRelojUrgeServ                         As String
Dim dtRelojInveServ                         As String
Dim dtRelojOtroServ                         As String
Dim dtRelojPresServ                         As String
Dim fechamodi                               As Date
Dim BOLfechamodi                            As Boolean
Dim cllHCTipUrge                            As New Collection
Dim cllHCTipInve                            As New Collection
Dim cllHCTipOtro                            As New Collection
Public bolHCTipUrge                         As Boolean
Public bolHCTipInve                         As Boolean
Public bolHCTipOtro                         As Boolean
Dim bolprimera                              As Boolean
Dim bolVolcar                               As Boolean
Dim grabando                                As Boolean
Public Minutos                              As Integer
Public intervalo                            As Integer

Dim WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1

Private Sub cmdEliminar_Click(Index As Integer)
  Dim indice As Integer
  If Index = 0 Then
    indice = 0
  Else
    indice = Index + 1
  End If
  If grdDBGrid1(indice).Rows > 0 Then
    mnuDatosOpcion_Click (60)
  End If
End Sub

Private Sub cmdTodo_Click(Index As Integer)
  'declaraciones
  Dim i As Integer
  Dim fila As Integer
  Dim rsfecha As rdoResultset
  Dim respuesta
  Dim indice As Integer
  objWin.DataRefresh
  If Index = 0 Then
    indice = 0
  Else
    indice = Index + 1
  End If
  If grdDBGrid1(indice).Rows > 0 Then
    respuesta = MsgBox("�Est� seguro de que desea grabar los cambios realizados?", vbYesNoCancel + vbApplicationModal + vbQuestion, "Pregunta")
    If respuesta = vbYes Then
      'inicializaciones
      Screen.MousePointer = vbHourglass
      fila = grdDBGrid1(indice).row
      Select Case Index
      Case 0
        Set cllHCTipUrge = Nothing
      Case 1
        Set cllHCTipInve = Nothing
      Case 2
        Set cllHCTipOtro = Nothing
      End Select
      grdDBGrid1(indice).MoveFirst
      For i = 0 To grdDBGrid1(indice).Rows - 1
        Select Case Index
        Case 0
          cllHCTipUrge.Add (grdDBGrid1(0).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(0).Columns("(CodTipolog�a)").Text)
        Case 1
          cllHCTipInve.Add (grdDBGrid1(2).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(2).Columns("(CodTipolog�a)").Text)
        Case 2
          cllHCTipOtro.Add (grdDBGrid1(3).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(3).Columns("(CodTipolog�a)").Text)
        End Select
        grdDBGrid1(indice).MoveNext
      Next i
      Command1.Enabled = True
      If Index <> 3 Then
        sql = "UPDATE AR0500 SET AR06CODESTADO = 'L' "
        Select Case Index
        Case 0
          sql = sql & "WHERE " & objHHCC.strWhere
        Case 1
          sql = sql & "WHERE " & objHHCC2.strWhere
        Case 2
          sql = sql & "WHERE " & objHHCC3.strWhere
        End Select
        Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
        QyInsert.Execute
        QyInsert.Close
        objWin.DataRefresh
        grdDBGrid1(indice).row = fila
      Else
        Set rsfecha = objApp.rdoConnect.OpenResultset(" Select TO_CHAR(sysdate,'DD/MM/YYYY HH24:MI:SS') from dual", rdOpenKeyset, rdConcurValues)
        sql = "UPDATE AR0500 SET AR05FECFINSOLICITUD = TO_DATE('" & rsfecha(0) & "', 'DD/MM/YYYY HH24:MI:SS') "
        sql = sql & "WHERE " & objHHCC4.strWhere
        Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
        QyInsert.Execute
        QyInsert.Close
        grdDBGrid1(4).MoveFirst
        For i = 0 To grdDBGrid1(indice).Rows - 1
          sql = " UPDATE AR0400 "
          sql = sql & " SET AR04FECFIN = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')"
          sql = sql & " WHERE CI22NUMHISTORIA =? "
          sql = sql & " AND AR00CODTIPOLOGIA =? "
          sql = sql & " AND AR04FECFIN IS NULL "
          Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
          QyInsert(0) = rsfecha(0)
          QyInsert(1) = grdDBGrid1(4).Columns("N�mero H.C.").Text
          QyInsert(2) = grdDBGrid1(4).Columns("(CodTipolog�a)").Text
          QyInsert.Execute
          QyInsert.Close
          sql = "INSERT INTO AR0400 ( CI22NUMHISTORIA, AR00CODTIPOLOGIA, "
          sql = sql & "AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO) "
          sql = sql & "VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), 'P',?)"
          Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
          QyInsert(0) = grdDBGrid1(4).Columns("N�mero H.C.").Text
          QyInsert(1) = grdDBGrid1(4).Columns("(CodTipolog�a)").Text
          QyInsert(2) = rsfecha(0)
          QyInsert(3) = puntos.Item(Combo1.ItemData(Combo1.ListIndex))  'PTO ARCHIVO
          QyInsert(3) = Mid(QyInsert(3), 2, Len(QyInsert(3)) - 2)
          QyInsert.Execute
          QyInsert.Close
          grdDBGrid1(4).MoveNext
        Next i
        objWin.DataRefresh
      End If
      'finalizaciones
      Screen.MousePointer = vbNormal
    End If
  End If
End Sub

Private Sub Combo1_Click()

Dim resp As Integer
 
If bolprimera = False Then
    Me.MousePointer = vbHourglass
    If Command1.Enabled = True Then
        Me.MousePointer = vbDefault
        resp = MsgBox("�Existen Rutas de Localizaci�n Pendientes de Emitir!" & Chr(13) & "�Desea emitirlas ahora ?", vbYesNoCancel + vbInformation, "Aviso")
        If resp = vbYes Then
            Call Command1_Click
            Combo1.ListIndex = valorcombo1
        ElseIf resp = vbNo Then
            Command1.Enabled = False
            Call selcombo
        Else
            Combo1.ListIndex = valorcombo1
        End If
    Else
        Call selcombo
    End If
Else
    Call selcombo
    bolprimera = False
End If
'inicializar tab urgentes
'objHHCC.strWhere = "(CI22NUMHISTORIA = 1 AND CI22NUMHISTORIA = 2)" 'condicion que siempre devuelve nada
'inicializar tab investigacion
objHHCC2.strWhere = "(1=2)" 'condicion que siempre devuelve nada
'inicializar tab otros
objHHCC3.strWhere = "(1=2)" 'condicion que siempre devuelve nada
'inicializar tab prestamos
objHHCC4.strWhere = "(1=2)" 'condicion que siempre devuelve nada
Call enable_disable
SSTab1_Click (0)
End Sub
Private Sub selcombo()
Dim indcoleccion As Integer

If Combo1.ListIndex <> -1 Then
        valorcombo1 = Combo1.ListIndex
        SSTab1.Enabled = True
        indcoleccion = Combo1.ItemData(Combo1.ListIndex)
        PUNTOSQL = "H.AR02CODPTOARCHIVO =" & puntos.Item(Combo1.ItemData(Combo1.ListIndex))
        PUNTOSQL2 = "AR02CODPTOARCHIVO =" & puntos.Item(Combo1.ItemData(Combo1.ListIndex))
        objHHCC.strWhere = "( " & PUNTOSQL2 & " AND AR01CODMOTIVOSOL = 'U' AND AR05FECFINSOLICITUD IS NULL)"
        strWhere = objHHCC.strWhere
        Me.MousePointer = vbHourglass
        Call fraFrame1_Click(1)
        Me.MousePointer = vbDefault
    Else
       Me.MousePointer = vbDefault
    End If
Call enable_disable
End Sub

Private Sub Command1_Click()
Dim i As Integer
Dim resp As Integer
objWin.DataRefresh
Set dlgRutaLocalizacion.cllHCTip = New Collection
If cllHCTipUrge.Count > 0 Then 'URGENTES
    bolHCTipUrge = True
    For i = 1 To cllHCTipUrge.Count
        dlgRutaLocalizacion.cllHCTip.Add cllHCTipUrge.Item(i)
    Next
    dlgRutaLocalizacion.Check1(0).Enabled = True
End If
If cllHCTipInve.Count > 0 Then ' INVESTIGACI�N
    bolHCTipInve = True
    For i = 1 To cllHCTipInve.Count
        dlgRutaLocalizacion.cllHCTip.Add cllHCTipInve.Item(i)
    Next
    dlgRutaLocalizacion.Check1(2).Enabled = True
End If
If cllHCTipOtro.Count > 0 Then
    bolHCTipOtro = True
    For i = 1 To cllHCTipOtro.Count
        dlgRutaLocalizacion.cllHCTip.Add cllHCTipOtro.Item(i)
    Next
    dlgRutaLocalizacion.Check1(3).Enabled = True
End If
    dlgRutaLocalizacion.dlgRLstrPuntoArchivo = puntos.Item(Combo1.ItemData(Combo1.ListIndex))  'PTO ARCHIVO
    dlgRutaLocalizacion.Show 1
    Set dlgRutaLocalizacion = Nothing
'borrar colecciones de los prestamos que se hayan imprimido
If bolHCTipUrge = False Then
  Set cllHCTipUrge = Nothing
End If
If bolHCTipInve = False Then
  Set cllHCTipInve = Nothing
End If
If bolHCTipOtro = False Then
  Set cllHCTipOtro = Nothing
End If
'des/habilitar boton rutas
If cllHCTipUrge.Count + cllHCTipInve.Count + cllHCTipOtro.Count = 0 Then
  Command1.Enabled = False
End If
End Sub

Private Sub Form_Activate()
  Call enable_disable
End Sub

Private Sub Form_Load()
  'declaraciones
  Dim cadena As String
  Dim strKey   As String
  Dim intGridIndex As Integer
  'inicializaciones
  bolVolcar = False
  Minutos = 5
  intervalo = Minutos
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objWin = New clsCWWin
  Call objWin.WinCreateInfo(cwModeSingleEmpty, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  With objWin.objDoc
    .cwEVT = ""
  End With
  'urgentes
  With objHHCC
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AR0500"
    Call .FormAddOrderField("AR05FECSOLICITUD", cwAscending)
    strKey = .strDataBase & .strTable
    .intCursorSize = -1
    .strName = "HHCC"
  End With
  'citas
  'objHHCC1
  'investigacion
  With objHHCC2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AR0500"
    Call .FormAddOrderField("SG02COD", cwAscending)
    Call .FormAddOrderField("CI22NUMHISTORIA", cwAscending)
    strKey = .strDataBase & .strTable
    .intCursorSize = -1
    .strName = "HHCC2"
  End With
  'otros
  With objHHCC3
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AR0500"
    Call .FormAddOrderField("CI22NUMHISTORIA", cwAscending)
    Call .FormAddOrderField("AR00CODTIPOLOGIA", cwAscending)
    strKey = .strDataBase & .strTable
    .intCursorSize = -1
    .strName = "HHCC3"
  End With
  'prestamos
  With objHHCC4
    Set .objFormContainer = fraFrame1(4)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(4)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AR0500 P"
    Call .FormAddOrderField("CI22NUMHISTORIA", cwAscending)
    Call .FormAddOrderField("AR00CODTIPOLOGIA", cwAscending)
    strKey = .strDataBase & .strTable
    .intCursorSize = -1
    .strName = "HHCC4"
  End With
  With objWin
    Call .FormAddInfo(objHHCC, cwFormMultiLine)
    Call .GridAddColumn(objHHCC, "(CodEstado)", "AR06CODESTADO")
    Call .GridAddColumn(objHHCC, "          ", "")
    Call .GridAddColumn(objHHCC, "Localizar", "", cwBoolean)
    Call .GridAddColumn(objHHCC, "N�mero H.C.", "CI22NUMHISTORIA")
    Call .GridAddColumn(objHHCC, "(CodTipolog�a)", "AR00CODTIPOLOGIA")
    Call .GridAddColumn(objHHCC, "Tipolog�a", "")
    Call .GridAddColumn(objHHCC, "Paciente", "")
    Call .GridAddColumn(objHHCC, "Fecha Solicitud", "AR05FECSOLICITUD")
    Call .GridAddColumn(objHHCC, "Observaciones", "AR05OBSERVACIO")
    Call .GridAddColumn(objHHCC, "(CodSolicitante)", "SG02COD")
    Call .GridAddColumn(objHHCC, "Solicitante", "")
    'Tabla Investigaci�n GRID del tercer  tab (del FORM 2)
    Call .FormAddInfo(objHHCC2, cwFormMultiLine)
    Call .GridAddColumn(objHHCC2, "(CodEstado)", "AR06CODESTADO")
    Call .GridAddColumn(objHHCC2, "          ", "")
    Call .GridAddColumn(objHHCC2, "Localizar", "", cwBoolean)
    Call .GridAddColumn(objHHCC2, "N�mero H.C.", "CI22NUMHISTORIA")
    Call .GridAddColumn(objHHCC2, "(CodTipolog�a)", "AR00CODTIPOLOGIA")
    Call .GridAddColumn(objHHCC2, "Tipolog�a", "")
    Call .GridAddColumn(objHHCC2, "Paciente", "")
    Call .GridAddColumn(objHHCC2, "(CodSolicitante)", "SG02COD")
    Call .GridAddColumn(objHHCC2, "Solicitante", "")
    Call .GridAddColumn(objHHCC2, "(CodDepartamento)", "AD02CODDPTO")
    Call .GridAddColumn(objHHCC2, "Departamento", "")
    Call .GridAddColumn(objHHCC2, "Fecha Solicitud", "AR05FECSOLICITUD")
        'Grid OTROS
    Call .FormAddInfo(objHHCC3, cwFormMultiLine)
    Call .GridAddColumn(objHHCC3, "(CodEstado)", "AR06CODESTADO")
    Call .GridAddColumn(objHHCC3, "          ", "")
    Call .GridAddColumn(objHHCC3, "Localizar", "", cwBoolean)
    Call .GridAddColumn(objHHCC3, "N�mero H.C.", "CI22NUMHISTORIA")
    Call .GridAddColumn(objHHCC3, "(CodTipolog�a)", "AR00CODTIPOLOGIA")
    Call .GridAddColumn(objHHCC3, "Tipolog�a", "")
    Call .GridAddColumn(objHHCC3, "Paciente", "")
    Call .GridAddColumn(objHHCC3, "(CodMotivo)", "AR01CODMOTIVOSOL")
    Call .GridAddColumn(objHHCC3, "Motivo Solicitud", "")
    Call .GridAddColumn(objHHCC3, "(CodDepartamento)", "AD02CODDPTO")
    Call .GridAddColumn(objHHCC3, "Departamento", "")
    Call .GridAddColumn(objHHCC3, "(CodSolicitante)", "SG02COD")
    Call .GridAddColumn(objHHCC3, "Solicitante", "")
    Call .GridAddColumn(objHHCC3, "Fecha Solicitud", "AR05FECSOLICITUD")
    Call .GridAddColumn(objHHCC3, "Observaciones", "AR05OBSERVACIO")
    'Grid Prestar
    Call .FormAddInfo(objHHCC4, cwFormMultiLine)
    Call .GridAddColumn(objHHCC4, "(CodEstado)", "AR06CODESTADO")
    Call .GridAddColumn(objHHCC4, "Prestar", "", cwBoolean)
    Call .GridAddColumn(objHHCC4, "N�mero H.C.", "CI22NUMHISTORIA")
    Call .GridAddColumn(objHHCC4, "(CodTipolog�a)", "AR00CODTIPOLOGIA")
    Call .GridAddColumn(objHHCC4, "Tipolog�a", "")
    Call .GridAddColumn(objHHCC4, "Paciente", "")
    Call .GridAddColumn(objHHCC4, "(CodDepartamento)", "AD02CODDPTO")
    Call .GridAddColumn(objHHCC4, "Departamento", "")
    Call .GridAddColumn(objHHCC4, "(CodMotivo)", "AR01CODMOTIVOSOL")
    Call .GridAddColumn(objHHCC4, "Tipo Pr�stamo", "")
    Call .GridAddColumn(objHHCC4, "(FecFin)", "AR05FECFINSOLICITUD")
    
    Call .FormCreateInfo(objHHCC)
    Call .FormCreateInfo(objHHCC2)
    Call .FormCreateInfo(objHHCC3)
    Call .FormCreateInfo(objHHCC4)
    
    objWin.CtrlGetInfo(grdDBGrid1(0).Columns("Observaciones")).blnReadOnly = True
    objWin.CtrlGetInfo(grdDBGrid1(3).Columns("Observaciones")).blnReadOnly = True
    
    'Tama�o de los campos del los Grid
     ' Grid Urgentes
       grdDBGrid1(0).Columns("Fecha Solicitud").Alignment = ssCaptionAlignmentCenter
       grdDBGrid1(0).Columns("Fecha Solicitud").Width = 1850
       grdDBGrid1(0).Columns("N�mero H.C.").Alignment = ssCaptionAlignmentRight
       grdDBGrid1(0).Columns("N�mero H.C.").Width = 1100
       grdDBGrid1(0).Columns("Tipolog�a").Width = 1300
       grdDBGrid1(0).Columns("Localizar").Width = 800
       grdDBGrid1(0).Columns("Observaciones").Width = 2800
       grdDBGrid1(0).Columns("Paciente").Width = 2800

     ' Grid Investigaci�n
       grdDBGrid1(2).Columns("Solicitante").Width = 2800
       grdDBGrid1(2).Columns("N�mero H.C.").Alignment = ssCaptionAlignmentRight
       grdDBGrid1(2).Columns("N�mero H.C.").Width = 1100
       grdDBGrid1(2).Columns("Tipolog�a").Width = 900
       grdDBGrid1(2).Columns("Fecha Solicitud").Alignment = ssCaptionAlignmentCenter
       grdDBGrid1(2).Columns("Fecha Solicitud").Width = 1600
       grdDBGrid1(2).Columns("Departamento").Width = 2200
       grdDBGrid1(2).Columns("Localizar").Width = 800
       grdDBGrid1(2).Columns("Paciente").Width = 2800
       
     ' Grid Otros
       grdDBGrid1(3).Columns("Motivo Solicitud").Width = 1900
       grdDBGrid1(3).Columns("Departamento").Width = 2300
       grdDBGrid1(3).Columns("Solicitante").Width = 2400
       grdDBGrid1(3).Columns("N�mero H.C.").Alignment = ssCaptionAlignmentRight
       grdDBGrid1(3).Columns("N�mero H.C.").Width = 1100
       grdDBGrid1(3).Columns("Tipolog�a").Width = 900
       grdDBGrid1(3).Columns("Localizar").Width = 800
       grdDBGrid1(3).Columns("Fecha Solicitud").Width = 1600
       grdDBGrid1(3).Columns("Observaciones").Width = 2800
       grdDBGrid1(3).Columns("Paciente").Width = 2800
       
     ' Grid Prestamos
       grdDBGrid1(4).Columns("N�mero H.C.").Alignment = ssCaptionAlignmentRight
       grdDBGrid1(4).Columns("N�mero H.C.").Width = 1100
       grdDBGrid1(4).Columns("Tipolog�a").Width = 1300
       grdDBGrid1(4).Columns("Departamento").Width = 2300
       grdDBGrid1(4).Columns("Tipo Pr�stamo").Width = 2300
       grdDBGrid1(4).Columns("Prestar").Alignment = ssCaptionAlignmentCenter
       grdDBGrid1(4).Columns("Paciente").Width = 2800
      
     ' OCULTAR CAMPOS
        ' Tabla Urgentes
            grdDBGrid1(0).Columns("(CodTipolog�a)").Visible = False
            grdDBGrid1(0).Columns("(CodEstado)").Visible = False
            grdDBGrid1(0).Columns("(CodSolicitante)").Visible = False
            grdDBGrid1(0).Columns("Solicitante").Visible = False
        ' Tabla Investigaci�n
            grdDBGrid1(2).Columns("(CodSolicitante)").Visible = False
            grdDBGrid1(2).Columns("(CodTipolog�a)").Visible = False
            grdDBGrid1(2).Columns("(CodDepartamento)").Visible = False
            grdDBGrid1(2).Columns("(CodEstado)").Visible = False
        ' Tabla Otros
            grdDBGrid1(3).Columns("(CodMotivo)").Visible = False
            grdDBGrid1(3).Columns("(CodDepartamento)").Visible = False
            grdDBGrid1(3).Columns("(CodSolicitante)").Visible = False
            grdDBGrid1(3).Columns("(CodTipolog�a)").Visible = False
            grdDBGrid1(3).Columns("(CodEstado)").Visible = False
        ' Tabla Prestamos
            grdDBGrid1(4).Columns("(CodTipolog�a)").Visible = False
            grdDBGrid1(4).Columns("(CodDepartamento)").Visible = False
            grdDBGrid1(4).Columns("(CodMotivo)").Visible = False
            grdDBGrid1(4).Columns("(CodEstado)").Visible = False
            grdDBGrid1(4).Columns("(FecFin)").Visible = False
    ' Mostrar las JOINS
    ' Tipolog�a
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).(CodTipolog�a)")), "AR00CODTIPOLOGIA", "SELECT  AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).(CodTipolog�a)")), grdDBGrid1(0).Columns("grddbgrid1(0).Tipolog�a"), "AR00DESTIPOLOGIA")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns("grddbgrid1(2).(CodTipolog�a)")), "AR00CODTIPOLOGIA", "SELECT  AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns("grddbgrid1(2).(CodTipolog�a)")), grdDBGrid1(2).Columns("grddbgrid1(2).Tipolog�a"), "AR00DESTIPOLOGIA")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns("grddbgrid1(3).(CodTipolog�a)")), "AR00CODTIPOLOGIA", "SELECT  AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns("grddbgrid1(3).(CodTipolog�a)")), grdDBGrid1(3).Columns("grddbgrid1(3).Tipolog�a"), "AR00DESTIPOLOGIA")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(4).Columns("grddbgrid1(4).(CodTipolog�a)")), "AR00CODTIPOLOGIA", "SELECT  AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns("grddbgrid1(4).(CodTipolog�a)")), grdDBGrid1(4).Columns("grddbgrid1(4).Tipolog�a"), "AR00DESTIPOLOGIA")
 '  SOLICITANTE
    intGridIndex = 0
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodSolicitante)")), "SG02COD", "SELECT SG02APE1||' '|| SG02APE2||', '||SG02NOM AS SOLI FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodSolicitante)")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitante"), "SOLI")
    For intGridIndex = 2 To 3
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodSolicitante)")), "SG02COD", "SELECT SG02APE1||' '|| SG02APE2||', '||SG02NOM AS SOLI FROM SG0200 WHERE SG02COD = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodSolicitante)")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitante"), "SOLI")
    Next
 ' Paciente
    intGridIndex = 0
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero H.C.")), "CI22NUMHISTORIA", "SELECT CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE AS PACIEN FROM CI2200 WHERE CI22NUMHISTORIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero H.C.")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Paciente"), "PACIEN")
    For intGridIndex = 2 To 4
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero H.C.")), "CI22NUMHISTORIA", "SELECT CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE AS PACIEN FROM CI2200 WHERE CI22NUMHISTORIA = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero H.C.")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Paciente"), "PACIEN")
    Next
 ' Departamento
    For intGridIndex = 2 To 4
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodDepartamento)")), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodDepartamento)")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento"), "AD02DESDPTO")
    Next
 ' MOTIVO
    intGridIndex = 3
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodMotivo)")), "AR01CODMOTIVOSOL", "SELECT AR01DESMOTIVOSOL FROM AR0100 WHERE AR01CODMOTIVOSOL = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodMotivo)")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Motivo Solicitud"), "AR01DESMOTIVOSOL")
    intGridIndex = 4
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodMotivo)")), "AR01CODMOTIVOSOL", "SELECT AR01DESMOTIVOSOL FROM AR0100 WHERE AR01CODMOTIVOSOL = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").(CodMotivo)")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Tipo Pr�stamo"), "AR01DESMOTIVOSOL")
'  ESTADO
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).(CodEstado)")), "AR06CODESTADO", "SELECT AR06DESESTADO FROM AR0600 WHERE AR06CODESTADO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("grddbgrid1(0).(CodEstado)")), grdDBGrid1(0).Columns("grddbgrid1(0).          "), "AR06DESESTADO")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns("grddbgrid1(2).(CodEstado)")), "AR06CODESTADO", "SELECT AR06DESESTADO FROM AR0600 WHERE AR06CODESTADO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns("grddbgrid1(2).(CodEstado)")), grdDBGrid1(2).Columns("grddbgrid1(2).          "), "AR06DESESTADO")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns("grddbgrid1(3).(CodEstado)")), "AR06CODESTADO", "SELECT AR06DESESTADO FROM AR0600 WHERE AR06CODESTADO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns("grddbgrid1(3).(CodEstado)")), grdDBGrid1(3).Columns("grddbgrid1(3).          "), "AR06DESESTADO")

    Call .WinRegister
    Call .WinStabilize
  End With
  
  Dim i As Integer
  Dim sql
  'borrar combo
  Combo1.Clear
  'borrar coleccion puntos de archivo
  Set puntos = Nothing
  sql = "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200"
  Set rdopuntos = objApp.rdoConnect.OpenResultset(sql, rdOpenKeyset, rdConcurValues)
  rdopuntos.MoveFirst
  i = 0
  Do Until rdopuntos.EOF
      Combo1.AddItem (rdopuntos("AR02CODPTOARCHIVO") & "  " & rdopuntos("AR02DESPTOARCHIVO"))
      Combo1.ItemData(i) = i + 1
      'rellenar coleccion
      puntos.Add ("'" & rdopuntos("AR02CODPTOARCHIVO") & "'")
      'pasar a la siguiente linea
      rdopuntos.MoveNext
      i = i + 1
  Loop
  rdopuntos.Close
  
  bolprimera = True
   
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWin.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWin.WinProcess(cwProcessKeys, intKeyCode, intShift)
  If intKeyCode = vbKeyF1 Then
    Call objApp.HelpContext
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  Dim resp
  intCancel = objWin.WinExit
  If Command1.Enabled = True Then
    resp = MsgBox("�Existen Rutas de Localizaci�n Pendientes de Emitir!" & Chr(13) & "�Desea emitirlas ahora ?", vbYesNoCancel + vbInformation, "Aviso")
    If resp = vbYes Then
      intCancel = True
      Call Command1_Click
    ElseIf resp = vbCancel Then
      intCancel = True
    End If
  End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWin.WinDeRegister
  Call objWin.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Change(Index As Integer)
Call objWin.CtrlDataChange
Call enable_disable
End Sub

Private Sub grdDBGrid1_DblClick(Index As Integer)
Call grdDBGrid1_Click(Index)
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessRegister, intIndex, 0)
  Call enable_disable
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessOptions, intIndex, 0)
  Call enable_disable
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWin.CtrlGotFocus
  Call enable_disable
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWin.GridChangeRowCol(vntLastRow, intLastCol)
  Call enable_disable
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
Dim rdofecha As rdoResultset
Dim sqlfecha As String
Dim fila As Integer
Dim rsfecha As rdoResultset
BOLfechamodi = False
Dim conta As Integer
Dim anadircolec As Boolean
If Index = 0 Then
  cmdTodo(0).Enabled = False
Else
  cmdTodo(Index - 1).Enabled = False
End If
Select Case Index
 Case 0 'urgentes
    If grdDBGrid1(Index).col <> -1 Then
        If grdDBGrid1(Index).Columns(grdDBGrid1(Index).col).Caption = "Localizar" Then

             If grdDBGrid1(Index).Columns("Localizar").Value = True Then    ' Si el check box esta activo
                                                 ' Compruebo si la descripci�n
                                                 ' de estado contiene algo
                                                 ' en caso de ser asi no hago nada
                If grdDBGrid1(Index).Columns("          ").Text = "" Or grdDBGrid1(Index).Columns("          ").Text = Null Then
                    grdDBGrid1(Index).Columns("Localizar").Value = Unchecked
                    grdDBGrid1(Index).Columns("(CodEstado)").Text = ""
                    fila = grdDBGrid1(Index).row
                    grdDBGrid1(Index).row = 0
                    grdDBGrid1(Index).row = fila
                    Call objWin.CtrlDataChange
                Else
                    grdDBGrid1(Index).Columns("Localizar").Value = Unchecked
                    grdDBGrid1(Index).Columns("(CodEstado)").Text = "L"
                    Call objWin.CtrlDataChange
                End If
            Else
              grdDBGrid1(Index).Columns("Localizar").Value = True
              grdDBGrid1(Index).Columns("(CodEstado)").Text = "L"
              Call objWin.CtrlDataChange
            End If
        End If
    End If

Case 2  'investigacion
    If grdDBGrid1(Index).col <> -1 Then
        If grdDBGrid1(Index).Columns(grdDBGrid1(Index).col).Caption = "Localizar" Then
             If grdDBGrid1(Index).Columns("Localizar").Value = True Then    ' Si el check box esta activo
                                                 ' Compruebo si la descripci�n
                                                 ' de estado contiene algo
                                                 ' en caso de ser asi no hago nada
                If grdDBGrid1(Index).Columns("          ").Text = "" Or grdDBGrid1(Index).Columns("          ").Text = Null Then
                    grdDBGrid1(Index).Columns("Localizar").Value = Unchecked
                    grdDBGrid1(Index).Columns("(CodEstado)").Text = ""
                    fila = grdDBGrid1(Index).row
                    grdDBGrid1(Index).row = 0
                    grdDBGrid1(Index).row = fila
                    Call objWin.CtrlDataChange
                Else
                    grdDBGrid1(Index).Columns("Localizar").Value = Unchecked
                    grdDBGrid1(Index).Columns("(CodEstado)").Text = "L"
                    Call objWin.CtrlDataChange
                End If
             Else
              grdDBGrid1(Index).Columns("Localizar").Value = True
              grdDBGrid1(Index).Columns("(CodEstado)").Text = "L"
              Call objWin.CtrlDataChange
            End If
        End If
    End If

Case 3  'otros
    If grdDBGrid1(Index).col <> -1 Then
        If grdDBGrid1(Index).Columns(grdDBGrid1(Index).col).Caption = "Localizar" Then
             If grdDBGrid1(Index).Columns("Localizar").Value = True Then    ' Si el check box esta activo
                                                 ' Compruebo si la descripci�n
                                                 ' de estado contiene algo
                                                 ' en caso de ser asi no hago nada
                If grdDBGrid1(Index).Columns("          ").Text = "" Or grdDBGrid1(Index).Columns("          ").Text = Null Then
                    grdDBGrid1(Index).Columns("Localizar").Value = Unchecked
                    grdDBGrid1(Index).Columns("(CodEstado)").Text = ""
                    fila = grdDBGrid1(Index).row
                    grdDBGrid1(Index).row = 0
                    grdDBGrid1(Index).row = fila
                    Call objWin.CtrlDataChange
                Else
                    grdDBGrid1(Index).Columns("Localizar").Value = Unchecked
                    grdDBGrid1(Index).Columns("(CodEstado)").Text = "L"
                    Call objWin.CtrlDataChange
                End If
        Else
              grdDBGrid1(Index).Columns("Localizar").Value = True
              grdDBGrid1(Index).Columns("(CodEstado)").Text = "L"
              Call objWin.CtrlDataChange
        End If
      End If
    End If

Case 4  'prestamos
    If grdDBGrid1(Index).col <> -1 Then
        If grdDBGrid1(Index).Columns(grdDBGrid1(Index).col).Caption = "Prestar" Then
            If grdDBGrid1(Index).Columns("Prestar").Value = True Then
                                                 ' Si el check box esta activo
                                                 ' Borro el contenido de fecfin
                grdDBGrid1(Index).Columns("Prestar").Value = Unchecked
                grdDBGrid1(Index).Columns("(FecFin)").Text = ""
                fila = grdDBGrid1(Index).row
                grdDBGrid1(Index).row = 0
                grdDBGrid1(Index).row = fila
                Call objWin.CtrlDataChange
          Else  'check inactivo, activando
                grdDBGrid1(Index).Columns("Prestar").Value = True
                sqlfecha = " Select sysdate from dual"
                Set rdofecha = objApp.rdoConnect.OpenResultset(sqlfecha, rdOpenKeyset, rdConcurValues)
                grdDBGrid1(Index).Columns("(FecFin)").Text = rdofecha(0)
                rdofecha.Close
                Call objWin.CtrlDataChange
            End If
        End If
    End If
End Select
Call enable_disable
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  If intIndex = 9 Then
    Call objWin.FormChangeActive(fraFrame1(SSTab1.Tab + 1), False, True)
  Else
    Call objWin.FormChangeActive(fraFrame1(intIndex), False, True)
  End If
End Sub

Private Sub objWin_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rs As rdoResultset
Dim rsinsert As rdoQuery
Dim sql As String
'si las colecciones tienen algo habilitar boton de rutas
If Command1.Enabled = False Then
  If (cllHCTipUrge.Count + cllHCTipInve.Count + cllHCTipOtro.Count) > 0 Then
    Command1.Enabled = True
  End If
End If
'si estamos grabando un prestamo, hacer update e insert en la tabla ar0400
If strFormName = "HHCC4" Then
  If grdDBGrid1(4).Columns("Prestar").Value = True Then
    sql = " UPDATE AR0400 "
    sql = sql & " SET AR04FECFIN = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')"
    sql = sql & " WHERE CI22NUMHISTORIA =? "
    sql = sql & " AND AR00CODTIPOLOGIA =? "
    sql = sql & " AND AR04FECFIN is null "
    Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
    QyInsert(0) = dtRelojPresServ
    QyInsert(1) = grdDBGrid1(4).Columns("N�mero H.C.").Text ' HISTORIA
    QyInsert(2) = grdDBGrid1(4).Columns("(CodTipolog�a)").Text ' TIPOLOGIA
    QyInsert.Execute
    QyInsert.Close
    sql = "INSERT INTO AR0400 ( CI22NUMHISTORIA, AR00CODTIPOLOGIA, "
    sql = sql & "AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO) "
    sql = sql & "VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), 'P',?)"
    Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
    QyInsert(0) = grdDBGrid1(4).Columns("N�mero H.C.").Text ' HISTORIA
    QyInsert(1) = grdDBGrid1(4).Columns("(CodTipolog�a)").Text ' TIPOLOG�A
    QyInsert(2) = dtRelojPresServ
    QyInsert(3) = puntos.Item(Combo1.ItemData(Combo1.ListIndex))  'PTO ARCHIVO
    QyInsert(3) = Mid(QyInsert(3), 2, Len(QyInsert(3)) - 2)
    QyInsert.Execute
    QyInsert.Close
  End If
End If
cmdTodo(SSTab1.Tab).Enabled = True
End Sub

Private Sub objWin_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  'declaraciones
  Dim bexisteruta As Boolean
  Dim j As Integer
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  Dim rsfecha As rdoResultset
  'inicializaciones
  Set c = objApp.rdoConnect
  'si es la primera vez que entramos en el prevalidate recordamos la fecha-hora
  If Not grabando Then
    Set rsfecha = objApp.rdoConnect.OpenResultset(" Select TO_CHAR(sysdate,'DD/MM/YYYY HH24:MI:SS') from dual", rdOpenKeyset, rdConcurValues)
    Select Case strFormName
      Case "HHCC" 'urgentes
        dtRelojUrgeServ = rsfecha(0)
      Case "HHCC2" 'investigacion
        dtRelojInveServ = rsfecha(0)
      Case "HHCC3" 'otras
        dtRelojOtroServ = rsfecha(0)
      Case "HHCC4" 'prestamos
        dtRelojPresServ = rsfecha(0)
    End Select
    grabando = True
  End If
  If strFormName = "HHCC" Then
    'TRATAMIENTO
    If grdDBGrid1(0).Columns("          ").Text = "" Then  ' Des. estado
      'A�ADIR A LA COLECCION
      cllHCTipUrge.Add (grdDBGrid1(0).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(0).Columns("(CodTipolog�a)").Text)
    ElseIf grdDBGrid1(0).Columns("          ").Text <> "" Then
      bexisteruta = False
      For j = 1 To cllHCTipUrge.Count
        If cllHCTipUrge.Item(j) = grdDBGrid1(0).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(0).Columns("(CodTipolog�a)").Text Then
          bexisteruta = True
          Exit For
        End If
      Next
      If bexisteruta = False Then
      cllHCTipUrge.Add (grdDBGrid1(0).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(0).Columns("(CodTipolog�a)").Text)
      End If
    End If
  ElseIf strFormName = "HHCC2" Then
    'TRATAMIENTO
    If grdDBGrid1(2).Columns("          ").Text = "" Then  ' Des. estado
      'A�ADIR A LA COLECCION
      cllHCTipInve.Add (grdDBGrid1(2).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(2).Columns("(CodTipolog�a)").Text)
    Else
      bexisteruta = False
      For j = 1 To cllHCTipInve.Count
        If cllHCTipInve.Item(j) = grdDBGrid1(2).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(2).Columns("(CodTipolog�a)").Text Then
          bexisteruta = True
          Exit For
        End If
      Next
      If bexisteruta = False Then
        cllHCTipInve.Add (grdDBGrid1(2).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(2).Columns("(CodTipolog�a)").Text)
      End If
    End If
  ElseIf strFormName = "HHCC3" Then
    'TRATAMIENTO
    If grdDBGrid1(3).Columns("          ").Text = "" Then  ' Des. estado
      'A�ADIR A LA COLECCION
      cllHCTipOtro.Add (grdDBGrid1(3).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(3).Columns("(CodTipolog�a)").Text)
    Else
      bexisteruta = False
      For j = 1 To cllHCTipOtro.Count
        If cllHCTipOtro.Item(j) = grdDBGrid1(3).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(3).Columns("(CodTipolog�a)").Text Then
          bexisteruta = True
          Exit For
        End If
      Next
      If bexisteruta = False Then
        cllHCTipOtro.Add (grdDBGrid1(3).Columns("N�mero H.C.").Text & "-" & grdDBGrid1(3).Columns("(CodTipolog�a)").Text)
      End If
    End If
  ElseIf strFormName = "HHCC4" Then 'prestar
    If grdDBGrid1(4).Columns("Prestar").Value = True Then
      grdDBGrid1(4).Columns("(FecFin)").Text = dtRelojPresServ
    Else
      grdDBGrid1(4).Columns("(FecFin)").Text = ""
    End If
  End If
End Sub

Private Sub picReloj_Click()
  dlgIntervalo.Show vbModal
  Set dlgIntervalo = Nothing
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  'declaraciones
  Dim rsfecha As rdoResultset
  'inicializaciones
  Me.MousePointer = vbHourglass
  Set rsfecha = objApp.rdoConnect.OpenResultset(" Select TO_CHAR(sysdate,'DD/MM/YYYY HH24:MI:SS')  from dual", rdOpenKeyset, rdConcurValues)
  cmdTodo(SSTab1.Tab).Enabled = True
   
Select Case SSTab1.Tab

Case 0    ' Urgentes
    Label1(3).Visible = False
    dtRelojUrgeServ = rsfecha(0)
    objHHCC.strWhere = strWhere
    objHHCC2.strWhere = "(1=2)"
    objHHCC3.strWhere = "(1=2)"
    objHHCC4.strWhere = "(1=2)"
    Call fraFrame1_Click(1)
Case 1 ' Investigaci�n
    dtRelojInveServ = rsfecha(0)
    objHHCC.strWhere = "(1=2)"
    objHHCC2.strWhere = "(" & PUNTOSQL2 & " AND AR01CODMOTIVOSOL = 'I' AND AR05FECFINSOLICITUD IS NULL)"
    objHHCC3.strWhere = "(1=2)"
    objHHCC4.strWhere = "(1=2)"
    Call fraFrame1_Click(2)
Case 2    ' Otros
    dtRelojOtroServ = rsfecha(0)
    objHHCC.strWhere = "(1=2)"
    objHHCC2.strWhere = "(1=2)"
    objHHCC3.strWhere = "(" & PUNTOSQL2 & " AND AR01CODMOTIVOSOL NOT IN ('U','C','I')  AND AR05FECFINSOLICITUD IS NULL)"
    objHHCC4.strWhere = "(1=2)"
    Call fraFrame1_Click(3)
Case 3    ' Prestamos
    dtRelojPresServ = rsfecha(0)
    objHHCC.strWhere = "(1=2)"
    objHHCC2.strWhere = "(1=2)"
    objHHCC3.strWhere = "(1=2)"
    objHHCC4.strWhere = "(" & PUNTOSQL2 & " AND AR05FECFINSOLICITUD IS NULL AND AR06CODESTADO ='L')"
    Call fraFrame1_Click(4)
End Select
Call objWin.DataRefresh
Me.MousePointer = vbDefault
End Sub

Private Sub Timer1_Timer()
  Dim rsnuevo                         As rdoResultset
  Dim rsnuevaURGE                     As rdoResultset
  Dim sql                             As String
  Dim hora_actual                     As Date
  Dim mensa                           As String
  Dim resp                            As Integer
  Dim rsserv                          As rdoResultset
  Minutos = Minutos - 1
  If Minutos = 0 Then
    Me.MousePointer = vbHourglass
    Minutos = intervalo
    If SSTab1.Enabled Then
      sql = "SELECT 1 FROM AR0500 WHERE (AR05FECSOLICITUD >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
      sql = sql & " OR AR05FECUPD >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')) "
      sql = sql & " AND  AR01CODMOTIVOSOL = 'U' AND AR05FECFINSOLICITUD IS NULL"
      Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
      QyConsulta(0) = dtRelojUrgeServ
      QyConsulta(1) = dtRelojUrgeServ
      Set rsnuevo = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurValues)
      If rsnuevo.RowCount > 0 Then 'hay modificaciones
        If SSTab1.Tab = 0 Then  'estamos en urgentes
          'actualizar grid
          objWin.DataRefresh
        Else  'no estamos en urgentes
          Label1(3).Visible = True
        End If
      End If
      rsnuevo.Close
      'actualizar relojes
      Set rsserv = objApp.rdoConnect.OpenResultset(" Select TO_CHAR(sysdate,'DD/MM/YYYY HH24:MI:SS') from dual", rdOpenKeyset, rdConcurValues)
      Select Case SSTab1.Tab
          Case 0
              dtRelojUrgeServ = rsserv(0)
          Case 1
              dtRelojInveServ = rsserv(0)
          Case 2
              dtRelojOtroServ = rsserv(0)
          Case 3
              dtRelojPresServ = rsserv(0)
      End Select
      rsserv.Close
    End If
    Me.MousePointer = vbDefault
  End If
End Sub

Private Sub tlbToolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
 Dim resp As Integer
Select Case Button.Index
    Case 30
        If Command1.Enabled = True Then
            resp = MsgBox("�Existen Rutas de Localizaci�n Pendientes de Emitir!" & Chr(13) & "�Desea emitirlas ahora ?", vbYesNoCancel + vbInformation, "Aviso")
            If resp = vbYes Then
                Call Command1_Click
            ElseIf resp = vbNo Then
                Call objWin.WinProcess(cwProcessToolBar, Button.Index, 0)
            End If
        Else
            Call objWin.WinProcess(cwProcessToolBar, Button.Index, 0)
        End If
    Case Else
        Call objWin.WinProcess(cwProcessToolBar, Button.Index, 0)
        Call enable_disable
End Select
End Sub

Sub enable_disable()
  'deshabilitar nuevo
  tlbToolbar1.Buttons(2).Enabled = False
  mnuDatosOpcion(10).Enabled = False
  'deshabilitar eliminar
  tlbToolbar1.Buttons(8).Enabled = False
  mnuDatosOpcion(60).Enabled = False
  'indicar que los datos no estan grabados para objWin.postWrite
  grabando = False
End Sub
