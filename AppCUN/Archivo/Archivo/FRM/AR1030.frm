VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTiposEntrega 
   Caption         =   "ARCHIVO. Preferencias para el Pr�stamo de Citas"
   ClientHeight    =   7620
   ClientLeft      =   1890
   ClientTop       =   975
   ClientWidth     =   8505
   HelpContextID   =   2
   LinkTopic       =   "Form1"
   ScaleHeight     =   7620
   ScaleWidth      =   8505
   Begin ComctlLib.Toolbar tlbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   8505
      _ExtentX        =   15002
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlMainSmall"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Guardar"
            Object.Tag             =   ""
            ImageIndex      =   15
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.ToolTipText     =   "Primero"
            Object.Tag             =   ""
            ImageIndex      =   19
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.ToolTipText     =   "Anterior"
            Object.Tag             =   ""
            ImageIndex      =   22
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.ToolTipText     =   "Siguiente"
            Object.Tag             =   ""
            ImageIndex      =   29
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.ToolTipText     =   "Ultimo"
            Object.Tag             =   ""
            ImageIndex      =   28
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Salir"
            Object.Tag             =   ""
            ImageIndex      =   13
         EndProperty
      EndProperty
      BorderStyle     =   1
      MouseIcon       =   "AR1030.frx":0000
   End
   Begin VB.Frame Frame1 
      Caption         =   "Preferencias de Pr�stamo"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6975
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   8235
      Begin TabDlg.SSTab sstTiposEntrega 
         Height          =   6495
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   7995
         _ExtentX        =   14102
         _ExtentY        =   11456
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AR1030.frx":031A
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboUsuario"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Frame2"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lstDepartamento"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AR1030.frx":0336
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "msgTabla"
         Tab(1).ControlCount=   1
         Begin VB.ListBox lstDepartamento 
            BackColor       =   &H8000000F&
            Height          =   1230
            Left            =   960
            TabIndex        =   1
            Top             =   1620
            Width           =   3735
         End
         Begin MSFlexGridLib.MSFlexGrid msgTabla 
            Height          =   5895
            Left            =   -74280
            TabIndex        =   9
            Top             =   420
            Width           =   6795
            _ExtentX        =   11986
            _ExtentY        =   10398
            _Version        =   327680
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            AllowBigSelection=   -1  'True
            FocusRect       =   0
            ScrollBars      =   2
            SelectionMode   =   1
            AllowUserResizing=   1
         End
         Begin VB.Frame Frame2 
            Caption         =   "Selecci�n de Tipolog�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3255
            Left            =   960
            TabIndex        =   7
            Top             =   3120
            Width           =   6015
            Begin VB.CommandButton cmdBorrar 
               Height          =   375
               Left            =   900
               Picture         =   "AR1030.frx":0352
               Style           =   1  'Graphical
               TabIndex        =   3
               Top             =   420
               Width           =   375
            End
            Begin VB.CommandButton cmdInsertar 
               Height          =   375
               Left            =   480
               Picture         =   "AR1030.frx":065C
               Style           =   1  'Graphical
               TabIndex        =   2
               Top             =   420
               Width           =   375
            End
            Begin MSFlexGridLib.MSFlexGrid msgTipologias 
               Height          =   1755
               Left            =   480
               TabIndex        =   8
               Top             =   960
               Width           =   5055
               _ExtentX        =   8916
               _ExtentY        =   3096
               _Version        =   327680
               FixedCols       =   0
               BackColorBkg    =   -2147483633
               FocusRect       =   0
               ScrollBars      =   2
               SelectionMode   =   1
               AllowUserResizing=   1
            End
         End
         Begin VB.ComboBox cboUsuario 
            BackColor       =   &H00FFFF00&
            Height          =   315
            Left            =   960
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   840
            Width           =   6015
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   960
            TabIndex        =   11
            Top             =   1380
            Width           =   1200
         End
         Begin ComctlLib.ImageList imlMainSmall 
            Left            =   4800
            Top             =   -660
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   327682
            BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
               NumListImages   =   29
               BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":0966
                  Key             =   "main"
               EndProperty
               BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":0C80
                  Key             =   "rol"
               EndProperty
               BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":0F9A
                  Key             =   "function"
               EndProperty
               BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":12B4
                  Key             =   "window"
               EndProperty
               BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":15CE
                  Key             =   "report"
               EndProperty
               BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":18E8
                  Key             =   "externalreport"
               EndProperty
               BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":1C02
                  Key             =   "process"
               EndProperty
               BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":1F1C
                  Key             =   "new"
               EndProperty
               BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":2236
                  Key             =   "big"
               EndProperty
               BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":2550
                  Key             =   "small"
               EndProperty
               BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":286A
                  Key             =   "list"
               EndProperty
               BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":2B84
                  Key             =   "details"
               EndProperty
               BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":2E9E
                  Key             =   "exit"
               EndProperty
               BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":31B8
                  Key             =   "refresh"
               EndProperty
               BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":34D2
                  Key             =   "save"
               EndProperty
               BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":37EC
                  Key             =   "print"
               EndProperty
               BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":3B06
                  Key             =   "CheckOff"
               EndProperty
               BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":3E20
                  Key             =   "CheckOn"
               EndProperty
               BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":413A
                  Key             =   "first"
               EndProperty
               BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":4454
                  Key             =   "uno"
               EndProperty
               BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":476E
                  Key             =   "dos"
               EndProperty
               BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":47CC
                  Key             =   "previous"
               EndProperty
               BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":4AE6
                  Key             =   "undo"
               EndProperty
               BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":4E00
                  Key             =   "delete"
               EndProperty
               BeginProperty ListImage25 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":511A
                  Key             =   "filoff"
               EndProperty
               BeginProperty ListImage26 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":5434
                  Key             =   "New"
               EndProperty
               BeginProperty ListImage27 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":574E
                  Key             =   "filon"
               EndProperty
               BeginProperty ListImage28 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":5A68
                  Key             =   "last"
               EndProperty
               BeginProperty ListImage29 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "AR1030.frx":5D82
                  Key             =   "next"
               EndProperty
            EndProperty
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   960
            TabIndex        =   6
            Top             =   600
            Width           =   630
         End
      End
   End
   Begin VB.Menu mnuAccion 
      Caption         =   "&Datos"
      Begin VB.Menu optAccion 
         Caption         =   "&Guardar"
         Index           =   0
         Shortcut        =   ^G
      End
      Begin VB.Menu optAccion 
         Caption         =   "-"
         Index           =   1
      End
      Begin VB.Menu optAccion 
         Caption         =   "&Salir"
         Index           =   2
      End
   End
   Begin VB.Menu MnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu optRegistro 
         Caption         =   "&Primero        CTRL+Inicio"
         Enabled         =   0   'False
         Index           =   0
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Anterior       Re Pag."
         Enabled         =   0   'False
         Index           =   1
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Siguiente     Av Pag."
         Enabled         =   0   'False
         Index           =   2
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&�ltimo          CTRL+Fin"
         Enabled         =   0   'False
         Index           =   3
      End
   End
   Begin VB.Menu mnuAYuda 
      Caption         =   "&?"
      Begin VB.Menu optAyuda 
         Caption         =   "&Ayuda"
         Index           =   0
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmTiposEntrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Constantes para determinar la entrega
Const ENTREGADA = "S�"
Const NOENTREGADA = "No"

'Declaracion de variables

Dim rsTipologias    As rdoResultset
Dim rsUsuarios      As rdoResultset
Dim rsTabla           As rdoResultset
Dim rsEntrega       As rdoResultset
Dim rsDepartamentos As rdoResultset
Dim QyConsulta    As rdoQuery

Dim strCodUsuario As String


Private Sub cmdBorrar_Click()
If msgTipologias.row <> 0 Then
With msgTipologias
    If .ColSel = 3 Then
        If .TextMatrix(.row, 3) = "" Then
            .TextMatrix(.row, 2) = NOENTREGADA
            .TextMatrix(.row, 3) = NOENTREGADA
        Else
            .TextMatrix(.row, 2) = NOENTREGADA
            .TextMatrix(.row, 3) = ""
        End If
    End If
End With
End If
msgTipologias.col = 0
End Sub

Private Sub GUARDAR()
'Declaracion de variables
Dim SQLInsertar As String
Dim SQLBorrar As String
Dim cn As rdoConnection
Dim QyInsertar As rdoQuery
Dim QyBorrar As rdoQuery

Dim inti As Integer
Dim blnCambios As Boolean
Dim respuesta As Variant

'Control de datos obligatorios
If cboUsuario.ListIndex = -1 Then
    Call MsgBox("El campo Usuario es Obligatorio", vbOKOnly + vbInformation, "Guardar")
    cboUsuario.SetFocus
    Exit Sub
End If

'Si no se han efectuado cambios
blnCambios = False
For inti = 1 To msgTipologias.Rows - 1
    If msgTipologias.TextMatrix(inti, 3) <> "" Then
        blnCambios = True
        Exit For
    End If
Next
If blnCambios = False Then
    Call MsgBox("No se han detectado cambios. Nada que guardar", vbOKOnly + vbInformation, "Guardar")
    Exit Sub
Else
    respuesta = MsgBox("�Desea grabar los cambios realizados?", vbYesNoCancel + vbApplicationModal + vbQuestion, "Guardar")
    If respuesta = vbNo Then
        Call Formulario_Cargar
        Exit Sub
    ElseIf respuesta = vbCancel Then
        Exit Sub
    End If
End If

'Guardar los datos
Screen.MousePointer = vbHourglass
Set cn = objApp.rdoConnect
For inti = 1 To msgTipologias.Rows - 1
    If msgTipologias.TextMatrix(inti, 3) = ENTREGADA Then
        'Insertar
        SQLInsertar = "INSERT INTO AR0900 (SG02COD, AR00CODTIPOLOGIA)"
        SQLInsertar = SQLInsertar & "  VALUES (?, ?)"
        Set QyInsertar = cn.CreateQuery("", SQLInsertar)
        QyInsertar(0) = Trim(Right(cboUsuario.List(cboUsuario.ListIndex), 6))
        QyInsertar(1) = Trim(msgTipologias.TextMatrix(inti, 0))
        QyInsertar.Execute
        QyInsertar.Close
    ElseIf msgTipologias.TextMatrix(inti, 3) = NOENTREGADA Then
    'Borrar
        SQLBorrar = "DELETE FROM AR0900"
        SQLBorrar = SQLBorrar & "  WHERE SG02COD=? "
        SQLBorrar = SQLBorrar & "  AND AR00CODTIPOLOGIA=? "
        Set QyBorrar = cn.CreateQuery("", SQLBorrar)
        QyBorrar(0) = Trim(Right(cboUsuario.List(cboUsuario.ListIndex), 6))
        QyBorrar(1) = Trim(msgTipologias.TextMatrix(inti, 0))
        QyBorrar.Execute
        QyBorrar.Close
    End If
    Screen.MousePointer = vbNormal
Next
Call Form_Load
End Sub

Private Sub Guardar2()
'Declaracion de variables
Dim SQLInsertar As String
Dim SQLBorrar As String
Dim cn As rdoConnection
Dim QyInsertar As rdoQuery
Dim QyBorrar As rdoQuery

Dim inti As Integer
Dim blnCambios As Boolean
Dim respuesta As Variant

'Control de datos obligatorios
If cboUsuario.ListIndex = -1 Then
    Call MsgBox("El campo Usuario es Obligatorio", vbOKOnly + vbInformation, "Guardar")
    cboUsuario.SetFocus
    Exit Sub
End If
'Guardar los datos
Screen.MousePointer = vbHourglass
Set cn = objApp.rdoConnect
For inti = 1 To msgTipologias.Rows - 1
    If msgTipologias.TextMatrix(inti, 3) = ENTREGADA Then
        'Insertar
        SQLInsertar = "INSERT INTO AR0900 (SG02COD, AR00CODTIPOLOGIA)"
        SQLInsertar = SQLInsertar & "  VALUES (?, ?)"
        Set QyInsertar = cn.CreateQuery("", SQLInsertar)
        QyInsertar(0) = Trim(Right(cboUsuario.List(cboUsuario.ListIndex), 6))
        QyInsertar(1) = Trim(msgTipologias.TextMatrix(inti, 0))
        QyInsertar.Execute
        QyInsertar.Close
    ElseIf msgTipologias.TextMatrix(inti, 3) = NOENTREGADA Then
    'Borrar
        SQLBorrar = "DELETE FROM AR0900"
        SQLBorrar = SQLBorrar & "  WHERE SG02COD=? "
        SQLBorrar = SQLBorrar & "  AND AR00CODTIPOLOGIA=? "
        Set QyBorrar = cn.CreateQuery("", SQLBorrar)
        QyBorrar(0) = Trim(Right(cboUsuario.List(cboUsuario.ListIndex), 6))
        QyBorrar(1) = Trim(msgTipologias.TextMatrix(inti, 0))
        QyBorrar.Execute
        QyBorrar.Close
    End If
    Screen.MousePointer = vbNormal
Next
End Sub

Private Sub cmdInsertar_Click()
If msgTipologias.row <> 0 Then
With msgTipologias
    If .ColSel = 3 Then
        If .TextMatrix(.row, 3) = "" Then
            .TextMatrix(.row, 2) = ENTREGADA
            .TextMatrix(.row, 3) = ENTREGADA
        Else
            .TextMatrix(.row, 2) = ENTREGADA
            .TextMatrix(.row, 3) = ""
        End If
    End If
End With
End If
msgTipologias.col = 0
End Sub

Private Sub Form_Load()
sstTiposEntrega.Tab = 0
cmdInsertar.Enabled = False
cmdBorrar.Enabled = False
lstDepartamento.Clear
'Contruir Grid de detalle
With msgTipologias
    .Redraw = False
    .Width = 3000
    .Clear
    .Rows = 1
    .cols = 4
    .row = 0
    .col = 0
    .ColWidth(0) = 0 'Codigo de la tipolog�a
    .col = 1
    .ColWidth(1) = 2000
    .TextMatrix(0, 1) = "Tipolog�a"
    .col = 2
    .ColWidth(2) = 1000
    .CellAlignment = 4 'Centro, Centro
    .TextMatrix(0, 2) = "Entrega"
    .col = 3
    .ColWidth(3) = 0 'Entrega
    .col = 0
    .Redraw = True
End With

'Seleccionar los usuarios con sus correspondientes entregas
'por tipolog�a
Call Cargar_Tabla
'Seleccionar los usuarios
Call Cargar_Usuarios
End Sub

Private Sub Cargar_Tabla()

'Declaracion de variables
Dim SQLTabla As String
Dim SQLTipologias As String
Dim intCols As Integer
Dim intCols2 As Integer
Dim inti As Integer

With msgTabla
.Redraw = False
.Clear
.Width = 6000
.cols = 4
.Rows = 1
.row = 0
.col = 0
.ColWidth(0) = 0 'Codigo Usuario
.col = 1
.ColWidth(1) = 4000
.TextMatrix(.row, .col) = "Usuario"
.col = 2
.ColWidth(2) = 0 'Codigo Tipologia
.col = 3
.ColWidth(3) = 2000
.TextMatrix(.row, .col) = "Tipolog�a"

End With
'Seleccionar los usuarios y sus entregas por tipolog�a
SQLTabla = "SELECT U.SG02APE1||' '|| U.SG02APE2||', '|| U.SG02NOM, U.SG02COD, T.AR00CODTIPOLOGIA, T.AR00DESTIPOLOGIA "
SQLTabla = SQLTabla & " FROM AR0000 T,   AR0900 E ,SG0200 U"
SQLTabla = SQLTabla & " WHERE U.AD30CODCATEGORIA IN (1,6,5,9) "
SQLTabla = SQLTabla & " AND E.SG02COD (+)= U.SG02COD"
SQLTabla = SQLTabla & " AND T.AR00CODTIPOLOGIA(+)=E.AR00CODTIPOLOGIA "
SQLTabla = SQLTabla & " ORDER BY 1,2 "
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLTabla)
Set rsTabla = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsTabla.RowCount > 0 Then
rsTabla.MoveFirst
'Seguir llenando la grid de tabla
With msgTabla
inti = 1
    While Not rsTabla.EOF
        .Rows = .Rows + 1
        .row = inti
        .col = 0
        .TextMatrix(.row, .col) = Trim(rsTabla(1))
        .col = 1
        .TextMatrix(.row, .col) = Trim(rsTabla(0))
        .col = 2
        If IsNull(rsTabla(2)) = False Then
        .TextMatrix(.row, .col) = Trim(rsTabla(2))
        Else
        .TextMatrix(.row, .col) = ""
        End If
        .col = 3
        If IsNull(rsTabla(3)) = False Then
            .TextMatrix(.row, .col) = Trim(rsTabla(3))
        Else
            .TextMatrix(.row, .col) = ""
        End If
         rsTabla.MoveNext
        inti = inti + 1
    Wend

End With
  tlbToolBar.Buttons(5).Enabled = True
  optRegistro(0).Enabled = True
  tlbToolBar.Buttons(6).Enabled = True
  optRegistro(1).Enabled = True
  tlbToolBar.Buttons(7).Enabled = True
  optRegistro(2).Enabled = True
  tlbToolBar.Buttons(8).Enabled = True
  optRegistro(3).Enabled = True

Else
  tlbToolBar.Buttons(5).Enabled = False
  optRegistro(0).Enabled = False
  tlbToolBar.Buttons(6).Enabled = False
  optRegistro(1).Enabled = False
  tlbToolBar.Buttons(7).Enabled = False
  optRegistro(2).Enabled = False
  tlbToolBar.Buttons(8).Enabled = False
  optRegistro(3).Enabled = False
End If
msgTabla.col = 0
msgTabla.Redraw = True
End Sub

Private Sub Cargar_Usuarios()

'Declaracion de variables
Dim SQLUsuarios As String
Dim strNomUsuario As String
Dim inti As Integer

cboUsuario.Clear

'Seleccionar los usuarios
SQLUsuarios = "SELECT SG02APE1||' '|| SG02APE2||', '|| SG02NOM, SG02COD "
SQLUsuarios = SQLUsuarios & " FROM SG0200 "
SQLUsuarios = SQLUsuarios & " WHERE AD30CODCATEGORIA IN (1,5,6,9) "
SQLUsuarios = SQLUsuarios & " ORDER BY 1 "
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLUsuarios)
Set rsUsuarios = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsUsuarios.RowCount > 0 Then
rsUsuarios.MoveFirst
inti = 0
    While Not rsUsuarios.EOF
        cboUsuario.AddItem Trim(rsUsuarios(0)) & Space(200) & Trim(rsUsuarios(1))
        cboUsuario.ItemData(cboUsuario.NewIndex) = inti
        inti = inti + 1
        rsUsuarios.MoveNext
    Wend
End If
End Sub

Private Sub cboUsuario_Click()
If cboUsuario.ListIndex <> -1 Then
    'Recoger el usuario
    strCodUsuario = Trim(Right(cboUsuario.List(cboUsuario.ListIndex), 6))
    'Cargar las tipologias y entregas para ese usuario
    Call Cargar_Tipologias
    Call msgTipologias_Click
    Call Cargar_Departamentos
End If
msgTipologias.col = 0
cmdInsertar.Enabled = False
cmdBorrar.Enabled = False
End Sub

Private Sub cboUsuario_DropDown()
Dim blnCambios4 As Boolean
Dim respuesta As Variant
Dim inti As Integer

'Si se han efectuado cambios
blnCambios4 = False
For inti = 1 To msgTipologias.Rows - 1
    If msgTipologias.TextMatrix(inti, 3) <> "" Then
        blnCambios4 = True
        Exit For
    End If
Next
If blnCambios4 = True Then
    respuesta = MsgBox("�Desea grabar los cambios realizados?", vbYesNoCancel + vbApplicationModal + vbQuestion, "Guardar")
    If respuesta = vbYes Then
        Call Guardar2
        Call Formulario_Cargar
    ElseIf respuesta = vbNo Then
        Call Formulario_Cargar
    Else
        msgTipologias.SetFocus
    End If
End If
End Sub


Private Sub Cargar_Tipologias()

'Decaracion de variables
Dim inti As Integer
Dim SQLTipologias As String
Dim strCodTipologia As String
Dim strDesTipologia As String

msgTipologias.Rows = 1
'Seleccionar las tipolog�as
SQLTipologias = "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00CODTIPOLOGIA"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLTipologias)
Set rsTipologias = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsTipologias.RowCount > 0 Then
    rsTipologias.MoveFirst
    inti = 1
    While Not rsTipologias.EOF
        With msgTipologias
            .Redraw = False
            .Rows = .Rows + 1
            .row = inti
            strCodTipologia = Trim(rsTipologias("ar00codtipologia"))
            strDesTipologia = Trim(rsTipologias("ar00destipologia"))
            .TextMatrix(inti, 0) = strCodTipologia
            .TextMatrix(inti, 1) = strDesTipologia
            'Procedimiento que evalua las entregas para el usuario seleccionado
            Call Cargar_Entrega(strCodTipologia)
        End With
        rsTipologias.MoveNext
        inti = inti + 1
    Wend
    rsTipologias.Close
End If
msgTipologias.col = 0
msgTipologias.Redraw = True
End Sub

Private Sub Cargar_Entrega(strCodTipologia As String)

'Declaracion de variables
Dim SQLEntrega As String

'Seleccionar las tipolog�as
SQLEntrega = " SELECT AR00CODTIPOLOGIA FROM AR0900 "
SQLEntrega = SQLEntrega & " WHERE SG02COD=? "
SQLEntrega = SQLEntrega & " AND AR00CODTIPOLOGIA=? "
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLEntrega)
QyConsulta(0) = strCodUsuario
QyConsulta(1) = strCodTipologia
Set rsEntrega = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsEntrega.RowCount > 0 Then
    rsEntrega.MoveFirst
    msgTipologias.col = 2
    msgTipologias.CellAlignment = 4
    msgTipologias.TextMatrix(msgTipologias.row, 2) = ENTREGADA
Else
    msgTipologias.col = 2
    msgTipologias.CellAlignment = 4
    msgTipologias.TextMatrix(msgTipologias.row, 2) = NOENTREGADA
End If
rsEntrega.Close
msgTipologias.col = 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim blnCambios2 As Boolean
Dim respuesta As Variant
Dim inti As Integer

'Si se han efectuado cambios
blnCambios2 = False
For inti = 1 To msgTipologias.Rows - 1
    If msgTipologias.TextMatrix(inti, 3) <> "" Then
        blnCambios2 = True
        Exit For
    End If
Next
If blnCambios2 = True Then
    respuesta = MsgBox("�Desea grabar los cambios realizados?", vbYesNoCancel + vbApplicationModal + vbQuestion, "Guardar")
    If respuesta = vbYes Then
        Call Guardar2
        Cancel = 0
    ElseIf respuesta = vbNo Then
        Cancel = 0
    Else
        Cancel = 1
    End If
Else
    Cancel = 0
End If
End Sub

Private Sub msgTabla_Click()
If msgTabla.Rows > 1 Then
With msgTabla
    .col = 0
    .rowsel = .row
    .ColSel = .cols - 1
End With
End If
End Sub

Private Sub msgTabla_DblClick()

'Declaracion de variables
Dim inti As Integer
    If msgTabla.Rows > 1 Then
    sstTiposEntrega.Tab = 0
    'Seleccionamos en la combo de usuarios el
    'correspondiente al seleccionado en la tabla
        For inti = 0 To cboUsuario.ListCount - 1
            If Trim(Right(cboUsuario.List(inti), 6)) = Trim(msgTabla.TextMatrix(msgTabla.row, 0)) Then
                cboUsuario.ListIndex = inti
            End If
        Next
End If
End Sub

Private Sub msgTipologias_Click()
If msgTipologias.row <> 0 Then
With msgTipologias
    If .TextMatrix(.row, 2) = ENTREGADA Then
        cmdInsertar.Enabled = False
        cmdBorrar.Enabled = True
    Else
        cmdInsertar.Enabled = True
        cmdBorrar.Enabled = False
    End If
  .col = 0
  .rowsel = .row
  .ColSel = .cols - 1
End With
End If
End Sub

Private Sub msgTipologias_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call msgTipologias_Click
End Sub

Private Sub optAccion_Click(Index As Integer)
Select Case Index
Case 0
    Call GUARDAR
Case 2
    Unload Me
End Select
End Sub

Private Sub optAyuda_Click(Index As Integer)
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 2)
End Sub

Private Sub optRegistro_Click(Index As Integer)
Select Case Index
  Case 0
    msgTabla.row = 1
  Case 1
    If msgTabla.row > 1 Then
      msgTabla.row = msgTabla.row - 1
    End If
  Case 2
    If msgTabla.row < msgTabla.Rows - 1 Then
      msgTabla.row = msgTabla.row + 1
    End If
  Case 3
    msgTabla.row = msgTabla.Rows - 1
End Select
End Sub

Private Sub sstTiposEntrega_Click(PreviousTab As Integer)

Dim blnCambios3 As Boolean
Dim respuesta As Variant
Dim inti As Integer

If sstTiposEntrega.Tab = 1 Then
    'Si se han efectuado cambios
    blnCambios3 = False
    For inti = 1 To msgTipologias.Rows - 1
        If msgTipologias.TextMatrix(inti, 3) <> "" Then
            blnCambios3 = True
            Exit For
        End If
    Next
    If blnCambios3 = True Then
        sstTiposEntrega.Tab = 0
        respuesta = MsgBox("�Desea grabar los cambios realizados?", vbYesNoCancel + vbApplicationModal + vbQuestion, "Guardar")
        If respuesta = vbYes Then
            Call Guardar2
            Call Formulario_Cargar
            sstTiposEntrega.Tab = 1
        ElseIf respuesta = vbNo Then
            Call Formulario_Cargar
            sstTiposEntrega.Tab = 1
        End If
    End If
End If
End Sub

Private Sub tlbToolBar_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Index
  Case 2  'Grabar
    Call optAccion_Click(0)
  Case 5 'Primero
    msgTabla.row = 1
    Call msgTabla_Click
  Case 6 'Anterior
    If msgTabla.row > 1 Then
      msgTabla.row = msgTabla.row - 1
      Call msgTabla_Click
    End If
  Case 7 'Siguiente
    If msgTabla.row < msgTabla.Rows - 1 Then
      msgTabla.row = msgTabla.row + 1
      Call msgTabla_Click
    End If
  Case 8 'ultimo
    msgTabla.row = msgTabla.Rows - 1
    Call msgTabla_Click
  Case 10 'Salir
    Call optAccion_Click(2)
End Select
End Sub
Private Sub Formulario_Cargar()
cmdInsertar.Enabled = False
cmdBorrar.Enabled = False
lstDepartamento.Clear
'Contruir Grid de detalle
With msgTipologias
    .Redraw = False
    .Width = 3000
    .Clear
    .Rows = 1
    .cols = 4
    .row = 0
    .col = 0
    .ColWidth(0) = 0 'Codigo de la tipolog�a
    .col = 1
    .ColWidth(1) = 2000
    .TextMatrix(0, 1) = "Tipolog�a"
    .col = 2
    .ColWidth(2) = 1000
    .CellAlignment = 4 'Centro, Centro
    .TextMatrix(0, 2) = "Entrega"
    .col = 3
    .ColWidth(3) = 0 'Entrega
    .col = 0
    .Redraw = True
End With

'Seleccionar los usuarios con sus correspondientes entregas
'por tipolog�a
Call Cargar_Tabla
'Seleccionar los usuarios
Call Cargar_Usuarios
End Sub

Private Sub Cargar_Departamentos()

'Decaracion de variables
Dim inti As Integer
Dim SQLDepartamentos As String
lstDepartamento.Clear
'Seleccionar los Departamentos
SQLDepartamentos = "SELECT D.AD02DESDPTO "
SQLDepartamentos = SQLDepartamentos & " FROM AD0200 D, AD0300 A "
SQLDepartamentos = SQLDepartamentos & " WHERE A.SG02COD=? "
SQLDepartamentos = SQLDepartamentos & " AND A.AD03FECFIN IS NULL "
SQLDepartamentos = SQLDepartamentos & " AND D.AD02CODDPTO=A.AD02CODDPTO "
SQLDepartamentos = SQLDepartamentos & " ORDER BY 1 "
Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQLDepartamentos)
QyConsulta(0) = Trim(Right(cboUsuario.List(cboUsuario.ListIndex), 6))
Set rsDepartamentos = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rsDepartamentos.RowCount > 0 Then
    rsDepartamentos.MoveFirst
    While Not rsDepartamentos.EOF
        lstDepartamento.AddItem (rsDepartamentos(0))
        rsDepartamentos.MoveNext
    Wend
End If
rsDepartamentos.Close
End Sub
