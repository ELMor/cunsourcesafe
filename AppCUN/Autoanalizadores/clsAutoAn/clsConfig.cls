VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const mstrRoot       As String = "SOFTWARE\SYSECA Bilbao\"
Dim mlngRoot    As Long
Dim mstrKeyName As String


' crea una nueva clave en el registro
Private Sub RegCreate()
  Dim lngNewKey As Long
  Dim lngRetVal As Long

  lngRetVal = RegCreateKeyEx(mlngRoot, mstrKeyName, 0&, vbNullString, _
                             cwWinRegOptionNonVolatile, cwWinRegKeyAllAccess, _
                             0&, lngNewKey, lngRetVal)
  Call RegCloseKey(lngNewKey)
End Sub

' cambia el valor de una clave del registro
Private Sub RegSet(ByVal strValueName As String, _
                   ByVal vntValue As Variant)
  Dim lngKey As Long

  Call RegOpenKeyEx(mlngRoot, mstrKeyName, 0, cwWinRegKeyAllAccess, lngKey)
  Call RegSetValueExString(lngKey, strValueName, 0&, _
                           cwWinRegStringZ, vntValue, Len(vntValue))
  Call RegCloseKey(lngKey)
End Sub

' devuelve el valor de una clave del registro
Private Function RegGet(ByVal strValueName As String) As Variant
  Dim lngKey   As Long
  Dim lngType  As Long
  Dim lngRet   As Long
  Dim strValue As String
  Dim lngSize  As Long
  
  Call RegOpenKeyEx(mlngRoot, mstrKeyName, 0, _
                    cwWinRegKeyAllAccess, lngKey)
  On Error GoTo RegGetValueError
  Call RegQueryValueExNULL(lngKey, strValueName, 0&, _
                           lngType, 0&, lngSize)
  strValue = String(lngSize, 0)
  lngRet = RegQueryValueExString(lngKey, strValueName, 0&, _
                                 lngType, strValue, lngSize)
  If lngRet = cwWinRegErrorNone Then
    RegGet = Left(strValue, lngSize - IIf(Asc(Mid(strValue, lngSize, 1)) = 0, 1, 0))
  Else
    RegGet = Empty
  End If
RegGetValueError:
  Call RegCloseKey(lngKey)
End Function

' determina si existe una clave en el registro
Private Function RegExist(ByVal strValueName As String) As Boolean
  Dim lngKey  As Long
  Dim lngType As Long
  Dim lngRet  As Long
  Dim lngSize As Long

  Call RegOpenKeyEx(mlngRoot, mstrKeyName, 0, cwWinRegKeyAllAccess, lngKey)
  On Error GoTo RegGetValueError
  lngRet = RegQueryValueExNULL(lngKey, strValueName, 0&, lngType, 0&, lngSize)
  RegExist = (lngRet = cwWinRegErrorNone)
RegGetValueError:
  Call RegCloseKey(lngKey)
End Function

' fuerza una escritura en el registro. Si el valor no existe lo crea
Private Sub RegForzeSet(ByVal strValueName As String, _
                        ByVal vntValue As Variant)
  If Not RegExist(strValueName) Then
    Call RegCreate
  End If
  Call RegSet(strValueName, vntValue)
End Sub

' recupera los valores por defecto del registro
Friend Sub Load()
  On Error GoTo Autoanerror
  
  With objAutoan
    mlngRoot = cwWinRegHKeyLocalMachine
    mstrKeyName = mstrRoot & objAutoan.objUserApp.ProductName & "\" & .strNetUser & "\"
      If IsStrEmpty(.strUserName) Then
        .strUserName = RegGet("UserName")
      End If
      If IsStrEmpty(.strDataSource) Then
        .strDataSource = RegGet("DataSource")
      End If
      On Error Resume Next
      .intUserEnv = CLng(RegGet("UserEnv"))
 
      On Error GoTo Autoanerror
    
  End With
'  mlngRoot = cwWinRegHKeyLocalMachine
  Exit Sub
  
Autoanerror:
  Call objError.InternalError(Me, "Load", " Info Registro")
End Sub

' guarda los valores por defecto en el registro
Friend Sub Save()
  On Error GoTo Autoanerror
      
  With objAutoan
    mlngRoot = cwWinRegHKeyLocalMachine
    mstrKeyName = mstrRoot & .objUserApp.ProductName & "\" & .strNetUser & "\"
    Call RegForzeSet("UserName", .strUserName)
    Call RegForzeSet("DataSource", .strDataSource)
    Call RegForzeSet("UserEnv", CStr(.intUserEnv))
  End With
  Exit Sub
  
Autoanerror:
  Call objError.InternalError(Me, "Save", "Salva Registro")
End Sub

Friend Sub LoadKeyFile()
  On Error GoTo cwIntError
  
  mlngRoot = cwWinRegHKeyLocalMachine
  mstrKeyName = mstrRoot & objAutoan.objUserApp.ProductName & "\" & objAutoan.strNetUser & "\"
    objAutoan.strKeyFile = RegGet("KeyFile")
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "LoadKeyFile")
End Sub

Friend Sub SaveKeyFile()
  On Error GoTo cwIntError
  
  mlngRoot = cwWinRegHKeyLocalMachine
  mstrKeyName = mstrRoot & objAutoan.objUserApp.ProductName & "\" & objAutoan.strNetUser & "\"
'  Call RegForzeSet(mstrCWSecCoded, "SYSECA Bilbao")
  Call RegForzeSet("KeyFile", objAutoan.strKeyFile)
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "SaveKeyFile")
End Sub


