VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsResultAuto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'Resultados definidos para el autoanalizador: se realizan pruebas, algunas de las cuales
'reportan varios resultados.
Public strCodResultAuto As String
Public intCodResult As Integer
Public strDesResultado As String
Public intCodUnidad As Integer
Public strProperty1 As String
Public strProperty2 As String
Public strProperty3 As String
Public intDecimales As Integer
Public intTiempo As Integer
