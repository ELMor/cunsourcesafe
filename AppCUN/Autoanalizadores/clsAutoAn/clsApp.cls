VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public strUserName        As String               ' usuario
Public strPassword        As String               ' password
Public strPasswordOld     As String               ' password con la vieja encriptacion
Public strDataSource      As String               ' ds de la base de datos
Public strReportsPath     As String               ' path de los reports
Public rdoEnv             As rdoEnvironment       ' el environment de rdo
Public WithEvents rdoConnect         As rdoConnection        ' el connect de rdo
Attribute rdoConnect.VB_VarHelpID = -1
Public blnConnected       As Boolean              ' hay conexión a la Base de Datos?
Public strPathMS          As String
Public strNetUser         As String
Public blnAskPassword     As Boolean
Public blnUseRegistry     As Boolean
Public blnReg             As Boolean           ' Activa o desactiva el registro de las acciones
Public dlgCommon          As Object           ' el common dialog de la aplicación

' valores por defecto para la clase AppInfo
Private Sub Class_Initialize()
  Dim strUser As String
  Dim lngSize As Long
  
  blnUseRegistry = False
  blnReg = True
  
  strUser = Space(255) & Chr(0)
  lngSize = Len(strUser)
  Call WNetGetUser(Empty, strUser, lngSize)
  If lngSize > 0 Then
    strUser = Left(strUser, lngSize - 1)
  Else
    strUser = ""
  End If
  strNetUser = strUser
  
  blnAskPassword = True
  
  End Sub



' crea la información de la aplicación
Public Function CreateInfo() As Boolean
  Dim frmUser       As New frmCWUser
  Dim blnNewVersion As Boolean
  Dim blnAsk        As Boolean
  Dim MiPath As String
  Dim objConfig     As New clsConfig

  On Error GoTo Autoanerror
  MiPath = Trim$(objAutoan.objUserApp.Path)
'  strReportsPath = IIf(Right$(MiPath, 1) = "\", Left$(MiPath, Len(MiPath) - 1), MiPath) & msgReportPath

  If blnUseRegistry Then
    Call objConfig.Load
    Set objConfig = Nothing
  End If
  
  Set rdoEnv = rdoEnvironments(0)
  
  blnAsk = True
  If Not blnAskPassword Then
    blnConnected = FConnect(strDataSource, strUserName, strPassword)
'    If Not blnConnected Then
'      blnConnected = FConnect(strDataSource, strUserName, strPasswordOld)
'      strPassword = strPasswordOld
'    End If
    blnAsk = Not blnConnected
  End If
  
  If blnAsk Then
    Load frmUser
    With frmUser
      Call .Show(vbModal)
      blnConnected = .blnConnection
    End With
    Unload frmUser
    Set frmUser = Nothing
    DoEvents
  End If
  
  If blnConnected Then
'    Call CheckInstance
'    Call CreateImageList
'    blnNewVersion = CheckVersion
  End If
  
  CreateInfo = blnConnected 'And Not blnNewVersion
  Exit Function

Autoanerror:
  Call objError.InternalError(Me, "CreateInfo", "CreateInfo")
End Function



' realiza la conexión a la base de datos
Public Function FConnect(ByVal strODBC As String, _
                        ByVal strUser As String, _
                        ByVal strPassword As String) As Boolean
  Dim blnNotError As Boolean
  Dim intReturned As Integer
'  objApp.objLog.LogWrite ("Connect started...")
'MsgBox "connect Started....."
    
  Screen.MousePointer = vbHourglass
  On Error Resume Next
  With objApli.rdoEnv
     .UserName = strUser
     .Password = strPassword
     .CursorDriver = rdUseOdbc
    Set objApli.rdoConnect = .OpenConnection("", rdDriverNoPrompt, False, "DSN=" & strODBC)
'    blnNotError = (Err.Number = 0)
    blnNotError = (Err = 0)

  End With
  Screen.MousePointer = vbDefault
 
  FConnect = blnNotError
  MsgBox "Connect Finished...."
  
'  objApp.objLog.LogWrite ("Connect ending...")
End Function

' elimina toda la información sobre la aplicación
Public Sub RemoveInfo()
 
  On Error GoTo Autoanerror
    
  If blnUseRegistry Then
'    Call objConfig.Save
  End If
  
  If blnConnected Then
    rdoConnect.Close
    rdoEnv.Close
  End If
  
  Set objUserApp = Nothing
  Set objUserScreen = Nothing
  
  Set frmFormMain = Nothing
'  Call objGen.RemoveCollection(mcllCache)
  
  blnConnected = False
  Exit Sub
  
Autoanerror:
  Call objError.InternalError(Me, "RemoveInfo", "RemoveInfo")
End Sub


