VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsActuacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'Pruebas en la lista de trabajo
Private MiIntFila As String 'n� de fila en el grid 1
Private MiStrCodAct As String '4
Private MiStrDesAct As String '5
'Private MiStrNumAct As String '6
Private MiStrCaso As String
Private MiStrSecuencia As String
Private MiIntCarpeta As Integer
Private MiStrCodActAuto As String '8
Private MiStrEstado As constEstado  '9
Private MiBlnUrgente As Boolean '11
Private MiIntRepeticion As Integer
'Private MiintRepeticion As Integer
Private MiStrProperty1 As String '9
Private MiStrProperty2 As String '11
Private MiStrProperty3 As String '11
Private MiStrCodMuestra As String
Private MiLngNumLista As Long
Private intMiDilucion  As Double

Public intFila As String  'n� de fila en el grid 1
Public BkMrk As Variant
Public ColPruebasResultados As Collection
Public Muestra As Object ' clsMuestra '

Public Property Let intDilucion(val As Double)  '15
  intMiDilucion = Format(val, "0.0")
  Call EscribirEnColumna(ctePruebaDILUCION, CStr(intMiDilucion))
End Property

Public Property Get intDilucion() As Double  '15
  intDilucion = intMiDilucion
End Property

Public Sub ColPruebasResultados_Add(objResultado As clsResultado, Optional strKey As String)
  ColPruebasResultados.Add objResultado, strKey
  objResultado.strCodAct = MiStrCodAct
  Call EscribirEnColumna(ctePruebaCODMUESTRAINVISIBLE, strCodMuestra)
End Sub

Public Sub ElegirFila()
Dim objResultado As clsResultado
  For Each objResultado In ColPruebasResultados
    objResultado.ElegirFila
  Next
End Sub

Public Sub QuitarPrueba()
  Dim objResultado As clsResultado

  For Each objResultado In ColPruebasResultados
    ColPruebasResultados.Remove objResultado.codResultado
  Next
  objAutoan.ColListasDeTrabajo(CStr(MiLngNumLista)).colActuaciones.Remove Muestra.strCodMuestra & "/" & strCodAct
  If objAutoan.ColListasDeTrabajo(CStr(MiLngNumLista)).colActuaciones.Count = 0 Then
    objAutoan.ColListasDeTrabajo.Remove CStr(MiLngNumLista)
  End If
  With Muestra.ColMuestrasPruebas
    .Remove strCodAct
    If .Count = 0 Then Muestra.EliminarMuestra
  End With
End Sub

Private Sub EscribirEnColumna(code As constColumnaGridResultados, valor As String)
Dim Colum As Column
On Error GoTo AutoanError

  With frmAutoan.ssdgPruebas
    For Each Colum In .Columns
      If Colum.TagVariant = code Then
'        .MoveFirst
'        .MoveRecords intFila
        .Bookmark = intFila
        Colum.Text = valor
        Exit For
      End If
    Next
  End With
  Exit Sub
    
AutoanError:
    Call objError.InternalError(Me, "Escritura en Columna de Grid", "Escritura en Grid")
End Sub

Public Property Let strSecuencia(ByVal vData As String)
'se usa cuando se asigna un valor a una propiedad, en el lado izquierdo de la asignaci�n.
'Syntax: X.strSecuencia = 5
    MiStrSecuencia = vData
End Property

Public Property Get strSecuencia() As String
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignaci�n.
'Syntax: Debug.Print X.strSecuencia
    strSecuencia = MiStrSecuencia
End Property

Public Property Let intCarpeta(val As Integer)
Dim strSQL$
Dim Qry As rdoQuery
Dim Rs As rdoResultset
On Error GoTo AutoanError

  If MiIntCarpeta <> val Then
  'Comprobamos que Esa Prueba Se Puede Pasar a Esa Carpeta
    strSQL = "select ccarpeta from pruebascarpetas where cprueba = ? and ccarpeta = ?"
    Set Qry = objAutoan.rdoConnect.CreateQuery("", strSQL)
    Qry(0) = strCodAct
    Qry(1) = val
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs.EOF Then
      strEstado = cteSINPROGRAMAR
      strSQL = "Update PruebaAsistencia PA set cCarpeta = ? " _
      & " Where Historia = " & Muestra.lngHistoria & " And Caso=" & strCaso _
      & " And Secuencia = " & strSecuencia & " And NRepeticion = " & intRepeticion
      Set Qry = objAutoan.rdoConnect.CreateQuery("", strSQL)
      Qry(0) = val 'cCarpeta
      Qry.Execute
      
      strSQL = "select cCarpeta from Carpetas where cCarpeta = ? and cAutoanalizador = ?"
      Set Qry = objAutoan.rdoConnect.CreateQuery("", strSQL)
      Qry(0) = val
      Qry(1) = objAutoan.intCodAutoAn
      Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Rs.EOF Then QuitarPrueba
    End If
  End If
  Exit Property
  
AutoanError:
    Call objError.InternalError(Me, "Cambio de carpeta " & MiStrNumAct & vbCrLf & "SQL: " & Sql, "Cambio de Estado")
End Property

Public Property Get intCarpeta() As Integer
  intCarpeta = MiIntCarpeta
End Property

Public Property Let strCaso(ByVal vData As String)
'se usa cuando se asigna un valor a una propiedad, en el lado izquierdo de la asignaci�n.
'Syntax: X.strCaso = 5
    MiStrCaso = vData
End Property

Public Property Get strCaso() As String
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignaci�n.
'Syntax: Debug.Print X.strCaso
    strCaso = MiStrCaso
End Property

Public Property Let strCodAct(val As String)  '4
  MiStrCodAct = val
  Call EscribirEnColumna(ctePruebaCODACTUACION, val)
  Call EscribirEnColumna(cteResultadoCODACTUACIONINVISIBLE, val)
End Property

Public Property Let strNumAct(val As String)   '6
  MiStrNumAct = val
  Call EscribirEnColumna(ctepruebaNUMACTUACION, val)
End Property

Public Property Let strCodActAuto(val As String)   '8
  MiStrCodActAuto = val
End Property

Public Property Let strDesAct(val As String)   '5
  MiStrDesAct = val
  Call EscribirEnColumna(ctePruebaDESCACTUACION, val)
End Property

Public Property Let strEstado(val As constEstado)     '9
Dim blnNotFirst As Boolean 'para que la 1� vez no intente ningun cambio de estado
Dim objResultado As clsResultado
Dim blnIncompleta As Boolean
Dim DescEstado As String
Dim EstadoResultado As constEstado
On Error GoTo AutoanError

  If val <> MiStrEstado Then
    DescEstado = strDescEstado(val)
    Call EscribirEnColumna(ctePruebaESTADO, DescEstado)
    blnNotFirst = (MiStrEstado <> 0)
    MiStrEstado = val
    objAutoan.EscribirLog "EstadoPrueba (" & Muestra.strCodMuestra & ", " & strCodAct & ") = " & DescEstado
    If blnNotFirst Then CambioEstado
  End If
  Select Case val
    Case cteSINPROGRAMAR: Muestra.strEstado = cteSINPROGRAMAR
    Case cteREALIZADA
      If ColPruebasResultados.Count > 0 Then
        For Each objResultado In ColPruebasResultados
          EstadoResultado = objResultado.strEstado
          If EstadoResultado <> cteREALIZADA _
          And EstadoResultado <> cteFUERADERANGO _
          And EstadoResultado <> cteAREPETIR Then
            blnIncompleta = True: Exit For
          End If
        Next
        If Not blnIncompleta Then
          MiStrEstado = cteREALIZACIONCOMPLETA
          Call EscribirEnColumna(ctePruebaESTADO, strDescEstado(MiStrEstado))
        End If
      End If
      Muestra.strEstado = Muestra.strEstado 'IIf(Muestra.strEstado <> cteFUERADERANGO, cteREALIZADA, Muestra.strEstado)
    Case cteFUERADERANGO
      If ColPruebasResultados.Count > 0 Then
        For Each objResultado In ColPruebasResultados
          EstadoResultado = objResultado.strEstado
          If EstadoResultado <> cteREALIZADA _
          And EstadoResultado <> cteFUERADERANGO _
          And EstadoResultado <> cteAREPETIR Then
            blnIncompleta = True: Exit For
          End If
        Next
        If Not blnIncompleta Then
          MiStrEstado = cteFUERADERANGOFIN
          Call EscribirEnColumna(ctePruebaESTADO, strDescEstado(MiStrEstado))
        End If
      End If
      Muestra.strEstado = cteFUERADERANGO
    Case cteAREPETIR
      If ColPruebasResultados.Count > 0 Then
        For Each objResultado In ColPruebasResultados
          EstadoResultado = objResultado.strEstado
          If EstadoResultado <> cteREALIZADA _
          And EstadoResultado <> cteFUERADERANGO _
          And EstadoResultado <> cteAREPETIR Then
            blnIncompleta = True: Exit For
          End If
        Next
        If Not blnIncompleta Then
          MiStrEstado = cteAREPETIRFIN
          Call EscribirEnColumna(ctePruebaESTADO, strDescEstado(MiStrEstado))
        End If
      End If
      Muestra.strEstado = cteAREPETIR
    Case cteACEPTADA
      If Muestra.strEstado = cteSINPROGRAMAR Then Muestra.strEstado = cteACEPTADA
    Case cteCOMPROBADA
      objAutoan.ColListasDeTrabajo(CStr(lngNumLista)).Estado = cteLISTAPVALIDTECNI
  End Select
  Exit Property
  
AutoanError:
    Call objError.InternalError(Me, "Cambio de Estado del N�Actuacion " & MiStrNumAct & vbCrLf & "SQL: " & Sql, "Cambio de Estado")
End Property

Public Property Let strProperty1(val As String)   '9
  If val <> MiStrProperty1 Then
    MiStrProperty1 = val
    Call EscribirEnColumna(ctePruebaPROPERTY1, val)
  End If
End Property

Public Property Let strProperty2(val As String)   '9
  If val <> MiStrProperty2 Then
    MiStrProperty2 = val
    Call EscribirEnColumna(ctePruebaPROPERTY2, val)
  End If
End Property

Public Property Let strProperty3(val As String)   '9
  If val <> MiStrProperty3 Then
    MiStrProperty3 = val
    Call EscribirEnColumna(ctePruebaPROPERTY3, val)
  End If
End Property

Public Property Let blnUrgente(val As Boolean)   '11
  MiBlnUrgente = val
  Call EscribirEnColumna(ctePruebaURGENTE, IIf(val, "SI", "NO"))
End Property

Public Property Let intRepeticion(val As Integer)
Dim objResultado As clsResultado
On Error GoTo AutoanError

  If MiIntRepeticion > 0 Then
    objAutoan.EscribirLog "IntRepeticion => " & val
    For Each objResultado In ColPruebasResultados
      objResultado.strEstado = cteSINPROGRAMAR
      objResultado.ResetResultado
    Next
    Sql = "UPDATE pruebaAsistencia" _
    & " SET estado = " & ctePRUEBAREPETIDA _
    & " WHERE Historia = ? and Caso = ? " _
    & " and Secuencia = ? and NRepeticion = ?"
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    Qry(0) = Muestra.lngHistoria
    Qry(1) = strCaso
    Qry(2) = strSecuencia
    Qry(3) = MiIntRepeticion
    objAutoan.EscribirLog "sql = " & Sql
    Qry.Execute
    
    Call EscribirEnColumna(ctePruebaREPETICION, CStr(val))
    Sql = "SELECT historia from PRUEBAASISTENCIA" _
    & " WHERE Historia = ? and Caso = ? " _
    & " and Secuencia = ? and NRepeticion = ? "
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    Qry(0) = Muestra.lngHistoria
    Qry(1) = strCaso
    Qry(2) = strSecuencia
    Qry(3) = val
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Rs.EOF Then
      Sql = "INSERT INTO pruebaAsistencia" _
      & " (historia, caso, secuencia, nrepeticion, estado," _
      & " ctipoprueba, ccarpeta, DPTOSOLICITUD, drsolicitud," _
      & " clistarealizacion, demorada, enviada, repetidapor," _
      & " investigacion, noestandar, urgenciaextraccion," _
      & " urgenciarealizacion, npeticion, FEXTRACCIONSOLICITADA," _
      & " HEXTRACCIONSOLICITADA, SECUENCIAHOST, COMENTARIOS," _
      & " ETIQUETA, FACTURADA, NREF)" _
      & " SELECT" _
      & " historia, caso, secuencia, ?, " & ctePRUEBASOLICITUDREALIZ & "," _
      & " ctipoprueba, ccarpeta, DPTOSOLICITUD, drsolicitud," _
      & " clistarealizacion, demorada, enviada, repetidapor," _
      & " investigacion, noestandar, urgenciaextraccion," _
      & " urgenciarealizacion, npeticion, FEXTRACCIONSOLICITADA," _
      & " HEXTRACCIONSOLICITADA, SECUENCIAHOST, COMENTARIOS," _
      & " ETIQUETA, FACTURADA, NREF" _
      & " FROM PRUEBAASISTENCIA" _
      & " WHERE Historia = ? and Caso = ? " _
      & " and Secuencia = ? and NRepeticion = 1"
      Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
      Qry(0) = val
      Qry(1) = Muestra.lngHistoria
      Qry(2) = strCaso
      Qry(3) = strSecuencia
      objAutoan.EscribirLog "sql = " & Sql
      Qry.Execute
    End If
  End If
  MiIntRepeticion = val
  Exit Property
  
AutoanError:
    Call objError.InternalError(Me, "Cambio de N�Repeticion del N�Actuacion " & MiStrNumAct & vbCrLf & "SQL: " & Sql, "Cambio de Estado")
End Property

Public Property Let strCodMuestra(val As String)
  MiStrCodMuestra = val
  Call EscribirEnColumna(ctePruebaCODMUESTRA, val)
  Call EscribirEnColumna(ctePruebaCODMUESTRAINVISIBLE, val)
End Property

Public Property Get strCodMuestra() As String
  strCodMuestra = MiStrCodMuestra
End Property

Public Property Get strCodAct() As String  '4
  strCodAct = MiStrCodAct
End Property

Public Property Get strDesAct() As String   '5
  strDesAct = MiStrDesAct
End Property

Public Property Get strNumAct() As String   '6
  strNumAct = MiStrNumAct
End Property

'Public Property Get intRepeticion() As Integer   '7
'  intRepeticion = MiintRepeticion
'End Property

Public Property Get strCodActAuto() As String   '8
  strCodActAuto = MiStrCodActAuto
End Property

Public Property Get strEstado() As constEstado   '9
  strEstado = MiStrEstado
End Property

Public Property Get blnUrgente() As Boolean   '11
  blnUrgente = MiBlnUrgente
End Property

Public Property Get strProperty1() As String   '11
  strProperty1 = MiStrProperty1
End Property

Public Property Get strProperty2() As String   '11
  strProperty2 = MiStrProperty2
End Property

Public Property Get strProperty3() As String   '11
  strProperty3 = MiStrProperty3
End Property

Public Property Get intRepeticion() As Integer
  intRepeticion = MiIntRepeticion
End Property

Public Property Get lngNumLista() As Long
  lngNumLista = MiLngNumLista
End Property

Public Property Let lngNumLista(val As Long)
Dim objlista As clsListaRealizacion
On Error GoTo AutoanError
  
  'If MiLngNumLista <> val Then
    MiLngNumLista = val
    
    Call EscribirEnColumna(ctePruebaLISTAREALIZACION, CStr(val))
    If Not objAutoan.blnExistLista(CStr(val)) Then
      Set objlista = New clsListaRealizacion
      objlista.lngNumero = val
           
      objAutoan.ColListasDeTrabajo.Add objlista, CStr(val)
    Else
        Set objlista = objAutoan.ColListasDeTrabajo(CStr(val))
    End If
    objlista.colActuaciones.Add Me, Muestra.strCodMuestra & "/" & strCodAct
  'End If
  Exit Property

AutoanError:
    Call objError.InternalError(Me, "pColListasDeTrabajo_Add", "NumLista = " & NumLista)
End Property

Private Sub CambioEstado()
Dim Sql$
Dim Qry As rdoQuery
Dim strResponsable As String
Dim EstadoPrueba As constEstadoPrueba
On Error GoTo AutoanError

  If MiStrEstado = cteACEPTADA _
  Or MiStrEstado = cteSINPROGRAMAR _
  Or MiStrEstado = cteEXTRAIDA _
  Or MiStrEstado = cteFUERADERANGO _
  Or MiStrEstado = cteAREPETIR _
  Or MiStrEstado = cteCOMPROBADA _
  Or MiStrEstado = cteREALIZADA Then
    Select Case MiStrEstado
      Case cteACEPTADA: EstadoPrueba = ctePRUEBAREALIZANDO
      Case cteSINPROGRAMAR: EstadoPrueba = ctePRUEBASOLICITUDREALIZ
      Case cteREALIZADA, cteFUERADERANGO, cteAREPETIR: EstadoPrueba = ctePRUEBARESULTANALIZ
      Case cteCOMPROBADA: EstadoPrueba = ctePRUEBAVALIDADA
      Case cteEXTRAIDA: EstadoPrueba = ctePRUEBAEXTRAIDA
    End Select
    If Not objAutoan.blnNoGuardarEstadoAC Or _
    EstadoPrueba <> ctePRUEBAREALIZANDO Then
      Sql = "UPDATE pruebaAsistencia" _
      & " SET ESTADO = ?" _
      & IIf(EstadoPrueba = ctePRUEBAEXTRAIDA, " ,CLISTAREALIZACION = NULL", "") _
      & " WHERE Historia = ? and Caso = ? " _
      & " and Secuencia = ? and NRepeticion = ?"
      Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
      Qry(0) = EstadoPrueba
      Qry(1) = Muestra.lngHistoria
      Qry(2) = strCaso
      Qry(3) = strSecuencia
      Qry(4) = MiIntRepeticion
      Qry.Execute
    End If
    If EstadoPrueba = ctePRUEBAEXTRAIDA Or _
    EstadoPrueba = ctePRUEBAVALIDADA Then
      QuitarPrueba
'      objAutoan.pRefreshGrids
    Else
      Muestra.strEstado = MiStrEstado
    End If
  End If
  
  'Se actualiza la tabla seguimientoPrueba.
  If MiStrEstado = cteFUERADERANGO _
  Or MiStrEstado = cteFUERADERANGOFIN _
  Or MiStrEstado = cteAREPETIR _
  Or MiStrEstado = cteAREPETIRFIN _
  Or MiStrEstado = cteRECIBIENDORESULTADOS _
  Or MiStrEstado = cteREALIZADA Then
    Sql = "SELECT historia"
    Sql = Sql & " FROM seguimientoPrueba"
    Sql = Sql & " WHERE historia = ?"
    Sql = Sql & " AND caso = ?"
    Sql = Sql & " AND secuencia = ?"
    Sql = Sql & " AND nrepeticion = ?"
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    Qry(0) = Muestra.lngHistoria
    Qry(1) = strCaso
    Qry(2) = strSecuencia
    Qry(3) = intRepeticion
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs.EOF Then
      Sql = "UPDATE seguimientoPrueba "
      Sql = Sql & " SET fecha = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
      Sql = Sql & ", responsable = ?"
      Sql = Sql & " WHERE historia = ?"
      Sql = Sql & " AND caso = ?"
      Sql = Sql & " AND secuencia = ?"
      Sql = Sql & " AND nRepeticion = ?"
      Sql = Sql & " AND proceso = ?"
    Else
      Sql = "INSERT INTO seguimientoPrueba (fecha, responsable,"
      Sql = Sql & " historia, caso, secuencia, nRepeticion, proceso)"
      Sql = Sql & " VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?)"
    End If
    Rs.Close
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    If lngNumLista <> 0 Then
      strResponsable = objAutoan.ColListasDeTrabajo(CStr(lngNumLista)).strResponsableLista
    End If
    Qry(0) = objAutoan.fFechaHoraActual()
    Qry(1) = strResponsable
    Qry(2) = Muestra.lngHistoria
    Qry(3) = strCaso
    Qry(4) = strSecuencia
    Qry(5) = intRepeticion
    Qry(6) = ctePROCESOREALIZACION
    Qry.Execute
    Qry.Close
  End If
  Exit Sub
    
AutoanError:
    Call objError.InternalError(Me, "Cambio de Estado del N�Actuacion " & MiStrNumAct & vbCrLf & "SQL: " & Sql, "Cambio de Estado")
End Sub

Public Sub CambioCarpeta(cCarpeta As Integer)
'Dim strSQL$
'Dim Qry As rdoQuery
'Dim Rs As rdoResultset
'  'Comprobamos que Esa Prueba Se Puede Pasar a Esa Carpeta
'  strSQL = "select ccarpeta from pruebascarpetas where cprueba = ? and ccarpeta = ?"
'  Set Qry = objAutoan.rdoConnect.CreateQuery("", strSQL)
'  Qry(0) = strCodAct
'  Qry(1) = cCarpeta
'  Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'  If Not Rs.EOF Then
'    strSQL = "Update PruebaAsistencia PA set cCarpeta = ? " _
'    & " Where Historia = " & Muestra.lngHistoria & " And Caso=" & strCaso _
'    & " And Secuencia = " & strSecuencia & " And NRepeticion = " & intRepeticion
'    Set Qry = objAutoan.rdoConnect.CreateQuery("", strSQL)
'    Qry(0) = cCarpeta
'    Qry.Execute
'    strSQL = "select cCarpeta from Carpetas " _
'    & "where cAutoanalizador = ? and cCarpeta = ?"
'    Set Qry = objAutoan.rdoConnect.CreateQuery("", strSQL)
'    Qry(0) = objAutoan.intCodAutoAn
'    Qry(1) = cCarpeta
'    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'    If Rs.EOF Then Muestra.ColMuestrasPruebas.Remove Me
'  End If
End Sub

