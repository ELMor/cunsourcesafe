Attribute VB_Name = "ModCX3"
Option Explicit

Public objAutoAn As New clsAutoAnalizador       'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid
'    .pCargaColEstados (Array(.VEst(cteAREPETIR), .VEst(cteSINPROGRAMAR), .VEst(cteEXTRAIDA)))
    'Activación de: Repetición Automática, Sin Salida Automática, Menu Propio, Cargar Especimen
    If Not .fInicializar(App, cteBECKMAN_CX3, , True, True, , , , , , , , True, True) Then End
    .Show
    .Borrar_Logs
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  With objAutoAn
    'Columnas del Grid Muestras
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    .AgregarColumnaAGridMuestras cteMuestraESPECIMEN
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoREFMINMAX
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub
