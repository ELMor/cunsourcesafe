VERSION 5.00
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Beckman CX3"
   ClientHeight    =   990
   ClientLeft      =   6075
   ClientTop       =   960
   ClientWidth     =   2955
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Cx3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   990
   ScaleWidth      =   2955
   Begin VB.Timer TimeOut 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   840
      Top             =   0
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   420
      Top             =   0
   End
   Begin VB.Timer Timer3 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   0
      Top             =   0
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "&Menu"
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "&Extraida"
         Index           =   1
      End
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "Sin &Programar"
         Index           =   2
      End
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "&Aceptada"
         Index           =   3
      End
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "&Borrada"
         Index           =   4
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Propiedades
Dim MiRespuestaEsperada As Boolean
      'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador.
      'Esto ocurrira cuando el sistema haya enviado un mensaje que requiere respuesta
      'False: no se espera ning�n mensaje de respuesta. El autoanalizador puede en esta
      'situaci�n enviar resultados u otros mensajes no solicitados
Dim MiRepetirPruebas As Integer
      'Indica si las pruebas se van a repetir o no
      'Estas 2 variables se definen como globales por manejarse en varias subrutinas y tener la necesidad de ser est�ticas
      'dentro del procedimiento que las utiliza. (Lo mismo ocurre con la variable Mensaje en los procedimientos de Lectura
      'de Protocolos pero para ella se prefiere realizar una copia ya que existen otras variables locales tambi�n llamadas
      'Mensaje que si no se cambian de nombre provocar�an errores).
Dim MiProgramPendiente As Integer
      'Indica si existe alguna prueba que deb�a haber sido programada pero no lo ha sido por haber
      'solicitado l�nea el autoanalizador al mismo tiempo
Dim MiBorradoPendiente As Integer
      'Indica si existe alguna muestra que todav�a est� pendiente de borrar por haber un numero de muestras a borrar mayor que 7
Dim MiRespuesta As TipoRespuesta
      'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
Dim MiRespuestaAfirmativa As Boolean
      'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>)
Dim MiTipoMensEnviado As TipoMensajeEnviado
      'Indica el tipo de mensaje que el sistema transmite al autoanalizador y que puede ser:
      '   1 - Funci�n 01 del stream 301: programaci�n de pruebas en autoanalizador
      '   3 - Funci�n 03 del stream 301: borrado de pruebas programadas para el actual carrusel
Dim MiUltimoEnvio As Integer
      'True: si no hay m�s env�os de prueba a programar
      'False: todav�a quedan pruebas a programar
Dim MiFuncionRespEsperada As TipoMensajeEnviado
      'Indica el tipo de mensaje que se est� esperando como respuesta
      '   2 - Funci�n 02 del stream 301: admisi�n de la programaci�n de pruebas
      '   4 - Funci�n 04 del stream 301: admisi�n del borrado de todas las pruebas programadas para el carrusel

Enum TipoMensajeEnviado
  ctePROGRAMACION = 1
  cteBORRADO = 3
End Enum

Enum TipoRespuesta
  cteACK = 1
  cteETX = 2
  cteNAK = 3
End Enum

Dim SubRutina As String

Private Sub CX3_Confirmacion(Mensaje As String, Caracter As String)
  'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
  'Si el mensaje es rechazado habr� que volver a enviarlo.
  Dim Contador As Integer
  On Error GoTo AutoAnError

  SubRutina = "CX3 Confirmacion"
  TimeOut.Enabled = False                 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then  'puede ser que el autoanalizador solicite ser master en caso de haber habido algun problema
      .Enviar Chr$(6)                     '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
      .Estado = cteESCLAVO
      .ButtonProgramEnabled False
      Mensaje = ""                        'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
      Timer3.Enabled = True
      Respuesta = cteACK                  'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = True          'Proxima respuesta afirmativa a enviar al autoanalizador
      Exit Sub
    End If
    Select Case Caracter
      Case Chr$(3)                        '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4)                   '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        'cuando dejamos de ser los transmitir esperamos la respuesta a estos mensajes
        RespuestaEsperada = True
        Exit Sub
      Case Chr$(6)                       '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
        Exit Sub
      Case Chr$(21)                     '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviarlo
        If TipoMensEnviado = ctePROGRAMACION Then
          Dim objMuestra As clsMuestra
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
              objMuestra.strEstado = cteSINPROGRAMAR
              UltimoEnvio = False
              Exit For
            End If
          Next
        End If
        .Estado = cteMASTER
        .ButtonProgramEnabled False
        Mensaje = ""
        Timer2.Enabled = True           'Se espera 1 segundo para retransmitir el bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Confirmacion", "plistaTrabajo")
End Sub

Private Sub CX3_Esclavo(sMensaje As String, sCaracter As String)
  'Identifica el final de un bloque y comprueba su check pasando a su lectura si fuera correcto.
  'Si el bloque ha llegado incorrectamente se contesta negativamente para que el autoanalizador retransmita.
  'Tambi�n se identifica el final de la comunicaci�n y se vuelve al estado de espera.
  Dim sql As String
  On Error GoTo AutoAnError
  
  SubRutina = "CX3 Esclavo"
  Timer3.Enabled = False                  'Al llegar datos del autoanalizador, se desactiva el Timer
  Select Case True
    
    Case sCaracter = Chr$(4)              '<EOT> - Final de la transmisi�n por parte del autoanalizador
      objAutoAn.Estado = cteESPERA
      sMensaje = ""
      Select Case True
        Case BorradoPendiente
          'cuando el numero de muestras a borrar supera 7 hay que mandar desde aqui sucesivos mensajes de borrado
          TipoMensEnviado = cteBORRADO
          Envio_Mensajes
        Case ProgramPendiente
          ProgramPendiente = False
          UltimoEnvio = False
          TipoMensEnviado = ctePROGRAMACION
          Envio_Mensajes
        Case RespuestaEsperada
          'cuando recibimos la finalizacion de las respuestas a nuestros mensajes se dejan de esperar estas
          RespuestaEsperada = False
          If TipoMensEnviado = ctePROGRAMACION Then
            If Not UltimoEnvio Then
              'aqui deberiamos seguir con la programacion automatica de las muestras de matrizprog solicitadas con el mensaje 6
              Envio_Mensajes
            Else
              If Not objAutoAn.blnExistMuestraSinEstado(cteNOACEPTADA) Then
                'todas las muestras estan NA
                'No se ha admitido la programaci�n de ninguna de las pruebas
                sql = "El autoanalizador " & frmPrincipal.Caption & _
                      " no ha aceptado la programci�n de ninguna prueba" & Chr$(10) & _
                      "Volver a ejecutar el programa para intentarlo de nuevo."
                MsgBox sql, vbExclamation, frmPrincipal.Caption
                Descargar
              End If
            End If
          End If
      End Select
    
    Case sCaracter = Chr$(5) '<ENQ> - El autoanalizador solicita respuesta
    'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
    'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
    'una vez se ha comprobado el check
      Select Case Respuesta
        Case cteACK: objAutoAn.Enviar Chr$(6) '<ACK>
        Case cteETX: objAutoAn.Enviar Chr$(3) '<ETX>
        Case cteNAK: objAutoAn.Enviar Chr$(21) '<NAK>
      End Select
      sMensaje = ""
      Timer3.Enabled = True
    
    Case Right$(sMensaje, 2) = Chr$(13) & Chr$(10)                  'Se ha identificado el final del bloque
      objAutoAn.Enviar IIf(RespuestaAfirmativa, Chr$(3), Chr$(6))   '<ETX>/<ACK>
      Respuesta = IIf(RespuestaAfirmativa, cteETX, cteACK)          'Ultima respuesta enviada al autoanalizador
      RespuestaAfirmativa = Not RespuestaAfirmativa                 'alternancia respuesta entre <ETX> y <ACK>
      Lectura_Datos_CX3 sMensaje                                    'Se procede a la lectura de datos
      sMensaje = ""
      Timer3.Enabled = True                                         'controlar tiempo hasta recibir siguiente mensaje
  End Select
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "CX3_Esclavo", "plistaTrabajo")
End Sub

Private Sub CX3_Espera(Mensaje As String, Caracter As String)
  'Identifica la llegada de los caracteres de solicitud de comunicaci�n, la acepta y pasa al estado de transmisi�n.
  On Error GoTo AutoAnError
  
  SubRutina = "CX3 Espera"
  With objAutoAn
  Select Case True
    Case (Right$(Mensaje, 2) = Chr$(4) & Chr$(1)) '<EOT><SOH> - Solicitud de comunicaci�n por parte del autoanalizador
      .Enviar Chr$(6)                             '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
      .Estado = cteESCLAVO
      Mensaje = ""                                'Se eliminan <EOT><SOH> y posibles caracteres enviados antes establecer comunicaci�n.
      Timer3.Enabled = True
      Respuesta = cteACK                          'Respuesta �ltima enviada al autoanalizador
      RespuestaAfirmativa = True                  'Proxima respuesta afirmativa a enviar al autoanalizador
    Case (Caracter = Chr$(5))                     '<ENQ> - El autoanalizador solicita respuesta a un bloque mandado tras
                                                  'haber ocurrido un T.O. No es normal que ocurra un T.O. en el autoanalizador
                                                  'ya que la la respuesta al bloque se env�a una vez se ha comprobado el check
      .Enviar Chr$(4)                             '<EOT> - El sistema da por finalizada la comunicaci�n
      Mensaje = ""
  End Select
  End With
  Exit Sub

AutoAnError:
Call objError.InternalError(Me, "CX3_Espera", "plistaTrabajo")
End Sub

Private Sub CX3_Master(Mensaje As String, Caracter As String)
  'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
  'Si el mensaje es rechazado habr� que volver a enviarlo.
  Dim objMuestra As clsMuestra
  On Error GoTo AutoAnError
  
  SubRutina = "CX3_Master"
  TimeOut.Enabled = False              'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    Select Case Caracter
    
      Case Chr$(3)                    '<ETX> - El autoanalizador acepta el bloque y da por finalizada la transmision
        .Enviar Chr$(4)               '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        RespuestaEsperada = True      'cuando recibimos la aceptacion y dejamos de transmitir esperamos la respuesta a estos mensajes
        
      Case Chr$(6)                    '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
        .Estado = cteCONFIRMACION
        .ButtonProgramEnabled False
        Envio_Mensajes                'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
        Mensaje = ""
        
      Case Chr$(21)                   '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviar
        If TipoMensEnviado = ctePROGRAMACION Then
          For Each objMuestra In .ColMuestras         'ColProgramMuestras
            With objMuestra
              If .strEstado = cteINTENTOPROGRAMAR Then
                .strEstado = cteSINPROGRAMAR          'Se actualiza la matriz que controla el grid
                UltimoEnvio = False
                Exit For
              End If
            End With
          Next
        End If
        Mensaje = ""
        Timer2.Enabled = True       'Se espera 1 segundo para retransmitir el ultimo bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Master", "plistaTrabajo")
End Sub

Private Sub CX3_Solicitud(Mensaje As String, Caracter As String)
  'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n,
  'el autoanalizador responde aceptando o negando dicha comunicaci�n.
  
  On Error GoTo AutoAnError
  SubRutina = "CX3 Solicitud"
  TimeOut.Enabled = False           'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    Select Case Caracter
      Case Chr$(6)                  '<ACK> - El autoanalizador ha aceptado la comunicaci�n
        .Estado = cteMASTER
        .ButtonProgramEnabled False
        Mensaje = ""
        Envio_Mensajes              'Se env�a el bloque de datos por parte del sistema al autoanalizador '!!MODIFICAR
        Exit Sub
      Case Chr$(21), Chr$(3)        '<NAK> o <ETX> - El autoanalizador no ha aceptado la comunicaci�n
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        Timer2.Enabled = True       'Se espera 1 segundo para intentar una nueva comunicaci�n
        Exit Sub
    End Select
    If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
      If TipoMensEnviado = ctePROGRAMACION Then ProgramPendiente = True
      .Enviar Chr$(6)               '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
      .Estado = cteESCLAVO
      .ButtonProgramEnabled False
      Mensaje = ""
      Timer3.Enabled = True
    End If
  End With
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "CX3_Solicitud", "plistaTrabajo")
End Sub

Private Sub Envio_Mensajes()
  'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer una peticci�n de linea mediante el
  'envio de los caracteres <EOT><SOH> y esperar a que el autoanalizador responda afirmativamente con <ACK>.
  'Luego se manda el mensaje propiamente dicho hasta que sea recibido correctamente por el autoanalizador.
  'Finalmente se cierra la comunicaci�n enviando <EOT> para dar paso a la respuesta del autoanalizador al
  'requerimiento del sistema.

  Static iContIntentosComunic As Integer   'cuenta el n� de veces que se intenta comunicar con el autoanalizador
  Static iContIntentosTrans As Integer     'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
  Dim lTiempo As Long
  Dim iRespuesta As Integer
  Dim sMensaje As String
  On Error GoTo AutoAnError
  
  SubRutina = "Envio Mensajes"
  objAutoAn.EscribirLog "Procedimiento: Envio Mensajes"
  With objAutoAn
    Select Case .Estado
      Case cteESPERA
        iContIntentosTrans = 0               'Se inicializa intContIntentosTrans por si se realiza una nueva comunicaci�n
        iContIntentosComunic = iContIntentosComunic + 1
        If iContIntentosComunic = 8 Then     'Fallo en la comunicaci�n despu�s de 7 intentos
          iContIntentosComunic = 1
          sMensaje = "No se puede establecer comunicaci�n."
          Select Case MsgBox(sMensaje, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
          Case vbRetry
            lTiempo = Timer + 5              'intenta establecer comunicaci�n de nuevo
            Do While lTiempo > Timer: iRespuesta = DoEvents(): Loop
            Sistema_Espera                   'el autoanalizador solicita de nuevo la linea de comunicaci�n
            .PanelEstado .fLeerEstado(.Estado) & " " & iContIntentosComunic
            Exit Sub
          Case vbCancel                      'no intenta establecer comunicaci�n de nuevo
          End Select
        Else                                 'solo cuando quede alguna prueba por programar, repetir o borrar
          If Not UltimoEnvio Or RepetirPruebas Or BorradoPendiente Then
            Sistema_Espera                   'solicitud de comunicacion
            .PanelEstado .fLeerEstado(.Estado) & " " & iContIntentosComunic
            Exit Sub
          End If
        End If
        
      Case cteMASTER
        iContIntentosComunic = 0        'Se inicializa el ContIntentosComunic por si se realiza una nueva comunicaci�n
        iContIntentosTrans = iContIntentosTrans + 1
        If iContIntentosTrans = 8 Then  'Fallo en la transmisi�n despu�s de 7 intentos
          iContIntentosTrans = 0
          .Estado = cteESPERA
          sMensaje = "No se puede transmitir informaci�n."
          Select Case MsgBox(sMensaje, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
            Case vbRetry
              lTiempo = Timer + 5
              Do While lTiempo > Timer: iRespuesta = DoEvents(): Loop
              Sistema_Espera
              iContIntentosComunic = 1
              .PanelEstado .fLeerEstado(.Estado) & " " & iContIntentosComunic
              Exit Sub
            Case vbCancel
          End Select
        Else
          Sistema_Master
          .PanelEstado .fLeerEstado(.Estado) & " " & iContIntentosTrans
          Exit Sub
        End If
        
      Case cteCONFIRMACION
        iContIntentosTrans = iContIntentosTrans + 1
        If iContIntentosTrans = 8 Then                    'Fallo en la transmisi�n despu�s de 7 intentos
          iContIntentosTrans = 0
          .Estado = cteESPERA
          .ButtonProgramEnabled True
          sMensaje = "No se puede transmitir informaci�n."
          Select Case MsgBox(sMensaje, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
            Case vbRetry
            Dim objMuestra As clsMuestra
            For Each objMuestra In objAutoAn.ColMuestras
              If objMuestra.strEstado = cteINTENTOPROGRAMAR Then objMuestra.strEstado = cteSINPROGRAMAR: Exit For
            Next
              lTiempo = Timer + 20                        'Se esperan 20 seg. para volver a iniciar la comunicacion
              Do While lTiempo > Timer: iRespuesta = DoEvents(): Loop
              Sistema_Espera
              iContIntentosComunic = 1
              .PanelEstado .fLeerEstado(.Estado) & " " & iContIntentosComunic
              Exit Sub
            Case vbCancel
          End Select
        Else
          Sistema_Confirmacion
          .PanelEstado .fLeerEstado(.Estado) & " " & iContIntentosTrans
          Exit Sub
        End If
    End Select
  End With
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Envio_Mensajes", "plistaTrabajo")
End Sub

Private Function fEstablecer_Check(sMensaje As String) As String
  Dim iCont As Integer              'cuenta el n�mero de caracteres le�dos
  Dim iSuma As Integer              'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal
  On Error GoTo AutoAnError
  
  SubRutina = "fEstablecer_Check"
  iSuma = 0
  For iCont = 1 To Len(sMensaje)
    iSuma = iSuma + Asc(Mid$(sMensaje, iCont, 1))
  Next iCont
  iSuma = 256 - iSuma Mod 256
  Select Case Len(Hex$(iSuma))
    Case 1:    fEstablecer_Check = "0" & Hex$(iSuma)
    Case 2:    fEstablecer_Check = Hex$(iSuma)
    Case Else: fEstablecer_Check = Right$(Hex$(iSuma), 2)
  End Select
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "fEstablecer_Check", "plistaTrabajo")
End Function

Private Sub Form_Load()
On Error GoTo AutoAnError

'  TimeOut.Interval = 0: Timer2.Interval = 0: Timer3.Interval = 0
  
  'Se inicializan las variables
  RespuestaEsperada = False
  RepetirPruebas = False
  ProgramPendiente = False
                                                  'Valores del menu emergente en el grid
  mnuItemEstadoMuestra(1).Tag = cteEXTRAIDA       'Extraida
  mnuItemEstadoMuestra(2).Tag = cteSINPROGRAMAR   'Sin Progromar
  mnuItemEstadoMuestra(3).Tag = cteACEPTADA       'Aceptada
  mnuItemEstadoMuestra(4).Tag = ctePREBORRADA     'Borrada
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Cargando Lista de Trabajo", "plistaTrabajo")
End Sub

Public Sub Lectura_Datos_CX3(sDatos As String)
  'Los datos que llegan pueden ser:
  '     esperados   (respuesta a un mensaje enviado anteriormente) o
  '     expont�neos (resultados enviados por el autoanalizador.
  
  Dim cResultadoAuto As String                      'C�digo del resultado definido en la tabla resultadosAutoanalizador
  Dim IdMuestra As String
  Dim strStream As String, strFunction As String
  Dim strResultado As String
  Dim i As Integer
  Dim objMuestra As clsMuestra
  Dim objActuacion As clsActuacion
  Dim objResultado As clsResultado
  Dim code As Integer
  Static num0 As Integer
  Dim num3 As Integer, num2 As Integer
  Dim sql As String
  Dim resp As Integer
  Dim CodeError As Integer
  On Error GoTo AutoAnError
  
  SubRutina = "Lectura_Datos_CX3"
  objAutoAn.EscribirLog "Procedimiento: Lectura Datos_CX3"
  objAutoAn.EscribirLog "Datos: " & sDatos
  If RespuestaEsperada Then                              'El mensaje es esperado.
    Select Case FuncionRespEsperada
      Case ctePROGRAMACION: Stream701_Funcion02 sDatos   'Respuesta a un intento de programar pruebas para una muestra.
      Case cteBORRADO:      Stream701_Funcion04 sDatos   'Respuesta a un intento de borrado de las pruebas programadas.
    End Select
  End If
  'El mensaje es de resultados.Identifica de que tipo de record se trata y lee sus datos seg�n lo expresado en el manual
  strStream = Mid$(sDatos, 5, 3)
  strFunction = Mid$(sDatos, 9, 2)
  Select Case strStream & strFunction
    Case "70102"
      'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
      CodeError = Val(Mid$(sDatos, 12, 2))
      IdMuestra = Trim$(Mid$(sDatos, 27, 11))
      For Each objMuestra In objAutoAn.ColMuestras
        With objMuestra
          If .strEstado = cteINTENTOPROGRAMAR And IdMuestra = .strIDMuestra Then
            Select Case CodeError
              Case 0: .strEstado = cteACEPTADA
              Case Else
                sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
                Select Case CodeError
                  Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                  Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                  Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                  Case 4: sql = sql & "Petici�n ORDAC invalida"
                  Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                  Case 6: sql = sql & "Control no configurado"
                  Case 7: sql = sql & "Solo sector de calibrador"
                  Case 8: sql = sql & "Error de modo (los modos Codigo de barras y Sector no son los mismos para el CX3 y Ordenador"
                  Case 9: sql = sql & "Error Interno en el CX3"
                  Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir mas muestras, solo modificarlas"
                  Case 11: sql = sql & "Tipos de fluido incompatibles"
                  Case 12: sql = sql & "Tipos de pruebas incompatibles"
                  Case 13: sql = sql & "Nombre de paciente incorrecto"
                  Case 14: sql = sql & "El C�digo de Muestra Ya Existe en el Autoanalizador"
                  Case 15: sql = sql & "El N� deRack debe estar entre 0000 y 9999"
                End Select
                resp = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
                Select Case resp
                  Case vbAbort: Descargar
                  Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                  Case vbIgnore: .strEstado = cteNOACEPTADA
                End Select
            End Select
            Exit For
          End If
        End With
      Next
    
    Case "70104"
    'Se lee la respuesta al intento de borrado de las pruebas programadas.
      code = Val(Mid$(sDatos, 15, 2))
      If code <> 0 And code <> 4 Then
        sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
        Select Case code
          Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
          Case 3: sql = sql & "Ha ocurrido un error en el CX3"
          Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
          Case 5: sql = sql & "El N� de Rack es Demasiado Largo"
        End Select
        resp = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en CX3")
        Select Case resp
          Case vbCancel: Exit Sub
          Case vbRetry:
            TipoMensEnviado = cteBORRADO
            For Each objMuestra In objAutoAn.ColMuestras
              If objMuestra.strEstado = cteINTENTOBORRADO Then objMuestra.strEstado = cteAREPETIR
            Next
            Envio_Mensajes
        End Select
      Else
        For i = 18 To 36 Step 3
          For Each objMuestra In objAutoAn.ColMuestras
            With objMuestra
              If .strEstado = cteINTENTOBORRADO Then
                Select Case Mid$(sDatos, i + 1, 1)
                  Case "0", "4"
                    num0 = num0 + 1
                    .strEstado = cteBORRADA
                    For Each objActuacion In .ColMuestrasPruebas
                      For Each objResultado In objActuacion.ColPruebasResultados
                        objResultado.EscribirEnColumna cteResultadoVALRESULTADO, ""
                      Next
                    Next
                  Case "2"
                    num2 = num2 + 1
                    .strEstado = cteNOBORRADA
                  Case "3"
                    num3 = num3 + 1
                    .strEstado = cteNOBORRADA
                End Select
              End If
            End With
          Next
        Next i
        If num3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
        If num2 > 0 Then sql = sql & Chr$(13) & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
        If Not BorradoPendiente Then
          'en caso de ir todo bien me pondra al final de todo el proceso de borrado el n�muestras borradas
          If num0 > 0 Then sql = sql & Chr$(13) & "Se Van a Repetir " & num0 & " Muestras." & Chr$(13) & Chr$(10) & " 1� -Pulse Aceptar y " & Chr$(13) & Chr$(10) & "2� -Pulse Iniciar Para Comenzar las Repeticiones"
          num0 = 0  'reinicializamos el n� muestras totales borradas
          If sql <> "" Then
            resp = MsgBox(sql, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
          Else
            resp = MsgBox("Pulse Iniciar en el " & frmPrincipal.Caption, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
          End If
        End If
      End If
      
    Case "70106"
    'el CX3 pide la programacion de las muestras leidas por el C�digo de Barras y estas se van programando
      ProgramPendiente = True
      For i = 12 To 84 Step 12
        IdMuestra = Mid$(sDatos, i, 11)
        If IdMuestra <> Space$(11) Then                         'hay mas muestra?
          For Each objMuestra In objAutoAn.ColMuestras          'busca esa muestra entre las de la lista de trabajo
            With objMuestra                                     'si la muestra esta sin programar o borrada
              If .strIDMuestra = Trim$(IdMuestra) And (.strEstado = cteSINPROGRAMAR Or .strEstado = cteBORRADA) Then
                .strEstado = cteLEIDA                           'pasa la muestra a estado de leida por el autoanalizador
                Exit For
              End If
            End With
          Next
        End If
      Next i
      If Not objAutoAn.blnExistMuestraConEstado(cteLEIDA) Then
        UltimoEnvio = True
        ProgramPendiente = False
      End If
      
    Case "70201"
      'se lee el codigo de la muestra. Este mensaje se envia siempre entes de los resultados.
      IdMuestra = Trim$(Mid$(sDatos, 57, 11))               'codigo num�rico de la muestra
      With objAutoAn
        If IsNumeric(IdMuestra) Then
          IdMuestra = .fNumeroACodigo(IdMuestra)            'la muestra pasa al estado de recibiendo resultados
          If .blnExistMuestra(IdMuestra) Then .ColMuestras(IdMuestra).strEstado = cteRECIBIENDORESULTADOS
        End If
      End With
      
    Case "70203"
      'Se leen los resultados y se guardan en la BD
      Dim strCodResAuto As String
      IdMuestra = Trim$(Mid$(sDatos, 48, 11))
      strCodResAuto = Trim$(Mid$(sDatos, 60, 4))            'codigo resultado del autoanalizador
      strResultado = CStr(Trim$(Mid$(sDatos, 82, 9)))       'lee el resultado
      For Each objMuestra In objAutoAn.ColMuestras
        With objMuestra
          If .strIDMuestra = IdMuestra Then
             Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, strCodResAuto, strResultado, True)
             Exit For
          End If
        End With
      Next
      
    Case "70211"
      'Transmision de un resultado especial calculado
      strResultado = CStr(Trim$(Mid$(sDatos, 77, 9)))
      For Each objMuestra In objAutoAn.ColMuestras
        With objMuestra
          If .strIDMuestra = Trim$(Mid$(sDatos, 38, 11)) Then
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(sDatos, 53, 20)), strResultado)
            Exit For
          End If
        End With
      Next
       
    Case "70317"
      'Se recibe la notificaci�n de que ya se han recibido todos los resultados
      'lblRepeticion.Visible = False 'una vez recibidas todas las repeticiones se quita el aviso
      objAutoAn.LabelVisible False
      'desde aqui es desde donde se activa la variable que mandar� repetir las pruebas al recibir EOT (CX_esclavo)
      If objAutoAn.blnRepetirPendiente Then
        BorradoPendiente = True
        'MsgBox "Se Van a Procesar las Muestras Pendientes de Repeticion" & vbCrLf & " 1� -Pulse Aceptar En Este Mensaje y " & vbCrLf & "2� -Pulse Iniciar En El Autoanalizador CX3"
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            objAutoAn.EscribirLog .strCodMuestra & ".Estado = " & .strEstado
            If (.strEstado = cteAREPETIR Or _
            .strEstado = cteAREPETIRFIN) And _
            .strEspecimen <> "Orina" Then
              For Each objActuacion In .ColMuestrasPruebas
                With objActuacion
                  objAutoAn.EscribirLog "CodAct:" & .strCodAct & ".Estado = " & .strEstado
                  If (.strEstado = cteAREPETIRFIN Or _
                  .strEstado = cteAREPETIR) And _
                  .intRepeticion = 1 Then
                    .intRepeticion = .intRepeticion + 1
                  Else
                    'objMuestra.ColMuestrasPruebas.Remove .strCodAct
                    objActuacion.QuitarPrueba
                  End If
                End With
              Next
            Else
              If .strEstado = cteREALIZACIONCOMPLETA Or .strEstado = cteREALIZADA Or .strEstado = cteFUERADERANGO Or _
                 .strEstado = cteAREPETIRFIN Or .strEstado = cteFUERADERANGOFIN Then
                objMuestra.QuitarMuestra
              End If
            End If
          End With
        Next
        objAutoAn.pRefreshGrids
      Else
        With objAutoAn
          If .blnExistMuestraConEstado(cteREALIZACIONCOMPLETA) Then
            .pSalir
          Else
            If .blnExistMuestraConEstado(cteAREPETIRFIN) Then
              .pSalir
            Else
              If .blnExistMuestraConEstado(cteFUERADERANGOFIN) Then
                .pSalir
              Else
                If .blnExistMuestraConEstado(cteREALIZADA) Then
                  .pSalir
                End If
              End If
            End If
          End If
        End With
      End If
    End Select
'  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos_CX3", "Lectura_Datos_CX3")
End Sub

Public Sub Lectura_Protocolo(sCaracter As String)
  Static sMensaje As String   'Contiene el mensaje que env�a el autoanalizador
  Dim sCopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo
  'y es la que se env�a a otras subrutinas pudiendo ser modificada en �stas.
  'Al ser modificada la copia, tambi�n deber� modificarse el original.
  'El hecho de utilizar esta copia se debe a que la variable strMensaje no puede modificar
  'su contenido en otras subrutinas por estar declarada como Static en esta subrutina
  
  On Error GoTo AutoAnError
  sMensaje = sMensaje & sCaracter:                sCopiaMensaje = sMensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      CX3_Espera sCopiaMensaje, sCaracter:        sMensaje = sCopiaMensaje
    Case cteSOLICITUD
      CX3_Solicitud sCopiaMensaje, sCaracter:     sMensaje = sCopiaMensaje
    Case cteESCLAVO
      CX3_Esclavo sCopiaMensaje, sCaracter:       sMensaje = sCopiaMensaje
    Case cteCONFIRMACION
      CX3_Confirmacion sCopiaMensaje, sCaracter:  sMensaje = sCopiaMensaje
    Case cteMASTER
      CX3_Master sCopiaMensaje, sCaracter:        sMensaje = sCopiaMensaje
    End Select
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Lectura_Protocolo", "plistaTrabajo")
End Sub

Private Sub Sistema_Confirmacion()
  'Se solicita que el autoanalizador repita o env�e la respuesta a un mensaje enviado anteriormente
  On Error GoTo AutoAnError
  
  objAutoAn.EscribirLog "Procedimiento: Sistema_Confirmacion"
  SubRutina = "Sistema_Confirmacion"
  objAutoAn.Enviar Chr$(5)           'Env�o de <ENQ>, se pide la respuesta al mensaje enviado
  TimeOut.Enabled = True             'Empieza a contar el tiempo para el TimeOut
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Sistema_Confirmacion", "plistaTrabajo")
End Sub

Private Sub Sistema_Espera()
  'Se env�a los caracteres de solicitud de comunicaci�n y se pasa a estado de Solicitud
  On Error GoTo AutoAnError
  SubRutina = "Sistema_Espera"
  With objAutoAn
    .EscribirLog "Procedimiento: Sistema_Espera"
    .Enviar Chr$(4) & Chr$(1)                     'Env�o de <EOT><SOH>, se "solicita l�nea"
    .Estado = cteSOLICITUD                        'pasa al estado de solicitud
    .ButtonProgramEnabled False                   'deshabilita el bot�n de programaci�n
  End With
  TimeOut.Enabled = True                           'Empieza a contar el tiempo para el TimeOut
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Sistema_Espera", "plistaTrabajo")
End Sub

Private Sub Sistema_Master()
  'Se transmiten datos al autoanalizador. Los datos a enviar pueden ser de dos tipos:
  'programaci�n de pruebas o borrado de pruebas programadas.
  Dim sMensaje As String                                  'Contiene un string correspondiente al mensaje a enviar
  On Error GoTo AutoAnError
  
  SubRutina = "Sistema_Master"
  Select Case TipoMensEnviado
    Case ctePROGRAMACION: sMensaje = Stream701_Funcion01  'programaci�n de pruebas
    Case cteBORRADO:      sMensaje = Stream701_Funcion03  'borrado de todas las pruebas programadas
  End Select
  'Se env�a el mensaje
  If sMensaje <> "" Then
    objAutoAn.Enviar sMensaje & vbCrLf
    TimeOut.Enabled = True                                'Empieza a contar el tiempo para el TimeOut
  Else
    objAutoAn.Estado = cteESPERA
  End If
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Sistema_Master", "plistaTrabajo")
End Sub

Private Function Stream701_Funcion01() As String
  'se forma un mensaje que responde al formato stream 701, funci�n 01 (programaci�n de pruebas),
  'de las muestras que est� en estado de leida
  'y se establecen otras caracter�sticas relacionadas con la respuesta esperada y futuros env�os
  Dim sMensaje As String
  Dim sCeros As String
  Dim sPruebas As String            'c�digos de las pruebas acumulados en un mismo string seg�n codificaci�n del CX3
  Dim sCodPruebaAutoan              'codigo de la prueba en el autoanalizador
  Dim iNumeroPruebas As Integer     'Numero de pruebas que se van a programar para una muestra
  Dim objMuestra As clsMuestra
  Dim objActuacion As clsActuacion

  Debug.Print "hola"
  On Error GoTo AutoAnError
  SubRutina = "Stream701_Funcion01"
  objAutoAn.EscribirLog "Procedimiento: Stream701_Funcion01"
  For Each objMuestra In objAutoAn.ColMuestras                      'ColProgramMuestras
    With objMuestra
      If .strEstado = cteLEIDA Then                                 'el autoan. ha leido su c�digo de barras
        'Se buscan y se codifican todas la pruebas que hay que realizar a la muestra.
        iNumeroPruebas = 0
        For Each objActuacion In .ColMuestrasPruebas
          With objActuacion
            sCodPruebaAutoan = objAutoAn.ColPruebaAuto(.strCodAct).strCodActAuto
            sPruebas = sPruebas & "," & sCodPruebaAutoan & " "      'codigo de la prueba
            sPruebas = sPruebas & ",0"                              'no hacer ORDAC
          End With
        Next
        iNumeroPruebas = CInt(Len(sPruebas) / 7)
        sCeros = String(3 - Len(CStr(iNumeroPruebas)), "0")     'ceros que faltan
      
        sMensaje = "["                                          'Se comienza a formar el mensaje
        sMensaje = sMensaje & "00"                              'Identificador de dispositivo
        sMensaje = sMensaje & ",701"                            'Stream
        sMensaje = sMensaje & ",01"                             'Funcion
        sMensaje = sMensaje & ",00"                             'no hay numero de sector en el c�digo de barras
        sMensaje = sMensaje & ",00"                             'no hay numero de copa en el codigo de barras
        sMensaje = sMensaje & ",1"                              'el �ltimo 1 es para poder programar solo nuevas pruebas
                                                                'a una muestra que ya exista y si no programa una nueva
                                                                'el caso 0 hace reemplazar todo lo que exista
        sMensaje = sMensaje & IIf(.blnUrgente, ",ST", ",RO")    'Se se�ala si es urgencia o no
        sMensaje = sMensaje & "," & .strEspecimenAuto           'tipo de muestra seg�n el autoanalizador
        sMensaje = sMensaje & "," & Trim$(.strIDMuestra) & " "  'Identificador de muestra
        sMensaje = sMensaje & "," & Space$(20)                  'Control Name
        sMensaje = sMensaje & "," & Space$(25)                  'Sample Comment Code 1
        sMensaje = sMensaje & "," & Space$(25)                  'Sample Comment Code 2
        sMensaje = sMensaje & "," & Space$(18)                  'Last Name
        sMensaje = sMensaje & "," & Space$(15)                  'First Name
        sMensaje = sMensaje & "," & Space$(1)                   'Middle Initial
        sMensaje = sMensaje & "," & Space$(12)                  'Patient ID
        sMensaje = sMensaje & "," & Space$(18)                  'Doctor
        sMensaje = sMensaje & "," & Format$(Now, "ddmmyy")      'Fecha de extracci�n
        sMensaje = sMensaje & "," & Format$(Now, "hhmm")        'Hora de extracci�n
        sMensaje = sMensaje & "," & Space$(20)                  'Localidad
        sMensaje = sMensaje & "," & Space$(3)                   'Edad
        sMensaje = sMensaje & "," & Space$(1)                   'Unidades de edad
        sMensaje = sMensaje & "," & Space$(6)                   'Fecha de nacimiento
        sMensaje = sMensaje & "," & Space$(1)                   'Sexo
        sMensaje = sMensaje & "," & Space$(25)                  'Comentario del Paciente
        sMensaje = sMensaje & "," & Space$(7)                   'Timer Urine Volumen
        sMensaje = sMensaje & "," & Space$(4)                   'Timer Urine Period
        sMensaje = sMensaje & "," & Space$(4)                   'Timed Urine Creatine
        sMensaje = sMensaje & "," & Space$(6)                   'Timed urine Area
        sMensaje = sMensaje & "," & sCeros & iNumeroPruebas     'Numero de pruebas
        sMensaje = sMensaje & sPruebas                          'Pruebas a realizar a la muestra
        sMensaje = sMensaje & "]"                               'fin del mensaje
        sMensaje = sMensaje & fEstablecer_Check(sMensaje)       'Se a�ade el check para el mensaje
        Stream701_Funcion01 = sMensaje
        .strEstado = cteINTENTOPROGRAMAR                        'Se coloca el estado "IP" para la muestra.
        Exit For
      End If
    End With
  Next
  'para que no intente mandar mensaje estando ya todo programado(mandaria "" & CRLF) y CX responder�a NAK
  If Not objAutoAn.blnExistMuestraConEstado(cteINTENTOPROGRAMAR) Then
    ProgramPendiente = False: UltimoEnvio = True
  End If
  'Se establecen el tipo de respuesta esperado par parte del CX3 al mensaje que se le va a enviar.
  FuncionRespEsperada = ctePROGRAMACION
  Exit Function
  
AutoAnError:
  Call objError.InternalError(Me, "Stream701_Funcion01", "Mensaje: " & sMensaje)
End Function

Private Sub Stream701_Funcion02(strDatos As String)
  'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
  Dim strStream As String, strFunction As String
  Dim sql As String
  Dim intRespuesta As Integer
  Dim intCodError As Integer
  Dim strCodMuestra As String
  Dim objMuestra As clsMuestra
  On Error GoTo AutoAnError
  SubRutina = "Stream701_Funcion02"
  objAutoAn.EscribirLog "Stream701_Funcion02"
  strStream = Mid$(strDatos, 5, 3)
  strFunction = Mid$(strDatos, 9, 2)
  If strStream = "701" And strFunction = "02" Then
    intCodError = Val(Mid$(strDatos, 12, 2))
    strCodMuestra = Trim$(Mid$(strDatos, 27, 11))
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        If .strEstado = cteINTENTOPROGRAMAR And strCodMuestra = .strIDMuestra Then
          Select Case intCodError
            Case 0: .strEstado = cteACEPTADA
            Case Else
              sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
              Select Case intCodError
                Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                Case 4: sql = sql & "Petici�n ORDAC invalida"
                Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                Case 6: sql = sql & "Control no configurado"
                Case 7: sql = sql & "Solo sector de calibrador"
                Case 8: sql = sql & "Error de modo (los modos c�digo de barras y sector no son los mismos para el CX3 y Ordenador"
                Case 9: sql = sql & "Error interno en el CX3"
                Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir m�s muestras, solo modificarlas"
                Case 11: sql = sql & "Tipos de fluido incompatibles"
                Case 12: sql = sql & "Tipos de pruebas incompatibles"
                Case 13: sql = sql & "Nombre de paciente incorrecto"
                Case 14: sql = sql & "El c�digo de muestra ya existe en el autoanalizador"
                Case 15: sql = sql & "El N� de Rack debe estar entre 0000 y 9999"
              End Select
              intRespuesta = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
              Select Case intRespuesta
                Case vbAbort: Descargar                                             'Form_Unload (False)
                Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                Case vbIgnore: .strEstado = cteNOACEPTADA
              End Select
          End Select
          Exit For
        End If
      End With
    Next
  End If
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Stream701_Funcion02", "Datos: " & strDatos)
End Sub

Private Function Stream701_Funcion03() As String
  'Se forma un mensaje que responde al formato del stream 701, funci�n 03 y
  'se establecen otras caracter�sticas relacionadas con la respuesta esperada y futuros env�os
  Dim sMensaje As String
  Dim objMuestra As clsMuestra
  Dim iCont As Integer
  Dim sLista As String                    'lista de las muestras a borrar
  Dim iNumMuestras As Integer             'lleva el control del n� de muestras a borrar
                                          'pues no podemos eliminar mas de 7 en un solo mensaje
  On Error GoTo AutoAnError
  SubRutina = "Stream701_Funcion03"
  objAutoAn.EscribirLog "Procedimiento: Stream701_Funcion03"
  sLista = ""
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      If .strEstado = cteAREPETIRFIN Or .strEstado = ctePREBORRADA Then
        .strEstado = cteINTENTOBORRADO
        iNumMuestras = iNumMuestras + 1
        sLista = sLista & .strIDMuestra & Space$(11 - Len(.strIDMuestra)) & ","
        If iNumMuestras >= 7 Then Exit For
      End If
    End With
  Next
  For iCont = iNumMuestras + 1 To 7
    sLista = sLista & Space$(11) & ","
  Next iCont
  sMensaje = "[00,701,03,00," & sLista & "]"
  Stream701_Funcion03 = sMensaje & fEstablecer_Check(sMensaje)
  FuncionRespEsperada = cteBORRADO
  UltimoEnvio = True
  BorradoPendiente = objAutoAn.blnExistMuestraConEstado(cteAREPETIRFIN)
  If Not BorradoPendiente Then BorradoPendiente = objAutoAn.blnExistMuestraConEstado(ctePREBORRADA)
  objAutoAn.EscribirLog "Borrado pendiente: " & BorradoPendiente
  Exit Function
  
AutoAnError:
  Call objError.InternalError(Me, "Stream701_Funcion03", "Mensaje: " & sMensaje)
End Function

Private Sub Stream701_Funcion04(Datos As String)
  'Se lee la respuesta al intento de borrado de las pruebas programadas.
  Dim strStream As String
  Dim strFunction As String
  Dim sql As String
  Dim intCodError As Integer
  Dim intRespuesta As Integer
  Dim intNum3 As Integer, intNum2 As Integer
  Dim intCont As Integer
  Static intNum0
  Dim objMuestra As clsMuestra
  Dim objActuacion As clsActuacion
  Dim objResultado As clsResultado
  On Error GoTo AutoAnError

  SubRutina = "Stream701_Funcion04"
  objAutoAn.EscribirLog "Stream701_Funcion04"
  strStream = Mid$(Datos, 5, 3)
  strFunction = Mid$(Datos, 9, 2)
  If strStream = "701" And strFunction = "04" Then
    intCodError = Val(Mid$(Datos, 15, 2))
    If intCodError <> 0 And intCodError <> 4 Then    'se ha producido un error al querer borrar las muestras programadas
      sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
      Select Case intCodError
        Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
        Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
        Case 3: sql = sql & "Ha ocurrido un error en el CX3"
        Case 5: sql = sql & "El N� de Rack es demasiado largo"
      End Select
      intRespuesta = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en " & frmPrincipal.Caption)
      Select Case intRespuesta
        Case vbCancel: Exit Sub
        Case vbRetry:
          TipoMensEnviado = cteBORRADO
          For Each objMuestra In objAutoAn.ColMuestras
            With objMuestra
              If .strEstado = cteINTENTOBORRADO Then .strEstado = cteAREPETIR
            End With
          Next
          Envio_Mensajes
      End Select
    Else                                            'no se ha producido ning�n error al borrar las muestras programadas
      For intCont = 18 To 36 Step 3
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteINTENTOBORRADO Then
              Select Case Mid$(Datos, intCont + 1, 1)
                Case "0", "4"
                  intNum0 = intNum0 + 1
                  .strEstado = cteBORRADA
                  For Each objActuacion In .ColMuestrasPruebas
                    For Each objResultado In objActuacion.ColPruebasResultados
                      objResultado.EscribirEnColumna cteResultadoVALRESULTADO, ""
                    Next
                  Next
                Case "2"
                  intNum2 = intNum2 + 1
                  .strEstado = cteNOBORRADA
                Case "3"
                  intNum3 = intNum3 + 1
                  .strEstado = cteNOBORRADA
              End Select
            End If
          End With
        Next
      Next intCont
      If intNum3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
      If intNum2 > 0 Then sql = sql & vbCr & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
      If Not BorradoPendiente Then
                  'en caso de ir todo bien, me pondra al final de todo el proceso de borrado el n� muestras borradas
        If intNum0 > 0 Then sql = sql & vbCr & "Se Van a Repetir " & intNum0 & " Muestras." & vbCrLf & _
                                  "1� - Pulse Aceptar" & vbCrLf & _
                                  "2� - Pulse Iniciar Para Comenzar las Repeticiones"
        intNum0 = 0   'reinicializamos el n� muestras totales borradas
        If sql <> "" Then
          intRespuesta = MsgBox(sql, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
        Else
          intRespuesta = MsgBox("Pulse iniciar en el " & frmPrincipal.Caption, vbInformation, _
                                "Borrado de Muestras en " & frmPrincipal.Caption)
        End If
      End If
    End If
  End If
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Stream701_Funcion04", "Datos: " & Datos)
End Sub

Private Sub mnuItemEstadoMuestra_Click(Index As Integer)
  Dim MuestrasSelecionada As Integer  'n�mero de muestras selecionadas
  Dim BkMrk
  Dim j As Boolean
  Dim strCodMuestra As String
  Dim EstadoMuestra As constEstado
  Dim NuevoEstado As constEstado      'Estado al que se quiere pasar la muestra/as selecionadas en el grid
  Dim ColMuestras As New Collection   'coleccci�n de muestras a cambiar de estado
  Dim objMuestra As clsMuestra        'una de las muestra a cambiar de estado
  Dim objActuacion As clsActuacion    'una prueba de una muestra a cambiar de estado

  NuevoEstado = mnuItemEstadoMuestra(Index).Tag
  With objAutoAn.GridMuestras
    For MuestrasSelecionada = 0 To .SelBookmarks.Count - 1      'crea una colecci�n con todas las muestras selecionadas
      BkMrk = .SelBookmarks(MuestrasSelecionada)                'donde el indice es el codigo de la muestra.
      strCodMuestra = .Columns("Muestra").CellValue(BkMrk)
      Err = 0: On Error Resume Next
      j = (ColMuestras(strCodMuestra).strCodMuestra <> "")      'da un error si la muestra no est� en la nueva colecci�n
      If Err <> 0 Then                                          'a�adi�ndolo a la misma
        ColMuestras.Add objAutoAn.ColMuestras(strCodMuestra), strCodMuestra
      End If
    Next MuestrasSelecionada
  End With
  
  For Each objMuestra In ColMuestras                            'Va recorriendo todas las muestras selecionadas
    For Each objActuacion In objMuestra.ColMuestrasPruebas      'cambiando su estado actual de cada prueba
      With objActuacion                                         'por el selecionado en el muenu emergente
        Select Case .strEstado                                  'lo hace en funci�n del estado que tenia anteriormente
          Case cteAREPETIRFIN, cteNOACEPTADA, cteNOBORRADA
            If NuevoEstado = cteAREPETIR Then
              .intRepeticion = .intRepeticion + 1
            Else
              .strEstado = NuevoEstado
            End If
          Case cteSINPROGRAMAR
          If NuevoEstado = cteACEPTADA Or NuevoEstado = cteEXTRAIDA Then .strEstado = NuevoEstado
          Case cteACEPTADA
            If NuevoEstado = cteSINPROGRAMAR Or NuevoEstado = cteEXTRAIDA Then .strEstado = NuevoEstado
          Case cteREALIZACIONCOMPLETA, cteFUERADERANGOFIN
            If NuevoEstado = cteAREPETIR Then .intRepeticion = .intRepeticion + 1
        End Select
      End With
    Next
    With objMuestra
      EstadoMuestra = .strEstado
      If (EstadoMuestra <> cteREALIZACIONCOMPLETA And EstadoMuestra <> cteRECIBIENDORESULTADOS _
          And EstadoMuestra <> cteREALIZADA) Or NuevoEstado = ctePREBORRADA Then _
            .strEstado = NuevoEstado   'si la muestra no est� en ninguno de estos estados la cambia al nuevo estado
    End With
  Next
  
  If MuestrasSelecionada > 0 Then         'si se ha selecionado alguna muestra
    objAutoAn.pRefreshGrids               'refresca los grids muestra y pruebas
    If NuevoEstado = ctePREBORRADA Then   'si se selecion� preborrado
      BorradoPendiente = True             'hay pendientes de borrar
      TipoMensEnviado = cteBORRADO
      Envio_Mensajes
    End If
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "mnuItemEstadoMuestra_Click", "Index:" & Index)
End Sub

Private Sub TimeOut_Timer()
  'Controla los Time Out que produce el autoanalizador
  On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Procedimiento: TimeOut_Timer (TimeOut)"
  SubRutina = "TimeOut_Timer"
  TimeOut.Enabled = False
  With objAutoAn
    Select Case .Estado
      Case cteSOLICITUD                 'Habiendo el sistema "solicitado l�nea", el autoanalizador tarda m�s
        .Estado = cteESPERA             'de 15 segundos en responder, por lo que pasa al estado de espera
        RespuestaEsperada = False
        Envio_Mensajes                  'para realizar un nuevo intento de comunicaci�n
      Case cteMASTER                    'Habiendo el sistema enviado datos, el autoanalizador tarda m�s
        .Estado = cteCONFIRMACION       'de 15 segundos en responder
        Envio_Mensajes                  'para realizar un nuevo intento de envio de datos
      Case cteCONFIRMACION              'Habiendo el sistema enviado datos, el autoanalizador tarda m�s
        .Estado = cteCONFIRMACION       'de 15 segundos en responder
        Envio_Mensajes                  'para realizar un nuevo intento de envio de datos
    End Select
  End With
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "TimeOut_Timer", "TimeOut_Timer")
End Sub

Private Sub Timer2_Timer()
  'Controla los tiempos entre env�os de solicitudes
  On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Procedimiento: Timer2_Timer (Tiempos entre envios)"
  SubRutina = "Timer2_Timer"
  Timer2.Enabled = False
  Envio_Mensajes
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer2_Timer", "Timer2_Timer")
End Sub

Private Sub Timer3_Timer()
  'Controla el tiempo que transcurre desde los �ltimo datos recibidos por el sistema.
  'Si estando el sistema en el estado de esclavo pasan 60 segundos sin recibir datos o
  'el final de la comunicaci�n <EOT> se finaliza la ejecuci�n
  Dim strMensaje As String
  Dim intRespuesta As Integer
  On Error GoTo AutoAnError
  
  objAutoAn.EscribirLog "Procedimiento: Timer3 (Tiempo sin Recibir datos)"
  SubRutina = "Timer3_Timer"
  Timer3.Enabled = False
  If objAutoAn.Estado = cteESCLAVO Then
    strMensaje = "Se ha producido un TimeOut durante la comunicaci�n por parte del Autoanalizador." _
                  & Chr$(10) & "Puede que se haya perdido la comunicaci�n." _
                  & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
    intRespuesta = MsgBox(strMensaje, vbExclamation + vbYesNo, frmPrincipal.Caption)
    Select Case intRespuesta
      Case vbYes
        Timer3.Enabled = True
      Case vbNo
        objAutoAn.Estado = cteESPERA
    End Select
    RespuestaEsperada = False
  End If
  Exit Sub

AutoAnError:
  Call objError.InternalError(Me, "Timer3_Timer", "Timer3_Timer")
End Sub

Public Function Salir() As Boolean
  'Decidimos si queremos salir del programa
  Dim strMensaje As String
  On Error GoTo AutoAnError
  
  SubRutina = "Salir"
  objAutoAn.EscribirLog "Procedimiento: Salir"
  strMensaje = "Desea pasar las muestra sin resultados a extraidas?"
  Salir = (MsgBox(strMensaje, vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar                 'se ha decidido finalizar el programa
  Exit Function
  
AutoAnError:
  Call objError.InternalError(Me, "Salir", "Salir")
End Function

Public Sub Descargar()
  'Finaliza el programa
  On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Procedimiento: Descargar"
  End
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Descargar", "Descargar")
End Sub

Private Property Let ProgramPendiente(Valor As Integer)
  'Indica si existe alguna prueba que deb�a haber sido programada
  MiProgramPendiente = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " PROGRAMPendiente = " & Valor
End Property

Private Property Let BorradoPendiente(Valor As Integer)
  'Indica si existe alguna muestra que todav�a est� pendiente de borrar
  MiBorradoPendiente = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " BORRADOPendiente = " & Valor
End Property

Private Property Let RepetirPruebas(Valor As Integer)
  'Indica si las pruebas se van a repetir o no
  MiRepetirPruebas = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RepetirPruebas = " & Valor
End Property

Private Property Let Respuesta(Valor As TipoRespuesta)
  'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
  Dim sValor As String
  Select Case Valor
    Case 0: sValor = "ACK"
    Case 1: sValor = "ETX"
    Case 2: sValor = "NAK"
  End Select
  MiRespuesta = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " Respuesta = " & sValor
End Property

Private Property Let RespuestaAfirmativa(Valor As Boolean)
  'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
  MiRespuestaAfirmativa = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RespuestaAfirmativa = " & Valor
End Property

Private Property Let TipoMensEnviado(Valor As TipoMensajeEnviado)
  'Indica el tipo de mensaje que el sistema transmite al autoanalizador
  MiTipoMensEnviado = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " TipoMensEnviado = " & Valor
End Property

Private Property Let UltimoEnvio(Valor As Integer)
  'True: si no hay m�s env�os de prueba a programar
  MiUltimoEnvio = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " UltimoEnvio = " & Valor
End Property

Private Property Let RespuestaEsperada(Valor As Boolean)
  'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador.
  MiRespuestaEsperada = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RespuestaEsperada = " & Valor
End Property

Private Property Let FuncionRespEsperada(Valor As TipoMensajeEnviado)
  MiFuncionRespEsperada = Valor
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " FuncionRespEsperada = " & Valor
End Property

Private Property Get ProgramPendiente() As Integer
  'Indica si existe alguna prueba que deb�a haber sido programada
  ProgramPendiente = MiProgramPendiente
End Property

Private Property Get BorradoPendiente() As Integer
  'Indica si existe alguna muestra que todav�a est� pendiente de borrar
  BorradoPendiente = MiBorradoPendiente
End Property

Private Property Get RepetirPruebas() As Integer
  'Indica si las pruebas se van a repetir o no
  RepetirPruebas = MiRepetirPruebas
End Property

Private Property Get Respuesta() As TipoRespuesta
  'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
  Respuesta = MiRespuesta
End Property

Private Property Get RespuestaAfirmativa() As Boolean
  'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
  RespuestaAfirmativa = MiRespuestaAfirmativa
End Property

Private Property Get TipoMensEnviado() As TipoMensajeEnviado
  'Indica el tipo de mensaje que el sistema transmite al autoanalizador y que puede ser:
  TipoMensEnviado = MiTipoMensEnviado
End Property

Private Property Get UltimoEnvio() As Integer
  'True: si no hay m�s env�os de prueba a programar
  UltimoEnvio = MiUltimoEnvio
End Property

Private Property Get RespuestaEsperada() As Boolean
  'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador.
  RespuestaEsperada = MiRespuestaEsperada
End Property

Private Property Get FuncionRespEsperada() As TipoMensajeEnviado
  FuncionRespEsperada = MiFuncionRespEsperada
End Property
