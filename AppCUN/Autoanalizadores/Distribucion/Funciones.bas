Attribute VB_Name = "Funciones"
Option Explicit

Public Function BuscaOrdenadorModelo()
  Dim strApplicationName As String
  Dim strKeyName As String
  Dim lpReturnedString As String * 4096
  Const nSize As Long = 4096
  Dim strFileName As String
  Dim Resultado As Long
  
  strApplicationName = "Ordenador_Modelo"
  strKeyName = "CPU"
  strFileName = FicheroModelo
  Resultado = GetPrivateProfileString(strApplicationName, strKeyName, 0&, lpReturnedString, nSize, strFileName)
  If Resultado = 0 Then MostrarMensajeError "Error al querer determinar el ordenador modelo"
  BuscaOrdenadorModelo = ObtenerString(lpReturnedString)
End Function
'obtiene una lista de todas las cadenas que hay en un buffer apuntado por un puntero
Public Function ObtenerString(strBuffer As String) As String
  Dim PosicionInicial As Long
  Dim PosicionFinal As String
  Dim Cadena As String
  
  PosicionInicial = 1
  Do While (Asc(Mid$(strBuffer, PosicionInicial, 1)) <> 0)
    PosicionFinal = InStr(PosicionInicial, strBuffer, Chr$(0))
    Cadena = Cadena + Mid$(strBuffer, PosicionInicial, PosicionFinal - PosicionInicial)
    PosicionInicial = PosicionFinal + 1
  Loop
  ObtenerString = Cadena
End Function
 
Public Function CrearUnidadRemota(strPath As String)
  Dim Password As String        'password de aaaceso a la unidad
  Dim NombreLocal As String     'unidad de red virtual que se crea localmente
  Dim Resultado As Long
  Dim Mensaje As String
  
  Password = ""                     'utiliza la del usuario que ejecuta la aplicacion
  NombreLocal = UnidadVirtual       'unidad virtual que crea
  Resultado = WNetAddConnection(strPath, Password, NombreLocal)
  If Resultado <> 0 Then
    Select Case Resultado
      Case 85:    Mensaje = " La unidad Virtual Z: ya existe"
      Case Else:  Mensaje = "No a podido conectarse a la maquina: " & Chr(10) & strPath
    End Select
    MostrarMensajeError Mensaje
  End If
End Function

Public Function EliminarUnidadRemota()
  Dim NombreRemoto As String
  Const Desconectar As Boolean = True
  
  NombreRemoto = UnidadVirtual
  EliminarUnidadRemota = WNetCancelConnection(NombreRemoto, Desconectar)
End Function

'devuelve los KB de una unidad de disco
'Unidad: unidad de disco del aque queremos conocer su espacio libre
Public Function EspacioDisco(Unidad As String) As Long
  Dim Espacio As Integer
  Dim Respuesta As Long
  Dim SectoresporCluster As Long, BytesporSector As Long
  Dim NumerodeClustersLibre As Long, NumeroTotaldeClusters As Long
  Dim KBytesTotales As Long, KBytesLibres As Long
  
  'determina la sintaxis correcta de la unidad
  Espacio = InStr(Unidad, " ")
  If Espacio > 0 Then Unidad = Left$(Unidad, Espacio - 1)
  If Right$(Unidad, 1) <> "\" Then Unidad = Unidad & "\"
  'determina el espacio libre y total de la unidad
  Respuesta = GetDiskFreeSpace(Unidad, SectoresporCluster, BytesporSector, NumerodeClustersLibre, NumeroTotaldeClusters)
  If Respuesta = 0 Then MostrarMensajeError ("No se ha posido determinar el espacio de disco remoto")
  KBytesTotales = NumeroTotaldeClusters * SectoresporCluster * (BytesporSector / 1024)
  KBytesLibres = NumerodeClustersLibre * SectoresporCluster * (BytesporSector / 1024)
  EspacioDisco = KBytesLibres
End Function

Public Function BuscarINI(Seccion As String, Parametro As String, Tipo As Tipos) As String
  Dim lpReturnedString As String * 4096
  Const nSize As Long = 4096
  Dim strFileName As String, Mensaje As String
  Dim Resultado As Long
  
  Select Case Tipo
    Case vbLocal:   strFileName = FicheroLocal
    Case vbModelo:  strFileName = FicheroModelo
  End Select

  Resultado = GetPrivateProfileString(Seccion, Parametro, -1&, lpReturnedString, nSize, strFileName)
  If Resultado = 0 Then
    If Tipo = vbLocal Then          'no se ha encontrado un valor para ese parametro en ddllocal.ini y se le da un valor por defecto
      Select Case Parametro
        Case "FechaDistribucion":   BuscarINI = FechaInic
        Case "Version":             BuscarINI = VersionInic
        Case "Tama�o":              BuscarINI = Tama�oInic
        Case "DirectorioModelo":    BuscarINI = DirectorioModeloInic
        Case "DirectorioLocal":     BuscarINI = DirectorioLocalInic
      End Select
      Mensaje = "Se le ha asignado el valor por defecto " & Parametro & "=" & BuscarINI
    End If
    Mensaje = "No se ha encontrado el valor " & Parametro & " de la DLL " & Seccion & " dentro del fichero " & strFileName & vbLf & Mensaje
    MostrarMensajeError Mensaje
  Else
    BuscarINI = ObtenerString(lpReturnedString)
  End If
End Function
'copia la dll desde el ordenador modelo al ordenador local
'Fichero: dll que hay que copiar
Public Function TraerDll(Fichero As String) As Boolean
  Dim DirectorioModelo As String, DirectorioLocal As String
  Dim Origen As String, Destino As String
  
  TraerDll = True
  DirectorioLocal = BuscarINI(Fichero, "DirectorioLocal", vbLocal)
  DirectorioModelo = BuscarINI(Fichero, "DirectorioModelo", vbLocal)
  Origen = UnidadVirtual & "\" & Fichero & ".dll"
  Destino = DirectorioLocal & "\" & Fichero & ".dll"
  If EspacioSuficiente(UnidadLocal, Origen) And Not ExisteLocal(Fichero, Destino) Then
    CopiarFichero.Show
    FileCopy Origen, Destino
    Unload CopiarFichero
    If Err <> 0 Then
      Select Case Err
        Case 53: MostrarMensajeError ("No se ha podido copiar la dll " & Fichero & " hasta este ordenador porque no se localizo el fichero")
      End Select
      TraerDll = False
    End If
  End If
End Function
Public Function ActualizarLocal(Fichero As String) As Boolean
  Dim Valor As String
  
  Valor = BuscarINI(Fichero, "FechaDistribucion", vbModelo)
  Call EscribirINI(Fichero, "FechaDistribucion", Valor, vbLocal)
  Valor = BuscarINI(Fichero, "Version", vbModelo)
  Call EscribirINI(Fichero, "Version", Valor, vbLocal)
  Valor = BuscarINI(Fichero, "Tama�o", vbModelo)
  Call EscribirINI(Fichero, "Tama�o", Valor, vbLocal)
  Call EscribirINI(Fichero, "FechaActualizacion", CDate(Now), vbLocal)
End Function

Public Function EscribirINI(Seccion As String, Parametro As String, Valor As String, Tipo As Tipos) As Boolean
  Dim strFileName As String
  Dim Resultado As Long
  
  Select Case Tipo
    Case vbLocal:   strFileName = FicheroLocal
    Case vbModelo:  strFileName = FicheroModelo
  End Select
  EscribirINI = WritePrivateProfileString(Seccion, Parametro, Valor, strFileName)
End Function

Public Sub VerificarDll(FicheroDll As String)
  Dim FechaModelo As String, FechaLocal As String, Fecha As String
  Dim VersionLocal As String, VersionModelo As String
  Dim Tama�oLocal As String, Tama�oModelo As String
  Dim Actualizar As Boolean, Error As Boolean
  Dim Resultado As String

  'comprobar que la dll existe en dlllocal.ini y en caso contrario crearlo
  If Not ExisteSeccion(FicheroDll, vbLocal) Then Resultado = CrearSeccion(FicheroDll, vbLocal)
  'obtenemos los distintos parametros de una dll, tanto locales como remotos
  FechaLocal = BuscarINI(FicheroDll, "FechaDistribucion", vbLocal)
  FechaModelo = BuscarINI(FicheroDll, "FechaDistribucion", vbModelo)
  VersionLocal = BuscarINI(FicheroDll, "Version", vbLocal)
  VersionModelo = BuscarINI(FicheroDll, "Version", vbModelo)
  Tama�oLocal = BuscarINI(FicheroDll, "Tama�o", vbLocal)
  Tama�oModelo = BuscarINI(FicheroDll, "Tama�o", vbModelo)
  'si en el ini modelo no se dispone de los datos minimos sobre una dll no hace nada
  If Not (FechaModelo = "" Or VersionModelo = "" Or Tama�oModelo = "") Then
    'si la version local no es la ultima la traemos y actualizamos el ini local
    If (FechaLocal <> FechaModelo) Or (VersionLocal <> VersionModelo) Or (Tama�oLocal <> Tama�oModelo) Then
'      Actualizar = TraerDll(FicheroDll)
      If TraerDll(FicheroDll) Then ActualizarLocal (FicheroDll)
    End If
  Else
    MostrarMensajeError ("En el fichero DllModelo.ini faltan datos sobre la dll " & FicheroDll)
  End If
End Sub

'devuelve True si existe espacio en la Unidad para poder copiar el Fichero, False si no hay espacio suficiente
Public Function EspacioSuficiente(Unidad As String, Fichero As String) As Boolean
  Dim Tama�o  As Long
  Dim Espacio As Long
  On Error GoTo Error:
  
  Tama�o = (FileLen(Fichero)) / 1024
  Espacio = EspacioDisco(Unidad)
  EspacioSuficiente = IIf(Espacio > Tama�o, True, False)
  Exit Function
  
Error:
  MostrarMensajeError "No existe el fichero " & Fichero & "en el ordenador modelo"
End Function

'determina si existe la seccion correspondiente de una dll en el fichero ini
'devuelve True o False seg�n exista o no.
Public Function ExisteSeccion(Seccion As String, Tipo As Tipos) As Boolean
  Dim lpReturnedString As String * 4096
  Const nSize As Long = 4096
  Dim strFileName As String
  Dim Resultado As Long

  Select Case Tipo
    Case vbLocal:   strFileName = FicheroLocal
    Case vbModelo:  strFileName = FicheroModelo
  End Select
  Resultado = GetPrivateProfileSection(Seccion, lpReturnedString, nSize, strFileName)
  If Resultado = 0 Then
    MostrarMensajeError "No se ha encontrado la seccion " & Seccion & " en el fichero " & strFileName
    ExisteSeccion = False
  Else
    ExisteSeccion = True
  End If
End Function

'crea una seccion en un fichero ini con una serie de valores por defecto
'devuelve True si tuvo exito y False si no
Public Function CrearSeccion(Seccion As String, Tipo As Tipos) As Boolean
  Dim lpString As String * 150
  Dim strFileName As String
  Dim Resultado As Long

  Select Case Tipo
    Case vbLocal:   strFileName = FicheroLocal
    Case vbModelo:  strFileName = FicheroModelo
  End Select
  lpString = "FechaDistribucion=" & FechaInic & Chr(0) & _
             "Version=" & VersionInic & Chr(0) & _
             "Tama�o=" & Tama�oInic & Chr(0) & _
             "CRC=" & Chr(0) & _
             "FechaActualizacion=" & Chr(0) & _
             "PCModelo=02231" & Chr(0) & _
             "DirectorioModelo=" & Chr(0) & _
             "DirectorioLocal=" & DirectorioLocalInic & Chr(0) & _
             " "
  Resultado = WritePrivateProfileSection(Seccion, lpString, strFileName)
  If Resultado = 0 Then
    MostrarMensajeError "No se ha podido crear la seccion " & Seccion & " en el fichero " & strFileName
    CrearSeccion = False
  Else
    CrearSeccion = True
  End If
End Function

Public Function ExisteLocal(Fichero As String, Directorio As String) As Boolean
  Dim FechaCreacion As Date, Fecha As Date
  
  On Error Resume Next
  FechaCreacion = FileDateTime(Directorio)
  If Err Then FechaCreacion = FechaInic    'el fichero ini contiene informacion sobre una dll que no existe
  Fecha = CDate(BuscarINI(Fichero, "FechaDistribucion", vbModelo))
  ExisteLocal = IIf(FechaCreacion = Fecha, True, False)
End Function
