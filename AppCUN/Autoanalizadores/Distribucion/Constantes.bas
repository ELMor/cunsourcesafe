Attribute VB_Name = "Constantes"
Option Explicit


Public Const UnidadLocal As String = "C:"
Public Const UnidadVirtual As String = "Z:"
Public Const FicheroModelo As String = "DllModelo.ini"
Public Const FicheroLocal As String = "DllLocal.ini"

Public Const FechaInic As String = "01/01/2000"
Public Const VersionInic As String = "0.0"
Public Const TamañoInic As String = "0"
Public Const DirectorioModeloInic As String = ""
Public Const DirectorioLocalInic As String = "C:\Dll"

Enum Tipos
  vbLocal = 1
  vbModelo = 2
End Enum

