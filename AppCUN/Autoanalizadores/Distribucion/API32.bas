Attribute VB_Name = "API32"
Option Explicit

'FICHEROS INI
'devuelve todas las llaves y valores de una secccion especifica
Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" _
  (ByVal lpAppName As String, _
  ByVal lpReturnedString As String, _
  ByVal nSize As Long, _
  ByVal lpFileName As String) As Long

'replaza las llaves y valores en una seccion especifica en un fichero ini
Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" _
  (ByVal lpAppName As String, _
  ByVal lpString As String, _
  ByVal lpFileName As String) As Long

'devuelve una cadena de un sector especificado en un fichero de inicializacion
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" _
  (ByVal lpApplicationName As String, _
  ByVal lpKeyName As Any, _
  ByVal lpDefault As String, _
  ByVal lpReturnedString As String, _
  ByVal nSize As Long, _
  ByVal lpFileName As String) As Long

'devuelve un entero asociado a una llave en un sector de un fichero de inicializacion
Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" _
  (ByVal lpApplicationName As String, _
  ByVal lpKeyName As String, _
  ByVal nDefault As Long, _
  ByVal lpFileName As String) As Long

'copia una cadena en un sector especifico de un fichero de inicializacion
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" _
  (ByVal lpApplicationName As String, _
  ByVal lpKeyName As Any, _
  ByVal lpString As Any, _
  ByVal lpFileName As String) As Long

'CONEXIONES REMOTAS POR RED
'Realiza una conexion de red a un ordenador
Declare Function WNetAddConnection Lib "mpr.dll" Alias "WNetAddConnectionA" _
  (ByVal lpszNetPath As String, _
  ByVal lpszPassword As String, _
  ByVal lpszLocalName As String) As Long

'Termina una conexion de red
Declare Function WNetCancelConnection Lib "mpr.dll" Alias "WNetCancelConnectionA" _
  (ByVal lpszName As String, _
  ByVal bForce As Long) As Long
'lpszName String  : The remote name or local name of the connected resource.
'bForceLong       : True to force disconnection even if there are open files or jobs on the connected resource.

'ESPACIO EN DISCO DURO REMOTO
Declare Function GetDiskFreeSpace Lib "kernel32" Alias "GetDiskFreeSpaceA" _
(ByVal lpRootPathName As String, _
lpSectorsPerCluster As Long, _
lpBytesPerSector As Long, _
lpNumberOfFreeClusters As Long, _
lpTtoalNumberOfClusters As Long) As Long

  


'CONSTANTES DE MASCARA DE SEGURIDAD

'CLAVES PREDEFINIDAS DEL REGISTRO USADAS EN EL ARGUMENTO hKey

'CODIGOS ERROR DE RETORNO

'CONSTANTES PUBLICAS DE TIPO DE DATOS

'OPCIONES

Sub MostrarCodigoErrores(CodError As Long)
  Select Case CodError
    Case Else
  End Select
End Sub

Public Sub MostrarMensajeError(Cadena As String)
  MsgBox Cadena, vbCritical, "Error"
End Sub
