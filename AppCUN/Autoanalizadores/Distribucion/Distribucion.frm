VERSION 5.00
Begin VB.Form Distribucion 
   Caption         =   "Form1"
   ClientHeight    =   735
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5130
   LinkTopic       =   "Form1"
   ScaleHeight     =   735
   ScaleWidth      =   5130
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Caption         =   "dll5"
      Height          =   495
      Left            =   4080
      TabIndex        =   4
      Top             =   120
      Width           =   800
   End
   Begin VB.CommandButton Command4 
      Caption         =   "dll4"
      Height          =   495
      Left            =   3120
      TabIndex        =   3
      Top             =   120
      Width           =   800
   End
   Begin VB.CommandButton Command3 
      Caption         =   "dll3"
      Height          =   495
      Left            =   2160
      TabIndex        =   2
      Top             =   120
      Width           =   800
   End
   Begin VB.CommandButton Command2 
      Caption         =   "dll2"
      Height          =   495
      Left            =   1200
      TabIndex        =   1
      Top             =   120
      Width           =   800
   End
   Begin VB.CommandButton Command1 
      Caption         =   "dll1"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   800
   End
End
Attribute VB_Name = "Distribucion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Call VerificarDll("dll1")
End Sub

Private Sub Command2_Click()
  Call VerificarDll("dll2")
End Sub

Private Sub Command3_Click()
  Call VerificarDll("dll3")
End Sub

Private Sub Command4_Click()
  Call VerificarDll("dll4")
End Sub

Private Sub Command5_Click()
  Call VerificarDll("dll5")
End Sub

Private Sub Form_Load()
  
  Dim CPUModelo As String       'ordenador modelo donde esta el fichero dllmodelo.ini
  Dim Directorio As String      'directorio en el ordenador modelo donde esta el fichero dllmodelo.ini
  Dim Resultado As Long
  Dim Tama�o As Long

  Screen.MousePointer = vbHourglass
  'buscar cual es el ordenador modelo y el directorio en el modelo en el que esta el fichero dllmodelo.dll
  CPUModelo = BuscarINI("Ordenador_Modelo", "CPU", vbModelo)
  Directorio = BuscarINI("Ordenador_Modelo", "DirIni", vbModelo)
  'conectar con el ordenador modelo
  Resultado = CrearUnidadRemota("\\" & CPUModelo & "\" & Directorio)
  'buscar cual es el directorio en el ordenador local en el que guarda el fichero dllmodelo.dll
  Directorio = BuscarINI("Ordenador_Local", "DirIni", vbModelo)
  Tama�o = (FileLen(UnidadVirtual & "\" & FicheroModelo) / 1024)
  If Tama�o < EspacioDisco(UnidadLocal) Then
    'traernos el fichero DllModelo.ini al directorio local
    FileCopy UnidadVirtual & "\" & FicheroModelo, Directorio & "\" & FicheroModelo
  Else
    MostrarMensajeError ("No hay espacio suficiente en el disco local para copiar el fichero " & FicheroModelo)
  End If
  Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim Resultado As Long
  
  Screen.MousePointer = vbHourglass
  'desconexion del ordenador modelo
  Resultado = EliminarUnidadRemota
  Screen.MousePointer = vbDefault
End Sub
