VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Axsym"
   ClientHeight    =   930
   ClientLeft      =   6225
   ClientTop       =   690
   ClientWidth     =   1965
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Axsym.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   930
   ScaleWidth      =   1965
   Begin VsOcxLib.VideoSoftAwk Awk4 
      Left            =   1440
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Timer TimeOutSistema 
      Enabled         =   0   'False
      Interval        =   20000
      Left            =   810
      Top             =   480
   End
   Begin VB.Timer TimeOutAuto 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   390
      Top             =   480
   End
   Begin VB.Timer TimerReintento 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   -30
      Top             =   480
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   960
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
  'Ultima revisi�n: 24-07-2000
  '***** NOTAS DE INTER�S GENERAL PARA EL FUNCIONAMIENTO *****
  'Por el momento no se considera la posibilidad de que se llene el buffer de recepci�n de datos y se produzca un error RXOVER.
  'Si esto pudiera llegar a ocurrir la soluci�n ser� una de las siguientes:
  '   1 - Aumentar el tama�o del buffer de recepci�n
  '   2 - Enviar un Xoff cuando el buffer est� proximo a llenarse
  
  Option Explicit
  
  Dim MiObjActuacion As clsActuacion
  Dim ContIntentosTrans As Integer        'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
  Dim ContIntentosComunic As Integer      'cuenta el n� de veces que se intenta comunicar con el autoanalizador
  Dim TipoFrame As Integer                'determina el orden en que han de enviarse los frames:Header, Patient, Order, Termination
  Dim UltimoEnvio As Integer              'True: si no hay m�s env�os de prueba a programar

Private Sub AXSYM_Esclavo(Mensaje As String, Caracter As String)
  'Identifica el final de un bloque pasando a su lectura.
  'Tambi�n se identifica el final de la comunicaci�n y se vuelve al estado de espera.
  
  On Error GoTo AutoAnError
  TimeOutAuto.Enabled = False                           'Al recibir algun caracter desactivamos el timer
  Select Case Caracter
    Case Chr$(EOT)                                      '<EOT> - Final de la transmisi�n por parte del autoanalizador
      objAutoAn.Estado = cteEspera
      Mensaje = ""
    Case Chr$(LF)
      objAutoAn.Enviar Chr$(ACK)                        'al haber recibido el final del TipoFrame se contesta <ACK>
      TimeOutAuto.Enabled = True                        'se activa el TimeOut para recibir el proximo TipoFrame
      If Mid$(Right$(Mensaje, 9), 1, 2) = "L|" Then     'Se comprueba si se trata del ultimo TipoFrame del mensaje
        Lectura_Datos (Mensaje)                         'para pasar a obtener el resultado de la prueba
        Mensaje = ""
      End If
  End Select
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "AXSYM_Esclavo", "AXSYM_Esclavo")
End Sub

Private Sub AXSYM_Espera(Mensaje As String, Caracter As String)
  'Identifica la llegada de los caracteres de solicitud de comunicaci�n, la acepta y pasa al estado de transmisi�n.
  
  On Error GoTo AutoAnError
  If Caracter = Chr$(ENQ) Then          '<ENQ> - El autoanalizador solicita respuesta a su solicitaci�n de comunicaci�n
    TimeOutSistema.Enabled = False      'por si acaso le habiamos mandado un <ENQ> simultaneo desactivamos el timer de espera
    TimeOutAuto.Enabled = True          'Se controla el tiempo que tarda el autoanaliz. en volver a mandar el siguiente mensaje
    objAutoAn.Enviar Chr$(ACK)          '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
    objAutoAn.Estado = cteESCLAVO
    Mensaje = ""                        'Se eliminan los posibles caracteres enviados antes de establecerse la comunicaci�n
  End If
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "AXSYM_Espera", "AXSYM_Espera")
End Sub

Private Sub AXSYM_Master(Mensaje As String, Caracter As String)
  'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
  'Si el mensaje es rechazado habr� que volver a enviarlo.
  'Si es aceptado se envia el siguiente TipoFrame del mensaje. Cada mensaje esta compuesto de 4 frames (vble TipoFrame):
  '-Header,-Patient,-Order,-Termination cada uno de ellos precedido por un n� secuencial (0-7) (vble linea)
  
  Dim Sql As String
  On Error GoTo AutoAnError
  TimeOutAuto.Enabled = False                       'Se desactiva el timeout, pues ya nos ha respondido algo
 
  Select Case Caracter
    Case Chr$(EOT)                                  'existe la posibilidad de que el Axsym conteste con <EOT> en vez de <ACK>
      objAutoAn.Estado = cteEspera                  'para solicitar que acabe la comunicaci�n. En tal caso pasaremos al estado de espera
    Case Chr$(ACK)                                  '<ACK> - El autoanalizador env�a un reconocimiento positivo
      ContIntentosTrans = 0
      If UltimoEnvio = True And TipoFrame = 4 Then  'cuando hemos acabado de transmitir todo
        objAutoAn.Enviar Chr$(EOT)                  '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        objAutoAn.Estado = cteEspera
        MiObjActuacion.strEstado = cteACEPTADA
        MiObjActuacion.Muestra.strEstado = cteACEPTADA
        Exit Sub
      End If
      If TipoFrame < 4 Then                            'quedan mas frames por transmitir
        TipoFrame = TipoFrame + 1
      Else                                             'como se han enviado todos los frames de una prueba
        MiObjActuacion.strEstado = cteACEPTADA         'se ha aceptado la programacion de una prueba
        TipoFrame = Header                             'y se iniciliza el TipoFrame para otra prueba
      End If
      Envio_Mensajes                                   'Se enviar� el siguiente mensaje
      Mensaje = ""
    Case Chr$(NAK)                                     '<NAK> - El autoanalizador no ha aceptado el ultimo TipoFrame enviado
      TimerReintento.Enabled = True                    'se activa el timerreintento para volver a reenviar el ultimo TipoFrame enviado
      objAutoAn.Estado = cteMASTER
      Mensaje = ""
      Linea cteResta                                   'para que linea no aumente, pues se va a reenviar la ultima linea
    Case Else
      Sql = "Se ha recibido un caracter." + (Caracter) 'parada de comprobacion para ver si se mandan otros caracteres
      MsgBox Sql, vbExclamation, frmPrincipal.Caption
  End Select
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "AXSYM_Master", "AXSYM_Master")
End Sub

Private Sub AXSYM_Solicitud(Mensaje As String, Caracter As String)
  'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n,
  'el autoanalizador responde aceptando <ACK> o negando dicha comunicaci�n <NAK>.

  On Error GoTo AutoAnError
  TimeOutAuto.Enabled = False           'puesto que ya nos ha respondido algo, se desactiva el TimeOut
  Select Case Caracter
    Case Chr$(ACK)                      '<ACK> - El autoanalizador ha aceptado la comunicaci�n
      ContIntentosComunic = 0           'inicializamos el n� de intentos de establecimiento de comunicacion pues ya se ha conseguido
      objAutoAn.Estado = cteMASTER
      Mensaje = ""
      Envio_Mensajes                    'Se env�a el bloque de datos por parte del sistema al autoanalizador '!!MODIFICAR
    Case Chr$(NAK)                      '<NAK>  - El autoanalizador no ha aceptado la comunicaci�n
      objAutoAn.Estado = cteEspera
      Mensaje = ""
      TimerReintento.Enabled = True     'Se espera 10 segundos para intentar una nueva comunicaci�n
    Case Chr$(ENQ)                      '<ENQ>- El autoanalizador ha "solicitado l�nea" a la vez que el sistema
      objAutoAn.Estado = cteEspera      'No se manda nada y se pasa a Espera para esperar a recibir el nuevo intento del Axsym
      Mensaje = ""
  End Select
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "AXSYM_Solicitud", "AXSYM_Solicitud")
End Sub

Public Sub Programar()
  On Error GoTo AutoAnError
 
  If objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then        'para solicitar comunicacion en caso de que haya algo que mandar.
    Envio_Mensajes
  Else
    MsgBox "�Todas las pruebas est�n ya programadas!" & vbCr & "Para volver a programarlas paselas a estado 'Sin Programar'", _
           vbExclamation, frmPrincipal.Caption
  End If
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Programar", "Programar")
End Sub

Private Sub Envio_Mensajes()
  'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar el
  'caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego se manda
  'el mensaje propiamente dicho hasta que sea recibido correctamente por el autoanalizador.
  'Finalmente se cierra la comunicaci�n enviando <EOT> para dar paso a la respuesta del
  'autoanalizador al requerimiento del sistema.
  
  Dim Tiempo As Long
  Dim RES As Integer
  Dim Sql As String
  On Error GoTo AutoAnError

  With objAutoAn
    Select Case .Estado
      Case cteEspera                                        'el autoanalizador esta en espera
        ContIntentosComunic = ContIntentosComunic + 1       'incrementa el numero de intentos
        If ContIntentosComunic = 6 Then                     'Fallo en la comunicaci�n despu�s de 6 intentos
          ContIntentosComunic = 1
          Sql = "No se puede establecer comunicaci�n."
          Select Case MsgBox(Sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
            Case vbRetry
              Tiempo = Timer + 5
              Do While Tiempo > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
              Exit Sub
            Case vbCancel
              Descargar
          End Select
        Else                                                'no hay fallo en la comunicacion
          Sistema_Espera                                    'realiza un solicitud de comunicacion y pasa a estado de solicitud
          .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
          Exit Sub
        End If
      Case cteMASTER
        ContIntentosTrans = ContIntentosTrans + 1
        If ContIntentosTrans = 6 Then                       'Fallo en la transmisi�n despu�s de 6 intentos
          ContIntentosTrans = 0
          .Estado = cteEspera
          Sql = "No se puede transmitir informaci�n."
          Select Case MsgBox(Sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
            Case vbRetry                                    'Se reintenta mandar todo el registro actual
              Tiempo = Timer + 5
              Do While Tiempo > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              ContIntentosComunic = 1
              Exit Sub
            Case vbAbort                                    'Se sale del programa y se queda la prueba sin programar
              Descargar
            Case vbIgnore                                   'Se queda la prueba como no aceptada y continua con el siguiente registro
              Tiempo = Timer + 5
              Do While Tiempo > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              Exit Sub
          End Select
        Else                                                'hay una transmision correcta
          Sistema_Master
          Exit Sub
        End If
    End Select
  End With
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Envio_Mensajes", "Envio_Mensajes")
End Sub

Private Function fCheckFin(sMensaje As String) As String
  'Subrutina que establece el check del mensaje m�s <CR><LF> del final del TipoFrame

  Dim iContador As Integer                         'cuenta el n�mero de caracteres le�dos
  Dim iSuma As Integer                             'suma de los c�digos ASCII
  Dim sCheck As String
  On Error GoTo AutoAnError

  iSuma = 0                                        'suma el ASCII de los caracteres del mensaje
  For iContador = 1 To Len(sMensaje)
    iSuma = iSuma + Asc(Mid$(sMensaje, iContador, 1))
  Next iContador
  If Len(Hex$(iSuma)) = 1 Then                     'establece el Check del mensaje a dos digitos
    sCheck = "0" & Hex$(iSuma)
  Else
    sCheck = Right$(Hex$(iSuma), 2)
  End If
  fCheckFin = sCheck + vbCrLf
  Exit Function
    
AutoAnError:
  Call objError.InternalError(Me, "fCheckFin", "fCheckFin")
End Function

Private Sub Lectura_Datos(datos As String)
  'determina si que el mensaje que ha recibido del autoanalizador es el resultado de una prueba (datos contiene todos los frames).
  'en caso afirmativo determina si esta la muestra existe, en cuyo caso determina su c�digo de muestra,
  'su dilucion si procediera, co lo que calcula el resultado de la prueba
  
  Dim Posicion As Integer
  Dim Resultado As String
  Dim i As Integer
  Dim CodMuestra As String
  Dim codResultadoAuto As String
  Dim dblDilucion As Double
  On Error GoTo AutoAnError

  Awk1.FS = Chr$(LF): Awk1 = datos            'Se separan los distintos tipos de registros del mensaje
  Awk2.FS = "|": Awk2 = Awk1.F(1)             'El awk1.F(1) contiene la cabecera (registro tipo H)
  If UCase$(Left$(Awk2.F(12), 1)) = "P" Then  'El awk1.F(12) nos indica que tipo de mensaje es: registro tipo P (resultados)
    Awk2.FS = "|": Awk2 = Awk1.F(3)           'Siempre va a haber un solo resultado por muestra-prueba
    Awk3.FS = "^": Awk3 = Awk2.F(4)
    Awk4 = Awk3.F(1): Awk4.FS = "/"           'awk4 contiene el c�digo de la muestra, y si corresponde las diluciones y el tiempos
    dblDilucion = 1                           'inicializacion de la dilucion
    For i = 1 To Awk4.NF
      Select Case i
        Case 1: CodMuestra = Awk4.F(1)        'calculo del codigo de la muestra
        Case Else
          Select Case True
            Case Left$(Awk4.F(i), 1) = "D": Posicion = 2
            Case InStr(1, Awk4.F(i), "D") > 0: Posicion = InStr(1, Awk4.F(i), "D") + 1
          End Select
          If Posicion > 0 Then
            If IsNumeric(Val(Mid$(Awk4.F(i), Posicion))) Then
              dblDilucion = Val(Mid$(Awk4.F(i), Posicion))        'calculo, si procede, de la dilucion
            Else
              MsgBox "Se Ha Recibido un resultado con dilucion  no determinada." & _
              "Muestra" & CodMuestra, vbCritical + vbOKOnly, "Diluci�n Indefinida"
            End If
          End If
      End Select
    Next i
    If objAutoAn.blnExistMuestra(CodMuestra) Then
      If UCase$(Mid$((Awk1.F(4)), 3, 1)) = "R" Then
        Awk2 = Awk1.F(3)
        Awk3.FS = "^": Awk3 = Awk2.F(5)
        codResultadoAuto = Awk3.F(5)                              'Se obtiene el c�digo de la prueba
        Awk2 = Awk1.F(4)
        If Awk2.F(4) <> "" Then
          Awk3.FS = "^"
          Awk3 = Awk2.F(3)
          Resultado = Awk2.F(4)                                   'se obtiene el resultado dela prueba sin diluir
          If dblDilucion <> 1 And dblDilucion <> 0 Then           'se obtiene el resultsdo de la muestra si esta diluida
            If IsNumeric(Resultado) Then
              Resultado = CStr(Val(Resultado) * dblDilucion)
            Else
              Resultado = Resultado & " (* " & CStr(dblDilucion) & ")"
            End If
          End If
          Call objAutoAn.pIntroResultConCResAuto(CodMuestra, codResultadoAuto, Resultado, True)
        End If
      End If
    End If
  End If
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Lectura_Datos", "Lectura_Datos")
End Sub

Public Sub Lectura_Protocolo(Caracter As String)
  Static Mensaje As String          'Contiene el mensaje que env�a el autoanalizador
  Dim CopiaMensaje As String        'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
  Mensaje = Mensaje & Caracter
  CopiaMensaje = Mensaje
  Select Case objAutoAn.Estado
    Case cteEspera
      AXSYM_Espera CopiaMensaje, Caracter:      Mensaje = CopiaMensaje
    Case cteSOLICITUD
      AXSYM_Solicitud CopiaMensaje, Caracter:   Mensaje = CopiaMensaje
    Case cteESCLAVO
      AXSYM_Esclavo CopiaMensaje, Caracter:     Mensaje = CopiaMensaje
    Case cteMASTER
      AXSYM_Master CopiaMensaje, Caracter:      Mensaje = CopiaMensaje
  End Select
End Sub

Private Function Linea(Optional Sumador As Operation) As String
  'controla el numero de frame que corresponde en cada momento, inicializandolo o
  'incrementandolo seg�n se al primero a enviar o no
  
  Static MiLinea As Integer
  Select Case Sumador
    Case cteReset:  MiLinea = 0                                     'inicializa
    Case cteResta:  MiLinea = IIf(MiLinea <> 0, MiLinea - 1, 7)     'decremente
    Case Else:      MiLinea = IIf(MiLinea < 7, MiLinea + 1, 0)      'incremente
  End Select
  Linea = Trim$(Str$(MiLinea))
End Function

Public Sub Descargar()
  'Fin de programa
  objAutoAn.EscribirLog Format(Now, "hh:mm:ss") & " Fin de Ejecuci�n"
  End
End Sub

Public Function Salir() As Boolean
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar
End Function

Private Function fRecords() As String
  'En esta subrutina se define que TipoFrame es el que toca enviar seg�n la vble TipoFrame
  
  Dim Mensaje As String
  Dim objMuestra As clsMuestra
  Dim objActuacion As clsActuacion
  On Error GoTo AutoAnError
  
  Select Case TipoFrame
    Case Header
      For Each objMuestra In objAutoAn.ColMuestras                          'busca una actuacion que se haya intentado programar
        For Each objActuacion In objMuestra.ColMuestrasPruebas
          If objActuacion.strEstado = cteINTENTOPROGRAMAR Then GoTo Salto
        Next
      Next
      For Each objMuestra In objAutoAn.ColMuestras                          'busca actuaciones sin programar y las intenta programar
        For Each objActuacion In objMuestra.ColMuestrasPruebas
          If objActuacion.strEstado = cteSINPROGRAMAR Then
            objActuacion.strEstado = cteINTENTOPROGRAMAR
            Set MiObjActuacion = objActuacion
            GoTo Salto
          End If
        Next
      Next
Salto:
                      Mensaje = Linea + "H|\^&||||||||||P|1"
    Case Patient:     Mensaje = Linea + "P|1||||"
    Case Order:       Mensaje = Linea & fOrderRecord
    Case Terminator:  Mensaje = Linea + "L|1"
  End Select
  Mensaje = Mensaje + Chr$(CR) + Chr$(ETX)                  '<CR><ETX>
  fRecords = Chr$(STX) & Mensaje & fCheckFin(Mensaje)       '<STX>+Mensaje+Check
  Exit Function
    
AutoAnError:
  Call objError.InternalError(Me, "fRecords", "fRecords")
End Function

Private Sub Sistema_Espera()
  'se hace la solicitud de cominicaci�n cuando se van a enviar datos al autoanalizador, pasando al estado de solicitud
  'se inicializa el tipo de frame y el numero de linea
  
  On Error GoTo AutoAnError
  objAutoAn.Enviar Chr$(ENQ)        'Se env�a el caracter de solicitud de comunicaci�n <ENQ>
  objAutoAn.Estado = cteSOLICITUD   'y se pasa a estado de Solicitud
  TipoFrame = Header                'Se reinicializa el contador del TipoFrame a transmitir
  Linea cteReset                    'y el n� linea secuencial
  TimeOutAuto.Enabled = True        'Empieza a contar el tiempo para el TimeOut
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Sistema_Espera", "Sistema_Espera")
End Sub

Private Sub Sistema_Master()
  'Se transmiten datos al autoanalizador. Los datos a enviar pueden ser de dos tipos:
  'programaci�n de pruebas o borrado de pruebas programadas.
  
  On Error GoTo AutoAnError
  objAutoAn.Enviar fRecords               'Se env�a el mensaje una vez lo ha construido seg�n el TipoFrame
  TimeOutAuto.Enabled = True              'Empieza a contar el tiempo para el TimeOut
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Sistema_Master", "Sistema_Master")
End Sub

Private Function fOrderRecord() As String
  'Se forma el TipoFrame que corresponde al de programacion de la muestra y prueba
  'Es el unico que depende de la matrizpruebas

  Dim Mensaje As String
  Dim CodMuestra As String                              'c�digo de la muestra
  Dim CodPrueba As String                               'c�digo de la prueba para el autoanalizador
  Dim objActuacion As clsActuacion
  Dim objMuestra As clsMuestra
  On Error GoTo AutoAnError
  
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      CodMuestra = .strCodMuestra
      If .intTiempo <> 0 Then                               'se a�ade el tiempo de la muestra en caso de curva (y que no sea basal)
        CodMuestra = CodMuestra + "/T" & CStr(.intTiempo)
      End If
      If .intDilucion <> 1 And .intDilucion <> 0 Then       'se a�ade la dilucion de la muestra
        CodMuestra = CodMuestra & "/D" & CStr(objAutoAn.fFormatearNumero(CStr(.intDilucion), ",", "."))
      End If
      For Each objActuacion In .ColMuestrasPruebas          'se programa una actuacion que se esta intentando
        If objActuacion.strEstado = cteINTENTOPROGRAMAR Then
          CodPrueba = objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto
          fOrderRecord = "O|1|" & CodMuestra & "||^^^" & CodPrueba & "|||||||A||||||||||||||O"
          Exit For
        End If
      Next
    End With
  Next
  UltimoEnvio = True
  For Each objMuestra In objAutoAn.ColMuestras                                          'mira si hay alguna actuacion sin programar?
    For Each objActuacion In objMuestra.ColMuestrasPruebas
      If objActuacion.strEstado = cteSINPROGRAMAR Then UltimoEnvio = False: Exit For    'si queda alguna muestra sin programar
    Next
    If Not UltimoEnvio Then Exit For
  Next
  Exit Function
    
AutoAnError:
  Call objError.InternalError(Me, "fOrderRecord", "fOrderRecord")
End Function

Private Sub TimerReintento_Timer()
  'Timer que se activa para volver a intentar transmitir
  TimerReintento.Enabled = False
  Select Case objAutoAn.Estado
    Case cteMASTER        'Habiendo el sistema enviado datos, el autoanalizador responde que el TipoFrame era incorrecto
      Envio_Mensajes      'para realizar un nuevo intento de envio de datos
    Case cteSOLICITUD     'Habiendo el sistema "solicitado l�nea", el autoanalizador responde que no da comunicacion y el sistema lo reintentar
      objAutoAn.Estado = cteEspera
      Envio_Mensajes      'para realizar un nuevo intento de envio de datos
  End Select
End Sub

Private Sub TimeOutSistema_Timer()
  'Timer utilizado para controlar los margenes de tiempo de respuesta (15 SEGUNDOS) TIMEOUT a nuestras transmisiones
  TimeOutAuto.Enabled = False
  With objAutoAn
    Select Case .Estado
      Case cteSOLICITUD         'Habiendo el sistema "solicitado l�nea", el autoanalizador no responde
        .Enviar Chr$(EOT)       '<EOT>
        .Estado = cteEspera
        Envio_Mensajes          'para realizar un nuevo intento de comunicacion
        ContIntentosTrans = 1
      Case cteMASTER            'Habiendo el sistema enviado un TipoFrame, el autoanalizador no responde
        .Enviar Chr$(EOT)       '<EOT>
        .Estado = cteEspera     'Se pasa a estado espera para volver a solicitar comunic. y seguir mandando frames
                                'a partir del ultimo enviado correctamente
        Envio_Mensajes          'para realizar un nuevo intento de comunicacion
        ContIntentosComunic = 1
    End Select
  End With
End Sub

Private Sub TimeOutAuto_Timer()
  'Controla el tiempo que transcurre desde los �ltimo datos recibidos por el sistema en estado de exclavo.
  'Si pasan 30 segundos sin recibir datos o el final de la comunicaci�n <EOT> se finaliza la ejecuci�n
  Dim Sql As String
  Dim Respuesta As Integer
  
  TimeOutAuto.Enabled = False
  Sql = "Se ha producido un TimeOut durante la comunicaci�n por parte del autoanalizador."
  Sql = Sql & Chr$(LF) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
  Respuesta = MsgBox(Sql, vbExclamation + vbYesNo, frmPrincipal.Caption)
  Select Case Respuesta
    Case vbYes: TimeOutAuto.Enabled = True
    Case vbNo:  Descargar
  End Select
  objAutoAn.Enviar Chr$(EOT)        'al haberse agotado el tiempo de espera, se le manda <EOT> al autoanalizador
  objAutoAn.Estado = cteEspera
End Sub

Public Sub pModificaMuestra(objMuestra As clsMuestra)
  'carga el tiempo de una muestra desde la tabla MuestraAsistencia en el grid de Muestras
  
  Dim Qry1 As rdoQuery
  Dim Rs1 As rdoResultset
  Dim Sql As String
  On Error GoTo AutoAnError
  
  With objMuestra
    Sql = "SELECT tiempo FROM MuestraAsistencia WHERE cMuestra = ?"
    Set Qry1 = objAutoAn.rdoConnect.CreateQuery("", Sql)
    Qry1(0) = .strCodMuestra
    Set Rs1 = Qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    .strProperty1 = IIf(IsNull(Rs1(0)), "", Rs1(0))                     'actualiza el tiempo
    Rs1.Close: Qry1.Close
    If .intDilucion = 0 Then .intDilucion = 1
  End With
  Exit Sub
AutoAnError:
  Call objError.InternalError(Me, "pModificaMuestra", "pModificaMuestra")
End Sub

Public Sub pModificaActuacion(objActuacion As clsActuacion)
  'se obtiene el ultimo valor de una de un tipo de prueba que se le hizo a un paciente (historial)
  
  Dim Qry2 As rdoQuery
  Dim Rs2 As rdoResultset
  Dim Sql As String, i%
  On Error GoTo AutoAnError
  
  With objActuacion
    Sql = "select resultadoalfanumerico from" _
        & " resultadoasistencia ra, pruebaasistencia pa where" _
        & " ra.historia = pa.historia and" _
        & " ra.caso = pa.caso and" _
        & " ra.secuencia = pa.secuencia and" _
        & " ra.nrepeticion = pa.nrepeticion and" _
        & " pa.historia = ? and pa.ctipoprueba = ?" _
        & " order by ra.fecha desc"
    Set Qry2 = objAutoAn.rdoConnect.CreateQuery("", Sql)
    Qry2(0) = .Muestra.lngHistoria                          'NumHistoria
    Qry2(1) = .strCodAct
    Set Rs2 = Qry2.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs2.EOF Then
      .strProperty1 = Rs2("resultadoalfanumerico")
    End If
    Rs2.Close: Qry2.Close
  End With
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "pModificaActuacion", "pModificaActuacion")
End Sub
