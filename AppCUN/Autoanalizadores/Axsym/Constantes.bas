Attribute VB_Name = "Constantes"
'Determina si el valor que devuelve la funcion LINEA si se inicializa o se decrementa respecto al que tenia
Enum Operation
  cteReset = 2
  cteResta = -1
End Enum

'Los distintos caracteres de comunicación entre el Host y el autoanalizador
Enum CaracteresComunicacion
  STX = 2
  ETX = 3
  EOT = 4
  ENQ = 5
  ACK = 6
  LF = 10
  CR = 13
  NAK = 21
End Enum

'Distintos tipos de frames
Enum TipoFrames
  Header = 1
  Patient = 2
  Order = 3
  Terminator = 4
End Enum
