VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmConsCamas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Camas"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11475
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   11475
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Consulta"
      ForeColor       =   &H00800000&
      Height          =   1275
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   11415
      Begin SSDataWidgets_B.SSDBCombo cboDpto 
         Height          =   375
         Left            =   720
         TabIndex        =   19
         Top             =   780
         Width           =   3495
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.TextBox txtTipoCama 
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Left            =   8100
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   300
         Width           =   3195
      End
      Begin VB.TextBox txtHistoria 
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   300
         Width           =   795
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   435
         Left            =   10080
         TabIndex        =   11
         Top             =   720
         Width           =   1215
      End
      Begin VB.Frame fraFiltros 
         Caption         =   "Mostrar"
         ForeColor       =   &H00800000&
         Height          =   555
         Left            =   4440
         TabIndex        =   8
         Top             =   660
         Width           =   4635
         Begin VB.OptionButton optT 
            Caption         =   "Todas"
            Height          =   195
            Left            =   3720
            TabIndex        =   18
            Top             =   240
            Width           =   855
         End
         Begin VB.OptionButton optL 
            Caption         =   "Libres"
            Height          =   195
            Left            =   2580
            TabIndex        =   17
            Top             =   240
            Width           =   1095
         End
         Begin VB.OptionButton optLyO 
            Caption         =   "Libres y Ocupadas"
            Height          =   195
            Left            =   720
            TabIndex        =   16
            Top             =   240
            Width           =   1695
         End
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H8000000A&
         Height          =   285
         Left            =   2460
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   300
         Width           =   3855
      End
      Begin VB.Label lblTipoCama 
         Caption         =   "Tip. Cama Solicitada:"
         Height          =   255
         Left            =   6540
         TabIndex        =   15
         Top             =   300
         Width           =   1575
      End
      Begin VB.Label lblHistoria 
         Caption         =   "Historia:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   300
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Planta:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   840
         Width           =   495
      End
      Begin VB.Label lblPaciente 
         AutoSize        =   -1  'True
         Caption         =   "Paciente:"
         Height          =   195
         Left            =   1740
         TabIndex        =   9
         Top             =   300
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "&Asignar"
      Height          =   375
      Left            =   1680
      TabIndex        =   5
      Top             =   6300
      Width           =   1335
   End
   Begin VB.CommandButton cmdReservar 
      Caption         =   "&Reservar"
      Height          =   375
      Left            =   60
      TabIndex        =   4
      Top             =   6300
      Width           =   1335
   End
   Begin VB.CommandButton cmdGestionCamas 
      Caption         =   "&Gestion de Camas"
      Height          =   375
      Left            =   5220
      TabIndex        =   3
      Top             =   6300
      Width           =   1575
   End
   Begin VB.CommandButton cmdListado 
      Caption         =   "&Listado de Ocupaci�n"
      Height          =   375
      Left            =   7020
      TabIndex        =   2
      Top             =   6300
      Width           =   1815
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   10260
      TabIndex        =   1
      Top             =   6300
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid grdCamas 
      Height          =   4785
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1320
      Width           =   11400
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   9
      Columns(0).Width=   2990
      Columns(0).Caption=   "Tipo Cama"
      Columns(0).Name =   "Tipo Cama"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "CamaReal"
      Columns(1).Name =   "CamaReal"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1455
      Columns(2).Caption=   "Cama"
      Columns(2).Name =   "Cama"
      Columns(2).Alignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3863
      Columns(3).Caption=   "Estado Cama"
      Columns(3).Name =   "Estado Cama"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "EstCama"
      Columns(4).Name =   "EstCama"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3545
      Columns(5).Caption=   "Estado Reserva Cama"
      Columns(5).Name =   "Estado Reserva Cama"
      Columns(5).Alignment=   2
      Columns(5).DataField=   "Column 6"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   6773
      Columns(6).Caption=   "Paciente"
      Columns(6).Name =   "Paciente"
      Columns(6).DataField=   "Column 7"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   1402
      Columns(7).Caption=   "Sexo"
      Columns(7).Name =   "Sexo"
      Columns(7).DataField=   "Column 8"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Departamento"
      Columns(8).Name =   "Departamento"
      Columns(8).DataField=   "Column 9"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      _ExtentX        =   20108
      _ExtentY        =   8440
      _StockProps     =   79
      BackColor       =   12632256
   End
End
Attribute VB_Name = "frmConsCamas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const Const_Dpto_Tipo_Planta = 3
Const const_Cama_Reservada = 6
Const const_Cama_Libre = 1
Const const_Cama_Ocupada = 2
Public lngPersona As Long
Public lngProceso As Long
Public lngAsistencia As Long
Dim lngDpto As Long
Public lngActPlan As Long
Public strCama As String
Private Sub pAsignarCama()
Dim SQL As String
Dim qry As rdoQuery
Dim strError As String
cmdReservar.Enabled = False
cmdAsignar.Enabled = False
On Error GoTo canceltrans
Err = 0
objApp.BeginTrans
 SQL = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null, "
    SQL = SQL & "AD14CODESTCAMA  = ?, "
    SQL = SQL & "AD01CODASISTENCI = ?, "
    SQL = SQL & "AD07CODPROCESO = ? "
    SQL = SQL & "WHERE AD15CODCAMA = GCFN07(?)  "
    
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = const_Cama_Reservada
        qry(1) = lngAsistencia
        qry(2) = lngProceso
        qry(3) = grdCamas.Columns("Cama").CellValue(grdCamas.SelBookmarks(0))
    qry.Execute
    If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible asignar esa cama"
        GoTo canceltrans
    End If
    qry.Close
        
    'EFS Actualizar Tabla Situaci�n Camas (AD1600)
    'PONEMOS FIN AL ESTADO ACTUAL (QUE DEBE SER LIBRE)
    SQL = "UPDATE AD1600 SET AD16FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & " WHERE AD15CODCAMA = GCFN07(?)"
    SQL = SQL & " AND AD16FECFIN IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = fAhora
        qry(1) = grdCamas.Columns("Cama").CellValue(grdCamas.SelBookmarks(0))
    qry.Execute
     If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible cambiar el estado de la cama"
        GoTo canceltrans
    End If
    qry.Close
    
    'INSERTAMOS EL NUEVO ESTADO QUE SERA EL DE RESERVADO.
    SQL = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA,"
    SQL = SQL & "AD01CODASISTENCI,AD07CODPROCESO) VALUES (GCFN07(?),"
    SQL = SQL & "TO_DATE(?,'DD/MM/YYYY HH24:MI:ss'),"
    SQL = SQL & "?,?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = grdCamas.Columns("Cama").CellValue(grdCamas.SelBookmarks(0))
        qry(1) = fAhora
        qry(2) = const_Cama_Reservada
        qry(3) = lngAsistencia
        qry(4) = lngProceso
    qry.Execute
     If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible crear un nuevo estado para la cama"
        GoTo canceltrans
    End If
    qry.Close
    'liberamos la cama que ten�a reservada
    If strCama <> "" Then
        SQL = "UPDATE CI0100 SET AD15CODCAMA=NULL WHERE PR04NUMACTPLAN=?"
        SQL = SQL & " AND CI01SITCITA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngActPlan
        qry(1) = constESTCITA_CITADA
        qry.Execute
         If qry.RowsAffected <> 1 Then
            strError = "No ha sido posible liberar la cama reservada en la cita"
            GoTo canceltrans
        End If
        qry.Close
        SQL = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null"
        SQL = SQL & " WHERE AD15CODCAMA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = strCama
        qry.Execute
         If qry.RowsAffected <> 1 Then
            strError = "No ha sido posible liberar la cama reservada"
            GoTo canceltrans
        End If
        qry.Close
    End If
    objApp.CommitTrans
    Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError <> "" Then
        MsgBox strError, vbCritical, Me.Caption
    Else
        MsgBox Error, vbCritical, Me.Caption
    End If
End Sub
Private Sub pCargarCombo()
    Dim SQL As String
    Dim rs  As rdoResultset
    Dim qry As rdoQuery
    SQL = "SELECT AD02CODDPTO, "
    SQL = SQL & "AD02DESDPTO "
    SQL = SQL & "FROM AD0200 "
    SQL = SQL & "WHERE AD32CODTIPODPTO =  ?" '&
    cboDpto.RemoveAll
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Const_Dpto_Tipo_Planta
    Set rs = qry.OpenResultset(rdOpenForwardOnly)
    cboDpto.AddItem "0" & Chr(9) & "TODOS"
    While rs.EOF = False
        cboDpto.AddItem rs("AD02CODDPTO").Value & Chr(9) & rs("AD02DESDPTO").Value
        cboDpto.Text = rs("AD02DESDPTO").Value
        cboDpto.MoveNext
        rs.MoveNext
    Wend
    rs.Close
    cboDpto.Text = ""
    Set rs = Nothing
End Sub
Private Sub pCargarGrid(rs As rdoResultset)
With grdCamas
Do While Not rs.EOF
    .AddItem (rs!AD13DESTIPOCAMA & Chr(9) & _
        rs!CAMAREAL & Chr(9) & _
        rs!CAMAVISOR & Chr(9) & _
        rs!AD14DESESTCAMA & Chr(9) & _
        rs!AD14CODESTCAMA & Chr(9) & _
        rs!AD37DESESTADO2CAMA & Chr(9) & _
        rs!PACIENTE & Chr(9) & _
        rs!CI30DESSEXO & Chr(9) & _
        rs!AD02CODDPTO)
        rs.MoveNext
    Loop
End With
End Sub
Private Sub pConsultar(intCol As Integer)
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strTipCama As String

Static sqlOrder As String
If cboDpto.Text = "" Then
    MsgBox "Debe seleccionar alg�n departamento", vbExclamation, Me.Caption
    Exit Sub
End If
If optL.Value = True Or cboDpto.Columns(0).Value = "0" Then
    strTipCama = "1"
Else
    If optLyO.Value = True Then
        strTipCama = "1,2"
    Else 'todas
        strTipCama = ""
    End If
End If
grdCamas.RemoveAll
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY CAMAVISOR" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdCamas.Columns(intCol).Caption
        Case "Tipo Cama": sqlOrder = " ORDER BY AD13DESTIPOCAMA"
        Case "Cama": sqlOrder = " ORDER BY CAMAVISOR"
        Case "Estado Cama": sqlOrder = " ORDER BY AD1400.AD14DESESTCAMA"
        Case "Estado Reserva Cama": sqlOrder = " ORDER BY AD3700.AD37DESESTADO2CAMA"
        Case "Paciente": sqlOrder = " ORDER BY PACIENTE"
        Case "Sexo": sqlOrder = " ORDER BY CI3000.CI30DESSEXO"
        Case Else: Exit Sub
        End Select
End If
Screen.MousePointer = vbHourglass
SQL = "SELECT DISTINCT "
SQL = SQL & "AD1300.AD13DESTIPOCAMA,"
SQL = SQL & "AD1500.AD15CODCAMA CAMAREAL,"
SQL = SQL & "GCFN06(AD1500.AD15CODCAMA) CAMAVISOR,"
SQL = SQL & "AD1400.AD14DESESTCAMA,"
SQL = SQL & "AD1500.AD14CODESTCAMA,"
SQL = SQL & "AD1500.AD14CODESTCAMA,"
SQL = SQL & "AD3700.AD37DESESTADO2CAMA,"
SQL = SQL & "DECODE(NVL(CI22PRIAPEL,'0'),'0','',CI22PRIAPEL||' '||NVL(CI22SEGAPEL,'')||', '||CI22NOMBRE) PACIENTE,"
SQL = SQL & "CI3000.CI30DESSEXO,"
SQL = SQL & "AD1500.AD02CODDPTO"
SQL = SQL & " FROM "
SQL = SQL & " AD1300 , AD1500, AD1400,AD3700,CI2200,CI3000,PR0400"
SQL = SQL & " WHERE "
SQL = SQL & " PR0400.AD01CODASISTENCI(+)=AD1500.AD01CODASISTENCI"
SQL = SQL & " AND PR0400.AD07CODPROCESO(+) =AD1500.AD07CODPROCESO"
SQL = SQL & " AND AD1500.AD14CODESTCAMA=AD1400.AD14CODESTCAMA"
SQL = SQL & " AND AD1500.AD37CODESTADO2CAMA=AD3700.AD37CODESTADO2CAMA(+)"
SQL = SQL & " AND AD1500.AD13CODTIPOCAMA=AD1300.AD13CODTIPOCAMA"
SQL = SQL & " AND PR0400.CI21CODPERSONA=CI2200.CI21CODPERSONA(+)"
SQL = SQL & " AND CI2200.CI30CODSEXO = CI3000.CI30CODSEXO(+)"
SQL = SQL & " AND AD1500.AD15FECFIN IS NULL"
If cboDpto.Columns(0).Value <> "0" Then
    SQL = SQL & " AND AD1500.AD02CODDPTO=?"
End If
If strTipCama <> "" Then
    SQL = SQL & " AND AD1500.AD14CODESTCAMA IN(" & strTipCama & ")"
End If
SQL = SQL & sqlOrder
If cboDpto.Columns(0).Value <> "0" Then
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngDpto
    Set rs = qry.OpenResultset
Else
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
End If
Call pCargarGrid(rs)
Screen.MousePointer = vbDefault

End Sub

Private Sub cboDpto_Change()
lngDpto = cboDpto.Columns(0).Value
If lngDpto = 0 Then
    optL.Value = True
    fraFiltros.Enabled = False
Else
    optLyO.Value = True
    fraFiltros.Enabled = False
End If
End Sub

Private Sub cboDpto_Click()
lngDpto = cboDpto.Columns(0).Value
If lngDpto = 0 Then
    optL.Value = True
    fraFiltros.Enabled = False
Else
    optLyO.Value = True
    fraFiltros.Enabled = True
End If

End Sub


Private Sub cboDpto_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    Call pConsultar(-1)
End If
End Sub



Private Sub cmdAsignar_Click()
Dim rs As rdoResultset
Dim SQL As String
Dim strCamareser As String
Dim camaasig As String
Dim qry As rdoQuery

Dim intRes As Integer
If grdCamas.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar alguna cama", vbExclamation, Me.Caption
    Exit Sub
End If
If grdCamas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n m�ltiple", vbExclamation, Me.Caption
    Exit Sub
End If 'EFS:compruebo que la cama no este todavia ocupada

 If grdCamas.Columns("EstCama").CellValue(grdCamas.SelBookmarks(0)) <> const_Cama_Libre Then
    MsgBox "Esta cama no se encuentra libre", vbExclamation, Me.Caption
    Exit Sub
End If
 If grdCamas.Columns("Estado Reserva Cama").CellValue(grdCamas.SelBookmarks(0)) <> "" And _
    grdCamas.Columns("CamaReal").CellValue(grdCamas.SelBookmarks(0)) <> strCama Then
    MsgBox "Esta cama ya est� reservada por otro paciente", vbExclamation, Me.Caption
    Exit Sub
End If

If strCama <> "" Then
    If strCama <> grdCamas.Columns("CamaReal").CellValue(grdCamas.SelBookmarks(0)) Then
        SQL = "SELECT GCFN06(?) CRESER FROM DUAL"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCama
        Set rs = qry.OpenResultset()
        strCamareser = rs!CRESER
        intRes = MsgBox("El paciente tiene reservada la cama " & strCamareser & ". Quiere seguir asignando la cama " & grdCamas.Columns("Cama").CellValue(grdCamas.SelBookmarks(0)) & "?", vbExclamation + vbYesNo, Me.Caption)
        If intRes = 6 Then
            Call pAsignarCama
        End If
    Else
        Call pAsignarCama
    End If
Else
    Call pAsignarCama

End If
Call objPipe.PipeSet("IN0102_CAMBIOS", True)

Unload Me
End Sub

Private Sub cmdConsultar_Click()
Call pConsultar(-1)

End Sub

Private Sub cmdGestionCamas_Click()
If grdCamas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n m�ltiple", vbExclamation, Me.Caption
    Exit Sub
End If

Call objPipe.PipeSet("IN0102_AD15CODCAMA", grdCamas.Columns("Cama").CellValue(grdCamas.SelBookmarks(0)))
Call objSecurity.LaunchProcess("IN0103")
Call pConsultar(-1)
End Sub

Private Sub cmdListado_Click()
    Dim sDpto           As String
    Dim strWhereTotal   As String
    If cboDpto.Text = "" Then
         strWhereTotal = "1=1"
    Else
         strWhereTotal = " AD1500.AD02CODDPTO = " & cboDpto.Columns(0).Value
    End If
    Call Imprimir_API(strWhereTotal, "AD1002.RPT")

End Sub

Private Sub cmdReservar_Click()
Dim strError As String
Dim SQL      As String
Dim qry    As rdoQuery
 If grdCamas.Columns("Estado Reserva Cama").CellValue(grdCamas.SelBookmarks(0)) <> "" Then
    MsgBox "Esta cama ya est� reservada", vbExclamation, Me.Caption
    Exit Sub
End If
 If strCama <> "" Then
    MsgBox "El paciente ya tiene una cama reservada", vbExclamation, Me.Caption
    Exit Sub
End If
If grdCamas.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar alguna cama", vbExclamation, Me.Caption
    Exit Sub
End If
If grdCamas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n m�ltiple", vbExclamation, Me.Caption
    Exit Sub
End If
If grdCamas.Columns("EstCama").CellValue(grdCamas.SelBookmarks(0)) <> const_Cama_Libre And grdCamas.Columns("EstCama").CellValue(grdCamas.SelBookmarks(0)) <> const_Cama_Ocupada Then
    MsgBox "S�lo pueden reservarse camas en estado Libre u Ocupada por paciente", vbExclamation, Me.Caption
    Exit Sub
End If
  'si llega hasta aqu� es porque se ha hecho la reserva,luego habilito y desabilito botones
 'Updatemos como reservada la cama en la tabla AD1500 la cama reservada
 cmdReservar.Enabled = False
 On Error GoTo canceltrans
 Err = 0
 objApp.BeginTrans
 SQL = "UPDATE AD1500 SET AD37CODESTADO2CAMA = ? "
 SQL = SQL & " WHERE AD15CODCAMA = ?  "
 Set qry = objApp.rdoConnect.CreateQuery("", SQL)
 qry(0) = 1
 qry(1) = grdCamas.Columns("CamaReal").CellValue(grdCamas.SelBookmarks(0))
 qry.Execute
  If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible dejar la cama como reservada"
        GoTo canceltrans
    End If
 qry.Close


 'Updatemos como reservada la cama en la tabla CI0100 la cama reservada
 SQL = "UPDATE CI0100 SET AD15CODCAMA = ? "
 SQL = SQL & "WHERE  PR04NUMACTPLAN = ?  "
 SQL = SQL & " AND CI01SITCITA=?"
 Set qry = objApp.rdoConnect.CreateQuery("", SQL)
 qry(0) = grdCamas.Columns("CamaReal").CellValue(grdCamas.SelBookmarks(0))    'Cama
 qry(1) = lngActPlan
 qry(2) = constESTCITA_CITADA
 qry.Execute
  If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible asignar la reserva de esta cama"
        GoTo canceltrans
    End If
 qry.Close
 Call objPipe.PipeSet("IN0102_CAMBIOS", True)
 Unload Me
 objApp.CommitTrans
 Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError <> "" Then
        MsgBox strError, vbCritical, Me.Caption
    Else
        MsgBox Error, vbCritical, Me.Caption
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
cboDpto.SetFocus
End Sub

Private Sub Form_Load()
Call pCargarCombo
End Sub

Private Sub grdCamas_HeadClick(ByVal ColIndex As Integer)
Call pConsultar(ColIndex)
End Sub


