VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Begin VB.Form frmIngresosPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingresos Pendientes"
   ClientHeight    =   8595
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdavisos 
      Height          =   375
      Left            =   8880
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   360
      Width           =   675
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Recitar"
      Height          =   555
      Left            =   9720
      TabIndex        =   25
      Top             =   8040
      Width           =   1035
   End
   Begin VB.Frame Frame1 
      Caption         =   "Consulta"
      ForeColor       =   &H00800000&
      Height          =   975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8775
      Begin SSDataWidgets_B.SSDBCombo cboDpto 
         Height          =   315
         Left            =   540
         TabIndex        =   21
         Top             =   360
         Width           =   2295
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DES"
         Columns(1).Name =   "DES"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4048
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "Consultar"
         Height          =   375
         Left            =   7680
         TabIndex        =   1
         Top             =   360
         Width           =   855
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
         Height          =   330
         Left            =   5880
         TabIndex        =   18
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Hasta"
         Top             =   360
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
         Height          =   330
         Left            =   3480
         TabIndex        =   19
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Desde"
         Top             =   360
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   7
         Left            =   2880
         TabIndex        =   4
         Top             =   360
         Width           =   510
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   5280
         TabIndex        =   3
         Top             =   360
         Width           =   465
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Dpto:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   390
      End
   End
   Begin VB.CommandButton cmdPendientes 
      Caption         =   "Imprimir pendientes"
      Height          =   555
      Left            =   7560
      TabIndex        =   24
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdAcunsa 
      Caption         =   "Imp. Pend. Acunsa"
      Height          =   555
      Left            =   8640
      TabIndex        =   23
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdAsigReser 
      Caption         =   "Asig. Reserva"
      Height          =   555
      Left            =   1080
      TabIndex        =   17
      Top             =   8040
      Width           =   1035
   End
   Begin VB.Frame Frame2 
      Caption         =   "Imprimir"
      ForeColor       =   &H00800000&
      Height          =   975
      Left            =   9600
      TabIndex        =   13
      Top             =   0
      Width           =   2235
      Begin VB.CheckBox chkIHojas 
         Caption         =   "Hojas"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   855
      End
      Begin VB.CheckBox chkIEti 
         Caption         =   "Etiquetas"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Imprimir"
         Height          =   375
         Left            =   1260
         TabIndex        =   14
         Top             =   420
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdCamas 
      Caption         =   "Camas"
      Height          =   555
      Left            =   0
      TabIndex        =   12
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   555
      Left            =   10920
      TabIndex        =   11
      Top             =   8040
      Width           =   915
   End
   Begin VB.CommandButton cmdHojafi 
      Caption         =   "Hoja Filiaci�n"
      Height          =   555
      Left            =   6480
      TabIndex        =   10
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdAnularReserva 
      Caption         =   "Anular Reserva"
      Height          =   555
      Left            =   2160
      TabIndex        =   9
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdDesasignarCama 
      Caption         =   "Desasignar "
      Height          =   555
      Left            =   3240
      TabIndex        =   8
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdAdmPac 
      Caption         =   "Admi. Paciente"
      Height          =   555
      Left            =   4320
      TabIndex        =   7
      Top             =   8040
      Width           =   1035
   End
   Begin VB.CommandButton cmdCamAsis 
      Caption         =   "Cam. Asistenci"
      Height          =   555
      Left            =   5400
      TabIndex        =   6
      Top             =   8040
      Width           =   1035
   End
   Begin SSDataWidgets_B.SSDBGrid grdIngresos 
      Height          =   7005
      Left            =   -60
      TabIndex        =   5
      Top             =   1020
      Width           =   11910
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   0
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   21008
      _ExtentY        =   12356
      _StockProps     =   79
      Caption         =   "Ingresos"
      ForeColor       =   8388608
   End
   Begin SSDataWidgets_B.SSDBDropDown ddTipoCama 
      Height          =   1455
      Left            =   7320
      TabIndex        =   20
      Top             =   6660
      Width           =   3495
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Des"
      Columns(1).Name =   "Des"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   2566
      _StockProps     =   77
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin vsViewLib.vsPrinter vsprinter 
      Height          =   855
      Left            =   540
      TabIndex        =   22
      Top             =   7860
      Visible         =   0   'False
      Width           =   255
      _Version        =   196608
      _ExtentX        =   450
      _ExtentY        =   1508
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
End
Attribute VB_Name = "frmIngresosPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intDpto As Integer
'Dim rs As rdoResultset
Dim strDpto As String
Private Sub pMensajes()
Dim sql As String
Dim rsMens As rdoResultset
Dim qry As rdoQuery
Dim imgX As ListImage

   sql = "SELECT count(*) from  CI0400 WHERE CI21CodPersona = ?" _
   & " and (ci04FecCadMens >= trunc(sysdate+1)" _
   & " Or ci04FecCadMens Is Null)"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
     If grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0)) <> 0 Then
        qry(0) = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
     Else
        qry(0) = 0
     End If
     Set rsMens = qry.OpenResultset()
     If rsMens(0) > 0 Then
       Set imgX = objApp.imlImageList.ListImages("i44")
       cmdavisos.Picture = imgX.Picture
     Else
       Set imgX = objApp.imlImageList.ListImages("i40")
       cmdavisos.Picture = imgX.Picture
     End If
End Sub

Private Sub pBotones()
If grdIngresos.SelBookmarks.Count = 0 Then
    cmdCamAsis.Enabled = False
    cmdAdmPac.Enabled = False
    cmdAsigReser.Enabled = False
    cmdAnularReserva.Enabled = False
    cmdCitar.Enabled = False
Else
    cmdCitar.Enabled = True
    cmdCamAsis.Enabled = True
    cmdAdmPac.Enabled = True
If grdIngresos.Columns("C.Reser.").CellValue(grdIngresos.SelBookmarks(grdIngresos.Row)) <> "" Then
    cmdAsigReser.Enabled = True
    cmdAnularReserva.Enabled = True
Else
    cmdAsigReser.Enabled = False
    cmdAnularReserva.Enabled = False
End If
If grdIngresos.Columns("C.Asig.").CellValue(grdIngresos.SelBookmarks(grdIngresos.Row)) <> "" Then
    cmdDesasignarCama.Enabled = True
Else
   cmdDesasignarCama.Enabled = False
End If
End If
End Sub
Private Sub pCambiar(col As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strError As String
On Error GoTo canceltrans
Err = 0
objApp.BeginTrans
If col = 5 Then
    sql = "UPDATE CI0100 SET AD13CODTIPOCAMA=? WHERE CI31NUMSOLICIT=? AND CI01NUMCITA=? "
    sql = sql & " AND CI01SITCITA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ddTipoCama.Columns(0).Value 'grdIngresos.Columns(col).CellValue(grdIngresos.RowBookmark(grdIngresos.Row))
    qry(1) = grdIngresos.Columns("NumSolicit").CellValue(grdIngresos.RowBookmark(grdIngresos.Row))
    qry(2) = grdIngresos.Columns("NumCita").CellValue(grdIngresos.RowBookmark(grdIngresos.Row))
    qry(3) = constESTCITA_CITADA
    qry.Execute
    If qry.RowsAffected <> 1 Then
        strError = "No se ha podido actualizar el tipo de cama solicitada"
        GoTo canceltrans
    End If
    qry.Close
End If
If col = 12 Then
    sql = "UPDATE PR0800 SET PR08DESOBSERV=? WHERE PR03NUMACTPEDI=? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = grdIngresos.Columns(col).Value '(grdIngresos.RowBookmark(grdIngresos.Row))
    qry(1) = grdIngresos.Columns("NumActPedi").CellValue(grdIngresos.RowBookmark(grdIngresos.Row))
    qry.Execute
     If qry.RowsAffected <> 1 Then
        strError = "No se ha podido actualizar las observaciones"
        GoTo canceltrans
    End If
    qry.Close
End If
objApp.CommitTrans
Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError = "" Then
        MsgBox Error, vbCritical, Me.Caption
    Else
        MsgBox strError, vbCritical, Me.Caption
    End If
    
End Sub
Private Sub pCargarddTipocama()
Dim sql As String
Dim rs As rdoResultset
sql = "SELECT AD13CODTIPOCAMA,AD13DESTIPOCAMA FROM AD1300 ORDER BY AD13DESTIPOCAMA"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
    ddTipoCama.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
Loop
grdIngresos.Columns("Tipo Cama").DropDownHwnd = ddTipoCama.hWnd
End Sub
Private Sub pAsignarReserva(BlnAbort As Boolean)
Dim sql      As String
Dim qry    As rdoQuery
Dim rs As rdoResultset
Dim strError As String
    'EFS:compruebo que la cama no este todavia ocupada
    sql = "SELECT AD14CODESTCAMA FROM AD1500 WHERE AD15CODCAMA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0))
    Set rs = qry.OpenResultset(sql)
    If rs.EOF = False Then
        If rs(0).Value <> const_Cama_Libre Then
            MsgBox "Esta cama no se encuentra libre. No se puede Asignar", vbExclamation, Me.Caption
            BlnAbort = True
            Exit Sub
        End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    On Error GoTo canceltrans
    Err = 0
    objApp.BeginTrans
    
    'Updatemaos del registro en la tabla AD1500 la cama asignada
    sql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null, "
    sql = sql & "AD14CODESTCAMA  = ?, "
    sql = sql & "AD01CODASISTENCI = ?, "
    sql = sql & "AD07CODPROCESO = ? "
    sql = sql & "WHERE AD15CODCAMA = ?  "
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = const_Cama_Reservada
        qry(1) = grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0))
        qry(2) = grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0))
        qry(3) = grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0))
    qry.Execute
    If qry.RowsAffected <> 1 Then
        strError = "No se ha podido asignar la cama"
        GoTo canceltrans
    End If
    qry.Close


        
    'EFS Actualizar Tabla Situaci�n Camas (AD1600)
    'PONEMOS FIN AL ESTADO ACTUAL (QUE DEBE SER LIBRE)
    sql = "UPDATE AD1600 SET AD16FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    sql = sql & " WHERE AD15CODCAMA = ?"
    sql = sql & " AND AD16FECFIN IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = fAhora 'strFecha_Sistema & " " & strHora_Sistema
        qry(1) = grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0))
    qry.Execute
    If qry.RowsAffected <> 1 Then
        strError = "No se ha podido finalizar el estado actual de la cama"
        GoTo canceltrans
    End If
    qry.Close
    
    'INSERTAMOS EL NUEVO ESTADO QUE SERA EL DE RESERVADO.
    sql = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA,"
    sql = sql & "AD01CODASISTENCI,AD07CODPROCESO) VALUES (?,"
    sql = sql & "TO_DATE(?,'DD/MM/YYYY HH24:MI:ss'),"
    sql = sql & "?,?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0))
        qry(1) = fAhora 'strFecha_Sistema & " " & strHora_Sistema()
        qry(2) = const_Cama_Reservada
        qry(3) = grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0))
        qry(4) = grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0))
    qry.Execute
    If qry.RowsAffected <> 1 Then
        strError = "No se ha podido crear un nuevo estado para la cama"
        GoTo canceltrans
    End If
    qry.Close
objApp.CommitTrans
Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError = "" Then
        MsgBox Error, vbCritical, Me.Caption
    Else
        MsgBox strError, vbCritical, Me.Caption
    End If
    

End Sub
Private Sub pAnularReserva()
Dim strCama As String
Dim qry As rdoQuery
Dim sql As String
Dim strError As String
strCama = grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0))
If strCama = "" Then
     MsgBox "El paciente no tiene ninguna cama reservada", vbExclamation, Me.Caption
     Exit Sub
 End If
On Error GoTo canceltrans
Err = 0
objApp.BeginTrans
 'Updatemos como Null la cama en la tabla AD1500 la cama reservada
 sql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null "
 sql = sql & "WHERE AD15CODCAMA = ? "
 Set qry = objApp.rdoConnect.CreateQuery("", sql)
 qry(0) = strCama
 qry.Execute
 If qry.RowsAffected <> 1 Then
        strError = "No se ha podido liberar la cama ya reservada"
        objApp.RollbackTrans
        GoTo canceltrans
    End If
 qry.Close

 'Updatemos como Null la cama en la tabla CI0100 la cama reservada
 sql = "UPDATE CI0100 SET AD15CODCAMA = Null "
 sql = sql & "WHERE  PR04NUMACTPLAN = ?  "
 sql = sql & " AND CI01SITCITA=?"
 
 Set qry = objApp.rdoConnect.CreateQuery("", sql)
 qry(0) = grdIngresos.Columns("NumActPlan").CellValue(grdIngresos.SelBookmarks(0))   'Num_Actuaci�n
 qry(1) = constESTCITA_CITADA
 qry.Execute
 If qry.RowsAffected <> 1 Then
        strError = "No se ha podido anular la reserva de cama de la cita"
        objApp.RollbackTrans
        GoTo canceltrans
    End If
 qry.Close
 objApp.CommitTrans
 Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError <> "" Then
         MsgBox strError, vbCritical, Me.Caption
    Else
        MsgBox Error, vbCritical, Me.Caption
    End If
End Sub
Private Sub pSeleccionar(intCol As Integer)

Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Static sqlOrder As String
grdIngresos.RemoveAll
grdIngresos.Row = 0
If CDate(dcboHasta.Text) < CDate(dcboDesde.Text) Then
        MsgBox "La Fecha Hasta es  menor que Fecha Desde", vbExclamation, Me.Caption
        Exit Sub
End If
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR0400.AD02CODDPTO,PACIENTE" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdIngresos.Columns(intCol).Caption
        Case "Fecha Cita": sqlOrder = " ORDER BY CI01FECCONCERT"
        Case "Hora": sqlOrder = " ORDER BY CI01FECCONCERT"
        Case "Doctor": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
        Case "T.Econ.": sqlOrder = " ORDER BY AD1100.CI32CODTIPECON"
        Case "Entidad": sqlOrder = " ORDER BY AD1100.CI13CODENTIDAD"
        Case "Tipo Cama": sqlOrder = " ORDER BY AD1300.AD13DESTIPOCAMA"
        Case "Historia": sqlOrder = " ORDER BY CI22NUMHISTORIA"
        Case "Paciente": sqlOrder = " ORDER BY PACIENTE"
        Case "C.Reser.": sqlOrder = " ORDER BY CAMARESERV"
        Case "C.Asig": sqlOrder = " ORDER BY CAMAASIG"
        Case "Observaciones": sqlOrder = " ORDER BY OBSERVACIONES"
        Case "Edad": sqlOrder = " ORDER BY EDAD"
        Case "Sexo": sqlOrder = " ORDER BY CI3000.CI30DESSEXO"
        Case "Actuaci�n": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
        Case "Dpto": sqlOrder = " ORDER BY PR0400.AD02CODDPTO"
        Case Else: Exit Sub
        End Select
End If
Screen.MousePointer = vbHourglass
sql = "SELECT /*+ ORDERED INDEX(CI0100 CI0105) */"
sql = sql & " TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YY HH24:MI') FECHA,"
sql = sql & "PR0400.AD02CODDPTO,"
sql = sql & "AG1100.AG11DESRECURSO,"
sql = sql & "CI2200.CI21CODPERSONA,"
sql = sql & "AD1100.CI32CODTIPECON,"
sql = sql & "AD1100.CI13CODENTIDAD,"
sql = sql & "AD1300.AD13CODTIPOCAMA,"
sql = sql & "CI2200.CI22NUMHISTORIA,"
sql = sql & "CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
sql = sql & "CI0100.AD15CODCAMA CAMARESERV,"
sql = sql & "AD1500.AD15CODCAMA CAMAASIG,"
sql = sql & "GCFN06(CI0100.AD15CODCAMA) CAMAREVISOR,"
sql = sql & "GCFN06(AD1500.AD15CODCAMA) CAMAASVISOR,"
sql = sql & "DECODE(TO_CHAR(CI2200.CI22FECFALLE,'dd/mm/yyyy'),"
sql = sql & "null,TRUNC((SYSDATE -CI2200.CI22FECNACIM)/365),"
sql = sql & "TRUNC((CI22FECFALLE -CI2200.CI22FECNACIM)/365)) EDAD,"
sql = sql & "CI3000.CI30DESSEXO,"
sql = sql & "PR0800.PR08DESOBSERV||'. '||PR0800.PR08DESINDICAC||'. '||PR0800.PR08DESMOTPET OBSERVACIONES,"
sql = sql & "PR0400.PR04NUMACTPLAN,"
sql = sql & "PR0100.PR01DESCORTA,"
sql = sql & "PR0400.PR03NUMACTPEDI,"
sql = sql & "PR0400.CI21CODPERSONA,"
sql = sql & "PR0400.AD07CODPROCESO,"
sql = sql & "PR0400.AD01CODASISTENCI,"
sql = sql & "CI0100.CI31NUMSOLICIT,"
sql = sql & "CI0100.CI01NUMCITA"
sql = sql & " FROM CI0100,PR0400,PR0100,AG1100,AD1100,"
sql = sql & " AD1300 , AD1500, CI2200, CI3000, PR0800"
sql = sql & " WHERE CI0100.CI01SITCITA=?" '''1'"
sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN"
sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
sql = sql & " AND PR0100.PR12CODACTIVIDAD=?" '209"
sql = sql & " AND PR0400.PR37CODESTADO = ?" '2"
sql = sql & " AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO"
sql = sql & " AND PR0400.AD07CODPROCESO = AD1100.AD07CODPROCESO (+)"
sql = sql & " AND PR0400.AD01CODASISTENCI = AD1100.AD01CODASISTENCI (+)"
sql = sql & " AND PR0400.AD01CODASISTENCI =AD1500.AD01CODASISTENCI(+)"
sql = sql & " AND PR0400.AD07CODPROCESO =AD1500.AD07CODPROCESO(+)"
sql = sql & " AND CI0100.AD13CODTIPOCAMA=AD1300.AD13CODTIPOCAMA(+)"
sql = sql & " AND PR0400.CI21CODPERSONA=CI2200.CI21CODPERSONA"
sql = sql & " AND CI2200.CI30CODSEXO = CI3000.CI30CODSEXO"
sql = sql & " AND PR0400.PR03NUMACTPEDI=PR0800.PR03NUMACTPEDI"
sql = sql & " AND CI0100.CI01FECCONCERT BETWEEN TO_DATE(? ,'DD/MM/YYYY')"
sql = sql & " AND TO_DATE(? ,'DD/MM/YYYY')"
sql = sql & " AND AD1100.AD11FECFIN IS NULL"
If intDpto <> 0 Then
    sql = sql & " AND PR0400.AD02CODDPTO=?"
End If
sql = sql & sqlOrder
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = constESTCITA_CITADA
qry(1) = constACTIVIDAD_HOSPITALIZACION
qry(2) = constESTACT_CITADA
qry(3) = dcboDesde.Text
qry(4) = CStr(DateAdd("d", 1, CDate(dcboHasta.Text)))
If intDpto <> 0 Then
    qry(5) = intDpto
End If
Set rs = qry.OpenResultset(rdOpenForwardOnly)
Call pCargarGrid(rs)
Screen.MousePointer = vbDefault
End Sub
Private Sub pCargarGrid(rs As rdoResultset)
With grdIngresos
    Do While Not rs.EOF
    .AddItem (rs!FECHA & Chr(9) & _
            rs!AD02CODDPTO & Chr(9) & _
            rs!AG11DESRECURSO & Chr(9) & _
            rs!CI32CODTIPECON & Chr(9) & _
            rs!CI13CODENTIDAD & Chr(9) & _
            rs!AD13CODTIPOCAMA & Chr(9) & _
            rs!CI22NUMHISTORIA & Chr(9) & _
            rs!PACIENTE & Chr(9) & _
            rs!CAMARESERV & Chr(9) & _
            rs!camaasig & Chr(9) & _
            rs!CAMAREVISOR & Chr(9) & _
            rs!CAMAASVISOR & Chr(9) & _
            rs!OBSERVACIONES & Chr(9) & _
            rs!EDAD & Chr(9) & _
            rs!CI30DESSEXO & Chr(9) & _
            rs!PR04NUMACTPLAN & Chr(9) & _
            rs!PR03NUMACTPEDI & Chr(9) & _
            rs!CI21CODPERSONA & Chr(9) & _
            rs!AD07CODPROCESO & Chr(9) & _
            rs!AD01CODASISTENCI & Chr(9) & _
            rs!CI31NUMSOLICIT & Chr(9) & _
            rs!CI01NUMCITA & Chr(9) & _
            rs!CI21CODPERSONA)
        rs.MoveNext
        grdIngresos.Columns("Tipo Cama").StyleSet = "Edicion"
        grdIngresos.Columns("Observaciones").StyleSet = "Edicion"

    Loop
End With
End Sub

Private Sub pCargarCombo()
Dim sql As String
Dim rs  As rdoResultset
Dim qry As rdoQuery
    strDpto = ""
    sql = "SELECT AD02CODDPTO, "
    sql = sql & "AD02DESDPTO "
    sql = sql & "FROM AD0200 "
    sql = sql & "WHERE AD32CODTIPODPTO =?" '& Const_Dpto_Ingresos
    sql = sql & " AND AD02FECFIN IS NULL"
    sql = sql & " ORDER BY AD02DESDPTO"
    cboDpto.RemoveAll
    cboDpto.AddItem (0 & Chr(9) & "TODOS")
    cboDpto.Text = "TODOS"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Const_Dpto_Ingresos
    Set rs = qry.OpenResultset()
    While rs.EOF = False
        cboDpto.AddItem rs("AD02CODDPTO").Value & Chr(9) & rs("AD02DESDPTO").Value
        strDpto = strDpto & rs("AD02CODDPTO").Value & ","
        rs.MoveNext
    Wend
    strDpto = Left(strDpto, Len(strDpto) - 1)
    rs.Close
    Set rs = Nothing
End Sub



Private Sub grdActuaciones_InitColumnProps()
'Fecha y hora
'C�digo de Departamento
'M�dico
'Tipo Economico
'Entidad
'Tipo cama
'Paciente
'cama RESERVADA
'Cama Asignada
'cama RESERVADA Visor
'Cama Asignada Visor
'Edad
'Sexo
'Observaciones
'Actuaci�n
'C�digo Actuaci�n Pedida
'C�digo Paciente
'Estado
'C�digo de Estado
'C�d.Estado Muestra
'Proceso
'asistencia

'Num.Peticion
'Num.Solicitud
'Num.Cita
End Sub
Private Sub pFormatearGrid()
With grdIngresos
    .Columns(0).Caption = "Fecha Cita"
    .Columns(0).Width = 1300
    .Columns(0).Locked = True
    .Columns(1).Caption = "Dpto"
    .Columns(1).Width = 500
    .Columns(1).Visible = True
    .Columns(1).Locked = True
    .Columns(2).Caption = "Doctor"
    .Columns(2).Width = 1300
    .Columns(2).Locked = True
    .Columns(3).Caption = "T.Econ."
    .Columns(3).Width = 400
    .Columns(3).Locked = True
    .Columns(4).Caption = "Entidad"
    .Columns(4).Width = 400
    .Columns(4).Locked = True
    .Columns(5).Caption = "Tipo Cama"
    .Columns(5).Width = 1800
    .Columns(5).style = ssStyleComboBox
    .Columns(6).Caption = "Historia"
    .Columns(6).Width = 700
    .Columns(6).Locked = True
    .Columns(7).Caption = "Paciente"
    .Columns(7).Width = 3000
    .Columns(7).Locked = True
    .Columns(8).Caption = "CReser"
    .Columns(8).Width = 0
    .Columns(8).Visible = False
    .Columns(8).Locked = True
    .Columns(9).Caption = "CAsig"
    .Columns(9).Width = 0
    .Columns(9).Visible = False
    .Columns(10).Caption = "C.Reser."
    .Columns(10).Width = 550
    .Columns(10).Locked = True
    .Columns(11).Caption = "C.Asig."
    .Columns(11).Width = 550
    .Columns(11).Locked = True
    .Columns(12).Caption = "Observaciones"
    .Columns(12).Width = 3000
    .Columns(13).Caption = "Edad"
    .Columns(13).Width = 500
    .Columns(13).Alignment = ssCaptionAlignmentCenter
    .Columns(13).Locked = True
    .Columns(14).Caption = "Sexo"
    .Columns(14).Width = 800
    .Columns(14).Locked = True
    .Columns(15).Caption = "NumActPlan"
    .Columns(15).Width = 0
    .Columns(15).Visible = False
    .Columns(16).Caption = "NumActPedi"
    .Columns(16).Width = 0
    .Columns(16).Visible = False
    .Columns(17).Caption = "CodPersona"
    .Columns(17).Width = 0
    .Columns(17).Visible = False
    .Columns(18).Caption = "CodProceso"
    .Columns(18).Width = 0
    .Columns(18).Visible = False
    .Columns(19).Caption = "CodAsistencia"
    .Columns(19).Width = 0
    .Columns(19).Visible = False
    .Columns(20).Caption = "NumSolicit"
    .Columns(20).Width = 0
    .Columns(20).Visible = False
    .Columns(21).Caption = "NumCita"
    .Columns(21).Width = 0
    .Columns(21).Visible = False
    .Columns(22).Caption = "CodPersona"
    .Columns(22).Visible = False
    .StyleSets.Add ("Edicion")
    .StyleSets("Edicion").BackColor = objApp.objUserColor.lngNormal
    .StyleSets.Add ("Lectura")
    .StyleSets("Lectura").BackColor = objApp.objUserColor.lngReadOnly
End With

End Sub
Private Sub cboDpto_Change()
If cboDpto.Columns(0).Value = 0 Then
    intDpto = 0
Else
    intDpto = cboDpto.Columns(0).Value
End If

End Sub

Private Sub cboDpto_Click()
If cboDpto.Columns(0).Value = 0 Then
  intDpto = 0
Else
    intDpto = cboDpto.Columns(0).Value
End If
End Sub

Private Sub cmdAcunsa_Click()
Dim sql As String
If cboDpto.Columns(0).Text = "0" Then
    sql = " (FECHA < TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboHasta.Text))) & "','DD/MM/YYYY') OR FECHA IS NULL) AND FECHA > =  TO_DATE('" & CStr(dcboDesde.Text) & "','DD/MM/YYYY') AND (AD14CODESTCAMA <> 2 OR AD14CODESTCAMA IS NULL)"
Else
    sql = " (FECHA < TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboHasta.Text))) & "','DD/MM/YYYY') OR FECHA IS NULL) AND FECHA > =  TO_DATE('" & CStr(dcboDesde.Text) & "','DD/MM/YYYY') AND (AD14CODESTCAMA <> 2 OR AD14CODESTCAMA IS NULL) AND PR0451J.AD02CODDPTO=" & cboDpto.Columns(0).Value
End If
Call Imprimir_API(sql, "AD1001.rpt", , , , True)
End Sub

Private Sub cmdAdmPac_Click()
Dim vntData(1)
Dim i As Integer
    If grdIngresos.SelBookmarks.Count = 0 Then
        MsgBox "Debe seleccionar alg�n ingreso", vbExclamation, Me.Caption
        Exit Sub
    End If
     If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
    End If
vntData(1) = grdIngresos.Columns("Codpersona").CellValue(grdIngresos.SelBookmarks(0))
Call objSecurity.LaunchProcess("AD2105", vntData)
'Call objSecurity.LaunchProcess("AD0201", vntData(1))
Call pSeleccionar(-1)
 For i = 1 To grdIngresos.Rows
    If grdIngresos.Columns("CodPersona").Value = vntData(1) Then
        grdIngresos.SelBookmarks.Add grdIngresos.RowBookmark(grdIngresos.Row)
        Exit For
    End If
    grdIngresos.MoveNext
Next
Call pBotones
End Sub

Private Sub cmdAnularReserva_Click()
    Dim sql      As String
    Dim qry    As rdoQuery
    Dim strCama As String
    Dim Persona As String
    Dim i As Integer
        'Updateo del registro en la tabla AD1500 la cama reservada
        'modificado por EFS
    If grdIngresos.SelBookmarks.Count = 0 Then
        MsgBox "Debe seleccionar alg�n ingreso", vbExclamation, Me.Caption
        Exit Sub
    End If
     If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
    End If
    Persona = grdIngresos.Columns("Codpersona").CellValue(grdIngresos.SelBookmarks(0))
    Call pAnularReserva
    Call pSeleccionar(-1)
     For i = 1 To grdIngresos.Rows
            If grdIngresos.Columns("CodPersona").Value = Persona Then
                grdIngresos.SelBookmarks.Add grdIngresos.RowBookmark(grdIngresos.Row)
                Exit For
            End If
            grdIngresos.MoveNext
        Next
    Call pBotones
End Sub

Private Sub cmdAsigReser_Click()
Dim dFecha As String
Dim sCodPaciente As String
Dim sMensaje As String
Dim iResp As Integer
Dim strSQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim Persona As String
Dim i As Integer
Dim BlnAbort As Boolean
    If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
    End If
    If grdIngresos.SelBookmarks.Count = 0 Then
            MsgBox "Debe seleccionar alg�n ingreso", vbExclamation, Me.Caption
            Exit Sub
    End If
    sCodPaciente = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
    'Comprobar si esta el paciente ya ingresado
    If bDBPacienteIngresado(sCodPaciente, sMensaje) = True Then
        dFecha = Format$(fAhora, "DD/MM/YYYY")
        If Format$(grdIngresos.Columns("Fecha Cita").Text, "DD/MM/YYYY") = dFecha Then
            MsgBox sMensaje, vbExclamation, Me.Caption
            Exit Sub
        Else
            sMensaje = sMensaje & Chr(13) & "�Desea Continuar? "
            iResp = MsgBox(sMensaje, vbQuestion + vbYesNo, Me.Caption)
            If iResp = 7 Then
                'si es seguir nada sino salimos de la funci�n
                Exit Sub
            End If
        End If
    End If
    
    ' Para asignar Cama, debe estar dada de alta la asistencia
    If grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0)) = "" Or grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0)) = "" Then
        Call MsgBox("Debe asociar antes el Proceso-Asistencia", vbExclamation, Me.Caption)
        Exit Sub
    End If
    strSQL = "SELECT AD1500.AD01CODASISTENCI, AD15CODCAMA "
    strSQL = strSQL & " FROM AD1500,AD0100 "
    strSQL = strSQL & " WHERE AD1500.AD01CODASISTENCI=?"
    strSQL = strSQL & " AND AD1500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI "
    strSQL = strSQL & " AND AD0100.CI21CODPERSONA=?"
    strSQL = strSQL & " AND AD0100.AD01FECFIN IS NULL"
    strSQL = strSQL & " AND AD07CODPROCESO=?"
    Set qry = objApp.rdoConnect.CreateQuery("", strSQL)
    qry(0) = grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0))
    qry(1) = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
    qry(2) = grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0))
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
      MsgBox "El paciente se encuentra en la cama:" & rs!AD15CODCAMA & " y tiene la asistencia:" & rs!AD01CODASISTENCI, vbInformation, Me.Caption
      Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    Persona = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
    If grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0)) <> "" Then 'El paciente tiene cama reservada
        iResp = MsgBox("�Se desea asignar la cama reservada?", vbQuestion + vbYesNo, Me.Caption)
        If iResp = 6 Then
            'Yes, pasamos la cama provisional ---> cama
            'Estado Real a Reservada
            'Estado2 a null
            Call pAsignarReserva(BlnAbort)
            If Not BlnAbort Then
                Call pAnularReserva
            End If
        Else
            'Call pAnularReserva
        End If
    Else
        MsgBox "El paciente no tiene cama reservada", vbExclamation, Me.Caption
    End If
Call pSeleccionar(-1)
 For i = 1 To grdIngresos.Rows
            If grdIngresos.Columns("CodPersona").Value = Persona Then
                grdIngresos.SelBookmarks.Add grdIngresos.RowBookmark(grdIngresos.Row)
                Exit For
            End If
            grdIngresos.MoveNext
        Next
    Call pBotones
    
End Sub

Private Sub cmdavisos_Click()
Dim vntData(1)
vntData(1) = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
Call objSecurity.LaunchProcess("PR0580", vntData)
Call pMensajes
End Sub

Private Sub cmdCamas_Click()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim Persona As Long
Dim i As Integer
Dim blnSeleccion As Boolean 'para saber si se ha llamado a otra pantalla con selecci�n o no
If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
End If
    
 Load frmConsCamas
 
    'Pasa el c�digo de persona para mostrarla despu�s
If grdIngresos.SelBookmarks.Count = 1 Then
    Persona = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0)) 'han seleccionado uno
     blnSeleccion = True

    If grdIngresos.Columns("CAsig").CellValue(grdIngresos.SelBookmarks(0)) <> "" Then
        MsgBox "El paciente ya tiene una cama asignada.", vbExclamation, Me.Caption
        Exit Sub
    End If
    frmConsCamas.lngPersona = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
    frmConsCamas.lngActPlan = grdIngresos.Columns("NumActPlan").CellValue(grdIngresos.SelBookmarks(0))

    frmConsCamas.txtPaciente.Text = grdIngresos.Columns("Paciente").CellValue(grdIngresos.SelBookmarks(0))
    frmConsCamas.txtHistoria.Text = grdIngresos.Columns("Historia").CellValue(grdIngresos.SelBookmarks(0))
    sql = "SELECT AD13DESTIPOCAMA FROM AD1300 WHERE AD13CODTIPOCAMA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = grdIngresos.Columns("Tipo Cama").CellValue(grdIngresos.SelBookmarks(0))
    Set rs = qry.OpenResultset
    If Not rs.EOF Then
        frmConsCamas.txtTipoCama = rs(0) ' grdIngresos.Columns("Tipo Cama").CellValue(grdIngresos.SelBookmarks(0))
    End If
    If grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0)) <> "" And grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0)) <> "" Then
    'llamada con proceso/asistencia
        If grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0)) <> "" Then
        'llamada con proceso/asistencia y con cama
            frmConsCamas.cmdReservar.Enabled = False
            frmConsCamas.cmdAsignar.Enabled = True
            frmConsCamas.lblPaciente.Enabled = True
            frmConsCamas.lblHistoria.Enabled = True
            frmConsCamas.lblTipoCama.Enabled = True
            frmConsCamas.optLyO.Value = True
            frmConsCamas.strCama = grdIngresos.Columns("CReser").CellValue(grdIngresos.SelBookmarks(0))
        Else
        'con proceso/asistencia sin cama
            frmConsCamas.cmdReservar.Enabled = True
            frmConsCamas.cmdAsignar.Enabled = True
            frmConsCamas.lblPaciente.Enabled = True
            frmConsCamas.lblHistoria.Enabled = True
            frmConsCamas.lblTipoCama.Enabled = True
            frmConsCamas.optLyO.Value = True
        End If
        frmConsCamas.lngProceso = grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0))
        frmConsCamas.lngAsistencia = grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0))
    Else
    'llamada sin proceso asistencia
        frmConsCamas.cmdReservar.Enabled = True
        frmConsCamas.cmdAsignar.Enabled = False
        frmConsCamas.lblPaciente.Enabled = True
        frmConsCamas.lblHistoria.Enabled = True
        frmConsCamas.lblTipoCama.Enabled = True
        frmConsCamas.optLyO.Value = True
    End If

Else
'llamada sin paciente
    frmConsCamas.cmdReservar.Enabled = False
    frmConsCamas.cmdAsignar.Enabled = False
    frmConsCamas.fraFiltros.Enabled = True
    frmConsCamas.lblPaciente.Enabled = False
    frmConsCamas.lblHistoria.Enabled = False
    frmConsCamas.lblTipoCama.Enabled = False
    frmConsCamas.optT.Value = True
End If
    Call frmConsCamas.Show(vbModal)
'    Call objSecurity.LaunchProcess("IN0102") '(, vntData)
    Set frmConsCamas = Nothing
    If objPipe.PipeExist("IN0102_CAMBIOS") Then
    If blnSeleccion Then
        Call pSeleccionar(-1)
        For i = 1 To grdIngresos.Rows
            If grdIngresos.Columns("CodPersona").Value = Persona Then
                grdIngresos.SelBookmarks.Add grdIngresos.RowBookmark(grdIngresos.Row)
                Exit For
            End If
            grdIngresos.MoveNext
        Next
    Call pBotones
    End If
    If grdIngresos.Columns("C.Reser.").CellValue(grdIngresos.SelBookmarks(grdIngresos.Row)) <> "" Then
        cmdAsigReser.Enabled = True
        cmdAnularReserva.Enabled = True
    Else
        cmdAsigReser.Enabled = False
        cmdAnularReserva.Enabled = False
    End If
    If grdIngresos.Columns("C.Asig.").CellValue(grdIngresos.SelBookmarks(grdIngresos.Row)) <> "" Then
        cmdDesasignarCama.Enabled = True
    Else
       cmdDesasignarCama.Enabled = False
    End If

    blnSeleccion = False
    Call objPipe.PipeRemove("IN0102_CAMBIOS")
    End If
End Sub

Private Sub cmdCamAsis_Click()
Dim i As Integer
    Dim vntData(1) As Variant
    If grdIngresos.SelBookmarks.Count = 0 Then
        MsgBox "Debe seleccionar alg�n ingreso", vbExclamation, Me.Caption
        Exit Sub
    End If
     If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
    End If
    'Pasa el c�digo de persona para mostrarla despu�s
    vntData(1) = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
    Call objSecurity.LaunchProcess("AD3000", vntData)
    Call pSeleccionar(-1)
    For i = 1 To grdIngresos.Rows
            If grdIngresos.Columns("CodPersona").Value = vntData(1) Then
                grdIngresos.SelBookmarks.Add grdIngresos.RowBookmark(grdIngresos.Row)
                Exit For
            End If
            grdIngresos.MoveNext
        Next
    Call pBotones
End Sub

Private Sub cmdCitar_Click()
 If grdIngresos.SelBookmarks.Count = 0 Then
        MsgBox "Debe seleccionar alg�n ingreso", vbExclamation, Me.Caption
        Exit Sub
    End If
     If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
    End If
Call pCitarOverbooking(grdIngresos.Columns("NumActPlan").CellValue(grdIngresos.SelBookmarks(0)))
Call pSeleccionar(-1)
End Sub
Public Sub pCitarOverbooking(strNAPlan$)
    Dim sql As String, rs As rdoResultset
    Dim cllNAP As New Collection
    Dim msgDptoRea$, msgNoCitables$
    
    If Right$(strNAPlan, 1) = "," Then strNAPlan = Left$(strNAPlan, Len(strNAPlan) - 1)
    
    'Sacamos los datos necesarios para ver si las actuacione son citables por ese usuario
    sql = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR12CODACTIVIDAD,"
    sql = sql & " PR0400.AD02CODDPTO, PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA"
    sql = sql & " FROM PR0400, PR0100 WHERE"
    sql = sql & " PR04NUMACTPLAN IN (" & strNAPlan & ")"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " ORDER BY PR0100.PR01DESCORTA"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    While Not rs.EOF
        Select Case rs!PR12CODACTIVIDAD
        Case constACTIV_PRUEBA, constACTIV_CONSULTA, constACTIV_INFORME, constACTIV_CONTCONSULTA
            If fblnCitarModiAnular(rs!AD02CODDPTO, rs!PR12CODACTIVIDAD, rs!PR01CODACTUACION, True) Then
                cllNAP.Add CStr(rs!PR04NUMACTPLAN)
            Else 'citables s�lo desde el Dpto realizador
                msgDptoRea = msgDptoRea & "--> " & rs!PR01DESCORTA & Chr$(13)
            End If
        Case constACTIV_HOSPITALIZACION
                If fblnCitarModiAnular(rs!AD02CODDPTO, rs!PR12CODACTIVIDAD, rs!PR01CODACTUACION, True) Then
                    cllNAP.Add CStr(rs!PR04NUMACTPLAN)
                Else 'citables s�lo desde el Dpto realizador
                    msgNoCitables = msgNoCitables & "--> " & rs!PR01DESCORTA & Chr$(13)
                End If
        Case Else
            msgNoCitables = msgNoCitables & "--> " & rs!PR01DESCORTA & Chr$(13)
        End Select
        rs.MoveNext
    Wend

    If msgDptoRea <> "" Then
        msgDptoRea = "Las siguientes actuaciones son s�lo citables por el Dpto. realizador:" & Chr$(13) & msgDptoRea & Chr$(13)
    End If
    If msgNoCitables <> "" Then
        msgNoCitables = "Las siguientes actuaciones no pueden ser citadas:" & Chr$(13) & msgNoCitables
    End If
    If msgDptoRea <> "" Or msgNoCitables <> "" Then
        MsgBox msgDptoRea & msgNoCitables, vbInformation, "Citaci�n"
    End If
    
    If cllNAP.Count > 0 Then
        Dim Item
        For Each Item In cllNAP
            Call objSecurity.LaunchProcess("CI1150", Item)
        Next
    End If
End Sub



Private Sub cmdConsultar_Click()
Call pSeleccionar(-1)
Call pBotones
End Sub

Private Sub cmdDesasignarCama_Click()
Dim sql      As String
Dim qry  As rdoQuery
Dim strCama As String
Dim strError As String
Dim Persona As String
Dim i As Integer
If grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0)) <> "" And grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0)) <> "" Then
        strCama = grdIngresos.Columns("CAsig").CellValue(grdIngresos.SelBookmarks(0))
        'Updateo del registro en la tabla AD1500 la cama reservada
        'modificado por EFS
    If grdIngresos.SelBookmarks.Count = 0 Then
        MsgBox "Debe seleccionar alg�n ingreso", vbExclamation, Me.Caption
        Exit Sub
    End If
     If grdIngresos.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
        Exit Sub
    End If
    If strCama = "" Then
        MsgBox "El paciente no tiene ninguna cama asignada", vbExclamation, Me.Caption
        Exit Sub
    End If
    Persona = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(0))
    On Error GoTo canceltrans
    Err = 0
    objApp.BeginTrans
        sql = "UPDATE AD1500 SET AD14CODESTCAMA = ?, "
        sql = sql & "AD37CODESTADO2CAMA = Null, "
        sql = sql & "AD01CODASISTENCI = Null,"
        sql = sql & "AD07CODPROCESO = Null "
        sql = sql & "WHERE AD01CODASISTENCI = ? "
        sql = sql & "AND AD07CODPROCESO = ? "
        sql = sql & "AND AD15CODCAMA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = const_Cama_Libre
        qry(1) = grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(0))
        qry(2) = grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(0))
        qry(3) = grdIngresos.Columns("CAsig").CellValue(grdIngresos.SelBookmarks(0))
        qry.Execute
        If qry.RowsAffected <> 1 Then
            strError = "No se ha podido dejar la cama libre"
            GoTo canceltrans
        End If
        qry.Close
        
                
        'EFS:Borrado del registro de la AD1600 y updateo el anterior para que se quede
        'como estaba
        sql = "DELETE AD1600"
        sql = sql & " WHERE AD15CODCAMA = ?"
        sql = sql & " AND AD16FECCAMBIO = "
        sql = sql & "(SELECT MAX(AD16FECCAMBIO) FROM AD1600 WHERE AD15CODCAMA = ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strCama
        qry(1) = strCama
        qry.Execute
         If qry.RowsAffected <> 1 Then
            strError = "El estado la cama no ha podido ser borrado"
            GoTo canceltrans
        End If
        qry.Close
        sql = "UPDATE AD1600 "
        sql = sql & "SET AD16FECFIN = Null"
        sql = sql & " WHERE AD15CODCAMA = ? "
        sql = sql & " AND AD16FECCAMBIO = "
        sql = sql & "(SELECT MAX(AD16FECCAMBIO) FROM AD1600 WHERE AD15CODCAMA = ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strCama
        qry(1) = strCama
        qry.Execute
         If qry.RowsAffected <> 1 Then
            strError = "No se ha podido dejar la cama en el estado anterior"
            GoTo canceltrans
        End If
        qry.Close
    objApp.CommitTrans
    Else
        MsgBox "Este paciente no tiene cama asociada para el P/A", vbExclamation, Me.Caption
    End If
    Call pSeleccionar(-1)
    For i = 1 To grdIngresos.Rows
            If grdIngresos.Columns("CodPersona").Value = Persona Then
                grdIngresos.SelBookmarks.Add grdIngresos.RowBookmark(grdIngresos.Row)
                Exit For
            End If
            grdIngresos.MoveNext
        Next
    Call pBotones
    Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError <> "" Then
        MsgBox Error, vbCritical, Me.Caption
    Else
        MsgBox strError, vbCritical, Me.Caption
    End If
End Sub

Private Sub cmdHojafi_Click()
Dim vntData
Screen.MousePointer = vbHourglass
If grdIngresos.SelBookmarks.Count <> 0 Then
    vntData = grdIngresos.Columns("Codpersona").CellValue(grdIngresos.SelBookmarks(0))
    Call objPipe.PipeSet("CODPERS", vntData)
    Call objSecurity.LaunchProcess("CI4017", vntData)
Else
    Call objSecurity.LaunchProcess("CI4017")
End If

End Sub

Private Sub cmdImprimir_Click()
Dim strWhere As String
Dim i As Integer
Dim fila As Integer
Dim col As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim X As Integer
Dim strnombre As String
Dim strFecha As String
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim qy As rdoQuery
Me.MousePointer = vbHourglass
If grdIngresos.SelBookmarks.Count > 0 Then
    If chkIHojas.Value = 0 And chkIEti.Value = 0 Then
        MsgBox "Debe indicar qu� listado desea imprimir", vbExclamation, Me.Caption
        Me.MousePointer = vbDefault
        Exit Sub
    End If
    If chkIHojas.Value = 1 Then
        For i = 1 To grdIngresos.SelBookmarks.Count
            If grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(i)) <> "" And grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(i)) <> "" Then
                strWhere = " Ad0823j.AD01CODASISTENCI=" & grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(i))
                strWhere = strWhere & " AND Ad0823j.AD07CODPROCESO=" & grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(i))
                Call Imprimir_API(strWhere, "filiacion2.rpt", , , , True)
                strWhere = " Ad0823j.AD01CODASISTENCI=" & grdIngresos.Columns("CodAsistencia").CellValue(grdIngresos.SelBookmarks(i))
                strWhere = strWhere & " AND Ad0823j.AD07CODPROCESO=" & grdIngresos.Columns("CodProceso").CellValue(grdIngresos.SelBookmarks(i))
                Call Imprimir_API(strWhere, "hojaverde2.rpt", , , , True)
                
            Else
                MsgBox "El Paciente no tiene asociado el proceso/asistencia", vbExclamation, Me.Caption
            End If
        Next
    End If
    If chkIEti.Value = 1 Then
            For i = 1 To grdIngresos.SelBookmarks.Count
                intPosX = 75
                intPosY = 335
                intIncX = 3900
                intIncY = 1433
                vsprinter.StartDoc
                vsprinter.CurrentX = intPosX
                vsprinter.CurrentY = intPosY
                'fecha de nacimiento
                sql = "SELECT  CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE,CI22FECNACIM "
                sql = sql & " FROM CI2200 WHERE CI21CODPERSONA = ? "
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = grdIngresos.Columns("CodPersona").CellValue(grdIngresos.SelBookmarks(i))
                Set rs = qry.OpenResultset
                If Not IsNull(rs!CI22FECNACIM) Then
                    strFecha = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
                Else
                    strFecha = ""
                End If
                If Not IsNull(rs!PACIENTE) Then
                    strnombre = rs!PACIENTE
                End If
                rs.Close
                qry.Close
                For fila = 1 To 11
                    For col = 1 To 3
                      X = vsprinter.CurrentX
                      vsprinter.FontSize = 12
                      vsprinter.Text = "Hist: "
                      vsprinter.FontBold = True
                      vsprinter.Text = grdIngresos.Columns("Historia").CellValue(grdIngresos.SelBookmarks(i))
                      vsprinter.FontBold = False
                      vsprinter.Text = Chr$(13)
                      vsprinter.CurrentX = X
                      vsprinter.FontSize = 8
                      vsprinter.Text = strnombre
                      vsprinter.Text = Chr$(13)
                      vsprinter.CurrentX = X
                      vsprinter.Text = strFecha
                      X = X + intIncX
                      vsprinter.CurrentX = X
                      vsprinter.CurrentY = intPosY
                    Next col
                    vsprinter.CurrentX = intPosX
                    intPosY = intPosY + intIncY
                    vsprinter.CurrentY = intPosY
                Next fila
                vsprinter.EndDoc
                vsprinter.PrintDoc
            Next
    End If
Else
    MsgBox " Debe seleccionar alg�n paciente", vbExclamation, Me.Caption
End If
    Me.MousePointer = vbDefault

End Sub

Private Sub cmdPendientes_Click()
Dim sql As String
If cboDpto.Columns(0).Text = "0" Then
    sql = " (FECHA < TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboHasta.Text))) & "','DD/MM/YYYY') OR FECHA IS NULL) AND FECHA > =  TO_DATE('" & CStr(dcboDesde.Text) & "','DD/MM/YYYY') AND (AD14CODESTCAMA <> 2 OR AD14CODESTCAMA IS NULL)"
Else
    sql = " (FECHA < TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboHasta.Text))) & "','DD/MM/YYYY') OR FECHA IS NULL) AND FECHA > =  TO_DATE('" & CStr(dcboDesde.Text) & "','DD/MM/YYYY') AND (AD14CODESTCAMA <> 2 OR AD14CODESTCAMA IS NULL) AND PR0451J.AD02CODDPTO=" & cboDpto.Columns(0).Value
End If
Call Imprimir_API(sql, "AD1000.rpt", , , , True)
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub dcboDesde_Change()
 If CDate(dcboDesde.Text) > CDate(dcboHasta.Text) Then
        dcboHasta.Text = dcboDesde.Text
    End If
End Sub

Private Sub dcboDesde_CloseUp()
 If CDate(dcboDesde.Text) > CDate(dcboHasta.Text) Then
        dcboHasta.Text = dcboDesde.Text
 End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Text) < CDate(dcboDesde.Text) Then
        dcboDesde.Text = dcboHasta.Text
    End If
End Sub

Private Sub dcboHasta_CloseUp()
If CDate(dcboHasta.Text) < CDate(dcboDesde.Text) Then
        dcboDesde.Text = dcboHasta.Text
End If
End Sub

Private Sub Form_Activate()
cboDpto.SetFocus
End Sub

Private Sub Form_Load()
Dim Res As Integer
Call pCargarCombo
Call pFormatearGrid
Call pCargarddTipocama
cmdAdmPac.Enabled = False
cmdDesasignarCama.Enabled = False
cmdAnularReserva.Enabled = False
cmdAsigReser.Enabled = False
cmdCamAsis.Enabled = False
cmdCitar.Enabled = False
Res = PEOpenEngine
End Sub

Private Sub grdIngresos_BeforeRowColChange(cancel As Integer)

If grdIngresos.Columns("Tipo Cama").ColChanged Or grdIngresos.Columns("Observaciones").ColChanged Then
    Call pCambiar(CInt(grdIngresos.col))
    
End If
End Sub

Private Sub grdIngresos_Click()
Call pBotones
Call pMensajes
'If grdIngresos.SelBookmarks.Count = 0 Then
'    cmdCamAsis.Enabled = False
'    cmdAdmPac.Enabled = False
'    cmdAsigReser.Enabled = False
'    cmdAnularReserva.Enabled = False
'Else
'    cmdCamAsis.Enabled = True
'    cmdAdmPac.Enabled = True
'End If
End Sub

Private Sub grdIngresos_HeadClick(ByVal ColIndex As Integer)
Call pSeleccionar(ColIndex)
Call pBotones
End Sub

Private Sub grdIngresos_LostFocus()
If grdIngresos.Columns("Tipo Cama").ColChanged Or grdIngresos.Columns("Observaciones").ColChanged Then
    grdIngresos.Update
    Call pCambiar(CInt(grdIngresos.col))
    
End If

End Sub

Private Sub grdIngresos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Call pBotones
'If grdIngresos.Columns("C.Reser.").CellValue(grdIngresos.SelBookmarks(grdIngresos.Row)) <> "" Then
'    cmdAsigReser.Enabled = True
'    cmdAnularReserva.Enabled = True
'Else
'    cmdAsigReser.Enabled = False
'    cmdAnularReserva.Enabled = False
'End If
'If grdIngresos.Columns("C.Asig.").CellValue(grdIngresos.SelBookmarks(grdIngresos.Row)) <> "" Then
'    cmdDesasignarCama.Enabled = True
'Else
'   cmdDesasignarCama.Enabled = False
'End If
End Sub

