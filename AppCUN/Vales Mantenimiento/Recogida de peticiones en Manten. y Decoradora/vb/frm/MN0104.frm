VERSION 5.00
Begin VB.Form frmImprimirDecoradora 
   Caption         =   "Partes de Petici�n a la Decoradora"
   ClientHeight    =   4875
   ClientLeft      =   2910
   ClientTop       =   2625
   ClientWidth     =   6300
   LinkTopic       =   "Form1"
   ScaleHeight     =   4875
   ScaleWidth      =   6300
   Begin VB.Frame Frame1 
      Height          =   2895
      Left            =   960
      TabIndex        =   1
      Top             =   1560
      Width           =   4335
      Begin VB.OptionButton Option1 
         Caption         =   "Diciembre"
         Height          =   495
         Index           =   12
         Left            =   3000
         TabIndex        =   14
         Top             =   1440
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Noviembre"
         Height          =   495
         Index           =   11
         Left            =   1800
         TabIndex        =   13
         Top             =   1440
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Octubre"
         Height          =   495
         Index           =   10
         Left            =   360
         TabIndex        =   12
         Top             =   1440
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Septiembre"
         Height          =   495
         Index           =   9
         Left            =   3000
         TabIndex        =   11
         Top             =   1080
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Agosto"
         Height          =   495
         Index           =   8
         Left            =   1800
         TabIndex        =   10
         Top             =   1080
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Julio"
         Height          =   495
         Index           =   7
         Left            =   360
         TabIndex        =   9
         Top             =   1080
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Junio"
         Height          =   495
         Index           =   6
         Left            =   3000
         TabIndex        =   8
         Top             =   720
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Mayo"
         Height          =   495
         Index           =   5
         Left            =   1800
         TabIndex        =   7
         Top             =   720
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Abril"
         Height          =   495
         Index           =   4
         Left            =   360
         TabIndex        =   6
         Top             =   720
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Marzo"
         Height          =   495
         Index           =   3
         Left            =   3000
         TabIndex        =   5
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Febrero"
         Height          =   495
         Index           =   2
         Left            =   1800
         TabIndex        =   4
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Enero"
         Height          =   495
         Index           =   1
         Left            =   360
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "I&mprimir Resumen del Mes Seleccionado"
         Height          =   495
         Left            =   480
         TabIndex        =   2
         Top             =   2160
         Width           =   3375
      End
   End
   Begin VB.CommandButton cmdParte 
      Caption         =   "&Imprimir Partes Pendientes"
      Height          =   495
      Left            =   1560
      TabIndex        =   0
      Top             =   480
      Width           =   3015
   End
End
Attribute VB_Name = "frmImprimirDecoradora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdMes_Click()
Dim i%, hoy$, a�o$, fechaValida$, SQL$, report$, mes$
Dim Matriz(2, 1) As String
Me.MousePointer = vbHourglass
For i = 1 To 12
  If Option1(i).Value = True Then
    mes = CStr(i)
    Exit For
  End If
Next
If mes = "" Then
  MsgBox "Elija un mes", vbInformation, ""
  Me.MousePointer = vbDefault
  Exit Sub
End If
hoy = strFecha_Sistema
If Len(mes) = 1 Then mes = "0" + mes
If Format(hoy, "MM") < mes Then
    a�o = Format(DateAdd("yyyy", -1, hoy), "YYYY")
   fechaValida = mes + "/" + a�o
Else
   a�o = Format(hoy, "YYYY")
   fechaValida = mes + "/" + a�o
End If
SQL = "MN6100.MN62CODESPECIALI= 11 AND MN6100.MN61INDTRANSMITIDA=-1"
  report = "ReportesMes.rpt"
  Matriz(1, 0) = "ia�o"
  Matriz(1, 1) = a�o
  Matriz(2, 0) = "imes"
  Matriz(2, 1) = mes
  
 ' formula(0)=
  Call Imprimir_API(SQL, report, Matriz(), , , True)

Me.MousePointer = vbDefault
End Sub

Private Sub cmdParte_Click()
Dim report$, CodPeticion&, SQL$, intTipoImpresora%, str$
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim qyup As rdoQuery

Me.MousePointer = vbHourglass
  SQL = "SELECT * FROM MN6100, MN6300 WHERE " & _
  "MN6100.MN63CODTIPOPETI = MN6300.MN63CODTIPOPETI AND " & _
  "MN6100.MN61INDTRANSMITIDA= ? AND MN62CODESPECIALI=?"
Set qy = objApp.rdoConnect.CreateQuery("", SQL)
  qy(0) = False
  qy(1) = 11
Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Do While Not rd.EOF
  SQL = "MN6100.MN61CODPETICION= " & rd!MN61CODPETICION
  report = "ImprimirDecoradora.rpt"
  Call Imprimir_API(SQL, report, , , , True)
  str = "UPDATE MN6100 SET MN61INDTRANSMITIDA=?, " & _
  "MN61FECTRANSMITIDA=SYSDATE WHERE MN61CODPETICION=?"
  Set qyup = objApp.rdoConnect.CreateQuery("", str)
    qyup(0) = True
    qyup(1) = rd!MN61CODPETICION
  qyup.Execute
  qyup.Close
  rd.MoveNext
Loop
Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
'Call CargarComboMes
End Sub
