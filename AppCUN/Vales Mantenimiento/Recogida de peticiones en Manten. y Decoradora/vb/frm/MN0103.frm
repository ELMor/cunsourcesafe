VERSION 5.00
Begin VB.Form frmPasoPeticion 
   Caption         =   "Recoger las peticiones"
   ClientHeight    =   3435
   ClientLeft      =   3210
   ClientTop       =   3000
   ClientWidth     =   5400
   LinkTopic       =   "Form1"
   ScaleHeight     =   3435
   ScaleWidth      =   5400
   Begin VB.TextBox txtContador 
      Height          =   375
      Left            =   2760
      TabIndex        =   1
      Top             =   2400
      Width           =   735
   End
   Begin VB.Data Data 
      Caption         =   "DBase dbf"
      Connect         =   "dBASE IV;"
      DatabaseName    =   "C:\Proyecto\Vales"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   420
      Left            =   2640
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   0  'Table
      RecordSource    =   "Almtra01"
      Top             =   120
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Recoger las peticiones"
      Default         =   -1  'True
      Height          =   615
      Left            =   1560
      TabIndex        =   0
      Top             =   1320
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero de Peticiones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   720
      TabIndex        =   2
      Top             =   2520
      Width           =   2055
   End
End
Attribute VB_Name = "frmPasoPeticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
  Dim str$
  Dim rd As rdoResultset
  Dim qyup As rdoQuery
  Dim qy As rdoQuery
  Dim RDu As rdoResultset
  Dim QYu As rdoQuery
  Dim ncanal$, tippet$, Usuario$
  Dim strIni As String 'Almacena el Path del archivo INI
  Dim lngPosArchivo As Long 'Posici�n actual en el archivo INI
  Dim cont%
  Me.MousePointer = vbHourglass
  strIni = "C:\Archivos de Programa\Cun\Departamento.ini"
  Open strIni For Binary Access Read Shared As #1 Len = FileLen(strIni)
  lngPosArchivo = 1
  Seek #1, lngPosArchivo
  Line Input #1, strIni
  Close #1
  
  On Error GoTo label
  str = "SELECT * FROM MN6100 where " & _
  "MN61INDTRANSMITIDA= ? AND "
    If strIni = "11" Then
      str = str + "MN62CODESPECIALI=?"
    Else
      str = str + "MN62CODESPECIALI<>?"
    End If
  Set qy = objApp.rdoConnect.CreateQuery("", str)
    qy(0) = False
    qy(1) = 11
  Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  cont = 0
  txtContador = 0
    While Not rd.EOF
        Data.Recordset.AddNew
        Data.Recordset.Fields("PET_NUMERO").Value = rd!MN61CODPETICION
        If Not IsNull(rd!AD02CODDPTO) Then
          Data.Recordset.Fields("PET_DEPART").Value = rd!AD02CODDPTO
        End If
        Data.Recordset.Fields("PET_FECHA").Value = rd!MN61FECADD
        If Not IsNull(rd!MN61PLANTA) Then
          Data.Recordset.Fields("PET_PLANTA").Value = rd!MN61PLANTA
        End If
        If Not IsNull(rd!MN61LUGAR) Then
          Data.Recordset.Fields("PET_LUGARO").Value = rd!MN61LUGAR
        End If
        If Not IsNull(rd!MN61DESPERFECTO) Then
          Data.Recordset.Fields("PET_DESPE1").Value = Left(rd!MN61DESPERFECTO, 50)
        End If
        If Not IsNull(rd!MN61DESPERFECTO) Then
        Data.Recordset.Fields("PET_DESPE2").Value = Mid(rd!MN61DESPERFECTO, 51, 50)
        End If
        If Not IsNull(rd!MN61DESPERFECTO) Then
           Data.Recordset.Fields("PET_DESPE3").Value = Mid(rd!MN61DESPERFECTO, 101, 50)
         End If
        If Not IsNull(rd!MN61PETICIONARIO) Then
            Data.Recordset.Fields("PET_PETICI").Value = rd!MN61PETICIONARIO
        End If
        If Not IsNull(rd!MN63CODTIPOPETI) Then
          If rd!MN63CODTIPOPETI = 1 Then tippet = "RE" Else tippet = "IN"
          Data.Recordset.Fields("PET_TIPO").Value = tippet
        End If
        If Not IsNull(rd!MN62CODESPECIALI) Then
          Data.Recordset.Fields("PET_ESPECI").Value = rd!MN62CODESPECIALI
        End If
        Data.Recordset.Update
        str = "UPDATE MN6100 SET MN61INDTRANSMITIDA=?, " & _
        "MN61FECTRANSMITIDA=SYSDATE WHERE MN61CODPETICION=?"
        Set qyup = objApp.rdoConnect.CreateQuery("", str)
          qyup(0) = True
          qyup(1) = rd!MN61CODPETICION
        qyup.Execute
        cont = cont + 1
        txtContador = cont
        rd.MoveNext
        qyup.Close
    Wend
    Data.Recordset.Close
    MsgBox "Transacci�n terminada", vbInformation, "Aviso"
  rd.Close
  qy.Close
  Me.MousePointer = vbDefault
  Exit Sub
label:
  If Err = 3420 Then Resume Next
End Sub

