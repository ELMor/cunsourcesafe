VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmPeticion 
   Caption         =   "Realizar una nueva petici�n a Mantenimiento"
   ClientHeight    =   5715
   ClientLeft      =   1305
   ClientTop       =   1875
   ClientWidth     =   9420
   LinkTopic       =   "Form1"
   ScaleHeight     =   5715
   ScaleWidth      =   9420
   Begin VB.Frame frmPeticion 
      Caption         =   "Petici�n"
      Enabled         =   0   'False
      Height          =   4095
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   9015
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "&Enviar a Mantenimiento"
         Height          =   495
         Left            =   6720
         TabIndex        =   19
         ToolTipText     =   "Guardar"
         Top             =   3240
         Width           =   2055
      End
      Begin VB.TextBox txtTransmitida 
         BackColor       =   &H00C0C0C0&
         Height          =   375
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   3240
         Width           =   1095
      End
      Begin VB.TextBox txtUsuario 
         BackColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   240
         TabIndex        =   15
         Top             =   1200
         Width           =   3735
      End
      Begin VB.TextBox txtDesperfecto 
         Height          =   975
         Left            =   3840
         MaxLength       =   150
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   14
         Top             =   1920
         Width           =   3495
      End
      Begin VB.TextBox txtLugar 
         Height          =   975
         Left            =   240
         MaxLength       =   30
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   1920
         Width           =   3495
      End
      Begin VB.TextBox txtPlanta 
         Height          =   375
         Left            =   7200
         MaxLength       =   15
         TabIndex        =   8
         Top             =   1200
         Width           =   1575
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   480
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483639
         Enabled         =   0   'False
         DefaultDate     =   ""
         ShowCentury     =   -1  'True
         AllowEdit       =   0   'False
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo ssdbDepartamento 
         Height          =   375
         Left            =   4200
         TabIndex        =   21
         Top             =   1200
         Width           =   2535
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   3969
         Columns(0).Caption=   "Departamento"
         Columns(0).Name =   "Departamento"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1111
         Columns(1).Caption=   "Cod. Departamento"
         Columns(1).Name =   "Cod. Departamento"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4471
         _ExtentY        =   661
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   12632256
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo ssdbEspecialidad 
         Height          =   375
         Left            =   240
         TabIndex        =   22
         Top             =   3240
         Width           =   2535
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "Especialidad"
         Columns(0).Name =   "Especialidad"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1296
         Columns(1).Caption=   "Cod. Especialidad"
         Columns(1).Name =   "Cod. Especialidad"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4471
         _ExtentY        =   661
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   12632256
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label Label10 
         Caption         =   "Transmitida"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3360
         TabIndex        =   18
         Top             =   3000
         Width           =   1335
      End
      Begin VB.Label Label8 
         Caption         =   "Nombre del Usuario"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Desperfecto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3840
         TabIndex        =   13
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Especialidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   3000
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "Lugar de la obra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1680
         Width           =   2175
      End
      Begin VB.Label Label4 
         Caption         =   "Planta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7200
         TabIndex        =   9
         Top             =   960
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4200
         TabIndex        =   7
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de Petici�n"
      Height          =   975
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   6855
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   240
         ScaleHeight     =   615
         ScaleWidth      =   735
         TabIndex        =   20
         Top             =   240
         Width           =   735
      End
      Begin VB.OptionButton optInstalacion 
         Caption         =   "Instalaci�n, Reforma"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4080
         TabIndex        =   3
         Top             =   360
         Width           =   2655
      End
      Begin VB.OptionButton optReparacion 
         Caption         =   "Reparaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1920
         TabIndex        =   2
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdSalir 
      Height          =   735
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Salir"
      Top             =   360
      Width           =   735
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   5
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MN0101.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MN0101.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MN0101.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MN0101.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MN0101.frx":0C68
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPeticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdGuardar_Click()
  Dim str$
  Dim qy As rdoQuery
  Dim res%
  If ssdbEspecialidad.Columns(1).Value = "" Then
    MsgBox "El Campo Especialidad es obligatorio", vbInformation, ""
    Exit Sub
  End If
  If ssdbDepartamento.Columns(1).Value = "" Then
    MsgBox "El Campo Departamento es obligatorio", vbInformation, ""
    Exit Sub
  End If
  If txtDesperfecto = "" Then
    MsgBox "El Campo Desperfecto es obligatorio", vbInformation, ""
    Exit Sub
  End If

  Me.MousePointer = vbHourglass
  str = "INSERT INTO MN6100 (MN61CODPETICION, " & _
  "AD02CODDPTO,MN61PLANTA, MN61LUGAR, MN62CODESPECIALI, " & _
  "MN61DESPERFECTO, MN61PETICIONARIO, MN61INDTRANSMITIDA, MN61FECTRANSMITIDA, " & _
  " MN63CODTIPOPETI) VALUES (?,?,?,?,?,?,?,?,?, " & _
  "?)"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
    qy(0) = CLng(fNextClaveSeq("MN61CODPETICION"))
    qy(1) = ssdbDepartamento.Columns(1).Value
    qy(2) = txtPlanta
    qy(3) = txtLugar
    qy(4) = ssdbEspecialidad.Columns(1).Value
    qy(5) = txtDesperfecto
    qy(6) = txtUsuario
    qy(7) = False
    qy(8) = Null
    If optReparacion = True Then
      qy(9) = 1
    Else
      qy(9) = 2
    End If
  qy.Execute
  Me.MousePointer = vbDefault
  res = MsgBox("La Petici�n ha sido enviada." & Chr(13) & Chr(10) & _
  "�Desea realizar una nueva petici�n?", vbInformation + vbYesNo + vbDefaultButton1, "Peticione a Mantenimiento")
  If res = vbYes Then
    optReparacion = False
    optInstalacion = False
    ssdbEspecialidad.Columns(0).Value = Null
    ssdbEspecialidad = ""
    txtPlanta = ""
    txtLugar = ""
    txtDesperfecto = ""
    txtTransmitida = "NO"
    SSDateCombo1.Date = strFecha_Sistema
  Else
    Unload Me
  End If
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Click()
If gblCodPeticion = 0 Then
  If optReparacion = False And optInstalacion = False Then
    MsgBox "Seleccione el tipo de petici�n antes de completar el resto de campos.", vbExclamation, ""
  End If
End If
End Sub

Private Sub Form_Load()
  Dim str$, DesDpto$
  Dim rd As rdoResultset
  Dim qy As rdoQuery
  Dim dpt%
  Set Picture1.Picture = ImageList1.ListImages.Item(cENGRANAJES).Picture
  Set cmdSalir.Picture = ImageList1.ListImages.Item(cSALIR).Picture
  If gblCodPeticion <> 0 Then
    str = "SELECT MN6100.MN61PLANTA, MN6100.MN61LUGAR, MN6100.MN63CODTIPOPETI, " & _
        "MN6100.MN61DESPERFECTO, MN6100.MN61INDTRANSMITIDA,MN6100.MN61FECADD, " & _
        "MN6200.MN62DESESPECIALI, MN6100.MN61PETICIONARIO, " & _
        "MN6100.AD02CODDPTO,AD0200.AD02DESDPTO FROM MN6100, MN6200,MN6300, AD0200 " & _
        "WHERE MN6100.MN61CODPETICION= ? " & _
        "AND MN6100.MN62CODESPECIALI=MN6200.MN62CODESPECIALI " & _
        "AND MN6100.MN63CODTIPOPETI=MN6300.MN63CODTIPOPETI " & _
        "AND AD0200.AD02CODDPTO=MN6100.AD02CODDPTO"
    Set qy = objApp.rdoConnect.CreateQuery("", str)
      qy(0) = gblCodPeticion
    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ssdbDepartamento.Columns(1).Value = rd!AD02CODDPTO
    ssdbDepartamento = rd!AD02DESDPTO
    ssdbEspecialidad = rd!MN62DESESPECIALI
    If Not IsNull(rd!MN61LUGAR) Then txtLugar = rd!MN61LUGAR
    If Not IsNull(rd!MN61DESPERFECTO) Then txtDesperfecto = rd!MN61DESPERFECTO
    If Not IsNull(rd!MN61PLANTA) Then txtPlanta = rd!MN61PLANTA
    If Not IsNull(rd!MN61PETICIONARIO) Then txtUsuario = rd!MN61PETICIONARIO
    If rd!MN61INDTRANSMITIDA Then
      txtTransmitida = "Si"
    Else: txtTransmitida = "No"
    End If
    If rd!MN63CODTIPOPETI = 1 Then
      optReparacion = True
    Else: optInstalacion = True
    End If

    SSDateCombo1.Date = rd!MN61FECADD
    cmdGuardar.Visible = False
    Frame1.Enabled = False
    frmPeticion.Enabled = False
  Else
    Call CargarComboEspecialidad
    Call CargarComboDepartamentos
    
'    txtCodUsuario = objSecurity.strUser
'    str = "SELECT SG02NOM,SG02APE1 FROM SG0200 WHERE SG02COD=?"
'    Set qy = objApp.rdoConnect.CreateQuery("", str)
'    qy(0) = objSecurity.strUser
'    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'    If rd.RowCount > 0 Then
'      txtUsuario = rd!SG02NOM + " " + rd!SG02APE1
      dpt = ObtenerDepartamento(DesDpto)
        If dpt <> 0 Then
          ssdbDepartamento.Columns(1).Value = dpt
          ssdbDepartamento = DesDpto
        Else
          ssdbDepartamento.Columns(1).Value = Null
          ssdbDepartamento = ""
        End If
'    End If
    txtTransmitida = "NO"
    SSDateCombo1.Date = strFecha_Sistema
  End If
  SSDateCombo1.BackColor = objApp.objUserColor.lngReadOnly
  txtTransmitida.BackColor = objApp.objUserColor.lngReadOnly
  ssdbEspecialidad.BackColor = objApp.objUserColor.lngMandatory
  ssdbDepartamento.BackColor = objApp.objUserColor.lngMandatory
  txtDesperfecto.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub CargarComboEspecialidad()
Dim str$, ww$
Dim rd As rdoResultset
Dim qy As rdoQuery
str = "SELECT MN62CODESPECIALI, MN62DESESPECIALI FROM MN6200 ORDER BY MN62CODESPECIALI ASC"
    Set rd = objApp.rdoConnect.OpenResultset(str, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
        ww = rd!MN62DESESPECIALI & "; " & rd!MN62CODESPECIALI
          ssdbEspecialidad.AddItem ww
        rd.MoveNext
    Wend
    rd.Close
   ssdbEspecialidad.Columns(0).Value = Null
End Sub
Private Sub CargarComboDepartamentos()
Dim str$, ww$
Dim rd As rdoResultset
Dim qy As rdoQuery
str = "SELECT AD02CODDPTO, AD02DESDPTO FROM AD0200 ORDER BY AD02DESDPTO ASC"
    Set rd = objApp.rdoConnect.OpenResultset(str, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
        ww = rd!AD02DESDPTO & "; " & rd!AD02CODDPTO
        ssdbDepartamento.AddItem ww
        rd.MoveNext
    Wend
    rd.Close
   ssdbDepartamento.Columns(1).Value = Null
End Sub

Private Sub optInstalacion_Click()
  Select Case optInstalacion
  Case True
    frmPeticion.Enabled = True
  Case Else
    frmPeticion.Enabled = False
  End Select
End Sub

Private Sub optReparacion_Click()
  Select Case optReparacion
  Case True
    frmPeticion.Enabled = True
  Case Else
    frmPeticion.Enabled = False
  End Select
End Sub

Private Sub txtCodUsuario_Change()
txtUsuario = ""
'ssdbDepartamento.Columns(1).Value = Null
'ssdbDepartamento = ""
End Sub

'Private Sub txtCodUsuario_KeyDown(KeyCode As Integer, Shift As Integer)
'  Dim str$, DesDpto$
'  Dim rd As rdoResultset
'  Dim qy As rdoQuery
'  Dim dpt%
'  If KeyCode = 13 Then
'    str = "SELECT SG02NOM,SG02APE1 FROM SG0200 WHERE SG02COD=?"
'    Set qy = objApp.rdoConnect.CreateQuery("", str)
'    qy(0) = txtCodUsuario
'    Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'    If rd.RowCount > 0 Then
'      txtUsuario = rd!SG02NOM + " " + rd!SG02APE1
'      dpt = ObtenerDepartamento(DesDpto)
'      If dpt <> 0 Then
'        ssdbDepartamento.Columns(1).Value = dpt
'        ssdbDepartamento = DesDpto
'      Else
'        ssdbDepartamento.Columns(1).Value = Null
'        ssdbDepartamento = ""
'      End If
'    Else
'      MsgBox "Este usuario no existe", vbCritical, ""
'      txtCodUsuario = ""
'    End If
'    rd.Close
'    qy.Close
'  End If
'End Sub
Private Function ObtenerDepartamento(DesDpto As String) As Integer
Dim str$
Dim rd As rdoResultset
Dim qy As rdoQuery

  str = "SELECT AD0300.AD02CODDPTO, AD0200.AD02DESDPTO " & _
  "FROM AD0300, AD0200 WHERE AD0300.SG02COD=? AND " & _
  "AD0300.AD02CODDPTO = AD0200.AD02CODDPTO"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
  qy(0) = objSecurity.strUser
  Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  If rd.RowCount > 0 Then
    DesDpto = rd!AD02DESDPTO
    ObtenerDepartamento = rd!AD02CODDPTO
  Else
    ObtenerDepartamento = 0
  End If
  rd.Close
  qy.Close
End Function

'Private Sub txtCodUsuario_LostFocus()
'  Call txtCodUsuario_KeyDown(13, 0)
'End Sub
