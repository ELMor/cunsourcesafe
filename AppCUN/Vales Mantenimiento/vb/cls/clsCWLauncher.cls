VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Const PIWinMNPeticion As String = "MN0101"
Const PIWinMNPeticionDpto As String = "MN0102"
Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
  Set objCW = mobjCW
End Sub
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean
'  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  Select Case strProcess
    Case PIWinMNPeticion
          Dim a
        On Error Resume Next
        a = vntData
        If Err = 0 Then
        gblCodPeticion = vntData
        End If
        Err = 0

        frmPeticion.Show vbModal
        gblCodPeticion = 0
        Set frmPeticion = Nothing
    Case PIWinMNPeticionDpto
        frmPeticionDepartamento.Show vbModal
        Set frmPeticionDepartamento = Nothing
    Case Else
        LaunchProcess = False
   End Select

'  Call Err.Clear
  End Function
  Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a procesos
  ReDim aProcess(1 To 2, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = PIWinMNPeticion
  aProcess(1, 2) = "Selecci�n de Petici�n"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = PIWinMNPeticionDpto
  aProcess(2, 2) = "Peticiones a Mantenimiento del Departamento"
  aProcess(2, 3) = True
  aProcess(3, 4) = cwTypeWindow

End Sub


