VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Los valores deben coincidir con el nombre del archivo en disco
Const CI02WinCitasPaciente        As String = "CI0201"
Const CI02WinCitasPaciente1       As String = "CI0202"
Const CI02WinCitasRecurso         As String = "CI0207" 'llamada por menu
Const CI02WinCitasRecurso1        As String = "CI0208" 'llamada arrastrando dtpo/doctor
Const CI02WinActDpto              As String = "CI0209"
Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
     
    Case CI02WinCitasPaciente
      frmCitasPaciente.Show vbModal
    Case CI02WinCitasPaciente1
      Call MostrarCitasPac(CLng(vntData))
      
    Case CI02WinCitasRecurso
        frmCitasRecurso.Show vbModal
    
    Case CI02WinCitasRecurso1
        'vntData(0) codigo del dpto
        'vntdata(1) c�diog del doctor
       Call MostrarCitasRecurso(Val(vntData(1)), Val(vntData(0)))
    Case CI02WinActDpto
        frmActDepartamento.Show vbModal
        
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n�mero de procesos
  ReDim aProcess(1 To 4, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = CI02WinCitasPaciente
  aProcess(1, 2) = "Citas del paciente"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = CI02WinCitasPaciente1
  aProcess(2, 2) = "Citas del paciente (por historia)"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = CI02WinCitasRecurso
  aProcess(3, 2) = "Citas del recurso"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(3, 1) = CI02WinCitasRecurso1
  aProcess(3, 2) = "Citas del recurso por recurso"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = CI02WinActDpto
  aProcess(4, 2) = "Actuaciones del Departamento"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
End Sub

