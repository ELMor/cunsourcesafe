VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmResponsables 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Responsables"
   ClientHeight    =   5865
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11475
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5865
   ScaleWidth      =   11475
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame ddireccion 
      Caption         =   "Direcci�n"
      ForeColor       =   &H00800000&
      Height          =   1740
      Left            =   0
      TabIndex        =   14
      Top             =   3780
      Width           =   11385
      Begin VB.CommandButton cmdBuscarCalle 
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   285
         Left            =   3780
         TabIndex        =   84
         Top             =   960
         Width           =   390
      End
      Begin VB.Frame frachkdir 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   8940
         TabIndex        =   81
         Top             =   1380
         Width           =   1095
         Begin VB.CheckBox chkdir 
            Alignment       =   1  'Right Justify
            Caption         =   "Principal"
            Height          =   255
            Left            =   120
            TabIndex        =   82
            Top             =   0
            Width           =   915
         End
      End
      Begin VB.VScrollBar vsDirec 
         Height          =   1425
         Left            =   10050
         TabIndex        =   31
         Top             =   225
         Width           =   240
      End
      Begin VB.CommandButton cmdbuscarcodpostal 
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   285
         Left            =   3780
         TabIndex        =   30
         Top             =   600
         Width           =   390
      End
      Begin VB.CommandButton cmdnuevodir 
         Caption         =   "&Nuevo"
         Height          =   375
         Left            =   10350
         TabIndex        =   29
         Top             =   225
         Width           =   885
      End
      Begin VB.CommandButton cmdguardardir 
         Caption         =   "&Guardar"
         Height          =   375
         Left            =   10350
         TabIndex        =   28
         Top             =   600
         Width           =   885
      End
      Begin VB.TextBox txtlocalidaddir 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   900
         MaxLength       =   30
         TabIndex        =   3
         Tag             =   "25"
         Top             =   600
         Width           =   2775
      End
      Begin VB.TextBox txtcodpostal 
         Height          =   285
         Left            =   4800
         MaxLength       =   5
         TabIndex        =   4
         Tag             =   "26"
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtcalle 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   900
         MaxLength       =   40
         TabIndex        =   5
         Tag             =   "27"
         Top             =   975
         Width           =   2760
      End
      Begin VB.TextBox txtportal 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   5025
         MaxLength       =   10
         TabIndex        =   6
         Tag             =   "28"
         Top             =   975
         Width           =   540
      End
      Begin VB.TextBox txtresdir 
         Height          =   285
         Left            =   6780
         MaxLength       =   30
         TabIndex        =   7
         Tag             =   "29"
         Top             =   975
         Width           =   3315
      End
      Begin VB.TextBox txttfnodir 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   6300
         MaxLength       =   15
         TabIndex        =   8
         Tag             =   "30"
         Top             =   225
         Width           =   1335
      End
      Begin VB.TextBox txtfax 
         Height          =   285
         Left            =   6300
         MaxLength       =   15
         TabIndex        =   9
         Tag             =   "31"
         Top             =   600
         Width           =   1305
      End
      Begin VB.TextBox txtobservaciones 
         Height          =   285
         Left            =   900
         MaxLength       =   2000
         TabIndex        =   12
         Tag             =   "34"
         Top             =   1350
         Width           =   8025
      End
      Begin VB.CommandButton cmdeliminardir 
         Caption         =   "&Eliminar"
         Height          =   375
         Left            =   10350
         TabIndex        =   15
         Top             =   1275
         Width           =   885
      End
      Begin SSDataWidgets_B.SSDBCombo cbopaisdir 
         Height          =   285
         Left            =   900
         TabIndex        =   1
         Tag             =   "23"
         Top             =   225
         Width           =   2010
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   3545
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo cboprovdir 
         Height          =   285
         Left            =   3750
         TabIndex        =   2
         Tag             =   "24"
         Top             =   225
         Width           =   1785
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   3149
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcbofecini 
         Height          =   315
         Left            =   8475
         TabIndex        =   10
         Tag             =   "32"
         Top             =   225
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcbofecfin 
         Height          =   315
         Left            =   8475
         TabIndex        =   11
         Tag             =   "33"
         Top             =   600
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483639
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Provincia:"
         Height          =   195
         Index           =   22
         Left            =   3000
         TabIndex        =   27
         Top             =   225
         Width           =   705
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Pais:"
         Height          =   195
         Index           =   23
         Left            =   450
         TabIndex        =   26
         Top             =   300
         Width           =   345
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Localidad:"
         Height          =   195
         Index           =   24
         Left            =   75
         TabIndex        =   25
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "C.P:"
         Height          =   195
         Index           =   25
         Left            =   4380
         TabIndex        =   24
         Top             =   660
         Width           =   300
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Calle:"
         Height          =   195
         Index           =   26
         Left            =   375
         TabIndex        =   23
         Top             =   1050
         Width           =   390
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Portal:"
         Height          =   195
         Index           =   27
         Left            =   4500
         TabIndex        =   22
         Top             =   1050
         Width           =   450
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Resto direcc:"
         Height          =   195
         Index           =   28
         Left            =   5775
         TabIndex        =   21
         Top             =   1050
         Width           =   945
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Tfno:"
         Height          =   195
         Index           =   29
         Left            =   5820
         TabIndex        =   20
         Top             =   300
         Width           =   375
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Fax:"
         Height          =   195
         Index           =   30
         Left            =   5880
         TabIndex        =   19
         Top             =   675
         Width           =   375
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "F. Inicio"
         Height          =   195
         Index           =   31
         Left            =   7725
         TabIndex        =   18
         Top             =   300
         Width           =   555
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin"
         Height          =   195
         Index           =   32
         Left            =   7800
         TabIndex        =   17
         Top             =   675
         Width           =   690
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observ:"
         Height          =   195
         Index           =   33
         Left            =   150
         TabIndex        =   16
         Top             =   1425
         Width           =   555
      End
   End
   Begin VB.CommandButton cmdsalir 
      Caption         =   "&Salir"
      Height          =   315
      Left            =   10200
      TabIndex        =   13
      Top             =   5550
      Width           =   1215
   End
   Begin VB.Frame dpersona 
      Caption         =   "Datos del paciente"
      ForeColor       =   &H00800000&
      Height          =   3690
      Left            =   0
      TabIndex        =   0
      Top             =   75
      Width           =   11385
      Begin VB.Frame dnacimiento 
         Caption         =   "Nacimiento"
         ForeColor       =   &H00800000&
         Height          =   1365
         Left            =   75
         TabIndex        =   34
         Top             =   1800
         Width           =   6510
         Begin VB.CheckBox chkfall 
            Alignment       =   1  'Right Justify
            Caption         =   "Fallecido"
            Height          =   315
            Left            =   2520
            TabIndex        =   83
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox txtlocalidadnac 
            Height          =   285
            Left            =   900
            MaxLength       =   30
            TabIndex        =   35
            Tag             =   "17"
            Top             =   975
            Width           =   5430
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcbofecnac 
            Height          =   285
            Left            =   900
            TabIndex        =   36
            Tag             =   "14"
            Top             =   240
            Width           =   1575
            _Version        =   65537
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483639
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
         End
         Begin SSDataWidgets_B.SSDBCombo cboprovnac 
            Height          =   285
            Left            =   3840
            TabIndex        =   37
            Tag             =   "16"
            Top             =   600
            Width           =   2505
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4419
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcbofecfall 
            Height          =   285
            Left            =   4725
            TabIndex        =   38
            Top             =   225
            Width           =   1575
            _Version        =   65537
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483639
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
         End
         Begin SSDataWidgets_B.SSDBCombo cbopaisnac 
            Height          =   285
            Left            =   900
            TabIndex        =   39
            Tag             =   "15"
            Top             =   600
            Width           =   2280
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4022
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "F. Fallecimiento:"
            Height          =   195
            Index           =   15
            Left            =   3600
            TabIndex        =   44
            Top             =   300
            Width           =   1140
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Localidad:"
            Height          =   195
            Index           =   14
            Left            =   75
            TabIndex        =   43
            Top             =   975
            Width           =   735
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Pais:"
            Height          =   195
            Index           =   13
            Left            =   450
            TabIndex        =   42
            Top             =   675
            Width           =   345
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Prov:"
            Height          =   195
            Index           =   12
            Left            =   3300
            TabIndex        =   41
            Top             =   675
            Width           =   375
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Fecha:"
            Height          =   195
            Index           =   11
            Left            =   300
            TabIndex        =   40
            Top             =   300
            Width           =   495
         End
      End
      Begin VB.TextBox txthistoria 
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   65
         Tag             =   "1"
         Top             =   300
         Width           =   675
      End
      Begin VB.TextBox txtape2 
         Height          =   285
         Left            =   975
         MaxLength       =   25
         TabIndex        =   64
         Tag             =   "5"
         Top             =   1425
         Width           =   2475
      End
      Begin VB.TextBox txtape1 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   975
         MaxLength       =   25
         TabIndex        =   63
         Tag             =   "4"
         Top             =   1050
         Width           =   2475
      End
      Begin VB.TextBox txtnombre 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   975
         MaxLength       =   25
         TabIndex        =   62
         Tag             =   "3"
         Top             =   675
         Width           =   2475
      End
      Begin VB.TextBox txtdni 
         Height          =   285
         Left            =   7725
         MaxLength       =   12
         TabIndex        =   61
         Tag             =   "9"
         Top             =   300
         Width           =   1035
      End
      Begin VB.TextBox txtsspro 
         Height          =   285
         Left            =   7725
         MaxLength       =   2
         TabIndex        =   60
         Tag             =   "10"
         Top             =   675
         Width           =   315
      End
      Begin VB.TextBox txtssnum 
         Height          =   285
         Left            =   8062
         MaxLength       =   8
         TabIndex        =   59
         Tag             =   "11"
         Top             =   675
         Width           =   1155
      End
      Begin VB.TextBox txtssnum2 
         Height          =   285
         Left            =   9240
         MaxLength       =   2
         TabIndex        =   58
         Tag             =   "12"
         Top             =   675
         Width           =   315
      End
      Begin VB.TextBox txtmovil 
         Height          =   285
         Left            =   7725
         MaxLength       =   12
         TabIndex        =   57
         Tag             =   "13"
         Top             =   1050
         Width           =   1845
      End
      Begin VB.CommandButton cmdbuscarper 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   9675
         TabIndex        =   56
         Top             =   975
         Width           =   975
      End
      Begin VB.CommandButton cmdnuevoper 
         Caption         =   "&Nuevo"
         Height          =   315
         Left            =   9675
         TabIndex        =   55
         Top             =   225
         Width           =   975
      End
      Begin VB.CommandButton cmdguardarper 
         Caption         =   "&Guardar"
         Height          =   315
         Left            =   9675
         TabIndex        =   54
         Top             =   600
         Width           =   975
      End
      Begin VB.Frame dprofesion 
         Caption         =   "Datos profesionales"
         ForeColor       =   &H00800000&
         Height          =   1380
         Left            =   6600
         TabIndex        =   45
         Top             =   1800
         Width           =   4605
         Begin VB.TextBox txtempresa 
            Height          =   285
            Left            =   900
            MaxLength       =   20
            TabIndex        =   48
            Tag             =   "20"
            Top             =   600
            Width           =   3585
         End
         Begin VB.TextBox txtpuesto 
            Height          =   285
            Left            =   900
            MaxLength       =   20
            TabIndex        =   47
            Tag             =   "21"
            Top             =   975
            Width           =   3585
         End
         Begin VB.TextBox txttfnotrab 
            Height          =   285
            Left            =   3375
            MaxLength       =   15
            TabIndex        =   46
            Tag             =   "19"
            Top             =   225
            Width           =   1110
         End
         Begin SSDataWidgets_B.SSDBCombo cboprofesion 
            Height          =   285
            Left            =   900
            TabIndex        =   49
            Tag             =   "18"
            Top             =   225
            Width           =   1995
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3519
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Profesion:"
            Height          =   195
            Index           =   18
            Left            =   120
            TabIndex        =   53
            Top             =   300
            Width           =   705
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Empresa:"
            Height          =   195
            Index           =   19
            Left            =   150
            TabIndex        =   52
            Top             =   675
            Width           =   660
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Puesto:"
            Height          =   195
            Index           =   20
            Left            =   225
            TabIndex        =   51
            Top             =   1050
            Width           =   540
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Tfno:"
            Height          =   195
            Index           =   21
            Left            =   2925
            TabIndex        =   50
            Top             =   300
            Width           =   375
         End
      End
      Begin VB.TextBox txtobser 
         Height          =   285
         Left            =   1290
         MaxLength       =   2000
         TabIndex        =   33
         Tag             =   "22"
         Top             =   3285
         Width           =   9885
      End
      Begin SSDataWidgets_B.SSDBCombo cbosexo 
         Height          =   285
         Left            =   2355
         TabIndex        =   66
         Tag             =   "2"
         Top             =   300
         Width           =   1095
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo cbotratam 
         Height          =   285
         Left            =   4725
         TabIndex        =   67
         Tag             =   "6"
         Top             =   300
         Width           =   2310
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4075
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo cboestcivil 
         Height          =   285
         Left            =   4725
         TabIndex        =   68
         Tag             =   "7"
         Top             =   675
         Width           =   2310
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4075
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo cboreludn 
         Height          =   285
         Left            =   4725
         TabIndex        =   69
         Tag             =   "8"
         Top             =   1050
         Width           =   2325
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4101
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblLabel 
         Caption         =   "Nombre:"
         Height          =   255
         Index           =   0
         Left            =   300
         TabIndex        =   80
         Top             =   750
         Width           =   630
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido:"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   79
         Top             =   1125
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido:"
         Height          =   195
         Index           =   2
         Left            =   150
         TabIndex        =   78
         Top             =   1500
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Sexo:"
         Height          =   195
         Index           =   3
         Left            =   1800
         TabIndex        =   77
         Top             =   315
         Width           =   405
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Estado civil:"
         Height          =   195
         Index           =   6
         Left            =   3750
         TabIndex        =   76
         Top             =   750
         Width           =   930
      End
      Begin VB.Label lblLabel 
         Caption         =   "N� Historia:"
         Height          =   315
         Index           =   8
         Left            =   150
         TabIndex        =   75
         Top             =   300
         Width           =   825
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Relacion UDN:"
         Height          =   195
         Index           =   10
         Left            =   3525
         TabIndex        =   74
         Top             =   1125
         Width           =   1080
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Tratamiento:"
         Height          =   195
         Index           =   4
         Left            =   3675
         TabIndex        =   73
         Top             =   375
         Width           =   885
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "M�vil:"
         Height          =   195
         Index           =   9
         Left            =   7200
         TabIndex        =   72
         Top             =   1125
         Width           =   420
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "S.S:"
         Height          =   195
         Index           =   7
         Left            =   7350
         TabIndex        =   71
         Top             =   750
         Width           =   300
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "D.N.I:"
         Height          =   195
         Index           =   5
         Left            =   7200
         TabIndex        =   70
         Top             =   375
         Width           =   420
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones:"
         Height          =   195
         Index           =   35
         Left            =   60
         TabIndex        =   32
         Top             =   3360
         Width           =   1110
      End
   End
End
Attribute VB_Name = "frmResponsables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim sql As String
Dim lngCodPaciente As Long
Dim rsdir As rdoResultset
Dim blnNuevaPer As Boolean
Dim lngCodPersonaFis As Long
Dim intTipo_Responsable As Integer
'Dim blnibm As Boolean
Dim blnsalir As Boolean
Dim lngNumDir As Long
Dim blnnuevo As Boolean  'PARA SABER SI LE HAN LLAMADO PARA CREAR UN NUEVO RESPONSABLE
Dim blnnuevadir As Boolean
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim blnDatosDirCargados As Boolean 'Cada vez que se cargan los datos de direccion
Dim blnCambiosDir As Boolean 'para controlar si ha habido cambios en la direccion
Dim blnCambiosPer As Boolean 'para controlar los cambios en la persona
Dim blnDatosCargados As Boolean 'cada vez que se cargan los datos de una persona
Dim blnLlamada As Boolean
Dim blnCodPostal As Boolean 'para saber si se ha ido a buscar el c�digo Postal y que al volver no se vuelva a preguntar si se quiere actualizar la direccion del responsable
Dim lngNumHistoria As Long

Private Sub pGuardarPer(blnabort As Boolean)
Dim vntDigControl
Dim strLetra As String
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strDatos As String
'Dim lngCodPaciente As Long
Dim vntData
If txthistoria.Text = "" And txtnombre.Text = "" Then
    MsgBox "No existe Ninguna Persona", vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
Else
    strDatos = fComprobaciones
    If Len(strDatos) > 0 Then
        strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
        MsgBox strDatos, vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
        blnabort = True
        Exit Sub
    End If

Screen.MousePointer = vbHourglass
  If txtdni.Text <> "" Then
        txtdni.Text = ValidarDNI(txtdni.Text)
        'Validar d�gito control del N.I.F.
        If Right(txtdni.Text, 1) Like "[a-zA-Z]" And _
           Left(Right(txtdni.Text, 2), 1) Like "#" Then
        
          If Right(txtdni.Text, 1) Like "[a-z]" Then
            'D�gito en min�sculas, lo pasamos a may�sculas.
            vntDigControl = UCase(Right(txtdni.Text, 1))
            txtdni.Text = Left(txtdni.Text, Len(txtdni.Text) - 1) & _
                  CStr(vntDigControl)
           End If
        
           strLetra = ValidaDigControlDNI(txtdni.Text)
           If strLetra <> Right(txtdni.Text, 1) Then
            MsgBox "El d�gito de control del D.N.I. es err�neo.", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
            txtdni.SetFocus
            blnabort = True
             Exit Sub
           End If
        End If
      End If
      
      'N� S.S.
      'Validar que metan C�d.Provincia y N�.
      If (txtsspro.Text <> "" And txtssnum.Text = "") Or _
        (txtsspro.Text = "" And txtssnum.Text <> "") Then
        'Call objError.SetError(cwCodeMsg,
         MsgBox "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        'Call objError.Raise
        'blnCancel = True
        txtsspro.SetFocus
        blnabort = True
        Exit Sub
        
      End If
      
      'Validar que no metan solo el d�gito de control.
      If txtsspro.Text = "" And txtssnum.Text = "" And _
         txtssnum2 <> "" Then
        'Call objError.SetError(cwCodeMsg,
          MsgBox "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero", vbExclamation + vbOKOnly + Right(Me.Caption, Len(Me.Caption) - 6)
        'Call objError.Raise
'        nCancel = True
        txtsspro.SetFocus
        blnabort = True
        Exit Sub
      End If
      
      'Validar C�d.Provincia del N� S.S.
      If txtsspro.Text <> "" Then
         If Val(txtsspro.Text) = 0 Or Val(txtsspro.Text) > 52 Then
            'Call objError.SetError(cwCodeMsg,
               MsgBox "El C�digo de Provincia del N� Seguridad Social es incorrecto.", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
            'Call objError.Raise
            'blnCancel = True
            txtsspro.SetFocus
            blnabort = True
            Exit Sub
         End If
      End If
      'validar que tanto la fecha de nacimiento como la fecha de fallecimiento sea menor que la del sistema
    If Trim(dcbofecnac.Text) <> "" Then
      If CDate(dcbofecnac.Text) > CDate(fAhora) Then
        MsgBox "La fecha de Nacimiento es mayor que la de hoy", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        blnabort = True
        Exit Sub
      End If
    End If
     If Trim(dcbofecfall.Text) <> "" Then
       If CDate(dcbofecfall.Text) > CDate(fAhora) Then
        MsgBox "La fecha de Fallecimiento es mayor que la de hoy", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        blnabort = True
        Exit Sub
      End If
     End If
        'Validar d�gito de control del N� S.S.
      If (txtsspro.Text <> "" And txtssnum.Text <> "") Then
        If txtssnum2.Text <> "" Then
            If Not ValidaDigControlNSS(Val(txtsspro.Text), _
                Val(txtssnum.Text), Val(txtssnum2.Text)) Then
               'Call objError.SetError(cwCodeMsg,
                MsgBox "El d�gito de control del N�mero de la Seguridad Social es err�neo.", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
               'Call objError.Raise
               'blnCancel = True
               txtssnum.SetFocus
               blnabort = True
               Exit Sub
            End If
        End If
      End If
On Error GoTo Canceltrans
    objApp.BeginTrans
    If Not blnNuevaPer Then 'And txthistoria.Locked = True Es una modificacion
        sql = "UPDATE CI2200 SET"
        sql = sql & " CI22NOMBRE=?,"
        sql = sql & " CI22PRIAPEL=?,"
        sql = sql & " CI22SEGAPEL=?,"
        sql = sql & " CI30CODSEXO=?,"
        sql = sql & " CI34CODTRATAMI=?,"
        sql = sql & " CI14CODESTCIVI=?,"
        sql = sql & " CI28CODRELUDN=?,"
        sql = sql & " CI22DNI=?,"
        sql = sql & " CI22NUMSEGSOC=?,"
        sql = sql & " CI22TFNOMOVIL=?,"
        sql = sql & " CI22FECNACIM=TO_DATE(?,'DD/MM/YYYY'),"
        sql = sql & " CI19CODPAIS=?," 'pais de nacimiento
        sql = sql & " CI26CODPROVI=?," 'provincia de nacimiento
        sql = sql & " CI22DESLOCALID=?," 'localidad de nacimiento
        sql = sql & " CI22FECFALLE=TO_DATE(?,'DD/MM/YYYY'),"
      
        sql = sql & " CI25CODPROFESI=?,"
        sql = sql & " CI22EMPRESA=?,"
        sql = sql & " CI22PUESTO=?,"
        sql = sql & " CI22TFNOEMPRESA=?,"
        sql = sql & " CI22INDFALLE=?,"
        sql = sql & " CI22OBSERVAC=? WHERE CI21CODPERSONA=?"
       
        


     Else 'ES UNO NUEVO
     sql = "INSERT INTO CI2100 (CI21CODPERSONA,CI33CODTIPPERS) VALUES (?,1)"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = fSigCodpersona
            lngCodPersonaFis = qry(0)
            qry.Execute
            qry.Close
      sql = "INSERT INTO CI2200 "
        sql = sql & " (CI22NOMBRE,CI22PRIAPEL,"
        sql = sql & " CI22SEGAPEL,CI30CODSEXO,"
        sql = sql & " CI34CODTRATAMI,CI14CODESTCIVI,"
        sql = sql & " CI28CODRELUDN,CI22DNI,"
        sql = sql & " CI22NUMSEGSOC,CI22TFNOMOVIL,"
        sql = sql & " CI22FECNACIM,"
        sql = sql & " CI19CODPAIS,CI26CODPROVI," 'PAIS Y provincia de nacimiento
        sql = sql & " CI22DESLOCALID,CI22FECFALLE,"
       
        sql = sql & " CI25CODPROFESI,"
        sql = sql & " CI22EMPRESA,CI22PUESTO,"
        sql = sql & " CI22TFNOEMPRESA,CI22INDFALLE,"
        sql = sql & " CI22OBSERVAC,CI21CODPERSONA,CI22INDPROVISI)"
        sql = sql & " VALUES (?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,0)"
          'colocamos los pipes para el paso a la pantalla de responsables y desde all� se actualiza el campor responsable familiar
        
    End If
    
       Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtnombre.Text
        qry(1) = txtape1.Text
        qry(2) = txtape2.Text
        qry(3) = cbosexo.Columns(0).Value
        
        qry(4) = cbotratam.Columns(0).Value
        If Trim(cboestcivil.Text) = "" Then
            qry(5) = Null
        Else
            qry(5) = cboestcivil.Columns(0).Value
        End If
        If Trim(cboreludn.Text) = "" Or cboreludn.Columns(0).Value = 0 Then
            qry(6) = Null
        Else
            qry(6) = cboreludn.Columns(0).Value
        End If
        qry(7) = txtdni.Text
        qry(8) = txtsspro.Text & txtssnum.Text & txtssnum2.Text
        qry(9) = txtmovil.Text
        If dcbofecnac.Date = "" Then
            qry(10) = Null
        Else
            qry(10) = dcbofecnac.Date
        End If
        If Trim(cbopaisnac.Text) = "" Then
            qry(11) = Null
        Else
            qry(11) = cbopaisnac.Columns(0).Value
        End If
        If Trim(cboprovnac.Text) = "" Then
            qry(12) = Null
        Else
            qry(12) = cboprovnac.Columns(0).Value
        End If
        qry(13) = txtlocalidadnac.Text
        If dcbofecfall.Date = "" Then
            qry(14) = Null
        Else
            qry(14) = dcbofecfall.Date
        End If
        If Trim(cboprofesion.Text) = "" Or cboprofesion.Columns(0).Value = 0 Then
            qry(15) = Null
        Else
            qry(15) = cboprofesion.Columns(0).Value
        End If
        qry(16) = txtempresa.Text
        qry(17) = txtpuesto.Text
        qry(18) = txttfnotrab.Text
        If dcbofecfall.Date = "" And chkfall.Value = 0 Then
            qry(19) = -1
        Else
            qry(19) = -chkfall.Value
        End If
        qry(20) = txtobser.Text
        qry(21) = lngCodPersonaFis
       
     qry.Execute
     objApp.CommitTrans
   vntData = lngCodPersonaFis
        Call objPipe.PipeSet("AD_CodPersona", vntData)
    
    End If
    If blnCambiosDir Then
        Call pGuardarDir(lngNumDir)
        blnCambiosDir = False
        'blnDatosDirCargados = False
    End If
    'blnDatosCargados = False
    blnCambiosPer = False
    cmdguardarper.Enabled = False
    cmdguardardir.Enabled = False
    Screen.MousePointer = vbDefault
    Exit Sub
Canceltrans:
    MsgBox Error, vbCritical + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    objApp.RollbackTrans
    Screen.MousePointer = vbDefault


End Sub


Private Function fcomprobardir() As Boolean
Dim c As Control
Dim strColor As String
For Each c In Me.Controls
    If c.Container.Name = ddireccion.Name Then
       If TypeOf c Is TextBox Or TypeOf c Is SSDBCombo Then
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
                If Trim(c.Text) = "" Or c.Text = "__:__" Then
                 Call psalir
                 fcomprobardir = True
                 Exit Function
                End If
            End If
        End If
        If TypeOf c Is SSDateCombo Then
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
               If c.Date = "" Then
                Call psalir
                fcomprobardir = True
                Exit Function
               End If
             End If
        End If
    End If
Next
End Function

Private Sub pibm() 'Desactiva los controles y los deja como s�lo lectura
Dim c As Control, strColor$, msg$
 On Error Resume Next
    For Each c In Me
         If TypeOf c Is TextBox Then
          c.BackColor = &HC0C0C0
          c.Locked = True
        End If
        If TypeOf c Is SSDBCombo Then
            c.BackColor = &HC0C0C0
            c.AllowInput = False
        End If
        If TypeOf c Is SSDateCombo Then
            c.BackColor = &HC0C0C0
            c.AllowEdit = False
        End If
        If TypeOf c Is CommandButton Or TypeOf c Is CheckBox Then
            c.Enabled = False
        End If
     Next
     cmdsalir.Enabled = True
End Sub
Private Sub pcargar_datos()
Dim sql As String
'Dim rs As rdoResultset
    sql = "select CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,"
    sql = sql & " ci22numhistoria,CI2200.CI21CODPERSONA,"
    sql = sql & " CI22DNI,CI22FECNACIM,CI22FECFALLE,"
    sql = sql & " CI22NUMSEGSOC,CI22TFNOMOVIL,"
    sql = sql & " CI2200.CI16CODLOCALID CODLOCNAC,CI22DESLOCALID DESLOCNAC,CI2200.CI26CODPROVI CODPROVNAC,CI26DESPROVI DESPROVNAC,"
    sql = sql & " NAC.CI19DESPAIS PAISNAC,CI2200.CI19CODPAIS,CI2200.CI25CODPROFESI,CI25DESPROFESI,CI2200.CI28CODRELUDN,CI28DESRELUDN,"
    sql = sql & " CI2200.CI34CODTRATAMI,CI34DESTRATAMI,CI2200.CI14CODESTCIVI,CI14DESESTCIVI,CI2200.CI30CODSEXO,CI30DESSEXO,"
    sql = sql & " CI2200.CI35CODTIPVINC,CI35DESTIPVINC,"
    sql = sql & " CI22OBSERVAC,CI22EMPRESA,CI22PUESTO,CI22TFNOEMPRESA,CI22INDFALLE,"
    sql = sql & " CI21CODPERSONA_RFA,CI21CODPERSONA_REC,"
    'a partir de aqu� se cargan los campos del IBM
    sql = sql & " CI22IBMPROFESION,CI22IBMRELUDN"
   
    sql = sql & " From"
    sql = sql & " CI2200,CI2100,CI2600,"
    sql = sql & " CI1900 NAC,CI2500,CI2800,"
    sql = sql & " CI3400,CI1400,CI3000,"
    sql = sql & " CI3500"
    sql = sql & " Where"
    sql = sql & " CI2200.CI26CODPROVI=CI2600.CI26CODPROVI(+)"
    sql = sql & " AND CI2200.CI19CODPAIS=NAC.CI19CODPAIS(+)"
    sql = sql & " AND CI2200.CI28CODRELUDN=CI2800.CI28CODRELUDN(+)"
    sql = sql & " AND CI2200.CI34CODTRATAMI=CI3400.CI34CODTRATAMI(+)"
    sql = sql & " AND CI2200.CI14CODESTCIVI=CI1400.CI14CODESTCIVI(+)"
    sql = sql & " AND CI2200.CI25CODPROFESI=CI2500.CI25CODPROFESI(+)"
    sql = sql & " AND CI2200.CI30CODSEXO=CI3000.CI30CODSEXO"
    sql = sql & " AND CI2200.CI35CODTIPVINC=CI3500.CI35CODTIPVINC(+)"
   ' sql = sql & " AND CI2200.CI21CODPERSONA=CI1000.CI21CODPERSONA"
    'sql = sql & " AND CI10INDDIRPRINC=-1"
    'sql = sql & " AND DIR.CI19CODPAIS=CI1000.CI19CODPAIS"
    sql = sql & " AND CI2200.CI21CODPERSONA=CI2100.CI21CODPERSONA"
    
    If lngCodPersonaFis <> 0 Then 'cargar los datos en funcion del c�digo de persona
         sql = sql & " AND CI2200.CI21CODPERSONA=" & lngCodPersonaFis
    Else
        If txthistoria.Text <> "" Then
            sql = sql & " AND ci22numhistoria =" & lngNumHistoria 'Val(txthistoria.Text)
        Else
            Exit Sub 'no existe ni numero de historia ni codigo de persona
        End If
        
    End If

     Set rs = objApp.rdoConnect.OpenResultset(sql)
     If rs.EOF Then 'no existe nadie con ese n�mero de historia
        MsgBox "No existe ninguna persona con ese n�mero de Historia.Utilice el buscador", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        txtnombre.SetFocus
            'txthistoria.Locked = True
            txthistoria.Text = ""
            Call plimpiar_controles
            'Call pcargarcombos
    Else 'Existe el n�mero de historia
   
        lngCodPersonaFis = rs!CI21CODPERSONA
        Call pselcombos
        cbosexo.Columns(0).Value = rs!CI30CODSEXO
        cbosexo.Text = rs!CI30DESSEXO
        txtnombre.Text = rs!CI22NOMBRE
        If Not IsNull(rs!CI22NUMHISTORIA) Then
            txthistoria.Text = rs!CI22NUMHISTORIA
        End If
       
        If Not IsNull(rs!CI22PRIAPEL) Then
            txtape1.Text = rs!CI22PRIAPEL
        End If
        If Not IsNull(rs!CI22SEGAPEL) Then
            txtape2.Text = rs!CI22SEGAPEL
        End If
        If Not IsNull(rs!CI34DESTRATAMI) Then
            cbotratam.Text = rs!CI34DESTRATAMI
            cbotratam.Columns(0).Value = rs!CI34CODTRATAMI
'            Else
'            cbotratam.Columns(0).Value = 0
        End If
        If Not IsNull(rs!CI14DESESTCIVI) Then
            cboestcivil.Text = rs!CI14DESESTCIVI
            cboestcivil.Columns(0).Value = rs!CI14CODESTCIVI
'        Else
'            cboestcivil.Columns(0).Value = 0
        End If
        If Not IsNull(rs!CI28DESRELUDN) Then
            cboreludn.Text = rs!CI28DESRELUDN
            cboreludn.Columns(0).Value = rs!CI28CODRELUDN
'            Else
'            cboreludn.Columns(0).Value = 0
'            If Not IsNull(rs!CI22IBMRELUDN) Then
'
'                cboreludn.Text = rs!CI22IBMRELUDN
'            End If
        End If
        If Not IsNull(rs!CI22DNI) Then
            txtdni.Text = rs!CI22DNI
        End If
        If Not IsNull(rs!CI22NUMSEGSOC) Then
         txtsspro.Text = Left(rs!CI22NUMSEGSOC, 2)
         txtssnum.Text = Mid(rs!CI22NUMSEGSOC, 3, 8)
          If Len(rs!CI22NUMSEGSOC) > 10 Then
            txtssnum2.Text = Right(rs!CI22NUMSEGSOC, 2)
          Else
            txtssnum2.Text = ""
          End If
        End If
        If Not IsNull(rs!CI22TFNOMOVIL) Then
            txtmovil.Text = rs!CI22TFNOMOVIL
        End If
        If Not IsNull(rs!CI22OBSERVAC) Then
            txtobser.Text = rs!CI22OBSERVAC
        End If
        If Not IsNull(rs!CI25DESPROFESI) Then
            cboprofesion.Text = rs!CI25DESPROFESI
            cboprofesion.Columns(0).Value = rs!CI25CODPROFESI
'            Else
'            cboprofesion.Columns(0).Value = 0
'            If Not IsNull(rs!CI22IBMPROFESION) Then
'                cboprofesion.Text = rs!CI22IBMPROFESION
'            End If
        End If
        If Not IsNull(rs!CI22EMPRESA) Then
            txtempresa.Text = rs!CI22EMPRESA
        End If
        If Not IsNull(rs!CI22PUESTO) Then
            txtpuesto.Text = rs!CI22PUESTO
        End If
        If Not IsNull(rs!CI22TFNOEMPRESA) Then
            txttfnotrab.Text = rs!CI22TFNOEMPRESA
        End If
        If Not IsNull(rs!PAISNAC) Then
            cbopaisnac.Text = rs!PAISNAC
            cbopaisnac.Columns(0).Value = rs!CI19CODPAIS
'           Else
'           cbopaisnac.Columns(0).Value = 0
        End If
         If cbopaisnac.Columns(0).Value = "45" Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600"
         Call pcargarcombos(sql, cboprovnac)
        End If
        If Not IsNull(rs!DESPROVNAC) Then
            cboprovnac.Text = rs!DESPROVNAC
            cboprovnac.Columns(0).Value = rs!CODPROVNAC
'        Else
'            cboprovnac.Columns(0).Value = 0
        End If
       
        If Not IsNull(rs!CI22FECFALLE) Then
            dcbofecfall.Date = Format(rs!CI22FECFALLE, "dd/mm/yyyy")
            chkfall.Value = 1
            chkfall.Enabled = False
            Else
            dcbofecfall.Date = ""
        End If
        If Not IsNull(rs!CI22FECNACIM) Then
            dcbofecnac.Date = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
            
        End If
       
        If Not IsNull(rs!DESLOCNAC) Then
            txtlocalidadnac.Text = rs!DESLOCNAC
        End If
         blnDatosCargados = True
         cmdguardarper.Enabled = False
       
    If Not blnNuevaPer Then
        Call seldir(lngCodPersonaFis)
        Call pdirecciones
        blnDatosDirCargados = False
        Call pllenardatosdir
    End If
        blnDatosDirCargados = True
      
    End If
    
End Sub

Private Sub psalir()
    Dim msg As String
    msg = "Existen datos obligatorios sin rellenar."
    MsgBox msg, vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
End Sub
Private Function fNombre(strnombre As String) As String
Select Case strnombre
Case "txtnombre"
    fNombre = "Nombre, "
Case "txtape1"
    fNombre = "Primer Apellido, "
Case "cbosexo"
    fNombre = "Sexo, "
Case "cbotratam"
    fNombre = "Tratamiento, "
Case cbopaisnac
    fNombre = "Pais de Nacimiento, "
Case "cbopaisdir"
    fNombre = "Pais de Direcci�n, "
Case "txtlocalidaddir"
    fNombre = "Localidad de Direcci�n, "
Case "txtcalle"
    fNombre = "Calle, "
Case "txtportal"
    fNombre = "Portal, "
Case "txttfnodir"
    fNombre = "Tel�fono de Direcci�n, "
Case "dcbofecini"
    fNombre = "Fecha de Inicio de Vigencia de la Direcci�n, "
End Select

End Function


Private Function fComprobaciones() As String
Dim c As Control, strColor$, msg$

    'campos obligatorios
    On Error Resume Next
        For Each c In Me
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
              If TypeOf c Is TextBox Or TypeOf c Is SSDBCombo Then
                If Trim(c.Text) = "" Or c.Text = "__:__" Then
                fComprobaciones = fComprobaciones & fNombre(c.Name)
                 'fComprobaciones = True
                 
                End If
               End If
              If TypeOf c Is SSDateCombo Then
               If c.Date = "" Then
                fComprobaciones = fComprobaciones & fNombre(c.Name)
                'fComprobaciones = True
               
                End If
              End If
            End If
        Next
End Function

Function fSigCodpersona() As Long 'Obtiene el siguiente codigo de persona hasta que se cree un sequence en la B.D.

Dim sql As String
Dim rsmax As rdoResultset

sql = "select max(ci21codpersona) from ci2100 "
Set rsmax = objApp.rdoConnect.OpenResultset(sql)
fSigCodpersona = rsmax(0) + 1
rsmax.Close


End Function

Private Sub pGuardarDir(Optional lngndir As Long)
Dim sql As String
Dim rsmax As rdoResultset
Dim qry As rdoQuery
Dim lngchardir As Long
Dim rsnum As rdoResultset
sql = "select max(ci10numdirecci) from ci1000 where ci21codpersona=" & lngCodPersonaFis
Set rsmax = objApp.rdoConnect.OpenResultset(sql)
On Error GoTo Canceltrans
objApp.BeginTrans
    If blnnuevadir Or blnNuevaPer Then
        sql = "INSERT INTO CI1000 "
        sql = sql & " (CI19CODPAIS,CI26CODPROVI,"
        sql = sql & "CI10CALLE,CI10PORTAL,CI10RESTODIREC,"
        sql = sql & "CI07CODPOSTAL,CI10DESLOCALID,CI10DESPROVI,"
        sql = sql & "CI10TELEFONO,CI10OBSERVACIO,CI10FECINIVALID,"
        sql = sql & "CI10INDDIRPRINC,CI10FECFINVALID,CI21CODPERSONA,CI10NUMDIRECCI)"
        sql = sql & " VALUES (?,?,?,?,?,?,?,?,?,?,TRUNC(SYSDATE),?,?,?,?)"
       
     Else 'es una modificacion
        sql = "UPDATE CI1000 SET "
        sql = sql & "CI19CODPAIS=?,CI26CODPROVI=?,"
        sql = sql & " CI10CALLE=?,CI10PORTAL=?,CI10RESTODIREC=?,"
        sql = sql & " CI07CODPOSTAL=?,CI10DESLOCALID=?,CI10DESPROVI=?,"
        sql = sql & " CI10TELEFONO=?,CI10OBSERVACIO=?,CI10FECINIVALID=TO_DATE('" & dcbofecini.Date & "','DD/MM/YYYY'),"
        sql = sql & " CI10INDDIRPRINC=?,CI10FECFINVALID=?"
        sql = sql & " WHERE CI21CODPERSONA=?"
        sql = sql & " AND CI10NUMDIRECCI=?"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    If Trim(cbopaisdir.Text) = "" Then
    qry(0) = Null
    Else
    qry(0) = cbopaisdir.Columns(0).Value
    End If
    If cbopaisdir.Columns(0).Value = 45 Then
        If Trim(cboprovdir.Text) = "" Then
            qry(1) = Null
        Else
            qry(1) = cboprovdir.Columns(0).Value
        End If
    Else
        qry(1) = Null
    End If
    qry(2) = txtcalle.Text
    qry(3) = txtportal.Text
    qry(4) = txtresdir.Text
    qry(5) = txtcodpostal.Text
    qry(6) = txtlocalidaddir.Text
    qry(7) = cboprovdir.Text
    qry(8) = txttfnodir.Text
    qry(9) = txtobservaciones.Text
    'qry(11) = dcbofecini.Date
     If blnNuevaPer Then
        qry(10) = -1
        chkdir.Value = 1
        frachkdir.Enabled = False
    Else
        qry(10) = -chkdir.Value
    End If
    If dcbofecfin.Date = "" Then
    qry(11) = Null
    Else
    qry(11) = dcbofecfin.Date
    End If
    qry(12) = lngCodPersonaFis
    
    If blnnuevadir Or blnNuevaPer Then
        If Not IsNull(rsmax(0)) Then
           qry(13) = rsmax(0) + 1
        Else
           qry(13) = 1
        End If
        Else 'no es nada nuevo
        If lngndir <> 0 Then
            qry(13) = lngndir
            Else
            qry(13) = rsdir!ci10numdirecci
            End If

    End If
    qry.Execute
    qry.Close
    sql = "select count(*) from ci1000 where ci21codpersona=" & lngCodPersonaFis
    sql = sql & " AND CI10INDDIRPRINC=-1"
    Set rsnum = objApp.rdoConnect.OpenResultset(sql)
    If rsnum(0) > 1 Then
        sql = "UPDATE CI1000 SET CI10INDDIRPRINC=0 WHERE CI10NUMDIRECCI<>"
        If lngndir <> 0 Then
                sql = sql & lngndir
            Else
            sql = sql & rsdir!ci10numdirecci
        End If
         sql = sql & " AND CI21CODPERSONA=" & lngCodPersonaFis
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry.Execute
        qry.Close
    End If
    objApp.CommitTrans
    blnnuevadir = False
    blnNuevaPer = False
    cmdguardardir.Enabled = False
    blnCambiosDir = False
    'blnDatosDirCargados = False
    
    Exit Sub
Canceltrans:
    MsgBox Error, vbCritical + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    objApp.RollbackTrans

End Sub

Private Sub pcargarcombos(sql As String, con As SSDBCombo)
    
 Dim rs As rdoResultset
 Set rs = objApp.rdoConnect.OpenResultset(sql)
 Do While Not rs.EOF
    con.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
 Loop
  
   
End Sub
Private Sub pSuprimir(con As SSDBCombo, KeyCode As Integer)
If KeyCode = 46 Then
    con.Columns(0).Value = "0"
    con.Text = ""
End If
End Sub
Private Sub pselcombos()
Dim sql As String
  sql = "SELECT CI30CODSEXO, CI30DESSEXO FROM CI3000"
        Call pcargarcombos(sql, cbosexo)
         sql = "SELECT CI34CODTRATAMI, CI34DESTRATAMI FROM CI3400 ORDER BY CI34DESTRATAMI"
         Call pcargarcombos(sql, cbotratam)
         sql = "SELECT CI14CODESTCIVI,CI14DESESTCIVI FROM CI1400 ORDER BY CI14DESESTCIVI"
        Call pcargarcombos(sql, cboestcivil)
         sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900 ORDER BY CI19DESPAIS"
        Call pcargarcombos(sql, cbopaisnac)
        Call pcargarcombos(sql, cbopaisdir)
         sql = "SELECT CI28CODRELUDN,CI28DESRELUDN FROM CI2800 ORDER BY CI28DESRELUDN"
        Call pcargarcombos(sql, cboreludn)
       
       
        sql = "SELECT CI25CODPROFESI,CI25DESPROFESI FROM CI2500"
        Call pcargarcombos(sql, cboprofesion)
        'Call pMensajes
End Sub
Private Sub plimpiar_controles()
Dim con As Control
For Each con In Me
    If TypeOf con Is CheckBox Then con.Value = 0
    If TypeOf con Is TextBox Then con.Text = ""
    If TypeOf con Is SSDBCombo Then
        con.RemoveAll
        con.Text = ""
    End If
    
Next
vsDirec.Enabled = False
lngCodPersonaFis = 0
frachkdir.Enabled = True
chkfall.Enabled = True
End Sub

Private Sub seldir(lngcod As Long, Optional blnpac As Boolean) 'esta vble (blnpac) se usa para
'saber si se est� seleccionando la direccion del paciente o del responsable
'porque si es la del paciente s�lo tiene que seleccionar la que sea dir principal.Se pasa cuando se va a crear un nuevo responsable�


 sql = "SELECT CI1000.CI16CODLOCALID CODLOCDIR,CI10DESLOCALID DESLOCDIR,CI1000.CI26CODPROVI CODPROVDIR,CI1000.CI10DESPROVI DESPROVDIR,"
    sql = sql & " CI19DESPAIS PAISDIR,CI1000.CI19CODPAIS,"
    sql = sql & " CI10CALLE,CI10PORTAL,"
    sql = sql & " CI10RESTODIREC,"
    sql = sql & " CI10TELEFONO,CI10OBSERVACIO,CI10FECINIVALID,"
    sql = sql & " CI10FECFINVALID,CI10INDDIRPRINC,CI10FAX,"
    sql = sql & " CI07CODPOSTAL,CI10NUMDIRECCI"
    sql = sql & " FROM CI1000,CI1900"
    sql = sql & " WHERE CI21CODPERSONA=?"
    'sql = sql & " AND CI10FECFINVALID IS NULL AND CI10FECFINVALID>"
    sql = sql & " AND CI1900.CI19CODPAIS=CI1000.CI19CODPAIS(+)"
    If blnpac Then
    sql = sql & " AND CI10INDDIRPRINC=-1"
    End If
Set qry = objApp.rdoConnect.CreateQuery("", sql)

    qry(0) = lngcod
Set rsdir = qry.OpenResultset(rdOpenKeyset)


End Sub
Function validardir() As Boolean
Dim intdirprinc As Integer
    Call seldir(lngCodPersonaFis)
    'Set rs = qry.OpenResultset(rdOpenKeyset)
    'rs.MoveFirst
    intdirprinc = 0
    Do While Not rsdir.EOF
        If rsdir!CI10INDDIRPRINC = -1 Then ' 0  rsdir!CI10INDDIRPRINC Is Not Null Then
            intdirprinc = intdirprinc + 1
        End If
        rsdir.MoveNext
    Loop
    If intdirprinc = 0 Then
        MsgBox "La persona no tiene ninguna direcci�n principal", vbOKOnly + vbCritical, Right(Me.Caption, Len(Me.Caption) - 6) & "Direcciones"
        validardir = True
    End If
    If intdirprinc > 1 Then
        MsgBox "La persona tiene mas de una direcci�n principal", vbOKOnly + vbCritical, Right(Me.Caption, Len(Me.Caption) - 6) & "Direcciones"
        validardir = True
  End If
End Function

Private Sub pdirecciones(Optional lngDirec As Long)

    If Not rsdir.EOF Then
   
     rsdir.MoveLast
    rsdir.MoveFirst
    vsDirec.Max = rsdir.RowCount
    vsDirec.Min = 1
    vsDirec.Value = 1
    If lngDirec = 0 Then
    'estar�a bien que en vez de posicionarse en la primera lo haga en la que se ha modificado
    vsDirec.Value = 1
    Else
    vsDirec.Value = lngDirec
    End If
    'Call pllenardatosdir
    End If
End Sub

Private Sub pllenardatosdir()
Dim sql As String
If Not rsdir.EOF Then
 If Not IsNull(rsdir!PAISDIR) Then
            cbopaisdir.Text = rsdir!PAISDIR
            cbopaisdir.Columns(0).Value = rsdir!CI19CODPAIS
        Else
            cbopaisdir.Columns(0).Value = 0
            cbopaisdir.Text = ""
        End If
        If cbopaisdir.Columns(0).Value = "45" Then
            cboprovdir.RemoveAll
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovdir)
        End If
        If Not IsNull(rsdir!DESPROVDIR) Then
            cboprovdir.Text = rsdir!DESPROVDIR
        
            
        Else
            cboprovdir.Text = ""
            'cboprovdir.Columns(0).Value = 0
        End If
          If Not IsNull(rsdir!CODPROVDIR) Then
             cboprovdir.Columns(0).Value = rsdir!CODPROVDIR
'             Else
'             cboprordir.Columns(0).Value = 0
             End If
       
        If Not IsNull(rsdir!CI07CODPOSTAL) Then
            txtcodpostal.Text = rsdir!CI07CODPOSTAL
            Else
            txtcodpostal.Text = ""
        End If
        If Not IsNull(rsdir!DESLOCDIR) Then
            txtlocalidaddir.Text = rsdir!DESLOCDIR
            Else
            txtlocalidaddir.Text = ""
        End If
         If Not IsNull(rsdir!CI10CALLE) Then
            txtcalle.Text = rsdir!CI10CALLE
            Else
            txtcalle.Text = ""
        End If
        If Not IsNull(rsdir!CI10PORTAL) Then
            txtportal.Text = rsdir!CI10PORTAL
            Else
            txtportal.Text = ""
        End If
        If Not IsNull(rsdir!CI10RESTODIREC) Then
            txtresdir.Text = rsdir!CI10RESTODIREC
            Else
            txtresdir.Text = ""
        End If
        If Not IsNull(rsdir!CI10TELEFONO) Then
            txttfnodir.Text = rsdir!CI10TELEFONO
            Else
            txttfnodir.Text = ""
        End If
        If Not IsNull(rsdir!CI10FAX) Then
            txtfax.Text = rsdir!CI10FAX
            Else
            txtfax.Text = ""
        End If
        If Not IsNull(rsdir!CI10FECINIVALID) Then
            dcbofecini.Date = Format(rsdir!CI10FECINIVALID, "dd/mm/yyyy")
            Else
            dcbofecini.Date = ""
        End If
        If Not IsNull(rsdir!CI10FECFINVALID) Then
            dcbofecfin.Date = Format(rsdir!CI10FECFINVALID, "dd/mm/yyyy")
            Else
            dcbofecfin.Date = ""
        End If
        If Not IsNull(rsdir!CI10INDDIRPRINC) Then
            chkdir.Value = Abs(rsdir!CI10INDDIRPRINC)
             If chkdir.Value = 1 Then
                frachkdir.Enabled = False
            Else
                frachkdir.Enabled = True
            End If
        End If
        If Not IsNull(rsdir!CI10OBSERVACIO) Then
            txtobservaciones.Text = rsdir!CI10OBSERVACIO
            Else
            txtobservaciones.Text = ""
        End If
        lngNumDir = rsdir!ci10numdirecci
        blnDatosDirCargados = True
        vsDirec.Enabled = True
    End If
cmdguardardir.Enabled = False

End Sub

Private Sub cboestcivil_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboestcivil_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboestcivil_KeyDown(KeyCode As Integer, Shift As Integer)
Call pSuprimir(cboestcivil, KeyCode)
End Sub

Private Sub cbopaisdir_Change()
Dim sql As String
txtlocalidaddir.Text = ""
txtcodpostal.Text = ""
txtcalle.Text = ""
txtportal.Text = ""
cboprovdir.RemoveAll
If cbopaisnac.Columns(0).Value = "45" Then
        sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600"
        Call pcargarcombos(sql, cboprovdir)
End If
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub cbopaisdir_Click()
Dim sql As String
txtlocalidaddir.Text = ""
txtcodpostal.Text = ""
txtcalle.Text = ""
txtportal.Text = ""
cboprovdir.RemoveAll
If cbopaisnac.Columns(0).Value = "45" Then
        sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600"
        Call pcargarcombos(sql, cboprovdir)
End If
  If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
  
End Sub

Private Sub cbopaisnac_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cbopaisnac_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cbopaisnac_KeyPress(KeyAscii As Integer)
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboprofesion_Change()
If blnDatosCargados Then
    blnCambiosPer = True
   cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboprofesion_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboprofesion_KeyDown(KeyCode As Integer, Shift As Integer)
Call pSuprimir(cboprofesion, KeyCode)

End Sub

Private Sub cboprovdir_Change()
'txtlocalidaddir.Text = ""
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub cboprovdir_Click()
'txtlocalidaddir.Text = ""
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""

If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub cboprovdir_KeyDown(KeyCode As Integer, Shift As Integer)
Call pSuprimir(cboprovdir, KeyCode)

End Sub

Private Sub cboprovnac_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboprovnac_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboprovnac_KeyDown(KeyCode As Integer, Shift As Integer)
Call pSuprimir(cboprovnac, KeyCode)

End Sub

Private Sub cboreludn_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboreludn_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cboreludn_KeyDown(KeyCode As Integer, Shift As Integer)
Call pSuprimir(cboreludn, KeyCode)

End Sub

Private Sub cbosexo_Change()
If blnDatosCargados Then
    blnCambiosPer = True
   cmdguardarper.Enabled = True
End If

End Sub

Private Sub cbosexo_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cbotratam_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub cbotratam_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub chkdir_Click()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub



Private Sub chkfall_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If
End Sub

Private Sub cmdBuscarCalle_Click()
    Dim vntData(0 To 2)
    Dim sql As String
Dim rs As rdoResultset
Dim strCodProv As String
Dim intTipPobla As Integer
blnCodPostal = True
strCodProv = cboprovdir.Columns(1).Value
    vntData(0) = strCodProv
    vntData(1) = UCase(Trim(txtlocalidaddir.Text))
    vntData(2) = UCase(Trim(txtcalle.Text))
    Call objPipe.PipeSet("CODPROVINCIA", vntData(0))
    Call objPipe.PipeSet("LOCALIDAD", vntData(1))
     Call objPipe.PipeSet("CALLE", vntData(2))
    frmCodigosPostales.Show vbModal
    Set frmCodigosPostales = Nothing
    If objPipe.PipeExist("CODPOSTAL") Then
        txtcodpostal.Text = objPipe.PipeGet("CODPOSTAL")
        Call objPipe.PipeRemove("CODPOSTAL")
    End If
    If objPipe.PipeExist("LOCALIDAD") Then
       txtlocalidaddir.Text = objPipe.PipeGet("LOCALIDAD")
        Call objPipe.PipeRemove("LOCALIDAD")
    End If
      If objPipe.PipeExist("CALLE") Then
        txtcalle.Text = objPipe.PipeGet("CALLE")
        Call objPipe.PipeRemove("CALLE")
    End If
    'blnCodPostal = False
End Sub

Private Sub cmdbuscarcodpostal_Click()
      Dim vntData(0 To 2)
    Dim sql As String
Dim rs As rdoResultset
Dim strCodProv As String
Dim strCodAyuda As Long
Dim intTipPobla As Integer
blnCodPostal = True
strCodProv = cboprovdir.Text
vntData(0) = strCodProv
vntData(1) = UCase(Trim(txtlocalidaddir.Text))
Call objPipe.PipeSet("CODPROVINCIA", vntData(0))
    
     
    Call objPipe.PipeSet("LOCALIDAD", vntData(1))
'    Call objPipe.PipeSet("Tip_Pobla", vntData(2))
    frmCodigosPostales.Show vbModal
    If objPipe.PipeExist("PROVINCIA") Then
        'sql = "SELECT CI26CODPROVI,CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
        'Call pcargarcombos(sql, cboprovdir)
        cboprovdir.MoveFirst
        strCodProv = objPipe.PipeGet("PROVINCIA")
        Do While cboprovdir.Columns(0).Value <> strCodProv
            strCodAyuda = cboprovdir.Columns(0).Value
            'lngCodProv = cboprovdir.Columns(0).Value
            cboprovdir.MoveNext
            If strCodAyuda = cboprovdir.Columns(0).Value Then Exit Do
        Loop
        cboprovdir.Text = cboprovdir.Columns(1).Value
        Call objPipe.PipeRemove("PROVINCIA")
    End If
'      If objPipe.PipeExist("CALLE") Then
    Set frmCodigosPostales = Nothing
    If objPipe.PipeExist("CODPOSTAL") Then
        txtcodpostal.Text = objPipe.PipeGet("CODPOSTAL")
        Call objPipe.PipeRemove("CODPOSTAL")
    End If
    If objPipe.PipeExist("LOCALIDAD") Then
       txtlocalidaddir.Text = objPipe.PipeGet("LOCALIDAD")
        Call objPipe.PipeRemove("LOCALIDAD")
    End If
   
'        txtcalle.Text = objPipe.PipeGet("CALLE")
'        Call objPipe.PipeRemove("CALLE")
'    End If
    
    'blnCodPostal = False

End Sub

Private Sub cmdbuscarper_Click()
Dim frm As Form
Dim intres As Integer
Dim blncargado As Boolean
Dim blnabort As Boolean
Dim vntData
vntData = 3
If blnDatosCargados Then
    If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        intres = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If intres = vbYes Then
            Call pGuardarPer(blnabort)
            If blnabort Then
                blnabort = False
                Exit Sub
            End If
            'Call pGuardarDir(lngCodPersonaFis, lngNumDir)
        End If 'de si quieren guardar los cambios
    End If
    blnCambiosDir = False
    blnDatosDirCargados = False
    blnDatosCargados = False
    blnCambiosPer = False
    cmdguardardir.Enabled = False
    cmdguardarper.Enabled = False
End If
''If Not validardir And Not fComprobaciones Then
''   If blnCambiosPer Or blnCambiosDir Then
''              intres = MsgBox("�Desea guardar los cambios?", vbQuestion + vbYesNo, Right(Right(Me.Caption, Len(Me.Caption) - 6), Len(Right(Me.Caption, Len(Me.Caption) - 6)) - 5))
''              If intres = vbYes Then
''                 cmdguardarper_Click
''                 Call pGuardardir(lngNumDir)
''              End If
''              blnCambiosDir = False
''              blnDatosDirCargados = False
''              blnDatosCargados = False
''              blnCambiosPer = False
''    End If
''    Else
''        Exit Sub
''End If
    
For Each frm In Forms
If frm.Name = "frmBuscaPersonas" Then
    blncargado = True
End If
Next
blnNuevaPer = False
If Not blncargado Then
    'Call objPipe.PipeSet("Tip_Res", vntdata)
    frmBuscaPersonas.Show vbModal
    Set frmBuscaPersonas = Nothing
Else
    Me.Hide
End If
If objPipe.PipeExist("AD_CodPersona") Or objPipe.PipeExist("AD_NumHistoria") Then
'si el buscador no devuelve nada no hay que borrar la pantalla ni volver a cargar los datos

     If objPipe.PipeExist("AD_CodPersona") Then
        lngCodPersonaFis = objPipe.PipeGet("AD_CodPersona")
        objPipe.PipeRemove ("AD_CodPersona")
    End If
    If objPipe.PipeExist("AD_NumHistoria") Then
        txthistoria.Text = objPipe.PipeGet("AD_NumHistoria")
        objPipe.PipeRemove ("AD_NumHistoria")
    End If
    Call plimpiar_controles

    Call pcargar_datos
End If

End Sub

Private Sub cmdeliminardir_Click()
Dim intres As Integer
If blnDatosDirCargados = False Then
    MsgBox "No existe Ninguna Persona", vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
Else
If chkdir.Value = 1 Then
    MsgBox "La direcci�n principal no puede ser eliminada", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
intres = MsgBox("�Est� seguro de eliminar los datos?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
If intres = vbYes Then
    sql = "DELETE FROM CI1000 WHERE CI21CODPERSONA=" & lngCodPersonaFis & " AND CI10NUMDIRECCI="
    If lngNumDir <> 0 Then
    sql = sql & lngNumDir
    Else
    sql = sql & rsdir!ci10numdirecci
    End If
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    Call seldir(lngCodPersonaFis)
    Call pdirecciones
End If
End If
End Sub

Private Sub cmdguardardir_Click()
If blnNuevaPer Then  'Or blnnuevo Then
        MsgBox "Debe guardar primero los datos de la persona", vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
        Exit Sub
End If
Call pGuardarDir(lngNumDir)
If Not blnsalir Then
Call seldir(lngCodPersonaFis)
    Call pdirecciones
End If
End Sub

Private Sub cmdguardarper_Click()
Call pGuardarPer(False)

End Sub

Private Sub cmdnuevodir_Click()
Dim sql As String
    txtresdir.Text = "" 'Se limpian los controles de direccion
    txtcodpostal.Text = ""
    txtlocalidaddir.Text = ""
    txtcalle.Text = ""
    txtportal.Text = ""
    txttfnodir.Text = ""
    txtobservaciones.Text = ""
    txtfax.Text = ""
    cbopaisdir.RemoveAll
    cbopaisdir.Text = ""
    cboprovdir.RemoveAll
    cboprovdir.Text = ""
    dcbofecini.Date = fAhora

    dcbofecfin.Date = ""
    chkdir.Value = 0
    'txtobservaciones.Text = rs
        
        sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900" 'Se cargan los combos de direcci�n
        Call pcargarcombos(sql, cbopaisdir)
     
        blnnuevadir = True
blnDatosDirCargados = True
blnCambiosDir = False
cmdguardardir.Enabled = False
cbopaisdir.SetFocus

End Sub

Private Sub cmdnuevoper_Click()
Dim intres As Integer
Dim blnpac As Boolean
Dim blnabort As Boolean
If blnDatosCargados Then
    If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        intres = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If intres = vbYes Then
            Call pGuardarPer(blnabort)
            If blnabort Then
                blnabort = False
                Exit Sub
            End If
            'Call pGuardarDir(lngCodPersonaFis, lngNumDir)
        End If 'de si quieren guardar los cambios
    End If
End If
    Call plimpiar_controles
    Call pselcombos
    cbopaisnac.Columns(0).Value = 45
    cbopaisnac.Text = "Espa�a"
    cbopaisdir.Columns(0).Value = 45
    cbopaisdir.Text = "Espa�a"
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovdir)
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovnac)
    dcbofecini.Date = fAhora
    txthistoria.Enabled = False
    txthistoria.BackColor = &HC0C0C0
    blnNuevaPer = True
    chkfall.Value = False
    chkfall.Enabled = True
    If lngCodPaciente <> 0 Then
     intres = MsgBox("Desea que el nuevo responsable tenga la misma direcci�n del paciente", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
     
     If intres = vbYes Then
           cmdguardardir.Enabled = False
           blnpac = True
           Call seldir(lngCodPaciente, blnpac)
           Call pdirecciones
           Call pllenardatosdir
        End If
    End If
    blnCambiosDir = False
    blnDatosDirCargados = True
    blnDatosCargados = True
    blnCambiosPer = False
    cmdguardardir.Enabled = False
    cmdguardarper.Enabled = False

    
    End Sub



Private Sub cmdSalir_Click()
blnsalir = True
Unload Me

End Sub

Private Sub dcbofecfall_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If
If dcbofecfall.Date = "" Then
    chkfall.Enabled = True
    chkfall.Value = 0
Else
    chkfall.Enabled = False
    chkfall.Value = 1
End If
End Sub

Private Sub dcbofecfall_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

If dcbofecfall.Date = "" Then
    chkfall.Enabled = True
    chkfall.Value = 0
Else
    chkfall.Enabled = False
    chkfall.Value = 1
End If

End Sub

Private Sub dcbofecfin_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub


Private Sub dcbofecfin_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub dcbofecini_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub



Private Sub dcbofecini_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub dcbofecnac_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If
End Sub


Private Sub dcbofecnac_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If
End Sub

Private Sub Form_Activate()
Dim sql As String
Dim intres As Integer
Dim blnpac As Boolean
Dim rs As rdoResultset
Dim rsPais As rdoResultset
Dim con As Control
If objPipe.PipeExist("Tip_Res") Then 'para saber si le han llamado desde p.f�sicas o desde responsables
    cmdnuevoper.Enabled = False
Else
    cmdnuevoper.Enabled = True
End If
If lngCodPersonaFis = 0 Then 'nuevo responsable
    txthistoria.Enabled = False
    blnNuevaPer = True
    cmdnuevoper.Enabled = True
    cmdbuscarper.Enabled = True
    cmdsalir.Enabled = True
    txthistoria.Enabled = False
    txthistoria.BackColor = &HC0C0C0
    If blnLlamada Then
        cmdbuscarper.Enabled = False
    End If
    Call pselcombos

    cbopaisnac.Columns(0).Value = 45
    cbopaisnac.Text = "Espa�a"
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovdir)
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovnac)

    dcbofecini.Date = fAhora
    vsDirec.Enabled = False
    blnDatosCargados = True
    blnDatosDirCargados = True
    If lngCodPaciente <> 0 And Not blnCodPostal Then
     intres = MsgBox("Desea que el nuevo responsable tenga la misma direcci�n del paciente", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
     
     If intres = vbYes Then
           cmdguardardir.Enabled = False
           blnpac = True
           Call seldir(lngCodPaciente, blnpac)
           Call pdirecciones
           Call pllenardatosdir
        Else
            cbopaisdir.Columns(0).Value = 45
            cbopaisdir.Text = "Espa�a"
            vsDirec.Enabled = False
     End If
    End If
Else
If blnLlamada Then
    cmdbuscarper.Enabled = False
End If
Call pcargar_datos 'ya viene cargados
End If

End Sub

Private Sub Form_Load()
Dim sql As String
Dim intres As Boolean
Dim blnpac As Boolean
Dim rs As rdoResultset
Dim rsPais As rdoResultset
If objPipe.PipeExist("PACIENTE") Then
  lngCodPaciente = objPipe.PipeGet("PACIENTE")
Call objPipe.PipeRemove("PACIENTE")
End If
If objPipe.PipeExist("RESP") Then
    lngCodPersonaFis = objPipe.PipeGet("RESP")
    Call objPipe.PipeRemove("RESP")
End If
If objPipe.PipeExist("LLAMADA") Then
    blnLlamada = objPipe.PipeGet("LLAMADA")
    Call objPipe.PipeRemove("LLAMADA")
End If
'If objPipe.PipeExist("TIP_RES") Then
'    Tip_Res = objPipe.PipeGet("TIP_RES")
'    Call objPipe.PipeRemove("TIP_RES")
'End If

''If lngCodPersonaFis <> 0 Then 'Or lngCodPaciente <> 0 Then 'PARA QUE SOLO CARGUE LOS DATOS DE EN CASO DE QUE EXISTA RESPONSABLE
''    sql = "select CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,"
''    sql = sql & " ci22numhistoria,CI2200.CI21CODPERSONA,"
''    sql = sql & " CI22DNI,CI22FECNACIM,CI22FECFALLE,"
''    sql = sql & " CI22NUMSEGSOC,CI22TFNOMOVIL,"
''    sql = sql & " CI2200.CI16CODLOCALID CODLOCNAC,CI22DESLOCALID DESLOCNAC,CI2200.CI26CODPROVI CODPROVNAC,CI22DESPROVI DESPROVNAC,"
''    sql = sql & " NAC.CI19DESPAIS PAISNAC,CI2200.CI19CODPAIS,CI2200.CI25CODPROFESI,CI25DESPROFESI,CI2200.CI28CODRELUDN,CI28DESRELUDN,"
''    sql = sql & " CI2200.CI34CODTRATAMI,CI34DESTRATAMI,CI2200.CI14CODESTCIVI,CI14DESESTCIVI,CI2200.CI30CODSEXO,CI30DESSEXO,"
''    sql = sql & " CI22OBSERVAC,"
''    sql = sql & " CI22IBMPROFESION,CI22IBMRELUDN,"
''    sql = sql & " CI22IBMRFPRIAPEL,CI22IBMRFSEGAPEL,CI22IBMRFNOMBRE,"
''    sql = sql & " CI22IBMRFDIRECC,"
''    sql = sql & " CI22IBMRFPOBLAC,CI26CODPROVI_IRF,"
''    sql = sql & " CI19CODPAIS_IRF,CI22IBMRFTELEFO"
''    sql = sql & " From"
''    sql = sql & " CI2200,CI2100,CI2600,"
''    sql = sql & " CI1900 NAC,CI2500,CI2800,"
''    sql = sql & " CI3400,CI1400,CI3000,"
''    sql = sql & " CI3500"
''    sql = sql & " Where"
''    sql = sql & " CI2200.CI26CODPROVI=CI2600.CI26CODPROVI(+)"
''    sql = sql & " AND CI2200.CI19CODPAIS=NAC.CI19CODPAIS(+)"
''    sql = sql & " AND CI2200.CI28CODRELUDN=CI2800.CI28CODRELUDN(+)"
''    sql = sql & " AND CI2200.CI34CODTRATAMI=CI3400.CI34CODTRATAMI(+)"
''    sql = sql & " AND CI2200.CI14CODESTCIVI=CI1400.CI14CODESTCIVI(+)"
''    sql = sql & " AND CI2200.CI25CODPROFESI=CI2500.CI25CODPROFESI(+)"
''    sql = sql & " AND CI2200.CI30CODSEXO=CI3000.CI30CODSEXO"
''   ' sql = sql & " AND CI2200.CI21CODPERSONA=CI1000.CI21CODPERSONA"
''    'sql = sql & " AND CI10INDDIRPRINC=-1"
''    'sql = sql & " AND DIR.CI19CODPAIS=CI1000.CI19CODPAIS"
''    sql = sql & " AND CI2200.CI21CODPERSONA=CI2100.CI21CODPERSONA"
''    If lngCodPersonaFis <> 0 Then 'cargar los datos en funcion del c�digo de persona
''         sql = sql & " AND CI2200.CI21CODPERSONA=" & lngCodPersonaFis
''    Else
''    If lngCodPaciente <> 0 Then
''        sql = sql & " AND CI2200.CI21CODPERSONA=" & lngCodPaciente
''    End If
''    End If
''    Set rs = objApp.rdoConnect.OpenResultset(sql)
'''AHORA HAY QUE DISTINGUIR SI SE CARGAN LOS DATOS DEL RESPONSABLE O LOS DEL IBM
''    Call pselcombos
''    If lngCodPaciente <> 0 And lngCodPersonaFis = 0 Then 'DATOS DEL IBM
''    If blnnuevo Then
''        Exit Sub
''    End If
''    blnibm = True
''    Call pibm
''    'hay que desabilitar todos los controles y poner el fondo en gris
''
''        lngCodPersonaFis = lngCodPaciente
''        If Not IsNull(rs!CI22IBMRFNOMBRE) Then
''            txtnombre.Text = rs!CI22IBMRFNOMBRE
''        End If
''        If Not IsNull(rs!CI22IBMRFPRIAPEL) Then
''             txtape1.Text = rs!CI22IBMRFPRIAPEL
''         End If
''         If Not IsNull(rs!CI22IBMRFSEGAPEL) Then
''             txtape2.Text = rs!CI22IBMRFSEGAPEL
''         End If
''        If Not IsNull(rs!CI22IBMRELUDN) Then
''                cboreludn.Text = rs!CI22IBMRELUDN
''        End If
''        If Not IsNull(rs!CI22IBMPROFESION) Then
''            cboprofesion.Text = rs!CI22IBMPROFESION
''        End If
''        If Not IsNull(rs!CI19CODPAIS_IRF) Then
''        sql = "SELECT CI19DESPAIS FROM CI1900 WHERE CI19CODPAIS=" & rs!CI19CODPAIS_IRF
''        Set rsPais = objApp.rdoConnect.OpenResultset(sql)
''            cbopaisnac.Text = rsPais!CI19DESPAIS
''            cbopaisnac.Columns(0).Value = rs!CI19CODPAIS_IRF
''        End If
''         If cbopaisnac.Columns(0).Value = "45" Then
''         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600"
''         Call pcargarcombos(sql, cboprovnac)
''        'End If
''        If Not IsNull(rs!CI26CODPROVI_IRF) Then
''        sql = "SELECT CI26DESPROVI FROM CI2600 WHERE CI26CODPROVI=" & rs!CI26CODPROVI_IRF
''        Set rsPais = objApp.rdoConnect.OpenResultset(sql)
''            cboprovnac.Text = rsPais!CI26DESPROVI
''            cboprovnac.Columns(0).Value = rs!CI26CODPROVI_IRF
''        End If
''        End If
''    Else 'PARA AQUELLOS QUE SI QUE TIENEN CODIGO DE RESPONSABLE
''        lngCodPersonaFis = rs!CI21CODPERSONA
''        cbosexo.Columns(0).Value = rs!CI30CODSEXO
''        cbosexo.Text = rs!CI30DESSEXO
''        txtnombre.Text = rs!CI22NOMBRE
''        If Not IsNull(rs!CI22PRIAPEL) Then
''            txtape1.Text = rs!CI22PRIAPEL
''        End If
''        If Not IsNull(rs!CI22SEGAPEL) Then
''            txtape2.Text = rs!CI22SEGAPEL
''        End If
''        If Not IsNull(rs!CI34DESTRATAMI) Then
''            cbotratam.Text = rs!CI34DESTRATAMI
''            cbotratam.Columns(0).Value = rs!CI34CODTRATAMI
'''            Else
'''            cbotratam.Columns(0).Value = 0
''        End If
''        If Not IsNull(rs!CI14DESESTCIVI) Then
''            cboestcivil.Text = rs!CI14DESESTCIVI
''            cboestcivil.Columns(0).Value = rs!CI14CODESTCIVI
'''            Else
'''            cboestcivil.Columns(0).Value = 0
''        End If
''        If Not IsNull(rs!CI28DESRELUDN) Then
''            cboreludn.Text = rs!CI28DESRELUDN
''            cboreludn.Columns(0).Value = rs!CI28CODRELUDN
'''            Else
'''            cboreludn.Columns(0).Value = 0
''        End If
''            If Not IsNull(rs!CI22DNI) Then
''                txtdni.Text = rs!CI22DNI
''            End If
''            If Not IsNull(rs!CI22NUMSEGSOC) Then
''             txtsspro.Text = Left(rs!CI22NUMSEGSOC, 2)
''             txtssnum.Text = Mid(rs!CI22NUMSEGSOC, 3, 8)
''              If Len(rs!CI22NUMSEGSOC) > 10 Then
''                txtssnum2.Text = Right(rs!CI22NUMSEGSOC, 2)
''              Else
''                txtssnum2.Text = ""
''              End If
''            End If
''            If Not IsNull(rs!CI22TFNOMOVIL) Then
''                txtmovil.Text = rs!CI22TFNOMOVIL
''            End If
''            If Not IsNull(rs!CI22OBSERVAC) Then
''                txtobser.Text = rs!CI22OBSERVAC
''            End If
''            If Not IsNull(rs!CI25DESPROFESI) Then
''                cboprofesion.Text = rs!CI25DESPROFESI
''                cboprofesion.Columns(0).Value = rs!CI25CODPROFESI
'''                Else
'''                cboprofesion.Columns(0).Value = 0
''            End If
''            If Not IsNull(rs!PAISNAC) Then
''                cbopaisnac.Text = rs!PAISNAC
''                cbopaisnac.Columns(0).Value = rs!CI19CODPAIS
'''                Else
'''                cbopaisnac.Columns(0).Value = 0
''            End If
''            If cbopaisnac.Columns(0).Value = "45" Then
''                sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600"
''                Call pcargarcombos(sql, cboprovnac)
''            End If
''            If Not IsNull(rs!DESPROVNAC) Then
''                    cboprovnac.Text = rs!DESPROVNAC
''                    cboprovnac.Columns(0).Value = rs!CI26CODPROVNAC
'''                    Else
'''                    cboprovnac.Columns(0).Value = 0
''            End If
''            If Not IsNull(rs!CI22FECFALLE) Then
''                dcbofecfall.Date = Format(rs!CI22FECFALLE, "Short Date")
''            Else
''                dcbofecfall.Date = ""
''            End If
''            If Not IsNull(rs!CI22FECNACIM) Then
''                dcbofecnac.Date = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
''            End If
''            If Not IsNull(rs!DESLOCNAC) Then
''                txtlocalidadnac.Text = rs!DESLOCNAC
''            End If
''    End If
''End If
'' Call seldir(lngCodPersonaFis)
''        Call pdirecciones
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim vntData
Dim intres As Integer
Dim blnabort As Boolean
If txtnombre.Text = "" Then
    blnsalir = False
    Exit Sub
Else
If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        blnsalir = True 'se va a cambiar de persona, luego no hace falta volver a seleccionar las direcciones
        intres = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If intres = vbYes Then
            Call pGuardarPer(blnabort)
            If blnabort Then
                blnabort = False
                Cancel = True
                Exit Sub
            End If
            'Call pGuardarDir(lngCodPersonaFis, lngNumDir)
        End If 'de si quieren guardar los cambios
    End If
    blnCambiosDir = False
    blnDatosDirCargados = False
    blnDatosCargados = False
    blnCambiosPer = False
    cmdguardardir.Enabled = False
    cmdguardarper.Enabled = False
End If
If lngCodPersonaFis <> 0 Then
    vntData = lngCodPersonaFis
    Call objPipe.PipeSet("AD_Codpersona", vntData)
End If

''If blnibm Then
'' blnibm = False
'' Exit Sub
'     res = MsgBox("Los datos de esta persona provienen del IBM. �Desea guardar sus datos en el nuevo formato?", vbYesNo + vbInformation, Right(Me.Caption, Len(Me.Caption) - 6))
'    If res = vbYes Then
'        blnNuevaPer = True
'        If Not fComprobaciones Then
'            cmdguardarper_Click
'        Else: Cancel = True
'                Exit Sub
'        End If
'        If validardir Then Cancel = True
'    End If
''Else
'''    If txtnombre.Text = "" Then
'''            blnsalir = False
'''            Cancel = False
'''            Exit Sub
'''    End If
'''    If Not fComprobaciones And Not validardir Then
'''              If blnCambiosPer Or blnCambiosDir Then
'''              intres = MsgBox("�Desea guardar los cambios?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
'''              If intres = vbYes Then
'''                 cmdguardarper_Click
'''                 Call pGuardardir(lngNumDir)
'''              End If
'''              End If
'''            Else:
'''                Cancel = True
'''                blnsalir = False
'''                Exit Sub
'''        End If
'''        If validardir Then Cancel = True
'''''End If
'''blnsalir = False
'''
'''
'''
'''
'''
'''

End Sub

Private Sub txtape1_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtape1_GotFocus()
txtape1.SelStart = 0
txtape1.SelLength = Len(Trim(txtape1.Text))
End Sub

Private Sub txtape2_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtape2_GotFocus()
txtape2.SelStart = 0
txtape2.SelLength = Len(Trim(txtape2.Text))

End Sub

Private Sub txtcalle_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtcalle_GotFocus()
txtcalle.SelStart = 0
txtcalle.SelLength = Len(Trim(txtcalle.Text))

End Sub

Private Sub txtcodpostal_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtcodpostal_GotFocus()
txtcodpostal.SelStart = 0
txtcodpostal.SelLength = Len(Trim(txtcodpostal.Text))

End Sub

Private Sub txtdni_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtdni_GotFocus()
txtdni.SelStart = 0
txtdni.SelLength = Len(Trim(txtdni.Text))

End Sub

Private Sub txtempresa_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtempresa_GotFocus()
txtempresa.SelStart = 0
txtempresa.SelLength = Len(Trim(txtempresa.Text))

End Sub

Private Sub txtfax_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtfax_GotFocus()
txtfax.SelStart = 0
txtfax.SelLength = Len(Trim(txtfax.Text))

End Sub

Private Sub txtlocalidaddir_Change()
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
If cboprovdir.Text <> "" And cbopaisdir.Columns(0).Value = "45" Then
        cmdbuscarcodpostal.Enabled = True
        If Trim(txtlocalidaddir.Text) <> "" Then
            cmdBuscarCalle.Enabled = True
        End If
End If

End Sub

Private Sub txtlocalidaddir_GotFocus()
  
txtlocalidaddir.SelStart = 0
txtlocalidaddir.SelLength = Len(Trim(txtlocalidaddir.Text))

End Sub

Private Sub txtlocalidadnac_Change()
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtlocalidadnac_GotFocus()
txtlocalidadnac.SelStart = 0
txtlocalidadnac.SelLength = Len(Trim(txtlocalidadnac.Text))

End Sub

Private Sub txtmovil_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtmovil_GotFocus()
txtmovil.SelStart = 0
txtmovil.SelLength = Len(Trim(txtmovil.Text))

End Sub

Private Sub txtnombre_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtnombre_GotFocus()
txtnombre.SelStart = 0
txtnombre.SelLength = Len(Trim(txtnombre.Text))

End Sub

Private Sub txtobser_Change()
 If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If
End Sub

Private Sub txtobser_GotFocus()
txtobser.SelStart = 0
txtobser.SelLength = Len(Trim(txtobser.Text))

End Sub

Private Sub txtobservaciones_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtobservaciones_GotFocus()
txtobservaciones.SelStart = 0
txtobservaciones.SelLength = Len(Trim(txtobservaciones.Text))

End Sub

Private Sub txtportal_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtportal_GotFocus()
txtportal.SelStart = 0
txtportal.SelLength = Len(Trim(txtportal.Text))

End Sub

Private Sub txtpuesto_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtpuesto_GotFocus()
txtpuesto.SelStart = 0
txtpuesto.SelLength = Len(Trim(txtpuesto.Text))

End Sub

Private Sub txtresdir_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtresdir_GotFocus()
txtresdir.SelStart = 0
txtresdir.SelLength = Len(Trim(txtresdir.Text))

End Sub

Private Sub txtssnum_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtssnum_GotFocus()
txtssnum.SelStart = 0
txtssnum.SelLength = Len(Trim(txtssnum.Text))

End Sub

Private Sub txtssnum2_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtssnum2_GotFocus()
txtssnum2.SelStart = 0
txtssnum2.SelLength = Len(Trim(txtssnum2.Text))

End Sub

Private Sub txtsspro_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtsspro_GotFocus()
txtsspro.SelStart = 0
txtsspro.SelLength = Len(Trim(txtsspro.Text))

End Sub

Private Sub txttfnodir_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub txttfnodir_GotFocus()
txttfnodir.SelStart = 0
txttfnodir.SelLength = Len(Trim(txttfnodir.Text))

End Sub

Private Sub txttfnotrab_Change()
 If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txttfnotrab_GotFocus()
txttfnotrab.SelStart = 0
txttfnotrab.SelLength = Len(Trim(txttfnotrab.Text))

End Sub

Private Sub vsDirec_Change()
Dim intres As Integer
If blnCambiosDir Then
    intres = MsgBox("�Desea guardar los cambios?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
    If intres = vbYes Then
       ' blnCambiosDir = True
        'blndatosdircargado
        'lngDire = lngNumDir
        Call pGuardarDir(lngNumDir)
        Call seldir(lngCodPersonaFis)
        blnCambiosDir = False
        cmdguardardir.Enabled = False
        Call pdirecciones(vsDirec.Value)
        
    End If
End If
blnDatosDirCargados = False
blnCambiosDir = False
cmdguardardir.Enabled = False
  If vsDirec.Value = vsDirec.Max Then
    rsdir.MoveLast
  Else
    rsdir.Move vsDirec.Value, 1
  End If
  Call pllenardatosdir

End Sub

