VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmCitasPaciente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citas del Paciente"
   ClientHeight    =   8280
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11550
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8280
   ScaleWidth      =   11550
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdImpCita 
      Caption         =   "&Imprimir Cita"
      Height          =   375
      Left            =   9000
      TabIndex        =   30
      Top             =   7800
      Width           =   1155
   End
   Begin VB.CommandButton cmdHF 
      Caption         =   "&Hoja Filiacion"
      Height          =   375
      Left            =   10200
      TabIndex        =   29
      Top             =   7800
      Width           =   1155
   End
   Begin VsOcxLib.VideoSoftAwk awkAct 
      Left            =   60
      Top             =   60
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdAnularPet 
      Caption         =   "&Anular Petici�n"
      Height          =   375
      Left            =   3840
      TabIndex        =   28
      Top             =   7800
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Ver"
      ForeColor       =   &H00800000&
      Height          =   1140
      Left            =   8760
      TabIndex        =   24
      Top             =   75
      Width           =   1605
      Begin VB.OptionButton optNoCit 
         Caption         =   "No Citables"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   720
         Width           =   1335
      End
      Begin VB.OptionButton optCitables 
         Caption         =   "Citables"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton optTodas 
         Caption         =   "Todas"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&OverBooking"
      Height          =   375
      Index           =   1
      Left            =   7740
      TabIndex        =   23
      Top             =   7800
      Width           =   1215
   End
   Begin VB.CommandButton cmdavisos 
      Height          =   375
      Left            =   10680
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   720
      Width           =   675
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10560
      TabIndex        =   16
      Top             =   150
      Width           =   870
   End
   Begin VB.CommandButton cmdAnularPrueba 
      Caption         =   "Anular &Prueba"
      Height          =   375
      Left            =   2580
      TabIndex        =   15
      Top             =   7800
      Width           =   1215
   End
   Begin VB.Frame Frame4 
      Caption         =   "Paciente"
      ForeColor       =   &H00800000&
      Height          =   1110
      Left            =   150
      TabIndex        =   10
      Top             =   75
      Width           =   5820
      Begin VB.TextBox txtFNacim 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   2775
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   225
         Width           =   1200
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   675
         Width           =   4575
      End
      Begin VB.TextBox txtHistoria 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   960
         TabIndex        =   0
         Top             =   240
         Width           =   825
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4575
         TabIndex        =   11
         Top             =   225
         Width           =   990
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "F. Nacim:"
         Height          =   195
         Index           =   1
         Left            =   2025
         TabIndex        =   22
         Top             =   300
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Paciente:"
         Height          =   195
         Left            =   225
         TabIndex        =   14
         Top             =   750
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Historia:"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   13
         Top             =   315
         Width           =   570
      End
   End
   Begin VB.CommandButton cmdDejarPend 
      Caption         =   "&Dejar Pdte"
      Height          =   375
      Left            =   6540
      TabIndex        =   9
      Top             =   7800
      Width           =   1155
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   7800
      Width           =   1215
   End
   Begin VB.CommandButton cmdIndSanit 
      Caption         =   "I&nd Sanitarios"
      Height          =   375
      Left            =   5220
      TabIndex        =   7
      Top             =   7800
      Width           =   1275
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "Da&tos Act."
      Height          =   375
      Left            =   1380
      TabIndex        =   6
      Top             =   7800
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Listados"
      ForeColor       =   &H00800000&
      Height          =   1140
      Left            =   6000
      TabIndex        =   1
      Top             =   75
      Width           =   2685
      Begin VB.CheckBox chkPet 
         Caption         =   "Peticion"
         Height          =   255
         Left            =   225
         TabIndex        =   5
         Top             =   225
         Width           =   975
      End
      Begin VB.CheckBox chkPac 
         Caption         =   "Prog. Paciente"
         Height          =   255
         Left            =   225
         TabIndex        =   4
         Top             =   525
         Width           =   1455
      End
      Begin VB.CheckBox chkDpto 
         Caption         =   "Prog. Dpto"
         Height          =   255
         Left            =   225
         TabIndex        =   3
         Top             =   825
         Width           =   1095
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "I&mprimir"
         Height          =   375
         Left            =   1680
         TabIndex        =   2
         Top             =   675
         Width           =   885
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdCitadas 
      Height          =   2250
      Left            =   150
      TabIndex        =   17
      Top             =   3825
      Width           =   11235
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   19817
      _ExtentY        =   3969
      _StockProps     =   79
      Caption         =   "Citadas"
      ForeColor       =   8388608
      BackColor       =   12632256
   End
   Begin SSDataWidgets_B.SSDBGrid grdPendientes 
      Height          =   1575
      Left            =   150
      TabIndex        =   18
      Top             =   6150
      Width           =   11220
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   19791
      _ExtentY        =   2778
      _StockProps     =   79
      Caption         =   "Pendientes de recitar"
      ForeColor       =   8388608
      BackColor       =   12632256
   End
   Begin SSDataWidgets_B.SSDBGrid grdSinCitar 
      Height          =   2475
      Left            =   150
      TabIndex        =   19
      Top             =   1275
      Width           =   11220
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   19791
      _ExtentY        =   4366
      _StockProps     =   79
      Caption         =   "Sin Citar"
      ForeColor       =   8388608
      BackColor       =   12632256
   End
End
Attribute VB_Name = "frmCitasPaciente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim blnDatosCargados As Boolean
Dim strHistoria_Ant As String
Public nPers As Long
Dim strMeCaption$

Private Sub pLimpiar()
'txtHistoria.Text = ""
txtPaciente.Text = ""
grdCitadas.RemoveAll
grdSinCitar.RemoveAll
grdPendientes.RemoveAll
End Sub

Public Sub pMensajes()
Dim sql As String
Dim rsMens As rdoResultset
Dim qry As rdoQuery
Dim imgX As ListImage

   sql = "SELECT count(*) from  CI0400 WHERE CI21CodPersona = ?" _
   & " and (ci04FecCadMens >= trunc(sysdate+1)" _
   & " Or ci04FecCadMens Is Null)"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
     If nPers <> 0 Then
        qry(0) = nPers
     Else
        qry(0) = 0
     End If
     Set rsMens = qry.OpenResultset()
     If rsMens(0) > 0 Then
       Set imgX = objApp.imlImageList.ListImages("i44")
       cmdavisos.Picture = imgX.Picture
     Else
       Set imgX = objApp.imlImageList.ListImages("i40")
       cmdavisos.Picture = imgX.Picture
     End If
End Sub
Private Sub pImprimir(ssgrid1 As SSDBGrid)
Dim strWhere As String
Dim i As Integer
    For i = 0 To ssgrid1.SelBookmarks.Count - 1
        strWhere = strWhere & "PR04NUMACTPLAN= " & Val(ssgrid1.Columns("NumActPlan").CellText(ssgrid1.SelBookmarks(i))) & " OR "
    Next i
    strWhere = Left$(strWhere, Len(strWhere) - 4)
    If ssgrid1.SelBookmarks.Count > 0 Then
        Call Imprimir_API(strWhere, "Peticion.rpt")
    Else
        Call Imprimir_API(strWhere, "PetAgrup.rpt")
    End If
End Sub

Private Function fGrid() As SSDBGrid 'para saber en que grid esta la actuaci�n
  
  If grdPendientes.SelBookmarks.Count > 0 Then
    Set fGrid = grdPendientes
    Exit Function
  End If
  
  If grdCitadas.SelBookmarks.Count > 0 Then
    Set fGrid = grdCitadas
    Exit Function
  End If
    
  If grdSinCitar.SelBookmarks.Count > 0 Then
    Set fGrid = grdSinCitar
    Exit Function
  End If
      
  MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
  Set fGrid = Nothing

End Function

Private Function fValidarEstado(nprs As String, Optional texto As String) As Integer
Dim sql As String
', texto As String
Dim rdo As rdoResultset
  
' Se chequea que las pruebas no tengan fecha de entrada en cola
  sql = "SELECT PR0100.PR01DesCorta " _
      & "FROM PR0100, PR0400 WHERE " _
      & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0400.PR04NumActPlan IN (" & nprs & ") AND " _
      & "PR0400.PR04FecEntrCola IS NOT NULL"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    texto = texto & Chr$(13) & rdo(0)
    rdo.MoveNext
  Wend
  If texto = "" Then
    fValidarEstado = True
  Else
    fValidarEstado = False
'    MsgBox "Las siguientes pruebas tienen fecha de entrada en cola: " & Chr$(13) & texto _
'        & Chr$(13) & Chr$(13) & "Debe seleccionar pruebas v�lidas.", vbInformation, Me.Caption
  End If

End Function
Public Sub pPendientes(intCol As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim sqlOrder As String
Dim qry As rdoQuery
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY CI0100.CI01FECCONCERT" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdCitadas.Columns(intCol).Caption
        Case "Fecha Cita": sqlOrder = " ORDER BY CI0100.CI01FECCONCERT"
        Case "Fecha Cancelaci�n": sqlOrder = " ORDER BY CI0100.CI01FECUPD"
        Case "Actuacion": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
        Case "Recurso": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
        Case "Dpto Realizador": sqlOrder = " ORDER BY DESDPTOREA"
        Case "Dpto Solicitante": sqlOrder = " ORDER BY DESDPTOSOL"
        Case "Doctor Solicitante": sqlOrder = " ORDER BY DOCTOR"
        Case Else: Exit Sub
        End Select
End If
        sql = "SELECT TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YY HH24:MI') FECHA,"
        sql = sql & "TO_CHAR(CI0100.CI01FECUPD,'DD/MM/YY HH24:MI') FECCANCEL,"
        sql = sql & "PR0100.PR01DESCORTA,"
        sql = sql & "AG1100.AG11DESRECURSO,"
        sql = sql & "AD02REL.AD02DESDPTO DESDPTOREA,"
        sql = sql & "AD02SOL.AD02DESDPTO DESDPTOSOL,"
        sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
        sql = sql & "PR0300.PR01CODACTUACION,"
        sql = sql & "PR0300.AD02CODDPTO CODDPTOREA,"
        sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL, "
        sql = sql & "PR0400.PR37CODESTADO,"
        sql = sql & "PR0400.PR04NUMACTPLAN,"
        sql = sql & "CI0100.AG11CODRECURSO,"
        sql = sql & "PR0100.PR12CODACTIVIDAD,"
        sql = sql & "PR0900.PR09NUMPETICION,"
        sql = sql & "CI0100.CI31NUMSOLICIT," 'eSTOS CAMPOS TAMPOCO SE PARA QUE SIRVEN
        sql = sql & "CI0100.CI01NUMCITA,"
        sql = sql & "CI0100.AD15CODCAMA,"
        sql = sql & "PR0300.PR03NUMACTPEDI"
        sql = sql & " FROM PR0400,PR0300,CI0100,PR0900,"
        sql = sql & "SG0200,PR0100,AD0200 AD02REL,AG1100,AD0200 AD02SOL"
        sql = sql & " WHERE CI0100.CI01SITCITA=?"
        sql = sql & "  AND PR0400.CI21CODPERSONA = ?"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION(+)"
        sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD"
        sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
        sql = sql & " AND PR0300.AD02CODDPTO=AD02REL.AD02CODDPTO"
        sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
        sql = sql & " AND CI0100.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
        'sql = sql & " ORDER BY PR0400.PR04FECPLANIFIC"
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = constESTCITA_PENDRECITAR
        qry(1) = nPers
        Set rs = qry.OpenResultset(sql)
        Call pCargarGrid(grdPendientes, rs)
        rs.Close
        qry.Close

End Sub


Public Sub pCitadas(intCol As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim sqlOrder As String
Dim qry As rdoQuery
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY CI0100.CI01FECCONCERT" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdCitadas.Columns(intCol).Caption
        Case "Fecha Cita": sqlOrder = " ORDER BY CI0100.CI01FECCONCERT"
        Case "Actuacion": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
        Case "Recurso": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
        Case "Dpto Realizador": sqlOrder = " ORDER BY DESDPTOREA"
        Case "Dpto Solicitante": sqlOrder = " ORDER BY DESDPTOSOL"
        Case "Doctor Solicitante": sqlOrder = " ORDER BY DOCTOR"
        Case Else: Exit Sub
        End Select
End If

  sql = "SELECT TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YY HH24:MI') FECHA,PR0100.PR01DESCORTA,"
        sql = sql & "AG1100.AG11DESRECURSO,"
        sql = sql & "AD02REA.AD02DESDPTO DESDPTOREA,AD02SOL.AD02DESDPTO DESDPTOSOL,"
        sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
        sql = sql & "PR0300.PR01CODACTUACION,"
        sql = sql & "PR0400.AD02CODDPTO CODDPTOREA,"
        sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL,"
        sql = sql & "PR0400.PR37CODESTADO,"
        sql = sql & "PR0400.PR04NUMACTPLAN,CI0100.AG11CODRECURSO,"
        sql = sql & "PR0100.PR12CODACTIVIDAD,PR0900.PR09NUMPETICION,"
        sql = sql & "CI0100.CI31NUMSOLICIT," 'Estos dos campos no tengo muy claro que hagan falta
        sql = sql & "CI0100.CI01NUMCITA,"
        sql = sql & "CI0100.AD15CODCAMA"
        sql = sql & " FROM PR0400,PR0300,CI0100,PR0900,SG0200,  "
        sql = sql & " PR0100,AD0200 AD02REA,AG1100,AD0200 AD02SOL"
        sql = sql & " WHERE CI0100.CI01SITCITA=?"
        sql = sql & " AND PR0400.PR37CODESTADO =?"
        sql = sql & " AND PR0400.CI21CODPERSONA = ?"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION "
        sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD"
        sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
        sql = sql & " AND PR0300.AD02CODDPTO=AD02REA.AD02CODDPTO"
        sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
        sql = sql & " AND CI0100.AG11CODRECURSO=AG1100.AG11CODRECURSO "
        'sql = sql & " ORDER BY CI0100.CI01FECCONCERT "
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = constESTCITA_CITADA
        qry(1) = constESTACT_CITADA
        qry(2) = nPers
        Set rs = qry.OpenResultset(sql)
        Call pCargarGrid(grdCitadas, rs)
        rs.Close
        qry.Close
End Sub
Public Sub pSinCitar(intCol As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim sqlOrder As String
Dim qry As rdoQuery
 
  If intCol = -1 Then
    If sqlOrder = "" Then sqlOrder = " ORDER BY PR0400.PR04FECPLANIFIC" 'Opci�n por defecto: Fecha Planificacion
  Else
    Select Case grdSinCitar.Columns(intCol).Caption
    Case "Fecha Planificaci�n": sqlOrder = " ORDER BY PR0400.PR04FECPLANIFIC"
    Case "Actuacion": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
    Case "Recurso Preferente": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
    Case "Dpto Realizador": sqlOrder = " ORDER BY DESDPTOREA"
    Case "Dpto Solicitante": sqlOrder = " ORDER BY DESDPTOSOL"
    Case "Doctor Solicitante": sqlOrder = " ORDER BY DOCTOR"
    Case Else: Exit Sub
    End Select
  End If

  sql = "SELECT"
  sql = sql & " TO_CHAR(PR0400.PR04FECPLANIFIC,'DD/MM/YY HH24:MI') FECHA,"
  sql = sql & "PR0400.PR04FECPLANIFIC FECHAORDEN,"
  sql = sql & "PR0100.PR01DESCORTA,"
  sql = sql & "AG1100.AG11DESRECURSO,"
  sql = sql & "AD02REA.AD02DESDPTO DESDPTOREA,"
  sql = sql & "AD02SOL.AD02DESDPTO DESDPTOSOL,"
  sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
  sql = sql & "PR0300.PR03INDCITABLE,"
  sql = sql & "PR0300.PR01CODACTUACION,"
  sql = sql & "PR0400.AD02CODDPTO CODDPTOREA,"
  sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL,"
  sql = sql & "PR0400.PR37CODESTADO,"
  sql = sql & "PR0400.PR04NUMACTPLAN,"
  sql = sql & "PR0100.PR12CODACTIVIDAD,"
  sql = sql & "PR0900.PR09NUMPETICION,"
  sql = sql & "PR1400.AG11CODRECURSO"
  sql = sql & " FROM"
  sql = sql & " PR0400, PR0300, PR0100, CI0100, AG1100, PR0900, SG0200, AD0200 AD02REA,"
  sql = sql & " AD0200 AD02SOL, PR1400"
  sql = sql & " WHERE PR0400.PR37CODESTADO =?"
  sql = sql & " AND PR0400.CI21CODPERSONA =?"
  sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
  sql = sql & " AND PR0300.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
  sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION"
  sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD"
  sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
  sql = sql & " AND PR0400.AD02CODDPTO=AD02REA.AD02CODDPTO"
  sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
  sql = sql & " AND PR1400.PR14INDRECPREFE = -1"
  If optCitables.Value = True Then
    sql = sql & " AND PR0300.PR03INDCITABLE = -1"
  ElseIf optNoCit.Value = True Then
    sql = sql & " AND PR0300.PR03INDCITABLE = 0"
  End If
  sql = sql & " AND PR1400.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
  sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
  sql = sql & " AND CI0100.CI01SITCITA IS NULL"
  sql = sql & " AND PR0400.PR04FecEntrCola IS NULL"
  sql = sql & " UNION"
  sql = sql & " SELECT"
  sql = sql & "  TO_CHAR(PR0400.PR04FECPLANIFIC,'DD/MM/YY HH24:MI') FECHA,"
  sql = sql & " PR0400.PR04FECPLANIFIC FECHAORDEN,"
  sql = sql & " PR0100.PR01DESCORTA,"
  sql = sql & " AG1100.AG11DESRECURSO,"
  sql = sql & " AD02REA.AD02DESDPTO DESDPTOREA,"
  sql = sql & " AD02SOL.AD02DESDPTO DESDPTOSOL,"
  sql = sql & " NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
  sql = sql & " PR0300.PR03INDCITABLE,"
  sql = sql & " PR0300.PR01CODACTUACION,"
  sql = sql & " PR0400.AD02CODDPTO CODDPTOREA,"
  sql = sql & " PR0900.AD02CODDPTO CODDPTOSOL,"
  sql = sql & " PR0400.PR37CODESTADO,"
  sql = sql & " PR0400.PR04NUMACTPLAN,"
  sql = sql & " PR0100.PR12CODACTIVIDAD,"
  sql = sql & " PR0900.PR09NUMPETICION ,"
  sql = sql & " PR1400.AG11CODRECURSO"
  sql = sql & " From"
  sql = sql & " PR0400, PR0300, PR0100,"
  sql = sql & " AG1100, PR0900, SG0200,AD0200 AD02REA,"
  sql = sql & " AD0200 AD02SOL, PR1400"
  sql = sql & " Where PR0400.PR37CODESTADO =?"
  sql = sql & " AND PR0400.CI21CODPERSONA =?"
  sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
  sql = sql & " AND PR0300.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
  sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION"
  sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD(+)"
  sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
  sql = sql & " AND PR0400.AD02CODDPTO=AD02REA.AD02CODDPTO"
  sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
  sql = sql & " AND PR1400.PR14INDRECPREFE = 0 AND PR0300.PR03INDCITABLE = 0"
  sql = sql & " AND PR1400.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
  sql = sql & " AND PR0400.PR04FecEntrCola IS NULL"
  sql = sql & " ORDER BY FECHAORDEN"

'************************************************
'Esta es la select de antes, no borrar
'**************************************

' sql = "SELECT  TO_CHAR(PR0400.PR04FECPLANIFIC,'DD/MM/YYYY HH24:MI') FECHA,PR0100.PR01DESCORTA,"
'        sql = sql & "AG1100.AG11DESRECURSO,"
'        sql = sql & "AD02REA.AD02DESDPTO DESDPTOREA,"
'        sql = sql & "AD02SOL.AD02DESDPTO DESDPTOSOL,"
'        sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
'        sql = sql & "PR0300.PR03INDCITABLE," 'HASTA AQU� SON LOS CAMPOS VISIBLES
'        sql = sql & "PR0300.PR01CODACTUACION,"
'        sql = sql & "PR0400.AD02CODDPTO CODDPTOREA,"
'        sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL,"
'        sql = sql & "PR0400.PR37CODESTADO,"
'        sql = sql & "PR0400.PR04NUMACTPLAN,"
'        sql = sql & "PR1400.AG11CODRECURSO,"
'        sql = sql & "PR0100.PR12CODACTIVIDAD,"
'        sql = sql & "PR0900.PR09NUMPETICION"
'        sql = sql & " FROM PR0400, PR0300, PR0100,"
'        sql = sql & "PR1400,AG1100,CI0100,"
'        sql = sql & " PR0900, SG0200,AD0200 AD02REA,AD0200 AD02SOL"
'        sql = sql & " WHERE  PR0400.PR37CODESTADO =?"
'        sql = sql & " AND PR0400.CI21CODPERSONA = ?"
'        sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI "
'        sql = sql & " AND PR0300.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
'        sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION "
'        sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD(+) "
'        sql = sql & " AND (PR1400.PR14INDRECPREFE = -1 OR (PR1400.PR14INDRECPREFE =0 AND PR0300.PR03INDCITABLE=0))"
'        sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
'        sql = sql & " AND PR0400.AD02CODDPTO=AD02REA.AD02CODDPTO"
'        sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
'        sql = sql & " AND PR1400.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
'        sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
'        sql = sql & " AND CI0100.CI01SITCITA IS NULL"
'        sql = sql & sqlOrder
'        'sql = sql & " ORDER BY PR0400.PR04FECPLANIFIC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = constESTACT_PLANIFICADA
    qry(1) = nPers
     qry(2) = constESTACT_PLANIFICADA
    qry(3) = nPers
    Set rs = qry.OpenResultset(sql)
    Call pCargarGrid(grdSinCitar, rs)
    rs.Close
    qry.Close
End Sub
Public Sub pIniciar()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

  If txtHistoria.Text = "" And nPers = 0 Then
    Exit Sub
  End If
  
  If txtHistoria.Text <> "" Then
    sql = "SELECT CI21CODPERSONA,"
    sql = sql & "CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE, "
    sql = sql & "CI22FECNACIM FECHANACIM "
    sql = sql & "FROM CI2200 WHERE CI22NUMHISTORIA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Val(txtHistoria.Text)
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
      nPers = rs!CI21CODPERSONA
      txtPaciente.Text = rs!PACIENTE
      txtFNacim.Text = Format$(rs!FECHANACIM, "dd/mm/yyyy")
      strHistoria_Ant = txtHistoria.Text
    Else
      MsgBox "No existe nadie con ese n�mero de historia. Utilice el buscador", vbExclamation, Me.Caption
      Exit Sub
    End If
  ElseIf nPers <> 0 Then
    sql = "SELECT CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE, "
    sql = sql & "CI22FECNACIM FECHANACIM "
    sql = sql & "FROM CI2200 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = nPers
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
      txtPaciente.Text = rs!PACIENTE
      txtFNacim.Text = Format$(rs!FECHANACIM, "dd/mm/yyyy")
      strHistoria_Ant = txtHistoria.Text
    End If
  End If

  Screen.MousePointer = vbHourglass
  Call pSinCitar(-1)
  Call pCitadas(-1)
  Call pPendientes(-1)
  Call pMensajes
  Screen.MousePointer = vbDefault

End Sub
Private Sub pCargarGrid(rsgrid As SSDBGrid, rs As rdoResultset)
rsgrid.RemoveAll
If rsgrid.Name = "grdSinCitar" Then
    Do While Not rs.EOF
         rsgrid.AddItem (rs!fecha & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!AG11DESRECURSO & Chr(9) & _
                        rs!DESDPTOREA & Chr(9) & _
                        rs!DESDPTOSOL & Chr(9) & _
                        rs!DOCTOR & Chr(9) & _
                        rs!PR03INDCITABLE & Chr(9) & _
                        rs!PR01CODACTUACION & Chr(9) & _
                        rs!CodDptoRea & Chr(9) & _
                        rs!CODDPTOSOL & Chr(9) & _
                        rs!PR37CODESTADO & Chr(9) & _
                        rs!PR04NUMACTPLAN & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!PR12CODACTIVIDAD & Chr(9) & _
                        rs!PR09NUMPETICION)
 
        rs.MoveNext
     Loop
    End If
    If rsgrid.Name = "grdCitadas" Then
     Do While Not rs.EOF
         rsgrid.AddItem (rs!fecha & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!AG11DESRECURSO & Chr(9) & _
                        rs!DESDPTOREA & Chr(9) & _
                        rs!DESDPTOSOL & Chr(9) & _
                        rs!DOCTOR & Chr(9) & _
                        rs!PR01CODACTUACION & Chr(9) & _
                        rs!CodDptoRea & Chr(9) & _
                        rs!CODDPTOSOL & Chr(9) & _
                        rs!PR37CODESTADO & Chr(9) & _
                        rs!PR04NUMACTPLAN & Chr(9) & _
                        rs!AG11CODRECURSO & Chr(9) & _
                        rs!PR12CODACTIVIDAD & Chr(9) & _
                        rs!PR09NUMPETICION & Chr(9) & _
                        rs!CI31NUMSOLICIT & Chr(9) & _
                        rs!CI01NUMCITA & Chr(9) & _
                        rs!AD15CODCAMA)
        rs.MoveNext
     Loop
    End If
    If rsgrid.Name = "grdPendientes" Then
     Do While Not rs.EOF
         rsgrid.AddItem (rs!fecha & Chr(9) & _
                        rs!FECCANCEL & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!AG11DESRECURSO & Chr(9) & _
                        rs!DESDPTOREA & Chr(9) & _
                        rs!DESDPTOSOL & Chr(9) & _
                        rs!DOCTOR & Chr(9) & _
                        rs!PR01CODACTUACION & Chr(9) & _
                        rs!CodDptoRea & Chr(9) & _
                        rs!CODDPTOSOL & Chr(9) & _
                        rs!PR37CODESTADO & Chr(9) & _
                        rs!PR04NUMACTPLAN & Chr(9) & _
                        rs!AG11CODRECURSO & Chr(9) & _
                        rs!PR12CODACTIVIDAD & Chr(9) & _
                        rs!PR09NUMPETICION & Chr(9) & _
                        rs!CI31NUMSOLICIT & Chr(9) & _
                        rs!CI01NUMCITA & Chr(9) & _
                        rs!AD15CODCAMA & Chr(9) & _
                        rs!PR03NUMACTPEDI)
        rs.MoveNext
    Loop
   End If

End Sub
Private Sub pFormatearGrids()
  With grdSinCitar
    .Columns(0).Caption = "Fecha Planificaci�n"
    .Columns(0).Width = 1600
    .Columns(1).Caption = "Actuacion"
    .Columns(1).Width = 2000
    .Columns(2).Caption = "Pedido para"
    .Columns(2).Width = 1500
    .Columns(3).Caption = "Dpto. Realizador"
    .Columns(3).Width = 1500
    .Columns(4).Caption = "Dpto. Solicitante"
    .Columns(4).Width = 1500
    .Columns(5).Caption = "Dr. Solicitante"
    .Columns(5).Width = 1500
    .Columns(6).Caption = "Citable"
    .Columns(6).Width = 700
    .Columns(6).style = ssStyleCheckBox
    .Columns(7).Caption = "CodActuacion"
    .Columns(7).Visible = False
    .Columns(8).Caption = "CodDptoRea"
    .Columns(8).Visible = False
    .Columns(9).Caption = "CodDptoSol"
    .Columns(9).Visible = False
    .Columns(10).Caption = "CodEstadoAct"
    .Columns(10).Visible = False
    .Columns(11).Caption = "NumActPlan"
    .Columns(11).Visible = False
    .Columns(12).Caption = "CodRecurso"
    .Columns(12).Visible = False
    .Columns(13).Caption = "CodActividad"
    .Columns(13).Visible = False
    .Columns(14).Caption = "NumPeticion"
    .Columns(14).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
  End With
  With grdCitadas
    .Columns(0).Caption = "Fecha Cita"
    .Columns(0).Width = 1600
    .Columns(1).Caption = "Actuacion"
    .Columns(1).Width = 2000
    .Columns(2).Caption = "Citado para"
    .Columns(2).Width = 1500
    .Columns(3).Caption = "Dpto. Realizador"
    .Columns(3).Width = 1500
    .Columns(4).Caption = "Dpto. Solicitante"
    .Columns(4).Width = 1500
    .Columns(5).Caption = "Dr. Solicitante"
    .Columns(5).Width = 1500
    .Columns(6).Caption = "CodActuacion"
    .Columns(6).Visible = False
    .Columns(7).Caption = "CodDptoRea"
    .Columns(7).Visible = False
    .Columns(8).Caption = "CodDptoSol"
    .Columns(8).Visible = False
    .Columns(9).Caption = "CodEstadoAct"
    .Columns(9).Visible = False
    .Columns(10).Caption = "NumActPlan"
    .Columns(10).Visible = False
    .Columns(11).Caption = "CodRecurso"
    .Columns(11).Visible = False
    .Columns(12).Caption = "CodActividad"
    .Columns(12).Visible = False
    .Columns(13).Caption = "NumPeticion"
    .Columns(13).Visible = False
    .Columns(14).Caption = "NumSolicitud"
    .Columns(14).Visible = False
    .Columns(15).Caption = "NumCita"
    .Columns(15).Visible = False
    .Columns(16).Caption = "CodCama"
    .Columns(16).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
  End With
  With grdPendientes
    .Columns(0).Caption = "Fecha Cita"
    .Columns(0).Width = 1600
    .Columns(1).Caption = "Fecha en Pendiente"
    .Columns(1).Width = 1600
    .Columns(2).Caption = "Actuacion"
    .Columns(2).Width = 2000
    .Columns(3).Caption = "Citado para"
    .Columns(3).Width = 1500
    .Columns(4).Caption = "Dpto. Realizador"
    .Columns(4).Width = 1500
    .Columns(5).Caption = "Dpto. Solicitante"
    .Columns(5).Width = 1500
    .Columns(6).Caption = "Dr. Solicitante"
    .Columns(6).Width = 1500
    .Columns(7).Caption = "CodActuacion"
    .Columns(7).Visible = False
    .Columns(8).Caption = "CodDptoRea"
    .Columns(8).Visible = False
    .Columns(9).Caption = "CodDptoSol"
    .Columns(9).Visible = False
    .Columns(10).Caption = "CodEstadoAct"
    .Columns(10).Visible = False
    .Columns(11).Caption = "NumActPlan"
    .Columns(11).Visible = False
    .Columns(12).Caption = "CodRecurso"
    .Columns(12).Visible = False
    .Columns(13).Caption = "CodActividad"
    .Columns(13).Visible = False
    .Columns(14).Caption = "NumPeticion"
    .Columns(14).Visible = False
    .Columns(15).Caption = "NumSolicitud"
    .Columns(15).Visible = False
    .Columns(16).Caption = "NumCita"
    .Columns(16).Visible = False
    .Columns(17).Caption = "CodCama"
    .Columns(17).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
  End With
End Sub

Private Sub cmdAnularPet_Click()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim rs1 As rdoResultset
Dim strCancel As String
Dim rsgrid As SSDBGrid
Dim lngPeticion As Long
Set rsgrid = fGrid
  If rsgrid Is Nothing Then Exit Sub
  If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
    If fValidarEstado(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) = False Then
        MsgBox "La prueba tiene fecha de entrada en cola" & Chr$(13) & "Debe seleccionar pruebas v�lidas.", vbInformation, Me.Caption
        Exit Sub
    End If
  If fblnAsociada(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) = True Then
     MsgBox "Esta prueba esta asociada a otra. Anule la Principal", vbExclamation, "Citaci�n"
    Exit Sub
  End If
  If Not fblnCitarModiAnular(CStr(rsgrid.Columns("CodDptoRea").CellValue(rsgrid.SelBookmarks(0))), _
    rsgrid.Columns("CodActividad").CellValue(rsgrid.SelBookmarks(0)), _
    rsgrid.Columns("CodActuacion").CellValue(rsgrid.SelBookmarks(0)), False) Then
    MsgBox "No tiene autorizacion a Modificar esta prueba", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  'Si hay varias actuaciones sin fecha de entrada en cola en la misma peticion
  'las mostramos
  sql = "SELECT PR09NUMPETICION,PR0300.PR03NUMACTPEDI FROM PR0300,PR0400 WHERE PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND PR0400.PR04NUMACTPLAN=?"
  Set qry = objApp.rdoConnect.CreateQuery("", sql)
  qry(0) = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))
  Set rs = qry.OpenResultset()

  sql = "SELECT COUNT(*) FROM PR0300,PR0400 WHERE PR09NUMPETICION=?"
  sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
  sql = sql & " AND PR0400.PR37CODESTADO in (2,1) and PR04FECENTRCOLA IS NULL"
  Set qry = objApp.rdoConnect.CreateQuery("", sql)
  qry(0) = rs(0)
  Set rs1 = qry.OpenResultset()

  If rs1(0) = 1 Then 'solo una actuacion en la peticion
    sql = "Si Ud. anula la actuaci�n ya no estar� disponible para ser realizada." & Chr$(13)
    sql = sql & "Indique el motivo por el que se va a anular la actuaci�n:"
    strCancel = Trim$(InputBox(sql, "Anular Actuaci�n"))
    If strCancel = "" Then
        sql = "La actuaci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    If rsgrid.Name = "grdSinCitar" Then
        Call pAnular(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)), "", strCancel)
    Else
        Call pAnular(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)), rsgrid.Columns("CodCama").CellValue(rsgrid.SelBookmarks(0)), strCancel)
    End If
    Call pIniciar
  Else 'varias actuaciones en la peticion
    If Not rs.EOF Then
      Call objPipe.PipeSet("ACT", rs(1))
      Call objPipe.PipeSet("PET", rs(0))
      lngPeticion = rs(0)
      frmAnular.Show vbModal
      Set frmAnular = Nothing
'      If objPipe.PipeExist("ACT") Then
'        lngActAnulada = Val(objPipe.PipeGet("ACT"))
'        If lngActAnulada = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) Then
'          rsgrid.DeleteSelected
'        End If
'        objPipe.PipeRemove ("ACT")
'      End If
      If objPipe.PipeExist("PET") Then
        rsgrid.DeleteSelected
        objPipe.PipeRemove ("PET")
      End If
    End If
    Call pIniciar
  End If
 

End Sub

Private Sub cmdAnularPrueba_Click()
Dim sql As String
Dim qry As rdoQuery
Dim blnCancelTrans      As Boolean
Dim blnReturn           As Boolean
Dim intGridIndex        As Integer
Dim vnta                As Variant
Dim blnAnularSolicitud As Boolean
Dim lngNumActPlan As Long
Dim NumCita As Byte
Dim rs As rdoResultset
Dim blnAnular As Boolean
Dim i As Integer
Dim strSql As String
Dim rs1 As rdoResultset
Dim intRow   As Integer
Dim vntRowBookmark As Variant
Dim rsgrid As SSDBGrid
Dim intres As String
Dim lngActAnulada As Long
Dim lngPeticion As Long
Dim strCancel$
Dim cllAct As New Collection
Dim strMensaje As String
Dim j As Integer

  Set rsgrid = fGrid
  If rsgrid Is Nothing Then Exit Sub
  awkAct = ""
  

'  If rsgrid.SelBookmarks.Count > 1 Then
'    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
'    Exit Sub
'  End If
  If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation, Me.Caption
    Exit Sub
  End If
For i = 0 To rsgrid.SelBookmarks.Count - 1
awkAct = awkAct & rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i)) & ","
awkAct = awkAct & True
  If fValidarEstado(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))) = True Then
  'si es una actuacion asociada (principalmente anestesia) no dejo anular
      If fblnAsociada(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))) = False Then
        If Not fblnCitarModiAnular(CStr(rsgrid.Columns("CodDptoRea").CellValue(rsgrid.SelBookmarks(i))), _
            rsgrid.Columns("CodActividad").CellValue(rsgrid.SelBookmarks(i)), _
            rsgrid.Columns("CodActuacion").CellValue(rsgrid.SelBookmarks(i)), False) Then
            awkAct.F(awkAct.NF) = False 'rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))
            cllAct.Add i 'rsgrid.Columns("Actuacion").CellValue(rsgrid.SelBookmarks(i))
            cllAct.Add "No tiene autorizaci�n para modificar esta prueba"
            'MsgBox "No tiene autorizacion a Modificar esta prueba", vbExclamation, Me.Caption
            'Exit Sub
        End If
      Else
        awkAct.F(awkAct.NF) = False                   'rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))
        cllAct.Add i 'rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))
        cllAct.Add "Tiene Actuaciones asociadas"
        'Exit Sub
      End If
  Else
    awkAct.F(awkAct.NF) = False 'rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))
    cllAct.Add i  'rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i))
    cllAct.Add "Tiene fecha de entrada en cola"
    'Exit Sub
  End If
  awkAct = awkAct & ","
Next i
  'Si hay varias actuaciones sin fecha de entrada en cola en la misma peticion
  'las mostramos
  
  
'  SQL = "SELECT PR09NUMPETICION,PR0300.PR03NUMACTPEDI "
'  SQL = SQL & "FROM PR0300,PR0400 WHERE PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI "
'  SQL = SQL & "AND PR0400.PR04NUMACTPLAN=?"
'  Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'  qry(0) = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))
'  Set rs = qry.OpenResultset()
'
'  SQL = "SELECT COUNT(*) FROM PR0300,PR0400 WHERE PR09NUMPETICION=?"
'  SQL = SQL & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
'  SQL = SQL & " AND PR0400.PR37CODESTADO in (2,1) and PR04FECENTRCOLA IS NULL"
'  Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'  qry(0) = rs(0)
'  Set rs1 = qry.OpenResultset()
'
'  If rs1(0) = 1 Then 'solo una actuacion en la peticion
If rsgrid.SelBookmarks.Count > 1 Then
    If cllAct.Count > 0 Then
        strMensaje = "Las siguientes actuaciones no van a poder ser anuladas:" & Chr(13) & Chr(13)
        For j = 1 To cllAct.Count Step 2
            Select Case rsgrid.Name
                Case "grdSinCitar"
                    strMensaje = strMensaje & rsgrid.Columns("Fecha Planificaci�n").CellValue(rsgrid.SelBookmarks(cllAct(j))) & "    "
                Case "grdCitadas"
                    strMensaje = strMensaje & rsgrid.Columns("Fecha Cita").CellValue(rsgrid.SelBookmarks(cllAct(j))) & "    "
                Case "grdPendientes "
                    strMensaje = strMensaje & rsgrid.Columns("Fecha en Pendiente").CellValue(rsgrid.SelBookmarks(cllAct(j))) & "    "
            End Select
            strMensaje = strMensaje & rsgrid.Columns("Actuacion").CellValue(rsgrid.SelBookmarks(cllAct(j))) & ": " & cllAct(j + 1) & Chr(13)
        Next j
        If cllAct.Count / 2 <> rsgrid.SelBookmarks.Count Then
            strMensaje = strMensaje & Chr(13) & "Desea continuar?" & Chr(13)
        Else
            MsgBox strMensaje, vbOKOnly + vbExclamation, Me.Caption
            Exit Sub
        End If
        intres = MsgBox(strMensaje, vbYesNo + vbExclamation, Me.Caption)
        If intres = vbNo Then
            Exit Sub
        End If
    '    For i = 1 To cllAct.Count
    '        cllAct.Remove i
    '    Next
    End If
Else
    If cllAct.Count > 0 Then
        strMensaje = "La actuacion no va a poder ser anulada:" & Chr(13) & Chr(13)
        For j = 1 To cllAct.Count Step 2
            Select Case rsgrid.Name
                Case "grdSinCitar"
                    strMensaje = strMensaje & rsgrid.Columns("Fecha Planificaci�n").CellValue(rsgrid.SelBookmarks(cllAct(j))) & "    "
                Case "grdCitadas"
                    strMensaje = strMensaje & rsgrid.Columns("Fecha Cita").CellValue(rsgrid.SelBookmarks(cllAct(j))) & "    "
                Case "grdPendientes "
                    strMensaje = strMensaje & rsgrid.Columns("Fecha en Pendiente").CellValue(rsgrid.SelBookmarks(cllAct(j))) & "    "
            End Select
            strMensaje = strMensaje & rsgrid.Columns("Actuacion").CellValue(rsgrid.SelBookmarks(cllAct(j))) & ": " & cllAct(j + 1) & Chr(13)
        Next j
        MsgBox strMensaje, vbOKOnly + vbExclamation, Me.Caption
        Exit Sub
    End If
End If

    'If cllAct.Count
    'If cllAct(i + 1) <> i Then
        sql = "Si Ud. anula la actuaci�n ya no estar� disponible para ser realizada." & Chr$(13)
        sql = sql & "Indique el motivo por el que se va a anular la actuaci�n:"
        strCancel = Trim$(InputBox(sql, "Anular Actuaci�n"))
        If strCancel = "" Then
            sql = "La actuaci�n NO ha sido anulada: hace falta indicar un motivo."
            MsgBox sql, vbExclamation, strMeCaption
            Exit Sub
        End If
    For i = 1 To awkAct.NF - 1 Step 2
        If awkAct.F(i + 1) = True Then
        If rsgrid.Name = "grdSinCitar" Then
            Call pAnular(awkAct.F(i), "", strCancel)
        Else
            For j = 0 To rsgrid.SelBookmarks.Count - 1
                If awkAct.F(i) = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(j)) Then
                    Call pAnular(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(j)), rsgrid.Columns("CodCama").CellValue(rsgrid.SelBookmarks(j)), strCancel)
                End If
            Next j
        End If
    End If
Next i
Call pIniciar
'  Else 'varias actuaciones en la peticion
'    If Not rs.EOF Then
'      Call objPipe.PipeSet("ACT", rs(1))
'      Call objPipe.PipeSet("PET", rs(0))
'      lngPeticion = rs(0)
'      frmAnular.Show vbModal
'      Set frmAnular = Nothing
'      If objPipe.PipeExist("ACT") Then
'        lngActAnulada = Val(objPipe.PipeGet("ACT"))
'        If lngActAnulada = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) Then
'          rsgrid.DeleteSelected
'        End If
'        objPipe.PipeRemove ("ACT")
'      End If
'      If objPipe.PipeExist("PET") Then
'        rsgrid.DeleteSelected
'        objPipe.PipeRemove ("PET")
'      End If
'    End If
'    Call pIniciar
'  End If

End Sub



Private Sub cmdAvisos_Click()
Dim vntData(1)
  If nPers <> 0 Then
    vntData(1) = nPers
    Call objSecurity.LaunchProcess("PR0580", vntData)
    Call pMensajes
  End If

End Sub

Private Sub cmdBuscar_Click()
  frmBuscaPersonas.Show vbModal
  If objPipe.PipeExist("AD_CodPersona") Then
    nPers = objPipe.PipeGet("AD_CodPersona")
    objPipe.PipeRemove ("AD_CodPersona")
  End If
  If objPipe.PipeExist("AD_NumHistoria") Then
    txtHistoria.Text = objPipe.PipeGet("AD_NumHistoria")
    objPipe.PipeRemove ("AD_NumHistoria")
  End If
  Call pIniciar
End Sub

Private Sub cmdCitar_Click(Index As Integer)
Dim vntData(1 To 7)
Dim rsgrid As SSDBGrid
Dim sql As String
Dim rs As rdoResultset
Dim strFecha As String, nprs As String
Dim i As Integer
Dim strNoCitables As String
Dim texto As String
  Set rsgrid = fGrid
  If rsgrid Is Nothing Then
    Exit Sub
  End If
  
  For i = 0 To rsgrid.SelBookmarks.Count - 1
    If rsgrid.Name = "grdSinCitar" Then
        If rsgrid.Columns("Citable").CellValue(rsgrid.SelBookmarks(i)) = True Then
            nprs = nprs & rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i)) & ", "
        Else
            strNoCitables = strNoCitables & " --> " & rsgrid.Columns("Actuacion").CellValue(rsgrid.SelBookmarks(i)) & Chr$(13)
        End If
    Else
        nprs = nprs & rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(i)) & ", "
    End If
  Next i
  If Len(Trim(strNoCitables)) > 0 Then
'    strNoCitables = Left(strNoCitables, Len(strNoCitables) - 2)
    MsgBox "Las siguientes actuaciones no son citables:" & Chr$(13) & _
    strNoCitables, vbCritical
  End If
  If Len(nprs) > 3 Then
    nprs = Left(nprs, Len(nprs) - 2)
    
    ' Para citar no debe tener fecha de entrada en cola
    If fValidarEstado(nprs, texto) = False Then
        MsgBox "Las siguientes pruebas tienen fecha de entrada en cola: " & Chr$(13) & texto _
        & Chr$(13) & Chr$(13) & "Debe seleccionar pruebas v�lidas.", vbInformation, Me.Caption
        Exit Sub
    End If
    Select Case Index
        Case 0 'citacion normal
            Call pCitarActuaciones(nprs)
        Case 1 'citacion por overbooking
            Call pCitarOverbooking(nprs)
    End Select
    Call pIniciar
  End If
End Sub


Private Sub cmdDatosAct_Click()
Dim rsgrid As SSDBGrid
Dim lngActPlan&
  
  Set rsgrid = fGrid
  If rsgrid Is Nothing Then
    Exit Sub
  End If
  If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "No se ha seleccionado ninguna actuacion", vbExclamation, strMeCaption
    Exit Sub
  End If
  If rsgrid.SelBookmarks.Count = 1 Then
    lngActPlan = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))
  Else
    MsgBox "Debe Ud. seleccionar una �nica Actuaci�n.", vbExclamation, strMeCaption
    Exit Sub
  End If
  lngActPlan = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) & ", "
  Call objSecurity.LaunchProcess("PR0510", lngActPlan)

End Sub

Private Sub cmdDejarPend_Click()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim blnabort As Boolean
Dim vntBookmark
Dim i As Integer
Dim intR As Integer
Dim strDpto As String

If grdCitadas.SelBookmarks.Count = 0 Then
MsgBox "Debe seleccionar alguna actuaci�n citada", vbExclamation, Me.Caption
  Exit Sub
End If
For i = 0 To grdCitadas.SelBookmarks.Count - 1
    If Not fblnCitarModiAnular(CStr(grdCitadas.Columns("CodDptoRea").CellValue(grdCitadas.SelBookmarks(i))), _
        grdCitadas.Columns("CodActividad").CellValue(grdCitadas.SelBookmarks(i)), grdCitadas.Columns("CodActuacion").CellValue(grdCitadas.SelBookmarks(i)), True) Then
        blnabort = True
        strDpto = strDpto & ", " & grdCitadas.Columns("Dpto. Realizador").CellValue(grdCitadas.SelBookmarks(i))
    End If
Next
If blnabort Then
    MsgBox "No tiene autorizacion para Modificar actuaciones del departamento" & Left(strDpto, Len(strDpto) - 1), vbInformation, Me.Caption
    Exit Sub
End If
intR = MsgBox("Desea Dejar en PENDIENTES las actuaciones seleccionadas", vbQuestion + vbYesNo, Me.Caption)
If intR = vbYes Then
    'objApp.BeginTrans
    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        sql = "UPDATE PR0400 SET PR37CODESTADO = 1 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdCitadas.Columns("NumActPlan").CellValue(grdCitadas.SelBookmarks(i))
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA = '4' WHERE PR04NUMACTPLAN = ?"
        sql = sql & " AND CI01SITCITA = '1'"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdCitadas.Columns("NumActPlan").CellValue(grdCitadas.SelBookmarks(i))
        qry.Execute
        qry.Close
'        If Err > 0 Then
'            MsgBox "Imposible Dejar en Pendiente", vbCritical
'            objApp.RollbackTrans
'            Exit Sub
        'End If
    Next
    'objApp.CommitTrans
    MsgBox "Las pruebas se han quedado pendientes de citar", vbInformation, Me.Caption
    Call pIniciar
    
End If
    
'     SQL1 = "SELECT B.PR37CODESTADO,C.CI01FECCONCERT,A.PR01CODACTUACION,A.AD02CODDPTO,C.AG11CODRECURSO,D.AD02CODDPTO,E.SG02NOM || ' ' || E.SG02APE1 || ' ' || E.SG02APE2 DOCTOR,C.CI01INDLISESPE,A.CI21CODPERSONA,B.PR04NUMACTPLAN,C.CI31NUMSOLICIT,C.CI01NUMCITA,H.PR01DESCORTA,I.AD02DESDPTO,J.AG11DESRECURSO,H.PR12CODACTIVIDAD,A.PR03NUMACTPEDI,C.CI01SITCITA,D.PR09NUMPETICION"
'     SQL1 = SQL1 & " FROM PR0300 A, PR0400 B, CI0100 C, PR0900 D, SG0200 E,PR0100 H,AD0200 I,AG1100 J"
'     SQL1 = SQL1 & " WHERE C.CI01SITCITA=" & constESTCITA_PENDRECITAR & " AND B.PR04NUMACTPLAN = ?"
'     SQL1 = SQL1 & " AND B.PR37CODESTADO =" & constESTACT_PLANIFICADA
'     SQL1 = SQL1 & " AND A.PR03NUMACTPEDI = B.PR03NUMACTPEDI"
'     SQL1 = SQL1 & " AND A.PR09NUMPETICION=D.PR09NUMPETICION (+)"
'     SQL1 = SQL1 & " AND D.SG02COD=E.SG02COD"
'     SQL1 = SQL1 & " AND B.PR04NUMACTPLAN=C.PR04NUMACTPLAN"
'     SQL1 = SQL1 & " AND B.PR01CODACTUACION=H.PR01CODACTUACION"
'     SQL1 = SQL1 & " AND A.AD02CODDPTO=I.AD02CODDPTO"
'     SQL1 = SQL1 & " AND C.AG11CODRECURSO=J.AG11CODRECURSO"
'
'     Set qry3 = objApp.rdoConnect.CreateQuery("", SQL1)
'     qry3(0) = grdDBGrid1(1).Columns("N�mero").Value
'
'     Set respuesta1c = qry3.OpenResultset()
'            sql4c = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & respuesta1c(5)
'            Set rs5c = objApp.rdoConnect.OpenResultset(sql4c)
'            grdDBGrid1(2).AddItem respuesta1c!CI01FECCONCERT & Chr$(9) _
'            & respuesta1c!PR01DESCORTA & Chr$(9) _
'            & respuesta1c!AD02DESDPTO & Chr$(9) _
'            & respuesta1c!AG11DESRECURSO & Chr$(9) _
'            & rs5c(0) & Chr$(9) _
'            & respuesta1c!DOCTOR & Chr$(9) _
'            & respuesta1c!CI21CODPERSONA & Chr$(9) _
'            & respuesta1c!PR04NUMACTPLAN & Chr$(9) _
'            & respuesta1c!PR12CODACTIVIDAD & Chr$(9) _
'            & respuesta1c!CI31NUMSOLICIT & Chr$(9) _
'            & respuesta1c!CI01NUMCITA & Chr$(9) _
'            & respuesta1c!PR03NUMACTPEDI & Chr$(9) _
'            & respuesta1c!CI01SITCITA & Chr$(9) _
'            & respuesta1c!PR01CODACTUACION & Chr$(9) _
'            & respuesta1c(3) & Chr$(9) _
'            & respuesta1c!PR09NUMPETICION
'            rs5c.Close
'        grdDBGrid1(1).SelBookmarks.Remove (0)
'    Next i
'    objApp.CommitTrans
'   'Me.Refresh
'
'
'    MsgBox "Las pruebas se han quedado pendientes de citar", vbOKOnly
'End If

End Sub

Private Sub cmdHF_Click()
If nPers <> 0 Then
    Call objSecurity.LaunchProcess("CI4017", nPers)
End If
End Sub

Private Sub cmdImpCita_Click()
    Dim i%, strNAPlan$

    'se mira si hay aguna actuaci�n seleccionada
    If grdCitadas.SelBookmarks.Count = 0 Then
        MsgBox "No se ha seleccionado ninguna Actuaci�n citada.", vbExclamation, Me.Caption
        Exit Sub
    End If

    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        strNAPlan = "," & grdCitadas.Columns("NumActPlan").CellText(grdCitadas.SelBookmarks(i))
    Next i
    strNAPlan = Mid$(strNAPlan, 2)
    Call objSecurity.LaunchProcess("AG0218", strNAPlan)
End Sub

Private Sub cmdImprimir_Click()
Dim sql As String
Dim rs As rdoResultset
Dim strW As String
Dim rsgrid As SSDBGrid

  Set rsgrid = fGrid
  If rsgrid Is Nothing Then
    Exit Sub
  End If
  
  If chkPet.Value = 1 Then
    Call pImprimir(rsgrid)
  End If
  
  If chkPac.Value = 1 Then
    strW = "PR0460J.CI21CODPERSONA = " & nPers
    Call Imprimir_API(strW, "PROPAC1.RPT")
  End If

  If chkDpto.Value = 1 Then
    sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = 4 "
    sql = sql & " AND SG02COD = '" & objSecurity.strUser & "'"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    If rs(0) = 0 Then
      strW = "PR0463J.CI21CODPERSONA= " & nPers
      Call Imprimir_API(strW, "PROPAC2.rpt")
    Else
      MsgBox "Este Programa es para uso interno del Departamento", vbCritical, Me.Caption
    End If
  End If

End Sub

Private Sub cmdIndSanit_Click()
Dim rsgrid As SSDBGrid
Dim vntData
Set rsgrid = fGrid
If rsgrid Is Nothing Then
    Exit Sub
End If
If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opci�n no est� disponible para una selecci�n m�ltiple", vbExclamation, Me.Caption
    Exit Sub
End If

    vntData = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))
    Call objSecurity.LaunchProcess("CI2052", vntData)
    'frmSanitarios.Show vbModal
    'Set frmSanitarios = Nothing
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

  blnDatosCargados = False
  Call pFormatearGrids
End Sub

Private Sub grdCitadas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub grdCitadas_Click()
grdPendientes.SelBookmarks.RemoveAll
grdSinCitar.SelBookmarks.RemoveAll
End Sub

Private Sub grdCitadas_HeadClick(ByVal ColIndex As Integer)
If grdCitadas.Rows > 0 Then Call pCitadas(ColIndex)
End Sub

Private Sub grdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdPendientes_Click()
grdCitadas.SelBookmarks.RemoveAll
grdSinCitar.SelBookmarks.RemoveAll

End Sub

Private Sub grdPendientes_HeadClick(ByVal ColIndex As Integer)
If grdPendientes.Rows > 0 Then Call pPendientes(ColIndex)

End Sub

Private Sub grdSinCitar_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdSinCitar_Click()
grdCitadas.SelBookmarks.RemoveAll
grdPendientes.SelBookmarks.RemoveAll
End Sub

Private Sub grdSinCitar_HeadClick(ByVal ColIndex As Integer)
If grdSinCitar.Rows > 0 Then Call pSinCitar(ColIndex)

End Sub

Private Sub optCitables_Click()
Call pSinCitar(-1)
End Sub

Private Sub optNoCit_Click()
Call pSinCitar(-1)
End Sub

Private Sub optTodas_Click()
Call pSinCitar(-1)
End Sub

Private Sub txtHistoria_Change()
  If txtHistoria.Text <> strHistoria_Ant Then
    Call pLimpiar
  End If
End Sub

Private Sub txtHistoria_GotFocus()
  txtHistoria.SelStart = 0
  txtHistoria.SelLength = Len(Trim(txtHistoria.Text))

End Sub

Private Sub txtHistoria_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    cmdBuscar.SetFocus
  End If
End Sub

Private Sub txthistoria_LostFocus()
  Call pIniciar
'Call pMensajes
End Sub

Private Sub txtPaciente_GotFocus()
txtPaciente.SelStart = 0
txtPaciente.SelLength = Len(Trim(txtPaciente.Text))

End Sub

Private Sub VideoSoftAwk1_Begin()

End Sub
