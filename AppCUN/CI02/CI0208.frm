VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmRecitacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recitación"
   ClientHeight    =   1785
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5325
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1785
   ScaleWidth      =   5325
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraRec 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   540
      Width           =   3795
      Begin SSDataWidgets_B.SSDBCombo cboRec 
         Height          =   315
         Left            =   780
         TabIndex        =   2
         Top             =   0
         Width           =   2985
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4445
         Columns(1).Caption=   "Rec"
         Columns(1).Name =   "Rec"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblRecurso 
         Alignment       =   1  'Right Justify
         Caption         =   "Recurso:"
         Height          =   255
         Left            =   0
         TabIndex        =   10
         Top             =   60
         Width           =   675
      End
   End
   Begin VB.Frame fraHora 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   2520
      TabIndex        =   8
      Top             =   180
      Width           =   735
      Begin MSMask.MaskEdBox mskHora 
         Height          =   300
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   529
         _Version        =   327681
         MaxLength       =   5
         Format          =   "hh:mm"
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
   End
   Begin VB.TextBox txtObserv 
      Height          =   675
      Left            =   900
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   1020
      Width           =   4215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   4140
      TabIndex        =   4
      Top             =   60
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4140
      TabIndex        =   3
      Top             =   540
      Width           =   975
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecha 
      DataField       =   "Pr09FecPeticion"
      Height          =   300
      Left            =   900
      TabIndex        =   0
      Tag             =   "Fecha Petición"
      Top             =   180
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   529
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Observ:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1020
      Width           =   675
   End
   Begin VB.Label lblFecha 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha:"
      Height          =   195
      Left            =   240
      TabIndex        =   5
      Top             =   240
      Width           =   555
   End
End
Attribute VB_Name = "frmRecitacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    If fComprobaciones Then
        Call objPipe.PipeSet("CI0208_FECHA", dcboFecha.Text)
        If mskHora.Text <> "__:__" Then
            Call objPipe.PipeSet("CI0208_HORA", mskHora.Text)
        End If
        If cboRec.Text <> "" Then
            Call objPipe.PipeSet("CI0208_AG11CODRECURSO", cboRec.Columns("Cod").Text)
        End If
        If txtObserv.Text <> "" Then
            Call objPipe.PipeSet("CI0208_CI01OBSERV", txtObserv.Text)
        End If
        Unload Me
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim strAhora$, strFecha$, strCodRec$

    Dim i As Integer
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    strAhora = fAhora
    strFecha = objPipe.PipeGet("CI0208_FECHA")
    Call objPipe.PipeRemove("CI0208_FECHA")
    dcboFecha.Date = Format(IIf(CDate(strAhora) < CDate(strFecha), strFecha, strAhora), "dd/mm/yyyy")
    dcboFecha.MinDate = strAhora
    dcboFecha.BackColor = objApp.objUserColor.lngMandatory
    If objPipe.PipeExist("CI0208_HORA") Then
        mskHora.Text = objPipe.PipeGet("CI0208_HORA")
        Call objPipe.PipeRemove("CI0208_HORA")
        mskHora.BackColor = objApp.objUserColor.lngMandatory
    Else
        fraHora.Enabled = False
        mskHora.BackColor = objApp.objUserColor.lngReadOnly
    End If
    If objPipe.PipeExist("CI0208_AG11CODRECURSO") Then
        strCodRec = objPipe.PipeGet("CI0208_AG11CODRECURSO")
        Call objPipe.PipeRemove("CI0208_AG11CODRECURSO")
        SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO"
        SQL = SQL & " FROM AG1100"
        SQL = SQL & " WHERE (AD02CODDPTO, AG14CODTIPRECU) IN"
        SQL = SQL & " (SELECT AD02CODDPTO, AG14CODTIPRECU"
        SQL = SQL & " FROM AG1100"
        SQL = SQL & " WHERE AG11CODRECURSO = ?)"
        SQL = SQL & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
        SQL = SQL & " ORDER BY AG11ORDEN, AG11DESRECURSO"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodRec
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            cboRec.AddItem rs!AG11CODRECURSO & Chr(9) & rs!AG11DESRECURSO
            rs.MoveNext
        Loop
        For i = 1 To cboRec.Rows
            If i = 1 Then cboRec.MoveFirst Else cboRec.MoveNext
            If strCodRec = cboRec.Columns(0).Value Then
                cboRec.Text = cboRec.Columns(1).Value
                Exit For
            End If
        Next
        cboRec.BackColor = objApp.objUserColor.lngMandatory
    Else
        cboRec.Enabled = False
        cboRec.BackColor = objApp.objUserColor.lngReadOnly
    End If
    txtObserv.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub mskHora_GotFocus()
    mskHora.SelStart = 0
    mskHora.SelLength = Len(mskHora.Text)
End Sub

Private Sub mskHora_LostFocus()
    If mskHora.Text <> "__:__" Then
        mskHora.Text = objGen.ReplaceStr(mskHora.Text, "_", "0", 0)
        If Not IsDate(mskHora.Text) Then 'no es una hora correcta
            mskHora.Text = "__:__"
            mskHora.SetFocus
        End If
    End If
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    
    fComprobaciones = True
End Function
