VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmCitasRecurso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Citas por recurso"
   ClientHeight    =   8595
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11670
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11670
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRecPlan 
      Caption         =   "&Recitaci�n"
      Height          =   435
      Left            =   10680
      TabIndex        =   27
      Top             =   5940
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CheckBox chkPendientes 
      Caption         =   "Todas "
      Height          =   555
      Left            =   10740
      TabIndex        =   26
      Top             =   6480
      Width           =   855
   End
   Begin VB.CommandButton cmdObser2 
      Caption         =   "&Observaciones2"
      Height          =   375
      Left            =   2220
      TabIndex        =   25
      Top             =   8220
      Width           =   1575
   End
   Begin VB.CommandButton cmdVolverCitada 
      Caption         =   "&Volver  Cit."
      Height          =   375
      Left            =   10680
      TabIndex        =   24
      Top             =   5460
      Width           =   945
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listados"
      Height          =   1215
      Left            =   8880
      TabIndex        =   19
      Top             =   0
      Width           =   2715
      Begin VB.OptionButton OptFechas 
         Caption         =   "En un rango de fechas"
         Height          =   255
         Left            =   180
         TabIndex        =   23
         Top             =   240
         Value           =   -1  'True
         Width           =   1995
      End
      Begin VB.OptionButton optDSolicitante 
         Caption         =   "Diario con solicitante"
         Height          =   195
         Left            =   180
         TabIndex        =   22
         Top             =   570
         Width           =   1815
      End
      Begin VB.OptionButton optDiario 
         Caption         =   "Diario"
         Height          =   255
         Left            =   180
         TabIndex        =   21
         Top             =   840
         Width           =   1395
      End
      Begin VB.CommandButton cmdlistados 
         Caption         =   "Im&primir"
         Height          =   375
         Left            =   1740
         TabIndex        =   20
         Top             =   780
         Width           =   915
      End
   End
   Begin VB.CommandButton cmdHojaPacientes 
      Caption         =   "&Pte. recitar"
      Height          =   375
      Left            =   10680
      TabIndex        =   18
      Top             =   4980
      Width           =   945
   End
   Begin VB.CommandButton cmdImprimirCarta 
      Caption         =   "&Carta"
      Height          =   375
      Left            =   10680
      TabIndex        =   16
      Top             =   1560
      Width           =   960
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Recitar"
      Height          =   375
      Left            =   60
      TabIndex        =   13
      Top             =   8220
      Width           =   1575
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   375
      Left            =   4320
      TabIndex        =   12
      Top             =   8220
      Width           =   1575
   End
   Begin VB.CommandButton cmdObservaciones 
      Caption         =   "&Obser./Cuestionario"
      Height          =   375
      Left            =   7140
      TabIndex        =   11
      Top             =   8220
      Width           =   1575
   End
   Begin VB.CommandButton cmdVisionGlobal 
      Caption         =   "&Vision Global"
      Height          =   375
      Left            =   8820
      TabIndex        =   10
      Top             =   8220
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10800
      TabIndex        =   9
      Top             =   8220
      Width           =   855
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   435
      Left            =   7860
      TabIndex        =   8
      Top             =   180
      Width           =   975
   End
   Begin VB.Frame fradptorecurso 
      BorderStyle     =   0  'None
      Caption         =   "Dpto. / Recurso"
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8895
      Begin VB.CommandButton cmdAgendaRec 
         Caption         =   "&Agenda"
         Height          =   435
         Left            =   7860
         TabIndex        =   14
         Top             =   660
         Width           =   975
      End
      Begin ComctlLib.ListView lvwRecurso 
         Height          =   915
         Left            =   4740
         TabIndex        =   1
         Top             =   180
         Width           =   3075
         _ExtentX        =   5424
         _ExtentY        =   1614
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSDataWidgets_B.SSDBCombo cboDpto 
         Height          =   315
         Left            =   660
         TabIndex        =   2
         Top             =   240
         Width           =   3975
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7011
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecini 
         Height          =   330
         Left            =   660
         TabIndex        =   4
         Tag             =   "Fecha superior de las citas"
         Top             =   660
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecFin 
         Height          =   330
         Left            =   3000
         TabIndex        =   5
         Tag             =   "Fecha superior de las citas"
         Top             =   660
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   510
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   2520
         TabIndex        =   6
         Top             =   720
         Width           =   465
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Dpto:"
         Height          =   315
         Index           =   0
         Left            =   60
         TabIndex        =   3
         Top             =   300
         Width           =   435
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdCitadas 
      Height          =   3375
      Left            =   0
      TabIndex        =   15
      Top             =   1260
      Width           =   10575
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18653
      _ExtentY        =   5953
      _StockProps     =   79
      Caption         =   "Actuaciones citadas"
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdPendientes 
      Height          =   3450
      Left            =   0
      TabIndex        =   17
      Top             =   4680
      Width           =   10575
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18653
      _ExtentY        =   6085
      _StockProps     =   79
      Caption         =   "Actuaciones pendientes de recitar"
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCitasRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim blnPendientes As Boolean 'variable para saber en que grid estamos
Dim intDptoSel As Integer
Dim i As Integer
Dim strRecurso As String
Public strMensaje As String
Const const_consulta = 201
Private Sub pA�adirCitadas(i As Integer)
grdCitadas.AddItem (grdPendientes.Columns("CodPersona").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("NumActPlan").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("CodAct").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Tip. Act").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Fecha Cita").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Historia").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Paciente").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Actuacion").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Recurso").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("Cama").CellValue(grdPendientes.SelBookmarks(i)) & Chr(9) & _
             grdPendientes.Columns("CodRecurso").CellValue(grdPendientes.SelBookmarks(i)))
End Sub

Private Sub pVolverCitada(lngNumact As Long)
Dim sql As String
Dim qry As rdoQuery
sql = "UPDATE CI0100 SET CI01SITCITA = '1' "
sql = sql & " WHERE PR04NUMACTPLAN = " & lngNumact
sql = sql & " AND CI01SITCITA = '4'"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close
sql = "UPDATE PR0400 SET PR37CODESTADO=2 WHERE PR04NUMACTPLAN=" & lngNumact
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close
End Sub
Private Sub pOrdenar(ColIndex As Integer)
Dim strOrder As String
strOrder = fOrder(ColIndex)
If blnPendientes Then
    grdPendientes.RemoveAll
Else
    grdCitadas.RemoveAll
End If
If blnPendientes And chkPendientes.Value = 1 Then
Call pSelPendientes(strOrder)
Else
Call pSeleccionar(strOrder)
End If
Call pCargarGrid
End Sub

Private Function fOrder(ColIndex As Integer) As String
If blnPendientes Then
    Select Case ColIndex
        Case 4
            fOrder = " ORDER BY FECHAINICIAL"
        Case 5
            fOrder = " ORDER BY CI01FECUPD"
        Case 6
            fOrder = " ORDER BY CI22NUMHISTORIA"
        Case 7
            fOrder = " ORDER BY PAC"
        Case 8
            fOrder = " ORDER BY PR01DESCORTA"
        Case 9
            fOrder = " ORDER BY AG1100.AG11DESRECURSO"
    End Select
Else
    Select Case ColIndex
         Case 4
            fOrder = " ORDER BY FECHAINICIAL"
        Case 5
            fOrder = " ORDER BY CI22NUMHISTORIA"
        Case 6
            fOrder = " ORDER BY PAC"
        Case 7
            fOrder = " ORDER BY PR01DESCORTA"
        Case 8
            fOrder = " ORDER BY AG1100.AG11DESRECURSO"
    End Select
End If
End Function
Private Function fValidarEstado(numActPlan As Long) As Boolean
sql = "select pr04fecentrcola from pr0400 where pr04numactplan=" & numActPlan
Set rs = objApp.rdoConnect.OpenResultset(sql)
If Not IsNull(rs(0)) Then fValidarEstado = True
End Function
Private Sub pObRecurso(strRecurso As String)
 strRecurso = ""

    For i = 1 To lvwRecurso.ListItems.Count
        If lvwRecurso.ListItems(i).Selected Then
            If i = 1 Then Exit For            'TODOS
            strRecurso = strRecurso & lvwRecurso.ListItems(i).Tag & ","
        End If
    Next i
   If strRecurso = "" Then
      For i = 2 To lvwRecurso.ListItems.Count
        strRecurso = strRecurso & lvwRecurso.ListItems(i).Tag & ","
      Next i
    End If
     If strRecurso <> "" Then
        strRecurso = Left$(strRecurso, Len(strRecurso) - 1)
    End If
End Sub
Private Sub pImprimirCartas(lngActPlan As Long)
   Dim formulas(1, 2)
formulas(1, 1) = "motivo"
formulas(1, 2) = "'" & strMensaje & "'"

Call Imprimir_API("PR0465J.PR04NUMACTPLAN = " & lngActPlan, "CARTAS.RPT", formulas)
End Sub


Private Function fGrid() As SSDBGrid 'para saber en que grid esta la actuaci�n
 If grdPendientes.SelBookmarks.Count > 0 Then
        Set fGrid = grdPendientes
    Else
        If grdCitadas.SelBookmarks.Count > 0 Then
            Set fGrid = grdCitadas
        Else
            MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
            Set fGrid = Nothing
            Exit Function
        End If
End If

End Function

Private Sub pVaciarGrids()
    grdPendientes.RemoveAll
    grdCitadas.RemoveAll
End Sub
Private Sub pCargarRecursos(intDptoSel%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim Item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    
    sql = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AG11DESRECURSO"

    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rs.EOF Then
        lvwRecurso.Enabled = False
        cmdConsultar.Enabled = False
        Exit Sub
    Else
        cmdConsultar.Enabled = True
        lvwRecurso.Enabled = True
    End If
    lvwRecurso.ListItems.Clear
    Set Item = lvwRecurso.ListItems.Add(, , "TODOS")
    Item.Tag = 0
    Item.Selected = True
    Do While Not rs.EOF
        Set Item = lvwRecurso.ListItems.Add(, , rs!AG11DESRECURSO)
        Item.Tag = rs!AG11CODRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub


Private Sub pCargarGrid()
'If rs.EOF And blnPendientes = False Then
'    MsgBox "No Exite ninguna Cita ", vbInformation, Me.Caption
'    Exit Sub
'End If
 
 
If blnPendientes Then
    With grdPendientes
        Do While Not rs.EOF
            .AddItem (rs!CI21CODPERSONA & Chr(9) & _
                    rs!PR04NUMACTPLAN & Chr(9) & _
                    rs!PR01CODACTUACION & Chr(9) & _
                    rs!PR12CODACTIVIDAD & Chr(9) & _
                    Format(rs!FECHAINICIAL, "dd/mm/yyyy hh:mm") & Chr(9) & _
                    Format(rs!CI01FECUPD, "dd/mm/yyyy") & Chr(9) & _
                    rs!CI22NUMHISTORIA & Chr(9) & _
                    rs!PAC & Chr(9) & _
                    rs!PR01DESCORTA & Chr(9) & _
                    rs!AG11DESRECURSO & Chr(9) & _
                    rs!AD15CODCAMA & Chr(9) & _
                    rs!AG11CODRECURSO)
                rs.MoveNext
        Loop
    End With
    
Else
    With grdCitadas
        Do While Not rs.EOF
            .AddItem (rs!CI21CODPERSONA & Chr(9) & _
                    rs!PR04NUMACTPLAN & Chr(9) & _
                    rs!PR01CODACTUACION & Chr(9) & _
                    rs!PR12CODACTIVIDAD & Chr(9) & _
                    Format(rs!FECHAINICIAL, "dd/mm/yyyy hh:mm") & Chr(9) & _
                    rs!CI22NUMHISTORIA & Chr(9) & _
                    rs!PAC & Chr(9) & _
                    rs!PR01DESCORTA & Chr(9) & _
                    rs!AG11DESRECURSO & Chr(9) & _
                    rs!AD15CODCAMA & Chr(9) & _
                    rs!AG11CODRECURSO)
            rs.MoveNext
        Loop
    End With
End If

End Sub
Private Sub pSelPendientes(Optional strOrder As String)
grdPendientes.RemoveAll

sql = "SELECT CI2200.CI21CODPERSONA,PR0400.PR04NUMACTPLAN,"
sql = sql & "PR0400.PR01CODACTUACION,PR0100.PR12CODACTIVIDAD," 'ESTOS SON LOS CAMPOS QUE NO HAY QUE MOSTRAR
sql = sql & "CI0100.CI01FECCONCERT FECHAINICIAL,"
sql = sql & "CI0100.CI01FECUPD,"
sql = sql & "CI2200.CI22NUMHISTORIA,"
sql = sql & "CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PAC,"
sql = sql & "PR0100.PR01DESCORTA,AG1100.AG11DESRECURSO,AG1100.AG11CODRECURSO,CI0100.AD15CODCAMA"
sql = sql & " FROM CI0100,PR0400,CI2200,PR0100,AG1100"
sql = sql & " WHERE"
sql = sql & " CI0100.CI01FECCONCERT> =TO_DATE(?,'DD/MM/YYYY')"
sql = sql & " AND CI2200.CI21CODPERSONA=PR0400.CI21CODPERSONA"
sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
sql = sql & " AND CI0100.AG11CODRECURSO=AG1100.AG11CODRECURSO"
sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecurso & ")"
sql = sql & " AND AG1100.AD02CODDPTO=?"
sql = sql & " AND CI0100.CI01SITCITA=?"
If strOrder = "" Then
    sql = sql & " ORDER BY FECHAINICIAL"
Else
    sql = sql & strOrder
End If
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = dcboFecini.Text
qry(1) = cboDpto.Columns(0).Value
qry(2) = 4
Set rs = qry.OpenResultset()
Screen.MousePointer = vbDefault

End Sub
Private Sub pSeleccionar(Optional strOrder As String)
Screen.MousePointer = vbHourglass
sql = "SELECT CI2200.CI21CODPERSONA,PR0400.PR04NUMACTPLAN,"
sql = sql & "PR0400.PR01CODACTUACION,PR0100.PR12CODACTIVIDAD," 'ESTOS SON LOS CAMPOS QUE NO HAY QUE MOSTRAR
sql = sql & "CI0100.CI01FECCONCERT FECHAINICIAL,"
If blnPendientes Then
    sql = sql & "CI0100.CI01FECUPD,"
End If
sql = sql & "CI2200.CI22NUMHISTORIA,"
sql = sql & "CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PAC,"
sql = sql & "PR0100.PR01DESCORTA,AG1100.AG11DESRECURSO,AG1100.AG11CODRECURSO,CI0100.AD15CODCAMA"
sql = sql & " FROM CI0100,PR0400,CI2200,PR0100,AG1100"
sql = sql & " WHERE"
sql = sql & " CI0100.CI01FECCONCERT BETWEEN TO_DATE(?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY')"
sql = sql & " AND CI2200.CI21CODPERSONA=PR0400.CI21CODPERSONA"
sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
sql = sql & " AND CI0100.AG11CODRECURSO=AG1100.AG11CODRECURSO"
sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecurso & ")"
sql = sql & " AND AG1100.AD02CODDPTO=?"
sql = sql & " AND CI0100.CI01SITCITA=?"
If Not blnPendientes Then
    sql = sql & " AND PR0400.PR37CODESTADO IN (2,3)"
End If
If strOrder = "" Then
    sql = sql & " ORDER BY FECHAINICIAL"
Else
    sql = sql & strOrder
End If
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = dcboFecini.Text
qry(1) = CStr(DateAdd("d", 1, CDate(dcboFecFin.Text)))
qry(2) = cboDpto.Columns(0).Value
If blnPendientes Then
    qry(3) = 4
Else
    qry(3) = 1
End If
Set rs = qry.OpenResultset()
Screen.MousePointer = vbDefault


End Sub
Private Sub pFormatearGrids()
    With grdCitadas
        .Columns(0).Caption = "CodPersona"
        .Columns(0).Width = 0
        .Columns(0).Visible = False
        .Columns(1).Caption = "NumActPlan"
        .Columns(1).Width = 0
        .Columns(1).Visible = False
        .Columns(2).Caption = "CodAct"
        .Columns(2).Width = 0
        .Columns(2).Visible = False
        .Columns(3).Caption = "Tip. Act"
        .Columns(3).Width = 0
        .Columns(3).Visible = False
        .Columns(4).Caption = "Fecha Cita"
        .Columns(4).Width = 1600
        .Columns(4).Alignment = ssCaptionAlignmentCenter
        .Columns(5).Caption = "Historia"
        .Columns(5).Width = 800
        .Columns(5).Alignment = ssCaptionAlignmentCenter
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 3000
        .Columns(7).Caption = "Actuacion" 'descripcion de la Actuacion
        .Columns(7).Width = 2000
        .Columns(8).Caption = "Recurso"
        .Columns(8).Width = 2000
        .Columns(9).Caption = "Cama"
        .Columns(9).Visible = False
        .Columns(10).Caption = "CodRecurso"
        .Columns(10).Visible = False
    End With
    With grdPendientes
        .Columns(0).Caption = "CodPersona"
        .Columns(0).Width = 0
        .Columns(0).Visible = False
        .Columns(1).Caption = "NumActPlan"
        .Columns(1).Width = 0
        .Columns(1).Visible = False
        .Columns(2).Caption = "CodAct"
        .Columns(2).Width = 0
        .Columns(2).Visible = False
        .Columns(3).Caption = "Tip. Act"
        .Columns(3).Width = 0
        .Columns(3).Visible = False
        .Columns(4).Caption = "Fecha Cita"
        .Columns(4).Width = 1600
        .Columns(4).Alignment = ssCaptionAlignmentCenter
        .Columns(5).Caption = "Fecha en Pendiente"
        .Columns(5).Width = 1600
        .Columns(5).Alignment = ssCaptionAlignmentCenter
        .Columns(6).Caption = "Historia"
        .Columns(6).Width = 800
        .Columns(6).Alignment = ssCaptionAlignmentCenter
        .Columns(7).Caption = "Paciente"
        .Columns(7).Width = 3000
        .Columns(8).Caption = "Actuacion" 'descripcion de la Actuacion
        .Columns(8).Width = 2000
        .Columns(9).Caption = "Recurso"
        .Columns(9).Width = 2000
        .Columns(10).Caption = "Cama"
        .Columns(10).Visible = False
        .Columns(11).Caption = "CodRecurso"
        .Columns(11).Visible = False
    End With
End Sub


Private Sub cboDpto_Change()
    If intDptoSel <> cboDpto.Columns(0).Value Then Call pVaciarGrids
    intDptoSel = cboDpto.Columns(0).Value
    
    Call pCargarRecursos(intDptoSel)
    Call pObRecurso(strRecurso)
End Sub

Private Sub cboDpto_Click()
   If intDptoSel <> cboDpto.Columns(0).Value Then Call pVaciarGrids
    intDptoSel = cboDpto.Columns(0).Value
    
    Call pCargarRecursos(intDptoSel)
     Call pObRecurso(strRecurso)
End Sub

Private Sub chkPendientes_Click()
If chkPendientes.Value = 1 Then
    Call pSelPendientes
    blnPendientes = True
    Call pCargarGrid
    blnPendientes = False
Else
    grdPendientes.RemoveAll
End If
End Sub

Private Sub cmdAgendaRec_Click()
Dim Item As ListItem
Dim cont As Integer
Dim i As Integer
For i = 1 To lvwRecurso.ListItems.Count
  If lvwRecurso.ListItems(i).Selected Then
            If i = 1 Then
                Exit For            'TODOS
            Else
                cont = cont + 1
            End If
End If
Next
If cboDpto.Columns(0).Value <> 0 And cont = 1 Then
        Call objPipe.PipeSet("DPTO", cboDpto.Text)
        Call objPipe.PipeSet("COD_REC", lvwRecurso.SelectedItem.Tag)
        Call objPipe.PipeSet("REC", lvwRecurso.SelectedItem.Text)
        Screen.MousePointer = vbHourglass
        
    Else
        MsgBox "Esta opcion s�lo est� disponible para un s�lo recurso", vbExclamation, Me.Caption
        Exit Sub
End If
    Call objSecurity.LaunchProcess("AG1008")
End Sub

Private Sub cmdAnular_Click()
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim vnta                As Variant

  Dim blnAnularSolicitud As Boolean
  Dim lngNumActPlan As Long
  Dim NumCita As Byte
Dim rs1 As rdoResultset
  Dim blnAnular As Boolean
  Dim i As Integer
  Dim strSql As String
Dim rdPruebas As rdoResultset
Dim intRow   As Integer
Dim vntRowBookmark As Variant
Dim rsgrid As SSDBGrid
Dim intres As String
Dim lngActAnulada As Long
Dim lngPeticion As Long
Set rsgrid = fGrid
If rsgrid Is Nothing Then Exit Sub

If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If
If fValidarEstado(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "La actuacion no puede ser anulada porque ya tiene fecha de entrada en cola", vbExclamation, Me.Caption
    Exit Sub
End If
If Not fblnCitarModiAnular(CStr(intDptoSel), _
    rsgrid.Columns("Tip. Act").CellValue(rsgrid.SelBookmarks(0)), _
    rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "No tiene autorizacion a Modificar esta prueba"
    Exit Sub
End If
sql = "SELECT PR09NUMPETICION,PR0300.PR03NUMACTPEDI FROM PR0300,PR0400 WHERE PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND PR0400.PR04NUMACTPLAN=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))
Set rs = qry.OpenResultset()




sql = "SELECT COUNT(*) FROM PR0300,PR0400 WHERE PR09NUMPETICION=?"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0400.PR37CODESTADO in (2,1) and PR04FECENTRCOLA IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = rs(0)
Set rs1 = qry.OpenResultset()




If rs1(0) = 1 Then
    intres = MsgBox("Esta seguro de eliminar la peticion?", vbQuestion + vbYesNoCancel, Me.Caption)
    If intres = vbYes Then
    Call pAnular(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)), rsgrid.Columns("Cama").CellValue(rsgrid.SelBookmarks(0)))
'        lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
'            If lngincidencia <> -1 And lngincidencia <> 0 Then
               
     rsgrid.DeleteSelected       'End If
    End If
Else
   
    If Not rs.EOF Then
        Call objPipe.PipeSet("ACT", rs(1))
        Call objPipe.PipeSet("PET", rs(0))
        lngPeticion = rs(0)
           frmAnular.Show 1
           Set frmAnular = Nothing
          If objPipe.PipeExist("ACT") Then
            lngActAnulada = Val(objPipe.PipeGet("ACT"))
            If lngActAnulada = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) Then
                rsgrid.DeleteSelected
            End If
            objPipe.PipeRemove ("ACT")
          End If
          If objPipe.PipeExist("PET") Then
            'If lngPeticion = Val(objPipe.PipeGet("PET")) Then
             'lngPeticion = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) Then
                rsgrid.DeleteSelected
            'End If
            objPipe.PipeRemove ("PET")
          End If
    End If
End If

End Sub

Private Sub cmdCitar_Click()
Dim rsgrid As SSDBGrid
Dim sql As String
Dim rs As rdoResultset

Set rsgrid = fGrid
If rsgrid Is Nothing Then Exit Sub

If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If

If fValidarEstado(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "La actuacion no puede ser modificada porque ya tiene fecha de entrada en cola", vbInformation, Me.Caption
    Exit Sub
End If
If Not fblnCitarModiAnular(CStr(intDptoSel), _
    rsgrid.Columns("Tip. Act").CellValue(rsgrid.SelBookmarks(0)), _
    rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "No tiene autorizacion a Modificar esta cita", vbInformation, Me.Caption
    Exit Sub
End If
If rsgrid.Columns("Tip. Act").CellValue(rsgrid.SelBookmarks(0)) = const_consulta Then
    Call objSecurity.LaunchProcess("AG0211", rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)))
Else
      Call objSecurity.LaunchProcess("CI1150", rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)))
End If
Call pVaciarGrids
'Call pObRecurso
blnPendientes = False
Call pSeleccionar
Call pCargarGrid
blnPendientes = True
Call pSeleccionar
Call pCargarGrid
blnPendientes = False
End Sub

Private Sub cmdConsultar_Click()
Dim i%
chkPendientes.Value = 0
Call pVaciarGrids
  
blnPendientes = False

Call pSeleccionar
Call pCargarGrid
blnPendientes = True
Call pSeleccionar
Call pCargarGrid
blnPendientes = False
End Sub


Private Sub cmdHojaPacientes_Click()
 If grdPendientes.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation, Me.Caption
    Exit Sub
  End If
If grdPendientes.Rows = 0 Then
    MsgBox "No hay ning�n paciente pendiente de recitar.", vbExclamation, Me.Caption
    Exit Sub
End If
If InStr(2, strRecurso, ",") <> 0 Then
    MsgBox "Esta opcion s�lo est� disponible para un �nico recurso.", vbExclamation, Me.Caption
    Exit Sub
End If
  Dim strWhereTotal As String
    Dim strTipoHoja As String
    Dim intCont As Integer
    Dim strSql As String
    Dim rdprocAsis As rdoResultset
    Dim A�oIn As Variant
    Dim MesIn As Variant
    Dim DiaIn As Variant
    Dim A�oOut As Variant
    Dim MesOut As Variant
    Dim DiaOut As Variant
    Dim DateIN As Date
    Dim DTFEC As Date
        
    DateIN = DateAdd("d", -2, CDate(dcboFecini.Text))
    A�oIn = Format(CDate(dcboFecini.Text), "YYYY")
    MesIn = Format(CDate(dcboFecini.Text), "MM")
    DiaIn = Format(CDate(dcboFecini.Text), "DD")
    A�oOut = Format(dcboFecFin.Text, "YYYY")
    MesOut = Format(dcboFecFin.Text, "MM")
    DiaOut = Format(dcboFecFin.Text, "DD")
    DTFEC = DiaOut & "/" & MesOut & "/" & A�oOut
    DTFEC = DateAdd("d", 1, DTFEC)
    A�oOut = Format(DTFEC, "YYYY")
    MesOut = Format(DTFEC, "MM")
    DiaOut = Format(DTFEC, "DD")
    strWhereTotal = "PR0432j.AG11CODRECURSO_ASI=" & strRecurso & _
        " and PR0432j.CI01FECCONCERT BETWEEN TO_DATE('" & dcboFecini.Text & "' ,'DD/MM/YYYY') AND TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboFecFin.Text))) & "','DD/MM/YYYY')"
        
   Call Imprimir_API(strWhereTotal, "Pendientes.rpt")
  
End Sub

Private Sub cmdImprimirCarta_Click()
Dim i As Integer
Dim vntB As Variant
If grdCitadas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If grdCitadas.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If
For i = 0 To grdCitadas.SelBookmarks.Count - 1

    sql = "SELECT COUNT(*) FROM CI0100 WHERE PR04NUMACTPLAN=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = grdCitadas.Columns("NUMACTPLAN").CellValue(i)
    qry.Execute
    Set rs = qry.OpenResultset()
    If rs.EOF Then
        MsgBox "Esta opcion no esta disponible para esta actuaci�n porque no ha sido recitada", vbExclamation, Me.Caption
        Exit Sub
    End If
    rs.Close
Next
    frmMensaje.Show vbModal
    Set frmMensaje = Nothing
    If objPipe.PipeExist("ABORT") Then
        Call objPipe.PipeRemove("ABORT")
        Exit Sub
    End If
    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        vntB = grdCitadas.SelBookmarks(i)
        Call pImprimirCartas(grdCitadas.Columns("Numactplan").CellValue(grdCitadas.SelBookmarks(i)))
    Next i

End Sub

Private Sub cmdlistados_Click()
Dim strWhere As String
Dim formula(1, 2)
strWhere = " PR0409J.AD02CODDPTO= " & intDptoSel & " AND " & _
                            "CI01FECCONCERT >= TO_DATE('" & dcboFecini.Text & "','DD/MM/YYYY')" & _
                            " AND CI01FECCONCERT < TO_DATE('" & DateAdd("d", 1, CDate(dcboFecFin.Text)) & " ','DD/MM/YYYY')"
                
strWhere = strWhere + " AND PR0409J.AG11CODRECURSO IN (" & strRecurso & ")"
formula(1, 1) = "fecha"
formula(1, 2) = "date(" & Right(dcboFecini.Text, 4) & "," & Trim(Mid(dcboFecini.Text, 4, 2)) & "," & Trim(Left(dcboFecini.Text, 2)) & ")"
If optDiario.Value = True Then
    Call Imprimir_API(strWhere, "CI01R.rpt", formula)
End If
If optDSolicitante.Value = True Then
    Call Imprimir_API(strWhere, "CI03R.rpt", formula)
End If
    
If OptFechas.Value = True Then
    Call Imprimir_API(strWhere, "CI024.rpt", formula)
End If
End Sub


Private Sub cmdObser2_Click()
    If grdPendientes.SelBookmarks.Count > 1 Or grdCitadas.SelBookmarks.Count > 1 Then
        MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
        Exit Sub
      End If
    If grdPendientes.SelBookmarks.Count = 0 And grdCitadas.SelBookmarks.Count = 0 Then
        MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
        Exit Sub
    End If
    Call objSecurity.LaunchProcess("PR0510", fGrid.Columns("NumActPlan").CellValue(fGrid.SelBookmarks(0)))
End Sub

Private Sub cmdObservaciones_Click()
Dim i As Integer
If grdPendientes.SelBookmarks.Count > 1 Or grdCitadas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If grdPendientes.SelBookmarks.Count = 0 And grdCitadas.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If

For i = 0 To fGrid.SelBookmarks.Count - 1
Call objPipe.PipeSet("PR_NActPlan", fGrid.Columns("NumActPlan").CellValue(fGrid.SelBookmarks(i)))
 
 Call objSecurity.LaunchProcess("PR0502")
Next

End Sub

Private Sub cmdRecPlan_Click()
''Dim vntdata
''Dim strAct As String
''Dim cont As Integer
''Dim i As Integer
''If grdPendientes.SelBookmarks.Count = 0 Then
''    MsgBox "Debe seleccionar alguna actuaci�n pendiente de recitar", vbInformation, Me.Caption
''    Exit Sub
''End If
''For i = 1 To lvwRecurso.ListItems.Count
''  If lvwRecurso.ListItems(i).Selected Then
''            If i = 1 Then
''                Exit For            'TODOS
''            Else
''                cont = cont + 1
''            End If
''    End If
''Next
''If i = 1 Or cont > 1 Then
''    MsgBox "Esta opcion s�lo est� disponible para un �nico recurso", vbExclamation, Me.Caption
''    Exit Sub
''End If
''strAct = ""
''For i = 0 To grdPendientes.SelBookmarks.Count - 1
''    If Not fblnCitarModiAnular(CStr(intDptoSel), _
''        grdPendientes.Columns("Tip. Act").CellValue(grdPendientes.SelBookmarks(i)), _
''        grdPendientes.Columns("NumActPlan").CellValue(grdPendientes.SelBookmarks(i))) Then
''        MsgBox "No tiene autorizacion a Modificar las cita", vbInformation, Me.Caption
''        Exit Sub
''    End If
''Next
''For i = 0 To grdPendientes.SelBookmarks.Count - 1
''
''    strAct = strAct & grdPendientes.Columns("NumActPlan").CellValue(grdPendientes.SelBookmarks(i))
''    strAct = strAct & ","
''Next
''strAct = Left(strAct, Len(strAct) - 1)
''vntdata = strAct
''Call objPipe.PipeSet("CI2820_AD02CODDPTO", intDptoSel)
''Call objPipe.PipeSet("CI2820_AG11CODRECURSO", strRecurso)
''Call objPipe.PipeSet("CI2820_strPR04NUMACTPLAN", vntdata)
''Form1.Show vbModal
''Set Form1 = Nothing
''Call pVaciarGrids
''blnPendientes = False
''Call pSeleccionar
''Call pCargarGrid
''blnPendientes = True
''Call pSeleccionar
''Call pCargarGrid
''blnPendientes = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdVisionGlobal_Click()
If grdPendientes.SelBookmarks.Count > 1 Or grdCitadas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If grdPendientes.SelBookmarks.Count = 0 And grdCitadas.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If
Me.MousePointer = vbHourglass
ReDim vntData(1) As Variant
    For i = 0 To fGrid.SelBookmarks.Count - 1
        vntData(1) = fGrid.Columns("CODPERSONA").CellText(fGrid.SelBookmarks(i))
    Next i
    Call objSecurity.LaunchProcess("AD1126", vntData)
    Me.MousePointer = vbDefault
End Sub

Private Sub cmdVolverCitada_Click()
Dim rsgrid As SSDBGrid
If grdPendientes.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuaci�n pendiente de recitar", vbInformation, Me.Caption
    Exit Sub
End If
Dim cont As Integer
Dim i As Integer
For i = 1 To lvwRecurso.ListItems.Count
  If lvwRecurso.ListItems(i).Selected Then
            If i = 1 Then
                Exit For            'TODOS
            Else
                cont = cont + 1
            End If
    End If
Next
If i = 1 Or cont > 1 Then
    MsgBox "Esta opcion s�lo est� disponible para un �nico recurso", vbExclamation, Me.Caption
    Exit Sub
End If
For i = 0 To grdPendientes.SelBookmarks.Count - 1
    Call pVolverCitada(grdPendientes.Columns("NumActPlan").CellValue(grdPendientes.SelBookmarks(i)))
    Call pA�adirCitadas(i)
Next
grdPendientes.DeleteSelected
End Sub

Private Sub Form_Load()
Dim intDptoSel As Integer
 'se cargan los departamentos realizadores a los que tiene acceso el usuario
    sql = "SELECT AD02CODDPTO, AD02DESDPTO"
    sql = sql & " FROM AD0200"
    sql = sql & " WHERE AD02CODDPTO IN ("
    sql = sql & " SELECT AD02CODDPTO FROM AD0300"
    sql = sql & " WHERE SG02COD = ?"
    sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
    sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objSecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        'Me.Caption = strMeCaption & ". Dpto: " & cboDpto.Text & ". Recursos: TODOS"
        Do While Not rs.EOF
            cboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
   
    cboDpto.Text = cboDpto.Columns(1).Value
    intDptoSel = cboDpto.Columns(0).Value
    'se formatean los grids
    Call pFormatearGrids
    lvwRecurso.ColumnHeaders.Add , , , 2000

    Call pCargarRecursos(intDptoSel)
    Call pObRecurso(strRecurso)
   
End Sub


Private Sub grdCitadas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdCitadas_Click()
If grdPendientes.SelBookmarks.Count > 0 Then
    grdPendientes.SelBookmarks.RemoveAll
End If
End Sub


Private Sub grdCitadas_HeadClick(ByVal ColIndex As Integer)
    If grdCitadas.Rows > 1 Then
        blnPendientes = False
        Call pOrdenar(ColIndex)
        
    End If
End Sub

Private Sub grdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdPendientes_Click()
If grdCitadas.SelBookmarks.Count > 0 Then
    grdCitadas.SelBookmarks.RemoveAll
End If
End Sub

Private Sub grdPendientes_HeadClick(ByVal ColIndex As Integer)
    If grdPendientes.Rows > 1 Then
        blnPendientes = True
        Call pOrdenar(ColIndex)
        
    End If
End Sub

Private Sub lvwRecurso_Click()
Dim tot As Integer
Call pObRecurso(strRecurso)
tot = 0
For i = 1 To lvwRecurso.ListItems.Count
    If lvwRecurso.ListItems(i).Selected Then
        tot = tot + 1
    End If
Next
    If tot = 0 Then
    lvwRecurso.ListItems(1).Selected = True
    End If
Call pVaciarGrids
End Sub

