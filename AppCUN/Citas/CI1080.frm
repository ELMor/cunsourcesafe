VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmConsultaIntervenciones 
   Caption         =   "CITAS. Consulta de Intervenciones Citadas"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Salir"
      Height          =   375
      Index           =   1
      Left            =   10440
      TabIndex        =   16
      Top             =   8040
      Width           =   1215
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Consultar"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Top             =   8040
      Width           =   1215
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Intervenciones Citadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6975
      Index           =   3
      Left            =   0
      TabIndex        =   13
      Top             =   960
      Width           =   11775
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6525
         HelpContextID   =   2
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   11535
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         stylesets.count =   1
         stylesets(0).Name=   "Urgente"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "CI1080.frx":0000
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "Fecha"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Intervencion"
         Columns(1).Name =   "Intervencion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Quirofano"
         Columns(2).Name =   "Quirofano"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "Departamento"
         Columns(3).Name =   "Departamento"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "Dr. Solicitante"
         Columns(4).Name =   "Dr. Solicitante"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1535
         Columns(5).Caption=   "Historia"
         Columns(5).Name =   "Historia"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Caption=   "Paciente"
         Columns(6).Name =   "Paciente"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Cama"
         Columns(7).Name =   "Cama"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   20346
         _ExtentY        =   11509
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Dpto Realizador"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   2
      Left            =   9480
      TabIndex        =   11
      Top             =   120
      Width           =   2295
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Tag             =   "Departamento"
         Top             =   240
         Width           =   2130
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   7620
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   3757
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tip de Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   1
      Left            =   4560
      TabIndex        =   5
      Top             =   120
      Width           =   4815
      Begin VB.OptionButton optOption1 
         Caption         =   "Hosp."
         Height          =   255
         Index           =   2
         Left            =   1680
         TabIndex        =   8
         Top             =   240
         Width           =   855
      End
      Begin VB.OptionButton optOption1 
         Caption         =   "Amb."
         Height          =   255
         Index           =   1
         Left            =   960
         TabIndex        =   7
         Top             =   240
         Width           =   855
      End
      Begin VB.OptionButton optOption1 
         Caption         =   "Todos"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   3000
         TabIndex        =   10
         Tag             =   "Departamento"
         Top             =   240
         Width           =   1650
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   7620
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Planta"
         Height          =   195
         Index           =   0
         Left            =   2520
         TabIndex        =   9
         Top             =   240
         Width           =   450
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Rango de Fechas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   1
         Left            =   2760
         TabIndex        =   1
         Tag             =   "Fecha superior de las citas"
         Top             =   240
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   0
         Left            =   600
         TabIndex        =   2
         Tag             =   "Fecha superior de las citas"
         Top             =   240
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Index           =   1
         Left            =   2280
         TabIndex        =   4
         Top             =   240
         Width           =   420
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   465
      End
   End
End
Attribute VB_Name = "frmConsultaIntervenciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCommand1_Click(Index As Integer)
Select Case Index
    Case 0
        Screen.MousePointer = vbHourglass
            pInicilizarGrid
        Screen.MousePointer = vbDefault
    Case 1
      Unload Me
End Select

End Sub

Private Sub Form_Load()
dtcDateCombo1(0).Text = strFecha_Sistema
dtcDateCombo1(1).Text = dtcDateCombo1(0).Text
pCargarDepartamentos
End Sub

Private Sub pCargarDepartamentos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE"
sql = sql & " AD32CODTIPODPTO = 3 ORDER BY AD02DESDPTO"

Set rs = objApp.rdoConnect.OpenResultset(sql)
cboSSDBCombo1(0).AddItem "" & Chr$(9) & "TODAS"
Do While Not rs.EOF
    cboSSDBCombo1(0).AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
    rs.MoveNext
Loop
rs.Close

sql = "SELECT AD02CODDPTO, AD02DESDPTO FROM AD0200 WHERE"
sql = sql & " (AD32CODTIPODPTO = 1  OR AD32CODTIPODPTO=2) ORDER BY AD02DESDPTO"
Set rs = objApp.rdoConnect.OpenResultset(sql)

cboSSDBCombo1(1).AddItem "" & Chr$(9) & "TODAS"
Do While Not rs.EOF
    cboSSDBCombo1(1).AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
    rs.MoveNext
Loop
rs.Close
End Sub

Private Sub pInicilizarGrid()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim i As Integer

grdDBGrid1(0).RemoveAll

sql = "SELECT /*+ ORDERED INDEX(CI0100 CI0105) */ "
sql = sql & " TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI') FECHA,"
sql = sql & " AG1100.AG11DESRECURSO , PR0100.PR01DESCORTA, "
sql = sql & " AD0200.AD02DESDPTO, 'Dr. '||SG0200.SG02APE1||' '||SG0200.SG02APE2 DOCTOR,"
sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA, CI2200.CI22NUMHISTORIA,"
sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||' '||CI22NOMBRE NOMBRE"
sql = sql & " FROM CI0100, PR0400, PR0100, PR0300, PR0900, AG1100, AD0200,"
sql = sql & "  SG0200, AD1500,CI2200"
sql = sql & " WHERE CI0100.CI01FECCONCERT >=  TO_DATE(?,'DD/MM/YYYY')"
sql = sql & " AND CI0100.CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY')"
sql = sql & " AND CI0100.CI01SITCITA = '1'"
sql = sql & " AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO"
sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
sql = sql & " AND PR0400.PR37CODESTADO = 2"
sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
If cboSSDBCombo1(1).Columns(0).Value <> "" Then
    sql = sql & " AND PR0400.AD02CODDPTO = ? "
End If
If optOption1(2).Value = True Then 'Pacientes hospitalizados
    sql = sql & " AND AD1500.AD15CODCAMA IS NOT NULL"
    If cboSSDBCombo1(0).Columns(0).Value <> "" Then 'de una planta concreta
        sql = sql & " AND AD1500.AD02CODDPTO = ? "
    End If
End If
If optOption1(1).Value = True Then 'Pacientes Ambulatorios
    sql = sql & " AND AD1500.AD15CODCAMA IS  NULL"
End If
sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
sql = sql & " AND PR0100.PR12CODACTIVIDAD = 207"
sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
sql = sql & " AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION"
sql = sql & " AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND PR0900.SG02COD = SG0200.SG02COD"
sql = sql & " ORDER BY CI01FECCONCERT"
i = 2
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(dtcDateCombo1(0).Text, "dd/mm/yyyy")
    qry(1) = Format(DateAdd("d", 1, dtcDateCombo1(1).Text), "dd/mm/yyyy")
    If cboSSDBCombo1(1).Columns(0).Value <> "" Then
      qry(i) = cboSSDBCombo1(1).Columns(0).Value
      i = i + 1
    End If
    If cboSSDBCombo1(0).Columns(0).Value <> "" Then 'de una planta concreta
        qry(i) = cboSSDBCombo1(0).Columns(0).Value
    End If
Set rs = qry.OpenResultset()
Do While Not rs.EOF
    grdDBGrid1(0).AddItem rs!fecha & Chr$(9) _
                          & rs!PR01DESCORTA & Chr$(9) _
                          & rs!AG11DESRECURSO & Chr$(9) _
                          & rs!AD02DESDPTO & Chr$(9) _
                          & rs!DOCTOR & Chr$(9) _
                          & rs!CI22NUMHISTORIA & Chr$(9) _
                          & rs!Nombre & Chr$(9) _
                          & rs!Cama
    rs.MoveNext
Loop
End Sub

Private Sub optOption1_Click(Index As Integer)
If optOption1(2).Value = True Then
    cboSSDBCombo1(0).Enabled = True
Else
    cboSSDBCombo1(0).Enabled = False
    cboSSDBCombo1(0).Columns(0).Value = ""
    cboSSDBCombo1(0).Text = "TODAS"
End If
End Sub
