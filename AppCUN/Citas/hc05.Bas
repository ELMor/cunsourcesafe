Attribute VB_Name = "modCrystalReport"
Option Explicit
Declare Function PEPrintReport Lib "crpe32.dll" (ByVal RptName$, ByVal Printer%, ByVal Window%, ByVal Title$, ByVal Lft%, ByVal Top%, ByVal Wdth%, ByVal Height%, ByVal style As Long, ByVal PWindow As Long) As Integer
Declare Function PEOpenEngine Lib "crpe32.dll" () As Integer
Declare Sub PECloseEngine Lib "crpe32.dll" ()
Declare Function PECanCloseEngine Lib "crpe32.dll" () As Integer
Declare Function PEOpenPrintJob Lib "crpe32.dll" (ByVal RptName$) As Integer
Declare Function PEClosePrintJob Lib "crpe32.dll" (ByVal printJob%) As Integer
Declare Function PEStartPrintJob Lib "crpe32.dll" (ByVal printJob%, ByVal WaitOrNot%) As Integer
Declare Function PEGetErrorCode Lib "crpe32.dll" (ByVal printJob%) As Integer
Declare Function PEGetErrorText Lib "crpe32.dll" (ByVal printJob%, TextHandle As Long, TextLength%) As Integer
Declare Function PEGetHandleString Lib "crpe32.dll" (ByVal TextHandle As Long, ByVal Buffer$, ByVal BufferLength%) As Integer
Declare Function PEOutputToPrinter Lib "crpe32.dll" (ByVal printJob As Integer, ByVal nCopies As Integer) As Integer
Declare Function PEOutputToWindow Lib "crpe32.dll" (ByVal printJob%, ByVal Title$, ByVal Left As Long, ByVal Top As Long, ByVal Width As Long, ByVal Height As Long, ByVal style As Long, ByVal PWindow As Long) As Integer
Declare Function PEGetNthTableLogOnInfo Lib "crpe32.dll" (ByVal printJob%, ByVal TableN%, LogOnInfo As PELogOnInfo) As Integer
Declare Function PESetNthTableLogOnInfo Lib "crpe32.dll" (ByVal printJob%, ByVal TableN%, LogOnInfo As PELogOnInfo, ByVal Propagate%) As Integer
Declare Function PESetSQLQuery Lib "crpe32.dll" (ByVal printJob%, ByVal QueryString$) As Integer
Declare Function PEGetSQLQuery Lib "crpe32.dll" (ByVal printJob%, TextHandle As Long, TextLength%) As Integer
Declare Function PELogOnSQLServerWithPrivateInfo Lib "crpe32.dll" (ByVal DLLName$, ByVal PrivateInfo As Long) As Integer
Declare Function PESetFormula Lib "crpe32.dll" (ByVal printJob%, ByVal FormulaName$, ByVal formulaString$) As Integer
Declare Function PESetSelectionFormula Lib "crpe32.dll" (ByVal printJob%, ByVal formulaString$) As Integer

Global Const PE_SERVERNAME_LEN = 128
Global Const PE_DATABASENAME_LEN = 128
Global Const PE_USERID_LEN = 128
Global Const PE_PASSWORD_LEN = 128
Global Const PE_SIZEOF_LOGON_INFO = 514  ' # bytes in PELogOnInfo

Global Const WS_MINIMIZE = 536870912
Global Const WS_VISIBLE = 268435456
Global Const WS_DISABLED = 134217728
Global Const WS_CLIPSIBLINGS = 67108864
Global Const WS_CLIPCHILDREN = 33554432
Global Const WS_MAXIMIZE = 16777216
Global Const WS_CAPTION = 12582912
Global Const WS_BORDER = 8388608
Global Const WS_DLGFRAME = 4194304
Global Const WS_VSCROLL = 2097152
Global Const WS_HSCROLL = 1048576
Global Const WS_SYSMENU = 524288
Global Const WS_THICKFRAME = 262144
Global Const WS_MINIMIZEBOX = 131072
Global Const WS_MAXIMIZEBOX = 65536
Global Const CW_USEDFAULT = -32768

Type PELogOnInfo
    StructSize As Integer
    ServerName As String * PE_SERVERNAME_LEN
    DatabaseName  As String * PE_DATABASENAME_LEN
    UserID As String * PE_USERID_LEN
    Password  As String * PE_PASSWORD_LEN
End Type
'Global ErrorCode As Integer

Public Function GetErrorString(Jobnum As Long, cError As Long) As String
Dim TextHandle As Long
Dim TextLength As Integer
Dim ErrorString As String
Dim result%
  
  cError = PEGetErrorCode(Jobnum)
  result% = PEGetErrorText(Jobnum, TextHandle, TextLength)
  ErrorString = String$(TextLength + 1, " ")
  result% = PEGetHandleString(TextHandle, ErrorString, TextLength)
  GetErrorString = ErrorString
  
End Function

Public Function Imprimir_API(sql As String, report As String, Optional Formula As Variant, Optional Caption As String, Optional rdoCon As Variant, Optional Impresora As Integer) As Integer
Dim job As Long, nerr As Long, donde As Long
Dim txtError As Variant, cError As Long
Dim LogOnInfo As PELogOnInfo
Dim sqlIni As String, mostrar As String, queda As String, resto As String, orden As String
Dim TxtHndl As Long
Dim Txtlen As Integer, i As Integer
Dim rdoConnect As rdoConnection

  ' Se pasa la conexi�n
  If IsMissing(rdoCon) Then
    Set rdoConnect = objApp.rdoConnect
  Else
    Set rdoConnect = rdoCon
  End If
  ' Se abre un trabajo nuevo (se abre el report)
  job = PEOpenPrintJob(objApp.strReportsPath & report)
  ' Se pasa el handle de la conexi�n a Crystal
  If PELogOnSQLServerWithPrivateInfo("PDSODBC.DLL", rdoConnect.hDbc) = False Then GoTo crError
  ' Se captura la sql del report (variable sqlIni)
  If PEGetSQLQuery(job, TxtHndl, Txtlen) = False Then GoTo crError
  sqlIni = String(Txtlen, " ")
  If PEGetHandleString(TxtHndl, sqlIni, Txtlen) = False Then GoTo crError

  ' Se modifica la sql a�adiendo el Where que se pase por ventana (variable sql)
  donde = InStr(sqlIni, "ORDER BY")
  If donde > 0 Then
    orden = Right(sqlIni, Len(sqlIni) - donde + 1)
    If InStr(sqlIni, "WHERE") > 0 Then
      sqlIni = Left(sqlIni, donde - 3) & " AND "
    Else
      sqlIni = Left(sqlIni, donde - 3) & " WHERE "
    End If
    sql = sqlIni & sql & Chr$(13) & Chr$(9) & orden
  Else
    sqlIni = Left(sqlIni, Len(sqlIni) - 1)
    If InStr(sqlIni, "WHERE") > 0 Then
      sql = sqlIni & " AND " & Chr$(13) & Chr$(9) & sql & Chr$(0)
    Else
      sql = sqlIni & " WHERE " & Chr$(13) & Chr$(9) & sql & Chr$(0)
    End If
  End If

  ' Si se pasan f�rmulas, se ponen en el report
  ' Variable Formula: matriz de dos dimensiones, 1�: Nombre de la f�rmula; 2�: Contenido
  If Not IsMissing(Formula) Then
    For i = 1 To UBound(Formula, 1)
      If PESetFormula(job, Formula(i, 1) & Chr$(0), Formula(i, 2)) = False Then GoTo crError
    Next i
  End If
  ' Se lanza la sql al report
  If PESetSQLQuery(job, sql) = False Then GoTo crError
  ' Se muestra el report, ya sea en vantana o a la impresora
  If Impresora = True Then
    If PEOutputToPrinter(job, 1) = False Then GoTo crError
  Else
    If PEOutputToWindow(job, Caption, 0, 0, 500, 500, WS_SYSMENU + WS_MAXIMIZE, 0) = False Then GoTo crError
  End If
  If PEStartPrintJob(job, True) = False Then GoTo crError
  ' Se cierra el trabajo (importante)
  Call PEClosePrintJob(job)
  Exit Function

crError:
  Call PEClosePrintJob(job)
  txtError = GetErrorString(job, cError) ' Se muestra un mensaje de error
  MsgBox Str(cError) + " - " + txtError
  Exit Function

End Function





