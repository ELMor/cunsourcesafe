VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBuscaPerJur 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "B�squedad de persona jur�dicas"
   ClientHeight    =   4800
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7470
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4800
   ScaleWidth      =   7470
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraBusqueda 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1395
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   5895
      Begin VB.TextBox txtCodPer 
         Height          =   285
         Left            =   1260
         TabIndex        =   0
         Top             =   240
         Width           =   1395
      End
      Begin VB.TextBox txtRazonSocial 
         Height          =   285
         Left            =   1260
         TabIndex        =   2
         Top             =   960
         Width           =   4515
      End
      Begin VB.TextBox txtCif 
         Height          =   285
         Left            =   1260
         TabIndex        =   1
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4800
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Raz�n Social:"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   10
         Top             =   1020
         Width           =   990
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Cif:"
         Height          =   195
         Index           =   0
         Left            =   960
         TabIndex        =   9
         Top             =   660
         Width           =   225
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Cod. Persona:"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   8
         Top             =   300
         Width           =   1005
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   5940
      TabIndex        =   4
      Top             =   120
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Personas localizadas"
      ForeColor       =   &H00C00000&
      Height          =   3915
      Left            =   0
      TabIndex        =   5
      Top             =   1440
      Width           =   7545
      Begin SSDataWidgets_B.SSDBGrid grdPersonas 
         Height          =   3045
         Left            =   60
         TabIndex        =   6
         Top             =   180
         Width           =   7365
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   12991
         _ExtentY        =   5371
         _StockProps     =   79
      End
   End
End
Attribute VB_Name = "frmBuscaPerJur"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub pformateargrid()
    Dim i%
    With grdPersonas
        .Columns(0).Caption = "Cod. Persona"
        .Columns(0).Width = 900
        .Columns(1).Caption = "CIF"
        .Columns(1).Width = 900
        .Columns(1).Alignment = ssCaptionAlignmentRight
        .Columns(2).Caption = "Raz�n Social"
        .Columns(2).Width = 3000
        .Columns(3).Caption = "Direccion"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "Localidad"
        .Columns(4).Width = 2000
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub cmdBuscar_Click()
Dim str As String
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sqlWhere As String
Dim i As Integer
Dim cllBuscar As New Collection
Screen.MousePointer = vbHourglass
grdPersonas.RemoveAll
If txtCif.Text = "" And txtCodPer = "" And txtRazonSocial = "" Then
    MsgBox "Debe indicar alg�n criterio de b�squeda", vbExclamation, Me.Caption
    Exit Sub
End If
If txtCif.Text <> "" Then
    sqlWhere = " UPPER(CI23CIF) LIKE ? AND"
    i = i + 1
    cllBuscar.Add "%" & UCase(Trim(txtCif.Text)) & "%"
End If
If txtRazonSocial.Text <> "" Then
    sqlWhere = sqlWhere & " UPPER(CI23RAZONSOCIAL) LIKE ? AND"
    cllBuscar.Add "%" & UCase(Trim(txtRazonSocial.Text)) & "%"
End If
If txtCodPer.Text <> "" Then
    sqlWhere = sqlWhere & " CI2300.CI21CODPERSONA=? AND"
    cllBuscar.Add Trim(txtCodPer.Text)
End If
sqlWhere = Left(sqlWhere, Len(sqlWhere) - 4)
sql = "SELECT CI2300.CI21CODPERSONA,CI23CIF,CI23RAZONSOCIAL,"
sql = sql & " CI10CALLE||' '||CI10PORTAL||' '||CI10RESTODIREC DIRECCION,"
sql = sql & " CI10DESLOCALID FROM CI1000,CI2300"
sql = sql & " WHERE "
sql = sql & " CI1000.CI21CODPERSONA=CI2300.CI21CODPERSONA "
sql = sql & " AND CI1000.CI10INDDIRPRINC=-1 AND"
sql = sql & sqlWhere
Set qry = objApp.rdoConnect.CreateQuery("", sql)
For i = 1 To cllBuscar.Count
    qry(i - 1) = cllBuscar(i)
Next
Set rs = qry.OpenResultset
If rs.EOF Then
    MsgBox "No se ha encontrado ning�n registro coincidente", vbInformation, Me.Caption
    Screen.MousePointer = vbDefault
    Exit Sub
Else
    Do While Not rs.EOF
        grdPersonas.AddItem rs!CI21CODPERSONA & Chr(9) & _
                            rs!CI23CIF & Chr(9) & _
                            rs!CI23RAZONSOCIAL & Chr(9) & _
                            rs!DIRECCION & Chr(9) & _
                            rs!CI10DESLOCALID
        rs.MoveNext
    Loop
End If
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdSalir_Click()
If grdPersonas.SelBookmarks.Count = 1 Then
    Call objPipe.PipeSet("CI_CI21CODPERSONA", grdPersonas.Columns(0).Value)
End If
Unload Me
End Sub

Private Sub Form_Load()
Call pformateargrid
End Sub


Private Sub grdPersonas_DblClick()
If grdPersonas.SelBookmarks.Count = 1 Then
    Call objPipe.PipeSet("CI_CI21CODPERSONA", grdPersonas.Columns(0).Value)
Unload Me
End If

End Sub

