Attribute VB_Name = "Module1"
Option Explicit
'Global dbCUN As rdoConnection

Public objCW As Object    ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
Public objMouse As Object
Public objSecurity As Object
Public objMRes As Object

Public Function llenar(strnombre As Variant, _
intLength As Integer, strCh As String, blnIzq As Boolean) As String
Dim strTextError As String
Dim i As Integer, j As Integer

If intLength = -1 Then
llenar = ""
Else
If Len(strCh) <> 1 Then
    strTextError = "Car�cter de relleno no v�lido"
'    MsgBox strTextError, vbCritical, "Error en Pac.llenar"
    Exit Function
End If

If IsNull(strnombre) Then strnombre = strCh


If Len(strnombre) > intLength Then
    
    If blnIzq Then
        llenar = Mid(strnombre, Len(strnombre) - intLength + 1)
    Else
        llenar = Mid(strnombre, 1, intLength)
    End If
Else
    j = intLength - Len(strnombre)
        
    llenar = ""
    For i = 1 To j
            llenar = llenar & strCh
    Next i
        
    If blnIzq = True Then
            llenar = llenar & strnombre
        Else
            llenar = strnombre & llenar
    End If

End If
End If

End Function


