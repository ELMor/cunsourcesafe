VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Los valores deben coincidir con el nombre del archivo en disco
Const CIRepCentroUniversidad     As String = "CI0001"
Const CIRepTipoPoliza            As String = "CI0016"
Const CIRepColectivos            As String = "CI0002"
Const CIRepEntidadColab          As String = "CI0004"
Const CIRepMotivoDescuento       As String = "CI0006"
Const CIRepRelTitular            As String = "CI0014"
Const CIRepConciertos            As String = "CI0003"
Const CIRepTipoEconomico         As String = "CI0015"
Const CIRepPersonalUni           As String = "CI0011"
'Const CIRepACUNSA                As String = "CI0010"
Const CIRepEstadoCivil           As String = "CI0005"
Const CIRepProfesion             As String = "CI0012"
Const CIRepTratamiento           As String = "CI0017"
Const CIRepVinculoFamiliar       As String = "CI0018"
Const CIRepPais                  As String = "CI0007"
Const CIRepProvincia             As String = "CI0013"
Const CIRepPersFisicas           As String = "CI0009"
Const CIRepPersJuridicas         As String = "CI0010"
'Const CIRepAvisos                As String = "CI0017"
Const CIRepPatologias            As String = "CI0008"
Const CIRepRecordatorio          As String = "CI0021"
Const CIRepRecordatorioMano      As String = "CI0022"
Const CIRepCitasConfirmadas      As String = "CI0019"
Const CIRepCitasGestionadas      As String = "CI0020"
Const CIRepRelacionesUDN         As String = "CI0023"


Const CIWinHospitalizaciones     As String = "CI0182"
Const CIWinTipoPoliza            As String = "CI1001"
Const CIWinColectivos            As String = "CI1002"
Const CIWinEntidadColab          As String = "CI1003"
Const CIWinCentroUniversidad     As String = "CI1004"
Const CIWinMotivoDescuento       As String = "CI1005"
Const CIWinRelTitular            As String = "CI1006"
Const CIWinConciertos            As String = "CI1007"
Const CIWinTipoEconomico         As String = "CI1008"
Const CIWinPersonalUni           As String = "CI1009"
Const CIWinACUNSA                As String = "CI1010"
Const CIWinEstadoCivil           As String = "CI1011"
Const CIWinProfesion             As String = "CI1012"
Const CIWinTratamiento           As String = "CI1013"
Const CIWinVinculoFamiliar       As String = "CI1014"
Const CIWinPais                  As String = "CI1015"
Const CIWinProvincia             As String = "CI1016"
Const CIWinPersJuridicas         As String = "CI1018"
Const CIWinPatologias            As String = "CI1020"
Const CIWinCargaMunicip          As String = "CI1021"
Const CIWinCargaTextos           As String = "CI1022"
Const CIWinRecordatorio          As String = "CI1023"
Const CIWinRecitacion            As String = "CI1029"
Const CIWinDocAutorizacion       As String = "CI1035"
Const CIWinRelacionUDN           As String = "CI1037"
Const CICitaAgenda               As String = "CI1048"

Const CICitaPautacion            As String = "CI1060"
Const CIPruebasPlanta            As String = "CI1070"
Const CICitasQuirofano           As String = "CI1072"
Const CiProcesosAbiertos         As String = "CI1073"
Const CiProcAsisAbiertos         As String = "CI1074"
Const CiConsultaIntervenciones   As String = "CI1080"
Const CICitaManualFI             As String = "CI1147"
Const CICitaManualMultiple       As String = "CI1148"
Const CICitaManual2FI            As String = "CI1150"
Const CIWinCitaRecurso           As String = "CI2028"
Const CIIngresosNW               As String = "CI2046"
Const CIWinCitasPaciente1        As String = "CI3024"
Const CIWinHojaFiliacion         As String = "CI4017"
Const CIWPersonasJuridicasNW     As String = "CI2018"
Const CIWinIndSanitarios          As String = "CI2052"


' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
    
    Case CIWinRelacionUDN
      'Call objSecurity.AddHelpContext(1)
      Call RelacionUDN
      Call objSecurity.RemoveHelpContext
    
    Case CIWinTratamiento
      Call objSecurity.AddHelpContext(1)
      Call Tratamientos
      Call objSecurity.RemoveHelpContext
      
    Case CIWinEstadoCivil
      Call objSecurity.AddHelpContext(2)
      Call Estados_Civiles
      Call objSecurity.RemoveHelpContext
      
    Case CIWinVinculoFamiliar
      Call objSecurity.AddHelpContext(3)
      Call V�nculos_Familiares
      Call objSecurity.RemoveHelpContext
      
    Case CIWinProfesion
      Call objSecurity.AddHelpContext(4)
      Call Profesiones
      Call objSecurity.RemoveHelpContext
      
    Case CIWinCentroUniversidad
      Call objSecurity.AddHelpContext(5)
      Call Centros_Cargo
      Call objSecurity.RemoveHelpContext
      
    Case CIWinColectivos
      Call objSecurity.AddHelpContext(6)
      Call Colectivos
      Call objSecurity.RemoveHelpContext
      
    Case CIWinACUNSA
      Call objSecurity.AddHelpContext(19)
      Load frmAsegurados
      Call frmAsegurados.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmAsegurados
      Set frmAsegurados = Nothing
      
    Case CIWinTipoEconomico
      Call objSecurity.AddHelpContext(152)
      Load frmTipoEcon
      Call frmTipoEcon.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmTipoEcon
      Set frmTipoEcon = Nothing
    
    Case CIWinHojaFiliacion
      If Not IsMissing(vntData) Then Call objPipe.PipeSet("CODPERS", vntData)
      frmHojaFiliacion.Show vbModal
      Set frmHojaFiliacion = Nothing
      Call objPipe.PipeSet("CodPersona", lngCodPers)

   Case CIIngresosNW
      Load frmIngresosNW
      Call frmIngresosNW.Show(vbModal)
      Set frmIngresosNW = Nothing
    Case CIWinTipoPoliza
      Call objSecurity.AddHelpContext(7)
      Call Tipo_Poliza
      Call objSecurity.RemoveHelpContext
      
    Case CIWinEntidadColab
      Call objSecurity.AddHelpContext(8)
      Call Entidad_Colaboradora
      Call objSecurity.RemoveHelpContext
      
    Case CIWinMotivoDescuento
      Call objSecurity.AddHelpContext(9)
      Call Motivo_Descuento
      Call objSecurity.RemoveHelpContext
      
    Case CIWinRelTitular
      Call objSecurity.AddHelpContext(12)
      Call Relaci�n_Titular
      Call objSecurity.RemoveHelpContext
      
    Case CIWinConciertos
      Call objSecurity.AddHelpContext(160)
      Load frmConcier
      Call frmConcier.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmConcier
      Set frmConcier = Nothing
      
    Case CIWinPersonalUni
      Call objSecurity.AddHelpContext(24)
      Load frmPersoUni
      Call frmPersoUni.Show(vbModal)
      'Unload frmPersoUni
      Set frmPersoUni = Nothing
      Call objSecurity.RemoveHelpContext
      
    Case CIWinPais
      Call objSecurity.AddHelpContext(14)
      Call Paises
      Call objSecurity.RemoveHelpContext
      
    Case CIWinProvincia
      Call objSecurity.AddHelpContext(68)
      Load frmProvMuniLoc
      Call frmProvMuniLoc.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmProvMuniLoc
      Set frmProvMuniLoc = Nothing
      
    Case CIWinPersJuridicas
      Call objSecurity.AddHelpContext(144)
      Load frmPersJur�dicas
      Call frmPersJur�dicas.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmPersJur�dicas
      Set frmPersJur�dicas = Nothing
      
    Case CIWinPatologias
      Call objSecurity.AddHelpContext(34)
      Load frmPatActu
      Call frmPatActu.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmPatActu
      Set frmPatActu = Nothing
      
    Case CIWinCargaMunicip
      Call objSecurity.AddHelpContext(47)
      Load frmCargaMunicipios
      Call frmCargaMunicipios.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCargaMunicipios
      Set frmCargaMunicipios = Nothing
      
     Case CIWinCargaTextos
      Call objSecurity.AddHelpContext(47)
      Load frmCargaTextos
      Call frmCargaTextos.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCargaTextos
      Set frmCargaTextos = Nothing
      
    Case CIWinRecordatorio
      'Call objSecurity.AddHelpContext(1)
      If vntData = Empty Then
        Load frmRecordatorio
        Call frmRecordatorio.Show(vbModal)
        'Unload frmRecordatorio
        Set frmRecordatorio = Nothing
      Else
        Load frmRecordatorio
        With frmRecordatorio
          .lngNumSolicit = Val(vntData)
          .dtcDateCombo1(0).Date = ""
          .dtcDateCombo1(1).Date = ""
          .dtcDateCombo1(0).BackColor = objApp.objColor.lngReadOnly
          .dtcDateCombo1(1).BackColor = objApp.objColor.lngReadOnly
          .dtcDateCombo1(0).Enabled = False
          .dtcDateCombo1(1).Enabled = False
          .txtText1(0).BackColor = objApp.objColor.lngReadOnly
          .txtText1(1).BackColor = objApp.objColor.lngReadOnly
          .txtText1(2).BackColor = objApp.objColor.lngReadOnly
          .txtText1(4).BackColor = objApp.objColor.lngReadOnly
          .chkCheck1(0).Enabled = False
          .chkCheck1(1).Enabled = False
          .cmdCommand1(0).Enabled = False
          .cmdCommand1(1).Enabled = False
          .cmdCommand1(2).Enabled = False
          .cboSSDBCombo1(0).BackColor = objApp.objColor.lngReadOnly
          .cboSSDBCombo1(0).Enabled = False
          .fraFrame1(0).Enabled = False
          .fraFrame1(2).Enabled = False
          .Show vbModal
        End With
        'Unload frmRecordatorio
        Set frmRecordatorio = Nothing
      End If
     'Call objSecurity.RemoveHelpContext
     
     Case CIWinCitasPaciente1
      Load frmCitasPaciente1
      Call frmCitasPaciente1.Show(vbModal)
      Set frmCitasPaciente1 = Nothing
     
     Case CIWinRecitacion
      ' Call objSecurity.AddHelpContext(47)
       Load frmRecitaCion
       frmRecitaCion.Show vbModal
       'Unload frmRecitaCion
       Set frmRecitaCion = Nothing
     'Call objSecurity.RemoveHelpContext
      
    Case CIWinCitaRecurso
      frmCitasRecurso.Show vbModal
      Set frmCitasRecurso = Nothing
    
     Case CIRepCitasConfirmadas
      Call PrintReport(CIRepCitasConfirmadas)

     Case CIRepCitasGestionadas
      Call PrintReport(CIRepCitasGestionadas)
    Case CICitaAgenda
      'vntData(1) codigo del paciente
      'vntData(2) codigo de actuacion planificada
      'vntData(3) descripcion de la actuacion
      lngPaciente = vntData(1)
      lngActuacion = vntData(2)
      strDesAct = vntData(3)
      Load frmCitasAgenda
      Call frmCitasAgenda.Show(vbModal)
      Unload frmCitasAgenda
      lngPaciente = 0
      lngActuacion = 0
      strDesAct = ""
      Set frmCitasAgenda = Nothing
    Case CICitaPautacion
       Load frmPautacion
       frmPautacion.Show vbModal
       Set frmPautacion = Nothing
       
    Case CiConsultaIntervenciones
        Load frmConsultaIntervenciones
        frmConsultaIntervenciones.Show vbModal
        Set frmConsultaIntervenciones = Nothing
    Case CIPruebasPlanta
        strDpto = vntData
        Load frmPlantas
        frmPlantas.Show vbModal
        strDpto = ""
        Set frmPlantas = Nothing
    Case CICitasQuirofano
        frmCitasQuirofano.Show vbModal
        Set frmCitasQuirofano = Nothing
    Case CIWinHospitalizaciones
      Load frmMovPacCamas
      Call frmMovPacCamas.Show(vbModal)
      Set frmMovPacCamas = Nothing
    Case CiProcesosAbiertos
      'vntData (0) departamento
      'vntData(1) recurso
      'vntdata(2) numero de actuacion pedida
      'vntdata(3) Codigo del paciente
      Call objPipe.PipeSet("DPTO", vntData(0))
      Call objPipe.PipeSet("RECURSO", vntData(1))
      Call objPipe.PipeSet("PR03NUMACTPEDI", vntData(2))
      Call objPipe.PipeSet("CI21CODPERSONA", vntData(3))
      Load FRMProcesos
      FRMProcesos.Show vbModal
      Set FRMProcesos = Nothing
    
    Case CICitaManual2FI
        Call objPipe.PipeSet("CI1150_PR04NUMACTPLAN", vntData)
        frmCitaManual2FI.Show vbModal
        Set frmCitaManual2FI = Nothing
    
    Case CICitaManualFI
      'Si se han pasado par�metros
      If Not IsMissing(vntData) Then
        'Si el form llamador es el de Admision Nuevo Paciente (AD2105), crea una pipe
        'donde se pone el valor pasado como primer par�metro (c�digo persona)
        If Trim(vntData(1)) <> "" Then
          Call objPipe.PipeSet("CODPERSONA_FROM_AD2105", vntData(1))
        End If
      End If

      Load frmCitaManualFI
      Call frmCitaManualFI.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCitasRecursos
      Set frmCitaManualFI = Nothing
 
    
    Case CICitaManualMultiple
      Load frmCitaMultiple
      Call frmCitaMultiple.Show(vbModal)
      Set frmCitaMultiple = Nothing
      
    Case CiProcAsisAbiertos
      'vntdata(1) numero de actuacion pedida
      'vntdata(2) Codigo del paciente
      Call objPipe.PipeSet("PR03NUMACTPEDI", vntData(1))
      Call objPipe.PipeSet("CI21CODPERSONA", vntData(2))
      Load frmProcesosAsistencia
      frmProcesosAsistencia.Show vbModal
      Set frmProcesosAsistencia = Nothing
      
      Case CIWinIndSanitarios
    
      Call objPipe.PipeSet("Actuacion", vntData)
      Load frmSanitarios2
      frmSanitarios2.Show vbModal
      Set frmSanitarios2 = Nothing
      
      Case CIWPersonasJuridicasNW
        Load frmPerJurNW
        frmPerJurNW.Show vbModal
        Set frmPerJurNW = Nothing
'--------
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n�mero de procesos
  ReDim aProcess(1 To 71, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = CIWinTipoPoliza
  aProcess(1, 2) = "Mantenimiento de Tipos de Poliza"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = CIWinColectivos
  aProcess(2, 2) = "Mantenimiento de Colectivos"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = CIWinEntidadColab
  aProcess(3, 2) = "Mantenimiento de Entidades Colaboradoras"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = CIWinCentroUniversidad
  aProcess(4, 2) = "Mantenimiento de Centros Cargo"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = CIWinMotivoDescuento
  aProcess(5, 2) = "Mantenimiento de Motivos de Descuento"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = CIWinRelTitular
  aProcess(6, 2) = "Mantenimiento de Relaciones del Titular"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = CIWinConciertos
  aProcess(7, 2) = "Mantenimiento de Conciertos Econ�micos"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = CIWinTipoEconomico
  aProcess(8, 2) = "Mantenimiento de Tipos Econ�micos"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = CIWinPersonalUni
  aProcess(9, 2) = "Mantenimiento de Personal Universitario"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = CIWinACUNSA
  aProcess(10, 2) = "Mantenimiento de Asegurados ACUNSA"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
     
  aProcess(11, 1) = CIWinEstadoCivil
  aProcess(11, 2) = "Mantenimiento de Estados Civiles"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
      
  aProcess(12, 1) = CIWinProfesion
  aProcess(12, 2) = "Mantenimiento de Profesiones"
  aProcess(12, 3) = True
  aProcess(12, 4) = cwTypeWindow
      
  aProcess(13, 1) = CIWinTratamiento
  aProcess(13, 2) = "Mantenimiento de Tratamientos"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
      
  aProcess(14, 1) = CIWinVinculoFamiliar
  aProcess(14, 2) = "Mantenimiento de V�nculos Familiares"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
      
  aProcess(15, 1) = CIWinPais
  aProcess(15, 2) = "Mantenimiento de Pa�ses"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
      
  aProcess(16, 1) = CIWinProvincia
  aProcess(16, 2) = "Mantenimiento de Provincias-Municipios"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
      
  aProcess(18, 1) = CIWinPersJuridicas
  aProcess(18, 2) = "Mantenimiento de Personas Jur�dicas"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
      
  aProcess(19, 1) = CIWinPatologias
  aProcess(19, 2) = "Mantenimiento de Patolog�as"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
      
  aProcess(20, 1) = CIWinRecordatorio
  aProcess(20, 2) = "Emitir Recordatorio"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
      
  aProcess(21, 1) = CIWinCargaMunicip
  aProcess(21, 2) = "Carga de Municipios-Localidades"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow
      
  aProcess(22, 1) = CIWinCargaTextos
  aProcess(22, 2) = "Carga de Ficheros de Texto"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow
     
  aProcess(23, 1) = CIRepTipoPoliza
  aProcess(23, 2) = "Relaci�n de Tipos de Poliza"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport
      
  aProcess(24, 1) = CIRepColectivos
  aProcess(24, 2) = "Relaci�n de Colectivos"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport
  
  aProcess(25, 1) = CIRepEntidadColab
  aProcess(25, 2) = "Relaci�n de Entidades Colaboradoras"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeReport
  
  aProcess(26, 1) = CIRepCentroUniversidad
  aProcess(26, 2) = "Relaci�n de Centros Universidad"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeReport

  aProcess(27, 1) = CIRepMotivoDescuento
  aProcess(27, 2) = "Relaci�n de Motivos de Descuento"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeReport
  
  aProcess(28, 1) = CIRepConciertos
  aProcess(28, 2) = "Relaci�n de Conciertos Econ�micos"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeReport

  aProcess(29, 1) = CIRepRelTitular
  aProcess(29, 2) = "Relaci�n de Relaciones del Titular"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport

  aProcess(30, 1) = CIRepTipoEconomico
  aProcess(30, 2) = "Relaci�n de Tipos Econ�micos"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport

  aProcess(31, 1) = CIRepPersonalUni
  aProcess(31, 2) = "Relaci�n de Personal de Universidad"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport

 ' aProcess(32, 1) = CIRepACUNSA
 ' aProcess(32, 2) = "Relaci�n de Asegurados ACUNSA"
 ' aProcess(32, 3) = False
 ' aProcess(32, 4) = cwTypeReport

  aProcess(33, 1) = CIRepEstadoCivil
  aProcess(33, 2) = "Relaci�n de Estados Civiles"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport

  aProcess(34, 1) = CIRepProfesion
  aProcess(34, 2) = "Relaci�n de Profesiones"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport

  aProcess(35, 1) = CIRepTratamiento
  aProcess(35, 2) = "Relaci�n de Tratamientos"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeReport

  aProcess(36, 1) = CIRepVinculoFamiliar
  aProcess(36, 2) = "Relaci�n de V�nculos Familiares"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport

  aProcess(37, 1) = CIRepPais
  aProcess(37, 2) = "Relaci�n de Pa�ses"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeReport

  aProcess(38, 1) = CIRepProvincia
  aProcess(38, 2) = "Relaci�n de Provincias-Municipios"
  aProcess(38, 3) = False
  aProcess(38, 4) = cwTypeReport

  aProcess(39, 1) = CIRepPersFisicas
  aProcess(39, 2) = "Relaci�n de Personas F�sicas"
  aProcess(39, 3) = False
  aProcess(39, 4) = cwTypeReport

  aProcess(40, 1) = CIRepPersJuridicas
  aProcess(40, 2) = "Relaci�n de Personas Jur�dicas"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeReport

  aProcess(41, 1) = CIRepPatologias
  aProcess(41, 2) = "Relaci�n de Patolog�as"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport

  aProcess(42, 1) = CIRepRecordatorio
  aProcess(42, 2) = "Recordatorio (Correo)"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport

  aProcess(45, 1) = CIWinRecitacion
  aProcess(45, 2) = "Mantenimiento de Recitaciones"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeWindow
 
  aProcess(51, 1) = CIWinDocAutorizacion
  aProcess(51, 2) = "Mantenimiento Documentos Autorizaci�n"
  aProcess(51, 3) = False
  aProcess(51, 4) = cwTypeWindow

  aProcess(54, 1) = CIRepRecordatorioMano
  aProcess(54, 2) = "Recordatorio (Mano)"
  aProcess(54, 3) = False
  aProcess(54, 4) = cwTypeReport

  aProcess(55, 1) = CIRepCitasConfirmadas
  aProcess(55, 2) = "Relaci�n de Citas Confirmadas"
  aProcess(55, 3) = True
  aProcess(55, 4) = cwTypeReport

  aProcess(56, 1) = CIRepCitasGestionadas
  aProcess(56, 2) = "Informe Estad�stico de Citas Gestionadas"
  aProcess(56, 3) = True
  aProcess(56, 4) = cwTypeReport

  aProcess(57, 1) = CIWinRelacionUDN
  aProcess(57, 2) = "Mantenimiento de Relaciones con la Universidad"
  aProcess(57, 3) = True
  aProcess(57, 4) = cwTypeWindow

  aProcess(58, 1) = CIRepRelacionesUDN
  aProcess(58, 2) = "Listado de Relaciones con la Universidad"
  aProcess(58, 3) = False
  aProcess(58, 4) = cwTypeReport

'Duplicados
  aProcess(59, 1) = CICitaManual2FI
  aProcess(59, 2) = "Citaci�n Manual Rapida - NUEVO"
  aProcess(59, 3) = True
  aProcess(59, 4) = cwTypeWindow
 
  aProcess(60, 1) = CICitaManualFI
  aProcess(60, 2) = "Citaci�n Manual - NUEVO"
  aProcess(60, 3) = True
  aProcess(60, 4) = cwTypeWindow

  
  aProcess(63, 1) = CiProcesosAbiertos
  aProcess(63, 2) = "Procesos Abiertos - NUEVO"
  aProcess(63, 3) = False
  aProcess(63, 4) = cwTypeWindow
  
  aProcess(64, 1) = CIIngresosNW
  aProcess(64, 2) = "Citar Ingresos - NUEVO"
  aProcess(63, 3) = False
  aProcess(64, 4) = cwTypeWindow
  
  aProcess(65, 1) = CICitaManualMultiple
  aProcess(65, 2) = "Citaci�n Manual Multiple"
  aProcess(65, 3) = True
  aProcess(65, 4) = cwTypeWindow

  aProcess(66, 1) = CiProcAsisAbiertos
  aProcess(66, 2) = "proc-asis abiertos"
  aProcess(66, 3) = True
  aProcess(66, 4) = cwTypeWindow
    
  aProcess(67, 1) = CIWinHojaFiliacion
  aProcess(67, 2) = "Hoja De Filiaci�n"
  aProcess(67, 3) = True
  aProcess(67, 4) = cwTypeWindow
  
  aProcess(68, 1) = CIWinCitaRecurso
  aProcess(68, 2) = "Mantenimiento Citas por Recurso (Nuevo)"
  aProcess(68, 3) = True
  aProcess(68, 4) = cwTypeWindow
  
  aProcess(69, 1) = CIWinCitasPaciente1
  aProcess(69, 2) = "Citas Paciente"
  aProcess(69, 3) = True
  aProcess(69, 4) = cwTypeWindow
  
  aProcess(70, 1) = CIWinIndSanitarios
  aProcess(70, 2) = "Indicaciones a Sanitarios"
  aProcess(70, 3) = False
  aProcess(70, 4) = cwTypeWindow

  aProcess(71, 1) = CIWPersonasJuridicasNW
  aProcess(71, 2) = "Personas Jur�dicas Nuevo"
  aProcess(71, 3) = True
  aProcess(71, 4) = cwTypeWindow
End Sub

