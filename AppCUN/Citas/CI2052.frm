VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmSanitarios2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Indicaciones a Sanitarios"
   ClientHeight    =   3075
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5595
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   5595
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBCombo cboMovilidad 
      Height          =   315
      Left            =   2100
      TabIndex        =   8
      Top             =   720
      Width           =   3375
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5953
      Columns(1).Caption=   "movilidad"
      Columns(1).Name =   "movilidad"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   5953
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBCombo cboTraslado 
      Height          =   315
      Left            =   2100
      TabIndex        =   7
      Top             =   240
      Width           =   3375
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5953
      Columns(1).Caption=   "Traslado"
      Columns(1).Name =   "Traslado"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5953
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   4140
      TabIndex        =   5
      Top             =   2520
      Width           =   1395
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Height          =   495
      Left            =   60
      TabIndex        =   4
      Top             =   2520
      Width           =   1395
   End
   Begin VB.Frame Frame1 
      Caption         =   "Indicaciones a Sanitario"
      ForeColor       =   &H00800000&
      Height          =   1335
      Left            =   0
      TabIndex        =   1
      Top             =   1080
      Width           =   5535
      Begin VB.TextBox txtIndicaciones 
         Height          =   1035
         Left            =   60
         TabIndex        =   6
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.TextBox txtText1 
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   5295
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Traslado del paciente en:"
      Height          =   195
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   360
      Width           =   1800
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Movilidad del paciente:"
      Height          =   195
      Index           =   1
      Left            =   60
      TabIndex        =   2
      Top             =   780
      Width           =   1635
   End
End
Attribute VB_Name = "frmSanitarios2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngActPlan As Long

Dim intI As Integer

Private Sub cmdGuardar_Click()
Dim qry As rdoQuery
Dim sql As String
 If cboTraslado.Text <> "" Or cboMovilidad.Text <> "" Or txtIndicaciones.Text <> "" Then
            sql = "UPDATE PR0400 SET PR59CODMOVPAC = ?, PR60CODNTRAPAC = ?, "
            sql = sql & "PR04DESINDSANIT = ? WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            If cboTraslado.Text = "" Then
               qry(1) = Null
            Else
               qry(1) = cboTraslado.Columns(0).Value
            End If
            If cboMovilidad.Text = "" Then
                qry(0) = Null
            Else
                qry(0) = cboMovilidad.Columns(0).Value
            End If
            If txtIndicaciones.Text = "" Then
                qry(2) = Null
            Else
                qry(2) = txtIndicaciones.Text
            End If
            qry(3) = lngActPlan
            qry.Execute
    Else
        MsgBox "Introduzca datos para guardar", vbInformation, "Aviso"
    End If
End Sub

Private Sub cmdSalir_Click()
Unload Me

End Sub

Private Sub Form_Load()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim intMov As Integer
Dim intTrans As Integer
Dim strDescrip As String
'intI = frmCitasPaciente.intIndGrid
If objPipe.PipeExist("Actuacion") Then
    lngActPlan = objPipe.PipeGet("Actuacion")
    Call objPipe.PipeRemove("Actuacion")
End If

'Buscamos las indicaciones a la actuacion (si hay varias cojo la primera)
sql = "SELECT PR59CODMOVPAC,PR60CODNTRAPAC, PR04DESINDSANIT FROM PR0400 WHERE"
sql = sql & " PR04NUMACTPLAN = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngActPlan
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    If Not IsNull(rs!PR59CODMOVPAC) Then
        intMov = rs!PR59CODMOVPAC
    Else
        intMov = 0
    End If
    If Not IsNull(rs!PR60CODNTRAPAC) Then
        intTrans = rs!PR60CODNTRAPAC
    Else
        intTrans = 0
    End If
    If Not IsNull(rs!PR04DESINDSANIT) Then
        txtIndicaciones.Text = rs!PR04DESINDSANIT
    End If

End If
rs.Close
qry.Close

'COMBO DE TRANSPORTE DEL PACIENTE
sql = "SELECT PR60CODNTRAPAC,PR60DESNTRAPAC FROM PR6000"
Set rs = objApp.rdoConnect.OpenResultset(sql)

Do While Not rs.EOF
    cboTraslado.AddItem rs("PR60CODNTRAPAC").Value & Chr(9) & rs("PR60DESNTRAPAC").Value
    rs.MoveNext
Loop
'cboTraslado.AddItem "" & Chr(9) & ""

If intTrans <> 0 Then
cboTraslado.MoveFirst
    Do While (intTrans <> cboTraslado.Columns(0).Value)
        cboTraslado.MoveNext
    Loop
    cboTraslado.Text = cboTraslado.Columns(1).Value
End If
rs.Close
'COMBO DE MOVILIDAD DEL PACIENTE
sql = "SELECT PR59CODMOVPAC,PR59DESMOVPAC FROM PR5900"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
    cboMovilidad.AddItem rs("PR59CODMOVPAC").Value & Chr(9) & rs("PR59DESMOVPAC").Value
    rs.MoveNext
Loop
'cboMovilidad.AddItem "" & Chr(9) & ""
If intMov <> 0 Then
cboMovilidad.MoveFirst
    Do While (intMov <> cboMovilidad.Columns(0).Value)
        cboMovilidad.MoveNext
    Loop
    cboMovilidad.Text = cboMovilidad.Columns(1).Value
End If
rs.Close
End Sub




