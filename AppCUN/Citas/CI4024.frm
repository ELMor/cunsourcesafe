VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBeneficiarios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Beneficiarios"
   ClientHeight    =   4395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5595
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4395
   ScaleWidth      =   5595
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   4320
      TabIndex        =   5
      Top             =   3960
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBGrid grdBeneficiarios 
      Height          =   3135
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   5295
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   0
      ForeColorEven   =   0
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   9340
      _ExtentY        =   5530
      _StockProps     =   79
      Caption         =   "Beneficiarios"
   End
   Begin VB.TextBox txtNH 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   4680
      TabIndex        =   3
      Top             =   240
      Width           =   735
   End
   Begin VB.TextBox txtRes 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   1200
      TabIndex        =   1
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "N� Historia"
      Height          =   195
      Left            =   3720
      TabIndex        =   2
      Top             =   240
      Width           =   750
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Responsable:"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmBeneficiarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngcEmp As Long
Dim strColec As String
Private Sub pFormatearGrid()
With grdBeneficiarios
    .Columns(0).Caption = "Beneficiario"
    .Columns(0).Width = 990
    .Columns(1).Caption = "Nombre"
    .Columns(1).Width = 2800
    .Columns(2).Caption = "Historia"
    .Columns(2).Width = 700
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
If objPipe.PipeExist("CI24CODEMPLEAD") Then
    lngcEmp = objPipe.PipeGet("CI24CODEMPLEAD")
    objPipe.PipeRemove ("CI24CODEMPLEAD")
Else
    lngcEmp = 0
End If
If objPipe.PipeExist("CI08CODCOLECTI") Then
    strColec = objPipe.PipeGet("CI08CODCOLECTI")
    objPipe.PipeRemove ("CI08CODCOLECTI")
Else
    strColec = ""
End If
pFormatearGrid
pCargardatos
End Sub
Private Sub pCargardatos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT CI24NUMBENEFIC, CI24APELLIBENEF||', '||CI24NOMBRBENEF BEN, "
sql = sql & "CI22NUMHISTORIA FROM CI2400 WHERE "
sql = sql & "CI24CODEMPLEAD= ? AND CI08CODCOLECTI= ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngcEmp
    qry(1) = strColec
Set rs = qry.OpenResultset()
While Not rs.EOF
    If rs!CI24NUMBENEFIC = 0 Then
        txtRes.Text = rs!BEN
        txtNH.Text = IIf(IsNull(rs!CI22NUMHISTORIA), " ", rs!CI22NUMHISTORIA)
    Else
        grdBeneficiarios.AddItem rs!CI24NUMBENEFIC & Chr$(9) & rs!BEN & Chr$(9) & rs!CI22NUMHISTORIA
    End If

    rs.MoveNext
Wend
End Sub
