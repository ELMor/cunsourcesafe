Attribute VB_Name = "GeneralCitas"
Option Explicit

Private objIntWord          As Object
'constantes para estados de las actuaciones PR3700
Public Const iActPlanificada = 1
Public Const iActCitada = 2
Public Const iActRealiz�ndose = 3
Public Const iActRealizada = 4
Public Const iActInformada = 5
Public Const iActCancelada = 6
'constantes para estados de las citas CI0100.CI01SITCITA
Public Const iCitaCitada = 1
Public Const iCitaAnulada = 2
Public Const iCitaRecitada = 3
Public Const iCitaPteCitar = 4
Public Const iCitaReservada = 5
Public Const iCitaCancelada = 6

'constantes para status de las actuaciones
Public Const ciPdteConfirmar    As String = "0"   'sin citar o citada sin confirmar
Public Const ciConfirmada       As String = "1"   'citada y confirmada
Public Const ciAnulada          As String = "2"   'citada y anulada
Public Const ciRecitada         As String = "3"   'citada y anulada por una recitaci�n
Public Const ciPdteRecitar      As String = "4"   'Pendiente de recitar
Public Const ciReservada        As String = "5"   'Reservada
Public Const ciAllCitas         As String = "9"   'Todas las situaciones

'constantes para status de las actuaciones
Public Const ciOpcAgenda        As String = "0"   'sin citar o citada sin confirmar
Public Const ciOpcModificar     As String = "1"   'citada y confirmada
Public Const ciOpcVerSolicitud  As String = "2"   'Pendiente de recitar
Public Const ciOpcRecitar       As String = "3"   'citada y anulada por una recitaci�n
Public Const ciOpcConfirmar     As String = "4"   'citada confirmarda y modificada despues sin salir de la solicitud sin salir de

'constantes para c�digos de incidencia
Public Const ciCodInciAnulacion    As Long = 150  'anulaci�n de cita
Public Const ciCodInciModificacion As Long = 160  'anulaci�n de cita
Public Const ciCodInciRecitacion   As Long = 170  'anulaci�n de cita

'constantes para tipos de comunicaci�n de recordatorio
Public Const ciRecordaMail      As Integer = 1
Public Const ciRecordaTfno      As Integer = 2

'variable para intervalo de minutos entre citas
Public ciIntervaloCitaMM   As Integer

'Mensaje de error para transacciones canceladas
Public Const ciErrRollback      As String = "Transacci�n Cancelada"
Public Const ciErrInciNoActive  As String = "El C�digo de Incidencia no esta activo"
Public Const ciMsgConfirmarOk   As String = "Asignaci�n correcta"

'variable para intervalo de fechas por defecto (Desde/Hasta)
Public ciIntervaloFechas  As Integer

'estas variables son de agenda
Public intCodMotInci            As Integer
Public blnActive                As Boolean  'Ventana de Incidencias
Public blnRollback              As Boolean
Public vntNuevoCod              As Variant
Public blnHideFrmInciden        As Boolean


'estas variables son los argumentos de la ventana de fases (pedidas, realizadas y citadas)
Public vntNumActPlan            As Variant
Public vntNumActPedi            As Variant
Public vntNumSolicit            As Variant
Public vntNumCita               As Variant

Public Const ciWinDocAutorizaci�n As String = "CI1035"
Public Const ciListaEsperaPac As String = "CI1033"
Public Const ciListaEsperaRec As String = "CI1034"

Public blnAddMode As Boolean
Public lngNumSolicitud As Long
Public blnRecMode As Boolean
'variables para la pantalla de citas agenda
Public lngPaciente             As Long
Public lngActuacion            As Long
Public strDesAct               As String
Public strFecha                As String
Public strHora                 As String
Public IntTipRecurso           As Integer
Public strFrmLlamador          As String
Public lngCodRec               As Long
'variables para la pantalla de consulta de Pruebas de planta
Public strDpto  As String





Public Function GetPersonName(lngIdPerson As Long) As String
  Dim cllDatosPersona As New Collection
  
  If lngIdPerson > 0 Then
    Set cllDatosPersona = GetTableColumn("SELECT CI22NUMHISTORIA, CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA=" & lngIdPerson)
    GetPersonName = Space(7 - Len(cllDatosPersona("CI22NUMHISTORIA"))) & (cllDatosPersona("CI22NUMHISTORIA")) & " " & cllDatosPersona("CI22PRIAPEL") & " " & cllDatosPersona("CI22SEGAPEL") & ", " & cllDatosPersona("CI22NOMBRE")
  Else
    GetPersonName = ""
  End If
End Function

Public Function ViewSolicitud(Optional blnModificar As Boolean = False) As Integer
   Load frmVerSolicitud
   '
   If blnModificar Then
     frmVerSolicitud.Label1 = "La Actuaci�n que va a modificar est� coordinada con las siguientes Actuaciones (inclu�da la que queremos modificar). Pulse  Aceptar para modificar la actuaci�n."
     frmVerSolicitud.cmdCommand1(2).Caption = "&Aceptar"
     frmVerSolicitud.cmdCommand1(0).Visible = False
   End If
   If Not frmVerSolicitud.blnAnularAct Then
     frmVerSolicitud.Show vbModal
   End If
   If frmVerSolicitud.blnAnularAct Then ViewSolicitud = 1
   If frmVerSolicitud.blnAnularSol Then ViewSolicitud = 2
   If frmVerSolicitud.blnCancelar Then ViewSolicitud = 3
   
   Unload frmVerSolicitud
   Set frmVerSolicitud = Nothing
End Function
Public Function strFecha_Sistema() As String
  Dim rs As rdoResultset
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY') from dual")
  strFecha_Sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
End Function
Public Function strFechaHora_Sistema() As String
  Dim rs As rdoResultset
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY HH24:MI') from dual")
  strFechaHora_Sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
End Function
Public Function strHora_Sistema() As String
  Dim rs As rdoResultset
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'hh24:mi:ss') from dual")
  strHora_Sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
End Function

Public Function fNextClave(strCampo$) As String
    Dim rs As rdoResultset, sql$
    sql = "SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    fNextClave = rs(0)
    rs.Close
End Function
