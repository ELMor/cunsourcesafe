Attribute VB_Name = "Estandar"
Option Explicit

Public Sub eventUpdate(ByVal objems As clsProcess, _
                       ByVal blnMode As Boolean)
  Select Case objems.strProcess
    Case "ACUNSA"
        Call UpdatePolizaACUNSA(objems, blnMode)
    Case "CódigosPostales"
        Call UpdateCodigosPostales(objems, blnMode)
  End Select
End Sub
Public Sub CargaPolizasACUNSA(ByVal objConnect As clsConnect)
  Dim objems As New clsProcess
  
  With objems
    .strProcess = "ACUNSA"
    .strInput = "SELECT " & _
                "NUMPOLIZA, NUMASEG, FECINIPOL, FECFINPOL, " & _
                "NOMBRETOM, NOMBREASEG, DNI, SEXO, FECNACIMIENTO, " & _
                "CORPAGO , EXCLUSIONES, TIPOPOLIZA " & _
                "From  ACUNSA.TXT Order By NUMPOLIZA,NUMASEG"
    .strOutput = "SELECT " & _
                 "CI03NUMPOLIZA, CI03NUMASEGURA, CI03FECINIPOLI, " & _
                 "CI03FECFINPOLI, CI03NOMBRTOMAD, CI03APELLITOMAD, " & _
                 "CI03NOMBRASEGU, CI03APELLIASEGU, CI22NUMHISTORIA, " & _
                 "CI03DNI, CI30CODSEXO, CI03FECNACIMIE, " & _
                 "CI03CORRIPAGO, CI36CODTIPPOLI, CI03EXCLUSION, CI03OBSERVACIO " & _
                 "From  CI0300 " & _
                 "ORDER BY CI03NUMPOLIZA, CI03NUMASEGURA"
   
   
    Call .cllCamposClaveEntrada.Add("NUMPOLIZA")
    Call .cllFormatoClaveEntrada.Add("000000")
    Call .cllCamposClaveEntrada.Add("NUMASEG")
    Call .cllFormatoClaveEntrada.Add("00000")
    
    Call .cllCamposClaveSalida.Add("CI03NUMPOLIZA")
    Call .cllFormatoClaveSalida.Add("0000000")
    Call .cllCamposClaveSalida.Add("CI03NUMASEGURA")
    Call .cllFormatoClaveSalida.Add("0000000")
   
    Call .emsInitProcess(objConnect)
    Call .emsRunProcess(objConnect)
    Call .emsEndProcess
  End With
  
  Set objems = Nothing
  
End Sub


Public Sub CargaCodigosPostales(ByVal objConnect As clsConnect)
  Dim objems As New clsProcess
  lngSecuencia = 0
  With objems
    .strProcess = "CódigosPostales"
    .strInput = "SELECT " & _
                "CODPROV,CODPOB, NOMCIUDAD, CALLE, NOMCALLE, " & _
                "COMIMPAR, FINIMPAR, COMPAR, FINPAR, CODPOSTAL " & _
                "From  CPOSTAL.TXT " & frmCargaTextos.strCodProv
    .strOutput = "SELECT  CI07SECUENCIA, " & _
                 "CI07CODPOSTAL, CI26CODPROVI, CI07TIPPOBLA,   " & _
                 "CI07DESPOBLA, CI07CALLE, CI07NUMIMPCOM, CI07NUMIMPFIN, " & _
                 "CI07NUMPARCOM, CI07NUMPARFIN " & _
                 "FROM  CI0700 WHERE CI07CODPOSTAL=0 "
   
   
   
    Call .emsInitProcess(objConnect)
    Call .emsRunProcess(objConnect)
    Call .emsEndProcess
  End With
  
  Set objems = Nothing
  
End Sub


Public Function ValidaDigControlDNI(ByVal strDni As String) As String

  Dim strAux As String
  Dim lngDni As Long
  Dim vntArrLetra, vntLetra
  Dim intIndex As Integer
  
  vntArrLetra = Array("T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", _
                      "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", _
                      "C", "K", "E")
  
    strAux = Left(strDni, Len(strDni) - 1)
    lngDni = Val(strAux)
    intIndex = (lngDni Mod 23)
    vntLetra = vntArrLetra(intIndex)
    ValidaDigControlDNI = CStr(vntLetra)
  
End Function

Public Function ValidaDigControlNSS(ByVal dblNp As Double, _
                                    ByVal dblNs As Double, _
                                    ByVal dblDs As Double) As Boolean

Dim dblAux As Double
Dim dblDec As Double
Dim intPos As Long
Dim strAux As String
Dim lngResto As Long

   'If dblNs < 100000 Then
   '   dblAux = dblNp * 100000 + dblNs
   'If dblNs < 1000000 Then
   '   dblAux = dblNp * 1000000 + dblNs
   If dblNs < 10000000 Then
      dblAux = dblNp * 10000000 + dblNs
   Else
      dblAux = dblNp * 100000000 + dblNs
   End If
                
   
   dblAux = dblAux / 97
   strAux = CStr(dblAux)
   intPos = InStr(1, strAux, ",")
   If intPos > 0 Then
     strAux = "0," & Right(strAux, Len(strAux) - intPos)
   End If
   dblDec = CDbl(strAux)
   lngResto = CLng(dblDec * 97)

   'dblAux = dblDig - CDbl(lngResto)
   
   'If dblDig - CDbl(dblAux) <> dblDs Then
   If CDbl(lngResto) <> dblDs Then
    ValidaDigControlNSS = False
   Else
    ValidaDigControlNSS = True
   End If
   
End Function

