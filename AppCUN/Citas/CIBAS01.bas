Attribute VB_Name = "SimpleCitas"
Option Explicit

Public Sub Tratamientos()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Tratamientos"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "10-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes Tratamientos "
      .cwUPD = "10-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Tratami"
      .objFormContainer.Caption = "Tratamientos"
      .intCursorSize = -1
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI3400"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0017", "Listado 1 Tipos de Tratamientos")
      
      Call .FormAddOrderField("CI34CODTRATAMI", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Tratamientos")
      Call .FormAddFilterWhere(strKey, "CI34CODTRATAMI", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI34DESTRATAMI", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI34DESTRATAMI", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI34CODTRATAMI")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI34DESTRATAMI")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub
Public Sub Profesiones()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Profesiones"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "10-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de las diferentes Profesiones"
      .cwUPD = "10-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Profesi"
      .objFormContainer.Caption = "Profesiones"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI2500"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0012", "Listado 1 de Profesiones")
      
      Call .FormAddOrderField("CI25CODPROFESI", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Profesiones")
      Call .FormAddFilterWhere(strKey, "CI25CODPROFESI", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI25DESPROFESI", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI25DESPROFESI", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI25CODPROFESI")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI25DESPROFESI")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub
Public Sub Colectivos()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Colectivos"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "10-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los Colectivos de Universidad"
      .cwUPD = "10-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Colecti"
      .objFormContainer.Caption = "Colectivos"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI0800"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0002", "Listado 1 de Colectivos Universidad")
      
      Call .FormAddOrderField("CI08CODCOLECTI", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Colectivos")
      Call .FormAddFilterWhere(strKey, "CI08CODCOLECTI", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI08DESCOLECTI", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI08DESCOLECTI", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI08CODCOLECTI")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI08DESCOLECTI")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub V�nculos_Familiares()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de V�nculos Familiares"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "I�aki Gabiola"
      .cwDAT = "17-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes v�nculos familiares "
      .cwUPD = "17-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "V�nculos"
      .objFormContainer.Caption = "V�nculos Familiares"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "cI3500"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0018", "Listado de v�nculos familiares")
      
      Call .FormAddOrderField("CI35CODTIPVINC", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de V�nculos familiares")
      Call .FormAddFilterWhere(strKey, "CI35DESTIPVINC", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI35DESTIPVINC", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI35CODTIPVINC")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI35DESTIPVINC")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
   
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub Centros_Cargo()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Centros Cargo"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "I�aki Gabiola"
      .cwDAT = "17-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes centros de cargo "
      .cwUPD = "17-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Centros_Cargo"
      .objFormContainer.Caption = "Centros de Cargo"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI0600"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0001", "Listado de centros de cargo")
      
      Call .FormAddOrderField("CI06CODCENCARG", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de V�nculos familiares")
      Call .FormAddFilterWhere(strKey, "CI06CODCENCARG", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI06DESCENCARG", "Nombre", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI06DESCENCARG", "Nombre")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI06CODCENCARG")
      Call .GridAddColumn(objMultiInfo, "Nombre", "CI06DESCENCARG")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
   
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub Tipo_Poliza()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Tipos de P�liza"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "21-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes Tipos de P�lizas "
      .cwUPD = "21-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "TipoPoli"
      .objFormContainer.Caption = "Tipos de P�liza"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI3600"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0016", "Listado 1 Tipos de P�lizas")
      
      Call .FormAddOrderField("CI36CODTIPPOLI", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Tipos de P�liza")
      Call .FormAddFilterWhere(strKey, "CI36CODTIPPOLI", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI36DESTIPPOLI", "Descripci�n", cwString)
      Call .FormAddFilterOrder(strKey, "CI36DESTIPPOLI", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI36CODTIPPOLI")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI36DESTIPPOLI")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub Entidad_Colaboradora()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Entidades Colaboradoras"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "21-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de las diferentes Entidades Colaboradoras"
      .cwUPD = "21-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "EntiCola"
      .objFormContainer.Caption = "Entidad Colaboradora"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI3700"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0004", "Listado 1 Entidades Colaboradoras")
      
      Call .FormAddOrderField("CI37CODENTCOLA", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Entidad Colaboradora")
      Call .FormAddFilterWhere(strKey, "CI37CODENTCOLA", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI37DESENTCOLA", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI37DESENTCOLA", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI37CODENTCOLA")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI37DESENTCOLA")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub
Public Sub Estados_Civiles()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Estados Civiles"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "I�aki Gabiola"
      .cwDAT = "17-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes Estados Civiles"
      .cwUPD = "17-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Estados_Civiles"
      .objFormContainer.Caption = "Estados Civiles"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI1400"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0005", "Listado de Estados Civiles")
      
      Call .FormAddOrderField("CI14CODESTCIVI", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Estados Civiles")
      Call .FormAddFilterWhere(strKey, "CI14DESESTCIVI", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI14DESESTCIVI", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI14CODESTCIVI")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI14DESESTCIVI")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
   
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub Motivo_Descuento()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Motivos de Descuento"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "21-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes Motivos de Descuento"
      .cwUPD = "21-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Motivo_Descuento"
      .objFormContainer.Caption = "Motivo Descuento"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI4100"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0006", "Listado 1 Motivos de Descuento")
      
      Call .FormAddOrderField("CI41CODMOTDESC", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Motivos Descuento")
      Call .FormAddFilterWhere(strKey, "CI41CODMOTDESC", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI41DESMOTDESC", "Descripci�n", cwString)
      Call .FormAddFilterWhere(strKey, "CI41ENTIRELA", "Relaci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI41DESMOTDESC", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI41CODMOTDESC")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI41DESMOTDESC")
      Call .GridAddColumn(objMultiInfo, "Relaci�n", "CI41ENTIRELA")
      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub Relaci�n_Titular()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Relaci�n Titular"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "21-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de las diferentes Relaciones del Titular"
      .cwUPD = "21-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Relaci�n_Titular"
      .objFormContainer.Caption = "Relaci�nes de Titulares"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI4200"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0014", "Listado 1 Relaciones de Titulares")
      
      Call .FormAddOrderField("CI42CODRELTITU", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Relaciones de Titulares")
      Call .FormAddFilterWhere(strKey, "CI42CODRELTITU", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI42DESRELTITU", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI42DESRELTITU", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI42CODRELTITU")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI42DESRELTITU")
      
      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub Paises()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Paises"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "21-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes Paises"
      .cwUPD = "22-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "Paises"
      .objFormContainer.Caption = "Paises"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI1900"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0007", "Listado 1 Paises")
      
      Call .FormAddOrderField("CI19DESPAIS", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Paises")
      Call .FormAddFilterWhere(strKey, "CI19CODPAIS", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI19DESPAIS", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI19DESPAIS", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI19CODPAIS")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI19DESPAIS")
      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub RelacionUDN()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "CITAS. Mantenimiento de Relaciones con la Universidad"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "CITAS"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "Jokin"
      .cwDAT = "21-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes Paises"
      .cwUPD = "22-07-97 - Jokin - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "RelUDN"
      .objFormContainer.Caption = "Relaciones con la Universidad"
      
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "CI2800"
      .intCursorSize = -1
' Reports generados por el form
      Call .objPrinter.Add("CI0023", "Listado de Relaciones con la Universidad")
      
      Call .FormAddOrderField("CI28DESRELUDN", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Paises")
      Call .FormAddFilterWhere(strKey, "CI28CODRELUDN", "C�digo", cwString)
      Call .FormAddFilterWhere(strKey, "CI28DESRELUDN", "Descripci�n", cwString)
                               
      Call .FormAddFilterOrder(strKey, "CI28DESRELUDN", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "CI28CODRELUDN")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "CI28DESRELUDN")
      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub


Public Sub pAsociadas(lngActPlan As Long, fecPlan As String)
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String
Dim lngActPedi As Long

sql = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngActPlan
Set rs = qry.OpenResultset()
lngActPedi = rs(0)
rs.Close
qry.Close

sql = "UPDATE PR0400 SET PR04FECPLANIFIC = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') WHERE PR04NUMACTPLAN IN ("
sql = sql & "SELECT PR04NUMACTPLAN FROM PR6100, PR0400"
sql = sql & " WHERE PR6100.PR03NUMACTPEDI_ASO = ? "
sql = sql & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0400.PR37CODESTADO IN (1,2) )"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(fecPlan, "DD/MM/YYYY HH:MM:SS")
    qry(1) = lngActPedi
qry.Execute

End Sub
Public Sub pAnularAsociadas(lngActPlan As Long)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim lngActPedi As Long

sql = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngActPlan
Set rs = qry.OpenResultset()
lngActPedi = rs(0)
rs.Close
qry.Close

sql = "UPDATE PR0400 SET PR37CODESTADO = 6 WHERE PR04NUMACTPLAN IN ("
sql = sql & "SELECT PR04NUMACTPLAN FROM PR6100, PR0400"
sql = sql & " WHERE PR6100.PR03NUMACTPEDI_ASO = ? "
sql = sql & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0400.PR37CODESTADO IN (1,2) )"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
'    QRY(0) = Format(fecPlan, "DD/MM/YYYY HH:MM:SS")
    qry(0) = lngActPedi
qry.Execute
qry.Close

sql = "UPDATE CI0100 SET CI01SITCITA = '2' WHERE CI01SITCITA = '1'"
sql = sql & "AND PR04NUMACTPLAN IN ("
sql = sql & "SELECT PR04NUMACTPLAN FROM PR6100, PR0400"
sql = sql & " WHERE PR6100.PR03NUMACTPEDI_ASO = ? "
sql = sql & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0400.PR37CODESTADO IN (1,2) )"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
'    QRY(0) = Format(fecPlan, "DD/MM/YYYY HH:MM:SS")
    qry(0) = lngActPedi
qry.Execute
qry.Close
End Sub

Public Sub pAsociarProceso(lngCodPersona As Long, lngNumActPedi As Long, strRecurso As String, lngCodDpto As Long, strFecha As String, strCodUsuario As String)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim qry1 As rdoQuery
Dim rs1 As rdoResultset
Dim strAsistencia As String
Dim intRes As Integer
Dim strHistoria As String
Dim strProceso As String
''constUSUARIO_PASTRANA & " " & constDESUSUARIO_PASTRANA
'FUNCION PARA ASOCIAR AUTOMATICAMENTE UN PROCESO
On Error Resume Next
'Comprobamos que si citamos para un departamento/doctor obtenemos el codigo de usuario
'sino el usuario ser Sin Doctor, excepto rehabilitacion que es la doctora casado
If strCodUsuario = "" Then
    Select Case lngCodDpto
        Case constDPTO_REHABILITACION
                strCodUsuario = constUSUARIO_CASADO
        Case constDPTO_URGENCIAS
                strCodUsuario = constUSUARIO_PASTRANA
        Case Else
            sql = "SELECT AG1100.SG02COD FROM AG1100,SG0200 WHERE AG11CODRECURSO = ? "
            sql = sql & " AND AG1100.SG02COD = SG0200.SG02COD"
            sql = sql & " AND AD30CODCATEGORIA = 1"
            
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = strRecurso
            Set rs = qry.OpenResultset()
            If Not rs.EOF And Not IsNull(rs(0)) Then
                strCodUsuario = rs(0)
            Else
                strCodUsuario = gstrSinDoctor
            End If
            rs.Close
            qry.Close
    End Select
End If

'vemos si existe un proceso con esas caracteristicas
sql = "SELECT AD0700.AD07CODPROCESO FROM AD0700,AD0400 WHERE "
sql = sql & " AD0700.CI21CODPERSONA = ? "
sql = sql & " AND AD0700.AD07CODPROCESO = AD0400.AD07CODPROCESO "
sql = sql & " AND SYSDATE BETWEEN AD07FECHORAINICI AND NVL(AD07FECHORAFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
sql = sql & " AND AD0400.AD02CODDPTO = ? "
sql = sql & " AND AD0400.SG02COD = ? "
sql = sql & " ORDER BY AD0700.AD07FECHORAINICI DESC"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodPersona
    qry(1) = lngCodDpto
    qry(2) = strCodUsuario
Set rs = qry.OpenResultset()

objApp.BeginTrans

If Not rs.EOF Then 'en caso de que exita el proceso
    'si la cita es para hoy miramos si hay alguna asistencia abierta
    strAsistencia = ""
    If (strFecha = strFecha_Sistema) Or lngCodDpto = constDPTO_REHABILITACION Then
        sql = "SELECT AD01CODASISTENCI FROM AD0800 WHERE AD07CODPROCESO = ? "
        sql = sql & " AND AD08FECFIN IS NULL AND AD08FECINICIO > TRUNC(SYSDATE)"
        Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
            qry1(0) = rs(0)
        Set rs1 = qry1.OpenResultset()
        If Not rs1.EOF Then strAsistencia = rs1(0)
        rs1.Close
        qry1.Close
    End If
    strProceso = rs(0)
Else 'el proceso no existe
'creamos el proceso
    Set rs1 = objApp.rdoConnect.OpenResultset("SELECT AD07CODPROCESO_S.NEXTVAL FROM DUAL")
    strProceso = rs1(0)
    rs1.Close
    
    sql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = ? "
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = lngCodPersona
    Set rs1 = qry1.OpenResultset()
    If Not IsNull(rs1(0)) Then strHistoria = rs1(0) Else strHistoria = ""
    rs1.Close
    qry1.Close
    
    sql = "INSERT INTO AD0700 (CI21CODPERSONA,AD07CODPROCESO,AD07FECHORAINICI,"
    sql = sql & " CI22NUMHISTORIA,AD34CODESTADO) VALUES(?,?,SYSDATE,?,?)"
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = lngCodPersona
        qry1(1) = strProceso
        If strHistoria <> "" Then qry1(2) = strHistoria Else qry1(2) = Null
        qry1(3) = "1"
    qry1.Execute
    If Err > 0 Or qry1.RowsAffected = 0 Then
        MsgBox "Error asociando el proceso", vbOKOnly
        objApp.RollbackTrans
    End If
    qry1.Close

    sql = "INSERT INTO AD0400 (AD07CODPROCESO,AD04FECINIRESPON,AD02CODDPTO,sg02cod)"
    sql = sql & " VALUES(?,SYSDATE,?,?)"
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = strProceso
        qry1(1) = lngCodDpto
        qry1(2) = strCodUsuario
    qry1.Execute
    If Err > 0 Or qry1.RowsAffected = 0 Then
        MsgBox "Error asociando el proceso", vbOKOnly
        objApp.RollbackTrans
    End If
    qry1.Close
End If
rs.Close
qry.Close
'actualizamos la pr04 y pr08 con el proceso asistencia obtenidos
sql = "UPDATE PR0400 SET AD01CODASISTENCI =?, AD07CODPROCESO = ? "
sql = sql & " WHERE PR03NUMACTPEDI = ? "
Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
    If strAsistencia <> "" Then qry1(0) = strAsistencia Else qry1(0) = Null
    qry1(1) = strProceso
    qry1(2) = lngNumActPedi
qry1.Execute
If strAsistencia <> "" Then MsgBox "Se ha asociado la asistencia y el proceso", vbOKOnly
If Err > 0 Or qry1.RowsAffected = 0 Then
    MsgBox "Error asociando el proceso", vbOKOnly
    objApp.RollbackTrans
End If
qry1.Close

sql = "UPDATE PR0800 SET AD01CODASISTENCI =?, AD07CODPROCESO = ? "
sql = sql & " WHERE PR03NUMACTPEDI = ? "
Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
    If strAsistencia <> "" Then qry1(0) = strAsistencia Else qry1(0) = Null
    qry1(1) = strProceso
    qry1(2) = lngNumActPedi
qry1.Execute
If Err > 0 Or qry1.RowsAffected = 0 Then
    MsgBox "Error asociando el proceso", vbOKOnly
    objApp.RollbackTrans
End If
qry1.Close
End Sub

