VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmRecordatorio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Emitir Recordatorio"
   ClientHeight    =   4650
   ClientLeft      =   510
   ClientTop       =   1725
   ClientWidth     =   9525
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   9525
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipo Recordatorio"
      Height          =   1125
      Index           =   3
      Left            =   6525
      TabIndex        =   35
      Top             =   1050
      Width           =   2940
      Begin VB.OptionButton optOption1 
         Caption         =   "En mano"
         Height          =   300
         Index           =   1
         Left            =   480
         TabIndex        =   15
         Top             =   660
         Width           =   1800
      End
      Begin VB.OptionButton optOption1 
         Caption         =   "Correo"
         Height          =   300
         Index           =   0
         Left            =   480
         TabIndex        =   14
         Top             =   285
         Value           =   -1  'True
         Width           =   1800
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Filtros"
      Height          =   3465
      Index           =   2
      Left            =   75
      TabIndex        =   27
      Top             =   1050
      Width           =   6405
      Begin Crystal.CrystalReport crCrystalReport1 
         Left            =   4920
         Top             =   480
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   1850
         TabIndex        =   8
         Top             =   1815
         Width           =   345
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   30104
         Index           =   3
         Left            =   2355
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "Descripci�n Actuaci�n"
         ToolTipText     =   "Descripci�n Actuaci�n"
         Top             =   1800
         Width           =   3915
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   2
         Left            =   165
         TabIndex        =   7
         Tag             =   "Actuaci�n"
         ToolTipText     =   "Tipo Actuaci�n"
         Top             =   1800
         Width           =   1545
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Actuaciones Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   165
         TabIndex        =   13
         Tag             =   "Indicador Actuaciones Pendientes"
         Top             =   3000
         Width           =   2505
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   1850
         TabIndex        =   11
         Top             =   2475
         Width           =   345
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   30104
         Index           =   5
         Left            =   2340
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Tag             =   "Descripci�n Recurso"
         ToolTipText     =   "Descripci�n Actuaci�n"
         Top             =   2490
         Width           =   3915
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   4
         Left            =   165
         TabIndex        =   10
         Tag             =   "C�digo Recurso"
         ToolTipText     =   "Tipo Actuaci�n"
         Top             =   2475
         Width           =   1545
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   0
         Left            =   165
         TabIndex        =   4
         Tag             =   "Fecha Desde"
         Top             =   450
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   1
         Left            =   2400
         TabIndex        =   5
         Tag             =   "Fecha Hasta"
         Top             =   435
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   165
         TabIndex        =   6
         Tag             =   "Departamento"
         Top             =   1125
         Width           =   4095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         FieldSeparator  =   ","
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   7620
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   7223
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   40
         Left            =   2370
         TabIndex        =   34
         Top             =   195
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   39
         Left            =   165
         TabIndex        =   33
         Top             =   210
         Width           =   1140
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   165
         TabIndex        =   32
         Top             =   915
         Width           =   1200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   2355
         TabIndex        =   31
         Top             =   1590
         Width           =   1935
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   165
         TabIndex        =   30
         Top             =   1575
         Width           =   1515
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Recurso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   2355
         TabIndex        =   29
         Top             =   2265
         Width           =   1785
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Recurso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   165
         TabIndex        =   28
         Top             =   2250
         Width           =   1365
      End
   End
   Begin VB.Frame fraFrame1 
      Height          =   2340
      Index           =   1
      Left            =   6525
      TabIndex        =   24
      Top             =   2175
      Width           =   2955
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   4
         Left            =   180
         TabIndex        =   20
         Top             =   1755
         Width           =   2565
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   330
         Index           =   6
         Left            =   1875
         TabIndex        =   17
         Tag             =   "N� Copias"
         Text            =   "1"
         ToolTipText     =   "Tipo Actuaci�n"
         Top             =   540
         Width           =   555
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   3
         Left            =   165
         TabIndex        =   19
         Top             =   1125
         Width           =   2580
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   2
         Left            =   150
         TabIndex        =   16
         ToolTipText     =   "Tipo Actividad"
         Top             =   540
         Width           =   1650
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         FieldSeparator  =   ","
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   2910
         Columns(0).Caption=   "Descripci�n"
         Columns(0).Name =   "Descripci�n"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin ComCtl2.UpDown updUpDown1 
         Height          =   330
         Index           =   0
         Left            =   2490
         TabIndex        =   18
         Top             =   540
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         Value           =   1
         AutoBuddy       =   -1  'True
         BuddyControl    =   "txtText1(6)"
         BuddyDispid     =   196612
         BuddyIndex      =   6
         OrigLeft        =   3420
         OrigTop         =   510
         OrigRight       =   3660
         OrigBottom      =   840
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   0   'False
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N� Copias"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   1860
         TabIndex        =   26
         Top             =   300
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Emitir por"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   135
         TabIndex        =   25
         Top             =   315
         Width           =   810
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Persona"
      Height          =   915
      Index           =   0
      Left            =   75
      TabIndex        =   21
      Top             =   105
      Width           =   9375
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   1425
         TabIndex        =   1
         Top             =   465
         Width           =   345
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   8310
         TabIndex        =   3
         Top             =   450
         Width           =   900
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   195
         TabIndex        =   0
         Tag             =   "C�digo Persona|C�digo"
         Top             =   450
         Width           =   1140
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   1875
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   450
         Width           =   6045
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   45
         Left            =   180
         TabIndex        =   23
         Top             =   225
         Width           =   600
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   1875
         TabIndex        =   22
         Top             =   240
         Width           =   660
      End
   End
End
Attribute VB_Name = "frmRecordatorio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Public lngNumSolicit As Long
Private Function GetDirNum(strFecha As String, vntCodPersona As Variant) As Long
  Dim rdoDir As rdoResultset
  Dim strSql As String
  
  strSql = "SELECT MAX(CI10NUMDIRECCI) FROM CI1000 WHERE " _
     & "CI21CODPERSONA=" & vntCodPersona _
     & " AND CI10INDDIRPRINC = 0 AND ( CI10FECINIVALID <" & objGen.ValueToSQL(strFecha, cwDate) _
     & " AND CI10FECFINVALID >" & objGen.ValueToSQL(strFecha, cwDate) _
     & " OR CI10FECFINVALID IS NULL  )"
  Set rdoDir = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
  If Not rdoDir.EOF Then
    GetDirNum = rdoDir(0)
    rdoDir.Close
  Else
    strSql = "SELECT CI1000.CI10NUMDIRECCI FROM CI1000 WHERE CI1000.CI21CODPERSONA=" & vntCodPersona & " AND CI1000.CI10INDDIRPRINC=-1"
    Set rdoDir = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
    If Not rdoDir.EOF Then
      GetDirNum = rdoDir(0)
      rdoDir.Close
    Else
      GetDirNum = 0
    End If
  End If
  Set rdoDir = Nothing
End Function

Private Sub GetReport()
  Dim strWherePersona As String
  Dim strWhereFecha As String
  Dim strWhereDpto As String
  Dim strWhereAct As String
  Dim strWherePdtes As String
  Dim strWhereTotal As String
  Dim strFechaDesde As String
  Dim strFechaHasta As String
  Dim strWhereDirs As String
  Dim strSql As String
  Dim rdoFechaRec As rdoResultset
  Dim rdoCita As rdoResultset
  Dim strFechaHoy As String
  
  On Error GoTo ReportError
  strFechaHoy = Format(objGen.GetDBDateTime, "dd/mm/yyyy")

  If lngNumSolicit = 0 Then
    If IsDate(dtcDateCombo1(0).Date) Then
      With dtcDateCombo1(0)
       strFechaDesde = " DateTime(" & Format(.Date, "YYYY") & "," & Format(.Date, "M") & "," & Format(.Date, "D") & ")"
      End With
      With dtcDateCombo1(1)
        strFechaHasta = " DateTime(" & Format(.Date, "YYYY") & "," & Format(.Date, "M") & "," & Format(.Date, "D") & ")"
      End With
      
      strWhereFecha = " and DTSToDateTime ({PR0434J.CI01FECCONCERT}) in  " & strFechaDesde & " to " & strFechaHasta
     Else
      strWhereFecha = ""
     End If
   
    If txtText1(0) <> "" Then
      strWherePersona = " and {PR0434J.CI21CODPERSONA} =" & Val(txtText1(0).Text)
    End If
    If cboSSDBCombo1(0).Text <> "" Then
      strWhereDpto = " and {PR0434J.AD02CODDPTO} = " & cboSSDBCombo1(0).Value
    Else
      strWhereDpto = ""
    End If
    If txtText1(2) <> "" Then
      strWhereAct = " and {PR0434J.PR01CODACTUACION} =" & Val(txtText1(2).Text)
    Else
      strWhereAct = ""
    End If
    If chkCheck1(1).Value = 1 Then
      strWherePdtes = " and Length({PR0434J.CI01FECCONCERT})=0 "
    Else
      strWherePdtes = ""
    End If
  
    strWhereDirs = "((  DTSToDateTime ({CI1000.CI10FECINIVALID})< DateTime(" & Format(strFechaHoy, "YYYY") & "," & Format(strFechaHoy, "M") & "," & Format(strFechaHoy, "D") & ")" _
            & " and DTSToDateTime ({CI1000.CI10FECFINVALID})> DateTime(" & Format(strFechaHoy, "YYYY") & "," & Format(strFechaHoy, "M") & "," & Format(strFechaHoy, "D") & ") )"
    strWhereTotal = strWhereDirs & " or Length({CI1000.CI10FECFINVALID})=0 or {CI1000.CI10INDDIRPRINC} = -1.00 ) and ( " _
           & "{PR0434J.CI01SITCITA} in [#1#, #4#]  AND {PR0434J.CI01INDRECORDA}=-1 " _
           & strWherePersona _
           & strWhereFecha _
           & strWhereDpto _
           & strWhereAct _
           & strWherePdtes & " )"
  Else
    strWhereDirs = "((  DTSToDateTime (ToText({CI1000.CI10FECINIVALID}))< DateTime(" & Format(strFechaHoy, "YYYY") & "," & Format(strFechaHoy, "M") & "," & Format(strFechaHoy, "D") & ")" _
            & " and DTSToDateTime (ToText({CI1000.CI10FECFINVALID}))> DateTime(" & Format(strFechaHoy, "YYYY") & "," & Format(strFechaHoy, "M") & "," & Format(strFechaHoy, "D") & ") )"
    strWhereTotal = strWhereDirs & " or Length(ToText({CI1000.CI10FECFINVALID}))=0 or {CI1000.CI10INDDIRPRINC} = -1.00 ) and ( " _
           & "{PR0434J.CI01SITCITA} in [#1#, #4#]   AND {PR0434J.CI01INDRECORDA}=-1 " _
           & " AND {PR0434J.CI31NUMSOLICIT}=" & lngNumSolicit & " )"
  End If
  With crCrystalReport1
    If OptOption1(0).Value = True Then
      .ReportFileName = objApp.strReportsPath & "CI0021.rpt"  'App.Path & "\Rpt\CI0019.rpt"
    Else
      .ReportFileName = objApp.strReportsPath & "CI0022.rpt" 'App.Path & "\Rpt\CI0020.rpt"
    End If
    .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
    If txtText1(6) <> "" Then
      .PrinterCopies = Val(txtText1(6))
    Else
      .PrinterCopies = 1
    End If
  
    If cboSSDBCombo1(2) = "Impresora" Then
      .Destination = crptToPrinter
    Else
      .Destination = crptToWindow
    End If
    .WindowTitle = "Recordatorio de Actuaciones"
    .Connect = objApp.rdoConnect.Connect
    
    .DiscardSavedData = True
    Me.MousePointer = vbHourglass
    .Action = 1
    
   ' .PrintReport
    Me.MousePointer = vbDefault
   
    If Err.Number = 0 Then
      If .Destination = crptToPrinter Then
        objApp.rdoConnect.BeginTrans
        objSecurity.RegSession
        If IsDate(dtcDateCombo1(0).Date) Then
        With dtcDateCombo1(0)
          strFechaDesde = objGen.ValueToSQL(.Date, cwDate)
        End With
        With dtcDateCombo1(1)
          strFechaHasta = objGen.ValueToSQL(.Date, cwDate)
        End With
      
          strWhereFecha = " and PR0434J.CI01FECCONCERT BETWEEN " & strFechaDesde & " AND " & strFechaHasta
      Else
          strWhereFecha = ""
      End If
 
      strSql = objGen.ReplaceStr("SELECT CI31NUMSOLICIT, CI01NUMCITA, CI21CODPERSONA  FROM PR0434J WHERE " _
           & "CI01SITCITA in ('1', '4') " _
           & strWherePersona _
           & strWhereDpto _
           & strWhereAct, "{", Chr(32), 0)
       strSql = objGen.ReplaceStr(strSql, "}", Chr(32), 0)
       strSql = strSql & strWhereFecha
       Set rdoFechaRec = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
       Do While Not rdoFechaRec.EOF
         strSql = "SELECT CI31NUMSOLICIT,CI01NUMCITA, CI01FECEMIRECO,CI01NUMENVRECO FROM CI0100 WHERE " _
                 & "CI31NUMSOLICIT=" & rdoFechaRec(0) _
                 & " AND CI01NUMCITA=" & rdoFechaRec(1)
         Set rdoCita = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         If Not rdoCita.EOF Then
            rdoCita.Edit
            rdoCita("CI01FECEMIRECO") = CDate(strFechaHoy)
            rdoCita("CI01NUMENVRECO") = GetDirNum(strFechaHoy, rdoFechaRec("CI21CODPERSONA"))
            rdoCita.Update
            rdoCita.Close
         End If
         Set rdoCita = Nothing
         rdoFechaRec.MoveNext
       Loop
       Set rdoFechaRec = Nothing
       If Err.Number = 0 Then
         objApp.rdoConnect.CommitTrans
       Else
         objApp.rdoConnect.RollbackTrans
       End If
      End If
    End If
  End With
  Set rdoCita = Nothing
  Exit Sub
ReportError:
  Call objError.SetError(cwInternalMsg, "Se ha producido un error al intentar procesar el Report")
  Call objError.Raise
  Resume Next

End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  If cboSSDBCombo1(2).Text = "Pantalla" Then
    txtText1(6).Enabled = False
    txtText1(6) = "1"
    updUpDown1(0).Enabled = False
  Else
    txtText1(6).Enabled = True
    updUpDown1(0).Enabled = True
  
  End If
End Sub

Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If (intIndex = 0 Or intIndex = 1) And KeyCode = 46 Then
    cboSSDBCombo1(intIndex).Text = ""
  End If

End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  If intIndex = 0 Then
    If chkCheck1(0).Value = 1 Then
      txtText1(0) = ""
      txtText1(0).Enabled = False
      txtText1(1) = ""
      txtText1(1).Enabled = False
    Else
      txtText1(0).Enabled = True
      txtText1(1).Enabled = True
    End If
  End If
End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim objField As clsCWFieldSearch

  If intIndex = 0 Then
      Set objSearch = New clsCWSearch
      With objSearch
       .strTable = "CI2200"
       .strWhere = ""
       .strOrder = ""
      
       Set objField = .AddField("CI21CODPERSONA")
       objField.strSmallDesc = "C�DIGO PERSONA"
       Set objField = .AddField("CI22NOMBRE")
       objField.strSmallDesc = "NOMBRE        "
       Set objField = .AddField("CI22PRIAPEL")
       objField.strSmallDesc = "PRIMER APELLIDO   "
       Set objField = .AddField("CI22SEGAPEL")
       objField.strSmallDesc = "SEGUNDO APELLIDO  "
       Set objField = .AddField("CI22DNI")
       objField.strSmallDesc = "DNI        "
       Set objField = .AddField("CI22FECNACIM")
       objField.strSmallDesc = "FECHA NACIMIENTO"

       If .Search Then
         txtText1(0) = .cllValues("CI21CODPERSONA")
         txtText1(1) = .cllValues("CI22NOMBRE") & " " & .cllValues("CI22PRIAPEL") & " " & .cllValues("CI22SEGAPEL")
          
       End If
      End With
     
    Set objSearch = Nothing
  End If
  
  If intIndex = 1 Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "PR0100"
      .strWhere = ""
      .strOrder = ""
      
      Set objField = .AddField("PR01CODACTUACION")
      objField.strSmallDesc = "C�digo Actuaci�n"
      Set objField = .AddField("PR01DESCORTA")
      objField.strSmallDesc = "Descripci�n Actuaci�n                              "
      
      If .Search Then
        txtText1(2) = .cllValues("PR01CODACTUACION")
        txtText1(3) = .cllValues("PR01DESCORTA")
      End If
    End With
     
    Set objSearch = Nothing
  End If
  
  If intIndex = 2 Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AG1100"
      .strWhere = ""
      .strOrder = ""
      
      Set objField = .AddField("AG11CODRECURSO")
      objField.strSmallDesc = "C�digo Recurso"
      Set objField = .AddField("AG11DESRECURSO")
      objField.strSmallDesc = "Descripci�n Recurso                            "
      
      If .Search Then
        txtText1(4) = .cllValues("AG11CODRECURSO")
        txtText1(5) = .cllValues("AG11DESRECURSO")
      End If
    End With
     
    Set objSearch = Nothing
  End If
  
  If intIndex = 3 Then
    Call GetReport
    cmdCommand1(4).Caption = "&Cerrar"
  End If
  If intIndex = 4 Then
    Me.Hide
  End If
End Sub

Private Sub Form_Load()
  Dim rdoDepartamentos As rdoResultset
  Dim rdoTipoActividad As rdoResultset
  Dim strSql As String
  
  strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"
  Set rdoDepartamentos = objApp.rdoConnect.OpenResultset(strSql)
  Do While Not rdoDepartamentos.EOF
    cboSSDBCombo1(0).AddItem (rdoDepartamentos("AD02CODDPTO") & "," & rdoDepartamentos("AD02DESDPTO"))
    rdoDepartamentos.MoveNext
  Loop
 ' strSql = "SELECT PR12CODACTIVIDAD, PR12DESACTIVIDAD  FROM " & objEnv.GetValue("Database") & "PR1200 ORDER BY PR12DESACTIVIDAD"
 ' Set rdoTipoActividad = objApp.rdoConnect.OpenResultset(strSql)
 ' Do While Not rdoTipoActividad.EOF
 '   cboSSDBCombo1(2).AddItem (rdoTipoActividad("PR12CODACTIVIDAD") & "," & rdoTipoActividad("PR12DESACTIVIDAD"))
 '   rdoTipoActividad.MoveNext
 ' Loop
  cboSSDBCombo1(2).AddItem "Pantalla"
  cboSSDBCombo1(2).AddItem "Impresora"
  cboSSDBCombo1(2).MoveFirst
  cboSSDBCombo1(2).Text = cboSSDBCombo1(2).Columns(0).Value
End Sub

