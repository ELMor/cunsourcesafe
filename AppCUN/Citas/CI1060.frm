VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "crystl32.ocx"
Begin VB.Form frmPautacion 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Actuaciones Planificadas por Recurso"
   ClientHeight    =   8070
   ClientLeft      =   1050
   ClientTop       =   870
   ClientWidth     =   11910
   ControlBox      =   0   'False
   Icon            =   "CI1060.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8070
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Height          =   420
      Left            =   120
      TabIndex        =   13
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdCommand2 
      Caption         =   "Cam&biar Actuaci�n"
      Height          =   375
      Index           =   3
      Left            =   6240
      TabIndex        =   17
      Top             =   7320
      Width           =   1695
   End
   Begin VB.CommandButton cmdCommand2 
      Caption         =   "Modificacion de Citas"
      Height          =   375
      Index           =   2
      Left            =   4240
      TabIndex        =   16
      Top             =   7320
      Width           =   1695
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   9135
      Top             =   7260
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdCommand2 
      Caption         =   "Imprimir Petici�n"
      Height          =   375
      Index           =   1
      Left            =   2240
      TabIndex        =   15
      Top             =   7320
      Width           =   1695
   End
   Begin VB.CommandButton cmdCommand2 
      Caption         =   "Introducir Pautacion"
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   14
      Top             =   7320
      Width           =   1695
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Consultar"
      Height          =   375
      Left            =   9120
      TabIndex        =   2
      Top             =   600
      Width           =   1455
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Citadas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   1320
      Width           =   11835
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5325
         HelpContextID   =   2
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   11655
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Urgente"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "CI1060.frx":000C
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20558
         _ExtentY        =   9393
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   7710
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   635
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   3165
      TabIndex        =   3
      Tag             =   "Fecha superior de las citas"
      Top             =   840
      Width           =   1650
      _Version        =   65537
      _ExtentX        =   2910
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   960
      TabIndex        =   4
      Tag             =   "Fecha superior de las citas"
      Top             =   840
      Width           =   1650
      _Version        =   65537
      _ExtentX        =   2910
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSDataWidgets_B.SSDBCombo sscboDpto 
      Height          =   315
      Left            =   960
      TabIndex        =   8
      Top             =   480
      Width           =   3855
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin ComctlLib.ListView lvwRec 
      Height          =   735
      Left            =   5760
      TabIndex        =   10
      Top             =   480
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   1296
      View            =   3
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin SSDataWidgets_B.SSDBCombo sscboRecurso 
      Height          =   315
      Left            =   8040
      TabIndex        =   11
      Top             =   480
      Visible         =   0   'False
      Width           =   735
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1296
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Recurso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   4560
      TabIndex        =   12
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Dpto."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   735
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   2640
      TabIndex        =   6
      Top             =   840
      Width           =   510
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   360
      TabIndex        =   5
      Top             =   840
      Width           =   555
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
   Begin VB.Menu mnuEstado 
      Caption         =   "Estados Cita"
      Visible         =   0   'False
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&0. Sin citar"
         Index           =   0
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&1. Confirmar"
         Index           =   1
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&2. Anular"
         Index           =   2
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&3. Recitar"
         Index           =   3
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&4. Pendiente de Recitar"
         Index           =   4
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "Lista de Espera ?  (S/N)"
         Index           =   6
      End
   End
End
Attribute VB_Name = "frmPautacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vnta As Variant
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Dim intDptoSel%, lngRecursoSel&
Dim strMeCaption$
Dim objMultiInfo1 As New clsCWForm

Private Sub pSortGrid(ssGrid As SSDBGrid, strColname As String, Optional blnRefresh As Boolean)
  Dim strFieldOrder As String
  
  'Get the database field linked to the specified column
  strFieldOrder = objWinInfo.CtrlGetInfo(ssGrid.Columns(strColname)).objControl.DataField
  
  'If a field exists, clear the old sort order and set the new one
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
  End If
  
  'Refresh the grid, if needed
  If Not IsMissing(blnRefresh) Then
    If blnRefresh Then objWinInfo.DataRefresh
  End If
End Sub

Private Sub pCambioActuacion(ssGrid As SSDBGrid)
  Dim strSql, qry As rdoQuery, rs As rdoResultset
  Dim lngNumActPlan&, lngActiv&
  Dim strActSel$, i%
  Dim vntParms(4) As Variant
  Dim blnModified As Boolean
  
  '--- Check to see whether there are selected lines or not
  '--- If not, send a message to the user and exit
  If (ssGrid.SelBookmarks.Count = 0) Then
    strSql = "No se ha seleccionado ninguna Actuaci�n."
    MsgBox strSql, vbExclamation, strMeCaption
    Exit Sub
  End If
  
  With ssGrid
    'se anota el N� Act. Plan. de una de las actuacciones seleccionadas para las _
    posteriores b�squedas de datos
    lngNumActPlan = .Columns("N�mero").CellText(.SelBookmarks(0))
    
    'se anotan todos los N� Act. Plan. seleccionados
    For i = 0 To .SelBookmarks.Count - 1
      strActSel = strActSel & .Columns("N�mero").CellText(.SelBookmarks(i)) & ","
    Next i
  End With

  'se busca el tipo de actividad
  strSql = "SELECT PR0100.PR12CODACTIVIDAD " & _
           "FROM   PR0400, PR0100 " & _
           "WHERE  PR0400.PR04NUMACTPLAN = ? AND " & _
           "       PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
  Set qry = objApp.rdoConnect.CreateQuery("", strSql)
  qry(0) = lngNumActPlan
  Set rs = qry.OpenResultset()
  lngActiv = rs(0)
  rs.Close
  qry.Close
  
  vntParms(1) = intDptoSel
  vntParms(2) = lngActiv
  vntParms(3) = strActSel
    
  objSecurity.LaunchProcess "PR0507", vntParms
  
  '---TEMPORARY CODE
  'Retrieve a return value using a pipe object
  'The PR0507 process creates a named pipe ("PR_CAMBIO") and puts a boolean
  'value in (TRUE=changes, FALSE=no changes)
  If objPipe.PipeExist("PR_CAMBIO") Then
    blnModified = objPipe.PipeGet("PR_CAMBIO")
    objPipe.PipeRemove ("PR_CAMBIO")
  End If

  '---TEMPORARY CODE Reload data into the grid to display the new situation
  If blnModified Then cmdCommand1_Click
End Sub

Private Sub cmdCommand1_Click()
  Dim i%, strRecSel$

  Screen.MousePointer = vbHourglass
  
  If dtcDateCombo1(0) = "" Or dtcDateCombo1(1) = "" Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde y la Fecha Hasta son obligatorios")
    Call objError.Raise
    Exit Sub
  Else
    If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        Call objError.SetError(cwCodeMsg, "La Fecha Hasta es menor que Fecha Desde")
        Call objError.Raise
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
    End If
  End If

  For i = 1 To lvwRec.ListItems.Count
    If lvwRec.ListItems(i).Selected Then
      strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
      If i = 1 Then Exit For 'TODOS
    End If
  Next i
  
  If strRecSel <> "" Then
    'Cambia el orden de la grid
    pSortGrid grdDBGrid1(0), "PACIENTE"
    
    strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
    Call pCargarActuaciones(strRecSel)
  Else
    MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
  End If

  Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActuaciones(stRecSel$)
If stRecSel <> "0" Then
    objMultiInfo1.strWhere = "AD02CODDPTO = " & sscboDpto.Columns(0).Value & _
    " AND AG11CODRECURSO_ASI IN (" & stRecSel & ") AND CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
        " AND CI01FECCONCERT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') AND CI01SITCITA='1'  AND PR37CODESTADO = 2"
Else
    objMultiInfo1.strWhere = "AD02CODDPTO = " & sscboDpto.Columns(0).Value & _
    " AND CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
        " AND CI01FECCONCERT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') AND CI01SITCITA='1' AND PR37CODESTADO = 2"
End If
objWinInfo.DataRefresh

End Sub

Private Sub cmdCommand2_Click(Index As Integer)
  Select Case Index
    Case 0 'introducir pautacion
      If grdDBGrid1(0).SelBookmarks.Count > 1 Then
        MsgBox "Seleccione s�lo una Actuaci�n para pautar", vbCritical
      Else
        pVerDatosActuacion (grdDBGrid1(0).Columns("N�mero").Value)
      End If
        'objWinInfo.DataRefresh
    Case 1 'imprimir peticion
      Call pImprimirPeticion(grdDBGrid1(0))
    Case 2 ' Modificacion de citas
      Call pModificaCitas(grdDBGrid1(0))
    Case 3 'Cambio de Actuaci�n
      Call pCambioActuacion(grdDBGrid1(0))
  End Select
End Sub

Private Sub pImprimirPeticion(ssGrid As SSDBGrid)
    Dim i%, sql$
    
    If ssGrid.SelBookmarks.Count = 1 Then
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
    Else
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
    End If
    With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToPrinter
        For i = 0 To ssGrid.SelBookmarks.Count - 1
            sql = sql & "{PR0457J.PR04NUMACTPLAN}= " & ssGrid.Columns("N�mero").CellText(ssGrid.SelBookmarks(i)) & " OR "
        Next i
        sql = Left$(sql, Len(sql) - 4)
        .SelectionFormula = "(" & sql & ")"
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Screen.MousePointer = vbHourglass
        .Action = 1
        Screen.MousePointer = vbDefault
    End With
End Sub

Private Sub Form_Load()
  Dim intGridIndex  As Integer
  Dim strFechaIni   As String
  Dim strFechaFin   As String
  Dim dtdatei       As Date
  Dim dtdatef       As Date
  Dim qry           As rdoQuery
  Dim rs            As rdoResultset
  Dim intDpto       As Integer
  Dim sql           As String
  
  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Actuaciones citadas por Recurso(s)"
    .cwDAT = "2-12-97"
    .cwAUT = "Elena Faedda"
    .cwDES = ""
    .cwUPD = "26 - 11 - 19999 - I�aki - Creaci�n del m�dulo"
  End With
  
  With objMultiInfo1
    .strName = "Confirmadas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strWhere = "AD02CODDPTO = 999 AND AG11CODRECURSO = 999"
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0432J"
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 200
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo1, "Historia", "CI22NUMHISTORIA")
    Call .GridAddColumn(objMultiInfo1, "Paciente", "PACIENTE")
    Call .GridAddColumn(objMultiInfo1, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo1, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo1, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo1, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo1, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo1, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo1, "Indicaciones a la Actuaci�n", "")
    Call .GridAddColumn(objMultiInfo1, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo1, "CodEstado", "PR37CODESTADO")
    Call .GridAddColumn(objMultiInfo1, "Estado", "")
    Call .GridAddColumn(objMultiInfo1, "F.en cola", "PR04FECENTRCOLA", cwDate)
    Call .GridAddColumn(objMultiInfo1, "Cama", "CAMA")
    Call .GridAddColumn(objMultiInfo1, "Dr.Respon", "RESPONSABLE")
    Call .FormCreateInfo(objMultiInfo1)
    intGridIndex = 0
    
    'comentarizados para realizar pruebas luego seran invisibles
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero").Visible = False
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo").Visible = False
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").Visible = False
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero Cita").Visible = False
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Situaci�n").Visible = False
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodEstado").Visible = False
    grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Actuaci�n Pedida").Visible = False
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns(8)), "PR01CODACTUACION", "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n"), "PR01DESCORTA")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("CodEstado")), "PR37CODESTADO", "SELECT PR37DESESTADO FROM PR3700 WHERE PR37CODESTADO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodEstado")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Estado"), "PR37DESESTADO")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("Actuaci�n Pedida")), "PR03NUMACTPEDI", "SELECT PR08DESINDICAC FROM PR0800 WHERE PR03NUMACTPEDI = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("Actuaci�n Pedida")), grdDBGrid1(0).Columns("Indicaciones a la Actuaci�n"), "PR08DESINDICAC")
  
    grdDBGrid1(0).Columns("CodEstado").Visible = False
    grdDBGrid1(0).Columns("Persona").Visible = False
    
    'Tama�o de los campos del grid
    'a�ado uno para las longitud
    'grdDBGrid1(0).Columns(3).width = 800
    grdDBGrid1(0).Columns(4).Width = 720
    grdDBGrid1(0).Columns(6).Width = 1725
    grdDBGrid1(0).Columns("Descripci�n").Width = 1860
    grdDBGrid1(0).Columns(5).Width = 2790
    grdDBGrid1(0).Columns(14).Width = 1000
    grdDBGrid1(0).Columns(16).Width = 871
    grdDBGrid1(0).Columns("Indicaciones a la Actuaci�n").Width = 3240
    grdDBGrid1(0).Columns("Cama").Width = 700
    grdDBGrid1(0).Columns("Dr.Respon").Width = 3500
        
    Call .WinRegister
    Call .WinStabilize
    blnCommit = False
  End With
  
  strMeCaption = Me.Caption

  'se cargan los departamentos realizadores a los que tiene acceso el usuario
  sql = "SELECT AD02CODDPTO, AD02DESDPTO"
  sql = sql & " FROM AD0200"
  sql = sql & " WHERE AD02CODDPTO IN ("
  sql = sql & " SELECT AD02CODDPTO FROM AD0300"
  sql = sql & " WHERE SG02COD = ?"
  sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
  sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
  sql = sql & " ORDER BY AD02DESDPTO"
  Set qry = objApp.rdoConnect.CreateQuery("", sql)
  qry(0) = objSecurity.strUser
  Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
  If Not rs.EOF Then
      intDptoSel = rs!AD02CODDPTO
      sscboDpto.Text = rs!AD02DESDPTO
      Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
      Do While Not rs.EOF
          If intDpto = rs!AD02CODDPTO Then
              intDptoSel = rs!AD02CODDPTO
              sscboDpto.Text = rs!AD02DESDPTO
          End If
          sscboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
          rs.MoveNext
      Loop
  End If
  rs.Close
  qry.Close
  'se cargan los recursos del Dpto. seleccionado
  lvwRec.ColumnHeaders.Add , , , 2000
  Call pCargarRecursos(intDptoSel)
  
  dtcDateCombo1(0).Date = strFecha_Sistema
  dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
  
  Call objApp.SplashOff
End Sub

Private Sub pCargarRecursos(intDptoSel%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    ''sscboRecurso.RemoveAll
    lvwRec.ListItems.Clear
    ''lngRecursoSel = 0
    ''sscboRecurso.Text = "TODOS"
    ''sscboRecurso.AddItem lngRecursoSel & Chr$(9) & sscboRecurso.Text
    Set item = lvwRec.ListItems.Add(, , "TODOS")
    item.Tag = 0
    item.Selected = True

    sql = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        ''sscboRecurso.AddItem rs!AG11CODRECURSO & Chr$(9) & rs!AG11DESRECURSO
        Set item = lvwRec.ListItems.Add(, , rs!AG11DESRECURSO)
        item.Tag = rs!AG11CODRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
  If grdDBGrid1(Index).SelBookmarks.Count > 0 Then
    If grdDBGrid1(Index).Columns("Historia").CellText(grdDBGrid1(Index).SelBookmarks(0)) <> grdDBGrid1(Index).Columns("Historia").Text Then
      MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
      grdDBGrid1(Index).SelBookmarks.RemoveAll
      grdDBGrid1(Index).SelBookmarks.Add (grdDBGrid1(Index).RowBookmark(grdDBGrid1(Index).Row))
      Exit Sub
    End If
  End If
  grdDBGrid1(Index).SelBookmarks.Add (grdDBGrid1(Index).RowBookmark(grdDBGrid1(Index).Row))
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
  'Change the sort order and refresh the grid (3rd parameter = true)
  pSortGrid grdDBGrid1(intIndex), grdDBGrid1(intIndex).Columns(ColIndex).Name, True
End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  
  Dim sDatosPersona     As String
  
    If intIndex > 0 Then
        grdDBGrid1(intIndex).Columns("Fecha Cita").Text = Format(grdDBGrid1(intIndex).Columns("Fecha Cita").Text, "DD/MM/YYYY - HH:NN")
        'grdDBGrid1(intIndex).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(intIndex).Columns("Fecha Preferencia").Text, "DD/MM/YYYY - HH:NN")
        'AJO 12/06/1999 Incluir en lista el n�mero de historia  :  grdDBGrid1(intIndex).Columns("Persona").Text = GetPersonName(Val(grdDBGrid1(intIndex).Columns("Persona").Value))
        sDatosPersona = GetPersonName(Val(grdDBGrid1(intIndex).Columns("Paciente").Value))
        grdDBGrid1(intIndex).Columns("Paciente").Text = Trim(Mid(sDatosPersona, 9, Len(sDatosPersona) - 7))
        grdDBGrid1(intIndex).Columns("Historia").Text = Trim(Left(sDatosPersona, 7))
        'grdDBGrid1(intIndex).Columns("Persona Solicitante").Text = GetStaffName(Val(grdDBGrid1(intIndex).Columns("Persona Solicitante").Value))
        Select Case intIndex
            Case 1
                If grdDBGrid1(1).Columns("Indice de Asignaci�n").Text = 0 Then
                    grdDBGrid1(1).Columns("Tipo de Asignaci�n").Text = "Asignaci�n Manual"
                End If
                If grdDBGrid1(1).Columns("Indice de Asignaci�n").Text = -1 Then
                    grdDBGrid1(1).Columns("Tipo de Asignaci�n").Text = "Asiganci�n Autom�tica"
                End If
        End Select
    End If
End Sub
Private Sub mnuEstadoOpcion_Click(intIndex As Integer)
  Call objWinInfo.DataRefresh
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub






Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   Call objWinInfo.DataRefresh
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

'eventos del combo
Private Sub sscboDpto_Click()
'    If intDptoSel <> sscboDpto.Columns(0).Value Then Call pVaciarGrid
    intDptoSel = sscboDpto.Columns(0).Value
    Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
    Call pCargarRecursos(intDptoSel)
End Sub

Private Sub sscboRecurso_Click()
    lngRecursoSel = sscboRecurso.Columns(0).Value
End Sub

Private Sub sscboRecurso_CloseUp()
    lngRecursoSel = sscboRecurso.Columns(0).Value
End Sub


Private Sub tlbtoolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Call objWinInfo.WinProcess(cwProcessToolBar, Button.Index, 0)
End Sub

Private Sub pVerDatosActuacion(lngNumActPlan&, Optional blnMostrarSoloSiHayDatos As Boolean)
'**************************************************************************************
'*  Llama a la pantalla que muestra los datos (observ., indic., intruccines...) de la
'*  actuaci�n y los cuestionarios (cuestionarios + restricciones)
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = True significa que s�lo hay que mostrar la
'*  pantalla en el caso de que exista alguna observ., indic. o pregunta en el cuestionario.
'*  En este caso se hace previamente el Load de la pantalla, la cual devolver� en un Pipe
'*  la informaci�n necesaria para saber si hay que hacer el Show o el Unload
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = False se muestra la pantalla directamente
'**************************************************************************************

    'se establece el Pipe con el n� de actuaci�n planificada
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    
    'se mira la opci�n seleccionada: cargar pantalla previamente o mostrar directamente
    If blnMostrarSoloSiHayDatos Then 'cargar pantalla previamente
        Call objPipe.PipeSet("PR_VerSoloSiHayDatos", True)
        'se elimina el posible Pipe devuelto por frmObservAct en operaciones previas
        If objPipe.PipeExist("PR_frmObservAct") Then Call objPipe.PipeRemove("PR_frmObservAct")
        'se carga en memoria la pantalla
        Load frmObservAct
        'se mira si hay que mostrarla o descargarla
        If objPipe.PipeExist("PR_frmObservAct") Then
            If objPipe.PipeGet("PR_frmObservAct") = True Then
                frmObservAct.Show vbModal
            Else
                Unload frmObservAct
            End If
            Call objPipe.PipeRemove("PR_frmObservAct")
        Else
            Unload frmObservAct
        End If
        Call objPipe.PipeRemove("PR_VerSoloSiHayDatos")
    Else 'mostrar pantalla directamente
        Call objPipe.PipeSet("PR_NactPedi", grdDBGrid1(0).Columns("Actuaci�n Pedida").Value)
        Call objPipe.PipeSet("PR_CodEstado", grdDBGrid1(0).Columns("CodEstado").Value)
            frmObservAct.Show vbModal
        Call objPipe.PipeRemove("PR_NactPedi")
        Call objPipe.PipeRemove("PR_CodEstado")
         Set frmObservAct = Nothing
        If objPipe.PipeExist("PR_INDI") Then
          grdDBGrid1(0).Columns("Indicaciones a la Actuaci�n").Value = objPipe.PipeGet("PR_INDI")
          Call objPipe.PipeRemove("PR_INDI")
        End If
    End If
End Sub
Private Sub pModificaCitas(ssGrid As SSDBGrid)
Dim i As Integer

For i = 0 To ssGrid.SelBookmarks.Count - 1
    If grdDBGrid1(0).Columns("CodEstado").CellText(ssGrid.SelBookmarks(i)) = 6 Then
        MsgBox "Actuaci�n Cancelada", vbCritical
    Else
        Call objSecurity.LaunchProcess("CI1150", grdDBGrid1(0).Columns("N�mero").CellText(ssGrid.SelBookmarks(i)))
    End If
Next i

End Sub
