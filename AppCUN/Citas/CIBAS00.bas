Attribute VB_Name = "General"
Option Explicit

Global modifCitas(10) As String

Public objCW As clsCW       ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As clsCWPipeLine
Public objGen As clsCWGen
Public objEnv As clsCWEnvironment
Public objError As clsCWError
Public objMouse As clsCWMouse
Public objSecurity As clsCWSecurity

Public blnRefrescar As Boolean
Public blnBorrarSel As Boolean
Public blnAceptar As Boolean
Public strWherePersDup As String
Public vntCodPersDup As Variant
Public blnCommit As Boolean

Public Const gstrSinDoctor As String = "SDR"

'Variables para consultar ACUNSA
'**********************************
Public blnPerFisica As Boolean 'se utiliza tambien en el caso de universidad
Public lngNumPoliza() As Long
Public lngNumHistoria As Long
Public blnAsocHistoria As Boolean
Public strApelAsegu  As String
Public strNombAsegu  As String

'**********************************

'Variables para consultar Personal de universidad
'**********************************
Public lngNumCodEmpl() As Long

'Variables para consultar los Huecos (frmHuecos)
'**********************************
'Se devuelve el c�digo de persona de la pantalla de Mantenimiento de Personas F�sicas
Public lngCodPers As Long
Public lngHistoria As Long

'**********************************
'Variable que se utiliza para que cuando se entra por
'segunda vez en la pantalla de personas fisicas  se
'desactiven algunos botones
Public blnSegunda As Boolean
'**********************************
'Variable global que indica si
'quiere guardar la direcci�n de los responsables
'como la direccion del paciente
'**********************************
'Variables para guardar la direccion del paciente
'en la direccion del responsable economico
' y responsable familiar
Public lCodigoIgual     As Long
Public bIgualDireccion  As Boolean


Public Const ciPersF�sicas           As String = "CI1017"
Public Const ciSoluciones            As String = "CI1126"
Public Const ciRecordatorio          As String = "CI1023"
Public Const ciFasesRecursos         As String = "CI1027"
Public Const ciRecitacion            As String = "CI1029"
Public Const ciCitarSinPlanificar    As String = "CI2001"
Public Const ciFasesSolucion         As String = "CI1036"

Public Const ciCitasConfirmadas      As String = "CI0019"
Public Const ciEstCitasConfirmadas   As String = "CI0020"

Public Const DptPediatria As String = "121"
Public lngUserCode As Long
Public lngNumPeticion As Long
Public lngDeptCode As Long
Public blnDataReadOnly As Boolean

Public blnHayCodPer As Boolean
Public Sub bDBUpdateCamaAnuReservada(Cama As String, NumAct As Long)
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
 'On Error GoTo activarerror
    'Updatemos como Null la cama en la tabla AD1500 la cama reservada
    sStmSql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ? "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = Cama
    qryUpdate.Execute
    qryUpdate.Close
   
    'Updatemos como Null la cama en la tabla CI0100 la cama reservada
    sStmSql = "UPDATE CI0100 SET AD15CODCAMA = Null "
    sStmSql = sStmSql & "WHERE  PR04NUMACTPLAN = ?  "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = NumAct  'Num_Actuaci�n
    qryUpdate.Execute
    qryUpdate.Close

   
End Sub

Public Sub pAnular(NumActPlan As Long, Cama As String)
Dim sql As String
Dim qry As rdoQuery

   'lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
   'If lngincidencia <> -1 And lngincidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
    On Error GoTo Canceltrans
    objApp.BeginTrans
      sql = "UPDATE PR0400 SET PR37CODESTADO=6,PR04FECCANCEL=SYSDATE WHERE PR04NUMACTPLAN=?"
      Set qry = objApp.rdoConnect.CreateQuery(" ", sql)
      qry(0) = NumActPlan
      qry.Execute
      'Set respuesta = qry.OpenResultset()
      qry.Close
      sql = "UPDATE CI0100 SET CI01SITCITA=2 WHERE PR04NUMACTPLAN=?"
      Set qry = objApp.rdoConnect.CreateQuery(" ", sql)
      qry(0) = NumActPlan
      'Set respuesta = qry.OpenResultset()
      qry.Execute
      qry.Close
      'grdDBGrid1.SelBookmarks.Remove (Val(grdDBGrid1.SelBookmarks(0)))
      If Cama <> "" Then 'hay que ir a anular la cama
            Call bDBUpdateCamaAnuReservada(Cama, NumActPlan)
      End If
      objApp.CommitTrans
      Exit Sub
Canceltrans:
     objApp.RollbackTrans
     MsgBox "Error en la anulacion", vbCritical, "Error"
        
End Sub







Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objMouse = objCW.objMouse
  Set objSecurity = objCW.objSecurity
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  
  'End
End Sub

'******************************************************************
'Funci�n que devuelve el siguiente n� de c�digo al �ltimo
'c�digo asignado.
'Tiene como par�metro de entrada una sentencia Sql que servira
'para crear el cursor
'******************************************************************

Public Function GetNewCode(strSql As String) As Long
 'Creaci�n de variables recordset
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  
 'Apertura del cursor
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
 'Devuelvo el nuevo n� de c�digo
  If rdoCodigo.RowCount = 0 Then
    GetNewCode = 1
  Else
    If IsNull(rdoCodigo(0)) Then
      GetNewCode = 1
    Else
      GetNewCode = rdoCodigo(0) + 1
    End If
  End If
 
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

'Public Function GetNewCode(strSQL As String) As Long
' 'Creaci�n de variables recordset
'  Dim rdoCodigo As rdoResultset
'  Dim rdoQueryCod As rdoQuery
'
' 'Apertura del cursor
'  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSQL)
'
' 'Establezco el n� de registros a recuperar a 1
'  rdoQueryCod.MaxRows = 1
'  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
'
' 'Devuelvo el nuevo n� de c�digo
'  If rdoCodigo.RowCount = 0 Then
'    GetNewCode = 1
'  Else
'    If IsNull(rdoCodigo(0)) Then
'      GetNewCode = 1
'    Else
'      GetNewCode = rdoCodigo(0) + 1
'    End If
'  End If
'
' 'Cierro cursores
'  rdoCodigo.Close
'  rdoQueryCod.Close
'
'End Function


'Devuelve una colecci�n de valores con las columnas de una tabla
'para la primera fila que cumple la condicion de strSql
Public Function GetTableColumn(strSql As String) As Collection
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  Dim intCol As Integer
  Dim cllValues As New Collection
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
  If rdoCodigo.RowCount = 0 Then
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
    Next
    
    'Set GetTableColumn = Nothing
  Else
    rdoCodigo.MoveFirst
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       If Not IsNull(rdoCodigo.rdoColumns(intCol).Value) Then
          Call cllValues.Add(rdoCodigo.rdoColumns(intCol).Value, rdoCodigo.rdoColumns(intCol).Name)
       Else
          Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
       End If
    Next
  End If
 Set GetTableColumn = cllValues
 Set cllValues = Nothing
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

Public Sub ShowDetail2(grdGrid As SSDBGrid)
  Dim strSql            As String
  Dim cllDatos As New Collection
  Dim lngDetPeticion As Long
  Dim blnDetListaEspera As Boolean
  Dim datDetFecPref As Date
  Dim lngDetRecPref As Long
  Dim lngNumPersona As Long
  Dim strFechaCita As String
  Dim lngCodDpto As Long
  
  On Error Resume Next
  
  Load frmDetalleCita
  With frmDetalleCita
    .txtText1(7) = "PENDIENTE CONFIRMAR"
    .txtText1(2) = grdGrid.Columns("Actuaci�n Pedida").Value
    strSql = "select PR09NUMPETICION,PR03FECPREFEREN,CI21CODPERSONA,AD02CODDPTO from pr0300 where PR03NUMACTPEDI=" & Val(grdGrid.Columns("Actuaci�n Pedida").Value)
    Set cllDatos = GetTableColumn(strSql)
    .txtText1(0) = cllDatos("PR09NUMPETICION")
    lngDetPeticion = cllDatos("PR09NUMPETICION")
    If cllDatos("PR03FECPREFEREN") <> "" Then
        datDetFecPref = cllDatos("PR03FECPREFEREN")
        .txtText1(11) = Format(datDetFecPref, "DD/MM/YYYY")
        .txtText1(12) = Format(datDetFecPref, "HH:NN")
    End If
    lngNumPersona = cllDatos("CI21CODPERSONA")
    lngCodDpto = cllDatos("AD02CODDPTO")
    strSql = "select ad02desdpto from ad0200 where AD02CODDPTO=" & lngCodDpto
    .txtText1(8) = GetTableColumn(strSql)("ad02desdpto")
    .IdPersona1.Text = lngNumPersona
    .IdPersona1.Enabled = False
    
    If .txtText1(0) <> "" Then
       strSql = "select pr09numgrupo from pr0900 where pr09numpeticion=" & lngDetPeticion
       .txtText1(1) = GetTableColumn(strSql)("pr09numgrupo")
    End If
    .txtText1(3) = grdGrid.Columns("N�mero").Value
    .txtText1(4) = grdGrid.Columns("Solicitud").Value
    .txtText1(5) = grdGrid.Columns("N�mero Cita").Value
    If .txtText1(5) <> "" Then
        Set cllDatos = Nothing
        strSql = "Select ag11codrecurso,ci01indlisespe,ag05numinciden,ci01sitcita from ci0100 where ci31numsolicit=" & grdGrid.Columns("Solicitud").Value & " and ci01numcita=" & grdGrid.Columns("N�mero Cita").Value
        Set cllDatos = GetTableColumn(strSql)
        .txtText1(15) = cllDatos("ag05numinciden")
        strSql = "select ag05desinciden from ag0500 where ag05numinciden=" & Val(.txtText1(15))
        .txtText1(16) = GetTableColumn(strSql)("ag05desinciden")
    
        lngDetRecPref = Val(cllDatos("ag11codrecurso"))
        If lngDetRecPref <> 0 Then
            strSql = "select ag11desrecurso from ag1100 where ag11codrecurso=" & lngDetRecPref
            .txtText1(10) = GetTableColumn(strSql)("ag11desrecurso")
        End If
        lngDetRecPref = 0
        .chkCheck1(0).Value = -Val(cllDatos("ci01indlisespe"))
        Select Case cllDatos("ci01sitcita")
            Case ciPdteConfirmar, ""
                .txtText1(7) = "PENDIENTE CONFIRMAR"
            Case ciConfirmada, "C"
                .txtText1(7) = "CITADA"
            Case ciAnulada, "A"
                .txtText1(7) = "ANULADA"
            Case ciRecitada, "RE"
                .txtText1(7) = "RECITADA"
            Case ciPdteRecitar, "PR"
                .txtText1(7) = "PENDIENTE RECITAR"
            Case ciReservada
                .txtText1(7) = "RESERVADA"
            Case Else
                .txtText1(7) = ""
        End Select
    End If
    
    '.txtText1(11) = Format(datDetFecPref, "DD/MM/YYYY")
    '.txtText1(12) = Format(datDetFecPref, "HH:NN")
    strFechaCita = objGen.ReplaceStr(grdGrid.Columns("Fecha Cita").Value, "-", " ", 0)
    .txtText1(13) = Format(strFechaCita, "DD/MM/YYYY")
    .txtText1(14) = Format(strFechaCita, "HH:NN")
    .txtText1(6) = grdGrid.Columns("Descripci�n").Value
    strSql = "select AG11CODRECURSO from PR1400 where PR03NUMACTPEDI=" & Val(grdGrid.Columns("Actuaci�n Pedida").Value) _
         & " AND PR14INDRECPREFE<>0"
    lngDetRecPref = GetTableColumn(strSql)("AG11CODRECURSO")
    If lngDetRecPref <> 0 Then
       strSql = "select ag11desrecurso from ag1100 where ag11codrecurso=" & lngDetRecPref
       .txtText1(9) = GetTableColumn(strSql)("ag11desrecurso")
    End If
                
    .chkCheck1(0).Enabled = False
   ' .IdPersona1.SearchPersona
    .Show (vbModal)
  End With
  Unload frmDetalleCita
  Set frmDetalleCita = Nothing
End Sub

'Funci�n que genera un nuevo c�digo de incidencia.
'Tiene dos par�metros de entrada :
'  -intCodMotInci -> C�digo del Motivo de Incidencia
'  -blnHideFrmInciden -> Si es True no saca la ventana de incidencias
'La funci�n devuelve:
'   -El nuevo numero de incidencia ( si todo ha ido bien )
'   -Si el intCodMotInci pasado no existe devuelve -1
'   -Si se cancela la operaci�n o intCodMotInci no est� activo
'    devuelve 0
Public Function AddInciden(intCodInci As Integer, Optional blnHideFrmInciden As Boolean = False) As Variant
  ' Variable resultset y query para la descripci�n del motivo
  Dim rdoMotivoInci As rdoResultset
  Dim rdoQueryMotivo As rdoQuery
  Dim rdoTiposIncidencia As rdoResultset
  
' Variable para la sentencia Sql
  Dim strSql As String
  
  Load frmIncidencias
' Inicializamos blnRollback a false
  blnRollback = False
  blnActive = True
' Abrimos el cursor para obtener la descripci�n del motivo
  
  strSql = "select AG06DESMOTINCI,AG06INDINCDISM, AG06FECACTIVA from AG0600 " _
         & "where AG06CODMOTINCI = " & intCodInci
  
  Set rdoQueryMotivo = objApp.rdoConnect.CreateQuery("", strSql)
 'Establecemos el tama�o del cursor a 1
  rdoQueryMotivo.MaxRows = 1
  Set rdoMotivoInci = rdoQueryMotivo.OpenResultset(rdOpenKeyset, _
                                                rdConcurReadOnly)
  
' Miramos si existe el c�digo del motivo de incidencia
  If rdoMotivoInci.RowCount = 0 Then
    'No existe el codigo de motivo incidencia
     Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & intCodMotInci)
     Call objError.Raise
     
    ' MsgBox "No existe el c�digo de motivo incidencia N�" & intCodMotInci, vbExclamation, "AGENDA. Incidencias"
     blnRollback = True
     AddInciden = -1
     
  Else
    'Si la fecha activa del c�digo de incidencia es
    'frmincidenciasnor que la actual visualizamos el form
    If CDate(rdoMotivoInci("AG06FECACTIVA")) < CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy")) Then
      With frmIncidencias
      If blnHideFrmInciden Then
        .cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
        Call .A�adir_Incidencia(intCodInci)
        blnHideFrmInciden = False
      Else
        'Asignamos la descripci�n del motivo de incidencia
        .ConfigureWindow (Val(rdoMotivoInci("AG06INDINCDISM")))
        If Val(rdoMotivoInci("AG06INDINCDISM")) = 1 Then
           .cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
        Else
          strSql = "SELECT AG06CODMOTINCI, AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06INDINCDISM=" & Val(rdoMotivoInci("AG06INDINCDISM")) & " AND AG06FECACTIVA < " & objGen.ValueToSQL(objGen.GetDBDateTime, cwDate)
          Set rdoTiposIncidencia = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
          Do While Not rdoTiposIncidencia.EOF
            Call .cboSSDBCombo1(0).AddItem(rdoTiposIncidencia(0) & "," & rdoTiposIncidencia(1))
            rdoTiposIncidencia.MoveNext
          Loop
          .cboSSDBCombo1(0).Value = intCodInci
          rdoTiposIncidencia.Close
        End If
        intCodMotInci = intCodInci
        .Show (vbModal)
      
        .MousePointer = vbDefault
      
      End If
      End With
      If blnRollback Then
        AddInciden = 0 ' Han pulsado cancelar
      Else
        AddInciden = vntNuevoCod
      End If
    Else
     'Si la fecha es mayor cancelamos la incidencia
     'pero no realizamos rollback
      AddInciden = 0
      blnActive = False
      
    End If
  End If
  Unload frmIncidencias
  Set frmIncidencias = Nothing
End Function

'Procedimiento que muestra los datos de un numero de incidencia

Public Sub ShowInciden(lngNumInciden As Long)
  Dim strSql As String
  Dim cllCodInciden As New Collection
  Dim cllIncidencia As New Collection
  
  strSql = "SELECT AG06CODMOTINCI,AG05DESALCANCE,AG05DESSOLUCIO FROM AG0500 WHERE AG05NUMINCIDEN=" & lngNumInciden
  Set cllIncidencia = GetTableColumn(strSql)
  If cllIncidencia(1) <> "" Then
    strSql = "SELECT AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06CODMOTINCI=" & Val(cllIncidencia(1))
    Set cllCodInciden = GetTableColumn(strSql)
    If cllCodInciden(1) = "" Then
      'No existe el codigo de motivo incidencia
      Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & cllCodInciden(0))
      Call objError.Raise
    Else
      Load frmIncidencias
      With frmIncidencias
        Call .ConfigureWindow(Val(cllCodInciden(2)))
        .cboSSDBCombo1(0).Text = cllCodInciden(1)
        .cboSSDBCombo1(0).Enabled = False
        .txtText1(1) = cllIncidencia(2)
        If Val(cllCodInciden(2)) = 1 Then
          .txtText1(2) = cllIncidencia(3)
        End If
        .txtText1(1).Locked = True
        .txtText1(2).Locked = True
        .cmdCommand1(0).Enabled = False
        .cmdCommand1(1).Enabled = False
        .Show (vbModal)
      End With
      Unload frmIncidencias
      Set frmIncidencias = Nothing
    End If
  Else
    Call objError.SetError(cwCodeMsg, "No existe el N�mero de Incidencia")
    Call objError.Raise
  
  End If
End Sub
Public Sub Main()
 'No hace nada
End Sub
Public Sub PrintReport(strHistoric As String)
  Dim objReport As New clsCWReport
  Dim strReportDesc As String
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
 
  Select Case strHistoric
    Case ciCitasConfirmadas
       strReportDesc = "Citas Conformadas"
    Case ciEstCitasConfirmadas
      strReportDesc = "Estad�stica de Citas Gestionadas"
  End Select
  If strReportDesc <> "" Then
    Call objReport.AddReport(strHistoric, strReportDesc)
    
    With objReport.PrinterDialog(False, strHistoric)
      intReport = .Selected
      If intReport > 0 Then
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub



'Devuelve el Historia/Caso correspondiente a un Proceso/Asistencia
Public Sub pProcesoAsist(lngNumHistoria As Long, intNumCaso As Integer, lngCodPro As Long, _
                                                                     lngCodAsis As Long)
Dim sql As String
Dim qryRespuesta As rdoQuery
Dim rstRespuesta As rdoResultset

sql = "SELECT AD08NUMCASO FROM AD0800 "
sql = sql & "WHERE  AD07CODPROCESO= ? AND AD01CODASISTENCI=?"
Set qryRespuesta = objApp.rdoConnect.CreateQuery("ProcesoAsistencia", sql)
   qryRespuesta(0) = lngCodPro
   qryRespuesta(1) = lngCodAsis
Set rstRespuesta = qryRespuesta.OpenResultset()

If IsNull(rstRespuesta(0)) Then
    lngNumHistoria = -1
    intNumCaso = -1
'    MsgBox "Historia/caso no encontrados"
    Exit Sub
End If

lngNumHistoria = Left(rstRespuesta(0), 6)
intNumCaso = Right(rstRespuesta(0), 4)
rstRespuesta.Close
qryRespuesta.Close
Set rstRespuesta = Nothing
Set qryRespuesta = Nothing

End Sub

Public Function fNextClaveSeq(campo$) As Long
'obtiene el siguiente c�digo a utilizar en un registro nuevo utilizando los sequences
    
    Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
    fNextClaveSeq = rsMaxClave(0)
    rsMaxClave.Close
    
End Function

Public Function llenar(strnombre As Variant, _
intLength As Integer, strCh As String, blnIzq As Boolean) As String
Dim strTextError As String
Dim i As Integer, j As Integer


If Len(strCh) <> 1 Then
    strTextError = "Car�cter de relleno no v�lido"
'    MsgBox strTextError, vbCritical, "Error en Pac.llenar"
    Exit Function
End If

If IsNull(strnombre) Then strnombre = strCh

strnombre = "" & strnombre


If Len(strnombre) > intLength Then
    
    If blnIzq Then
        llenar = Mid(strnombre, Len(strnombre) - intLength + 1)
    Else
        llenar = Mid(strnombre, 1, intLength)
    End If
Else
    j = intLength - Len(strnombre)
        
    llenar = ""
    For i = 1 To j
            llenar = llenar & strCh
    Next i
        
    If blnIzq = True Then
            llenar = llenar & strnombre
        Else
            llenar = strnombre & llenar
    End If

End If

End Function

'POSIBLEMENTE ESTA FUNCION SE QUITE ****************

Public Function HallarNumActPlan(lngNumSolicitud As Long, NumCita As Byte) As Long
   Dim QyConsulta As rdoQuery
  Dim QyInsert As rdoQuery
  Dim RsRespuesta As rdoResultset
  Dim sql As String


    sql = "SELECT PR04NUMACTPLAN "
    sql = sql & "FROM CI0100 "
    sql = sql & "WHERE CI31NUMSOLICIT = ?"
    sql = sql & " AND CI01NUMCITA=?"
    
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = lngNumSolicitud
    QyConsulta(1) = NumCita

Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
HallarNumActPlan = RsRespuesta(0)
End Function

Private Function blnOver2000(strYYYYMMDD As String) As Boolean
If CInt(Left(strYYYYMMDD, 4)) >= 2000 Then
    blnOver2000 = True
Else
    blnOver2000 = False
End If
End Function





Public Function BuscaResponsable(lngCodPersona As Double) As String
    Dim strSql As String
    Dim rscod As rdoResultset
    
        strSql = " SELECT CI21CODPERSONA_REC FROM CI2200 WHERE  CI21CODPERSONA=" & lngCodPersona
        Set rscod = objApp.rdoConnect.OpenResultset(strSql)
        If Not IsNull(rscod.rdoColumns(0)) Then
            BuscaResponsable = rscod.rdoColumns(0)
        Else
            BuscaResponsable = lngCodPersona
        End If
End Function


