VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Begin VB.Form frmPerJurNW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Personas Jur�dicas"
   ClientHeight    =   4155
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11445
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   11445
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame dpersona 
      Caption         =   "Personas Jur�dicas"
      ForeColor       =   &H00800000&
      Height          =   2310
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9825
      Begin VB.Frame fraFacturacion 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   375
         Left            =   5520
         TabIndex        =   52
         Top             =   1080
         Width           =   3195
         Begin SSDataWidgets_B.SSDBCombo cboFacturacion 
            Height          =   315
            Left            =   0
            TabIndex        =   6
            Top             =   0
            Width           =   3135
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(1).Width=   4577
            Columns(1).Caption=   "DES"
            Columns(1).Name =   "DES"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5530
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
      End
      Begin VB.TextBox txtCodPer 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1320
         TabIndex        =   0
         Top             =   300
         Width           =   1275
      End
      Begin VB.TextBox txtCif 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   720
         Width           =   1395
      End
      Begin VB.TextBox txtAttde 
         Height          =   285
         Left            =   3360
         MaxLength       =   40
         TabIndex        =   4
         Top             =   720
         Width           =   5295
      End
      Begin VB.TextBox txtDiremail 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1260
         MaxLength       =   40
         TabIndex        =   5
         Top             =   1080
         Width           =   1995
      End
      Begin VB.TextBox txtRazonSocial 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   3660
         MaxLength       =   300
         TabIndex        =   1
         Top             =   300
         Width           =   4995
      End
      Begin VB.CommandButton cmdEliminarPer 
         Caption         =   "&Eliminar"
         Height          =   315
         Left            =   8775
         TabIndex        =   26
         Top             =   1380
         Width           =   975
      End
      Begin VB.CommandButton cmdBuscarPer 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   8775
         TabIndex        =   25
         Top             =   975
         Width           =   975
      End
      Begin VB.CommandButton cmdNuevoPer 
         Caption         =   "&Nuevo"
         Height          =   315
         Left            =   8760
         TabIndex        =   24
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdGuardarPer 
         Caption         =   "&Guardar"
         Enabled         =   0   'False
         Height          =   315
         Left            =   8775
         TabIndex        =   23
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtObserPer 
         Height          =   705
         Left            =   1260
         MaxLength       =   2000
         TabIndex        =   7
         Top             =   1485
         Width           =   7425
      End
      Begin VB.Label Label1 
         Caption         =   "Destino Carta de reclamacion:"
         Height          =   255
         Left            =   3360
         TabIndex        =   51
         Top             =   1140
         Width           =   2175
      End
      Begin VB.Label lblLabel 
         Alignment       =   1  'Right Justify
         Caption         =   "Cod.Persona:"
         Height          =   255
         Index           =   3
         Left            =   300
         TabIndex        =   50
         Top             =   300
         Width           =   945
      End
      Begin VB.Label lblLabel 
         Alignment       =   1  'Right Justify
         Caption         =   "Raz�n social:"
         Height          =   255
         Index           =   0
         Left            =   2640
         TabIndex        =   31
         Top             =   300
         Width           =   990
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n e-mail:"
         Height          =   195
         Index           =   1
         Left            =   60
         TabIndex        =   30
         Top             =   1080
         Width           =   1170
      End
      Begin VB.Label lblLabel 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Att. de:"
         Height          =   195
         Index           =   2
         Left            =   2820
         TabIndex        =   29
         Top             =   780
         Width           =   510
      End
      Begin VB.Label lblLabel 
         Alignment       =   1  'Right Justify
         Caption         =   "CIF:"
         Height          =   255
         Index           =   8
         Left            =   960
         TabIndex        =   28
         Top             =   720
         Width           =   285
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones:"
         Height          =   195
         Index           =   35
         Left            =   120
         TabIndex        =   27
         Top             =   1500
         Width           =   1110
      End
   End
   Begin VB.Frame ddireccion 
      Caption         =   "Direcci�n"
      ForeColor       =   &H00800000&
      Height          =   1740
      Left            =   0
      TabIndex        =   32
      Top             =   2340
      Width           =   11385
      Begin VB.Frame fradir 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   180
         TabIndex        =   53
         Top             =   240
         Width           =   5475
         Begin SSDataWidgets_B.SSDBCombo cbopaisdir 
            Height          =   285
            Left            =   720
            TabIndex        =   10
            Top             =   0
            Width           =   2010
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   3545
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16776960
         End
         Begin SSDataWidgets_B.SSDBCombo cboprovdir 
            Height          =   285
            Left            =   3660
            TabIndex        =   11
            Top             =   0
            Width           =   1785
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   3149
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Pais:"
            Height          =   195
            Index           =   23
            Left            =   300
            TabIndex        =   55
            Top             =   60
            Width           =   345
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Provincia:"
            Height          =   195
            Index           =   22
            Left            =   2940
            TabIndex        =   54
            Top             =   0
            Width           =   705
         End
      End
      Begin VB.CommandButton cmdBuscarCalle 
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   285
         Left            =   4020
         TabIndex        =   39
         Top             =   960
         Width           =   390
      End
      Begin VB.Frame frachkdir 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   9060
         TabIndex        =   38
         Top             =   1320
         Width           =   915
         Begin VB.CheckBox chkdir 
            Alignment       =   1  'Right Justify
            Caption         =   "Principal"
            Height          =   255
            Left            =   0
            TabIndex        =   22
            Top             =   60
            Width           =   915
         End
      End
      Begin VB.VScrollBar vsDirec 
         Height          =   1425
         Left            =   10080
         Min             =   1
         TabIndex        =   37
         Top             =   240
         Value           =   1
         Width           =   240
      End
      Begin VB.CommandButton cmdbuscarcodpostal 
         Caption         =   "..."
         Enabled         =   0   'False
         Height          =   285
         Left            =   4020
         TabIndex        =   36
         Top             =   600
         Width           =   390
      End
      Begin VB.CommandButton cmdNuevoDir 
         Caption         =   "Nue&vo"
         Height          =   375
         Left            =   10350
         TabIndex        =   35
         Top             =   225
         Width           =   885
      End
      Begin VB.CommandButton cmdGuardarDir 
         Caption         =   "Guar&dar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   10350
         TabIndex        =   34
         Top             =   600
         Width           =   885
      End
      Begin VB.TextBox txtlocalidaddir 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   900
         MaxLength       =   200
         TabIndex        =   13
         Top             =   600
         Width           =   3075
      End
      Begin VB.TextBox txtcodpostal 
         Height          =   285
         Left            =   4860
         MaxLength       =   5
         TabIndex        =   14
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtcalle 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   900
         MaxLength       =   60
         TabIndex        =   16
         Top             =   975
         Width           =   3060
      End
      Begin VB.TextBox txtportal 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   5025
         MaxLength       =   10
         TabIndex        =   17
         Top             =   975
         Width           =   540
      End
      Begin VB.TextBox txtresdir 
         Height          =   285
         Left            =   6840
         MaxLength       =   30
         TabIndex        =   18
         Top             =   975
         Width           =   3255
      End
      Begin VB.TextBox txttfnodir 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   6300
         MaxLength       =   15
         TabIndex        =   12
         Top             =   225
         Width           =   1335
      End
      Begin VB.TextBox txtfax 
         Height          =   285
         Left            =   6300
         MaxLength       =   15
         TabIndex        =   15
         Top             =   600
         Width           =   1305
      End
      Begin VB.TextBox txtObserDir 
         Height          =   285
         Left            =   900
         MaxLength       =   2000
         TabIndex        =   19
         Top             =   1350
         Width           =   8025
      End
      Begin VB.CommandButton cmdEliminarDir 
         Caption         =   "E&liminar"
         Height          =   375
         Left            =   10350
         TabIndex        =   33
         Top             =   1275
         Width           =   885
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcbofecini 
         Height          =   315
         Left            =   8475
         TabIndex        =   20
         Top             =   225
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         ShowCentury     =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcbofecfin 
         Height          =   315
         Left            =   8475
         TabIndex        =   21
         Top             =   600
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483639
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Localidad:"
         Height          =   195
         Index           =   24
         Left            =   75
         TabIndex        =   49
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "C.P:"
         Height          =   195
         Index           =   25
         Left            =   4440
         TabIndex        =   48
         Top             =   660
         Width           =   300
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Calle:"
         Height          =   195
         Index           =   26
         Left            =   375
         TabIndex        =   47
         Top             =   1050
         Width           =   390
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Portal:"
         Height          =   195
         Index           =   27
         Left            =   4500
         TabIndex        =   46
         Top             =   1050
         Width           =   450
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Resto direcc:"
         Height          =   195
         Index           =   28
         Left            =   5775
         TabIndex        =   45
         Top             =   1050
         Width           =   945
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Tfno:"
         Height          =   195
         Index           =   29
         Left            =   5775
         TabIndex        =   44
         Top             =   300
         Width           =   375
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Fax:"
         Height          =   195
         Index           =   30
         Left            =   5700
         TabIndex        =   43
         Top             =   675
         Width           =   375
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "F. Inicio"
         Height          =   195
         Index           =   31
         Left            =   7725
         TabIndex        =   42
         Top             =   300
         Width           =   555
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin"
         Height          =   195
         Index           =   32
         Left            =   7800
         TabIndex        =   41
         Top             =   675
         Width           =   690
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observ:"
         Height          =   195
         Index           =   33
         Left            =   150
         TabIndex        =   40
         Top             =   1425
         Width           =   555
      End
   End
   Begin VB.CommandButton cmdTipEcon 
      Caption         =   "Mant. Tip. Econ."
      Height          =   375
      Left            =   9900
      TabIndex        =   8
      Top             =   1920
      Width           =   1395
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   9900
      TabIndex        =   2
      Top             =   120
      Width           =   1395
   End
End
Attribute VB_Name = "frmPerJurNW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public lngPerJur As Long
Dim lngPaciente As Long
Dim blnDatosCargados As Boolean
Dim blnCambiosPer As Boolean
Dim blnCambiosDir As Boolean
Dim blnDatosDirCargados As Boolean
Dim blnnuevadir As Boolean
Dim blnNuevaPer As Boolean
Dim rsdir As rdoResultset
Dim blnSalir As Boolean
Dim blnLlamada As Boolean 'Si se ha entrado en blanco o con una persona ya cargada

Private Sub pDesHabilitarControles()
Dim con As Control

For Each con In Me
If TypeOf con Is TextBox Then
    con.Locked = True
    con.BackColor = objApp.objUserColor.lngReadOnly
End If
If TypeOf con Is CommandButton Then
    con.Enabled = False
End If
If TypeOf con Is SSDBCombo Then
    con.BackColor = objApp.objUserColor.lngReadOnly
End If
If TypeOf con Is SSDateCombo Then
    con.Enabled = False
    con.BackColor = objApp.objUserColor.lngReadOnly
End If
Next
txtCodPer.Enabled = True
cmdsalir.Enabled = True
cmdnuevoper.Enabled = True
cmdbuscarper.Enabled = True
cmdnuevodir.Enabled = False
cmdeliminardir.Enabled = False

End Sub
Private Sub pHabilitarControles()
Dim con As Control

For Each con In Me
If TypeOf con Is SSDBCombo Or TypeOf con Is SSDateCombo Or TypeOf con Is TextBox Then
    con.Enabled = True
    If con.Name = "cbopaisdir" Or con.Name = "txtlocalidaddir" Or con.Name = "txtcalle" Or con.Name = "txttfnodir" Or con.Name = "txtRazonSocial" Or con.Name = "dcbofecini" Or con.Name = "txtportal" Then
        con.BackColor = objApp.objUserColor.lngMandatory
    Else
        con.BackColor = objApp.objUserColor.lngNormal
    End If
End If
If TypeOf con Is CheckBox Then con.Enabled = True
If TypeOf con Is CommandButton Then
    con.Enabled = False
End If
Next
cmdsalir.Enabled = True
cmdnuevoper.Enabled = True
cmdnuevodir.Enabled = True
cmdbuscarper.Enabled = True
cmdTipEcon.Enabled = True
End Sub
'Private Sub pGuardarDirVector(BlnAbort As Boolean)
'Dim indice As Integer
'Dim I As Integer
'Dim strDatos As String
'If blnNuevaPer Then  'Or blnnuevo Then
'    MsgBox "Debe guardar primero los datos de la persona", vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
'    BlnAbort = True
'    Exit Sub
'End If
'strDatos = fComprobaciones
'If Len(strDatos) > 0 Then
'    strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
'    MsgBox strDatos, vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
'    BlnAbort = True
'    Exit Sub
'End If
'blnCambiosDir = False
'cmdGuardarDir.Enabled = False
'cmdNuevoDir.Enabled = True
'End Sub
Private Sub pControlCambios(blnAbort As Boolean)
Dim IntRes As Integer
Dim strDatos As String
If cmdguardarper.Enabled = True Or cmdguardardir.Enabled = True Then
    IntRes = MsgBox("�Desea guardar los cambios?", vbQuestion + vbYesNo, Me.Caption)
    If IntRes = vbYes Then
        strDatos = fComprobaciones
        If Len(strDatos) > 0 Then
            strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
            MsgBox strDatos, vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
            blnAbort = True
            Exit Sub
        End If
        Call pGuardarPer
    End If
    blnCambiosDir = False
    blnCambiosPer = False
    cmdguardarper.Enabled = False
    cmdguardardir.Enabled = False
End If
End Sub
Function fSigCodpersona() As Long 'Obtiene el siguiente codigo de persona hasta que se cree un sequence en la B.D.

Dim sql As String
Dim rsmax As rdoResultset

sql = "select max(ci21codpersona) from ci2100 "
Set rsmax = objApp.rdoConnect.OpenResultset(sql)
fSigCodpersona = rsmax(0) + 1
rsmax.Close


End Function
Private Function fNombre(strnombre As String) As String
Select Case strnombre
Case "txtRazonSocial"
    fNombre = "Raz�n Social,"
Case "cbopaisdir"
    fNombre = "Pais de Direcci�n, "
Case "txtlocalidaddir"
    fNombre = "Localidad de Direcci�n, "
Case "txtcalle"
    fNombre = "Calle, "
Case "txtportal"
    fNombre = "Portal, "
Case "txttfnodir"
    fNombre = "Tel�fono de Direcci�n, "
Case "dcbofecini"
    fNombre = "Fecha de Inicio de Vigencia de la Direcci�n, "
End Select

End Function
Private Sub pGuardarDir(lngCodPer As Long, Optional lngndir As Long)
Dim sql As String
Dim rsmax As rdoResultset
Dim qry As rdoQuery
Dim lngchardir As Long
Dim rsnum As rdoResultset
Dim intDirPrincipal As Integer

sql = "select max(ci10numdirecci) from ci1000 where ci21codpersona=" & lngCodPer
Set rsmax = objApp.rdoConnect.OpenResultset(sql)
    If blnnuevadir Or blnNuevaPer Then
        sql = "INSERT INTO CI1000 "
        sql = sql & " (CI19CODPAIS,CI26CODPROVI,"
        sql = sql & "CI10CALLE,CI10PORTAL,CI10RESTODIREC,"
        sql = sql & "CI07CODPOSTAL,CI10DESLOCALID,CI10DESPROVI,"
        sql = sql & "CI10TELEFONO,CI10OBSERVACIO,CI10FECINIVALID,"
        sql = sql & "CI10INDDIRPRINC,CI10FECFINVALID,CI10FAX,CI21CODPERSONA,CI10NUMDIRECCI)"
        sql = sql & " VALUES (?,?,?,?,?,?,?,?,?,?,TRUNC(SYSDATE),?,TO_DATE(?,'DD/MM/YYYY'),?,?,?)"
    Else 'es una modificacion
        sql = "UPDATE CI1000 SET "
        sql = sql & "CI19CODPAIS=?,CI26CODPROVI=?,"
        sql = sql & " CI10CALLE=?,CI10PORTAL=?,CI10RESTODIREC=?,"
        sql = sql & " CI07CODPOSTAL=?,CI10DESLOCALID=?,CI10DESPROVI=?,"
        sql = sql & " CI10TELEFONO=?,CI10OBSERVACIO=?,CI10FECINIVALID=TO_DATE('" & dcbofecini.Text & "','DD/MM/YYYY'),"
        sql = sql & " CI10INDDIRPRINC=?,CI10FECFINVALID=TO_DATE(?,'DD/MM/YYYY'),CI10FAX=?"
        sql = sql & " WHERE CI21CODPERSONA=?"
        sql = sql & " AND CI10NUMDIRECCI=?"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    If Trim(cbopaisdir.Text) = "" Then
    qry(0) = Null
    Else
    qry(0) = Val(cbopaisdir.Columns(0).Value)
    End If
    If cbopaisdir.Columns(0).Value = 45 Then
        If Trim(cboprovdir.Text) = "" Then
        qry(1) = Null
        Else
        qry(1) = Val(cboprovdir.Columns(0).Value)
        End If
    Else
     qry(1) = Null
    End If
    qry(2) = txtcalle.Text
    qry(3) = txtportal.Text
    qry(4) = txtresdir.Text
    qry(5) = txtcodpostal.Text
    qry(6) = txtlocalidaddir.Text
    qry(7) = cboprovdir.Text
    qry(8) = txttfnodir.Text
    qry(9) = txtObserDir.Text
    'qry(11) = dcbofecini.Date
    'If rsNum <= 1 Then 'tiene una sola direccion o no tiene ninguna,luego la que estamos guardando tiene que se la principal
    If blnNuevaPer Then
        qry(10) = -1
        chkdir.Value = 1
        frachkdir.Enabled = False
        
    Else
        qry(10) = -chkdir.Value
    End If
    If dcbofecfin.Text = "" Then
    qry(11) = Null
    Else
    qry(11) = dcbofecfin.Text
    End If
    If txtfax.Text = "" Then
        qry(12) = Null
    Else
        qry(12) = txtfax.Text
    End If
    qry(13) = lngCodPer
    If blnnuevadir Or blnNuevaPer Then
        If Not IsNull(rsmax(0)) Then
           qry(14) = rsmax(0) + 1
        Else
           qry(14) = 1
        End If
        Else 'no es nada nuevo
        If lngndir = 0 Then
                qry(14) = rsdir!ci10numdirecci
            Else
                qry(14) = lngndir 'rsdir!ci10numdirecci
        End If
    End If
    'vamos a hacer los cambios de direccion principal
    qry.Execute
    
    qry.Close
    sql = "select count(*) from ci1000 where ci21codpersona=" & lngCodPer
    sql = sql & " AND CI10INDDIRPRINC=-1"
    Set rsnum = objApp.rdoConnect.OpenResultset(sql)
    If rsnum(0) > 1 Then
        sql = "UPDATE CI1000 SET CI10INDDIRPRINC=0 WHERE CI10NUMDIRECCI<>"
        If lngndir <> 0 Then
                sql = sql & lngndir
            Else
            sql = sql & rsdir!ci10numdirecci
        End If
         sql = sql & " AND CI21CODPERSONA=" & lngCodPer
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry.Execute
        qry.Close
    End If
    blnnuevadir = False
    blnNuevaPer = False
    blnCambiosDir = False
    cmdguardardir.Enabled = False
    cmdnuevodir.Enabled = True
   End Sub

Private Sub pGuardarPer()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strDatos As String
Dim IntRes As String
Dim blnAbort As Boolean
Screen.MousePointer = vbHourglass
On Error GoTo canceltrans
    objApp.BeginTrans
    If Not blnNuevaPer Then
            sql = "UPDATE CI2300 SET"
            sql = sql & " CI23RAZONSOCIAL=?,"
            sql = sql & " CI23DIREMAIL=?,"
            sql = sql & " CI23OBSERVACIO=?,"
            sql = sql & " CI23CIF=?,"
            sql = sql & " CI23ATENCION=?"
            sql = sql & " FA21CODDESTCARE=?"
            sql = sql & " WHERE CI21CODPERSONA=?"
        Else 'ES UNO NUEVO
         sql = "INSERT INTO CI2100 (CI21CODPERSONA,CI33CODTIPPERS) VALUES (?," & constPER_JURIDICA & ")"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = fSigCodpersona
                lngPerJur = qry(0)
                qry.Execute
                qry.Close
          sql = "INSERT INTO CI2300 "
            sql = sql & " (CI23RAZONSOCIAL,CI23DIREMAIL,"
            sql = sql & " CI23OBSERVACIO,CI23CIF,"
            sql = sql & " CI23ATENCION,FA21CODDESTCARE,CI21CODPERSONA)"
            sql = sql & " VALUES (?,?,?,?,?,?,?)"
        'End If
        End If
           Set qry = objApp.rdoConnect.CreateQuery("", sql)
            If Trim(txtRazonSocial.Text) = "" Then
                qry(0) = Null
            Else
                qry(0) = txtRazonSocial.Text
            End If
            If Trim(txtDiremail.Text) = "" Then
                qry(1) = Null
            Else
                qry(1) = txtDiremail.Text
            End If
            If Trim(txtObserPer.Text) = "" Then
                qry(2) = Null
            Else
                qry(2) = txtObserPer.Text
            End If
            If Trim(txtCif.Text) = "" Then
                qry(3) = Null
            Else
                qry(3) = txtCif.Text
            End If
            If Trim(txtAttde.Text) = "" Then
                qry(4) = Null
            Else
                qry(4) = txtAttde.Text
            End If
            If cboFacturacion.Text = "" Then
                qry(5) = Null
            Else
                qry(5) = cboFacturacion.Columns(0).Value
            End If
            qry(6) = lngPerJur
            qry.Execute
            If qry.RowsAffected <> 1 Then
                MsgBox "Se ha producido un error al guardar los datos.", vbCritical, Me.Caption
                objApp.RollbackTrans
                Exit Sub
            End If
            
            qry.Close
            Call pGuardarDir(lngPerJur)
            If Not blnSalir Then
                Call pSeldir
            End If
        blnNuevaPer = False
        objApp.CommitTrans
        Screen.MousePointer = vbDefault
        blnCambiosDir = False
        blnCambiosPer = False
        cmdguardarper.Enabled = False
        cmdguardardir.Enabled = False
        blnNuevaPer = False
        txtCodPer.Locked = False
        txtCodPer.BackColor = objApp.objColor.lngNormal
        
    Exit Sub
canceltrans:
        objApp.RollbackTrans
        MsgBox Error, vbCritical, Right(Me.Caption, Len(Me.Caption) - 6)

End Sub
Private Function fComprobaciones() As String
Dim c As Control, strColor$, msg$
    On Error Resume Next
        For Each c In Me
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
              If TypeOf c Is TextBox Or TypeOf c Is SSDBCombo Then
                If Trim(c.Text) = "" Or c.Text = "__:__" Then
                fComprobaciones = fComprobaciones & fNombre(c.Name)
                End If
               End If
              If TypeOf c Is SSDateCombo Then
               If c.Date = "" Then
                fComprobaciones = fComprobaciones & fNombre(c.Name)
               End If
              End If
            End If
        Next
End Function

Private Sub pLimpiarControles(Optional BlnCodper As Boolean)
Dim sql As String
Dim i As Integer
Dim con As Control
lngPerJur = 0
blnCambiosDir = False
blnDatosDirCargados = False
blnDatosCargados = False
blnCambiosPer = False
cmdguardarper.Enabled = False
cmdguardardir.Enabled = False
For Each con In Me
    If TypeOf con Is CheckBox Then con.Value = 0
    If TypeOf con Is TextBox Then
        If con.Name <> "txtCodPer" Then
            con.Text = ""
        Else 'es el c�digo de persona
            If Not BlnCodper Then 'hay que borrar el codigo de persona
                con.Text = ""
            End If
        End If
    End If
    'End If
    If TypeOf con Is SSDateCombo Then con.Text = ""
    If TypeOf con Is SSDBCombo Then
        con.RemoveAll
        con.Text = ""
    End If
Next
frachkdir.Enabled = True
vsDirec.Enabled = False
i = 1
sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900 ORDER BY CI19DESPAIS"
Call pcargarcombos(sql, cbopaisdir)
Do While cbopaisdir.Columns(0).Value <> "45"
    If cbopaisdir.Rows = i Then
        cbopaisdir.Text = ""
        Exit Do
    End If
    cbopaisdir.MoveNext
    i = i + 1
    cbopaisdir.Text = cbopaisdir.Columns(1).Value
Loop
sql = "SELECT FA21CODDESTCARE,FA21DESDESTCARE FROM FA2100"
Call pcargarcombos(sql, cboFacturacion)
i = 0
cboFacturacion.Text = cboFacturacion.Columns(1).Value
Do While cboFacturacion.Columns(0).Value <> 1
    If cboFacturacion.Rows = i Then Exit Do
    cboFacturacion.MoveNext
    i = i + 1
    cboFacturacion.Text = cboFacturacion.Columns(1).Value
Loop

cmdBuscarCalle.Enabled = True
cmdbuscarcodpostal.Enabled = True
dcbofecini.Text = Format(strFecha_Sistema, "dd/mm/yyyy")
End Sub
Private Sub pLimpiarControlesDir()
Dim sql As String
   txtresdir.Text = ""
    txtcodpostal.Text = ""
    txtlocalidaddir.Text = ""
    txtcalle.Text = ""
    txtportal.Text = ""
    txttfnodir.Text = ""
    txtObserDir.Text = ""
    txtfax.Text = ""
    cbopaisdir.RemoveAll
    cbopaisdir.Text = ""
    cboprovdir.RemoveAll
    cboprovdir.Text = ""
    dcbofecini.Text = strFecha_Sistema
    dcbofecini.Date = strFecha_Sistema
    dcbofecfin.Text = ""
    dcbofecfin.Date = ""
    chkdir.Value = 0
    vsDirec.Enabled = True
sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900 order by ci19despais" 'Se cargan los combos de direcci�n
Call pcargarcombos(sql, cbopaisdir)
cbopaisdir.SetFocus
blnCambiosDir = False
blnDatosDirCargados = True
cmdguardardir.Enabled = False
frachkdir.Enabled = True
End Sub
Private Sub pcargarcombos(sql As String, con As SSDBCombo)
Dim rs As rdoResultset
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
    con.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
Loop
End Sub
Private Sub pCargarDatos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim i As Integer
 sql = "SELECT CI23RAZONSOCIAL,CI23DIREMAIL,"
    sql = sql & " CI23OBSERVACIO,CI23CIF,CI23ATENCION,FA21CODDESTCARE "
    sql = sql & " FROM CI2300 WHERE "
    sql = sql & " CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
    Set rs = qry.OpenResultset(rdOpenForwardOnly)
    txtCodPer.Text = lngPerJur
    If Not rs.EOF Then
        If Not IsNull(rs!CI23RAZONSOCIAL) Then
            txtRazonSocial.Text = rs!CI23RAZONSOCIAL
        End If
        If Not IsNull(rs!CI23OBSERVACIO) Then
            txtObserPer.Text = rs!CI23OBSERVACIO
        End If
        If Not IsNull(rs!CI23DIREMAIL) Then
            txtDiremail.Text = rs!CI23DIREMAIL
        End If
        If Not IsNull(rs!CI23CIF) Then
            txtCif.Text = rs!CI23CIF
        End If
        If Not IsNull(rs!CI23ATENCION) Then
            txtAttde.Text = rs!CI23ATENCION
        End If
        If Not IsNull(rs!FA21CODDESTCARE) Then
        i = 0
        cboFacturacion.Text = cboFacturacion.Columns(1).Text
        Do While cboFacturacion.Columns(0).Value <> rs!FA21CODDESTCARE
            If i = cboFacturacion.Rows Then Exit Do
            cboFacturacion.MoveNext
            i = i + 1
            cboFacturacion.Text = cboFacturacion.Columns(1).Text
        Loop
        End If
        blnDatosCargados = True
    End If
End Sub
Private Sub pSeldir()
Dim sql As String
Dim qry As rdoQuery
 sql = "SELECT CI1000.CI16CODLOCALID CODLOCDIR,CI10DESLOCALID DESLOCDIR,CI1000.CI26CODPROVI CODPROVDIR,CI1000.CI10DESPROVI DESPROVDIR,"
    sql = sql & " CI1000.CI19CODPAIS,CI19DESPAIS PAISDIR,"
    sql = sql & " CI10CALLE,CI10PORTAL,"
    sql = sql & " CI10RESTODIREC,"
    sql = sql & " CI10TELEFONO,CI10OBSERVACIO,CI10FECINIVALID,"
    sql = sql & " CI10FECFINVALID,CI10INDDIRPRINC,CI10FAX,"
    sql = sql & " CI07CODPOSTAL,CI10NUMDIRECCI"
    sql = sql & " FROM CI1000,CI1900"
    sql = sql & " WHERE CI21CODPERSONA=?"
    sql = sql & " AND CI1900.CI19CODPAIS=CI1000.CI19CODPAIS(+) ORDER BY CI10INDDIRPRINC"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
Set rsdir = qry.OpenResultset(rdOpenKeyset)
vsDirec.Max = rsdir.RowCount
End Sub

Private Sub cbopaisdir_Change()
Dim sql As String
cboprovdir.RemoveAll
If cbopaisdir.Columns(0).Value = "45" Then
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovdir)
End If
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub


Private Sub cbopaisdir_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub cboprovdir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub cboprovdir_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True

End If

End Sub

Private Sub chkdir_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub cmdBuscarCalle_Click()
Dim vntData(0 To 2)
Dim sql As String
Dim rs As rdoResultset
Dim strCodProv As String
Dim intTipPobla As Integer
If cboprovdir.Text = "" Then
    MsgBox "Debe seleccionar alguna provincia", vbExclamation, Me.Caption
    Exit Sub
End If
If txtlocalidaddir.Text = "" Then
    MsgBox "Debe indicar alguna localidad", vbExclamation, Me.Caption
    Exit Sub
End If

strCodProv = cboprovdir.Columns(1).Value
    vntData(0) = strCodProv
    vntData(1) = UCase(Trim(txtlocalidaddir.Text))
    vntData(2) = UCase(Trim(txtcalle.Text))
    Call objPipe.PipeSet("CODPROVINCIA", vntData(0))
    Call objPipe.PipeSet("LOCALIDAD", vntData(1))
     Call objPipe.PipeSet("CALLE", vntData(2))
    frmcodigos_postales.Show vbModal
    Set frmcodigos_postales = Nothing
    If objPipe.PipeExist("CODPOSTAL") Then
        txtcodpostal.Text = objPipe.PipeGet("CODPOSTAL")
        Call objPipe.PipeRemove("CODPOSTAL")
    End If
    If objPipe.PipeExist("LOCALIDAD") Then
       txtlocalidaddir.Text = objPipe.PipeGet("LOCALIDAD")
        Call objPipe.PipeRemove("LOCALIDAD")
    End If
      If objPipe.PipeExist("CALLE") Then
        txtcalle.Text = objPipe.PipeGet("CALLE")
        Call objPipe.PipeRemove("CALLE")
    End If

End Sub

Private Sub cmdbuscarcodpostal_Click()
Dim vntData(0 To 2)
Dim sql As String
Dim rs As rdoResultset
Dim strCodProv As String
Dim strCodAyuda As Long
Dim intTipPobla As Integer
strCodProv = cboprovdir.Text
If cboprovdir.Text = "" Then
    MsgBox "Debe seleccionar alguna provincia", vbExclamation, Me.Caption
    Exit Sub
End If
vntData(0) = strCodProv
vntData(1) = UCase(Trim(txtlocalidaddir.Text))
Call objPipe.PipeSet("CODPROVINCIA", vntData(0))
Call objPipe.PipeSet("LOCALIDAD", vntData(1))
frmcodigos_postales.Show vbModal
If objPipe.PipeExist("PROVINCIA") Then
    cboprovdir.MoveFirst
    strCodProv = objPipe.PipeGet("PROVINCIA")
    Do While cboprovdir.Columns(0).Value <> strCodProv
        strCodAyuda = cboprovdir.Columns(0).Value
        cboprovdir.MoveNext
        If strCodAyuda = cboprovdir.Columns(0).Value Then Exit Do
    Loop
    cboprovdir.Text = cboprovdir.Columns(1).Value
    Call objPipe.PipeRemove("PROVINCIA")
End If
Set frmcodigos_postales = Nothing
If objPipe.PipeExist("CODPOSTAL") Then
    txtcodpostal.Text = objPipe.PipeGet("CODPOSTAL")
    Call objPipe.PipeRemove("CODPOSTAL")
End If

If objPipe.PipeExist("LOCALIDAD") Then
   txtlocalidaddir.Text = objPipe.PipeGet("LOCALIDAD")
    Call objPipe.PipeRemove("LOCALIDAD")
End If
End Sub

Private Sub cmdbuscarper_Click()
Dim blnAbort As Boolean
Call pControlCambios(blnAbort)
If Not blnAbort Then
    frmBuscaPerJur.Show vbModal
     Set frmBuscaPerJur = Nothing
    If objPipe.PipeExist("CI_CI21CODPERSONA") Then
        Call pHabilitarControles
        Call pLimpiarControles
        lngPerJur = objPipe.PipeGet("CI_CI21CODPERSONA")
        Call objPipe.PipeRemove("CI_CI21CODPERSONA")
        Call pCargarDatos
        Call pSeldir
        Call pLlenarDatosDir
        
    End If
End If
If lngPerJur <> 0 Then
    cmdeliminarper.Enabled = True
    cmdeliminardir.Enabled = True
End If


End Sub

Private Sub cmdEliminarDir_Click()
Dim qry As rdoQuery
Dim sql As String
Dim IntRes As Integer
If blnDatosDirCargados = False Then
    MsgBox "No existe Ninguna Persona", vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
Else
If chkdir.Value = 1 Then
    MsgBox "La direcci�n principal no puede ser eliminada", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
IntRes = MsgBox("�Est� seguro de eliminar los datos?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
If IntRes = vbYes Then

    sql = "DELETE FROM CI1000 WHERE CI21CODPERSONA=?  AND CI10NUMDIRECCI=?"
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
'    If lngNumDir <> 0 Then
'    sql = sql & lngNumDir
'    Else
'    sql = sql & rsdir!ci10numdirecci
'    End If
    qry(1) = rsdir!ci10numdirecci
    qry.Execute
    If qry.RowsAffected <> 1 Then
    GoTo errorborrar
    End If
    Call pSeldir
    'Call pllenardirecciones
    Call pLlenarDatosDir
End If
End If
Exit Sub
errorborrar:
    MsgBox "Ha ocurrido un error al intentar eliminar la direccion"
    Exit Sub
    
End Sub

Private Sub cmdeliminarper_Click()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim IntRes As Integer
If lngPerJur = 0 Then
    MsgBox "Debe seleccionar alguna persona", vbExclamation, Me.Caption
    Exit Sub
End If
sql = "SELECT /*+ ORDERED INDEX(CI2200 CI2205) */ CI22NUMHISTORIA,CI22PRIAPEL,CI22SEGAPEL,CI22NOMBRE  FROM CI2200 WHERE CI21CODPERSONA_REC=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If Not rs.EOF Then
    MsgBox "La persona no puede ser eliminada porque es responsable econ�mico de:" & Chr(13) & "Historia: " & rs(0) & Chr(13) & "Paciente: " & rs(1) & " " & rs(2) & " " & rs(3), vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
sql = "SELECT /*+ ORDERED INDEX(AD1100 AD1103) */ AD01CODASISTENCI,AD07CODPROCESO FROM AD1100 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If Not rs.EOF Then
    MsgBox "La persona no puede ser eliminada porque es responsable econ�mico de Alg�n proceso/asistencia", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
sql = "SELECT COUNT(*) FROM CI1100 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If rs(0) <> 0 Then
     MsgBox "La persona no puede ser eliminada porque tiene un concierto firmado con la cl�nica", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
     Exit Sub
End If
sql = "SELECT COUNT(*) FROM FA1700 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If rs(0) <> 0 Then
     MsgBox "La persona no puede ser eliminada porque tiene pagos realizados", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
     Exit Sub
End If
sql = "SELECT COUNT(*) FROM CI2900 WHERE CI21CODPERSONA_REC=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If rs(0) <> 0 Then
     MsgBox "La persona no puede ser eliminada porque es responsable econ�mico", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
     Exit Sub
End If
sql = "SELECT COUNT(*) FROM FA0400 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If rs(0) <> 0 Then
     MsgBox "La persona no puede ser eliminada porque tiene facturas", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
     Exit Sub
End If
sql = "SELECT COUNT(*) FROM FA6000 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPerJur
Set rs = qry.OpenResultset
If rs(0) <> 0 Then
     MsgBox "La persona no puede ser eliminada porque tiene cartas pendientes", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
     Exit Sub
End If
Screen.MousePointer = vbHourglass



IntRes = MsgBox("Est� seguro de eliminar a la persona seleccionada?", vbQuestion + vbYesNo, Me.Caption)
If IntRes = vbYes Then
    objApp.BeginTrans
    sql = "DELETE FROM CI1000 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
    qry.Execute
    qry.Close
   
    
    sql = "DELETE FROM CI2300 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
    qry.Execute
    If qry.RowsAffected <> 1 Then
        MsgBox "Se ha producido un error al intentar elminar a la persona", vbCritical, Me.Caption
        GoTo canceltrans
    End If
    qry.Close
    sql = "DELETE FROM CI0900 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
    qry.Execute
    qry.Close
     sql = "DELETE FROM CI2100 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPerJur
    qry.Execute
    If qry.RowsAffected <> 1 Then
        MsgBox "Se ha producido un error al intentar elminar a la persona", vbCritical, Me.Caption
        GoTo canceltrans
    End If
    qry.Close
    objApp.CommitTrans
    MsgBox "La persona ha sido eliminada", vbInformation, Me.Caption
    Call pLimpiarControles
End If
Screen.MousePointer = vbDefault
Exit Sub
canceltrans:
    objApp.RollbackTrans
    Screen.MousePointer = vbDefault

    MsgBox Err, vbCritical, Me.Caption
End Sub

Private Sub pGuardarDirTran(lngCodPer As Long)
On Error GoTo canceltrans
objApp.BeginTrans
  Call pGuardarDir(lngCodPer)
objApp.CommitTrans
Exit Sub
canceltrans:
    objApp.RollbackTrans
    MsgBox Error, vbCritical
    Err = 0
End Sub

Private Sub cmdGuardarDir_Click()
Dim blnAbort As Boolean
Dim strDatos As String
If blnNuevaPer Then  'Or blnnuevo Then
    MsgBox "Debe guardar primero los datos de la persona", vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If

strDatos = fComprobaciones
        If Len(strDatos) > 0 Then
            strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
            MsgBox strDatos, vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
            blnAbort = True
            Exit Sub
        End If
If Not blnAbort Then
    Call pGuardarDirTran(lngPerJur)
    If Not blnSalir Then
        Call pSeldir
    End If
End If
End Sub

Private Sub cmdGuardarPer_Click()
Dim blnAbort As Boolean
Dim strDatos As String

strDatos = fComprobaciones
        If Len(strDatos) > 0 Then
            strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
            MsgBox strDatos, vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
            blnAbort = True
            Exit Sub
        End If
If Not blnAbort Then
    Call pGuardarPer
End If
cmdnuevoper.Enabled = True
End Sub

Private Sub cmdNuevoDir_Click()
Dim i As Integer
Dim blnAbort As Boolean
Call pControlCambios(blnAbort)
If Not blnAbort Then
    Call pLimpiarControlesDir
    i = 0
    Do While i < cbopaisdir.Rows
        cbopaisdir.MoveNext
        i = i + 1
        If cbopaisdir.Columns(0).Text = "45" Then
            cbopaisdir.Text = cbopaisdir.Columns(1).Value
            Exit Do
        End If
    Loop
    blnnuevadir = True
    cmdnuevodir.Enabled = False
End If

End Sub

Private Sub cmdNuevoPer_Click()
Dim i As Integer

Dim blnAbort As Boolean
  
Call pControlCambios(blnAbort)
If Not blnAbort Then
    'Call pLimpiarControles
    Call pHabilitarControles
    txtCodPer.Locked = True
    txtCodPer.BackColor = objApp.objUserColor.lngReadOnly
    blnNuevaPer = True
    blnnuevadir = True
    Call pLimpiarControles
    blnDatosDirCargados = True
    blnDatosCargados = True
    cmdnuevoper.Enabled = False
End If
txtRazonSocial.SetFocus
End Sub

Private Sub cmdSalir_Click()
Dim blnAbort As Boolean
blnSalir = True
Call pControlCambios(blnAbort)
blnSalir = False
If Not blnAbort Then
    Unload Me
End If
End Sub

Private Sub cmdTipEcon_Click()
Dim vntData(1 To 2)
If lngPerJur <> 0 Then
vntData(1) = lngPerJur
    Call objSecurity.LaunchProcess("AD3009", vntData)
Else
    MsgBox "Debe seleccionar alguna persona Juridica", vbExclamation, Me.Caption
End If

End Sub
Private Sub dcbofecfin_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub
Private Sub dcbofecfin_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub
Private Sub dcbofecini_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub
Private Sub dcbofecini_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub


Private Sub pLlenarDatosDir()
Dim strCodAyuda As String
Dim sql As String
If Not rsdir.EOF Then

 If Not IsNull(rsdir!PAISDIR) Then
            cbopaisdir.Text = rsdir!PAISDIR
            cbopaisdir.Columns(0).Value = rsdir!CI19CODPAIS
        Else
            cbopaisdir.Text = ""
            cbopaisdir.Columns(0).Value = 0
        End If
        cboprovdir.RemoveAll 'PARA VACIAR EL COMBO POR SI EST� LLENO
        If cbopaisdir.Columns(0).Value = "45" Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovdir)
        End If
        If Not IsNull(rsdir!DESPROVDIR) Then
            cboprovdir.Text = rsdir!DESPROVDIR
        Else
            cboprovdir.Text = ""
            
        End If
        If Not IsNull(rsdir!CODPROVDIR) Then
            cboprovdir.MoveFirst
            Do While cboprovdir.Columns(0).Value <> rsdir!CODPROVDIR
                strCodAyuda = cboprovdir.Columns(1).Value
                cboprovdir.MoveNext
                If strCodAyuda = cboprovdir.Columns(1).Value Then Exit Do
            Loop
            cboprovdir.Text = cboprovdir.Columns(1).Value
            Else
                cboprovdir.Columns(0).Value = 0
        End If
        If Not IsNull(rsdir!DESLOCDIR) Then
            txtlocalidaddir.Text = rsdir!DESLOCDIR
            Else
            txtlocalidaddir.Text = ""
        End If
         If Not IsNull(rsdir!CI10CALLE) Then
            txtcalle.Text = rsdir!CI10CALLE
            Else
            txtcalle.Text = ""
        End If
        If Not IsNull(rsdir!CI10PORTAL) Then
            txtportal.Text = rsdir!CI10PORTAL
            Else
            txtportal.Text = ""
        End If
        If Not IsNull(rsdir!CI10RESTODIREC) Then
            txtresdir.Text = rsdir!CI10RESTODIREC
            Else
            txtresdir.Text = ""
        End If
        If Not IsNull(rsdir!CI10TELEFONO) Then
            txttfnodir.Text = rsdir!CI10TELEFONO
            Else
            txttfnodir.Text = ""
        End If
        If Not IsNull(rsdir!CI10FAX) Then
            txtfax.Text = rsdir!CI10FAX
            Else
            txtfax.Text = ""
        End If
        If Not IsNull(rsdir!CI10FECINIVALID) Then
            dcbofecini.Text = Format(rsdir!CI10FECINIVALID, "dd/mm/yyyy")
            dcbofecini.Date = Format(rsdir!CI10FECINIVALID, "dd/mm/yyyy")
        Else
            dcbofecini.Text = ""
            dcbofecini.Date = ""
        End If
        If Not IsNull(rsdir!CI10FECFINVALID) Then
            dcbofecfin.Text = Format(rsdir!CI10FECFINVALID, "dd/mm/yyyy")
            dcbofecfin.Date = Format(rsdir!CI10FECFINVALID, "dd/mm/yyyy")
            Else
            dcbofecfin.Text = ""
            dcbofecfin.Date = ""
        End If
        If Not IsNull(rsdir!CI10INDDIRPRINC) Then
            chkdir.Value = Abs(rsdir!CI10INDDIRPRINC)
            If chkdir.Value = 1 Then
                frachkdir.Enabled = False
            Else
                frachkdir.Enabled = True
        End If
        If Not IsNull(rsdir!CI10OBSERVACIO) Then
            txtObserDir.Text = rsdir!CI10OBSERVACIO
            Else
            txtObserDir.Text = ""
        End If
         If Not IsNull(rsdir!CI07CODPOSTAL) Then
            txtcodpostal.Text = rsdir!CI07CODPOSTAL
            Else
            txtcodpostal.Text = ""
        End If
        
    End If
     
   End If
blnDatosDirCargados = True
cmdbuscarcodpostal.Enabled = True
cmdBuscarCalle.Enabled = True
vsDirec.Enabled = True
cmdnuevodir.Enabled = True
cmdeliminardir.Enabled = True
cmdguardardir.Enabled = False


End Sub

Private Sub Form_Load()
Dim sql As String
If objPipe.PipeExist("RESP") Then
    lngPerJur = objPipe.PipeGet("RESP")
    Call objPipe.PipeRemove("RESP")
End If
If objPipe.PipeExist("PACIENTE") Then
    lngPaciente = objPipe.PipeGet("PACIENTE")
    Call objPipe.PipeRemove("PACIENTE")
End If
sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900 ORDER BY CI19DESPAIS"
Call pcargarcombos(sql, cbopaisdir)
sql = "SELECT FA21CODDESTCARE,FA21DESDESTCARE FROM FA2100"
Call pcargarcombos(sql, cboFacturacion)
If lngPerJur <> 0 Then 'han entrado desde una pantalla, todo deshabilitado excepto el bot�n de salir
    Call pDesHabilitarControles
    Call pCargarDatos
    Call pSeldir
    Call pLlenarDatosDir
    vsDirec.Enabled = True
    cmdnuevoper.Enabled = False
    cmdbuscarper.Enabled = False
    'txtCodPer.Enabled = False
    cmdnuevodir.Enabled = False
    cmdeliminardir.Enabled = False
    vsDirec.Enabled = False
    frachkdir.Enabled = False
    fraFacturacion.Enabled = False
    fradir.Enabled = False
    cmdBuscarCalle.Enabled = False
    cmdbuscarcodpostal.Enabled = False
    blnLlamada = True

Else
    Call pDesHabilitarControles
    'txtCodPer.Enabled = True
    txtCodPer.Locked = False
    txtCodPer.BackColor = objApp.objUserColor.lngNormal
    vsDirec.Enabled = False
End If
cmdguardarper.Enabled = False
cmdguardardir.Enabled = False
End Sub

Private Sub txtAttde_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtAttde_GotFocus()
txtAttde.SelStart = 0
txtAttde.SelLength = Len(txtAttde.Text)

End Sub

Private Sub txtcalle_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If
End Sub

Private Sub txtcalle_GotFocus()
txtcalle.SelStart = 0
txtcalle.SelLength = Len(txtcalle.Text)

End Sub

Private Sub txtCif_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If
End Sub

Private Sub txtCif_GotFocus()
txtCif.SelStart = 0
txtCif.SelLength = Len(txtCif.Text)

End Sub

Private Sub txtCodPer_Change()
Dim i As Integer
Dim blnAbort As Boolean
If lngPerJur <> 0 And (cmdguardardir.Enabled Or cmdguardarper.Enabled) Then
    Call pControlCambios(blnAbort)
End If
If Not blnAbort And blnDatosCargados Then
    Call pLimpiarControles(True) 'le paso un valor booleano para saber que no tiene que limpiar el c�digo de persona
End If
    
    
End Sub

Private Sub txtCodPer_GotFocus()
txtCodPer.SelStart = 0
txtCodPer.SelLength = Len(txtCodPer.Text)
End Sub

Private Sub txtCodPer_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub

Private Sub txtCodPer_LostFocus()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
    If txtCodPer.Text <> "" Then
        sql = "SELECT COUNT(*) FROM CI2300 WHERE CI21CODPERSONA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Trim(CLng(txtCodPer.Text))
        Set rs = qry.OpenResultset
        If rs(0) = 0 Then
            MsgBox "No existe ninguna persona con ese c�digo", vbExclamation, Me.Caption
            Exit Sub
        End If
        lngPerJur = Val(txtCodPer.Text)
        If Not blnLlamada Then 'para que cuando se entre con una persona ya cargada, al perder
            Call pHabilitarControles 'el foco no se habiliten los controles
            Call pCargarDatos
            Call pSeldir
            Call pLlenarDatosDir
        End If
    End If
    
End Sub

Private Sub txtcodpostal_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub txtcodpostal_GotFocus()
txtcodpostal.SelStart = 0
txtcodpostal.SelLength = Len(txtcodpostal.Text)

End Sub

Private Sub txtDiremail_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True

End If

End Sub

Private Sub txtDiremail_GotFocus()
txtDiremail.SelStart = 0
txtDiremail.SelLength = Len(txtDiremail.Text)

End Sub

Private Sub txtfax_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True

End If

End Sub

Private Sub txtfax_GotFocus()
txtfax.SelStart = 0
txtfax.SelLength = Len(txtfax.Text)

End Sub

Private Sub txtlocalidaddir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True

End If
End Sub

Private Sub txtlocalidaddir_GotFocus()
txtlocalidaddir.SelStart = 0
txtlocalidaddir.SelLength = Len(txtlocalidaddir.Text)

End Sub

Private Sub txtObserDir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True

End If

End Sub

Private Sub txtObserDir_GotFocus()
txtObserDir.SelStart = 0
txtObserDir.SelLength = Len(txtObserDir.Text)

End Sub

Private Sub txtObserPer_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True
End If

End Sub

Private Sub txtObserPer_GotFocus()
txtObserPer.SelStart = 0
txtObserPer.SelLength = Len(txtObserPer.Text)

End Sub

Private Sub txtportal_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True

End If

End Sub

Private Sub txtportal_GotFocus()
txtportal.SelStart = 0
txtportal.SelLength = Len(txtportal.Text)

End Sub

Private Sub txtRazonSocial_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdguardarper.Enabled = True

End If

End Sub

Private Sub txtRazonSocial_GotFocus()
txtRazonSocial.SelStart = 0
txtRazonSocial.SelLength = Len(txtRazonSocial.Text)

End Sub

Private Sub txtresdir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True

End If

End Sub

Private Sub txtresdir_GotFocus()
txtresdir.SelStart = 0
txtresdir.SelLength = Len(txtresdir.Text)

End Sub

Private Sub txttfnodir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdguardardir.Enabled = True
End If

End Sub

Private Sub txttfnodir_GotFocus()
txttfnodir.SelStart = 0
txttfnodir.SelLength = Len(txttfnodir.Text)

End Sub

Private Sub vsDirec_Change()
Dim IntRes As Integer
Dim blnAbort As Boolean
If cmdguardardir.Enabled = True Then
    IntRes = MsgBox("�Desea guardar los cambios en la direcci�n?", vbQuestion + vbYesNo, Me.Caption)
    If IntRes = vbYes Then
        Call pGuardarDir(lngPerJur)
        Call pSeldir
    End If
End If
Call pLimpiarControlesDir
blnDatosDirCargados = False
blnCambiosDir = False
cmdguardardir.Enabled = False
  If vsDirec.Value = vsDirec.Max Then
    rsdir.MoveLast
  Else
    rsdir.Move vsDirec.Value, 1
  End If
  Call pLlenarDatosDir

End Sub
