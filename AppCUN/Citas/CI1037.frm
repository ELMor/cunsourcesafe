VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmSelProvincias 
   Caption         =   "CITAS. C�digos Postales"
   ClientHeight    =   4980
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8535
   ControlBox      =   0   'False
   Icon            =   "CI1037.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4980
   ScaleWidth      =   8535
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   495
      Index           =   1
      Left            =   5520
      TabIndex        =   5
      Top             =   4200
      Width           =   2775
   End
   Begin SSDataWidgets_B.SSDBGrid grdSSDBGrid2 
      Height          =   3135
      Left            =   5520
      TabIndex        =   4
      Top             =   240
      Width           =   2775
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ","
      Col.Count       =   2
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   4154
      Columns(1).Caption=   "Provincia"
      Columns(1).Name =   "Provincia"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      _ExtentX        =   4895
      _ExtentY        =   5530
      _StockProps     =   79
      Caption         =   "Provincias Seleccionadas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdSSDBGrid1 
      Height          =   4455
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   3135
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ","
      Col.Count       =   2
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   4842
      Columns(1).Caption=   "Provincia"
      Columns(1).Name =   "Provincia"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      _ExtentX        =   5530
      _ExtentY        =   7858
      _StockProps     =   79
      Caption         =   "Provincias"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Importar"
      Height          =   495
      Index           =   0
      Left            =   5520
      TabIndex        =   2
      Top             =   3600
      Width           =   2775
   End
   Begin VB.CommandButton Command1 
      Caption         =   "<<"
      Height          =   495
      Index           =   1
      Left            =   3720
      TabIndex        =   1
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   ">>"
      Height          =   495
      Index           =   0
      Left            =   3720
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frmSelProvincias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public strCods As String


Private Sub Command1_Click(intIndex As Integer)
  Dim intPos As Integer
  Dim vntBook As Variant

  If intIndex = 0 Then
    If grdSSDBGrid1.SelBookmarks.Count > 0 Then
    For intPos = 0 To grdSSDBGrid1.SelBookmarks.Count - 1
      vntBook = grdSSDBGrid1.SelBookmarks(intPos)
      grdSSDBGrid2.AddItem grdSSDBGrid1.Columns("Codigo").CellValue(vntBook) & "," & grdSSDBGrid1.Columns("Provincia").CellValue(vntBook)
      Call grdSSDBGrid1.RemoveItem(Val(vntBook))
    Next
    grdSSDBGrid1.SelBookmarks.RemoveAll
    End If
  End If
  If intIndex = 1 Then
    If grdSSDBGrid2.SelBookmarks.Count > 0 Then
    For intPos = 0 To grdSSDBGrid2.SelBookmarks.Count - 1
      vntBook = grdSSDBGrid2.SelBookmarks(intPos)
      grdSSDBGrid1.AddItem grdSSDBGrid2.Columns("Codigo").CellValue(vntBook) & "," & grdSSDBGrid2.Columns("Provincia").CellValue(vntBook)
      Call grdSSDBGrid2.RemoveItem(Val(vntBook))
    
    Next
    grdSSDBGrid2.SelBookmarks.RemoveAll
    
    End If
  End If
  
End Sub

Private Sub Command2_Click(intIndex As Integer)
'Llamada a importar c�digos postales
  Dim vntBook As Variant
  Dim intPos As Integer
  If intIndex = 0 Then
  
    If grdSSDBGrid2.Rows > 0 Then
      grdSSDBGrid2.MoveFirst
      For intPos = 0 To grdSSDBGrid2.Rows - 1
      
        strCods = strCods & "'" & grdSSDBGrid2.Columns("Codigo").Value & "',"
        grdSSDBGrid2.MoveNext
      Next
      strCods = Left(strCods, Len(strCods) - 1)
      frmCargaTextos.strCodProv = " WHERE CODPROV IN (" & strCods & ")"
      Me.Hide
  
    End If
  End If
  If intIndex = 1 Then
    frmCargaTextos.strCodProv = ""
  End If
End Sub

Private Sub Form_Load()
  Dim rdoProvincias As rdoResultset
  Dim strSql As String
  
  strSql = "SELECT CI26CODPROVI,CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
  Set rdoProvincias = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
  Do While Not rdoProvincias.EOF
    grdSSDBGrid1.AddItem rdoProvincias(0) & "," & rdoProvincias(1)
    rdoProvincias.MoveNext
  Loop
End Sub
