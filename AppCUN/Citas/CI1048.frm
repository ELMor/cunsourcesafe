VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{35ED254B-6E76-11D2-A484-00C04F7D9E25}#2.0#0"; "Horario.ocx"
Begin VB.Form frmCitasAgenda 
   Caption         =   "Citacion por Agenda"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   Begin VB.CommandButton cmdCitar 
      Caption         =   "SALIR"
      Height          =   375
      Index           =   2
      Left            =   10080
      TabIndex        =   11
      Top             =   1080
      Width           =   1575
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   3
      Left            =   1320
      TabIndex        =   10
      Top             =   1080
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "Citar"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   10080
      TabIndex        =   9
      Top             =   360
      Width           =   1575
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   2
      Left            =   8400
      TabIndex        =   6
      Top             =   240
      Width           =   1335
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   1
      Left            =   1320
      TabIndex        =   4
      Top             =   720
      Width           =   3615
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   0
      Left            =   1320
      TabIndex        =   2
      Top             =   240
      Width           =   6015
   End
   Begin Control_Agenda.DiaRecurso DiaRecurso1 
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   1440
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   12515
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCmbRecurso 
      Height          =   300
      Left            =   6240
      TabIndex        =   7
      Top             =   720
      Width           =   3480
      DataFieldList   =   "Column 1"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   1349
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4604
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6138
      _ExtentY        =   529
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Recurso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   3
      Left            =   5160
      TabIndex        =   8
      Top             =   720
      Width           =   945
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Historia:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   7440
      TabIndex        =   5
      Top             =   240
      Width           =   885
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Actuaci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Paciente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   990
   End
End
Attribute VB_Name = "frmCitasAgenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public blnCitManual As Boolean

Private Sub cmdCitar_Click(Index As Integer)
Select Case Index
    Case 0
      blnCitManual = True
      frmCitaManual2FI.Show vbModal
      DoEvents
      DiaRecurso1.pRepresentar Val(SSDBCmbRecurso.Columns(0).Value), objApp.rdoConnect, , objSecurity.strUser
    Case 1
      blnCitManual = False
      frmCitaManual2FI.Show vbModal
      DoEvents
      DiaRecurso1.pRepresentar Val(SSDBCmbRecurso.Columns(0).Value), objApp.rdoConnect, , objSecurity.strUser
    Case 2
      Unload frmCitasAgenda
End Select
    
    
End Sub



Private Sub Form_Load()
Call Datos_Paciente(lngPaciente)
txtText1(1).Text = strDesAct
Call Cargar_combo(lngActuacion)

'DiaRecurso1.pRepresentar Val(txtText1(0).Text), objApp.rdoConnect, , objSecurity.strUser
End Sub
Public Sub Datos_Paciente(lngCodPer As Long)
Dim strSql As String
Dim qryA   As rdoQuery
Dim rstA   As rdoResultset

strSql = "SELECT CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI22NUMHISTORIA FROM " & _
         "CI2200 WHERE CI21CODPERSONA = ?"
Set qryA = objApp.rdoConnect.CreateQuery("", strSql)
    qryA(0) = lngCodPer
Set rstA = qryA.OpenResultset()
If Not rstA.EOF Then
    If Not IsNull(rstA(2).Value) Then
        txtText1(0).Text = rstA(1).Value & ", " & rstA(2).Value & ", " & rstA(0).Value
    Else
        txtText1(0).Text = rstA(1).Value & ", " & rstA(0).Value
    End If
    If Not IsNull(rstA(3).Value) Then
        txtText1(2).Text = rstA(3).Value
    Else
        txtText1(2).Text = ""
    End If
End If
rstA.Close
qryA.Close
End Sub

'Funcion para cargar los recursos que realizan la actuacion
'seleccionada
Public Sub Cargar_combo(lngActPlan As Long)

'Consultas SQL
Dim SQL As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset
Dim qryRecursos As rdoQuery
Dim rstRecursos As rdoResultset

SQL = "SELECT AD02CODDPTO, PR01CODACTUACION FROM PR0400 WHERE "
SQL = SQL & "PR04NUMACTPLAN = ? "
Set qryConsulta = objApp.rdoConnect.CreateQuery("", SQL)
    qryConsulta(0).Value = lngActPlan
Set rstConsulta = qryConsulta.OpenResultset()
If Not rstConsulta.EOF Then
    'Recursos
    SQL = "SELECT DISTINCT AG11CODRECURSO, AG11DESRECURSO,AG1400.AG14CODTIPRECU "
    SQL = SQL & " FROM AG1100, AD0300, PR1300, AG1400 "
    SQL = SQL & " WHERE  AD0300.AD02CODDPTO = ? "                                           'Dpto seleccionado
    SQL = SQL & " AND     PR1300.PR01CODACTUACION = ? "                                     '-- actuaci�n seleccionada
    SQL = SQL & " AND     PR1300.PR13INDPREFEREN = -1 "                                     '-- Sea preferente"
    SQL = SQL & " AND     PR1300.PR13INDPLANIF = -1 "                                       '-- Planificable el tipo
    SQL = SQL & " AND     AG1100.AG14CODTIPRECU = AG1400.AG14CODTIPRECU "                   '-- Recursos con tipos de recursos
    SQL = SQL & " AND     PR1300.AG14CODTIPRECU  = AG1400.AG14CODTIPRECU "                  '-- Relacionado con AG1400 tipo recurso
    SQL = SQL & " AND     AD0300.AD03FECINICIO <= SYSDATE "
    SQL = SQL & " AND     (AD0300.AD03FECFIN >= SYSDATE OR AD0300.AD03FECFIN IS NULL) "
    SQL = SQL & " AND     (AD0300.SG02COD = AG1100.SG02COD OR AG1100.SG02COD IS NULL ) "
    SQL = SQL & " AND     AD0300.AD02CODDPTO = AG1100.AD02CODDPTO "
    SQL = SQL & " AND     AG1100.AG11FECINIVREC <= SYSDATE "
    SQL = SQL & " AND     (AG1100.AG11FECFINVREC >= SYSDATE OR AG1100.AG11FECFINVREC IS NULL) "
    SQL = SQL & " ORDER BY AG11DESRECURSO "
    
    Set qryRecursos = objApp.rdoConnect.CreateQuery("", SQL)
       qryRecursos(0) = rstConsulta(0).Value
       qryRecursos(1) = rstConsulta(1).Value
    Set rstRecursos = qryRecursos.OpenResultset()
    Do While Not rstRecursos.EOF
       IntTipRecurso = rstRecursos(2).Value
       SSDBCmbRecurso.AddItem rstRecursos(0) & ";" & rstRecursos(1)
       rstRecursos.MoveNext
    Loop
    rstRecursos.Close
    qryRecursos.Close
End If
txtText1(3).Text = rstConsulta(1).Value
rstConsulta.Close
qryConsulta.Close
End Sub

Private Sub SSDBCmbRecurso_CloseUp()
    DiaRecurso1.pRepresentar Val(SSDBCmbRecurso.Columns(0).Value), objApp.rdoConnect, , objSecurity.strUser
    cmdCitar(0).Enabled = True
'    cmdCitar(1).Enabled = True
End Sub


