VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmHojaFiliacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Hoja de Filiaci�n"
   ClientHeight    =   7650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11475
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7650
   ScaleWidth      =   11475
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdImPrograma 
      Caption         =   "&Imprimir "
      Height          =   495
      Left            =   1140
      TabIndex        =   108
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton cmdSolNH 
      Caption         =   "Solicitar Historia"
      Height          =   495
      Left            =   2220
      TabIndex        =   62
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton cmdetiquetas 
      Caption         =   "E&tiquetas"
      Height          =   495
      Left            =   120
      TabIndex        =   61
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton cmdCamas 
      Caption         =   "Camas"
      Height          =   495
      Left            =   3300
      TabIndex        =   55
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton Cmdcitaconsultas 
      Caption         =   "Cita Consultas"
      Height          =   495
      Left            =   4380
      TabIndex        =   54
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton CmdIngresos 
      Caption         =   "Cita Ingresos"
      Height          =   495
      Left            =   5460
      TabIndex        =   53
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton cmdUniversidad 
      Caption         =   "Consultar U.N."
      Height          =   495
      Left            =   6540
      TabIndex        =   52
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton cmdAcunsa 
      Caption         =   "Consultar AC."
      Height          =   495
      Left            =   7620
      TabIndex        =   51
      Top             =   6840
      Width           =   975
   End
   Begin VB.CommandButton cmdVer 
      Caption         =   "Visi�n Global"
      Height          =   495
      Left            =   8700
      TabIndex        =   50
      Top             =   6840
      Width           =   975
   End
   Begin VB.Frame ddireccion 
      Caption         =   "Direcci�n"
      ForeColor       =   &H00800000&
      Height          =   1740
      Left            =   0
      TabIndex        =   36
      Top             =   5025
      Width           =   11385
      Begin VB.CommandButton cmdBuscarCalle 
         Caption         =   "..."
         Height          =   285
         Left            =   4020
         TabIndex        =   107
         Top             =   960
         Width           =   390
      End
      Begin VB.Frame frachkdir 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   9060
         TabIndex        =   104
         Top             =   1320
         Width           =   915
         Begin VB.CheckBox chkdir 
            Alignment       =   1  'Right Justify
            Caption         =   "Principal"
            Height          =   255
            Left            =   0
            TabIndex        =   105
            Top             =   60
            Width           =   915
         End
      End
      Begin VB.VScrollBar vsDirec 
         Height          =   1425
         Left            =   10050
         TabIndex        =   59
         Top             =   225
         Width           =   240
      End
      Begin VB.CommandButton cmdbuscarcodpostal 
         Caption         =   "..."
         Height          =   285
         Left            =   4020
         TabIndex        =   58
         Top             =   600
         Width           =   390
      End
      Begin VB.CommandButton cmdnuevodir 
         Caption         =   "&Nuevo"
         Height          =   375
         Left            =   10350
         TabIndex        =   57
         Top             =   225
         Width           =   885
      End
      Begin VB.CommandButton cmdguardardir 
         Caption         =   "&Guardar"
         Height          =   375
         Left            =   10350
         TabIndex        =   56
         Top             =   600
         Width           =   885
      End
      Begin VB.TextBox txtlocalidaddir 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   900
         MaxLength       =   30
         TabIndex        =   24
         Top             =   600
         Width           =   3075
      End
      Begin VB.TextBox txtcodpostal 
         Height          =   285
         Left            =   4800
         MaxLength       =   5
         TabIndex        =   25
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtcalle 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   900
         MaxLength       =   40
         TabIndex        =   26
         Top             =   975
         Width           =   3060
      End
      Begin VB.TextBox txtportal 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   5025
         MaxLength       =   10
         TabIndex        =   27
         Top             =   975
         Width           =   540
      End
      Begin VB.TextBox txtresdir 
         Height          =   285
         Left            =   6750
         MaxLength       =   30
         TabIndex        =   28
         Top             =   975
         Width           =   3315
      End
      Begin VB.TextBox txttfnodir 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   6300
         MaxLength       =   15
         TabIndex        =   29
         Top             =   225
         Width           =   1335
      End
      Begin VB.TextBox txtFax 
         Height          =   285
         Left            =   6300
         MaxLength       =   15
         TabIndex        =   30
         Top             =   600
         Width           =   1305
      End
      Begin VB.TextBox txtobservaciones 
         Height          =   285
         Left            =   900
         MaxLength       =   2000
         TabIndex        =   33
         Top             =   1350
         Width           =   8025
      End
      Begin VB.CommandButton cmdeliminardir 
         Caption         =   "&Eliminar"
         Height          =   375
         Left            =   10350
         TabIndex        =   37
         Top             =   1275
         Width           =   885
      End
      Begin SSDataWidgets_B.SSDBCombo cbopaisdir 
         Height          =   285
         Left            =   900
         TabIndex        =   22
         Top             =   225
         Width           =   2010
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   3545
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboprovdir 
         Height          =   285
         Left            =   3750
         TabIndex        =   23
         Top             =   225
         Width           =   1785
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   3149
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcbofecini 
         Height          =   315
         Left            =   8475
         TabIndex        =   31
         Top             =   225
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcbofecfin 
         Height          =   315
         Left            =   8475
         TabIndex        =   32
         Top             =   600
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483639
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Provincia:"
         Height          =   195
         Index           =   22
         Left            =   3000
         TabIndex        =   49
         Top             =   225
         Width           =   705
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Pais:"
         Height          =   195
         Index           =   23
         Left            =   450
         TabIndex        =   48
         Top             =   300
         Width           =   345
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Localidad:"
         Height          =   195
         Index           =   24
         Left            =   75
         TabIndex        =   47
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "C.P:"
         Height          =   195
         Index           =   25
         Left            =   4440
         TabIndex        =   46
         Top             =   660
         Width           =   300
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Calle:"
         Height          =   195
         Index           =   26
         Left            =   375
         TabIndex        =   45
         Top             =   1050
         Width           =   390
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Portal:"
         Height          =   195
         Index           =   27
         Left            =   4500
         TabIndex        =   44
         Top             =   1050
         Width           =   450
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Resto direcc:"
         Height          =   195
         Index           =   28
         Left            =   5775
         TabIndex        =   43
         Top             =   1050
         Width           =   945
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Tfno:"
         Height          =   195
         Index           =   29
         Left            =   5775
         TabIndex        =   42
         Top             =   300
         Width           =   375
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Fax:"
         Height          =   195
         Index           =   30
         Left            =   5700
         TabIndex        =   41
         Top             =   675
         Width           =   375
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "F. Inicio"
         Height          =   195
         Index           =   31
         Left            =   7725
         TabIndex        =   40
         Top             =   300
         Width           =   555
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin"
         Height          =   195
         Index           =   32
         Left            =   7800
         TabIndex        =   39
         Top             =   675
         Width           =   690
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observ:"
         Height          =   195
         Index           =   33
         Left            =   150
         TabIndex        =   38
         Top             =   1425
         Width           =   555
      End
   End
   Begin VB.CommandButton cmdsalir 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   10440
      TabIndex        =   35
      Top             =   6840
      Width           =   975
   End
   Begin VB.Frame dpersona 
      Caption         =   "Datos del paciente"
      ForeColor       =   &H00800000&
      Height          =   4890
      Left            =   0
      TabIndex        =   5
      Top             =   60
      Width           =   11385
      Begin VB.CommandButton cmdavisos 
         Height          =   375
         Left            =   8820
         Style           =   1  'Graphical
         TabIndex        =   106
         Top             =   1440
         Width           =   675
      End
      Begin VB.Frame dnacimiento 
         Caption         =   "Nacimiento"
         ForeColor       =   &H00800000&
         Height          =   1365
         Left            =   75
         TabIndex        =   63
         Top             =   1800
         Width           =   6510
         Begin VB.CheckBox chkfall 
            Alignment       =   1  'Right Justify
            Caption         =   "Fallecido"
            Height          =   315
            Left            =   2520
            TabIndex        =   103
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox txtlocalidadnac 
            Height          =   285
            Left            =   900
            MaxLength       =   30
            TabIndex        =   17
            Top             =   975
            Width           =   5430
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcbofecnac 
            Height          =   285
            Left            =   900
            TabIndex        =   14
            Top             =   225
            Width           =   1575
            _Version        =   65537
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483639
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
         End
         Begin SSDataWidgets_B.SSDBCombo cboprovnac 
            Height          =   285
            Left            =   3825
            TabIndex        =   16
            Top             =   600
            Width           =   2505
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4419
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcbofecfall 
            Height          =   285
            Left            =   4725
            TabIndex        =   64
            Top             =   225
            Width           =   1575
            _Version        =   65537
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483639
            DefaultDate     =   ""
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
         End
         Begin SSDataWidgets_B.SSDBCombo cbopaisnac 
            Height          =   285
            Left            =   900
            TabIndex        =   15
            Top             =   600
            Width           =   2280
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4022
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16776960
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "F. Fallecimiento:"
            Height          =   195
            Index           =   15
            Left            =   3540
            TabIndex        =   69
            Top             =   300
            Width           =   1140
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Localidad:"
            Height          =   195
            Index           =   14
            Left            =   75
            TabIndex        =   68
            Top             =   975
            Width           =   735
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Pais:"
            Height          =   195
            Index           =   13
            Left            =   450
            TabIndex        =   67
            Top             =   675
            Width           =   345
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Prov:"
            Height          =   195
            Index           =   12
            Left            =   3300
            TabIndex        =   66
            Top             =   675
            Width           =   375
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Fecha:"
            Height          =   195
            Index           =   11
            Left            =   300
            TabIndex        =   65
            Top             =   300
            Width           =   495
         End
      End
      Begin VB.TextBox txthistoria 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   975
         TabIndex        =   0
         Top             =   300
         Width           =   675
      End
      Begin VB.TextBox txtape2 
         Height          =   285
         Left            =   975
         MaxLength       =   25
         TabIndex        =   3
         Top             =   1425
         Width           =   2475
      End
      Begin VB.TextBox txtape1 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   975
         MaxLength       =   25
         TabIndex        =   2
         Top             =   1050
         Width           =   2475
      End
      Begin VB.TextBox txtnombre 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   960
         MaxLength       =   25
         TabIndex        =   1
         Top             =   660
         Width           =   2475
      End
      Begin VB.TextBox txtdni 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   7680
         MaxLength       =   12
         TabIndex        =   9
         Top             =   300
         Width           =   1035
      End
      Begin VB.TextBox txtsspro 
         Height          =   285
         Left            =   7725
         MaxLength       =   2
         TabIndex        =   10
         Top             =   675
         Width           =   315
      End
      Begin VB.TextBox txtssnum 
         Height          =   285
         Left            =   8040
         MaxLength       =   8
         TabIndex        =   11
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtssnum2 
         Height          =   285
         Left            =   9180
         MaxLength       =   2
         TabIndex        =   12
         Top             =   675
         Width           =   315
      End
      Begin VB.TextBox txtmovil 
         Height          =   285
         Left            =   7725
         MaxLength       =   12
         TabIndex        =   13
         Top             =   1020
         Width           =   1785
      End
      Begin VB.CommandButton cmdeliminarper 
         Caption         =   "&Eliminar"
         Height          =   315
         Left            =   9675
         TabIndex        =   89
         Top             =   1500
         Width           =   975
      End
      Begin VB.CommandButton cmdbuscarper 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   9675
         TabIndex        =   88
         Top             =   975
         Width           =   975
      End
      Begin VB.CommandButton cmdnuevoper 
         Caption         =   "&Nuevo"
         Height          =   315
         Left            =   9660
         TabIndex        =   87
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdguardarper 
         Caption         =   "&Guardar"
         Height          =   315
         Left            =   9675
         TabIndex        =   86
         Top             =   600
         Width           =   975
      End
      Begin VB.Frame dresponsables 
         Caption         =   "Responsable"
         ForeColor       =   &H00800000&
         Height          =   1140
         Left            =   75
         TabIndex        =   75
         Top             =   3225
         Width           =   11160
         Begin VB.CommandButton cmddatoshistoricos 
            Caption         =   "&Datos Hist�ricos"
            Height          =   285
            Left            =   9480
            TabIndex        =   101
            Top             =   300
            Width           =   1575
         End
         Begin VB.CommandButton cmdbuscarresecon 
            Caption         =   "..."
            Height          =   285
            Left            =   8100
            TabIndex        =   83
            Top             =   675
            Width           =   405
         End
         Begin VB.TextBox txtresecon 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Left            =   975
            Locked          =   -1  'True
            TabIndex        =   82
            Top             =   675
            Width           =   7050
         End
         Begin VB.CommandButton cmdeliminarresecon 
            Caption         =   "&Eliminar "
            Enabled         =   0   'False
            Height          =   285
            Left            =   8550
            TabIndex        =   81
            Top             =   675
            Width           =   855
         End
         Begin VB.TextBox txtrespf 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Left            =   975
            Locked          =   -1  'True
            TabIndex        =   78
            Top             =   300
            Width           =   4010
         End
         Begin VB.CommandButton cmdeliminarresponsablef 
            Caption         =   "&Eliminar "
            Enabled         =   0   'False
            Height          =   285
            Left            =   8550
            TabIndex        =   77
            Top             =   300
            Width           =   855
         End
         Begin VB.CommandButton cmdbuscarresf 
            Caption         =   "..."
            Height          =   285
            Left            =   5025
            TabIndex        =   76
            Top             =   300
            Width           =   405
         End
         Begin SSDataWidgets_B.SSDBCombo cbovinculacion 
            Height          =   285
            Left            =   6525
            TabIndex        =   79
            Top             =   300
            Width           =   1935
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3413
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin vsViewLib.vsPrinter vs 
            Height          =   255
            Left            =   10260
            TabIndex        =   102
            Top             =   720
            Visible         =   0   'False
            Width           =   495
            _Version        =   196608
            _ExtentX        =   873
            _ExtentY        =   450
            _StockProps     =   229
            Appearance      =   1
            BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ConvInfo        =   1418783674
            MarginLeft      =   0
            MarginRight     =   0
            MarginTop       =   0
            MarginBottom    =   0
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Econ�mico:"
            Height          =   195
            Index           =   17
            Left            =   75
            TabIndex        =   85
            Top             =   750
            Width           =   915
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Familiar:"
            Height          =   195
            Index           =   16
            Left            =   300
            TabIndex        =   84
            Top             =   375
            Width           =   720
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Vinculacion:"
            Height          =   195
            Index           =   34
            Left            =   5550
            TabIndex        =   80
            Top             =   360
            Width           =   945
         End
      End
      Begin VB.Frame dprofesion 
         Caption         =   "Datos profesionales"
         ForeColor       =   &H00800000&
         Height          =   1380
         Left            =   6600
         TabIndex        =   70
         Top             =   1800
         Width           =   4605
         Begin VB.TextBox txtempresa 
            Height          =   285
            Left            =   900
            MaxLength       =   20
            TabIndex        =   20
            Top             =   600
            Width           =   3585
         End
         Begin VB.TextBox txtpuesto 
            Height          =   285
            Left            =   900
            MaxLength       =   20
            TabIndex        =   21
            Top             =   975
            Width           =   3585
         End
         Begin VB.TextBox txttfnotrab 
            Height          =   285
            Left            =   3375
            MaxLength       =   15
            TabIndex        =   19
            Top             =   225
            Width           =   1110
         End
         Begin SSDataWidgets_B.SSDBCombo cboprofesion 
            Height          =   285
            Left            =   900
            TabIndex        =   18
            Top             =   225
            Width           =   1995
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   3
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3519
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Profesion:"
            Height          =   195
            Index           =   18
            Left            =   120
            TabIndex        =   74
            Top             =   300
            Width           =   705
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Empresa:"
            Height          =   195
            Index           =   19
            Left            =   150
            TabIndex        =   73
            Top             =   675
            Width           =   660
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Puesto:"
            Height          =   195
            Index           =   20
            Left            =   225
            TabIndex        =   72
            Top             =   1050
            Width           =   540
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Tfno:"
            Height          =   195
            Index           =   21
            Left            =   2925
            TabIndex        =   71
            Top             =   300
            Width           =   375
         End
      End
      Begin VB.TextBox txtobser 
         Height          =   285
         Left            =   1350
         MaxLength       =   2000
         TabIndex        =   34
         Top             =   4425
         Width           =   9885
      End
      Begin SSDataWidgets_B.SSDBCombo cbosexo 
         Height          =   285
         Left            =   2355
         TabIndex        =   4
         Top             =   300
         Width           =   1095
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cbotratam 
         Height          =   285
         Left            =   4725
         TabIndex        =   6
         Top             =   300
         Width           =   2310
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4075
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboestcivil 
         Height          =   285
         Left            =   4725
         TabIndex        =   7
         Top             =   660
         Width           =   2310
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4075
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo cboreludn 
         Height          =   285
         Left            =   4725
         TabIndex        =   8
         Top             =   1050
         Width           =   2325
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "des"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4101
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblLabel 
         Caption         =   "Nombre:"
         Height          =   255
         Index           =   0
         Left            =   300
         TabIndex        =   100
         Top             =   750
         Width           =   630
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido:"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   99
         Top             =   1125
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido:"
         Height          =   195
         Index           =   2
         Left            =   150
         TabIndex        =   98
         Top             =   1500
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Sexo:"
         Height          =   195
         Index           =   3
         Left            =   1800
         TabIndex        =   97
         Top             =   315
         Width           =   405
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Estado civil:"
         Height          =   195
         Index           =   6
         Left            =   3750
         TabIndex        =   96
         Top             =   750
         Width           =   930
      End
      Begin VB.Label lblLabel 
         Caption         =   "N� Historia:"
         Height          =   315
         Index           =   8
         Left            =   150
         TabIndex        =   95
         Top             =   300
         Width           =   825
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Relacion UDN:"
         Height          =   195
         Index           =   10
         Left            =   3525
         TabIndex        =   94
         Top             =   1125
         Width           =   1080
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Tratamiento:"
         Height          =   195
         Index           =   4
         Left            =   3675
         TabIndex        =   93
         Top             =   375
         Width           =   885
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "M�vil:"
         Height          =   195
         Index           =   9
         Left            =   7200
         TabIndex        =   92
         Top             =   1125
         Width           =   420
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "S.S:"
         Height          =   195
         Index           =   7
         Left            =   7350
         TabIndex        =   91
         Top             =   750
         Width           =   300
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "D.N.I:"
         Height          =   195
         Index           =   5
         Left            =   7200
         TabIndex        =   90
         Top             =   375
         Width           =   420
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones:"
         Height          =   195
         Index           =   35
         Left            =   120
         TabIndex        =   60
         Top             =   4500
         Width           =   1110
      End
   End
   Begin VB.Menu mnuImprimir 
      Caption         =   "mnuPrint"
      Visible         =   0   'False
      Begin VB.Menu mnuOpProg 
         Caption         =   "&Programa"
      End
      Begin VB.Menu mnuOpPaciente 
         Caption         =   "&Datos del Paciente"
      End
   End
End
Attribute VB_Name = "frmHojaFiliacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngCodPersonaFis As Long
Dim lngCodPaciente As Long
Dim lngNumHistoria As Long
Dim rs As rdoResultset
Dim rsdir As rdoResultset
Dim blnnuevadir As Boolean
Dim blnNuevaPer As Boolean 'Si se va a insertar una persona nueva
Dim blnnuevo As Boolean
Dim qry As rdoQuery
Dim lngCodPersonaFis_Resecon As Long
Dim lngCodPersonaFis_Resf As Long
Dim lngnumhistoria_ant As Long
Dim blnSalir As Boolean
Dim lngNumDir As Long
Dim blnBuscar As Boolean
Dim blnmodonuevo As Boolean
Dim sql As String
Dim blnLHis As Boolean
Dim blnDatosDirCargados As Boolean 'Cada vez que se cargan los datos de direccion
Dim blnCambiosDir As Boolean 'para controlar si ha habido cambios en la direccion
Dim blnCambiosPer As Boolean 'para controlar los cambios en la persona
Dim blnDatosCargados As Boolean 'cada vez que se cargan los datos de una persona
Dim blnIbm As Boolean 'para saber si tiene datos del ibm
Dim blnInicio As Boolean 'La primera vez que se entra en la pantalla
Dim blnLlamada As Boolean
Private Sub pSuprimir(con As SSDBCombo, KeyCode As Integer)
If KeyCode = 46 Then
    con.Columns(0).Value = "0"
    con.Text = ""
End If
End Sub
Private Sub pHabilitar_Controles()
Dim con As Control

For Each con In Me
If TypeOf con Is SSDBCombo Or TypeOf con Is SSDateCombo Or TypeOf con Is TextBox Or TypeOf con Is CheckBox Then
    con.Enabled = True
    If Not TypeOf con Is CheckBox Then
        If fNombre(con.Name) <> "" Then
            con.BackColor = &HFFFF00
        Else
            con.BackColor = &HFFFFFF
        End If
    End If
End If
Next
txtrespf.BackColor = &HC0C0C0
txtresecon.BackColor = &HC0C0C0
cmdbuscarresf.Enabled = True
cmdbuscarresecon.Enabled = True
cmdbuscarcodpostal.Enabled = True
cmdBuscarCalle.Enabled = True
End Sub
Function fMensajeRes(strTRes As String) As String
 'seldir (lngCodPersonaFis_Resf)
                fMensajeRes = "La direccion principal del Responsable " & strTRes & " es: " & Chr(13)
                If Not IsNull(rsdir!CI10CALLE) Then fMensajeRes = fMensajeRes & rsdir!CI10CALLE & ", "
                If Not IsNull(rsdir!CI10PORTAL) Then fMensajeRes = fMensajeRes & rsdir!CI10PORTAL & " "
                If Not IsNull(rsdir!CI10RESTODIREC) Then fMensajeRes = fMensajeRes & rsdir!CI10RESTODIREC & " "
                fMensajeRes = fMensajeRes & Chr(13)
                If Not IsNull(rsdir!CI07CODPOSTAL) Then fMensajeRes = fMensajeRes & rsdir!CI07CODPOSTAL & " "
                If Not IsNull(rsdir!DESLOCDIR) Then fMensajeRes = fMensajeRes & rsdir!DESLOCDIR & " "
                If Not IsNull(rsdir!DESPROVDIR) Then fMensajeRes = fMensajeRes & rsdir!DESPROVDIR & " "
                If Not IsNull(rsdir!PAISDIR) Then fMensajeRes = fMensajeRes & rsdir!PAISDIR & " "
                fMensajeRes = fMensajeRes & Chr(13)
                If Not IsNull(rsdir!CI10TELEFONO) Then fMensajeRes = fMensajeRes & "Tel�fono: " & rsdir!CI10TELEFONO & ",   "
                If Not IsNull(rsdir!CI10FAX) Then fMensajeRes = fMensajeRes & "Fax: " & rsdir!CI10FAX & ", "
                fMensajeRes = fMensajeRes & Chr(13) & Chr(13)
                fMensajeRes = fMensajeRes & "�Desea Actualizarla a esta?:" & Chr(13)
                If Trim(txtcalle.Text) <> "" Then fMensajeRes = fMensajeRes & txtcalle.Text & ", "
                If Trim(txtportal.Text) <> "" Then fMensajeRes = fMensajeRes & txtportal.Text & " "
                If Trim(txtresdir.Text) <> "" Then fMensajeRes = fMensajeRes & txtresdir.Text & " "
                fMensajeRes = fMensajeRes & Chr(13)
                If Trim(txtcodpostal.Text) <> "" Then fMensajeRes = fMensajeRes & txtcodpostal.Text & " "
                If Trim(txtlocalidaddir.Text) <> "" Then fMensajeRes = fMensajeRes & txtlocalidaddir.Text & " "
                If Trim(cboprovdir.Text) <> "" Then fMensajeRes = fMensajeRes & cboprovdir.Text & " "
                If Trim(cbopaisdir.Text) <> "" Then fMensajeRes = fMensajeRes & cbopaisdir.Text & " "
                fMensajeRes = fMensajeRes & Chr(13)
                If Trim(txttfnodir.Text) <> "" Then fMensajeRes = fMensajeRes & "Tel�fono: " & txttfnodir.Text & ",   "
                If Trim(txtfax.Text) <> "" Then fMensajeRes = fMensajeRes & "Fax: " & txtfax.Text & ", "
                fMensajeRes = Left(fMensajeRes, Len(fMensajeRes) - 2)
  End Function
Private Sub pGuardarPer(blnAbort As Boolean)

Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim vntDigControl
Dim strLetra As String
Dim strDatos As String
Dim IntRes As String
If txthistoria.Text = "" And txtnombre.Text = "" Then
    MsgBox "No existe Ninguna Persona", vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
Else
    strDatos = fComprobaciones
    If Len(strDatos) > 0 Then
        txthistoria.Text = lngnumhistoria_ant
        strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
        MsgBox strDatos, vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
        blnAbort = True
        Exit Sub
    End If
          'Guardar valor del N.I.F sin puntos ni guiones.
          If txtdni.Text <> "" Then
            txtdni.Text = ValidarDNI(txtdni.Text)
            'Validar d�gito control del N.I.F.
            If Right(txtdni.Text, 1) Like "[a-zA-Z]" And _
               Left(Right(txtdni.Text, 2), 1) Like "#" Then
            
              If Right(txtdni.Text, 1) Like "[a-z]" Then
                'D�gito en min�sculas, lo pasamos a may�sculas.
                vntDigControl = UCase(Right(txtdni.Text, 1))
                txtdni.Text = Left(txtdni.Text, Len(txtdni.Text) - 1) & _
                      CStr(vntDigControl)
              End If
            
               strLetra = ValidaDigControlDNI(txtdni.Text)
               If strLetra <> Right(txtdni.Text, 1) Then
                MsgBox "El d�gito de control del D.N.I. es err�neo.", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
                txtdni.SetFocus
                blnAbort = True
                 Exit Sub
               End If
            End If
          End If
          
          'N� S.S.
          'Validar que metan C�d.Provincia y N�.
          If (txtsspro.Text <> "" And txtssnum.Text = "") Or _
            (txtsspro.Text = "" And txtssnum.Text <> "") Then
            'Call objError.SetError(cwCodeMsg,
             MsgBox "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
            'Call objError.Raise
            'blnCancel = True
            txtsspro.SetFocus
            blnAbort = True
            Exit Sub
          End If
          
          'Validar que no metan solo el d�gito de control.
          If txtsspro.Text = "" And txtssnum.Text = "" And _
             txtssnum2 <> "" Then
            'Call objError.SetError(cwCodeMsg,
              MsgBox "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero", vbExclamation + vbOKOnly + Right(Me.Caption, Len(Me.Caption) - 6)
            'Call objError.Raise
    '        nCancel = True
            txtsspro.SetFocus
            blnAbort = True
            Exit Sub
          End If
          
          'Validar C�d.Provincia del N� S.S.
          If txtsspro.Text <> "" Then
             If Val(txtsspro.Text) = 0 Or Val(txtsspro.Text) > 52 Then
                'Call objError.SetError(cwCodeMsg,
                   MsgBox "El C�digo de Provincia del N� Seguridad Social es incorrecto.", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
                'Call objError.Raise
                'blnCancel = True
                txtsspro.SetFocus
                blnAbort = True
                Exit Sub
             End If
          End If
          
          'Validar d�gito de control del N� S.S.
          If (txtsspro.Text <> "" And txtssnum.Text <> "") Then
            If txtssnum2.Text <> "" Then
                If Not ValidaDigControlNSS(Val(txtsspro.Text), _
                    Val(txtssnum.Text), Val(txtssnum2.Text)) Then
                   'Call objError.SetError(cwCodeMsg,
                    MsgBox "El d�gito de control del N�mero de la Seguridad Social es err�neo.", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
                   'Call objError.Raise
                   'blnCancel = True
                   txtssnum.SetFocus
                   blnAbort = True
                   Exit Sub
                End If
            End If
          End If
   If Trim(dcbofecnac.Text) <> "" Then
    If CDate(dcbofecnac.Text) > CDate(strFecha_Sistema) Then
        MsgBox "La fecha de Nacimiento es mayor que la de hoy", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        Exit Sub
        blnAbort = True
      End If
    End If
     If Trim(dcbofecfall.Text) <> "" Then
      If dcbofecfall.Text <> "" Then
       If CDate(dcbofecfall.Text) > CDate(strFecha_Sistema) Then
        MsgBox "La fecha de Fallecimiento es mayor que la de hoy", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        blnAbort = True
        Exit Sub
      End If
    End If
    End If

Screen.MousePointer = vbHourglass
On Error GoTo canceltrans
    objApp.BeginTrans
    'cmdguardardir.Enabled = True
        If Not blnNuevaPer Then  'And txthistoria.Locked = True Es una modificacion
            sql = "UPDATE CI2200 SET"
            sql = sql & " CI22NOMBRE=?,"
            sql = sql & " CI22PRIAPEL=?,"
            sql = sql & " CI22SEGAPEL=?,"
            sql = sql & " CI30CODSEXO=?,"
            sql = sql & " CI34CODTRATAMI=?,"
            sql = sql & " CI14CODESTCIVI=?,"
            sql = sql & " CI28CODRELUDN=?,"
            sql = sql & " CI22DNI=?,"
            sql = sql & " CI22NUMSEGSOC=?,"
            sql = sql & " CI22TFNOMOVIL=?,"
            sql = sql & " CI22FECNACIM=TO_DATE(?,'DD/MM/YYYY'),"
            sql = sql & " CI19CODPAIS=?," 'pais de nacimiento
            sql = sql & " CI26CODPROVI=?," 'provincia de nacimiento
            sql = sql & " CI22DESLOCALID=?," 'localidad de nacimiento
            sql = sql & " CI22FECFALLE=TO_DATE(?,'DD/MM/YYYY'),"
            sql = sql & " CI35CODTIPVINC=?,"
            sql = sql & " CI25CODPROFESI=?,"
            sql = sql & " CI22EMPRESA=?,"
            sql = sql & " CI22PUESTO=?,"
            sql = sql & " CI22TFNOEMPRESA=?,"
            sql = sql & " CI22INDFALLE=?,"
            sql = sql & " CI21CODPERSONA_RFA=?,"
            sql = sql & " CI21CODPERSONA_REC=?,"
            sql = sql & " CI22OBSERVAC=? WHERE CI21CODPERSONA=?"
        Else 'ES UNO NUEVO
        cmdAcunsa.Enabled = True
        cmdavisos.Enabled = True
        cmdCamas.Enabled = True
        Cmdcitaconsultas.Enabled = True
        CmdIngresos.Enabled = True
        cmdSolNH.Enabled = True
        cmdImPrograma.Enabled = True
        cmdUniversidad.Enabled = True
        cmdVer.Enabled = True
        cmdetiquetas.Enabled = True
         sql = "INSERT INTO CI2100 (CI21CODPERSONA,CI33CODTIPPERS) VALUES (?,1)"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = fSigCodpersona
                lngCodPersonaFis = qry(0)
                qry.Execute
                qry.Close
          sql = "INSERT INTO CI2200 "
            sql = sql & " (CI22NOMBRE,CI22PRIAPEL,"
            sql = sql & " CI22SEGAPEL,CI30CODSEXO,"
            sql = sql & " CI34CODTRATAMI,CI14CODESTCIVI,"
            sql = sql & " CI28CODRELUDN,CI22DNI,"
            sql = sql & " CI22NUMSEGSOC,CI22TFNOMOVIL,"
            sql = sql & " CI22FECNACIM,"
            sql = sql & " CI19CODPAIS,CI26CODPROVI," 'PAIS Y provincia de nacimiento
            sql = sql & " CI22DESLOCALID,CI22FECFALLE,"
            sql = sql & " CI35CODTIPVINC,"
            sql = sql & " CI25CODPROFESI,"
            sql = sql & " CI22EMPRESA,CI22PUESTO,"
            sql = sql & " CI22TFNOEMPRESA,CI22INDFALLE,CI21CODPERSONA_RFA,CI21CODPERSONA_REC,"
            sql = sql & " CI22OBSERVAC,CI21CODPERSONA,CI22INDPROVISI)"
            sql = sql & " VALUES (?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,0)"
        'End If
        End If
           Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = txtnombre.Text
            qry(1) = txtape1.Text
            qry(2) = txtape2.Text
            qry(3) = cbosexo.Columns(0).Value
            qry(4) = cbotratam.Columns(0).Value
            If Trim(cboestcivil.Text) = "" Then
            qry(5) = Null
            Else
            qry(5) = cboestcivil.Columns(0).Value
            End If
            If Trim(cboreludn.Text) = "" Or cboreludn.Columns(0).Value = 0 Then
            qry(6) = Null
            Else
            qry(6) = cboreludn.Columns(0).Value
            End If
            qry(7) = txtdni.Text
            qry(8) = txtsspro.Text & txtssnum.Text & txtssnum2.Text
            qry(9) = txtmovil.Text
            If dcbofecnac.Text = "" Then
            qry(10) = Null
            Else
            qry(10) = dcbofecnac.Text
            End If
            If Trim(cbopaisnac.Text) = "" Then
            qry(11) = Null
            Else
            qry(11) = cbopaisnac.Columns(0).Value
            End If
            
            If Trim(cboprovnac.Text) = "" Then
            qry(12) = Null
            Else
            qry(12) = cboprovnac.Columns(0).Value
            End If
            qry(13) = txtlocalidadnac.Text
            If dcbofecfall.Text = "" Then
            qry(14) = Null
            Else
            qry(14) = dcbofecfall.Text
            End If
            If Trim(cbovinculacion.Text) = "" Then
            qry(15) = Null
            Else
            qry(15) = cbovinculacion.Columns(0).Value
            End If
             If Trim(cboprofesion.Text) = "" Or cboprofesion.Columns(0).Value = 0 Then
                qry(16) = Null
            Else
                qry(16) = cboprofesion.Columns(0).Value
            End If
            qry(17) = txtempresa.Text
            qry(18) = txtpuesto.Text
            qry(19) = txttfnotrab.Text
            If dcbofecfall.Text = "" And chkfall.Value = 0 Then
                qry(20) = 0
            Else
                qry(20) = -chkfall.Value
            End If
            If lngCodPersonaFis_Resf <> 0 Then
                qry(21) = lngCodPersonaFis_Resf
            Else
            qry(21) = Null
            End If
            If lngCodPersonaFis_Resecon <> 0 Then
                qry(22) = lngCodPersonaFis_Resecon
            Else
            qry(22) = Null
                
            End If
            qry(23) = txtobser.Text
            qry(24) = lngCodPersonaFis
           
         qry.Execute
         
        End If
   If blnCambiosDir Then 'Esto se quita porque dicen que hay que hacerlo s�lo cuando le den a guardar o cuando se pregunte
    If chkdir.Value = 1 And (lngCodPersonaFis_Resf <> 0 Or lngCodPersonaFis_Resecon <> 0) Then
            If lngCodPersonaFis_Resf <> 0 And lngCodPersonaFis_Resf <> lngCodPersonaFis Then
                Call seldir(lngCodPersonaFis_Resf)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Familiar"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDir(lngCodPersonaFis_Resf)
                    End If
                End If
            End If 'Actualizar responsable familiar
            If lngCodPersonaFis_Resecon <> 0 And lngCodPersonaFis_Resecon <> lngCodPersonaFis And lngCodPersonaFis_Resecon <> lngCodPersonaFis_Resf Then
                Call seldir(lngCodPersonaFis_Resecon)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Econ�mico"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDir(lngCodPersonaFis_Resecon)
                    End If
                End If
            End If
        End If
    End If 'de los cambios de direccion
        Call pGuardarDir(lngCodPersonaFis, lngNumDir)
        objApp.CommitTrans
        Screen.MousePointer = vbDefault
        blnCambiosDir = False
        blnCambiosPer = False
        cmdGuardarPer.Enabled = False
        cmdGuardarDir.Enabled = False
        blnNuevaPer = False
        If Not blnSalir Then
            Call seldir(lngCodPersonaFis)
            Call pdirecciones
        End If
       
    Exit Sub
canceltrans:
        MsgBox Error, vbCritical, Right(Me.Caption, Len(Me.Caption) - 6)
        objApp.RollbackTrans
End Sub
Private Function fNombre(strnombre As String) As String
Select Case strnombre
Case "txtnombre"
    fNombre = "Nombre, "
Case "txtape1"
    fNombre = "Primer Apellido, "
Case "cbosexo"
    fNombre = "Sexo, "
Case "cbotratam"
    fNombre = "Tratamiento, "
Case "cbopaisnac"
    fNombre = "Pais de Nacimiento, "
Case "cbopaisdir"
    fNombre = "Pais de Direcci�n, "
Case "txtlocalidaddir"
    fNombre = "Localidad de Direcci�n, "
Case "txtcalle"
    fNombre = "Calle, "
Case "txtportal"
    fNombre = "Portal, "
Case "txttfnodir"
    fNombre = "Tel�fono de Direcci�n, "
Case "dcbofecini"
    fNombre = "Fecha de Inicio de Vigencia de la Direcci�n, "
End Select

End Function
Private Function fcomprobardir() As String
If Trim(cbopaisdir.Text) = "" Then
    fcomprobardir = fcomprobardir & fNombre("cbopaisdir")
End If
If Trim(txtlocalidaddir.Text) = "" Then
    fcomprobardir = fcomprobardir & fNombre("txtlocalidaddir")
End If
If Trim(txtcalle.Text) = "" Then
    fcomprobardir = fcomprobardir & fNombre("txtcalle")
End If
If Trim(txtportal.Text) = "" Then
    fcomprobardir = fcomprobardir & fNombre("txtportal")
End If
If Trim(txttfnodir.Text) = "" Then
    fcomprobardir = fcomprobardir & fNombre("txttfnodir")
End If
If Trim(dcbofecini.Text) = "" Then
    fcomprobardir = fcomprobardir & fNombre("dcbofecini")
End If


'For Each c In Me.Controls
'    If c.Container.Name = ddireccion.Name Then
'       If TypeOf c Is TextBox Or TypeOf c Is SSDBCombo Then
'            strColor = c.BackColor
'            If strColor = objApp.objUserColor.lngMandatory Then
'                If Trim(c.Text) = "" Or c.Text = "__:__" Then
'                 fcomprobardir = fcomprobardir & fNombre(c.Name)
'                End If
'            End If
'        End If
'        If TypeOf c Is SSDateCombo Then
'            strColor = c.BackColor
'            If strColor = objApp.objUserColor.lngMandatory Then
'               If c.Text = "" Then
'                fcomprobardir = fcomprobardir & fNombre(c.Name)
'               End If
'             End If
'        End If
'    End If
'Next
End Function

Private Function fComprobaciones() As String
Dim c As Control, strColor$, msg$

    'campos obligatorios
    On Error Resume Next
        For Each c In Me
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
              If TypeOf c Is TextBox Or TypeOf c Is SSDBCombo Then
                If Trim(c.Text) = "" Or c.Text = "__:__" Then
                fComprobaciones = fComprobaciones & fNombre(c.Name)
                 'fComprobaciones = True
                 
                End If
               End If
              If TypeOf c Is SSDateCombo Then
               If c.Date = "" Then
                fComprobaciones = fComprobaciones & fNombre(c.Name)
                End If
              End If
            End If
        Next
End Function

Public Function Hay_Persona() As Boolean
If txtnombre.Text <> "" Then
    Hay_Persona = True
Else
    MsgBox "No hay ninguna persona seleccionada", vbInformation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Hay_Persona = False
End If
End Function

Private Sub pImprimirEtiquetas(CodPersona As String)
Dim fila As Integer
Dim col As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim X As Integer
Dim strnombre As String
Dim strFecha As String
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433
vs.StartDoc
vs.CurrentX = intPosX
vs.CurrentY = intPosY

'datos previos
sql = "SELECT CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI22FECNACIM FROM CI2200 WHERE "
sql = sql & "CI21CODPERSONA = ? "
'
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = CodPersona
Set rs = qry.OpenResultset()
If rs.EOF Then
    MsgBox "Persona Fisica inexistente", vbInformation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
If Not IsNull(rs!CI22SEGAPEL) Then
    strnombre = rs!CI22PRIAPEL & " " & rs!CI22SEGAPEL & ", " & rs!CI22NOMBRE
Else
  strnombre = rs!CI22PRIAPEL & ", " & rs!CI22NOMBRE
End If
If Len(strnombre) > 40 Then _
strnombre = Right(strnombre, 40)
If Not IsNull(rs!CI22FECNACIM) Then
    strFecha = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
Else
    strFecha = ""
End If
rs.Close
qry.Close
For fila = 1 To 11
    For col = 1 To 3
      X = vs.CurrentX
      vs.FontSize = 12
      vs.Text = "Hist: "
      vs.FontBold = True
      vs.Text = lngNumHistoria
      vs.FontBold = False
      vs.Text = Chr$(13)
      vs.CurrentX = X
      vs.FontSize = 8
      vs.Text = strnombre
      vs.Text = Chr$(13)
      vs.CurrentX = X
      vs.Text = strFecha
      X = X + intIncX
      vs.CurrentX = X
      vs.CurrentY = intPosY
    Next col
    vs.CurrentX = intPosX
    intPosY = intPosY + intIncY
    vs.CurrentY = intPosY
Next fila
vs.EndDoc
vs.PrintDoc
End Sub
Private Sub pGuardarDirTran(lngCodPer As Long, Optional lngndir As Long)
On Error GoTo canceltrans
objApp.BeginTrans
  Call pGuardarDir(lngCodPer, lngndir)
objApp.CommitTrans
Exit Sub
canceltrans:
    objApp.RollbackTrans
    MsgBox Error, vbCritical
    Err = 0
End Sub
Private Sub pHistoriasSIC(strHistoria As String)
'Funcion para cambiar la direccion en el sistema de
'Historias Clinicas. Solo modificamos la direci�n principal
Dim strSql As String
Dim strDirec As String
Dim QyUpdate As rdoQuery
On Error GoTo errSic

    strDirec = Trim(txtcalle.Text) & " " & Trim(txtportal.Text) & " " & Trim(txtresdir.Text)
    strSql = "  UPDATE PAC SET "
    If Len(strDirec) > 2 Then
        strSql = strSql & "CALLE = '" & Trim(strDirec) & "'"
    End If
    If txtcodpostal.Text <> "" Then
        strSql = strSql & ", CP = " & Trim(txtcodpostal.Text)
    End If
    If cbopaisdir.Text <> "" Then
        strSql = strSql & ", CPAIS = " & Trim(cbopaisdir.Columns(0).Value)
    End If
    If cboprovdir.Text <> "" Then
        strSql = strSql & ", PCIA = '" & Trim(cboprovdir.Text) & "'"
    End If
    If txtlocalidaddir.Text <> "" Then
        strSql = strSql & ", POBL = '" & Trim(txtlocalidaddir.Text) & "'"
    End If
    If txttfnodir.Text <> "" Then
        strSql = strSql & ", TFNO = '" & Trim(txttfnodir.Text) & "'"
    End If
    If txtfax.Text <> "" Then
        strSql = strSql & ", FAX = '" & Trim(txtfax.Text) & "'"
    End If
    strSql = strSql & " WHERE NH = " & strHistoria
    Set QyUpdate = objApp.rdoConnect.CreateQuery("", strSql)
    QyUpdate.Execute
    If QyUpdate.RowsAffected <> 1 Then
        GoTo errSic
    End If
    QyUpdate.Close
    Exit Sub
errSic:
  MsgBox "Ha habido un Error al actualizar la direccion en el SIC", vbCritical
  QyUpdate.Close
End Sub
Private Sub pGuardarDir(lngCodPer As Long, Optional lngndir As Long)
Dim sql As String
Dim rsmax As rdoResultset
Dim qry As rdoQuery
Dim lngchardir As Long
Dim rsnum As rdoResultset
Dim intDirPrincipal As Integer

sql = "select max(ci10numdirecci) from ci1000 where ci21codpersona=" & lngCodPer
Set rsmax = objApp.rdoConnect.OpenResultset(sql)
    If blnnuevadir Or blnNuevaPer Then
        sql = "INSERT INTO CI1000 "
        sql = sql & " (CI19CODPAIS,CI26CODPROVI,"
        sql = sql & "CI10CALLE,CI10PORTAL,CI10RESTODIREC,"
        sql = sql & "CI07CODPOSTAL,CI10DESLOCALID,CI10DESPROVI,"
        sql = sql & "CI10TELEFONO,CI10OBSERVACIO,CI10FECINIVALID,"
        sql = sql & "CI10INDDIRPRINC,CI10FECFINVALID,CI10FAX,CI21CODPERSONA,CI10NUMDIRECCI)"
        sql = sql & " VALUES (?,?,?,?,?,?,?,?,?,?,TRUNC(SYSDATE),?,TO_DATE(?,'DD/MM/YYYY'),?,?,?)"
    Else 'es una modificacion
        sql = "UPDATE CI1000 SET "
        sql = sql & "CI19CODPAIS=?,CI26CODPROVI=?,"
        sql = sql & " CI10CALLE=?,CI10PORTAL=?,CI10RESTODIREC=?,"
        sql = sql & " CI07CODPOSTAL=?,CI10DESLOCALID=?,CI10DESPROVI=?,"
        sql = sql & " CI10TELEFONO=?,CI10OBSERVACIO=?,CI10FECINIVALID=TO_DATE('" & dcbofecini.Text & "','DD/MM/YYYY'),"
        sql = sql & " CI10INDDIRPRINC=?,CI10FECFINVALID=TO_DATE(?,'DD/MM/YYYY'),"
        sql = sql & " CI10FAX=?"
        sql = sql & " WHERE CI21CODPERSONA=?"
        sql = sql & " AND CI10NUMDIRECCI=?"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    If Trim(cbopaisdir.Text) = "" Then
    qry(0) = Null
    Else
    qry(0) = Val(cbopaisdir.Columns(0).Value)
    End If
    If cbopaisdir.Columns(0).Value = 45 Then
        If Trim(cboprovdir.Text) = "" Then
        qry(1) = Null
        Else
        qry(1) = Val(cboprovdir.Columns(0).Value)
        End If
    Else
     qry(1) = Null
    End If
    qry(2) = txtcalle.Text
    qry(3) = txtportal.Text
    qry(4) = txtresdir.Text
    qry(5) = txtcodpostal.Text
    qry(6) = txtlocalidaddir.Text
    qry(7) = cboprovdir.Text
    qry(8) = txttfnodir.Text
    qry(9) = txtobservaciones.Text
    If blnNuevaPer Then
        qry(10) = -1
        chkdir.Value = 1
        frachkdir.Enabled = False
        
    Else
        qry(10) = -chkdir.Value
    End If
    intDirPrincipal = Abs(qry(10))
    If dcbofecfin.Text = "" Then
    qry(11) = Null
    Else
    qry(11) = dcbofecfin.Text
    End If
    If txtfax.Text = "" Then
    qry(12) = Null
    Else
    qry(12) = txtfax.Text
    End If
    qry(13) = lngCodPer
    If blnnuevadir Or blnNuevaPer Or blnmodonuevo Then
        If Not IsNull(rsmax(0)) Then
           qry(14) = rsmax(0) + 1
        Else
           qry(14) = 1
        End If
        Else 'no es nada nuevo
        If lngndir = 0 Then
                qry(14) = rsdir!ci10numdirecci
            Else
                qry(14) = lngndir 'rsdir!ci10numdirecci
        End If
    End If
    'vamos a hacer los cambios de direccion principal
    qry.Execute
    qry.Close
    sql = "select count(*) from ci1000 where ci21codpersona=" & lngCodPer
    sql = sql & " AND CI10INDDIRPRINC=-1"
    Set rsnum = objApp.rdoConnect.OpenResultset(sql)
    If rsnum(0) > 1 Then
        sql = "UPDATE CI1000 SET CI10INDDIRPRINC=0 WHERE CI10NUMDIRECCI<>"
        If lngndir <> 0 Then
                sql = sql & lngndir
            Else
            sql = sql & rsdir!ci10numdirecci
        End If
         sql = sql & " AND CI21CODPERSONA=" & lngCodPer
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry.Execute
        qry.Close
    End If
    blnnuevadir = False
    blnNuevaPer = False
    blnCambiosDir = False
    If intDirPrincipal = 1 Then
        sql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodPer
        Set rs = qry.OpenResultset
        If Not rs.EOF And Not IsNull(rs(0)) Then
            Call pHistoriasSIC(rs(0))
        End If
        rs.Close
        qry.Close
    End If
   End Sub
Private Sub pselcombos()
Dim sql As String
  sql = "SELECT CI30CODSEXO, CI30DESSEXO FROM CI3000"
        Call pcargarcombos(sql, cbosexo)
         sql = "SELECT CI34CODTRATAMI, CI34DESTRATAMI FROM CI3400 ORDER BY CI34DESTRATAMI"
         Call pcargarcombos(sql, cbotratam)
         sql = "SELECT CI14CODESTCIVI,CI14DESESTCIVI FROM CI1400 ORDER BY CI14DESESTCIVI"
        Call pcargarcombos(sql, cboestcivil)
         sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900 ORDER BY CI19DESPAIS"
        Call pcargarcombos(sql, cbopaisnac)
        Call pcargarcombos(sql, cbopaisdir)
         sql = "SELECT CI28CODRELUDN,CI28DESRELUDN FROM CI2800 ORDER BY CI28DESRELUDN"
        Call pcargarcombos(sql, cboreludn)
        sql = "SELECT CI35CODTIPVINC,CI35DESTIPVINC FROM CI3500 ORDER BY CI35DESTIPVINC"
        Call pcargarcombos(sql, cbovinculacion)
        sql = "SELECT CI25CODPROFESI,CI25DESPROFESI FROM CI2500 ORDER BY CI25DESPROFESI"
        Call pcargarcombos(sql, cboprofesion)
       ' Call pMensajes
End Sub
Public Function flngNextClaveseq(strCampo As String) As Long
'Esta funcion hay que utilizarla cuando se haya creado un sequence para ci21codpersona
    Dim rstMaxClave As rdoResultset

    On Error Resume Next
    Err = 0
    Set rstMaxClave = objApp.rdoConnect.OpenResultset("SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL")
        flngNextClaveseq = rstMaxClave(0)
     rstMaxClave.Close
    Set rstMaxClave = Nothing
    If Err > 0 Then flngNextClaveseq = -1
End Function
Private Sub seldir(lngcod As Long)
 sql = "SELECT CI1000.CI16CODLOCALID CODLOCDIR,CI10DESLOCALID DESLOCDIR,CI1000.CI26CODPROVI CODPROVDIR,CI1000.CI10DESPROVI DESPROVDIR,"
    sql = sql & " CI1000.CI19CODPAIS,CI19DESPAIS PAISDIR,"
    sql = sql & " CI10CALLE,CI10PORTAL,"
    sql = sql & " CI10RESTODIREC,"
    sql = sql & " CI10TELEFONO,CI10OBSERVACIO,CI10FECINIVALID,"
    sql = sql & " CI10FECFINVALID,CI10INDDIRPRINC,CI10FAX,"
    sql = sql & " CI07CODPOSTAL,CI10NUMDIRECCI"
    sql = sql & " FROM CI1000,CI1900"
    sql = sql & " WHERE CI21CODPERSONA=?"
    'sql = sql & " AND CI10FECFINVALID IS NULL OR CI10FECFINVALID >TRUNC(SYSDATE)"
    sql = sql & " AND CI1900.CI19CODPAIS=CI1000.CI19CODPAIS(+)ORDER BY CI10INDDIRPRINC"
Set qry = objApp.rdoConnect.CreateQuery("", sql)

    qry(0) = lngcod
Set rsdir = qry.OpenResultset(rdOpenKeyset)


End Sub
Function validardir() As Boolean
Dim intdirprinc As Integer
If blnNuevaPer Or blnnuevo Or blnmodonuevo Then
    If chkdir.Value = 0 Then
        MsgBox "La persona no tiene ninguna direcci�n principal", vbOKOnly + vbCritical, Right(Me.Caption, Len(Me.Caption) - 6) & "Direcciones"
        validardir = True
        Exit Function
    End If
Else
    Call seldir(lngCodPersonaFis)
    Call pdirecciones
    Call pLlenarDatosDir
    'Set rs = qry.OpenResultset(rdOpenKeyset)
    'rs.MoveFirst
    intdirprinc = 0
    Do While Not rsdir.EOF
        If rsdir!CI10INDDIRPRINC = -1 Then ' 0  rsdir!CI10INDDIRPRINC Is Not Null Then
            intdirprinc = intdirprinc + 1
        End If
        rsdir.MoveNext
    Loop
    If intdirprinc = 0 Then
        MsgBox "La persona no tiene ninguna direcci�n principal", vbOKOnly + vbCritical, Right(Me.Caption, Len(Me.Caption) - 6) & "Direcciones"
        validardir = True
    End If
    If intdirprinc > 1 Then
        MsgBox "La persona tiene mas de una direcci�n principal", vbOKOnly + vbCritical, Right(Me.Caption, Len(Me.Caption) - 6) & "Direcciones"
        validardir = True
    End If
End If
'rs.MoveFirst
'Call pdirecciones  31/05/00 por si esto no va muy bien

End Function
Private Sub pLlenarDatosDir()
Dim strCodAyuda As String
Dim sql As String
If Not rsdir.EOF Then

 If Not IsNull(rsdir!PAISDIR) Then
            cbopaisdir.Text = rsdir!PAISDIR
            cbopaisdir.Columns(0).Value = rsdir!CI19CODPAIS
        Else
            cbopaisdir.Text = ""
            cbopaisdir.Columns(0).Value = 0
        End If
        cboprovdir.RemoveAll 'PARA VACIAR EL COMBO POR SI EST� LLENO
        If cbopaisdir.Columns(0).Value = "45" Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovdir)
        End If
        If Not IsNull(rsdir!DESPROVDIR) Then
            cboprovdir.Text = rsdir!DESPROVDIR
        Else
            cboprovdir.Text = ""
            
        End If
        If Not IsNull(rsdir!CODPROVDIR) Then
            cboprovdir.MoveFirst
            Do While cboprovdir.Columns(0).Value <> rsdir!CODPROVDIR
                strCodAyuda = cboprovdir.Columns(1).Value
                'lngCodProv = cboprovdir.Columns(0).Value
                cboprovdir.MoveNext
                If strCodAyuda = cboprovdir.Columns(1).Value Then Exit Do
            Loop
            cboprovdir.Text = cboprovdir.Columns(1).Value
    '       cboprovdir.Columns(0).Value = rsdir!CODPROVDIR
    '       cboprovdir.Text = cboprovdir.Columns(1).Value
            Else
                cboprovdir.Columns(0).Value = 0
        End If
        
             
        
        If Not IsNull(rsdir!DESLOCDIR) Then
            txtlocalidaddir.Text = rsdir!DESLOCDIR
            Else
            txtlocalidaddir.Text = ""
        End If
         If Not IsNull(rsdir!CI10CALLE) Then
            txtcalle.Text = rsdir!CI10CALLE
            Else
            txtcalle.Text = ""
        End If
        If Not IsNull(rsdir!CI10PORTAL) Then
            txtportal.Text = rsdir!CI10PORTAL
            Else
            txtportal.Text = ""
        End If
        If Not IsNull(rsdir!CI10RESTODIREC) Then
            txtresdir.Text = rsdir!CI10RESTODIREC
            Else
            txtresdir.Text = ""
        End If
        If Not IsNull(rsdir!CI10TELEFONO) Then
            txttfnodir.Text = rsdir!CI10TELEFONO
            Else
            txttfnodir.Text = ""
        End If
        If Not IsNull(rsdir!CI10FAX) Then
            txtfax.Text = rsdir!CI10FAX
            Else
            txtfax.Text = ""
        End If
        If Not IsNull(rsdir!CI10FECINIVALID) Then
            dcbofecini.Text = Format(rsdir!CI10FECINIVALID, "dd/mm/yyyy")
            dcbofecini.Date = Format(rsdir!CI10FECINIVALID, "dd/mm/yyyy")
        Else
            dcbofecini.Text = ""
            dcbofecini.Date = ""
        End If
        If Not IsNull(rsdir!CI10FECFINVALID) Then
            dcbofecfin.Text = Format(rsdir!CI10FECFINVALID, "dd/mm/yyyy")
            dcbofecfin.Date = Format(rsdir!CI10FECFINVALID, "dd/mm/yyyy")
            Else
            dcbofecfin.Text = ""
            dcbofecfin.Date = ""
        End If
        If Not IsNull(rsdir!CI10INDDIRPRINC) Then
            chkdir.Value = Abs(rsdir!CI10INDDIRPRINC)
            If chkdir.Value = 1 Then
                frachkdir.Enabled = False
            Else
                frachkdir.Enabled = True
        End If
        If Not IsNull(rsdir!CI10OBSERVACIO) Then
            txtobservaciones.Text = rsdir!CI10OBSERVACIO
            Else
            txtobservaciones.Text = ""
        End If
         lngNumDir = rsdir!ci10numdirecci
         If Not IsNull(rsdir!CI07CODPOSTAL) Then
            txtcodpostal.Text = rsdir!CI07CODPOSTAL
            Else
            txtcodpostal.Text = ""
        End If
        
    End If
     
   End If
blnDatosDirCargados = True
cmdbuscarcodpostal.Enabled = True
cmdBuscarCalle.Enabled = True
vsDirec.Enabled = True
cmdNuevoDir.Enabled = True
cmdEliminarDir.Enabled = True
End Sub
Private Sub pdirecciones(Optional lngDirec As Long)

'Dim rs As rdoResultset
'Call seldir
    'Set rs = qry.OpenResultset(rdOpenKeyset)
    If Not rsdir.EOF Then
    'AQUI HAY QUE CARGAR EL SCROLL BAR
        rsdir.MoveLast
        rsdir.MoveFirst
        vsDirec.Max = rsdir.RowCount
        vsDirec.Min = 1
        If lngDirec = 0 Then
        'estar�a bien que en vez de posicionarse en la primera lo haga en la que se ha modificado
        vsDirec.Value = 1
        Else
        vsDirec.Value = lngDirec
        End If
    Else
       
        
    'Call pllenardatosdir
    'blnDatosDirCargados = True
    End If
End Sub

Private Sub pcargar_datos()
Dim sql As String
Dim lngAyuda As Long
lngCodPersonaFis_Resf = 0
lngCodPersonaFis_Resecon = 0
cmdbuscarresf.Enabled = True
cmdbuscarresecon.Enabled = True
'cmdnuevoresf.Enabled = True
'Dim rs As rdoResultset
    sql = "select CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,"
    sql = sql & " ci22numhistoria,CI2200.CI21CODPERSONA,"
    sql = sql & " CI22DNI,CI22FECNACIM,CI22FECFALLE,"
    sql = sql & " CI22NUMSEGSOC,CI22TFNOMOVIL,"
    sql = sql & " CI2200.CI16CODLOCALID CODLOCNAC,CI22DESLOCALID DESLOCNAC,CI2200.CI26CODPROVI CODPROVNAC,CI26DESPROVI DESPROVNAC,"
    sql = sql & " NAC.CI19DESPAIS PAISNAC,CI2200.CI19CODPAIS,CI2200.CI25CODPROFESI,CI25DESPROFESI,CI2200.CI28CODRELUDN,CI28DESRELUDN,"
    sql = sql & " CI2200.CI34CODTRATAMI,CI34DESTRATAMI,CI2200.CI14CODESTCIVI,CI14DESESTCIVI,CI2200.CI30CODSEXO,CI30DESSEXO,"
    sql = sql & " CI2200.CI35CODTIPVINC,CI35DESTIPVINC,"
    sql = sql & " CI22OBSERVAC,CI22EMPRESA,CI22PUESTO,CI22TFNOEMPRESA,CI22INDFALLE,"
    sql = sql & " CI21CODPERSONA_RFA,CI21CODPERSONA_REC,"
    sql = sql & " CI22INDFALLE,"
    'a partir de aqu� se cargan los campos del IBM
    sql = sql & " CI22IBMPROFESION,CI22IBMRELUDN,"
    sql = sql & " CI22IBMRFPRIAPEL||' '||CI22IBMRFSEGAPEL||' '||CI22IBMRFNOMBRE  RESIBM,"
    sql = sql & " CI22IBMRFDIRECC,"
    sql = sql & " CI22IBMRFPOBLAC,CI26CODPROVI_IRF,"
    sql = sql & " CI19CODPAIS_IRF,CI22IBMRFTELEFO"
    sql = sql & " From"
    sql = sql & " CI2200,CI2100,CI2600,"
    sql = sql & " CI1900 NAC,CI2500,CI2800,"
    sql = sql & " CI3400,CI1400,CI3000,"
    sql = sql & " CI3500"
    sql = sql & " Where"
    sql = sql & " CI2200.CI26CODPROVI=CI2600.CI26CODPROVI(+)"
    sql = sql & " AND CI2200.CI19CODPAIS=NAC.CI19CODPAIS(+)"
    sql = sql & " AND CI2200.CI28CODRELUDN=CI2800.CI28CODRELUDN(+)"
    sql = sql & " AND CI2200.CI34CODTRATAMI=CI3400.CI34CODTRATAMI(+)"
    sql = sql & " AND CI2200.CI14CODESTCIVI=CI1400.CI14CODESTCIVI(+)"
    sql = sql & " AND CI2200.CI25CODPROFESI=CI2500.CI25CODPROFESI(+)"
    sql = sql & " AND CI2200.CI30CODSEXO=CI3000.CI30CODSEXO"
    sql = sql & " AND CI2200.CI35CODTIPVINC=CI3500.CI35CODTIPVINC(+)"
   ' sql = sql & " AND CI2200.CI21CODPERSONA=CI1000.CI21CODPERSONA"
    'sql = sql & " AND CI10INDDIRPRINC=-1"
    'sql = sql & " AND DIR.CI19CODPAIS=CI1000.CI19CODPAIS"
    sql = sql & " AND CI2200.CI21CODPERSONA=CI2100.CI21CODPERSONA"
    
    If lngCodPersonaFis <> 0 And blnBuscar = True Then 'And lngnumhistoria_ant <> 0 Then 'cargar los datos en funcion del c�digo de persona
        blnBuscar = False
         sql = sql & " AND CI2200.CI21CODPERSONA=" & lngCodPersonaFis
    Else
        If txthistoria.Text <> "" Then
            sql = sql & " AND ci22numhistoria =" & lngNumHistoria 'Val(txthistoria.Text)
        Else
            Exit Sub 'no existe ni numero de historia ni codigo de persona
        End If
        
    End If

     Set rs = objApp.rdoConnect.OpenResultset(sql)
     If rs.EOF Then 'no existe nadie con ese n�mero de historia
        MsgBox "No existe ninguna persona con ese n�mero de Historia.Utilice el buscador", vbInformation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        txthistoria.SetFocus
        blnLHis = True
        'txthistoria.Text = ""
        Call plimpiar_controles(True)
        blnDatosCargados = False
        blnDatosDirCargados = False
        blnCambiosDir = False
        blnCambiosPer = False
        cmdGuardarPer.Enabled = False
        cmdGuardarDir.Enabled = False
        Exit Sub
    Else 'Existe el n�mero de historia
   
        lngCodPersonaFis = rs!CI21CODPERSONA
        Call pselcombos
        cbosexo.Columns(0).Value = rs!CI30CODSEXO
        cbosexo.Text = rs!CI30DESSEXO
        txtnombre.Text = rs!CI22NOMBRE
        If Not IsNull(rs!CI22NUMHISTORIA) Then
            txthistoria.Text = rs!CI22NUMHISTORIA
            lngNumHistoria = Val(txthistoria.Text)
            
        End If
       
        If Not IsNull(rs!CI22PRIAPEL) Then
            txtape1.Text = rs!CI22PRIAPEL
        End If
        If Not IsNull(rs!CI22SEGAPEL) Then
            txtape2.Text = rs!CI22SEGAPEL
        End If
        If Not IsNull(rs!CI34DESTRATAMI) Then
            cbotratam.Text = rs!CI34DESTRATAMI
            cbotratam.Columns(0).Value = rs!CI34CODTRATAMI
'            Else
'            cbotratam.Columns(0).Value = 0

        End If
        If Not IsNull(rs!CI14DESESTCIVI) Then
            cboestcivil.Text = rs!CI14DESESTCIVI
            cboestcivil.Columns(0).Value = rs!CI14CODESTCIVI
'        Else
'            cboestcivil.Columns(0).Value = 0
        End If
        If Not IsNull(rs!CI28DESRELUDN) Then
            cboreludn.Text = rs!CI28DESRELUDN
            cboreludn.Columns(0).Value = rs!CI28CODRELUDN
            Else 'Esto lo tendr� que pasa a otra pantalla
'            cboreludn.Columns(0).Value = 0 'Tengo que darle el valor cero porque los datos del ibm est�n sin codificar
            If Not IsNull(rs!CI22IBMRELUDN) Then
                blnIbm = True
            End If
        End If
        If Not IsNull(rs!CI22DNI) Then
            txtdni.Text = rs!CI22DNI
        End If
        If Not IsNull(rs!CI22NUMSEGSOC) Then
         txtsspro.Text = Left(rs!CI22NUMSEGSOC, 2)
         txtssnum.Text = Mid(rs!CI22NUMSEGSOC, 3, 8)
          If Len(rs!CI22NUMSEGSOC) > 10 Then
            txtssnum2.Text = Right(rs!CI22NUMSEGSOC, 2)
          Else
            txtssnum2.Text = ""
          End If
        End If
        If Not IsNull(rs!CI22TFNOMOVIL) Then
            txtmovil.Text = rs!CI22TFNOMOVIL
        End If
        If Not IsNull(rs!CI22OBSERVAC) Then
            txtobser.Text = rs!CI22OBSERVAC
        End If
        If Not IsNull(rs!CI25DESPROFESI) Then
            cboprofesion.Text = rs!CI25DESPROFESI
            cboprofesion.Columns(0).Value = rs!CI25CODPROFESI
            
             Else 'pasarlo a otra pantalla
'              cboprofesion.Columns(0).Value = 0 'lo mismo que con el combo de reludn
            If Not IsNull(rs!CI22IBMPROFESION) Then
'                cboprofesion.Columns(0).Value = 0
               blnIbm = True
            End If
        End If
        If Not IsNull(rs!CI22EMPRESA) Then
            txtempresa.Text = rs!CI22EMPRESA
        End If
        If Not IsNull(rs!CI22PUESTO) Then
            txtpuesto.Text = rs!CI22PUESTO
        End If
        If Not IsNull(rs!CI22TFNOEMPRESA) Then
            txttfnotrab.Text = rs!CI22TFNOEMPRESA
        End If
        
        If Not IsNull(rs!PAISNAC) Then
            cbopaisnac.MoveFirst
            'cbopaisnac.Columns(0).Value = rs!CI19CODPAIS
            Do While cbopaisnac.Columns(0).Value <> rs!CI19CODPAIS
                lngAyuda = Val(cbopaisnac.Columns(0).Value)
                cbopaisnac.MoveNext
                If lngAyuda = Val(cbopaisnac.Columns(0).Value) Then
                    Exit Do
                End If
            Loop
            cbopaisnac.Columns(1).Value = rs!PAISNAC
            cbopaisnac.Text = rs!PAISNAC
'            Else
'            cbopaisnac.Columns(0).Value = 0
        End If
         If cbopaisnac.Columns(0).Value = "45" Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovnac)
        End If
        If Not IsNull(rs!DESPROVNAC) Then
            cboprovnac.Text = rs!DESPROVNAC
            cboprovnac.Columns(0).Value = rs!CODPROVNAC
            
'            Else
'            cboprovnac.Columns(0).Value = 0
        End If
        If Not IsNull(rs!CI22INDFALLE) Then
            If rs!CI22INDFALLE = -1 Then
                chkfall.Value = 1
            End If
        End If
        If Not IsNull(rs!CI22FECFALLE) Then
            dcbofecfall.Text = Format(rs!CI22FECFALLE, "dd/mm/yyyy")
            dcbofecfall.Date = Format(rs!CI22FECFALLE, "dd/mm/yyyy")
            chkfall.Value = 1
            Else
            dcbofecfall.Text = ""
            dcbofecfall.Date = ""
        End If
        If Not IsNull(rs!CI22FECNACIM) Then
            dcbofecnac.Text = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
            dcbofecnac.Date = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
            Else
            dcbofecnac.Text = ""
            dcbofecnac.Date = ""
            
        End If
       
        If Not IsNull(rs!DESLOCNAC) Then
            txtlocalidadnac.Text = rs!DESLOCNAC
        End If
         If Not IsNull(rs!CI21CODPERSONA_REC) Then
            lngCodPersonaFis_Resecon = rs!CI21CODPERSONA_REC
            
        End If
         If Not IsNull(rs!CI21CODPERSONA_RFA) And rs!CI21CODPERSONA_RFA <> rs!CI21CODPERSONA Then
            cbovinculacion.Enabled = True
            cbovinculacion.BackColor = &HFFFFFF
           If Not IsNull(rs!CI35CODTIPVINC) Then
          
                cbovinculacion.Columns(0).Value = rs!CI35CODTIPVINC
                cbovinculacion.Text = rs!CI35DESTIPVINC
            End If
            
            lngCodPersonaFis_Resf = rs!CI21CODPERSONA_RFA
           If rs!CI21CODPERSONA <> rs!CI21CODPERSONA_RFA Then 'And Not IsNull(rs!CI21CODPERSONA_RFA) Then 'Existe un responsable familiar distinto que el paciente
                sql = "SELECT CI22PRIAPEL||' '||nvl(CI22SEGAPEL,'')||', '||CI22NOMBRE RES_FAMILIAR"
                sql = sql & " FROM CI2200 "
                sql = sql & " WHERE CI21CODPERSONA=" & rs!CI21CODPERSONA_RFA
                Set rs = objApp.rdoConnect.OpenResultset(sql)
                txtrespf.Text = rs!RES_FAMILIAR
            End If
        Else 'pasarlo a otra pantalla
         If Not IsNull(rs!RESIBM) And Trim(rs!RESIBM) <> "" Then
'
'                txtrespf.Text = Trim(rs!RESIBM)
'                cmdeliminarresponsablef.Enabled = False
'                cmdbuscarresf.Enabled = False
'                cmdnuevoresf.Enabled = False
                blnIbm = True
                lngCodPersonaFis_Resf = 0
            End If
        End If
       
      If lngCodPersonaFis <> lngCodPersonaFis_Resecon And lngCodPersonaFis_Resecon <> 0 Then 'Existe un responsable econ�mico distinto que el paciente
      sql = "SELECT ADFN01(?) RESECON FROM DUAL"
           Set qry = objApp.rdoConnect.CreateQuery("", sql)
           qry(0) = lngCodPersonaFis_Resecon
           Set rs = qry.OpenResultset()
           txtresecon.Text = rs!RESECON
'        sql = "SELECT CI22PRIAPEL||' '||NVL(CI22SEGAPEL,'')||', '||CI22NOMBRE RES_ECONOMICO"
'        sql = sql & " FROM CI2200 "
'        sql = sql & " WHERE CI21CODPERSONA=" & lngCodPersonaFis_Resecon
'        Set rs = objApp.rdoConnect.OpenResultset(sql)
'        txtresecon.Text = rs!RES_ECONOMICO
    End If
       If lngCodPersonaFis <> 0 Then
       
        Call seldir(lngCodPersonaFis)
        Call pdirecciones
        blnDatosDirCargados = False
        Call pLlenarDatosDir
        End If
    End If
    lngnumhistoria_ant = lngNumHistoria
    'cmdguardardir.Enabled = True
    blnDatosCargados = True
'    cmdbuscarcodpostal.Enabled = False
'    cmdBuscarCalle.Enabled = False
    Call pMensajes
     cmdAcunsa.Enabled = True
    cmdavisos.Enabled = True
    cmdetiquetas.Enabled = True
    cmdCamas.Enabled = True
    cmdUniversidad.Enabled = True
    cmdSolNH.Enabled = True
    cmdImPrograma.Enabled = True
    If objPipe.PipeExist("AG0210_ACCESO") Then
      If objPipe.PipeGet("AG0210_ACCESO") = "AG0210" Then
        Cmdcitaconsultas.Enabled = False
      Else
        Cmdcitaconsultas.Enabled = True
      End If
    Else
      Cmdcitaconsultas.Enabled = True
    End If
    cmdEliminarPer.Enabled = True
    CmdIngresos.Enabled = True
    cmdVer.Enabled = True
    cmddatoshistoricos.Enabled = True
         
   ' blnDatosDirCargados = True
    
End Sub
Private Sub plimpiar_controles(Optional blnlimhis As Boolean)
Dim con As Control
lngCodPersonaFis = 0
lngCodPersonaFis_Resf = 0
lngCodPersonaFis_Resecon = 0
blnCambiosDir = False
blnDatosDirCargados = False
blnDatosCargados = False
blnCambiosPer = False
cmdGuardarPer.Enabled = False
cmdGuardarDir.Enabled = False

    For Each con In Me
    If TypeOf con Is CheckBox Then con.Value = 0
    If TypeOf con Is TextBox Then
       
        If con.Name <> "txthistoria" Then
            con.Text = ""
          Else
            If blnlimhis Then con.Text = ""
        End If
       End If
    'End If
    If TypeOf con Is SSDateCombo Then con.Text = ""
    If TypeOf con Is SSDBCombo Then
        con.RemoveAll
        con.Text = ""
    End If

Next
lngnumhistoria_ant = 0
frachkdir.Enabled = True
chkfall.Value = False
cmdeliminarresponsablef.Enabled = False
cmdeliminarresecon.Enabled = False
vsDirec.Enabled = False
Set rsdir = Nothing
If Not txthistoria.Enabled Then
    txthistoria.Enabled = True
    txthistoria.BackColor = &HFFFFFF
End If
cbovinculacion.Enabled = False
cbovinculacion.BackColor = &HC0C0C0
cmddatoshistoricos.Enabled = False

End Sub
Private Sub pMensajes()
Dim sql As String
Dim rsMens As rdoResultset
Dim qry As rdoQuery
Dim imgX As ListImage

   sql = "SELECT count(*) from  CI0400 WHERE CI21CodPersona = ?" _
   & " and (ci04FecCadMens >= trunc(sysdate+1)" _
   & " Or ci04FecCadMens Is Null)"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
     If lngCodPersonaFis <> 0 Then
        qry(0) = lngCodPersonaFis
     Else
        qry(0) = 0
     End If
     Set rsMens = qry.OpenResultset()
     If rsMens(0) > 0 Then
       Set imgX = objApp.imlImageList.ListImages("i44")
       cmdavisos.Picture = imgX.Picture
     Else
       Set imgX = objApp.imlImageList.ListImages("i40")
       cmdavisos.Picture = imgX.Picture
     End If
End Sub
Private Sub pcargarcombos(sql As String, con As SSDBCombo)
    
    Dim rs As rdoResultset
   Set rs = objApp.rdoConnect.OpenResultset(sql)
   INTCOUNT = 0
   Do While Not rs.EOF
    con.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
   Loop
End Sub

Private Sub psalir()
Dim msg As String
    msg = "Existen datos obligatorios sin rellenar."
    MsgBox msg, vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
End Sub

Private Sub cboestcivil_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub



Private Sub cboestcivil_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cboestcivil_KeyDown(KeyCode As Integer, Shift As Integer)
 Call pSuprimir(cboestcivil, KeyCode)
End Sub

Private Sub cbopaisdir_Change()
txtlocalidaddir.Text = ""
txtcodpostal.Text = ""
txtcalle.Text = ""
txtportal.Text = ""
cboprovdir.RemoveAll
If cbopaisdir.Columns(0).Value = 45 Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovdir)
End If
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If
End Sub

Private Sub cbopaisdir_Click()
Dim sql As String
txtlocalidaddir.Text = ""
txtcodpostal.Text = ""
txtcalle.Text = ""
txtportal.Text = ""
cboprovdir.RemoveAll
If cbopaisdir.Columns(0).Value = 45 Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovdir)
End If
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If

'If cbopaisnac.Columns(0).Value = 34 Then
End Sub

Private Sub cbopaisnac_Change()
cboprovnac.RemoveAll
If cbopaisnac.Columns(0).Value = "45" Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovnac)
End If 'If cbopaisnac.Columns(0).Value = 34 Then

If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cbopaisnac_Click()
Dim sql As String
cboprovnac.RemoveAll

If cbopaisnac.Columns(0).Value = "45" Then
         sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
         Call pcargarcombos(sql, cboprovnac)
End If 'If cbopaisnac.Columns(0).Value = 34 Then
    
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cbopaisnac_KeyPress(KeyAscii As Integer)
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cboprofesion_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cboprofesion_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cboprofesion_KeyDown(KeyCode As Integer, Shift As Integer)
 Call pSuprimir(cboprofesion, KeyCode)

End Sub

Private Sub cboprovdir_Change()
'txtlocalidaddir.Text = ""
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True

End If
End Sub


Private Sub cboprovdir_Click()
'txtlocalidaddir.Text = ""
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True

End If

End Sub

Private Sub cboprovdir_KeyDown(KeyCode As Integer, Shift As Integer)
 Call pSuprimir(cboprovdir, KeyCode)

End Sub

Private Sub cboprovdir_KeyPress(KeyAscii As Integer)
'txtlocalidaddir.Text = ""
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True

End If
End Sub



Private Sub cboprovnac_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub cboprovnac_Click()
 If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
   
End Sub

Private Sub cboprovnac_KeyDown(KeyCode As Integer, Shift As Integer)
     Call pSuprimir(cboprovnac, KeyCode)

End Sub

Private Sub cboprovnac_KeyPress(KeyAscii As Integer)
'txtlocalidadnac.Text = ""
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub cboreludn_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cboreludn_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cboreludn_KeyDown(KeyCode As Integer, Shift As Integer)
 Call pSuprimir(cboreludn, KeyCode)

End Sub

Private Sub cbosexo_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub



Private Sub cbosexo_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cbotratam_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub




Private Sub cbotratam_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cbovinculacion_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub cbovinculacion_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cbovinculacion_KeyDown(KeyCode As Integer, Shift As Integer)
 Call pSuprimir(cbovinculacion, KeyCode)

End Sub

Private Sub chkdir_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If


End Sub


Private Sub chkfall_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub cmdAcunsa_Click()
Dim rdoAcunsa As rdoResultset
Dim strSql As String
Dim intI As Integer
Dim intJ As Integer
Dim blnAcunsa As Boolean
Dim blnVerAsegu As Boolean
Dim blnNoEncontrado As Boolean
Dim blnBaja As Boolean


'Call cmdguardarper_Click
If Not Hay_Persona Then Exit Sub
'Buscamos por historia si hay
Me.MousePointer = vbHourglass
If txthistoria.Text <> "" Then 'Not txtText1(8).Text = "" Then
    strSql = "SELECT * FROM CI0300 WHERE CI22NUMHISTORIA=" & txthistoria.Text 'txtText1(8)
    strSql = strSql & " AND (CI03FECFINPOLI IS NULL OR CI03FECFINPOLI >= SYSDATE)"
    Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    If Not rdoAcunsa.RowCount = 0 Then
    blnAcunsa = True
    Else
      strSql = "SELECT * FROM CI0300 WHERE CI22NUMHISTORIA=" & txthistoria.Text 'txtText1(8)
        Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
        If Not rdoAcunsa.EOF Then blnBaja = True
    End If
End If

If blnAcunsa Then
'encontramos el numero de historia 1 vez
    If objGen.GetRowCount(rdoAcunsa) = 1 Then
        If rdoAcunsa("CI03CORRIPAGO") = 1 Then
            intI = MsgBox("El paciente no esta al corriente de pago" & Chr$(13) & "�Desea ver su ficha?" _
            , vbQuestion + vbYesNo)
            If intI = vbYes Then
               ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                    rdoAcunsa.MoveNext
               Next intJ
               blnVerAsegu = True
           End If
        ElseIf rdoAcunsa("CI03FECFINPOLI") < strFecha_Sistema Then
        intI = MsgBox("Se ha encontrado una poliza finalizada" & Chr$(13) & "�Desea verla en la pantalla de mantenimiento?" _
            , vbQuestion + vbYesNo)
            If intI = vbYes Then
               ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                    rdoAcunsa.MoveNext
               Next intJ
               blnVerAsegu = True
            End If
        ElseIf IsNull(rdoAcunsa("CI03EXCLUSION")) And IsNull(rdoAcunsa("CI03OBSERVACIO")) Then
         intI = MsgBox("El paciente pertenece a Acunsa, sin observaciones ni restrinciones." & Chr$(13) & _
                "�Desea verlo?", vbQuestion + vbYesNo)
             
             If intI = vbYes Then
              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                    rdoAcunsa.MoveNext
               Next intJ
               blnVerAsegu = True
            End If
        Else
            intI = MsgBox("El paciente pertenece a Acunsa" & Chr$(13) & "�Desea ver sus restrinciones u observaciones?" _
            , vbQuestion + vbYesNo)
            If intI = vbYes Then
              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                    rdoAcunsa.MoveNext
               Next intJ
               blnVerAsegu = True
            End If
        End If
'Encontramos varias polizas para ese numero de historia
    Else
    
      intI = MsgBox("Se han encontrado " & objGen.GetRowCount(rdoAcunsa) & " polizas para ese n�mero de historia" & Chr$(13) & "�Desea verlas?" _
            , vbQuestion + vbYesNo)
            If intI = vbYes Then
              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                    rdoAcunsa.MoveNext
               Next intJ
               blnVerAsegu = True
             End If
     
    End If
    If intI = vbYes Then
    Call objPipe.PipeSet("PrimerNumPoliza", lngNumPoliza(0))
    End If
Else
If blnBaja Then
       intI = MsgBox("Este paciente est� dado de baja en Acunsa" & Chr$(13) & _
                            "�Desea verlo en la pantalla de mantenimiento?", vbQuestion + vbYesNo)
       If intI = vbYes Then
              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                    rdoAcunsa.MoveNext
               Next intJ
               blnVerAsegu = True
               Call objPipe.PipeSet("PrimerNumPoliza", lngNumPoliza(0))
      End If
Else
'*******************************************************************
'Caso de que no este el numero de historia miramos si esta el nombre y los apellidos y el DNI.
'*******************************************************************
    strSql = "SELECT * FROM CI0300 WHERE CI03NOMBRASEGU='" & txtnombre.Text & "'"
    strSql = strSql & " AND CI03APELLIASEGU='" & txtape1.Text & " " & txtape2.Text & "'"
'    If Not IsNull(txtText1(4)) Then
'      strSQL = strSQL & "AND CI03DNI =" & txtText1(4)
'    End If
    Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    If objGen.GetRowCount(rdoAcunsa) > 0 Then
    'Se han encontrado registros con datos similares a los del paciente pero sin numero de historia
        intI = MsgBox("No hay ningun registro asociado a este n� de Historia " & Chr$(13) & "pero se han encontrado " & objGen.GetRowCount(rdoAcunsa) & " registros similares" _
        & Chr$(13) & "�Desea buscarlos y asociar a uno el n� de Historia?", vbQuestion + vbYesNo)
        If intI = vbYes Then
           ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
           For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
                lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
                strApelAsegu = rdoAcunsa("CI03APELLIASEGU")
                strNombAsegu = rdoAcunsa("CI03NOMBRASEGU")
                rdoAcunsa.MoveNext
           Next intJ
           blnAsocHistoria = True
           lngNumHistoria = Val(txthistoria.Text)
           blnVerAsegu = True
        End If
    Else
        intI = MsgBox("No se ha encontrado en ACUNSA" _
        & Chr$(13) & "�Desea ver la pantalla de mantenimiento?", vbQuestion + vbYesNo)
        If intI = vbYes Then
           blnAsocHistoria = True
           lngNumHistoria = Val(txthistoria.Text) 'txtText1(8).Text)
           blnNoEncontrado = True
           Call objPipe.PipeSet("numeroHistoria", txthistoria.Text) 'txtText1(8).Text)
           Call objSecurity.LaunchProcess("CI1010")
           If objPipe.PipeExist("numeroHistoria") Then objPipe.PipeRemove ("numeroHistoria")
        End If
    End If
    
   
'    If intI = vbYes And blnNoEncontrado = False Then
'        Call objPipe.PipeSet("Nombreac", txtText1(1).Text)
'        Call objPipe.PipeSet("Primerapac", txtText1(2).Text)
'        Call objPipe.PipeSet("Segapac", txtText1(3).Text)
'        Call objPipe.PipeSet("PrimerNumPoliza", lngNumPoliza(0))
'    End If
End If
End If
rdoAcunsa.Close
'Si tenemos que ir a la otra pantalla
If blnVerAsegu Then
    blnPerFisica = True
    Call objPipe.PipeSet("numeroHistoria", txthistoria.Text) 'txtText1(8).Text)
    Call objSecurity.LaunchProcess("CI1010")
    If objPipe.PipeExist("numeroHistoria") Then objPipe.PipeRemove ("numeroHistoria")
''    If objPipe.PipeExist("nombreac") Then objPipe.PipeRemove ("Nombreac")
''    If objPipe.PipeExist("Primerapac") Then objPipe.PipeRemove ("Primerapac")
''    If objPipe.PipeExist("Segapac") Then objPipe.PipeRemove ("Segapac")
''    If objPipe.PipeExist("Historia") Then objPipe.PipeRemove ("Historia")
    If objPipe.PipeExist("PrimerNumPoliza") Then objPipe.PipeRemove ("PrimerNumPoliza")


End If
Me.MousePointer = vbDefault
lngNumHistoria = 0
blnPerFisica = False
blnVerAsegu = False
blnAsocHistoria = False
'lngNumPoliza = ""


End Sub


Private Sub cmdAvisos_Click()
Dim vntData(1)

vntData(1) = lngCodPersonaFis
Call objSecurity.LaunchProcess("PR0580", vntData)
Call pMensajes


End Sub

Private Sub cmdBuscarCalle_Click()
Dim vntData(0 To 2)
Dim sql As String
Dim rs As rdoResultset
Dim strCodProv As String
Dim intTipPobla As Integer
If cboprovdir.Text = "" Then
    MsgBox "Debe seleccionar alguna provincia", vbExclamation, Me.Caption
    Exit Sub
End If
If txtlocalidaddir.Text = "" Then
    MsgBox "Debe indicar alguna localidad", vbExclamation, Me.Caption
    Exit Sub
End If

strCodProv = cboprovdir.Columns(1).Value
    vntData(0) = strCodProv
    vntData(1) = UCase(Trim(txtlocalidaddir.Text))
    vntData(2) = UCase(Trim(txtcalle.Text))
    Call objPipe.PipeSet("CODPROVINCIA", vntData(0))
    Call objPipe.PipeSet("LOCALIDAD", vntData(1))
     Call objPipe.PipeSet("CALLE", vntData(2))
    frmcodigos_postales.Show vbModal
    Set frmcodigos_postales = Nothing
    If objPipe.PipeExist("CODPOSTAL") Then
        txtcodpostal.Text = objPipe.PipeGet("CODPOSTAL")
        Call objPipe.PipeRemove("CODPOSTAL")
    End If
    If objPipe.PipeExist("LOCALIDAD") Then
       txtlocalidaddir.Text = objPipe.PipeGet("LOCALIDAD")
        Call objPipe.PipeRemove("LOCALIDAD")
    End If
      If objPipe.PipeExist("CALLE") Then
        txtcalle.Text = objPipe.PipeGet("CALLE")
        Call objPipe.PipeRemove("CALLE")
    End If
    


End Sub

Private Sub cmdbuscarcodpostal_Click()
    Dim vntData(0 To 2)
    Dim sql As String
Dim rs As rdoResultset
Dim strCodProv As String
Dim strCodAyuda As Long
Dim intTipPobla As Integer
strCodProv = cboprovdir.Text
If cboprovdir.Text = "" Then
    MsgBox "Debe seleccionar alguna provincia", vbExclamation, Me.Caption
    Exit Sub
End If


'
    vntData(0) = strCodProv
    vntData(1) = UCase(Trim(txtlocalidaddir.Text))
    Call objPipe.PipeSet("CODPROVINCIA", vntData(0))
    
     
    Call objPipe.PipeSet("LOCALIDAD", vntData(1))
    frmcodigos_postales.Show vbModal
    If objPipe.PipeExist("PROVINCIA") Then
        cboprovdir.MoveFirst
        strCodProv = objPipe.PipeGet("PROVINCIA")
        Do While cboprovdir.Columns(0).Value <> strCodProv
            strCodAyuda = cboprovdir.Columns(0).Value
            cboprovdir.MoveNext
            If strCodAyuda = cboprovdir.Columns(0).Value Then Exit Do
        Loop
        cboprovdir.Text = cboprovdir.Columns(1).Value
        Call objPipe.PipeRemove("PROVINCIA")
    End If
    Set frmcodigos_postales = Nothing
    If objPipe.PipeExist("CODPOSTAL") Then
        txtcodpostal.Text = objPipe.PipeGet("CODPOSTAL")
        Call objPipe.PipeRemove("CODPOSTAL")
    End If
    
    If objPipe.PipeExist("LOCALIDAD") Then
       txtlocalidaddir.Text = objPipe.PipeGet("LOCALIDAD")
        Call objPipe.PipeRemove("LOCALIDAD")
    End If
  
    
    
End Sub

Private Sub cmdbuscarper_Click()
Dim blnAbort As Boolean
Dim IntRes As Integer

If blnDatosCargados Then
    If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        IntRes = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If IntRes = vbYes Then
            Call pGuardarPer(blnAbort)
            If blnAbort Then
                blnAbort = False
                Exit Sub
            End If
        End If 'de si quieren guardar los cambios
    End If
    blnCambiosDir = False
    blnDatosDirCargados = False
    blnDatosCargados = False
    blnCambiosPer = False
    cmdGuardarPer.Enabled = False
    cmdGuardarDir.Enabled = False
End If
lngnumhistoria_ant = 0
Call plimpiar_controles
blnBuscar = True
frmBuscaPersonas.Show vbModal
Set frmBuscaPersonas = Nothing
 If objPipe.PipeExist("AD_CodPersona") Then
    lngCodPersonaFis = objPipe.PipeGet("AD_CodPersona")
    objPipe.PipeRemove ("AD_CodPersona")
End If
If objPipe.PipeExist("AD_NumHistoria") Then
    txthistoria.Text = objPipe.PipeGet("AD_NumHistoria")
   lngNumHistoria = Val(txthistoria.Text)
    objPipe.PipeRemove ("AD_NumHistoria")
End If
If blnInicio And (lngNumHistoria <> 0 Or lngCodPersonaFis) <> 0 Then
    Call pHabilitar_Controles
    blnInicio = False
End If
blnIbm = False
blnNuevaPer = False
blnnuevadir = False
blnmodonuevo = False
blnnuevo = False
Call pcargar_datos
End Sub

Private Sub cmdbuscarresecon_Click()
Dim vntData(0 To 2)
If txtresecon.Text = "" Then 'no hay responsable econ�mico
        vntData(0) = 1
        vntData(1) = lngCodPersonaFis
        Call objPipe.PipeSet("Tip_Res", vntData(0))
        Call objPipe.PipeSet("PACIENTE", vntData(1))
        If blnLlamada Then
            vntData(2) = True
            Call objPipe.PipeSet("LLAMADA", vntData(2))
        End If
        frmBuscaPersonas.Show vbModal
        Set frmBuscaPersonas = Nothing
        If objPipe.PipeExist("AD_CodPersona") Then
           lngCodPersonaFis_Resecon = objPipe.PipeGet("AD_CodPersona")
           objPipe.PipeRemove ("AD_CodPersona")
           sql = "SELECT ADFN01(?) RESECON FROM DUAL"
           Set qry = objApp.rdoConnect.CreateQuery("", sql)
           qry(0) = lngCodPersonaFis_Resecon
           Set rs = qry.OpenResultset()
           txtresecon.Text = rs!RESECON
        End If
Else 'ya tiene responsable econ�mico
    sql = "SELECT CI33CODTIPPERS FROM CI2100 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodPersonaFis_Resecon
    Set rs = qry.OpenResultset(rdOpenForwardOnly)
    If rs(0) = constPER_FISICA Then 'El responsable es una persona f�sica
        vntData(0) = lngCodPersonaFis_Resecon
        vntData(1) = lngCodPersonaFis
        Call objPipe.PipeSet("RESP", vntData(0))
        Call objPipe.PipeSet("PACIENTE", vntData(1))
        frmresponsablesnw.Show vbModal
        Set frmresponsablesnw = Nothing
    Else 'El responsable es una persona jur�dica
        vntData(0) = lngCodPersonaFis_Resecon
        vntData(1) = lngCodPersonaFis
        Call objPipe.PipeSet("RESP", vntData(0))
        Call objPipe.PipeSet("PACIENTE", vntData(1))
        frmPerJurNW.Show vbModal
        Set frmPerJurNW = Nothing
        'Call objSecurity.LaunchProcess("CI1018", vntData)
    End If
    If objPipe.PipeExist("AD_CodPersona") Then
        lngCodPersonaFis_Resecon = objPipe.PipeGet("AD_CodPersona")
        objPipe.PipeRemove ("AD_CodPersona")
        sql = "SELECT CI22PRIAPEL||' '||nvl(CI22SEGAPEL,'')||', '||CI22NOMBRE RESECON"
        sql = sql & " FROM CI2200 WHERE CI21CODPERSONA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodPersonaFis_Resecon
        Set rs = qry.OpenResultset()
        txtresecon.Text = rs!RESECON
    End If
End If
End Sub

Private Sub cmdbuscarresf_Click()
Dim vntData(0 To 2)
If txtrespf.Text = "" Then
    vntData(0) = 2
    vntData(1) = lngCodPersonaFis
    Call objPipe.PipeSet("Tip_Res", vntData(0))
    Call objPipe.PipeSet("PACIENTE", vntData(1))
    
    If blnLlamada Then
        vntData(2) = True
        Call objPipe.PipeSet("LLAMADA", vntData(2))
    End If
    frmBuscaPersonas.Show vbModal
    Set frmBuscaPersonas = Nothing
    
     If objPipe.PipeExist("AD_CodPersona") Then
        lngCodPersonaFis_Resf = objPipe.PipeGet("AD_CodPersona")
        objPipe.PipeRemove ("AD_CodPersona")
        sql = "SELECT CI22PRIAPEL||' '||nvl(CI22SEGAPEL,'')||', '||CI22NOMBRE RESF"
        sql = sql & " FROM CI2200 WHERE CI21CODPERSONA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodPersonaFis_Resf
        Set rs = qry.OpenResultset()
        txtrespf.Text = rs!RESF
    End If
Else
    vntData(0) = 2
    vntData(1) = lngCodPersonaFis
    vntData(2) = lngCodPersonaFis_Resf
    Call objPipe.PipeSet("Tip_Res", vntData(0))
    Call objPipe.PipeSet("PACIENTE", vntData(1))
    Call objPipe.PipeSet("RESP", vntData(2))
    frmresponsablesnw.Show vbModal
    Set frmresponsablesnw = Nothing
    
    If objPipe.PipeExist("AD_CodPersona") Then
        lngCodPersonaFis_Resf = objPipe.PipeGet("AD_CodPersona")
        objPipe.PipeRemove ("AD_CodPersona")
        sql = "SELECT CI22PRIAPEL||' '||nvl(CI22SEGAPEL,'')||', '||CI22NOMBRE RESF"
        sql = sql & " FROM CI2200 WHERE CI21CODPERSONA=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodPersonaFis_Resf
        Set rs = qry.OpenResultset()
        If Not rs.EOF Then
        txtrespf.Text = rs!RESF
        End If
        'ESTO NO LO TENGO MUY CLARO, HAY QUE VER QUE PASA CON LOS DEL IBM
    End If
End If

End Sub

Private Sub cmdCamas_Click()
Dim vntData(1)

If Not Hay_Persona Then Exit Sub
Me.MousePointer = vbHourglass

'Historico de Hospitalizaciones de un paciente
cmdCamas.Enabled = False
vntData(1) = lngCodPersonaFis
lngCodPers = lngCodPersonaFis
blnHayCodPer = True
Call objPipe.PipeSet("CODPER", vntData(1))
Call objSecurity.LaunchProcess("CI0182", vntData(1))
blnHayCodPer = False
cmdCamas.Enabled = True
Me.MousePointer = vbDefault

End Sub

Private Sub Cmdcitaconsultas_Click()
    If Not Hay_Persona Then Exit Sub

    If IsNull(rsdir!ci10numdirecci) Then
        MsgBox "No es posible citar sin una Direcci�n.", vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
        Screen.MousePointer = vbDefault
        Exit Sub
    End If
    Cmdcitaconsultas.Enabled = False
    Call objSecurity.LaunchProcess("AG0213", lngCodPersonaFis)
    Cmdcitaconsultas.Enabled = True
End Sub

Private Sub cmddatoshistoricos_Click()
Dim sql As String
Dim rs1 As rdoResultset
Dim vntData
If blnIbm Then
        vntData = lngCodPersonaFis
        Call objPipe.PipeSet("PAC", vntData)
        frmdatoshistoricos.Show vbModal
        Set frmdatoshistoricos = Nothing
    Else
        MsgBox "El paciente no tiene Datos hist�ricos", vbInformation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
        Exit Sub
End If
End Sub

Private Sub cmdEliminarDir_Click()
Dim IntRes As Integer
If blnDatosDirCargados = False Then
    MsgBox "No existe Ninguna Persona", vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
Else
If chkdir.Value = 1 Then
    MsgBox "La direcci�n principal no puede ser eliminada", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
IntRes = MsgBox("�Est� seguro de eliminar los datos?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
If IntRes = vbYes Then

    sql = "DELETE FROM CI1000 WHERE CI21CODPERSONA=" & lngCodPersonaFis & " AND CI10NUMDIRECCI="
    If lngNumDir <> 0 Then
    sql = sql & lngNumDir
    Else
    sql = sql & rsdir!ci10numdirecci
    End If
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    Call seldir(lngCodPersonaFis)
    Call pdirecciones
    Call pLlenarDatosDir
End If
End If
End Sub
Function fSigCodpersona() As Long 'Obtiene el siguiente codigo de persona hasta que se cree un sequence en la B.D.

Dim sql As String
Dim rsmax As rdoResultset

sql = "select max(ci21codpersona) from ci2100 "
Set rsmax = objApp.rdoConnect.OpenResultset(sql)
fSigCodpersona = rsmax(0) + 1
rsmax.Close


End Function

Private Sub cmdeliminarper_Click()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
sql = "SELECT COUNT(*) FROM PR0400 WHERE CI21CODPERSONA=?"
sql = sql & " AND PR37CODESTADO<>6"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If rs(0) > 0 Then
    MsgBox "La persona no puede ser eliminada porque tiene actuaciones pendientes", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
sql = "SELECT COUNT(*) FROM FR6600 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If rs(0) > 0 Then
    MsgBox "La persona no puede ser eliminada porque tiene peticiones pendientes en farmacia", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
sql = "SELECT COUNT(*) FROM AD0100 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If rs(0) > 0 Then
    MsgBox "La persona no puede ser eliminada porque tiene Asistencias", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If

sql = "SELECT COUNT(*) FROM FR6500 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If rs(0) > 0 Then
    MsgBox "La persona no puede ser eliminada porque tiene Perfiles FTP en farmacia", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
sql = "SELECT /*+ ORDERED INDEX(CI2200 CI2205) */ CI22NUMHISTORIA,CI22PRIAPEL,CI22SEGAPEL,CI22NOMBRE  FROM CI2200 WHERE CI21CODPERSONA_REC=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If Not rs.EOF Then
    MsgBox "La persona no puede ser eliminada porque es responsable econ�mico de:" & Chr(13) & "Historia: " & rs(0) & Chr(13) & "Paciente: " & rs(1) & " " & rs(2) & " " & rs(3), vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
sql = "SELECT /*+ ORDERED INDEX(AD1100 AD1103) */ AD01CODASISTENCI,AD07CODPROCESO FROM AD1100 WHERE CI21CODPERSONA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If Not rs.EOF Then
    MsgBox "La persona no puede ser eliminada porque es responsable econ�mico de Alg�n proceso/asistencia", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If

sql = "SELECT /*+ ORDERED INDEX(CI2200 CI2207) */ CI22NUMHISTORIA,CI22PRIAPEL,CI22SEGAPEL,CI22NOMBRE  FROM CI2200 WHERE CI21CODPERSONA_RFA=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset
If Not rs.EOF Then
    MsgBox "La persona no puede ser eliminada porque es responsable familiar de:" & Chr(13) & "Historia: " & rs(0) & Chr(13) & "Paciente: " & rs(1) & " " & rs(2) & " " & rs(3), vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
'Aqu� empieza la eliminacion
Dim intR As Integer
Dim qry1 As rdoQuery
Dim SQL1 As String
On Error GoTo canceltrans


sql = "SELECT CI22PRIAPEL||' '||nvl(CI22SEGAPEL,'')||', '||CI22NOMBRE FROM CI2200 WHERE CI21CODPERSONA= ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodPersonaFis
Set rs = qry.OpenResultset()
intR = MsgBox("Borrando : " & rs(0) & " Desea Continuar?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
rs.Close
qry.Close
If intR = vbNo Then Exit Sub
Screen.MousePointer = vbHourglass
objApp.rdoConnect.BeginTrans
sql = "DELETE FROM CI2700 WHERE"
sql = sql & " CI31NUMSOLICIT IN (SELECT CI31NUMSOLICIT FROM CI0100 WHERE"
sql = sql & " PR04NUMACTPLAN IN ( SELECT PR04NUMACTPLAN"
sql = sql & " From PR0400"
sql = sql & " WHERE CI21CODPERSONA =" & lngCodPersonaFis & "));"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM CI1500 WHERE"
sql = sql & " CI31NUMSOLICIT IN (SELECT CI31NUMSOLICIT FROM CI0100 WHERE"
sql = sql & " PR04NUMACTPLAN IN ( SELECT PR04NUMACTPLAN"
sql = sql & " From PR0400"
sql = sql & " WHERE CI21CODPERSONA ="
sql = sql & lngCodPersonaFis & "))"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close


sql = "DELETE FROM CI0100 WHERE"
sql = sql & " PR04NUMACTPLAN IN ( SELECT PR04NUMACTPLAN FROM PR0400"
sql = sql & " WHERE CI21CODPERSONA =" & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close


sql = "DELETE FROM PR1000 WHERE"
sql = sql & " PR04NUMACTPLAN IN ( SELECT PR04NUMACTPLAN FROM PR0400"
sql = sql & " WHERE CI21CODPERSONA =" & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR0700 WHERE"
sql = sql & " PR04NUMACTPLAN IN ( SELECT PR04NUMACTPLAN FROM PR0400"
sql = sql & " WHERE CI21CODPERSONA =" & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "UPDATE PR0900 SET PR04NUMACTPLAN = NULL WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "UPDATE SIHCCUN.PRASIST SET PR04NUMACTPLAN = NULL  WHERE"
sql = sql & " PR04NUMACTPLAN IN ( SELECT PR04NUMACTPLAN FROM PR0400"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR4200 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM"
sql = sql & " PR0400  WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close



'mjgv****borra el registro de las hojas de quir�fano
sql = "DELETE FROM PR6200 WHERE PR62CODHOJAQUIR IN (SELECT PR62CODHOJAQUIR FROM"
sql = sql & " PR0400  WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR0400 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR1400 WHERE"
sql = sql & " PR03NUMACTPEDI IN ( SELECT PR03NUMACTPEDI FROM PR0300"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR0600 WHERE"
sql = sql & " PR03NUMACTPEDI IN ( SELECT PR03NUMACTPEDI FROM PR0300"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR0800 WHERE"
sql = sql & " PR09NUMPETICION IN ( SELECT PR09NUMPETICION FROM PR0900"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR4100 WHERE"
sql = sql & " PR03NUMACTPEDI IN ( SELECT PR03NUMACTPEDI FROM PR0300"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR4700 WHERE"
sql = sql & " PR09NUMPETICION IN ( SELECT PR09NUMPETICION FROM PR0900"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR3900 WHERE"
sql = sql & " PR03NUMACTPEDI IN ( SELECT PR03NUMACTPEDI FROM PR0300"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR3800 WHERE"
sql = sql & " PR03NUMACTPEDI IN ( SELECT PR03NUMACTPEDI FROM PR0300"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR6100 WHERE PR03NUMACTPEDI_ASO IN (SELECT PR03NUMACTPEDI"
sql = sql & " FROM PR0300 WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR0300 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR4700 WHERE PR09NUMPETICION IN (SELECT PR09NUMPETICION"
sql = sql & " FROM PR0900 WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM PR0900 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM FR2800 WHERE FR66CODPETICION IN (SELECT FR66CODPETICION "
sql = sql & " FROM FR6600 WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM FRA400 WHERE FR66CODPETICION IN (SELECT FR66CODPETICION "
sql = sql & " FROM FR6600 WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM FR6500 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM FR6600 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close


sql = "DELETE FROM AD1100 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close
sql = "DELETE FROM AD0500 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD1600 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD1800 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "UPDATE AD1500 SET AD01CODASISTENCI = NULL,"
sql = sql & " AD07CODPROCESO = NULL,"
sql = sql & " AD14CODESTCAMA = '1',"
sql = sql & " AD37CODESTADO2CAMA = Null"
sql = sql & " Where"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD0800 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD0400 WHERE"
sql = sql & " AD07CODPROCESO IN ( SELECT AD07CODPROCESO FROM AD0700"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD0700 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD2500 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

sql = "DELETE FROM AD3800 WHERE"
sql = sql & " AD01CODASISTENCI IN ( SELECT AD01CODASISTENCI FROM AD0100"
sql = sql & " WHERE CI21CODPERSONA = " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

'mjgv****borra el registro Personas relacionadas con Universidad
If lngNumHistoria <> 0 Then
    sql = "DELETE FROM CI2400 WHERE"
    sql = sql & " ci22numhistoria = " & lngNumHistoria
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
End If
'mjgv****borra registro de volantes
sql = "delete from fa2500 where "
sql = sql & " ad01codasistenci in (select ad01codasistenci from ad0100"
sql = sql & " where ci21codpersona= " & lngCodPersonaFis & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close

SQL1 = "SELECT AD01CODASISTENCI FROM AD0100 WHERE CI21CODPERSONA = ?"
Set qry1 = objApp.rdoConnect.CreateQuery("", SQL1)
    qry1(0) = lngCodPersonaFis
Set rs = qry1.OpenResultset()
While Not rs.EOF
    sql = "DELETE FROM AD0100 WHERE AD01CODASISTENCI = " & rs(0)
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    rs.MoveNext
Wend
sql = "DELETE FROM AD0100 WHERE CI21CODPERSONA = " & lngCodPersonaFis
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry.Execute
qry.Close
If lngNumHistoria <> 0 Then
    sql = "DELETE FROM AR0700 WHERE CI22NUMHISTORIA = " & lngNumHistoria
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
    
    sql = "DELETE FROM AD2100 WHERE CI22NUMHISTORIA = " & lngNumHistoria
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close



    sql = "DELETE FROM AR0400 WHERE CI22NUMHISTORIA = " & lngNumHistoria
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
    
    sql = "DELETE FROM AR0500 WHERE CI22NUMHISTORIA = " & lngNumHistoria
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
 End If
    
    sql = "DELETE FROM CI1000 WHERE CI21CODPERSONA = " & lngCodPersonaFis
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
    
    
    sql = "DELETE FROM CI2200 WHERE CI21CODPERSONA = " & lngCodPersonaFis
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
    
    sql = "DELETE FROM CI2100 WHERE CI21CODPERSONA = " & lngCodPersonaFis
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    qry.Close
'End If
objApp.rdoConnect.CommitTrans
MsgBox "Realizado", vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
Screen.MousePointer = vbDefault
Call plimpiar_controles
blnDatosCargados = False
blnDatosDirCargados = False
blnCambiosPer = False
blnCambiosDir = False
cmdGuardarPer.Enabled = False
cmdGuardarDir.Enabled = False


Exit Sub
canceltrans:
MsgBox Error, vbCritical + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
objApp.rdoConnect.RollbackTrans

End Sub


Private Sub cmdeliminarresecon_Click()
Dim IntRes As Integer
IntRes = MsgBox("�Est� seguro de eliminar el Responsable Econ�mico?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
If IntRes = vbYes Then
    lngCodPersonaFis_Resecon = 0
    txtresecon.Text = ""
    cbovinculacion.Text = ""
End If
End Sub

Private Sub cmdeliminarresponsablef_Click()
Dim IntRes As Integer
IntRes = MsgBox("�Est� seguro de eliminar el Responsable Familiar?", vbYesNo + vbQuestion, Right(Me.Caption, Len(Me.Caption) - 6))
If IntRes = vbYes Then
    lngCodPersonaFis_Resf = 0
    txtrespf.Text = ""
    cbovinculacion.Text = ""
End If
End Sub

Private Sub cmdEtiquetas_Click()
Dim IntRes As Integer
If Not Hay_Persona Then Exit Sub
    IntRes = MsgBox("�Est� seguro de imprimir etiquetas de la persona?", vbQuestion + vbYesNoCancel, Right(Me.Caption, Len(Me.Caption) - 6))
    If IntRes = vbYes Then
        pImprimirEtiquetas (lngCodPersonaFis)
    End If
End Sub

Private Sub cmdGuardarDir_Click()
Dim lngDir As Long
Dim IntRes As String
Dim strDatos As String

If txthistoria.Text = "" And txtnombre.Text = "" Then
    MsgBox "No existe Ninguna Persona", vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
Else
    If blnNuevaPer Then  'Or blnnuevo Then
        MsgBox "Debe guardar primero los datos de la persona", vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
        Exit Sub
    End If
    strDatos = fcomprobardir
    If Len(strDatos) > 0 Then
        txthistoria.Text = lngnumhistoria_ant
        strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
        MsgBox strDatos, vbExclamation, Right(Me.Caption, Len(Me.Caption) - 6)
        Exit Sub
    End If
    If chkdir.Value = 1 And (lngCodPersonaFis_Resf <> 0 Or lngCodPersonaFis_Resecon <> 0) Then
            If lngCodPersonaFis_Resf <> 0 And lngCodPersonaFis_Resf <> lngCodPersonaFis Then
                Call seldir(lngCodPersonaFis_Resf)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Familiar"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDirTran(lngCodPersonaFis_Resf)
                    End If
                End If
            End If 'Actualizar responsable familiar
            If lngCodPersonaFis_Resecon <> 0 And lngCodPersonaFis_Resecon <> lngCodPersonaFis And lngCodPersonaFis_Resecon <> lngCodPersonaFis_Resf Then
                Call seldir(lngCodPersonaFis_Resecon)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Econ�mico"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDirTran(lngCodPersonaFis_Resecon)
                    End If
                End If
            End If
    End If
    lngDir = lngNumDir
    Call pGuardarDirTran(lngCodPersonaFis, lngNumDir)
    
    Call seldir(lngCodPersonaFis)
    Call pdirecciones
    Call pLlenarDatosDir
    blnCambiosDir = False
    cmdGuardarDir.Enabled = False
End If
End Sub

Private Sub cmdGuardarPer_Click()
'Dim intres As Integer
Dim blnAbort As Boolean
Call pGuardarPer(blnAbort)
blnSalir = False
End Sub


Private Sub cmdImpHF_Click()
If Not Hay_Persona Then Exit Sub
Me.PopupMenu mnuImprimir
End Sub

Private Sub cmdImPrograma_Click()
If Not Hay_Persona Then Exit Sub
Me.PopupMenu mnuImprimir

'  Dim msg$, strW$, i%
'    Dim sql As String
'    Dim rs As rdoResultset
'
'    If lngCodPersonaFis = 0 Then Exit Sub
'    sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = 4 "
'    sql = sql & " AND SG02COD = '" & objSecurity.strUser & "'"
'    Set rs = objApp.rdoConnect.OpenResultset(sql)
'    If rs(0) = 0 Then
'      strW = "PR0463J.CI21CODPERSONA= " & lngCodPersonaFis
'      Call Imprimir_API(strW, "PROPAC2.rpt")
'    Else
'      MsgBox "Este Programa es para uso interno del Departamento", vbCritical, Me.Caption
'    End If
End Sub

Private Sub CmdIngresos_Click()
Dim intMsgBox   As Integer
 Dim vntData(1 To 2)
If Not Hay_Persona Then Exit Sub
Me.MousePointer = vbHourglass
    If IsNull(rsdir!ci10numdirecci) Then
        MsgBox "No es posible citar sin una Direcci�n", vbExclamation, "Aviso"
        Me.MousePointer = vbDefault
        Exit Sub
    End If
        CmdIngresos.Enabled = False
        vntData(1) = lngCodPersonaFis
    
        If txthistoria.Text = "" Then
            vntData(2) = 0
        Else
            vntData(2) = lngNumHistoria
        End If
        Call objPipe.PipeSet("CODPER", vntData(1))
        Call objPipe.PipeSet("NHISTORIA", vntData(2))
        lngCodPers = lngCodPersonaFis '5/07/2000 Estas llamadas habr� que quitarlas cuando se elimine definitivamente
        lngHistoria = lngNumHistoria 'la pantalla de personas f�sicas
        Call objSecurity.LaunchProcess("CI2046")
        CmdIngresos.Enabled = True
  Me.MousePointer = vbDefault

End Sub

Private Sub cmdNuevoDir_Click()
Dim sql As String
Dim IntRes As Integer
Dim strDatos As String
If blnCambiosDir Then
    IntRes = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
    If IntRes = vbYes Then
        strDatos = fcomprobardir
        If Len(strDatos) > 0 Then
            strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
            MsgBox strDatos, vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
            Exit Sub
        End If
        If chkdir.Value = 1 And (lngCodPersonaFis_Resf <> 0 Or lngCodPersonaFis_Resecon <> 0) Then
            If lngCodPersonaFis_Resf <> 0 And lngCodPersonaFis_Resf <> lngCodPersonaFis Then
                Call seldir(lngCodPersonaFis_Resf)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Familiar"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDirTran(lngCodPersonaFis_Resf)
                    End If
                End If
            End If 'Actualizar responsable familiar
            If lngCodPersonaFis_Resecon <> 0 And lngCodPersonaFis_Resecon <> lngCodPersonaFis And lngCodPersonaFis_Resecon <> lngCodPersonaFis_Resf Then
                Call seldir(lngCodPersonaFis_Resecon)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Econ�mico"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDirTran(lngCodPersonaFis_Resecon)
                    End If
                End If
            End If
            Call pGuardarDirTran(lngCodPersonaFis)
        End If 'de si quieren
    End If 'De si existen cambios
End If
    'cmdguardardir.Enabled = True
    txtresdir.Text = "" 'Se limpian los controles de direccion
    txtcodpostal.Text = ""
    txtlocalidaddir.Text = ""
    txtcalle.Text = ""
    txtportal.Text = ""
    txttfnodir.Text = ""
    txtobservaciones.Text = ""
    txtfax.Text = ""
    cbopaisdir.RemoveAll
    cbopaisdir.Text = ""
    cboprovdir.RemoveAll
    cboprovdir.Text = ""
    dcbofecini.Text = strFecha_Sistema
    dcbofecini.Date = strFecha_Sistema
    dcbofecfin.Text = ""
    dcbofecfin.Date = ""
    chkdir.Value = 0
    vsDirec.Enabled = True
    
    
    'txtobservaciones.Text = rs
        
sql = "SELECT CI19CODPAIS,CI19DESPAIS FROM CI1900 order by ci19despais" 'Se cargan los combos de direcci�n
Call pcargarcombos(sql, cbopaisdir)
blnnuevadir = True
cbopaisdir.SetFocus
blnCambiosDir = False
blnDatosDirCargados = True
cmdGuardarDir.Enabled = False
End Sub

Private Sub cmdNuevoPer_Click()
Dim IntRes As Integer
Dim blnAbort As Boolean
If blnDatosCargados Then
    If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        IntRes = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If IntRes = vbYes Then
            Call pGuardarPer(blnAbort)
            If blnAbort Then
                blnAbort = False
                Exit Sub
            End If
        End If 'de si quieren guardar los cambios
    End If
    blnCambiosDir = False
    blnDatosDirCargados = False
    blnDatosCargados = False
    blnCambiosPer = False
    cmdGuardarPer.Enabled = False
    cmdGuardarDir.Enabled = False
    
End If
    If blnInicio Then
        Call pHabilitar_Controles
        blnInicio = False
    Else
        Call plimpiar_controles(True)
    End If
    
    txthistoria.Enabled = False
    txthistoria.BackColor = &HC0C0C0
    Call pselcombos

    cbopaisnac.Columns(0).Value = 45
    cbopaisnac.Text = "Espa�a"
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovnac)
    cbopaisdir.Columns(0).Value = 45
    cbopaisdir.Text = "Espa�a"
    sql = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovdir)
    dcbofecini.Date = strFecha_Sistema
    dcbofecini.Text = strFecha_Sistema

    blnDatosCargados = True
    blnDatosDirCargados = True
    blnNuevaPer = True
    blnnuevadir = True
End Sub


Private Sub cmdSalir_Click()
    blnSalir = True
    Unload Me
End Sub
Private Sub cmdSolNH_Click()
Dim vntData(1)

If txthistoria.Text <> "" Then
    vntData(1) = txthistoria.Text
    Call objSecurity.LaunchProcess("AR1021", vntData)
End If

End Sub

Private Sub cmdUniversidad_Click()
Dim rdoUniver As rdoResultset
Dim strSql As String
Dim intI As Integer
Dim intJ As Integer
Dim blnUniver As Boolean
Dim blnVerPersonal As Boolean
Dim blnBaja As Boolean

'Call cmdguardarper_Click
If Not Hay_Persona Then Exit Sub
'Buscamos por historia si hay
If txthistoria <> 0 Then 'Not txtText1(8) = "" Then
    strSql = "SELECT * FROM CI2400 WHERE CI22NUMHISTORIA=" & txthistoria.Text 'xtText1(8)
    strSql = strSql & " AND (CI24FECFIN IS NULL OR CI24FECFIN >= SYSDATE)"
    Set rdoUniver = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    If Not rdoUniver.RowCount = 0 Then
    blnUniver = True
    Else
        strSql = "SELECT * FROM CI2400 WHERE CI22NUMHISTORIA=" & txthistoria.Text 'txtText1(8)
        Set rdoUniver = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
        If Not rdoUniver.EOF Then blnBaja = True
    End If
End If
If blnUniver Then
    intI = MsgBox("EL paciente pertenece al personal Universitario" & Chr$(13) & _
    "�Desea verlo en la pantalla de Personal Universitario?", vbQuestion + vbYesNo)
    If intI = vbYes Then
        blnPerFisica = True
        ReDim lngNumCodEmpl(0 To objGen.GetRowCount(rdoUniver) - 1)
        For intJ = 0 To objGen.GetRowCount(rdoUniver) - 1
             lngNumCodEmpl(intJ) = Val(rdoUniver("CI24CODEMPLEAD"))
             rdoUniver.MoveNext
        Next intJ
        blnPerFisica = True
        lngNumHistoria = Val(txthistoria.Text) 'txtText1(8).Text)
        Call objPipe.PipeSet("Histuni", lngNumHistoria)
        Call objPipe.PipeSet("PrimerNumEmplead", lngNumCodEmpl(0))
        Call objSecurity.LaunchProcess("CI1009")
        objPipe.PipeRemove ("Histuni")
        objPipe.PipeRemove ("PrimerNumEmplead")
    End If
    rdoUniver.Close
Else
    If blnBaja Then
       intI = MsgBox("Este paciente est� dado de baja en Personal Universitario" & Chr$(13) & _
                            "�Desea verlo en la pantalla de mantenimiento?", vbQuestion + vbYesNo)
       If intI = vbYes Then
        blnPerFisica = True
        ReDim lngNumCodEmpl(0 To objGen.GetRowCount(rdoUniver) - 1)
        For intJ = 0 To objGen.GetRowCount(rdoUniver) - 1
             lngNumCodEmpl(intJ) = Val(rdoUniver("CI24CODEMPLEAD"))
             rdoUniver.MoveNext
        Next intJ
        blnPerFisica = True
        lngNumHistoria = Val(txthistoria.Text) 'txtText1(8).Text)
        Call objPipe.PipeSet("Histuni", lngNumHistoria)
        Call objPipe.PipeSet("PrimerNumEmplead", lngNumCodEmpl(0))
        Call objSecurity.LaunchProcess("CI1009")
        objPipe.PipeRemove ("Histuni")
        objPipe.PipeRemove ("PrimerNumEmplead")
       End If
    Else
    strSql = "SELECT CI24CODEMPLEAD FROM CI2400 WHERE CI24NOMBRBENEF LIKE '%" & txtnombre.Text & "%'" 'Text1(1) & "%'"
  strSql = strSql & " AND CI24APELLIBENEF LIKE '%" & txtnombre.Text & " " & txtape1.Text & "%'"
  Set rdoUniver = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
  If objGen.GetRowCount(rdoUniver) > 0 Then
    intI = MsgBox("Se han encontrado " & objGen.GetRowCount(rdoUniver) & " similares, pero no estan " & Chr$(13) & "asociados al n� de Historia " _
    & "�Desea buscarlos para asociarlos?", vbQuestion + vbYesNo)
    If intI = vbYes Then
        blnPerFisica = True
        ReDim lngNumCodEmpl(0 To objGen.GetRowCount(rdoUniver) - 1)
        For intJ = 0 To objGen.GetRowCount(rdoUniver) - 1
             lngNumCodEmpl(intJ) = Val(rdoUniver("CI24CODEMPLEAD"))
             rdoUniver.MoveNext
        Next intJ
        blnPerFisica = True
        lngNumHistoria = Val(txthistoria.Text) 'Text1(8).Text)
        Call objPipe.PipeSet("Histuni", lngNumHistoria)
        Call objPipe.PipeSet("NomBenef", txtnombre.Text) 'txtText1(1).Text)
        Call objPipe.PipeSet("ApelBenef", txtape1.Text) 'txtText1(2).Text)
        Call objPipe.PipeSet("PrimerNumEmplead", lngNumCodEmpl(0))
        Call objSecurity.LaunchProcess("CI1009")
        objPipe.PipeRemove ("Histuni")
        objPipe.PipeRemove ("NomBenef")
        objPipe.PipeRemove ("ApelBenef")
        objPipe.PipeRemove ("PrimerNumEmplead")
    End If
  Else
   intI = MsgBox("No se ha encontrado el paciente." & Chr$(13) & "�Desea ver el mantenimiento " & _
     "de personal universitario?", vbQuestion + vbYesNo)
    If intI = vbYes Then
    lngNumHistoria = Val(txthistoria.Text) 'txtText1(8).Text)
        Call objPipe.PipeSet("Histuni", lngNumHistoria)
        Call objSecurity.LaunchProcess("CI1009")
        objPipe.PipeRemove ("Histuni")
    End If
 End If
 End If
End If
 blnPerFisica = False

End Sub

Private Sub cmdVer_Click()
'Call cmdguardarper_Click
Me.MousePointer = vbHourglass
If Not Hay_Persona Then Exit Sub
ReDim vntData(1) As Variant
        cmdVer.Enabled = False
        ReDim vntData(1 To 2) As Variant
        vntData(1) = lngCodPersonaFis
        vntData(2) = 2
        Call objSecurity.LaunchProcess("HC02", vntData())
        Me.MousePointer = vbDefault
        cmdVer.Enabled = True
End Sub







Private Sub Command1_Click()

End Sub

Private Sub dcbofecfall_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
If dcbofecfall.Text = "" Then
    chkfall.Value = 0
Else
    chkfall.Value = 1
End If

End Sub

Private Sub dcbofecfall_Click()
If dcbofecfall.Text = "" Then
    chkfall.Value = 0
Else
    chkfall.Value = 1
End If
End Sub

Private Sub dcbofecfin_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If
End Sub


Private Sub dcbofecfin_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If
End Sub

Private Sub dcbofecini_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If


End Sub


Private Sub dcbofecini_Click()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If

End Sub

Private Sub dcbofecnac_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub dcbofecnac_Click()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub Form_Load()
Dim con As Control
Screen.MousePointer = vbDefault
If objPipe.PipeExist("CODPERS") Then
'If lngUserCode <> 0 Then
    'lngCodPersonaFis = lngUserCode
    lngCodPersonaFis = objPipe.PipeGet("CODPERS")
    objPipe.PipeRemove ("CODPERS")
    blnLlamada = True
    blnBuscar = True
    Call pcargar_datos
Else
    blnInicio = True ' la primera ejecucion se desabilitan todos los controles excepto nuevo, buscar, y el textbox de historia
    For Each con In Me
        If TypeOf con Is SSDBCombo Or TypeOf con Is SSDateCombo Or TypeOf con Is TextBox Or TypeOf con Is CheckBox Or TypeOf con Is CommandButton Or TypeOf con Is VScrollBar Then
        con.Enabled = False
            If Not (TypeOf con Is CheckBox Or TypeOf con Is VScrollBar) Then  'Or Not TypeOf Con Is CheckBox) Then
                con.BackColor = &HC0C0C0
                End If
         End If
    Next
    txthistoria.Enabled = True
    txthistoria.BackColor = &HFFFFFF
    cmdNuevoPer.Enabled = True
    cmdBuscarPer.Enabled = True
    cmdSalir.Enabled = True
End If
'    Call pselcombos
'    cbopaisnac.Columns(0).Value = 45
'    cbopaisnac.Text = "Espa�a"
'    SQL = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
'    Call pcargarcombos(SQL, cboprovnac)
'    cbopaisdir.Columns(0).Value = 45
'    cbopaisdir.Text = "Espa�a"
'    SQL = "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
'    Call pcargarcombos(SQL, cboprovdir)
'    dcbofecini.Date = strFecha_Sistema
'    dcbofecfall.Date = ""
'    'txthistoria.SetFocus
'End If
 If blnLlamada Then
    cmdNuevoDir.Enabled = False
    cmdNuevoPer.Enabled = False
    cmdBuscarPer.Enabled = False
    cmdGuardarPer.Enabled = False
    cmdGuardarDir.Enabled = False
    txthistoria.Locked = True
    
   
    'cmdsalir.Enabled = True
End If
  Screen.MousePointer = vbDefault
 End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim IntRes As Integer
Dim blnAbort As Boolean
If txtnombre.Text = "" Then
    blnSalir = False
    Exit Sub
Else
If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        blnSalir = True 'se va a cambiar de persona, luego no hace falta volver a seleccionar las direcciones
        IntRes = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If IntRes = vbYes Then
            Call pGuardarPer(blnAbort)
            If blnAbort Then
                blnAbort = False
                Cancel = True
                Exit Sub
            End If
            'Call pGuardarDir(lngCodPersonaFis, lngNumDir)
        End If 'de si quieren guardar los cambios
    End If
    blnCambiosDir = False
    blnDatosDirCargados = False
    blnDatosCargados = False
    blnCambiosPer = False
    cmdGuardarPer.Enabled = False
    cmdGuardarDir.Enabled = False
    
End If




'If blnCambiosPer Or blnCambiosDir Or blnNuevaPer Then
'              intres = MsgBox("�Desea guardar los cambios?", vbQuestion + vbYesNo, Right(Right(Me.Caption, Len(Me.Caption) - 6), Len(Right(Me.Caption, Len(Me.Caption) - 6)) - 6))
'              If intres = vbYes Then
'                 cmdguardarper_Click
'                 If chkdir.Value = 1 And (lngCodPersonaFis_Resf <> 0 Or lngCodPersonaFis_Resecon <> 0) Then
'                    intres = MsgBox("Desea actualizar la direccion del responsable", vbQuestion + vbYesNo, Right(Right(Me.Caption, Len(Me.Caption) - 6), Len(Right(Me.Caption, Len(Me.Caption) - 6)) - 6))
'                    If intres = vbYes Then
'                        If lngCodPersonaFis_Resf <> 0 Then
'                            Call seldir(lngCodPersonaFis_Resf)
'                            Call pGuardarDir(lngCodPersonaFis_Resf)
'                        End If
'                        If lngCodPersonaFis_Resecon <> 0 Then
'                            Call seldir(lngCodPersonaFis_Resecon)
'                            Call pGuardarDir(lngCodPersonaFis_Resecon)
'                        End If
'                     End If
'                    End If
'
'                 Call pGuardarDir(lngCodPersonaFis, lngNumDir)
'               End If
'              End If
'              'Else
'              'Cancel = True
'             'End If
'        'Else
''                blnsalir = False
''                Cancel = True
''                Exit Sub
'        End If
'    '
'    'End If
'    blnsalir = False

End Sub

Private Sub Form_Unload(Cancel As Integer)
  lngCodPers = lngCodPersonaFis
  'PGM Establezco el codigo de persona para pasarlo a otro formulario
  If lngCodPersonaFis <> 0 Then
    Call objPipe.PipeSet("CODPER", lngCodPersonaFis)
  End If
  lngNumHistoria = 0
  lngnumhistoria_ant = 0
  blnDatosDirCargados = False
  blnDatosCargados = False
  blnCambiosDir = False
  blnCambiosPer = False
  cmdGuardarPer.Enabled = False
  cmdGuardarDir.Enabled = False
End Sub


Private Sub mnuOpPaciente_Click()
Dim strWhere As String
strWhere = "CI2223J.CI21CODPERSONA=" & lngCodPersonaFis
Call Imprimir_API(strWhere, "CI4017.rpt")
End Sub

Private Sub mnuOpProg_Click()
  Dim msg$, strW$, i%
    Dim sql As String
    Dim rs As rdoResultset
    
    If lngCodPersonaFis = 0 Then Exit Sub
    sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = 4 "
    sql = sql & " AND SG02COD = '" & objSecurity.strUser & "'"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    If rs(0) = 0 Then
      strW = "PR0463J.CI21CODPERSONA= " & lngCodPersonaFis
      Call Imprimir_API(strW, "PROPAC2.rpt")
    Else
      MsgBox "Este Programa es para uso interno del Departamento", vbCritical, Me.Caption
    End If
End Sub

Private Sub txtape1_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtape1_GotFocus()
txtape1.SelStart = 0
txtape1.SelLength = Len(Trim(txtape1.Text))

End Sub

Private Sub txtape2_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtape2_GotFocus()
txtape2.SelStart = 0
txtape2.SelLength = Len(Trim(txtape2.Text))

End Sub

Private Sub txtcalle_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If

End Sub

Private Sub txtcalle_GotFocus()
txtcalle.SelStart = 0
txtcalle.SelLength = Len(Trim(txtcalle.Text))


End Sub

Private Sub txtcodpostal_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If


End Sub

Private Sub txtcodpostal_GotFocus()
  
txtcodpostal.SelStart = 0
txtcodpostal.SelLength = Len(Trim(txtcodpostal.Text))

End Sub

Private Sub txtdni_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtdni_GotFocus()
txtdni.SelStart = 0
txtdni.SelLength = Len(Trim(txtdni.Text))

End Sub

Private Sub txtempresa_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub txtempresa_GotFocus()
txtempresa.SelStart = 0
txtempresa.SelLength = Len(Trim(txtempresa.Text))

End Sub

Private Sub txtfax_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If

End Sub

Private Sub txtfax_GotFocus()
txtfax.SelStart = 0
txtfax.SelLength = Len(Trim(txtfax.Text))

End Sub

Private Sub txtHistoria_Change()
Dim blnAbort As Boolean
Dim IntRes As Integer
Dim strDatos As String
If Val(txthistoria.Text) = lngnumhistoria_ant Then Exit Sub  'Esto es porque cuando les sale el mensaje de datos obligatorios, a volver a cargar la historia se vuelve a provocar el evento change
If blnDatosCargados Then
If blnCambiosPer Or blnCambiosDir Then 'aqui hay que mardarlo al control de campos
        blnSalir = True 'se va a cambiar de persona, luego no hace falta volver a seleccionar las direcciones
        IntRes = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
        If IntRes = vbYes Then
            Call pGuardarPer(blnAbort)
            If blnAbort Then
                blnAbort = False
                Exit Sub
            End If
            'Call pGuardarDir(lngCodPersonaFis, lngNumDir)
        End If 'de si quieren guardar los cambios
    End If
    blnCambiosDir = False
    blnDatosDirCargados = False
    blnDatosCargados = False
    blnCambiosPer = False
    cmdGuardarPer.Enabled = False
    cmdGuardarDir.Enabled = False
End If
If blnInicio Then
    Call pHabilitar_Controles
    blnInicio = False
    Exit Sub
End If
If txthistoria.Text <> "" And blnLlamada = False Then 'hay datos cargados y no viene de otra pantalla
    blnLHis = False
Call plimpiar_controles(blnLHis)
End If
'blnLlamada = False
blnIbm = False
'txthistoria.Text = lngnumhistoria_ant
'End If 'de cambios

End Sub



Private Sub txtHistoria_GotFocus()
txthistoria.SelStart = 0
txthistoria.SelLength = Len(Trim(txthistoria.Text))
End Sub

Private Sub txtHistoria_KeyPress(KeyAscii As Integer)
If KeyAscii > 64 And KeyAscii < 91 Or KeyAscii > 96 And KeyAscii < 123 Then
    MsgBox "La historia es siempre num�rica. Vuelva a introducir un n�mero correcto", vbExclamation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    KeyAscii = 0
End If
If KeyAscii = 13 Then txthistoria_LostFocus
End Sub

Private Sub txthistoria_LostFocus()
Dim IntRes As Integer
Dim con As Control
lngNumHistoria = Val(txthistoria.Text)
If lngnumhistoria_ant <> lngNumHistoria Then
  If lngNumHistoria = 0 Then
      Exit Sub
   Else
      If txtnombre.Text = "" And txthistoria.Text <> 0 Then
        blnIbm = False
        Call plimpiar_controles
        Call pcargar_datos
        If Not blnLHis Then
            txthistoria.Text = lngNumHistoria
        End If
        blnLHis = False
       Else
       Call plimpiar_controles
       Call pcargar_datos
     End If
  End If
  lngnumhistoria_ant = lngNumHistoria
End If
End Sub

Private Sub txtlocalidaddir_Change()
'txtcodpostal.Text = ""
'txtcalle.Text = ""
'txtportal.Text = ""
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If
'If cboprovdir.Text <> "" And cbopaisdir.Columns(0).Value = "45" Then
'        cmdbuscarcodpostal.Enabled = True
'        If Trim(txtlocalidaddir.Text) <> "" Then
'            cmdBuscarCalle.Enabled = True
'        End If
'End If

        


End Sub

Private Sub txtlocalidaddir_GotFocus()
txtlocalidaddir.SelStart = 0
txtlocalidaddir.SelLength = Len(Trim(txtlocalidaddir.Text))

End Sub

Private Sub txtlocalidadnac_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtlocalidadnac_GotFocus()
txtlocalidadnac.SelStart = 0
txtlocalidadnac.SelLength = Len(Trim(txtlocalidadnac.Text))

End Sub

Private Sub txtmovil_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtmovil_GotFocus()
txtmovil.SelStart = 0
txtmovil.SelLength = Len(Trim(txtmovil.Text))

End Sub

Private Sub txtnombre_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtnombre_GotFocus()
txtnombre.SelStart = 0
txtnombre.SelLength = Len(txtnombre.Text)
End Sub

Private Sub txtobser_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtobser_GotFocus()
txtobser.SelStart = 0
txtobser.SelLength = Len(Trim(txtobser.Text))

End Sub


Private Sub txtobservaciones_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If

End Sub

Private Sub txtobservaciones_GotFocus()
txtobservaciones.SelStart = 0
txtobservaciones.SelLength = Len(Trim(txtobservaciones.Text))

End Sub

Private Sub txtportal_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If


End Sub

Private Sub txtportal_GotFocus()
txtportal.SelStart = 0
txtportal.SelLength = Len(Trim(txtportal.Text))

End Sub

Private Sub txtpuesto_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub txtpuesto_GotFocus()
txtpuesto.SelStart = 0
txtpuesto.SelLength = Len(Trim(txtpuesto.Text))

End Sub

Private Sub txtresdir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If


End Sub

Private Sub txtresdir_GotFocus()
txtresdir.SelStart = 0
txtresdir.SelLength = Len(Trim(txtresdir.Text))

End Sub

Private Sub txtresecon_Change()
If Trim(txtresecon.Text) = "" Then
            cmdeliminarresecon.Enabled = False
            
    Else
            cmdeliminarresecon.Enabled = True
            
            
End If
 If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
 End If
End Sub

Private Sub txtrespf_Change()
If Trim(txtrespf.Text) <> "" Then
           ' cmdverresf.Enabled = True
            cmdeliminarresponsablef.Enabled = True
            cbovinculacion.Enabled = True
            cbovinculacion.BackColor = &HFFFFFF
    Else
            'cmdverresf.Enabled = False
            cmdeliminarresponsablef.Enabled = False
            cbovinculacion.Enabled = False
            cbovinculacion.BackColor = &HC0C0C0
End If
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub txtssnum_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtssnum_GotFocus()
txtssnum.SelStart = 0
txtssnum.SelLength = Len(Trim(txtssnum.Text))

End Sub

Private Sub txtssnum2_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtssnum2_GotFocus()
txtssnum2.SelStart = 0
txtssnum2.SelLength = Len(Trim(txtssnum2.Text))

End Sub

Private Sub txtsspro_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If

End Sub

Private Sub txtsspro_GotFocus()
txtsspro.SelStart = 0
txtsspro.SelLength = Len(Trim(txtsspro.Text))

End Sub

Private Sub txttfnodir_Change()
If blnDatosDirCargados Then
    blnCambiosDir = True
    cmdGuardarDir.Enabled = True
End If


End Sub

Private Sub txttfnodir_GotFocus()
txttfnodir.SelStart = 0
txttfnodir.SelLength = Len(Trim(txttfnodir.Text))

End Sub

Private Sub txttfnotrab_Change()
If blnDatosCargados Then
    blnCambiosPer = True
    cmdGuardarPer.Enabled = True
End If
End Sub

Private Sub txttfnotrab_GotFocus()
txttfnotrab.SelStart = 0
txttfnotrab.SelLength = Len(Trim(txttfnotrab.Text))

End Sub

Private Sub vsDirec_Change()
Dim IntRes As Integer
Dim strDatos As String
Dim strMensaje As String
'Dim lngDire As Long
'if
If blnCambiosDir Then
  IntRes = MsgBox("�Desea Guardar los cambios realizados?", vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
  If IntRes = vbYes Then
        strDatos = fcomprobardir
        If Len(strDatos) > 0 Then
            strDatos = "Los siguientes Datos son obligatorios y est�n en blanco: " & Left(strDatos, Len(strDatos) - 2)
            MsgBox strDatos, vbInformation, Right(Me.Caption, Len(Me.Caption) - 6)
            Exit Sub
        End If
        If chkdir.Value = 1 And (lngCodPersonaFis_Resf <> 0 Or lngCodPersonaFis_Resecon <> 0) Then
            If lngCodPersonaFis_Resf <> 0 And lngCodPersonaFis_Resf <> lngCodPersonaFis Then
                Call seldir(lngCodPersonaFis_Resf)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Familiar"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDirTran(lngCodPersonaFis_Resf)
                    End If
                End If
            End If 'Actualizar responsable familiar
            If lngCodPersonaFis_Resecon <> 0 And lngCodPersonaFis_Resecon <> lngCodPersonaFis And lngCodPersonaFis_Resecon <> lngCodPersonaFis_Resf Then
                Call seldir(lngCodPersonaFis_Resecon)
                If Not rsdir.EOF Then
                    IntRes = MsgBox(fMensajeRes("Econ�mico"), vbQuestion + vbYesNo, Right(Me.Caption, Len(Me.Caption) - 6))
                    If IntRes = vbYes Then
                        Call pGuardarDirTran(lngCodPersonaFis_Resecon)
                    End If
                End If
            End If
        End If
        Call pGuardarDirTran(lngCodPersonaFis, lngNumDir)
        Call seldir(lngCodPersonaFis)
        blnCambiosDir = False
        cmdGuardarDir.Enabled = False
        Call pdirecciones(vsDirec.Value)
    End If 'de guardar los cambios
End If 'de que hay cambios
blnDatosDirCargados = False
blnCambiosDir = False
cmdGuardarDir.Enabled = False
  If vsDirec.Value = vsDirec.Max Then
    rsdir.MoveLast
  Else
    rsdir.Move vsDirec.Value, 1
  End If
  Call pLlenarDatosDir
  'End If
  'blnDatosDirCargados = True
End Sub


