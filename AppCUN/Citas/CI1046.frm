VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Begin VB.Form frmIngresos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingresos"
   ClientHeight    =   5790
   ClientLeft      =   3525
   ClientTop       =   1500
   ClientWidth     =   4845
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5790
   ScaleWidth      =   4845
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Datos"
      Height          =   5175
      Left            =   120
      TabIndex        =   12
      Top             =   0
      Width           =   4575
      Begin VB.TextBox txtActual 
         Height          =   375
         Left            =   120
         TabIndex        =   28
         Top             =   4725
         Visible         =   0   'False
         Width           =   4350
      End
      Begin VB.Frame Frame2 
         Caption         =   "Ultimo Proceso / Asistencia"
         Height          =   1095
         Left            =   120
         TabIndex        =   19
         Top             =   3600
         Width           =   4350
         Begin VB.Label Label12 
            Caption         =   "Cama"
            Height          =   255
            Left            =   2520
            TabIndex        =   27
            Top             =   720
            Width           =   735
         End
         Begin VB.Label LBLCAMA 
            BackColor       =   &H8000000B&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   3240
            TabIndex        =   26
            Top             =   720
            Width           =   750
         End
         Begin VB.Label Label10 
            Caption         =   "Tipo de Cama"
            Height          =   255
            Left            =   360
            TabIndex        =   25
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label LBLTIPCAMA 
            BackColor       =   &H8000000B&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   1680
            TabIndex        =   24
            Top             =   720
            Width           =   750
         End
         Begin VB.Label Label8 
            Caption         =   "Entidad"
            Height          =   255
            Left            =   2520
            TabIndex        =   23
            Top             =   360
            Width           =   735
         End
         Begin VB.Label LBLENTID 
            BackColor       =   &H8000000B&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   3240
            TabIndex        =   22
            Top             =   360
            Width           =   750
         End
         Begin VB.Label Label6 
            Caption         =   "Tipo Econ�mico"
            Height          =   255
            Left            =   360
            TabIndex        =   21
            Top             =   360
            Width           =   1215
         End
         Begin VB.Label LBLTIPECO 
            BackColor       =   &H8000000B&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   1680
            TabIndex        =   20
            Top             =   360
            Width           =   750
         End
      End
      Begin VB.TextBox txtHora 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   2040
         MaxLength       =   2
         TabIndex        =   1
         Tag             =   "Hora"
         Top             =   445
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         HelpContextID   =   3
         Index           =   0
         Left            =   2670
         MaxLength       =   2
         TabIndex        =   3
         Tag             =   "Minutos"
         Top             =   445
         Width           =   390
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "PR09DESOBSERVAC"
         Height          =   930
         HelpContextID   =   30104
         Index           =   1
         Left            =   120
         MaxLength       =   256
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Tag             =   "Observaciones de la Petici�n|Observaciones"
         Top             =   2160
         Width           =   4350
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDCFecha 
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   480
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbDpto 
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Width           =   2415
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4260
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbRecurso 
         Height          =   255
         Left            =   2520
         TabIndex        =   7
         Top             =   1680
         Width           =   1935
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3413
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbPrueba 
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1680
         Width           =   2295
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4048
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin ComCtl2.UpDown UpDownMinuto 
         Height          =   330
         Index           =   0
         Left            =   3060
         TabIndex        =   4
         Top             =   405
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   3060
         OrigTop         =   405
         OrigRight       =   3300
         OrigBottom      =   735
         Increment       =   5
         Max             =   55
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDownHora 
         Height          =   330
         Index           =   0
         Left            =   2430
         TabIndex        =   2
         Top             =   405
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   2430
         OrigTop         =   405
         OrigRight       =   2670
         OrigBottom      =   735
         Max             =   23
         Enabled         =   -1  'True
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbTipoCama 
         Height          =   255
         Left            =   2040
         TabIndex        =   9
         Top             =   3240
         Width           =   2415
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4260
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label Label7 
         Caption         =   "Tipo de Cama solicitada:"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   3240
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora de Inicio"
         Height          =   195
         Index           =   5
         Left            =   2040
         TabIndex        =   18
         Top             =   240
         Width           =   990
      End
      Begin VB.Label Label5 
         Caption         =   "Observaciones"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1920
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Departamento"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Prueba"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha de Ingreso"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Recurso"
         Height          =   255
         Left            =   2520
         TabIndex        =   13
         Top             =   1440
         Width           =   1095
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Salir"
      Height          =   375
      Left            =   2880
      TabIndex        =   11
      Top             =   5280
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Citar ingreso"
      Enabled         =   0   'False
      Height          =   375
      Left            =   480
      TabIndex        =   10
      Top             =   5280
      Width           =   1455
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   3960
      Top             =   4320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   5
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1046.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1046.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1046.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1046.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1046.frx":0C68
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmIngresos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Datos necesarios para buscar huecos
   Dim strRecurso As String 'Almacena el recurso para el que busca los huecos disponibles
   Dim FechaActual As Date 'Almacena la Fecha en la que est� buscando Huecos
'Huecos
   'N�mero de D�as encontrados con Huecos disponibles (contador), para dejar de buscar al
   'llegar al valor de la constante conDias
   Dim bytDias As Byte
   'Si hay hueco (TRUE), no sigue buscando cuando est� cargando los d�as con huecos libres...
   Dim blnHueco As Boolean
   'Si hay que parar de buscar d�as con huecos libres (por orden del usuario)...
   Dim blnParar As Boolean
   Dim nodNodo As Node 'Nodo del TreeView (tvwHuecos)
   'EFS: Variable para guardar el numero de peticion para luego insertar las
   'restricciones
   Dim lngNumPet As Long
   Dim lngNumActPlan As Long
   Dim intTipoRecur As Long
Private Sub Command1_Click()
    
    Dim intMsgBox   As Integer
    Dim intContador As Integer
    
    Screen.MousePointer = vbHourglass
    'Realizamos la creaci�n de la cita del Ingreso
    If lngHistoria > 0 Then
        txtActual.Visible = True
        txtActual.Text = "Citando..."
        If fblnCitar Then 'Citar
            txtActual.Text = "Citado"
            'Asociar Proceso-Asistencia *****- SE QUITAR� CUANDO DESAPAREZCA EL IBM -*****
            ReDim VNTDATA(2) As Variant
            VNTDATA(1) = lngCodPers
            VNTDATA(2) = 2 'Viene de Citas   ??? AJO No se si ser� as�.
            Screen.MousePointer = vbDefault
            Call objSecurity.LaunchProcess("AD0105", VNTDATA)
            'If Not blnInsert_Restri Then
            '    MsgBox "Ha habido errores al asociar las restrinciones", vbCritical
            'End If
        Else
            MsgBox "Se ha producido un error al citar. Por favor, int�ntelo de nuevo", vbCritical
        End If 'fblnCitar
        txtActual.Visible = False
        SSDBCmbDpto.Value = ""
        SSDBCmbPrueba.Value = ""
        SSDBCmbRecurso.Value = ""
        SSDBCmbTipoCama.Value = ""
        txtHora(0) = "16"
        txtMinuto(0) = "00"
        txtText1(1) = ""
    Else
       intMsgBox = MsgBox("Si ha chequeado la casilla 'Paciente' en la pantalla de 'Personas F�sicas', pulse reintentar", vbRetryCancel, "El paciente " & lngCodPers & " no tiene todav�a n�mero de historia")
       txtActual.Visible = True
       txtActual.Text = "Esperando el n� de historia"
       If intMsgBox = 4 Then
            Do While lngHistoria = 0
                DoEvents
                lngHistoria = flngHistoria(lngCodPers)
                intContador = intContador + 1
                If intContador > 200 Then Exit Do
            Loop 'lngHistoria = 0
            txtActual.Visible = True
            txtActual.Text = "Citando..."
            If fblnCitar Then 'Citar
                txtActual.Text = "Citado"
                'Asociar Proceso-Asistencia *****- SE QUITAR� CUANDO DESAPAREZCA EL IBM -*****
                ReDim VNTDATA(2) As Variant
                VNTDATA(1) = lngCodPers
                VNTDATA(2) = 2 'Viene de Citas   ??? AJO No se si ser� as�.
                Screen.MousePointer = vbDefault
                Call objSecurity.LaunchProcess("AD0105", VNTDATA)
                'If Not blnInsert_Restri Then
                '    MsgBox "Ha habido errores al asociar las restrinciones", vbCritical
                'End If
            Else
                MsgBox "Se ha producido un error al citar. Por favor, int�ntelo de nuevo", vbCritical
            End If 'fblnCitar
            txtActual.Visible = False
            SSDBCmbDpto.Value = ""
            SSDBCmbPrueba.Value = ""
            SSDBCmbRecurso.Value = ""
            txtHora(0) = "16"
            txtMinuto(0) = "00"
            txtText1(1) = ""
       
       End If 'intMsgBox = 5
    End If 'lngHistoria
    txtActual.Text = ""
    txtActual.Visible = False
    Screen.MousePointer = vbDefault
    
End Sub
Private Sub Command2_Click()
Unload Me
End Sub
Private Sub Form_Load()

'Consulta SQL
Dim SQL As String
Dim qryCombos As rdoQuery
Dim rstCombos As rdoResultset

Screen.MousePointer = vbArrowHourglass
'Se cargan los Combo Box de...

'...Departamentos Responsables de Procesos
   SQL = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO FROM AD0200, AD0300 WHERE "
   'Departamentos a los que tiene acceso ese usuario (AD0300)
   SQL = SQL & "AD0300.SG02COD = ? AND AD03FECINICIO <= SYSDATE AND "
   SQL = SQL & "(AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL) AND "
   'Descripci�n del departamento (AD0200)
   SQL = SQL & "AD0300.AD02CODDPTO = AD0200.AD02CODDPTO AND AD02FECINICIO <= SYSDATE "
   SQL = SQL & "AND (AD02FECFIN >= SYSDATE OR AD02FECFIN IS NULL) AND "
   SQL = SQL & "(AD32CODTIPODPTO = 1 OR AD32CODTIPODPTO = 2)" 'Serv. Cl�nicos o B�sicos
   SQL = SQL & "AND AD02INDRESPONPROC = -1 ORDER BY AD02DESDPTO"
   Set qryCombos = objApp.rdoConnect.CreateQuery("Departamentos", SQL)
      qryCombos(0) = objSecurity.strUser
   Set rstCombos = qryCombos.OpenResultset()
   Do While Not rstCombos.EOF
      SSDBCmbDpto.AddItem rstCombos(0) & ";" & rstCombos(1)
      rstCombos.MoveNext
   Loop 'While Not rstCombos.EOF
   rstCombos.Close
   qryCombos.Close
   
   'Iniciamos la hora a las 4 de la tarde como entrada t�pica
   txtHora(0) = "16"
   txtMinuto(0) = "00"
   'Cargar los datos de Ultimo Proceso/Asistencia para el paciente
   Call pCargar_ProcesoAsistencia
   'Cargar tipos de camas
   Call pCargar_TipoCama
   Screen.MousePointer = vbDefault

End Sub

Private Sub SSDBCmbDpto_CloseUp()
   Call pCargar_PruebaRecurso 'Carga los combos de Prueba y Recurso
   
   'EFS: Si el departamento es urgencias cargamos directamente la
   'actuacion y el recurso
   If SSDBCmbDpto.Columns(0).Text = 216 Then
        SSDBCmbRecurso.Text = SSDBCmbRecurso.Columns(1).Text
        SSDBCmbPrueba.Text = SSDBCmbPrueba.Columns(1).Text
        Call pCitas_Urgencias
        Command2.Enabled = False
        Command1.Enabled = False
    'Si esta rellena la fecha, Dpto y Prueba
    'Se habilitar� el bot�n para citar el ingrewso
   ElseIf SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" Then
        Command1.Enabled = True
        Command2.Enabled = True
   Else
        Command1.Enabled = False
        Command2.Enabled = True
   End If
End Sub

Private Sub SSDBCmbDpto_KeyUp(KeyCode As Integer, Shift As Integer)
   Call pCargar_PruebaRecurso 'Carga los combos de Prueba y Recurso
End Sub

Private Sub pCargar_PruebaRecurso() 'Carga los combos de Prueba y Recurso

'Consultas SQL
Dim SQL As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset

'Inicilizar los ComboBox de Pruebas y Recursos
SSDBCmbPrueba.Text = ""
SSDBCmbRecurso.Text = ""
SSDBCmbPrueba.RemoveAll
SSDBCmbRecurso.RemoveAll
'Vaciar el TreeView
'tvwHuecos.Nodes.Clear

'Recursos
SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO FROM AG1100, AD0300 "
SQL = SQL & "WHERE AD0300.AD02CODDPTO = ? AND AD03FECINICIO <= SYSDATE AND "
SQL = SQL & "(AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL) AND "
SQL = SQL & "AD0300.SG02COD = AG1100.SG02COD AND AD0300.AD02CODDPTO = AG1100.AD02CODDPTO "
SQL = SQL & "AND AG11INDPLANIFI = -1 AND AG11FECINIVREC <= SYSDATE AND "
SQL = SQL & "(AG11FECFINVREC >= SYSDATE OR AG11FECFINVREC IS NULL)"
Set qryConsulta = objApp.rdoConnect.CreateQuery("", SQL)
   qryConsulta(0) = SSDBCmbDpto.Columns(0).Text
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbRecurso.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
SSDBCmbRecurso.AddItem ";"
rstConsulta.Close
qryConsulta.Close
'Actuaciones
SQL = "SELECT PR0100.PR01CODACTUACION, PR01DESCORTA FROM PR0100, PR0200 "
SQL = SQL & "WHERE PR0200.AD02CODDPTO = ? AND "
SQL = SQL & "PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION AND PR12CODACTIVIDAD = 209 "
SQL = SQL & "AND PR01FECINICO <= SYSDATE AND "
SQL = SQL & "(PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
Set qryConsulta = objApp.rdoConnect.CreateQuery("", SQL)
   qryConsulta(0) = SSDBCmbDpto.Columns(0).Text
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbPrueba.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
rstConsulta.Close
qryConsulta.Close

SSDBCmbPrueba.MoveFirst
SSDBCmbPrueba.Text = SSDBCmbPrueba.Columns(1).Text

End Sub

Private Sub SSDBCmbPrueba_CloseUp()
    'Si esta rellena la fecha, Dpto y Prueba
    'Se habilitar� el bot�n para citar el ingreso
    If SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" Then
        Command1.Enabled = True
    Else
        Command1.Enabled = False
    End If
    
'Vaciar el TreeView
'tvwHuecos.Nodes.Clear
End Sub

Private Sub SSDBCmbPrueba_KeyUp(KeyCode As Integer, Shift As Integer)
'Vaciar el TreeView
'tvwHuecos.Nodes.Clear
End Sub

Private Sub SSDBCmbRecurso_CloseUp()
'Vaciar el TreeView
'tvwHuecos.Nodes.Clear
End Sub

Private Sub SSDBCmbRecurso_KeyUp(KeyCode As Integer, Shift As Integer)
'Vaciar el TreeView
'tvwHuecos.Nodes.Clear
End Sub
Private Function fblnCitar() As Boolean

Dim fecCita As Date
'Conversi�n de tiempos
   Dim intDias As Integer
   Dim bytHoras As Byte
   Dim bytMinutos As Byte
'Campos Primery Key que se obtienen antes de insertar un registro
   Dim numPetic As Long
   Dim numActPedi As Long
   Dim numActPlan As Long
   Dim numSolicit As Long
'Datos necesarios que se obtienen con funciones
   Dim sg02cod As String 'Departamento del usuario
   'Fase
   Dim strDesFase As String 'Descripci�n
   Dim lngMinutFase As Long 'Minutos de ocupaci�n del paciente
   'Tipo Recurso- Fase Actuaci�n
   Dim lngNumNecesid As Long
   Dim intIndPlan As Integer
   Dim lngTiemRec As Long
   'C�digo de recurso y Tipo de Recurso para codDoctor
   Dim intTipRec As Integer
   Dim lngCodRecurso As Long
'Consultas SQL
   Dim SQL As String
   Dim QyInsert As rdoQuery
   Dim RsRespuesta As rdoResultset
'valor de las restrinciones
   Dim vntRest As Variant
   

'********************Inicializaci�n********************
On Error Resume Next
Err = 0
objApp.rdoConnect.BeginTrans

'COMIENZA EL BAILE**************************************************************************

'''Datos previos
''SQL = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD = ? AND AD03FECINICIO <= SYSDATE AND "
''SQL = SQL & "(AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL)"
''Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
''   QyInsert(0) = objSecurity.strUser
''Set RsRespuesta = QyInsert.OpenResultset()
''If Not RsRespuesta.EOF Then codDpto = RsRespuesta(0)
SQL = "SELECT SG02COD FROM AG1100 WHERE AG11CODRECURSO = ?"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
    QyInsert(0) = SSDBCmbRecurso.Columns(0).Value
Set RsRespuesta = QyInsert.OpenResultset()
If Not RsRespuesta.EOF Then sg02cod = RsRespuesta(0)
RsRespuesta.Close
QyInsert.Close

'PR0900 PETICI�N
SQL = "INSERT INTO PR0900 (PR09NUMPETICION, CI21CODPERSONA, AD02CODDPTO, SG02COD, "
If txtText1(1).Text = "" Then
    SQL = SQL & "PR09FECPETICION, PR09NUMGRUPO) VALUES (?, ?, ?, ?, SYSDATE, ?)"
Else
    SQL = SQL & "PR09FECPETICION, PR09NUMGRUPO, PR09DESOBSERVAC) VALUES (?, ?, ?, ?, SYSDATE, ?, ?)"
End If

Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numPetic = flngNextClaveSeq("PR09NUMPETICION")
   lngNumPet = numPetic
   QyInsert(0) = numPetic
   QyInsert(1) = lngCodPers
   QyInsert(2) = SSDBCmbDpto.Columns(0).Value
   QyInsert(3) = sg02cod
   QyInsert(4) = numPetic
   If txtText1(1).Text <> "" Then QyInsert(5) = txtText1(1).Text
QyInsert.Execute
QyInsert.Close

If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
'PR0300 ACTUACI�N PEDIDA
SQL = "INSERT INTO PR0300 (PR03NUMACTPEDI, AD02CODDPTO, CI21CODPERSONA, PR09NUMPETICION, "
SQL = SQL & "PR01CODACTUACION, PR03INDCONSFIRM, PR03INDLUNES, PR03INDMARTES, "
SQL = SQL & "PR03INDMIERCOLES, PR03INDJUEVES, PR03INDVIERNES, PR03INDSABADO, PR03INDDOMINGO,"
SQL = SQL & " PR03INDCITABLE) VALUES (?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, -1)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numActPedi = flngNextClaveSeq("PR03NUMACTPEDI") 'Se usar� m�s adelante
   QyInsert(0) = numActPedi
   QyInsert(1) = SSDBCmbDpto.Columns(0).Text
   QyInsert(2) = lngCodPers
   QyInsert(3) = numPetic
   QyInsert(4) = SSDBCmbPrueba.Columns(0).Text
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'PR0800 PETICI�N-ACTUACI�N PEDIDA
If txtText1(1).Text = "" Then
    SQL = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA) "
    SQL = SQL & " VALUES (?, ?, 1)"
Else
    SQL = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA, PR08DESOBSERV) "
    SQL = SQL & " VALUES (?, ?, 1, ?)"
End If

Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numPetic
   QyInsert(1) = numActPedi
   If txtText1(1).Text <> "" Then QyInsert(2) = txtText1(1).Text
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
txtText1(1).Text = ""
'Datos previos
If Not fblnFase(SSDBCmbPrueba.Columns(0).Text, strDesFase, lngMinutFase) Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
'PR0600 FASE PEDIDA
SQL = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC, "
SQL = SQL & "PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN) "
'SQL = SQL & "VALUES (?, 1, ?, ?, 0, 0, 0, 0)"
SQL = SQL & "VALUES (?, 1, ?, 0, 0, 0, 0, 0)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numActPedi
   QyInsert(1) = strDesFase
   'QyInsert(2) = lngMinutFase
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'Datos previos
If Not fblnRecFaseAct(SSDBCmbPrueba.Columns(0).Text, lngNumNecesid, lngTiemRec, _
                                                                       intIndPlan) Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
If Trim(SSDBCmbRecurso.Columns(0).Text) <> "" Then
    strRecurso = SSDBCmbRecurso.Columns(0).Text
Else
    objApp.rdoConnect.RollbackTrans
    Exit Function
End If
If Not fblnTipRec(intTipRec) Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
'EFS: Variable que luego utilizamos en la inserci�n de restricciones
intTipoRecur = intTipRec
'PR1400 TIPO RECURSO PEDIDO
SQL = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU, "
SQL = SQL & "AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC, PR14INDRECPREFE, "
'SQL = SQL & "PR14INDPLANIF) VALUES (?, 1, ?, ?, ?, 1, ?, 0, -1, ?)"
SQL = SQL & "PR14INDPLANIF) VALUES (?, 1, ?, ?, ?, 1, 0, 0, -1, ?)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numActPedi 'PR03NUMACTPEDI
   QyInsert(1) = lngNumNecesid 'PR14NUMNECESID
   QyInsert(2) = intTipRec 'AG14CODTIPRECU
   QyInsert(3) = SSDBCmbDpto.Columns(0).Text 'AD02CODDPTO
   'QyInsert(4) = lngTiemRec 'PR14NUMMINOCU
   QyInsert(4) = intIndPlan 'PR14INDPLANIF
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'PR0400 ACTUACI�N PLANIFICADA
If SSDBCmbDpto.Columns(0).Text = "216" Then
    SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
    SQL = SQL & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC,PR04FECENTRCOLA) "
    SQL = SQL & "VALUES (?, ?, ?, ?, 2, ?, 0,SYSDATE)"
Else
    SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
    SQL = SQL & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC) "
    SQL = SQL & "VALUES (?, ?, ?, ?, 2, ?, 0)"
End If
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numActPlan = flngNextClaveSeq("PR04NUMACTPLAN")
   lngNumActPlan = numActPlan
   QyInsert(0) = numActPlan
   QyInsert(1) = SSDBCmbPrueba.Columns(0).Text
   QyInsert(2) = SSDBCmbDpto.Columns(0).Text 'AD02CODDPTO
   QyInsert(3) = lngCodPers
   QyInsert(4) = numActPedi
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'CI3100 SOLICITUD
SQL = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31FECPREFDES, CI31FECPREFHAS, CI31INDLUNPREF, "
SQL = SQL & "CI31INDMARPREF, CI31INDMIEPREF, CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF,"
SQL = SQL & " CI31INDDOMPREF) VALUES (?, SYSDATE, SYSDATE + 15,  1, 1, 1, 1, 1, 1, 1)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numSolicit = flngNextClave("CI31NUMSOLICIT", "CI3100")
   QyInsert(0) = numSolicit
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   If Err = 40002 Then
      Err = 0
      SQL = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31FECPREFDES, CI31FECPREFHAS, CI31INDLUNPREF, "
      SQL = SQL & "CI31INDMARPREF, CI31INDMIEPREF, CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF,"
      SQL = SQL & " CI31INDDOMPREF) VALUES (?, SYSDATE, SYSDATE + 15,  1, 1, 1, 1, 1, 1, 1)"
      Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
         numSolicit = flngNextClave("CI31NUMSOLICIT", "CI3100")
         QyInsert(0) = numSolicit
      QyInsert.Execute
      QyInsert.Close
      If Err > 0 Then
         objApp.rdoConnect.RollbackTrans
         Exit Function
      End If
   Else
      objApp.rdoConnect.RollbackTrans
      Exit Function
   End If
End If
If SSDCFecha.Text <> "" And txtHora(0) <> "" And txtMinuto(0) <> "" Then
    fecCita = SSDCFecha.Text & " " & txtHora(0) & ":" & txtMinuto(0)
Else
    fecCita = strFechaHora_Sistema
End If

'CI0100 ACTUACI�N CITADA
SQL = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN, "
SQL = SQL & "CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO, "
SQL = SQL & "CI01INDLISESPE, CI01INDASIG, AD13CODTIPOCAMA) "
SQL = SQL & "VALUES (?, 1, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), 1, -1, 1, 1, 0, 0, ?)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
    QyInsert(0) = numSolicit
    QyInsert(1) = strRecurso
    QyInsert(2) = numActPlan
    QyInsert(3) = objGen.ReplaceStr(Format(fecCita, "DD/MM/YYYY HH:MM:SS"), ".", ":", 0)
    'Grabar el tipo de cama
    If SSDBCmbTipoCama.Columns(0).Value <> "" Then
        QyInsert(4) = SSDBCmbTipoCama.Columns(0).Value
    Else
        QyInsert(4) = Null
    End If
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'CI1500 FASE CITADA
Call pConvertMinutos(lngMinutFase, intDias, bytHoras, bytMinutos)
SQL = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, CI15DESFASECITA, "
SQL = SQL & "CI15FECCONCPAC, CI15NUMDIASPAC, CI15NUMHORAPAC, CI15NUMMINUPAC) "
SQL = SQL & "VALUES (?, 1, 1, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numSolicit
   QyInsert(1) = strDesFase
   QyInsert(2) = objGen.ReplaceStr(Format(fecCita, "DD/MM/YYYY HH:MM:SS"), ".", ":", 0)
   QyInsert(3) = intDias
   QyInsert(4) = bytHoras
   QyInsert(5) = bytMinutos
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'CI2700 RECURSO CITADO
Call pConvertMinutos(lngTiemRec, intDias, bytHoras, bytMinutos)
SQL = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO, "
SQL = SQL & "CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC) "
SQL = SQL & "VALUES (?, 1, 1, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numSolicit
   QyInsert(1) = strRecurso
   QyInsert(2) = objGen.ReplaceStr(Format(fecCita, "DD/MM/YYYY HH:MM:SS"), ".", ":", 0)
   QyInsert(3) = intDias
   QyInsert(4) = bytHoras
   QyInsert(5) = bytMinutos
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

objApp.rdoConnect.CommitTrans
fblnCitar = True

End Function
Private Function blnInsert_Restri() As Boolean
   Dim SQL As String
   Dim QyConsul As rdoQuery
   Dim QyInsert As rdoQuery
   Dim RsRespuesta As rdoResultset
'valor de las restrinciones
   Dim vntRest As Variant
   Dim lngProceso As Long
   Dim lngAsistencia As Long
On Error Resume Next
'Datos previos
'Buscamos el proceso asistencia
SQL = "SELECT AD01CODASISTENCI,AD07CODPROCESO FROM PR0400 WHERE PR04NUMACTPLAN = ? "
Set QyConsul = objApp.rdoConnect.CreateQuery("", SQL)
 QyConsul(0) = lngNumActPlan
Set RsRespuesta = QyConsul.OpenResultset()
'si hay proceso/asistencia lo pasamos a variables
If Not RsRespuesta.EOF Then
    If Not IsNull(RsRespuesta(0).Value) Then
        lngAsistencia = RsRespuesta(0).Value
    Else
        lngAsistencia = 0
    End If
    If Not IsNull(RsRespuesta(1).Value) Then
        lngProceso = RsRespuesta(1).Value
    Else
        lngProceso = 0
    End If
End If

'RESTRINCIONES: Miramos si el recurso tiene alguna restrincion por
'franja y si es asi guardamos el valor.
SQL = "SELECT AG16CODTIPREST FROM AG1500 WHERE AG14CODTIPRECU = ?"
Set QyConsul = objApp.rdoConnect.CreateQuery("", SQL)
   QyConsul(0) = intTipoRecur
Set RsRespuesta = QyConsul.OpenResultset()
If Not RsRespuesta.EOF Then
    objApp.BeginTrans
    Do While Not RsRespuesta.EOF
        vntRest = fValorRest(RsRespuesta(0).Value, lngProceso, lngAsistencia)
        If vntRest <> " " Then
            SQL = "INSERT INTO PR4700 (PR09NUMPETICION,AG16CODTIPREST,PR47VALOR) "
            SQL = SQL & "VALUES (?,?,?)"
            Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
               QyInsert(0) = lngNumPet
               QyInsert(1) = RsRespuesta(0).Value
               QyInsert(2) = vntRest
            QyInsert.Execute
            QyInsert.Close
            If Err > 0 Then
               objApp.rdoConnect.RollbackTrans
               Exit Function
            End If
        End If 'vntRest <>
    RsRespuesta.MoveNext
    Loop 'resRespuesta.eof
    objApp.CommitTrans
End If 'resRespuesta.eof
blnInsert_Restri = True
QyConsul.Close
End Function
'Devuelve el siguiente c�digo a utilizar en un registro nuevo utilizando los SEQUENCE
'Si se produce alg�n error, devuelve -1
Public Function flngNextClaveSeq(strCampo As String) As Long
    
Dim rstMaxClave As rdoResultset

On Error Resume Next
Err = 0
Set rstMaxClave = objApp.rdoConnect.OpenResultset("SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL")
flngNextClaveSeq = rstMaxClave(0)
rstMaxClave.Close
Set rstMaxClave = Nothing
If Err > 0 Then flngNextClaveSeq = -1
    
End Function

'HALLAR LA DESCRIPCI�N Y DURACI�N DE LA FASE QUE SE QUIERE CITAR
Private Function fblnFase(lngCodActuacion As Long, strDesFase As String, _
                           lngMinutFase As Long) As Boolean
Dim SQL As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset

SQL = "SELECT PR05DESFASE, PR05NUMOCUPACI FROM PR0500 "
SQL = SQL & "WHERE PR01CODACTUACION = ? AND PR05NUMFASE = 1"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = lngCodActuacion
Set RsRespuesta = QyInsert.OpenResultset()
If RsRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   strDesFase = RsRespuesta(0)
   lngMinutFase = RsRespuesta(1)
   RsRespuesta.MoveNext
   If Not RsRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
RsRespuesta.Close
QyInsert.Close
Set RsRespuesta = Nothing
Set QyInsert = Nothing
fblnFase = True

End Function

Private Function fblnTipRec(intTipRec As Integer) As Boolean
Dim SQL As String
Dim qryInsert As rdoQuery
Dim rstRespuesta As rdoResultset

'Se obtiene el Tipo de Recurso al que pertenece al Doctor
SQL = "SELECT AG14CODTIPRECU FROM AG1100 WHERE AG11CODRECURSO = ?"
Set qryInsert = objApp.rdoConnect.CreateQuery("", SQL)
   qryInsert(0) = strRecurso
Set rstRespuesta = qryInsert.OpenResultset()
If rstRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   intTipRec = rstRespuesta(0)
   rstRespuesta.MoveNext
   If Not rstRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
rstRespuesta.Close
qryInsert.Close
Set rstRespuesta = Nothing
Set qryInsert = Nothing
fblnTipRec = True

End Function

'Devuelve el siguiente valor que se puede usar de un campo clave que no dispone de SEQUENCE
'Si se produce alg�n error, devuelve -1
Public Function flngNextClave(strCampo As String, strTabla As String) As Long

Dim rstClave As rdoResultset

On Error Resume Next
Err = 0
Set rstClave = objApp.rdoConnect.OpenResultset("SELECT MAX(" & strCampo & ") + 1 FROM " & strTabla)
If IsNull(rstClave(0)) Then flngNextClave = 1 Else flngNextClave = rstClave(0)
rstClave.Close
Set rstClave = Nothing
If Err > 0 Then flngNextClave = -1

End Function

Private Sub pConvertMinutos(lngMin As Long, intDias As Integer, bytHoras As Byte, _
                                 bytMinutos As Byte)
Dim lngMinutosRestSinHoras As Long

intDias = Int(lngMin / 60 / 24)
lngMinutosRestSinHoras = lngMin - intDias * 24
bytHoras = Int(lngMinutosRestSinHoras / 60)
bytMinutos = lngMinutosRestSinHoras - bytHoras * 60

End Sub

Private Function fblnRecFaseAct(lngCodActuacion As Long, lngNumNecesid As Long, _
                                 lngTiemRec As Long, intIndPlan As Integer) As Boolean
Dim SQL As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset

SQL = "SELECT PR13NUMNECESID, PR13NUMTIEMPREC, PR13INDPLANIF FROM PR1300 WHERE "
SQL = SQL & "PR01CODACTUACION = ? AND PR13INDPREFEREN = -1 AND PR13INDPLANIF = -1"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = lngCodActuacion
Set RsRespuesta = QyInsert.OpenResultset()
If RsRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   lngNumNecesid = RsRespuesta(0)
   lngTiemRec = RsRespuesta(1)
   intIndPlan = RsRespuesta(2)
   RsRespuesta.MoveNext
   If Not RsRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
RsRespuesta.Close
QyInsert.Close
Set RsRespuesta = Nothing
Set QyInsert = Nothing
fblnRecFaseAct = True

End Function
Private Function flngHistoria(lngCodPers As Long) As Long

Dim SQL As String
Dim qryHist As rdoQuery
Dim rstHist As rdoResultset

SQL = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = ?"
Set qryHist = objApp.rdoConnect.CreateQuery("Historia", SQL)
   qryHist(0) = lngCodPers
Set rstHist = qryHist.OpenResultset
If Not rstHist.EOF Then If Not IsNull(rstHist(0)) Then flngHistoria = rstHist(0)
rstHist.Close
qryHist.Close

End Function

Private Sub pCitar()
   txtActual.Text = "Citando..."
   If fblnCitar Then 'Citar
      txtActual.Text = "Citado"
      'Asociar Proceso-Asistencia *****- SE QUITAR� CUANDO DESAPAREZCA EL IBM -*****
      ReDim VNTDATA(2) As Variant
      VNTDATA(1) = lngCodPers
      VNTDATA(2) = 2 'Viene de Citas
      Screen.MousePointer = vbDefault
      Call objSecurity.LaunchProcess("AD0105", VNTDATA)
      If Not blnInsert_Restri Then
        MsgBox "Ha habido errores al asociar las restrinciones", vbCritical
      End If
   Else
      MsgBox "Se ha producido un error al citar. Por favor, int�ntelo de nuevo", _
               vbCritical
   End If 'fblnCitar
    
End Sub
Private Sub pCitas_Urgencias()
Dim lngHistoria As Long
Dim intMsgBox As Integer
Dim intContador As Integer

         'Screen.MousePointer = vbHourglass
         'tvwHuecos.Enabled = False
         'Call objSecurity.LaunchProcess("CI1017") 'Llamar a Mant. Personas F�sicas
         'Screen.MousePointer = vbHourglass
         If lngCodPers <> 0 Then 'Si se ha seleccionado una persona...
            If MsgBox("�Quiere realizar la citaci�n?", vbYesNo, "Citaci�n") = vbYes Then
               FechaActual = SSDCFecha.Text
              ' txtActual.Visible = True
               lngHistoria = flngHistoria(lngCodPers)
               If lngHistoria > 0 Then
                  strRecurso = SSDBCmbRecurso.Columns(0).Text
                  Call pCitar
               Else
                  intMsgBox = MsgBox("Si ha chequeado la casilla 'Paciente' en la pantalla de 'Personas F�sicas', pulse reintentar", vbRetryCancel, "El paciente " & lngCodPers & " no tiene todav�a n�mero de historia")
                '  txtActual.Text = "Esperando el n� de historia"
                  If intMsgBox = 4 Then
                     Do While lngHistoria = 0
                        DoEvents
                        lngHistoria = flngHistoria(lngCodPers)
                        intContador = intContador + 1
                        If intContador > 200 Then Exit Do
                     Loop 'lngHistoria = 0
                     If lngHistoria > 0 Then Call pCitar Else MsgBox "C�digo de persona = " & lngCodPers, vbInformation, "Int�ntelo m�s tarde"
                  End If 'intMsgBox = 5
               End If 'lngHistoria
            End If 'MsgBox
         End If 'lngCodPers <> 0
        Screen.MousePointer = Default
        
End Sub
Private Function fValorRest(lngCodRest As Long, proceso As Long, asistencia As Long) As Variant
    Dim strSelect As String
    Dim rstSelect As rdoResultset
    
    Select Case lngCodRest
      Case lngCodResUsu
        fValorRest = objSecurity.strUser
      Case lngTipoPac
        If proceso <> 0 And asistencia <> 0 Then
            strSelect = "select AD10DESTIPPACIEN from ad1000 where AD10CODTIPPACIEN in " & _
                        " (select AD10CODTIPPACIEN from ad0800 where AD07CODPROCESO=" & _
                        proceso & " and AD01CODASISTENCI=" & asistencia & ")"
            Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
            If rstSelect.EOF = False Then
                fValorRest = rstSelect(0).Value
            Else
                fValorRest = " "
            End If
        Else
           fValorRest = " "
        End If
      Case lngTipoEco
        If proceso <> 0 And asistencia <> 0 Then
            strSelect = "select CI32DESTIPECON from ci3200 where CI32CODTIPECON in " & _
                        " (select CI32CODTIPECON from ad1100 where AD07CODPROCESO=" & _
                        proceso & " and AD01CODASISTENCI=" & asistencia & ")"
            Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
            If rstSelect.EOF = False Then
                fValorRest = rstSelect(0).Value
            Else
                fValorRest = " "
            End If
        Else
            fValorRest = " "
        End If
      Case lngEdad
        strSelect = "select CI22FECNACIM from ci2200 where CI21CODPERSONA=" & _
                    lngCodPers
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        If IsNull(rstSelect(0).Value) = False Then
            fValorRest = DateDiff("d", rstSelect(0).Value, Date) \ 365
        Else
            fValorRest = " "
        End If
      Case lngSexo
        strSelect = "select CI30DESSEXO from ci3000 where CI30CODSEXO in " & _
                    " (select CI30CODSEXO from ci2200 where CI21CODPERSONA=" & txtText1(0).Text & ")"
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        If rstSelect.EOF = False Then
            fValorRest = rstSelect(0).Value
        Else
            fValorRest = " "
        End If
'''      Case lngDpto
'''        fValorRest = frmpedir.txtText1(1).Text
      Case Else
        fValorRest = " "
    End Select
End Function

Private Sub pCargar_ProcesoAsistencia() 'Carga  datos de proceso-asistencia

'Cargar por pantalla el ultimo Proceso/Asistencia
'con hospitalizaci�n (TipoEconomico, Entidad y Tipo de cama, cama) - preferente
'Sin hospitalizaci�n (TipoEconomico, Entidad)
'Para el paciente que desea la cita de ingreso

Dim sStmSql     As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset
Dim bExiste     As Boolean
'�Tiene alguna asistencia con hospitalizaci�n?
sStmSql = "SELECT "
sStmSql = sStmSql & " 1 FROM AD1100, AD0100, AD2500 "
sStmSql = sStmSql & "WHERE AD1100.CI21CODPERSONA = " & lngCodPers & " "
sStmSql = sStmSql & "AND AD1100.AD01CODASISTENCI = AD0100.AD01CODASISTENCI "
sStmSql = sStmSql & "AND AD1100.CI21CODPERSONA = AD0100.CI21CODPERSONA "
sStmSql = sStmSql & "AND AD2500.AD01CODASISTENCI = AD0100.AD01CODASISTENCI "
sStmSql = sStmSql & "AND AD2500.AD12CODTIPOASIST = 1 "

Set qryConsulta = objApp.rdoConnect.CreateQuery("", sStmSql)
Set rstConsulta = qryConsulta.OpenResultset()
If rstConsulta.EOF Then
   bExiste = False
Else
   bExiste = True
End If
rstConsulta.Close
qryConsulta.Close

If bExiste Then
    sStmSql = "SELECT "
    sStmSql = sStmSql & "AD1100.CI32CODTIPECON, "
    sStmSql = sStmSql & "AD1100.CI13CODENTIDAD, "
    sStmSql = sStmSql & "AD1500.AD13CODTIPOCAMA, "
    sStmSql = sStmSql & "AD1500.AD15CODCAMA "
    sStmSql = sStmSql & "FROM AD1100, AD1500, AD0100, AD2500 "
    sStmSql = sStmSql & "WHERE AD1100.CI21CODPERSONA = " & lngCodPers & " "
    sStmSql = sStmSql & "AND AD1100.AD01CODASISTENCI = AD0100.AD01CODASISTENCI "
    sStmSql = sStmSql & "AND AD1100.CI21CODPERSONA = AD0100.CI21CODPERSONA "
    sStmSql = sStmSql & "AND AD0100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI "
    sStmSql = sStmSql & "AND AD2500.AD12CODTIPOASIST = 1 "
    sStmSql = sStmSql & "AND AD1100.AD01CODASISTENCI = AD1500.AD01CODASISTENCI "
    sStmSql = sStmSql & "AND AD1100.AD07CODPROCESO = AD1500.AD07CODPROCESO "
    sStmSql = sStmSql & "ORDER BY AD1100.AD11FECFIN DESC "
        
    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sStmSql)
    Set rstConsulta = qryConsulta.OpenResultset()
    'Solo recuperamos el primero
    If Not rstConsulta.EOF Then
        LBLTIPECO.Caption = rstConsulta(0)
        LBLENTID.Caption = rstConsulta(1)
        LBLTIPCAMA.Caption = rstConsulta(2)
        LBLCAMA.Caption = rstConsulta(3)
    Else
        LBLTIPECO.Caption = ""
        LBLENTID.Caption = ""
        LBLTIPCAMA.Caption = ""
        LBLCAMA.Caption = ""
    End If
    rstConsulta.Close
    qryConsulta.Close
Else
    sStmSql = "SELECT "
    sStmSql = sStmSql & "AD1100.CI32CODTIPECON, "
    sStmSql = sStmSql & "AD1100.CI13CODENTIDAD "
    sStmSql = sStmSql & "FROM AD1100, AD0100 "
    sStmSql = sStmSql & "WHERE AD1100.CI21CODPERSONA = " & lngCodPers & " "
    sStmSql = sStmSql & "AND AD1100.AD01CODASISTENCI = AD0100.AD01CODASISTENCI "
    sStmSql = sStmSql & "AND AD1100.CI21CODPERSONA = AD0100.CI21CODPERSONA "
    sStmSql = sStmSql & "ORDER BY AD1100.AD11FECFIN DESC"
    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sStmSql)
    Set rstConsulta = qryConsulta.OpenResultset()
    'Solo recuperamos el primero
    If Not rstConsulta.EOF Then
        LBLTIPECO.Caption = rstConsulta(0)
        LBLENTID.Caption = rstConsulta(1)
        LBLTIPCAMA.Caption = ""
        LBLCAMA.Caption = ""
    Else
        LBLTIPECO.Caption = ""
        LBLENTID.Caption = ""
        LBLTIPCAMA.Caption = ""
        LBLCAMA.Caption = ""
    End If
    rstConsulta.Close
    qryConsulta.Close
End If
    
End Sub

Private Sub SSDCFecha_Change()
    'Si esta rellena la fecha, Dpto y Prueba
    'Se habilitar� el bot�n para citar el ingreso
    If SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" Then
        Command1.Enabled = True
    Else
        Command1.Enabled = False
    End If
End Sub

Private Sub txtHora_Change(Index As Integer)
 If Len(txtHora(Index)) = 2 Then
        If CInt(txtHora(Index)) < 0 Or CInt(txtHora(Index)) > 23 Then
            MsgBox "El rango para las horas es de 0 a 23", vbInformation
            txtHora(Index) = "16"
        End If
    End If
End Sub

Private Sub txtHora_LostFocus(Index As Integer)
    If Len(txtHora(Index)) < 2 Then
        txtHora(Index) = Format(txtHora(Index), "00")
    End If
End Sub

Private Sub txtMinuto_Change(Index As Integer)
 If Len(txtMinuto(Index)) = 2 Then
        If CInt(txtMinuto(Index)) < 0 Or CInt(txtMinuto(Index)) > 59 Then
            MsgBox "El rango para los minutos es de 0 a 59", vbInformation
            txtMinuto(Index) = "00"
        End If
    End If
End Sub

Private Sub UpDownHora_DownClick(Index As Integer)

    If txtHora(Index) > UpDownHora(Index).Max Then
        txtHora(Index) = UpDownHora(Index).Max
    ElseIf CInt(txtHora(Index)) <= UpDownHora(Index).Min Then
        txtHora(Index) = UpDownHora(Index).Min
    Else
        txtHora(Index) = txtHora(Index) - UpDownHora(Index).Increment
    End If
    If Len(txtHora(Index)) < 2 Then
        txtHora(Index) = Format(txtHora(Index), "00")
    End If
    
End Sub
Private Sub UpDownHora_UpClick(Index As Integer)
    
    If txtHora(Index) >= UpDownHora(Index).Max Then
        txtHora(Index) = UpDownHora(Index).Max
    ElseIf txtMinuto(Index) < UpDownHora(Index).Min Then
        txtHora(Index) = UpDownHora(Index).Min
    Else
        txtHora(Index) = txtHora(Index) + UpDownHora(Index).Increment
    End If
    If Len(txtHora(Index)) < 2 Then
        txtHora(Index) = Format(txtHora(Index), "00")
    End If
    
End Sub

Private Sub UpDownMinuto_DownClick(Index As Integer)
    If txtMinuto(Index) > UpDownMinuto(Index).Max Then
        txtMinuto(Index) = UpDownMinuto(Index).Max
    ElseIf txtMinuto(Index) <= UpDownMinuto(Index).Min Then
        txtMinuto(Index) = UpDownMinuto(Index).Min
    Else
        txtMinuto(Index) = txtMinuto(Index) - UpDownMinuto(Index).Increment
    End If
    If Len(txtMinuto(Index)) < 2 Then
        txtMinuto(Index) = Format(txtMinuto(Index), "00")
    End If
End Sub
Private Sub UpDownMinuto_UpClick(Index As Integer)
       
    If txtMinuto(Index) >= UpDownMinuto(Index).Max Then
        txtMinuto(Index) = UpDownMinuto(Index).Max
    ElseIf CInt(txtMinuto(Index)) < UpDownMinuto(Index).Min Then
        txtMinuto(Index) = UpDownMinuto(Index).Min
    Else
        txtMinuto(Index) = txtMinuto(Index) + UpDownMinuto(Index).Increment
    End If
    If Len(txtMinuto(Index)) < 2 Then
        txtMinuto(Index) = Format(txtMinuto(Index), "00")
    End If
End Sub
Private Sub pCargar_TipoCama()  'Carga los combos de Tipo de cama deseada

'Consultas SQL
Dim SQL As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset

'Inicilizar los ComboBox de Pruebas y Recursos

SSDBCmbTipoCama.Text = ""
SSDBCmbTipoCama.RemoveAll

'Tipos de Cama
SQL = "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 "
SQL = SQL + "ORDER BY AD13CODTIPOCAMA "
Set qryConsulta = objApp.rdoConnect.CreateQuery("", SQL)
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbTipoCama.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
SSDBCmbTipoCama.AddItem ";"
rstConsulta.Close
qryConsulta.Close

End Sub
