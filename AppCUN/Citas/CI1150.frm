VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmCitaManual2FI 
   Caption         =   "Citaci�n Manual"
   ClientHeight    =   5745
   ClientLeft      =   2115
   ClientTop       =   1470
   ClientWidth     =   7410
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   5745
   ScaleWidth      =   7410
   Begin VB.TextBox txtObservaciones 
      Height          =   735
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   16
      Top             =   2400
      Width           =   6975
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   7155
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         Height          =   375
         Left            =   6060
         TabIndex        =   14
         Top             =   720
         Width           =   915
      End
      Begin VB.TextBox txtPac 
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   300
         Width           =   4815
      End
      Begin VB.TextBox txtAct 
         Height          =   285
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   780
         Width           =   3615
      End
      Begin VB.TextBox txtNH 
         Height          =   285
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   300
         Width           =   915
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   300
         TabIndex        =   13
         Top             =   360
         Width           =   765
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   12
         Top             =   840
         Width           =   870
      End
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Left            =   6360
      TabIndex        =   3
      Top             =   1740
      Width           =   915
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecha 
      Height          =   315
      Left            =   3960
      TabIndex        =   1
      Top             =   1740
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShowCentury     =   -1  'True
      StartofWeek     =   2
   End
   Begin SSDataWidgets_B.SSDBGrid grdCitas 
      Height          =   2385
      Left            =   120
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   3300
      Width           =   7125
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   3
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   3
      Columns(0).Width=   2990
      Columns(0).Caption=   "Fecha "
      Columns(0).Name =   "Fecha"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   7
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   6376
      Columns(1).Caption=   "Actuaci�n"
      Columns(1).Name =   "Descripci�n Actuaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   8811
      Columns(2).Caption=   "Observaciones"
      Columns(2).Name =   "Observaciones"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   12568
      _ExtentY        =   4207
      _StockProps     =   79
      Caption         =   "Citas Pendientes del Paciente"
   End
   Begin MSMask.MaskEdBox mskHora 
      Height          =   315
      Left            =   5640
      TabIndex        =   2
      Top             =   1740
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   556
      _Version        =   327681
      BackColor       =   16776960
      MaxLength       =   5
      Mask            =   "##:##"
      PromptChar      =   "_"
   End
   Begin SSDataWidgets_B.SSDBCombo cboRecursos 
      Height          =   315
      Left            =   240
      TabIndex        =   0
      Top             =   1740
      Width           =   3675
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7144
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6482
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Observaciones:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   240
      TabIndex        =   15
      Top             =   2160
      Width           =   1335
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Recurso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   3
      Left            =   240
      TabIndex        =   4
      Top             =   1500
      Width           =   780
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hora:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   5640
      TabIndex        =   7
      Top             =   1500
      Width           =   480
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   3960
      TabIndex        =   6
      Top             =   1500
      Width           =   600
   End
End
Attribute VB_Name = "frmCitaManual2FI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strAhora$, strFechaCita$
Dim lngNAPlan&, lngCodDpto&, lngCodAct&, lngCodActiv&, strFecInicial$
Private Sub pCambiarRecurso()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim lngDptoAnt As Long
Dim strUsuarioAnt As String
Dim strUsuarioNuevo As String
Dim lngDptoNW As Long
Dim rsSGcod As rdoResultset
sql = "SELECT AD07CODPROCESO,AD01CODASISTENCI,PR12CODACTIVIDAD FROM PR0400,PR0100 "
sql = sql & " WHERE PR04NUMACTPLAN=?"
sql = sql & " AND PR0100.PR01CODACTUACION=PR0400.PR01CODACTUACION"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngNAPlan
Set rs = qry.OpenResultset
If Not rs.EOF Then
    If IsNull(rs(0)) Or IsNull(rs(1)) Then
        Exit Sub
    End If
    If rs!PR12CODACTIVIDAD = constACTIV_HOSPITALIZACION Then
        'MIRAMOS A VER SI SE HA CAMBIADO EL RESPONSABLE
        sql = "SELECT SG02COD,AD02CODDPTO FROM AG1100 WHERE"
        sql = sql & " AG11CODRECUrsO=?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = cboRecursos.Columns(0).Value
        Set rsSGcod = qry.OpenResultset
        If Not rsSGcod Then
            strUsuarioNuevo = rsSGcod!sg02cod
            lngDptoNW = rsSGcod!AD02CODDPTO
        End If
        rsSGcod.Close

        sql = "SELECT SG02COD,AD02CODDPTO FROM AD0500"
        sql = sql & " WHERE AD07CODPROCESO=? AND AD01CODASISTENCI=? "
        sql = sql & " AND AD05FECFINRESPON IS NULL"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = rs!AD07CODPROCESO
        qry(1) = rs!AD01CODASISTENCI
        Set rsSGcod = qry.OpenResultset
        If Not rsSGcod Then
            strUsuarioAnt = rsSGcod!sg02cod
            lngDptoAnt = rsSGcod!AD02CODDPTO
        End If
        rsSGcod.Close
       
        If strUsuarioAnt <> strUsuarioNuevo Then 'HA HABIDO UN CAMBIO DE RESPONSABILIDAD
            sql = "UPDATE AD0500 SET AD05FECFINRESPON=TO_DATE(?,'DD/MM/YYYY HH24:MI') "
            sql = sql & "WHERE AD07CODPROCESO=? AND AD01CODASISTENCI=? "
            sql = sql & " AND AD05FECFINRESPON IS NULL"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = Format(strFechaHora_Sistema, "dd/mm/yyyy hh:mm")
            qry(1) = rs!AD07CODPROCESO
            qry(2) = rs!AD01CODASISTENCI
            qry.Execute
            If qry.RowsAffected <> 1 Then
                GoTo ControlError
            End If
      
            sql = "INSERT INTO AD0500 (AD05FECINIRESPON,"
            sql = sql & " AD07CODPROCESO,AD01CODASISTENCI, "
            sql = sql & " SG02COD,AD02CODDPTO)"
            sql = sql & " VALUES(TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?,?,?) "
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = Format(strFechaHora_Sistema, "dd/mm/yyyy hh:mm")
            qry(1) = rs!AD07CODPROCESO
            qry(2) = rs!AD01CODASISTENCI
            qry(3) = strUsuarioNuevo
            qry(4) = lngDptoNW
            qry.Execute
            If qry.RowsAffected <> 1 Then
                GoTo ControlError
            End If
            qry.Close
        End If
        
    End If
End If
Exit Sub
ControlError:
    MsgBox "Ha ocurrido un error al actualizar el nuevo Responsable", vbCritical, Me.Caption
End Sub

Private Sub cmdCitar_Click()
    cmdCitar.Enabled = False
    If Not fComprobaciones Then cmdCitar.Enabled = True: Exit Sub
  
    If fblnCitar Then
        MsgBox "Cita realizada correctamente.", vbInformation, Me.Caption
        Unload Me
    Else
        MsgBox "Se ha producido un error al citar. Por favor, int�ntelo de nuevo.", vbExclamation, Me.Caption
        cmdCitar.Enabled = True
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
        
    lngNAPlan = objPipe.PipeGet("CI1150_PR04NUMACTPLAN")
    Call objPipe.PipeRemove("CI1150_PR04NUMACTPLAN")
    
    Call pCargarDatos
    
    'Si la fecha es nula, mostramos el d�a de hoy. Si no, la fecha preferente
    strAhora = strFechaHora_Sistema 'se utiliza tambi�n para comprobaciones posteriores
    If strFecInicial <> "" Then
        If CDate(strFecInicial) >= CDate(strAhora) Then
            dcboFecha.Text = Format(strFecInicial, "dd/mm/yyyy")
            mskHora = Format(strFecInicial, "hh:mm")
        Else
            dcboFecha.Text = Format(strAhora, "dd/mm/yyyy")
            mskHora = Format(strAhora, "hh:mm")
        End If
    Else
        dcboFecha.Text = Format(strAhora, "dd/mm/yyyy")
        mskHora = Format(strAhora, "hh:mm")
    End If
    Call pOverbooking
    cboRecursos.BackColor = objApp.objUserColor.lngMandatory
    dcboFecha.BackColor = objApp.objUserColor.lngMandatory
    'dcboFecha.MinDate = Format(strAhora, "dd/mm/yyyy")
    mskHora.BackColor = objApp.objUserColor.lngMandatory
    txtNH.BackColor = objApp.objUserColor.lngReadOnly
    txtPac.BackColor = objApp.objUserColor.lngReadOnly
    txtAct.BackColor = objApp.objUserColor.lngReadOnly
    grdCitas.BackColorEven = objApp.objUserColor.lngReadOnly
    grdCitas.BackColorOdd = objApp.objUserColor.lngReadOnly
End Sub

Public Function fblnCitar() As Boolean
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strRecurso$, strNumSolicit$, intNCitas%
    Dim lngTipCama As Long
    Dim strCodCama As String
    
    'datos
    strRecurso = cboRecursos.Columns(0).Value

    'se mira si es una recita: hay alguna cita o est� pendiente de recita se tratar� como _
    una recita; en caso contrario ser� una cita
    sql = "SELECT COUNT(CI31NUMSOLICIT)"
    sql = sql & " FROM CI0100"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
    sql = sql & " OR CI01SITCITA = '" & constESTCITA_PENDRECITAR & "')"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    intNCitas = rs(0)
    rs.Close
    qry.Close

    On Error Resume Next
    objApp.BeginTrans

    If intNCitas > 0 Then 'RECITAR
        'CI0100: estado de la cita
        Call pCambiarRecurso
        sql = "SELECT AD13CODTIPOCAMA,AD15CODCAMA FROM CI0100"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        sql = sql & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
        sql = sql & " OR CI01SITCITA = '" & constESTCITA_PENDRECITAR & "')"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNAPlan
        Set rs = qry.OpenResultset
        If Not IsNull(rs!AD13CODTIPOCAMA) Then
            lngTipCama = rs(0)
        End If
        If Not IsNull(rs!AD15CODCAMA) Then
            strCodCama = rs!AD15CODCAMA
        End If
            
        sql = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_RECITADA & "'"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        sql = sql & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
        sql = sql & " OR CI01SITCITA = '" & constESTCITA_PENDRECITAR & "')"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNAPlan
        qry.Execute
        qry.Close
        
        If Err > 0 Then
            objApp.RollbackTrans
            MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
            Exit Function
        End If
    End If

    'PR0400: estado de la actuaci�n
    If lngCodDpto = constDPTO_RAYOS _
        And CDate(strFechaCita) <= CDate(DateAdd("n", 10, strAhora)) _
        And lngCodActiv <> constACTIV_HOSPITALIZACION Then
        'Para Rayos, si se cita para antes de 10 min., se anota tambi�n la entrada en cola
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CITADA & ","
        sql = sql & " PR04FECENTRCOLA = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
        qry(1) = lngNAPlan
    Else
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CITADA
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNAPlan
    End If
    qry.Execute
    qry.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
        Exit Function
    End If
    
    strNumSolicit = fNextClave("CI31NUMSOLICIT")
    
    'CI3100: a�adir la solicitud
    sql = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31INDLUNPREF, CI31INDMARPREF, CI31INDMIEPREF,"
    sql = sql & " CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF, CI31INDDOMPREF)"
    sql = sql & " VALUES (?, 1, 1, 1, 1, 1, 1, 1)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
   
    qry.Execute
    qry.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
        Exit Function
    End If
    
    'CI0100: a�adir cita
    sql = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN,"
    sql = sql & " CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO,"
    sql = sql & " CI01INDLISESPE, CI01INDASIG,CI01OBSERV,AD13CODTIPOCAMA,AD15CODCAMA)"
    sql = sql & " VALUES (?, 1, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, -1, 1, 1, 0, 0,?,?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
    qry(1) = strRecurso
    qry(2) = lngNAPlan
    qry(3) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(4) = constESTCITA_CITADA
    If Trim(txtobservaciones.Text) <> "" Then
        qry(5) = txtobservaciones.Text
    Else
        qry(5) = Null
    End If
    If lngTipCama = 0 Then
        qry(6) = Null
    Else
        qry(6) = lngTipCama
    End If
    If strCodCama = "" Then
        qry(7) = Null
    Else
        qry(7) = strCodCama
    End If
    qry.Execute
    qry.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
        Exit Function
    End If

    'CI1500: a�adir fase citada
    sql = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15FECCONCPAC, CI15NUMFASECITA,"
    sql = sql & " CI15DESFASECITA, CI15NUMDIASPAC, CI15NUMHORAPAC,"
    sql = sql & " CI15NUMMINUPAC)"
    sql = sql & " SELECT ?, 1, TO_DATE(?,'DD/MM/YYYY HH24:MI'), PR05NUMFASE,"
    sql = sql & " PR05DESFASE, FLOOR(PR05NUMOCUPACI/1440), FLOOR(MOD(PR05NUMOCUPACI,1440)/60),"
    sql = sql & " MOD(MOD(PR05NUMOCUPACI,1440),60)"
    sql = sql & " FROM PR0500"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
    qry(1) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(2) = lngCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
        Exit Function
    End If

    'CI2700: A�adir recurso citado
    sql = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO,"
    sql = sql & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC)"
    sql = sql & " SELECT ?, 1, PR05NUMFASE, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'),"
    sql = sql & " FLOOR(PR13NUMTIEMPREC/1440), FLOOR(MOD(PR13NUMTIEMPREC,1440)/60),"
    sql = sql & " MOD(MOD(PR13NUMTIEMPREC,1440),60)"
    sql = sql & " FROM PR1300"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    sql = sql & " AND PR13INDPREFEREN = -1"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
    qry(1) = strRecurso
    qry(2) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(3) = lngCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
        Exit Function
    End If

    'Se anota la nueva fecha de planificaci�n de las pruebas asociadas
    Call pAsociadas(lngNAPlan, strFechaCita)
    'Si se trata de una intervenci�n, se citan las intervenciones asociadas
    If lngCodActiv = constACTIV_INTERVENCION Then
        If Not fCitarIntervAsociadas(strNumSolicit, strRecurso, strFechaCita) Then
            objApp.RollbackTrans
            MsgBox "Se ha producido un error al realizar la cita." & Chr$(13) & Chr$(13) & Error, vbExclamation, Me.Caption
            Exit Function
        End If
    End If
    
    objApp.CommitTrans
    fblnCitar = True
    On Error GoTo 0
End Function

Public Sub pCargarRecursos()
'Funcion para cargar los recursos que realizan la actuacion seleccionada
    Dim sql$, qry As rdoQuery, rs As rdoResultset, i%
  
    'Recursos
    sql = "SELECT DISTINCT AG11CODRECURSO, AG11DESRECURSO, AG1400.AG14CODTIPRECU"
    sql = sql & " FROM AG1100, AD0300, PR1300, AG1400"
    sql = sql & " WHERE AD0300.AD02CODDPTO = ?"                                           'Dpto seleccionado
    sql = sql & " AND PR1300.PR01CODACTUACION = ?"                                     '-- actuaci�n seleccionada
    sql = sql & " AND PR1300.PR13INDPREFEREN = -1"                                     '-- Sea preferente"
    sql = sql & " AND PR1300.PR13INDPLANIF = -1"                                       '-- Planificable el tipo
    sql = sql & " AND AG1100.AG14CODTIPRECU = AG1400.AG14CODTIPRECU"                   '-- Recursos con tipos de recursos
    sql = sql & " AND PR1300.AG14CODTIPRECU  = AG1400.AG14CODTIPRECU"                  '-- Relacionado con AG1400 tipo recurso
    sql = sql & " AND AD0300.AD03FECINICIO <= SYSDATE"
    sql = sql & " AND (AD0300.AD03FECFIN >= SYSDATE OR AD0300.AD03FECFIN IS NULL)"
    sql = sql & " AND (AD0300.SG02COD = AG1100.SG02COD OR AG1100.SG02COD IS NULL )"
    sql = sql & " AND AG1100.AG11FECINIVREC <= SYSDATE"
    sql = sql & " AND (AG1100.AG11FECFINVREC >= SYSDATE OR AG1100.AG11FECFINVREC IS NULL)"
    sql = sql & " ORDER BY AG11DESRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodDpto
    qry(1) = lngCodAct
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboRecursos.AddItem rs!AG11CODRECURSO & Chr$(9) & rs!AG11DESRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'se mira si la prueba ten�a ya una cita previa para "arrastrar" el recurso
    sql = "SELECT AG11CODRECURSO"
    sql = sql & " FROM CI0100"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & "')"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        For i = 1 To cboRecursos.Rows
            If i = 1 Then cboRecursos.MoveFirst Else cboRecursos.MoveNext
            If cboRecursos.Columns(0).Text = rs!AG11CODRECURSO Then
                cboRecursos.Text = cboRecursos.Columns(1).Text
                Exit For
            End If
        Next i
    End If
    rs.Close
    qry.Close
End Sub

Public Sub pCargarDatos()
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    sql = "SELECT PR0400.CI21CODPERSONA, CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC,"
    sql = sql & " PR0400.AD02CODDPTO, PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA,"
    sql = sql & " PR0100.PR12CODACTIVIDAD,"
    sql = sql & " PR0400.PR04FECPLANIFIC"
    sql = sql & " FROM CI2200, PR0100, PR0400"
    sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ?"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        txtPac.Text = rs!PAC
        If Not IsNull(rs!CI22NUMHISTORIA) Then txtNH.Text = rs!CI22NUMHISTORIA Else txtNH.Text = ""
        txtAct.Text = rs!PR01DESCORTA
        Call pCargarActCitadas(rs!CI21CODPERSONA)
        lngCodDpto = rs!AD02CODDPTO
        lngCodAct = rs!PR01CODACTUACION
        Call pCargarRecursos
        lngCodActiv = rs!PR12CODACTIVIDAD
        If Not IsNull(rs!PR04FECPLANIFIC) Then strFecInicial = rs!PR04FECPLANIFIC
    End If
    rs.Close
    qry.Close
    
    sql = "SELECT CI01FECCONCERT"
    sql = sql & " FROM CI0100"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & " ')"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strFecInicial = rs!CI01FECCONCERT
    End If
    rs.Close
    qry.Close
End Sub

Public Sub pCargarActCitadas(lngCodPers&)
    Dim sql$, qry As rdoQuery, rs As rdoResultset

    sql = "SELECT TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI'),"
    sql = sql & " PR0100.PR01DESCORTA, CI01OBSERV"
    sql = sql & " FROM CI0100, PR0100, PR0400"
    sql = sql & " WHERE CI0100.CI01SITCITA = " & constESTCITA_CITADA
    sql = sql & " AND CI0100.CI01FECCONCERT >= TRUNC(SYSDATE)"
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR0400.CI21CODPERSONA = ?"
    sql = sql & " ORDER BY CI0100.CI01FECCONCERT "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodPers
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        grdCitas.AddItem rs(0) & Chr(9) & rs(1) & Chr(9) & rs(2)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub mskHora_GotFocus()
    mskHora.SelStart = 0
    mskHora.SelLength = Len(mskHora.Text)
End Sub

Private Sub mskHora_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub mskHora_LostFocus()
    If Not mskHora.Text = "__:__" Then
        mskHora.Text = objGen.ReplaceStr(mskHora.Text, "_", "0", 0)
        If Not IsDate(mskHora.Text) Then mskHora.Text = "__:__"
    End If
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    
    'fecha de cita
    If CDate(dcboFecha.Text & " " & mskHora.Text) < CDate(strAhora) Then
        msg = "La hora de la cita no puede ser anterior a la hora actual."
        MsgBox msg, vbExclamation, Me.Caption
        Exit Function
    End If

    strFechaCita = dcboFecha.Text & " " & mskHora.Text
    fComprobaciones = True
End Function

Private Sub pOverbooking()
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    sql = "SELECT PR0100.PR12CODACTIVIDAD,"
    sql = sql & " PR6600.PR66INDCITAEXTERNA"
    sql = sql & " FROM PR0400, PR0100, PR6600"
    sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ? "
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR6600.AD02CODDPTO (+)= PR0400.AD02CODDPTO"
    sql = sql & " AND PR6600.PR01CODACTUACION (+)= PR0400.PR01CODACTUACION"
    sql = sql & " ORDER BY PR66INDCITAEXTERNA, PR0100.PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Select Case rs!PR12CODACTIVIDAD
        Case constACTIV_CONSULTA 'citables por huecos
                txtobservaciones.BackColor = objApp.objUserColor.lngMandatory
        Case constACTIV_PRUEBA
            If IsNull(rs!PR66INDCITAEXTERNA) Then
                txtobservaciones.BackColor = objApp.objUserColor.lngNormal
            Else
                txtobservaciones.BackColor = objApp.objUserColor.lngMandatory
            End If
        Case Else
            txtobservaciones.BackColor = objApp.objUserColor.lngNormal
        End Select
    End If
End Sub

Private Function fCitarIntervAsociadas(strNumSolicit$, strRecurso$, strFechaCita$) As Boolean
'***************************************************************************************
'*  Se citan las intervenciones asociadas a otras intervenciones
'*  De momento se supone que s�lo puede haber intervenciones asociadas a intervenciones
'***************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim qryPR03, qryPR04, qryCI01, qryCI15, qryCI27 As rdoQuery
    Dim lngNAPlanAso&, lngNAPediAso&, lngCodActAso&, nCita%
    
    'se buscan las intervenciones asociadas a la que se est� citando
    sql = "SELECT PR0400B.PR04NUMACTPLAN, PR0400B.PR03NUMACTPEDI, PR0400B.PR01CODACTUACION"
    sql = sql & " FROM PR0100, PR0400 PR0400B, PR6100, PR0400 PR0400A"
    sql = sql & " WHERE PR0400A.PR04NUMACTPLAN = ?"
    sql = sql & " AND PR6100.PR03NUMACTPEDI_ASO = PR0400A.PR03NUMACTPEDI"
    sql = sql & " AND PR0400B.PR03NUMACTPEDI = PR6100.PR03NUMACTPEDI"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400B.PR01CODACTUACION"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    nCita = 1
    If Not rs.EOF Then
        'PR0300: cambiar la prueba a citable
        sql = "UPDATE PR0300 SET PR03INDCITABLE = -1 WHERE PR03NUMACTPEDI = ?"
        Set qryPR03 = objApp.rdoConnect.CreateQuery("", sql)
        'PR0400: cambiar el estado de la prueba a citada
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CITADA
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qryPR04 = objApp.rdoConnect.CreateQuery("", sql)
        'CI0100: a�adir cita
        sql = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN,"
        sql = sql & " CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO,"
        sql = sql & " CI01INDLISESPE, CI01INDASIG)"
        sql = sql & " VALUES (?, ?, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, -1, 1, 1, 0, 0)"
        Set qryCI01 = objApp.rdoConnect.CreateQuery("", sql)
        qryCI01(0) = strNumSolicit
        qryCI01(2) = strRecurso
        qryCI01(4) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
        qryCI01(5) = constESTCITA_CITADA
        'CI1500: a�adir fase citada
        sql = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15FECCONCPAC, CI15NUMFASECITA,"
        sql = sql & " CI15DESFASECITA, CI15NUMDIASPAC, CI15NUMHORAPAC,"
        sql = sql & " CI15NUMMINUPAC)"
        sql = sql & " SELECT ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI'), PR05NUMFASE,"
        sql = sql & " PR05DESFASE, FLOOR(PR05NUMOCUPACI/1440), FLOOR(MOD(PR05NUMOCUPACI,1440)/60),"
        sql = sql & " MOD(MOD(PR05NUMOCUPACI,1440),60)"
        sql = sql & " FROM PR0500"
        sql = sql & " WHERE PR01CODACTUACION = ?"
        Set qryCI15 = objApp.rdoConnect.CreateQuery("", sql)
        qryCI15(0) = strNumSolicit
        qryCI15(2) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
        'CI2700: A�adir recurso citado
        sql = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO,"
        sql = sql & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC)"
        sql = sql & " SELECT ?, ?, PR05NUMFASE, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'),"
        sql = sql & " FLOOR(PR13NUMTIEMPREC/1440), FLOOR(MOD(PR13NUMTIEMPREC,1440)/60),"
        sql = sql & " MOD(MOD(PR13NUMTIEMPREC,1440),60)"
        sql = sql & " FROM PR1300"
        sql = sql & " WHERE PR01CODACTUACION = ?"
        sql = sql & " AND PR13INDPREFEREN = -1"
        Set qryCI27 = objApp.rdoConnect.CreateQuery("", sql)
        qryCI27(0) = strNumSolicit
        qryCI27(2) = strRecurso
        qryCI27(3) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    
        On Error Resume Next
        Do While Not rs.EOF
            lngNAPlanAso = rs!PR04NUMACTPLAN
            lngNAPediAso = rs!PR03NUMACTPEDI
            lngCodActAso = rs!PR01CODACTUACION
            'PR0300: cambiar la prueba a citable
            qryPR03(0) = lngNAPediAso
            qryPR03.Execute
            qryPR03.Close
            If Err > 0 Then Exit Function
            'PR0400: cambiar el estado de la prueba a citada
            qryPR04(0) = lngNAPlanAso
            qryPR04.Execute
            qryPR04.Close
            If Err > 0 Then Exit Function
            'CI0100: a�adir cita
            nCita = nCita + 1
            qryCI01(1) = nCita
            qryCI01(3) = lngNAPlanAso
            qryCI01.Execute
            qryCI01.Close
            If Err > 0 Then Exit Function
            'CI1500: a�adir fase citada
            qryCI15(1) = nCita
            qryCI15(3) = lngCodActAso
            qryCI15.Execute
            qryCI15.Close
            If Err > 0 Then Exit Function
            'CI2700: A�adir recurso citado
            qryCI27(1) = nCita
            qryCI27(4) = lngCodActAso
            qryCI27.Execute
            qryCI27.Close
            If Err > 0 Then Exit Function
    
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
    
    fCitarIntervAsociadas = True
End Function
