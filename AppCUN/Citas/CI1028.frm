VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmCitasRecursos 
   Caption         =   "Citas. Actuaciones Planificadas por Recurso"
   ClientHeight    =   8310
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   11790
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   11790
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edicion"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener"
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo Valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner Filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar Filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero          Ctrl+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior         Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente       Av Pag"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo            Ctrl+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar Registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Acerca de ..."
         Index           =   30
      End
   End
   Begin VB.Menu mnuEstados 
      Caption         =   "Estados Cita"
      Visible         =   0   'False
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "&0. Sin Citar"
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "&1. Confirmar"
         Index           =   1
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "&2. Anular"
         Index           =   2
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "&3. Recitar"
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "&4. Pendiente de Recitar"
         Index           =   4
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "-"
         Index           =   5
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEstadosOpcion 
         Caption         =   "Lista de Espera? (S/N)"
         Index           =   6
      End
   End
End
Attribute VB_Name = "frmCitasRecursos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim vnta As Variant
Dim strRecursos As String
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public strMensaje As String
Private Sub bDBUpdateCamaAnuReservada(Cama As String, NumAct As Long)
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
   
    'Updatemos como Null la cama en la tabla AD1500 la cama reservada
    sStmSql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ? "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = Cama
    qryUpdate.Execute
    qryUpdate.Close
   
    'Updatemos como Null la cama en la tabla CI0100 la cama reservada
    sStmSql = "UPDATE CI0100 SET AD15CODCAMA = Null "
    sStmSql = sStmSql & "WHERE  PR04NUMACTPLAN = ?  "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = NumAct  'Num_Actuaci�n
    qryUpdate.Execute
    qryUpdate.Close
   
End Sub
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  If intIndex > 0 Then
    Call objWinInfo.CtrlGotFocus
  End If
End Sub
Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    cboSSDBCombo1(intIndex).Text = ""
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
  End If
End Sub
Private Sub cboSSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 And (KeyAscii = 65 Or KeyAscii = 97) Then
        If cboSSDBCombo1(Index).Text = "" Then
            KeyAscii = Asc("W")
        End If
        
    End If
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  If intIndex > 0 Then
    Call objWinInfo.CtrlLostFocus
  End If
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  If intIndex > 0 Then
    Call objWinInfo.CtrlDataChange
  End If
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  If intIndex > 0 Then
    Call objWinInfo.CtrlDataChange
  End If
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  objApp.rdoConnect.QueryTimeout = 600
  Call objWinInfo.FormChangeActive(cboSSDBCombo1(intIndex).Container, False, True)
  If intIndex = 0 And cboSSDBCombo1(0).Text <> "" Then
'******************************************************************
  ' Cambio de departamento
'******************************************************************
  ' Modifico la strWhere para obtener los nuevos registro del
  ' departamento elegido y refresco el cursor
    With objWinInfo
      '.objWinActiveForm.strInitialWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value
      .objWinActiveForm.strWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value & " and (AG1100.AG11FECFINVREC > SYSDATE OR AG1100.AG11FECFINVREC IS NULL)"
      .DataRefresh
    End With
    'Activar el bot�n de impresi�n Mariajo 1999/06/24
    chkCon(0).Value = False
  End If
  objApp.rdoConnect.QueryTimeout = 30
  
End Sub

Private Sub cmdaccionanular_Click()
 Dim lngincidencia       As Long
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim blnAnularSolicitud As Boolean
  Dim lngNumActPlan As Long
  Dim NumCita As Byte
  If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
    If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
   End If
'  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, intDptoRayos) Then
'        Call objError.SetError(cwCodeMsg, "No puede Anular Actuaciones de Radiolog�a")
'        vnta = objError.Raise
'        Exit Sub
'  End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, 213) Then
''        Call objError.SetError(cwCodeMsg, "No puede Anular Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
  If grdDBGrid1(2).SelBookmarks.Count = 0 Then
    Call objError.SetError(cwCodeMsg, "Debe Seleccionar Alguna Cita", vnta)
    vnta = objError.Raise
  Else
      If freserva Then
        Call objError.SetError(cwCodeMsg, "Ha seleccionado una Reserva, debe Seleccionar Alguna Cita", vnta)
        vnta = objError.Raise

      Else
          On Error GoTo Canceltrans
          lngNumSolicitud = grdDBGrid1(2).Columns("Solicitud").Value
          NumCita = grdDBGrid1(2).Columns("N�mero Cita").Value

         lngNumActPlan = HallarNumActPlan(lngNumSolicitud, NumCita)

         Select Case ViewSolicitud
           Case 1 'han pulsado anular actuacion
              blnAnularSolicitud = False
           Case 2 'han pulsado anular SOLICITUD
              blnAnularSolicitud = True
           Case 3 'han pulsado cancelar
             Exit Sub
         End Select

          blnActive = True
          objApp.BeginTrans
          objSecurity.RegSession
          intGridIndex = tabtab1(2).tab + 1
          lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
          If lngincidencia <> -1 And lngincidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
'             '**********************************************************
'            ' Modificaci�n para paso IBM 15/1/1999
'             If blnAnularSolicitud Then 'han pulsado anular actuacion
'                    Call insCitIbmNumSol(3, lngNumSolicitud)
'               Else 'han pulsado anular actuacion
'                    Call insCitIbmNumActPlan(3, lngNumActPlan)
'             End If
'            '**********************************************************
            If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciAnulada, lngincidencia, blnAnularSolicitud) Then
              blnCancelTrans = True
            End If
          Else
            blnCancelTrans = True
          End If

          If blnCancelTrans Then
            objApp.RollbackTrans
            Call objError.SetError(cwCodeMsg, IIf(blnActive, "Anulaci�n Cancelada", ciErrInciNoActive & vbCrLf))
            Call objError.Raise
          Else
            Call pAnularAsociadas(grdDBGrid1(2).Columns("N�mero").Value)

            Me.MousePointer = vbHourglass
            objApp.CommitTrans
            Call objWinInfo.DataRefresh
            Call Me.Refresh
            Me.MousePointer = vbDefault
            DoEvents
          End If

          Exit Sub

Canceltrans:
           blnCancelTrans = True
           Resume Next
    End If
End If
End Sub

Private Sub cmdAgendaRec_Click()
    If cboSSDBCombo1(0).Text <> "" And txtText1(0).Text <> "" And txtText1(1).Text <> "" Then
        Call objPipe.PipeSet("DPTO", cboSSDBCombo1(0).Text)
        Call objPipe.PipeSet("COD_REC", txtText1(0).Text)
        Call objPipe.PipeSet("REC", txtText1(1).Text)
    End If
    Call objSecurity.LaunchProcess("AG1008")
End Sub

Private Sub cmdAnular_Click(Index As Integer)
  Dim lngincidencia       As Long
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim blnAnularSolicitud As Boolean
  Dim lngNumActPlan As Long
  Dim NumCita As Byte
   If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, intDptoRayos) Then
''        Call objError.SetError(cwCodeMsg, "No puede Anular Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, 213) Then
''        Call objError.SetError(cwCodeMsg, "No puede Anular Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
    If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
   End If
  If grdDBGrid1(1).SelBookmarks.Count = 0 Then
    Call objError.SetError(cwCodeMsg, "Debe Seleccionar Alguna Cita", vnta)
    vnta = objError.Raise
  Else
      If freserva Then
        Call objError.SetError(cwCodeMsg, "Ha seleccionado una Reserva, debe Seleccionar Alguna Cita", vnta)
        vnta = objError.Raise
        
      Else
          On Error GoTo Canceltrans
          lngNumSolicitud = grdDBGrid1(1).Columns("Solicitud").Value
          NumCita = grdDBGrid1(1).Columns("N�mero Cita").Value
         
         lngNumActPlan = HallarNumActPlan(lngNumSolicitud, NumCita)
         
         Select Case ViewSolicitud
           Case 1 'han pulsado anular actuacion
              blnAnularSolicitud = False
           Case 2 'han pulsado anular SOLICITUD
              blnAnularSolicitud = True
           Case 3 'han pulsado cancelar
             Exit Sub
         End Select
         
          blnActive = True
          objApp.BeginTrans
          objSecurity.RegSession
          intGridIndex = tabtab1(2).tab + 1
          lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
          If lngincidencia <> -1 And lngincidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
'''             '**********************************************************
'''            ' Modificaci�n para paso IBM 15/1/1999
'''             If blnAnularSolicitud Then 'han pulsado anular actuacion
'''                    Call insCitIbmNumSol(3, lngNumSolicitud)
'''               Else 'han pulsado anular actuacion
'''                    Call insCitIbmNumActPlan(3, lngNumActPlan)
'''             End If
'''            '**********************************************************
            If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciAnulada, lngincidencia, blnAnularSolicitud) Then
              blnCancelTrans = True
            End If
          Else
            blnCancelTrans = True
          End If
        
          If blnCancelTrans Then
            objApp.RollbackTrans
            Call objError.SetError(cwCodeMsg, IIf(blnActive, "Anulaci�n Cancelada", ciErrInciNoActive & vbCrLf))
            Call objError.Raise
          Else
            Call pAnularAsociadas(grdDBGrid1(1).Columns("N�mero").Value)
            
            Me.MousePointer = vbHourglass
            objApp.CommitTrans
            Call objWinInfo.DataRefresh
            Call Me.Refresh
            Me.MousePointer = vbDefault
            DoEvents
          End If
         
          Exit Sub
        
Canceltrans:
            MsgBox Error
           blnCancelTrans = True
           Resume Next
    End If
End If
End Sub




Private Sub cmdavisos_Click()
Dim vntData(1)
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
vntData(1) = grdDBGrid1(2).Columns("Persona").Value
Call objSecurity.LaunchProcess("PR0580", vntData)
End Sub

Private Sub cmdCitar_Click()
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
    If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
    End If
      Call objSecurity.LaunchProcess("CI1150", grdDBGrid1(1).Columns("N�mero").Value)
  'refrescar
  Call objWinInfo.DataRefresh
End Sub

Private Sub cmdCommand1_Click() 'consultar

Dim i As Integer

Screen.MousePointer = vbHourglass
'comprobamos las fechas
If cboSSDBCombo1(0).Text <> "" Then
    If dtcDateCombo1(0) = "" Or dtcDateCombo1(1) = "" Then
      Call objError.SetError(cwCodeMsg, "La Fecha Desde y la Fecha Hasta son obligatorios")
      Call objError.Raise
      Exit Sub
    Else
      If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
          Call objError.Raise
          Exit Sub
        End If
    End If
   End If
   'nos ponemos en las citadas
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If chkCon(0).Value = 0 Then 'si queremos un solo recurso.
        objWinInfo.objWinActiveForm.strWhere = "CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
            " AND CI01FECCONCERT <= TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')" & _
            " AND (CI01SITCITA = '" & ciConfirmada & "' OR CI01SITCITA = '" & ciReservada & _
            "') AND AG11CODRECURSO_ASI='" & txtText1(0).Text & "'"
        objWinInfo.DataRefresh
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
         objWinInfo.objWinActiveForm.strWhere = "CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
            " AND CI01FECCONCERT < TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')" & _
            " AND CI01SITCITA = '" & ciPdteRecitar & "' AND AG11CODRECURSO_ASI=" & txtText1(0).Text
        objWinInfo.DataRefresh
    Else 'si queremos todos los recursos del departamento
        strRecursos = ""
        grdDBGrid1(0).MoveFirst
        For i = 1 To grdDBGrid1(0).Rows
            strRecursos = strRecursos & grdDBGrid1(0).Columns(1).Value & ","
            grdDBGrid1(0).MoveNext
        Next i
        strRecursos = Left(strRecursos, Len(strRecursos) - 1)
                objWinInfo.objWinActiveForm.strWhere = "CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
            " AND CI01FECCONCERT <= TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')" & _
            " AND (CI01SITCITA = '" & ciConfirmada & "' OR CI01SITCITA = '" & ciReservada & _
            "') AND AG11CODRECURSO_ASI IN (" & strRecursos & ")"
        objWinInfo.DataRefresh
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
         objWinInfo.objWinActiveForm.strWhere = "CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
            " AND CI01FECCONCERT < TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')" & _
            " AND CI01SITCITA = '" & ciPdteRecitar & "' AND AG11CODRECURSO_ASI IN (" & strRecursos & ")"
        objWinInfo.DataRefresh
    End If
End If
Screen.MousePointer = vbDefault

End Sub

Private Sub cmdDetalle_Click(intIndex As Integer)
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
  If Val(grdDBGrid1(tabtab1(2).tab + 1).Columns("N�mero").Value) > 0 Then
     ' Call ShowDetail(Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("N�mero").Value), Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("Solicitud").Value), Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("N�mero Cita").Value))
   Call ShowDetail2(grdDBGrid1(tabtab1(2).tab + 1))
  End If
End Sub

Private Sub cmdHoja_Click()
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
If grdDBGrid1(2).Rows = 0 Then
    MsgBox "No hay ning�n paciente pendiente de recitar."
    Exit Sub
End If
  Dim strWhereTotal As String
    Dim strTipoHoja As String
    Dim intCont As Integer
    Dim strSql As String
    Dim rdprocAsis As rdoResultset
    Dim A�oIn As Variant
    Dim MesIn As Variant
    Dim DiaIn As Variant
    Dim A�oOut As Variant
    Dim MesOut As Variant
    Dim DiaOut As Variant
    Dim DateIN As Date
    Dim DTFEC As Date
        
    DateIN = DateAdd("d", -2, CDate(dtcDateCombo1(0).Text))
    A�oIn = Format(CDate(dtcDateCombo1(0).Text), "YYYY")
    MesIn = Format(CDate(dtcDateCombo1(0).Text), "MM")
    DiaIn = Format(CDate(dtcDateCombo1(0).Text), "DD")
    A�oOut = Format(dtcDateCombo1(1).Text, "YYYY")
    MesOut = Format(dtcDateCombo1(1).Text, "MM")
    DiaOut = Format(dtcDateCombo1(1).Text, "DD")
    DTFEC = DiaOut & "/" & MesOut & "/" & A�oOut
    DTFEC = DateAdd("d", 1, DTFEC)
    A�oOut = Format(DTFEC, "YYYY")
    MesOut = Format(DTFEC, "MM")
    DiaOut = Format(DTFEC, "DD")
    strWhereTotal = "({PR0432j.AG11CODRECURSO_ASI}=" & txtText1(0).Text & _
        " and {PR0432j.CI01FECCONCERT} >= Date(" & A�oIn & "," & MesIn & "," & DiaIn & ") and " & _
        " {PR0432J.CI01FECCONCERT} <= Date(" & A�oOut & "," & MesOut & "," & DiaOut & "))"
   
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Pendientes.rpt"
    With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToWindow
        .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Me.MousePointer = vbHourglass

        .Action = 1
        Me.MousePointer = vbDefault
    End With
End Sub


Private Sub cmdImprimirCarta_Click(Index As Integer)
Dim i As Integer
Dim vntB As Variant
If grdDBGrid1(1).SelBookmarks.Count = 0 Then
    MsgBox "Seleccione los pacientes", vbOKOnly
Else
     frmMensaje.Show vbModal
    For i = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
        vntB = grdDBGrid1(1).SelBookmarks(i)
        Call pImprimirCartas(grdDBGrid1(1).Columns("N�mero").CellValue(vntB))
    Next i
End If
End Sub

Private Sub cmdModificar_Click(Index As Integer)

  Dim lngincidencia       As Long
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim lngNumSolicitud As Long
  Dim lngNumActPlan As Long
  Dim NumCita As Byte
  Dim vnta As Variant
  Dim intF As Integer
  Dim lngActped As Long
  Dim vntRBookmark As Variant
  If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, intDptoRayos) Then
''        Call objError.SetError(cwCodeMsg, "No puede Modificar Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
    If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
   End If
  If freserva Then
        Call objError.SetError(cwCodeMsg, "Ha seleccionado una Reserva, debe Seleccionar Alguna Cita", vnta)
        vnta = objError.Raise
  Else
      lngNumSolicitud = grdDBGrid1(1).Columns("Solicitud").Value
      NumCita = grdDBGrid1(1).Columns("N�mero Cita").Value
        intGridIndex = tabtab1(2).tab + 1
  
        'EFS: ACTUALIZAMOS LA RESTRINCION DE USUARIO
        If grdDBGrid1(intGridIndex).SelBookmarks.Count > 0 Then
          For intF = 0 To grdDBGrid1(intGridIndex).SelBookmarks.Count - 1
            vntRBookmark = grdDBGrid1(intGridIndex).SelBookmarks(intF)
            lngActped = grdDBGrid1(intGridIndex).Columns(grdDBGrid1(intGridIndex).Name & "(" & grdDBGrid1(intGridIndex).Index & ").Actuaci�n Pedida").CellValue(vntRBookmark)
            Call sActRestrincion(lngActped)
          Next intF
         End If
      objApp.BeginTrans
      objSecurity.RegSession
      On Error GoTo Canceltrans
      blnActive = True
      On Error GoTo 0
      lngNumSolicitud = Val(grdDBGrid1(1).Columns("Solicitud").Value)
    
      If ViewSolicitud(True) <> 3 Then
        intGridIndex = tabtab1(2).tab + 1
        lngincidencia = AddInciden(ciCodInciModificacion) 'esta variable se encuentra en el m�dulo general
        If lngincidencia <> -1 And lngincidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
          If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciAnulada, lngincidencia) Then
            blnCancelTrans = True
          Else
            Me.MousePointer = vbHourglass
            Call objSelRefresh(grdDBGrid1(tabtab1(2).tab + 1), 0, ciOpcModificar)
            blnCommit = False
            
            'Load frmSolicitud
            'frmSolicitud.Show vbModal
            'Unload frmSolicitud
            'Set frmSolicitud = Nothing
            
        'variable pipe referente al pasoIbm **************** 19/01/1999
          lngNumActPlan = HallarNumActPlan(lngNumSolicitud, NumCita)
          Call objPipe.PipeSet("modifActuacCI1028", lngNumActPlan)
            Call objSecurity.LaunchProcess(ciSoluciones)
            Me.MousePointer = vbDefault
     
          End If
        Else
          blnCancelTrans = True
        End If
    
        If blnCancelTrans Or Not blnCommit Then
          objApp.RollbackTrans
          Call objError.SetError(cwCodeMsg, IIf(blnActive, "Modificaci�n Cancelada", ciErrInciNoActive & vbCrLf))
          Call objError.Raise
        Else
          objApp.CommitTrans
          Call objWinInfo.DataRefresh
          Call Me.Refresh
          DoEvents
        End If
      End If
    
      Exit Sub
    
Canceltrans:
       blnReturn = False
       blnCancelTrans = True
       Resume Next
  End If
End Sub

Private Sub cmdObservaciones_Click()
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
If grdDBGrid1(1).Columns("N�mero").Value = "" Then Exit Sub
 Call objPipe.PipeSet("PR_NActPlan", grdDBGrid1(1).Columns("N�mero").Value)
 Call objSecurity.LaunchProcess("PR0502")
End Sub

Private Sub cmdObservaciones1_Click()
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
If grdDBGrid1(2).Columns("N�mero").Value = "" Then Exit Sub

 Call objPipe.PipeSet("PR_NActPlan", grdDBGrid1(2).Columns("N�mero").Value)
 Call objSecurity.LaunchProcess("PR0502")
End Sub

Private Sub cmdRecitarPlan_Click(Index As Integer)

  Dim blnCancelTrans As Boolean
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
  If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
   End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, intDptoRayos) Then
''        Call objError.SetError(cwCodeMsg, "No puede Recitar Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
  'objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  
  blnRecMode = True
  Call objSecurity.LaunchProcess(ciRecitacion)
  If blnCancelTrans Then
  '  objApp.rdoConnect.RollbackTrans
    'Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.SetError(cwCodeMsg, "Recitar Actuaciones Cancelado")
    Call objError.Raise
  Else
    If blnCommit Then
   '   objApp.rdoConnect.CommitTrans
      Call objWinInfo.DataRefresh
      Call Me.Refresh
      DoEvents
      blnCommit = False
    End If
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdRecitarAgenda_Click(Index As Integer)

  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim intF                As Integer
  Dim vntRBookmark         As Variant
  Dim lngActped           As Long
   If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
  If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
  End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, intDptoRayos) Then
''        Call objError.SetError(cwCodeMsg, "No puede Recitar Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
  intGridIndex = tabtab1(2).tab + 1
  
    'EFS: ACTUALIZAMOS LA RESTRINCION DE USUARIO
    If grdDBGrid1(intGridIndex).SelBookmarks.Count > 0 Then
      For intF = 0 To grdDBGrid1(intGridIndex).SelBookmarks.Count - 1
        vntRBookmark = grdDBGrid1(intGridIndex).SelBookmarks(intF)
        lngActped = grdDBGrid1(intGridIndex).Columns(grdDBGrid1(intGridIndex).Name & "(" & grdDBGrid1(intGridIndex).Index & ").Actuaci�n Pedida").CellValue(vntRBookmark)
        Call sActRestrincion(lngActped)
      Next intF
     End If
  
  objApp.BeginTrans
  objSecurity.RegSession
  On Error GoTo Canceltrans
  blnActive = True

  intGridIndex = tabtab1(2).tab + 1
  If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciRecitada) Then
    blnCancelTrans = True
  Else
    Call objSelRefresh(grdDBGrid1(tabtab1(2).tab + 1), 0, ciOpcRecitar)
    If grdDBGrid1(tabtab1(2).tab + 1).SelBookmarks.Count > 0 Then
     'LLamar a la pantalla de Solicitud de citas
      Me.MousePointer = vbHourglass
      blnCommit = False
      
      Call objSecurity.LaunchProcess(ciSoluciones)
      Me.MousePointer = vbDefault
    Else
      Call objError.SetError(cwCodeMsg, "No hay ninguna Actuaci�n seleccionada")
      Call objError.Raise
      Exit Sub
    End If

  End If

  If blnCancelTrans Or Not blnCommit Then
    objApp.RollbackTrans
    Call objError.SetError(cwCodeMsg, "Citar Actuaciones Cancelado")
    Call objError.Raise
  Else
    objApp.CommitTrans
    If blnCommit Then
      Call objWinInfo.DataRefresh
      Call Me.Refresh
      DoEvents
    End If
  End If

  Exit Sub

Canceltrans:
   blnReturn = False
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdVerFases_Click(Index As Integer)

  Dim blnCancelTrans As Boolean
  Dim intGridIndex As Integer
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
  On Error GoTo Canceltrans
  
  intGridIndex = tabtab1(2).tab + 1
  vntNumActPlan = grdDBGrid1(intGridIndex).Columns("N�mero").Value
  vntNumActPedi = grdDBGrid1(intGridIndex).Columns("Actuaci�n Pedida").Value
  vntNumSolicit = grdDBGrid1(intGridIndex).Columns("Solicitud").Value
  vntNumCita = grdDBGrid1(intGridIndex).Columns("N�mero Cita").Value
  Me.MousePointer = vbHourglass
  
  Call objSecurity.LaunchProcess(ciFasesRecursos)
  Me.MousePointer = vbDefault

  If blnCancelTrans Then
    Call objError.SetError(cwCodeMsg, "Ver Fases Cancelado")
    Call objError.Raise
  Else
    Call Me.Refresh
    DoEvents
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdVerListaEspera_Click(Index As Integer)
  Dim aVals(1 To 2)
   If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
  aVals(1) = txtText1(0)
  aVals(2) = cboSSDBCombo1(0).Value
  Call objSecurity.LaunchProcess(ciListaEsperaRec, aVals)

End Sub

Private Sub cmdVerSeleccion_Click(Index As Integer)
 If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
  Call objGen.RemoveCollection(objSolicitud.cllPeticiones)
  Call objSelRefresh(grdDBGrid1(tabtab1(2).tab + 1))
  Call objShowSelected
  If blnBorrarSel Then
    grdDBGrid1(tabtab1(2).tab + 1).SelBookmarks.RemoveAll
    blnBorrarSel = False
  End If
End Sub

Private Sub Command1_Click()

End Sub

Private Sub cmdVG_Click()
If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
Me.MousePointer = vbHourglass
ReDim vntData(1) As Variant
        If grdDBGrid1(1).Rows = 0 Then
        Me.MousePointer = vbDefault
        Exit Sub
        End If
        vntData(1) = grdDBGrid1(1).Columns("Persona").Value
        Call objSecurity.LaunchProcess("AD1126", vntData)
        Me.MousePointer = vbDefault
End Sub

Private Sub cmdVisionGlobal_Click()
 If grdDBGrid1(2).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
Me.MousePointer = vbHourglass
ReDim vntData(1) As Variant
        If grdDBGrid1(2).Rows = 0 Then
        Me.MousePointer = vbDefault
        Exit Sub
        End If
        vntData(1) = grdDBGrid1(2).Columns("Persona").Value
        Call objSecurity.LaunchProcess("AD1126", vntData)
        Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
  
  Dim objMasterInfo     As New clsCWForm
  Dim objMultiInfo1     As New clsCWForm
  Dim objMultiInfo2     As New clsCWForm
  Dim objMultiInfo3     As New clsCWForm
  Dim intGridIndex      As Integer
  Dim strFechaIni       As String
  Dim strFechaFin       As String
  Dim dtdatei          As Date
  Dim dtdatef         As Date
  
  chkCon(0).Value = False
  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  tabtab1(2).tab = 0
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  strFechaIni = strFecha_Sistema()
'  dtdatef = DateAdd("m", 1, strFechaIni)
'  strFechaFin = Format(dtdatef, "DD/MM/YYYY")
    strFechaFin = strFechaIni
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Actuaciones seg�n su situaci�n por Recurso "
    .cwDAT = "2-12-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = ""
    
    .cwUPD = "2-12-97 - I�aki - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
  With objMasterInfo
   ' Asigno nombre al frame
   ' .strName = "Recurso"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabtab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1100"
    .strWhere = " AG11FECFINVREC > SYSDATE OR AG11FECFINVRECN IS NULL"
  ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowReadOnly
    .intCursorSize = -1
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
  ' Creaci�n de los filtros de busqueda
    
  ' Creaci�n de los criterios de ordenaci�n
  'Impreso
  Call .objPrinter.Add("CI024", "Listado en un Rango de Fechas")
  Call .objPrinter.Add("CI01R", "Listado Diario")
  Call .objPrinter.Add("CI03R", "Listado Diario con Solicitante")
  End With
  

  With objMultiInfo2
    .strName = "Confirmadas"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strWhere = "CI01SITCITA = '" & ciConfirmada & "' OR CI01SITCITA = & '" & _
    ciReservada & "'"
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0432J"
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 200
    
    Call .FormAddOrderField("CI01FECCONCERT", False)
     
  End With

  With objMultiInfo3
    .strName = "Pendientes"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0432J"
    .intAllowance = cwAllowReadOnly
    .blnEnabled = False
    .intCursorSize = 200
    Call .FormAddOrderField("CI01FECCONCERT", False)
  End With


  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo3, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    Call .FormCreateInfo(objMasterInfo)

    
    Call .GridAddColumn(objMultiInfo2, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo2, "Historia", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo2, "Paciente", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo2, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo2, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo2, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo2, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo2, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo2, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo2, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo2, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo2, "CodEstado", "PR37CODESTADO")
    Call .GridAddColumn(objMultiInfo2, "Estado", "")
    Call .GridAddColumn(objMultiInfo2, "Lista Espera", "CI01INDLISESPE", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Indice de Asignaci�n", "CI01INDASIG", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Tipo de Asignaci�n", "")
    Call .GridAddColumn(objMultiInfo2, "F.en cola", "PR04FECENTRCOLA", cwDate)
    Call .GridAddColumn(objMultiInfo2, "CodTipAct", "")
    Call .FormCreateInfo(objMultiInfo2)

    Call .GridAddColumn(objMultiInfo3, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo3, "Historia", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo3, "Paciente", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo3, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo3, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo3, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo3, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo3, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo3, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo3, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo3, "Incidencia", "AG05NUMINCIDEN")
    Call .GridAddColumn(objMultiInfo3, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo3, "CodEstado", "PR37CODESTADO")
    Call .GridAddColumn(objMultiInfo3, "Estado", "")
    Call .GridAddColumn(objMultiInfo3, "CodRecAsi", "AG11CODRECURSO_ASI")
    Call .GridAddColumn(objMultiInfo3, "C�dDepartamento", "AD02CODDPTO")
    Call .GridAddColumn(objMultiInfo3, "F.en cola", "PR04FECENTRCOLA", cwDate)
    Call .GridAddColumn(objMultiInfo3, "CodTipAct", "")

    grdDBGrid1(2).Columns("Incidencia").Visible = False
        Call .FormCreateInfo(objMultiInfo3)
    For intGridIndex = 1 To 2
       
       'comentarizados para realizar pruebas luego seran invisibles
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero Cita").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Situaci�n").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodEstado").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodEstado").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodTipAct").Visible = False
       
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Paciente").Width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n").Width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Estado").Width = TextWidth(String(30, " "))

       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns(8)), "PR01CODACTUACION", "SELECT PR01DESCORTA,PR12CODACTIVIDAD FROM PR0100 WHERE PR01CODACTUACION = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n"), "PR01DESCORTA")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodTipAct"), "PR12CODACTIVIDAD")
       
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("CodEstado")), "PR37CODESTADO", "SELECT PR37DESESTADO FROM PR3700 WHERE PR37CODESTADO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodEstado")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Estado"), "PR37DESESTADO")
       
    Next
    
    grdDBGrid1(2).Columns("CodRecAsi").Visible = False
    grdDBGrid1(2).Columns("C�dDepartamento").Visible = False
    grdDBGrid1(1).Columns("Indice de Asignaci�n").Visible = False
    
    grdDBGrid1(1).Columns("CodEstado").Visible = False
    grdDBGrid1(2).Columns("CodEstado").Visible = False
        
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1  AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL) ORDER BY AD02DESDPTO"
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnNegotiated = False
    
    'Tama�o de los campos del grid
    'a�ado uno para las longitud
    
    grdDBGrid1(1).Columns(3).Width = 800
    grdDBGrid1(1).Columns(4).Width = 800
    grdDBGrid1(1).Columns(6).Width = 1600
    grdDBGrid1(1).Columns(9).Width = 2000
    grdDBGrid1(1).Columns(14).Width = 1000
    
    grdDBGrid1(2).Columns(3).Width = 800
    grdDBGrid1(2).Columns(4).Width = 800
    grdDBGrid1(2).Columns(6).Width = 1600
    
    
    Call .WinRegister
    Call .WinStabilize
    blnCommit = False
  End With
      
      cmdVerFases(0).Enabled = False
      cmdVerFases(2).Enabled = False
      cmdDetalle(1).Enabled = False
      cmdDetalle(2).Enabled = False
      cmdVerSeleccion(1).Enabled = False
      cmdVerSeleccion(2).Enabled = False
      cmdAnular(0).Enabled = False
      cmdaccionanular.Enabled = False
      
      cmdModificar(0).Enabled = False
      cmdVerSolicitud(0).Enabled = False
      cmdVerSolicitud(1).Enabled = False
      cmdRecitarPlan(0).Enabled = False
      cmdRecitarAgenda(0).Enabled = False
      dtcDateCombo1(0).Date = strFechaIni
      dtcDateCombo1(1).Date = strFechaFin
 objWinInfo.cllWinForms("fraFrame1(" & tabtab1(0).tab + 1 & ")").blnEnabled = True
 Call objApp.SplashOff
End Sub

Private Sub g_Click()

End Sub

Private Sub grdDBGrid1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBGrid1_Click(intIndex As Integer)
    Dim sCodEstado As String
    
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  Select Case intIndex
    Case 1
        If grdDBGrid1(intIndex).Columns(0).Value <> "" Then
            sCodEstado = grdDBGrid1(intIndex).Columns("CodEstado").Value
            If (sCodEstado = "2" Or sCodEstado = "5") And grdDBGrid1(intIndex).Columns("F.en cola").Value = "" Then
                cmdAnular(0).Enabled = True
                cmdaccionanular.Enabled = True
                cmdModificar(0).Enabled = True
                cmdVerSolicitud(0).Enabled = True
            Else
                cmdAnular(0).Enabled = False
                cmdaccionanular.Enabled = False
                cmdModificar(0).Enabled = False
                cmdVerSolicitud(0).Enabled = False
            End If
        End If
    Case 2
        If grdDBGrid1(intIndex).Columns(0).Value <> "" Then
            sCodEstado = grdDBGrid1(intIndex).Columns("CodEstado").Value
            If sCodEstado = 1 Then
                cmdVerSolicitud(1).Enabled = True
                cmdRecitarPlan(0).Enabled = True
                cmdRecitarAgenda(0).Enabled = True
            Else
                cmdVerSolicitud(1).Enabled = False
                cmdRecitarPlan(0).Enabled = False
                cmdRecitarAgenda(0).Enabled = False
            End If
        End If
    End Select
    tlbToolbar1.Buttons(28).Enabled = True
    
    
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub
Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  
  Dim sDatosPersona     As String
  
    If intIndex > 0 Then
        grdDBGrid1(intIndex).Columns("Fecha Cita").Text = Format(grdDBGrid1(intIndex).Columns("Fecha Cita").Text, "DD/MM/YYYY - HH:NN")
        'grdDBGrid1(intIndex).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(intIndex).Columns("Fecha Preferencia").Text, "DD/MM/YYYY - HH:NN")
        'AJO 12/06/1999 Incluir en lista el n�mero de historia  :  grdDBGrid1(intIndex).Columns("Persona").Text = GetPersonName(Val(grdDBGrid1(intIndex).Columns("Persona").Value))
        sDatosPersona = GetPersonName(Val(grdDBGrid1(intIndex).Columns("Paciente").Value))
        grdDBGrid1(intIndex).Columns("Paciente").Text = Trim(Mid(sDatosPersona, 9, Len(sDatosPersona) - 7))
        grdDBGrid1(intIndex).Columns("Historia").Text = Trim(Left(sDatosPersona, 7))
        'grdDBGrid1(intIndex).Columns("Persona Solicitante").Text = GetStaffName(Val(grdDBGrid1(intIndex).Columns("Persona Solicitante").Value))
        Select Case intIndex
            Case 1
                If grdDBGrid1(1).Columns("Indice de Asignaci�n").Text = 0 Then
                    grdDBGrid1(1).Columns("Tipo de Asignaci�n").Text = "Asignaci�n Manual"
                End If
                If grdDBGrid1(1).Columns("Indice de Asignaci�n").Text = -1 Then
                    grdDBGrid1(1).Columns("Tipo de Asignaci�n").Text = "Asiganci�n Autom�tica"
                End If
        End Select
    End If
End Sub

Private Sub grdDBGrid1_SelChange(intIndex As Integer, ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
    cmdAnular(0).Enabled = True
    cmdaccionanular.Enabled = True
    cmdModificar(0).Enabled = True
    cmdVerSolicitud(0).Enabled = True
End Sub
Private Sub lblLabel1_Click(Index As Integer)
Call objWinInfo.FormChangeActive(lblLabel1(Index).Container, False, True)
End Sub
Private Sub mnuEstadoOpcion_Click(intIndex As Integer)
  Dim intGridIndex As Integer
  intGridIndex = tabtab1(2).tab + 1
  Call CitasChangeStatus(grdDBGrid1(intGridIndex), LTrim(Str(intIndex)))
  Call objWinInfo.DataRefresh
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub
Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub
Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
Dim vntData(1) As Variant
If strFormName = "Confirmadas" Then
  vntData(1) = grdDBGrid1(1).Columns("Persona").Text
Else
  vntData(1) = grdDBGrid1(2).Columns("Persona").Text
End If
Call objSecurity.LaunchProcess("CI1024", vntData)
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  fraFrame1(9).ForeColor = fraFrame1(tabtab1(2).tab + 1).ForeColor
    If strFormName = "fraFrame1(0)" Then
    cmdVerFases(0).Enabled = False
    cmdVerFases(2).Enabled = False
    cmdDetalle(1).Enabled = False
    cmdDetalle(2).Enabled = False
    cmdVerSeleccion(1).Enabled = False
    cmdVerSeleccion(2).Enabled = False
    cmdAnular(0).Enabled = False
    cmdaccionanular.Enabled = False
    cmdModificar(0).Enabled = False
    cmdVerSolicitud(0).Enabled = False
    cmdVerSolicitud(1).Enabled = False
    cmdRecitarPlan(0).Enabled = False
    cmdRecitarAgenda(0).Enabled = False
  Else
    'If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
    If grdDBGrid1((tabtab1(2).tab) + 1).Rows > 0 Then
      cmdVerFases(0).Enabled = True
      cmdVerFases(2).Enabled = True
      cmdDetalle(1).Enabled = True
      cmdDetalle(2).Enabled = True
      cmdVerSeleccion(1).Enabled = True
      cmdVerSeleccion(2).Enabled = True
      cmdAnular(0).Enabled = True
      cmdModificar(0).Enabled = True
      cmdVerSolicitud(0).Enabled = True
      cmdVerSolicitud(1).Enabled = True
      cmdRecitarPlan(0).Enabled = True
      cmdRecitarAgenda(0).Enabled = True
      cmdaccionanular.Enabled = True
    Else
      cmdVerFases(0).Enabled = False
      cmdVerFases(2).Enabled = False
      cmdDetalle(1).Enabled = False
      cmdDetalle(2).Enabled = False
      cmdVerSeleccion(1).Enabled = False
      cmdVerSeleccion(2).Enabled = False
      cmdAnular(0).Enabled = False
      cmdModificar(0).Enabled = False
      cmdVerSolicitud(0).Enabled = False
      cmdVerSolicitud(1).Enabled = False
      cmdRecitarPlan(0).Enabled = False
      cmdRecitarAgenda(0).Enabled = False
      cmdaccionanular.Enabled = False
      
    End If
  End If

End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Call pMensajes
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  Dim A�oIn As String
  Dim MesIn As String
  Dim DiaIn As String
  Dim A�oOut As String
  Dim MesOut As String
  Dim DiaOut As String
  Dim SQL As String
  Dim rs As rdoResultset
  Dim sFecHasta As String
  Dim intDestino As Integer
  Dim formula(1, 2)
   
     A�oIn = Format(CDate(dtcDateCombo1(0).Text), "YYYY")
    MesIn = Format(CDate(dtcDateCombo1(0).Text), "MM")
    DiaIn = Format(CDate(dtcDateCombo1(0).Text), "DD")
    
    'A�adimos uno a la fecha hasta para que las incluya
    SQL = "select to_date('" & dtcDateCombo1(1).Text & "','dd/mm/yyyy') + 1 from dual"
    Set rs = objApp.rdoConnect.OpenResultset(SQL, rdOpenKeyset, rdConcurValues)
    sFecHasta = rs(0)
    A�oOut = Format(sFecHasta, "YYYY")
    MesOut = Format(sFecHasta, "MM")
    DiaOut = Format(sFecHasta, "DD")
  If strFormName = "fraFrame1(0)" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      intDestino = .Destination(intReport)
'      If .Destination = 1 Then
'      End If
      If intReport > 0 Then
      Select Case intReport
'        Case 1
'             strWhere = " WHERE PR0432J.AD02CODDPTO = " & cboSSDBCombo1(0).Columns(0).Text & " "
'            If CHKCon(0).Value = False Then
'                If txtText1(0).Text <> "" Then
'                     strWhere = strWhere & " and PR0432j.AG11CODRECURSO_ASI =" & txtText1(0).Text
'                Else
'                    MsgBox "No existe ningun Recurso seleccionado, seleccione uno e intentelo de nuevo", vbOKOnly
'                    Exit Sub
'                End If
'             End If
'             strWhere = strWhere & " and  TRUNC(PR0432j.CI01FECCONCERT) >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')"
'             strWhere = strWhere & " and  TRUNC(PR0432j.CI01FECCONCERT) <= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')"
'             Call .ShowReport(strWhere, strOrder)
        Case 1
             strWhere = " PR0409J.AD02CODDPTO = " & cboSSDBCombo1(0).Columns(0).Text & " " & _
                         "AND CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
                          " AND CI01FECCONCERT <= TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')"
            If chkCon(0).Value = False Then
                If txtText1(0).Text <> "" Then
                     strWhere = strWhere & " and PR0409J.AG11CODRECURSO =" & txtText1(0).Text
                Else
                    MsgBox "No existe ningun Recurso seleccionado, seleccione uno e intentelo de nuevo", vbOKOnly
                    Exit Sub
                End If
             End If
'            strWhere = strWhere & " and  TRUNC(PR0409J.CI01FECCONCERT) >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')"
'            strWhere = strWhere & " and  TRUNC(PR0409J.CI01FECCONCERT) <= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')"
'            Call .ShowReport(strWhere, strOrder)
             formula(1, 1) = "fecha"
             formula(1, 2) = "date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
             Call Imprimir_API(strWhere, "CI024.rpt", formula)
        Case 2
                strWhere = " PR0409J.AD02CODDPTO= " & cboSSDBCombo1(0).Columns(0).Text & " AND " & _
                            "CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
                            " AND CI01FECCONCERT <= TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')"
                'Si han seleccionado un doctor
                If chkCon(0).Value = False Then
                   If txtText1(0).Text <> "" Then
                        strWhere = strWhere + " AND PR0409J.AG11CODRECURSO= " & txtText1(0).Text & " "
                   Else
                       MsgBox "No existe ningun Recurso seleccionado, seleccione uno e intentelo de nuevo", vbOKOnly
                       Exit Sub
                   End If
                End If
'                With crtCrystalReport1
'                    'date(1999,6,14)
'                    .formulas(1) = "fecha = date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
'                    .ReportFileName = objApp.strReportsPath & "CI01R.rpt"
'                    .PrinterCopies = 1
'                    '.Destination = crptToPrinter '21/6/1999  Cambiamos a Pantalla el reporte
'                    .Destination = crptToWindow
'                    .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
'                    .Connect = objApp.rdoConnect.Connect
'                    .DiscardSavedData = True
'                     Me.MousePointer = vbHourglass
'                    .Action = 1
'                     Me.MousePointer = vbDefault
'                End With
                formula(1, 1) = "fecha"
                formula(1, 2) = "date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
                Call Imprimir_API(strWhere, "CI01R.rpt", formula)

        Case 3
                strWhere = " PR0409J.AD02CODDPTO= " & cboSSDBCombo1(0).Columns(0).Text & " AND " & _
                            "CI01FECCONCERT >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')" & _
                            " AND CI01FECCONCERT <= TO_DATE('" & dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI')"
                'Si han seleccionado un doctor
                If chkCon(0).Value = False Then
                   If txtText1(0).Text <> "" Then
                        strWhere = strWhere + " AND PR0409J.AG11CODRECURSO= " & txtText1(0).Text & " "
                   Else
                       MsgBox "No existe ningun Recurso seleccionado, seleccione uno e intentelo de nuevo", vbOKOnly
                       Exit Sub
                   End If
                End If
'                With crtCrystalReport1
'                    'date(1999,6,14)
'                    .formulas(1) = "fecha = date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
'                    .ReportFileName = objApp.strReportsPath & "CI03R.rpt"
'                    .PrinterCopies = 1
'                    '.Destination = crptToPrinter '21/6/1999  Cambiamos a Pantalla el reporte
'                    .Destination = crptToWindow
'                    .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
'                    .Connect = objApp.rdoConnect.Connect
'                    .DiscardSavedData = True
'                     Me.MousePointer = vbHourglass
'                    .Action = 1
'                     Me.MousePointer = vbDefault
'                End With
                ' variable formula: matriz de dos dimensiones, 1�: Nombre de la f�rmula; 2�Contenido
                formula(1, 1) = "fecha"
                formula(1, 2) = "date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
                Call Imprimir_API(strWhere, "CI03R.rpt", formula)
                           
        End Select
      End If
    End With
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabtab1_Click(intIndex As Integer, PreviousTab As Integer)
    Dim intTab As Integer
    Dim strMensaje As String
    'Limpiamos los botones Primera pesta�a
    cmdVerFases(0).Enabled = False
    cmdVerFases(2).Enabled = False
    cmdDetalle(1).Enabled = False
    cmdDetalle(2).Enabled = False
    cmdVerSeleccion(1).Enabled = False
    cmdVerSeleccion(2).Enabled = False
    cmdAnular(0).Enabled = False
    cmdModificar(0).Enabled = False
    cmdVerSolicitud(0).Enabled = False
    cmdVerSolicitud(1).Enabled = False
    cmdRecitarPlan(0).Enabled = False
    cmdRecitarAgenda(0).Enabled = False
    cmdaccionanular.Enabled = False
    If intIndex > 0 Then
        objWinInfo.cllWinForms("fraFrame1(" & PreviousTab + 1 & ")").blnEnabled = False
        objWinInfo.cllWinForms("fraFrame1(" & tabtab1(intIndex).tab + 1 & ")").blnEnabled = True
    End If
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
 ' If btnButton.Key = "b9" And objWinInfo.objWinActiveForm.strName = "fraFrame1(0)" Then
 '   IdPersona1.SearchPersona
 
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   Call objWinInfo.DataRefresh
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)

  If intIndex = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(tabtab1(intIndex)), False, True)
  Else
    
    Me.MousePointer = vbHourglass
    Call objWinInfo.FormChangeActive(fraFrame1(tabtab1(intIndex).tab + 1), False, True)

    Me.MousePointer = vbDefault
  End If
 End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
'   Me.MousePointer = vbHourglass
'   If IntIndex = 9 Then
'      Call objWinInfo.FormChangeActive(fraFrame1(tabTab1(2).Tab + 1), False, True)
'   Else
'       Call objWinInfo.FormChangeActive(fraFrame1(IntIndex), False, True)
'   End If
'   Me.MousePointer = vbDefault
Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub
Private Sub pMensajes()
Dim SQL As String
Dim rsMens As rdoResultset
Dim qry As rdoQuery
Dim imgX As ListImage

   SQL = "SELECT count(*) from  CI0400 WHERE CI21CodPersona = ?" _
   & " and (ci04FecCadMens >= trunc(sysdate+1)" _
   & " Or ci04FecCadMens Is Null)"
     Set qry = objApp.rdoConnect.CreateQuery("", SQL)
     If grdDBGrid1(2).Columns("Persona").Value <> "" Then
        qry(0) = grdDBGrid1(2).Columns("Persona").Value
     Else
        qry(0) = 0
     End If
     Set rsMens = qry.OpenResultset()
     If rsMens(0) > 0 Then
       Set imgX = objApp.imlImageList.ListImages("i44")
       cmdavisos.Picture = imgX.Picture
     Else
       Set imgX = objApp.imlImageList.ListImages("i40")
       cmdavisos.Picture = imgX.Picture
     End If
End Sub

Private Sub cmdVerSolicitud_Click(intIndex As Integer)

  Dim blnCancelTrans      As Boolean
  Dim intGridIndex        As Integer
  Dim lngSolicitud        As Long
  Dim intF                As Integer
  Dim lngActped           As Long
  Dim vntRBookmark        As Variant
   If grdDBGrid1(1).SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
''  If Not blnCompDepto(cboSSDBCombo1(0).Columns(0).Value, intDptoRayos) Then
''        Call objError.SetError(cwCodeMsg, "No puede Modificar Actuaciones de Radiolog�a")
''        vnta = objError.Raise
''        Exit Sub
''  End If
    If Not fblnCitarModiAnular(cboSSDBCombo1(0).Columns(0).Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("CodTipAct").Value, _
        grdDBGrid1(tabtab1(2).tab + 1).Columns("C�digo").Value) Then
           MsgBox "No tiene autorizacion a Modificar esta prueba"
           Exit Sub
   End If
If freserva Then
        Call objError.SetError(cwCodeMsg, "Ha seleccionado una Reserva, debe Seleccionar Alguna Cita", vnta)
        vnta = objError.Raise
Else

'  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans

  intGridIndex = tabtab1(2).tab + 1
  
  'EFS: ACTUALIZAMOS LA RESTRINCION DE USUARIO
  If grdDBGrid1(intGridIndex).SelBookmarks.Count > 0 Then
    For intF = 0 To grdDBGrid1(intGridIndex).SelBookmarks.Count - 1
      vntRBookmark = grdDBGrid1(intGridIndex).SelBookmarks(intF)
      lngActped = grdDBGrid1(intGridIndex).Columns(grdDBGrid1(intGridIndex).Name & "(" & grdDBGrid1(intGridIndex).Index & ").Actuaci�n Pedida").CellValue(vntRBookmark)
      Call sActRestrincion(lngActped)
    Next intF
   End If
  lngSolicitud = grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").Value

  objSolicitud.RemovePeticiones

  Me.MousePointer = vbHourglass
  Call objLoadFromSolicit(lngSolicitud, ciOpcVerSolicitud)
 
  'Load frmSolicitud
  'frmSolicitud.Show vbModal
  'Unload frmSolicitud
  'Set frmSolicitud = Nothing

  'variable pipe referente al pasoIbm **************** 19/01/1999
  Call objPipe.PipeSet("modifSolicitCI1028", lngSolicitud)
  Call objSecurity.LaunchProcess(ciSoluciones)
  Me.MousePointer = vbDefault
  
  If blnCancelTrans Or Not blnCommit Then
 '   objApp.rdoConnect.RollbackTrans
    If blnCancelTrans Then
      Call objError.SetError(cwCodeMsg, ciErrRollback)
      Call objError.Raise
    End If
  Else
 '    objApp.rdoConnect.CommitTrans
     Call objWinInfo.DataRefresh
     Call Me.Refresh
     DoEvents
     blnCommit = False
    
  End If

  Call objGen.RemoveCollection(objSolicitud.cllPeticiones)

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next
End If
End Sub
Private Function CitasChangeStatus(objGrid As SSDBGrid, strNewStatus As String, _
                                  Optional lngincidencia As Long = -1, Optional blnDelSol As Boolean = False) As Integer
  Dim intRow As Integer
  Dim vntRowBookmark As Variant
  Dim lngSolicitud As Long
  Dim lngCita As Long
  Dim strSql As String
  Dim rsActuacionCitada As rdoResultset
  Dim intRowsChanged As Integer
  Dim rdPruebas As rdoResultset
  Dim qry As rdoQuery
   
  intRowsChanged = 0
  If objGrid.SelBookmarks.Count > 0 Then
    For intRow = 0 To objGrid.SelBookmarks.Count - 1
      vntRowBookmark = objGrid.SelBookmarks(intRow)
      lngSolicitud = objGrid.Columns(objGrid.Name & "(" & objGrid.Index & ").Solicitud").CellValue(vntRowBookmark)
      lngCita = objGrid.Columns(objGrid.Name & "(" & objGrid.Index & ").N�mero Cita").CellValue(vntRowBookmark)
      If Not blnDelSol Then
        strSql = "Select ROWID,CI01SITCITA,AD15CODCAMA,AG05NUMINCIDEN,PR04NUMACTPLAN " & _
               "from CI0100 " & _
               "where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita
      Else
        strSql = "Select ROWID,CI01SITCITA,AD15CODCAMA,AG05NUMINCIDEN,PR04NUMACTPLAN " & _
               "from CI0100 " & _
               "where CI31NUMSOLICIT = " & lngSolicitud
      End If
      Set rsActuacionCitada = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, _
                              rdConcurRowVer)
      If rsActuacionCitada.RowCount > 0 Then
        
       While Not rsActuacionCitada.EOF
         'rsActuacionCitada.Edit
         strSql = "UPDATE CI0100 SET CI01SITCITA=" & strNewStatus & ","
         'rsActuacionCitada.rdoColumns("CI01SITCITA") = strNewStatus
         If lngincidencia <> -1 Then
            strSql = strSql & "AG05NUMINCIDEN=" & lngincidencia
         End If
        If Not blnDelSol Then
            strSql = strSql & " where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita
        Else
            strSql = strSql & " where CI31NUMSOLICIT = " & lngSolicitud
        'Else
            'rsActuacionCitada.rdoColumns("AG05NUMINCIDEN") = lngIncidencia
         End If
         Set qry = objApp.rdoConnect.CreateQuery("", strSql)
         qry.Execute
         qry.Close
         If Not IsNull(rsActuacionCitada.rdoColumns("AD15CODCAMA")) Then
            Call bDBUpdateCamaAnuReservada(rsActuacionCitada.rdoColumns("AD15CODCAMA"), rsActuacionCitada.rdoColumns("PR04NUMACTPLAN"))
        End If
         
         'Actualizando campo PR37CODESTADO
         strSql = "Select PR04NUMACTPLAN,PR37CODESTADO from PR0400 " _
            & " where PR04NUMACTPLAN=" & rsActuacionCitada.rdoColumns("PR04NUMACTPLAN").Value
         Set rdPruebas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
         If Not rdPruebas.EOF Then
            'rdPruebas.Edit
            If strNewStatus = ciAnulada Then
                strSql = "UPDATE PR0400 SET PR37CODESTADO=6 WHERE PR04NUMACTPLAN=" & rsActuacionCitada.rdoColumns("PR04NUMACTPLAN").Value
                'rdPruebas("PR37CODESTADO") = 6 'Anulada
                Set qry = objApp.rdoConnect.CreateQuery("", strSql)
                qry.Execute
                qry.Close
            End If
            'rdPruebas.Update
            rdPruebas.Close
         End If
         Set rdPruebas = Nothing
         'Fin actualizaci�n de PR37CODESTADO
         
         'rsActuacionCitada.Update
         rsActuacionCitada.MoveNext
       Wend
      End If
      rsActuacionCitada.Close
      intRowsChanged = intRowsChanged + 1
    Next
  End If
  Set rsActuacionCitada = Nothing

  CitasChangeStatus = intRowsChanged

End Function



Private Sub txtText1_Change(Index As Integer)
Dim intPos As Integer

  Call objWinInfo.CtrlDataChange
  If Index = 0 Then
    For intPos = 1 To 2
      grdDBGrid1(intPos).RemoveAll
    Next
  dtcDateCombo1(0).Text = strFecha_Sistema()
'  dtcDateCombo1(1).Date = DateAdd("m", 1, strFecha_Sistema())
    dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
'  Format(dtcDateCombo1(0).Text, "DD/MM/YYYY")
    objWinInfo.cllWinForms("fraFrame1(1)").strWhere = "AG11CODRECURSO_ASI=0"
    objWinInfo.cllWinForms("fraFrame1(2)").strWhere = "AG11CODRECURSO_ASI=0"
'    objWinInfo.cllWinForms("fraFrame1(1)").strInitialWhere = objWinInfo.cllWinForms("fraFrame1(1)").strWhere
  End If
End Sub
Public Function freserva() As Boolean
    If Trim(grdDBGrid1(1).Columns(13).Value) = "5" Then
        freserva = True
    Else
        freserva = False
    End If
End Function
Private Sub pImprimirCartas(lngActPlan As Long)
   Dim formulas(1, 2)
'    With crtCrystalReport1
'        .Formulas(1) = "motivo = '" & strMensaje & "'"
'        .ReportFileName = objApp.strReportsPath & "Cartas.rpt"
'        .SelectionFormula = "{PR0465J.PR04NUMACTPLAN} = " & lngActPlan
'        .PrinterCopies = 1
'        .Destination = crptToPrinter
'
'        .Connect = objApp.rdoConnect.Connect
'        .DiscardSavedData = True
'        Me.MousePointer = vbHourglass
'        .Action = 1
'        Me.MousePointer = vbDefault
'    End With
'Variable Formula: matriz de dos dimensiones, 1�: Nombre de la f�rmula; 2�: Contenido
formulas(1, 1) = "motivo"
formulas(1, 2) = "'" & strMensaje & "'"

Call Imprimir_API("PR0465J.PR04NUMACTPLAN = " & lngActPlan, "CARTAS.RPT", formulas)
End Sub

