VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmCWMainMenu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sistema de Informaci�n de la Cl�nica Universitaria"
   ClientHeight    =   7215
   ClientLeft      =   2175
   ClientTop       =   2895
   ClientWidth     =   11325
   ClipControls    =   0   'False
   HelpContextID   =   101
   Icon            =   "MEFRM00.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7215
   ScaleWidth      =   11325
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11325
      _ExtentX        =   19976
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlMainSmall"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   9
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Refrescar"
            Object.Tag             =   ""
            ImageKey        =   "refresh"
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Iconos grandes"
            Object.Tag             =   ""
            ImageKey        =   "big"
            Style           =   2
            Value           =   1
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Iconos peque�os"
            Object.Tag             =   ""
            ImageKey        =   "small"
            Style           =   2
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Lista"
            Object.Tag             =   ""
            ImageKey        =   "list"
            Style           =   2
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Detalles"
            Object.Tag             =   ""
            ImageKey        =   "details"
            Style           =   2
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.ToolTipText     =   "Salir del sistema"
            Object.Tag             =   ""
            ImageKey        =   "exit"
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ListView lvwItems 
      Height          =   5235
      Left            =   5850
      TabIndex        =   1
      Top             =   540
      Width           =   4065
      _ExtentX        =   7170
      _ExtentY        =   9234
      Arrange         =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   327682
      Icons           =   "imlMainBig"
      SmallIcons      =   "imlMainSmall"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.PictureBox picSplitter 
      BorderStyle     =   0  'None
      Height          =   5100
      Left            =   4995
      MousePointer    =   9  'Size W E
      ScaleHeight     =   5100
      ScaleWidth      =   45
      TabIndex        =   2
      Top             =   540
      Width           =   50
   End
   Begin ComctlLib.TreeView tvwItems 
      Height          =   5235
      Left            =   225
      TabIndex        =   0
      Top             =   495
      Width           =   4200
      _ExtentX        =   7408
      _ExtentY        =   9234
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "imlMainSmall"
      Appearance      =   1
   End
   Begin Crystal.CrystalReport crCrystalReport 
      Left            =   3555
      Top             =   6165
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   2925
      Top             =   6120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin ComctlLib.ImageList imlMainBig 
      Left            =   855
      Top             =   6030
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin ComctlLib.ImageList imlMainSmall 
      Left            =   225
      Top             =   6030
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   14
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":030A
            Key             =   "main"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":0624
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":093E
            Key             =   "function"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":0C58
            Key             =   "window"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":0F72
            Key             =   "report"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":128C
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":15A6
            Key             =   "process"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":18C0
            Key             =   "application"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":1BDA
            Key             =   "big"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":1EF4
            Key             =   "small"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":220E
            Key             =   "list"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":2528
            Key             =   "details"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":2842
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MEFRM00.frx":2B5C
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList imlImageList 
      Left            =   2250
      Top             =   6030
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Refrescar"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Cambio de Usuario..."
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   40
      End
   End
   Begin VB.Menu mnuVer 
      Caption         =   "&Ver"
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Barra de herramientas"
         Checked         =   -1  'True
         Index           =   10
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "Iconos &grandes"
         Checked         =   -1  'True
         Index           =   30
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Iconos peque�os"
         Index           =   40
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Lista"
         Index           =   50
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Detalles"
         Index           =   60
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de..."
         Index           =   30
      End
   End
   Begin VB.Menu mnuPropiedades 
      Caption         =   "&Propiedades"
      Visible         =   0   'False
      Begin VB.Menu mnuPropiedadesOpcion 
         Caption         =   "&Abrir..."
         Index           =   10
      End
      Begin VB.Menu mnuPropiedadesOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuPropiedadesOpcion 
         Caption         =   "&Propiedades"
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmCWMainMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWSecure
' Coded by Manu Roibal
' **********************************************************************************


Const cwMsgMainExit  As String = "�Desea realmente salir del sistema?"


Dim mstrCaption   As String
Dim mblnMoving    As Boolean
Dim mobjSelected  As Object
Dim mblnTree      As Boolean
Dim blnShowed     As Boolean


Private Sub Form_Load()
  Dim strEntorno  As String
  Dim strUser     As String
  Dim strPassword As String
  Dim picImage    As IImage
  
  If InitApp Then
    Call FormResize
  
    With objApp
      Call .Register(App, Screen)
      Set .frmFormMain = Me
      Set .imlImageList = imlImageList
      Set .crCrystal = crCrystalReport
      Set .dlgCommon = dlgCommonDialog
    
      .blnUseRegistry = True
      .blnAskPassword = False
      .strUserName = objSecurity.GetDataBaseUser
      .strPassword = objSecurity.GetDataBasePassword
    End With

    strEntorno = "Entorno de Explotaci�n"
    Call objEnv.AddEnv(strEntorno)
    Call objEnv.AddValue(strEntorno, "DataBase", "")

    For Each picImage In imlMainSmall.ListImages
      Call imlMainBig.ListImages.Add(, picImage.Key, picImage.Picture)
    Next
  
    mstrCaption = Me.Caption
  
    If Not objApp.CreateInfo Then
      Unload Me
    Else
      If Not objSecurity.Identify Then
        Unload Me
      Else
        blnShowed = True
        Me.Caption = mstrCaption & ", " & objSecurity.strFullName
        Call objSecurity.LoadInfo
        Call objSecurity.CreateTreeView(tvwItems, lvwItems, imlMainSmall, imlMainBig)
      End If
    End If
  Else
    Unload Me
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  If blnShowed Then
    Call objError.SetError(cwCodeQuery, cwMsgMainExit)
    If objError.Raise <> vbYes Then
      intCancel = 1
    Else
      Call objApp.RemoveInfo
    End If
  End If
End Sub

Private Sub Form_Resize()
  Call FormResize
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objSecurity.LoadInfo
      Call objSecurity.CreateTreeView(tvwItems, lvwItems, imlMainSmall, imlMainBig)
    Case 20
      Call objSecurity.Identify
      Me.Caption = mstrCaption & ", " & objSecurity.strFullName
      Call objSecurity.LoadInfo
      Call objSecurity.CreateTreeView(tvwItems, lvwItems, imlMainSmall, imlMainBig)
    Case 40
      Unload Me
  End Select
End Sub

Private Sub mnuVerOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      mnuVerOpcion(10).Checked = Not mnuVerOpcion(10).Checked
      tlbToolBar.Visible = mnuVerOpcion(10).Checked
      Call FormResize
    Case 30
      tlbToolBar.Buttons(4).Value = tbrPressed
      Call tlbToolBar_ButtonClick(tlbToolBar.Buttons(4))
    Case 40
      tlbToolBar.Buttons(5).Value = tbrPressed
      Call tlbToolBar_ButtonClick(tlbToolBar.Buttons(5))
    Case 50
      tlbToolBar.Buttons(6).Value = tbrPressed
      Call tlbToolBar_ButtonClick(tlbToolBar.Buttons(6))
    Case 60
      tlbToolBar.Buttons(7).Value = tbrPressed
      Call tlbToolBar_ButtonClick(tlbToolBar.Buttons(7))
  End Select
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 30
      Call objApp.About
  End Select
End Sub


Private Sub mnuPropiedadesOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objSecurity.Dispatcher(mobjSelected, mblnTree)
    Case 30
      Call objSecurity.ShowProperties(mobjSelected, mblnTree, imlMainBig)
  End Select
End Sub


Private Sub tlbToolBar_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Select Case btnButton.Index
    Case 2
      Call objSecurity.LoadInfo
      Call objSecurity.CreateTreeView(tvwItems, lvwItems, imlMainSmall, imlMainBig)
    Case 4
      lvwItems.View = lvwIcon
      Call EnableMenuMode(30)
    Case 5
      lvwItems.View = lvwSmallIcon
      Call EnableMenuMode(40)
    Case 6
      lvwItems.View = lvwList
      Call EnableMenuMode(50)
    Case 7
      lvwItems.View = lvwReport
      Call EnableMenuMode(60)
    Case 9
      Unload Me
  End Select
End Sub


Private Sub EnableMenuMode(ByVal intNumber As Integer)
  mnuVerOpcion(30).Checked = False
  mnuVerOpcion(40).Checked = False
  mnuVerOpcion(50).Checked = False
  mnuVerOpcion(60).Checked = False
  mnuVerOpcion(intNumber).Checked = True
End Sub


Private Sub FormResize()
  Dim intTop   As Integer
  Dim intHeight As Integer
  
  If WindowState <> vbMinimized Then
    If tlbToolBar.Visible Or Not Me.Visible Then
      intTop = tlbToolBar.Height
      intHeight = Me.ScaleHeight - tlbToolBar.Height
    Else
      intTop = 0
      intHeight = Me.ScaleHeight
    End If
  
    With tvwItems
      .Top = intTop
      .Left = 0
      .Height = intHeight
    End With
    With picSplitter
      .Top = intTop
      .Left = tvwItems.Width
      .Height = intHeight
    End With
    With lvwItems
      .Top = intTop
      .Left = tvwItems.Width + picSplitter.Width
      .Width = Me.ScaleWidth - .Left
      .Height = intHeight
    End With
  End If
End Sub


Private Sub picSplitter_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  mblnMoving = True
End Sub

Private Sub picSplitter_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
  mblnMoving = False
End Sub

Private Sub picSplitter_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  Dim lngSize As Long
  
  If mblnMoving Then
    lngSize = tvwItems.Width
    If tvwItems.Width + x > 1000 Then
      lngSize = tvwItems.Width + x
    End If
    If tvwItems.Width + x > Me.ScaleWidth - 1000 Then
      lngSize = Me.ScaleWidth - 1000
    End If
    If tvwItems.Width <> lngSize Then
      tvwItems.Width = lngSize
      Call FormResize
    End If
  End If
End Sub


Private Sub tvwItems_KeyPress(intKeyAscii As Integer)
  If intKeyAscii = vbKeyReturn Then
    Call objSecurity.Dispatcher(tvwItems.SelectedItem, True)
  End If
End Sub

Private Sub tvwItems_NodeClick(ByVal nodNode As ComctlLib.Node)
  Call objSecurity.CreateListView(tvwItems, lvwItems, imlMainSmall, imlMainBig, nodNode)
End Sub

Private Sub tvwItems_DblClick()
  Call objSecurity.Dispatcher(tvwItems.SelectedItem, True)
End Sub

Private Sub tvwItems_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Not tvwItems.HitTest(x, y) Is Nothing Then
    tvwItems.HitTest(x, y).Selected = True
  End If
  If Button = vbRightButton And Shift = 0 Then
    With tvwItems
      If Not .SelectedItem Is Nothing Then
        If .SelectedItem.Selected And .SelectedItem.Children = 0 And objGen.NodeGetLevel(.SelectedItem) > 1 Then
          Set mobjSelected = .SelectedItem
          mblnTree = True
          Call PopupMenu(mnuPropiedades, _
                         vbPopupMenuLeftAlign Or vbPopupMenuRightButton, _
                         x + 10, tvwItems.Top + y + 10, mnuPropiedadesOpcion(10))
        End If
      End If
    End With
  End If
End Sub


Private Sub lvwItems_KeyPress(intKeyAscii As Integer)
  If intKeyAscii = vbKeyReturn Then
    Call objSecurity.Dispatcher(lvwItems.SelectedItem, False)
  End If
End Sub

Private Sub lvwItems_DblClick()
  Call objSecurity.Dispatcher(lvwItems.SelectedItem, False)
End Sub

Private Sub lvwItems_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Not lvwItems.HitTest(x, y) Is Nothing Then
    lvwItems.HitTest(x, y).Selected = True
  End If
  If Button = vbRightButton And Shift = 0 Then
    With lvwItems
      If Not .SelectedItem Is Nothing Then
        If .SelectedItem.Selected Then
          Set mobjSelected = .SelectedItem
          mblnTree = False
          Call PopupMenu(mnuPropiedades, _
                         vbPopupMenuLeftAlign Or vbPopupMenuRightButton, _
                         lvwItems.Left + x + 10, tvwItems.Top + y + 10, mnuPropiedadesOpcion(10))
        End If
      End If
    End With
  End If
End Sub


