VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWKey"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Class clsCWKey
' Coded by SYSECA Bilbao
' **********************************************************************************


' esta estructura identifica a cada control
' de datos que se incluye en la primary key
' u 'order by' suministrando datos sobre el mismo


Public strFieldName As String           ' nombre del campo clave o de order by
Public strValue     As String           ' valor que tiene el mismo en un momento dado
Public intType      As Integer          ' tipo de campo
Public blnDirection As Boolean          ' false = ASC, true = DESC
Public intFunction  As Integer          ' La funci�n sql
Public cllFields    As New Collection   ' colecci�n de campos de la funci�n
Public cllValues    As New Collection   ' colecci�n de valores equivalentes
Public cllTypes     As New Collection   ' colecci�n de tipos equivalentes

