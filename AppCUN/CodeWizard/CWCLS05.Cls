VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWCtrl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWCtrl
' Coded by SYSECA Bilbao
' **********************************************************************************


' tipos de datos de los datos internos de las columnas
Public Enum cwDataType
  cwString = vbString
  cwNumeric = vbDecimal
  cwDecimal = vbDouble
  cwBoolean = vbBoolean
  cwDate = vbDate
  cwVariant = vbVariant
End Enum

' mascaras para los controles de datos
Public Enum cwMaskType
  cwMaskString = 1
  cwMaskUpper = 2
  cwMaskLower = 3
  cwMaskInteger = 4
  cwMaskReal = 5
End Enum


Public blnMandatory    As Boolean              ' obligatorio?
Public blnBlocked      As Boolean              ' bloqueado?
Public vntDefaultValue As Variant              ' valor por defecto
Public vntLastValue    As Variant              ' ultimo valor
Public intKeyNo        As Integer              ' posici�n en la clave � 0
Public strSmallDesc    As String               ' descripci�n corta
Public strBigDesc      As String               ' descripci�n larga
Public blnInFind       As Boolean              ' interviene en b�squedas
Public intType         As cwDataType           ' tipo de datos
Public intRDOType      As Integer              ' tipo de datos RDO
Public intSize         As Integer              ' tama�o del campo
Public intDecs         As Integer              ' n�mero de decimales
Public blnReadOnly     As Boolean              ' es de salida o entrada/salida
Public blnValidate     As Boolean              ' se debe validar el control
Public blnAllowSpaces  As Boolean              ' espacio en blanco es nulo?
Public objControl      As Object               ' el control en si mismo
Public vntMinValue     As Variant              ' valor m�nimo para num�ricos
Public vntMaxValue     As Variant              ' valor m�ximo para num�ricos
Public intMask         As cwMaskType           ' mascara, por defecto STRING
Public blnInGrid       As Boolean              ' determina si una columna ir� al grid
Public blnForeign      As Boolean              ' tiene mantenimiento
Public blnNegotiated   As Boolean              ' indica si un control es gestionado por la librer�a
Public strSQL          As String               ' la SQL asociada para obtener informaci�n de otra tabla
Public objLinked       As New clsCWLinked      ' almacena la informaci�n sobre los campos relacionados
Public objItemData     As New clsCWComboBox    ' almacena las claves de los combobox
Public strName         As String               ' nombre del control


Private Sub Class_Initialize()
  blnNegotiated = True
End Sub


Private Sub Class_Terminate()
  With objGen
    Call .RemoveCollection(objLinked.cllLinkedControls)
    Call .RemoveCollection(objLinked.cllLinkedColumns)
  End With
  On Error Resume Next
  If objLinked.blnHasLinked = True Then
    objLinked.rdoLinkedQuery.Close 'No hay query
    Set objLinked.rdoLinkedQuery = Nothing
    objLinked.rdoLinkedCursor.Close
    Set objLinked.rdoLinkedCursor = Nothing
  End If
End Sub
