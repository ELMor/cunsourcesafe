VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCW
' Coded by Manu Roibal
' **********************************************************************************


' variables p�blicas. Dan significado al sistema
Public objApp      As New clsCWApp             ' objecto clsCWApp de l a Aplicaci�n
Public objPipe     As New clsCWPipeLine        ' objeto clsCWPipe de la Aplicaci�n
Public objGen      As New clsCWGen             ' objeto clsCWGen de la Aplicaci�n
Public objError    As New clsCWError
Public objEnv      As New clsCWEnvironment
Public objMouse    As New clsCWMouse
Public objSecurity As New clsCWSecurity


Private Sub Class_Initialize()
  Set System.objCW = Me
  Set System.objGen = Me.objGen
  Set System.objApp = Me.objApp
  Set System.objPipe = Me.objPipe
  Set System.objError = Me.objError
  Set System.objEnv = Me.objEnv
  Set System.objMouse = Me.objMouse
  Set System.objSecurity = Me.objSecurity
End Sub

Private Sub Class_Terminate()
'  Call Me.objApp.RemoveInfo
  Set System.objApp = Nothing
  Set System.objPipe = Nothing
  Set System.objPipe = Nothing
  Set System.objEnv = Nothing
  Set System.objMouse = Nothing
  Set System.objSecurity = Nothing
  Set System.objGen = Nothing
  Set System.objCW = Nothing
End Sub

