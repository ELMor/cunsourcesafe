VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWTreeLevel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWTreeLevel
' Coded by Manu Roibal
' **********************************************************************************


Public Enum cwTreeLevelType
  cwTreeLevelTypeSQL = 1
  cwTreeLevelTypeTables = 2
  cwTreeLevelTypeColumns = 3
  cwTreeLevelTypeNothing = 4
End Enum


Public intType    As cwTreeLevelType
Public strText    As String
Public objSource  As clsCWTreeSource
