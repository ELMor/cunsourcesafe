VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmCWSearch 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Localizar"
   ClientHeight    =   2880
   ClientLeft      =   1770
   ClientTop       =   4320
   ClientWidth     =   8175
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CWFRM18.frx":0000
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2880
   ScaleWidth      =   8175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtLocaText 
      Height          =   330
      Index           =   0
      Left            =   2115
      TabIndex        =   9
      Top             =   180
      Visible         =   0   'False
      Width           =   3500
   End
   Begin VB.ComboBox cboLocaBool 
      Height          =   315
      Index           =   0
      ItemData        =   "CWFRM18.frx":000C
      Left            =   2115
      List            =   "CWFRM18.frx":0019
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   675
      Visible         =   0   'False
      Width           =   2670
   End
   Begin VB.ComboBox cboSearch 
      Height          =   315
      ItemData        =   "CWFRM18.frx":003F
      Left            =   5760
      List            =   "CWFRM18.frx":004C
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   2475
      Width           =   2310
   End
   Begin VB.CommandButton cmdButton 
      Caption         =   "C&onsultar"
      Height          =   375
      Index           =   3
      Left            =   6480
      TabIndex        =   4
      Top             =   1125
      Visible         =   0   'False
      Width           =   1545
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcLocaDate 
      Height          =   315
      Index           =   0
      Left            =   2115
      TabIndex        =   1
      Top             =   1170
      Visible         =   0   'False
      Width           =   1995
      _Version        =   65537
      _ExtentX        =   3519
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1899/1/1"
      MaxDate         =   "2099/12/31"
      Format          =   "DD/MM/YYYY"
      stylesets.count =   1
      stylesets(0).Name=   "Festivos"
      stylesets(0).ForeColor=   255
      stylesets(0).Picture=   "CWFRM18.frx":009D
      DayOfWeek(1).StyleSet=   "Festivos"
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      BeepOnError     =   0   'False
      StartofWeek     =   2
   End
   Begin VB.CheckBox chkCase 
      Caption         =   "MAY�SCULAS/Min�sculas"
      Height          =   255
      Left            =   5760
      TabIndex        =   6
      Top             =   2160
      Width           =   2310
   End
   Begin VB.CommandButton cmdButton 
      Cancel          =   -1  'True
      Caption         =   "&Cerrar"
      Height          =   375
      Index           =   2
      Left            =   6480
      TabIndex        =   5
      Top             =   1665
      Width           =   1545
   End
   Begin VB.CommandButton cmdButton 
      Caption         =   "Buscar &siguiente"
      Height          =   375
      Index           =   1
      Left            =   6480
      TabIndex        =   3
      Top             =   585
      Width           =   1545
   End
   Begin VB.CommandButton cmdButton 
      Caption         =   "&Buscar primero"
      Default         =   -1  'True
      Height          =   375
      Index           =   0
      Left            =   6480
      TabIndex        =   2
      Top             =   135
      Width           =   1545
   End
   Begin VB.Label lblLocaLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Etiqueta "
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   150
      TabIndex        =   8
      Top             =   180
      Visible         =   0   'False
      Width           =   1845
   End
End
Attribute VB_Name = "frmCWSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWSearch
' Coded by Manu Roibal
' **********************************************************************************


Const intConstVerticalSize As Integer = 400
Const intConstVerticalInit As Integer = 250


Dim mcllFields   As New Collection
Dim mcllInfo     As New Collection
Dim mcllValues   As Collection
Dim mintMaxCombo As Integer
Dim mintMaxText  As Integer
Dim mintMaxDate  As Integer
Dim mintMaxLabel As Integer
Dim mintTop      As Integer


Public blnInternalMode As Boolean
Public objZoom         As clsCWSearch
Public strZoom         As String
Public strZoomName     As String


Private Sub Form_Load()
  mintMaxCombo = 1
  mintMaxText = 1
  mintMaxLabel = 1
  mintMaxDate = 1
  mintTop = intConstVerticalInit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  If blnInternalMode Then
    With objApp.objActiveWin.objWinActiveForm
      .intSearchLeft = Left
      .intSearchTop = Top
    End With
  End If
  
  Do While mcllFields.Count > 0
    If Not (mcllFields(1).objControl Is Nothing) Then
      Unload mcllFields(1).objControl
    End If
    Call mcllFields.Remove(1)
  Loop
  
  Call objGen.RemoveCollection(mcllInfo)
  Call objMouse.BusyOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Unload")
End Sub

Private Sub cmdButton_Click(intIndex As Integer)
  Dim blnFound As Boolean
  Dim blnFirst As Boolean
  
  On Error GoTo cwIntError
  
  Select Case intIndex
    Case 0, 1
      If CheckValues Then
        Call objMouse.BusyOn
        blnFirst = (intIndex = 0)
        Select Case objApp.objActiveWin.objWinActiveForm.intFormType
          Case cwFormDetail
            If objApp.objActiveWin.objWinActiveForm.blnFilterOn Then
              blnFound = FindCursor(objApp.objActiveWin, blnFirst)
            Else
              blnFound = FindSQL(objApp.objActiveWin, blnFirst)
            End If
          Case cwFormMultiLine
            blnFound = FindGrid(objApp.objActiveWin, blnFirst)
        End Select
        If Not blnFound Then
          With objError
            Call .SetError(cwCodeMsg, msgUsrSearchNotFound)
            Call .Raise
          End With
        Else
          Select Case objApp.objActiveWin.objWinActiveForm.intFormType
            Case cwFormDetail
              With objApp.objActiveWin
                Call .FormChangeStatus(cwModeSingleEdit)
                Call .DataRead
                Call .WinPrepareScr
              End With
            Case cwFormMultiLine
              With objApp.objActiveWin
                Call .FormChangeStatus(cwModeMultiLineEdit)
                Call .WinPrepareScr
              End With
          End Select
          Call cmdButton(1).SetFocus
        End If
        Call objMouse.BusyOff
      End If
    Case 2
      Call cmdButton(2).SetFocus
      Call Hide
    Case 3
      If blnInternalMode Then
        With objApp.objActiveWin
          Select Case .objWinActiveForm.intFormType
            Case cwFormDetail
              If Not .objWinActiveForm.blnFilterOn Then
                If ViewSQL(objApp.objActiveWin) Then
                  Call .FormChangeStatus(cwModeSingleEdit)
                  Call .DataRead
                  Call .WinPrepareScr
                  Call Me.Hide
                End If
              End If
            Case cwFormMultiLine
          End Select
        End With
      Else
        If ViewSQL(objApp.objActiveWin) Then
          If Me.Visible Then
            Call Me.Hide
          End If
        End If
      End If
  End Select
  
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "ButtonClick")
End Sub

Friend Sub AddControl(ByVal objField As clsCWFieldSearch)
  If objField.blnInDialog Then
    Load lblLocaLabel(mintMaxLabel)
    With lblLocaLabel(mintMaxLabel)
      .Enabled = True
      .Visible = True
      .Caption = objField.strSmallDesc
      .Top = mintTop
    End With
    mintMaxLabel = mintMaxLabel + 1
  
    Select Case objField.intType
      Case cwNumeric, cwDecimal, cwString
        Load txtLocaText(mintMaxText)
        With txtLocaText(mintMaxText)
          .Enabled = True
          .Visible = True
          .MaxLength = objField.intSize
          .Text = ""
          .Top = mintTop
          .Width = objGen.GetMin((.MaxLength + 5) * cwCharWidth, 3400)
          .DataField = objField.strField
          .Tag = objField.strField
          .TabIndex = mcllFields.Count
          .ToolTipText = objField.strBigDesc
        End With
        Set objField.objControl = txtLocaText(mintMaxText)
        mintMaxText = mintMaxText + 1
      Case cwBoolean
        Load cboLocaBool(mintMaxCombo)
        With cboLocaBool(mintMaxCombo)
          .Enabled = True
          .Visible = True
          Call .AddItem(cboLocaBool(0).List(0))
          Call .AddItem(cboLocaBool(0).List(1))
          Call .AddItem(cboLocaBool(0).List(2))
          .ListIndex = 0
          .Top = mintTop
          .DataField = objField.strField
          .Tag = objField.strField
          .TabIndex = mcllFields.Count
          .ToolTipText = objField.strBigDesc
        End With
        Set objField.objControl = cboLocaBool(mintMaxCombo)
        mintMaxCombo = mintMaxCombo + 1
      Case cwDate
        Load dtcLocaDate(mintMaxDate)
        With dtcLocaDate(mintMaxDate)
          .Enabled = True
          .Visible = True
          .Top = mintTop
          .DataField = objField.strField
          .Tag = objField.strField
          .TabIndex = mcllFields.Count
          .ToolTipText = objField.strBigDesc
'          .Mask =
'          .Format =
          .StartOfWeek = vbMonday
          .ShowCentury = True
        End With
        Set objField.objControl = dtcLocaDate(mintMaxDate)
        mintMaxDate = mintMaxDate + 1
    End Select
    mintTop = mintTop + intConstVerticalSize
  End If
  
  Call mcllFields.Add(objField, objField.strField)
  
End Sub

Friend Sub CreateInfo(ByVal strTable As String, _
                      ByVal strWhere As String, _
                      ByVal strOrder As String, _
                      ByVal cllValues As Collection)
  Dim objField  As clsCWFieldSearch
  Dim strFields As String
  
  For Each objField In mcllFields
    strFields = strFields & objField.strField & ","
  Next
  strFields = Left(strFields, Len(strFields) - 1)
  With mcllInfo
    Call .Add(strFields)
    Call .Add(strTable)
    Call .Add(strWhere)
    Call .Add(strOrder)
  End With
  Set mcllValues = cllValues
  Call objGen.RemoveCollection(mcllValues)
End Sub

Friend Function Prepare() As Boolean
  Dim objCtrl      As clsCWCtrl
  Dim objForm      As clsCWForm
  Dim objWin       As clsCWWin
  Dim intMiddleScr As Integer
  Dim intMiddleWin As Integer
  Dim objField     As New clsCWFieldSearch
  Dim strKey       As String
  
  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  If blnInternalMode Then
    Set objWin = objApp.objActiveWin
    Set objForm = objWin.objWinActiveForm
  
    For Each objCtrl In objForm.cllControls
      If objCtrl.blnInFind Then
        Select Case objWin.objWinActiveForm.intFormType
          Case cwFormDetail
            Load lblLocaLabel(mintMaxLabel)
            With lblLocaLabel(mintMaxLabel)
              .Enabled = True
              .Visible = True
              .Caption = objCtrl.strSmallDesc
              .Top = mintTop
            End With
            mintMaxLabel = mintMaxLabel + 1
            Select Case objCtrl.intType
              Case cwNumeric, cwDecimal, cwString
                Load txtLocaText(mintMaxText)
                With txtLocaText(mintMaxText)
                  .Enabled = True
                  .Visible = True
                  .MaxLength = objCtrl.objControl.MaxLength
                  .Text = ""
                  .Top = mintTop
                  .Width = objGen.GetMin(objCtrl.objControl.Width, 3400)
                  .DataField = objCtrl.objControl.DataField
                  strKey = objWin.CtrlGetKey(objCtrl.objControl)
                  .Tag = strKey
                  .TabIndex = mcllFields.Count
                  .ToolTipText = objCtrl.strBigDesc
                  
                  With objField
                    objField.strField = strKey
                    .intMask = objCtrl.intMask
                    .intType = objCtrl.intType
                    .strSmallDesc = objCtrl.strSmallDesc
                    Set .objControl = txtLocaText(mintMaxText)
                  End With
                  Call mcllFields.Add(objField, strKey)
                  Set objField = Nothing
                End With
                mintMaxText = mintMaxText + 1
              Case cwBoolean
                Load cboLocaBool(mintMaxCombo)
                With cboLocaBool(mintMaxCombo)
                  .Enabled = True
                  .Visible = True
                  Call .AddItem(cboLocaBool(0).List(0))
                  Call .AddItem(cboLocaBool(0).List(1))
                  Call .AddItem(cboLocaBool(0).List(2))
                  .ListIndex = 0
                  .Top = mintTop
                  .DataField = objCtrl.objControl.DataField
                  strKey = objWin.CtrlGetKey(objCtrl.objControl)
                  .Tag = strKey
                  .TabIndex = mcllFields.Count
                  .ToolTipText = objCtrl.strBigDesc
                  
                  With objField
                    objField.strField = strKey
                    .intMask = objCtrl.intMask
                    .intType = objCtrl.intType
                    .strSmallDesc = objCtrl.strSmallDesc
                    Set .objControl = cboLocaBool(mintMaxCombo)
                  End With
                  Call mcllFields.Add(objField, strKey)
                  Set objField = Nothing
                End With
                mintMaxCombo = mintMaxCombo + 1
              Case cwDate
                Load dtcLocaDate(mintMaxDate)
                With dtcLocaDate(mintMaxDate)
                  .Enabled = True
                  .Visible = True
                  .Top = mintTop
                  .DataField = objCtrl.objControl.DataField
                  strKey = objWin.CtrlGetKey(objCtrl.objControl)
                  .Tag = strKey
                  .TabIndex = mcllFields.Count
                  .ToolTipText = objCtrl.strBigDesc
                  .Mask = objCtrl.objControl.Mask
                  .Format = objCtrl.objControl.Format
                  .StartOfWeek = objCtrl.objControl.StartOfWeek
                  .ShowCentury = objCtrl.objControl.ShowCentury
                  
                  With objField
                    objField.strField = strKey
                    .intMask = objCtrl.intMask
                    .intType = objCtrl.intType
                    .strSmallDesc = objCtrl.strSmallDesc
                    Set .objControl = dtcLocaDate(mintMaxDate)
                  End With
                  Call mcllFields.Add(objField, strKey)
                  Set objField = Nothing
                End With
                mintMaxDate = mintMaxDate + 1
            End Select
          Case cwFormMultiLine
            Load lblLocaLabel(mintMaxLabel)
            With lblLocaLabel(mintMaxLabel)
              .Enabled = True
              .Visible = True
              .Caption = objCtrl.strSmallDesc
              .Top = mintTop
            End With
            mintMaxLabel = mintMaxLabel + 1
            Select Case objCtrl.intType
              Case cwNumeric, cwDecimal, cwString
                Load txtLocaText(mintMaxText)
                With txtLocaText(mintMaxText)
                  .Enabled = True
                  .Visible = True
                  .MaxLength = objCtrl.objControl.FieldLen
                  .Text = ""
                  .Top = mintTop
                  .Width = objGen.GetMin(objCtrl.objControl.Width, 3400)
                  .DataField = objCtrl.objControl.DataField
                  strKey = objWin.CtrlGetKey(objCtrl.objControl)
                  .Tag = strKey
                  .TabIndex = mcllFields.Count
                  .ToolTipText = objCtrl.strBigDesc

                  With objField
                    objField.strField = strKey
                    .intMask = objCtrl.intMask
                    .intType = objCtrl.intType
                    .strSmallDesc = objCtrl.strSmallDesc
                    Set .objControl = txtLocaText(mintMaxText)
                  End With
                  Call mcllFields.Add(objField, strKey)
                  Set objField = Nothing
                End With
                mintMaxText = mintMaxText + 1
              Case cwBoolean
                Load cboLocaBool(mintMaxCombo)
                With cboLocaBool(mintMaxCombo)
                  .Enabled = True
                  .Visible = True
                  Call .AddItem(cboLocaBool(0).List(0))
                  Call .AddItem(cboLocaBool(0).List(1))
                  Call .AddItem(cboLocaBool(0).List(2))
                  .ListIndex = 0
                  .Top = mintTop
                  .DataField = objCtrl.objControl.DataField
                  strKey = objWin.CtrlGetKey(objCtrl.objControl)
                  .Tag = strKey
                  .TabIndex = mcllValues.Count
                  .ToolTipText = objCtrl.strBigDesc

                  With objField
                    objField.strField = strKey
                    .intMask = objCtrl.intMask
                    .intType = objCtrl.intType
                    .strSmallDesc = objCtrl.strSmallDesc
                    Set .objControl = cboLocaBool(mintMaxCombo)
                  End With
                  Call mcllFields.Add(objField, strKey)
                  Set objField = Nothing
                End With
                mintMaxCombo = mintMaxCombo + 1
              Case cwDate
                Load dtcLocaDate(mintMaxDate)
                With dtcLocaDate(mintMaxDate)
                  .Enabled = True
                  .Visible = True
                  .Top = mintTop
                  .DataField = objCtrl.objControl.DataField
                  strKey = objWin.CtrlGetKey(objCtrl.objControl)
                  .Tag = strKey
                  .TabIndex = mcllValues.Count
                  .ToolTipText = objCtrl.strBigDesc
                  .StartOfWeek = ssStartOfWeekMonday
                  .ShowCentury = True

                  With objField
                    objField.strField = strKey
                    .intMask = objCtrl.intMask
                    .intType = objCtrl.intType
                    .strSmallDesc = objCtrl.strSmallDesc
                    Set .objControl = dtcLocaDate(mintMaxDate)
                  End With
                  Call mcllFields.Add(objField, strKey)
                  Set objField = Nothing
                End With
                mintMaxDate = mintMaxDate + 1
            End Select
        End Select
        mintTop = mintTop + intConstVerticalSize
      End If
    Next
  
    If (mcllFields.Count > 0) Then
      cboSearch.ListIndex = 0
  
      Select Case objWin.objWinActiveForm.intFormType
        Case cwFormDetail
          If objWin.objWinActiveForm.blnFilterOn Then
            cmdButton(3).Visible = False
            cmdButton(2).Top = cmdButton(3).Top
          Else
            cmdButton(3).Visible = True
          End If
        Case cwFormMultiLine
          cmdButton(3).Visible = False
          cmdButton(2).Top = cmdButton(3).Top
      End Select
      
      If Me.Height < mintTop + intConstVerticalSize Then
        Me.Height = mintTop + intConstVerticalSize + 100
      End If
  
      If objForm.intSearchLeft <> -1 Then
        Me.Left = objForm.intSearchLeft
      Else
        Me.Left = (objForm.objFormContainer.Width - Me.Width) / 2
      End If
  
      If objForm.intSearchTop <> -1 Then
        Me.Top = objForm.intSearchTop
      Else
        intMiddleScr = objUserScreen.Height / 2
        intMiddleWin = objForm.objFormContainer.Top + _
                       objForm.objFormContainer.Height / 2
        If intMiddleWin > intMiddleScr Then
          Me.Top = (intMiddleScr - Me.Height) / 2
        Else
          Me.Top = intMiddleScr + (intMiddleScr - Me.Height) / 2
        End If
      End If
      Prepare = True
    Else
      With objError
        Call .SetError(cwCodeMsg, msgUsrSearchNotInfo)
        Call .Raise
      End With
    End If
  
    Set objForm = Nothing
    Set objWin = Nothing
  Else
  
    cboSearch.ListIndex = 0
    cmdButton(0).Visible = False
    cmdButton(0).Visible = False
    cmdButton(3).Visible = True
    cmdButton(3).Top = cmdButton(0).Top
    cmdButton(2).Top = cmdButton(1).Top
    If Me.Height < mintTop + intConstVerticalSize Then
      Me.Height = mintTop + intConstVerticalSize + 100
    End If
  End If
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "Load")
End Function

Private Function FindCursor(ByVal objWin As clsCWWin, _
                            ByVal blnFirst As Boolean) As Boolean
  Dim vntMark   As Variant
  Dim blnFound  As Boolean
  Dim blnSearch As Boolean
  Dim objField  As clsCWFieldSearch
  Dim vntData1  As Variant
  Dim vntData2  As Variant

  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  With objWin.objWinActiveForm
    vntMark = .rdoCursor.Bookmark
    If blnFirst Then
      If .blnDirection Then
        Call .rdoCursor.MoveFirst
      Else
        Call .rdoCursor.MoveLast
      End If
    Else
      If .blnDirection Then
        Call .rdoCursor.MoveNext
      Else
        Call .rdoCursor.MovePrevious
      End If
    End If
    Do While ((.blnDirection And Not .rdoCursor.EOF) Or _
              (Not .blnDirection And Not .rdoCursor.BOF)) And _
             Not blnFound
      blnFound = True
      For Each objField In mcllValues
        vntData1 = .rdoCursor(objWin.CtrlGetField(objField.objControl))
        vntData1 = IIf(IsNull(vntData1), Empty, vntData1)
        If TypeName(objField.objControl) = cwComboBox Then
          vntData1 = CBool(vntData1)
        ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
          If IsDate(vntData1) Then
            vntData1 = CStr(CDate(vntData1))
          End If
        End If
        vntData1 = IIf(chkCase, vntData1, UCase(vntData1))
        If TypeName(objField.objControl) = cwComboBox Then
          vntData2 = IIf(objField.objControl.ListIndex = 0, "", CBool(objField.objControl.ListIndex - 2))
        ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
          If IsDate(objField.objControl.Date) Then
            vntData2 = CStr(CDate(objField.objControl.Date))
          Else
            vntData2 = ""
          End If
        Else
          vntData2 = objField.objControl
          vntData2 = IIf(IsNull(vntData2), Empty, vntData2)
        End If
        vntData2 = IIf(chkCase, vntData2, UCase(vntData2))
        If Not objGen.IsStrEmpty(CStr(vntData2)) Then
          Select Case cboSearch.ListIndex
            Case 0
              blnSearch = Left(vntData1, Len(Trim(vntData2))) = Trim(vntData2)
            Case 1
              blnSearch = InStr(vntData1, vntData2) > 0
            Case 2
              blnSearch = (vntData1 = vntData2)
          End Select
          If Not blnSearch Then
            blnFound = False
            Exit For
          End If
        End If
      Next
      If Not blnFound Then
        If .blnDirection Then
          Call .rdoCursor.MoveNext
        Else
          Call .rdoCursor.MovePrevious
        End If
      End If
    Loop
    If Not blnFound Then
      .rdoCursor.Bookmark = vntMark
    End If
    FindCursor = blnFound
  End With
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "FindCursor")
End Function

Private Function FindSQL(ByVal objWin As clsCWWin, _
                         ByVal blnFirst As Boolean) As Boolean
  Dim objField   As clsCWFieldSearch
  Dim strMiWhere As String
  Dim strData1   As String
  Dim vntData2   As Variant
  Dim strControl As String
  Dim blnFound   As Boolean
  Dim cllValues  As New Collection
  Dim intType    As Integer
  Dim objCtrl    As clsCWCtrl

  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  With objWin.objWinActiveForm
    For Each objField In mcllFields
      If Not objGen.IsStrEmpty(objField.objControl) Then
        intType = mcllFields(objField.strField).intType
        Select Case intType
          Case cwNumeric, cwDecimal, cwDate
            strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
            strMiWhere = strMiWhere & objWin.CtrlGetField(objField.objControl) & _
                         "=" & objGen.ValueToSQL(objField.objControl, intType)
          Case cwBoolean
            If objField.objControl.ListIndex > 0 Then
              strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
              strMiWhere = strMiWhere & objWin.CtrlGetField(objField.objControl) & _
                           "=" & objGen.ValueToSQL(objField.objControl.ListIndex = 1, intType)
            End If
          Case cwString
            strControl = Trim(objField.objControl)
            strData1 = IIf(chkCase, "", "{fn ucase(") & objWin.CtrlGetField(objField.objControl) & IIf(chkCase, "", ")}")
            vntData2 = IIf(chkCase, strControl, UCase(strControl))
            strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
            Select Case cboSearch.ListIndex
              Case 0
                strMiWhere = strMiWhere & strData1 & " LIKE " & _
                             objGen.ValueToSQL(vntData2 & "%", cwString)
              Case 1
                strMiWhere = strMiWhere & strData1 & " LIKE " & _
                             objGen.ValueToSQL("%" & vntData2 & "%", cwString)
              Case 2
                strMiWhere = strMiWhere & strData1 & "=" & _
                             objGen.ValueToSQL(vntData2, cwString)
            End Select
        End Select
      End If
    Next
    
    Call objWin.DataCreateKeys(cllValues, False, True)
    Call objWin.DataRemoveCursor
    If blnFirst Then
      Call objWin.DataCreateCursor(cwGotoFirst, strMiWhere, False, True, cllValues)
    Else
      Call objWin.DataCreateCursor(cwGotoForzeNext, strMiWhere, False, True, cllValues)
    End If
    blnFound = objGen.GetRowCount(.rdoCursor) > 0
    If Not blnFound Then
      Call objWin.DataRemoveCursor
      Call objWin.DataCreateCursor(cwGotoActual, "", False, False, cllValues)
    End If
    Call objGen.RemoveCollection(cllValues)
    FindSQL = blnFound
  End With
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "FindSQL")
End Function

Private Function FindGrid(ByVal objWin As clsCWWin, _
                          ByVal blnFirst As Boolean) As Boolean
  Dim vntMark      As Variant
  Dim lngPosition  As Long
  Dim blnFound     As Boolean
  Dim blnSearch    As Boolean
  Dim objField     As clsCWFieldSearch
  Dim vntData1     As Variant
  Dim vntData2     As Variant
  Dim blnNotSearch As Boolean

  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  With objWin.objWinActiveForm
    .grdGrid.Redraw = False
    vntMark = .grdGrid.Bookmark
    lngPosition = -1
    If blnFirst Then
      If .grdGrid.Rows > 0 Then
        Call .grdGrid.MoveFirst
      Else
        blnNotSearch = True
      End If
    Else
      If .grdGrid.AddItemRowIndex(.grdGrid.Bookmark) < .grdGrid.Rows - 1 Then
        Call .grdGrid.MoveNext
      Else
        blnNotSearch = True
      End If
    End If
    If Not blnNotSearch Then
      Do While lngPosition <> .grdGrid.AddItemRowIndex(.grdGrid.Bookmark) And Not blnFound
        lngPosition = .grdGrid.AddItemRowIndex(.grdGrid.Bookmark)
        blnFound = True
        For Each objField In mcllFields
          vntData1 = objWin.objWinActiveForm.cllControls(objField.strField).objControl.Value
          vntData1 = IIf(IsNull(vntData1), Empty, vntData1)
          If TypeName(objField.objControl) = cwComboBox Then
            vntData1 = CBool(vntData1)
          ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
            If IsDate(vntData1) Then
              vntData1 = CStr(CDate(vntData1))
            End If
          End If
          vntData1 = IIf(chkCase, vntData1, UCase(vntData1))
          If TypeName(objField.objControl) = cwComboBox Then
            vntData2 = IIf(objField.objControl.ListIndex = 0, "", CBool(objField.objControl.ListIndex - 2))
          ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
            If IsDate(objField.objControl.Date) Then
              vntData2 = CStr(CDate(objField.objControl.Date))
            Else
              vntData2 = ""
            End If
          Else
            vntData2 = objField.objControl
            vntData2 = IIf(IsNull(vntData2), Empty, vntData2)
          End If
          vntData2 = IIf(chkCase, vntData2, UCase(vntData2))
          If Not objGen.IsStrEmpty(CStr(vntData2)) Then
            Select Case cboSearch.ListIndex
              Case 0
                blnSearch = Left(vntData1, Len(Trim(vntData2))) = Trim(vntData2)
              Case 1
                blnSearch = InStr(vntData1, vntData2) > 0
              Case 2
                blnSearch = (vntData1 = vntData2)
            End Select
            If Not blnSearch Then
              blnFound = False
              Exit For
            End If
          End If
        Next
        If Not blnFound Then
          Call .grdGrid.MoveNext
        End If
      Loop
    End If
    If blnFound Then
      vntMark = .grdGrid.Bookmark
    End If
    .grdGrid.Redraw = True
    .grdGrid.Bookmark = vntMark
    FindGrid = blnFound
  End With
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "FindGrid")
End Function

Private Function ViewSQL(ByVal objWin As clsCWWin) As Boolean
  Dim objField   As clsCWFieldSearch
  Dim strSQL     As String
  Dim strMiWhere As String
  Dim strOrder   As String
  Dim strWhere   As String
  Dim strData1   As String
  Dim vntData2   As Variant
  Dim strControl As String
  Dim intType    As Integer
  Dim objCtrl    As clsCWCtrl
  Dim objColumn  As New clsCWColumn
  Dim cllColumns As New Collection
  Dim cllKeys    As New Collection
  Dim objKey     As New clsCWKey
  Dim frmView    As New frmCWView
  
  On Error GoTo cwIntError
  
  For Each objField In mcllFields
    If Not (objField.objControl Is Nothing) Then
    If Not objGen.IsStrEmpty(objField.objControl) Then
      intType = mcllFields(objField.strField).intType
      Select Case intType
        Case cwNumeric, cwDecimal, cwDate
          strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
          If blnInternalMode Then
            strData1 = objWin.CtrlGetField(objField.objControl)
          Else
            strData1 = objField.strField
          End If
          strMiWhere = strMiWhere & strData1 & "=" & objGen.ValueToSQL(objField.objControl, intType)
        Case cwBoolean
          If objField.objControl.ListIndex > 0 Then
            strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
            If blnInternalMode Then
              strData1 = objWin.CtrlGetField(objField.objControl)
            Else
              strData1 = objField.strField
            End If
            strMiWhere = strMiWhere & strData1 & "=" & objGen.ValueToSQL(objField.objControl.ListIndex = 1, intType)
          End If
        Case cwString
          strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
          strControl = Trim(objField.objControl)
          If blnInternalMode Then
            strData1 = objWin.CtrlGetField(objField.objControl)
          Else
            strData1 = objField.strField
          End If
          If chkCase Then
            strData1 = "{fn ucase(" & strData1 & ")}"
          End If
          vntData2 = IIf(chkCase, strControl, UCase(strControl))
          Select Case cboSearch.ListIndex
            Case 0
              strMiWhere = strMiWhere & strData1 & " LIKE " & _
                           objGen.ValueToSQL(vntData2 & "%", cwString)
            Case 1
              strMiWhere = strMiWhere & strData1 & " LIKE " & _
                           objGen.ValueToSQL("%" & vntData2 & "%", cwString)
            Case 2
              strMiWhere = strMiWhere & strData1 & "=" & _
                           objGen.ValueToSQL(vntData2, cwString)
          End Select
      End Select
      End If
    End If
  Next
    
  If blnInternalMode Then
    strWhere = objWin.DataGetWhere(False)
    If objGen.IsStrEmpty(strMiWhere) Then
      strMiWhere = strWhere
    ElseIf Not objGen.IsStrEmpty(strWhere) Then
      strMiWhere = strWhere & " AND " & strMiWhere
    Else
      strMiWhere = "WHERE " & strMiWhere
    End If
    With objWin.objWinActiveForm
      strSQL = objGen.CreateSQL(.strColumns, _
                                   .strDataBase & .strTable, _
                                   strMiWhere, _
                                   objWin.DataGetOrder(False, False))
    End With
    Call objGen.RemoveCollection(cllColumns)
    Call objWin.DataCreateKeys(cllKeys, True, True)
  
    For Each objKey In cllKeys
      objColumn.strCaption = objKey.strFieldname
      Call objGen.RemoveCollection(objColumn.cllValues)
      Call cllColumns.Add(objColumn)
      Set objColumn = Nothing
    Next
  
  Else
    strWhere = mcllInfo(3)
    If objGen.IsStrEmpty(strMiWhere) Then
      strMiWhere = strWhere
    ElseIf Not objGen.IsStrEmpty(strWhere) Then
      strMiWhere = strWhere & " AND " & strMiWhere
    Else
      strMiWhere = "WHERE " & strMiWhere
    End If
    
    strSQL = objGen.CreateSQL(mcllInfo(1), mcllInfo(2), strMiWhere, mcllInfo(4))
    Call objGen.RemoveCollection(cllColumns)
  
    For Each objField In mcllFields
      objColumn.strCaption = objField.strField
      Call objGen.RemoveCollection(objColumn.cllValues)
      Call cllColumns.Add(objColumn)
      Set objColumn = Nothing
    Next
  End If
  
  Load frmView
  
  With frmView
    Set .cllColumns = cllColumns
    .strSQL = strSQL
    .intSelectType = cwSingleSelect
    .blnSearchCaptions = blnInternalMode
    If Not blnInternalMode Then
      Set .cllFields = mcllFields
      If Not objZoom Is Nothing Then
        .strZoom = strZoom
        Set .objZoom = Me
      End If
    End If
  End With
  
  If frmView.Raise Then
    ViewSQL = True
    If blnInternalMode Then
      For Each objKey In cllKeys
        For Each objColumn In cllColumns
          If objKey.strFieldname = objColumn.strCaption Then
            objKey.strValue = objColumn.cllValues(1)
          End If
        Next
      Next
      Call objWin.DataRemoveCursor
      Call objWin.DataCreateCursor(cwGotoActual, "", False, True, cllKeys)
      If objGen.GetRowCount(objWin.objWinActiveForm.rdoCursor) = 0 Then
        Call objWin.DataRemoveCursor
        Call objWin.DataCreateCursor(cwGotoFirst, "", False, False, cllKeys)
      End If
    Else
      For Each objColumn In cllColumns
        Call mcllValues.Add(objColumn.cllValues(1), objColumn.strCaption)
      Next
    End If
  End If
  
  Unload frmView
  Set frmView = Nothing
    
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "ViewSQL")
End Function

Private Sub txtLocaText_KeyPress(intIndex As Integer, intKeyAscii As Integer)
  Dim intMask    As Integer
  Dim objControl As Object
  
  Set objControl = txtLocaText(intIndex)
  intMask = mcllFields(objControl.Tag).intMask
  
  Select Case intMask
    Case cwMaskString
    Case cwMaskUpper
      intKeyAscii = Asc(UCase(Chr(intKeyAscii)))
    Case cwMaskLower
      intKeyAscii = Asc(LCase(Chr(intKeyAscii)))
    Case cwMaskInteger
      Select Case intKeyAscii
        Case 32, 45, vbKeyBack
        Case vbKey0 To vbKey9
        Case Else
          intKeyAscii = 0
      End Select
    Case cwMaskReal
      Select Case intKeyAscii
        Case 32, 45, vbKeyBack
        Case 44
          intKeyAscii = IIf(InStr(objControl, Chr(intKeyAscii)) > 0, 0, intKeyAscii)
        Case vbKey0 To vbKey9
        Case Else
          intKeyAscii = 0
      End Select
  End Select
  
  Set objControl = Nothing
End Sub

Private Function CheckValues() As Boolean
  Dim objField As clsCWFieldSearch
  Dim blnRet   As Boolean
  
  blnRet = True
  For Each objField In mcllFields
    Select Case TypeName(objField.objControl)
      Case cwTextBox
        If Not objGen.IsStrEmpty(objField.objControl) Then
          Select Case mcllFields(objField.strField).intMask
            Case cwMaskInteger
              blnRet = objGen.ChangeType(objField.objControl, cwNumeric)
            Case cwMaskReal
              blnRet = objGen.ChangeType(objField.objControl, cwDecimal)
            Case Else
          End Select
          If Not blnRet Then
            Exit For
          End If
        End If
      Case Else
    End Select
  Next
  If Not blnRet Then
    With objError
      Call .SetError(cwCodeMsg, msgUsrFilterMaxSize, mcllFields(objField.strField).strSmallDesc)
      Call .Raise
    End With
  End If
  CheckValues = blnRet
End Function

' m�todo de descenso de mensajes para zoom
Friend Sub ZoomData(ByVal cllValues As Collection)
  If Not (objZoom Is Nothing) Then
    Call objZoom.ZoomData(cllValues)
  End If
End Sub


