VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmSpy 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CW Spy. Sistema de Seguridad de CodeWizard"
   ClientHeight    =   4500
   ClientLeft      =   2175
   ClientTop       =   2895
   ClientWidth     =   7770
   ClipControls    =   0   'False
   DrawStyle       =   5  'Transparent
   HelpContextID   =   101
   Icon            =   "SGFRM18.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4500
   ScaleWidth      =   7770
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.TreeView trvForm 
      Height          =   2535
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   4245
      _ExtentX        =   7488
      _ExtentY        =   4471
      _Version        =   327682
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "imlSecure"
      Appearance      =   1
   End
   Begin Crystal.CrystalReport crCrystalReport1 
      Left            =   7155
      Top             =   3825
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   6525
      Top             =   3825
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   5040
      Top             =   3735
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin ComctlLib.ImageList imlSecure 
      Left            =   5760
      Top             =   3735
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   6
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SGFRM18.frx":030A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SGFRM18.frx":0624
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SGFRM18.frx":093E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SGFRM18.frx":0C58
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SGFRM18.frx":0F72
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SGFRM18.frx":128C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Aplicaciones"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Usuarios"
         Index           =   30
         Begin VB.Menu mnuUsersOpcion 
            Caption         =   "&Grupos de Usuarios"
            Index           =   10
         End
         Begin VB.Menu mnuUsersOpcion 
            Caption         =   "&Usuarios"
            Index           =   20
         End
         Begin VB.Menu mnuUsersOpcion 
            Caption         =   "&Roles por Grupo"
            Index           =   30
         End
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   40
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Roles"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Funciones"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Tablas"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   80
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   90
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmSpy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWSpy
' Coded by Manu Roibal
' **********************************************************************************


Private Sub Form_Load()
  Dim strEntorno As String
  
  With objApp
    Call .Register(App, Screen)
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
    .blnUseRegistry = True
  End With

  strEntorno = "Entorno de Explotación"
  Call objEnv.AddEnv(strEntorno)
  Call objEnv.AddValue(strEntorno, "DataBase", "")

  If Not objApp.CreateInfo Then
    Unload Me
  End If
  
  trvForm.Top = 0
  trvForm.Left = 0
  trvForm.Height = Me.ScaleHeight
  trvForm.Width = Me.ScaleWidth
  With trvForm.Nodes
    Call .Add(, , , "Sistema de Seguridad", 1)
    Call .Add(1, tvwChild, , "Aplicaciones", 6)
    Call .Add(1, tvwChild, , "Grupos de Usuarios", 2)
    Call .Add(1, tvwChild, , "Usuarios", 3)
    Call .Add(1, tvwChild, , "Roles", 4)
    Call .Add(1, tvwChild, , "Roles por Grupo", 5)
    Call .Add(1, tvwChild, , "Tipos de Procesos", 6)
    Call .Add(1, tvwChild, , "Procesos", 6)
    Call .Add(1, tvwChild, , "Funciones", 6)
    Call .Add(1, tvwChild, , "Restricciones por Tablas y Columnas", 6)
    Call .Add(1, tvwChild, , "Cambiar clave de Acceso", 6)
    Call .Add(1, tvwChild, , "Forzar nueva clave de Acceso", 6)
    Call .Add(1, tvwChild, , "Procesos por Rol", 6)
    Call .Add(1, tvwChild, , "Delegación de Procesos", 6)
    Call .Add(1, tvwChild, , "Limpieza de Delegaciones", 6)
    Call .Add(1, tvwChild, , "Consulta de Delegaciones", 6)
    Call .Add(1, tvwChild, , "Listados por Ventana", 6)
    Call .Add(1, tvwChild, , "Columnas por Listado", 6)
    Call .Add(1, tvwChild, , "Encriptar Textos", 6)
    Call .Add(1, tvwChild, , "Auditar Tablas", 6)
    Call .Add(1, tvwChild, , "Visualizar el Log de Auditoría", 6)
    Call .Add(1, tvwChild, , "Visualizar y purgar el Log de Auditoría", 6)
  End With
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  intCancel = 0
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 20
      Call objApp.About
  End Select
End Sub

Private Sub trvForm_DblClick()
  Select Case trvForm.SelectedItem.Index
    Case 2
      Load frmCWApps
      Call frmCWApps.Show(vbModal)
      Unload frmCWApps
      Set frmCWApps = Nothing
    Case 3
      Load frmCWGroups
      Call frmCWGroups.Show(vbModal)
      Unload frmCWGroups
      Set frmCWGroups = Nothing
    Case 4
      Load frmCWUsers
      Call frmCWUsers.Show(vbModal)
      Unload frmCWUsers
      Set frmCWUsers = Nothing
    Case 5
      Load frmCWRoles
      Call frmCWRoles.Show(vbModal)
      Unload frmCWRoles
      Set frmCWRoles = Nothing
    Case 6
      Load frmCWRolesGroup
      Call frmCWRolesGroup.Show(vbModal)
      Unload frmCWRolesGroup
      Set frmCWRolesGroup = Nothing
    Case 7
      Load frmCWTypeProcess
      Call frmCWTypeProcess.Show(vbModal)
      Unload frmCWTypeProcess
      Set frmCWTypeProcess = Nothing
    Case 8
      Load frmCWProcess
      Call frmCWProcess.Show(vbModal)
      Unload frmCWProcess
      Set frmCWProcess = Nothing
    Case 9
      Load frmCWFuncs
      Call frmCWFuncs.Show(vbModal)
      Unload frmCWFuncs
      Set frmCWFuncs = Nothing
    Case 10
      Load frmCWSegTabCol
      Call frmCWSegTabCol.Show(vbModal)
      Unload frmCWSegTabCol
      Set frmCWSegTabCol = Nothing
    Case 11
      Load frmCWChangePW
      If frmCWChangePW.Prepare(False) Then
        Call frmCWChangePW.Show(vbModal)
      End If
      Unload frmCWChangePW
      Set frmCWChangePW = Nothing
    Case 12
      Load frmCWChangePW
      If frmCWChangePW.Prepare(True) Then
        Call frmCWChangePW.Show(vbModal)
      End If
      Unload frmCWChangePW
      Set frmCWChangePW = Nothing
    Case 13
      Load frmCWProcessRol
      Call frmCWProcessRol.Show(vbModal)
      Unload frmCWProcessRol
      Set frmCWProcessRol = Nothing
    Case 14
      Load frmCWDelega
      Call frmCWDelega.Show(vbModal)
      Unload frmCWDelega
      Set frmCWDelega = Nothing
    Case 15
      Load frmCWPurgeDelega
      Call frmCWPurgeDelega.Show(vbModal)
      Unload frmCWPurgeDelega
      Set frmCWPurgeDelega = Nothing
    Case 16
      Load frmCWQueryDelega
      Call frmCWQueryDelega.Show(vbModal)
      Unload frmCWQueryDelega
      Set frmCWQueryDelega = Nothing
    Case 17
      Load frmCWVenLis
      Call frmCWVenLis.Show(vbModal)
      Unload frmCWVenLis
      Set frmCWVenLis = Nothing
    Case 18
      Load frmCWPrnTabCol
      Call frmCWPrnTabCol.Show(vbModal)
      Unload frmCWPrnTabCol
      Set frmCWPrnTabCol = Nothing
    Case 19
      Load frmCWEncript
      Call frmCWEncript.Show(vbModal)
      Unload frmCWEncript
      Set frmCWEncript = Nothing
    Case 20
      Load frmCWAuditTables
      Call frmCWAuditTables.Show(vbModal)
      Unload frmCWAuditTables
      Set frmCWAuditTables = Nothing
    Case 21
      Load frmCWViewAudit
      If frmCWViewAudit.Prepare(False) Then
        Call frmCWViewAudit.Show(vbModal)
      End If
      Unload frmCWViewAudit
      Set frmCWViewAudit = Nothing
    Case 22
      Load frmCWViewAudit
      If frmCWViewAudit.Prepare(True) Then
        Call frmCWViewAudit.Show(vbModal)
      End If
      Unload frmCWViewAudit
      Set frmCWViewAudit = Nothing
  End Select
End Sub
