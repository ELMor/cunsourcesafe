VERSION 5.00
Begin VB.Form frmCWLoadProcess 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGURIDAD. Carga Autom�tica de Procesos"
   ClientHeight    =   1665
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4020
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   70
   Icon            =   "SGFRM20.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1665
   ScaleWidth      =   4020
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   476
      TabIndex        =   2
      Top             =   1125
      Width           =   1400
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2144
      TabIndex        =   1
      Top             =   1125
      Width           =   1400
   End
   Begin VB.ComboBox cboApplications 
      Height          =   315
      Left            =   315
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   450
      Width           =   3390
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Aplicaci�n"
      Height          =   195
      Index           =   0
      Left            =   360
      TabIndex        =   3
      Top             =   225
      Width           =   735
   End
End
Attribute VB_Name = "frmCWLoadProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWLoadProcess
' Coded by Manu Roibal
' **********************************************************************************


Const cwLoadProcAutoError As String = "Se produjo un error conectando con la Aplicaci�n %1"
Const cwLoadProcOk        As String = "Se han a�adido %1 procesos"


Dim mcllApplications As New Collection
Dim mcllServers      As New Collection
Dim mcllFecAct       As New Collection
Dim mcllFecDes       As New Collection


Private Sub Form_Load()
  Dim rdoCursor As rdoResultset
  Dim strSQL    As String
  
  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  strSQL = "SELECT sg00id, sg00des, sg00svr, sg00fecact, sg00fecdes FROM sg0000 ORDER BY sg00des"
  Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
  
  Do While Not rdoCursor.EOF
    If Not objGen.IsStrEmpty(rdoCursor(2) & "") Then
      Call mcllApplications.Add(rdoCursor(0).Value)
      Call mcllServers.Add(rdoCursor(2).Value)
      Call mcllFecAct.Add(rdoCursor(3).Value)
      Call mcllFecDes.Add(rdoCursor(4).Value)
      Call cboApplications.AddItem(rdoCursor(1).Value)
    End If
    Call rdoCursor.MoveNext
  Loop
  
  Call rdoCursor.Close
  Set rdoCursor = Nothing
  
  If cboApplications.ListCount > 0 Then
    cboApplications.ListIndex = 0
  End If
  
cwResError:
  Call objMouse.BusyOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "FormLoad")
  Resume cwResError
End Sub


Private Sub cmdAccept_Click()
  Dim strAppDesc    As String
  Dim strAppCod     As String
  Dim intListIndex  As Integer
  Dim strServer     As String
  Dim strObject     As String
  Dim objAutomation As Object
  Dim strCod        As String
  Dim strDesc       As String
  Dim blnMenu       As Boolean
  Dim intType       As cwTypeProcess
  Dim intInd        As Integer
  Dim blnError      As Boolean
  Dim strSQL        As String
  Dim strFecAct     As String
  Dim strFecDes     As String
  Dim intProcess    As Integer
  Dim aProcess()    As Variant
  
  Call objMouse.BusyOn
  
  intListIndex = cboApplications.ListIndex + 1
  
  If intListIndex > 0 Then
    strAppCod = mcllApplications(intListIndex)
    strAppDesc = cboApplications.Text
    strFecAct = objGen.ValueToSQL(mcllFecAct(intListIndex) & "", cwDate)
    strFecDes = objGen.ValueToSQL(mcllFecDes(intListIndex) & "", cwDate)
    strServer = mcllServers(intListIndex)
    strObject = strServer & ".clsCWLauncher"
    
    Call Err.Clear
    On Error Resume Next
    Set objAutomation = CreateObject(strObject)
    blnError = Err.Number <> 0
    On Error GoTo 0
    
    If Not blnError Then
      Call objAutomation.GetProcess(aProcess)
      For intInd = LBound(aProcess) To UBound(aProcess)
        strCod = aProcess(intInd, 1)
        strDesc = Left(aProcess(intInd, 2), 40)
        blnMenu = aProcess(intInd, 3)
        intType = aProcess(intInd, 4)
        strSQL = "INSERT INTO sg0400 " & _
                    "(sg04cod, sg17cod, sg04des, sg04fecact, sg04fecdes, sg00id, sg04indmenu) " & _
                    "VALUES ('" & strCod & "'," & CInt(intType) & ",'" & strDesc & "'," & strFecAct & "," & strFecDes & ",'" & strAppCod & "'," & CInt(blnMenu) & ")"
        On Error Resume Next
        Call objApp.rdoConnect.Execute(strSQL)
        If Err.Number = 0 Then
          intProcess = intProcess + 1
        End If
        On Error GoTo 0
      Next

      Call objError.SetError(cwCodeMsg, cwLoadProcOk, intProcess)
      Call objError.Raise
      Unload Me
    Else
      Call objError.SetError(cwCodeMsg, cwLoadProcAutoError, strAppDesc)
      Call objError.Raise
    End If
  End If
  Call objMouse.BusyOff
End Sub


Private Sub cmdCancel_Click()
  Unload Me
End Sub

