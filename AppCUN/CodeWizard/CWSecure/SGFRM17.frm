VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCWViewAudit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGURIDAD. Gesti�n del Log de Auditor�a"
   ClientHeight    =   6555
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9945
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   98
   Icon            =   "SGFRM17.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6555
   ScaleWidth      =   9945
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   3372
      TabIndex        =   4
      Top             =   6120
      Width           =   1400
   End
   Begin VB.PictureBox picSplitter 
      BorderStyle     =   0  'None
      Height          =   50
      Left            =   540
      MousePointer    =   7  'Size N S
      ScaleHeight     =   45
      ScaleWidth      =   8820
      TabIndex        =   3
      Top             =   4455
      Width           =   8820
   End
   Begin VB.TextBox txtDetails 
      BackColor       =   &H8000000F&
      Height          =   1410
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   4635
      Width           =   9915
   End
   Begin VB.CommandButton cmdAccept 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   5172
      TabIndex        =   0
      Top             =   6120
      Width           =   1400
   End
   Begin ComctlLib.ListView lvwLog 
      Height          =   4335
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   7646
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmCWViewAudit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWViewAudit
' Coded by Manu Roibal
' **********************************************************************************


Const cwViewAuditMsgEmpty As String = "El Log de Auditor�a est� vac�o"
Const cwViewAuditQueryDel As String = "�Desea borrar el/los registros del Log de Auditor�a?"


Dim mblnMoving As Boolean
Dim mblnAllowDelete As Boolean


Public Function Prepare(ByVal blnAllowDelete As Boolean) As Boolean
  Dim rdoCursor  As rdoResultset
  Dim strSQL     As String
  Dim objCatalog As clsCWCatalog
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  mblnAllowDelete = blnAllowDelete
  
  Call FormResize
  
  Call lvwLog.ListItems.Clear
  Call lvwLog.ColumnHeaders.Clear
  
  With lvwLog.ColumnHeaders
    Call .Add(, , "Fecha", 1200)
    Call .Add(, , "Acci�n", 600)
    Call .Add(, , "Tabla", 600)
    Call .Add(, , "Descripci�n", 2000)
    Call .Add(, , "Usuario", 600)
    Call .Add(, , "Nombre", 3000)
  End With
  
  With lvwLog
    .Sorted = True
    .SortOrder = lvwDescending
    .SortKey = 0
    .View = lvwReport
    .LabelEdit = lvwManual
    .HideSelection = False
  End With
  
  strSQL = "SELECT L.sg13fec, L.sg09cod, L.sg02cod, U.sg02nom || ' ' || U.sg02ape1 || ' ' || U.sg02ape2, L.sg13indaccion " & _
                "FROM sg1300 L, sg0200 U " & _
                "WHERE L.sg02cod = U.sg02cod " & _
                "ORDER BY L.sg13fec"
  
  Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly, rdAsyncEnable)
  
  Call objGen.WaitCursor(rdoCursor, True)

  Do While Not rdoCursor.EOF Or rdoCursor.StillExecuting
    If Not rdoCursor.EOF Then
      
      With lvwLog.ListItems.Add(, , rdoCursor(0))
        Select Case rdoCursor(4)
          Case cwAuditSelect
            .SubItems(1) = "Select"
          Case cwAuditInsert
            .SubItems(1) = "Insert"
          Case cwAuditUpdate
            .SubItems(1) = "Update"
          Case cwAuditDelete
            .SubItems(1) = "Delete"
        End Select
        .SubItems(2) = UCase(rdoCursor(1))
        
        Set objCatalog = objApp.GetCatalog(rdoCursor(1))
        .SubItems(3) = objCatalog.strTableDesc
        Set objCatalog = Nothing
        
        .SubItems(4) = UCase(rdoCursor(2))
        .SubItems(5) = rdoCursor(3)
      End With
      
      Call rdoCursor.MoveNext
    End If
  Loop
  
  cmdDelete.Enabled = mblnAllowDelete And (rdoCursor.RowCount > 0)
  If Not cmdDelete.Enabled Then
    cmdDelete.Visible = False
    cmdAccept.Left = (Me.ScaleWidth / 2) - (cmdAccept.Width / 2)
  End If
  
  If rdoCursor.RowCount > 0 Then
    Prepare = True
  Else
    Call objError.SetError(cwCodeMsg, cwViewAuditMsgEmpty)
    Call objError.Raise
  End If
  
  Call rdoCursor.Close
  Set rdoCursor = Nothing
  
cwResError:
  Call objApp.SplashOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "Prepare")
  Resume cwResError
End Function


Private Sub lvwLog_ColumnClick(ByVal ColumnHeader As ColumnHeader)
  If lvwLog.SortKey = ColumnHeader.Index - 1 Then
    If lvwLog.SortOrder = lvwAscending Then
      lvwLog.SortOrder = lvwDescending
    Else
      lvwLog.SortOrder = lvwAscending
    End If
  Else
    lvwLog.SortKey = ColumnHeader.Index - 1
    lvwLog.SortOrder = lvwAscending
  End If
End Sub

Private Sub lvwLog_ItemClick(ByVal Item As ComctlLib.ListItem)
  Dim lngSize   As Long
  Dim strTexto  As String
  Dim strBuffer As String
  Dim rdoCursor As rdoResultset
  Dim strSQL    As String
  
  On Error GoTo cwIntError
  
  txtDetails = ""
  If objGen.IsStrEmpty(Item.Tag) Then
    strSQL = "SELECT sg13fec, sg13datos FROM sg1300 WHERE sg13fec = " & objGen.ValueToSQL(Item.Text, cwDate)
    Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
    If rdoCursor.RowCount > 0 Then
      lngSize = 1024
      Do While lngSize = 1024
        strBuffer = "" & rdoCursor(1).GetChunk(1024)
        lngSize = Len(strBuffer)
        strTexto = strTexto & strBuffer
      Loop
      Item.Tag = strTexto
    End If
    Call rdoCursor.Close
    Set rdoCursor = Nothing
  End If
  txtDetails = Item.Tag
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "ItemClick")
End Sub

Private Sub cmdAccept_Click()
  Call Me.Hide
End Sub


Private Sub cmdDelete_Click()
  Dim lngInd   As Long
  Dim blnWrite As Boolean
  Dim strSQL   As String
  
  If mblnAllowDelete Then
    Call objError.SetError(cwCodeQuery, cwViewAuditQueryDel)
    If objError.Raise = vbYes Then
      strSQL = "DELETE FROM sg1300 WHERE sg13fec = "
  
      Call objApp.rdoConnect.BeginTrans
  
      On Error Resume Next
  
      With lvwLog
        For lngInd = 1 To .ListItems.Count
          If .ListItems(lngInd).Selected Then
            blnWrite = True
            Call objApp.rdoConnect.Execute(strSQL & objGen.ValueToSQL(.ListItems(lngInd).Text, cwDate))
            If Err.Number > 0 Then
              blnWrite = False
              Exit For
            End If
          End If
        Next
        If blnWrite Then
          For lngInd = 1 To .ListItems.Count
            If .ListItems(lngInd).Selected Then
              Call .ListItems.Remove(lngInd)
            End If
          Next
        End If
      End With
  
      If blnWrite Then
        Call objApp.rdoConnect.CommitTrans
      Else
        Call objApp.rdoConnect.RollbackTrans
      End If
      Call Prepare(mblnAllowDelete)
    End If
  End If
End Sub

Private Sub picSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mblnMoving = True
End Sub

Private Sub picSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mblnMoving = False
End Sub

Private Sub picSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim lngSize As Long
  
  If mblnMoving Then
    lngSize = lvwLog.Height
    If lvwLog.Height + Y > 1000 Then
      lngSize = lvwLog.Height + Y
    End If
    If lvwLog.Height + Y > Me.ScaleHeight - 1000 Then
      lngSize = Me.ScaleHeight - 1000
    End If
    If lvwLog.Height <> lngSize Then
      lvwLog.Height = lngSize
      Call FormResize
    End If
  End If
End Sub

Private Sub FormResize()
  Dim lngTop As Long
  
  If picSplitter.Top <> lvwLog.Height Then
    picSplitter.Top = lvwLog.Height
    lngTop = picSplitter.Top + picSplitter.Height
    If txtDetails.Top <> lngTop Then
      txtDetails.Height = Me.ScaleHeight - lngTop - 600
      txtDetails.Top = lngTop
    End If
  End If
End Sub
