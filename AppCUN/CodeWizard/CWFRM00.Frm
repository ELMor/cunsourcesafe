VERSION 5.00
Begin VB.Form frmCWUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Identificación"
   ClientHeight    =   4200
   ClientLeft      =   2010
   ClientTop       =   3480
   ClientWidth     =   6480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CWFRM00.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   6480
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraEstandar 
      Height          =   1680
      Left            =   90
      TabIndex        =   8
      Top             =   990
      Width           =   4605
      Begin VB.TextBox txtPassword 
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   180
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1170
         Width           =   4155
      End
      Begin VB.TextBox txtUser 
         Height          =   330
         Left            =   180
         MaxLength       =   30
         TabIndex        =   0
         Top             =   450
         Width           =   4155
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Clave de Acceso"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   10
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre de Usuario"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   9
         Top             =   225
         Width           =   1365
      End
   End
   Begin VB.Frame fraAvanzado 
      Height          =   1185
      Left            =   90
      TabIndex        =   11
      Top             =   2880
      Width           =   6270
      Begin VB.ComboBox cboSources 
         Height          =   315
         Left            =   1665
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   270
         Width           =   4425
      End
      Begin VB.ComboBox cboEnvironment 
         Height          =   315
         Left            =   1665
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   720
         Width           =   2625
      End
      Begin VB.CommandButton cmdODBC 
         Caption         =   "&ODBC Manager"
         Height          =   375
         Left            =   4545
         TabIndex        =   7
         Top             =   675
         Width           =   1545
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Entorno"
         Height          =   195
         Index           =   3
         Left            =   180
         TabIndex        =   13
         Top             =   765
         Width           =   555
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Conexión ODBC"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   12
         Top             =   315
         Width           =   1155
      End
   End
   Begin VB.Frame fraTitulo 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Left            =   90
      TabIndex        =   14
      Top             =   0
      Width           =   6270
      Begin VB.Label lblCode 
         Alignment       =   2  'Center
         Caption         =   "CodeWizard"
         Height          =   240
         Left            =   45
         TabIndex        =   16
         Top             =   585
         Width           =   6180
      End
      Begin VB.Label lblApp 
         Alignment       =   2  'Center
         Caption         =   "Aplicación"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   45
         TabIndex        =   15
         Top             =   225
         Width           =   6180
      End
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   4815
      TabIndex        =   2
      Top             =   1080
      Width           =   1545
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4815
      TabIndex        =   3
      Top             =   1575
      Width           =   1545
   End
   Begin VB.CommandButton cmdAdvanced 
      Caption         =   "A&vanzado..."
      Height          =   375
      Left            =   4815
      TabIndex        =   4
      Top             =   2295
      Width           =   1545
   End
End
Attribute VB_Name = "frmCWUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWUser
' Coded by SYSECA Bilbao
' **********************************************************************************


Const intConstSmallWinSize As Integer = 3200
Const intConstBigWinSize   As Integer = 4600


Public blnConnection As Boolean


Private Sub Form_Activate()
  If objGen.IsStrEmpty(txtUser) Then
    Call txtUser.SetFocus
  ElseIf objGen.IsStrEmpty(txtPassword) Then
    Call txtPassword.SetFocus
  ElseIf cboSources.ListIndex = -1 Then
    Call cboSources.SetFocus
  Else
    Call cmdAccept.SetFocus
  End If
End Sub



Private Sub Form_Load()
  Dim cllType   As New Collection
  Dim vntBuffer As Variant
  
  txtUser = objApp.strUserName
  txtPassword = objApp.strPassword
  Call SearchSources(objApp.strDataSource)
  
  Set cllType = objEnv.GetEnvs()
  For Each vntBuffer In cllType
    Call cboEnvironment.AddItem(vntBuffer)
  Next
  cboEnvironment.ListIndex = objApp.intUserEnv
  
  Me.Height = intConstSmallWinSize
  
  If cboSources.ListIndex = -1 Then
    cmdAdvanced.Value = True
  End If
End Sub

Private Sub cmdAccept_Click()
  If Not objGen.Connect(cboSources.Text, txtUser, txtPassword) Then
    With objError
      Call .SetError(cwCodeMsg, msgUsrAccess)
      Call .Raise
    End With
    Call txtUser.SetFocus
    txtUser.SelStart = 0
    txtUser.SelLength = Len(txtUser)
  Else
    With objApp
      .strUserName = txtUser
      .strPassword = txtPassword
      .strDataSource = cboSources.Text
      .intUserEnv = cboEnvironment.ListIndex
    End With
    blnConnection = True
    Call Hide
  End If
End Sub

Private Sub cmdAdvanced_Click()
  Me.Height = IIf(Me.Height = intConstBigWinSize, _
                  intConstSmallWinSize, _
                  intConstBigWinSize)
End Sub

Private Sub cmdCancel_Click()
  blnConnection = False
  Call Hide
End Sub

Private Sub cmdODBC_Click()
  Call objGen.LaunchApplet("odbccp32.cpl")
End Sub

Private Sub SearchSources(ByVal strDataSource As String)
  Dim objEnv    As rdoEnvironment
  Dim strBuffer As String
  Dim strDesc   As String
  Dim intBuffer As Integer
  Dim intDesc   As Integer
  Dim lngCode   As Long
  Dim intPos    As Integer
  
  Set objEnv = rdoEnvironments(0)
  
  cboSources.Clear
  strBuffer = Space(512)
  strDesc = Space(512)
  lngCode = SQLDataSources(objEnv.henv, SQL_FETCH_NEXT, _
                           strBuffer, Len(strBuffer), intBuffer, _
                           strDesc, Len(strDesc), intDesc)
  
  Do While lngCode <> SQL_NO_DATA_FOUND
    strBuffer = Left(strBuffer, intBuffer)
    strDesc = Left(strDesc, intDesc)
    Call cboSources.AddItem(strBuffer)
    If UCase(strDataSource) = UCase(strBuffer) Then
      cboSources.ListIndex = intPos
    Else
      intPos = intPos + 1
    End If
    strBuffer = Space(512)
    strDesc = Space(512)
    lngCode = SQLDataSources(objEnv.henv, SQL_FETCH_NEXT, _
                             strBuffer, Len(strBuffer), intBuffer, _
                             strDesc, Len(strDesc), intDesc)
  Loop
End Sub

Private Sub cboEnvironment_GotFocus()
  Call MakeBigWindow
End Sub

Private Sub cboSources_GotFocus()
  Call MakeBigWindow
End Sub

Private Sub cmdODBC_GotFocus()
  Call MakeBigWindow
End Sub

Private Sub MakeBigWindow()
  If Me.Height = intConstSmallWinSize Then
    Me.Height = intConstBigWinSize
  End If
End Sub

