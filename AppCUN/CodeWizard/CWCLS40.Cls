VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWComboBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWComboBox
' Coded by SYSECA Bilbao
' **********************************************************************************

Dim mcllKeys As New Collection
Dim mcllData As New Collection


Friend Function HasItems() As Boolean
  HasItems = (mcllKeys.Count > 0)
End Function


Friend Sub Add(ByVal strKey As String, _
               ByVal strData As String)
  Call mcllKeys.Add(strKey)
  Call mcllData.Add(strData)
End Sub


Friend Sub Clear()
  Call objGen.RemoveCollection(mcllKeys)
  Call objGen.RemoveCollection(mcllData)
End Sub


Friend Function GetKey(ByVal strData As String) As String
  Dim intInd  As Integer
  Dim intSize As Integer
  
  intSize = mcllKeys.Count
  strData = Trim(UCase(strData))
  For intInd = 1 To intSize
    If Trim(UCase(mcllData(intInd))) = strData Then
      GetKey = mcllKeys(intInd)
      Exit For
    End If
  Next
End Function


Friend Function GetData(ByVal strKey As String) As String
  Dim intInd  As Integer
  Dim intSize As Integer
  
  intSize = mcllKeys.Count
  strKey = Trim(UCase(strKey))
  For intInd = 1 To intSize
    If Trim(UCase(mcllKeys(intInd))) = strKey Then
      GetData = mcllData(intInd)
      Exit For
    End If
  Next
End Function


Friend Function GetPosData(ByVal strData As String) As Integer
  Dim intInd  As Integer
  Dim intSize As Integer
  Dim intRet  As Integer
  intSize = mcllKeys.Count
  strData = Trim(UCase(strData))
  For intInd = 1 To intSize
    If Trim(UCase(mcllData(intInd))) = strData Then
      intRet = intInd
      Exit For
    End If
  Next
  GetPosData = intRet - 1
End Function


