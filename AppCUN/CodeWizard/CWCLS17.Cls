VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWGen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWGen
' Coded by SYSECA Bilbao
' **********************************************************************************

Const cwMsgConnectWait As String = "Conectando con la Base de Datos, espere un momento por favor..."


' carga un combobox con los datos de un cursor
Public Sub LoadCombo(ByVal objWin As clsCWWin, _
                     ByVal objCtrl As clsCWCtrl)
  Dim rdoCursor    As rdoResultset
  Dim strData      As String
  Dim intMaxLen    As Integer
  Dim objColumn    As Object
  Dim strDelimiter As String
  Dim strSeparator As String
  
  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  With objCtrl
    Call .objItemData.Clear
    Select Case objWin.objWinActiveForm.intFormType
      Case cwFormDetail
        Select Case TypeName(.objControl)
          Case cwComboBox, cwSSDBCombo
            Select Case TypeName(.objControl)
              Case cwComboBox
                Call .objControl.Clear
              Case cwSSDBCombo
                strDelimiter = objGen.GetDelSep(.objControl, True)
                strSeparator = objGen.GetDelSep(.objControl, False)
                Call .objControl.RemoveAll
            End Select
            Set rdoCursor = objApp.rdoConnect.OpenResultset(.strSQL, rdOpenKeyset, rdConcurReadOnly, rdAsyncEnable)
            Call WaitCursor(rdoCursor, True)
            Do While rdoCursor.StillExecuting Or Not rdoCursor.EOF
              If Not rdoCursor.EOF Then
                strData = ""
                Select Case TypeName(.objControl)
                  Case cwComboBox
                    Call .objControl.AddItem(rdoCursor.rdoColumns(1).Value)
                   .objControl.ItemData(objCtrl.objControl.NewIndex) = rdoCursor.rdoColumns(0).Value
                  Case cwSSDBCombo
                    strDelimiter = objGen.GetDelSep(.objControl, True)
                    strSeparator = objGen.GetDelSep(.objControl, False)
                    For Each objColumn In rdoCursor.rdoColumns
                      strData = strData & strSeparator & strDelimiter & objColumn.Value & strDelimiter
                    Next
                    Call .objControl.AddItem(Mid(strData, 1 + Len(strSeparator)))
                End Select
                Call rdoCursor.MoveNext
              End If
            Loop
            rdoCursor.Close
            Set rdoCursor = Nothing
          Case Else
        End Select
      Case cwFormMultiLine
        .objControl.style = ssStyleComboBox
        Call .objControl.RemoveAll
        Set rdoCursor = objApp.rdoConnect.OpenResultset(.strSQL, rdOpenKeyset, rdConcurReadOnly, rdAsyncEnable)
        Call WaitCursor(rdoCursor, True)
        intMaxLen = Len(.objControl.Caption)
        Do While rdoCursor.StillExecuting Or Not rdoCursor.EOF
          If Not rdoCursor.EOF Then
            Call .objControl.AddItem(rdoCursor.rdoColumns(1).Value)
            Call .objItemData.Add(rdoCursor.rdoColumns(0).Value, rdoCursor.rdoColumns(1).Value)
            ' intMaxLen = GetMax(intMaxLen, Len(rdoCursor.rdoColumns(1).Value))
            Call rdoCursor.MoveNext
          End If
        Loop
        .objControl.Width = intMaxLen * cwCharWidth + 250
        .objControl.FieldLen = rdoCursor.rdoColumns(1).Size
        rdoCursor.Close
        Set rdoCursor = Nothing
    End Select
  End With
  
cwResError:
  Call objMouse.BusyOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "LoadCombo", objCtrl.strSQL)
  Resume cwResError
End Sub


' un and binario para n�meros bit a bit
Public Function IsAnd(ByVal intData1 As Integer, _
                      ByVal intData2 As Integer) As Boolean
  IsAnd = ((intData1 And intData2) > 0)
End Function


' esta vac�o el string?
Public Function IsStrEmpty(ByVal strString As Variant) As Boolean
  IsStrEmpty = IIf(strString > "", (Trim(strString) = ""), True)
End Function

Public Function Assign(ByRef vntVar As Variant, ByVal vntValue As Variant) As Boolean
Select Case TypeName(vntVar)
    Case "Decimal"
        Select Case TypeName(vntValue)
            Case "Decimal"
                vntVar = vntValue
                Assign = True
            Case "String"
                Assign = ChangeType(vntValue, cwDecimal)
                If Assign Then
                   vntVar = vntValue
                 Else
                   vntVar = 0
                End If
            Case Else
                vntVar = 0
        End Select
    Case "String"
        Select Case TypeName(vntValue)
            Case "String"
                vntVar = vntValue
                Assign = True
            Case "String"
                Assign = ChangeType(vntValue, cwString)
                If Assign Then
                   vntVar = vntValue
                 Else
                   vntVar = ""
                End If
            Case Else
                vntVar = ""
        End Select
    Case Else
        ' Unsupported assignment, try directly
        On Error Resume Next
        vntVar = vntValue
        If Err.Number = 0 Then
            Assign = True
        End If
        On Error GoTo 0
End Select
End Function


' intercambia tipos de datos vb y rdo
Public Function Rdo2Vb(ByVal intRDOType As Integer) As cwDataType
  Select Case intRDOType
    Case rdTypeNUMERIC, rdTypeDECIMAL, rdTypeINTEGER, _
         rdTypeSMALLINT, rdTypeBIGINT, rdTypeTINYINT
      Rdo2Vb = cwNumeric
    Case rdTypeFLOAT, rdTypeREAL, rdTypeDOUBLE
      Rdo2Vb = cwDecimal
    Case rdTypeDATE, rdTypeTIMESTAMP
      Rdo2Vb = cwDate
    Case rdTypeTIME
      Rdo2Vb = cwString
    Case rdTypeCHAR, rdTypeVARCHAR, rdTypeBINARY, rdTypeVARBINARY
      Rdo2Vb = cwString
    Case rdTypeLONGVARCHAR, rdTypeLONGVARBINARY
      Rdo2Vb = cwString
    Case rdTypeBIT
      Rdo2Vb = cwBoolean
    Case Else
      Rdo2Vb = cwVariant
  End Select
End Function


' cambia un dato de un tipo a otro y devuelve un l�gico
' indicando si es posible la conversi�n
Public Function ChangeType(ByVal vntNewValue As Variant, _
                           ByVal intType As Integer, _
                           Optional vntValue As Variant) As Boolean
  Dim vntDummy As Variant
  
  On Error GoTo ErrorControl
  
  vntNewValue = IIf(IsNull(vntNewValue), "", vntNewValue)
  If IsStrEmpty(vntNewValue) Then
    Select Case intType
      Case cwString
        vntDummy = IIf(Len(vntNewValue) = 0, Empty, vntNewValue)
      Case Else
        vntDummy = Empty
    End Select
  Else
    Select Case intType
      Case cwNumeric
        vntDummy = CCur(vntNewValue)
      Case cwDecimal
        vntDummy = CDbl(vntNewValue)
      Case cwDate
        vntDummy = CDate(vntNewValue)
      Case cwString
        vntDummy = CStr(vntNewValue)
      Case cwBoolean
        vntDummy = CBool(vntNewValue)
    End Select
  End If
  ChangeType = True
  If Not IsMissing(vntValue) Then
    vntValue = vntDummy
  End If
  Exit Function
ErrorControl:
  ChangeType = False
End Function


' borra una colecci�n
Public Sub RemoveCollection(ByVal cllCollection As Collection)
  Dim lngSize As Long
  Dim lngInd  As Long
  
  With cllCollection
    lngSize = .Count
    For lngInd = 1 To lngSize
      Call .Remove(1)
    Next
  End With
End Sub


' devuelve el valor m�ximo
Public Function GetMax(ByVal vntValue1 As Variant, _
                       ByVal vntValue2 As Variant) As Variant
  GetMax = IIf(vntValue1 > vntValue2, vntValue1, vntValue2)
End Function


' devuelve el valor m�ximo
Public Function GetMin(ByVal vntValue1 As Variant, _
                       ByVal vntValue2 As Variant) As Variant
  GetMin = IIf(vntValue1 < vntValue2, vntValue1, vntValue2)
End Function


' esta funci�n crear una sentencia sql a partir de sus componentes
Public Function CreateSQL(ByVal strNewData As String, _
                          ByVal strNewTable As String, _
                          ByVal strNewWhere As String, _
                          ByVal strNewOrderBy As String) As String
                          
'Optional ByVal strNewHint) As String
'If IsMissing(strNewHint) Then
    CreateSQL = "SELECT " & strNewData & _
              " FROM " & strNewTable & _
              " " & strNewWhere & " " & strNewOrderBy
'Else
'    CreateSQL = "SELECT " & strNewHint & " " & strNewData & _
'              " FROM " & strNewTable & _
'              " " & strNewWhere & " " & strNewOrderBy
'End If
End Function


' realiza la conexi�n a la base de datos
Public Function Connect(ByVal strODBC As String, _
                        ByVal strUser As String, _
                        ByVal strPassword As String) As Boolean
  Dim blnNotError As Boolean
  Dim intReturned As Integer
  objApp.objLog.LogWrite ("Connect started...")
    
  ' Call objApp.SplashOn(cwMsgConnectWait)
  Call objMouse.BusyOn
  On Error Resume Next
  With objApp.rdoEnv
    .UserName = strUser
    .Password = strPassword
    .CursorDriver = rdUseOdbc
    Set objApp.rdoConnect = .OpenConnection("", rdDriverNoPrompt, False, "DSN=" & strODBC)
    blnNotError = (Err.Number = 0)
  End With
  Call objMouse.BusyOff
  ' Call objApp.SplashOff
  
  Connect = blnNotError
  objApp.objLog.LogWrite ("Connect ending...")
End Function


' lanza un applet del panel de control
Public Sub LaunchApplet(ByVal strApplet As String)
  On Error Resume Next
  Call Shell("Rundll32.exe shell32.dll,Control_RunDLL " & strApplet)
End Sub

' cambia un string por otro
Public Function ReplaceStr(ByVal strValue As String, _
                           ByVal strSource As String, _
                           ByVal strTarget As String, _
                           ByVal intTimes As Integer) As String
  Dim intPos  As Integer
  Dim intLoop As Integer
  
  intPos = InStr(strValue, strSource)
  Do While intPos > 0
    strValue = Left(strValue, intPos - 1) & _
               strTarget & _
               Mid(strValue, intPos + Len(strSource))
    intLoop = intLoop + 1
    If intTimes > 0 Then
      If intLoop >= intTimes Then
        Exit Do
      End If
    End If
    intPos = InStr(strValue, strSource)
  Loop
  ReplaceStr = strValue
End Function


' determina el n�mero de filas de un cursor
Public Function GetRowCount(ByVal objCursor As rdoResultset) As Long
  On Error Resume Next
  GetRowCount = objCursor.RowCount 'No hay cursor
End Function


' obtiene el separador de sheridan
Public Function GetDelSep(ByVal objControl As Object, _
                          ByVal blnDelim As Boolean) As String
  Dim strRet As String
  
  If blnDelim Then
    Select Case UCase(objControl.FieldDelimiter)
      Case "(NONE)"
        strRet = ""
      Case Else
        strRet = objControl.FieldDelimiter
    End Select
  Else
    Select Case UCase(objControl.FieldSeparator)
      Case "(TAB)"
        strRet = vbTab
      Case "(SPACE)"
        strRet = " "
      Case Else
        strRet = objControl.FieldSeparator
    End Select
  End If
  GetDelSep = strRet
End Function


' realiza una espera hasta que el cursor as�ncrono termina
' o hasta que devuelve al menos una fila
Public Sub WaitCursor(ByVal rdoCursor As rdoResultset, _
                      ByVal blnWaitReg As Boolean)
  If rdoCursor Is Nothing Then Exit Sub
  Do While rdoCursor.StillExecuting
    If blnWaitReg And GetRowCount(rdoCursor) > 0 Then
      Exit Do
    End If
  Loop
End Sub


' convierte de un tipo de variable de VB a un tipo v�lido para una sentencia
' SQL con/sin comillas y verificando NULL
Public Function ValueToSQL(ByVal strValue As String, _
                           ByVal intDataType As cwDataType) As String
  Dim strCommaInit As String
  Dim strCommaEnd  As String
  Dim intPos       As Integer
  
  On Error GoTo cwIntError
  
  Select Case intDataType
    Case cwNumeric
      strCommaInit = ""
      strCommaEnd = ""
      strValue = ReplaceStr(strValue, ".", "", 0)
      strValue = ReplaceStr(strValue, ",", "", 0)
    Case cwDecimal
      strCommaInit = ""
      strCommaEnd = ""
      intPos = InStr(strValue, ",")
      strValue = ReplaceStr(strValue, ".", "", 0)
      strValue = ReplaceStr(strValue, ",", ".", 1)
      strValue = ReplaceStr(strValue, ",", "", 0)
    Case cwBoolean
      strCommaInit = ""
      strCommaEnd = ""
      strValue = CStr(CInt(CBool(strValue)))
    Case cwDate
      strCommaInit = "{ts '"
      strCommaEnd = "'}"
      strValue = Format(strValue, "YYYY-MM-DD HH:MM:SS")
    Case Else
      strCommaInit = "'"
      strCommaEnd = "'"
  End Select
  ValueToSQL = IIf(IsStrEmpty(strValue), "NULL", strCommaInit & strValue & strCommaEnd)
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "DataGetValue")
End Function

' convierte una fecha de un formato a otro
Public Function ToDate(ByVal strDate As String) As String
  Dim dblDate As Double
  Dim lngDate As Long
  
  If IsDate(strDate) Then
    dblDate = CDbl(CDate(strDate))
    lngDate = CLng(dblDate)
    If dblDate - lngDate = 0 Then
      ToDate = Format(strDate, "DD/MM/YYYY")
    Else
      ToDate = Format(strDate, "DD/MM/YYYY HH:NN:SS")
    End If
  End If
End Function


' devuelve la fecha y hora de la base de datos
Public Function GetDBDateTime() As String
  Dim rdoCursorDB As rdoResultset
  Dim strSQL    As String
  
  On Error GoTo cwIntError
  
  strSQL = "SELECT to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') FROM dual"
  Set rdoCursorDB = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurValues)
  If rdoCursorDB.RowCount > 0 Then
    GetDBDateTime = rdoCursorDB(0).Value
  End If
  Call rdoCursorDB.Close
  Set rdoCursorDB = Nothing
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetDBDateTime")
End Function


Public Function NodeGetLevel(ByVal nodNode As ComctlLib.Node) As Integer
  Dim intLevel As Integer
  Dim nodDummy As ComctlLib.Node
  
  On Error Resume Next
  Set nodDummy = nodNode
  Do While Err.Number = 0
    Set nodDummy = nodDummy.Parent
    intLevel = intLevel + 1
  Loop
  intLevel = intLevel - 1
  Set nodDummy = Nothing
  NodeGetLevel = intLevel
End Function
