VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWEnvironment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWEnvironment
' Coded by SYSECA Bilbao
' **********************************************************************************


Dim mcllEnvs   As New Collection
Dim mcllValues As New Collection


' a�ade un tipo a la colecci�n
Public Sub AddEnv(ByVal strEnv As String)
  On Error GoTo cwIntError
  
  Call mcllEnvs.Add(strEnv, strEnv)
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "AddEnv", strEnv)
End Sub

Public Property Get GetEnvs() As Collection
  Set GetEnvs = mcllEnvs
End Property

Public Sub AddValue(ByVal strEnv As String, _
                    ByVal strItem As String, _
                    ByVal strValue As String)
  Dim strDummy As String
  
  On Error GoTo cwIntError
  
  strDummy = mcllEnvs(strEnv)
  Call mcllValues.Add(strValue, strEnv & strItem)
  Exit Sub

cwIntError:
  Call objError.InternalError(Me, "AddValue", strItem)
End Sub

Public Function GetValue(ByVal strItem As String) As String
  On Error GoTo cwIntError
  
  GetValue = mcllValues(mcllEnvs(objApp.intUserEnv + 1) & strItem)
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetValue", strItem)
End Function

Public Function ExistValue(ByVal strItem As String) As Boolean
  Dim strDummy As String
  
  On Error Resume Next
  strDummy = mcllValues(mcllEnvs(objApp.intUserEnv + 1) & strItem)
  ExistValue = (Err.Number = 0)
End Function


