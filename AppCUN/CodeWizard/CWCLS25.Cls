VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWFieldSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Module clsCWFieldSearch
' Coded by SYSECA Bilbao
' **********************************************************************************


Public strField     As String
Public blnInDialog  As Boolean
Public blnInGrid    As Boolean
Public strSmallDesc As String
Public strBigDesc   As String
Public intSize      As Integer
Public intType      As cwDataType
Public intMask      As cwMaskType
Public intKeyNo     As Integer
Public blnMandatory As Boolean
Public blnReadOnly  As Boolean
Public objControl   As Object


Private Sub Class_Initialize()
  blnInDialog = True
  blnInGrid = True
  intSize = 10
  intType = cwString
  intMask = cwMaskString
End Sub
