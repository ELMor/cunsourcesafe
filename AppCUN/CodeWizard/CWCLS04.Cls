VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWFilterFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Class clsCWFilterFields
' Coded by SYSECA Bilbao
' **********************************************************************************


' esta estructura identifica a cada control
' de datos que se incluye en filtros
' suministrando datos sobre el mismo


Public strFieldName As String               ' nombre del campo
Public strFieldDesc As String               ' descripción del campo
Public intFieldType As Integer              ' tipo del campo
Public cllValues    As New Collection       ' valores del campo en el filtro
Public strSQL       As String               ' sql si es una foreing key

