VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'LISTADOS
Const ADRepPersonal              As String = "AD002011"
Const ADRepTipoDepartamento      As String = "AD002021"
Const ADRepCategoria             As String = "AD002051"
Const ADRepPuestoCategoria       As String = "AD002052"
Const ADRepDepartamentoTipo      As String = "AD002061"
Const ADRepTipoAsistencia        As String = "AD003011"
Const ADRepTipoPaciente          As String = "AD003021"
Const ADRepTipoDocumento         As String = "AD003031"
Const ADRepParteSobre            As String = "AD003041"
Const ADRepMotivoAltaAsistencia  As String = "AD003051"
Const ADRepCama                  As String = "AD004011"
Const ADRepTipoCama              As String = "AD004021"
Const ADRepEstadoCama            As String = "AD004031"
Const ADRepMotivoAlta            As String = "AD004041"
Const ADRepMotivoOcupacionCama   As String = "AD004061"
Const ADRepMotivoCambioCama      As String = "AD004071"
Const ADRepDocumentacionAportada As String = "AD005011"

'VENTANAS
Const ADWinProcesos                            As String = "AD0102"
Const ADWinFinAsistencia                       As String = "AD0113"
Const ADWinReasignarAsistencia                 As String = "AD0114"
Const ADWinAsignarCama                         As String = "AD0121"
Const ADWinAsistencias                         As String = "AD0123"
Const ADWinResponsables                        As String = "AD0128"
Const ADWinTipoDepartamento                    As String = "AD0202"
Const ADWinDepartamentosPersonalPuestos        As String = "AD0203"
Const ADWinCategorias                          As String = "AD0205"
Const ADWinPuestos                             As String = "AD0207"
Const ADWinTipoAsistencia                      As String = "AD0301"
Const ADWinTipoPaciente                        As String = "AD0302"
Const ADWinTipoDocumentoAportado               As String = "AD0303"
Const ADWinParteSobreHistoria                  As String = "AD0304"
Const ADWinMotivoAltaAsistencia                As String = "AD0305"
Const ADWinGestionCamas                        As String = "AD0401"
Const ADWinTipoCamas                           As String = "AD0402"
Const ADWinEstadosCama                         As String = "AD0403"
Const ADWinMotivoCambioCama                    As String = "AD0406"
Const ADWinSituacionCamas                      As String = "AD0408"
Const ADWinAbrirAsistencia                     As String = "AD0550" 'HAY QUE A�ADIRLO A LA MATRIZ
Const ADWinEntradasArchivos                    As String = "AD0602"
Const ADWinSalidasArchivos                     As String = "AD0603" 'HAY QUE A�ADIRLO A LA MATRIZ
Const ADWinMovimientosNuevo                    As String = "AD0604"
Const ADWinEscogerProceso                      As String = "AD0701"
Const ADWinPendVolante                         As String = "AD0801"
Const ADWinTipoVolPacInsalud                   As String = "AD0901"
Const ADWinInspPacInsalud                      As String = "AD0902"
Const ADWinEstados                             As String = "AD0904"
Const ADWinIngresoPend                         As String = "AD1000" 'HAY QUE A�ADIRLO A LA MATRIZ
Const ADWinPacientesIngr                       As String = "AD1010" 'HAY QUE A�ADIRLO A LA MATRIZ
Const ADWinAsocProcAsis                        As String = "AD1020"
Const ADWinPacientesIngrNW                     As String = "AD1011" 'HAY QUE A�ADIRLO A LA MATRIZ

'AD1021 --> Llamada directa, sin proceso
'AD1022 --> Llamada directa, sin proceso
Const ADWinVisionGlobal                        As String = "AD1126"
Const ADWinPruebaspendientesdpto               As String = "AD1127"
Const ADWinActuacionesPlanificadas             As String = "AD2105"
Const ADWinBusMedicosRem                       As String = "AD2127"
Const ADWinDocumentacion                       As String = "AD2501"
Const ADWinAsitenciasAbiertas                  As String = "AD2600"
Const ADWinCargaInicial                        As String = "AD2610"
Const ADWinRevEntArchivo                       As String = "AD2620"
Const ADWinConsultasPacienteDia                As String = "AD2621"
Const ADWinSalidasMasivas                      As String = "AD2630"
Const ADWinDoctoresUrgencias                   As String = "AD2640"
Const ADWinBuscadorPersonasFisicas             As String = "AD2999"
Const ADWinDatosAsistencia                     As String = "AD3000"
Const ADWinHistoriasPsiquiatria                As String = "AD3620"
Const ADWinListHistPsiquiatria                 As String = "AD3630"
'AD3001 --> Llamada directa, sin proceso
'AD3002 --> Llamada directa, sin proceso
'AD3003 --> Llamada directa, sin proceso
'AD3004 --> Llamada directa, sin proceso
'AD3005 --> Llamada directa, sin proceso
Const ADWinMantDatosEconPA                     As String = "AD3006"
'AD3007 --> Llamada directa, sin proceso
'AD3008 --> Llamada directa, sin proceso
Const ADWinAsocTipoEcon_RespEcon               As String = "AD3009"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objSecurity = mobjCW.objSecurity
End Sub

Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean
' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based

    Dim a As Variant
    
    On Error Resume Next
  
    'fija el valor de retorno a verdadero
    LaunchProcess = True
    
    Select Case strProcess
    Case ADWinActuacionesPlanificadas
        'vntData(2)Indica si viene de citas valor 2
        'Si viene de vision global valor 1
        gintProcedencia = vntData(2)
        lngPerCodVi = vntData(1)
        Load frmActuacionesPlanificadasNW
        Call objSecurity.AddHelpContext(2)
        Call frmActuacionesPlanificadasNW.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        lngPerCodVi = 0
        Unload frmActuacionesPlanificadasNW
        Set frmActuacionesPlanificadasNW = Nothing
      
    Case ADWinAsignarCama
        Load frmAsignarCama
        Call objSecurity.AddHelpContext(32)
        Call frmAsignarCama.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmAsignarCama
        Set frmAsignarCama = Nothing
      
    Case ADWinAsistencias
        Load frmAsistencias
        Call objSecurity.AddHelpContext(27)
        Call frmAsistencias.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmAsistencias
        Set frmAsistencias = Nothing
    
    Case ADWinDatosAsistencia
        a = vntData(1)
        If Err = 0 Then Call objPipe.PipeSet("AD3000_CI21CODPERSONA", a)
        Err = 0
        frmDatosAsistencia.Show vbModal
        Set frmDatosAsistencia = Nothing
        
    Case ADWinMantDatosEconPA
        Call objPipe.PipeSet("AD3006_AD01CODASISTENCI", vntData(1))
        Call objPipe.PipeSet("AD3006_AD07CODPROCESO", vntData(2))
        Call objPipe.PipeSet("AD3006_CI22NUMHISTORIA", vntData(3))
        frmMantDatosEconPA.Show vbModal
        Set frmMantDatosEconPA = Nothing
    
    Case ADWinAsocTipoEcon_RespEcon
        If Not IsMissing(vntData(1)) Then
            Call objPipe.PipeSet("AD_CodRespEco", vntData(1))
        End If
        frmMantRespEcon.Show vbModal
        Set frmMantRespEcon = Nothing
      
    Case ADWinBuscadorPersonasFisicas
        frmBuscaPersonas.Show vbModal
        Set frmBuscaPersonas = Nothing
        
    Case ADWinCategorias
        Load frmCategorias
        Call objSecurity.AddHelpContext(12)
        Call frmCategorias.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmCategorias
        Set frmCategorias = Nothing
      
    Case ADWinPuestos
        Load frmPuestos
        Call objSecurity.AddHelpContext(12)
        Call frmPuestos.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmPuestos
        Set frmPuestos = Nothing
     
    Case ADWinDepartamentosPersonalPuestos
        Load frmDepartamentos
        Call objSecurity.AddHelpContext(11)
        Call frmDepartamentos.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmDepartamentos
        Set frmDepartamentos = Nothing
    
    Case ADWinDocumentacion
         If Not IsMissing(vntData) Then
            Call objPipe.PipeSet("Persona", vntData(0))
            Call objPipe.PipeSet("Asistencia", vntData(1))
            Call objPipe.PipeSet("Proceso", vntData(2))
        End If
        Load frmDocumentacionNW
        Call objSecurity.AddHelpContext(25)
        Call frmDocumentacionNW.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmDocumentacionNW
        Set frmDocumentacionNW = Nothing
        'If objPipe.PipeExist("Persona") Then Call objPipe.PipeRemove("Persona")
        'If objPipe.PipeExist("Asistencia") Then Call objPipe.PipeRemove("Asistencia")
        'If objPipe.PipeExist("Proceso") Then Call objPipe.PipeRemove("Proceso")
        'If objPipe.PipeExist("DeDondeViene") Then Call objPipe.PipeRemove("DeDondeViene")
    
    Case ADWinEstadosCama
        Load frmEstadosCama
        Call objSecurity.AddHelpContext(21)
        Call frmEstadosCama.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmEstadosCama
        Set frmEstadosCama = Nothing
      
    Case ADWinFinAsistencia
        If Not IsMissing(vntData) Then
            Call objPipe.PipeSet("CODPERSONA_FROM_AD1020", vntData(2))
            Call objPipe.PipeSet("CODASISTENCIA_FROM_AD1020", vntData(3))
        End If
        Load frmFinAsistencia
        Call objSecurity.AddHelpContext(5)
        Call frmFinAsistencia.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmFinAsistencia
        Set frmFinAsistencia = Nothing
      
    Case ADWinCargaInicial
        Load frmCargaInicial
        Call objSecurity.AddHelpContext(26)
        Call frmCargaInicial.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmCargaInicial
        Set frmCargaInicial = Nothing
      
    Case ADWinGestionCamas
        Load frmGestionCamas
        Call objSecurity.AddHelpContext(20)
        Call frmGestionCamas.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmGestionCamas
        Set frmGestionCamas = Nothing
      
    Case ADWinMotivoAltaAsistencia
        Load frmMotivoAltaAsistencia
        Call objSecurity.AddHelpContext(18)
        Call frmMotivoAltaAsistencia.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmMotivoAltaAsistencia
        Set frmMotivoAltaAsistencia = Nothing
      
    Case ADWinMotivoCambioCama
        Load frmMotivoCambioCama
        Call objSecurity.AddHelpContext(23)
        Call frmMotivoCambioCama.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmMotivoCambioCama
        Set frmMotivoCambioCama = Nothing
      
    Case ADWinMovimientosNuevo
        Load frmNuevosMovimientos
        Call objSecurity.AddHelpContext(26)
        Call frmNuevosMovimientos.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmNuevosMovimientos
        Set frmNuevosMovimientos = Nothing
     
    Case ADWinEntradasArchivos
        Load frmEntradasArchivo
        Call objSecurity.AddHelpContext(26)
        Call frmEntradasArchivo.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmEntradasArchivo
        Set frmEntradasArchivo = Nothing
     
    Case ADWinParteSobreHistoria
        Load frmParteSobreHistoria
        Call objSecurity.AddHelpContext(17)
        Call frmParteSobreHistoria.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmParteSobreHistoria
        Set frmParteSobreHistoria = Nothing
      
    Case ADWinProcesos
        Load frmProcesos
        Call objSecurity.AddHelpContext(3)
        Call frmProcesos.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmProcesos
        Set frmProcesos = Nothing
      
    Case ADWinReasignarAsistencia
        Load frmReasignarAsistencia
        Call objSecurity.AddHelpContext(6)
        Call frmReasignarAsistencia.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmReasignarAsistencia
        Set frmReasignarAsistencia = Nothing
      
    Case ADWinSituacionCamas
        Load frmSituacionCamas
        Call objSecurity.AddHelpContext(29)
        Call frmSituacionCamas.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmSituacionCamas
        Set frmSituacionCamas = Nothing
      
    Case ADWinTipoAsistencia
        Load frmTipoAsistencia
        Call objSecurity.AddHelpContext(14)
        Call frmTipoAsistencia.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmTipoAsistencia
        Set frmTipoAsistencia = Nothing
      
    Case ADWinTipoDepartamento
        Load frmTipoDepartamento
        Call objSecurity.AddHelpContext(10)
        Call frmTipoDepartamento.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmTipoDepartamento
        Set frmTipoDepartamento = Nothing
      
    Case ADWinTipoDocumentoAportado
        Load frmTipoDocumentoAportado
        Call objSecurity.AddHelpContext(16)
        Call frmTipoDocumentoAportado.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmTipoDocumentoAportado
        Set frmTipoDocumentoAportado = Nothing
      
    Case ADWinTipoPaciente
        Load frmTipoPaciente
        Call objSecurity.AddHelpContext(15)
        Call frmTipoPaciente.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmTipoPaciente
        Set frmTipoPaciente = Nothing
    
    Case ADWinTipoCamas
        Load frmTiposCamas
        Call objSecurity.AddHelpContext(22)
        Call frmTiposCamas.Show(vbModal)
        Call objSecurity.RemoveHelpContext
        Unload frmTiposCamas
        Set frmTiposCamas = Nothing
   
    Case ADWinEscogerProceso
        frmEscogerProceso.Show vbModal
        Set frmEscogerProceso = Nothing
   
    Case ADWinPendVolante
        frmPendVolante.Show vbModal
        Set frmPendVolante = Nothing
   
'    Case ADWinVisionGlobal
'        'vntdata(1): Codigo de persona
'        lngPerCod = vntData(1)
'        frmVisualizarNW.Show vbModal
'        Set frmVisualizarNW = Nothing
  
    Case ADWinEstados
        FrmMantEstados.Show vbModal
        Set FrmMantEstados = Nothing
    
    Case ADWinTipoVolPacInsalud
        frmTipoVolPacInsalud.Show vbModal
        Set frmTipoVolPacInsalud = Nothing
    
    Case ADWinInspPacInsalud
        frmInspPacInsalud.Show vbModal
        Set frmInspPacInsalud = Nothing
    
    Case ADWinIngresoPend
        frmIngresoPend.Show vbModal
        Set frmIngresoPend = Nothing
    
    Case ADWinResponsables
        frmResponsables.Show vbModal
        Set frmResponsables = Nothing
    
    Case ADWinBusMedicosRem
        frmBusMedicosRemNW.Show vbModal
        Set frmBusMedicosRemNW = Nothing
    
    Case ADWinPacientesIngr
        frmPacientesIngr.Show vbModal
        Set frmPacientesIngr = Nothing
        
    Case ADWinPacientesIngrNW
        frmPacientesIngrNW.Show vbModal
        Set frmPacientesIngrNW = Nothing
    
    Case ADWinSalidasArchivos
        frmSalidasArchivo.Show vbModal
        Set frmSalidasArchivo = Nothing
    
    Case ADWinAbrirAsistencia
        frmAbrirAsistencias.Show vbModal
        Set frmAbrirAsistencias = Nothing
    
    Case ADWinAsitenciasAbiertas
        frmAsistenciasAbiertas.Show vbModal
        Set frmAsistenciasAbiertas = Nothing
        
''        frmAsistenciasAbiertas1.Show vbModal
''        Set frmAsistenciasAbiertas1 = Nothing
    
    Case ADWinRevEntArchivo
        frmRevEntArch.Show vbModal
        Set frmRevEntArch = Nothing
    
    Case ADWinSalidasMasivas
        frmSalidasMasivas.Show vbModal
        Set frmSalidasMasivas = Nothing
    
    Case ADWinConsultasPacienteDia
        frmConsultaPacientesdia.Show vbModal
        Set frmConsultaPacientesdia = Nothing
    
    Case ADWinPruebaspendientesdpto
        frmpruebaspendientesdpto.Show vbModal
        Set frmpruebaspendientesdpto = Nothing
   
    Case ADWinAsocProcAsis
        Call objPipe.PipeSet("CI_CODPER", vntData(1)) ' vntdata(1) codigo de persona
        Call objPipe.PipeSet("PR_ACTPEDI", vntData(2)) ' vntdata(2) numeros de actuacion pedida
        Call objPipe.PipeSet("AD_PROCESO", vntData(3)) ' vntdata(3) proceso (0 si no hay proceso)
        Call objPipe.PipeSet("CI_HISTORIA", vntData(4)) ' vntData(4) historia
        Call objPipe.PipeSet("DPTOSOL", vntData(5)) ' vntData(5) Dto Responsable
        Call objPipe.PipeSet("DRSOL", vntData(6)) ' vntData(6) Dr. Responsable
        Call objPipe.PipeSet("PR_ACTPLAN", vntData(7))
        frmAsoProcAsis.Show vbModal
        Set frmAsoProcAsis = Nothing
        Call objPipe.Remove("CI_CODPER")
        Call objPipe.Remove("PR_ACTPLAN")
        Call objPipe.Remove("AD_PROCESO")
        Call objPipe.Remove("CI_HISTORIA")
        Call objPipe.Remove("DPTOSOL")
        Call objPipe.Remove("DRSOL")
        Call objPipe.Remove("PR_ACTPLAN")
    
    Case ADWinDoctoresUrgencias
        'vntData(0) asistencia
        'vntdata(1) proceso
        Call objPipe.PipeSet("AD01CODASISTENCI", vntData(0))
        Call objPipe.PipeSet("AD07CODPROCESO", vntData(1))
        frmDoctoresUrgencias.Show vbModal
        Call objPipe.PipeRemove("AD07CODPROCESO")
        Call objPipe.PipeRemove("AD01CODASISTENCI")
        Set frmDoctoresUrgencias = Nothing
    Case ADWinHistoriasPsiquiatria
        frmHisPsiquiatria.Show vbModal
        Set frmHisPsiquiatria = Nothing
    Case ADWinListHistPsiquiatria
        frmListPsiquiatria.Show vbModal
        Set frmListPsiquiatria = Nothing
        
'LISTADOS
    Case ADRepPersonal
      'Call AD002011
    Case ADRepTipoDepartamento
      'Call AD002021
    Case ADRepCategoria
      'Call AD002051
    Case ADRepPuestoCategoria
      'Call AD002052
    Case ADRepDepartamentoTipo
      'Call AD002061
    Case ADRepTipoAsistencia
      'Call AD003011
    Case ADRepTipoPaciente
      'Call AD003021
    Case ADRepTipoDocumento
      'Call AD003031
    Case ADRepParteSobre
      'Call AD003041
    Case ADRepMotivoAltaAsistencia
      'Call AD003051
    Case ADRepCama
      'Call AD004011
    Case ADRepTipoCama
      'Call AD004021
    Case ADRepEstadoCama
      'Call AD004031
    Case ADRepMotivoAlta
      'Call AD004041
    Case ADRepMotivoOcupacionCama
      'Call AD004061
    Case ADRepMotivoCambioCama
      'Call AD004071
    Case ADRepDocumentacionAportada
      'Call AD005011
    Case Else
      LaunchProcess = False
    End Select
    Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  ' Redimensionar la matriz al n� de procesos existentes
  ReDim aProcess(1 To 63, 1 To 4) As Variant
      
  ' VENTANAS
  'aProcess(1, 1) = Nombre_ventana
  'aProcess(1, 2) = "Titulo....."
  'aProcess(1, 3) = True (Si aparece en menu principal)
  'aProcess(1, 4) = cwTypeWindow /ventana
  'aProcess(1, 4) = cwTypereport/ Listado
  'aProcess(1, 4) = cwTypeRoutina
        
  aProcess(1, 1) = ADWinProcesos
  aProcess(1, 2) = "Procesos de Pacientes"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = ADWinAsignarCama
  aProcess(2, 2) = "Asignar Cama"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
      
  aProcess(3, 1) = ADWinDatosAsistencia
  aProcess(3, 2) = "Cambios de Asistencias Nuevo"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = ADWinMantDatosEconPA
  aProcess(4, 2) = "Mant. Datos Econ�micos del P/A"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = ADWinAsocTipoEcon_RespEcon
  aProcess(5, 2) = "Asociar Responsables/Tipos Econ�micos"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = ADWinRevEntArchivo
  aProcess(6, 2) = "Revision o Entrada con Lector"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = ADWinConsultasPacienteDia
  aProcess(7, 2) = "Consulta Pacientes del d�a"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = ADWinSalidasMasivas
  aProcess(8, 2) = "Salida Masiva de Archivo"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = ADWinDoctoresUrgencias
  aProcess(9, 2) = "Doctores Urgencias"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
            
  aProcess(10, 1) = ADWinCategorias
  aProcess(10, 2) = "Categorias"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
        
  aProcess(11, 1) = ADWinAsistencias
  aProcess(11, 2) = "Asistencias"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
      
  aProcess(12, 1) = ADWinFinAsistencia
  aProcess(12, 2) = "Finalizar Asistencia"
  aProcess(12, 3) = True
  aProcess(12, 4) = cwTypeWindow
      
  aProcess(13, 1) = ADWinEstadosCama
  aProcess(13, 2) = "Estados de Camas"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = ADWinReasignarAsistencia
  aProcess(14, 2) = "Reasignar Asistencia a Proceso"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow

  aProcess(15, 1) = ADWinResponsables
  aProcess(15, 2) = "Crear Responsables"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = ADWinGestionCamas
  aProcess(16, 2) = "Camas"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
      
  aProcess(17, 1) = ADWinMotivoAltaAsistencia
  aProcess(17, 2) = "Motivo Alta Asistencia"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
      
  aProcess(18, 1) = ADWinMotivoCambioCama
  aProcess(18, 2) = "Motivo Cambio Cama"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = ADWinTipoDepartamento
  aProcess(19, 2) = "Tipo Departamento"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
         
  aProcess(20, 1) = ADWinParteSobreHistoria
  aProcess(20, 2) = "Parte Sobre Historia"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = ADWinDepartamentosPersonalPuestos
  aProcess(21, 2) = "Departamentos / Personal / Puestos"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = ADWinPuestos
  aProcess(22, 2) = "Puestos"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = ADWinTipoAsistencia
  aProcess(23, 2) = "Tipo Asistencia"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeWindow

  aProcess(24, 1) = ADWinSituacionCamas
  aProcess(24, 2) = "Situacion Camas"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow
  
  aProcess(25, 1) = ADWinTipoPaciente
  aProcess(25, 2) = "Tipo Paciente"
  aProcess(25, 3) = True
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = ADWinTipoDocumentoAportado
  aProcess(26, 2) = "Tipo Documento Aportado"
  aProcess(26, 3) = True
  aProcess(26, 4) = cwTypeWindow
  
  aProcess(27, 1) = ADWinTipoCamas
  aProcess(27, 2) = "Tipo  de Camas"
  aProcess(27, 3) = True
  aProcess(27, 4) = cwTypeWindow
    
  aProcess(28, 1) = ADWinEntradasArchivos
  aProcess(28, 2) = "Entradas de Archivos"
  aProcess(28, 3) = True
  aProcess(28, 4) = cwTypeWindow
  
  aProcess(29, 1) = ADWinMovimientosNuevo
  aProcess(29, 2) = "Movimientos del sobre Nuevo"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow

  aProcess(30, 1) = ADRepPersonal
  aProcess(30, 2) = "Personal"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport
  
  aProcess(31, 1) = ADRepTipoDepartamento
  aProcess(31, 2) = "Tipo Departamento"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport
  
  aProcess(32, 1) = ADRepCategoria
  aProcess(32, 2) = "Categoria"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeReport
  
  aProcess(33, 1) = ADRepPuestoCategoria
  aProcess(33, 2) = "Puesto Categoria"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport
  
  aProcess(34, 1) = ADRepDepartamentoTipo
  aProcess(34, 2) = "Departamento por Tipo"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport
  
  aProcess(35, 1) = ADRepTipoAsistencia
  aProcess(35, 2) = "Tipo Asistencia"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeReport
  
  aProcess(36, 1) = ADRepTipoPaciente
  aProcess(36, 2) = "Tipo Paciente"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport
  
  aProcess(37, 1) = ADRepTipoDocumento
  aProcess(37, 2) = "Tipo Documento"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeReport
  
  aProcess(38, 1) = ADRepParteSobre
  aProcess(38, 2) = "Parte Sobre"
  aProcess(38, 3) = False
  aProcess(38, 4) = cwTypeReport
  
  aProcess(39, 1) = ADRepMotivoAltaAsistencia
  aProcess(39, 2) = "Motivo Alta Asistencia"
  aProcess(39, 3) = False
  aProcess(39, 4) = cwTypeReport
  
  aProcess(40, 1) = ADRepCama
  aProcess(40, 2) = "Cama"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeReport
  
  aProcess(41, 1) = ADRepTipoCama
  aProcess(41, 2) = "Tipo Cama"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport
  
  aProcess(42, 1) = ADRepEstadoCama
  aProcess(42, 2) = "Estado Cama"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport
  
  aProcess(43, 1) = ADRepMotivoAlta
  aProcess(43, 2) = "Motivo Alta"
  aProcess(43, 3) = False
  aProcess(43, 4) = cwTypeReport
  
  aProcess(44, 1) = ADRepMotivoOcupacionCama
  aProcess(44, 2) = "Motivo Ocupacion Cama"
  aProcess(44, 3) = False
  aProcess(44, 4) = cwTypeReport
  
  aProcess(45, 1) = ADRepMotivoCambioCama
  aProcess(45, 2) = "Motivo Cambio Cama"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeReport
  
  aProcess(46, 1) = ADRepDocumentacionAportada
  aProcess(46, 2) = "Documentacion Aportada"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeReport
  
  aProcess(47, 1) = ADWinEscogerProceso
  aProcess(47, 2) = "Escoger Proceso"
  aProcess(47, 3) = False
  aProcess(47, 4) = cwTypeWindow
  
  aProcess(48, 1) = ADWinPendVolante
  aProcess(48, 2) = "Pendientes de Volante"
  aProcess(48, 3) = True
  aProcess(48, 4) = cwTypeWindow
  
  aProcess(49, 1) = ADWinTipoVolPacInsalud
  aProcess(49, 2) = "Tipo Volante Paciente INSALUD"
  aProcess(49, 3) = True
  aProcess(49, 4) = cwTypeWindow
  
  aProcess(50, 1) = ADWinInspPacInsalud
  aProcess(50, 2) = "Inspecci�n Paciente INSALUD"
  aProcess(50, 3) = True
  aProcess(50, 4) = cwTypeWindow
  
  aProcess(51, 1) = ADWinEstados
  aProcess(51, 2) = "Mantenimiento de Estados"
  aProcess(51, 3) = True
  aProcess(51, 4) = cwTypeWindow
  
  aProcess(52, 1) = ADWinAsocProcAsis
  aProcess(52, 2) = "Asociar Proceso Asistencia"
  aProcess(52, 3) = False
  aProcess(52, 4) = cwTypeWindow
  
  aProcess(53, 1) = ADWinVisionGlobal
  aProcess(53, 2) = "Vision Global"
  aProcess(53, 3) = False
  aProcess(53, 4) = cwTypeWindow
    
  aProcess(54, 1) = ADWinPruebaspendientesdpto
  aProcess(54, 2) = "Pruebas Pendientes por Dpto"
  aProcess(54, 3) = True
  aProcess(54, 4) = cwTypeWindow

  aProcess(55, 1) = ADWinActuacionesPlanificadas
  aProcess(55, 2) = "Admitir Paciente Nueva"
  aProcess(55, 3) = True
  aProcess(55, 4) = cwTypeWindow
  
  aProcess(56, 1) = ADWinBusMedicosRem
  aProcess(56, 2) = "Crear Responsables Nueva"
  aProcess(56, 3) = False
  aProcess(56, 4) = cwTypeWindow
  
  aProcess(57, 1) = ADWinDocumentacion
  aProcess(57, 2) = "Documentaci�n Nueva"
  aProcess(57, 3) = True
  aProcess(57, 4) = cwTypeWindow
  
  aProcess(58, 1) = ADWinAsitenciasAbiertas
  aProcess(58, 2) = "Asistencias Abiertas"
  aProcess(58, 3) = True
  aProcess(58, 4) = cwTypeWindow
  
  aProcess(59, 1) = ADWinCargaInicial
  aProcess(59, 2) = "Carga Inicial"
  aProcess(59, 3) = True
  aProcess(59, 4) = cwTypeWindow
  
  aProcess(60, 1) = ADWinBuscadorPersonasFisicas
  aProcess(60, 2) = "Buscador Personas F�sicas"
  aProcess(60, 3) = False
  aProcess(60, 4) = cwTypeWindow
  
  aProcess(61, 1) = ADWinHistoriasPsiquiatria
  aProcess(61, 2) = "Historias Psiquiatria (entrada manual)"
  aProcess(61, 3) = True
  aProcess(61, 4) = cwTypeWindow
  
  aProcess(62, 1) = ADWinListHistPsiquiatria
  aProcess(62, 2) = "Historias Psiquiatria (por lista)"
  aProcess(62, 3) = True
  aProcess(62, 4) = cwTypeWindow
  
  aProcess(63, 1) = ADWinPacientesIngrNW
  aProcess(63, 2) = "Pacientes Ingresados"
  aProcess(63, 3) = True
  aProcess(63, 4) = cwTypeWindow
End Sub
