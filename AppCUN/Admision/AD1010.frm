VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPacientesIngr 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ADMISION. Consulta de Pacientes Ingresados"
   ClientHeight    =   7140
   ClientLeft      =   720
   ClientTop       =   1470
   ClientWidth     =   9075
   ControlBox      =   0   'False
   FillColor       =   &H00404040&
   FillStyle       =   0  'Solid
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7140
   ScaleWidth      =   9075
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BUTCon 
      Caption         =   "Listado por plantas"
      Height          =   375
      Index           =   4
      Left            =   5160
      TabIndex        =   27
      Top             =   6360
      Width           =   1515
   End
   Begin VB.CommandButton BUTCon 
      Caption         =   "Imprimir"
      Height          =   375
      Index           =   3
      Left            =   3540
      TabIndex        =   23
      Top             =   6360
      Width           =   1515
   End
   Begin VB.CommandButton BUTCon 
      Caption         =   "Limpiar"
      Height          =   375
      Index           =   2
      Left            =   1920
      TabIndex        =   13
      Top             =   6360
      Width           =   1515
   End
   Begin VB.Frame fraConsulta 
      Caption         =   "Ordenaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   2055
      Index           =   1
      Left            =   6840
      TabIndex        =   19
      Top             =   120
      Width           =   1935
      Begin VB.CheckBox CHKConsulta 
         Caption         =   "N� Historia"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   25
         Top             =   1680
         Width           =   1335
      End
      Begin VB.CheckBox CHKConsulta 
         Caption         =   "Departamento"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   10
         Top             =   1440
         Width           =   1335
      End
      Begin VB.CheckBox CHKConsulta 
         Caption         =   "Fecha Ingreso"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   9
         Top             =   1170
         Width           =   1335
      End
      Begin VB.CheckBox CHKConsulta 
         Caption         =   "T.Eco./Entid."
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   8
         Top             =   900
         Width           =   1695
      End
      Begin VB.CheckBox CHKConsulta 
         Caption         =   "Alfabetico"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   630
         Width           =   1335
      End
      Begin VB.CheckBox CHKConsulta 
         Caption         =   "Camas"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame fraConsulta 
      Caption         =   "Filtros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   1935
      Index           =   0
      Left            =   240
      TabIndex        =   17
      Top             =   120
      Width           =   6495
      Begin VB.TextBox TXTConsulta 
         Height          =   375
         Index           =   2
         Left            =   4440
         TabIndex        =   2
         Top             =   360
         Width           =   1935
      End
      Begin VB.TextBox TXTConsulta 
         Height          =   375
         Index           =   1
         Left            =   2520
         TabIndex        =   1
         Top             =   360
         Width           =   1815
      End
      Begin VB.TextBox TXTConsulta 
         Height          =   375
         Index           =   0
         Left            =   1320
         TabIndex        =   0
         Top             =   360
         Width           =   1095
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   0
         Left            =   4680
         TabIndex        =   3
         Tag             =   "Fecha superior de las citas"
         Top             =   1320
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBConsulta 
         DataField       =   "AD10CODTIPPACIEN"
         Height          =   330
         Index           =   0
         Left            =   600
         TabIndex        =   4
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   840
         Width           =   2580
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4551
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBConsulta 
         DataField       =   "AD10CODTIPPACIEN"
         Height          =   330
         Index           =   1
         Left            =   1320
         TabIndex        =   5
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   1320
         Width           =   2100
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3704
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBConsulta 
         DataField       =   "AD10CODTIPPACIEN"
         Height          =   330
         Index           =   2
         Left            =   3840
         TabIndex        =   26
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   840
         Width           =   2580
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4551
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Doctor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   3240
         TabIndex        =   24
         Top             =   960
         Width           =   585
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Eco."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   22
         Top             =   1440
         Width           =   840
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Dpto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   21
         Top             =   960
         Width           =   420
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   20
         Top             =   480
         Width           =   765
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "F.Ingreso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   3720
         TabIndex        =   18
         Top             =   1440
         Width           =   810
      End
   End
   Begin VB.CommandButton BUTCon 
      Caption         =   "Salir"
      Height          =   375
      Index           =   1
      Left            =   7380
      TabIndex        =   14
      Top             =   6360
      Width           =   1455
   End
   Begin VB.CommandButton BUTCon 
      Caption         =   "Consultar"
      Height          =   375
      Index           =   0
      Left            =   300
      TabIndex        =   12
      Top             =   6360
      Width           =   1515
   End
   Begin VB.Frame fraConsulta 
      Caption         =   "Pacientes Ingresados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3960
      Index           =   2
      Left            =   240
      TabIndex        =   16
      Top             =   2160
      Width           =   8535
      Begin VsOcxLib.VideoSoftAwk Awk1 
         Left            =   7680
         Top             =   1680
         _Version        =   327680
         _ExtentX        =   847
         _ExtentY        =   847
         _StockProps     =   0
      End
      Begin Crystal.CrystalReport crtCrystalReport1 
         Left            =   7680
         Top             =   2280
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3465
         Index           =   0
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   8265
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   7
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   7
         Columns(0).Width=   1826
         Columns(0).Caption=   "F.Ingreso"
         Columns(0).Name =   "F.Ingreso"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6535
         Columns(1).Caption=   "Paciente"
         Columns(1).Name =   "Paciente"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1164
         Columns(2).Caption=   "Cama"
         Columns(2).Name =   "Habitacion"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1799
         Columns(3).Caption=   "Dpto."
         Columns(3).Name =   "Dpto."
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1429
         Columns(4).Caption=   "N� H�"
         Columns(4).Name =   "N� Historia"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Doctor"
         Columns(5).Name =   "Doctor"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1746
         Columns(6).Caption=   "T.Eco./Entid"
         Columns(6).Name =   "T.Eco."
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   14579
         _ExtentY        =   6112
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   6855
      Width           =   9075
      _ExtentX        =   16007
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
End
Attribute VB_Name = "frmPacientesIngr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: ADMISION (PACIENTES INGRESADOS)                            *
'* NOMBRE:                                                              *
'* AUTOR: MARIA JOSE DE MIGUEL                                          *
'* FECHA: 25 DE DICIEMBRE DE 1999                                       *
'* DESCRIPCI�N: Consultar Pacientes Ingresados                          *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

'Dim WithEvents objWinInfo As clsCWWin

Dim blnCitManual As Boolean
'numero de filas
Dim clBuscador As New Collection
Dim intFilaActual As Integer
'Constantes de Botones
Const iBUTConsulta = 0
Const iBUTSalir = 1
Const iBUTLimpiar = 2
Const iBUTImprimir = 3
Const iBUTImprimir_Listados = 4
'Constantes de Textos
Const iTXTNombre = 0
Const iTXTApellido1 = 1
Const iTXTApellido2 = 2
'Constantes de Combos
Const iCBODpto = 0
Const iCBOTEco = 1
Const iCBODoctor = 2
'Constantes de Checkbox
Const iCHKCama = 0
Const iCHKAlfa = 1
Const iCHKTEco = 2
Const iCHKFIni = 3
Const iCHKDpto = 4
Const iCHKNumHis = 5

Dim sStmSql As String
Private Sub BUTCon_Click(Index As Integer)
  Dim sNombre   As String
  Dim sApe1     As String
  Dim sApe2     As String
  Dim sFInicio  As String
  Dim sTEco     As String
  Dim sDpto     As String
  Dim sOrden    As String
  Dim sDoctor As String
        Select Case Index
        Case iBUTConsulta
            Screen.MousePointer = vbHourglass
            If dtcDateCombo1(0).Text <> "" Then
                If Not IsDate(dtcDateCombo1(0).Date) Then
                    MsgBox "La fecha no es una fecha valida", vbCritical
                    'Fecha iniciar a d�a de hoy
                    dtcDateCombo1(0).Date = ""
                    Exit Sub
                Else
                    sFInicio = CStr(dtcDateCombo1(0).Date)
                End If
            Else
                sFInicio = ""
            End If
            sNombre = Trim(TXTConsulta(iTXTNombre))
            sApe1 = Trim(TXTConsulta(iTXTApellido1))
            sApe2 = Trim(TXTConsulta(iTXTApellido2))
            If cboDBConsulta(iCBOTEco).Text <> "" Then
                sTEco = Trim(cboDBConsulta(iCBOTEco).Columns(0).Text)
            End If
            If cboDBConsulta(iCBODoctor).Text <> "" Then
                sDoctor = Trim(cboDBConsulta(iCBODoctor).Columns(0).Text)
            End If
            If cboDBConsulta(iCBODpto).Text <> "TODOS" Then
                sDpto = Trim(cboDBConsulta(iCBODpto).Text)
            End If
            sOrden = ""
            If CHKConsulta(iCHKCama).Value = 1 Then
                sOrden = sOrden & "CAMA, "
            End If
            If CHKConsulta(iCHKAlfa).Value = 1 Then
                sOrden = sOrden & "CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE, "
            End If
            If CHKConsulta(iCHKTEco).Value = 1 Then
                sOrden = sOrden & "CI32CODTIPECON, CI13CODENTIDAD, "
            End If
            If CHKConsulta(iCHKFIni).Value = 1 Then
                sOrden = sOrden & "AD25FECINICIO, "
            End If
            If CHKConsulta(iCHKDpto).Value = 1 Then
                sOrden = sOrden & "AD02DESDPTO, "
            End If
            If CHKConsulta(iCHKNumHis).Value = 1 Then
                sOrden = sOrden & "CI22NUMHISTORIA, "
            End If
            If sOrden <> "" Then
                sOrden = Mid(sOrden, 1, Len(sOrden) - 2)
            End If
            Call pDBPacenCama(sNombre, sApe1, sApe2, sFInicio, sTEco, sDpto, sDoctor, sOrden)
                
            Screen.MousePointer = vbDefault
        Case iBUTLimpiar
              Call pLimpiar
              
        Case iBUTSalir
            Unload Me
            
        Case iBUTImprimir
         Dim strWhereTotal   As String
         Dim strwhereand As String
         strWhereTotal = ""
         strwhereand = ""
'         If Trim(cboDBConsulta(iCBODpto).Text) <> "" Then
'            strWhereTotal = "{ad1504J.ad02coddpto}= " & cboDBConsulta(iCBODpto).Columns(0).Text
'            strwhereand = " and "
'         End If
         If Trim(cboDBConsulta(iCBODpto).Text) = "TODOS" Then
            strWhereTotal = ""
         Else
            strWhereTotal = "{ad1504J.ad02coddpto}= " & cboDBConsulta(iCBODpto).Columns(0).Text
            strwhereand = " and "
         End If
         If Trim(cboDBConsulta(iCBODoctor).Text) <> "" Then
            strWhereTotal = strWhereTotal & strwhereand & "{ad1504J.sg02cod}= '" & cboDBConsulta(iCBODoctor).Columns(0).Text & "'"
         End If
         crtCrystalReport1.ReportFileName = objApp.strReportsPath & "AD10100.RPT"
        With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToWindow
        .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Me.MousePointer = vbHourglass
        .Action = 1
        Me.MousePointer = vbDefault
        End With
        Case iBUTImprimir_Listados
        
            strWhereTotal = "ad1504J.ad02coddpto= " & cboDBConsulta(iCBODpto).Columns(0).Text
            Call Imprimir_API(strWhereTotal, "AD2012.rpt")
        End Select
End Sub



Private Sub cboDBConsulta_Change(Index As Integer)
Dim qry As rdoQuery
Dim rs As rdoResultset
  
    If Index = iCBODpto Then
        cboDBConsulta(iCBODoctor).RemoveAll
        cboDBConsulta(iCBODoctor).Text = ""
        If cboDBConsulta(iCBODpto).Text <> "" Then
            sStmSql = "SELECT DISTINCT SG02COD, DOCTOR "
            sStmSql = sStmSql & "FROM ad1504J "
            sStmSql = sStmSql & "where ad02coddpto= ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sStmSql)
                qry(0) = cboDBConsulta(iCBODpto).Columns(0).Value
            Set rs = qry.OpenResultset()
            cboDBConsulta(iCBODoctor).RemoveAll
            While Not rs.EOF
                cboDBConsulta(iCBODoctor).AddItem rs!SG02COD & Chr$(9) & rs!DOCTOR
                rs.MoveNext
            Wend
        End If
'        cboDBConsulta(2).Redraw = True
'        cboDBConsulta(2).Refresh
    End If
    
End Sub

Private Sub cboDBConsulta_CloseUp(Index As Integer)
Dim qry As rdoQuery
Dim rs As rdoResultset
  
    If Index = iCBODpto Then
        cboDBConsulta(iCBODoctor).RemoveAll
        cboDBConsulta(iCBODoctor).Text = ""
        If cboDBConsulta(iCBODpto).Text <> "" Then
            sStmSql = "SELECT DISTINCT SG02COD, DOCTOR "
            sStmSql = sStmSql & "FROM ad1504J "
            sStmSql = sStmSql & "where ad02coddpto= ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sStmSql)
                qry(0) = cboDBConsulta(iCBODpto).Columns(0).Value
            Set rs = qry.OpenResultset()
            cboDBConsulta(iCBODoctor).RemoveAll
            While Not rs.EOF
                cboDBConsulta(iCBODoctor).AddItem rs!SG02COD & Chr$(9) & rs!DOCTOR
                rs.MoveNext
            Wend
        End If
'        cboDBConsulta(2).Redraw = True
'        cboDBConsulta(2).Refresh
    End If

End Sub



Private Sub Form_Load()
    'Limpiamos la pantalla
    Call pLimpiar
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
End Sub
Private Sub pDBPacenCama(sNombre As String, sApe1 As String, sApe2 As String, sFInicio As String, sTEco As String, sDpto As String, sDoctor As String, sOrden As String)

'Dim sStmSql As String
Dim qry     As rdoQuery
Dim rst     As rdoResultset
Dim sRow    As String

    grdDBGrid1(0).RemoveAll
    
    sStmSql = "SELECT CAMA, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, "
    sStmSql = sStmSql & "CI22NUMHISTORIA, DOCTOR, "
    sStmSql = sStmSql & "TO_CHAR(AD25FECINICIO, 'DD/MM/YYYY HH24:MI:SS') FECHA, "
    sStmSql = sStmSql & "CI32CODTIPECON, AD02DESDPTO, CI13CODENTIDAD "
    sStmSql = sStmSql & "FROM AD1504J "
    If sNombre <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND CI22NOMBRE Like '%" & sNombre & "%' "
        Else
            sStmSql = sStmSql & "WHERE CI22NOMBRE Like '%" & sNombre & "%' "
        End If
    End If
    If sApe1 <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND CI22PRIAPEL Like '%" & sApe1 & "%' "
        Else
            sStmSql = sStmSql & "WHERE CI22PRIAPEL Like '%" & sApe1 & "%' "
        End If
    End If
    If sApe2 <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND CI22SEGAPEL Like '%" & sApe2 & "%' "
        Else
            sStmSql = sStmSql & "WHERE CI22SEGAPEL Like '%" & sApe2 & "%' "
        End If
    End If
    If CStr(sFInicio) <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND TRUNC(AD25FECINICIO) = TO_DATE('" & sFInicio & "', 'DD/MM/YYYY')"
        Else
            sStmSql = sStmSql & "WHERE TRUNC(AD25FECINICIO) = TO_DATE('" & sFInicio & "', 'DD/MM/YYYY')"
        End If
    End If
       
    If sTEco <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND CI32CODTIPECON = '" & sTEco & "' "
        Else
            sStmSql = sStmSql & "WHERE CI32CODTIPECON = '" & sTEco & "' "
        End If
    End If

    If sDpto <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND AD02DESDPTO = '" & sDpto & "' "
        Else
            sStmSql = sStmSql & "WHERE AD02DESDPTO = '" & sDpto & "' "
        End If
    End If
    If sDoctor <> "" Then
        If InStr(sStmSql, "WHERE") Then
            sStmSql = sStmSql & "AND SG02COD = '" & sDoctor & "' "
        Else
            sStmSql = sStmSql & "WHERE SG02COD = '" & sDoctor & "' "
        End If
    End If
    If sOrden <> "" Then
        sStmSql = sStmSql & "ORDER BY " & sOrden
    End If
    
    Set qry = objApp.rdoConnect.CreateQuery("", sStmSql)
    Set rst = qry.OpenResultset()
    Do While Not rst.EOF
        sRow = rst!fecha & Chr$(9)
        sRow = sRow & rst!CI22PRIAPEL & " " & rst!CI22SEGAPEL & ", " & rst!CI22NOMBRE & Chr$(9)
        sRow = sRow & rst!Cama & Chr$(9)
        sRow = sRow & rst!AD02DESDPTO & Chr$(9)
        sRow = sRow & rst!CI22NUMHISTORIA & Chr$(9)
        sRow = sRow & rst!DOCTOR & Chr$(9)
        sRow = sRow & rst!CI32CODTIPECON & "/" & rst!CI13CODENTIDAD
        grdDBGrid1(0).AddItem sRow
        rst.MoveNext
    Loop
    qry.Close
End Sub

Private Sub pLimpiar()
    Dim iCont As Integer
    'Textos
    For iCont = 0 To 2
        TXTConsulta(iCont).Text = ""
    Next
    'Fecha iniciar a d�a de hoy
    dtcDateCombo1(0).Date = ""
    
    'Textos
    For iCont = 0 To 2
        Call pCargaCombo(iCont)
'        cboDBConsulta(iCont).Value = ""
    Next
    'Chequeo de orden
    For iCont = 0 To 5
        CHKConsulta(iCont).Value = 0
    Next
    grdDBGrid1(0).RemoveAll
End Sub
Private Sub pCargaCombo(iCombo As Integer)
    Dim sStmSql     As String
    Dim rstCombo As rdoResultset
    Dim APE1 As String
    Dim APE2 As String
    Dim rs As rdoResultset
    Dim qry As rdoQuery
    Dim SQL As String
    
    cboDBConsulta(iCombo).RemoveAll
    
    Select Case iCombo
        Case iCBODpto
            SQL = "SELECT COUNT(*) FROM AD0300 WHERE SG02COD = ? "
            SQL = SQL & " AND AD02CODDPTO IN (" & constDPTO_INFORMACION
            SQL = SQL & "," & constDPTO_ADMINISTRACION & "," & constDPTO_ARCHIVO
            SQL = SQL & "," & constDPTO_UC & ")"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
                qry(0) = objSecurity.strUser
            Set rs = qry.OpenResultset()
            If rs(0) > 0 Then
                cboDBConsulta(iCombo).AddItem "0" & Chr(9) & "TODOS"
                sStmSql = "SELECT AD02CODDPTO, AD02DESDPTO "
                sStmSql = sStmSql & "FROM AD0200 "
                sStmSql = sStmSql & "WHERE AD0200.AD32CODTIPODPTO = 1 "      'Servicios clinicos
                sStmSql = sStmSql & "ORDER BY  AD02DESDPTO "
            Else
                sStmSql = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO "
                sStmSql = sStmSql & "FROM AD0200,AD0300 "
                sStmSql = sStmSql & "WHERE AD0200.AD32CODTIPODPTO = 1 "      'Servicios clinicos
                sStmSql = sStmSql & " AND AD0200.AD02CODDPTO = AD0300.AD02CODDPTO"
                sStmSql = sStmSql & " AND AD0300.SG02COD = '" & objSecurity.strUser
                sStmSql = sStmSql & "' ORDER BY  AD02DESDPTO "
            End If
            rs.Close
            qry.Close
        Case iCBODoctor
            If cboDBConsulta(iCBODpto).Columns(0).Value = "0" Then
                sStmSql = "SELECT DISTINCT SG02COD,DOCTOR "
                sStmSql = sStmSql & "FROM ad1504J "
                sStmSql = sStmSql & "ORDER BY DOCTOR "
            Else
                sStmSql = "SELECT DISTINCT SG02COD,DOCTOR "
                sStmSql = sStmSql & "FROM ad1504J "
                sStmSql = sStmSql & " WHERE AD02DESDPTO = '" & cboDBConsulta(iCBODpto).Columns(1).Value
                sStmSql = sStmSql & "' ORDER BY DOCTOR "
            End If
        Case iCBOTEco
            sStmSql = "SELECT  CI32CODTIPECON, CI32DESTIPECON "
            sStmSql = sStmSql & "FROM CI3200 "
            sStmSql = sStmSql & "ORDER BY CI32DESTIPECON "
    End Select
   
   Set rstCombo = objApp.rdoConnect.OpenResultset(sStmSql, rdOpenKeyset)
        While rstCombo.EOF = False
            cboDBConsulta(iCombo).AddItem rstCombo(0).Value & Chr(9) & rstCombo(1).Value
            rstCombo.MoveNext
        Wend
        rstCombo.Close
    If iCombo = iCBODpto Then
        cboDBConsulta(iCombo).Text = cboDBConsulta(iCombo).Columns(1).Value
    End If
End Sub



