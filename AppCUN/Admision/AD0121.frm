VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Begin VB.Form frmAsignarCama 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Asignar Cama"
   ClientHeight    =   8340
   ClientLeft      =   525
   ClientTop       =   420
   ClientWidth     =   10980
   ClipControls    =   0   'False
   Icon            =   "AD0121.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   10980
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtText1 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      Index           =   3
      Left            =   3000
      TabIndex        =   24
      TabStop         =   0   'False
      Tag             =   "Cama del Paciente|Cama del Paciente"
      Top             =   1320
      Width           =   915
   End
   Begin VB.TextBox txtText1 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      Index           =   2
      Left            =   6090
      TabIndex        =   22
      TabStop         =   0   'False
      Tag             =   "Sexo|Sexo de la Persona"
      Top             =   300
      Width           =   1395
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Camas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5520
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   2280
      Width           =   10770
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4785
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   390
         Width           =   10545
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "Libre"
         stylesets(0).BackColor=   65280
         stylesets(0).Picture=   "AD0121.frx":000C
         stylesets(1).Name=   "Ocupada"
         stylesets(1).BackColor=   16777215
         stylesets(1).Picture=   "AD0121.frx":0028
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18600
         _ExtentY        =   8440
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   3.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Index           =   3
      Left            =   120
      TabIndex        =   8
      Top             =   840
      Width           =   8145
      Begin VB.TextBox txtHora 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   5910
         MaxLength       =   2
         TabIndex        =   18
         Tag             =   "Hora de Preferencia"
         Top             =   480
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   1  'Right Justify
         Height          =   330
         HelpContextID   =   3
         Index           =   0
         Left            =   6570
         MaxLength       =   2
         TabIndex        =   17
         Tag             =   "Minutos de la Preferencia"
         Top             =   480
         Width           =   390
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   0
         Left            =   2880
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "Cama del Paciente|Cama del Paciente"
         Top             =   120
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.OptionButton optOption1 
         Caption         =   "Cama asignada al paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Value           =   -1  'True
         Width           =   2685
      End
      Begin ComCtl2.UpDown UpDownMinuto 
         Height          =   330
         Index           =   0
         Left            =   6960
         TabIndex        =   15
         Top             =   480
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         BuddyControl    =   "txtMinuto(0)"
         BuddyDispid     =   196613
         BuddyIndex      =   0
         OrigLeft        =   6360
         OrigTop         =   360
         OrigRight       =   6600
         OrigBottom      =   690
         Increment       =   5
         Max             =   55
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDownHora 
         Height          =   330
         Index           =   0
         Left            =   6300
         TabIndex        =   16
         Top             =   480
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         BuddyControl    =   "txtHora(0)"
         BuddyDispid     =   196612
         BuddyIndex      =   0
         OrigLeft        =   5760
         OrigTop         =   360
         OrigRight       =   6000
         OrigBottom      =   690
         Max             =   23
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   1
         Left            =   3960
         TabIndex        =   19
         Tag             =   "Fecha de Preferencia"
         Top             =   480
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Asignaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   3960
         TabIndex        =   21
         Top             =   240
         Width           =   1800
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora de Asignaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   5880
         TabIndex        =   20
         Top             =   240
         Width           =   1680
      End
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Selecci�n"
      Height          =   405
      Index           =   4
      Left            =   6360
      TabIndex        =   14
      Top             =   7860
      Width           =   1215
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Anular Asignaci�n"
      Height          =   405
      Index           =   3
      Left            =   1560
      TabIndex        =   13
      Top             =   7860
      Width           =   1755
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Asignar"
      Height          =   405
      Index           =   0
      Left            =   120
      TabIndex        =   12
      Top             =   7860
      Width           =   1395
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Salir"
      Height          =   405
      Index           =   1
      Left            =   9488
      TabIndex        =   11
      Top             =   7860
      Width           =   1395
   End
   Begin VB.TextBox txtText1 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      Index           =   5
      Left            =   9330
      TabIndex        =   3
      TabStop         =   0   'False
      Tag             =   "Sexo|Sexo de la Persona"
      Top             =   300
      Width           =   1545
   End
   Begin VB.TextBox txtText1 
      BackColor       =   &H00C0C0C0&
      DataField       =   "CI22NOMBRE"
      Height          =   330
      Index           =   1
      Left            =   90
      TabIndex        =   2
      TabStop         =   0   'False
      Tag             =   "Nombre Persona|Nombre de la Persona"
      Top             =   300
      Width           =   5895
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      DataField       =   "CI22FECNACIM"
      Height          =   330
      Index           =   0
      Left            =   7590
      TabIndex        =   4
      Tag             =   "Fecha Nacimiento|Fecha Nacimiento"
      Top             =   300
      Width           =   1635
      _Version        =   65537
      _ExtentX        =   2884
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1000/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      StartofWeek     =   2
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "N� de Historia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   6
      Left            =   6120
      TabIndex        =   23
      Top             =   60
      Width           =   1200
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Sexo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   8
      Left            =   9330
      TabIndex        =   7
      Top             =   60
      Width           =   435
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Nacimiento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   7590
      TabIndex        =   6
      Top             =   60
      Width           =   1545
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   90
      TabIndex        =   5
      Top             =   60
      Width           =   1245
   End
End
Attribute VB_Name = "frmAsignarCama"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim strEstado As String
Dim strCama As String
Dim strSQL As String
Dim strFecha_AD1600 As String
Dim strHora_AD1600 As String

Dim vntError As Variant
Private Sub Form_Activate()
    Dim Rs As rdoResultset
  Dim rdoConsulta As rdoQuery
  Dim strSQL As String

    strSQL = "SELECT * FROM AD1500,AD1400 WHERE " & _
                                    "AD1500.AD14CODESTCAMA=AD1400.AD14CODESTCAMA" & _
                                    " AND AD1500.AD01CODASISTENCI=? " & _
                                    " AND AD1500.AD07CODPROCESO=? " & _
                                    " AND AD1400.AD14INDOCU=-1"
            
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = objPipe.PipeGet("CodigoAsistencia")
            rdoConsulta(1) = objPipe.PipeGet("Proceso")
        'Set rs = rdoConsulta.OpenResultset(strSQL, rdOpenDynamic)
        Set Rs = rdoConsulta.OpenResultset(strSQL)
    
    Rs.Close
    Set Rs = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim rdoConsulta As rdoQuery
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, Nothing, Nothing, _
                                cwWithoutNothing)

  With objMultiInfo
    .strName = "Camas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "AD1501J" 'VISTA
    .strWhere = "(ESTADO_ASISTENCIA IS NULL OR ESTADO_ASISTENCIA=1)  AND (ESTADO_PERSONA IS NULL OR ESTADO_PERSONA=1)"
    Call .FormAddOrderField("AD15CODCAMA", cwAscending)
       
    .intAllowance = cwAllowReadOnly
    Call .FormCreateFilterWhere("AD1501J", "Filtro de Camas")
    'Call .FormAddFilterWhere("AD1501J", "AD02CODDPTO", "Departamento", cwString, "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200")
    Call .FormAddFilterWhere("AD1501J", "AD02CODDPTO", "Departamento", cwString, "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objSecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL" & _
             " AND AD32CODTIPODPTO = 3" & _
             " ORDER BY AD0200.AD02DESDPTO")
    Call .FormAddFilterWhere("AD1501J", "AD13CODTIPOCAMA", "Tipo Cama", cwString, "SELECT AD13CODTIPOCAMA,AD13DESTIPOCAMA FROM AD1300 ORDER BY AD13CODTIPCAMA")
    Call .FormAddFilterWhere("AD1501J", "AD14CODESTCAMA", "Estado Cama", cwString, "SELECT AD14CODESTCAMA,AD14DESESTCAMA FROM AD1400 ORDER BY AD14CODESTCAMA")
 
    Call .FormAddFilterOrder("AD1501J", "AD02CODDPTO", "Departamento")
    Call .FormAddFilterOrder("AD1501J", "AD13CODTIPOCAMA", "Tipo Cama")
    
  End With
 
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
     
    Call .GridAddColumn(objMultiInfo, "Cama", "CAMA", cwString, 4)
    Call .GridAddColumn(objMultiInfo, "CamaReal", "AD15CODCAMA", cwString, 4)
    Call .GridAddColumn(objMultiInfo, "C�digo Departamento", "AD02CODDPTO", cwString, 2)
    Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString, 22)
    Call .GridAddColumn(objMultiInfo, "C�digo Tipo de Cama", "AD13CODTIPOCAMA", cwString, 2)
    Call .GridAddColumn(objMultiInfo, "Tipo de Cama", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "C�digo Estado Cama", "AD14CODESTCAMA", cwString, 2)
    Call .GridAddColumn(objMultiInfo, "Estado Cama", "", cwString, 22)
    Call .GridAddColumn(objMultiInfo, "Sexo", "CI30DESSEXO", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Fecha Nacimiento", "CI22FECNACIM", cwDate)
    Call .GridAddColumn(objMultiInfo, "Paciente", "PACIENTE", cwString)
    Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric)
    'Call .GridAddColumn(objMultiInfo, "Dpto Responsable", "", cwNumeric)
    'Call .GridAddColumn(objMultiInfo, "Dpto", "", cwString)

    Call .FormCreateInfo(objMultiInfo)
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
    Call .FormChangeColor(objMultiInfo)

    grdDBGrid1(0).Columns(1).Visible = False
    grdDBGrid1(0).Columns(4).Visible = False
    grdDBGrid1(0).Columns(5).Visible = False
    grdDBGrid1(0).Columns(7).Visible = False
    grdDBGrid1(0).Columns(9).Visible = False
    grdDBGrid1(0).Columns(3).Width = 676
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "AD02CODDPTO", "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "AD02DESDPTO")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(7)), "AD13CODTIPOCAMA", "SELECT AD13CODTIPOCAMA,AD13DESTIPOCAMA FROM AD1300 WHERE AD13CODTIPOCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(7)), grdDBGrid1(0).Columns(8), "AD13DESTIPOCAMA")
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(9)), "AD14CODESTCAMA", "SELECT AD14CODESTCAMA,AD14DESESTCAMA FROM AD1400 WHERE AD14CODESTCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(9)), grdDBGrid1(0).Columns(10), "AD14DESESTCAMA")
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), "AD07CODPROCESO", "SELECT AD02CODDPTO FROM AD0500 WHERE ad05fecfinrespon is null and AD01CODASISTENCI=? and AD07CODPROCESO = ? ")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), grdDBGrid1(0).Columns(15), "AD02CODDPTO")
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(15)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(15)), grdDBGrid1(0).Columns(16), "AD02DESDPTO")
    
    '----------------------------------------------------------
    ' SACAR DATOS PACIENTE: IDENTIFICATIVOS Y CAMAS QUE OCUPA
    '----------------------------------------------------------
    ' Datos Identificativos del Paciente
    dtcDateCombo1(0).Date = objPipe.PipeGet("FechaNacimiento")
    txtText1(1) = objPipe.PipeGet("Nombre")
    txtText1(2) = objPipe.PipeGet("Historia")
    txtText1(5) = objPipe.PipeGet("Sexo")
    
       
   'Sacar N� cama y fechas de inicio
    txtText1(0) = objPipe.PipeGet("Cama")
    txtText1(3) = objPipe.PipeGet("CamaVisor")
    If txtText1(0) = "" Then
      dtcDateCombo1(1).Date = Format(objPipe.PipeGet("FechaHoraInicioTipoAsistencia"), "dd/mm/yyyy")
      txtHora(0) = Left(Format(objPipe.PipeGet("FechaHoraInicioTipoAsistencia"), "hh:mm"), 2)
      txtMinuto(0) = Right(Format(objPipe.PipeGet("FechaHoraInicioTipoAsistencia"), "hh:mm"), 2)
    Else
      Dim Rs As rdoResultset
      
      strSQL = "SELECT MAX(TO_CHAR(AD16FECCAMBIO,'DD/MM/YYYY HH24:MI')) ADFEC FROM AD1600"
      strSQL = strSQL & " WHERE AD15CODCAMA=?"
      
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = objPipe.PipeGet("Cama")
      
      Set Rs = rdoConsulta.OpenResultset(strSQL, rdOpenKeyset)
      dtcDateCombo1(1).Date = Format(Rs!ADFEC, "dd/mm/yyyy")
      txtHora(0) = Left(Format(Rs!ADFEC, "hh:mm"), 2)
      txtMinuto(0) = Right(Format(Rs!ADFEC, "hh:mm"), 2)
      Rs.Close
      Set Rs = Nothing
    End If
    
    ' El Boton de Asociar cama aparecer� Deshabilitado al entrar
    cmdCommand1(0).Enabled = False
    
    ' El Boton de Anular Asignaci�n cama aparecer� Deshabilitado si no Hay Cama para Paciente
    If txtText1(0).Text = "" Then
       cmdCommand1(3).Enabled = False
    Else
       cmdCommand1(3).Enabled = True
    End If
 
   
    txtText1(0).Locked = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
End Sub
Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    
    'para comprobar que se trata del control en cuestion
    If strFormName = "Camas" And strCtrlName = "grdDBGrid1(0).Proceso" Then
        aValues(2) = grdDBGrid1(0).Columns(15).Value
    End If
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
  Call objPipe.PipeSet("Cama", txtText1(0).Text)

End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub grdDBGrid1_BeforeDelete(intIndex As Integer, Cancel As Integer, DispPromptMsg As Integer)
  If intIndex = 1 Then
     DispPromptMsg = 0
  End If
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
  ' Si la cama esta Ocupada, no  se podr� seleccionar la fila(cama)
  ' Estado Libre = 1
  If intIndex = 0 Then
    If grdDBGrid1(0).Columns(9).Value = 1 Then
      grdDBGrid1(0).SelectTypeRow = ssSelectionTypeSingleSelect
      If txtText1(0).Text = "" Then
        cmdCommand1(0).Enabled = True
      End If
    Else
      grdDBGrid1(0).SelectTypeRow = ssSelectionTypeNone
      grdDBGrid1(0).Bookmark = 0
    End If
  Else
    cmdCommand1(3).Enabled = True
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  If intIndex = 0 Then
    Call objWinInfo.CtrlGotFocus
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
  If intIndex = 0 Then
    Call objWinInfo.GridDblClick
  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  If intIndex = 0 Then
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  If intIndex = 0 Then
    Call objWinInfo.CtrlDataChange
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  ' Si la Cama esta Libre, se visualizar� en Rojo, sino a Blanco
  If intIndex = 0 Then
    If grdDBGrid1(0).Columns(9).Value = "1" Then
      Call grdDBGrid1(0).Columns(3).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(4).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(6).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(8).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(10).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(11).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(12).CellStyleSet("Libre", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(13).CellStyleSet("Libre", grdDBGrid1(0).Row)
    Else
      Call grdDBGrid1(0).Columns(3).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(4).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(6).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(8).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(9).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(10).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(11).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(12).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
      Call grdDBGrid1(0).Columns(12).CellStyleSet("Ocupada", grdDBGrid1(0).Row)
    End If
  End If
End Sub

Private Sub optOption1_Click(intIndex As Integer)
  Select Case intIndex
    Case 0
      ' Cama Paciente
      ' Deshabilitar Combo Tipo ocupaci�n, Grid, Fecha y Hora
      grdDBGrid1(1).Enabled = False
      dtcDateCombo1(2).Enabled = False
      txtHora(1).Enabled = False
      txtMinuto(1).Enabled = False
      UpDownHora(1).Enabled = False
      UpDownMinuto(1).Enabled = False
      dtcDateCombo1(1).Enabled = True
      txtHora(0).Enabled = True
      txtMinuto(0).Enabled = True
      UpDownHora(0).Enabled = True
      UpDownMinuto(0).Enabled = True
      If txtText1(0).Text <> "" Then
        cmdCommand1(3).Enabled = True
      End If
    Case 1
      ' Otra Cama
      If txtText1(0).Text = "" Then
        ' Error, se quiere asignar Otra Cama sin Asignarle al Paciente
        Call objError.SetError(cwCodeMsg, "Se Debe asignar Primero Cama a Paciente", vntError)
        vntError = objError.Raise
        optOption1(0).Value = True
      Else
        ' Deshabiltar Boton Anular Asignaci�n
        cmdCommand1(3).Enabled = False
        grdDBGrid1(1).Enabled = True
        dtcDateCombo1(2).Enabled = True
        txtHora(1).Enabled = True
        txtMinuto(1).Enabled = True
        UpDownHora(1).Enabled = True
        UpDownMinuto(1).Enabled = True
        dtcDateCombo1(1).Enabled = False
        txtHora(0).Enabled = False
        txtMinuto(0).Enabled = False
        UpDownHora(0).Enabled = False
        UpDownMinuto(0).Enabled = False
      End If
  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Button
' -----------------------------------------------

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim strFecha As String
  Dim intI As Integer
  Dim vntFila As Variant
  Dim rdoConsulta As rdoQuery
  Dim strSQL As String

  Dim sCodAsistencia    As String
  Dim sCodProceso       As String
  Dim sCodCama          As String

  Dim sStmSql   As String
  Dim qrySelect As rdoQuery
  Dim rdoCaso   As rdoResultset
  Dim strCaso   As String
  Dim sHistoria As String
  Dim qryInsert As rdoQuery
  Dim sNumRegistro  As String
  Dim rdoConsul As rdoResultset
    
  Select Case intIndex
    Case 0
      ' ---------------
      ' Asignar Camas
      ' ---------------
      
      If grdDBGrid1(0).SelBookmarks.Count > 0 Then
        strCama = grdDBGrid1(0).Columns(4).CellValue(grdDBGrid1(0).SelBookmarks(grdDBGrid1(0).Row))
        vntFila = grdDBGrid1(0).Bookmark
        ' Comprobar que esa Cama no esta ya Ocupada (estado libre = 1)
        Dim Rs As rdoResultset
        
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
        
'        Set rs = _
'        objApp.rdoConnect.OpenResultset("SELECT AD14CODESTCAMA " & _
'                                        "FROM AD1500 WHERE " & _
'                                        "AD15CODCAMA = '" & strCama & "'" _
'                                        , rdOpenDynamic)
        strSQL = "SELECT AD14CODESTCAMA " & _
                                        "FROM AD1500 WHERE " & _
                                        "AD15CODCAMA = ?"
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = strCama
        Set Rs = rdoConsulta.OpenResultset(strSQL)
        If Not Rs.EOF Then
          If Rs!AD14CODESTCAMA = 1 Then
            If optOption1(0).Value = True Then
              ' Cama ocupada por Paciente ( Estado=2)
              'strEstado = "2" 'Estado Ocupado
              strEstado = "6" 'Estado Ocupado
              txtText1(0).Text = grdDBGrid1(0).Columns(3).Value
              cmdCommand1(3).Enabled = True
              strFecha = Format(dtcDateCombo1(1).Date, "DD/MM/YYYY") & " " & txtHora(0) & ":" & txtMinuto(0)
            End If
            
            ' Actualizar estado en Tabla de Camas (AD1500)
            
'            strSQL = "UPDATE AD1500 SET AD14CODESTCAMA = '"
'            strSQL = strSQL & strEstado & "',AD01CODASISTENCI=" & objPipe.PipeGet("CodigoAsistencia")
'            'IVM se pasa el proceso
'            strSQL = strSQL & ",AD07CODPROCESO=" & objPipe.PipeGet("Proceso")
'            strSQL = strSQL & " WHERE AD15CODCAMA = '"
'            strSQL = strSQL & strCama & "'"
            strSQL = "UPDATE AD1500 SET AD14CODESTCAMA = ?,AD01CODASISTENCI=? "
            strSQL = strSQL & ",AD07CODPROCESO=? "
            strSQL = strSQL & " WHERE AD15CODCAMA = ?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = strEstado
            rdoConsulta(1) = objPipe.PipeGet("CodigoAsistencia")
            rdoConsulta(2) = objPipe.PipeGet("Proceso")
            rdoConsulta(3) = strCama
            rdoConsulta.Execute
'            objApp.rdoConnect.Execute strSQL, 64
            'objApp.rdoConnect.Execute "Commit", 64
            
            ' Actualizar Tabla Situaci�n Camas (AD1600)
'            strSQL = "UPDATE AD1600 SET AD16FECFIN=TO_DATE('" & strFecha & "','DD/MM/YYYY HH24:MI')"
'            strSQL = strSQL & " WHERE AD15CODCAMA = '"
'            strSQL = strSQL & strCama & "' AND AD16FECFIN IS NULL"
            strSQL = "UPDATE AD1600 SET AD16FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI')"
            strSQL = strSQL & " WHERE AD15CODCAMA = ? AND AD16FECFIN IS NULL"
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = strFecha
            rdoConsulta(1) = strCama
            rdoConsulta.Execute
            'objApp.rdoConnect.Execute sStmSql, 64
            'objApp.rdoConnect.Execute "Commit", 64
            
            
            
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
  
'            strSQL = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA,"
'            strSQL = strSQL & "AD01CODASISTENCI,AD07CODPROCESO) VALUES ('" & strCama
'            strSQL = strSQL & "',TO_DATE('" & strFecha & "','DD/MM/YYYY HH24:MI'),"
'            strSQL = strSQL & "'" & strEstado & "'," & objPipe.PipeGet("CodigoAsistencia")
'            'IVM se mete el proceso
'            strSQL = strSQL & "," & objPipe.PipeGet("Proceso") & ")"

            strSQL = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA,"
            strSQL = strSQL & "AD01CODASISTENCI,AD07CODPROCESO) VALUES (?"
            strSQL = strSQL & ",TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
            strSQL = strSQL & "?,?"
            strSQL = strSQL & ",?)"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = strCama
            rdoConsulta(1) = strFecha
            rdoConsulta(2) = strEstado
            rdoConsulta(3) = objPipe.PipeGet("CodigoAsistencia")
            rdoConsulta(4) = objPipe.PipeGet("Proceso")
            rdoConsulta.Execute
            ' Deshabilitar Bot�n Asignar
            cmdCommand1(0).Enabled = False
            
            objWinInfo.DataRefresh
            grdDBGrid1(0).Bookmark = vntFila
                 
          Else
            ' La Cama no est� Libre
            Call objError.SetError(cwCodeMsg, "La Cama Seleccionada no est� Libre", vntError)
            vntError = objError.Raise
          End If
        End If
        Rs.Close
        Set Rs = Nothing
            
            
      Else
        ' No se ha seleccionado ninguna Cama
        Call objError.SetError(cwCodeMsg, "Seleccione una Cama", vntError)
        vntError = objError.Raise
      End If
         
    Case 1
      '-------
      ' Salir
      '-------
      Call objPipe.PipeSet("Cama", txtText1(0).Text)
      Unload Me
    Case 3
      '-------------------------
      ' Anular Asignaci�n Cama
      '-------------------------
      ' Se comprobar� que se le haya asignado alguna Cama al Paciente para poder anularla
      ' Si anula la asignaci�n de cama del paciente, se anular�n tambi�n las otras camas
      ' que se hayan asignado para ese paciente
      If txtText1(0).Text <> "" Then
        strEstado = "1" ' Estado Libre
        If optOption1(0).Value = True Then
        ' Cama ocupada por Paciente
          strCama = txtText1(0).Text
          
          Call Actualizar_Camas
                                
          txtText1(0).Text = ""
          objWinInfo.DataRefresh
        ElseIf optOption1(1).Value = True Then
          ' Cama Ocupada por otro motivo, se actualizar�n a estado libre en la tabla Camas AD1500 s�lo las seleccionadas
          ' y se borrar�n de la tabla AD1600
          ' grdDBGrid1(1).Redraw = False
          For intI = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
            strCama = grdDBGrid1(1).Columns(0).CellValue(grdDBGrid1(1).SelBookmarks(grdDBGrid1(1).Row))
            Call Actualizar_Camas
          Next intI
          grdDBGrid1(1).DeleteSelected
                                               
        End If
        ' Deshabilitar Bot�n Anular Asignaci�n
        cmdCommand1(3).Enabled = False
        objWinInfo.DataRefresh
      Else
        ' No hay ninguna cama asignada al Paciente
        Call objError.SetError(cwCodeMsg, "No hay ninguna cama asociada al Paciente", vntError)
        vntError = objError.Raise
      End If
    Case 4
      '--------------
      ' Filtro Camas
      '--------------
      Call objWinInfo.FormShowFilter
          
  End Select
End Sub

' -------------------------------------------------------------------------
' NUEVA SECCI�N
'
' SUBRUTINAS :
'----------------------------------------------------------------------------

'--------------------------------------------------------------------------
'Actualizar Estados y Situaci�n de Camas al anular Asignaci�n
' --------------------------------------------------------------------------
Private Sub Actualizar_Camas()

  Dim rdoConsulta As rdoQuery
  
  ' Actualizar tabla Camas AD1500
    strSQL = "UPDATE AD1500 SET AD14CODESTCAMA = "
    strSQL = strSQL & "?, AD01CODASISTENCI=NULL,AD07CODPROCESO=NULL"
    strSQL = strSQL & " WHERE AD15CODCAMA =? "
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = strEstado
    rdoConsulta(1) = strCama
    rdoConsulta.Execute
    
    strSQL = "DELETE AD1600"
    strSQL = strSQL & " WHERE AD15CODCAMA = "
    strSQL = strSQL & "? AND AD16FECCAMBIO = "
    strSQL = strSQL & "(SELECT MAX(AD16FECCAMBIO) FROM AD1600 WHERE AD15CODCAMA = "
    strSQL = strSQL & "?)"
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = strCama
    rdoConsulta(1) = strCama
    rdoConsulta.Execute
End Sub
Private Sub txtHora_LostFocus(Index As Integer)
  txtHora(Index).Text = Val(txtHora(Index).Text)
End Sub

Private Sub txtMinuto_LostFocus(Index As Integer)
  txtMinuto(Index) = Val(txtMinuto(Index))
End Sub

Private Sub txtText1_Change(Index As Integer)
  If Index = 0 Then
    If txtText1(0).Text <> "" Then
      txtHora(0).Enabled = False
      txtMinuto(0).Enabled = False
      UpDownHora(0).Enabled = False
      UpDownMinuto(0).Enabled = False
      dtcDateCombo1(1).Enabled = False
    Else
      txtHora(0).Enabled = True
      txtMinuto(0).Enabled = True
      UpDownHora(0).Enabled = True
      UpDownMinuto(0).Enabled = True
      dtcDateCombo1(1).Enabled = True
    End If
  End If
End Sub

Private Sub UpDownHora_DownClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownHora_UpClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownMinuto_DownClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub

Private Sub UpDownMinuto_UpClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub
