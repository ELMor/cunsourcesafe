VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmCambioTipoAsist 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tipo de Asistencia"
   ClientHeight    =   1230
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1230
   ScaleWidth      =   5415
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   555
      Left            =   540
      TabIndex        =   4
      Top             =   60
      Width           =   3495
      Begin MSMask.MaskEdBox mskHora 
         Height          =   315
         Left            =   2220
         TabIndex        =   5
         Top             =   120
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   556
         _Version        =   327681
         BackColor       =   12632256
         MaxLength       =   5
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecha 
         Height          =   315
         Left            =   600
         TabIndex        =   6
         Top             =   120
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Inicio:"
         Height          =   255
         Index           =   2
         Left            =   60
         TabIndex        =   7
         Top             =   180
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4260
      TabIndex        =   2
      Top             =   660
      Width           =   1035
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   4260
      TabIndex        =   1
      Top             =   120
      Width           =   1035
   End
   Begin SSDataWidgets_B.SSDBCombo cboTipoAsist 
      Height          =   315
      Left            =   1140
      TabIndex        =   0
      Top             =   720
      Width           =   2955
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5212
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo Asist.:"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   3
      Top             =   780
      Width           =   915
   End
End
Attribute VB_Name = "frmCambioTipoAsist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strAhora$, strTipoAsistOld$

Private Sub cboTipoAsist_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdAceptar_Click()
    Dim msg$
    
    If cboTipoAsist.Columns(0).Text = strTipoAsistOld Then
        msg = "No se ha cambiado el tipo de asistencia."
        MsgBox msg, vbInformation, Me.Caption
        Exit Sub
    End If
    
    If fComprobaciones Then
        msg = "�Desea Ud. se�alar el tipo de asistencia como " & cboTipoAsist.Text & "?"
        If MsgBox(msg, vbQuestion + vbOKCancel, Me.Caption) = vbOK Then
            Call objPipe.PipeSet("AD3002_AD12CODTIPOASIST", cboTipoAsist.Columns(0).Text)
            Unload Me
        End If
    End If
End Sub

Private Sub cmdCancelar_Click()
    If MsgBox("�Desea Ud. salir SIN GUARDAR los cambios?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
        Unload Me
    End If
End Sub

Private Sub dcboFecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub Form_Load()
    Call pObtenerDatosOld
    Call pFormatearControles
    Call pCargarTiposAsist
End Sub

Private Sub mskHora_GotFocus()
    mskHora.SelStart = 0
    mskHora.SelLength = Len(mskHora.Text)
End Sub

Private Sub mskHora_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub mskHora_LostFocus()
    If Not mskHora.Text = "__:__" Then
        mskHora.Text = objGen.ReplaceStr(mskHora.Text, "_", "0", 0)
        If Not IsDate(mskHora.Text) Then mskHora.Text = "__:__"
    End If
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    
    fComprobaciones = True
End Function

Private Sub pFormatearControles()
    cboTipoAsist.BackColor = objApp.objUserColor.lngMandatory
    dcboFecha.BackColor = objApp.objUserColor.lngReadOnly
    mskHora.BackColor = objApp.objUserColor.lngReadOnly

    strAhora = strFechaHora_Sistema
    dcboFecha.Date = Format(strAhora, "dd/mm/yyyy")
    dcboFecha.MinDate = dcboFecha.Date
    dcboFecha.MaxDate = DateAdd("d", 1, dcboFecha.Date)
    mskHora.Text = Format(strAhora, "hh:mm")
End Sub

Private Sub pCargarTiposAsist()
    Dim SQL$, rs As rdoResultset
    Dim I%, strTipoAsistPropuesto$
    
    SQL = "SELECT AD12CODTIPOASIST, AD12DESTIPOASIST"
    SQL = SQL & " FROM AD1200"
    SQL = SQL & " WHERE AD12FECFIN IS NULL"
    SQL = SQL & " ORDER BY AD12DESTIPOASIST"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipoAsist.AddItem rs!AD12CODTIPOASIST & Chr$(9) & rs!AD12DESTIPOASIST
        rs.MoveNext
    Loop
    rs.Close
    
    If strTipoAsistOld = constASIST_HOSP Then
        strTipoAsistPropuesto = constASIST_AMBUL 'nuevo tipo propuesto
    Else
        strTipoAsistPropuesto = constASIST_HOSP 'nuevo tipo propuesto
    End If
    For I = 1 To cboTipoAsist.Rows
        If I = 1 Then cboTipoAsist.MoveFirst Else cboTipoAsist.MoveNext
        If cboTipoAsist.Columns(0).Text = strTipoAsistPropuesto Then
            cboTipoAsist.Text = cboTipoAsist.Columns(1).Text
            Exit For
        End If
    Next I
End Sub

Private Sub pObtenerDatosOld()
    If objPipe.PipeExist("AD3002_AD12CODTIPOASIST") Then
        strTipoAsistOld = objPipe.PipeGet("AD3002_AD12CODTIPOASIST")
        Call objPipe.PipeRemove("AD3002_AD12CODTIPOASIST")
    End If
End Sub

