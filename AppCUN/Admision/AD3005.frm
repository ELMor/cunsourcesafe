VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmCamasAsist 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Camas de la Asistencia"
   ClientHeight    =   4470
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4470
   ScaleWidth      =   10575
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Ver"
      ForeColor       =   &H00FF0000&
      Height          =   915
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   3075
      Begin VB.OptionButton optVer 
         Caption         =   "S�lo ocupaciones (O)"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   4
         Top             =   600
         Width           =   1995
      End
      Begin VB.OptionButton optVer 
         Caption         =   "Ocupaciones (O) y Reservas (R)"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Value           =   -1  'True
         Width           =   2715
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   9480
      TabIndex        =   1
      Top             =   540
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBGrid grdCamas 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   1020
      Width           =   10365
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18283
      _ExtentY        =   5953
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCamasAsist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodAsist$

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    Call pFormatearGrid
    If objPipe.PipeExist("AD3005_AD01CODASISTENCI") Then
        strCodAsist = objPipe.PipeGet("AD3005_AD01CODASISTENCI")
        Call objPipe.PipeRemove("AD3005_AD01CODASISTENCI")
        Call pCargarCamas
    End If
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarCamas()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strEstado$
    
    grdCamas.RemoveAll
    
    SQL = "SELECT GCFN06(AD15CODCAMA) AD15CODCAMA, AD1600.AD07CODPROCESO,"
    SQL = SQL & " NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) DR, AD02DESDPTO,"
    SQL = SQL & " AD16FECCAMBIO, AD16FECFIN,"
    SQL = SQL & " AD14CODESTCAMA"
    SQL = SQL & " FROM SG0200, AD0200, AD0500, AD1600"
    SQL = SQL & " WHERE AD1600.AD01CODASISTENCI = ?"
    If optVer(0).Value = True Then
        SQL = SQL & " AND AD14CODESTCAMA IN ('" & constCAMA_OCUPADA & "','" & constCAMA_RESERVADA & "')"
    ElseIf optVer(1).Value = True Then
        SQL = SQL & " AND AD14CODESTCAMA = '" & constCAMA_OCUPADA & "'"
    End If
    SQL = SQL & " AND AD0500.AD01CODASISTENCI = AD1600.AD01CODASISTENCI"
    SQL = SQL & " AND AD0500.AD07CODPROCESO = AD1600.AD07CODPROCESO"
    SQL = SQL & " AND AD05FECFINRESPON IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0500.SG02COD"
    SQL = SQL & " AND AD0200.AD02CODDPTO = AD0500.AD02CODDPTO"
    SQL = SQL & " ORDER BY AD16FECCAMBIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAsist
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        Select Case rs!AD14CODESTCAMA
        Case constCAMA_OCUPADA: strEstado = "O"
        Case constCAMA_RESERVADA: strEstado = "R"
        End Select
        grdCamas.AddItem rs!AD15CODCAMA & Chr$(9) _
                    & strEstado & Chr$(9) _
                    & Format(rs!AD16FECCAMBIO, "dd/mm/yyyy hh:mm") & Chr$(9) _
                    & Format(rs!AD16FECFIN, "dd/mm/yyyy hh:mm") & Chr$(9) _
                    & rs!AD07CODPROCESO & Chr$(9) _
                    & rs!DR & Chr$(9) _
                    & rs!AD02DESDPTO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pFormatearGrid()
    With grdCamas
        .Columns(0).Caption = "Cama"
        .Columns(0).Width = 800
        .Columns(0).Alignment = ssCaptionAlignmentRight
        .Columns(1).Caption = "Estado"
        .Columns(1).Width = 800
        .Columns(1).Alignment = ssCaptionAlignmentCenter
        .Columns(2).Caption = "Fecha Inicio"
        .Columns(2).Width = 1450
        .Columns(3).Caption = "Fecha Fin"
        .Columns(3).Width = 1450
        .Columns(4).Caption = "Proceso"
        .Columns(4).Width = 1100
        .Columns(5).Caption = "Dr. Responsable"
        .Columns(5).Width = 2500
        .Columns(6).Caption = "Dpto. Responsable"
        .Columns(6).Width = 2000
        
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub optVer_Click(Index As Integer)
    Call pCargarCamas
End Sub
