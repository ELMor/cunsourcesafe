VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmNuevosMovimientos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION.Movimientos del Sobre"
   ClientHeight    =   6705
   ClientLeft      =   285
   ClientTop       =   1020
   ClientWidth     =   11190
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6705
   ScaleWidth      =   11190
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Personas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2385
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   450
      Width           =   11025
      Begin TabDlg.SSTab tabTab1 
         Height          =   1875
         HelpContextID   =   90001
         Index           =   0
         Left            =   150
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   10725
         _ExtentX        =   18918
         _ExtentY        =   3307
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Det&alle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "IdPersona1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "&Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   9360
            TabIndex        =   19
            Tag             =   "N� Historia|N� Historia"
            Top             =   210
            Visible         =   0   'False
            Width           =   810
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1620
            Index           =   0
            Left            =   -74880
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   120
            Width           =   10125
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17859
            _ExtentY        =   2857
            _StockProps     =   79
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1335
            Left            =   240
            TabIndex        =   1
            Top             =   240
            Width           =   9975
            _ExtentX        =   17595
            _ExtentY        =   2355
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Movimientos del Sobre"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Index           =   1
      Left            =   60
      TabIndex        =   9
      Top             =   2880
      Width           =   11025
      Begin TabDlg.SSTab tabTab1 
         Height          =   2730
         Index           =   1
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   330
         Width           =   10785
         _ExtentX        =   19024
         _ExtentY        =   4815
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AD0604.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(7)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(6)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(9)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cboDBCombo1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboDBCombo1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboDBCombo1(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(12)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtMinuto(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtHora(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   5580
            MaxLength       =   2
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   1440
            Width           =   420
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   3
            Index           =   0
            Left            =   6270
            MaxLength       =   2
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   1440
            Width           =   420
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   12
            Left            =   1260
            MaxLength       =   50
            TabIndex        =   18
            Tag             =   "Nombre Doctor|Nombre Doctor"
            Top             =   2160
            Width           =   6165
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   315
            HelpContextID   =   30101
            Index           =   6
            Left            =   9120
            TabIndex        =   7
            Tag             =   "N� Historia|N� Historia"
            Top             =   120
            Visible         =   0   'False
            Width           =   885
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3750
            Index           =   1
            Left            =   -74880
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   120
            Width           =   10155
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17912
            _ExtentY        =   6615
            _StockProps     =   79
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "SG02COD"
            Height          =   330
            Index           =   5
            Left            =   120
            TabIndex        =   5
            Tag             =   "C�digo Doctor|C�digo Doctor"
            Top             =   2160
            Width           =   975
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Primer Apellido"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "Segundo Apellido"
            Columns(2).Name =   "Apellido 1"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "Nombre"
            Columns(3).Name =   "Apellido 2"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "Fecha Fin"
            Columns(4).Name =   "Fecha Fin"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   1720
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AR07FECMVTO"
            Height          =   330
            Index           =   0
            Left            =   3480
            TabIndex        =   2
            Tag             =   "Fecha Alta|Fecha Alta"
            Top             =   1440
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            ForeColor       =   0
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AR00CODTIPOLOGIA"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   3
            Tag             =   "Parte del Sobre|Parte del Sobre"
            Top             =   600
            Width           =   2880
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5080
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   7080
            TabIndex        =   4
            Tag             =   "Departamento Destino|Departamento Destino"
            Top             =   1380
            Width           =   2655
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   4683
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AR04FECINICIO"
            Height          =   330
            Index           =   1
            Left            =   120
            TabIndex        =   23
            Tag             =   "Fecha Alta|Fecha Alta"
            Top             =   1440
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            ForeColor       =   0
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio Tipolog�a"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   24
            Top             =   1200
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Alta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   5580
            TabIndex        =   22
            Top             =   1200
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Doctor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   90
            TabIndex        =   16
            Top             =   1950
            Width           =   585
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   7080
            TabIndex        =   14
            Top             =   1170
            Width           =   2025
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipolog�a"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   90
            TabIndex        =   13
            Top             =   330
            Width           =   825
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha del Movimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   3480
            TabIndex        =   11
            Top             =   1200
            Width           =   1875
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   6285
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   741
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmNuevosMovimientos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim strHora As String

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
 
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    .strName = "Personas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strTable = "CI2200"
    .strWhere = " CI2200.AD34CODESTADO = 1 "
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)


    Call .FormCreateFilterWhere("CI2200", "Personas")
    Call .FormAddFilterWhere("CI2200", "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22NOMBRE", "Nombre", cwString)
   '
    Call .FormAddFilterOrder("CI2200", "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder("CI2200", "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder("CI2200", "CI22NOMBRE", "Nombre")
  End With
  
  With objDetailInfo
    .strName = "Movimientos del Sobre"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "AR0700"
    If Not fblnArchivo Then
        .intAllowance = cwAllowAdd
    End If
    Call .FormAddOrderField("CI22NUMHISTORIA", cwAscending)
    Call .FormAddOrderField("AR07FECMVTO", cwDescending)

    Call .FormAddRelation("CI22NUMHISTORIA", txtText1(1))


    Call .FormCreateFilterWhere("AR0700", "Movimientos del Sobre")
    Call .FormAddFilterWhere("AR0700", "AR07FECMVTO", "Fecha del Movimiento", cwDate)
    Call .FormAddFilterWhere("AR0700", "AR00CODTIPOLOGIA", "Tipolog�a", cwNumeric)
    Call .FormAddFilterWhere("AR0700", "CI22NUMHISTORIA", "N�mero de Historia", cwNumeric)
    Call .FormAddFilterWhere("AR0700", "SG02COD", "Dra./Dr. responsable del movimiento", cwNumeric)

    Call .FormAddFilterOrder("AR0700", "AR07FECMVTO", "Fecha del Movimiento")
    Call .FormAddFilterOrder("AR0700", "AR00CODTIPOLOGIA", "Tipolog�a")
    Call .FormAddFilterOrder("AR0700", "CI22NUMHISTORIA", "N�mero de Historia")
    Call .FormAddFilterOrder("AR0700", "SG02COD", "Dra./Dr. responsable del movimiento")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
     
 
'    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    'Incluir campos obligatorios
    .CtrlGetInfo(cboDBCombo1(0)).blnMandatory = True
    .CtrlGetInfo(cboDBCombo1(2)).blnMandatory = True
    
    .CtrlGetInfo(txtHora(0)).blnForeign = True
    .CtrlGetInfo(txtMinuto(0)).blnForeign = True
'    .CtrlGetInfo(txtText1(2)).blnReadOnly = True
   .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AR00CODTIPOLOGIA,AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA"
    .CtrlGetInfo(cboDBCombo1(2)).strSQL = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE (AD02FECFIN IS NULL) " & _
                                          " OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)) ORDER BY AD02DESDPTO"

    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(5)), "SG02COD", "SELECT SG02COD,SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 AS DOCTOR FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(5)), txtText1(12), "DOCTOR")
  
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(3)), "AD07CODPROCESO", "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700 WHERE AD0700.AD34CODESTADO = 1 AND AD07CODPROCESO = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(3)), txtText1(0), "AD07DESNOMBPROCE")
    
    .CtrlGetInfo(txtText1(1)).blnInGrid = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
 'Invisible bot�n de B�squeda
  IdPersona1.blnSearchButton = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim strNombre As String
  
  Select Case strCtrl
    Case "IdPersona1"                  'nuevo
      IdPersona1.SearchPersona         'nuevo
'    Case "txtText1(15)"
'      If strFormName = "Movimientos del Sobre" Then
'        Dim objField As clsCWFieldSearch
'
'        Set objSearch = New clsCWSearch
'        With objSearch
'          .strTable = "SG0200"
'          .strOrder = "ORDER BY AD30CODCATEGORIA ASC,SG02APE1 ASC,SG02APE2 ASC,SG02NOM ASC"
'          .strWhere = "WHERE SG02FECDES IS NULL"
'
'          Set objField = .AddField("SG02COD")
'          objField.strSmallDesc = "C�digo"
'
'          Set objField = .AddField("SG02APE1")
'          objField.strSmallDesc = "Primer Apellido"
'
'          Set objField = .AddField("SG02APE2")
'          objField.strSmallDesc = "Segundo Apellido"
'
'          Set objField = .AddField("SG02NOM")
'          objField.strSmallDesc = "Nombre"
'
'          Set objField = .AddField("AD30CODCATEGORIA")
'          objField.strSmallDesc = "Categoria"
'
'          If .Search Then
'            Call objWinInfo.CtrlSet(txtText1(15), .cllValues("SG02COD"))
'          End If
'        End With
'        Set objSearch = Nothing
'      End If
    Case "cboDBCombo1(0)"
      'Call frmParteSobreHistoria.Show(vbModal)
      Call objSecurity.LaunchProcess("AD0304")
    '  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
'    Case "cboDBCombo1(1)"
'      Call objSecurity.LaunchProcess("AD0203")
'      'Call frmDepartamentos.Show(vbModal)
'      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(2)))
    Case "cboDBCombo1(2)"
      Call objSecurity.LaunchProcess("AD0204")
      'Call frmDepartamentosPersonal.Show(vbModal)
 '     Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
    Case "cboDBCombo1(5)"
      Call objSecurity.LaunchProcess("AD0201")
      'Call frmPersonal.Show(vbModal)
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(6)))
  End Select
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If strFormName = "Movimientos del Sobre" Then
    If intNewStatus = cwModeSingleAddKey Then
    Else
      strHora = txtHora(0) & ":" & txtMinuto(0)
    End If
  End If
  
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
'  objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700 WHERE AD0700.AD34CODESTADO = 1 AND CI21CODPERSONA=" & IdPersona1.Text
'  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
  
'  objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD01CODASISTENCI FROM AD0100 WHERE CI21CODPERSONA=" & IdPersona1.Text & " AND AD0100.AD34CODESTADO = 1 "
'  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
  
  Call objWinInfo.CtrlSet(txtText1(6), IdPersona1.Text) 'nuevo
'  Call objWinInfo.CtrlSet(dtcDateCombo1(0), strFecha_Sistema)
'  strHora = strHora_Sistema
''  Call objWinInfo.CtrlSet(txtText1(15), objSecurity.strUser) 'nuevo
'
'  txtHora(0).Text = Left(Format(strHora, "hh:mm"), 2)
'  txtMinuto(0).Text = Right(Format(strHora, "hh:mm"), 2)
'''  UpDownHora(0).Value = txtHora(0).Text
'''  UpDownMinuto(0).Value = txtMinuto(0).Text
'  txtHora(0).Enabled = True
'  txtMinuto(0).Enabled = True
'  cboDBCombo1(0).Enabled = True
''  UpDownHora(0).Enabled = True
''  UpDownMinuto(0).Enabled = True
  Call objWinInfo.FormChangeStatus(cwModeSingleAddRest)
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
If strFormName = "Movimientos del Sobre" Then
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      If objWinInfo.objWinActiveForm.rdoCursor.RowCount = 0 Then Exit Sub
      txtHora(0).Text = Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AR07FECMVTO, "HH:MM"), 2)
      txtMinuto(0).Text = Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AR07FECMVTO, "HH:MM"), 2)
      txtHora(0).Enabled = False
      txtMinuto(0).Enabled = False
'      UpDownHora(0).Enabled = False
'      UpDownMinuto(0).Enabled = False
      strHora = txtHora(0).Text & ":" & txtMinuto(0).Text
      
'      If cboDBCombo1(3).Value <> "" Then
'        'Asistencias del Proceso
'        objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD0800.AD01CODASISTENCI FROM AD0800,AD0100 " & _
'          " WHERE AD0800.AD07CODPROCESO=" & cboDBCombo1(3).Value & _
'          " AND AD0100.AD01CODASISTENCI=AD0800.AD01CODASISTENCI" & _
'          " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'          " ORDER BY AD0800.AD01CODASISTENCI"
'        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'        If cboDBCombo1(4).Value <> "" Then
'          'Procesos de Asistencias
'          objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD0700.AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700,AD0800" & _
'            " WHERE AD0800.AD01CODASISTENCI=" & cboDBCombo1(4).Value & _
'            " AND AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO" & _
'            " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'            " ORDER BY AD07DESNOMBPROCE"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        Else
'          'Procesos del paciente
'          objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700" & _
'            " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'            " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & _
'            " ORDER BY AD07DESNOMBPROCE"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        End If
'      Else
'        'Asistencias del paciente
'        objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD01CODASISTENCI FROM AD0100 " & _
'          " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'          " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 " & _
'          " ORDER BY AD01CODASISTENCI"
'        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'        If cboDBCombo1(4).Value <> "" Then
'          objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD0700.AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700,AD0800" & _
'            " WHERE AD0800.AD01CODASISTENCI=" & cboDBCombo1(4).Value & _
'            " AND AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO" & _
'            " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'            " ORDER BY AD07DESNOMBPROCE"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        Else
'          'Procesos del paciente
'          objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700 " & _
'            " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'            " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & _
'            " ORDER BY AD07DESNOMBPROCE"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        End If
'      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim rd As rdoResultset
  Dim qy As rdoQuery
  Dim qyUP As rdoQuery
  Dim STR$
  If strFormName = "Movimientos del Sobre" Then
      STR = "SELECT COUNT(*) FROM AR0700 WHERE CI22NUMHISTORIA=? AND " & _
      "AR00CODTIPOLOGIA=? AND AR04FECINICIO=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
      "AR07FECFINMVTO IS NULL AND AR07FECMVTO <> TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
      Set qy = objApp.rdoConnect.CreateQuery("", STR)
      qy(0) = IdPersona1.Historia
      qy(1) = cboDBCombo1(0).Columns(0).Value
      qy(2) = Format(dtcDateCombo1(1).Date, "DD/MM/YYYY HH:MM:SS")
      qy(3) = Format(dtcDateCombo1(0).Date & " " & txtHora(0) & ":" & txtMinuto(0) & ":00", "DD/MM/YYYY HH:MM:SS")
      Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rd.rdoColumns(0) > 0 Then
          STR = "UPDATE AR0700 SET AR07FECFINMVTO = SYSDATE WHERE CI22NUMHISTORIA=? AND " & _
          "AR00CODTIPOLOGIA = ? AND AR04FECINICIO = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
          "AR07FECFINMVTO IS NULL AND AR07FECMVTO <> TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
          Set qyUP = objApp.rdoConnect.CreateQuery("", STR)
          qyUP(0) = IdPersona1.Historia
          qyUP(1) = cboDBCombo1(0).Columns(0).Value
          qyUP(2) = Format(dtcDateCombo1(1).Date, "DD/MM/YYYY HH:MM:SS")
          qyUP(3) = dtcDateCombo1(0).Date & " " & txtHora(0) & ":" & txtMinuto(0) & ":00"
          qyUP.Execute
          qyUP.Close
        End If
        rd.Close
        qy.Close
  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntA As Variant
  Dim rs As rdoResultset
  
  If cboDBCombo1(0).Columns(2).Value <> "" Then
    Call objError.SetError(cwCodeMsg, "Esta Parte de Sobre est� dado de baja. Elija otra.", vntA)
    vntA = objError.Raise
    blnCancel = True
  End If
  If cboDBCombo1(2).Columns(2).Value <> "" Then
    Call objError.SetError(cwCodeMsg, "Este Departamento destino est� dado de baja. Elija otro.", vntA)
    vntA = objError.Raise
    blnCancel = True
  End If
  If cboDBCombo1(5).Columns(4).Value <> "" And cboDBCombo1(5).Text <> "" Then
    Call objError.SetError(cwCodeMsg, "Este Doctor est� dado de baja. Elija otro.", vntA)
    vntA = objError.Raise
    blnCancel = True
  End If

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  If strFormName = "Movimientos del Sobre" Then
    If Not blnError Then
      If objWinInfo.intWinStatus = cwModeSingleAddRest Or objWinInfo.intWinStatus = cwModeSingleEdit Then
        strHora = txtHora(0).Text & ":" & txtMinuto(0).Text
        objWinInfo.objWinActiveForm.rdoCursor!AR07FECMVTO = _
        objWinInfo.objWinActiveForm.rdoCursor!AR07FECMVTO & " " & strHora & ":00"
      End If
    End If
 End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim STR$
Dim qy As rdoQuery
Dim i%
Dim dptoAnterior As String
  If btnButton.Index = 16 Then
'  txtText1(1).Text = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    Call IdPersona1.Buscar
    DoEvents
    If txtText1(1).Text = "" Then
        Call objWinInfo.CtrlSet(txtText1(1), IdPersona1.Historia)
        objMasterInfo.strWhere = "CI2200.CI21CODPERSONA = " & IdPersona1.Object
        objWinInfo.DataRefresh
    End If
  Else
    If btnButton.Index = 30 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Else
'        dptoAnterior = txtText1(2)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        cboDBCombo1(0).Enabled = True
'        txtText1(2) = dptoAnterior
    End If
  End If
 Screen.MousePointer = vbDefault
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

  Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  dtcDateCombo1(0).Refresh
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Dim strNombre As String
  Call objWinInfo.CtrlLostFocus
  Select Case intIndex
    Case 2
      If cboDBCombo1(2).Text = "" Then
        Call objWinInfo.CtrlSet(cboDBCombo1(5), "")
      End If
'    Case 3
'      If cboDBCombo1(3).Text = "" Then
'        objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD01CODASISTENCI FROM AD0100" & _
'          " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'          " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 "
'        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'        If cboDBCombo1(4).Value <> "" Then
'          'Procesos de Asistencias
'          objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD0700.AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700,AD0800" & _
'            " WHERE AD0800.AD01CODASISTENCI=" & cboDBCombo1(4).Value & _
'            " AND AD0800.AD07CODPROCESO = AD0700.AD07CODPROCESO" & _
'            " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'            " ORDER BY AD07DESNOMBPROCE"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        Else
'          'Procesos del paciente
'          objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700" & _
'            " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'            " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & _
'            " ORDER BY AD07DESNOMBPROCE"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        End If
'      End If
'    Case 4
'      If cboDBCombo1(4).Text = "" Then
'        objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700" & _
'          " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'          " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & _
'          " ORDER BY AD07DESNOMBPROCE"
'        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'        If cboDBCombo1(3).Value <> "" Then
'          'Asistencias del Proceso
'          objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD0800.AD01CODASISTENCI FROM AD0800,AD0100 " & _
'            " WHERE AD0800.AD07CODPROCESO=" & cboDBCombo1(3).Value & _
'            " AND AD0100.AD01CODASISTENCI=AD0800.AD01CODASISTENCI" & _
'            " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'            " ORDER BY AD0800.AD01CODASISTENCI"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'        Else
'          objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD01CODASISTENCI FROM AD0100 " & _
'            " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'            " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 " & _
'            " ORDER BY AD0100.AD01CODASISTENCI"
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'        End If
'      End If
  End Select
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
Dim STR$
Dim rd As rdoResultset
Dim qy As rdoQuery
Dim RDU As rdoResultset
Dim QYU As rdoQuery
Dim RDAR As rdoResultset
Dim QYAR As rdoQuery
Dim QYin As rdoQuery
Dim RDal As rdoResultset
Dim QYal As rdoQuery
Dim fechaInicio As String
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
'  If cboDBCombo1(0).Columns(0).Value <> 3 Then
        
        cboDBCombo1(2).Columns(0).Value = Null
       ' cboDBCombo1(1).Columns(0).Value = Null
        cboDBCombo1(2).Text = ""
        STR = "SELECT AD02CODDPTO FROM AR0700 WHERE AR00CODTIPOLOGIA =? " & _
        "AND CI22NUMHISTORIA=? ORDER BY AR07FECMVTO DESC"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
            qy(0) = cboDBCombo1(0).Columns(0).Value
            qy(1) = IdPersona1.Historia
        Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rd.RowCount <> 0 Then
            rd.MoveFirst 'LA MAYOR FECHA
            STR = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=?"
            Set QYU = objApp.rdoConnect.CreateQuery("", STR)
            QYU(0) = rd!AD02CODDPTO
            Set RDU = QYU.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'            txtText1(2) = RDU!AD02DESDPTO
            RDU.Close
            QYU.Close
        Else
'         txtText1(2) = ""
        End If
        'Cargar los dtcdatecombo
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), strFecha_Sistema)
        strHora = strHora_Sistema
        txtHora(0).Text = Left(Format(strHora, "hh:mm"), 2)
        txtMinuto(0).Text = Right(Format(strHora, "hh:mm"), 2)
        txtHora(0).Enabled = True
        txtMinuto(0).Enabled = True
        rd.Close
        qy.Close
        STR = "SELECT AR04FECINICIO FROM AR0400 WHERE " & _
        "CI22NUMHISTORIA= ? AND AR00CODTIPOLOGIA= ? " & _
        " AND AR04FECFIN IS NULL"
        Set QYAR = objApp.rdoConnect.CreateQuery("", STR)
          QYAR(0) = IdPersona1.Historia
          QYAR(1) = cboDBCombo1(0).Columns(0).Value
'          QYAR(2) = "A"
        Set RDAR = QYAR.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If RDAR.RowCount = 0 Then 'Sino existe el registro en la AR0400 se crea
            'y se introduce la fecha de inicio del papel
            STR = "SELECT AR04FECINICIO FROM AR0400 WHERE " & _
            "CI22NUMHISTORIA= ? AND AR00CODTIPOLOGIA= ? " & _
            "AND AR06CODESTADO=?"
            Set QYal = objApp.rdoConnect.CreateQuery("", STR)
              QYal(0) = IdPersona1.Historia
              QYal(1) = "01"
              QYal(2) = "A"
            Set RDal = QYal.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            fechaInicio = Format(RDal!AR04FECINICIO, "dd/mm/yyyy hh:mm:ss") 'fecha inicio del papel
            RDal.Close
            QYal.Close
              STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
              "AR04FECINICIO, AR06CODESTADO) VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?)"
              Set QYin = objApp.rdoConnect.CreateQuery("", STR)
                QYin(0) = IdPersona1.Historia
                QYin(1) = cboDBCombo1(0).Columns(0).Value
                QYin(2) = Format(fechaInicio, "dd/mm/yyyy hh:mm:ss")
                QYin(3) = "A"
                QYin.Execute
                dtcDateCombo1(1).Date = fechaInicio
        Else 'Si existe se pone en la pantalla
                dtcDateCombo1(1).Date = Format(RDAR!AR04FECINICIO, "DD/MM/YYYY HH:MM:SS")
        End If
        RDAR.Close
        QYAR.Close
  End If
  ' sacar doctores relacionados a un departamento
  If intIndex = 2 Then
    Call objWinInfo.CtrlSet(cboDBCombo1(5), "")
    If cboDBCombo1(2).Value <> "" Then
      objWinInfo.CtrlGetInfo(cboDBCombo1(5)).blnReadOnly = False
      objWinInfo.CtrlGetInfo(cboDBCombo1(5)).strSQL = _
      "SELECT DISTINCT AD0300.SG02COD,SG02APE1,SG02APE2,SG02NOM,AD03FECFIN " & _
      "FROM SG0200,AD0300 " & _
      "WHERE SG0200.SG02COD=AD0300.SG02COD "
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        objWinInfo.CtrlGetInfo(cboDBCombo1(5)).strSQL = _
        objWinInfo.CtrlGetInfo(cboDBCombo1(5)).strSQL & "AND AD0300.AD03FECFIN IS NULL AND SG02FECDES IS NULL "
      End If
      objWinInfo.CtrlGetInfo(cboDBCombo1(5)).strSQL = _
      objWinInfo.CtrlGetInfo(cboDBCombo1(5)).strSQL & "AND AD0300.AD02CODDPTO=" & cboDBCombo1(2).Value & _
      " AND AD31CODPUESTO <> '21'" & _
      " ORDER BY SG0200.SG02APE1,SG0200.SG02APE2,SG0200.SG02NOM"
      
      
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(5)))
    End If
  End If
'  If intIndex = 3 Then
'    ' rellenar combo asistencias de la persona respecto al campo procesos
'    If cboDBCombo1(3).Value <> "" Then
'      'Asistencias del Proceso
'      objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD0800.AD01CODASISTENCI FROM AD0800,AD0100 " & _
'        " WHERE AD0800.AD07CODPROCESO=" & cboDBCombo1(3).Value & _
'        " AND AD0100.AD01CODASISTENCI=AD0800.AD01CODASISTENCI" & _
'        " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'        " ORDER BY AD0800.AD01CODASISTENCI"
'      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'    Else
'      objWinInfo.CtrlGetInfo(cboDBCombo1(4)).strSQL = "SELECT AD01CODASISTENCI FROM AD0100 " & _
'        " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'        " AND AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 " & _
'        " ORDER BY AD01CODASISTENCI"
'      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(4)))
'    End If
'  End If
'  If intIndex = 4 Then
'    ' rellenar combo procesos de la persona respecto al campo asistencias
'    If cboDBCombo1(4).Value <> "" Then
'      'Procesos de Asistencias
'      objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD0700.AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700,AD0800" & _
'        " WHERE AD0800.AD01CODASISTENCI=" & cboDBCombo1(4).Value & _
'        " AND AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO" & _
'        " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & " AND AD0800.AD34CODESTADO = 1 " & _
'        " ORDER BY AD07DESNOMBPROCE"
'      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'    Else
'      'Procesos del paciente
'      objWinInfo.CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT AD07CODPROCESO,AD07DESNOMBPROCE FROM AD0700" & _
'        " WHERE CI21CODPERSONA=" & IdPersona1.Text & _
'        " AND AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 " & _
'        " ORDER BY AD07DESNOMBPROCE"
'      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(3)))
'    End If
'  End If
  If intIndex = 5 And cboDBCombo1(5).Text = "" Then
    cboDBCombo1(5).Columns(4).Value = Null
  End If
End Sub

Private Sub cboDBCombo1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  Select Case intIndex
    Case 0, 1, 2, 3
      If cboDBCombo1(intIndex).Columns(2).Value = "" Then
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
      Else
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
      End If
    Case 4
      If cboDBCombo1(intIndex).Columns(1).Value = "" Then
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
      Else
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
      End If
    Case 5
      If cboDBCombo1(intIndex).Columns(4).Value = "" Then
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(2).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(3).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
      Else
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(2).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(3).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
      End If
  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Function fblnArchivo() As Boolean
Dim STR As String
Dim qry As rdoQuery
Dim rs As rdoResultset

STR = "SELECT COUNT(*) FROM AD0300 WHERE SG02COD = ? AND"
STR = STR & " AD02CODDPTO = 8"
Set qry = objApp.rdoConnect.CreateQuery("", STR)
     qry(0) = objSecurity.strUser
 Set rs = qry.OpenResultset()
 If rs(0) = 0 Then
     fblnArchivo = False
 Else
     fblnArchivo = True
 End If
rs.Close
qry.Close
End Function


