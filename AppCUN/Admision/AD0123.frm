VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmAsistencias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Asistencias del Paciente"
   ClientHeight    =   7935
   ClientLeft      =   840
   ClientTop       =   795
   ClientWidth     =   9690
   ClipControls    =   0   'False
   Icon            =   "AD0123.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7935
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Asistencias del Proceso"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2370
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   5190
      Width           =   9510
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1905
         Index           =   1
         Left            =   135
         TabIndex        =   10
         Top             =   360
         Width           =   9255
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "Abierta"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "AD0123.frx":000C
         stylesets(1).Name=   "Cerrada"
         stylesets(1).BackColor=   16777215
         stylesets(1).Picture=   "AD0123.frx":0028
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16325
         _ExtentY        =   3360
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox txtText1 
      BackColor       =   &H00C0C0C0&
      Height          =   300
      HelpContextID   =   40102
      Index           =   1
      Left            =   1890
      Locked          =   -1  'True
      TabIndex        =   6
      Tag             =   "Nombre del Proceso|Nombre"
      Top             =   4290
      Width           =   7500
   End
   Begin VB.TextBox txtText1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      Height          =   300
      HelpContextID   =   40101
      Index           =   0
      Left            =   180
      Locked          =   -1  'True
      MaxLength       =   10
      TabIndex        =   5
      Tag             =   "C�digo de Proceso|Proceso"
      Top             =   4320
      Width           =   1155
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Asociar"
      Height          =   405
      Index           =   0
      Left            =   4050
      TabIndex        =   4
      Top             =   4680
      Width           =   1395
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Asistencias del Paciente"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3480
      Index           =   0
      Left            =   90
      TabIndex        =   1
      Top             =   495
      Width           =   9510
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2985
         Index           =   0
         Left            =   135
         TabIndex        =   2
         Top             =   360
         Width           =   9240
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "Abierta"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "AD0123.frx":0044
         stylesets(1).Name=   "Cerrada"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "AD0123.frx":0060
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16298
         _ExtentY        =   5265
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7650
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Proceso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   180
      TabIndex        =   8
      Top             =   4080
      Width           =   705
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Nombre"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   9
      Left            =   1890
      TabIndex        =   7
      Top             =   4050
      Width           =   660
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAsistencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub Form_Activate()
  
  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
  objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim strKey As String
  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "AD0100"
    '.strWhere = "CI21CODPERSONA = " & frmProcesos.IdPersona1.Text
    .strWhere = "CI21CODPERSONA = " & frmProcesos.txtText1(1).Text & " AND AD0100.AD34CODESTADO = 1 "
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("AD01FECINICIO", cwDescending)
    Call .FormAddOrderField("AD01FECFIN", cwDescending)
    
    Call .FormCreateFilterWhere("AD0100", "Tabla de Asistencias por Paciente")
    Call .FormAddFilterWhere("AD0100", "AD01CODASISTENCI", "C�d. Asistencia", cwString)
    Call .FormAddFilterOrder("AD0100", "AD01CODASISTENCI", "C�d. Asistencia")
  End With
  
  With objMultiInfo2
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "AD0802J"
    .strWhere = "AD07CODPROCESO = " & frmProcesos.txtText1(6).Text
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("AD01FECINICIO", cwDescending)
    Call .FormAddOrderField("AD01FECFIN", cwDescending)
    
    
    Call .FormCreateFilterWhere("AD0802J", "Tabla de Asistencias por Proceso")
    Call .FormAddFilterWhere("AD0802J", "AD01CODASISTENCI", "C�d. Asistencia", cwString)
    Call .FormAddFilterOrder("AD0802J", "AD01CODASISTENCI", "C�d. Asistencia")
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo1, "C�digo Asistencia", "AD01CODASISTENCI", cwNumeric, 4)
    'Call .GridAddColumn(objMultiInfo1, "Observaciones Asistencia", "AD01DESOBSERVA", cwString, 50)
    Call .GridAddColumn(objMultiInfo1, "Fecha Inicio", "AD01FECINICIO", cwDate, 9)
    Call .GridAddColumn(objMultiInfo1, "Fecha Fin", "AD01FECFIN", cwDate, 9)
    
    Call .GridAddColumn(objMultiInfo2, "C�digo Asistencia", "AD01CODASISTENCI", cwNumeric, 4)
    'Call .GridAddColumn(objMultiInfo2, "Observaciones Asistencia", "AD01DESOBSERVA", cwString, 50)
    Call .GridAddColumn(objMultiInfo2, "Fecha Inicio", "AD01FECINICIO", cwDate, 9)
    Call .GridAddColumn(objMultiInfo2, "Fecha Fin", "AD01FECFIN", cwDate, 9)

    Call .FormCreateInfo(objMultiInfo1)
    Call .FormCreateInfo(objMultiInfo2)
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    txtText1(0).Text = frmProcesos!txtText1(6).Text
    txtText1(1).Text = frmProcesos!txtText1(7).Text
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  grdDBGrid1(0).Columns(3).Width = 1700
  'grdDBGrid1(0).Columns(4).Width = 4500
  grdDBGrid1(0).Columns(4).Width = 1200
  grdDBGrid1(0).Columns(5).Width = 1200
  
  grdDBGrid1(1).Columns(3).Width = 1700
  'grdDBGrid1(1).Columns(4).Width = 4500
  grdDBGrid1(1).Columns(4).Width = 1200
  grdDBGrid1(1).Columns(5).Width = 1200
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If grdDBGrid1(intIndex).Columns(5).Value = "" Then
    Call grdDBGrid1(intIndex).Columns(3).CellStyleSet("Abierta", grdDBGrid1(intIndex).Row)
    'Call grdDBGrid1(intIndex).Columns(4).CellStyleSet("Abierta", grdDBGrid1(intIndex).Row)
    Call grdDBGrid1(intIndex).Columns(4).CellStyleSet("Abierta", grdDBGrid1(intIndex).Row)
    Call grdDBGrid1(intIndex).Columns(5).CellStyleSet("Abierta", grdDBGrid1(intIndex).Row)
  Else
    Call grdDBGrid1(intIndex).Columns(3).CellStyleSet("Cerrada", grdDBGrid1(intIndex).Row)
    'Call grdDBGrid1(intIndex).Columns(4).CellStyleSet("Cerrada", grdDBGrid1(intIndex).Row)
    Call grdDBGrid1(intIndex).Columns(4).CellStyleSet("Cerrada", grdDBGrid1(intIndex).Row)
    Call grdDBGrid1(intIndex).Columns(5).CellStyleSet("Cerrada", grdDBGrid1(intIndex).Row)
  End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Buttons
' -----------------------------------------------

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim strSQL As String
  Dim rdoConsulta As rdoQuery
  Dim vntA As Variant
  
  Select Case intIndex
   Case 0
    If grdDBGrid1(0).Columns(5).Value = "" Then
    'PROCESO-ASISTENCIA
      Dim rs As rdoResultset
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
      
'      strSQL = "SELECT AD01CODASISTENCI " & _
'                                      "FROM AD0800 WHERE " & _
'                                      "AD07CODPROCESO = " & txtText1(0).Text & " AND AD0800.AD34CODESTADO = 1 " & _
'                                      " AND AD01CODASISTENCI = " & grdDBGrid1(0).Columns(3).Value
      strSQL = "SELECT AD01CODASISTENCI " & _
               "FROM AD0800 WHERE " & _
               "AD07CODPROCESO = ? AND AD0800.AD34CODESTADO = 1 " & _
               " AND AD01CODASISTENCI = ?"
      
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = txtText1(0).Text
            rdoConsulta(1) = grdDBGrid1(0).Columns(3).Value
      
      Set rs = _
      rdoConsulta.OpenResultset(strSQL) ', rdOpenDynamic)
'      Set rs = _
'      objApp.rdoConnect.OpenResultset("SELECT AD01CODASISTENCI " & _
'                                      "FROM AD0800 WHERE " & _
'                                      "AD07CODPROCESO = " & txtText1(0).Text & " AND AD0800.AD34CODESTADO = 1 " & _
'                                      " AND AD01CODASISTENCI = " & grdDBGrid1(0).Columns(3).Value _
'                                      , rdOpenDynamic)
      If Not rs.EOF Then
        'Call objError.SetError(cwCodeMsg, "Error, Ya esta asociado el Proceso a la Asistencia", vntA)
        'vntA = objError.Raise
      Else
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'        strSQL = "INSERT INTO AD0800 (AD07CODPROCESO,AD01CODASISTENCI,AD08FECINICIO) VALUES ("
'        strSQL = strSQL & txtText1(0).Text & ","
'        strSQL = strSQL & grdDBGrid1(0).Columns(3).Value & ","
'        strSQL = strSQL & "SYSDATE)"
        strSQL = "INSERT INTO AD0800 (AD07CODPROCESO,AD01CODASISTENCI,AD08FECINICIO) VALUES ("
        strSQL = strSQL & "?,?,SYSDATE)"
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = txtText1(0).Text
            rdoConsulta(1) = grdDBGrid1(0).Columns(3).Value
            rdoConsulta.Execute
'        objApp.rdoConnect.Execute strSQL, 64
        'objApp.rdoConnect.Execute "Commit", 64
        
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'        strSQL = "INSERT INTO AD0500 (AD07CODPROCESO,AD01CODASISTENCI"
'        strSQL = strSQL & ",AD05FECINIRESPON,AD02CODDPTO,SG02COD) VALUES ("
'        strSQL = strSQL & txtText1(0).Text & ","
'        strSQL = strSQL & grdDBGrid1(0).Columns(3).Value & ","
'        strSQL = strSQL & "TO_DATE('" & frmProcesos.dtcDateCombo1(3) & "','DD-MM-YYYY'),"
'        strSQL = strSQL & frmProcesos.cboDBCombo1(1) & ",'"
'        strSQL = strSQL & frmProcesos.cboDBCombo1(2) & "')"
        strSQL = "INSERT INTO AD0500 (AD07CODPROCESO,AD01CODASISTENCI"
        strSQL = strSQL & ",AD05FECINIRESPON,AD02CODDPTO,SG02COD) VALUES ("
        strSQL = strSQL & "?,?,TO_DATE(?,'DD-MM-YYYY'),?,?)"
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = txtText1(0).Text
            rdoConsulta(1) = grdDBGrid1(0).Columns(3).Value
            rdoConsulta(2) = frmProcesos.dtcDateCombo1(3)
            rdoConsulta(3) = frmProcesos.cboDBCombo1(1)
            rdoConsulta(4) = frmProcesos.cboDBCombo1(2)
            rdoConsulta.Execute
'        objApp.rdoConnect.Execute strSQL, 64
        'objApp.rdoConnect.Execute "Commit", 64
      End If
      rs.Close
      Set rs = Nothing
    'RESPONSABLE PROCESO-ASISTENCIA
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'      Set rs = _
'      objApp.rdoConnect.OpenResultset("SELECT AD01CODASISTENCI " & _
'                                      "FROM AD0500 WHERE " & _
'                                      "AD07CODPROCESO = " & txtText1(0).Text & _
'                                      " AND AD01CODASISTENCI = " & grdDBGrid1(0).Columns(3).Value & _
'                                      " AND AD05FECINIRESPON = TO_DATE ('" & frmProcesos.dtcDateCombo1(3).Date & "','dd/mm/yy')" _
'                                      , rdOpenDynamic)
      strSQL = "SELECT AD01CODASISTENCI " & _
              "FROM AD0500 WHERE " & _
              "AD07CODPROCESO =? AND AD01CODASISTENCI =? " & _
              " AND AD05FECINIRESPON = TO_DATE (?,'DD/MM/YYYY')"
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = txtText1(0).Text
            rdoConsulta(1) = grdDBGrid1(0).Columns(3).Value
            rdoConsulta(2) = frmProcesos.dtcDateCombo1(3).Date
      Set rs = _
      rdoConsulta.OpenResultset(strSQL) ', rdOpenDynamic)
      
      
      
      If Not rs.EOF Then
       ' Call objError.SetError(cwCodeMsg, "Error, Ya esta asociado el responsable del proceso a la Asistencia", vntA)
       ' vntA = objError.Raise
      Else
      End If
    Else
      Call objError.SetError(cwCodeMsg, "Error, Ha seleccionado una Asistencia cerrada.", vntA)
      vntA = objError.Raise
    End If
  End Select
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh
End Sub
