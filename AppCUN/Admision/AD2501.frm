VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmDocumentacionNW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Documentaci�n Aportada"
   ClientHeight    =   6600
   ClientLeft      =   315
   ClientTop       =   1635
   ClientWidth     =   11205
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6600
   ScaleWidth      =   11205
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   11205
      _ExtentX        =   19764
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Personas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   480
      Width           =   11115
      Begin TabDlg.SSTab tabTab1 
         Height          =   1875
         HelpContextID   =   90001
         Index           =   0
         Left            =   150
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   390
         Width           =   10845
         _ExtentX        =   19129
         _ExtentY        =   3307
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Det&alle"
         TabPicture(0)   =   "AD2501.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "IdPersona1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD2501.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1635
            Index           =   0
            Left            =   -74880
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   120
            Width           =   10245
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18071
            _ExtentY        =   2884
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1335
            Left            =   240
            TabIndex        =   1
            Tag             =   "Persona"
            Top             =   240
            Width           =   9975
            _ExtentX        =   17595
            _ExtentY        =   2355
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Documentacion Aportada"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Index           =   1
      Left            =   60
      TabIndex        =   11
      Top             =   2970
      Width           =   11115
      Begin TabDlg.SSTab tabTab1 
         Height          =   2745
         HelpContextID   =   90001
         Index           =   1
         Left            =   150
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   360
         Width           =   10905
         _ExtentX        =   19235
         _ExtentY        =   4842
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Det&alle"
         TabPicture(0)   =   "AD2501.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cboDBCombo1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboDBCombo1(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "cboDBCombo1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtcDateCombo1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(7)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(5)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(6)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(4)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(14)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(0)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(2)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(8)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).ControlCount=   22
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD2501.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Index           =   8
            Left            =   1560
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Inspecci�n INSALUD|Inspecci�n INSALUD"
            Top             =   2280
            Width           =   3500
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Index           =   2
            Left            =   3480
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "Descripci�n Tipo Volante|Descripci�n Tipo Volante"
            Top             =   1680
            Width           =   3500
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD07CODPROCESO"
            Height          =   300
            HelpContextID   =   40101
            Index           =   0
            Left            =   8400
            TabIndex        =   6
            Tag             =   "Proceso|C�digo de Proceso"
            Top             =   480
            Width           =   1155
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD01CODASISTENCI"
            Height          =   300
            HelpContextID   =   40101
            Index           =   14
            Left            =   7140
            TabIndex        =   5
            Tag             =   "Asistencia|C�digo de Asistencia"
            Top             =   480
            Width           =   1155
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   315
            Index           =   4
            Left            =   8520
            TabIndex        =   10
            Top             =   1080
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Index           =   6
            Left            =   3000
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Descripci�n Tipo Documento|Descripci�n Tipo Documento"
            Top             =   480
            Width           =   3500
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "AD18CODDOCUMENTO"
            Height          =   315
            HelpContextID   =   40102
            Index           =   5
            Left            =   210
            ScrollBars      =   2  'Vertical
            TabIndex        =   2
            Tag             =   "N� Documento|N� Documento"
            Top             =   480
            Width           =   1395
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD18DESDOCUMENTO"
            Height          =   315
            HelpContextID   =   40102
            Index           =   7
            Left            =   210
            ScrollBars      =   2  'Vertical
            TabIndex        =   7
            Tag             =   "Descripci�n del Documento|Descripci�n"
            Top             =   1080
            Width           =   7275
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD18FECDOCUMENTO"
            Height          =   300
            Index           =   0
            Left            =   210
            TabIndex        =   8
            Tag             =   "Fecha Aportaci�n|Fecha Aportaci�n"
            Top             =   1680
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2505
            Index           =   1
            Left            =   -74880
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   120
            Width           =   10305
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18177
            _ExtentY        =   4419
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD19CODTIPODOCUM"
            Height          =   330
            Index           =   0
            Left            =   1800
            TabIndex        =   3
            Tag             =   "C�digo Tipo de Documento|C�digo Tipo de Documento"
            Top             =   480
            Width           =   1050
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "AD2501.frx":0070
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD2501.frx":008C
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1852
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD18FECVOLANTE"
            Height          =   300
            Index           =   1
            Left            =   7200
            TabIndex        =   9
            Tag             =   "Fecha Volante INSALUD|Fecha Volante"
            Top             =   1680
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD35CODTIPVOL"
            Height          =   330
            Index           =   1
            Left            =   2280
            TabIndex        =   29
            Tag             =   "C�digo Tipo de volante|C�digo Tipo de volante"
            Top             =   1680
            Width           =   1050
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "AD2501.frx":00A8
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD2501.frx":00C4
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1852
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD36CODINSPEC"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   30
            Tag             =   "C�digo Tipo de Documento|C�digo Tipo de Documento"
            Top             =   2280
            Width           =   1050
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "AD2501.frx":00E0
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD2501.frx":00FC
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1852
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Inspecci�n INSALUD"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   240
            TabIndex        =   27
            Top             =   2040
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Volante"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   7230
            TabIndex        =   26
            Top             =   1440
            Width           =   1515
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Volante"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   2280
            TabIndex        =   24
            Top             =   1440
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proceso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   8400
            TabIndex        =   23
            Top             =   240
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Asistencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   7140
            TabIndex        =   22
            Top             =   240
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Documento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   1770
            TabIndex        =   18
            Top             =   240
            Width           =   1410
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Documento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   17
            Top             =   240
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Aportaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   16
            Top             =   1440
            Width           =   1515
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   240
            TabIndex        =   15
            Top             =   840
            Width           =   1020
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   12
      Top             =   6315
      Width           =   11205
      _ExtentX        =   19764
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDocumentacionNW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim strCodPersSel As String
Dim strCodProcSel As String
Dim strCodAsisSel As String

Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Screen.MousePointer = vbHourglass
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  Set objWinInfo = New clsCWWin
  If objPipe.PipeExist("Persona") Then
    strCodPersSel = objPipe.PipeGet("Persona")
    Call objPipe.PipeRemove("Persona")
    End If
  If objPipe.PipeExist("Proceso") Then
    strCodProcSel = objPipe.PipeGet("Proceso")
    Call objPipe.PipeRemove("Proceso")
  End If
  If objPipe.PipeExist("Asistencia") Then
    strCodAsisSel = objPipe.PipeGet("Asistencia")
    Call objPipe.PipeRemove("Asistencia")
  End If
  
  If strCodPersSel <> "" Then
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
  Else
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
  End If
  
  With objMasterInfo
        .strName = "Personas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strTable = "CI2200"
    .intAllowance = cwAllowReadOnly
    If strCodPersSel <> "" Then
      .strWhere = "CI21CODPERSONA=" & strCodPersSel & " AND CI2200.AD34CODESTADO = 1 "
    Else
      .strWhere = " CI2200.AD34CODESTADO = 1 "
    End If
    
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
    
    Call .FormCreateFilterWhere("CI2200", "Personas")
    Call .FormAddFilterWhere("CI2200", "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22NOMBRE", "Nombre", cwString)

    Call .FormAddFilterOrder("CI2200", "CI22NOMBRE", "Nombre")

  End With
  
  With objDetailInfo
    .strName = "Documentaci�n Aportada"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    
    If objPipe.PipeExist("AD2501_ACCESO") Then
        .intAllowance = objPipe.PipeGet("AD2501_ACCESO")
        Call objPipe.PipeRemove("AD2501_ACCESO")
    Else
        .intAllowance = cwAllowAll
    End If
    
    .blnHasMaint = True
    
    .strTable = "AD1800"
    If strCodAsisSel <> "" Then
      .strWhere = "AD01CODASISTENCI=" & strCodAsisSel & " AND AD07CODPROCESO=" & strCodProcSel
    End If
    
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
    Call .FormAddOrderField("AD18CODDOCUMENTO", cwDescending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)              'nuevo
  
    Call .FormCreateFilterWhere("AD1800", "Documentaci�n Aportada")
    Call .FormAddFilterWhere("AD1800", "AD18CODDOCUMENTO", "C�d. Documento", cwNumeric)
    Call .FormAddFilterWhere("AD1800", "AD18DESDOCUMENTO", "Documento", cwNumeric)
    Call .FormAddFilterWhere("AD1800", "CI21CODPERSONA", "C�d. Persona", cwNumeric)
    Call .FormAddFilterWhere("AD1800", "AD07CODPROCESO", "C�d. Proceso", cwNumeric)
    Call .FormAddFilterWhere("AD1800", "AD35CODTIPVOL", "C�d. Tipo Volante", cwNumeric)
    Call .FormAddFilterWhere("AD1800", "AD18FECVOLANTE", "Fecha Volante", cwDate)
    Call .FormAddFilterWhere("AD1800", "AD36CODINSPEC", "C�d. Inspecci�n", cwNumeric)
    
    Call .FormAddFilterOrder("AD1800", "AD18CODDOCUMENTO", "Documento")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True

    .CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    .CtrlGetInfo(cboDBCombo1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(14)).blnReadOnly = True
    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    
    .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AD19CODTIPODOCUM, AD19DESTIPODOCUM, AD19FECFIN FROM AD1900"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(0)), "AD19CODTIPODOCUM", "SELECT AD19CODTIPODOCUM, AD19DESTIPODOCUM FROM AD1900 WHERE AD19CODTIPODOCUM = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(0)), txtText1(6), "AD19DESTIPODOCUM")
   .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT  AD35CODTIPVOL,AD35DESTIPVOL FROM AD3500 WHERE ((AD35FECFINVIG IS NULL) OR (AD35FECFINVIG > (SELECT SYSDATE FROM DUAL)))"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(1)), "AD35CODTIPVOL", "SELECT AD35CODTIPVOL, AD35DESTIPVOL FROM AD3500 WHERE AD35CODTIPVOL = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(1)), txtText1(2), "AD35DESTIPVOL")
    
     .CtrlGetInfo(cboDBCombo1(2)).strSQL = "SELECT AD36CODINSPEC, AD36DESINSPEC FROM AD3600 WHERE (AD36FECFINVIG IS NULL OR AD36FECFINVIG > SYSDATE)"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(2)), "AD36CODINSPEC", "SELECT AD36CODINSPEC, AD36DESINSPEC FROM AD3600 WHERE AD36CODINSPEC = ? AND (AD36FECFINVIG IS NULL OR AD36FECFINVIG > SYSDATE)")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(2)), txtText1(8), "AD36DESINSPEC")
    
    .CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    .CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo
    IdPersona1.ReadPersona                     'nuevo
  
    Call .WinRegister
    Call .WinStabilize
  End With
  'Invisible bot�n de B�squeda
  IdPersona1.blnSearchButton = False
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
   intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Set objSearch = New clsCWSearch
  
  Select Case strCtrl
    Case "txtText1(0)"
      With objSearch
        .strTable = "AD0803J"
        .strOrder = "ORDER BY AD07CODPROCESO"
        If txtText1(14).Text = "" Then
          .strWhere = "WHERE CI21CODPERSONA=" & IdPersona1.Text
        Else
          .strWhere = "WHERE CI21CODPERSONA=" & IdPersona1.Text & " AND AD01CODASISTENCI = " & txtText1(14).Text
        End If
        
        Set objField = .AddField("AD07CODPROCESO")
        objField.strSmallDesc = "C�digo de Proceso"
        
        Set objField = .AddField("AD08FECINICIO")
        objField.strSmallDesc = "Fecha de Inicio"
        
        Set objField = .AddField("AD08FECFIN")
        objField.strSmallDesc = "Fecha de Fin"
        
        If .Search Then
          Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD01CODASISTENCI"))
          Call objWinInfo.CtrlSet(txtText1(0), .cllValues("AD07CODPROCESO"))
        End If
      End With
      Set objSearch = Nothing
    Case "IdPersona1"                     'nuevo
      IdPersona1.SearchPersona         'nuevo
    Case "txtText1(14)"
      With objSearch
        .strTable = "AD0803J"
        .strOrder = "ORDER BY AD01CODASISTENCI"
        If txtText1(0).Text = "" Then
          .strWhere = "WHERE CI21CODPERSONA=" & IdPersona1.Text
        Else
          .strWhere = "WHERE CI21CODPERSONA=" & IdPersona1.Text & " AND AD07CODPROCESO = " & txtText1(0).Text
        End If
        
        Set objField = .AddField("AD01CODASISTENCI")
        objField.strSmallDesc = "C�digo de Asistencia"
        
        Set objField = .AddField("AD07CODPROCESO")
        objField.strSmallDesc = "C�digo de Proceso"
        objField.blnInDialog = False
        
        Set objField = .AddField("AD08FECINICIO")
        objField.strSmallDesc = "Fecha de Inicio"
        
        Set objField = .AddField("AD08FECFIN")
        objField.strSmallDesc = "Fecha de Fin"
        If .Search Then
          Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD01CODASISTENCI"))
          Call objWinInfo.CtrlSet(txtText1(0), .cllValues("AD07CODPROCESO"))
        End If
      End With
      Set objSearch = Nothing
    Case "txtText1(3)"
      With objSearch
        .strTable = "AD3600"
        .strOrder = "ORDER BY AD36CODINSPEC"
        .strWhere = " WHERE ((AD36FECFINVIG IS NULL) OR (AD36FECFINVIG > (SELECT SYSDATE FROM DUAL))) "
                
        Set objField = .AddField("AD36CODINSPEC")
        objField.strSmallDesc = "C�digo Inspecci�n"
        
        Set objField = .AddField("AD36DESINSPEC")
        objField.strSmallDesc = "Inspecci�n INSALUD"
        
        If .Search Then
          Call objWinInfo.CtrlSet(txtText1(3), .cllValues("AD36CODINSPEC"))
        End If
      End With
      Set objSearch = Nothing
  End Select
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Select Case strFormName
    Case "Documentaci�n Aportada"
      Call objSecurity.LaunchProcess("AD0303")
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(0)))
  End Select
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  Dim rs As rdoResultset
  Dim strSQL As String
  
  Call objWinInfo.CtrlSet(txtText1(4), IdPersona1.Text) 'nuevo
  Set rs = objApp.rdoConnect.OpenResultset("SELECT AD18CODDOCUMENTO_S.NEXTVAL FROM DUAL", rdOpenKeyset)
  Call objWinInfo.CtrlSet(txtText1(5), rs.rdoColumns(0).Value)
  rs.Close
  Set rs = Nothing
  SendKeys "{TAB}", True
  
  If strCodAsisSel <> "" Then 'objPipe.PipeExist("Asistencia") Then
    Call objWinInfo.CtrlSet(txtText1(14), strCodAsisSel) 'objPipe.PipeGet("Asistencia"))
    Call objWinInfo.CtrlSet(txtText1(0), strCodProcSel) 'objPipe.PipeGet("Proceso"))
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    If strFormName = "Documentaci�n Aportada" And cboDBCombo1(0).Text = "1" Then
        Call PendVolante
    End If
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  objWinInfo.CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AD19CODTIPODOCUM, AD19DESTIPODOCUM, AD19FECFIN FROM AD1900 ORDER BY AD19CODTIPODOCUM"
  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(0)))
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntA As Variant
  Dim rs As rdoResultset
  Dim rdoConsulta As rdoQuery
  Dim strSQL As String
  
  If txtText1(14).Text <> "" Then
    strSQL = "SELECT AD01CODASISTENCI FROM AD0100 WHERE CI21CODPERSONA=?"
    strSQL = strSQL & " AND AD01CODASISTENCI=? AND AD0100.AD34CODESTADO = 1 "
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = IdPersona1.Text
    rdoConsulta(1) = txtText1(14).Text
    Set rs = rdoConsulta.OpenResultset(strSQL)
    If rs.EOF Then
      MsgBox "La asistencia no pertenece a esta persona", vbInformation, "Aviso"
      blnCancel = True
    End If
    rs.Close
  Else
    MsgBox "No ha asignado ninguna asistencia", vbInformation, "Aviso"
  End If
  
  If cboDBCombo1(0).Columns(2).Value <> "" Then
    Call objError.SetError(cwCodeMsg, "Esta Tipo de Documento est� dado de baja. Elija otro.", vntA)
    vntA = objError.Raise
    blnCancel = True
  End If
End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    If btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24 Then
        If objWinInfo.objWinActiveForm.strName = "Personas" Then
            Exit Sub
        End If
    End If
  If btnButton.Index = 16 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If grdDBGrid1(1).Rows > 0 Then
    Call GridSize
  End If
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  If grdDBGrid1(1).Rows > 0 Then
    Call GridSize
  End If
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If grdDBGrid1(1).Rows > 0 Then
    Call GridSize
  End If
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If cboDBCombo1(intIndex).Columns(2).Value = "" Then
    Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
    Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
  Else
    Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
    Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
  End If
End Sub

Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Public Sub PendVolante()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, qryU As rdoQuery
    Dim strPA$
    
    If txtText1(14).Text <> "" And txtText1(0).Text <> "" Then 'SI  HAY PROCESO ASISTENCIA
        SQL = "SELECT COUNT(AD11INDVOLANTE) FROM AD1100"
        SQL = SQL & " WHERE AD07CODPROCESO = ?"
        SQL = SQL & " AND AD01CODASISTENCI = ?"
        SQL = SQL & " AND AD11INDVOLANTE = -1"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = txtText1(0).Text
        qry(1) = txtText1(14).Text
        Set rs = qry.OpenResultset()
        If Not rs.EOF Then
            If rs(0) > 1 Then
                Call objPipe.PipeSet("CodAsist_PendVol", txtText1(14).Text)
                Call objPipe.PipeSet("CodProc_PendVol", txtText1(0).Text)
                frmVolanteAportado.Show vbModal
                Set frmVolanteAportado = Nothing
            ElseIf rs(0) = 1 Then
                SQL = "�Desea Ud. ANOTAR COMO RECIBIDO el volante pendiente del paciente"
                SQL = SQL & " para este Proceso/Asistencia?"
                If MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption) = vbYes Then
                    SQL = "UPDATE AD1100 SET AD11INDVOLANTE = 0"
                    SQL = SQL & " WHERE AD07CODPROCESO = ?"
                    SQL = SQL & " AND AD01CODASISTENCI = ?"
                    SQL = SQL & " AND AD11INDVOLANTE = -1"
                    Set qryU = objApp.rdoConnect.CreateQuery("", SQL)
                    qryU(0) = txtText1(0).Text
                    qryU(1) = txtText1(14).Text
                    qryU.Execute
                    qryU.Close
                End If
            End If
        End If
        rs.Close
        qry.Close
    Else 'HAY NO  PROCESO ASISTENCIA
        SQL = "SELECT AD07CODPROCESO, AD01CODASISTENCI"
        SQL = SQL & " FROM AD1111J WHERE CI21CODPERSONA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = IdPersona1.Text
        Set rs = qry.OpenResultset(SQL)
        If Not rs.EOF Then
            While Not rs.EOF
                strPA = strPA & Chr(13) & rs("AD07CODPROCESO") & " / " & rs("AD01CODASISTENCI")
                rs.MoveNext
            Wend
            SQL = "Este Paciente esta pendiente de volante para el proceso/asistencia:" _
                & strPA & Chr(13) & "�Desea Ud. ANOTAR COMO RECIBIDO el volante pendiente?"
            If MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption) = vbYes Then
                SQL = "UPDATE AD1100 SET AD11INDVOLANTE = 0"
                SQL = SQL & " WHERE (AD07CODPROCESO, AD01CODASISTENCI) IN "
                SQL = SQL & " (SELECT AD07CODPROCESO, AD01CODASISTENCI"
                SQL = SQL & " FROM AD1111J"
                SQL = SQL & " WHERE CI21CODPERSONA=?)"
                Set qryU = objApp.rdoConnect.CreateQuery("", SQL)
                qryU(0) = IdPersona1.Text
                qryU.Execute
                qryU.Close
            End If
        End If
        rs.Close
        qry.Close
    End If
End Sub

Public Sub GridSize()
    grdDBGrid1(1).Columns(1).Width = 1000
    grdDBGrid1(1).Columns(2).Width = 500
    grdDBGrid1(1).Columns(3).Width = 1000
    grdDBGrid1(1).Columns(4).Width = 1000
    grdDBGrid1(1).Columns(5).Width = 1000
    grdDBGrid1(1).Columns(6).Width = 1000
    grdDBGrid1(1).Columns(7).Width = 1000
    grdDBGrid1(1).Columns(8).Width = 1000
    grdDBGrid1(1).Columns(9).Width = 1000
    grdDBGrid1(1).Columns(10).Width = 1000
    grdDBGrid1(1).Columns(11).Width = 1000
    grdDBGrid1(1).Columns(12).Width = 1000
End Sub

