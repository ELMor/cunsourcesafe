VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmHisPsiquiatria 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga Inicial"
   ClientHeight    =   8355
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   7035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8355
   ScaleWidth      =   7035
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   5640
      TabIndex        =   7
      Top             =   7800
      Width           =   1335
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   0
      Left            =   3360
      MaxLength       =   2
      TabIndex        =   4
      Text            =   "1"
      Top             =   7920
      Width           =   255
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   1
      Left            =   4080
      MaxLength       =   1
      TabIndex        =   3
      Text            =   "1"
      Top             =   7920
      Width           =   255
   End
   Begin VB.CommandButton cmdCargar 
      Caption         =   "Confirmar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   7800
      Width           =   1335
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   7725
      HelpContextID   =   2
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6975
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   2
      stylesets.count =   1
      stylesets(0).Name=   "Urgente"
      stylesets(0).BackColor=   255
      stylesets(0).Picture=   "AD3620.frx":0000
      SelectTypeRow   =   3
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   2
      Columns(0).Width=   2170
      Columns(0).Caption=   "Historia"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   9155
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   12303
      _ExtentY        =   13626
      _StockProps     =   79
   End
   Begin vsViewLib.vsPrinter vs 
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   450
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Fila"
      Height          =   255
      Index           =   0
      Left            =   3000
      TabIndex        =   6
      Top             =   8040
      Width           =   255
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Col."
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   5
      Top             =   8040
      Width           =   255
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Comienzo Impresion:"
      Height          =   195
      Left            =   1440
      TabIndex        =   2
      Top             =   8040
      Width           =   1455
   End
End
Attribute VB_Name = "frmHisPsiquiatria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim strHistoria As String
Dim intNF As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim TG(9) As String
Dim BNS As Integer
Dim BBS As Integer
Dim BAL As Integer

Private Sub cmdCargar_Click()
'Comprobar datos inicio
intNF = 0
Call pConfirEntradas
grdDBGrid1(0).RemoveAll
If grdDBGrid1(0).Rows = 0 Then
    grdDBGrid1(0).AddNew
End If
SendKeys "{TAB}"
End Sub


Private Sub cmdSalir_Click()
Unload Me
End Sub


Private Sub Form_Load()
intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433

TG(0) = "00110"
TG(1) = "10001"
TG(2) = "01001"
TG(3) = "11000"
TG(4) = "00101"
TG(5) = "10100"
TG(6) = "01100"
TG(7) = "00011"
TG(8) = "10010"
TG(9) = "01010"

BNS = 15
BBS = 45
BAL = 700
  grdDBGrid1(0).AddNew
  intNF = 0
End Sub

 
Private Sub grdDBGrid1_KeyPress(Index As Integer, KeyAscii As Integer)
If grdDBGrid1(0).col = 0 Then
    If KeyAscii = 13 Then
      KeyAscii = 8
      Call LlenarLinea
      Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
     KeyAscii = 1
    End If
    If Len(grdDBGrid1(0).Columns(0).Value) > 5 Then KeyAscii = 1
Else
    If KeyAscii = 13 Then KeyAscii = 8
End If
End Sub

Private Sub LlenarLinea()
    Dim sql As String
    Dim qry As rdoQuery
    Dim rs  As rdoResultset
    Dim strHistoria$
    Dim SQL1$
    Dim rs1 As rdoResultset
    Dim qry1 As rdoQuery
    Dim strCodDpto$
    Dim intR As Integer
    
    strHistoria = Trim(grdDBGrid1(0).Columns(0).Value)
    'NOMBRE
    sql = "SELECT CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL NOMBRE FROM CI2200 WHERE"
    sql = sql & " CI22NUMHISTORIA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Trim(strHistoria)
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        grdDBGrid1(0).Columns("Nombre").Value = rs(0).Value
    Else
        MsgBox "No existe esta historia", vbCritical
        Exit Sub
    End If
        rs.Close
        qry.Close
    sql = "SELECT COUNT(*) FROM AD0500 WHERE AD02CODDPTO = 122 AND AD01CODASISTENCI IN ("
    sql = sql & "SELECT AD01CODASISTENCI FROM AD0100 WHERE CI22NUMHISTORIA = ?) "
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = Trim(strHistoria)
    Set rs1 = qry1.OpenResultset()
    If rs1(0) = 0 Then
    intR = MsgBox("Este paciente no tiene ningun responsable de psiquiatria" & Chr$(13) _
                    & "Desea continuar?", vbYesNo)
        If intR = vbNo Then
            grdDBGrid1(0).Columns("Nombre").Value = ""
            Exit Sub
        End If
    End If
    rs1.Close
    qry1.Close
    intNF = intNF + 1
    If intNF = 33 Then
        pConfirEntradas
        intNF = 0
    Else
        grdDBGrid1(0).Update
        grdDBGrid1(0).AddNew
        SendKeys "{TAB}"
    End If
End Sub

Private Sub pAltaHistoria(strH As String)
Dim i As Integer
Dim strtipo$, strHistoria$
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql As String
strtipo = "04"

sql = "SELECT COUNT(*) FROM AR0400 WHERE CI22NUMHISTORIA = ? "
sql = sql & " AND  AR00CODTIPOLOGIA = '04' "
sql = sql & " AND AR06CODESTADO = 'A'"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strH
Set rs = qry.OpenResultset()
If rs(0) = 0 Then
    rs.Close
    qry.Close
    sql = "INSERT INTO AR0400 (CI22NUMHISTORIA,AR00CODTIPOLOGIA,"
    sql = sql & " AR04FECINICIO,AR06CODESTADO,AR02CODPTOARCHIVO)"
    sql = sql & " VALUES(?,'04',SYSDATE,'A','01')"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strH
    qry.Execute
    If Err > 0 Or qry.RowsAffected = 0 Then
        MsgBox Error & " H: " & strH, vbExclamation
    End If
    qry.Close
End If


End Sub

Private Sub pConfirEntradas()
Dim intPX As Integer
Dim intPY As Integer
Dim i As Integer
Dim strHistoria As String

On Error GoTo Canceltrans

objApp.BeginTrans
vs.PaperBin = 1
Dim vntBookMark As Variant
    intPX = intPosX + (intIncX * (Int(txtText1(1).Text) - 1))
    intPY = intPosY + (intIncY * (Int(txtText1(0).Text) - 1))
        grdDBGrid1(0).MoveFirst
        vs.StartDoc
        For i = 0 To grdDBGrid1(0).Rows - 1
        If grdDBGrid1(0).Columns("Historia").Value <> "" Then
            strHistoria = grdDBGrid1(0).Columns("Historia").CellValue(vntBookMark)
            If Len(strHistoria) < 6 Then strHistoria = fstrPonerCeros(strHistoria)
            Call pAltaHistoria(grdDBGrid1(0).Columns("Historia").Value)
            Call pEtiquetas(strHistoria & "04", intPX, intPY)
            Call pNombreFecha(strHistoria, "04", intPX, intPY)
            intPX = intPX + intIncX
            If intPX > 10000 Then
                intPX = intPosX
                intPY = intPY + intIncY
            End If
            If intPY > 17000 Then
                vs.EndDoc
                vs.PrintDoc
                vs.StartDoc
                intPY = intPosY
            End If
        End If
            grdDBGrid1(0).MoveNext
        Next i
        vs.EndDoc
        vs.PrintDoc

objApp.CommitTrans
MsgBox "ENTRADAS CONFIRMADAS", vbOKOnly
grdDBGrid1(0).RemoveAll
If grdDBGrid1(0).Rows = 0 Then
    grdDBGrid1(0).AddNew
End If
SendKeys "{TAB}"
Exit Sub
Canceltrans:
    objApp.RollbackTrans
    MsgBox "Errores al dar de alta las historias", vbExclamation, Me.Caption
End Sub
Private Sub pEtiquetas(Num As String, ByVal POSX As Integer, POSY As Integer)
Dim i As Integer
Dim resto As Integer
Dim j As String
Dim str1 As String
Dim str2 As String
''Dim POSX As String
''Dim POSY As String


resto = Len(Num) - Int(Len(Num) / 2) * 2
If resto > 0 Then Num = "0" & Num
str1 = "00"
str2 = "00"

For i = 0 To (Len(Num) / 2) - 1
    j = Mid(Num, i * 2 + 1, 1)
    str1 = str1 & TG(Int(j))
    j = Mid(Num, i * 2 + 2, 1)
    str2 = str2 & TG(Int(j))
Next i
str1 = str1 & "10"
str2 = str2 & "00"
For i = 0 To Len(str1) - 1
    If Mid(str1, i + 1, 1) = 0 Then
       Call vs.DrawRectangle(POSX, POSY, POSX + BNS, POSY + BAL)
       POSX = POSX + BNS
    Else
        Call vs.DrawRectangle(POSX, POSY, POSX + BBS, POSY + BAL)
        POSX = POSX + BBS
    End If
    If Mid(str2, i + 1, 1) = 0 Then
       POSX = POSX + BNS
    Else
       POSX = POSX + BBS
    End If
Next i
vs.CurrentX = POSX + 80
vs.CurrentY = POSY + 200
End Sub
Private Sub pNombreFecha(strHistoria As String, inttip As Integer, px As Integer, py As Integer)
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String
vs.Font = "Times New Roman"
vs.FontSize = 26
vs.FontBold = True
vs.Text = Format(strHistoria, "##,##000000")
vs.FontBold = False
vs.FontSize = 8
If inttip = 1 Then vs.Text = " H"
If inttip = 2 Then vs.Text = " R"
If inttip = 3 Then vs.Text = " G"
If inttip = 4 Then vs.Text = " P"
 
vs.FontSize = 10
sql = "SELECT CI22PRIAPEL||', '||CI22SEGAPEL||', '||CI22NOMBRE NOMBRE, CI22FECNACIM FROM "
sql = sql & "CI2200 WHERE CI22NUMHISTORIA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(strHistoria)
Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        vs.CurrentX = px
        vs.CurrentY = py + 730
        If Len(rs!Nombre) > 30 Then
            vs.Text = Left(rs!Nombre, 30)
            vs.Text = Chr$(13)
            vs.CurrentX = px
            vs.Text = Right(rs!Nombre, Len(rs!Nombre) - 30)
            If Not IsNull(rs!CI22FECNACIM) Then
                vs.Text = " " & rs!CI22FECNACIM
            End If
        Else
            vs.Text = rs!Nombre
            vs.Text = Chr$(13)
            vs.CurrentX = px
            If Not IsNull(rs!CI22FECNACIM) Then
                vs.Text = " " & rs!CI22FECNACIM
            End If
        End If
    End If
rs.Close
qry.Close

End Sub
Private Function fstrPonerCeros(strH As String) As String
Dim l As Integer
Dim i As Integer
fstrPonerCeros = strH
l = 6 - Len(strH)
For i = 0 To l - 1
    fstrPonerCeros = "0" & fstrPonerCeros
Next i
End Function

