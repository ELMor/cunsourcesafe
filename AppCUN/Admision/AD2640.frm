VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form frmDoctoresUrgencias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Doctores de Urgencias"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5880
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   5880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtCodMed 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   1680
      TabIndex        =   5
      Top             =   480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdResponsable 
      Caption         =   "Confirmar Responsable"
      Height          =   375
      Left            =   1680
      TabIndex        =   4
      Top             =   6240
      Width           =   2055
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   5055
      Left            =   240
      TabIndex        =   3
      Top             =   960
      Width           =   4935
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      ForeColorEven   =   0
      BackColorEven   =   -2147483633
      BackColorOdd    =   -2147483633
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   8705
      _ExtentY        =   8916
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSForms.Frame Frame1 
      Height          =   5415
      Left            =   0
      OleObjectBlob   =   "AD2640.frx":0000
      TabIndex        =   2
      Top             =   720
      Width           =   5415
   End
   Begin VB.TextBox txtText1 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   2040
      TabIndex        =   1
      Top             =   120
      Width           =   3375
   End
   Begin VB.Label Label1 
      Caption         =   "Responsable Actual"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1815
   End
End
Attribute VB_Name = "frmDoctoresUrgencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strProceso As String
Dim strAsistencia As String

Private Sub cmdResponsable_Click()
Dim sql$
Dim rs As rdoResultset
Dim qry As rdoQuery

If SSDBGrid1.SelBookmarks.Count = 0 Then 'si no tenemos ningun medico seleccionado
    If txtCodMed = constUSUARIO_PASTRANA Then 'si era pastrana lo dejamos
        MsgBox "El Dr. Pastrana queda como responsable de este Paciente", vbOKOnly
    Else  'si no es pastrana no podemos dejar que se quede el generico
        MsgBox "Seleccione un Responsable. ", vbCritical
        Exit Sub
    End If
Else 'si hemos seleccionado un medico
    If txtCodMed = constUSUARIO_PASTRANA Then 'si es pastrana
    'hacemos un cambio de responsabilidad
        sql = "UPDATE AD0500 SET AD05FECFINRESPON = SYSDATE WHERE AD01CODASISTENCI = ? AND "
        sql = sql & " AD07CODPROCESO = ? AND AD05FECFINRESPON IS NULL"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = strAsistencia
            qry(1) = strProceso
        qry.Execute
        qry.Close
        sql = "INSERT INTO AD0500 (AD07CODPROCESO, AD01CODASISTENCI,AD05FECINIRESPON,"
        sql = sql & " AD02CODDPTO,SG02COD) VALUES (?,?,SYSDATE,?,?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = strProceso
            qry(1) = strAsistencia
            qry(2) = constDPTO_URGENCIAS
            qry(3) = SSDBGrid1.Columns(0).Value
        qry.Execute
        qry.Close
        sql = "SELECT COUNT(*) FROM AD0800 WHERE AD07CODPROCESO = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = strProceso
        Set rs = qry.OpenResultset()
        If rs(0) = 1 Then
            rs.Close
            qry.Close
            sql = "UPDATE AD0400 SET AD04FECFINRESPON = SYSDATE WHERE "
            sql = sql & " AD07CODPROCESO = ? AND AD04FECFINRESPON IS NULL"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = strProceso
            qry.Execute
            qry.Close
            sql = "INSERT INTO AD0400 (AD07CODPROCESO,AD04FECINIRESPON,AD02CODDPTO,SG02COD) "
            sql = sql & "VALUES (?,SYSDATE,?,?)"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = strProceso
                qry(1) = constDPTO_URGENCIAS
                qry(2) = SSDBGrid1.Columns(0).Value
            qry.Execute
        End If
        
    Else 'si no es pastrana hacemos un update directamente.
     sql = "UPDATE AD0500 SET SG02COD = ? WHERE AD01CODASISTENCI = ? AND "
     sql = sql & " AD07CODPROCESO = ? AND AD05FECFINRESPON IS NULL"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
         qry(0) = SSDBGrid1.Columns(0).Value
         qry(1) = strAsistencia
         qry(2) = strProceso
     qry.Execute
     qry.Close
     sql = "SELECT COUNT(*) FROM AD0800 WHERE AD07CODPROCESO = ?"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
         qry(0) = strProceso
     Set rs = qry.OpenResultset()
     If rs(0) = 1 Then 'rs(0)
         rs.Close
         qry.Close
         sql = "UPDATE AD0400 SET SG02COD = ? WHERE "
         sql = sql & " AD07CODPROCESO = ? AND AD04FECFINRESPON IS NULL"
         Set qry = objApp.rdoConnect.CreateQuery("", sql)
             qry(0) = SSDBGrid1.Columns(0).Value
             qry(1) = strProceso
         qry.Execute
         qry.Close
     End If 'rs(0)
    End If 'si es o no pastrana
     MsgBox "El " & SSDBGrid1.Columns(1).Value & " queda como responsable de este Paciente", vbOKOnly
End If 'si hay o no medico seleccionado
Unload Me
End Sub

Private Sub Form_Load()
strProceso = objPipe.PipeGet("AD07CODPROCESO")
strAsistencia = objPipe.PipeGet("AD01CODASISTENCI")
Call pFormatearGrid
Call pResponsableActual
Call pLlenarGrid
End Sub
Private Sub pFormatearGrid()
With SSDBGrid1
    .Columns(0).Caption = "CODDOCTOR"
    .Columns(0).Visible = False
    .Columns(1).Caption = "Doctor / Doctora"
    .Columns(1).Width = 4000
End With
End Sub
Private Sub pResponsableActual()
Dim sql$
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT SG02NOM||' '||SG02APE1||' '||SG02APE2 DOCTOR, AD0500.SG02COD FROM AD0500,SG0200"
sql = sql & " WHERE AD0500.AD01CODASISTENCI = ? "
sql = sql & " AND AD0500.AD07CODPROCESO = ? "
sql = sql & " AND AD0500.AD05FECFINRESPON IS NULL"
sql = sql & " AND AD0500.SG02COD = SG0200.SG02COD"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strAsistencia
    qry(1) = strProceso
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    txtText1 = rs(0)
    txtCodMed = rs(1)
End If
End Sub
Private Sub pLlenarGrid()
Dim sql$
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT SG02COD, SG02TXTFIRMA FROM SG0200 WHERE SG02INDGUAR = -1"
sql = sql & " ORDER BY SG02APE1"
Set rs = objApp.rdoConnect.OpenResultset(sql)
SSDBGrid1.AddItem constUSUARIO_PASTRANA & Chr$(9) & constDESUSUARIO_PASTRANA
While Not rs.EOF
    SSDBGrid1.AddItem rs(0) & Chr$(9) & rs(1)
    rs.MoveNext
Wend
End Sub

