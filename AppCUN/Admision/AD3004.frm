VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmCambioDatosEconPA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambios Econ�micos en Proceso / Asistencia"
   ClientHeight    =   3120
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9300
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3120
   ScaleWidth      =   9300
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBuscarResp 
      Caption         =   "&Buscar Entidad Responsable"
      Height          =   435
      Left            =   3360
      TabIndex        =   11
      Top             =   120
      Width           =   2355
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Econ�micos"
      ForeColor       =   &H00FF0000&
      Height          =   2295
      Left            =   60
      TabIndex        =   5
      Top             =   720
      Width           =   8115
      Begin VB.CheckBox chkPendVol 
         Alignment       =   1  'Right Justify
         Caption         =   "Pendiente volante"
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   1920
         Width           =   1635
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Otra persona"
         Height          =   375
         Left            =   6900
         TabIndex        =   6
         Top             =   1320
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipoEcon 
         Height          =   315
         Left            =   1560
         TabIndex        =   0
         Top             =   300
         Width           =   3315
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboEntidad 
         Height          =   315
         Left            =   1560
         TabIndex        =   1
         Top             =   840
         Width           =   4275
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7408
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7541
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboRespEcon 
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   1380
         Width           =   5295
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8811
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   9340
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo Econ�mico:"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   1275
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Entidad:"
         Height          =   195
         Index           =   1
         Left            =   540
         TabIndex        =   8
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Resp. Econ�mico:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   1440
         Width           =   1395
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   8280
      TabIndex        =   3
      Top             =   840
      Width           =   915
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   8280
      TabIndex        =   4
      Top             =   2640
      Width           =   915
   End
End
Attribute VB_Name = "frmCambioDatosEconPA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strNH$
''Dim blnAcunsa As Boolean, blnEntColab As Boolean
Dim strTipoEconOld$, strEntidadOld$, strCodRespOld$, strPendVolOld$, intAcceso%

Private Sub cboEntidad_Click()
    If cboTipoEcon.Columns(0).Text <> constTE_PRIVADO Then
        cboRespEcon.RemoveAll: cboRespEcon.Text = ""
        Call pCargarRespEcon
    End If
End Sub

Private Sub cboEntidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboRespEcon_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboTipoEcon_Click()
    cboEntidad.RemoveAll: cboEntidad.Text = ""
    cboRespEcon.RemoveAll: cboRespEcon.Text = ""
    Call pCargarEntidades
    If cboTipoEcon.Columns(0).Text = constTE_PRIVADO Then Call pCargarRespEcon
End Sub

Private Sub cboTipoEcon_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdAceptar_Click()
    Dim msg$
    
    If intAcceso = 0 Then
        If cboTipoEcon.Columns(0).Text = strTipoEconOld _
        And cboEntidad.Columns(0).Text = strEntidadOld _
        And cboRespEcon.Columns(0).Text = strCodRespOld Then
            msg = "No se ha cambiado el Responsable Econ�mico."
            MsgBox msg, vbInformation, Me.Caption
            Exit Sub
        End If
    End If
    
    If fComprobaciones Then
        Call objPipe.PipeSet("AD_TipoEcon", cboTipoEcon.Columns(0).Text)
        Call objPipe.PipeSet("AD_Entidad", cboEntidad.Columns(0).Text)
        Call objPipe.PipeSet("AD_RespEcon", cboRespEcon.Columns(0).Text)
        Call objPipe.PipeSet("AD_PendVol", -CInt(chkPendVol.Value))
        Unload Me
    End If
End Sub

Private Sub cmdBuscar_Click()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strCodPers$
    
    Call objSecurity.LaunchProcess("AD2999")
    If objPipe.PipeExist("AD2999_CI21CODPERSONA") Then
        strCodPers = objPipe.PipeGet("AD2999_CI21CODPERSONA")
        Call objPipe.PipeRemove("AD2999_CI21CODPERSONA")
        Call objPipe.PipeRemove("AD2999_CI22NUMHISTORIA")
        cboRespEcon.RemoveAll
        SQL = "SELECT CI21CODPERSONA CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA) RECO_DES"
        SQL = SQL & " FROM CI2200"
        SQL = SQL & " WHERE CI21CODPERSONA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodPers
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            cboRespEcon.AddItem rs!CI21CODPERSONA_REC & Chr$(9) & rs!CI21CODPERSONA_REC & " - " & rs!RECO_DES
            rs.MoveNext
        Loop
        rs.Close
        qry.Close
        If cboRespEcon.Rows = 1 Then
            cboRespEcon.Text = cboRespEcon.Columns(1).Text
        Else
            cboRespEcon.Text = ""
        End If
    End If
End Sub

Private Sub cmdBuscarResp_Click()

    frmBuscarTipoEco.Show vbModal
    Set frmBuscarTipoEco = Nothing
    If objPipe.PipeExist("CODRECO") Then
        Call pCargarDatos(objPipe.PipeGet("TIPOECO"), objPipe.PipeGet("ENTIDAD"), objPipe.PipeGet("CODRECO"), strPendVolOld)
        Call objPipe.PipeRemove("DESRECO")
        Call objPipe.PipeRemove("TIPOECO")
        Call objPipe.PipeRemove("ENTIDAD")
        Call objPipe.PipeRemove("CODRECO")
    End If
End Sub

Private Sub cmdSalir_Click()
    If MsgBox("�Desea Ud. salir SIN GUARDAR los cambios?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    Call pFormatearControles
    Call pObtenerDatosOld
    Call pCargarTiposEcon
    If intAcceso = 1 Then
        'Acceso para cambio retroactivo
        Call pCargarDatos(strTipoEconOld$, strEntidadOld$, strCodRespOld$, strPendVolOld$)
    End If
End Sub

Private Sub pCargarTiposEcon()
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT CI32CODTIPECON, CI32DESTIPECON"
    SQL = SQL & " FROM CI3200"
    SQL = SQL & " WHERE CI32FECFIVGTEC IS NULL"
'    If Not blnAcunsa Then SQL = SQL & " AND CI32CODTIPECON <> '" & constTE_ACUNSA & "'"
'    If Not blnEntColab Then SQL = SQL & " AND CI32CODTIPECON <> '" & constTE_ENTCOLAB & "'"
    SQL = SQL & " ORDER BY CI32CODTIPECON"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipoEcon.AddItem rs!CI32CODTIPECON & Chr$(9) & rs!CI32CODTIPECON & " - " & rs!CI32DESTIPECON
        rs.MoveNext
    Loop
    rs.Close
End Sub
    
Private Sub pCargarEntidades()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strTE$
    
    strTE = cboTipoEcon.Columns(0).Text
    
    SQL = "SELECT CI13CODENTIDAD, CI13DESENTIDAD"
    SQL = SQL & " FROM CI1300"
    SQL = SQL & " WHERE CI13FECFIVGENT IS NULL"
    SQL = SQL & " AND CI32CODTIPECON = ?"
''    If strTE = constTE_PRIVADO Then
''        If Not blnAcunsa Then SQL = SQL & " AND CI13CODENTIDAD NOT LIKE('J%')"
''        If Not blnEntColab Then SQL = SQL & " AND CI13CODENTIDAD NOT LIKE('U%')"
''    End If
    SQL = SQL & " ORDER BY CI13CODENTIDAD"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strTE
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEntidad.AddItem rs!CI13CODENTIDAD & Chr$(9) & rs!CI13CODENTIDAD & " - " & rs!CI13DESENTIDAD
        rs.MoveNext
    Loop
    rs.Close
    If cboEntidad.Rows = 1 Then
        cboEntidad.Text = cboEntidad.Columns(1).Text
        cboEntidad_Click
    End If
End Sub

Private Sub pCargarRespEcon(Optional strCodResp$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strTE$
    
    strTE = cboTipoEcon.Columns(0).Text
    
    Select Case strTE
    Case constTE_PRIVADO
        If strCodResp <> "" Then
            SQL = "SELECT CI21CODPERSONA CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA) RECO_DES"
            SQL = SQL & " FROM CI2100"
            SQL = SQL & " WHERE CI21CODPERSONA = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = strCodResp
        Else
            SQL = "SELECT CI21CODPERSONA CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA) RECO_DES"
            SQL = SQL & " FROM CI2200"
            SQL = SQL & " WHERE CI22NUMHISTORIA = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = strNH
        End If
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            cboRespEcon.AddItem rs!CI21CODPERSONA_REC & Chr$(9) & rs!CI21CODPERSONA_REC & " - " & rs!RECO_DES
            rs.MoveNext
        Loop
        rs.Close
        qry.Close
    Case Else
        If cboEntidad.Text <> "" Then
            SQL = "SELECT CI21CODPERSONA_REC, RECO_DES"
            SQL = SQL & " FROM CI2901J"
            SQL = SQL & " WHERE CI32CODTIPECON = ?"
            SQL = SQL & " AND CI13CODENTIDAD = ?"
            SQL = SQL & " ORDER BY RECO_DES"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = cboTipoEcon.Columns(0).Text
            qry(1) = cboEntidad.Columns(0).Text
            Set rs = qry.OpenResultset()
            Do While Not rs.EOF
                cboRespEcon.AddItem rs!CI21CODPERSONA_REC _
                            & Chr$(9) & rs!CI21CODPERSONA_REC & " - " & rs!RECO_DES
                rs.MoveNext
            Loop
            rs.Close
            qry.Close
        End If
    End Select
    
    If cboRespEcon.Rows = 1 Then
        cboRespEcon.Text = cboRespEcon.Columns(1).Text
    Else
        cboRespEcon.Text = ""
    End If
End Sub

Private Sub pFormatearControles()
    cboTipoEcon.BackColor = objApp.objUserColor.lngMandatory
    cboEntidad.BackColor = objApp.objUserColor.lngMandatory
    cboRespEcon.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    fComprobaciones = True
End Function

Private Sub pObtenerDatosOld()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strCodAsist$, strCodProc$
    
    strNH = objPipe.PipeGet("AD3004_CI22NUMHISTORIA")
    intAcceso = objPipe.PipeGet("AD3004_ACCESO")
    
    SQL = "SELECT CI32CODTIPECON, CI13CODENTIDAD, CI21CODPERSONA, NVL(AD11INDVOLANTE,0) AD11INDVOLANTE"
    SQL = SQL & " FROM AD1100"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?"
    SQL = SQL & " AND AD11FECFIN IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = objPipe.PipeGet("AD3004_AD01CODASISTENCI")
    qry(1) = objPipe.PipeGet("AD3004_AD07CODPROCESO")
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strTipoEconOld = rs!CI32CODTIPECON
        strEntidadOld = rs!CI13CODENTIDAD
        If Not IsNull(rs!CI21CODPERSONA) Then strCodRespOld = rs!CI21CODPERSONA Else strCodRespOld = 0
        strPendVolOld = rs!AD11INDVOLANTE
    End If
    rs.Close
    qry.Close
    
    Call objPipe.PipeRemove("AD3004_CI22NUMHISTORIA")
    Call objPipe.PipeRemove("AD3004_ACCESO")
    Call objPipe.PipeRemove("AD3004_AD01CODASISTENCI")
    Call objPipe.PipeRemove("AD3004_AD07CODPROCESO")
    
''    'se mira si el paciente pertenece a Acunsa y/o a la Entidad Colaboradora
''    blnAcunsa = fTEAcunsa(strNH)
''    blnEntColab = fTEEntColab(strNH)
End Sub

Private Sub pCargarDatos(strTipoEcon$, strEntidad$, strCodResp$, strPendVol$)
    Dim i%
    For i = 1 To cboTipoEcon.Rows
        If i = 1 Then cboTipoEcon.MoveFirst Else cboTipoEcon.MoveNext
        If cboTipoEcon.Columns(0).Text = strTipoEcon Then
            cboTipoEcon.Text = cboTipoEcon.Columns(1).Text
            Call pCargarEntidades
            Exit For
        End If
    Next i
    For i = 1 To cboEntidad.Rows
        If i = 1 Then cboEntidad.MoveFirst Else cboEntidad.MoveNext
        If cboEntidad.Columns(0).Text = strEntidad Then
            cboEntidad.Text = cboEntidad.Columns(1).Text
            Call pCargarRespEcon(strCodResp)
            Exit For
        End If
    Next i
    For i = 1 To cboRespEcon.Rows
        If i = 1 Then cboRespEcon.MoveFirst Else cboRespEcon.MoveNext
        If cboRespEcon.Columns(0).Text = strCodResp Then
            cboRespEcon.Text = cboRespEcon.Columns(1).Text
            Exit For
        End If
    Next i
    If strPendVol = 0 Then chkPendVol.Value = 0 Else chkPendVol.Value = 1
End Sub
