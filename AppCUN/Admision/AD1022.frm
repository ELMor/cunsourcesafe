VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmHistoTipEco 
   Caption         =   "Admision.Historico de Tipos Econůmicos"
   ClientHeight    =   3405
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7065
   LinkTopic       =   "Form1"
   ScaleHeight     =   3405
   ScaleWidth      =   7065
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7020
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   6
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      MaxSelectedRows =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   6
      Columns(0).Width=   2805
      Columns(0).Caption=   "Fecha Inicio"
      Columns(0).Name =   "Fecha Inicio"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "Fecha Fin"
      Columns(1).Name =   "Fecha Fin"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1217
      Columns(2).Caption=   "Tip Eco"
      Columns(2).Name =   "Tip Eco"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   847
      Columns(3).Caption=   "Ent."
      Columns(3).Name =   "Ent."
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   4233
      Columns(4).Caption=   "Responsable Economico"
      Columns(4).Name =   "Responsable Economico"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "CODRES"
      Columns(5).Name =   "CODRES"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   12383
      _ExtentY        =   5953
      _StockProps     =   79
   End
End
Attribute VB_Name = "frmHistoTipEco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_Load()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT AD1100.CI32CODTIPECON,AD1100.CI13CODENTIDAD, AD1100.CI21CODPERSONA, ADFN01(AD1100.CI21CODPERSONA) RESPONSABLE, "
sql = sql & " AD0800.AD08FECINICIO, AD0800.AD08FECFIN "
sql = sql & " FROM AD0700,AD0100,AD0800,AD1100"
sql = sql & " WHERE AD0700.CI21CODPERSONA = ? "
sql = sql & " AND AD0100.CI21CODPERSONA = ? "
sql = sql & " AND AD0100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI"
sql = sql & " AND AD0700.AD07CODPROCESO = AD0800.AD07CODPROCESO"
sql = sql & " AND AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI"
sql = sql & " AND AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO"
sql = sql & " ORDER BY AD08FECINICIO DESC"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objPipe.PipeGet("PERSONA")
    qry(1) = qry(0)
Set rs = qry.OpenResultset()
Do While Not rs.EOF
    SSDBGrid1.AddItem rs!AD08FECINICIO & Chr$(9) _
                    & rs!AD08FECFIN & Chr$(9) _
                    & rs!CI32CODTIPECON & Chr$(9) _
                    & rs!CI13CODENTIDAD & Chr$(9) _
                    & rs!RESPONSABLE & Chr$(9) _
                    & rs!CI21CODPERSONA
    rs.MoveNext
Loop
End Sub

Private Sub SSDBGrid1_DblClick()
  Call objPipe.PipeSet("DESRECO", SSDBGrid1.Columns(4).Value)
  Call objPipe.PipeSet("TIPOECO", SSDBGrid1.Columns(2).Value)
  Call objPipe.PipeSet("ENTIDAD", SSDBGrid1.Columns(3).Value)
  Call objPipe.PipeSet("CODRECO", SSDBGrid1.Columns(5).Value)
  Unload Me
End Sub

