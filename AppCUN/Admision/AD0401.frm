VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmGestionCamas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Gesti�n de Camas"
   ClientHeight    =   5955
   ClientLeft      =   1320
   ClientTop       =   1950
   ClientWidth     =   9570
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5955
   ScaleWidth      =   9570
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Cambios Situaci�n"
      Height          =   405
      Index           =   0
      Left            =   4020
      TabIndex        =   24
      Top             =   5130
      Width           =   1545
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Camas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4545
      Index           =   0
      Left            =   60
      TabIndex        =   21
      Top             =   480
      Width           =   9420
      Begin TabDlg.SSTab tabTab1 
         Height          =   4050
         Index           =   0
         Left            =   120
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   360
         Width           =   9150
         _ExtentX        =   16140
         _ExtentY        =   7144
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AD0401.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(4)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(9)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboDBCombo1(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboDBCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtcDateCombo1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txttext1(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txttext1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txttext1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txttext1(3)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txttext1(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txttext1(5)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txttext1(6)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txttext1(8)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txttext1(7)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txttext1(9)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "TEXT1"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).ControlCount=   25
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AD0401.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox TEXT1 
            DataField       =   "GCFN06(AD15CODCAMA)"
            Height          =   285
            Left            =   2040
            TabIndex        =   2
            Tag             =   "Codigo Cama | Codigo Cama"
            Top             =   360
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   9
            Left            =   360
            TabIndex        =   30
            Tag             =   "C�digo Cama|C�digo Cama"
            Top             =   360
            Width           =   1470
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD07CODPROCESO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   7
            Left            =   360
            MaxLength       =   10
            TabIndex        =   12
            Tag             =   "C�digo Proceso|C�digo Proceso"
            Top             =   3600
            Width           =   1470
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   8
            Left            =   1890
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Nomre Proceso|Nomre Proceso"
            Top             =   3600
            Width           =   5565
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   1890
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Paciente|Paciente"
            Top             =   2910
            Width           =   5565
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD01CODASISTENCI"
            Height          =   330
            HelpContextID   =   30101
            Index           =   5
            Left            =   360
            MaxLength       =   10
            TabIndex        =   10
            Tag             =   "C�digo Asistencia|C�digo Asistencia"
            Top             =   2910
            Width           =   1470
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "AD14CODESTCAMA"
            Height          =   330
            Index           =   4
            Left            =   7950
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "C�digo Estado Cama"
            Top             =   990
            Visible         =   0   'False
            Width           =   525
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   3
            Left            =   1650
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Descripci�n Departamento|Descripci�n Departamento"
            Top             =   1590
            Width           =   5025
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   5100
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Descripci�n Estado Cama"
            Top             =   990
            Width           =   2775
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   1
            Left            =   1410
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n Tipo Cama|Descripci�n Tipo Cama"
            Top             =   990
            Width           =   3525
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD15CODCAMA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   480
            TabIndex        =   1
            Tag             =   "C�digo Cama|C�digo Cama"
            Top             =   360
            Width           =   1095
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD15FECINICIO"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   8
            Tag             =   "Fecha Inicio|Fecha Inicio"
            Top             =   2190
            Width           =   1845
            _Version        =   65537
            _ExtentX        =   3254
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD15FECFIN"
            Height          =   330
            Index           =   1
            Left            =   2760
            TabIndex        =   9
            Tag             =   "Fecha Fin|Fecha Fin"
            Top             =   2190
            Width           =   1845
            _Version        =   65537
            _ExtentX        =   3254
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3765
            Index           =   0
            Left            =   -74850
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   120
            Width           =   8535
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15055
            _ExtentY        =   6641
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD13CODTIPOCAMA"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   0
            Tag             =   "C�digo Tipo de Cama|Tipo de Cama"
            Top             =   990
            Width           =   930
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "AD0401.frx":0038
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD0401.frx":0054
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1640
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   1
            Left            =   360
            TabIndex        =   6
            Tag             =   "C�digo Departamento|Departamento"
            Top             =   1590
            Width           =   1170
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "AD0401.frx":0070
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD0401.frx":008C
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   2064
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proceso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   360
            TabIndex        =   29
            Top             =   3360
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre Proceso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1890
            TabIndex        =   28
            Top             =   3360
            Width           =   1410
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Paciente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1890
            TabIndex        =   27
            Top             =   2670
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Asistencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   360
            TabIndex        =   26
            Top             =   2670
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   2760
            TabIndex        =   20
            Top             =   1980
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   360
            TabIndex        =   19
            Top             =   1980
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   5100
            TabIndex        =   17
            Top             =   780
            Width           =   1125
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   330
            TabIndex        =   16
            Top             =   750
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   360
            TabIndex        =   18
            Top             =   1380
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   14
            Top             =   120
            Width           =   750
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   23
      Top             =   5670
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmGestionCamas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim strcodcama As String
Dim strcod As String




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim strSQL As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Gesti�n de Camas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "AD1500"
    .intAllowance = cwAllowAdd + cwAllowModify
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    Call .FormAddOrderField("AD15CODCAMA", cwAscending)
    
    Call .objPrinter.Add("AD004011", "Listado de Camas")
    
    .blnHasMaint = True
    
    
    Call .FormCreateFilterWhere("AD1500", "Gesti�n de Camas")
    'Call .FormAddFilterWhere("AD1500", "AD13CODTIPOCAMA", "Tipo de Cama", cwString)
    Call .FormAddFilterWhere("AD1500", "AD13CODTIPOCAMA", "Tipo de Cama", cwString, "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 ORDER BY AD13CODTIPOCAMA")
    'Call .FormAddFilterWhere("AD1500", "AD14CODESTCAMA", "Estado de Cama", cwString)
    Call .FormAddFilterWhere("AD1500", "AD14CODESTCAMA", "Estado de Cama", cwString, "SELECT AD14CODESTCAMA, AD14DESESTCAMA FROM AD1400 ORDER BY AD14CODESTCAMA")
    'Call .FormAddFilterWhere("AD1500", "AD02CODDPTO", "Departamento", cwNumeric)
    Call .FormAddFilterWhere("AD1500", "AD02CODDPTO", "Departamento", cwString, "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objSecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL" & _
             " AND AD32CODTIPODPTO = 3" & _
             " ORDER BY AD0200.AD02DESDPTO")
    Call .FormAddFilterOrder("AD1500", "AD15CODCAMA", "N�mero de Cama")

  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
   
    Call .FormCreateInfo(objDetailInfo)
    'PGM: EN LA BUSQUEDA LO BUSCA POR EL CODIGO DE CAMA
    'CONVERTIDO POR LA FUNCION GCFN06 NO EL QUE ESTA
    'EN LA BASE DE DATOS
    .CtrlGetInfo(TEXT1).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    
    '.CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    '.CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    '.CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    '.CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
  
    .CtrlGetInfo(cboDBCombo1(0)).blnForeign = True
    .CtrlGetInfo(cboDBCombo1(1)).blnForeign = True
          
    .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA, AD13FECFIN FROM AD1300"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(0)), "AD13CODTIPOCAMA", "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 WHERE AD13CODTIPOCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(0)), txtText1(1), "AD13DESTIPOCAMA")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "AD14CODESTCAMA", "SELECT AD14CODESTCAMA, AD14DESESTCAMA FROM AD1400 WHERE AD14CODESTCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(2), "AD14DESESTCAMA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "AD15CODCAMA", "SELECT GCFN06(AD15CODCAMA)CAMA FROM AD1500 WHERE AD15CODCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(9), "CAMA")

    
    strSQL = "SELECT AD01CODASISTENCI,"
    strSQL = strSQL & "CI22NOMBRE || ' ' || CI22PRIAPEL || ' ' || CI22SEGAPEL PACIENTE "
    strSQL = strSQL & "FROM AD0100,CI2200 WHERE CI2200.AD34CODESTADO = 1 AND AD0100.CI21CODPERSONA=CI2200.CI21CODPERSONA " & " AND AD0100.AD34CODESTADO = 1 "
    strSQL = strSQL & "AND AD01CODASISTENCI = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "AD01CODASISTENCI", strSQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "PACIENTE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "AD07CODPROCESO", "SELECT AD07DESNOMBPROCE FROM AD0700 WHERE  AD0700.AD34CODESTADO = 1 AND AD07CODPROCESO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(8), "AD07DESNOMBPROCE")
    
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD02CODDPTO, AD02DESDPTO, AD02FECFIN FROM AD0200 WHERE AD02INDCAMA =-1 AND (AD02FECFIN IS NULL OR AD02FECFIN<(SELECT SYSDATE FROM DUAL))"
  
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(1)), "AD02CODDPTO", "SELECT AD02CODDPTO, AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(1)), txtText1(3), "AD02DESDPTO")

    Call .WinRegister
    Call .WinStabilize
  End With
     txtText1(9).Locked = False
     txtText1(9).BackColor = &H8000000F
     
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)

  If objWinInfo.intWinStatus = cwModeSingleAddRest Then
     Call objWinInfo.CtrlSet(txtText1(4), "1")
     Dim rs As rdoResultset
     Set rs = objApp.rdoConnect.OpenResultset("SELECT AD14DESESTCAMA " & "FROM AD1400 WHERE " & "AD14CODESTCAMA = " & txtText1(4).Text, rdOpenDynamic)
     If Not rs.EOF Then
        Call objWinInfo.CtrlSet(txtText1(2), rs!AD14DESESTCAMA)
        'txtText1(2).Text = rs!AD14DESESTCAMA
     End If
     rs.Close
     Set rs = Nothing
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  objWinInfo.CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA,AD13FECFIN FROM AD1300 WHERE AD13FECFIN IS NULL"
  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(0)))
  objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD02CODDPTO, AD02DESDPTO, AD02FECFIN FROM AD0200 WHERE AD02INDCAMA =-1 AND (AD02FECFIN IS NULL OR AD02FECFIN<(SELECT SYSDATE FROM DUAL))"
  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strSQL As String

  If Not blnError Then
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      strSQL = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA)"
      strSQL = strSQL & " VALUES ('"
      strSQL = strSQL & txtText1(0).Text & "',"
      strSQL = strSQL & "TO_DATE('" & dtcDateCombo1(0).Text & " " & strHora_Sistema
      strSQL = strSQL & "','DD/MM/YYYY HH24:MI:SS'),1)"
      objApp.rdoConnect.Execute strSQL, 64
      'objApp.rdoConnect.Execute "Commit", 64
    End If
  End If
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  objWinInfo.CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA,AD13FECFIN FROM AD1300"
  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(0)))
  objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD02CODDPTO, AD02DESDPTO, AD02FECFIN FROM AD0200 WHERE AD02INDCAMA =-1 AND (AD02FECFIN IS NULL OR AD02FECFIN<(SELECT SYSDATE FROM DUAL))"
  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntA As Variant
  If cboDBCombo1(0).Columns(2).Value <> "" Then
    Call objError.SetError(cwCodeMsg, "Este Tipo de Cama est� dado de baja. Elija otro", vntA)
    vntA = objError.Raise
    blnCancel = True
  End If
  If cboDBCombo1(1).Columns(2).Value <> "" Then
    Call objError.SetError(cwCodeMsg, "Este Departamento est� dado de baja. Elija otro", vntA)
    vntA = objError.Raise
    blnCancel = True
  End If
  If dtcDateCombo1(1).Date <> "" Then
    If CDate(Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(1).Date, "DD/MM/YYYY")) Then
      Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
      vntA = objError.Raise
      blnCancel = True
    End If
    If txtText1(4).Text = 2 Then
      Call objError.SetError(cwCodeMsg, "Esta Cama est� ocupada por un Paciente. No puede ponerle Fecha Fin .", vntA)
      dtcDateCombo1(1).Text = ""
      vntA = objError.Raise
      blnCancel = True
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Gesti�n de Camas" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Select Case strCtrl
    'Case "cboDBCombo1(0)"
    '     Call objSecurity.LaunchProcess("AD0402")
         'Call frmTiposCamas.Show(vbModal)
    '     Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(0)))
    Case "cboDBCombo1(1)"
         Call objSecurity.LaunchProcess("AD0203")
         'Call frmDepartamentosPersonal.Show(vbModal)
         Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
    End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 3 Or btnButton.Index = 2 Then
    txtText1(9).SetFocus
    txtText1(9).Text = ""
  End If
  If btnButton.Index = 30 Then
    Exit Sub
  End If
  If Not IsNull(objWinInfo.objWinActiveForm.rdoCursor!AD01CODASISTENCI) Then
    dtcDateCombo1(1).Enabled = False
    
 Else
    dtcDateCombo1(1).Enabled = True
   
    
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If cboDBCombo1(intIndex).Columns(2).Value = "" Then
    Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
    Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
  Else
    Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
    Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
   'PGM: AL TEXTBOX DEL NUMERO DE CAMA LE ASIGNO EL CODIGO
   'QUE LE HE INTRODUCIDO (EN EL DATACHANGE LO PIERDE)
  If strcod <> "" Then
  txtText1(9).Text = strcod
  strcod = ""
  End If
  
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)


DoEvents
  Dim sql As String
  Dim qry As rdoQuery
  Dim rst As rdoResultset
  On Error GoTo Cancel
  If intIndex = 9 And txtText1(9).Text <> "" Then
    sql = "SELECT AD15CODCAMA FROM AD1500 WHERE AD15CODCAMA=GCFN07('"
    sql = sql & txtText1(9).Text & "')"
    Set rst = objApp.rdoConnect.OpenResultset(sql)
    If Not rst.EOF Then
      Call objWinInfo.CtrlSet(txtText1(0), rst(0).Value)
    Else
      'PGM: SI NO EXISTE GUARDO EL CODIGO DE LA CAMA
      'Y CONVIERTO ESE CODIGO AL DE LA BASE DE DATOS
      strcod = txtText1(9).Text
      Call Convertir_Codigo
      txtText1(0).Text = strcodcama
    End If

   
  End If
  
Cancel:
   Call objWinInfo.CtrlLostFocus
   'PGM: AL TEXTBOX DEL NUMERO DE CAMA LE ASIGNO EL CODIGO
   'QUE LE HE INTRODUCIDO (EN EL LOSTFOCUS LO PIERDE)
   If strcod <> "" Then
    txtText1(9).Text = strcod
    strcod = ""
   End If
   
End Sub
Private Sub Convertir_Codigo()
Dim strprimero As String
Dim strsegundo As String
Dim strtercero As String
If Len(txtText1(9).Text) > 3 Then
  strprimero = "0" & Mid(txtText1(9).Text, 1, 1)
  strsegundo = "0" & Mid(txtText1(9).Text, 2, 2)
  strtercero = "0" & Mid(txtText1(9).Text, 5, 1)
Else
  strprimero = "0" & Mid(txtText1(9).Text, 1, 1)
  strsegundo = "0" & Mid(txtText1(9).Text, 2, 2)
  strtercero = "00"
End If
strcodcama = strprimero & strsegundo & strtercero


End Sub

Private Sub txtText1_Change(intIndex As Integer)

  Call objWinInfo.CtrlDataChange
  
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command
' -----------------------------------------------

Private Sub Command1_Click(intIndex As Integer)
  Dim vntA As Variant
  
  Select Case intIndex
    Case 0
      If txtText1(0).Text = "" Then
        Call objError.SetError(cwCodeMsg, "Hay que seleccionar una Cama", vntA)
        vntA = objError.Raise
        Exit Sub
      Else
        Call objSecurity.LaunchProcess("AD0408")
        'Call frmSituacionCamas.Show(vbModal)
        objWinInfo.DataRefresh
      End If
  End Select
End Sub


