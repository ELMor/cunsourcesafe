VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmAsistenciasAbiertas 
   Caption         =   "Admision. Asistencias Ambularotias Abiertas"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdFactura 
      Caption         =   "Facturacion"
      Height          =   375
      Left            =   8880
      TabIndex        =   16
      Top             =   8040
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvisos 
      Height          =   375
      Left            =   7680
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   8040
      Width           =   1095
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "SALIR"
      Height          =   375
      Index           =   3
      Left            =   10200
      TabIndex        =   12
      Top             =   8040
      Width           =   1455
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Citas Paciente"
      Height          =   375
      Index           =   2
      Left            =   6120
      TabIndex        =   11
      Top             =   8040
      Width           =   1455
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Vision Global"
      Height          =   375
      Index           =   1
      Left            =   4560
      TabIndex        =   10
      Top             =   8040
      Width           =   1455
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Fin de Asistencia"
      Height          =   375
      Index           =   0
      Left            =   2880
      TabIndex        =   9
      Top             =   8040
      Width           =   1455
   End
   Begin VB.CommandButton cmdConsular 
      Caption         =   "Consultar"
      Height          =   375
      Left            =   9840
      TabIndex        =   8
      Top             =   240
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Procesos/Asistencia Abiertos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6855
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid ssgrdAsistencias 
         Height          =   6375
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   11205
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowDelete     =   -1  'True
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         MaxSelectedRows =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19764
         _ExtentY        =   11245
         _StockProps     =   79
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sscboDpto 
      Height          =   315
      Left            =   7200
      TabIndex        =   0
      Top             =   240
      Width           =   2295
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   1320
      TabIndex        =   1
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   240
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   4200
      TabIndex        =   2
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   240
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Frame Frame2 
      Caption         =   "Finalizar Asistencia"
      Height          =   735
      Left            =   120
      TabIndex        =   13
      Top             =   7800
      Width           =   4335
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         DataField       =   "AD27CODALTAASIST"
         Height          =   330
         HelpContextID   =   30110
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Tag             =   "Motivo fin Asistencia|Motivo fin Asistencia"
         Top             =   240
         Width           =   2505
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldDelimiter  =   """"
         FieldSeparator  =   ";"
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).Picture=   "AD2600.frx":0000
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "AD2600.frx":001C
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1296
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5556
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "FechaFin"
         Columns(2).Name =   "FechaFin"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   4419
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   5880
      TabIndex        =   5
      Top             =   240
      Width           =   1275
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   120
      TabIndex        =   4
      Top             =   240
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   3000
      TabIndex        =   3
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "frmAsistenciasAbiertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strDptos As String




Private Sub cmdAvisos_Click()
Dim vntData(1)
If ssgrdAsistencias.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un paciente", vbCritical, "AVISO"
    Exit Sub
End If
vntData(1) = ssgrdAsistencias.Columns("CI21CODPERSONA").Value
Call objSecurity.LaunchProcess("PR0580", vntData)
End Sub

Private Sub cmdComun_Click(Index As Integer)
Dim strpersona As String
If ssgrdAsistencias.SelBookmarks.Count = 0 And Index <> 3 Then
    MsgBox "Seleccione un paciente", vbCritical, "AVISO"
    Exit Sub
End If
Screen.MousePointer = vbHourglass
strpersona = ssgrdAsistencias.Columns("CI21CODPERSONA").Value
Select Case Index
    Case 0 'fin de asistencia
        Call pFinAsitencia(strpersona)
    Case 1 'vision global
        Call pvisionGlobal(strpersona)
    Case 2 'mantenimiento de citas paciente
        Call pCitasPaciente(strpersona)
    Case 3 ' salir
        Unload Me
End Select
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdConsular_Click()
Screen.MousePointer = vbHourglass
pLlenarGrids
Screen.MousePointer = vbDefault
End Sub



Private Sub cmdFactura_Click()
Dim vntData
  'Comprueba si hay un paciente seleccionado
If ssgrdAsistencias.SelBookmarks.Count = 0 Then
    MsgBox "No hay ningun paciente selecionado.", vbExclamation, Me.Caption
    Exit Sub
End If
vntData = ssgrdAsistencias.Columns("CI21CODPERSONA").CellValue(ssgrdAsistencias.SelBookmarks(0))
Call objSecurity.LaunchProcess("FA0101", vntData)
End Sub

Private Sub dtcDateCombo1_CloseUp(Index As Integer)
    If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 3 Then
        MsgBox "Solo esta Permitido un Rando de 3 Dias", vbCritical
        dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
    End If
End Sub

Private Sub dtcDateCombo1_LostFocus(Index As Integer)
    If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 3 Then
        MsgBox "Solo esta Permitido un Rando de 3 Dias", vbCritical
        dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
    End If
End Sub

Private Sub Form_Load()
Call pFormatearGrid
Call pLlenarCombo
cmdAvisos.Picture = objApp.imlImageList.ListImages("i40").Picture

End Sub

Private Sub pFormatearGrid()

With ssgrdAsistencias
        .Columns(0).Caption = "Fecha Inicio"
        .Columns(0).Width = 1300
        .Columns(1).Caption = "N� Hist."
        .Columns(1).Alignment = 1
        .Columns(1).Width = 700
        .Columns(2).Caption = "Paciente"
        .Columns(2).Width = 3100
        .Columns(3).Caption = "Dr Responsable"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "Dpto Responsable"
        .Columns(4).Width = 1900
        .Columns(5).Caption = "CI21CODPERSONA"
        .Columns(5).Visible = False
        .Columns(6).Caption = "AD07CODPROCESO"
        .Columns(6).Visible = False
        .Columns(6).Visible = False
        .Columns(7).Caption = "AD01CODASISTENCI"
        .Columns(7).Visible = False
        .Columns(8).Caption = "Pruebas Pend."
        .Columns(8).Width = 700
        .Columns(9).Caption = "Avisos"
        .Columns(9).Width = 700
        .Columns(9).style = ssStyleCheckBox
End With

End Sub
Private Sub pLlenarCombo()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

strDptos = ""

sql = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO FROM AD0300, AD0200 WHERE "
sql = sql & "AD0300.SG02COD = ? AND AD0300.AD02CODDPTO = AD0200.AD02CODDPTO"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objSecurity.strUser
Set rs = qry.OpenResultset()
sscboDpto.AddItem "0;TODOS"
strDptos = "("
While Not rs.EOF
    sscboDpto.AddItem rs!AD02CODDPTO & ";" & rs!AD02DESDPTO
    strDptos = strDptos & rs!AD02CODDPTO & ","
    rs.MoveNext
Wend
strDptos = Left(strDptos, Len(strDptos) - 1)
strDptos = strDptos & ")"
rs.Close
qry.Close
sql = "SELECT AD27CODALTAASIST, AD27DESALTAASIST FROM " & "AD2700 ORDER BY AD27CODALTAASIST"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    cboDBCombo1(1).AddItem rs!AD27CODALTAASIST & ";" & rs!AD27DESALTAASIST
    rs.MoveNext
Wend
rs.Close
End Sub
Private Sub pLlenarGrids()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim qry1 As rdoQuery
Dim rs1 As rdoResultset
Dim strPend As String
Dim intMensaje As Integer


ssgrdAsistencias.RemoveAll

sql = "SELECT AD0800.AD01CODASISTENCI, AD0800.AD07CODPROCESO, AD0800.AD08FECINICIO,"
sql = sql & "AD0200.AD02DESDPTO, 'Dr '||SG02APE1 DOCTOR, CI2200.CI22NUMHISTORIA, CI2200.CI21CODPERSONA,"
sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE "
sql = sql & " FROM AD0800,AD0100,CI2200, SG0200, AD0200, AD0500, AD2500"
sql = sql & " WHERE AD0800.AD08FECFIN IS NULL"
sql = sql & " AND AD0800.AD08FECINICIO >= TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY')"
sql = sql & " AND AD0800.AD08FECINICIO < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY')"
sql = sql & " AND AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO"
sql = sql & " AND AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI"
If sscboDpto.Columns(0).Value = 0 Then
    sql = sql & " AND AD0500.AD02CODDPTO IN " & strDptos
Else
    sql = sql & " AND AD0500.AD02CODDPTO =  " & sscboDpto.Columns(0).Value
End If
sql = sql & " AND AD0500.AD05FECFINRESPON IS NULL"
sql = sql & " AND AD0500.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND AD0500.SG02COD = SG0200.SG02COD"
sql = sql & " AND AD0800.AD01CODASISTENCI = AD0100.AD01CODASISTENCI"
sql = sql & " AND AD0100.AD01FECFIN IS NULL"
sql = sql & " AND AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA"
sql = sql & " AND AD0100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI"
sql = sql & " AND AD2500.AD25FECFIN IS NULL"
sql = sql & " AND AD2500.AD12CODTIPOASIST <> 1"
sql = sql & " ORDER BY AD0500.AD02CODDPTO, AD0800.AD08FECINICIO"

Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    sql = "SELECT COUNT(*) FROM PR0400 WHERE PR0400.CI21CODPERSONA = ?"
    sql = sql & " AND PR0400.PR37CODESTADO IN (1,2)"
    sql = sql & " AND PR0400.AD01CODASISTENCI = ?"
    sql = sql & " AND PR0400.AD07CODPROCESO = ?"
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = rs!CI21CODPERSONA
        qry1(1) = rs!AD01CODASISTENCI
        qry1(2) = rs!AD07CODPROCESO
    Set rs1 = qry1.OpenResultset()
    If rs1(0) = 0 Then strPend = "NO" Else strPend = "SI"
    rs1.Close
    qry1.Close
     sql = "SELECT count(*) from  CI0400 WHERE CI21CodPersona = ?" _
            & " and (ci04FecCadMens >= trunc(sysdate+1)" _
            & " Or ci04FecCadMens Is Null)"
     Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = rs!CI21CODPERSONA
     Set rs1 = qry1.OpenResultset()
     If rs1(0) = 0 Then intMensaje = 0 Else intMensaje = 1
     rs1.Close
     qry1.Close
    ssgrdAsistencias.AddItem rs!AD08FECINICIO & Chr$(9) _
    & rs!CI22NUMHISTORIA & Chr$(9) _
    & rs!Paciente & Chr$(9) _
    & rs!DOCTOR & Chr$(9) & rs!AD02DESDPTO & Chr$(9) _
    & rs!CI21CODPERSONA & Chr$(9) _
    & rs!AD07CODPROCESO & Chr$(9) & rs!AD01CODASISTENCI & Chr$(9) _
    & strPend & Chr$(9) & intMensaje
    rs.MoveNext
Wend
End Sub

Private Sub pFinAsitencia(strCodP As String)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strMensaje As String
Dim intR As Integer
Dim I As Index
Dim intI As Integer
Dim blnUrgencias As Boolean
Dim strProcUrg As String
Dim dtcFechaFin As Date
Dim strFechaFin As String
strMensaje = ""

'MOTIVO DEL ALTA
If cboDBCombo1(1).Text = "" Then
    MsgBox "Seleccione un motivo de Alta", vbCritical
    Exit Sub
End If

'Miramos si hay mas procesos/asistencia relacionados
sql = "SELECT AD02DESDPTO, 'Dr '||SG02APE1 DOCTOR, AD0500.AD02CODDPTO,"
sql = sql & " AD0800.AD07CODPROCESO"
sql = sql & " FROM AD0800,AD0500,AD0200,SG0200 WHERE "
sql = sql & " AD0800.AD01CODASISTENCI = ? "
sql = sql & " AND AD0800.AD07CODPROCESO <> ? "
sql = sql & " AND AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI"
sql = sql & " AND AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO"
sql = sql & " AND AD0500.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND AD0500.SG02COD = SG0200.SG02COD"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
    qry(1) = ssgrdAsistencias.Columns("AD07CODPROCESO").Value
Set rs = qry.OpenResultset()

While Not rs.EOF
    strMensaje = Chr$(13) & rs!AD02DESDPTO & " " & rs!DOCTOR
    rs.MoveNext
Wend
rs.Close
qry.Close
 
'MIRAMOS SI TIENE ALGUNA HOSPITALIZACION
sql = "SELECT PR01DESCORTA FROM PR0100,PR0400 WHERE PR0400.CI21CODPERSONA = ?"
sql = sql & " AND PR0400.PR37CODESTADO IN (1,2,3)"
sql = sql & " AND PR0400.AD01CODASISTENCI = ?"
sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
sql = sql & " AND PR12CODACTIVIDAD = " & constACTIV_HOSPITALIZACION
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodP
    qry(1) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    MsgBox "Esta Asistencia tiene la prueba de hospitalizacion: " & Chr$(13) & _
    rs!PR01DESCORTA & "No es posible cerrarla", vbExclamation
    Exit Sub
End If

'Miramos si tiene actuaciones pendientes
sql = "SELECT PR01DESCORTA FROM PR0100,PR0400 WHERE PR0400.CI21CODPERSONA = ?"
sql = sql & " AND PR0400.PR37CODESTADO IN (1,2)"
sql = sql & " AND PR0400.AD01CODASISTENCI = ?"
sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodP
    qry(1) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
Set rs = qry.OpenResultset()
intI = 0
Do While Not rs.EOF
    strMensaje = strMensaje & Chr$(13) & rs!PR01DESCORTA
    rs.MoveNext
    intI = intI + 1
    If intI > 25 Then
        strMensaje = strMensaje & Chr$(13) & "etc. (consultar Citas paciente)"
        Exit Do
    End If
Loop
If strMensaje <> "" Then
    intR = MsgBox("ACTUACIONES PENDIENTES" & strMensaje & Chr$(13) _
    & "Desea Cerrarlo?", vbYesNo)
    If intR = vbNo Then Exit Sub
End If
rs.Close
qry.Close
strMensaje = ""

'si estamos en urgencias hay que sacar los responsables
If sscboDpto.Columns(0).Value = constDPTO_URGENCIAS Then
    blnUrgencias = True
    strProcUrg = ssgrdAsistencias.Columns("AD07CODPROCESO").Value
End If


If strMensaje <> "" Then
    intR = MsgBox("Si cierra la asistencia, cerrar� tambien los siguienes" & _
    "procesos/asistencia" & strMensaje & Chr$(13) _
    & "Desea Cerrarlo?", vbYesNo)
    If intR = vbNo Then Exit Sub
End If
objApp.BeginTrans
On Error Resume Next
'Ponemos fecha fin a la asistencia
'BUSCAMOS LA MAXIMA FECHA DE LAS PRUEBAS REALIZADAS, O LA MAXIMA FECHA DE
'EXTRACCION
sql = "SELECT MAX(PR04FECFINACT) FROM PR0400 WHERE "
sql = sql & " PR0400.AD01CODASISTENCI = ? "
sql = sql & " AND PR0400.CI21CODPERSONA = ?"
sql = sql & " AND PR0400.PR37CODESTADO = 4"
sql = sql & " AND PR0400.AD02CODDPTO NOT IN (" & strDptLab & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
    qry(1) = strCodP
Set rs = qry.OpenResultset()
If IsNull(rs(0)) Then
    strFechaFin = "00/00/00"
Else
    strFechaFin = Format$(rs(0), "dd/mm/yyyy hh:mm")
End If
rs.Close
qry.Close

sql = "SELECT MAX(PR04FECINIACT) FROM PR0400 WHERE "
sql = sql & " PR0400.AD01CODASISTENCI = ? "
sql = sql & " AND PR0400.CI21CODPERSONA = ?"
sql = sql & " AND PR0400.AD02CODDPTO  IN (" & strDptLab & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
    qry(1) = strCodP
Set rs = qry.OpenResultset()
If Not IsNull(rs(0)) Then
    If Format$(strFechaFin, "dd/mm/yyyy") = Format$("00/00/00", "dd/mm/yyyy") Then
        strFechaFin = Format$(rs(0), "dd/mm/yyyy  hh:mm")
    Else
        If Format$(strFechaFin, "dd/mm/yyyy  hh:mm") < Format$(rs(0), "dd/mm/yyyy  hh:mm") Then strFechaFin = Format$(rs(0), "dd/mm/yyyy  hh:mm")
    End If
End If
rs.Close
qry.Close
If Format$(strFechaFin, "dd/mm/yyyy") = Format$("00/00/00", "dd/mm/yyyy") Then
    sql = "SELECT COUNT(*) FROM PR0400 WHERE "
    sql = sql & " PR0400.AD01CODASISTENCI = ? "
    sql = sql & " AND PR0400.CI21CODPERSONA = ?"
    sql = sql & " AND PR37CODESTADO <> 6 "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
        qry(1) = strCodP
    Set rs = qry.OpenResultset()
    If rs(0) = 0 Then
        rs.Close
        qry.Close
        sql = "SELECT AD01FECINICIO FROM AD0100 WHERE AD01CODASISTENCI = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
         Set rs = qry.OpenResultset()
         strFechaFin = Format$(rs(0), "dd/mm/yyyy  hh:mm")
         rs.Close
         qry.Close
    Else
        strFechaFin = Format$(Now, "dd/mm/yyyy  hh:mm")
    End If
End If
'CERRAMOS LAS ASISTENCIA
sql = "UPDATE AD0100 SET AD01FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI'), AD27CODALTAASIST = ? WHERE AD01CODASISTENCI = ? "
sql = sql & " AND AD01FECFIN IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format$(strFechaFin, "dd/mm/yyyy  hh:mm")
    qry(1) = cboDBCombo1(1).Columns(0).Value
    qry(2) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
qry.Execute
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede cerrar esta Asistencia", vbOKOnly
    Exit Sub
End If
'Ponenos fecha fin a el/los proceso/asistencia
sql = "UPDATE AD0800 SET AD08FECFIN = SYSDATE WHERE AD01CODASISTENCI =?"
sql = sql & " AND AD08FECFIN IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ssgrdAsistencias.Columns("AD01CODASISTENCI").Value
qry.Execute
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede cerrar este Proceso/Asistencia", vbOKOnly
    Exit Sub
End If
objApp.CommitTrans

''If blnUrgencias Then 'en caso de que haya algun proceso asistencia con responsable urgencias
''    Call pResponsableUrgencias(strProcUrg, ssgrdAsistencias.Columns("AD01CODASISTENCI").Value)
''End If

ssgrdAsistencias.DeleteSelected
End Sub

Private Sub ssgrdAsistencias_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub ssgrdAsistencias_Click()
Dim imgX As ListImage
ssgrdAsistencias.SelBookmarks.RemoveAll
ssgrdAsistencias.SelBookmarks.Add (ssgrdAsistencias.RowBookmark(ssgrdAsistencias.Row))
    If ssgrdAsistencias.Columns("Avisos").Value = True Then
      Set imgX = objApp.imlImageList.ListImages("i44")
      cmdAvisos.Picture = imgX.Picture
    Else
      Set imgX = objApp.imlImageList.ListImages("i40")
      cmdAvisos.Picture = imgX.Picture
    End If
End Sub
Private Sub pvisionGlobal(strpersona As String)
Dim vntData() As Variant
ReDim vntData(1 To 2) As Variant
vntData(1) = strpersona
vntData(2) = 2
Call objSecurity.LaunchProcess("HC02", vntData())
Me.MousePointer = vbDefault

End Sub

Private Sub pCitasPaciente(strpersona As String)
Dim vntdatos
    vntdatos = strpersona
    Call objSecurity.LaunchProcess("CI0202", vntdatos)
End Sub
Private Sub pResponsableUrgencias(strProceso As String, strAsistencia As String)
Dim vntData(2)
    Screen.MousePointer = vbDefault
    vntData(0) = strAsistencia
    vntData(1) = strProceso
    Call objSecurity.LaunchProcess("AD2640", vntData)
    
End Sub
