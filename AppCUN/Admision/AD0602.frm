VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmEntradasArchivo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION.Entradas de Archivo"
   ClientHeight    =   8625
   ClientLeft      =   -45
   ClientTop       =   285
   ClientWidth     =   11910
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin vsViewLib.vsPrinter vs 
      Height          =   255
      Left            =   5160
      TabIndex        =   14
      Top             =   7680
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   450
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "Impresi�n de Etiquetas"
      Height          =   735
      Left            =   120
      TabIndex        =   7
      Top             =   7680
      Width           =   4815
      Begin VB.TextBox txtText1 
         Height          =   285
         Index           =   1
         Left            =   1440
         MaxLength       =   1
         TabIndex        =   11
         Text            =   "1"
         Top             =   240
         Width           =   255
      End
      Begin VB.TextBox txtText1 
         Height          =   285
         Index           =   0
         Left            =   720
         MaxLength       =   2
         TabIndex        =   10
         Text            =   "1"
         Top             =   240
         Width           =   255
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Impr. Todas"
         Height          =   375
         Index           =   5
         Left            =   3360
         TabIndex        =   9
         Top             =   240
         Width           =   1320
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Impr. Selecci�n"
         Height          =   375
         Index           =   4
         Left            =   1920
         TabIndex        =   8
         Top             =   240
         Width           =   1320
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Col."
         Height          =   375
         Index           =   1
         Left            =   1080
         TabIndex        =   13
         Top             =   240
         Width           =   255
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fila"
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   12
         Top             =   240
         Width           =   255
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Salir"
      Height          =   375
      Index           =   3
      Left            =   10320
      TabIndex        =   6
      Top             =   7920
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Confirmar"
      Height          =   375
      Index           =   2
      Left            =   8640
      TabIndex        =   5
      Top             =   7920
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Borrar Todo"
      Height          =   375
      Index           =   1
      Left            =   6840
      TabIndex        =   4
      Top             =   7920
      Width           =   1560
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Borrar Seleccionada"
      Height          =   375
      Index           =   0
      Left            =   5040
      TabIndex        =   3
      Top             =   7920
      Width           =   1560
   End
   Begin VB.Frame fraframe1 
      Height          =   7575
      Index           =   1
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11775
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   7125
         HelpContextID   =   2
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   11535
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   10
         stylesets.count =   1
         stylesets(0).Name=   "Urgente"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "AD0602.frx":0000
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   10
         Columns(0).Width=   1482
         Columns(0).Caption=   "Historia"
         Columns(0).Name =   "Historia"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3995
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Parte del Sobre"
         Columns(2).Name =   "Parte del Sobre"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1032
         Columns(3).Caption=   "Cod Dpto OR"
         Columns(3).Name =   "Cod Dpto OR"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2408
         Columns(4).Caption=   "Dpto 0rigen"
         Columns(4).Name =   "Dpto 0rigen"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   979
         Columns(5).Caption=   "Cod Dpto Destino"
         Columns(5).Name =   "Cod Dpto Destino"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1852
         Columns(6).Caption=   "Dpto Destino"
         Columns(6).Name =   "Dpto Destino"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   2328
         Columns(7).Caption=   "Fecha"
         Columns(7).Name =   "Fecha"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1984
         Columns(8).Caption=   "Hora"
         Columns(8).Name =   "Hora"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   1402
         Columns(9).Caption=   "Usuario"
         Columns(9).Name =   "Usuario"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   20346
         _ExtentY        =   12568
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   8325
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   529
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
End
Attribute VB_Name = "frmEntradasArchivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strHistoria As String
Dim intTipo As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim TG(9) As String
Dim BNS As Integer
Dim BBS As Integer
Dim BAL As Integer



Private Sub Command1_Click(Index As Integer)
Select Case Index
    Case 0 'BORRAR SELECCI�N
        pBorrarSeleccion
    Case 1 'BORRAR TODO
        grdDBGrid1(0).RemoveAll
        If grdDBGrid1(0).Rows = 0 Then
            grdDBGrid1(0).AddNew
        End If
'        SendKeys "{TAB}"
    Case 2 'CONFIRMAR
        Call pConfirEntradas
    Case 3 'SALIR
        Unload Me
    Case 4 'IMPRIMR SELECCI�N DE ETIQUETAS
        Call pImprimiEtiquetas(1)
    Case 5
        Call pImprimiEtiquetas(2)
End Select
End Sub

Private Sub Form_Load()
intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433

TG(0) = "00110"
TG(1) = "10001"
TG(2) = "01001"
TG(3) = "11000"
TG(4) = "00101"
TG(5) = "10100"
TG(6) = "01100"
TG(7) = "00011"
TG(8) = "10010"
TG(9) = "01010"

BNS = 15
BBS = 45
BAL = 700

grdDBGrid1(0).AddNew
End Sub



Private Sub grdDBGrid1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If grdDBGrid1(0).col = 0 Then
    If KeyCode = 13 Then
'     KeyCode = 9
      KeyCode = 8
     strHistoria = Left(grdDBGrid1(0).Columns(0).Value, Len(grdDBGrid1(0).Columns(0).Value) - 2)
     intTipo = Int(Right(grdDBGrid1(0).Columns(0).Value, 2))
     Call LlenarLinea
    End If
Else
    If KeyCode = 13 Then KeyCode = 9
End If
End Sub
Private Sub LlenarLinea()
Dim SQL As String
Dim qry As rdoQuery
Dim rs  As rdoResultset
'NOMBRE
SQL = "SELECT CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL NOMBRE FROM CI2200 WHERE"
SQL = SQL & " CI22NUMHISTORIA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Trim(strHistoria)
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    grdDBGrid1(0).Columns("Nombre").Value = rs(0).Value
Else
    MsgBox "No existe esta historia", vbCritical
    Exit Sub
End If
rs.Close
qry.Close
'DEPARTAMENTO DESTINO Y ORIGEN
SQL = "SELECT AD02CODDPTO_ORI, AD02ORI.AD02DESDPTO DORIGEN, AD02CODDPTO_DES, AD02DES.AD02DESDPTO DDESTINO,"
SQL = SQL & "SG02COD_USU FROM AD2100, AD0200 AD02ORI, AD0200 AD02DES  WHERE "
SQL = SQL & "AD2100.AD02CODDPTO_ORI = AD02ORI.AD02CODDPTO(+) AND "
SQL = SQL & "AD2100.AD02CODDPTO_DES = AD02DES.AD02CODDPTO(+)AND "
SQL = SQL & "AD2100.CI22NUMHISTORIA = ? AND "
SQL = SQL & "AD2100.AD20CODPARTSOBRE = ? "
SQL = SQL & "ORDER BY AD21FECMOVIMIENT DESC"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strHistoria
    qry(1) = intTipo
Set rs = qry.OpenResultset()
If Not rs.EOF Then
   grdDBGrid1(0).Columns("Cod Dpto OR").Value = rs!AD02CODDPTO_DES
   grdDBGrid1(0).Columns("Dpto 0rigen").Value = rs!DDESTINO
End If
rs.Close
qry.Close
SQL = "SELECT AR00DESTIPOLOGIA FROM AR0000 WHERE AR00CODTIPOLOGIA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = "0" & intTipo
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    grdDBGrid1(0).Columns("Parte del Sobre").Value = intTipo & " " & rs!AR00DESTIPOLOGIA
End If
rs.Close
qry.Close
grdDBGrid1(0).Columns("Cod Dpto Destino").Value = "8"
grdDBGrid1(0).Columns("Dpto Destino").Value = "Archivo"
grdDBGrid1(0).Columns("Usuario").Value = objSecurity.strUser
grdDBGrid1(0).Columns("Fecha").Value = strFecha_Sistema
grdDBGrid1(0).Columns("Hora").Value = strHora_Sistema
grdDBGrid1(0).Columns(0).Value = strHistoria
grdDBGrid1(0).AddNew
SendKeys "{TAB}"
End Sub

Private Sub pBorrarSeleccion()
Dim i As Integer
If grdDBGrid1(0).SelBookmarks.Count = 0 Then
    MsgBox "Por favor, Seleccione la historia para borrar", vbCritical
    Exit Sub
End If

'grdDBGrid1(0).SelBookmarks.item
grdDBGrid1(0).RemoveItem grdDBGrid1(0).Row
grdDBGrid1(0).Refresh

grdDBGrid1(0).AddNew
SendKeys "{TAB}"
End Sub
Private Sub pConfirEntradas()
Dim i As Integer
Dim SQL As String
Dim rs As rdoResultset
Dim qry As rdoQuery
On Error Resume Next
 grdDBGrid1(0).MoveFirst
 SQL = "INSERT INTO AD2100 (CI22NUMHISTORIA,AD21FECMOVIMIENT,SG02COD_USU,"
 SQL = SQL & " AD20CODPARTSOBRE,AD02CODDPTO_ORI,AD02CODDPTO_DES) VALUES "
 SQL = SQL & "(?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?,?,?)"
 objApp.BeginTrans
 Set qry = objApp.rdoConnect.CreateQuery("", SQL)
 
 Do While grdDBGrid1(0).Columns("Historia").Value <> ""
 strHistoria = Trim(grdDBGrid1(0).Columns("Historia").Value)
 intTipo = Int(Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1))
 
 
    qry(0) = Trim(grdDBGrid1(0).Columns("Historia").Value)
    qry(1) = Format(grdDBGrid1(0).Columns("Fecha").Value & " " & grdDBGrid1(0).Columns("Hora").Value, _
    "dd/mm/yyyy hh:mm")
    qry(2) = grdDBGrid1(0).Columns("Usuario").Value
    qry(3) = Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1)
    If Trim(grdDBGrid1(0).Columns("Cod dpto OR").Value) <> "" Then
        qry(4) = grdDBGrid1(0).Columns("Cod dpto OR").Value
    Else
        qry(4) = Null
    End If
    qry(5) = grdDBGrid1(0).Columns("Cod Dpto Destino").Value
    qry.Execute
    If Err > 0 Then
        MsgBox "SQL: " & SQL & Chr$(13) & qry(0) & Chr$(13) & qry(0) & Chr$(13) & qry(0) & Chr$(13) & qry(0) & Chr$(13) & qry(0), vbOKOnly
        MsgBox "Error confirmando la fila " & grdDBGrid1(0).Row & Chr$(13) & _
        "Confirme los datos", vbCritical
        objApp.RollbackTrans
        Exit Sub
    End If
    grdDBGrid1(0).MoveNext
    If strHistoria = Trim(grdDBGrid1(0).Columns("Historia").Value) And intTipo = Int(Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1)) Then
     Exit Do
    End If
Loop
objApp.CommitTrans
MsgBox "ENTRADAS CONFIRMADAS", vbOKOnly
grdDBGrid1(0).RemoveAll
If grdDBGrid1(0).Rows = 0 Then
    grdDBGrid1(0).AddNew
End If
'grdDBGrid1(0).RemoveItem 1
SendKeys "{TAB}"
End Sub

Private Sub pImprimiEtiquetas(intIndex As Integer)
Dim intPX As Integer
Dim intPY As Integer
Dim i As Integer
Dim strHistoria As String
vs.PaperBin = 1
Dim vntBookMark As Variant
    intPX = intPosX + (intIncX * (Int(txtText1(1).Text) - 1))
    intPY = intPosY + (intIncY * (Int(txtText1(0).Text) - 1))
Select Case intIndex

    Case 1
        If grdDBGrid1(0).SelBookmarks.Count = 0 Then
            MsgBox "Por favor, Seleccione las historias para Imprimir", vbCritical
            Exit Sub
        End If
        vs.StartDoc
        For i = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
            vntBookMark = grdDBGrid1(0).SelBookmarks(i)
            strHistoria = grdDBGrid1(0).Columns("Historia").CellValue(vntBookMark)
            If Len(strHistoria) < 6 Then strHistoria = fstrPonerCeros(strHistoria)
            Call pEtiquetas(strHistoria & "0" & Int(Left(grdDBGrid1(0).Columns(2).CellValue(vntBookMark), 1)), intPX, intPY)
            Call pNombreFecha(strHistoria, Int(Left(grdDBGrid1(0).Columns(2).CellValue(vntBookMark), 1)), intPX, intPY)
            intPX = intPX + intIncX
            If intPX > 10000 Then
                intPX = intPosX
                intPY = intPY + intIncY
            End If
            If intPY > 17000 Then
                vs.EndDoc
                vs.PrintDoc
                vs.StartDoc
                intPY = intPosY
            End If
        Next i
        vs.EndDoc
        vs.PrintDoc
    Case 2
        grdDBGrid1(0).MoveFirst
        vs.StartDoc
        For i = 0 To grdDBGrid1(0).Rows - 1
        If grdDBGrid1(0).Columns("Historia").Value <> "" Then
            Call pEtiquetas(grdDBGrid1(0).Columns("Historia").Value & "0" & Int(Left(grdDBGrid1(0).Columns(2).Value, 1)), intPX, intPY)
            Call pNombreFecha(grdDBGrid1(0).Columns("Historia").Value, Int(Left(grdDBGrid1(0).Columns(2).Value, 1)), intPX, intPY)
            intPX = intPX + intIncX
            If intPX > 10000 Then
                intPX = intPosX
                intPY = intPY + intIncY
            End If
            If intPY > 17000 Then
                vs.EndDoc
                vs.PrintDoc
                vs.StartDoc
                intPY = intPosY
            End If
        End If
            grdDBGrid1(0).MoveNext
        Next i
        vs.EndDoc
        vs.PrintDoc
End Select
End Sub
Private Sub pEtiquetas(Num As String, ByVal POSX As Integer, POSY As Integer)
Dim i As Integer
Dim resto As Integer
Dim j As String
Dim str1 As String
Dim str2 As String
''Dim POSX As String
''Dim POSY As String


resto = Len(Num) - Int(Len(Num) / 2) * 2
If resto > 0 Then Num = "0" & Num
str1 = "00"
str2 = "00"

For i = 0 To (Len(Num) / 2) - 1
    j = Mid(Num, i * 2 + 1, 1)
    str1 = str1 & TG(Int(j))
    j = Mid(Num, i * 2 + 2, 1)
    str2 = str2 & TG(Int(j))
Next i
str1 = str1 & "10"
str2 = str2 & "00"
For i = 0 To Len(str1) - 1
    If Mid(str1, i + 1, 1) = 0 Then
       Call vs.DrawRectangle(POSX, POSY, POSX + BNS, POSY + BAL)
       POSX = POSX + BNS
    Else
        Call vs.DrawRectangle(POSX, POSY, POSX + BBS, POSY + BAL)
        POSX = POSX + BBS
    End If
    If Mid(str2, i + 1, 1) = 0 Then
       POSX = POSX + BNS
    Else
       POSX = POSX + BBS
    End If
Next i
vs.CurrentX = POSX + 80
vs.CurrentY = POSY + 200
End Sub
Private Sub pNombreFecha(strHistoria As String, inttip As Integer, px As Integer, py As Integer)
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim SQL As String
Dim strSegApel As String
vs.Font = "Courier"
vs.FontSize = 25
vs.FontBold = True
vs.Text = Format(strHistoria, "##,##000000")
'vs.FontBold = False
vs.Font = "Courier New"
vs.FontSize = 10
vs.CurrentX = vs.CurrentX - 100
If inttip = 1 Then vs.Text = " H"
If inttip = 2 Then vs.Text = " R"
If inttip = 3 Then vs.Text = " G"
If inttip = 4 Then vs.Text = " P"
 
vs.FontSize = 10
SQL = "SELECT CI22PRIAPEL,CI22SEGAPEL,CI22NOMBRE, CI22FECNACIM FROM "
SQL = SQL & "CI2200 WHERE CI22NUMHISTORIA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Trim(strHistoria)
Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        vs.CurrentX = px
        vs.CurrentY = py + 730
        If IsNull(rs!CI22SEGAPEL) Then strSegApel = "" Else strSegApel = rs!CI22SEGAPEL
        If Len(rs!CI22PRIAPEL & strSegApel & rs!CI22NOMBRE) > 24 Then
            If Len(rs!CI22PRIAPEL & strSegApel) > 24 Then
                vs.Text = rs!CI22PRIAPEL
'                vs.Text = Chr$(13)
                vs.CurrentY = vs.CurrentY + 190
                vs.CurrentX = px
                If Len(rs!CI22NOMBRE & strSegApel) > 24 Then
                    vs.Text = Left(strSegApel & "," & rs!CI22NOMBRE, 26)
                Else
                    vs.Text = strSegApel & "," & rs!CI22NOMBRE
                End If
                If Not IsNull(rs!CI22FECNACIM) Then
'                    vs.Text = Chr$(13)
                    vs.CurrentY = vs.CurrentY + 190
                    vs.CurrentX = px
                    vs.Text = " " & rs!CI22FECNACIM
                End If
            Else
               vs.Text = rs!CI22PRIAPEL & "," & strSegApel & ","
'               vs.Text = Chr$(13)
                vs.CurrentY = vs.CurrentY + 190
               vs.CurrentX = px
               vs.Text = rs!CI22NOMBRE
               If Not IsNull(rs!CI22FECNACIM) Then
'                    vs.Text = Chr$(13)
                    vs.CurrentY = vs.CurrentY + 190
                    vs.CurrentX = px
                    vs.Text = " " & rs!CI22FECNACIM
                End If
            End If
        Else
            vs.Text = rs!CI22PRIAPEL & "," & strSegApel & "," & rs!CI22NOMBRE
'            vs.Text = Chr$(13)
            vs.CurrentY = vs.CurrentY + 190
            vs.CurrentX = px
            If Not IsNull(rs!CI22FECNACIM) Then
                vs.Text = " " & rs!CI22FECNACIM
            End If
        End If
    End If
rs.Close
qry.Close

End Sub
Private Function fstrPonerCeros(strH As String) As String
Dim l As Integer
Dim i As Integer
fstrPonerCeros = strH
l = 6 - Len(strH)
For i = 0 To l - 1
    fstrPonerCeros = "0" & fstrPonerCeros
Next i
End Function

