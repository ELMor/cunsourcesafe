VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmCambioRespPA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Responsable del Proceso/Asistencia"
   ClientHeight    =   1290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6330
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1290
   ScaleWidth      =   6330
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5160
      TabIndex        =   5
      Top             =   780
      Width           =   1035
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   5160
      TabIndex        =   4
      Top             =   240
      Width           =   1035
   End
   Begin SSDataWidgets_B.SSDBCombo cboDr 
      Height          =   315
      Left            =   1200
      TabIndex        =   1
      Top             =   780
      Width           =   3795
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6694
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBCombo cboDpto 
      Height          =   315
      Left            =   1200
      TabIndex        =   0
      Top             =   240
      Width           =   3795
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6694
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin MSMask.MaskEdBox mskHora 
      Height          =   315
      Left            =   5640
      TabIndex        =   3
      Top             =   -60
      Visible         =   0   'False
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   556
      _Version        =   327681
      BackColor       =   16777215
      MaxLength       =   5
      Mask            =   "##:##"
      PromptChar      =   "_"
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecha 
      Height          =   315
      Left            =   4020
      TabIndex        =   2
      Top             =   -60
      Visible         =   0   'False
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShowCentury     =   -1  'True
      StartofWeek     =   2
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Desde:"
      Height          =   255
      Index           =   2
      Left            =   3000
      TabIndex        =   8
      Top             =   0
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Dr:"
      Height          =   255
      Index           =   1
      Left            =   180
      TabIndex        =   7
      Top             =   840
      Width           =   915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Dpto:"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   6
      Top             =   300
      Width           =   915
   End
End
Attribute VB_Name = "frmCambioRespPA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodDptoOld$, strCodDrOld$

Private Sub cboDpto_Click()
    If cboDpto.Text <> cboDpto.Columns(0).Text Then
        cboDr.Text = "": cboDr.RemoveAll
        Call pCargarDr(cboDpto.Columns(0).Text)
    End If
End Sub

Private Sub cboDpto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboDr_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdAceptar_Click()
    Dim msg$
    
    If cboDpto.Columns(0).Text = strCodDptoOld And cboDr.Columns(0).Text = strCodDrOld Then
        msg = "No se ha cambiado el Dpto./Dr. responsable."
        MsgBox msg, vbInformation, Me.Caption
        Exit Sub
    End If
    
    If fComprobaciones Then
        Call objPipe.PipeSet("AD_DptoRespPA", cboDpto.Columns(0).Text)
        Call objPipe.PipeSet("AD_DrRespPA", cboDr.Columns(0).Text)
        Unload Me
    End If
End Sub

Private Sub cmdCancelar_Click()
    If MsgBox("�Desea Ud. salir SIN GUARDAR los cambios?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
        Unload Me
    End If
End Sub

Private Sub dcboFecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub Form_Load()
    Call pObtenerDatosOld
    Call pFormatearControles
    Call pCargarDptos
End Sub

Private Sub mskHora_GotFocus()
    mskHora.SelStart = 0
    mskHora.SelLength = Len(mskHora.Text)
End Sub

Private Sub mskHora_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub mskHora_LostFocus()
    If Not mskHora.Text = "__:__" Then
        mskHora.Text = objGen.ReplaceStr(mskHora.Text, "_", "0", 0)
        If Not IsDate(mskHora.Text) Then mskHora.Text = "__:__"
    End If
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    fComprobaciones = True
End Function

Private Sub pFormatearControles()
    cboDpto.BackColor = objApp.objUserColor.lngMandatory
    cboDr.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub pCargarDptos()
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT AD02CODDPTO, AD02DESDPTO"
    SQL = SQL & " FROM AD0200"
    SQL = SQL & " WHERE AD32CODTIPODPTO IN (" & constTIPODPTO_SERVCLINICOS & "," & constTIPODPTO_SERVBASICOS & ")"
    SQL = SQL & " ORDER BY AD02DESDPTO"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
        rs.MoveNext
    Loop
    rs.Close
End Sub

Private Sub pCargarDr(lngDpto&)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    'SQL = "SELECT SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
    SQL = "SELECT SG0200.SG02COD, DECODE(SG02APE1,'DPT',SG02APE2,NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM)) NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO IN (" & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & "," & constPUESTO_JEFE & ")"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " AND SG0200.SG02COD <> 'SDR'"
    SQL = SQL & " ORDER BY DECODE(SG02APE1,'DPT',SG02APE2,SG02APE1||' '||SG02APE2||', '||SG02NOM)"
    'SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngDpto
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboDr.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pObtenerDatosOld()
    If objPipe.PipeExist("AD_DPTORESP") Then
        strCodDptoOld = objPipe.PipeGet("AD_DPTORESP")
        Call objPipe.PipeRemove("AD_DPTORESP")
    End If
    If objPipe.PipeExist("AD_DRRESP") Then
        strCodDrOld = objPipe.PipeGet("AD_DRRESP")
        Call objPipe.PipeRemove("AD_DRRESP")
    End If
End Sub
