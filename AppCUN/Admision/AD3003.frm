VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmCambioOtrosDatosPA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambios en Proceso / Asistencia"
   ClientHeight    =   3795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8160
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3795
   ScaleWidth      =   8160
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Height          =   3675
      Left            =   60
      TabIndex        =   5
      Top             =   60
      Width           =   7035
      Begin VB.CommandButton cmdPersFisicas 
         Caption         =   "&Crear nuevo"
         Height          =   375
         Left            =   5760
         TabIndex        =   14
         Top             =   180
         Width           =   1155
      End
      Begin VB.TextBox txtObserv 
         Height          =   975
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   2
         Top             =   2640
         Width           =   6795
      End
      Begin VB.Frame Frame1 
         Caption         =   "Persona env�o Informe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1755
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   6795
         Begin VB.TextBox txtEnvio 
            Height          =   315
            Left            =   1380
            Locked          =   -1  'True
            TabIndex        =   1
            Top             =   960
            Width           =   4335
         End
         Begin VB.OptionButton optEnvio 
            Caption         =   "S�lo al paciente"
            Height          =   255
            Index           =   0
            Left            =   300
            TabIndex        =   11
            Top             =   300
            Value           =   -1  'True
            Width           =   1755
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "&Buscar"
            Height          =   375
            Left            =   5760
            TabIndex        =   10
            Top             =   900
            Width           =   915
         End
         Begin VB.OptionButton optEnvio 
            Caption         =   "Paciente y Dr./Dra. Remitente"
            Height          =   255
            Index           =   1
            Left            =   300
            TabIndex        =   9
            Top             =   660
            Width           =   2895
         End
         Begin VB.OptionButton optEnvio 
            Caption         =   "Paciente y "
            Height          =   255
            Index           =   2
            Left            =   300
            TabIndex        =   8
            Top             =   1020
            Width           =   1095
         End
         Begin VB.OptionButton optEnvio 
            Caption         =   "No requiere informe m�dico"
            Height          =   255
            Index           =   3
            Left            =   300
            TabIndex        =   7
            Top             =   1380
            Width           =   2895
         End
      End
      Begin SSDataWidgets_B.SSDBCombo cboRemitente 
         Height          =   315
         Left            =   1920
         TabIndex        =   0
         Top             =   240
         Width           =   3795
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6006
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6694
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Caption         =   "Dr./Dra. Remitente:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1755
      End
      Begin VB.Label Label1 
         Caption         =   "Observaciones:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   2400
         Width           =   2115
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   7200
      TabIndex        =   4
      Top             =   3300
      Width           =   915
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   7200
      TabIndex        =   3
      Top             =   180
      Width           =   915
   End
End
Attribute VB_Name = "frmCambioOtrosDatosPA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodAsist$, strCodProc$
Dim strCodPEnvio$

Private Sub cboRemitente_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboRemitente.Text = ""
End Sub

Private Sub cmdAceptar_Click()
    If fComprobaciones Then
        Call pGuardar
        Unload Me
    End If
End Sub

Private Sub cmdBuscar_Click()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    Call objSecurity.LaunchProcess("AD2999")
    If objPipe.PipeExist("AD2999_CI21CODPERSONA") Then
        strCodPEnvio = objPipe.PipeGet("AD2999_CI21CODPERSONA")
        Call objPipe.PipeRemove("AD2999_CI21CODPERSONA")
        Call objPipe.PipeRemove("AD2999_CI22NUMHISTORIA")
        SQL = "SELECT CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC"
        SQL = SQL & " FROM CI2200"
        SQL = SQL & " WHERE CI21CODPERSONA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodPEnvio
        Set rs = qry.OpenResultset()
        txtEnvio.Text = rs!PAC
    End If
End Sub

Private Sub cmdPersFisicas_Click()
    Dim strCodPRem$, i%
    
    If cboRemitente.Text <> "" Then strCodPRem = cboRemitente.Columns(0).Text
    
    Screen.MousePointer = vbHourglass
    Call objSecurity.LaunchProcess("CI4017")
    Screen.MousePointer = vbDefault
    
    Call pCargarRemitentes
    For i = 1 To cboRemitente.Rows
        If i = 1 Then cboRemitente.MoveFirst Else cboRemitente.MoveNext
        If cboRemitente.Columns(0).Text = strCodPRem Then
            cboRemitente.Text = cboRemitente.Columns(1).Text
            Exit For
        End If
    Next i
    If i > cboRemitente.Rows Then Call pCargarRemitenteInactivo(strCodPRem)
End Sub

Private Sub cmdSalir_Click()
    If MsgBox("�Desea Ud. salir SIN GUARDAR los cambios?", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    strCodAsist = objPipe.PipeGet("AD3003_AD01CODASISTENCI")
    Call objPipe.PipeRemove("AD3003_AD01CODASISTENCI")
    strCodProc = objPipe.PipeGet("AD3003_AD07CODPROCESO")
    Call objPipe.PipeRemove("AD3003_AD07CODPROCESO")
    Call pCargarRemitentes
    Call pCargarDatos
End Sub

Private Sub optEnvio_Click(Index As Integer)
    Select Case Index
    Case 1
        cboRemitente.BackColor = objApp.objUserColor.lngMandatory
        txtEnvio.BackColor = objApp.objUserColor.lngNormal
        txtEnvio.Text = ""
        strCodPEnvio = ""
    Case 2
        cboRemitente.BackColor = objApp.objUserColor.lngNormal
        txtEnvio.BackColor = objApp.objUserColor.lngMandatory
        On Error Resume Next
        txtEnvio.SetFocus
        On Error GoTo 0
    Case Else
        cboRemitente.BackColor = objApp.objUserColor.lngNormal
        txtEnvio.BackColor = objApp.objUserColor.lngNormal
        txtEnvio.Text = ""
        strCodPEnvio = ""
    End Select
End Sub

Private Sub txtEnvio_Change()
    If txtEnvio.Text <> "" Then optEnvio(2).Value = True
End Sub

Private Sub txtEnvio_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
    Case 46: txtEnvio.Text = "": strCodPEnvio = ""
    Case 13: txtObserv.SetFocus
    End Select
End Sub

Private Sub pGuardar()
    Dim SQL$, qry As rdoQuery, qrySIHC As rdoQuery
    
    SQL = "UPDATE AD0800"
    SQL = SQL & " SET CI21CODPERSONA_MED = ?,"
    SQL = SQL & " CI21CODPERSONA_ENV = ?,"
    SQL = SQL & " AD08INDREQIM = ?,"
    SQL = SQL & " AD08OBSERVAC = ?"
    SQL = SQL & " WHERE AD07CODPROCESO = ?"
    SQL = SQL & " AND AD01CODASISTENCI = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If cboRemitente.Text = "" Then qry(0) = Null Else qry(0) = cboRemitente.Columns(0).Text
    If optEnvio(1).Value = True Then
        qry(1) = cboRemitente.Columns(0).Text
    ElseIf optEnvio(2).Value = True Then
        qry(1) = strCodPEnvio
    Else
        qry(1) = Null
    End If
    If optEnvio(3).Value = True Then qry(2) = 0 Else qry(2) = -1
    If txtObserv.Text = "" Then qry(3) = Null Else qry(3) = txtObserv.Text
    qry(4) = strCodProc
    qry(5) = strCodAsist
    
    'sihc
    SQL = "UPDATE Asist SET ReqIm = ?"
    SQL = SQL & " WHERE AD08NumCaso IN ("
    SQL = SQL & " SELECT AD08NumCaso"
    SQL = SQL & " FROM AD0800"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?)"
    Set qrySIHC = objApp.rdoConnect.CreateQuery("", SQL)
    If optEnvio(3).Value = True Then qrySIHC(0) = 0 Else qrySIHC(0) = 1
    qrySIHC(1) = strCodAsist
    qrySIHC(2) = strCodProc
            
    'actualizaci�n de la BD
    objApp.BeginTrans
    On Error Resume Next
    qry.Execute
    qry.Close
    qrySIHC.Execute
    qrySIHC.Close
    If Err = 0 Then
        objApp.CommitTrans
    Else
        objApp.RollbackTrans
        SQL = "Se ha producido un error al realizar el cambio de los datos"
        SQL = SQL & " del Proceso/Asistencia."
        SQL = SQL & Chr$(13)
        SQL = SQL & "Vuelva a intentarlo."
        MsgBox SQL, vbExclamation, Me.Caption
    End If
    On Error GoTo 0
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
        
    fComprobaciones = True
End Function

Private Sub pCargarRemitentes()
    Dim SQL$, rs As rdoResultset
    
    cboRemitente.RemoveAll
    SQL = "SELECT CI21CODPERSONA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC"
    SQL = SQL & " FROM CI2200"
    SQL = SQL & " WHERE CI28CODRELUDN IN (" & constREL_UN_DRREMITENTE & "," & constREL_UN_DRASOCIADO & ")"
    SQL = SQL & " ORDER BY PAC"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboRemitente.AddItem rs!CI21CODPERSONA & Chr$(9) & rs!PAC
        rs.MoveNext
    Loop
    rs.Close
End Sub

Private Sub pCargarDatos()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim i%, strCodPRem$
    
    SQL = "SELECT CI21CODPERSONA_MED, NVL(AD08INDREQIM,0) AD08INDREQIM, AD08OBSERVAC,"
    SQL = SQL & " CI21CODPERSONA_ENV, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC"
    SQL = SQL & " FROM AD0800, CI2200"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?"
    SQL = SQL & " AND CI2200.CI21CODPERSONA (+)= AD0800.CI21CODPERSONA_ENV"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        If Not IsNull(rs!CI21CODPERSONA_MED) Then
            strCodPRem = rs!CI21CODPERSONA_MED
            For i = 1 To cboRemitente.Rows
                If i = 1 Then cboRemitente.MoveFirst Else cboRemitente.MoveNext
                If cboRemitente.Columns(0).Text = strCodPRem Then
                    cboRemitente.Text = cboRemitente.Columns(1).Text
                    Exit For
                End If
            Next i
            If i > cboRemitente.Rows Then Call pCargarRemitenteInactivo(strCodPRem)
        End If
        If rs!AD08INDREQIM = 0 Then
            optEnvio(3).Value = True
        Else
            If IsNull(rs!CI21CODPERSONA_ENV) Then
                optEnvio(0).Value = True
            ElseIf rs!CI21CODPERSONA_ENV = strCodPRem Then
                optEnvio(1).Value = True
            Else
                optEnvio(2).Value = True
                txtEnvio.Text = rs!PAC
            End If
        End If
        If Not IsNull(rs!AD08OBSERVAC) Then
            txtObserv.Text = rs!AD08OBSERVAC
        End If
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pCargarRemitenteInactivo(strCodPers$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT CI21CODPERSONA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC"
    SQL = SQL & " FROM CI2200"
    SQL = SQL & " WHERE CI21CODPERSONA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodPers
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        cboRemitente.Text = rs(0)
        cboRemitente.AddItem strCodPers & Chr$(9) & rs(0)
        cboRemitente.MoveLast
    End If
    rs.Close
    qry.Close
End Sub

