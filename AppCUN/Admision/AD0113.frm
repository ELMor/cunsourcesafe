VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "idperson.ocx"
Begin VB.Form frmFinAsistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Finalizaci�n de Asistencias"
   ClientHeight    =   5760
   ClientLeft      =   150
   ClientTop       =   1800
   ClientWidth     =   11610
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5760
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Index           =   0
      Left            =   90
      TabIndex        =   0
      Top             =   510
      Width           =   11445
      Begin TabDlg.SSTab tabTab1 
         Height          =   1665
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   360
         Width           =   11235
         _ExtentX        =   19817
         _ExtentY        =   2937
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Detalle"
         TabPicture(0)   =   "AD0113.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "IdPersona1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD0113.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1425
            Index           =   0
            Left            =   -74850
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   120
            Width           =   10635
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18759
            _ExtentY        =   2514
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1335
            Left            =   120
            TabIndex        =   1
            Top             =   120
            Width           =   9975
            _ExtentX        =   17595
            _ExtentY        =   2355
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Asistencias"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2685
      Index           =   1
      Left            =   90
      TabIndex        =   2
      Top             =   2730
      Width           =   11430
      Begin TabDlg.SSTab tabTab1 
         Height          =   2175
         HelpContextID   =   90001
         Index           =   1
         Left            =   180
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   3836
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Det&alle"
         TabPicture(0)   =   "AD0113.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(20)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(74)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(18)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(6)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "UpDownHora(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "UpDownMinuto(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtcDateCombo1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "cboDBCombo1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(10)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(8)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(11)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(9)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtHora(0)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtMinuto(0)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtMinuto(1)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtHora(1)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).ControlCount=   25
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD0113.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Enabled         =   0   'False
            Height          =   315
            Index           =   1
            Left            =   3180
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   31
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   270
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Enabled         =   0   'False
            Height          =   315
            HelpContextID   =   3
            Index           =   1
            Left            =   3690
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   30
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   270
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   315
            HelpContextID   =   3
            Index           =   0
            Left            =   7230
            MaxLength       =   2
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   930
            Width           =   390
         End
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   315
            Index           =   0
            Left            =   6540
            MaxLength       =   2
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   930
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   0
            Left            =   8790
            TabIndex        =   3
            Tag             =   "Persona|C�digo de Persona"
            Top             =   900
            Visible         =   0   'False
            Width           =   1245
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   9
            Left            =   120
            TabIndex        =   7
            Tag             =   "Tipo Asistencia|Tipo Asistencia"
            Top             =   930
            Width           =   2370
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD01CODASISTENCI"
            Height          =   315
            HelpContextID   =   40101
            Index           =   7
            Left            =   90
            MaxLength       =   10
            TabIndex        =   4
            Tag             =   "C�digo de Asistencia|C�digo de Asistencia"
            Top             =   270
            Width           =   1005
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   11
            Left            =   1170
            MaxLength       =   100
            TabIndex        =   15
            Tag             =   "Descripci�n Motivo fin Asistencia|Descripci�n Motivo fin Asistencia"
            Top             =   1650
            Width           =   3975
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD01DIAGNOSSALI"
            Height          =   315
            Index           =   8
            Left            =   5130
            MaxLength       =   100
            TabIndex        =   6
            Tag             =   "Diagn�stico de Salida|Diagn�stico de Salida"
            Top             =   270
            Width           =   4035
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   10
            Left            =   3030
            TabIndex        =   8
            Tag             =   "C�digo de  Cama|C�digo de Cama"
            Top             =   960
            Width           =   1140
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1935
            Index           =   1
            Left            =   -74850
            TabIndex        =   19
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   3413
            _StockProps     =   79
            ForeColor       =   0
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD27CODALTAASIST"
            Height          =   330
            HelpContextID   =   30110
            Index           =   1
            Left            =   120
            TabIndex        =   14
            Tag             =   "Motivo fin Asistencia|Motivo fin Asistencia"
            Top             =   1650
            Width           =   945
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldDelimiter  =   """"
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).Picture=   "AD0113.frx":0070
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD0113.frx":008C
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1296
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).Alignment=   1
            Columns(0).CaptionAlignment=   1
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5556
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1667
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD01FECINICIO"
            Height          =   315
            Index           =   1
            Left            =   1170
            TabIndex        =   5
            Tag             =   "Fecha de Inicio|Fecha de Inicio "
            Top             =   270
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD01FECFIN"
            Height          =   315
            Index           =   2
            Left            =   4530
            TabIndex        =   9
            Tag             =   "Fecha de Fin|Fecha de Fin"
            Top             =   930
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   65535
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin ComCtl2.UpDown UpDownMinuto 
            Height          =   315
            Index           =   0
            Left            =   7620
            TabIndex        =   13
            Top             =   930
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            BuddyControl    =   "txtMinuto(0)"
            BuddyDispid     =   196611
            BuddyIndex      =   0
            OrigLeft        =   6360
            OrigTop         =   360
            OrigRight       =   6600
            OrigBottom      =   690
            Increment       =   5
            Max             =   55
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   0   'False
         End
         Begin ComCtl2.UpDown UpDownHora 
            Height          =   315
            Index           =   0
            Left            =   6930
            TabIndex        =   11
            Top             =   930
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            BuddyControl    =   "txtHora(0)"
            BuddyDispid     =   196610
            BuddyIndex      =   0
            OrigLeft        =   5760
            OrigTop         =   360
            OrigRight       =   6000
            OrigBottom      =   690
            Max             =   23
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   0   'False
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   3600
            TabIndex        =   33
            Top             =   330
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3180
            TabIndex        =   32
            Top             =   60
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   6540
            TabIndex        =   29
            Top             =   690
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   4560
            TabIndex        =   26
            Top             =   690
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Asistencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   90
            TabIndex        =   25
            Top             =   720
            Width           =   1320
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Asistencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   74
            Left            =   90
            TabIndex        =   24
            Top             =   60
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Motivo del Fin de la Asistencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   23
            Top             =   1410
            Width           =   2625
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Diagn�stico de Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   5160
            TabIndex        =   22
            Top             =   60
            Width           =   1875
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1230
            TabIndex        =   21
            Top             =   60
            Width           =   1305
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3030
            TabIndex        =   20
            Top             =   690
            Width           =   750
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   16
      Top             =   5475
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFinAsistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const iEstado_OcupaLibreHoy = 10    '10 Ocupada Libre Hoy
'Constante para estados de las pruebas
Const iPrue_Realizada = 4

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim QyInsert As rdoQuery

Private mCodPersona As Long
Private mCodAsistencia As Long

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Si se ha llamado la form desde la form de asociaci�n proceso/asistencia,
  'extrae los par�metros de las pipes
  If objPipe.PipeExist("CODPERSONA_FROM_AD1020") Then
    mCodPersona = objPipe.PipeGet("CODPERSONA_FROM_AD1020")
    objPipe.PipeRemove ("CODPERSONA_FROM_AD1020")
  End If
  If objPipe.PipeExist("CODASISTENCIA_FROM_AD1020") Then
    mCodAsistencia = objPipe.PipeGet("CODASISTENCIA_FROM_AD1020")
    objPipe.PipeRemove ("CODASISTENCIA_FROM_AD1020")
  End If
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  Set objWinInfo = New clsCWWin
  
  
  If mCodPersona <> 0 Then
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  Else
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  End If
  
  With objWinInfo.objDoc
    .cwPRJ = "ADMISION"
    .cwMOD = "M�dulo Maestro-Detalle"
    .cwAUT = "A.R."
    .cwDAT = "08-10-97"
    .cwDES = "Mantenimiento Finalizaci�n Asistencia"
    .cwUPD = "08-10-97  - A.R. - xxxxxxxx"
    .cwEVT = "Descripci�n del evento xxxxx"
  End With
  
  With objMasterInfo
    .strName = "Pacientes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "CI2200"
    .strWhere = " CI2200.AD34CODESTADO = 1 "
    If mCodPersona <> 0 Then
      .strWhere = .strWhere + "AND CI2200.CI21CODPERSONA = " & mCodPersona
    End If
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
  End With
  
  With objDetailInfo
    .strName = "Asistencia Paciente"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "AD0100"
    Call .FormAddOrderField("AD01CODASISTENCI", cwAscending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)              'nuevo
    .intAllowance = cwAllowModify
    .strWhere = " (AD01FECFIN IS NULL) " & " AND AD0100.AD34CODESTADO = 1 " _
    & " AND AD0100.AD01FECINICIO >= TO_DATE('01/01/1999','DD/MM/YYYY')" _
    & " AND AD0100.AD01FECINICIO <= TRUNC(SYSDATE+1)"
    If mCodAsistencia <> 0 Then
      .strWhere = .strWhere + "AND AD0100.AD01CODASISTENCI = " & mCodAsistencia
    End If
    
    'Call .FormCreateFilterWhere("AD0100", "Tabla de Asistencia del Paciente")
    'Call .FormAddFilterWhere("AD0100", "AD01CODASISTENCI", "C�digo Asistencia", cwNumeric)
    'Call .FormAddFilterOrder("AD0100", "AD01CODASISTENCI", "C�digo Situaci�n Asistencia")
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(9)).blnReadOnly = True
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    
    .CtrlGetInfo(cboDBCombo1(1)).blnMandatory = True
'    .CtrlGetInfo(txtText1(8)).blnMandatory = True

    'ASISTENCIAS DEl PACIENTE
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD27CODALTAASIST, AD27DESALTAASIST FROM " & "AD2700 ORDER BY AD27CODALTAASIST"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(1)), "AD27CODALTAASIST", "SELECT AD27CODALTAASIST, AD27DESALTAASIST FROM AD2700 WHERE AD27CODALTAASIST = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(1)), txtText1(11), "AD27DESALTAASIST")

    .CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    .CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo
    IdPersona1.ReadPersona                     'nuevo

    Call .WinRegister
    Call .WinStabilize
  End With
  
  'Para que idperson no este visible
  IdPersona1.blnSearchButton = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Select Case strCtrl
    Case "IdPersona1"
      IdPersona1.SearchPersona
  End Select
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName = "Pacientes" Then
    txtHora(0).Enabled = False
    txtMinuto(0).Enabled = False
    UpDownHora(0).Enabled = False
    UpDownMinuto(0).Enabled = False
  Else
    If dtcDateCombo1(2) <> "" Then
      txtHora(0).Enabled = True
      txtMinuto(0).Enabled = True
      UpDownHora(0).Enabled = True
      UpDownMinuto(0).Enabled = True
    End If
  End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  'If strFormName = "Asistencia Paciente" Then
    'If objWinInfo.intWinStatus = cwModeSingleAddKey Then
      'Call objWinInfo.CtrlSet(txtText1(0), IdPersona1.Text) 'nuevo
    'End If
    'objWinInfo.DataRefresh
  'End If
  'objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD27CODALTAASIST, AD27DESALTAASIST FROM AD2700 WHERE AD27FECFIN IS NULL ORDER BY AD27CODALTAASIST"
  'Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
End Sub



Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

  Dim rdoConsulta As rdoQuery
  
  If strFormName = "Asistencia Paciente" Then
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      If txtText1(7).Text <> "" Then

        Dim rs As rdoResultset
        Dim strSQL As String
        'Aqu� lo pone a 00:00 AJO
'Modificamos : Realizamos la select de la tabla ad2500 para recuperar la hora y minutos
'de la fecha de inicio.
        strSQL = "SELECT TO_CHAR(AD25FECINICIO, 'HH24'), "
        strSQL = strSQL & " TO_CHAR(AD25FECINICIO, 'MI') "
        strSQL = strSQL & " FROM AD2500 "
        strSQL = strSQL & " WHERE AD01CODASISTENCI = ? "
        strSQL = strSQL & " AND TRUNC(AD25FECINICIO) = TO_DATE(?, 'DD/MM/YYYY') "
        
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = txtText1(7).Text
    rdoConsulta(1) = Format$(dtcDateCombo1(1).Date, "dd/mm/yyyy")
    Set rs = rdoConsulta.OpenResultset(strSQL)
        If Not rs.EOF Then
            txtHora(1).Text = Format(rs.rdoColumns(0), "00")
            txtMinuto(1).Text = Format(rs.rdoColumns(1).Value, "00")
        End If
        rs.Close
        Set rs = Nothing
        
        'txtHora(1).Text = Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECINICIO, "hh:mm"), 2)
        'txtMinuto(1).Text = Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECINICIO, "hh:mm"), 2)
        ' Visualizar Tipo de Asistencia
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'        strSQL = "SELECT AD12DESTIPOASIST FROM"
'        strSQL = strSQL & " AD2500,AD1200,AD0100 WHERE "
'        strSQL = strSQL & " AD2500.AD01CODASISTENCI = " & txtText1(7).Text
'        strSQL = strSQL & " AND AD0100.AD01CODASISTENCI = " & txtText1(7).Text
'        strSQL = strSQL & " AND (AD25FECFIN IS NULL OR "
'        strSQL = strSQL & "TO_CHAR(AD25FECFIN ,'DD/MM/YYYY HH24:MI:SS') = "
'        strSQL = strSQL & "TO_CHAR(AD01FECFIN ,'DD/MM/YYYY HH24:MI:SS'))"
'        strSQL = strSQL & " AND AD1200.AD12CODTIPOASIST = AD2500.AD12CODTIPOASIST" & " AND AD0100.AD34CODESTADO = 1 "
'        Set rs = objApp.rdoConnect.OpenResultset(strSQL, rdOpenDynamic)
        strSQL = "SELECT AD12DESTIPOASIST FROM"
        strSQL = strSQL & " AD2500,AD1200,AD0100 WHERE "
        strSQL = strSQL & " AD2500.AD01CODASISTENCI =? "
        strSQL = strSQL & " AND AD0100.AD01CODASISTENCI =? "
        strSQL = strSQL & " AND (AD25FECFIN IS NULL OR "
        strSQL = strSQL & "TO_CHAR(AD25FECFIN ,'DD/MM/YYYY HH24:MI:SS') = "
        strSQL = strSQL & "TO_CHAR(AD01FECFIN ,'DD/MM/YYYY HH24:MI:SS'))"
        strSQL = strSQL & " AND AD1200.AD12CODTIPOASIST = AD2500.AD12CODTIPOASIST AND AD0100.AD34CODESTADO = 1 "
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = txtText1(7).Text
    rdoConsulta(1) = txtText1(7).Text
    Set rs = rdoConsulta.OpenResultset(strSQL)
        If Not rs.EOF Then
          Call objWinInfo.CtrlSet(txtText1(9), rs.rdoColumns(0).Value)
        End If
        rs.Close
        Set rs = Nothing
        ' Visualizar cama de La Asistencia ( Cama Ocupada por Paciente= Estado(2))
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'        strSQL = "SELECT AD15CODCAMA FROM AD1500"
'        strSQL = strSQL & " WHERE AD01CODASISTENCI ="
'        strSQL = strSQL & txtText1(7)
'        strSQL = strSQL & " AND AD14CODESTCAMA = '2'"
'        Set rs = objApp.rdoConnect.OpenResultset(strSQL, rdOpenDynamic)
        strSQL = "SELECT AD15CODCAMA FROM AD1500"
        strSQL = strSQL & " WHERE AD01CODASISTENCI =?"
        strSQL = strSQL & " AND AD14CODESTCAMA = '2'"
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = txtText1(7)
    Set rs = rdoConsulta.OpenResultset(strSQL)
        If Not rs.EOF Then
          Call objWinInfo.CtrlSet(txtText1(10), rs.rdoColumns(0).Value)
        End If
        rs.Close
        Set rs = Nothing
        If dtcDateCombo1(2).Date <> "" Then
          txtHora(0).Text = Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECFIN, "hh:mm"), 2)
          txtMinuto(0).Text = Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECFIN, "hh:mm"), 2)
          txtHora(0).Enabled = True
          txtMinuto(0).Enabled = True
          UpDownHora(0).Enabled = True
          UpDownMinuto(0).Enabled = True
        Else
          txtHora(0).Text = ""
          txtMinuto(0).Text = ""
          txtHora(0).Enabled = False
          txtMinuto(0).Enabled = False
          UpDownHora(0).Enabled = False
          UpDownMinuto(0).Enabled = False
        End If
      Else
        txtHora(0).Text = ""
        txtHora(0).Enabled = False
        txtMinuto(0).Text = ""
        txtMinuto(0).Enabled = False
        UpDownHora(0).Enabled = False
        UpDownMinuto(0).Enabled = False
      End If
    End If
  Else
  End If
  
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim strSQL As String
Dim qryUpdate As rdoQuery
Dim rs As rdoResultset
Dim vntData(2)

 ' LMF 11/01/2000 poner fecha fin a los procesos/asistencias de una al dar
 '                fin asistencia a una asistencia determinada
If strFormName = "Asistencia Paciente" Then
  If dtcDateCombo1(2) <> "" Then ' tiene fecha fin
     strSQL = "UPDATE AD0800 SET AD08FECFIN = "
     strSQL = strSQL & "TO_DATE(?,'DD/MM/YYYY, HH24:MI:SS')"
     strSQL = strSQL & " WHERE AD01CODASISTENCI = ? "
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", strSQL)
    qryUpdate(0) = Format(dtcDateCombo1(2), "dd/mm/yyyy hh:mm:ss")
    qryUpdate(1) = txtText1(7).Text
    qryUpdate.Execute
    qryUpdate.Close
  End If
End If

'
End Sub



Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)

If strFormName = "Asistencia Paciente" Then
  If objWinInfo.intWinStatus <> cwModeSingleEmpty Then
    If Not objWinInfo.cllWinForms(2).rdoCursor.EOF Then
      If Not IsNull(objWinInfo.objWinActiveForm.rdoCursor!AD01FECFIN) Then
        Call objWinInfo.CtrlSet(dtcDateCombo1(2), Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECFIN, "DD/MM/YY"))
      Else
        Call objWinInfo.CtrlSet(dtcDateCombo1(2), "")
      End If
    End If
  End If
'  objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD27CODALTAASIST, AD27DESALTAASIST FROM " & "AD2700 ORDER BY AD27CODALTAASIST"
'  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim strSQL As String
  Dim rdoConsulta As rdoQuery
  Dim vntA As Variant
  Dim rs As rdoResultset
  Dim str As String
  Dim strFecha_Sistema As String

  str = "SELECT TO_CHAR (SYSDATE,'DD/MM/YYYY') FROM DUAL"
  Set rs = objApp.rdoConnect.OpenResultset(str, rdOpenKeyset)
  strFecha_Sistema = rs.rdoColumns(0).Value
  rs.Close
  Set rs = Nothing
  str = ""
  
  If strFormName = "Asistencia Paciente" Then
    If objWinInfo.intWinStatus <> cwModeSingleOpen Then
      If dtcDateCombo1(2).Date <> "" Then
        ' LMF 11/01/2000 evitar dar altas apacientes con cama en este formulario
        If txtText1(10).Text <> "" Then ' asistencia con cama
          MsgBox "En esta ventana no se pueden dar altas a pacientes hospitalizados"
          blnCancel = True
          objWinInfo.DataRefresh
          Exit Sub
        End If

        If dtcDateCombo1(1).Date <> "" Then
          If CDate(Format(dtcDateCombo1(2) & " " & txtHora(0) & ":" & txtMinuto(0), "dd/mm/yyyy hh:mm")) < _
          CDate(Format(dtcDateCombo1(1) & " " & txtHora(1) & ":" & txtMinuto(1), "dd/mm/yyyy hh:mm")) Then
            Call objError.SetError(cwCodeMsg, "Fecha Fin Asistencia Inferior a Fecha Inicio Asistencia", vntA)
            vntA = objError.Raise
            blnCancel = True
            Exit Sub
          End If
        End If
        ' Comprobar que la fecha Fin Asistencia sea igual o mayor
        ' que la fecha inicio del Tipo de la asistencia
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'        Set rs = objApp.rdoConnect.OpenResultset("SELECT AD25FECINICIO FROM AD2500 WHERE" & _
'                                                    " AD01CODASISTENCI = " & txtText1(7) & _
'                                                    " AND AD25FECFIN IS NULL", rdOpenDynamic)
        strSQL = "SELECT AD25FECINICIO FROM AD2500 WHERE" & _
                                                    " AD01CODASISTENCI =? " & _
                                                    " AND AD25FECFIN IS NULL"
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = txtText1(7)
    Set rs = rdoConsulta.OpenResultset(strSQL)
    
        If Not rs.EOF Then
          If CDate(Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")) < CDate(Format(rs!AD25FECINICIO, "DD/MM/YYYY")) Then
            Call objError.SetError(cwCodeMsg, "Fecha Fin Asistencia Inferior a Fecha Inicio del Tipo Asistencia actual. C�mbiela.", vntA)
            vntA = objError.Raise
            blnCancel = True
            rs.Close
            Set rs = Nothing
            Exit Sub
          End If
        End If
        rs.Close
        Set rs = Nothing
      End If
    End If
    If cboDBCombo1(1).Columns(2).Value <> "" Then
      Call objError.SetError(cwCodeMsg, "Este Motivo de Fin de Asistencia est� dado de baja. Elija otro.", vntA)
      vntA = objError.Raise
      blnCancel = True
      Exit Sub
    End If
    If dtcDateCombo1(2).Date <> "" Then
      If dtcDateCombo1(1).Date <> "" Then
        If CDate(Format(dtcDateCombo1(1).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")) Then
          Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
          vntA = objError.Raise
          blnCancel = True
          Exit Sub
        End If
      End If
    End If
    
  If dtcDateCombo1(2).Date <> "" Then
    If CDate(Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")) > CDate(Format(strFecha_Sistema, "DD/MM/YYYY")) Then
      Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser mayor a la Fecha del Sistema.", vntA)
      vntA = objError.Raise
      blnCancel = True
    Exit Sub
  End If
 End If

    str = "Paciente:" & IdPersona1.Apellido1 & ", " & IdPersona1.Apellido2
    str = str & ", " & IdPersona1.Nombre & Chr$(13) & Chr$(10)
    str = str & "�Est� seguro de que desea finalizar esta asistencia?"
    Call objError.SetError(cwCodeQuery, str, vntA)
    vntA = objError.Raise
    If vntA = 7 Then
      blnCancel = True
      Exit Sub
    End If
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'    Set rs = objApp.rdoConnect.OpenResultset("SELECT PR0100.PR01DESCORTA FROM PR0100,PR0400" & _
'                  " WHERE PR0100.PR01CODACTUACION=PR0400.PR01CODACTUACION" & _
'                  " AND AD01CODASISTENCI = " & txtText1(7) & _
'                  " AND (PR37CODESTADO=1 OR PR37CODESTADO=2)", rdOpenDynamic)
    strSQL = "SELECT PR0100.PR01DESCORTA FROM PR0100,PR0400" & _
                  " WHERE PR0100.PR01CODACTUACION=PR0400.PR01CODACTUACION" & _
                  " AND AD01CODASISTENCI =? " & _
                  " AND (PR37CODESTADO=1 OR PR37CODESTADO=2)"
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoConsulta(0) = txtText1(7)
    Set rs = rdoConsulta.OpenResultset(strSQL)
    
    If Not rs.EOF Then
      str = "Existen actuaciones pendientes en esta asistencia: "
      Do While Not rs.EOF
        str = str & rs!PR01DESCORTA & ", "
        rs.MoveNext
      Loop
      str = str & "�Desea continuar?"
      Call objError.SetError(cwCodeQuery, str, vntA)
      vntA = objError.Raise
      If vntA = 7 Then
        blnCancel = True
      End If
      
    End If
  End If

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strFecha As String
  If strFormName = "Asistencia Paciente" Then
    If dtcDateCombo1(2).Date <> "" Then
      strFecha = Format(dtcDateCombo1(2).Date, "dd/mm/yyyy") & " " & Right("0" & txtHora(0).Text, 2) & ":" & Right("0" & txtMinuto(0).Text, 2) & ":00"
      objWinInfo.objWinActiveForm.rdoCursor!AD01FECFIN = strFecha
    End If
  End If
  'Call objWinInfo.CtrlSet(txtText1(0), IdPersona1.Text) 'nuevo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
If btnButton.Index = 16 Then
    Call objWinInfo.FormChangeActive(tabTab1(0), False, True)
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If dtcDateCombo1(2).Date = "" Then
  Else
    Dim strHora As String
    
    If txtHora(0).Text = "" Then
      strHora = strHora_Sistema
      Call objWinInfo.CtrlSet(txtHora(0), Left(Format(strHora, "hh:mm"), 2))
      Call objWinInfo.CtrlSet(txtMinuto(0), Right(Format(strHora, "hh:mm"), 2))
      UpDownHora(0).Value = txtHora(0).Text
      If Val(txtMinuto(0).Text) < 55 Then
        UpDownMinuto(0).Value = txtMinuto(0).Text
      Else
        UpDownMinuto(0).Value = 55
      End If
    End If
  End If
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  'txtHora(0).Text = "00"
  'txtMinuto(0).Text = "00"
  If dtcDateCombo1(2).Date = "" Then
  '  txtHora(0).Enabled = False
  '  txtMinuto(0).Enabled = False
  '  UpDownHora(0).Enabled = False
  '  UpDownMinuto(0).Enabled = False
  Else
      Dim strHora As String
    If txtHora(0).Text = "" Then
      strHora = strHora_Sistema
      Call objWinInfo.CtrlSet(txtHora(0), Left(Format(strHora, "hh:mm"), 2))
      Call objWinInfo.CtrlSet(txtMinuto(0), Right(Format(strHora, "hh:mm"), 2))
    End If
    UpDownHora(0).Value = txtHora(0).Text
    If Val(txtMinuto(0).Text) < 55 Then
      UpDownMinuto(0).Value = txtMinuto(0).Text
    Else
      UpDownMinuto(0).Value = 55
    End If
    txtHora(0).Enabled = True
    txtMinuto(0).Enabled = True
    UpDownHora(0).Enabled = True
    UpDownMinuto(0).Enabled = True
  End If
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If dtcDateCombo1(2).Text = "" Then
    txtHora(0) = ""
    txtMinuto(0) = ""
    txtHora(0).Enabled = False
    txtMinuto(0).Enabled = False
    UpDownHora(0).Enabled = False
    UpDownMinuto(0).Enabled = False
  Else
    txtHora(0).Enabled = True
    txtMinuto(0).Enabled = True
    UpDownHora(0).Enabled = True
    UpDownMinuto(0).Enabled = True
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If cboDBCombo1(intIndex).Columns(2).Value = "" Then
    Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
    Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
  Else
    Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
    Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
  End If
End Sub

Private Sub txtHora_Change(Index As Integer)
'  txtHora(0).Text = Right("0" & txtHora(0).Text, 2)
End Sub

Private Sub txtHora_KeyPress(Index As Integer, KeyAscii As Integer)
'  KeyAscii = 0
End Sub

Private Sub txtMinuto_Change(Index As Integer)
'  txtMinuto(0).Text = Right("0" & txtMinuto(0).Text, 2)
End Sub

Private Sub txtMinuto_KeyPress(Index As Integer, KeyAscii As Integer)
'  KeyAscii = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
  'If IdPersona1.Text <> "" Then
    'objWinInfo.cllWinForms(2).strWhere = " AD01FECFIN=(SELECT MAX(AD01FECFIN) FROM AD0100" & _
    '                                     " WHERE CI21CODPERSONA=" & IdPersona1.Text & ")" & _
    '                                     " OR AD01FECFIN IS NULL"
    'objWinInfo.DataRefresh
  'Else
    'objWinInfo.cllWinForms(2).strWhere = ""
    'objWinInfo.DataRefresh
  'End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de UpDown
' -----------------------------------------------
Private Sub UpDownHora_DownClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownHora_UpClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownMinuto_DownClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub

Private Sub UpDownMinuto_UpClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub


