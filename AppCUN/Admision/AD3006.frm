VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmMantDatosEconPA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Responsables Econ�micos"
   ClientHeight    =   6840
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9645
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   9645
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEliminar 
      Caption         =   "&Eliminar"
      Height          =   375
      Left            =   8640
      TabIndex        =   18
      Top             =   1860
      Width           =   915
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Height          =   375
      Left            =   8640
      TabIndex        =   17
      Top             =   1320
      Width           =   915
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   8640
      TabIndex        =   15
      Top             =   120
      Width           =   915
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   8640
      TabIndex        =   14
      Top             =   780
      Width           =   915
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Econ�micos"
      ForeColor       =   &H00FF0000&
      Height          =   2895
      Left            =   120
      TabIndex        =   8
      Top             =   660
      Width           =   8415
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Otra persona"
         Height          =   375
         Left            =   7080
         TabIndex        =   10
         Top             =   1860
         Width           =   1095
      End
      Begin VB.CheckBox chkPendVol 
         Alignment       =   1  'Right Justify
         Caption         =   "Pendiente volante"
         Height          =   195
         Left            =   300
         TabIndex        =   9
         Top             =   2460
         Width           =   1635
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipoEcon 
         Height          =   315
         Left            =   1740
         TabIndex        =   4
         Top             =   840
         Width           =   3315
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboEntidad 
         Height          =   315
         Left            =   1740
         TabIndex        =   5
         Top             =   1380
         Width           =   4275
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7408
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7541
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboRespEcon 
         Height          =   315
         Left            =   1740
         TabIndex        =   6
         Top             =   1920
         Width           =   5295
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8811
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   9340
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecIni 
         Height          =   315
         Left            =   1740
         TabIndex        =   0
         Top             =   360
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecFin 
         Height          =   315
         Left            =   5520
         TabIndex        =   2
         Top             =   360
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin MSMask.MaskEdBox mskHoraIni 
         Height          =   315
         Left            =   3480
         TabIndex        =   1
         Top             =   360
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   556
         _Version        =   327681
         BackColor       =   16776960
         MaxLength       =   5
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskHoraFin 
         Height          =   315
         Left            =   7260
         TabIndex        =   3
         Top             =   360
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   556
         _Version        =   327681
         BackColor       =   16777215
         MaxLength       =   5
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha Fin:"
         Height          =   195
         Index           =   4
         Left            =   4500
         TabIndex        =   20
         Top             =   420
         Width           =   975
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha Inicio:"
         Height          =   195
         Index           =   3
         Left            =   420
         TabIndex        =   19
         Top             =   420
         Width           =   1275
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Resp. Econ�mico:"
         Height          =   195
         Index           =   2
         Left            =   300
         TabIndex        =   13
         Top             =   1980
         Width           =   1395
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Entidad:"
         Height          =   195
         Index           =   1
         Left            =   720
         TabIndex        =   12
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo Econ�mico:"
         Height          =   195
         Index           =   0
         Left            =   420
         TabIndex        =   11
         Top             =   900
         Width           =   1275
      End
   End
   Begin VB.CommandButton cmdBuscarResp 
      Caption         =   "&Buscar Entidad Responsable"
      Height          =   435
      Left            =   3420
      TabIndex        =   7
      Top             =   120
      Width           =   2355
   End
   Begin SSDataWidgets_B.SSDBGrid grdRespEconPA 
      Height          =   3135
      Left            =   120
      TabIndex        =   16
      Top             =   3600
      Width           =   9405
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   16589
      _ExtentY        =   5530
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmMantDatosEconPA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strNH$, strCodAsist$, strCodProc$, strAhora$
'Dim blnAcunsa As Boolean, blnEntColab As Boolean

Private Sub cboEntidad_Click()
    If cboTipoEcon.Columns(0).Text <> constTE_PRIVADO Then
        cboRespEcon.RemoveAll: cboRespEcon.Text = ""
        Call pCargarRespEcon
    End If
End Sub

Private Sub cboEntidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboRespEcon_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboTipoEcon_Click()
    cboEntidad.RemoveAll: cboEntidad.Text = ""
    cboRespEcon.RemoveAll: cboRespEcon.Text = ""
    Call pCargarEntidades
    If cboTipoEcon.Columns(0).Text = constTE_PRIVADO Then Call pCargarRespEcon
End Sub

Private Sub cboTipoEcon_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdBuscar_Click()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strCodPers$
    
    Call objSecurity.LaunchProcess("AD2999")
    If objPipe.PipeExist("AD2999_CI21CODPERSONA") Then
        strCodPers = objPipe.PipeGet("AD2999_CI21CODPERSONA")
        Call objPipe.PipeRemove("AD2999_CI21CODPERSONA")
        Call objPipe.PipeRemove("AD2999_CI22NUMHISTORIA")
        cboRespEcon.RemoveAll
        SQL = "SELECT CI21CODPERSONA CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA) RECO_DES"
        SQL = SQL & " FROM CI2200"
        SQL = SQL & " WHERE CI21CODPERSONA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodPers
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            cboRespEcon.AddItem rs!CI21CODPERSONA_REC & Chr$(9) & rs!CI21CODPERSONA_REC & " - " & rs!RECO_DES
            rs.MoveNext
        Loop
        rs.Close
        qry.Close
        If cboRespEcon.Rows = 1 Then
            cboRespEcon.Text = cboRespEcon.Columns(1).Text
        Else
            cboRespEcon.Text = ""
        End If
    End If
End Sub

Private Sub cmdBuscarResp_Click()
    frmBuscarTipoEco.Show vbModal
    Set frmBuscarTipoEco = Nothing
    If objPipe.PipeExist("CODRECO") Then
        Call pCargarDatos(objPipe.PipeGet("TIPOECO"), objPipe.PipeGet("ENTIDAD"), _
                    objPipe.PipeGet("CODRECO"), False)
        Call objPipe.PipeRemove("DESRECO")
        Call objPipe.PipeRemove("TIPOECO")
        Call objPipe.PipeRemove("ENTIDAD")
        Call objPipe.PipeRemove("CODRECO")
    End If
End Sub

Private Sub cmdEliminar_Click()
    Call pEliminar
End Sub

Private Sub cmdGuardar_Click()
    Call pGuardar
End Sub

Private Sub cmdNuevo_Click()
    Call pLimpiar
End Sub

Private Sub cmdSalir_Click()
    If fComprobarAlSalir Then Unload Me
End Sub

Private Sub dcboFecFin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then dcboFecFin.Date = "": mskHoraFin.Text = "__:__"
End Sub

Private Sub dcboFecIni_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then dcboFecIni.Date = "": mskHoraIni.Text = "__:__"
End Sub

Private Sub Form_Load()
    Call pFormatearControles
    Call pObtenerDatos
    Call pCargarGrid
    Call pCargarTiposEcon
End Sub

Private Sub pCargarTiposEcon()
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT CI32CODTIPECON, CI32DESTIPECON"
    SQL = SQL & " FROM CI3200"
    SQL = SQL & " WHERE CI32FECFIVGTEC IS NULL"
''    If Not blnAcunsa Then SQL = SQL & " AND CI32CODTIPECON <> '" & constTE_ACUNSA & "'"
''    If Not blnEntColab Then SQL = SQL & " AND CI32CODTIPECON <> '" & constTE_ENTCOLAB & "'"
    SQL = SQL & " ORDER BY CI32CODTIPECON"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipoEcon.AddItem rs!CI32CODTIPECON & Chr$(9) & rs!CI32CODTIPECON & " - " & rs!CI32DESTIPECON
        rs.MoveNext
    Loop
    rs.Close
End Sub
    
Private Sub pCargarEntidades()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strTE$
    
    strTE = cboTipoEcon.Columns(0).Text
    
    SQL = "SELECT CI13CODENTIDAD, CI13DESENTIDAD"
    SQL = SQL & " FROM CI1300"
    SQL = SQL & " WHERE CI13FECFIVGENT IS NULL"
    SQL = SQL & " AND CI32CODTIPECON = ?"
''    If strTE = constTE_PRIVADO Then
''        If Not blnAcunsa Then SQL = SQL & " AND CI13CODENTIDAD NOT LIKE('J%')"
''        If Not blnEntColab Then SQL = SQL & " AND CI13CODENTIDAD NOT LIKE('U%')"
''    End If
    SQL = SQL & " ORDER BY CI13CODENTIDAD"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strTE
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEntidad.AddItem rs!CI13CODENTIDAD & Chr$(9) & rs!CI13CODENTIDAD & " - " & rs!CI13DESENTIDAD
        rs.MoveNext
    Loop
    rs.Close
    If cboEntidad.Rows = 1 Then
        cboEntidad.Text = cboEntidad.Columns(1).Text
        cboEntidad_Click
    End If
End Sub

Private Sub pCargarRespEcon(Optional strCodResp$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strTE$
    
    strTE = cboTipoEcon.Columns(0).Text
    
    Select Case strTE
    Case constTE_PRIVADO
        If strCodResp <> "" Then
            SQL = "SELECT CI21CODPERSONA CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA) RECO_DES"
            SQL = SQL & " FROM CI2100"
            SQL = SQL & " WHERE CI21CODPERSONA = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = strCodResp
        Else
            SQL = "SELECT CI21CODPERSONA CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA) RECO_DES"
            SQL = SQL & " FROM CI2200"
            SQL = SQL & " WHERE CI22NUMHISTORIA = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = strNH
        End If
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            cboRespEcon.AddItem rs!CI21CODPERSONA_REC & Chr$(9) & rs!CI21CODPERSONA_REC & " - " & rs!RECO_DES
            rs.MoveNext
        Loop
        rs.Close
        qry.Close
    Case Else
        If cboEntidad.Text <> "" Then
            SQL = "SELECT CI21CODPERSONA_REC, RECO_DES"
            SQL = SQL & " FROM CI2901J"
            SQL = SQL & " WHERE CI32CODTIPECON = ?"
            SQL = SQL & " AND CI13CODENTIDAD = ?"
            SQL = SQL & " ORDER BY RECO_DES"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = cboTipoEcon.Columns(0).Text
            qry(1) = cboEntidad.Columns(0).Text
            Set rs = qry.OpenResultset()
            Do While Not rs.EOF
                cboRespEcon.AddItem rs!CI21CODPERSONA_REC _
                            & Chr$(9) & rs!CI21CODPERSONA_REC & " - " & rs!RECO_DES
                rs.MoveNext
            Loop
            rs.Close
            qry.Close
        End If
    End Select
    
    If cboRespEcon.Rows = 1 Then
        cboRespEcon.Text = cboRespEcon.Columns(1).Text
    Else
        cboRespEcon.Text = ""
    End If
End Sub

Private Sub pFormatearControles()
    
    cboTipoEcon.BackColor = objApp.objUserColor.lngMandatory
    cboEntidad.BackColor = objApp.objUserColor.lngMandatory
    cboRespEcon.BackColor = objApp.objUserColor.lngMandatory

    With grdRespEconPA
        .Columns(0).Caption = "T"
        .Columns(0).Width = 500
        .Columns(1).Caption = "E"
        .Columns(1).Width = 500
        .Columns(2).Caption = "Cod. Resp."
        .Columns(2).Width = 1000
        .Columns(3).Caption = "Responsable"
        .Columns(3).Width = 2800
        .Columns(4).Caption = "Fecha Inicio" 'dd/mm/yyyy hh:mm:ss
        .Columns(4).Width = 1450
        .Columns(5).Caption = "Fecha Fin" 'dd/mm/yyyy hh:mm:ss
        .Columns(5).Width = 1450
        .Columns(6).Caption = "Volante"
        .Columns(6).Width = 800
        .Columns(6).Alignment = ssCaptionAlignmentCenter
        .Columns(7).Caption = "FecIni" 'dd/mm/yyyy hh:mm:ss
        .Columns(7).Visible = False

        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Function fComprobarAlGuardar() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    
    'fecha fin
    If (dcboFecFin.Text = "" And mskHoraFin.Text <> "__:__") _
    Or (dcboFecFin.Text <> "" And mskHoraFin.Text = "__:__") Then
        msg = "La fecha-hora de fin no es correcta."
        MsgBox msg, vbExclamation, Me.Caption
        Exit Function
    End If
    
    fComprobarAlGuardar = True
End Function

Private Sub pObtenerDatos()
    strNH = objPipe.PipeGet("AD3006_CI22NUMHISTORIA")
    strCodAsist = objPipe.PipeGet("AD3006_AD01CODASISTENCI")
    strCodProc = objPipe.PipeGet("AD3006_AD07CODPROCESO")
    Call objPipe.PipeRemove("AD3006_CI22NUMHISTORIA")
    Call objPipe.PipeRemove("AD3006_AD01CODASISTENCI")
    Call objPipe.PipeRemove("AD3006_AD07CODPROCESO")
    
    strAhora = strFechaHora_Sistema
    
''    'se mira si el paciente pertenece a Acunsa y/o a la Entidad Colaboradora
''    blnAcunsa = fTEAcunsa(strNH)
''    blnEntColab = fTEEntColab(strNH)
End Sub

Private Sub pCargarDatos(strTipoEcon$, strEntidad$, strCodResp$, blnOtrosDatos As Boolean)
    Dim i%
    For i = 1 To cboTipoEcon.Rows
        If i = 1 Then cboTipoEcon.MoveFirst Else cboTipoEcon.MoveNext
        If cboTipoEcon.Columns(0).Text = strTipoEcon Then
            cboTipoEcon.Text = cboTipoEcon.Columns(1).Text
            Call pCargarEntidades
            Exit For
        End If
    Next i
    For i = 1 To cboEntidad.Rows
        If i = 1 Then cboEntidad.MoveFirst Else cboEntidad.MoveNext
        If cboEntidad.Columns(0).Text = strEntidad Then
            cboEntidad.Text = cboEntidad.Columns(1).Text
            Call pCargarRespEcon(strCodResp)
            Exit For
        End If
    Next i
    For i = 1 To cboRespEcon.Rows
        If i = 1 Then cboRespEcon.MoveFirst Else cboRespEcon.MoveNext
        If cboRespEcon.Columns(0).Text = strCodResp Then
            cboRespEcon.Text = cboRespEcon.Columns(1).Text
            Exit For
        End If
    Next i
    If blnOtrosDatos Then
        If grdRespEconPA.Columns("Volante").CellText(grdRespEconPA.SelBookmarks(0)) = "" Then
            chkPendVol.Value = 0
        Else
            chkPendVol.Value = 1
        End If
        dcboFecIni.Date = Format(grdRespEconPA.Columns("Fecha Inicio").CellText(grdRespEconPA.SelBookmarks(0)), "dd/mm/yyyy")
        mskHoraIni.Text = Format(grdRespEconPA.Columns("Fecha Inicio").CellText(grdRespEconPA.SelBookmarks(0)), "hh:mm")
        If grdRespEconPA.Columns("Fecha Fin").CellText(grdRespEconPA.SelBookmarks(0)) <> "" Then
            dcboFecFin.Date = Format(grdRespEconPA.Columns("Fecha Fin").CellText(grdRespEconPA.SelBookmarks(0)), "dd/mm/yyyy")
            mskHoraFin.Text = Format(grdRespEconPA.Columns("Fecha Fin").CellText(grdRespEconPA.SelBookmarks(0)), "hh:mm")
        Else
            dcboFecFin.Date = ""
            mskHoraFin.Text = "__:__"
        End If
    End If
End Sub

Private Sub pCargarGrid()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strFI$, strFF$, strPendVol$
    
    LockWindowUpdate grdRespEconPA.hWnd
    grdRespEconPA.RemoveAll
    SQL = "SELECT CI32CODTIPECON, CI13CODENTIDAD,"
    SQL = SQL & " CI21CODPERSONA, ADFN01(CI21CODPERSONA) RESP,"
    SQL = SQL & " NVL(AD11INDVOLANTE,0) AD11INDVOLANTE,"
    SQL = SQL & " AD11FECINICIO, AD11FECFIN"
    SQL = SQL & " FROM AD1100"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?"
    SQL = SQL & " ORDER BY AD11FECINICIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Do While Not rs.EOF
            strFI = Format(rs!AD11FECINICIO, "dd/mm/yyyy hh:mm")
            If Not IsNull(rs!AD11FECFIN) Then strFF = Format(rs!AD11FECFIN, "dd/mm/yyyy hh:mm") Else strFF = ""
            If rs!AD11INDVOLANTE = -1 Then strPendVol = "Pendiente" Else strPendVol = ""
            grdRespEconPA.AddItem rs!CI32CODTIPECON & Chr$(9) _
                            & rs!CI13CODENTIDAD & Chr$(9) _
                            & rs!CI21CODPERSONA & Chr$(9) _
                            & rs!RESP & Chr$(9) _
                            & strFI & Chr$(9) _
                            & strFF & Chr$(9) _
                            & strPendVol & Chr$(9) _
                            & Format(rs!AD11FECINICIO, "dd/mm/yyyy hh:mm:ss")
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
    LockWindowUpdate 0&
End Sub

Private Sub pLimpiar()
    grdRespEconPA.SelBookmarks.RemoveAll
    dcboFecIni.Date = ""
    mskHoraIni.Text = "__:__"
    dcboFecFin.Date = ""
    mskHoraFin.Text = "__:__"
    cboTipoEcon.Text = ""
    cboEntidad.RemoveAll: cboEntidad.Text = ""
    cboRespEcon.RemoveAll: cboRespEcon.Text = ""
    chkPendVol.Value = 0
    dcboFecIni.SetFocus
End Sub

Private Sub grdRespEconPA_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdRespEconPA_Click()
    Dim intPendVol%
    
    grdRespEconPA.SelBookmarks.RemoveAll
    grdRespEconPA.SelBookmarks.Add (grdRespEconPA.RowBookmark(grdRespEconPA.Row))
    If grdRespEconPA.Columns("Volante").CellText(grdRespEconPA.SelBookmarks(0)) = "" Then
        intPendVol = 0
    Else
        intPendVol = 1
    End If
    Call pCargarDatos(grdRespEconPA.Columns("T").CellText(grdRespEconPA.SelBookmarks(0)), _
                grdRespEconPA.Columns("E").CellText(grdRespEconPA.SelBookmarks(0)), _
                grdRespEconPA.Columns("Cod. Resp.").CellText(grdRespEconPA.SelBookmarks(0)), _
                True)
End Sub

Private Sub grdRespEconPA_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If grdRespEconPA.SelBookmarks.Count = 0 Then grdRespEconPA_Click
End Sub

Private Sub mskHoraFin_GotFocus()
    mskHoraFin.SelStart = 0
    mskHoraFin.SelLength = Len(mskHoraFin.Text)
End Sub

Private Sub mskHoraFin_LostFocus()
    If Not mskHoraFin.Text = "__:__" Then
        mskHoraFin.Text = objGen.ReplaceStr(mskHoraFin.Text, "_", "0", 0)
        If Not IsDate(mskHoraFin.Text) Then
            mskHoraFin.Text = "__:__"
        End If
    End If
End Sub

Private Sub mskHoraIni_GotFocus()
    mskHoraIni.SelStart = 0
    mskHoraIni.SelLength = Len(mskHoraIni.Text)
End Sub

Private Sub mskHoraIni_LostFocus()
    If Not mskHoraIni.Text = "__:__" Then
        mskHoraIni.Text = objGen.ReplaceStr(mskHoraIni.Text, "_", "0", 0)
        If Not IsDate(mskHoraIni.Text) Then
            mskHoraIni.Text = "__:__"
        End If
    End If
End Sub

Private Sub pEliminar()
    Dim SQL$, qry As rdoQuery
    
    If MsgBox("�Desea UD. elimiar el registro seleccionado?", vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
    
    SQL = "DELETE FROM AD1100"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?"
    SQL = SQL & " AND AD11FECINICIO = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    qry(2) = Format(grdRespEconPA.Columns("FecIni").CellText(grdRespEconPA.SelBookmarks(0)), "dd/mm/yyyy hh:mm:ss")
    qry.Execute
    If qry.RowsAffected > 0 Then
        grdRespEconPA.DeleteSelected
    Else
        MsgBox "No se ha podido eliminar el registro.", vbExclamation, Me.Caption
    End If
    qry.Close
End Sub

Private Sub pGuardar()
    Dim SQL$, qry As rdoQuery
    
    If Not fComprobarAlGuardar Then Exit Sub
    
    If grdRespEconPA.SelBookmarks.Count = 0 Then 'NUEVO
        SQL = "INSERT INTO AD1100 (AD11FECFIN, CI32CODTIPECON, CI13CODENTIDAD, CI21CODPERSONA,"
        SQL = SQL & " AD11INDVOLANTE, AD01CODASISTENCI, AD07CODPROCESO, AD11FECINICIO)"
        SQL = SQL & " VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
    Else 'MODIFICACI�N
        SQL = "UPDATE AD1100 SET AD11FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),"
        SQL = SQL & " CI32CODTIPECON = ?,"
        SQL = SQL & " CI13CODENTIDAD = ?,"
        SQL = SQL & " CI21CODPERSONA = ?,"
        SQL = SQL & " AD11INDVOLANTE = ?,"
        SQL = SQL & " AD11FECINICIO = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
        SQL = SQL & " WHERE AD01CODASISTENCI = ?"
        SQL = SQL & " AND AD07CODPROCESO = ?"
        SQL = SQL & " AND AD11FECINICIO = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If dcboFecFin.Text <> "" Then
        qry(0) = Format(dcboFecFin.Text & " " & mskHoraFin.Text, "dd/mm/yyyy hh:mm:ss")
    Else
        qry(0) = Null
    End If
    qry(1) = cboTipoEcon.Columns(0).Text
    qry(2) = cboEntidad.Columns(0).Text
    qry(3) = cboRespEcon.Columns(0).Text
    qry(4) = -(chkPendVol.Value)
    If grdRespEconPA.SelBookmarks.Count = 0 Then 'NUEVO
        qry(5) = strCodAsist
        qry(6) = strCodProc
        qry(7) = Format(dcboFecIni.Text & " " & mskHoraIni.Text, "dd/mm/yyyy hh:mm:ss")
    Else 'MODIFICACI�N
        If Format(dcboFecIni.Text & " " & mskHoraIni.Text, "dd/mm/yyyy hh:mm") = Format(grdRespEconPA.Columns("FecIni").CellText(grdRespEconPA.SelBookmarks(0)), "dd/mm/yyyy hh:mm") Then
            qry(5) = Format(grdRespEconPA.Columns("FecIni").CellText(grdRespEconPA.SelBookmarks(0)), "dd/mm/yyyy hh:mm:ss")
        Else
            qry(5) = Format(dcboFecIni.Text & " " & mskHoraIni.Text, "dd/mm/yyyy hh:mm:ss")
        End If
        qry(6) = strCodAsist
        qry(7) = strCodProc
        qry(8) = Format(grdRespEconPA.Columns("FecIni").CellText(grdRespEconPA.SelBookmarks(0)), "dd/mm/yyyy hh:mm:ss")
    End If
    qry.Execute
    Call pCargarGrid
    Call pLimpiar
End Sub

Private Function fComprobarAlSalir() As Boolean
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim cllFecIni As New Collection, cllFecFin As New Collection, i%
    Dim msg$
    
    SQL = "SELECT AD11FECINICIO, AD11FECFIN"
    SQL = SQL & " FROM AD1100"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?"
    SQL = SQL & " ORDER BY AD11FECINICIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Do While Not rs.EOF
            cllFecIni.Add Format(rs(0), "dd/mm/yyyy hh:mm")
            cllFecFin.Add Format(rs(1), "dd/mm/yyyy hh:mm")
            rs.MoveNext
        Loop
        If cllFecFin.item(1) <> "" Then
            msg = msg & "El �ltimo responsable debe estar sin Fecha Fin." & Chr$(13)
        End If
        For i = 1 To cllFecIni.Count - 1
            If cllFecFin.item(i) <> "" Then
                If CDate(cllFecIni.item(i)) > CDate(cllFecFin.item(i)) Then
                    msg = msg & "No existe concordancia entre la Fecha Inicio " & cllFecIni.item(i) _
                            & " y la Fecha Fin " & cllFecFin.item(i) & "." & Chr$(13)
                End If
            End If
            If cllFecIni.item(i) <> cllFecFin.item(i + 1) Then
                msg = msg & "No existe concordancia entre la Fecha Inicio " & cllFecIni.item(i) _
                        & " y la Fecha Fin " & cllFecFin.item(i + 1) & " del responsable anterior." & Chr$(13)
            End If
        Next i
        If cllFecFin.item(1) <> "" Then
            If CDate(cllFecIni.item(i)) > CDate(cllFecFin.item(i)) Then
                msg = msg & "No existe concordancia entre la Fecha Inicio " & cllFecIni.item(i) _
                        & " y la Fecha Fin " & cllFecFin.item(i) & "." & Chr$(13)
            End If
        End If
    Else
        msg = "No se ha encontrado ning�n responsable econ�mico."
    End If
    rs.Close
    qry.Close
        
    If msg <> "" Then
        MsgBox msg, vbExclamation, Me.Caption
    Else
        fComprobarAlSalir = True
    End If
End Function
