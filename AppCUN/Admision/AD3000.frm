VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmDatosAsistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asistencias del paciente"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVisionGlobal 
      Caption         =   "&Visi�n Global"
      Height          =   495
      Left            =   10980
      TabIndex        =   29
      Top             =   60
      Width           =   855
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   11460
      Top             =   900
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   6840
      TabIndex        =   20
      Top             =   0
      Width           =   4035
      Begin VB.CheckBox chkHojaVerde 
         Caption         =   "Hoja Verde"
         Height          =   195
         Left            =   1800
         TabIndex        =   28
         Top             =   600
         Width           =   1155
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   375
         Left            =   3000
         TabIndex        =   24
         Top             =   480
         Width           =   915
      End
      Begin VB.CheckBox chkProgramaPac 
         Caption         =   "Programa paciente"
         Height          =   195
         Left            =   1800
         TabIndex        =   23
         Top             =   240
         Width           =   1755
      End
      Begin VB.CheckBox chkHojaDatos 
         Caption         =   "Hoja de Datos"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   1695
      End
      Begin VB.CheckBox chkHojaFiliacion 
         Caption         =   "Hoja de Filiaci�n"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10980
      TabIndex        =   9
      Top             =   600
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos"
      ForeColor       =   &H00C00000&
      Height          =   4755
      Index           =   2
      Left            =   60
      TabIndex        =   5
      Top             =   3840
      Width           =   11775
      Begin TabDlg.SSTab tabDatos 
         Height          =   4395
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   11595
         _ExtentX        =   20452
         _ExtentY        =   7752
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Tipo de Asistencia"
         TabPicture(0)   =   "AD3000.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "grdTipoAsist"
         Tab(0).Control(1)=   "cmdCambiarTipoAsist"
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Procesos / Asistencia"
         TabPicture(1)   =   "AD3000.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdProcesos"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "ddTipoPac"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "cmdCambiarRemitente"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "tabSubPA"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "cmdDocum"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).ControlCount=   5
         Begin VB.CommandButton cmdDocum 
            Caption         =   "&Docum. aportada"
            Height          =   375
            Left            =   2100
            TabIndex        =   18
            Top             =   3960
            Width           =   1515
         End
         Begin TabDlg.SSTab tabSubPA 
            Height          =   3975
            Left            =   6180
            TabIndex        =   13
            Top             =   360
            Width           =   5325
            _ExtentX        =   9393
            _ExtentY        =   7011
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Dpto. y Dr. Responsable"
            TabPicture(0)   =   "AD3000.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "grdRespPA"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "cmdCambiarRespPA"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).ControlCount=   2
            TabCaption(1)   =   "Responsable Econ�mico"
            TabPicture(1)   =   "AD3000.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "cmdMantDatosEconPA"
            Tab(1).Control(1)=   "cmdUpdateRespEcon"
            Tab(1).Control(2)=   "cmdCambiarDatosEcon"
            Tab(1).Control(3)=   "grdRespEconPA"
            Tab(1).ControlCount=   4
            Begin VB.CommandButton cmdMantDatosEconPA 
               Caption         =   "&Mantenimiento"
               Height          =   375
               Left            =   -71340
               TabIndex        =   27
               Top             =   3540
               Width           =   1575
            End
            Begin VB.CommandButton cmdUpdateRespEcon 
               Caption         =   "&Cambio retroactivo"
               Height          =   375
               Left            =   -73740
               TabIndex        =   26
               Top             =   3540
               Width           =   1575
            End
            Begin VB.CommandButton cmdCambiarDatosEcon 
               Caption         =   "N&uevo"
               Height          =   375
               Left            =   -74940
               TabIndex        =   16
               Top             =   3540
               Width           =   1095
            End
            Begin VB.CommandButton cmdCambiarRespPA 
               Caption         =   "&Nuevo"
               Height          =   375
               Left            =   60
               TabIndex        =   15
               Top             =   3540
               Width           =   1095
            End
            Begin SSDataWidgets_B.SSDBGrid grdRespPA 
               Height          =   3135
               Left            =   60
               TabIndex        =   14
               Top             =   360
               Width           =   5205
               _Version        =   131078
               DataMode        =   2
               RecordSelectors =   0   'False
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowColumnMoving=   0
               AllowColumnSwapping=   0
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   9181
               _ExtentY        =   5530
               _StockProps     =   79
            End
            Begin SSDataWidgets_B.SSDBGrid grdRespEconPA 
               Height          =   3135
               Left            =   -74940
               TabIndex        =   17
               Top             =   360
               Width           =   5205
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RecordSelectors =   0   'False
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowColumnMoving=   0
               AllowColumnSwapping=   0
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   9181
               _ExtentY        =   5530
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.CommandButton cmdCambiarRemitente 
            Caption         =   "&Observ. / Dr. Remitente"
            Height          =   375
            Left            =   60
            TabIndex        =   12
            Top             =   3960
            Width           =   1935
         End
         Begin VB.CommandButton cmdCambiarTipoAsist 
            Caption         =   "Nu&evo"
            Enabled         =   0   'False
            Height          =   375
            Left            =   -74940
            TabIndex        =   11
            Top             =   3960
            Width           =   1095
         End
         Begin SSDataWidgets_B.SSDBDropDown ddTipoPac 
            Height          =   1035
            Left            =   2100
            TabIndex        =   10
            Top             =   840
            Width           =   1875
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2831
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3307
            _ExtentY        =   1826
            _StockProps     =   77
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdProcesos 
            Height          =   3555
            Left            =   60
            TabIndex        =   7
            Top             =   360
            Width           =   6075
            _Version        =   131078
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowColumnMoving=   0
            AllowColumnSwapping=   0
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   10716
            _ExtentY        =   6271
            _StockProps     =   79
         End
         Begin SSDataWidgets_B.SSDBGrid grdTipoAsist 
            Height          =   3555
            Left            =   -74940
            TabIndex        =   8
            Top             =   360
            Width           =   6225
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowColumnMoving=   0
            AllowColumnSwapping=   0
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   10980
            _ExtentY        =   6271
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Asistencias"
      ForeColor       =   &H00C00000&
      Height          =   2775
      Index           =   1
      Left            =   60
      TabIndex        =   1
      Top             =   1020
      Width           =   11775
      Begin VB.CommandButton cmdCamas 
         Caption         =   "&Hist�rico Camas de la Asistencia"
         Height          =   795
         Left            =   10620
         TabIndex        =   19
         Top             =   240
         Width           =   1035
      End
      Begin SSDataWidgets_B.SSDBGrid grdAsistencias 
         Height          =   2415
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   10425
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18389
         _ExtentY        =   4260
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Paciente"
      ForeColor       =   &H00C00000&
      Height          =   975
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   6735
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   5700
         TabIndex        =   25
         Top             =   360
         Width           =   915
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   420
         Width           =   4695
      End
      Begin VB.TextBox txtNH 
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Top             =   420
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmDatosAsistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strNH$, strCodPers$
Dim strTipoAsistActual$, strCamaActual$
Dim strDptoRespActual$, strDrRespActual$

Private Sub cmdBuscar_Click()
    Call objSecurity.LaunchProcess("AD2999")
    If objPipe.PipeExist("AD2999_CI22NUMHISTORIA") Then
        strNH = objPipe.PipeGet("AD2999_CI22NUMHISTORIA"): txtNH.Text = strNH
        Call objPipe.PipeRemove("AD2999_CI22NUMHISTORIA")
        Call objPipe.PipeRemove("AD2999_CI21CODPERSONA")
        Call pCargarAsistencias
    End If
End Sub

Private Sub cmdCamas_Click()
    Call objPipe.PipeSet("AD3005_AD01CODASISTENCI", grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)))
    frmCamasAsist.Show vbModal
    Set frmCamasAsist = Nothing
End Sub

Private Sub cmdCambiarDatosEcon_Click()
    Call pCambiarDatosEconPA
End Sub

Private Sub cmdCambiarRemitente_Click()
    Call pCambiarRemitente
End Sub

Private Sub cmdCambiarRespPA_Click()
    Call pCambiarRespPA
End Sub

Private Sub cmdCambiarTipoAsist_Click()
    Call pCambiarTipoAsist
End Sub

Private Sub cmdDocum_Click()
    Dim strCodAsist$, strCodProc$
    Dim vntData(0 To 3)
    Dim sql As String
    Dim rs As rdoResultset
    Dim qry As rdoQuery
    
    strCodAsist = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    strCodProc = grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
    
    If fFacturado(strCodProc, strCodAsist, grdProcesos.Columns("Fecha Inicio").CellText(grdProcesos.SelBookmarks(0))) Then
        sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? "
        sql = sql & " AND SG02COD = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = constDPTO_ADMINISTRACION
            qry(1) = objSecurity.strUser
        Set rs = qry.OpenResultset
        If rs(0) = 0 Then vntData(3) = 1 Else vntData(3) = 0
        rs.Close
        qry.Close
    Else
        vntData(3) = 0
    End If
    
    vntData(0) = strCodPers
    vntData(1) = strCodProc
    vntData(2) = strCodAsist
    Call objSecurity.LaunchProcess("AD0206", vntData)
    Call pCargarDatosEconPA(strCodAsist, strCodProc)
End Sub

Private Sub CmdImprimir_Click()
    Dim msg$, blnHF As Boolean, blnHD As Boolean, blnPP As Boolean, blnHV As Boolean
    
    If txtNH.Text = "" Then Exit Sub
    
    If Not chkHojaFiliacion.Value = 0 Then blnHF = True
    If Not chkHojaDatos.Value = 0 Then blnHD = True
    If Not chkProgramaPac.Value = 0 Then blnPP = True
    If Not chkHojaVerde.Value = 0 Then blnHV = True
    If blnHF Or blnHD Or blnPP Or blnHV Then
        msg = "�Desea Ud. imprimir las hojas marcadas para el Proceso/Asistencia seleccionado?"
        If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbYes Then
            Call pImprimir(blnHF, blnHD, blnPP, blnHV)
        End If
        chkHojaFiliacion.Value = 0
        chkHojaDatos.Value = 0
        chkProgramaPac.Value = 0
        chkHojaVerde.Value = 0
    Else
        msg = "No se ha seleccionado ninguna Hoja para imprimir."
        MsgBox msg, vbExclamation, Me.Caption
    End If
End Sub

Private Sub cmdMantDatosEconPA_Click()
    Dim strCodAsist$, strCodProc$
    Dim vntData(1 To 3)
        
    strCodAsist = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    strCodProc = grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
    
    'Llamada a la pantalla para la introduci�n de datos econ�micos
    vntData(1) = strCodAsist
    vntData(2) = strCodProc
    vntData(3) = txtNH.Text
    Call objSecurity.LaunchProcess("AD3006", vntData)
    Call pCargarDatosEconPA(strCodAsist, strCodProc)
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdUpdateRespEcon_Click()
    Call pCambiarDatosEconRetroactivo
End Sub

Private Sub cmdVisionGlobal_Click()
    Dim vntData()
    Screen.MousePointer = vbHourglass
    ReDim vntData(1 To 2) As Variant
    vntData(1) = strCodPers
    vntData(2) = 2
    Call objSecurity.LaunchProcess("HC02", vntData())
    Screen.MousePointer = vbDefault
End Sub

Private Sub ddTipoPac_CloseUp()
    Dim sql$, qry As rdoQuery, qrySIHC As rdoQuery
    
    sql = "UPDATE AD0800"
    sql = sql & " SET AD10CODTIPPACIEN = ?"
    sql = sql & " WHERE AD01CODASISTENCI = ?"
    sql = sql & " AND AD07CODPROCESO = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = grdProcesos.Columns("Tipo Paciente").Value
    qry(1) = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    qry(2) = grdProcesos.Columns("N� Proceso").Text
    
    'sihc
    sql = "UPDATE Asist SET cTiPac = ?"
    sql = sql & " WHERE AD08NumCaso IN ("
    sql = sql & " SELECT AD08NumCaso "
    sql = sql & " FROM AD0800"
    sql = sql & " WHERE AD01CODASISTENCI = ?"
    sql = sql & " AND AD07CODPROCESO = ?)"
    Set qrySIHC = objApp.rdoConnect.CreateQuery("", sql)
    qrySIHC(0) = grdProcesos.Columns("Tipo Paciente").Value
    qrySIHC(1) = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    qrySIHC(2) = grdProcesos.Columns("N� Proceso").Text
        
    'actualizaci�n de la BD
    objApp.BeginTrans
    On Error Resume Next
    qry.Execute
    qry.Close
    qrySIHC.Execute
    qrySIHC.Close
    If Err = 0 Then
        objApp.CommitTrans
    Else
        objApp.RollbackTrans
        sql = "Se ha producido un error al realizar el cambio de tipo de Paciente."
        sql = sql & Chr$(13)
        sql = sql & "Vuelva a intentarlo."
        MsgBox sql, vbExclamation, Me.Caption
    End If
    On Error GoTo 0
End Sub

Private Sub Form_Load()
    Call pFormatearGrids
    Call pCargarTiposPac
    
    If objPipe.PipeExist("AD3000_CI21CODPERSONA") Then
        Dim sql$, qry As rdoQuery, rs As rdoResultset
        
        sql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objPipe.PipeGet("AD3000_CI21CODPERSONA")
        Set rs = qry.OpenResultset()
        If Not IsNull(rs!CI22NUMHISTORIA) Then
            strNH = rs!CI22NUMHISTORIA: txtNH = strNH
        Else
            MsgBox "El paciente todav�a no tiene n� de Historia.", vbExclamation, Me.Caption
            Exit Sub
        End If
        rs.Close
        qry.Close
        Call pCargarAsistencias
    ElseIf objPipe.PipeExist("AD3000_CI22NUMHISTORIA") Then
        strNH = objPipe.PipeGet("AD3000_CI22NUMHISTORIA"): txtNH = strNH
        Call pCargarAsistencias
    End If
    If objPipe.PipeExist("AD3000_CI21CODPERSONA") Then Call objPipe.PipeRemove("AD3000_CI21CODPERSONA")
    If objPipe.PipeExist("AD3000_CI22NUMHISTORIA") Then Call objPipe.PipeRemove("AD3000_CI22NUMHISTORIA")
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If objPipe.PipeExist("AD3000_CI21CODPERSONA") Then Call objPipe.PipeRemove("AD3000_CI21CODPERSONA")
    If objPipe.PipeExist("AD3000_CI22NUMHISTORIA") Then Call objPipe.PipeRemove("AD3000_CI22NUMHISTORIA")
End Sub

Private Sub grdAsistencias_Click()
    grdAsistencias.SelBookmarks.RemoveAll
    grdAsistencias.SelBookmarks.Add (grdAsistencias.RowBookmark(grdAsistencias.Row))
    Call pCargarTipoAsist(grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)))
    Call pCargarProcesos(grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)))
End Sub

Private Sub grdAsistencias_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If grdAsistencias.SelBookmarks.Count = 0 Then grdAsistencias_Click
End Sub

Private Sub grdProcesos_Click()
    grdProcesos.SelBookmarks.RemoveAll
    grdProcesos.SelBookmarks.Add (grdProcesos.RowBookmark(grdProcesos.Row))
    Call pCargarRespPA(grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)), grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0)))
    Call pCargarDatosEconPA(grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)), grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0)))
End Sub

Private Sub grdProcesos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If grdProcesos.SelBookmarks.Count = 0 Then grdProcesos_Click
End Sub

Private Sub txtNH_LostFocus()
    If txtNH.Text = "" Then txtNH.Text = strNH
    If strNH <> txtNH.Text Then
        If txtNH.Text = "" Then
            txtNH.Text = strNH
        Else
            strNH = txtNH.Text
            Call pCargarAsistencias
        End If
    End If
End Sub

Private Sub txtNH_GotFocus()
    txtNH.SelStart = 0
    txtNH.SelLength = Len(txtNH.Text)
End Sub

Private Sub txtNH_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case 8
    Case 13: SendKeys "{TAB}"
    Case Is < Asc("0"), Is > Asc("9"): KeyAscii = 0
    End Select
End Sub

Private Sub pFormatearGrids()
    With grdAsistencias
        .Columns(0).Caption = "N� Asistencia"
        .Columns(0).Width = 1100
        .Columns(1).Caption = "Fecha Inicio"
        .Columns(1).Width = 1450
        .Columns(2).Caption = "Fecha Fin"
        .Columns(2).Width = 1450
        .Columns(3).Caption = "Tipo Asistencia"
        .Columns(3).Width = 1400
        .Columns(4).Caption = "Cama"
        .Columns(4).Width = 600
        .Columns(5).Caption = "Motivo alta"
        .Columns(5).Width = 1400
        .Columns(6).Caption = "Diagn�stico salida"
        .Columns(6).Width = 2750
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    With grdTipoAsist
        .Columns(0).Caption = "Fecha Inicio"
        .Columns(0).Width = 1450
        .Columns(1).Caption = "Fecha Fin"
        .Columns(1).Width = 1450
        .Columns(2).Caption = "Tipo Asistencia"
        .Columns(2).Width = 1400
        .Columns(3).Caption = "Cama"
        .Columns(3).Width = 600
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    With grdProcesos
        .StyleSets.Add ("blocked")
        .StyleSets("blocked").BackColor = objApp.objUserColor.lngReadOnly
        .StyleSets.Add ("mandatory")
        .StyleSets("mandatory").BackColor = objApp.objUserColor.lngMandatory
        
        .Columns(0).Caption = "N� Proceso"
        .Columns(0).Width = 1100
        .Columns(0).StyleSet = "blocked"
        .Columns(0).Locked = True
        .Columns(1).Caption = "Fecha Inicio"
        .Columns(1).Width = 1450
        .Columns(1).StyleSet = "blocked"
        .Columns(1).Locked = True
        .Columns(2).Caption = "Cama"
        .Columns(2).Width = 600
        .Columns(2).StyleSet = "blocked"
        .Columns(2).Locked = True
        .Columns(3).Caption = "Dpto. Resp."
        .Columns(3).Width = 1800
        .Columns(3).StyleSet = "blocked"
        .Columns(3).Locked = True
        .Columns(4).Caption = "Dr. Resp."
        .Columns(4).Width = 1800
        .Columns(4).StyleSet = "blocked"
        .Columns(4).Locked = True
        .Columns(5).Caption = "Tipo Paciente"
        .Columns(5).Width = 1700
        .Columns(5).StyleSet = "mandatory"
        .Columns(6).Caption = "Fecha Fin"
        .Columns(1).Width = 1450
        .Columns(6).StyleSet = "blocked"
        .Columns(6).Locked = True
    End With
    
    With grdRespPA
        .Columns(0).Caption = "Departamento"
        .Columns(0).Width = 1800
        .Columns(1).Caption = "Doctor"
        .Columns(1).Width = 1800
        .Columns(2).Caption = "Fecha Inicio"
        .Columns(2).Width = 1450
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    With grdRespEconPA
        .Columns(0).Caption = "T / E"
        .Columns(0).Width = 600
        .Columns(1).Caption = "Responsable"
        .Columns(1).Width = 2800
        .Columns(2).Caption = "Volante"
        .Columns(2).Width = 800
        .Columns(2).Alignment = ssCaptionAlignmentCenter
        .Columns(3).Caption = "Fecha Inicio"
        .Columns(3).Width = 1450
        .Columns(4).Caption = "Fecha Fin"
        .Columns(4).Width = 1450
        .Columns(5).Caption = "Cod. Resp."
        .Columns(5).Width = 1000
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub pCargarAsistencias()
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strCodAsist$, strFI$, strFF$
    
    Screen.MousePointer = vbHourglass
    
    grdAsistencias.RemoveAll
    grdTipoAsist.RemoveAll
    grdProcesos.RemoveAll
    grdRespPA.RemoveAll

    'datos del paciente
    sql = "SELECT CI21CODPERSONA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC"
    sql = sql & " FROM CI2200"
    sql = sql & " WHERE CI22NUMHISTORIA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNH
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        txtPaciente.Text = rs!PAC
        strCodPers = rs!CI21CODPERSONA
    Else
        txtPaciente.Text = ""
    End If
    rs.Close
    qry.Close
    If txtPaciente.Text = "" Then
        Screen.MousePointer = vbDefault
        sql = "No se ha encontrado ning�n paciente con el n� de Historia se�alado."
        MsgBox sql, vbInformation, Me.Caption
        On Error Resume Next
        txtNH.SetFocus
        On Error GoTo 0
        Exit Sub
    End If
    
    'datos de las asistencias
    'NOTA: s�lo se muestra la cama si la asistencia es la actual ya que el acceso a al
    'AD1600 para mostrar la cama de asistencias anteriores es muy lento
    LockWindowUpdate grdAsistencias.hWnd
    sql = "SELECT AD0100.AD01CODASISTENCI, AD01FECINICIO, AD01FECFIN,"
    sql = sql & " AD27DESALTAASIST, AD01DIAGNOSSALI,"
    sql = sql & " AD12DESTIPOASIST, GCFN06(AD15CODCAMA) AD15CODCAMA"
    sql = sql & " FROM AD2700, AD2500, AD1200, AD1500, AD0100"
    sql = sql & " WHERE CI22NUMHISTORIA = ?"
    sql = sql & " AND AD2700.AD27CODALTAASIST (+)= AD0100.AD27CODALTAASIST"
    sql = sql & " AND AD2500.AD01CODASISTENCI = AD0100.AD01CODASISTENCI"
    sql = sql & " AND AD25FECFIN IS NULL"
    sql = sql & " AND AD1200.AD12CODTIPOASIST = AD2500.AD12CODTIPOASIST"
    sql = sql & " AND AD1500.AD01CODASISTENCI (+)= AD0100.AD01CODASISTENCI"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " ORDER BY AD01FECINICIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNH
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strCodAsist = rs!AD01CODASISTENCI
        Do While Not rs.EOF
            If Not IsNull(rs!AD01FECINICIO) Then strFI = Format(rs!AD01FECINICIO, "dd/mm/yyyy hh:mm") Else strFI = ""
            If Not IsNull(rs!AD01FECFIN) Then strFF = Format(rs!AD01FECFIN, "dd/mm/yyyy hh:mm") Else strFF = ""
            grdAsistencias.AddItem rs!AD01CODASISTENCI & Chr$(9) _
                            & strFI & Chr$(9) _
                            & strFF & Chr$(9) _
                            & rs!AD12DESTIPOASIST & Chr$(9) _
                            & rs!AD15CODCAMA & Chr$(9) _
                            & rs!AD27DESALTAASIST & Chr$(9) _
                            & rs!AD01DIAGNOSSALI
            rs.MoveNext
        Loop
        grdAsistencias.MoveFirst
        grdAsistencias.SelBookmarks.Add (grdAsistencias.RowBookmark(grdAsistencias.Row))
        On Error Resume Next
        grdAsistencias.SetFocus
        On Error GoTo 0
    End If
    rs.Close
    qry.Close
    LockWindowUpdate 0&
    
    'tipos de asistencia
    If strCodAsist <> "" Then
        Call pCargarTipoAsist(strCodAsist)
        Call pCargarProcesos(strCodAsist)
    End If
    
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarTipoAsist(strCodAsist$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strFI$, strFF$, strAux$
    Dim strCama$
    
    LockWindowUpdate grdTipoAsist.hWnd
    grdTipoAsist.RemoveAll
    sql = "SELECT AD25FECINICIO, AD25FECFIN,"
    sql = sql & " AD2500.AD12CODTIPOASIST, AD12DESTIPOASIST,"
    sql = sql & " GCFN06(AD15CODCAMA) AD15CODCAMA"
    sql = sql & " FROM AD2500, AD1200, AD1600"
    sql = sql & " WHERE AD2500.AD01CODASISTENCI = ?"
    sql = sql & " AND AD1200.AD12CODTIPOASIST = AD2500.AD12CODTIPOASIST"
    sql = sql & " AND AD1600.AD01CODASISTENCI (+)= AD2500.AD01CODASISTENCI"
    sql = sql & " AND AD14CODESTCAMA (+) = " & constCAMA_OCUPADA
    sql = sql & " ORDER BY AD25FECINICIO DESC, AD16FECCAMBIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodAsist
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strTipoAsistActual = rs!AD12CODTIPOASIST
        If Not IsNull(rs!AD15CODCAMA) Then strCamaActual = rs!AD15CODCAMA Else strCamaActual = ""
        Do While Not rs.EOF
            If strAux <> rs!AD25FECINICIO Then
                strAux = rs!AD25FECINICIO
                strFI = Format(rs!AD25FECINICIO, "dd/mm/yyyy hh:mm")
                If Not IsNull(rs!AD25FECFIN) Then strFF = Format(rs!AD25FECFIN, "dd/mm/yyyy hh:mm") Else strFF = ""
                If rs!AD12CODTIPOASIST = constASIST_HOSP Then
                    If Not IsNull(rs!AD15CODCAMA) Then strCama = rs!AD15CODCAMA Else strCama = ""
                Else
                    strCama = ""
                End If
                grdTipoAsist.AddItem strFI & Chr$(9) _
                                & strFF & Chr$(9) _
                                & rs!AD12DESTIPOASIST & Chr$(9) _
                                & strCama
            End If
            rs.MoveNext
        Loop
        grdTipoAsist.MoveFirst
        grdTipoAsist.SelBookmarks.Add (grdTipoAsist.RowBookmark(grdTipoAsist.Row))
    Else
        strTipoAsistActual = "": strCamaActual = ""
    End If
    rs.Close
    qry.Close
    LockWindowUpdate 0&
End Sub

Private Sub pCargarProcesos(strCodAsist$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strCodProc$, strFI$, strFF$

    LockWindowUpdate grdProcesos.hWnd
    
    'si la asistencia est� finalizada no se puede cambiar...
    If grdAsistencias.Columns("Fecha Fin").CellText(grdAsistencias.SelBookmarks(0)) = "" Then
        grdProcesos.AllowUpdate = True
        grdProcesos.Columns("Tipo Paciente").StyleSet = "mandatory"
        grdProcesos.Columns("Tipo Paciente").Locked = False
        cmdCambiarTipoAsist.Enabled = True
        cmdCambiarRemitente.Enabled = True
        cmdDocum.Enabled = True
        cmdCambiarRespPA.Enabled = True
        cmdCambiarDatosEcon.Enabled = True
    Else
        '...el tipo de paciente
        grdProcesos.AllowUpdate = False
        grdProcesos.Columns("Tipo Paciente").StyleSet = "blocked"
        grdProcesos.Columns("Tipo Paciente").Locked = True
        '...el tipo de asistencia
        cmdCambiarTipoAsist.Enabled = False
''        '...el remitente y las observaciones
''        cmdCambiarRemitente.Enabled = False
''        '...la documentaci�n aportada
''        cmdDocum.Enabled = False
        '...el responsable
        cmdCambiarRespPA.Enabled = False
        '...el responsable econ�mico
        cmdCambiarDatosEcon.Enabled = False
    End If
    
    grdProcesos.RemoveAll
    sql = "SELECT AD0800.AD07CODPROCESO, AD08FECINICIO, AD08FECFIN,"
    sql = sql & " AD0800.AD10CODTIPPACIEN, AD10DESTIPPACIEN,"
    sql = sql & " NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) DR,"
    sql = sql & " AD02DESDPTO, GCFN06(AD15CODCAMA) AD15CODCAMA"
    sql = sql & " FROM SG0200, AD0200, AD0500, AD1000, AD1500, AD0800"
    sql = sql & " WHERE AD0800.AD01CODASISTENCI = ?"
    sql = sql & " AND AD1000.AD10CODTIPPACIEN = AD0800.AD10CODTIPPACIEN"
    sql = sql & " AND AD0500.AD01CODASISTENCI = AD0800.AD01CODASISTENCI"
    sql = sql & " AND AD0500.AD07CODPROCESO = AD0800.AD07CODPROCESO"
    sql = sql & " AND AD1500.AD01CODASISTENCI (+)= AD0800.AD01CODASISTENCI"
    sql = sql & " AND AD1500.AD07CODPROCESO (+)= AD0800.AD07CODPROCESO"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " AND AD05FECFINRESPON IS NULL"
    sql = sql & " AND SG0200.SG02COD = AD0500.SG02COD"
    sql = sql & " AND AD0200.AD02CODDPTO = AD0500.AD02CODDPTO"
    sql = sql & " ORDER BY AD15CODCAMA, AD08FECINICIO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodAsist
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strCodProc = rs!AD07CODPROCESO
        Do While Not rs.EOF
            If Not IsNull(rs!AD08FECINICIO) Then strFI = Format(rs!AD08FECINICIO, "dd/mm/yyyy hh:mm") Else strFI = ""
            If Not IsNull(rs!AD08FECFIN) Then strFF = Format(rs!AD08FECFIN, "dd/mm/yyyy hh:mm") Else strFF = ""
            grdProcesos.AddItem rs!AD07CODPROCESO & Chr$(9) _
                            & strFI & Chr$(9) _
                            & rs!AD15CODCAMA & Chr$(9) _
                            & rs!AD02DESDPTO & Chr$(9) _
                            & rs!DR & Chr$(9) _
                            & rs!AD10CODTIPPACIEN & Chr$(9) _
                            & strFF
            rs.MoveNext
        Loop
        grdProcesos.MoveFirst
        grdProcesos.SelBookmarks.Add (grdProcesos.RowBookmark(grdProcesos.Row))
    End If
    rs.Close
    qry.Close
    LockWindowUpdate 0&
    
    If strCodProc <> "" Then
        Call pCargarRespPA(strCodAsist, strCodProc)
        Call pCargarDatosEconPA(strCodAsist, strCodProc)
    End If
End Sub

Private Sub pCargarRespPA(strCodAsist$, strCodProc$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strFI$
    
    LockWindowUpdate grdRespPA.hWnd
    grdRespPA.RemoveAll
    sql = "SELECT AD0500.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) DR,"
    sql = sql & " AD0500.AD02CODDPTO, AD02DESDPTO,"
    sql = sql & " AD05FECINIRESPON"
    sql = sql & " FROM SG0200, AD0200, AD0500"
    sql = sql & " WHERE AD01CODASISTENCI = ?"
    sql = sql & " AND AD07CODPROCESO = ?"
    sql = sql & " AND SG0200.SG02COD = AD0500.SG02COD"
    sql = sql & " AND AD0200.AD02CODDPTO = AD0500.AD02CODDPTO"
    sql = sql & " ORDER BY AD05FECINIRESPON DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strDptoRespActual = rs!AD02CODDPTO: strDrRespActual = rs!SG02COD
        Do While Not rs.EOF
            If Not IsNull(rs!AD05FECINIRESPON) Then strFI = Format(rs!AD05FECINIRESPON, "dd/mm/yyyy hh:mm") Else strFI = ""
            grdRespPA.AddItem rs!AD02DESDPTO & Chr$(9) _
                            & rs!DR & Chr$(9) _
                            & strFI
            rs.MoveNext
        Loop
        grdRespPA.MoveFirst
        grdRespPA.SelBookmarks.Add (grdRespPA.RowBookmark(grdRespPA.Row))
    Else
        strDptoRespActual = "": strDrRespActual = ""
    End If
    rs.Close
    qry.Close
    LockWindowUpdate 0&
End Sub

Private Sub pCargarDatosEconPA(strCodAsist$, strCodProc$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strFI$, strFF$, strPendVol$
    
    LockWindowUpdate grdRespEconPA.hWnd
    grdRespEconPA.RemoveAll
    sql = "SELECT CI32CODTIPECON, CI13CODENTIDAD,"
    sql = sql & " CI21CODPERSONA, ADFN01(CI21CODPERSONA) RESP,"
    sql = sql & " NVL(AD11INDVOLANTE,0) AD11INDVOLANTE,"
    sql = sql & " AD11FECINICIO, AD11FECFIN"
    sql = sql & " FROM AD1100"
    sql = sql & " WHERE AD01CODASISTENCI = ?"
    sql = sql & " AND AD07CODPROCESO = ?"
    sql = sql & " ORDER BY AD11FECINICIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Do While Not rs.EOF
            If Not IsNull(rs!AD11FECINICIO) Then strFI = Format(rs!AD11FECINICIO, "dd/mm/yyyy hh:mm") Else strFI = ""
            If Not IsNull(rs!AD11FECFIN) Then strFF = Format(rs!AD11FECFIN, "dd/mm/yyyy hh:mm") Else strFF = ""
            If rs!AD11INDVOLANTE = -1 Then strPendVol = "Pendiente" Else strPendVol = ""
            grdRespEconPA.AddItem rs!CI32CODTIPECON & " - " & rs!CI13CODENTIDAD & Chr$(9) _
                            & rs!RESP & Chr$(9) _
                            & strPendVol & Chr$(9) _
                            & strFI & Chr$(9) _
                            & strFF & Chr$(9) _
                            & rs!CI21CODPERSONA
            rs.MoveNext
        Loop
        grdRespEconPA.MoveFirst
        grdRespEconPA.SelBookmarks.Add (grdRespEconPA.RowBookmark(grdRespEconPA.Row))
    End If
    rs.Close
    qry.Close
    LockWindowUpdate 0&
End Sub

Private Sub pCargarTiposPac()
    Dim sql$, rs As rdoResultset
    
    sql = "SELECT AD10CODTIPPACIEN, AD10DESTIPPACIEN"
    sql = sql & " FROM AD1000"
    sql = sql & " WHERE AD10FECFIN IS NULL"
    sql = sql & " ORDER BY AD10DESTIPPACIEN"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rs.EOF
        ddTipoPac.AddItem rs!AD10CODTIPPACIEN & Chr$(9) _
                        & rs!AD10DESTIPPACIEN
        rs.MoveNext
    Loop
    rs.Close
    grdProcesos.Columns("Tipo Paciente").DropDownHwnd = ddTipoPac.hWnd
End Sub

Private Sub pCambiarRespPA()
    'Llamada a la pantalla para la introduci�n de datos (Dpto. y Dr.)
    Call objPipe.PipeSet("AD_DPTORESP", strDptoRespActual)
    Call objPipe.PipeSet("AD_DRRESP", strDrRespActual)
    frmCambioRespPA.Show vbModal
    Set frmCambioRespPA = Nothing
    
    If objPipe.PipeExist("AD_DptoRespPA") Then 'si existe Dpto., tambi�n existe Dr.
        Dim sql$, qryU As rdoQuery, qryI As rdoQuery
        Dim strAhora$, strCodAsist$, strCodProc$
        
        strAhora = strFechaHora_Sistema
        strCodAsist = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
        strCodProc = grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
        
        'qry de Fecha Fin del responsable anterior
        sql = "UPDATE AD0500 SET AD05FECFINRESPON = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
        sql = sql & " WHERE AD01CODASISTENCI = ?"
        sql = sql & " AND AD07CODPROCESO = ?"
        sql = sql & " AND AD05FECFINRESPON IS NULL"
        Set qryU = objApp.rdoConnect.CreateQuery("", sql)
        qryU(0) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qryU(1) = strCodAsist
        qryU(2) = strCodProc
        
        'qry del nuevo registro de responsable
        sql = "INSERT INTO AD0500 (AD01CODASISTENCI, AD07CODPROCESO, AD05FECINIRESPON,"
        sql = sql & " SG02COD, AD02CODDPTO)"
        sql = sql & " VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?, ?)"
        Set qryI = objApp.rdoConnect.CreateQuery("", sql)
        qryI(0) = strCodAsist
        qryI(1) = strCodProc
        qryI(2) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qryI(3) = objPipe.PipeGet("AD_DrRespPA")
        qryI(4) = objPipe.PipeGet("AD_DptoRespPA")

        'eliminr los pipes
        Call objPipe.PipeRemove("AD_DptoRespPA")
        Call objPipe.PipeRemove("AD_DrRespPA")
        
        'actualizaci�n de la BD
        objApp.BeginTrans
        On Error Resume Next
        qryU.Execute
        qryI.Execute
        qryU.Close
        qryI.Close
        If Err = 0 Then
            objApp.CommitTrans
            'actualizaci�n del grid
            Call pCargarRespPA(strCodAsist, strCodProc)
            grdProcesos.Columns("Dpto. Resp.").Text = grdRespPA.Columns("Departamento").Text
            grdProcesos.Columns("Dr. Resp.").Text = grdRespPA.Columns("Doctor").Text
        Else
            objApp.RollbackTrans
            sql = "Se ha producido un error al realizar el cambio de responsable del Proceso/Asistencia."
            sql = sql & Chr$(13)
            sql = sql & "Vuelva a intentarlo."
            MsgBox sql, vbExclamation, Me.Caption
        End If
        On Error GoTo 0
    End If
End Sub

Private Sub pCambiarDatosEconPA()
    Dim strCodAsist$, strCodProc$
        
    strCodAsist = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    strCodProc = grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
    
    'Llamada a la pantalla para la introduci�n de datos econ�micos
    Call objPipe.PipeSet("AD3004_CI22NUMHISTORIA", txtNH.Text)
    Call objPipe.PipeSet("AD3004_AD01CODASISTENCI", strCodAsist)
    Call objPipe.PipeSet("AD3004_AD07CODPROCESO", strCodProc)
    Call objPipe.PipeSet("AD3004_ACCESO", 0)
    frmCambioDatosEconPA.Show vbModal
    Set frmCambioDatosEconPA = Nothing
    
    If objPipe.PipeExist("AD_TipoEcon") Then 'si existe Tipo Econ., tambi�n existen los dem�s datos
        Dim sql$, qryU As rdoQuery, qryI As rdoQuery, qrySIHC As rdoQuery, strAhora$
        
        strAhora = strFechaHora_Sistema
        
        'qry de Fecha Fin de los datos econ�micos anteriores
        sql = "UPDATE AD1100 SET AD11FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
        sql = sql & " WHERE AD07CODPROCESO = ?"
        sql = sql & " AND AD01CODASISTENCI = ?"
        sql = sql & " AND AD11FECFIN IS NULL"
        Set qryU = objApp.rdoConnect.CreateQuery("", sql)
        qryU(0) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qryU(1) = strCodProc
        qryU(2) = strCodAsist
        
        sql = "INSERT INTO AD1100 (AD07CODPROCESO, AD01CODASISTENCI, AD11FECINICIO,"
        sql = sql & " CI32CODTIPECON, CI13CODENTIDAD, CI21CODPERSONA, AD11INDVOLANTE)"
        sql = sql & " VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, ?)"
        Set qryI = objApp.rdoConnect.CreateQuery("", sql)
        qryI(0) = strCodProc
        qryI(1) = strCodAsist
        qryI(2) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qryI(3) = objPipe.PipeGet("AD_TipoEcon")
        qryI(4) = objPipe.PipeGet("AD_Entidad")
        qryI(5) = objPipe.PipeGet("AD_RespEcon")
        qryI(6) = objPipe.PipeGet("AD_PendVol")
        
        'sihc
        sql = "UPDATE Asist SET cTiEco = ?"
        sql = sql & " WHERE AD08NumCaso IN ("
        sql = sql & " SELECT AD08NumCaso "
        sql = sql & " FROM AD0800"
        sql = sql & " WHERE AD01CODASISTENCI = ?"
        sql = sql & " AND AD07CODPROCESO = ?)"
        Set qrySIHC = objApp.rdoConnect.CreateQuery("", sql)
        qrySIHC(0) = objPipe.PipeGet("AD_TipoEcon")
        qrySIHC(1) = strCodAsist
        qrySIHC(2) = strCodProc

        'eliminr los pipes
        Call objPipe.PipeRemove("AD_TipoEcon")
        Call objPipe.PipeRemove("AD_Entidad")
        Call objPipe.PipeRemove("AD_RespEcon")
        Call objPipe.PipeRemove("AD_PendVol")
        
        'actualizaci�n de la BD
        objApp.BeginTrans
        On Error Resume Next
        qryU.Execute
        qryU.Close
        qryI.Execute
        qryI.Close
        qrySIHC.Execute
        qrySIHC.Close
        If Err = 0 Then
            objApp.CommitTrans
            'actualizaci�n del grid
            Call pCargarDatosEconPA(strCodAsist, strCodProc)
        Else
            objApp.RollbackTrans
            sql = "Se ha producido un error al realizar el cambio de los datos econ�micos"
            sql = sql & " del Proceso/Asistencia."
            sql = sql & Chr$(13)
            sql = sql & "Vuelva a intentarlo."
            MsgBox sql, vbExclamation, Me.Caption
        End If
        On Error GoTo 0
    End If
End Sub

Private Sub pCambiarDatosEconRetroactivo()
    Dim strCodAsist$, strCodProc$
        
    strCodAsist = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    strCodProc = grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
    
    If fFacturado(strCodProc, strCodAsist, grdRespEconPA.Columns("Fecha Inicio").CellText(grdRespEconPA.SelBookmarks(0))) Then
        Exit Sub
    End If
    
    'Llamada a la pantalla para la introduci�n de datos econ�micos
    Call objPipe.PipeSet("AD3004_CI22NUMHISTORIA", txtNH.Text)
    Call objPipe.PipeSet("AD3004_AD01CODASISTENCI", strCodAsist)
    Call objPipe.PipeSet("AD3004_AD07CODPROCESO", strCodProc)
    Call objPipe.PipeSet("AD3004_ACCESO", 1)
    frmCambioDatosEconPA.Show vbModal
    Set frmCambioDatosEconPA = Nothing
    
    If objPipe.PipeExist("AD_TipoEcon") Then 'si existe Tipo Econ., tambi�n existen los dem�s datos
        Dim sql$, qry As rdoQuery, qrySIHC As rdoQuery
        
        sql = "UPDATE AD1100 SET CI32CODTIPECON = ?,"
        sql = sql & " CI13CODENTIDAD = ?,"
        sql = sql & " CI21CODPERSONA = ?,"
        sql = sql & " AD11INDVOLANTE = ?"
        sql = sql & " WHERE AD07CODPROCESO = ?"
        sql = sql & " AND AD01CODASISTENCI = ?"
        sql = sql & " AND AD11FECFIN IS NULL"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objPipe.PipeGet("AD_TipoEcon")
        qry(1) = objPipe.PipeGet("AD_Entidad")
        qry(2) = objPipe.PipeGet("AD_RespEcon")
        qry(3) = objPipe.PipeGet("AD_PendVol")
        qry(4) = strCodProc
        qry(5) = strCodAsist
        
        'sihc
        sql = "UPDATE Asist SET cTiEco = ?"
        sql = sql & " WHERE AD08NumCaso IN ("
        sql = sql & " SELECT AD08NumCaso "
        sql = sql & " FROM AD0800"
        sql = sql & " WHERE AD01CODASISTENCI = ?"
        sql = sql & " AND AD07CODPROCESO = ?)"
        Set qrySIHC = objApp.rdoConnect.CreateQuery("", sql)
        qrySIHC(0) = objPipe.PipeGet("AD_TipoEcon")
        qrySIHC(1) = strCodAsist
        qrySIHC(2) = strCodProc

        'eliminar los pipes
        Call objPipe.PipeRemove("AD_TipoEcon")
        Call objPipe.PipeRemove("AD_Entidad")
        Call objPipe.PipeRemove("AD_RespEcon")
        Call objPipe.PipeRemove("AD_PendVol")
        
        'actualizaci�n de la BD
        objApp.BeginTrans
        On Error Resume Next
        qry.Execute
        qry.Close
        qrySIHC.Execute
        qrySIHC.Close
        If Err = 0 Then
            objApp.CommitTrans
            'actualizaci�n del grid
            Call pCargarDatosEconPA(strCodAsist, strCodProc)
        Else
            objApp.RollbackTrans
            sql = "Se ha producido un error al realizar el cambio de los datos econ�micos"
            sql = sql & " del Proceso/Asistencia."
            sql = sql & Chr$(13)
            sql = sql & "Vuelva a intentarlo."
            MsgBox sql, vbExclamation, Me.Caption
        End If
        On Error GoTo 0
    End If
End Sub

Private Sub pCambiarTipoAsist()
    Dim sql$, qry As rdoQuery, rs As rdoResultset, qryU As rdoQuery, qryI As rdoQuery, qrySIHC As rdoQuery
    Dim strAhora$, strCodAsist$, strCodProc$, strNewTipoAsist$, strCama$
    
    'si el paciente est� ingresado, no se puede cambiar el tipo de asistencia
    If strTipoAsistActual = constASIST_HOSP And strCamaActual <> "" Then
        sql = "El paciente est� ingresado. No se puede realizar un cambio del tipo de asistencia."
        sql = sql & Chr$(13)
        sql = sql & "�nicamente podr� dar de Alta al paciente."
        MsgBox sql, vbInformation, Me.Caption
        Exit Sub
    End If
    
    strCodAsist = grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0))
    strCodProc = grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))

    'Llamada a la pantalla para la introduci�n de datos (Tipo Asist.)
    Call objPipe.PipeSet("AD3002_AD12CODTIPOASIST", strTipoAsistActual)
    frmCambioTipoAsist.Show vbModal
    Set frmCambioTipoAsist = Nothing
    
    If objPipe.PipeExist("AD3002_AD12CODTIPOASIST") Then
        strAhora = strFechaHora_Sistema
        strNewTipoAsist = objPipe.PipeGet("AD3002_AD12CODTIPOASIST")
        Call objPipe.PipeRemove("AD3002_AD12CODTIPOASIST")
        
        sql = "UPDATE AD2500 SET AD25FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
        sql = sql & " WHERE AD01CODASISTENCI = ?"
        sql = sql & " AND AD25FECFIN IS NULL"
        Set qryU = objApp.rdoConnect.CreateQuery("", sql)
        qryU(0) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qryU(1) = strCodAsist
        
        sql = "INSERT INTO AD2500 (AD01CODASISTENCI, AD25FECINICIO, AD12CODTIPOASIST)"
        sql = sql & " VALUES (?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?)"
        Set qryI = objApp.rdoConnect.CreateQuery("", sql)
        qryI(0) = strCodAsist
        qryI(1) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qryI(2) = strNewTipoAsist

        'sihc
        sql = "UPDATE Asist SET cTiAsist = ?"
        sql = sql & " WHERE AD08NumCaso IN ("
        sql = sql & " SELECT AD08NumCaso "
        sql = sql & " FROM AD0800"
        sql = sql & " WHERE AD01CODASISTENCI = ?)"
        Set qrySIHC = objApp.rdoConnect.CreateQuery("", sql)
        qrySIHC(0) = strNewTipoAsist
        qrySIHC(1) = strCodAsist
        
        'actualizaci�n de la BD
        objApp.BeginTrans
        On Error Resume Next
        qryU.Execute
        qryU.Close
        qryI.Execute
        qryI.Close
        qrySIHC.Execute
        qrySIHC.Close
        If Err = 0 Then
            objApp.CommitTrans
            'actualizaci�n del grid
            Call pCargarTipoAsist(strCodAsist)
            grdAsistencias.Columns("Tipo Asistencia").Text = grdTipoAsist.Columns("Tipo Asistencia").Text
            grdAsistencias.Columns("Cama").Text = grdTipoAsist.Columns("Cama").Text
        Else
            objApp.RollbackTrans
            sql = "Se ha producido un error al realizar el cambio de tipo de Asistencia."
            sql = sql & Chr$(13)
            sql = sql & "Vuelva a intentarlo."
            MsgBox sql, vbExclamation, Me.Caption
        End If
        On Error GoTo 0
    End If
End Sub

Private Sub pCambiarRemitente()
    Call objPipe.PipeSet("AD3003_AD01CODASISTENCI", grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)))
    Call objPipe.PipeSet("AD3003_AD07CODPROCESO", grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0)))
    frmCambioOtrosDatosPA.Show vbModal
    Set frmCambioOtrosDatosPA = Nothing
End Sub

Private Sub pImprimir(blnHojaFiliacion As Boolean, blnHojaDatos As Boolean, _
                                        blnProgramaPac As Boolean, blnHojaVerde As Boolean)
    Dim strWhere$
   
    Me.MousePointer = vbHourglass
   
    If blnHojaFiliacion Then
        strWhere = "{AD0823J.AD01CODASISTENCI} = " & grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)) & _
                " AND {AD0823J.AD07CODPROCESO} = " & grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
        With crtCrystalReport1
            .ReportFileName = objApp.strReportsPath & "filiacion2.rpt"
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
            .Destination = crptToPrinter
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            .Action = 1
        End With
    End If
    
    If blnHojaDatos Then
        strWhere = "{AD0823J.AD01CODASISTENCI} = " & grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)) & _
                " AND {AD0823J.AD07CODPROCESO} = " & grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
        With crtCrystalReport1
            .ReportFileName = objApp.strReportsPath & "ConfirmarFiliacion.rpt"
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
            .Destination = crptToPrinter
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            .Action = 1
        End With
    End If
    
    If blnProgramaPac Then
        strWhere = "{PR0460J.CI21CODPERSONA} = " & strCodPers
        With crtCrystalReport1
            .ReportFileName = objApp.strReportsPath & "ProPac1.rpt"
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
            .Destination = crptToPrinter
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            .Action = 1
        End With
    End If
    
    If blnHojaVerde Then
        strWhere = "{AD0823J.AD01CODASISTENCI} = " & grdAsistencias.Columns("N� Asistencia").CellText(grdAsistencias.SelBookmarks(0)) & _
                " AND {AD0823J.AD07CODPROCESO} = " & grdProcesos.Columns("N� Proceso").CellText(grdProcesos.SelBookmarks(0))
        With crtCrystalReport1
            .ReportFileName = objApp.strReportsPath & "hojaverde2.rpt"
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
            .Destination = crptToPrinter
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            .Action = 1
        End With
    End If
    
    Me.MousePointer = vbDefault
End Sub

Private Function fFacturado(strCodProc$, strCodAsist$, strFecha$) As Boolean
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    sql = "SELECT MAXFECHA FROM FA0418J"
    sql = sql & " WHERE AD07CODPROCESO = ?"
    sql = sql & " AND AD01CODASISTENCI = ?"
    sql = sql & " AND TO_DATE(?,'DD/MM/YYYY HH24:MI') <= MAXFECHA"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodProc
    qry(1) = strCodAsist
    qry(2) = Format(strFecha, "dd/mm/yyyy hh:mm")
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            sql = "El proceso/asistencia est� facturado hasta el " & rs(0) & "."
            sql = sql & Chr$(13)
            sql = sql & "Para realizar cualquier cambio avise a Administraci�n."
            MsgBox sql, vbInformation, Me.Caption
            fFacturado = True
        End If
    End If
    rs.Close
    qry.Close
End Function

