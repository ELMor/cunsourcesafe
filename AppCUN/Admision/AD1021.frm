VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmBuscarTipoEco 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Buscador de Responsables Econ�micos (Entidades)"
   ClientHeight    =   6660
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7380
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6660
   ScaleWidth      =   7380
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraBusqueda 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1095
      Left            =   60
      TabIndex        =   6
      Top             =   60
      Width           =   6015
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   1320
         TabIndex        =   0
         Top             =   660
         Width           =   3495
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   1320
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4920
         TabIndex        =   2
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Raz�n social:"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   8
         Top             =   720
         Width           =   960
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo:"
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   7
         Top             =   300
         Width           =   540
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Entidades localizadas"
      ForeColor       =   &H00C00000&
      Height          =   5115
      Left            =   60
      TabIndex        =   4
      Top             =   1200
      Width           =   7245
      Begin SSDataWidgets_B.SSDBGrid grdResponsables 
         Height          =   4785
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   7005
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   12356
         _ExtentY        =   8440
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   6300
      TabIndex        =   3
      Top             =   780
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "En caso de NO PODER LOCALIZAR el responsable econ�mico deseado avise a Administraci�n."
      Height          =   255
      Left            =   180
      TabIndex        =   9
      Top             =   6360
      Width           =   7035
   End
End
Attribute VB_Name = "frmBuscarTipoEco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call pFormatearGrid
End Sub

Private Sub grdResponsables_DblClick()
    If grdResponsables.Rows > 0 Then
        Call objPipe.PipeSet("DESRECO", grdResponsables.Columns("Responsable").Text)
        Call objPipe.PipeSet("TIPOECO", grdResponsables.Columns("Tipo Econ.").Text)
        Call objPipe.PipeSet("ENTIDAD", grdResponsables.Columns("Entidad").Text)
        Call objPipe.PipeSet("CODRECO", grdResponsables.Columns("C�digo").Text)
        Unload Me
    End If
End Sub

Private Sub grdResponsables_HeadClick(ByVal ColIndex As Integer)
    If grdResponsables.Rows > 0 Then Call pBuscar(ColIndex)
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then Call pBuscar(-1)
End Sub

Private Sub cmdBuscar_Click()
    Call pBuscar(-1)
End Sub

Private Sub pBuscar(intCol%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Static sqlOrder$
   
    If txtBuscar(0).Text = "" And txtBuscar(1).Text = "" Then Exit Sub
   
    Screen.MousePointer = vbHourglass
        
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY RECO_DES, CI21CODPERSONA_REC, CI32CODTIPECON, CI13CODENTIDAD"
    Else
        Select Case UCase(grdResponsables.Columns(intCol).Caption)
        Case UCase("C�digo"): sqlOrder = " ORDER BY CI21CODPERSONA_REC"
        Case UCase("Responsable"): sqlOrder = " ORDER BY RECO_DES, CI21CODPERSONA_REC, CI32CODTIPECON, CI13CODENTIDAD"
        Case UCase("Tipo Econ."), UCase("Entidad"): sqlOrder = " ORDER BY CI32CODTIPECON, CI13CODENTIDAD, RECO_DES, CI21CODPERSONA_REC"
        End Select
    End If

    SQL = "SELECT CI21CODPERSONA_REC, RECO_DES,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD"
    SQL = SQL & " FROM CI2901J"
    If txtBuscar(0).Text <> "" Then
        SQL = SQL & " WHERE CI21CODPERSONA_REC = ?"
    ElseIf txtBuscar(1).Text <> "" Then
        SQL = SQL & " WHERE UPPER(RECO_DES) LIKE ?"
    End If
    SQL = SQL & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If txtBuscar(0).Text <> "" Then
        qry(0) = txtBuscar(0).Text
    ElseIf txtBuscar(1).Text <> "" Then
        qry(0) = "%" & UCase(txtBuscar(1).Text) & "%"
    End If
    Set rs = qry.OpenResultset()
    grdResponsables.RemoveAll
    If Not rs.EOF Then
        Do While Not rs.EOF
            grdResponsables.AddItem rs!CI21CODPERSONA_REC & Chr$(9) _
                            & rs!RECO_DES & Chr$(9) _
                            & rs!CI32CODTIPECON & Chr$(9) _
                            & rs!CI13CODENTIDAD
            rs.MoveNext
        Loop
    Else
        Screen.MousePointer = vbDefault
        MsgBox "No se ha encontrado ning�n registro coincidente.", vbInformation, Me.Caption
    End If
    rs.Close
    qry.Close
    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrid()
    Dim i%
    With grdResponsables
        .Columns(0).Caption = "C�digo"
        .Columns(0).Width = 800
        .Columns(0).Alignment = ssCaptionAlignmentCenter
        .Columns(1).Caption = "Responsable"
        .Columns(1).Width = 4000
        .Columns(2).Caption = "Tipo Econ."
        .Columns(2).Width = 1000
        .Columns(2).Alignment = ssCaptionAlignmentCenter
        .Columns(3).Caption = "Entidad"
        .Columns(3).Width = 800
        .Columns(3).Alignment = ssCaptionAlignmentCenter

        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub


