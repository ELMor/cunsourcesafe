Attribute VB_Name = "modSIHC"
Option Explicit

Sub PasoSIHC(AD08NumCaso As String)
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim NH As Long, NC As Long
    
  On Error Resume Next
  
  NH = Val(Left$(AD08NumCaso, 6))
  NC = Val(Right$(AD08NumCaso, 4))
  
' Se mira si est� en la tabla PAC
  sql = "SELECT NH FROM SIHCCUN.PAC WHERE NH = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    sql = "INSERT INTO SIHCCUN.HTOPAC (FTRANS, HTRANS, NH, ACCION, AP1, AP2, NOM, DNI, SEXO, " _
        & "FNAC, CALLE, POBL, PCIA, CPAIS, CP, TFNO, FAX, SS, ESTADO) " _
        & "SELECT TRUNC(SYSDATE), TO_CHAR(SYSDATE,'HH24:MI:SS'), CI2200.CI22NUMHISTORIA, 'A', " _
        & "CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE, CI2200.CI22DNI, " _
        & "CI2200.CI30CODSEXO, CI2200.CI22FECNACIM, " _
        & "CI1000.CI10CALLE ||' '||CI1000.CI10PORTAL||' '||CI1000.CI10RESTODIREC, " _
        & "CI1000.CI10DESLOCALID, CI2600.CI26DESPROVI, CI1000.CI19CODPAIS, " _
        & "CI1000.CI07CODPOSTAL, CI1000.CI10TELEFONO, CI1000.CI10FAX, " _
        & "CI2200.CI22NUMSEGSOC, 1 " _
        & "FROM CI1000, CI2600, CI2200 WHERE " _
        & "CI1000.CI21CODPERSONA (+)= CI2200.CI21CODPERSONA AND " _
        & "CI1000.CI10NUMDIRECCI (+)= CI2200.CI22NUMDIRPRINC AND " _
        & "CI2600.CI26CODPROVI (+)= CI1000.CI26CODPROVI AND " _
        & "CI2200.CI22NUMHISTORIA = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = NH
    rdoQ.Execute
  End If

  ' Se mira si existe en la tabla pacientes
  sql = "SELECT Historia FROM PACIENTES WHERE Historia = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    sql = "INSERT INTO PACIENTES " _
        & "(HISTORIA, CASO1, NPET1, NPPENDIENTES, ULTIMAIMPRESION, ULTIMAIMPRESIONURG) " _
        & "VALUES (?, 0, 0, 0, TO_DATE('1/1/1980','DD/MM/YYYY'), " _
        & "TO_DATE('1/1/1980','DD/MM/YYYY'))"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = NH
    rdoQ.Execute
  End If

  ' Se inserta en la tabla HToAsist
  sql = "INSERT INTO SIHCCUN.HTOASIST " _
      & "(FTRANS, HTRANS, NH, NC, ACCION, CTIPAC, CTIASIST, CTIECO, CDPTRESP, CDRRESP, " _
      & "PLANTA, CAMA, REQIM, FINGRESO, HINGRESO, FALTA, HALTA, ESTADO, ESTADODOC) " _
      & "SELECT  TRUNC(SYSDATE), TO_CHAR(SYSDATE,'HH24:MI:SS'), ?, ?, 'A', " _
      & "AD0800.AD10CODTIPPACIEN, AD2500.AD12CODTIPOASIST, AD1100.CI32CODTIPECON, " _
      & "AD0500.AD02CODDPTO, AD0300.CDR, AD1500.AD02CODDPTO, " _
      & "REPLACE(GCFN06(AD1500.AD15CODCAMA),'-',''), AD0800.AD08INDREQIM, " _
      & "AD0800.AD08FECINICIO, AD0800.AD08FECINICIO, AD0800.AD08FECFIN, " _
      & "AD0800.AD08FECFIN, 1, 1 " _
      & "FROM AD2500, AD1100, AD0500, AD1500, AD0300, AD0800 WHERE " _
      & "AD0800.AD01CodAsistenci = AD2500.AD01CodAsistenci AND " _
      & "AD2500.AD25FECFIN IS NULL AND " _
      & "AD0800.AD01CodAsistenci = AD1100.AD01CodAsistenci AND " _
      & "AD0800.AD07CodProceso = AD1100.AD07CodProceso AND " _
      & "AD1100.AD11FECFIN IS NULL AND " _
      & "AD0800.AD01CodAsistenci = AD0500.AD01CodAsistenci AND " _
      & "AD0800.AD07CodProceso = AD0500.AD07CodProceso AND " _
      & "AD0500.AD05FECFINRESPON IS NULL AND " _
      & "AD0500.AD02CODDPTO = AD0300.AD02CODDPTO AND " _
      & "AD0500.SG02COD = AD0300.SG02COD AND " _
      & "AD0800.AD01CODASISTENCI = AD1500.AD01CODASISTENCI (+) AND " _
      & "AD0800.AD07CODPROCESO = AD1500.AD07CODPROCESO (+) AND " _
      & "AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA _
      & " AND AD0800.AD08NumCaso = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = NH
    rdoQ(1) = NC
    rdoQ(2) = AD08NumCaso
    rdoQ.Execute
End Sub
