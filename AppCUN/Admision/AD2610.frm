VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmCargaInicial 
   Caption         =   "Carga Inicial"
   ClientHeight    =   7500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5805
   LinkTopic       =   "Form1"
   ScaleHeight     =   7500
   ScaleWidth      =   5805
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCargar 
      Caption         =   "Cargar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2040
      TabIndex        =   5
      Top             =   6840
      Width           =   1335
   End
   Begin VB.TextBox txtUbicacion 
      Height          =   375
      Left            =   3720
      MaxLength       =   9
      TabIndex        =   3
      Top             =   120
      Width           =   1815
   End
   Begin VB.TextBox txtPtoArch 
      Height          =   375
      Left            =   1800
      MaxLength       =   2
      TabIndex        =   1
      Top             =   120
      Width           =   495
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   6045
      HelpContextID   =   2
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   5535
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   2
      stylesets.count =   1
      stylesets(0).Name=   "Urgente"
      stylesets(0).BackColor=   255
      stylesets(0).Picture=   "AD2610.frx":0000
      SelectTypeRow   =   3
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   2
      Columns(0).Width=   2170
      Columns(0).Caption=   "Historia"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6720
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9763
      _ExtentY        =   10663
      _StockProps     =   79
   End
   Begin VB.Label Label1 
      Caption         =   "Ubicacion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2640
      TabIndex        =   2
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Punto de Archivo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmCargaInicial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim strHistoria As String

Private Sub cmdCargar_Click()
Dim i As Integer
'Comprobar datos inicio
If txtPtoArch <> "01" Then
    MsgBox "Punto de Archivo Incorrecto escriba 01"
    Exit Sub
End If

If Len(txtUbicacion.Text) <> 9 Then
    MsgBox "Escriba correctamente la ubicacion con 9 digitos" & Chr$(13) & _
    "4 para el numero de linea, 5 para el numero de hueco" & Chr$(13) & _
    "ej: 004500335", vbOKOnly
    Exit Sub
End If

Call pConfirEntradas

End Sub


Private Sub Form_Load()
  grdDBGrid1(0).AddNew
End Sub

Private Sub grdDBGrid1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If grdDBGrid1(0).col = 0 Then
    If KeyCode = 13 Then
      KeyCode = 8
'     strHistoria = Left(grdDBGrid1(0).Columns(0).Value, Len(grdDBGrid1(0).Columns(0).Value) - 2)
'     intTipo = Int(Right(grdDBGrid1(0).Columns(0).Value, 2))
     Call LlenarLinea
    End If
Else
    If KeyCode = 13 Then KeyCode = 9
End If
End Sub


Private Sub LlenarLinea()
    Dim sql As String
    Dim qry As rdoQuery
    Dim rs  As rdoResultset
    Dim strHistoria$
    strHistoria = Left(grdDBGrid1(0).Columns(0).Value, Len(grdDBGrid1(0).Columns(0).Value) - 2)
    'NOMBRE
    sql = "SELECT CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL NOMBRE FROM CI2200 WHERE"
    sql = sql & " CI22NUMHISTORIA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Trim(strHistoria)
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        grdDBGrid1(0).Columns("Nombre").Value = rs(0).Value
        grdDBGrid1(0).Columns("Historia").Value = grdDBGrid1(0).Columns("Historia").Value
    Else
        MsgBox "No existe esta historia", vbCritical
        Exit Sub
    End If
    rs.Close
    qry.Close
    grdDBGrid1(0).AddNew
    SendKeys "{TAB}"
End Sub

Private Sub pConfirEntradas()
Dim i As Integer
Dim sql1 As String
Dim sql2 As String
Dim qry1 As rdoQuery
Dim qry2 As rdoQuery
Dim rdy1 As rdoResultset
Dim rdy2 As rdoResultset
Dim qyUP As rdoQuery
Dim strtipo$, strHistoria$
Dim STR As String
Dim rd As rdoResultset
Dim qy As rdoQuery

On Error Resume Next

objApp.BeginTrans
 grdDBGrid1(0).MoveFirst
 sql1 = "SELECT AR00CODTIPOLOGIA FROM AR0300 WHERE AR02CODPTOARCHIVO = ?"
 sql1 = sql1 & " AND AR03CODLINEA = ? "
 Set qry1 = objApp.rdoConnect.CreateQuery("", sql1)
    qry1(0) = txtPtoArch 'con ceros
    qry1(1) = CLng(Left(txtUbicacion, 4))
 Set rdy1 = qry1.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
 Do While grdDBGrid1(0).Columns("Historia").Value <> ""
  strHistoria = Left(grdDBGrid1(0).Columns(0).Value, Len(grdDBGrid1(0).Columns(0).Value) - 2)
  strtipo = Right(grdDBGrid1(0).Columns(0).Value, 2)

     If Not rdy1.EOF Then
      If Not IsNull(rdy1!AR00CODTIPOLOGIA) Then 'Si la tipologia no es nula en AR0300
        If strtipo <> rdy1!AR00CODTIPOLOGIA Then
          MsgBox "No se puede meter la Historia = " & strHistoria & " en este hueco"
          objApp.RollbackTrans
          Exit Sub
        End If
      End If
    Else
        MsgBox "PUNTO DE UBICACION ERRONEO", vbCritical
        objApp.RollbackTrans
        Exit Sub
    End If
    'accedemos ahora a la tabla AR0400 pto(2.1) ***Es este el pto.2.1????
    sql2 = "SELECT AR06CODESTADO FROM AR0400 WHERE CI22NUMHISTORIA=? AND AR00CODTIPOLOGIA= ? " & _
    "AND AR04FECFIN IS NULL"
    Set qry2 = objApp.rdoConnect.CreateQuery("", sql2)
      qry2(0) = CLng(strHistoria)
      qry2(1) = strtipo
    Set rdy2 = qry2.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rdy2.RowCount <> 0 Then 'Si existe el registro (IF Numero A)
      STR = "UPDATE AR0400 SET AR04FECFIN=SYSDATE WHERE " & _
      "CI22NUMHISTORIA=? AND AR00CODTIPOLOGIA=? AND AR04FECFIN IS NULL"
      Set qy = objApp.rdoConnect.CreateQuery("", STR)
         qy(0) = CLng(strHistoria)
         qy(1) = strtipo
      qy.Execute
      qy.Close
      If rdy2!AR06CODESTADO = "P" Or rdy2!AR06CODESTADO = "A" Then 'Si el Estado es P o A
        STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
        " AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO, AR04FECFIN) " & _
        "VALUES (?,?,SYSDATE,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
           qy(0) = CLng(strHistoria)
           qy(1) = strtipo
           qy(2) = "R"
           qy(3) = txtPtoArch
           qy(4) = Format(DateAdd("n", 1, strFechaHora_Sistema), "DD/MM/YYYY HH:MM:SS")
        qy.Execute
        qy.Close
         'Sea el estado o no P se realiza el siguiente INSERT
        STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
        " AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO,  " & _
        " AR03CODLINEA, AR04NUMHUECO) " & _
        "VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?)"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
           qy(0) = CLng(strHistoria)
           qy(1) = strtipo
           qy(2) = Format(DateAdd("n", 1, strFechaHora_Sistema), "DD/MM/YYYY HH:MM:SS")
           qy(3) = "U"
           qy(4) = txtPtoArch
           qy(5) = CLng(Left(txtUbicacion, 4))
           qy(6) = CLng(Right(txtUbicacion, 5))
        qy.Execute
        qy.Close
      Else
      'Sea el estado o no P se realiza el siguiente INSERT
        STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
        " AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO,  " & _
        " AR03CODLINEA, AR04NUMHUECO) " & _
        "VALUES (?,?,SYSDATE,?,?,?,?)"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
           qy(0) = CLng(strHistoria)
           qy(1) = strtipo
           qy(2) = "U"
           qy(3) = txtPtoArch
           qy(4) = CLng(Left(txtUbicacion, 4))
           qy(5) = CLng(Right(txtUbicacion, 5))
        qy.Execute
        qy.Close
     End If
    Else 'si no existe (IF Numero A)
        STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
        " AR04FECINICIO, AR06CODESTADO, AR04FECFIN) " & _
        "VALUES (?,?,SYSDATE,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
           qy(0) = CLng(strHistoria)
           qy(1) = strtipo
           qy(2) = "A"
           qy(3) = Format(DateAdd("n", 1, strFechaHora_Sistema), "DD/MM/YYYY HH:MM:SS")
        qy.Execute
        qy.Close
        STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
        " AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO, AR04FECFIN) " & _
        "VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?," & _
        "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
           qy(0) = CLng(strHistoria)
           qy(1) = strtipo
           qy(2) = Format(DateAdd("n", 1, strFechaHora_Sistema), "DD/MM/YYYY HH:MM:SS")
           qy(3) = "R"
           qy(4) = txtPtoArch
           qy(5) = Format(DateAdd("n", 2, strFechaHora_Sistema), "DD/MM/YYYY HH:MM:SS")
        qy.Execute
        qy.Close
        STR = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
        " AR04FECINICIO, AR06CODESTADO, AR02CODPTOARCHIVO, AR03CODLINEA, AR04NUMHUECO) " & _
        "VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?," & _
        "?,?)"
        Set qy = objApp.rdoConnect.CreateQuery("", STR)
           qy(0) = CLng(strHistoria)
           qy(1) = strtipo
           qy(2) = Format(DateAdd("n", 2, strFechaHora_Sistema), "DD/MM/YYYY HH:MM:SS")
           qy(3) = "U"
           qy(4) = txtPtoArch
           qy(5) = CLng(Left(txtUbicacion, 4))
           qy(6) = CLng(Right(txtUbicacion, 5))
        qy.Execute
        qy.Close
    End If '(IF Numero A)
    If err > 0 Then
        MsgBox "Error confirmando la fila " & grdDBGrid1(0).Row & Chr$(13) & _
        "Confirme los datos", vbCritical
        objApp.RollbackTrans
        Exit Sub
    End If
    grdDBGrid1(0).MoveNext
 Loop
objApp.CommitTrans
MsgBox "ENTRADAS CONFIRMADAS", vbOKOnly
grdDBGrid1(0).RemoveAll
If grdDBGrid1(0).Rows = 0 Then
    grdDBGrid1(0).AddNew
End If
SendKeys "{TAB}"
End Sub


