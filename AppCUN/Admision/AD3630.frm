VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmListPsiquiatria 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Historias de Psiquiatria"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   7575
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtHS 
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   5520
      TabIndex        =   13
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consultar"
      Height          =   255
      Left            =   2280
      TabIndex        =   10
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txtHistoria 
      Height          =   285
      Left            =   1200
      TabIndex        =   9
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton cmdCargar 
      Caption         =   "Confirmar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   8160
      Width           =   1215
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   1
      Left            =   4440
      MaxLength       =   1
      TabIndex        =   3
      Text            =   "1"
      Top             =   8160
      Width           =   255
   End
   Begin VB.TextBox txtText1 
      Height          =   285
      Index           =   0
      Left            =   3720
      MaxLength       =   2
      TabIndex        =   2
      Text            =   "1"
      Top             =   8160
      Width           =   255
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   6360
      TabIndex        =   1
      Top             =   8160
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   7605
      HelpContextID   =   2
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   7575
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   3
      stylesets.count =   1
      stylesets(0).Name=   "Urgente"
      stylesets(0).BackColor=   255
      stylesets(0).Picture=   "AD3630.frx":0000
      AllowUpdate     =   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   3
      Columns(0).Width=   900
      Columns(0).Caption=   "Dar Alta"
      Columns(0).Name =   "Dar Alta"
      Columns(0).Alignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   1455
      Columns(1).Caption=   "Historia"
      Columns(1).Name =   "Historia"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   10001
      Columns(2).Caption=   "Nombre"
      Columns(2).Name =   "Nombre"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   13361
      _ExtentY        =   13414
      _StockProps     =   79
   End
   Begin vsViewLib.vsPrinter vs 
      Height          =   255
      Left            =   5160
      TabIndex        =   11
      Top             =   8160
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   450
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.Label Label3 
      Caption         =   "Historias Seleccionadas"
      Height          =   255
      Left            =   3720
      TabIndex        =   12
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Historia Inicio"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Comienzo Impresion:"
      Height          =   195
      Left            =   1800
      TabIndex        =   7
      Top             =   8280
      Width           =   1455
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Col."
      Height          =   255
      Index           =   1
      Left            =   4080
      TabIndex        =   6
      Top             =   8280
      Width           =   255
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Fila"
      Height          =   255
      Index           =   0
      Left            =   3360
      TabIndex        =   5
      Top             =   8280
      Width           =   255
   End
End
Attribute VB_Name = "frmListPsiquiatria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cllBuscar As New Collection
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim TG(9) As String
Dim BNS As Integer
Dim BBS As Integer
Dim BAL As Integer

Private Sub cmdCargar_Click()
Call pConfirEntradas
CargarHistorias
End Sub

Private Sub cmdConsultar_Click()
If Trim(txtHistoria.Text) = "" Then
    MsgBox "Introduzca una historia Inicial"
Else
    CargarHistorias
End If
End Sub



Private Sub CargarHistorias()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim i  As Integer

grdDBGrid1.RemoveAll
txtHS = 0
Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
sql = "SELECT * FROM GC1600 WHERE GC16INDALTAARCH = 0"
sql = sql & " AND CI22NUMHISTORIA <=? "
sql = sql & " ORDER BY CI22NUMHISTORIA DESC "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(txtHistoria.Text)
Set rs = qry.OpenResultset()
i = 0
Do While Not rs.EOF
    i = i + 1
    grdDBGrid1.AddItem 0 & Chr$(9) & rs(0) & Chr$(9) _
                    & rs(1)
'    cllBuscar.Add rs(0)
    rs.MoveNext
    If i > 100 Then Exit Do
Loop

End Sub

Sub pFormatearGrid()

With grdDBGrid1
    .AllowUpdate = True
    .Columns(1).Locked = True
    .Columns(2).Locked = True
End With
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433

TG(0) = "00110"
TG(1) = "10001"
TG(2) = "01001"
TG(3) = "11000"
TG(4) = "00101"
TG(5) = "10100"
TG(6) = "01100"
TG(7) = "00011"
TG(8) = "10010"
TG(9) = "01010"

BNS = 15
BBS = 45
BAL = 700
pFormatearGrid
End Sub
Private Sub pConfirEntradas()
Dim intPX As Integer
Dim intPY As Integer
Dim i As Integer
Dim strHistoria As String

On Error GoTo Canceltrans

objApp.BeginTrans
Screen.MousePointer = vbHourglass
vs.PaperBin = 1
intPX = intPosX + (intIncX * (Int(txtText1(1).Text) - 1))
intPY = intPosY + (intIncY * (Int(txtText1(0).Text) - 1))
    vs.StartDoc
    For i = 1 To cllBuscar.Count
        strHistoria = cllBuscar(i)
        If Len(strHistoria) < 6 Then strHistoria = fstrPonerCeros(strHistoria)
        Call pAltaHistoria(cllBuscar(i))
        Call pEtiquetas(strHistoria & "04", intPX, intPY)
        Call pNombreFecha(strHistoria, "04", intPX, intPY)
        intPX = intPX + intIncX
        If intPX > 10000 Then
            intPX = intPosX
            intPY = intPY + intIncY
        End If
        If intPY > 16000 Then
            vs.EndDoc
            vs.PrintDoc
            vs.StartDoc
            intPY = intPosY
            intPX = intPosX
        End If
    Next i
vs.EndDoc
vs.PrintDoc

objApp.CommitTrans
Screen.MousePointer = vbDefault
MsgBox "ENTRADAS CONFIRMADAS", vbOKOnly
Exit Sub
Canceltrans:
    objApp.RollbackTrans
    MsgBox "Errores al dar de alta las historias", vbExclamation, Me.Caption
End Sub
Private Sub pEtiquetas(Num As String, ByVal POSX As Integer, POSY As Integer)
Dim i As Integer
Dim resto As Integer
Dim j As String
Dim str1 As String
Dim str2 As String
''Dim POSX As String
''Dim POSY As String


resto = Len(Num) - Int(Len(Num) / 2) * 2
If resto > 0 Then Num = "0" & Num
str1 = "00"
str2 = "00"

For i = 0 To (Len(Num) / 2) - 1
    j = Mid(Num, i * 2 + 1, 1)
    str1 = str1 & TG(Int(j))
    j = Mid(Num, i * 2 + 2, 1)
    str2 = str2 & TG(Int(j))
Next i
str1 = str1 & "10"
str2 = str2 & "00"
For i = 0 To Len(str1) - 1
    If Mid(str1, i + 1, 1) = 0 Then
       Call vs.DrawRectangle(POSX, POSY, POSX + BNS, POSY + BAL)
       POSX = POSX + BNS
    Else
        Call vs.DrawRectangle(POSX, POSY, POSX + BBS, POSY + BAL)
        POSX = POSX + BBS
    End If
    If Mid(str2, i + 1, 1) = 0 Then
       POSX = POSX + BNS
    Else
       POSX = POSX + BBS
    End If
Next i
vs.CurrentX = POSX + 80
vs.CurrentY = POSY + 200
End Sub
Private Sub pNombreFecha(strHistoria As String, inttip As Integer, px As Integer, py As Integer)
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String
Dim strSegApel As String
vs.Font = "Courier"
vs.FontSize = 25
vs.FontBold = True
vs.Text = Format(strHistoria, "##,##000000")
'vs.FontBold = False
vs.Font = "Courier New"
vs.FontSize = 10
vs.CurrentX = vs.CurrentX - 100
If inttip = 1 Then vs.Text = " H"
If inttip = 2 Then vs.Text = " R"
If inttip = 3 Then vs.Text = " G"
If inttip = 4 Then vs.Text = " P"
 
vs.FontSize = 10
sql = "SELECT CI22PRIAPEL,CI22SEGAPEL,CI22NOMBRE, CI22FECNACIM FROM "
sql = sql & "CI2200 WHERE CI22NUMHISTORIA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(strHistoria)
Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        vs.CurrentX = px
        vs.CurrentY = py + 730
        If IsNull(rs!CI22SEGAPEL) Then strSegApel = "" Else strSegApel = rs!CI22SEGAPEL
        If Len(rs!CI22PRIAPEL & strSegApel & rs!CI22NOMBRE) > 24 Then
            If Len(rs!CI22PRIAPEL & strSegApel) > 24 Then
                vs.Text = rs!CI22PRIAPEL
'                vs.Text = Chr$(13)
                vs.CurrentY = vs.CurrentY + 190
                vs.CurrentX = px
                If Len(rs!CI22NOMBRE & strSegApel) > 24 Then
                    vs.Text = Left(strSegApel & "," & rs!CI22NOMBRE, 26)
                Else
                    vs.Text = strSegApel & "," & rs!CI22NOMBRE
                End If
                If Not IsNull(rs!CI22FECNACIM) Then
'                    vs.Text = Chr$(13)
                    vs.CurrentY = vs.CurrentY + 190
                    vs.CurrentX = px
                    vs.Text = " " & rs!CI22FECNACIM
                End If
            Else
               vs.Text = rs!CI22PRIAPEL & ", " & strSegApel & ","
'               vs.Text = Chr$(13)
                vs.CurrentY = vs.CurrentY + 190
               vs.CurrentX = px
               vs.Text = rs!CI22NOMBRE
               If Not IsNull(rs!CI22FECNACIM) Then
'                    vs.Text = Chr$(13)
                    vs.CurrentY = vs.CurrentY + 190
                    vs.CurrentX = px
                    vs.Text = " " & rs!CI22FECNACIM
                End If
            End If
        Else
            vs.Text = rs!CI22PRIAPEL & ", " & strSegApel & ", " & rs!CI22NOMBRE
'            vs.Text = Chr$(13)
            vs.CurrentY = vs.CurrentY + 190
            vs.CurrentX = px
            If Not IsNull(rs!CI22FECNACIM) Then
                vs.Text = " " & rs!CI22FECNACIM
            End If
        End If
    End If
rs.Close
qry.Close

End Sub
Private Function fstrPonerCeros(strH As String) As String
Dim l As Integer
Dim i As Integer
fstrPonerCeros = strH
l = 6 - Len(strH)
For i = 0 To l - 1
    fstrPonerCeros = "0" & fstrPonerCeros
Next i
End Function

Private Sub pAltaHistoria(strH As String)
Dim i As Integer
Dim strtipo$, strHistoria$
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql As String
strtipo = "04"

sql = "SELECT COUNT(*) FROM AR0400 WHERE CI22NUMHISTORIA = ? "
sql = sql & " AND  AR00CODTIPOLOGIA = '04' "
sql = sql & " AND AR06CODESTADO = 'A'"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strH
Set rs = qry.OpenResultset()
If rs(0) = 0 Then
    rs.Close
    qry.Close
    sql = "INSERT INTO AR0400 (CI22NUMHISTORIA,AR00CODTIPOLOGIA,"
    sql = sql & " AR04FECINICIO,AR06CODESTADO,AR02CODPTOARCHIVO)"
    sql = sql & " VALUES(?,'04',SYSDATE,'A','01')"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strH
    qry.Execute
    If Err > 0 Or qry.RowsAffected = 0 Then
        MsgBox Error & " H: " & strH, vbExclamation
    End If
    qry.Close
End If

sql = "UPDATE GC1600 SET GC16INDALTAARCH =-1 WHERE CI22NUMHISTORIA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strH
qry.Execute
qry.Close

End Sub




Private Sub grdDBGrid1_Change()
Dim i As Integer

    If grdDBGrid1.Columns(0).Value = -1 Then
        cllBuscar.Add grdDBGrid1.Columns("Historia").Value
        txtHS = cllBuscar.Count
    Else
        For i = 1 To cllBuscar.Count
            If cllBuscar(i) = grdDBGrid1.Columns("Historia").Value Then
                cllBuscar.Remove i
                Exit For
            End If
        Next i
        txtHS = cllBuscar.Count
    End If
End Sub
