VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmVolanteAportado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Entrega de Volantes pendientes"
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10605
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   10605
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   9480
      TabIndex        =   3
      Top             =   2760
      Width           =   915
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   9480
      TabIndex        =   2
      Top             =   180
      Width           =   915
   End
   Begin VB.Frame Frame1 
      Caption         =   "Volantes pendientes"
      ForeColor       =   &H00FF0000&
      Height          =   3075
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9315
      Begin SSDataWidgets_B.SSDBGrid grdPendVol 
         Height          =   2715
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   9075
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16007
         _ExtentY        =   4789
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmVolanteAportado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodAsist$, strCodProc$

Private Sub cmdAceptar_Click()
    Call pGuardar
    Unload Me
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    strCodAsist = objPipe.PipeGet("CodAsist_PendVol")
    objPipe.PipeRemove ("CodAsist_PendVol")
    strCodProc = objPipe.PipeGet("CodProc_PendVol")
    objPipe.PipeRemove ("CodProc_PendVol")

    Call pFormatearGrid
    Call pCargarVolantesPed
End Sub

Private Sub pCargarVolantesPed()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strFI$, strFF$
    
    SQL = "SELECT CI32CODTIPECON, CI13CODENTIDAD,"
    SQL = SQL & " CI21CODPERSONA, ADFN01(CI21CODPERSONA) RESP,"
    SQL = SQL & " NVL(AD11INDVOLANTE,0) AD11INDVOLANTE,"
    SQL = SQL & " AD11FECINICIO, AD11FECFIN"
    SQL = SQL & " FROM AD1100"
    SQL = SQL & " WHERE AD01CODASISTENCI = ?"
    SQL = SQL & " AND AD07CODPROCESO = ?"
    SQL = SQL & " AND AD11INDVOLANTE = -1"
    SQL = SQL & " ORDER BY AD11FECINICIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAsist
    qry(1) = strCodProc
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If Not IsNull(rs!AD11FECINICIO) Then strFI = Format(rs!AD11FECINICIO, "dd/mm/yyyy hh:mm:ss") Else strFI = ""
        If Not IsNull(rs!AD11FECFIN) Then strFF = Format(rs!AD11FECFIN, "dd/mm/yyyy hh:mm:ss") Else strFF = ""
        grdPendVol.AddItem rs!CI32CODTIPECON & " - " & rs!CI13CODENTIDAD & Chr$(9) _
                        & rs!RESP & Chr$(9) _
                        & strFI & Chr$(9) _
                        & strFF & Chr$(9) _
                        & -rs!AD11INDVOLANTE
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pFormatearGrid()
    With grdPendVol
        .StyleSets.Add ("blocked")
        .StyleSets("blocked").BackColor = objApp.objUserColor.lngReadOnly
        .StyleSets.Add ("mandatory")
        .StyleSets("normal").BackColor = objApp.objUserColor.lngNormal
        
        .Columns(0).Caption = "Tipo Econ."
        .Columns(0).Width = 1200
        .Columns(0).StyleSet = "blocked"
        .Columns(1).Caption = "Responsable"
        .Columns(1).Width = 3000
        .Columns(1).StyleSet = "blocked"
        .Columns(2).Caption = "Fecha Inicio"
        .Columns(2).Width = 1650
        .Columns(2).StyleSet = "blocked"
        .Columns(3).Caption = "Fecha Fin"
        .Columns(3).Width = 1650
        .Columns(3).StyleSet = "blocked"
        .Columns(4).Caption = "Pendiente"
        .Columns(4).Width = 900
        .Columns(4).StyleSet = "normal"
        .Columns(4).Style = ssStyleCheckBox
    End With
End Sub

Private Sub pGuardar()
    Dim i%, cllFechas As New Collection

    LockWindowUpdate grdPendVol.hWnd

    For i = 1 To grdPendVol.Rows
        If i = 1 Then grdPendVol.MoveFirst Else grdPendVol.MoveNext
        If grdPendVol.Columns("Pendiente").Value = 0 Then
            cllFechas.Add grdPendVol.Columns("Fecha Inicio").Value
        End If
    Next i
    
    If cllFechas.Count > 0 Then
        Dim SQL$, qry As rdoQuery, item As Variant
        
        SQL = "UPDATE AD1100 SET AD11INDVOLANTE = 0"
        SQL = SQL & " WHERE AD07CODPROCESO = ?"
        SQL = SQL & " AND AD01CODASISTENCI = ?"
        SQL = SQL & " AND AD11INDVOLANTE = -1"
        SQL = SQL & " AND ("
        For Each item In cllFechas
            SQL = SQL & "AD11FECINICIO = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') OR "
        Next
        SQL = Left$(SQL, Len(SQL) - 4)
        SQL = SQL & ")"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodProc
        qry(1) = strCodAsist
        i = 1
        For Each item In cllFechas
            i = i + 1
            qry(i) = Format(item, "dd/mm/yyyy hh:mm:ss")
        Next
        qry.Execute
        qry.Close
    End If

    LockWindowUpdate 0&
End Sub
