VERSION 5.00
Begin VB.Form frmFotos 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   Caption         =   "Tablas de Medicamentos"
   ClientHeight    =   8190
   ClientLeft      =   -75
   ClientTop       =   960
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8190
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.PictureBox tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      ScaleHeight     =   360
      ScaleWidth      =   11970
      TabIndex        =   0
      Top             =   0
      Width           =   12000
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   1
      Left            =   120
      TabIndex        =   35
      Top             =   8100
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   0
      Left            =   120
      TabIndex        =   34
      Top             =   8100
      Width           =   11895
   End
   Begin VB.PictureBox tabSSTab1 
      Height          =   7755
      Left            =   60
      ScaleHeight     =   7725
      ScaleWidth      =   11865
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   360
      Width           =   11895
      Begin VB.PictureBox SSFrame1 
         Height          =   7095
         Left            =   180
         ScaleHeight     =   7065
         ScaleWidth      =   11505
         TabIndex        =   9
         Top             =   420
         Width           =   11535
         Begin VB.ComboBox cboCombo1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_GRUPO"
            Height          =   300
            Index           =   1
            Left            =   480
            Style           =   2  'Dropdown List
            TabIndex        =   38
            Top             =   4560
            Width           =   1770
         End
         Begin VB.ComboBox cboCombo1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_PROVEEDOR"
            Height          =   300
            Index           =   0
            Left            =   480
            Style           =   2  'Dropdown List
            TabIndex        =   37
            Top             =   3900
            Width           =   1770
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_ENVASE"
            Height          =   285
            HelpContextID   =   30101
            Index           =   5
            Left            =   3120
            MaxLength       =   5
            TabIndex        =   24
            Tag             =   "Envase | Envase del Medicamento"
            Top             =   5250
            Width           =   1245
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_GRUPO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   8
            Left            =   2340
            MaxLength       =   36
            TabIndex        =   23
            Tag             =   "Grupo | Grupo del Medicamento"
            Top             =   4560
            Width           =   3315
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_PROVEEDOR"
            Height          =   285
            HelpContextID   =   30101
            Index           =   7
            Left            =   2340
            MaxLength       =   36
            TabIndex        =   22
            Tag             =   "Proveedor | Proveedor del Medicamento"
            Top             =   3900
            Width           =   3315
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_VVENT"
            Height          =   285
            HelpContextID   =   30101
            Index           =   6
            Left            =   500
            MaxLength       =   15
            TabIndex        =   5
            Tag             =   "Vvent | Vvent del Medicamento"
            Top             =   5940
            Width           =   2235
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_UBICACION"
            Height          =   285
            HelpContextID   =   30101
            Index           =   4
            Left            =   500
            MaxLength       =   8
            TabIndex        =   4
            Tag             =   "Ubicaci�n|Ubicaci�n del Medicamento"
            Top             =   5280
            Width           =   1245
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR01_CODIGO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   0
            Left            =   500
            MaxLength       =   12
            TabIndex        =   1
            Tag             =   "C�digo|C�digo Medicamento"
            Top             =   570
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "FR01_REFERENCIA"
            Height          =   285
            HelpContextID   =   30101
            Index           =   1
            Left            =   500
            MaxLength       =   15
            TabIndex        =   2
            Tag             =   "Referencia|Referencia del Medicamento"
            Top             =   1140
            Width           =   2175
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_DESCRIPCION"
            Height          =   1155
            HelpContextID   =   30101
            Index           =   2
            Left            =   500
            MaxLength       =   300
            MultiLine       =   -1  'True
            TabIndex        =   3
            Tag             =   "Descripcion | Descripcion Medicamento"
            Top             =   1740
            Width           =   5085
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_NOMBREFOTO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   3
            Left            =   500
            MaxLength       =   40
            TabIndex        =   6
            Tag             =   "Archivo Foto | Foto del Medicamento"
            Top             =   3240
            Width           =   4725
         End
         Begin VB.CommandButton Command1 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   5250
            TabIndex        =   10
            Top             =   3210
            Width           =   345
         End
         Begin VB.PictureBox SSDateCombo1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_FECHAALTA"
            Height          =   330
            Index           =   0
            Left            =   3120
            ScaleHeight     =   300
            ScaleWidth      =   1830
            TabIndex        =   36
            Tag             =   "Fecha Alta"
            Top             =   5880
            Width           =   1860
         End
         Begin VB.PictureBox CmDialog1 
            Height          =   480
            Left            =   5430
            ScaleHeight     =   450
            ScaleWidth      =   1170
            TabIndex        =   39
            Top             =   330
            Width           =   1200
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   9
            Left            =   480
            TabIndex        =   21
            Top             =   4320
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Proveedor"
            Height          =   255
            Index           =   8
            Left            =   480
            TabIndex        =   20
            Top             =   3690
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de Alta"
            DataField       =   "FR01_FALTA"
            Height          =   255
            Index           =   7
            Left            =   3120
            TabIndex        =   19
            Top             =   5640
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "VVent"
            Height          =   255
            Index           =   6
            Left            =   480
            TabIndex        =   18
            Top             =   5700
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Envase"
            Height          =   255
            Index           =   5
            Left            =   3120
            TabIndex        =   17
            Top             =   4980
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Ubicaci�n"
            Height          =   255
            Index           =   4
            Left            =   480
            TabIndex        =   16
            Top             =   5040
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   14
            Top             =   330
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Referencia"
            Height          =   255
            Index           =   1
            Left            =   480
            TabIndex        =   13
            Top             =   900
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Descripcion"
            Height          =   255
            Index           =   2
            Left            =   480
            TabIndex        =   12
            Top             =   1500
            Width           =   1455
         End
         Begin VB.Image Image1 
            BorderStyle     =   1  'Fixed Single
            Height          =   6465
            Left            =   5940
            Stretch         =   -1  'True
            Top             =   360
            Width           =   5355
         End
         Begin VB.Label Label1 
            Caption         =   "Foto"
            DataField       =   "GC50_FOTO"
            Height          =   255
            Index           =   3
            Left            =   480
            TabIndex        =   11
            Tag             =   "Foto | Path de la Foto"
            Top             =   3030
            Width           =   1455
         End
      End
      Begin VB.PictureBox SSFrame2 
         Height          =   7215
         Left            =   -74880
         ScaleHeight     =   7185
         ScaleWidth      =   11625
         TabIndex        =   25
         Top             =   420
         Width           =   11655
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   2
            Left            =   5820
            MaxLength       =   40
            TabIndex        =   33
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   1860
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   5
            Left            =   3030
            MaxLength       =   40
            TabIndex        =   32
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   6606
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   3
            Left            =   8670
            MaxLength       =   40
            TabIndex        =   31
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   1860
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   0
            Left            =   60
            MaxLength       =   40
            TabIndex        =   30
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   1950
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   1
            Left            =   3060
            MaxLength       =   40
            TabIndex        =   29
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   1920
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   4
            Left            =   0
            MaxLength       =   40
            TabIndex        =   28
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   6666
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   6
            Left            =   5850
            MaxLength       =   40
            TabIndex        =   27
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   6426
            Width           =   2055
         End
         Begin VB.TextBox txtMedicamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            HelpContextID   =   30101
            Index           =   7
            Left            =   8610
            MaxLength       =   40
            TabIndex        =   26
            Tag             =   "Designacion|Designacion Medicamento"
            Top             =   6366
            Width           =   2055
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   7
            Left            =   8790
            Stretch         =   -1  'True
            Top             =   3870
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   6
            Left            =   5880
            Stretch         =   -1  'True
            Top             =   3840
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   5
            Left            =   3150
            Stretch         =   -1  'True
            Top             =   3840
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   4
            Left            =   300
            Stretch         =   -1  'True
            Top             =   3840
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   3
            Left            =   8700
            Stretch         =   -1  'True
            Top             =   30
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   2
            Left            =   5850
            Stretch         =   -1  'True
            Top             =   0
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   1
            Left            =   3000
            Stretch         =   -1  'True
            Top             =   30
            Width           =   1995
         End
         Begin VB.Image Image2 
            BorderStyle     =   1  'Fixed Single
            Height          =   1875
            Index           =   0
            Left            =   300
            Stretch         =   -1  'True
            Top             =   60
            Width           =   1995
         End
      End
      Begin VB.PictureBox grdSSDBGrid1 
         BackColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   -74400
         ScaleHeight     =   165
         ScaleWidth      =   10725
         TabIndex        =   15
         Top             =   360
         Visible         =   0   'False
         Width           =   10755
      End
   End
   Begin VB.PictureBox stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   750
      Left            =   0
      ScaleHeight     =   720
      ScaleWidth      =   11970
      TabIndex        =   7
      Top             =   7440
      Width           =   12000
   End
   Begin VB.Image Image3 
      BorderStyle     =   1  'Fixed Single
      Height          =   8325
      Index           =   0
      Left            =   2040
      Stretch         =   -1  'True
      Top             =   30
      Visible         =   0   'False
      Width           =   8115
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edicion"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener"
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero                              CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior                              Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente                           Av Pag"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo                                CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&RefrescarRegistros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de ayuda                              F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFotos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public objWinActiveFormInfo As Object   ' formulario activo
Public objDetailInfo As New clsCWForm
Dim objAsistInfo As New clsCWForm
Dim objPruebaInfo As New clsCWForm
Public objWinInfo As New clsCWWin ' realizar una instancia de la clase ventana de la libreria
Dim nreg As Integer, numpag As Integer
Dim pagina() As Variant
Dim nver As Integer
Dim nhor As Integer
Dim anch As Double
Dim alt As Double
Dim sepve As Double
Dim sepho As Double
Dim nfotospagina
Dim paginactual As Integer
Dim primera As Integer
Dim posic As Integer 'posicion del cursor :inicio 0 o final de pagina 1
'?FRMFOTOS.objDetailInfo.RDOSTATEMENT.SQL
Dim navegacion As Boolean 'indica si se esta navegando
'desde la 1� pag (true) o desde la ultima (false)

Dim nfot As Integer

Sub PagePrevious()
    Dim i%, j%, q%, X%, txt$
  
    aviso2 = False
    Text1(1).Text = objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    If aviso = True Then Exit Sub
    
    If posic = 1 Then
        For i = 1 To nfot + 2
            Call objWinInfo.MovePrevious
        Next i
        If frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code1 Then Call objWinInfo.MovePrevious
        posic = 0
    End If
    
    If frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code1 Then aviso = True
    
    'Al cambiarnos de pagina borramos lo que habia
    For q = 0 To nfotospagina - 1
        frmFotos.Image2(q).Visible = False
        frmFotos.txtMedicamento(q).Visible = False
        frmFotos.Image2(q).Picture = LoadPicture()
    Next q
    
    q = 7
    
    SSFrame2.Visible = False
    For i = nver To 1 Step -1
        For j = nhor To 1 Step -1
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
             frmFotos.Image2(q).Left = j * sepho * 567 + ((11955 - (nhor + 1) * (sepho * 567)) / nhor) * (j - 1)
             frmFotos.Image2(q).Top = i * sepve * 567 + ((7400 - (nver + 1) * (sepve * 567)) / nver) * (i - 1)
             frmFotos.Image2(q).Width = ((11955 - (nhor + 1) * (sepho * 567)) / nhor)
             frmFotos.Image2(q).Height = ((7400 - (nver + 1) * (sepve * 567)) / nver)
             If Not IsNull(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                 On Error Resume Next
                 frmFotos.Image2(q).Picture = LoadPicture(Trim$(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                 'frmFotos.Image2(q).Picture = LoadPicture(Trim$(grdSSDBGrid1.Columns(5).Text))
                 Err = 0
             End If
             
             frmFotos.txtMedicamento(q).Left = frmFotos.Image2(q).Left
             frmFotos.txtMedicamento(q).Top = frmFotos.Image2(q).Top + frmFotos.Image2(q).Height + 50
             frmFotos.txtMedicamento(q).Width = frmFotos.Image2(q).Width
             txt = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
'            frmFotos.txtMedicamento(q).Text = grdSSDBGrid1.Columns(0).Text & " - " & grdSSDBGrid1.Columns(1).Text
            
             q = q - 1
             'frmFotos.objDetailInfo.rdoCursor.MoveNext
             If objDetailInfo.blnDirection = False Then
'                Call objWinInfo.stdWinProcess(cwProcessToolBar, 22, 0)
                Call objWinInfo.MovePrevious
             Else
'                Call objWinInfo.stdWinProcess(cwProcessToolBar, 23, 0)
                Call objWinInfo.MovePrevious
             End If
             
            frmFotos.txtMedicamento(q + 1).Text = txt
                       
            If aviso = True Then
                nfot = 7 - q - 2
                For X = 7 To q + 1 Step -1
                    frmFotos.Image2(X).Visible = True
                    frmFotos.txtMedicamento(X).Visible = True
                Next X
                SSFrame2.Visible = True
                Text1(1).Text = Text1(1).Text & "nfot" & nfot
                Text1(1).Text = Text1(1).Text & "fin:" & objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
                Screen.MousePointer = 0
                Exit Sub
            End If
            
            If frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code1 Then aviso = True
 
            Text1(0).Text = frmFotos.objDetailInfo.rdoStatement.SQL
            If frmFotos.objDetailInfo.rdoCursor.EOF = True Then
'                frmfotos.frame1.Caption = "Registros N� " & nfotospagina * (page - 1) + 1 & " a " & nfotospagina * (page - 1) + q
'                frmfotos.Frame1.Caption = "P�gina " & page & " de " & numpag
                'frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
                'Exit Sub
            End If
        Next j
    Next i
    
    Text1(1).Text = Text1(1).Text & "fin:" & objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    For q = 0 To 7
        frmFotos.Image2(q).Visible = True
        frmFotos.txtMedicamento(q).Visible = True
    Next q
    
    SSFrame2.Visible = True
    nfot = 7
    Text1(1).Text = Text1(1).Text & "nfot" & nfot
    Screen.MousePointer = 0
End Sub
Sub PageLast()
    
    Dim i%, j%, q%, X%, txt
    
    navegacion = False
    q = 0
    
    If aviso2 = True Then Exit Sub
    aviso2 = True
    aviso = False
    'Al cambiarnos de pagina borramos lo que habia
    For q = 0 To nfotospagina - 1
        frmFotos.Image2(q).Visible = False
        frmFotos.txtMedicamento(q).Visible = False
        frmFotos.Image2(q).Picture = LoadPicture()
    Next q
    posic = 0
'    Call objWinInfo.stdWinProcess(cwProcessToolBar, 24, 0)
    Call objWinInfo.MoveLast
    
    code2 = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    q = 7
    SSFrame2.Visible = False
    For i = nver To 1 Step -1
        For j = nhor To 1 Step -1
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
            frmFotos.Image2(q).Left = j * sepho * 567 + ((11625 - (nhor + 1) * (sepho * 567)) / nhor) * (j - 1)
            frmFotos.Image2(q).Top = i * sepve * 567 + ((7100 - (nver + 1) * (sepve * 567)) / nver) * (i - 1)
            frmFotos.Image2(q).Width = ((11625 - (nhor + 1) * (sepho * 567)) / nhor)
            frmFotos.Image2(q).Height = ((7100 - (nver + 1) * (sepve * 567)) / nver)
            If Not IsNull(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                On Error Resume Next
                frmFotos.Image2(q).Picture = LoadPicture(Trim$(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                'frmFotos.Image2(q).Picture = LoadPicture(Trim$(grdSSDBGrid1.Columns(5).Text))
                Err = 0
            End If
            
            frmFotos.txtMedicamento(q).Left = frmFotos.Image2(q).Left
            frmFotos.txtMedicamento(q).Top = frmFotos.Image2(q).Top + frmFotos.Image2(q).Height + 50
            frmFotos.txtMedicamento(q).Width = frmFotos.Image2(q).Width
            frmFotos.txtMedicamento(q).Text = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
'           frmFotos.txtMedicamento(q).Text = grdSSDBGrid1.Columns(0).Text & " - " & grdSSDBGrid1.Columns(1).Text

            q = q - 1
            'frmFotos.objDetailInfo.rdoCursor.MoveNext
            'Call objWinInfo.stdWinProcess(cwProcessToolBar, 22, 0)
            Call objWinInfo.MovePrevious
            frmFotos.txtMedicamento(q).Text = txt
            Text1(0).Text = frmFotos.objDetailInfo.rdoStatement.SQL
'            code3 = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
            
        Next j
    Next i
    
    For q = 0 To 7
        frmFotos.Image2(q).Visible = True
        frmFotos.txtMedicamento(q).Visible = True
    Next q
    
    SSFrame2.Visible = True
    nfot = 7
    Text1(1).Text = Text1(1).Text & "nfot" & nfot
    Screen.MousePointer = 0
     
End Sub

Private Function LoadProveedores()

Dim strSQL As String
Dim rdoCursor As rdoResultset

strSQL = "select fr03_cproveedor,fr03_proveedor from fr0300 order by fr03_Cproveedor"
Set rdoCursor = objApp.rdoAppConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurValues)
If rdoCursor.RowCount > 0 Then
    rdoCursor.MoveFirst
    Do While Not rdoCursor.EOF
    '   SSDBCombo1(0).AddItem (rdoCursor.rdoColumns("fr03_cproveedor").Value & Chr(9) & _
             rdoCursor.rdoColumns("fr03_proveedor").Value)
        rdoCursor.MoveNext
    Loop
    'SSDBCombo1(0).MoveFirst
    'SSDBCombo1(0).FirstRow = SSDBCombo1(0).Bookmark
End If

End Function

Private Function LoadGrupos()

Dim strSQL As String
Dim rdoCursor As rdoResultset

strSQL = "select fr02_cgrupo,fr02_grupo from fr0200 order by fr02_Cgrupo"
Set rdoCursor = objApp.rdoAppConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurValues)
If rdoCursor.RowCount > 0 Then
    rdoCursor.MoveFirst
    Do While Not rdoCursor.EOF
       'SSDBCombo1(1).AddItem (rdoCursor.rdoColumns("fr02_cgrupo").Value & Chr(9) & _
             rdoCursor.rdoColumns("fr02_grupo").Value)
        rdoCursor.MoveNext
    Loop
    'SSDBCombo1(1).MoveFirst
    'SSDBCombo1(1).FirstRow = SSDBCombo1(1).Bookmark
End If

End Function

Function LeerDimPagina()

    Dim res%, i%, j%
    Dim alto As String * 5
    Dim ancho As String * 5
    Dim sepver As String * 5
    Dim sephor As String * 5
    
   ' res = GetPrivateProfileString("config", "anchura", "6", ancho, 5, "C:\FARMACIA\DIMFOTOS.INI")
   ' res = GetPrivateProfileString("config", "altura", "6", alto, 5, "C:\FARMACIA\DIMFOTOS.INI")
   ' res = GetPrivateProfileString("config", "sephoriz", "1", sephor, 5, "C:\FARMACIA\DIMFOTOS.INI")
   ' res = GetPrivateProfileString("config", "sepverti", "1", sepver, 5, "C:\FARMACIA\DIMFOTOS.INI")
   '
   ' anch = CDbl(Left(ancho, InStr(ancho, Chr(0)) - 1))
   ' alt = CDbl(Left(alto, InStr(alto, Chr(0)) - 1))
   ' sepve = CDbl(Left(sephor, InStr(sephor, Chr(0)) - 1))
   ' sepho = CDbl(Left(sepver, InStr(sepver, Chr(0)) - 1))
    anch = 2
    alt = 2
    sepve = 1
    sepho = 1
   
    nhor = Int((8000 - 567 * sepho) / (anch * 567 + sepho * 567))
    nver = Int((6000 - 465 - 567 * sepve) / (alt * 567 + sepve * 567))
    
    nfotospagina = nhor * nver
    
End Function
Function Movepage(page As Integer)

    Dim i%, j%, q%, X%
    q = 0
   
    'Al cambiarnos de pagina borramos lo que habia
    For q = 0 To nfotospagina - 1
        frmFotos.Image2(q).Visible = False
        frmFotos.txtMedicamento(q).Visible = False
        frmFotos.Image2(q).Picture = LoadPicture()
    Next q
        




Dim bkmrk As Variant ' Bookmarks are always defined as variants

'nTotalSelRows = GRDSSDBGrid1.SelBookmarks.Count
' grdSSDBGrid1.MoveFirst
 
 'For X = VIS To SSDBGrid1.VisibleRows - 1
 '  SSDBGrid1.SelBookmarks.Add SSDBGrid1.RowBookmark(X)
 'Next X

'For X = 0 To 1
   'grdSSDBGrid1.SelBookmarks.Add grdSSDBGrid1.RowBookmark(X)
  
'   frmfotos.txtMedicamento(X).Text = frmfotos.grdSSDBGrid1.Columns(1).Text
'    grdSSDBGrid1.MoveNext
'Next X
    
' In the following, get the bookmark of the selected rows
'    For i = 0 To grdSSDBGrid1.SelBookmarks.Count


 '     bkmrk = grdSSDBGrid1.SelBookmarks(i)
      ' bkmrk = grdSSDBGrid1.RowBookmark(i)
      'frmfotos.objDetailInfo.rdoCursor.Bookmark = bkmrk
      'frmfotos.txtMedicamento(i).Text = frmfotos.objDetailInfo.rdoCursor.rdoColumns("FR01_CODIGO")
 '     frmfotos.txtMedicamento(i).Text = frmfotos.grdSSDBGrid1.Columns(1).CellValue(bkmrk)
      'grdSSDBGrid1.MoveNext
 '   Next i


    If frmFotos.objDetailInfo.rdoCursor.RowCount > 0 Then
        frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
        paginactual = page
        q = 0
        For i = 1 To nver
            For j = 1 To nhor
                If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
                 frmFotos.Image2(q).Left = j * sepho * 567 + ((11625 - (nhor + 1) * (sepho * 567)) / nhor) * (j - 1)
                 frmFotos.Image2(q).Top = i * sepve * 567 + ((7100 - (nver + 1) * (sepve * 567)) / nver) * (i - 1)
                 frmFotos.Image2(q).Width = ((11625 - (nhor + 1) * (sepho * 567)) / nhor)
                 frmFotos.Image2(q).Height = ((7100 - (nver + 1) * (sepve * 567)) / nver)
                 If Not IsNull(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                     On Error Resume Next
                     frmFotos.Image2(q).Picture = LoadPicture(Trim$(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                     'frmFotos.Image2(q).Picture = LoadPicture(Trim$(grdSSDBGrid1.Columns(5).Text))
                     Err = 0
                 End If
                 frmFotos.Image2(q).Visible = True
                 frmFotos.txtMedicamento(q).Left = frmFotos.Image2(q).Left
                 frmFotos.txtMedicamento(q).Top = frmFotos.Image2(q).Top + frmFotos.Image2(q).Height + 50
                 frmFotos.txtMedicamento(q).Width = frmFotos.Image2(q).Width
                 frmFotos.txtMedicamento(q).Text = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
'                 frmFotos.txtMedicamento(q).Text = grdSSDBGrid1.Columns(0).Text & " - " & grdSSDBGrid1.Columns(1).Text

                 frmFotos.txtMedicamento(q).Visible = True
                 q = q + 1
                 frmFotos.objDetailInfo.rdoCursor.MoveNext
                 'Call objWinInfo.stdWinProcess(cwProcessToolBar, 23, 0)
    
                If frmFotos.objDetailInfo.rdoCursor.EOF = True Then
    '                frmfotos.frame1.Caption = "Registros N� " & nfotospagina * (page - 1) + 1 & " a " & nfotospagina * (page - 1) + q
    '                frmfotos.Frame1.Caption = "P�gina " & page & " de " & numpag
                    frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
                    Exit Function
                End If
            Next j
        Next i
        frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
    '         frmfotos.Frame1.Caption = "P�gina " & page & " de " & numpag
    '        FRMARCHMEDICAMENTOS.pRegistros.Caption = "Registros N� " & nfotospagina * (page - 1) + 1 & " a " & nfotospagina * (page - 1) + q
    Else
    '         frmfotos.Frame1.Caption = "P�gina 0 de 0"
    '        FRMARCHMEDICAMENTOS.pRegistros.Caption = "Ning�n Registro Seleccionado"
    End If
End Function
Sub calculonumpag()
Dim i As Integer, j%

nreg = frmFotos.objDetailInfo.rdoCursor.RowCount

If nreg / nfotospagina > Int(nreg / nfotospagina) Then
    numpag = Int(nreg / nfotospagina) + 1
Else
    numpag = Int(nreg / nfotospagina)
End If

ReDim Preserve pagina(1 To numpag)
'frmFotos.objDetailInfo.rdoCursor.MoveFirst
Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
pagina(1) = frmFotos.objDetailInfo.rdoCursor.Bookmark

For i = 2 To numpag
    For j = 1 To nfotospagina
        'frmFotos.objDetailInfo.rdoCursor.MoveNext
        Call objWinInfo.WinProcess(cwProcessToolBar, 23, 0)
    Next j
    pagina(i) = frmFotos.objDetailInfo.rdoCursor.Bookmark
Next i

End Sub

Sub PageFirst()

    Dim i%, j%, q%, X%, txt$
    navegacion = True
    q = 0
    
    If aviso = True Then Exit Sub
    aviso = True
    aviso2 = False
    'Al cambiarnos de pagina borramos lo que habia
    For q = 0 To nfotospagina - 1
        frmFotos.Image2(q).Visible = False
        frmFotos.txtMedicamento(q).Visible = False
        frmFotos.Image2(q).Picture = LoadPicture()
    Next q
    
    'Call objWinInfo.stdWinProcess(cwProcessToolBar, 21, 0)
    Call objWinInfo.MoveFirst
    code1 = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    posic = 1
    q = 0
    
    SSFrame2.Visible = False
    For i = 1 To nver
        For j = 1 To nhor
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
             frmFotos.Image2(q).Left = j * sepho * 567 + ((11625 - (nhor + 1) * (sepho * 567)) / nhor) * (j - 1)
             frmFotos.Image2(q).Top = i * sepve * 567 + ((7100 - (nver + 1) * (sepve * 567)) / nver) * (i - 1)
             frmFotos.Image2(q).Width = ((11625 - (nhor + 1) * (sepho * 567)) / nhor)
             frmFotos.Image2(q).Height = ((7100 - (nver + 1) * (sepve * 567)) / nver)
             If Not IsNull(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                 On Error Resume Next
                 frmFotos.Image2(q).Picture = LoadPicture(Trim$(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                 'frmFotos.Image2(q).Picture = LoadPicture(Trim$(grdSSDBGrid1.Columns(5).Text))
                 Err = 0
             End If
            
             frmFotos.txtMedicamento(q).Left = frmFotos.Image2(q).Left
             frmFotos.txtMedicamento(q).Top = frmFotos.Image2(q).Top + frmFotos.Image2(q).Height + 50
             frmFotos.txtMedicamento(q).Width = frmFotos.Image2(q).Width
             txt = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
'                 frmFotos.txtMedicamento(q).Text = grdSSDBGrid1.Columns(0).Text & " - " & grdSSDBGrid1.Columns(1).Text

            
             q = q + 1
             'frmFotos.objDetailInfo.rdoCursor.MoveNext
             'Call objWinInfo.stdWinProcess(cwProcessToolBar, 23, 0)
             Call objWinInfo.MoveNext
             frmFotos.txtMedicamento(q - 1).Text = txt
             Text1(0).Text = frmFotos.objDetailInfo.rdoStatement.SQL
             If frmFotos.objDetailInfo.rdoCursor.EOF = True Or frmFotos.objDetailInfo.rdoCursor.BOF = True Then
'                frmfotos.frame1.Caption = "Registros N� " & nfotospagina * (page - 1) + 1 & " a " & nfotospagina * (page - 1) + q
'                frmfotos.Frame1.Caption = "P�gina " & page & " de " & numpag
                 'frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
                 'Exit Sub
             End If
        Next j
    Next i
    For q = 0 To 7
        frmFotos.Image2(q).Visible = True
        frmFotos.txtMedicamento(q).Visible = True
    Next q
     SSFrame2.Visible = True
     nfot = 7
     Text1(1).Text = Text1(1).Text & "nfot" & nfot
     Screen.MousePointer = 0
End Sub

Sub PageNext()

    Dim i%, j%, q%, X%, txt$
    
    aviso = False
    If aviso2 = True Then Exit Sub
     
    Text1(1).Text = objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    If posic = 0 Then
        For i = 1 To nfot + 2
            Call objWinInfo.MoveNext
        Next i
        If frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code2 Then Call objWinInfo.MoveNext
        posic = 1
    End If
    
    If frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code2 Then aviso2 = True
    
    'Al cambiarnos de pagina borramos lo que habia
    For q = 0 To nfotospagina - 1
        frmFotos.Image2(q).Visible = False
        frmFotos.txtMedicamento(q).Visible = False
        frmFotos.Image2(q).Picture = LoadPicture()
    Next q
    
    q = 0
    
    SSFrame2.Visible = False
    For i = 1 To nver
        For j = 1 To nhor
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
             frmFotos.Image2(q).Left = j * sepho * 567 + ((11625 - (nhor + 1) * (sepho * 567)) / nhor) * (j - 1)
             frmFotos.Image2(q).Top = i * sepve * 567 + ((7100 - (nver + 1) * (sepve * 567)) / nver) * (i - 1)
             frmFotos.Image2(q).Width = ((11625 - (nhor + 1) * (sepho * 567)) / nhor)
             frmFotos.Image2(q).Height = ((7100 - (nver + 1) * (sepve * 567)) / nver)
             If Not IsNull(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                 On Error Resume Next
                 frmFotos.Image2(q).Picture = LoadPicture(Trim$(frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                 'frmFotos.Image2(q).Picture = LoadPicture(Trim$(grdSSDBGrid1.Columns(5).Text))
                 Err = 0
             End If
             
             frmFotos.txtMedicamento(q).Left = frmFotos.Image2(q).Left
             frmFotos.txtMedicamento(q).Top = frmFotos.Image2(q).Top + frmFotos.Image2(q).Height + 50
             frmFotos.txtMedicamento(q).Width = frmFotos.Image2(q).Width
             txt = frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
'                 frmFotos.txtMedicamento(q).Text = grdSSDBGrid1.Columns(0).Text & " - " & grdSSDBGrid1.Columns(1).Text
             q = q + 1
             'frmFotos.objDetailInfo.rdoCursor.MoveNext
             If objDetailInfo.blnDirection = False Then
'                Call objWinInfo.stdWinProcess(cwProcessToolBar, 23, 0)
                Call objWinInfo.MoveNext
             Else
'                Call objWinInfo.stdWinProcess(cwProcessToolBar, 22, 0)
                Call objWinInfo.MoveNext
             End If
             
            frmFotos.txtMedicamento(q - 1).Text = txt
            Text1(0).Text = frmFotos.objDetailInfo.rdoStatement.SQL
            
            If aviso2 = True Then
                nfot = q - 2
                SSFrame2.Visible = True
                For X = 0 To q - 1
                    frmFotos.Image2(X).Visible = True
                    frmFotos.txtMedicamento(X).Visible = True
                Next X
                Text1(1).Text = Text1(1).Text & "nfot" & nfot
                Text1(1).Text = Text1(1).Text & "fin:" & objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
                Screen.MousePointer = 0
                Exit Sub
            End If
            
            If frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code2 Then aviso2 = True
            
            'If frmFotos.objDetailInfo.rdoCursor.EOF = True Then
'                frmfotos.frame1.Caption = "Registros N� " & nfotospagina * (page - 1) + 1 & " a " & nfotospagina * (page - 1) + q
'                frmfotos.Frame1.Caption = "P�gina " & page & " de " & numpag
                'frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
            '    Exit Sub
            'End If
        Next j
    Next i
    
    Text1(1).Text = Text1(1).Text & "fin:" & objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    For q = 0 To 7
        frmFotos.Image2(q).Visible = True
        frmFotos.txtMedicamento(q).Visible = True
    Next q
    
    SSFrame2.Visible = True
    nfot = 7
    Text1(1).Text = Text1(1).Text & "nfot" & nfot
    Screen.MousePointer = 0
End Sub

Private Sub cboCombo1_Change(Index As Integer)
 Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_GotFocus(Index As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub Command1_Click(Index As Integer)
 '?frmfotos.objdetailinfo.rdocursor.RDOCOLUMNS("GC50_CODIGO")
    Dim q$, extension$
    Dim X, fd
    CmDialog1.DialogTitle = "Insertar el Archivo Foto"
    CmDialog1.Action = 1
    q = CmDialog1.filename
    If q <> "" Then
        txtText1(3).SetFocus
        txtText1(3).Text = q
        On Error Resume Next
        fd = FileDateTime(q)
        If Err <> 0 Then  'no existe el archivo bmp
            Err = 0
        Else
            Image1.Picture = LoadPicture(q)
            Exit Sub
        End If
        
    '    extension = Right$(q, Len(q) - InStr(q, "."))
    '    If LCase(extension) = "bmp" Then
    '        X = Shell("PBRUSH.EXE " & q, 1)
    '    End If
    End If
End Sub

Private Sub Command2_Click()

End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)

End Sub


Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  Dim i As Integer
  Dim historia As String
      
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  'se busca la historia, si se ha accedido con una historia particular
  If objPipe.PipeExist("BuscHistoria") Then
    historia = objPipe.PipeGet("BuscHistoria")
  Else
    historia = ""
  End If
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objAsistInfo
    .strName = "ASIST"
    Set .objFormContainer = fraPac(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPac(0)
    Set .grdGrid = grdSSPac(0)
    .intAllowance = cwAllowReadOnly
    '.strDataBase = objEnv.GetValue("Main")
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    .blnHasMaint = False
    
    .strTable = "PAC_1"
    SQL = "(nh) IN "
    SQL = SQL & "   (SELECT historia FROM pruebaAsistencia pA, carpetas c"
    SQL = SQL & "   WHERE c.cCarpeta=pA.cCarpeta"
    SQL = SQL & "   AND c.cDptoSecc = " & departamento
    SQL = SQL & "   )"
    If historia <> "" Then
        SQL = SQL & " AND nh = " & historia
    End If
    .strWhere = SQL
    Call .FormAddOrderField("NH", cwAscending)
        
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "ASIST")
    Call .FormAddFilterWhere(strKey, "NH", "Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "Ap1", "Apellido 1", cwString)
    Call .FormAddFilterWhere(strKey, "Ap2", "Apellido 2", cwString)
    Call .FormAddFilterWhere(strKey, "Nom", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "fNac", "Nacimiento", cwDate)
    Call .FormAddFilterWhere(strKey, "cama", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "pobl", "Poblaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "pcia", "Provincia", cwString)
    
    Call .FormAddFilterOrder(strKey, "NH", "Historia")
    Call .FormAddFilterOrder(strKey, "Ap1", "Apellido 1")
    Call .FormAddFilterOrder(strKey, "Ap2", "Apellido 2")
    Call .FormAddFilterOrder(strKey, "Nom", "Nombre")
    Call .FormAddFilterOrder(strKey, "fNac", "Nacimiento")
    Call .FormAddFilterOrder(strKey, "cama", "Cama")
    Call .FormAddFilterOrder(strKey, "pobl", "Poblaci�n")
    Call .FormAddFilterOrder(strKey, "pcia", "Provincia")
  End With
   
  With objPruebaInfo
    .strName = "PRUEBAASISTENCIA"
    Set .objFormContainer = fraPruebaAsistencia(0)
    Set .objFatherContainer = fraPac(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPruebaAsistencia(0)
    .intAllowance = cwAllowReadOnly
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    '.strDataBase = objEnv.GetValue("Main")
    
    .strTable = "PRUEBAASISTENCIA_3"
    SQL = "proceso  = " & constPROCESOSOLICITUD
    SQL = SQL & " AND cCarpeta IN "
    SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    .strWhere = SQL
    Call .FormAddOrderField("fechaIngreso", cwDescending)
    Call .FormAddOrderField("secuencia", cwDescending)
    Call .FormAddRelation("historia", txtAsist(0))

    'strKey = .strDataBase & .strTable
  End With
  
  With objWinInfo
    Call .FormAddInfo(objAsistInfo, cwFormDetail)
    Call .FormAddInfo(objPruebaInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objPruebaInfo, "N� Ref.", "nRef", cwNumeric, 12)
    Call .GridAddColumn(objPruebaInfo, "Caso", "caso", cwNumeric, 6)
    Call .GridAddColumn(objPruebaInfo, "Secuencia", "secuencia", cwNumeric, 4)
    Call .GridAddColumn(objPruebaInfo, "F. Ingreso", "fechaIngreso", cwDate, 22)
    Call .GridAddColumn(objPruebaInfo, "Prueba", "desigPrueba", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "F. Petici�n", "fechaPeticion", cwDate, 22)
    Call .GridAddColumn(objPruebaInfo, "Muestra", "cMuestra", cwString, 10)
    Call .GridAddColumn(objPruebaInfo, "F. Extracci�n", "fechaExtraccion", cwDate, 22)
    Call .GridAddColumn(objPruebaInfo, "Dpto.", "dptSolicitante", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "Doctor", "drSolicitante", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "cEstado", "estado", cwNumeric, 2)
    Call .GridAddColumn(objPruebaInfo, "Estado", "", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "Cama", "cama", cwString, 6)
    
    Call .FormCreateInfo(objAsistInfo)
    'campos no visibles en la tabla del objAsistInfo
    '.CtrlGetInfo(txtAsist(1)).blnInGrid = False
    .CtrlGetInfo(txtAsist(5)).blnInGrid = False
    'campos inclu�dos en la b�squeda del objAsistInfo
    .CtrlGetInfo(txtAsist(0)).blnInFind = True
    .CtrlGetInfo(txtAsist(3)).blnInFind = True
    .CtrlGetInfo(txtAsist(4)).blnInFind = True
    .CtrlGetInfo(txtAsist(2)).blnInFind = True
    '.CtrlGetInfo(txtAsist(8)).blnInFind = True 'CWCun.dll no soporta b�squedas por campos tipo fecha
    .CtrlGetInfo(txtAsist(9)).blnInFind = True
    .CtrlGetInfo(txtAsist(11)).blnInFind = True
    .CtrlGetInfo(txtAsist(13)).blnInFind = True
    'campos ReadOnly del objAsistInfo
    For i = 0 To txtAsist.Count - 1
        .CtrlGetInfo(txtAsist(i)).blnReadOnly = True
    Next i
    'campos ReadOnly del objPruebaInfo
    For i = 3 To grdssPruebaAsistencia(0).Cols - 1
        .CtrlGetInfo(grdssPruebaAsistencia(0).Columns(i)).blnReadOnly = True
    Next i
    'campos no visibles en la tabla del objPruebaInfo
    grdssPruebaAsistencia(0).Columns(13).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
  End With

  Screen.MousePointer = vbDefault 'Call objApp.SplashOff

End Sub
Private Sub Form_QueryUnload(intCancel As Integer, UnloadMode As Integer)
  intCancel = objWinInfo.WinExit

End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo

End Sub


Private Sub grdSSDBGrid1_DblClick()
    'cambia a la solapa DETALLE actualizando los datos del formulario con la informaci�n de
    'la fila seleccionada
    Call objWinInfo.GridDblClick
End Sub
Private Sub grdSSDBGrid1_UnboundReadData(ByVal objRowBuf As ssRowBuffer, _
                                        vntStartLocation As Variant, _
                                        ByVal blnReadPriorRows As Boolean)
    'alimenta la informaci�n del GRID en modo UNBOUND durante la navegaci�n.
    Call objWinInfo.GridUnboundRead(objRowBuf, vntStartLocation, blnReadPriorRows)
End Sub
Private Sub grdSSDBGrid1_RowColChange(ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
    'controla el movimiento de los registrosen el Grid.
'    Call objWinInfo.DataChangeGridRowCol(vntLastRow, intLastCol)
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Image3(0).Picture = Image1.Picture
    tabSSTab1.Visible = False
    frmFotos.Caption = txtText1(0).Text & " - " & txtText1(1).Text
    stbStatusBar1.Visible = False
    tlbToolbar1.Visible = False
    Image3(0).Visible = True
End Sub

Private Sub Image2_Click(Index As Integer)
    Image3(0).Picture = Image2(Index).Picture
    tabSSTab1.Visible = False
    frmFotos.Caption = txtMedicamento(Index).Text
    stbStatusBar1.Visible = False
    tlbToolbar1.Visible = False
    Image3(0).Visible = True
End Sub

Private Sub Image3_Click(Index As Integer)
    Image3(0).Visible = False
    tabSSTab1.Visible = True
    frmFotos.Caption = "Tabla de Medicamentos"
    stbStatusBar1.Visible = True
    tlbToolbar1.Visible = True
End Sub

Private Sub SSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
'    IF INDEX=0 THENTXTTEXT1(7)=
End Sub

Private Sub SSDBCombo1_Click(Index As Integer)

'If Index = 0 Then txtText1(7) = SSDBCombo1(0).Columns(1).Text
'If Index = 1 Then txtText1(8) = SSDBCombo1(1).Columns(1).Text

End Sub

Private Sub SSDBCombo1_CloseUp(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub SSDBCombo1_GotFocus(Index As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub SSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = objWinInfo.CtrlKeyPress(txtText1(Index), KeyAscii)
End Sub

Private Sub SSDBCombo1_LostFocus(Index As Integer)
Call objWinInfo.CtrlLostFocus
End Sub

Private Sub SSDateCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub SSDateCombo1_GotFocus(Index As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub SSDateCombo1_LostFocus(Index As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    ' esta funci�n analiza el bot�n seleccionado y ejecuta la acci�n asociada
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim res
    
    ' analiza el bot�n seleccionado y ejecuta la acci�n asociada
    If tabSSTab1.Tab = 1 And (btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24) Then
        Select Case btnButton.Index
            Case 21
                If aviso = True Then
                    Beep
                Else
                    PageFirst
                End If
            Case 22
                If aviso = True Then
                    Beep
                Else
                    PagePrevious
                End If
            Case 23
                If aviso2 = True Then
                    Beep
                Else
                    PageNext
                End If
            Case 24
                If aviso2 = True Then
                    Beep
                Else
                   PageLast
                End If
        End Select
    Else
        If btnButton.Index = 18 Then
            For res = 0 To nfotospagina - 1
                frmFotos.Image2(res).Visible = False
                frmFotos.txtMedicamento(res).Visible = False
                frmFotos.Image2(res).Picture = LoadPicture()
            Next res
            tabSSTab1.Tab = 0
        End If
        
        Select Case btnButton.Key
            Case cwToolBarButtonOpen
                Call objWinInfo.WinProcess(cwProcessRegister, 10, 0)
            Case Else
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End Select
        If btnButton.Index = 19 Then 'Or btnButton.Index = 19 Or btnButton.Index = 1 Then
            'calculonumpag
            'Movepage (1)
            PageFirst
        End If
    End If
    
    
    
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    'en los eventos Click de todas las opciones del men� est�ndar
    'evalua la opci�n de men� seleccionada y ejecuta la acci�n correspondiente
    Select Case intIndex
    Case 20
        Call objWinInfo.WinProcess(cwProcessRegister, 10, 0)
    Case Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End Select
End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub
Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub
Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub
Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub
Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
'Private Sub tabSSTab1_Click(intPreviousTab As Integer)
'    'avisa al formulario que se ha producido un cambio de solapa y realiza las acciones
'    'derivadas de dicho cambio
'
'    'Call objWinInfo.FormChangeTab(intPreviousTab)
'    If tabSSTab1.Tab = 1 Then
'        'calculonumpag
'        'paginactual = 1
'        'Movepage (1)
'        'PageLast
'        PageFirst
'    Else
'        Call objWinInfo.FormChangeTab(intPreviousTab)
'    End If
'End Sub

Private Sub txtText1_Change(intIndex As Integer)
    'comunicar al formulario que se han realizado cambios en el control y habilitar
    'la opci�n Grabar
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtText1_GotFocus(intIndex As Integer)
    'comunicar al formulario cual es el control que recibe el foco y realizar las acciones
    'pertinentes,tales como cambiar el control activo,actualizar los paneles de la barra
    'de estado,etc.
    Call objWinInfo.CtrlGotFocus
End Sub
Private Sub txtText1_KeyPress(intIndex As Integer, intKeyAscii As Integer)
    'controlar la pulsaci�n de teclas del control activo.
    intKeyAscii = objWinInfo.CtrlKeyPress(txtText1(intIndex), intKeyAscii)
End Sub
Private Sub txtText1_LostFocus(intIndex As Integer)
    'controlar p�rdida del foco en los controles.Las acciones a ejecutar ser�n diferentes
    'en funci�n del modo del formulario y la informaci�n interna de los controles.
    'este m�todo podr� realizar desde validaciones sencillas sobre la informaci�n de los
    'controles hasta forzar lecturas diversas dela base de datos
    Call objWinInfo.CtrlLostFocus
 '   If Index = 5 Then
 '       Call RevisarCodigoPostal
 '   End If
End Sub
Private Sub CreateFilterInfo(ByVal objFormInfo As clsCWForm)
With objWinInfo
  Call .FormCreateFilterWhere(objFormInfo, objData.GetValue("main") & "FR0100", "Tabla de Medicamentos", "", _
                                Array(Array("FR01_CODIGO", "C�digo", cwString, ""), _
                                   Array("FR01_REFERENCIA", "Designaci�n", cwString, ""), _
                                   Array("FR02_CGRUPO", "C�digo de Grupo", cwString, ""), _
                                   Array("FR01_GRUPO", "Grupo", cwString, ""), _
                                   Array("FR01_PROVEEDOR", "Proveedor", cwString, "")), "", "")
  Call .FormCreateFilterOrder(objFormInfo, _
                               Array(Array("FR01_CODIGO", "C�digo"), _
                                     Array("FR01_REFERENCIA", "Designaci�n"), _
                                     Array("FR02_CGRUPO", "C�digo de Grupo"), _
                                     Array("FR01_GRUPO", "Grupo"), _
                                     Array("FR01_PROVEEDOR", "Proveedor")))
End With
End Sub
Public Function cwEventHandler(ByVal intEvento As Integer, _
                              ByVal vntData1 As Variant, _
                              ByVal vntData2 As Variant) As Boolean
    Dim rdoContaContactos As rdoResultset
    Dim strSQL As String
    Dim objCtrlInfo As Object
    Dim vntvalue As Variant
'    Set rdoContaContactos = objApp.rdoAppConnect.OpenResultset(strSQL, rdOpenKeyset, _
        rdConcurValues)
    
    
    
    
    
    
       Select Case intEvento
        Case cwEventPreRead
        Case cwEventPostRead
            Image1.Picture = LoadPicture(txtText1(3))
        Case cwEventPreWrite
        Case cwEventPostWrite
        Case cwEventPreDelete
        Case cwEventPostDelete
        Case cwEventPreRestore
        Case cwEventPostRestore
        Print txtText1(0).Text
        Case cwEventPreValidate
        Case cwEventPostValidate
        Case cwEventPreDefault
        Case cwEventPostDefault
        Case cwEventPreChangeStatus
        Case cwEventPostChangeStatus
            'btmap=
            'Set objCtrlInfo = objWinInfo.stdCtrlGetInfo(txtText1(0))
            'Call stdDataSetColumn(objWinInfo, objCtrlInfo)
             'Call stdDataGetColumn(, vntvalue)
                             
            
        Case cwEventPreChanged
        Case cwEventPostChanged
        
    End Select
End Function

