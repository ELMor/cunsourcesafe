VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmI_HojaTrabajoRepeticion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Repetici�n de Hojas de Trabajo"
   ClientHeight    =   4980
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4980
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin Threed.SSFrame fraHojasTrabajo 
      Height          =   4215
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   120
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   7435
      _StockProps     =   14
      Caption         =   "Hojas de Trabajo"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin SSDataWidgets_B.SSDBGrid grdSSHojasTrabajo 
         Height          =   3795
         Index           =   0
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   300
         Width           =   5310
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   4
         DividerType     =   0
         DividerStyle    =   2
         BevelColorHighlight=   -2147483634
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3175
         Columns(0).Caption=   "Fecha de Impresi�n"
         Columns(0).Name =   "Fecha de Impresi�n"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3889
         Columns(1).Caption=   "Realiza"
         Columns(1).Name =   "Realiza"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1773
         Columns(2).Caption=   "N� T�cnicas"
         Columns(2).Name =   "N� T�cnicas"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "cUser"
         Columns(3).Name =   "cUser"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   9366
         _ExtentY        =   6694
         _StockProps     =   79
         BackColor       =   -2147483636
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   390
      Left            =   2820
      TabIndex        =   0
      Top             =   4500
      Width           =   1140
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   390
      Left            =   4425
      TabIndex        =   1
      Top             =   4500
      Width           =   1140
   End
End
Attribute VB_Name = "frmI_HojaTrabajoRepeticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private cCarpeta As String      ' Carpeta cuyas hojas se muestras
Private blnSiembras As Boolean  ' Indica si es una lista de siembras o no
Private blnHojaTrabajo As Boolean ' Indica si hay que imprimir hoja o seleccionar hoja para introducir resultados
Private entrada As Boolean

Private Sub pPrepararVentana()
    If blnHojaTrabajo = False Then
        cmdImprimir.Caption = "&Imprimir"
    ElseIf blnHojaTrabajo = True Then
        cmdImprimir.Caption = "&Resultados"
    End If
End Sub


Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdImprimir_Click()
    If blnHojaTrabajo = False Then
        Dim res As Boolean
        res = fImprimirHojaRepeticion(cCarpeta, (grdSSHojasTrabajo(0).Columns(3).Text), (grdSSHojasTrabajo(0).Columns(0).Text), blnSiembras)
        If res = True Then
            Unload Me
        End If
    Else
        pVentanaHojaResultados cCarpeta, (grdSSHojasTrabajo(0).Columns(0).Text), (grdSSHojasTrabajo(0).Columns(3).Text)
    End If
End Sub

Private Sub Form_Load()
  Dim SQL As String
  Dim qryHojas As rdoQuery
  Dim rsHojas As rdoResultset
  Dim usuario As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
    pFormatearGrid grdSSHojasTrabajo(0)

    cCarpeta = objPipe.PipeGet("MB_cCarpeta")
    blnSiembras = objPipe.PipeGet("MB_Siembras")
    blnHojaTrabajo = objPipe.PipeGet("MB_HojaTrabajo")
    pPrepararVentana
    
    SQL = "SELECT   "
    SQL = SQL & "   to_char(MB20_fecReali,'dd/mm/yyyy hh24:mi:ss') as fecha,users.nombre||' '||users.apellido1 as usuario,"
    SQL = SQL & "   COUNT(distinct MB2000.NREF||MB2000.MB20_CODTECASIST) as nTec,"
    SQL = SQL & "   MB2000.cUser"
    SQL = SQL & " FROM MB2000,users, pruebaAsistencia pA"
    SQL = SQL & " WHERE "
    SQL = SQL & "   users.cUser (+)= MB2000.cUser"
    SQL = SQL & "   AND pA.nRef = MB2000.NRef"
    SQL = SQL & "   AND MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREALIZADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & "   AND MB2000.MB20_fecReali >= TO_DATE(?,'DD/MM/YYYY')"
    If blnHojaTrabajo = False Then
        SQL = SQL & "   AND MB20_INDSiembra = ?"
    End If
    If Trim$(cCarpeta) <> "" Then
        SQL = SQL & "   AND pA.cCarpeta = ?"
    Else
        SQL = SQL & " AND pA.cCarpeta IN "
        SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = ?)"
    End If
    If blnHojaTrabajo = True Then
        'SQL = SQL & " AND MB2000.MB09_CODTEC IN (SELECT MB09_CODTEC FROM MB0900 WHERE (MB0900.MB04_CODTITEC >=" & constBIOQUIMICAS & ")"
        SQL = SQL & " AND MB2000.MB09_CODTEC IN (SELECT MB09_CODTEC FROM MB0900 WHERE (MB0900.MB04_CODTITEC >=" & constBIOQUIMICAS & " OR MB0900.MB04_CODTITEC =" & constOBSERVDIRECTA & "))"
    End If
    SQL = SQL & " GROUP BY MB20_fecReali, users.nombre||' '||users.apellido1,MB2000.cUser"
    SQL = SQL & " ORDER BY MB20_fecReali DESC"
    
    Set qryHojas = objApp.rdoConnect.CreateQuery("Repeticion Hojas", SQL)
    qryHojas(0) = Format(DateAdd("d", -4, fFechaActual()), "dd/mm/yyyy")
    If blnHojaTrabajo = False Then
        qryHojas(1) = blnSiembras
        If Trim$(cCarpeta) <> "" Then
            qryHojas(2) = cCarpeta
        Else
            qryHojas(2) = departamento
        End If
    Else
        If Trim$(cCarpeta) <> "" Then
            qryHojas(1) = cCarpeta
        Else
            qryHojas(1) = departamento
        End If
    End If
    Set rsHojas = qryHojas.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsHojas.EOF
        usuario = ""
        If Trim$(rsHojas!usuario) <> "" Then
            usuario = rsHojas!usuario
        Else
            usuario = "Siembras"
        End If
        grdSSHojasTrabajo(0).AddItem rsHojas!fecha & Chr$(9) & usuario & Chr$(9) & rsHojas!nTec & Chr$(9) & rsHojas!cUser
        rsHojas.MoveNext
    Loop
    rsHojas.Close
    qryHojas.Close
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff

End Sub

