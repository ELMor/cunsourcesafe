VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmA_Tecnica 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "T�cnicas"
   ClientHeight    =   7425
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9705
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7425
   ScaleWidth      =   9705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   9705
      _ExtentX        =   17119
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Left            =   120
      TabIndex        =   15
      Top             =   3720
      Width           =   9465
      Begin TabDlg.SSTab tabDatos 
         Height          =   2880
         Left            =   120
         TabIndex        =   23
         Top             =   300
         Width           =   9075
         _ExtentX        =   16007
         _ExtentY        =   5080
         _Version        =   327681
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Resultados"
         TabPicture(0)   =   "mb00109.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraResult"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tiempo Lectura"
         TabPicture(1)   =   "mb00109.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraTiempo"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Protocolos"
         TabPicture(2)   =   "mb00109.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraProt"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "T�cnicas asociadas"
         TabPicture(3)   =   "mb00109.frx":0054
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "fraDef"
         Tab(3).ControlCount=   1
         Begin VB.Frame fraDef 
            BorderStyle     =   0  'None
            Height          =   2415
            Left            =   -74940
            TabIndex        =   37
            Top             =   360
            Width           =   8895
            Begin SSDataWidgets_B.SSDBGrid grdssDef 
               Height          =   2340
               Index           =   0
               Left            =   60
               TabIndex        =   38
               Top             =   60
               Width           =   8775
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15478
               _ExtentY        =   4128
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraProt 
            BorderStyle     =   0  'None
            Height          =   2415
            Left            =   -74940
            TabIndex        =   27
            Top             =   360
            Width           =   8895
            Begin SSDataWidgets_B.SSDBGrid grdssProt 
               Height          =   2340
               Index           =   0
               Left            =   60
               TabIndex        =   28
               Top             =   60
               Width           =   8775
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15478
               _ExtentY        =   4128
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraTiempo 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2475
            Left            =   -74940
            TabIndex        =   25
            Top             =   360
            Width           =   8895
            Begin SSDataWidgets_B.SSDBGrid grdssTiempo 
               Height          =   2340
               Index           =   0
               Left            =   60
               TabIndex        =   26
               Top             =   60
               Width           =   8775
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15478
               _ExtentY        =   4128
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraResult 
            BorderStyle     =   0  'None
            Height          =   2415
            Left            =   60
            TabIndex        =   24
            Top             =   360
            Width           =   8955
            Begin TabDlg.SSTab tabResult 
               Height          =   2295
               Left            =   60
               TabIndex        =   29
               TabStop         =   0   'False
               Top             =   60
               Width           =   8835
               _ExtentX        =   15584
               _ExtentY        =   4048
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "mb00109.frx":0070
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(4)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(6)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(8)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(10)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "cbossResult"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "txtResult(3)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "txtResult(0)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "txtResult(1)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "chkResult(0)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtResult(2)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtResult(4)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "fraLaboratorio"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).ControlCount=   12
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "mb00109.frx":008C
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdssResult(0)"
               Tab(1).ControlCount=   1
               Begin VB.Frame fraLaboratorio 
                  BorderStyle     =   0  'None
                  Caption         =   "fraLaboratorio"
                  Height          =   495
                  Left            =   4920
                  TabIndex        =   41
                  Top             =   1380
                  Width           =   2775
                  Begin VB.CheckBox chkResult 
                     Alignment       =   1  'Right Justify
                     Caption         =   "Laboratorio"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   315
                     Index           =   1
                     Left            =   780
                     TabIndex        =   42
                     Tag             =   "Activa|Activa"
                     Top             =   120
                     Width           =   1470
                  End
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "cResultado"
                  Height          =   285
                  Index           =   4
                  Left            =   5700
                  MaxLength       =   20
                  TabIndex        =   40
                  Tag             =   "CResultado|cResultado"
                  Top             =   1860
                  Visible         =   0   'False
                  Width           =   1515
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFF00&
                  DataField       =   "MB32_DESCRIP"
                  Height          =   285
                  Index           =   2
                  Left            =   1740
                  MaxLength       =   50
                  TabIndex        =   10
                  Tag             =   "Descripci�n|Descripci�n del Dato del Resultado"
                  Top             =   1020
                  Width           =   6390
               End
               Begin VB.CheckBox chkResult 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Activo"
                  DataField       =   "MB32_INDACTIVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   0
                  Left            =   420
                  TabIndex        =   12
                  Tag             =   "Activa|Activa"
                  Top             =   1800
                  Width           =   1530
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFF00&
                  DataField       =   "MB32_DESIG"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   1
                  Left            =   1755
                  Locked          =   -1  'True
                  MaxLength       =   20
                  TabIndex        =   9
                  Tag             =   "Designaci�n|Nombre abreviado del Dato del Resultado"
                  Top             =   600
                  Width           =   3240
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "MB32_CODRESULT"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   0
                  Left            =   1740
                  Locked          =   -1  'True
                  MaxLength       =   2
                  TabIndex        =   8
                  Tag             =   "C�digo|C�digo del Resultado"
                  Top             =   180
                  Width           =   615
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "MB09_CODTEC"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   3
                  Left            =   3000
                  Locked          =   -1  'True
                  MaxLength       =   3
                  TabIndex        =   30
                  Top             =   180
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin SSDataWidgets_B.SSDBGrid grdssResult 
                  Height          =   2070
                  Index           =   0
                  Left            =   -74850
                  TabIndex        =   31
                  TabStop         =   0   'False
                  Top             =   105
                  Width           =   8205
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RecordSelectors =   0   'False
                  Col.Count       =   0
                  BevelColorFrame =   0
                  BevelColorHighlight=   16777215
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowGroupShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  MaxSelectedRows =   0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   0   'False
                  _ExtentX        =   14473
                  _ExtentY        =   3651
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "CUNIDAD"
                  Height          =   315
                  Left            =   1740
                  TabIndex        =   11
                  Tag             =   "Unidad|Unidad"
                  Top             =   1440
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Unidad:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   420
                  TabIndex        =   35
                  Top             =   1440
                  Width           =   1230
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Descripci�n:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   420
                  TabIndex        =   34
                  Top             =   1020
                  Width           =   1200
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "C�digo:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   420
                  TabIndex        =   33
                  Top             =   180
                  Width           =   1275
               End
               Begin VB.Label lblLabel1 
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Designaci�n:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   420
                  TabIndex        =   32
                  Top             =   600
                  Width           =   1125
               End
            End
         End
      End
   End
   Begin VB.Frame fraTec 
      Caption         =   "T�cnicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Left            =   90
      TabIndex        =   14
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabTec 
         Height          =   2730
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   4815
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00109.frx":00A8
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "cbossTec(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cbossTec(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cbossTec(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtTec(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkTec(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtTec(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtTec(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtTec(3)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "chkTec(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTec"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkTec 
            Alignment       =   1  'Right Justify
            Caption         =   "Aplicar antibi�ticos por defecto"
            DataField       =   "MB09_INDDEFECTO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   4620
            TabIndex        =   43
            Tag             =   "Antib. por defecto|Aplicar antibi�ticos por defecto"
            Top             =   1800
            Width           =   3285
         End
         Begin VB.TextBox txtTec 
            BackColor       =   &H00FFFF00&
            DataField       =   "CDPTOSECC"
            Height          =   285
            Index           =   3
            Left            =   4260
            MaxLength       =   50
            TabIndex        =   36
            Tag             =   "Secci�n|Secci�n"
            Top             =   180
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.TextBox txtTec 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB09_CODTEC"
            Height          =   285
            Index           =   0
            Left            =   1860
            MaxLength       =   3
            TabIndex        =   1
            Tag             =   "C�digo|C�digo de la T�cnica"
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox txtTec 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB09_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1860
            MaxLength       =   20
            TabIndex        =   2
            Tag             =   "Designaci�n|Nombre abreviado de la T�cnica"
            Top             =   600
            Width           =   4455
         End
         Begin VB.CheckBox chkTec 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB09_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   360
            TabIndex        =   7
            Tag             =   "Activa|Activa"
            Top             =   2160
            Width           =   1725
         End
         Begin VB.TextBox txtTec 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB09_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1860
            MaxLength       =   50
            TabIndex        =   3
            Tag             =   "Descripci�n|Descripci�n de la T�cnica"
            Top             =   960
            Width           =   6495
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTec 
            Height          =   2490
            Left            =   -74880
            TabIndex        =   13
            Top             =   120
            Width           =   8700
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15346
            _ExtentY        =   4392
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTec 
            DataField       =   "MB04_CODTITEC"
            Height          =   315
            Index           =   0
            Left            =   1860
            TabIndex        =   4
            Tag             =   "Tipo T�cnica|Tipo de T�cnica"
            Top             =   1320
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTec 
            DataField       =   "MB03_CODRECIP"
            Height          =   315
            Index           =   1
            Left            =   5880
            TabIndex        =   5
            Tag             =   "Tipo Recipiente|Tipo de Recipiente"
            Top             =   1320
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTec 
            DataField       =   "cAutoanalizador"
            Height          =   315
            Index           =   2
            Left            =   1845
            TabIndex        =   6
            Tag             =   "Autoanalizador|Autoanalizador"
            Top             =   1740
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Autoanalizador:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   300
            TabIndex        =   39
            Top             =   1800
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   315
            TabIndex        =   22
            Top             =   240
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   315
            TabIndex        =   21
            Top             =   600
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Recipiente:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   4620
            TabIndex        =   20
            Top             =   1380
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo T�cnica:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   315
            TabIndex        =   19
            Top             =   1380
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   315
            TabIndex        =   18
            Top             =   960
            Width           =   1380
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   16
      Top             =   7140
      Width           =   9705
      _ExtentX        =   17119
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Tecnica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim objResult As New clsCWForm
Dim objTiempo As New clsCWForm
Dim objProt As New clsCWForm
Dim objDef As New clsCWForm


Dim blnPostRead As Boolean

Private Sub cbossResLabor_Click()
  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossResLabor_Change()
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub cbossResLabor_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossResLabor_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossResult_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossResult_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossResult_Click()
  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossResult_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkResult_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkResult_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Select Case intIndex
    Case 0
        'Call objWinInfo.CtrlDataChange
    Case 1  ' Laboratorio
        If blnPostRead = False Then
            If chkResult(1).Value = 1 Then
                txtResult(4).Text = fCodigoResultadoLaboratorio(departamento, True)
            Else
                txtResult(4).Text = ""
            End If
        End If
End Select
End Sub

Private Sub Form_Load()
  
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    .strName = "Tec"
    Set .objFormContainer = fraTec
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTec
    Set .grdGrid = grdssTec
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB0900"
    .strWhere = "MB0900.cDptoSecc = " & departamento 'nombre tabla para report
    Call .FormAddOrderField("MB0900.MB09_CODTEC", cwAscending) 'nombre tabla para report
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "T�cnicas")
    Call .FormAddFilterWhere(strKey, "MB0900.MB09_CODTEC", "C�digo", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB09_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB09_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB0900.MB04_CODTITEC", "Tipo de T�cnica", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB0900.MB03_CODRECIP", "Recipiente", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB09_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "MB0900.MB09_CODTEC", "C�digo") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB09_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB09_DESCRIP", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "MB0900.MB04_CODTITEC", "Tipo de T�cnica") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB0900.MB03_CODRECIP", "Recipiente") 'nombre tabla para report
    
    Call .objPrinter.Add("MB00130", "T�cnicas ordenadas alfab�ticamente")
    Call .objPrinter.Add("MB00131", "Tipos de T�cnicas")
    Call .objPrinter.Add("MB00132", "Resultados de T�cnicas")
    Call .objPrinter.Add("MB00133", "Tiempos de Lectura de T�cnicas")
    Call .objPrinter.Add("MB00134", "Protocolos")
    Call .objPrinter.Add("MB00146", "T�cnicas asociadas a Cultivos")
  End With
  
  With objResult
    .strName = "Result"
    Set .objFormContainer = fraResult
    Set .objFatherContainer = fraTec
    Set .tabMainTab = tabResult
    Set .grdGrid = grdssResult(0)
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB3200"
    .blnAskPrimary = False
    Call .FormAddOrderField("MB3200.MB32_CODRESULT", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("MB09_CODTEC", txtTec(0))
    
    'Call .objPrinter.Add("MB00132", "Resultados de T�cnicas") 'columna ambigua
  End With
  
  With objTiempo
    .strName = "Tiempo"
    Set .objFormContainer = fraTiempo
    Set .objFatherContainer = fraTec
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssTiempo(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1300"
    Call .FormAddOrderField("MB1300.MB02_CODCOND", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("MB09_CODTEC", txtTec(0))
    
    'Call .objPrinter.Add("MB00133", "Tiempos de Lectura de T�cnicas") 'columna ambigua
     
  End With
  
  With objProt
    .strName = "Prot"
    Set .objFormContainer = fraProt
    Set .objFatherContainer = fraTec
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssProt(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1400"
    Call .FormAddOrderField("MB1400.MB05_CODPROT", cwAscending) 'nombre de tabla para report
    Call .FormAddOrderField("MB1400.MB02_CODCOND", cwAscending) 'nombre de tabla para report
    Call .FormAddRelation("MB09_CODTEC", txtTec(0))

    
    'Call .objPrinter.Add("MB00134", "Protocolos") 'columna ambigua
  End With
  
With objDef
    .strName = "Def"
    Set .objFormContainer = fraDef
    Set .objFatherContainer = fraTec
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDef(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB4300"
    Call .FormAddOrderField("MB09_CodTec_T", cwAscending)
    Call .FormAddRelation("MB09_CODTEC_C", txtTec(0))

    Call .objPrinter.Add("MB00146", "T�cnicas asociadas")
  
End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
        
    Call .FormAddInfo(objResult, cwFormDetail)
    
    'NOTA: se dejan espacios en los captions de las columnas de los grids para aumentar el _
    tama�o de la columna (establecido as� por la CodeWizard)
    Call .FormAddInfo(objTiempo, cwFormMultiLine)
    Call .GridAddColumn(objTiempo, "T�cnica", "MB09_CODTEC", cwNumeric, 3)
    Call .GridAddColumn(objTiempo, "Cond. Ambientales      ", "MB02_CODCOND", cwNumeric, 2)
    Call .GridAddColumn(objTiempo, "t. (horas)", "MB13_TIEMPO", cwNumeric, 3)
    Call .GridAddColumn(objTiempo, "t. Resiembra (horas)", "MB13_TRESIEMBRA", cwNumeric, 3)
    Call .GridAddColumn(objTiempo, "Activa", "MB13_INDACTIVA", cwBoolean, 1)

    Call .FormAddInfo(objProt, cwFormMultiLine)
    Call .GridAddColumn(objProt, "T�cnica", "MB09_CODTEC", cwNumeric, 3)
    Call .GridAddColumn(objProt, "Protocolo          ", "MB05_CODPROT", cwNumeric, 2)
    Call .GridAddColumn(objProt, "Cond. Ambientales  ", "MB02_CODCOND", cwNumeric, 2)
    Call .GridAddColumn(objProt, "N� veces", "MB14_NUMVECES", cwNumeric, 1)
    Call .GridAddColumn(objProt, "Activa", "MB14_INDACTIVA", cwBoolean, 1)
    
    Call .FormAddInfo(objDef, cwFormMultiLine)
    Call .GridAddColumn(objDef, "Tipo t�cnica", "MB09_codTec_C", cwNumeric, 3)
    Call .GridAddColumn(objDef, "T�cnicas          ", "MB09_codTec_T", cwNumeric, 3)
    
    Call .FormCreateInfo(objMasterInfo)
    .CtrlGetInfo(chkTec(0)).blnInFind = True
    .CtrlGetInfo(txtTec(0)).blnValidate = False
    .CtrlGetInfo(txtTec(0)).blnInFind = True
    .CtrlGetInfo(txtTec(1)).blnInFind = True
    .CtrlGetInfo(txtTec(2)).blnInFind = True
    .CtrlGetInfo(cbossTec(0)).blnInFind = True
    .CtrlGetInfo(cbossTec(1)).blnInFind = True
    .CtrlGetInfo(txtTec(3)).blnValidate = False
    .CtrlGetInfo(txtTec(3)).blnInGrid = False
    
    .CtrlGetInfo(txtResult(4)).blnMandatory = False
    
    SQL = "SELECT MB04_codTiTec, MB04_desig FROM MB0400"
    SQL = SQL & " WHERE MB04_indActiva = -1"
    SQL = SQL & " ORDER BY MB04_desig"
    .CtrlGetInfo(cbossTec(0)).strSQL = SQL
    .CtrlGetInfo(cbossTec(0)).blnForeign = True
        
    SQL = "SELECT MB03_codRecip, MB03_desig FROM MB0300"
    SQL = SQL & " WHERE MB03_indActiva = -1"
    SQL = SQL & " ORDER BY MB03_desig"
    .CtrlGetInfo(cbossTec(1)).strSQL = SQL
    .CtrlGetInfo(cbossTec(1)).blnForeign = True
        
    SQL = "SELECT cAutoanalizador, designacion FROM autoanalizadores"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(cbossTec(2)).strSQL = SQL
    '.CtrlGetInfo(cbossTec(2)).blnForeign = True
    
    Call .FormChangeColor(objResult)
    .CtrlGetInfo(txtResult(0)).blnValidate = False
    .CtrlGetInfo(txtResult(3)).blnInGrid = False
    Call .FormChangeColor(objTiempo)
    grdssTiempo(0).Columns(3).Visible = False
    Call .FormChangeColor(objProt)
    grdssProt(0).Columns(3).Visible = False
    Call .FormChangeColor(objDef)
    grdssDef(0).Columns(3).Visible = False
    
    ' se llenan los DropDown
    SQL = "SELECT cUnidad, designacion FROM unidades"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(cbossResult).strSQL = SQL
    
    'NOTA LABOR:
'    SQL = "SELECT DISTINCT pr.cResultado, pr.designacion"
'    SQL = SQL & " FROM pruebasResultados pr, pruebasCarpetas pc, Carpetas c"
'    SQL = SQL & " WHERE cDptoSecc = " & departamento
'    SQL = SQL & " AND pc.cCarpeta = c.cCarpeta"
'    SQL = SQL & " AND pr.cPrueba = pc.cPrueba"
'    SQL = SQL & " ORDER BY pr.designacion"
'    .CtrlGetInfo(cbossResLabor).strSQL = SQL
    
    SQL = "SELECT MB02_codCond, MB02_desig FROM MB0200"
    SQL = SQL & " WHERE MB02_indActiva = -1"
    SQL = SQL & " ORDER BY MB02_desig"
    .CtrlGetInfo(grdssTiempo(0).Columns(4)).strSQL = SQL
    grdssTiempo(0).Columns(4).Width = 2000
    .CtrlGetInfo(grdssProt(0).Columns(5)).strSQL = SQL
    grdssProt(0).Columns(5).Width = 2000
    .CtrlGetInfo(grdssTiempo(0).Columns(4)).blnForeign = True
    .CtrlGetInfo(grdssProt(0).Columns(5)).blnForeign = True
    
    SQL = "SELECT MB05_codProt, MB05_desig FROM MB0500"
    SQL = SQL & " WHERE MB05_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB05_desig"
    .CtrlGetInfo(grdssProt(0).Columns(4)).strSQL = SQL
    grdssProt(0).Columns(4).Width = 2000
    .CtrlGetInfo(grdssProt(0).Columns(4)).blnForeign = True
    
    SQL = "SELECT MB09_codTec, MB09_Desig FROM MB0900"
    SQL = SQL & " WHERE MB09_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " AND MB04_codTiTec >= " & constBIOQUIMICAS
    SQL = SQL & " ORDER BY MB09_Desig"
    .CtrlGetInfo(grdssDef(0).Columns(4)).strSQL = SQL
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraResult, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraTiempo, True, True)
    Case 2
        Call objWinInfo.FormChangeActive(fraProt, True, True)
    Case 3
        Call objWinInfo.FormChangeActive(fraDef, True, True)
    End Select
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    Select Case strFormName
        Case objMasterInfo.strName
            Select Case strCtrl
                Case objWinInfo.CtrlGetInfo(cbossTec(0)).strName
                    frmA_TipoTecnica.Show vbModal
                    Set frmA_TipoTecnica = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTec(0)))
                Case objWinInfo.CtrlGetInfo(cbossTec(1)).strName
                    frmA_Recipiente.Show vbModal
                    Set frmA_Recipiente = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTec(1)))
            End Select
        Case objTiempo.strName
            If strCtrl = objWinInfo.CtrlGetInfo(grdssTiempo(0).Columns(4)).strName Then
                frmA_Condicion.Show vbModal
                Set frmA_Condicion = Nothing
                Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssTiempo(0).Columns(4)))
            End If
        Case objProt.strName
            Select Case strCtrl
            Case objWinInfo.CtrlGetInfo(grdssProt(0).Columns(4)).strName
                frmA_Protocolo.Show vbModal
                Set frmA_Protocolo = Nothing
                Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssProt(0).Columns(4)))
            Case objWinInfo.CtrlGetInfo(grdssProt(0).Columns(5)).strName
                frmA_Condicion.Show vbModal
                Set frmA_Condicion = Nothing
                Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssProt(0).Columns(5)))
            End Select
    End Select
    
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
    
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: t�cnicas y protocolos
    
'Si se realiza alguna operaci�n en el formulario de t�cnicas(objMasterInfo) habr� que anotar _
el mantenimiento para t�cnicas y para protocolos ya que puede haberse puesto inactiva alguna _
que estuviera en alg�n protocolo

'Si se realiza alguna operaci�n en el formulario de protocolos(objProt) habr� que anotar _
el mantenimiento para los protocolos
    
    If strFormName = objMasterInfo.strName Or strFormName = objProt.strName Then
        If strFormName = objMasterInfo.strName Then
            Call pAnotarMantenimiento(constMANTTECNICAS)
        End If
        Call pAnotarMantenimiento(constMANTPROTOCOLOS)
    End If
    
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'    Select Case strFormName
'        Case objResult.strName
'            If objMasterInfo.rdoCursor("MB04_CODTITEC") <> constCULTIVO Then
'        '   If cbossTec(0) <> "Cultivos" Then
'                fraDef.Enabled = False
'                grdssDef(0).Enabled = False
'                grdssDef(0).Visible = False
'            Else
'                fraDef.Enabled = True
'                grdssDef(0).Enabled = True
'                grdssDef(0).Visible = True
'            End If
'    End Select
    
    Select Case strFormName
        Case objMasterInfo.strName
            If objGen.GetRowCount(objMasterInfo.rdoCursor) > 0 Then
                If Not IsNull(objMasterInfo.rdoCursor!MB04_CODTITEC) Then
                    If Val(objMasterInfo.rdoCursor!MB04_CODTITEC) = constANTIBIOGRAMA Then
                        chkTec(1).Visible = True
                    Else
                        chkTec(1).Visible = False
                    End If
                Else
                    chkTec(1).Visible = False
                End If
            End If
        Case objResult.strName
            If Not IsNull(objResult.rdoCursor!cResultado) Then
                chkResult(1).Enabled = False
                fraLaboratorio.Enabled = False
                chkResult(1).Value = 1
            Else
                chkResult(1).Enabled = True
                fraLaboratorio.Enabled = True
                chkResult(1).Value = 0
            End If
    End Select
    blnPostRead = False

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: t�cnicas y protocolos
    
'Si se realiza alguna operaci�n en el formulario de t�cnicas(objMasterInfo) habr� que anotar _
el mantenimiento para t�cnicas y para protocolos ya que puede haberse puesto inactiva alguna _
que estuviera en alg�n protocolo

'Si se realiza alguna operaci�n en el formulario de protocolos(objProt) habr� que anotar _
el mantenimiento para los protocolos
    
    If strFormName = objMasterInfo.strName Or strFormName = objProt.strName Then
        If strFormName = objMasterInfo.strName Then
            Call pAnotarMantenimiento(constMANTTECNICAS)
        End If
        Call pAnotarMantenimiento(constMANTPROTOCOLOS)
    ElseIf strFormName = objResult.strName Then
        ' Si se ha introducido la relaci�n con laboratorio -> se genera el resultado en laboratorio
        If fraLaboratorio.Enabled = True And chkResult(1).Value = 1 Then
            blnError = fInsertarResultadoLaboratorio(txtResult(4).Text, txtResult(2).Text, departamento)
        End If
    End If
    
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
    blnPostRead = True
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim rsTec As rdoResultset, qryTec As rdoQuery
    Dim SQL As String
    
    Select Case strFormName
    Case objMasterInfo.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtTec(0).Text = fNextClave("MB09_codTec", "MB0900")
            txtTec(3).Text = departamento
            objMasterInfo.rdoCursor("MB09_codTec") = txtTec(0).Text
            objMasterInfo.rdoCursor("cDptoSecc") = txtTec(3).Text
        End If
        
    Case objResult.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            SQL = "SELECT MAX(MB32_codResult)"
            SQL = SQL & " FROM MB3200"
            SQL = SQL & " WHERE MB09_codTec = ?"
            Set qryTec = objApp.rdoConnect.CreateQuery("SelMaxCodResultTec", SQL)
            qryTec(0) = txtTec(0).Text
            Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If IsNull(rsTec(0)) Then
                txtResult(0).Text = 1
            Else
                txtResult(0).Text = rsTec(0) + 1
            End If
            objResult.rdoCursor("MB32_codResult") = txtResult(0).Text
            
            rsTec.Close
            qryTec.Close
        End If
    
    End Select
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objMasterInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
    Case objResult.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
          End If
        Set objPrinter = Nothing
    Case objTiempo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
            Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                        objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
    Case objProt.strName
         Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
          End If
        Set objPrinter = Nothing
    
    Case objDef.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
          End If
        Set objPrinter = Nothing
        
  End Select
End Sub


Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
   Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraResult, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraTiempo, True, True)
    Case 2
        Call objWinInfo.FormChangeActive(fraProt, True, True)
    Case 3
        Call objWinInfo.FormChangeActive(fraDef, True, True)
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDef(0).Columns(4)))
        'objWinInfo.WinPrepareScr
    End Select
End Sub
Private Sub tabResult_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(fraResult, True, True)
End Sub

Private Sub Timer1_Timer()
Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraResult.ForeColor
    Case 1
        fraDatos.ForeColor = fraTiempo.ForeColor
    Case 2
        fraDatos.ForeColor = fraProt.ForeColor
    Case 3
        fraDatos.ForeColor = fraDef.ForeColor
    End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssTec_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTec_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTec_RowColChange(ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTec_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssProt_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssProt_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssProt_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssProt_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssDef_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssDef_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDef_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)

'EL LOADCOMBO EST� PUESTO EN EL
'    If grdssDef(intIndex).Col = 4 Then
'        If grdssDef(intIndex).Columns(0).Text = "A�adido" Then
'            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDef(0).Columns(4)))
'        End If
'    End If
  
End Sub

Private Sub grdssDef_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssTiempo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTiempo_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTiempo_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTiempo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResult_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResult_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabTec_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabTec, False, True)
End Sub

Private Sub fraTec_Click()
  Call objWinInfo.FormChangeActive(fraTec, False, True)
End Sub

Private Sub fraResult_Click()
  Call objWinInfo.FormChangeActive(fraResult, False, True)
End Sub

Private Sub fraTiempo_Click()
  Call objWinInfo.FormChangeActive(fraTiempo, False, True)
End Sub

Private Sub fraProt_Click()
    Call objWinInfo.FormChangeActive(fraProt, False, True)
End Sub

Private Sub fraDef_Click()
    Call objWinInfo.FormChangeActive(fraDef, False, True)
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub cbossTec_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossTec_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossTec_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus
  Select Case intIndex
    Case 0  'Tipo t�cnica
        ' Si es antibiograma se muestra visible el check de defecto
        If cbossTec(intIndex).Columns(0).Value = constANTIBIOGRAMA Then
            chkTec(1).Visible = True
        Else
            chkTec(1).Visible = False
        End If
  End Select
End Sub

Private Sub cbossTec_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkTec_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTec_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTec_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtResult_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtResult_Change(intIndex As Integer)
    Select Case intIndex
    Case 3
        If txtResult(4).Text = "" Then
            chkResult(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkResult(1).Value = 0
        End If
  End Select
  Call objWinInfo.CtrlDataChange

End Sub

Private Sub txtTec_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTec_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTec_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


