VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Morfologia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Morfolog�a"
   ClientHeight    =   7200
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   10110
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7200
   ScaleWidth      =   10110
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   10110
      _ExtentX        =   17833
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Left            =   120
      TabIndex        =   20
      Top             =   3240
      Width           =   9855
      Begin TabDlg.SSTab tabDatos 
         Height          =   3120
         Left            =   180
         TabIndex        =   21
         Top             =   360
         Width           =   9495
         _ExtentX        =   16748
         _ExtentY        =   5503
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Microorganismos"
         TabPicture(0)   =   "mb00112.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "fraMicro"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Resultados"
         TabPicture(1)   =   "mb00112.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "fraResult"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.Frame fraResult 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2715
            Left            =   60
            TabIndex        =   24
            Top             =   360
            Width           =   9315
            Begin TabDlg.SSTab tabResult 
               Height          =   2595
               Left            =   60
               TabIndex        =   25
               TabStop         =   0   'False
               Top             =   60
               Width           =   8955
               _ExtentX        =   15796
               _ExtentY        =   4577
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "mb00112.frx":0038
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(4)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(0)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(8)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(10)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(11)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "cbossResult(2)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "cbossResult(1)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "cbossResult(0)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "txtResult(3)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtResult(2)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtResult(0)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "chkResult"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtResult(1)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).ControlCount=   13
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "mb00112.frx":0054
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdssResult(0)"
               Tab(1).ControlCount=   1
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "MB25_OBSERV"
                  Height          =   765
                  HelpContextID   =   40101
                  Index           =   1
                  Left            =   1560
                  Locked          =   -1  'True
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   9
                  Tag             =   "Observaciones|Observaciones"
                  Top             =   1680
                  Width           =   6780
               End
               Begin VB.CheckBox chkResult 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Activo"
                  DataField       =   "MB25_INDACTIVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Left            =   5100
                  TabIndex        =   10
                  Tag             =   "Activa|Activa"
                  Top             =   600
                  Width           =   945
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "MB25_RESULT"
                  Height          =   555
                  HelpContextID   =   40101
                  Index           =   0
                  Left            =   1200
                  Locked          =   -1  'True
                  MaxLength       =   1000
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   8
                  Tag             =   "Valor|Valor del resultado"
                  Top             =   1020
                  Width           =   7140
               End
               Begin VB.TextBox txtResult 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFF00&
                  DataField       =   "MB12_CODMORF"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   2
                  Left            =   3780
                  Locked          =   -1  'True
                  MaxLength       =   3
                  TabIndex        =   27
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtResult 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H0000FFFF&
                  DataField       =   "MB25_CODRESMICRO"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   3
                  Left            =   3780
                  Locked          =   -1  'True
                  MaxLength       =   4
                  TabIndex        =   26
                  Top             =   420
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin SSDataWidgets_B.SSDBGrid grdssResult 
                  Height          =   2310
                  Index           =   0
                  Left            =   -74850
                  TabIndex        =   28
                  TabStop         =   0   'False
                  Top             =   105
                  Width           =   8205
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RecordSelectors =   0   'False
                  Col.Count       =   0
                  BevelColorFrame =   0
                  BevelColorHighlight=   16777215
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowGroupShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  MaxSelectedRows =   0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   -1  'True
                  _ExtentX        =   14473
                  _ExtentY        =   4075
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "MB09_CODTEC"
                  Height          =   315
                  Index           =   0
                  Left            =   1200
                  TabIndex        =   5
                  Tag             =   "T�cnica|T�cnica"
                  Top             =   180
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorEven   =   16776960
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "MB32_CODRESULT"
                  Height          =   315
                  Index           =   1
                  Left            =   5820
                  TabIndex        =   6
                  Tag             =   "Resultado|Resultado"
                  Top             =   180
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  AllowNull       =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorEven   =   16776960
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "CUNIDAD"
                  Height          =   315
                  Index           =   2
                  Left            =   1200
                  TabIndex        =   7
                  Tag             =   "Unidad|Unidad"
                  Top             =   600
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Resultado:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   4380
                  TabIndex        =   33
                  Top             =   240
                  Width           =   1305
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Unidad:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   180
                  TabIndex        =   32
                  Top             =   600
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Observaciones:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   60
                  TabIndex        =   31
                  Top             =   1680
                  Width           =   1425
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "T�cnica:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   180
                  TabIndex        =   30
                  Top             =   180
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Valor:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   180
                  TabIndex        =   29
                  Top             =   1020
                  Width           =   900
               End
            End
         End
         Begin VB.Frame fraMicro 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2685
            Left            =   -74940
            TabIndex        =   22
            Top             =   360
            Width           =   9405
            Begin SSDataWidgets_B.SSDBGrid grdssMicro 
               Height          =   2505
               Index           =   0
               Left            =   60
               TabIndex        =   23
               Top             =   120
               Width           =   9225
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   16272
               _ExtentY        =   4419
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraMorf 
      Caption         =   "Morfolog�a"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2685
      Left            =   120
      TabIndex        =   13
      Top             =   450
      Width           =   9825
      Begin TabDlg.SSTab tabMorf 
         Height          =   2190
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   9510
         _ExtentX        =   16775
         _ExtentY        =   3863
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00112.frx":0070
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cbossTipoMicro"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkMorf(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtMorf(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtMorf(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtMorf(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtMorf(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "fraLaboratorio"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssMorf"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraLaboratorio 
            BorderStyle     =   0  'None
            Caption         =   "fraLaboratorio"
            Height          =   435
            Left            =   4860
            TabIndex        =   35
            Top             =   1320
            Width           =   1575
            Begin VB.CheckBox chkMorf 
               Alignment       =   1  'Right Justify
               Caption         =   "Laboratorio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   1
               Left            =   0
               TabIndex        =   36
               Tag             =   "Activa|Activa"
               Top             =   60
               Width           =   1470
            End
         End
         Begin VB.TextBox txtMorf 
            BackColor       =   &H00E0E0E0&
            DataField       =   "cResultado"
            Height          =   285
            Index           =   3
            Left            =   2940
            MaxLength       =   20
            TabIndex        =   34
            Tag             =   "CResultado|cResultado"
            Top             =   1800
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.TextBox txtMorf 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB12_CODMORF"
            Height          =   285
            Index           =   0
            Left            =   1860
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de la Morfolog�a"
            Top             =   120
            Width           =   495
         End
         Begin VB.TextBox txtMorf 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB12_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1860
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Nombre abreviado de la Morfolog�a"
            Top             =   540
            Width           =   4635
         End
         Begin VB.TextBox txtMorf 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB12_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1860
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n de la Morfolog�a"
            Top             =   960
            Width           =   7215
         End
         Begin VB.CheckBox chkMorf 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB12_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   1140
            TabIndex        =   4
            Tag             =   "Activa|Activa"
            Top             =   1800
            Width           =   945
         End
         Begin SSDataWidgets_B.SSDBGrid grdssMorf 
            Height          =   1950
            Left            =   -74880
            TabIndex        =   12
            Top             =   90
            Width           =   8880
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15663
            _ExtentY        =   3440
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTipoMicro 
            DataField       =   "MB08_CODTIMIC"
            Height          =   315
            Left            =   1860
            TabIndex        =   3
            Tag             =   "Tipo|Tipo de microorganismo"
            Top             =   1380
            Width           =   2670
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4710
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tipo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   450
            TabIndex        =   19
            Top             =   1380
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   450
            TabIndex        =   18
            Top             =   120
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   450
            TabIndex        =   17
            Top             =   540
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   450
            TabIndex        =   16
            Top             =   960
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   6915
      Width           =   10110
      _ExtentX        =   17833
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Morfologia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMorf As New clsCWForm
Dim objMicro As New clsCWForm
Dim objResult As New clsCWForm

Dim blnPostRead As Boolean
Private Sub cbossResult_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossResult_CloseUp(Index As Integer)
    'Se llena el combo de los resultados seg�n la t�cnica seleccionada
    If Index = 0 Then
        If cbossResult(0).Columns(0).Text <> "" Then
            cbossResult(0).Text = cbossResult(0).Columns(1).Text
            Call pComboResultCargar(objWinInfo, cbossResult(1), cbossResult(0).Columns(0).Text)
            cbossResult(1).Text = cbossResult(1).Columns(1).Text
        End If
    End If

End Sub

Private Sub cbossResult_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossResult_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossResult_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossTipoMicro_Click()
    Call objWinInfo.CtrlDataChange
    Call cbossTipoMicro_LostFocus
End Sub

Private Sub cbossTipoMicro_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossTipoMicro_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossTipoMicro_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkResult_Click()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkResult_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkResult_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Form_Load()
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMorf
    .strName = "Morf"
    Set .objFormContainer = fraMorf
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabMorf
    Set .grdGrid = grdssMorf
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1200"
    .strWhere = "MB1200.MB08_codTiMic IN (SELECT MB08_codTiMic FROM MB0800 WHERE cDptoSecc = " & departamento & ")" 'nombre tabla para report
    Call .FormAddOrderField("MB12_CODMORF", cwAscending)
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Morfolog�a")
    Call .FormAddFilterWhere(strKey, "MB12_CODMORF", "C�digo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "MB12_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB12_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB12_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "MB12_CODMORF", "C�digo")
    Call .FormAddFilterOrder(strKey, "MB12_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB12_DESCRIP", "Descripci�n")
    
    Call .objPrinter.Add("MB00151", "Morfolog�as")
    
  End With
     
  With objMicro
    .strName = "Micro"
    Set .objFormContainer = fraMicro
    Set .objFatherContainer = fraMorf
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssMicro(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1800"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("MB18_CODMICRO", cwAscending)
    Call .FormAddRelation("MB12_CODMORF", txtMorf(0))
  End With
  
  With objResult
    .strName = "Result"
    Set .objFormContainer = fraResult
    Set .objFatherContainer = fraMorf
    Set .tabMainTab = tabResult
    Set .grdGrid = grdssResult(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2500"
    .blnAskPrimary = False
    Call .FormAddOrderField("MB09_CODTEC", cwAscending)
    Call .FormAddOrderField("MB32_CODRESULT", cwAscending)
    Call .FormAddRelation("MB12_CODMORF", txtMorf(0))
  End With
   
  With objWinInfo
    Call .FormAddInfo(objMorf, cwFormDetail)
    
    Call .FormAddInfo(objMicro, cwFormMultiLine)
    Call .GridAddColumn(objMicro, "codMicro", "MB18_CODMICRO", cwNumeric, 4)
    Call .GridAddColumn(objMicro, "Microorganismo", "MB18_DESIG", cwString, 20)
    Call .GridAddColumn(objMicro, "G�nero Microorganismo", "", cwString, 20)

    Call .FormAddInfo(objResult, cwFormDetail)
    
    Call .FormCreateInfo(objMorf)
    
    .CtrlGetInfo(chkMorf(0)).blnInFind = True
    .CtrlGetInfo(txtMorf(0)).blnInFind = True
    .CtrlGetInfo(txtMorf(0)).blnValidate = False
    .CtrlGetInfo(txtMorf(1)).blnInFind = True
    .CtrlGetInfo(txtMorf(2)).blnInFind = True
    .CtrlGetInfo(cbossTipoMicro).blnInFind = True
    .CtrlGetInfo(txtMorf(3)).blnInGrid = False
    
    .CtrlGetInfo(txtMorf(3)).blnMandatory = False
    
    SQL = "SELECT MB08_CodTiMic, MB08_Desig FROM MB0800"
    SQL = SQL & " WHERE MB08_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB08_Desig"
    .CtrlGetInfo(cbossTipoMicro).strSQL = SQL
    .CtrlGetInfo(cbossTipoMicro).blnForeign = True
    
    Call .FormChangeColor(objMicro)
    grdssMicro(0).Columns(0).Visible = False
    grdssMicro(0).Columns(3).Visible = False
    .CtrlGetInfo(grdssMicro(0).Columns(4)).blnReadOnly = True
    Call .FormChangeColor(objResult)
    .CtrlGetInfo(txtResult(2)).blnInGrid = False
    .CtrlGetInfo(txtResult(3)).blnValidate = False
    .CtrlGetInfo(txtResult(3)).blnInGrid = False
    
    ' se a�aden los links
    SQL = "SELECT MB11_desig FROM MB1100,MB1800 "
    SQL = SQL & " WHERE MB1100.MB11_codGeMicro = MB1800.MB11_codGeMicro"
    SQL = SQL & " AND MB1800.MB18_codMicro = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssMicro(0).Columns(3)), "MB18_CODMICRO", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssMicro(0).Columns(3)), grdssMicro(0).Columns(5), "MB11_DESIG")
    
    ' se llenan los DropDown
    SQL = "SELECT MB09_codTec,MB09_desig FROM MB0900"
    SQL = SQL & " WHERE cDptoSecc=" & departamento
    SQL = SQL & " AND MB09_indActiva =-1"
    SQL = SQL & " AND (MB04_codTiTec <> " & constCULTIVO & " AND MB04_codTiTec <> " & constANTIBIOGRAMA & ")"
    SQL = SQL & " AND MB09_codTec  IN"
    SQL = SQL & "       (SELECT DISTINCT MB09_codTec FROM MB3200"
    SQL = SQL & "        WHERE MB32_indActiva = -1)"
    SQL = SQL & " ORDER BY MB09_desig"
    .CtrlGetInfo(cbossResult(0)).strSQL = SQL
    .CtrlGetInfo(cbossResult(0)).blnForeign = True
    
    SQL = "SELECT MB32_codResult,MB32_desig FROM MB3200"
    SQL = SQL & " WHERE MB09_CODTEC = 0" 'No traer ning�n resultado hasta que no se seleccione la t�cnica
    SQL = SQL & " ORDER BY MB32_desig"
    .CtrlGetInfo(cbossResult(1)).strSQL = SQL
'    .CtrlGetInfo(cbossResult(1)).blnForeign = True
    
    SQL = "SELECT cUnidad, designacion FROM unidades"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(cbossResult(2)).strSQL = SQL
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraMicro, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraResult, True, True)
    End Select
End Sub

Private Sub fraResult_Click()
  Call objWinInfo.FormChangeActive(fraResult, False, True)
End Sub

Private Sub grdssResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResult_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResult_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    Select Case strFormName
    Case objMorf.strName
        If strCtrl = objWinInfo.CtrlGetInfo(cbossTipoMicro).strName Then
            frmA_TipoMicroorganismo.Show vbModal
            Set frmA_TipoMicroorganismo = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTipoMicro))
        End If
    Case objResult.strName
        If strCtrl = objWinInfo.CtrlGetInfo(cbossResult(0)).strName Then
            frmA_Tecnica.Show vbModal
            Set frmA_Tecnica = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossResult(0)))
        End If
    End Select
    
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
    
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: morfolog�as
    
'Si se realiza alguna operaci�n en el formulario de morfolog�as(objMorf) habr� que anotar _
el mantenimiento para las morfolog�as
    
    If strFormName = objMorf.strName Then
        Call pAnotarMantenimiento(constMANTMORFOLOGIAS)
    End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    
    'blnPostRead = True
    If strFormName = objResult.strName Then
         If grdssResult(0).Columns(1).Text <> "" Then
            Call pComboResultCargar(objWinInfo, cbossResult(1), objResult.rdoCursor("MB09_CodTec"))
            If grdssResult(0).Columns(2).Text <> "" Then
                Call pComboResultTexto(cbossResult(1), objResult.rdoCursor("MB09_codTec"), objResult.rdoCursor("MB32_codResult"))
            End If
        End If
    ElseIf strFormName = objMorf.strName Then
        If Not IsNull(objMorf.rdoCursor!cResultado) Then
            chkMorf(1).Enabled = False
            fraLaboratorio.Enabled = False
            chkMorf(1).Value = 1
        Else
            chkMorf(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkMorf(1).Value = 0
        End If
    End If
    blnPostRead = False
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: morfolog�as
    
'Si se realiza alguna operaci�n en el formulario de morfolog�as(objMorf) habr� que anotar _
el mantenimiento para las morfolog�as
    
    If strFormName = objMorf.strName Then
        Call pAnotarMantenimiento(constMANTMORFOLOGIAS)
        ' Si se ha introducido la relaci�n con laboratorio -> se genera el resultado en laboratorio
        If fraLaboratorio.Enabled = True And chkMorf(1).Value = 1 Then
            blnError = fInsertarResultadoLaboratorio(txtMorf(3).Text, txtMorf(2).Text, departamento)
        End If
    End If
  
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
    blnPostRead = True
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
    Select Case strFormName
    Case objMorf.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtMorf(0).Text = fNextClave("MB12_codMorf", "MB1200")
            objMorf.rdoCursor("MB12_codMorf") = txtMorf(0).Text
        End If
    Case objResult.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtResult(3).Text = fNextClave("MB25_codResMicro", "MB2500")
            objResult.rdoCursor("MB25_codResMicro") = txtResult(3).Text
        End If
    End Select
    
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objMorf.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
    End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraMicro, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraResult, True, True)
    End Select
End Sub
Private Sub tabResult_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabResult, False, True)
End Sub

Private Sub Timer1_Timer()
    Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraMicro.ForeColor
    Case 1
        fraDatos.ForeColor = fraResult.ForeColor
    End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssMorf_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssMorf_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssMorf_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssMorf_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssMicro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssMicro_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssMicro_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssMicro_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tabMorf_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabMorf, False, True)
End Sub



Private Sub fraMorf_Click()
  Call objWinInfo.FormChangeActive(fraMorf, False, True)
End Sub
Private Sub fraMicro_Click()
  Call objWinInfo.FormChangeActive(fraMicro, False, True)
End Sub


Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub chkMorf_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkMorf_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkMorf_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Select Case intIndex
    Case 0
        'Call objWinInfo.CtrlDataChange
    Case 1  ' Laboratorio
        If blnPostRead = False Then
            If chkMorf(1).Value = 1 Then
                txtMorf(3).Text = fCodigoResultadoLaboratorio(departamento, False)
            Else
                txtMorf(3).Text = ""
            End If
        End If
End Select
  
End Sub
Private Sub txtMorf_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtMorf_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtMorf_Change(intIndex As Integer)

  Select Case intIndex
    Case 3
        If txtMorf(3).Text = "" Then
            chkMorf(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkMorf(1).Value = 0
        End If
  End Select
  Call objWinInfo.CtrlDataChange
  
End Sub
Private Sub txtResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtResult_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
pAutotexto KeyCode
End Sub

Private Sub txtResult_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

