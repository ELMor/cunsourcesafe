VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Begin VB.Form frmConfiguracion 
   Caption         =   "Configuraci�n de P�gina"
   ClientHeight    =   1395
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3720
   LinkTopic       =   "Form1"
   ScaleHeight     =   1395
   ScaleWidth      =   3720
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   495
      Left            =   2160
      TabIndex        =   7
      Top             =   180
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Aceptar"
      Height          =   495
      Left            =   2160
      TabIndex        =   6
      Top             =   780
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Separaciones"
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   1320
      Visible         =   0   'False
      Width           =   2175
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Top             =   660
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1200
         TabIndex        =   10
         Top             =   300
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Horizontal"
         Height          =   315
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   1155
      End
      Begin VB.Label Label3 
         Caption         =   "Vertical"
         Height          =   315
         Left            =   240
         TabIndex        =   4
         Top             =   720
         Width           =   1155
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "N� Fotos por Pagina"
      Height          =   1155
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1875
      Begin ComCtl2.UpDown UpDown2 
         Height          =   255
         Left            =   1440
         TabIndex        =   13
         Top             =   720
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         _Version        =   327681
         AutoBuddy       =   -1  'True
         BuddyControl    =   "nfver"
         BuddyDispid     =   196622
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   2040
         OrigBottom      =   675
         SyncBuddy       =   -1  'True
         BuddyProperty   =   22
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   255
         Left            =   1455
         TabIndex        =   12
         Top             =   360
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         _Version        =   327681
         BuddyControl    =   "nfhor"
         BuddyDispid     =   196621
         OrigLeft        =   1680
         OrigTop         =   240
         OrigRight       =   1920
         OrigBottom      =   675
         SyncBuddy       =   -1  'True
         BuddyProperty   =   22
         Enabled         =   -1  'True
      End
      Begin MSMask.MaskEdBox nfhor 
         Height          =   255
         Left            =   1200
         TabIndex        =   8
         Top             =   360
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   450
         _Version        =   327681
         MaxLength       =   1
         Mask            =   "#"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox nfver 
         Height          =   255
         Left            =   1200
         TabIndex        =   9
         Top             =   720
         Width           =   270
         _ExtentX        =   476
         _ExtentY        =   450
         _Version        =   327681
         MaxLength       =   1
         Mask            =   "#"
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         Caption         =   "Vertical"
         Height          =   315
         Left            =   180
         TabIndex        =   2
         Top             =   720
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Horizontal"
         Height          =   315
         Left            =   180
         TabIndex        =   1
         Top             =   360
         Width           =   1155
      End
   End
End
Attribute VB_Name = "frmConfiguracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

Call objPipe.PipeSet("nfotosh", nfhor.Text)
Call objPipe.PipeSet("nfotosv", nfver.Text)
'Call objPipe.PipeSet("seph", Text1.Text)
'Call objPipe.PipeSet("sepv", Text2.Text)
'(ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
'(ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
res = WritePrivateProfileString("config", "anchura", nfhor.Text, "C:\FARMACIA\DIMFOTOS.INI")
res = WritePrivateProfileString("config", "altura", nfver.Text, "C:\FARMACIA\DIMFOTOS.INI")
'res = WritePrivateProfileString("config", "sephoriz", Text1.Text, "C:\FARMACIA\DIMFOTOS.INI")
'res = WritePrivateProfileString("config", "sepverti", Text2.Text, "C:\FARMACIA\DIMFOTOS.INI")
Me.Hide
If frmA_Medicamento.tabTab1.Item(0).Tab = 2 Then
    frmA_Medicamento.QuitaMarcosTextos
    frmA_Medicamento.LeeDimensiones
    frmA_Medicamento.PosicionaFotos
    frmA_Medicamento.CreatePage
End If
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()

Dim nfh%, nfv%, ancho As Double, alto As Double

    nfhor.Text = objPipe.PipeGet("nfotosh")
    nfver.Text = objPipe.PipeGet("nfotosv")
'    Text1.Text = objPipe.PipeGet("seph")
'    Text2.Text = objPipe.PipeGet("sepv")
    UpDown1.AutoBuddy = True
    UpDown1.BuddyControl = nfhor
End Sub

