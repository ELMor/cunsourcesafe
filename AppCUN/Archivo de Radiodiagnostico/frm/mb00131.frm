VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_GrAntibiotico 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Grupo Antibi�ticos"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9795
   HelpContextID   =   30001
   Icon            =   "mb00131.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9795
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraAnti 
      Caption         =   "Antibi�ticos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   3120
      Width           =   9465
      Begin SSDataWidgets_B.SSDBGrid grdssAnti 
         Height          =   2940
         Index           =   0
         Left            =   180
         TabIndex        =   13
         Tag             =   "Antibi�ticos|Antibi�ticos"
         Top             =   300
         Width           =   9195
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16219
         _ExtentY        =   5186
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraGrAnti 
      Caption         =   "Grupos de Antibi�ticos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2565
      Index           =   0
      Left            =   90
      TabIndex        =   6
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabGrAnti 
         Height          =   2070
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   3651
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00131.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chkGrAnti(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtGrAnti(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtGrAnti(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtGrAnti(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtGrAnti(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00131.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssGrAnti(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtGrAnti 
            BackColor       =   &H00FFFF00&
            DataField       =   "CDPTOSECC"
            Height          =   285
            Index           =   3
            Left            =   4680
            MaxLength       =   50
            TabIndex        =   14
            Tag             =   "C. Secci�n|C�digo de la Secci�n"
            Top             =   120
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.TextBox txtGrAnti 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB31_CODGRANTI"
            Height          =   285
            Index           =   0
            Left            =   1860
            MaxLength       =   2
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del grupo"
            Top             =   180
            Width           =   495
         End
         Begin VB.TextBox txtGrAnti 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB31_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1860
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del grupo"
            Top             =   660
            Width           =   3375
         End
         Begin VB.TextBox txtGrAnti 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB31_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1860
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del grupo"
            Top             =   1140
            Width           =   5835
         End
         Begin VB.CheckBox chkGrAnti 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB31_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   1020
            TabIndex        =   3
            Tag             =   "Activa|Activa"
            Top             =   1620
            Width           =   1065
         End
         Begin SSDataWidgets_B.SSDBGrid grdssGrAnti 
            Height          =   1815
            Index           =   0
            Left            =   -74850
            TabIndex        =   5
            Top             =   90
            Width           =   8595
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15161
            _ExtentY        =   3201
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   480
            TabIndex        =   12
            Top             =   180
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   450
            TabIndex        =   11
            Top             =   660
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   480
            TabIndex        =   10
            Top             =   1140
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   6555
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_GrAntibiotico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
  
Private Sub Form_Load()
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraGrAnti(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabGrAnti(0)
    Set .grdGrid = grdssGrAnti(0)
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB3100"
    .strWhere = "MB3100.cDptoSecc = " & departamento 'nombre tabla para report
    Call .FormAddOrderField("MB3100.MB31_CODGRANTI", cwAscending) 'nombre tabla para report
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Grupos de Antibi�ticos")
    Call .FormAddFilterWhere(strKey, "MB3100.MB31_CODGRANTI", "C�digo", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB31_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB31_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB31_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "MB3100.MB31_CODGRANTI", "C�digo") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB31_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB31_DESCRIP", "Descripci�n")
    
    Call .objPrinter.Add("MB00136", "Grupos de Antibi�ticos")
    
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraAnti(0)
    Set .objFatherContainer = fraGrAnti(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssAnti(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1700"
    Call .FormAddOrderField("MB1700.MB07_CODANTI", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("MB31_CODGRANTI", txtGrAnti(0))
  
    Call .objPrinter.Add("MB00136", "Grupos de Antibi�ticos")
  
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "Grupo Antibi�tico", "MB31_CODGRANTI", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Antibi�tico", "MB07_CODANTI", cwNumeric, 4)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(chkGrAnti(0)).blnInFind = True
    .CtrlGetInfo(txtGrAnti(0)).blnValidate = False
    .CtrlGetInfo(txtGrAnti(0)).blnInFind = True
    .CtrlGetInfo(txtGrAnti(1)).blnInFind = True
    .CtrlGetInfo(txtGrAnti(2)).blnInFind = True
    .CtrlGetInfo(txtGrAnti(3)).blnValidate = False
    .CtrlGetInfo(txtGrAnti(3)).blnInGrid = False
   
    Call .FormChangeColor(objMultiInfo)
    grdssAnti(0).Columns(3).Visible = False
    
    SQL = "SELECT MB07_CodAnti, MB07_Desig FROM MB0700 "
    SQL = SQL & " WHERE MB07_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB07_Desig"
    .CtrlGetInfo(grdssAnti(0).Columns(4)).strSQL = SQL
    .CtrlGetInfo(grdssAnti(0).Columns(4)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    If strFormName = objMultiInfo.strName Then
        If strCtrl = objWinInfo.CtrlGetInfo(grdssAnti(0).Columns(4)).strName Then
            frmA_Antibiotico.Show vbModal
            Set frmA_Antibiotico = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssAnti(0).Columns(4)))
        End If
    End If
    
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
       
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: antibi�ticos
    
'Si se realiza alguna operaci�n en el formulario de grupos de antibi�ticos(objMasterInfo) _
o sobre el de antibi�ticos del grupo)objMultiInfo) habr� que anotar el mantenimiento para _
los antibi�ticos
    
    If strFormName = objMasterInfo.strName Or strFormName = objMultiInfo.strName Then
        Call pAnotarMantenimiento(constMANTANTIBIOTICOS)
    End If
    
End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: antibi�ticos
    
'Si se realiza alguna operaci�n en el formulario de grupos de antibi�ticos(objMasterInfo) _
o sobre el de antibi�ticos del grupo)objMultiInfo) habr� que anotar el mantenimiento para _
los antibi�ticos
    
    If strFormName = objMasterInfo.strName Or strFormName = objMultiInfo.strName Then
        Call pAnotarMantenimiento(constMANTANTIBIOTICOS)
    End If
    
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
    If strFormName = objMasterInfo.strName Then
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtGrAnti(0).Text = fNextClave("MB31_codGrAnti", "MB3100")
            txtGrAnti(3).Text = departamento
            objMasterInfo.rdoCursor("MB31_codGrAnti") = txtGrAnti(0).Text
            objMasterInfo.rdoCursor("cDptoSecc") = txtGrAnti(3).Text
        End If
    End If
    
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objMasterInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
    Case objMultiInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  
  End Select

End Sub


Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssGrAnti_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssGrAnti_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssGrAnti_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssGrAnti_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssAnti_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssAnti_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssAnti_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssAnti_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tabGrAnti_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabGrAnti(intIndex), False, True)
End Sub



Private Sub fraGrAnti_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraGrAnti(intIndex), False, True)
End Sub
Private Sub fraAnti_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraAnti(intIndex), False, True)
End Sub


Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub chkGrAnti_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkGrAnti_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkGrAnti_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtGrAnti_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtGrAnti_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtGrAnti_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

