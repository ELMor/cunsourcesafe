VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form frmG_Problema 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Problemas Detectados"
   ClientHeight    =   4245
   ClientLeft      =   75
   ClientTop       =   360
   ClientWidth     =   7995
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   7995
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Caption         =   "Aceptar"
      Height          =   390
      Left            =   5220
      TabIndex        =   2
      Top             =   3600
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   390
      Left            =   6825
      TabIndex        =   3
      Top             =   3600
      Width           =   1140
   End
   Begin Threed.SSPanel SSPanel2 
      Height          =   2175
      Left            =   60
      TabIndex        =   11
      Top             =   1260
      Width           =   7875
      _Version        =   65536
      _ExtentX        =   13891
      _ExtentY        =   3836
      _StockProps     =   15
      Caption         =   "Problema Detectado"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin VB.TextBox txtProblema 
         DataField       =   "MB28_OBSERV"
         DataSource      =   "msrdcProblemas"
         Height          =   1095
         Left            =   1260
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Tag             =   "Observaciones|Observaciones"
         Top             =   900
         Width           =   6495
      End
      Begin SSDataWidgets_B.SSDBCombo cbossProblema 
         Height          =   315
         Left            =   1260
         TabIndex        =   0
         Tag             =   "Problema|Tipo de Problema"
         Top             =   360
         Width           =   2115
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Designaci�n"
         Columns(1).Name =   "Designaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3731
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Observaciones:"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   13
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Problema:"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   12
         Top             =   420
         Width           =   915
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   1035
      Left            =   60
      TabIndex        =   4
      Top             =   180
      Width           =   7875
      _Version        =   65536
      _ExtentX        =   13891
      _ExtentY        =   1826
      _StockProps     =   15
      Caption         =   "Identificaci�n"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   1
         Left            =   3480
         Locked          =   -1  'True
         TabIndex        =   14
         Tag             =   "N� Muestra|N�mero de Muestra"
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   3
         Left            =   7320
         Locked          =   -1  'True
         TabIndex        =   9
         Tag             =   "N� Colonia|N�mero de Colonia"
         Top             =   420
         Width           =   435
      End
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   2
         Left            =   5580
         Locked          =   -1  'True
         TabIndex        =   7
         Tag             =   "N� Placa|N�mero de Placa"
         Top             =   420
         Width           =   615
      End
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   0
         Left            =   1260
         Locked          =   -1  'True
         TabIndex        =   5
         Tag             =   "N� Ref|N�mero de Referencia"
         Top             =   420
         Width           =   975
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Muestra:"
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   15
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Colonia:"
         Height          =   255
         Index           =   3
         Left            =   6300
         TabIndex        =   10
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Placa:"
         Height          =   255
         Index           =   2
         Left            =   4740
         TabIndex        =   8
         Top             =   480
         Width           =   735
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Referencia:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   1095
      End
   End
   Begin MSRDC.MSRDC msrdcProblemas 
      Height          =   330
      Left            =   180
      Top             =   3600
      Visible         =   0   'False
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   1
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   15
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmG_Problema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim nRef As String
    Dim cMuestra As String
    Dim codTecAsist As String
    Dim codPlaca As String
    Dim CodCol As String
    Dim existeRegistro As Boolean
    Dim intCancelarGuardar As Boolean
Private Function fGuardarProblema() As Boolean
    Dim SQL As String
    Dim qryProbl As rdoQuery
    Dim rsProbl As rdoResultset
    Dim cProbl As String
    Dim ahora As String
    Dim tiOrig As String
    Dim codOrig As String
    'Dim cUser As String
    
    On Error Resume Next
    If cbossProblema.Text <> "" Then
        cProbl = cbossProblema.Columns(0).Text
        ahora = fFechaHoraActual
        tiOrig = fOrigenProblema(codOrig)
        
        If existeRegistro = False Then
            msrdcProblemas.Resultset!nRef = nRef
            msrdcProblemas.Resultset!MB28_TiOrig = tiOrig
            msrdcProblemas.Resultset!MB28_CodOrig = codOrig
            msrdcProblemas.Resultset!MB22_CodProbl = cProbl
            msrdcProblemas.Resultset!MB28_FecProbl = ahora
            msrdcProblemas.Resultset.Update
            If Err <> 0 Then
                MsgBox "Imposible guardar los datos.", vbError, "Problema"
            Else
                fGuardarProblema = True
            End If
        Else
            fGuardarProblema = True
        End If
    Else
        MsgBox "Debe especificar el tipo de problema producido.", vbExclamation, "Problema"
    End If
End Function

Private Function fOrigenProblema(codOrig$) As String
    If CodCol <> "" Then
        fOrigenProblema = constORIGENCOLONIA
        codOrig = CodCol
    ElseIf codPlaca <> "" Then
        fOrigenProblema = constORIGENPLACA
        codOrig = codTecAsist
    ElseIf codTecAsist <> "" Then
        fOrigenProblema = constORIGENTECNICA
        codOrig = codTecAsist
    Else
        fOrigenProblema = constORIGENMUESTRA
        codOrig = cMuestra
    End If
End Function
Private Sub pCargarComboProblemas()
    ' Llena el combo con los diversos tipos de problemas definidos
    Dim rsProb As rdoResultset
    Dim SQL As String

    
    ' Se recogen los tipos de problemas
    SQL = "SELECT MB22_CODProbl,MB22_Desig FROM MB2200"
    SQL = SQL & " WHERE MB22_INDActiva=-1"
    SQL = SQL & " ORDER BY MB22_Desig"
    Set rsProb = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsProb.EOF
        cbossProblema.AddItem rsProb!MB22_CodProbl & Chr$(9) & rsProb!MB22_DESIG
        rsProb.MoveNext
    Loop
    rsProb.Close
    
    ' Se actualiza el color del combo a obligatorio
    cbossProblema.BackColor = objApp.objColor.lngMandatory
End Sub

Private Sub pPrepararVentana()

    nRef = objPipe.PipeGet("nRef")
    cMuestra = objPipe.PipeGet("cMuestra")
    codTecAsist = objPipe.PipeGet("codTecAsist")
    codPlaca = objPipe.PipeGet("codPlaca")
    CodCol = objPipe.PipeGet("codCol")
    
    txtIdentificacion(0).Text = Val(Mid$(nRef, 6))
    
    If cMuestra <> "" Then
        txtIdentificacion(1).Text = cMuestra
        txtIdentificacion(1).Visible = True
        lblIdentificacion(1).Visible = True
    Else
        txtIdentificacion(1).Visible = False
        lblIdentificacion(1).Visible = False
    End If
    If codPlaca <> "" Then
        txtIdentificacion(2).Text = codPlaca
        txtIdentificacion(2).Visible = True
        lblIdentificacion(2).Visible = True
    Else
        If codTecAsist <> "" Then
            txtIdentificacion(2).Text = codTecAsist
            txtIdentificacion(2).Visible = True
            txtIdentificacion(2).Tag = "N� T�cnica|N�mero de T�cnica en N�mero de Referencia"
            lblIdentificacion(2).Caption = "N� T�c."
            lblIdentificacion(2).Visible = True
        Else
            txtIdentificacion(2).Visible = False
            lblIdentificacion(2).Visible = False
        End If
    End If
    If CodCol <> "" Then
        txtIdentificacion(3).Text = CodCol
        txtIdentificacion(3).Visible = True
        lblIdentificacion(3).Visible = True
    Else
        txtIdentificacion(3).Visible = False
        lblIdentificacion(3).Visible = False
    End If

End Sub

Private Sub cbossProblema_Click()
    Dim SQL As String
    Dim codOrig As String
    Dim tiOrig As String

    If cbossProblema.Text <> "" Then
        'Se anula lo escrito si no se ha pulsado "Aceptar", para que
        'no proteste al llegar a msrdcProblemas.Refresh
        txtProblema.DataChanged = False
        
        SQL = "SELECT nRef,MB28_tiOrig,MB28_codOrig,MB22_codProbl,MB28_FECProbl,MB28_Observ FROM MB2800"
        SQL = SQL & " WHERE nRef = '" & nRef & "'"
        'Obtenci�n del codOrig
        tiOrig = fOrigenProblema(codOrig)
        SQL = SQL & " AND MB28_tiOrig = " & tiOrig
        SQL = SQL & " AND MB28_codOrig = '" & codOrig & "'"
        SQL = SQL & " AND MB22_codProbl = " & cbossProblema.Columns(0).Text
        msrdcProblemas.SQL = SQL
        msrdcProblemas.Refresh
        If msrdcProblemas.Resultset.EOF = False Then
            existeRegistro = True
        Else
            msrdcProblemas.Resultset.AddNew
            existeRegistro = False
        End If
    Else
        Exit Sub
    End If
End Sub

Private Sub cmdCancel_Click()
    txtProblema.DataChanged = False
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Dim res As Boolean
    
    res = fGuardarProblema()
    If res = True Then
        Unload Me
    End If
End Sub
Private Sub Form_Load()
    msrdcProblemas.Connect = objApp.rdoConnect.Connect
    pPrepararVentana
    pCargarComboProblemas
    existeRegistro = True
End Sub

Private Sub txtProblema_KeyDown(KeyCode As Integer, Shift As Integer)
pAutotexto KeyCode
End Sub


