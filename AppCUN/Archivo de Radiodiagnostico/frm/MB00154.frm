VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmI_HojaTrabajo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Impresi�n de Hojas de Trabajo"
   ClientHeight    =   4245
   ClientLeft      =   1200
   ClientTop       =   1635
   ClientWidth     =   9315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4245
   ScaleWidth      =   9315
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbInicio 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   688
      _Version        =   327682
   End
   Begin VB.Frame fraCarpeta 
      Caption         =   "Carpetas"
      Height          =   3375
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   9075
      Begin TabDlg.SSTab tabCarpeta 
         Height          =   2775
         HelpContextID   =   90001
         Index           =   0
         Left            =   180
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   300
         Width           =   8715
         _ExtentX        =   15372
         _ExtentY        =   4895
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00154.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboSSCarpeta"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtCarpeta(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtCarpeta(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cmdHojaTrabajo(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "fraCuentaSiembras(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "fraCuentaResiembras(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtCarpeta(6)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cmdHojaTrabajo(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdSSCarpeta(0)"
         Tab(1).ControlCount=   1
         Begin VB.CommandButton cmdHojaTrabajo 
            Caption         =   "&Hoja de Resultados"
            Height          =   495
            Index           =   1
            Left            =   6900
            TabIndex        =   22
            Top             =   1560
            Width           =   1215
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H80000004&
            DataField       =   "cCarpeta"
            Height          =   285
            Index           =   6
            Left            =   6120
            TabIndex        =   21
            Top             =   120
            Visible         =   0   'False
            Width           =   795
         End
         Begin VB.Frame fraCuentaResiembras 
            Caption         =   "Resiembras"
            Height          =   675
            Index           =   0
            Left            =   60
            TabIndex        =   16
            Top             =   1380
            Width           =   6135
            Begin VB.TextBox txtCarpeta 
               BackColor       =   &H00C0C0C0&
               Height          =   285
               Index           =   4
               Left            =   1080
               MaxLength       =   2
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "N� T�cnicas|N� T�cnicas a Realizar en la Carpeta"
               Text            =   "40"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtCarpeta 
               BackColor       =   &H00C0C0C0&
               Height          =   285
               Index           =   5
               Left            =   5100
               MaxLength       =   2
               TabIndex        =   17
               TabStop         =   0   'False
               Tag             =   "N� Muestras|N�mero de muestras a utilizar para las t�cnicas"
               Text            =   "11"
               Top             =   240
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� T�cnicas"
               Height          =   255
               Index           =   4
               Left            =   60
               TabIndex        =   20
               Top             =   270
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Muestras"
               Height          =   255
               Index           =   3
               Left            =   3600
               TabIndex        =   19
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame fraCuentaSiembras 
            Caption         =   "Siembras"
            Height          =   675
            Index           =   0
            Left            =   60
            TabIndex        =   11
            Top             =   600
            Width           =   6135
            Begin VB.TextBox txtCarpeta 
               BackColor       =   &H00C0C0C0&
               Height          =   285
               Index           =   3
               Left            =   5040
               MaxLength       =   2
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "N� Muestras|N�mero de muestras a utilizar para las t�cnicas"
               Text            =   "11"
               Top             =   240
               Width           =   855
            End
            Begin VB.TextBox txtCarpeta 
               BackColor       =   &H00C0C0C0&
               Height          =   285
               Index           =   2
               Left            =   1080
               MaxLength       =   2
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "N� T�cnicas|N� T�cnicas a Realizar en la Carpeta"
               Text            =   "40"
               Top             =   240
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Muestras"
               Height          =   255
               Index           =   1
               Left            =   3600
               TabIndex        =   15
               Top             =   240
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� T�cnicas"
               Height          =   255
               Index           =   5
               Left            =   60
               TabIndex        =   14
               Top             =   270
               Width           =   975
            End
         End
         Begin VB.CommandButton cmdHojaTrabajo 
            Caption         =   "&Repetir Hoja"
            Height          =   495
            Index           =   0
            Left            =   6900
            TabIndex        =   10
            Top             =   780
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H80000004&
            DataField       =   "cCarpeta"
            Height          =   285
            Index           =   0
            Left            =   5100
            TabIndex        =   8
            Top             =   150
            Visible         =   0   'False
            Width           =   795
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H00C0C0C0&
            DataField       =   "designacion"
            Height          =   285
            Index           =   1
            Left            =   1140
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Carpeta|Carpeta"
            Top             =   150
            Width           =   2175
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSCarpeta 
            Height          =   315
            Left            =   1080
            TabIndex        =   5
            Tag             =   "Usuario|Usuario que realizar� las t�cnicas"
            Top             =   2340
            Width           =   4815
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   8493
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   8493
            _ExtentY        =   556
            _StockProps     =   93
            Text            =   "E. Rodr�guez"
            BackColor       =   16777088
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdSSCarpeta 
            Height          =   2565
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   60
            Width           =   8175
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14420
            _ExtentY        =   4524
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carpeta"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   7
            Top             =   150
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Realiza"
            Height          =   255
            Index           =   0
            Left            =   60
            TabIndex        =   6
            Top             =   2430
            Width           =   975
         End
      End
   End
   Begin ComctlLib.StatusBar stbInicio 
      Align           =   2  'Align Bottom
      Height          =   240
      Left            =   0
      TabIndex        =   1
      Top             =   4005
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   423
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.ImageList imlIconos 
      Left            =   480
      Top             =   300
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "frmI_HojaTrabajo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim objCarpetaInfo As New clsCWForm
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim cUser As String
Private Sub pImprimirHoja(blnImprimirPorCarpeta As Boolean, blnSiembras As Boolean)
        Dim intRes As Integer
        Dim strCCarpeta As String
        Dim strTexto As String
        ' Comprueba que se hayan introducido los datos de impresi�n y se imprime la hoja de trabajo
        
        If blnImprimirPorCarpeta = True Then
            strCCarpeta = txtCarpeta(0).Text
            strTexto = "�Desea imprimir la hoja de trabajo de la carpeta '" & txtCarpeta(1).Text & "'?"
        Else
            strCCarpeta = ""
            strTexto = "�Desea imprimir la hoja de trabajo Completa?"
        End If
        
        If cUser = "" Then
            MsgBox "Debe indicar la persona que realizar� las t�cnicas.", vbExclamation, "Hoja de Trabajo"
        Else
            intRes = MsgBox(strTexto, vbQuestion + vbYesNo, "Hoja de Trabajo")
            If intRes = vbYes Then
                Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
                intRes = fImprimirHoja(strCCarpeta, cUser, blnSiembras)
                'pActualizarDatos
                Screen.MousePointer = vbDefault 'Call objApp.SplashOff
            End If
        End If
          

End Sub

Private Sub pActualizarDatos()
    ' Actualiza los datos de n� de pruebas y t�cnicas en la ventana
    
    Dim SQL As String
    Dim qryDatos As rdoQuery
    Dim rsDatos As rdoResultset
    
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec, COUNT(distinct mP.cMuestra) as nMuestra"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND pA.estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND MB20_INDSiembra=-1"
    SQL = SQL & " AND MB2000.MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & " AND MB09_CODTEC NOT IN (SELECT MB09_CODTEC FROM MB0900 WHERE MB04_CODTITEC IN (" & constRECUENTO & "," & constRESULTADOS & "))"
    SQL = SQL & " GROUP BY pA.cCarpeta"
        
    Set qryDatos = objApp.rdoConnect.CreateQuery("Datos Carpeta", SQL)
    qryDatos(0) = txtCarpeta(0).Text
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        txtCarpeta(2).Text = rsDatos!nTec
        txtCarpeta(3).Text = rsDatos!nMuestra
    Else
        txtCarpeta(2).Text = ""
        txtCarpeta(3).Text = ""
    End If
    rsDatos.Close
    qryDatos.Close
    
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec, COUNT(distinct mP.cMuestra) as nMuestra"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND pA.estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND MB20_INDSiembra=0"
    SQL = SQL & " AND MB2000.MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & " AND MB09_CODTEC NOT IN (SELECT MB09_CODTEC FROM MB0900 WHERE MB04_CODTITEC IN (" & constRECUENTO & "," & constRESULTADOS & "))"
    SQL = SQL & " GROUP BY pA.cCarpeta"
        
    Set qryDatos = objApp.rdoConnect.CreateQuery("Datos Carpeta", SQL)
    qryDatos(0) = txtCarpeta(0).Text
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        txtCarpeta(4).Text = rsDatos!nTec
        txtCarpeta(5).Text = rsDatos!nMuestra
    Else
        txtCarpeta(4).Text = ""
        txtCarpeta(5).Text = ""
    End If
    rsDatos.Close
    qryDatos.Close

End Sub


Private Sub pImprimirHojaRepeticion(blnImprimirPorCarpeta As Boolean, blnSiembras As Boolean)
    Dim cCarpeta As String
    If blnImprimirPorCarpeta = True Then
        cCarpeta = txtCarpeta(0).Text
    Else
        cCarpeta = ""
    End If
    pVentanaHojaTrabajoRepeticion cCarpeta, blnSiembras, False
End Sub

Private Sub pMostrarUsuario()
    Dim nombreUser As String
    
    ' Se muestra el usuario seleccionado por defecto en la carpeta
    cUser = ""
    cUser = fUserDefectoCarpeta(objCarpetaInfo.rdoCursor!cCarpeta, nombreUser)

    cboSSCarpeta.Text = nombreUser
End Sub

Private Sub cboSSCarpeta_Click()
cUser = cboSSCarpeta.Columns(0).Text
End Sub

Private Sub cboSSCarpeta_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSCarpeta_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cmdHojaTrabajo_Click(intIndex As Integer)
    Select Case intIndex
        'Case 0  ' Hojas de repetici�n
            'pVentanaHojaTrabajoRepeticion (txtCarpeta(0).Text)
            'pActualizarDatos
        Case 1
            pVentanaHojaTrabajoRepeticion txtCarpeta(0).Text, False, True
            pActualizarDatos
    End Select
End Sub

Private Sub Form_Load()

  Dim strKey As String
  Dim SQL As String
  Dim i As Integer
    
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbInicio, stbInicio, _
                                cwWithToolBar + cwWithStatusBar + cwWithoutMenu)
  
  With objCarpetaInfo
    .strName = "CARPETA"
    Set .objFormContainer = fraCarpeta(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabCarpeta(0)
    Set .grdGrid = grdSSCarpeta(0)
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "carpetas"
    .strWhere = "cDptoSecc=" & departamento
    .intAllowance = cwAllowModify
'    .intAllowance = cwAllowReadOnly
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    
    Call .FormAddOrderField("designacion", cwAscending)
  
    strKey = .strDataBase & .strTable
    
    Call .objPrinter.Add("MB00101", "Hoja de Trabajo de Siembras por Carpeta", 1, crptToPrinter)
    Call .objPrinter.Add("MB00102", "Hoja de Trabajo de Resiembras por Carpeta", 1, crptToPrinter)
    Call .objPrinter.Add("MB00103", "Hoja de Trabajo de Siembras Completa", 1, crptToPrinter)
    Call .objPrinter.Add("MB00104", "Hoja de Trabajo de Resiembras Completa", 1, crptToPrinter)
    Call .objPrinter.Add("MB00105", "Repetir H. Trabajo de Siembras por Carpeta", 1, crptToPrinter)
    Call .objPrinter.Add("MB00106", "Repetir H. Trabajo de Resiembras por Carpeta", 1, crptToPrinter)
    Call .objPrinter.Add("MB00107", "Repetir H. Trabajo de Siembras Completa", 1, crptToPrinter)
    Call .objPrinter.Add("MB00108", "Repetir H. Trabajo de Resiembras Completa", 1, crptToPrinter)
    
  End With
  
  'With objMultiInfo
  '  Set .objFormContainer = fraAnti(0)
  '  Set .objFatherContainer = fraTiTec(0)
  '  Set .tabMainTab = Nothing
  '  Set .grdGrid = grdssAnti(0)
  '  .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  '
  '  .strDataBase = objEnv.GetValue("Main")
  '  .strTable = "MB1700"

  '  Call .FormAddOrderField("MB07_CODANTI", cwAscending)
  '  Call .FormAddRelation("MB31_CODTiTec", txtTiTec(0))
  
  '  strKey = .strDataBase & .strTable
  'End With
  
  With objWinInfo
    Call .FormAddInfo(objCarpetaInfo, cwFormDetail)
  '  Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
  '  Call .GridAddColumn("Grupo Antibi�tico", "MB31_CODTiTec", cwNumeric, 4)
  '  Call .GridAddColumn("Antibi�tico", "MB07_CODANTI", cwNumeric, 4)

    Call .FormCreateInfo(objCarpetaInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    
  '  grdssAnti(0).Columns(3).Visible = False
  '  Call .FormChangeColor(objMultiInfo)
    
  '  .CtrlGetInfo(grdssAnti(0).Columns(4)).strSQL = "SELECT MB07_CODANTI, MB07_DESIG FROM " & objEnv.GetValue("Main") & "MB0700 ORDER BY MB07_DESIG"
  
    
    ' Siembras
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec, COUNT(distinct mP.cMuestra) as nMuestra"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND pA.estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND MB2000.MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & " AND MB2000.MB20_INDSiembra = -1"
    SQL = SQL & " AND MB09_CODTEC NOT IN (SELECT MB09_CODTEC FROM MB0900 WHERE MB04_CODTITEC IN (" & constRECUENTO & "," & constRESULTADOS & "))"
    SQL = SQL & " GROUP BY pA.cCarpeta"

    Call .CtrlCreateLinked(.CtrlGetInfo(txtCarpeta(0)), "cCarpeta", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(0)), txtCarpeta(2), "nTec")
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(0)), txtCarpeta(3), "nMuestra")
  

    ' Resiembras
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec1, COUNT(distinct mP.cMuestra) as nMuestra1"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND pA.estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND MB2000.MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & " AND MB2000.MB20_INDSiembra= 0"
    SQL = SQL & " AND MB09_CODTEC NOT IN (SELECT MB09_CODTEC FROM MB0900 WHERE MB04_CODTITEC IN (" & constRECUENTO & "," & constRESULTADOS & "))"
    SQL = SQL & " GROUP BY pA.cCarpeta"

    Call .CtrlCreateLinked(.CtrlGetInfo(txtCarpeta(6)), "cCarpeta", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(6)), txtCarpeta(4), "nTec1")
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(6)), txtCarpeta(5), "nMuestra1")

    ' Combo de usuarios
    SQL = "SELECT DISTINCT u.cUser,nombre||' '||Apellido1||' '||Apellido2 as nomUser"
    SQL = SQL & " FROM users u, usersPerfil uP "
    SQL = SQL & " WHERE u.cUser=uP.cUser"
    SQL = SQL & " AND uP.cDptoSecc = " & departamento
    .CtrlGetInfo(cboSSCarpeta).strSQL = SQL

    For i = 0 To 6
        .CtrlGetInfo(txtCarpeta(i)).blnReadOnly = True
    Next i
    
    .CtrlGetInfo(txtCarpeta(0)).blnInGrid = False
    '.CtrlGetInfo(txtCarpeta(2)).blnInGrid = True
    '.CtrlGetInfo(txtCarpeta(2)).blnNegotiated = False

    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, UnloadMode As Integer)
intCancel = objWinInfo.WinExit
End Sub


Private Sub Form_Unload(Cancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub fraCarpeta_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraCarpeta(intIndex), False, True)
End Sub

Private Sub grdSSCarpeta_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSCarpeta_GotFocus(Index As Integer)
   Call objWinInfo.CtrlGotFocus

End Sub


Private Sub grdSSCarpeta_RowColChange(Index As Integer, ByVal intLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(intLastRow, intLastCol)

End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
        
    Select Case UCase(strFormName)
        Case "CARPETA"
            If objCarpetaInfo.rdoCursor.RowCount <> 0 Then
                pMostrarUsuario
            End If
    End Select
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    blnCancel = True
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  
  Select Case strFormName
    Case objCarpetaInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        Select Case intReport
            Case 1
                pImprimirHoja True, True
            Case 2
                pImprimirHoja True, False
            Case 3
                pImprimirHoja False, True
            Case 4
                pImprimirHoja False, False
            Case 5
                pImprimirHojaRepeticion True, True
            Case 6
                pImprimirHojaRepeticion True, False
            Case 7
                pImprimirHojaRepeticion False, True
            Case 8
                pImprimirHojaRepeticion False, False
        End Select
        Set objPrinter = Nothing
        pActualizarDatos
  End Select
End Sub


Private Sub tabCarpeta_MouseDown(intIndex As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabCarpeta(intIndex), False, True)

End Sub


Private Sub tlbInicio_ButtonClick(ByVal btnButton As ComctlLib.Button)
    
    
    'Select Case btnButton.Key
    '    Case cwToolBarButtonPrint  ' Imprimir
    '        pImprimirHoja
    '    Case Else
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'End Select
End Sub

Private Sub txtCarpeta_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtCarpeta_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub


Private Sub txtCarpeta_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus

End Sub


