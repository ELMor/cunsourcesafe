VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmP_Resultados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Resultados de T�cnicas"
   ClientHeight    =   6855
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9750
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "MB00163.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6855
   ScaleWidth      =   9750
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   9750
      _ExtentX        =   17198
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraResultados 
      Caption         =   "Resultados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   3120
      Width           =   9465
      Begin SSDataWidgets_B.SSDBGrid grdssResultados 
         Height          =   2940
         Index           =   0
         Left            =   180
         TabIndex        =   9
         Top             =   300
         Width           =   9195
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         MultiLine       =   0   'False
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16219
         _ExtentY        =   5186
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraCarp 
      Caption         =   "Carpetas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2565
      Index           =   0
      Left            =   90
      TabIndex        =   4
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabCarp 
         Height          =   2025
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   3572
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00163.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtCarpetas(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtCarpetas(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtCarpetas(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtCarpetas(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtCarpetas(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtCarpetas(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "MB00163.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssCarp(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            DataField       =   "codTec\\MB0900.MB09_CODTEC"
            Height          =   285
            Index           =   5
            Left            =   4800
            MaxLength       =   3
            TabIndex        =   16
            Tag             =   "C�digo|C�digo de la carpeta"
            Top             =   480
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB09_DESIG"
            Height          =   285
            Index           =   4
            Left            =   6120
            MaxLength       =   50
            TabIndex        =   14
            Tag             =   "Designaci�n|Designaci�n de la carpeta"
            Top             =   900
            Width           =   2595
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   3
            Left            =   1200
            MaxLength       =   50
            TabIndex        =   12
            Tag             =   "Impreso por|Identificador de la persona que imprimi� la hoja de trabajo"
            Top             =   840
            Width           =   2595
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   2
            Left            =   6120
            MaxLength       =   50
            TabIndex        =   10
            Tag             =   "Fecha de Impresi�n|Fecha de Impresi�n"
            Top             =   120
            Width           =   2595
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   0
            Left            =   3960
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de la carpeta"
            Top             =   480
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   1
            Left            =   1200
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n de la carpeta"
            Top             =   120
            Width           =   2595
         End
         Begin SSDataWidgets_B.SSDBGrid grdssCarp 
            Height          =   1770
            Index           =   0
            Left            =   -74910
            TabIndex        =   3
            Top             =   90
            Width           =   8700
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   15346
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "T�cnica:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   4140
            TabIndex        =   15
            Top             =   900
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Impreso por:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   13
            Top             =   840
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha de Impresi�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   4140
            TabIndex        =   11
            Top             =   120
            Width           =   1905
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carpeta:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   8
            Top             =   180
            Width           =   1305
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   6570
      Width           =   9750
      _ExtentX        =   17198
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmP_Resultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm

Dim strFecha As String
Dim strCUser As String
Dim strCCarpeta As String

Private Sub pAnchuraColumna()
    grdssResultados(0).Columns(4).Width = 1000
    grdssResultados(0).Columns(11).Width = 3000
End Sub

Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  Dim i As Integer

  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
    
    strCCarpeta = objPipe.PipeGet("MB_cCarpeta_Result")
    strFecha = objPipe.PipeGet("MB_fechaReali_Result")
    strCUser = objPipe.PipeGet("MB_cUser_Result")
    

  Set objWinInfo = New clsCWWin

  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  With objMasterInfo
    Set .objFormContainer = fraCarp(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabCarp(0)
    Set .grdGrid = grdssCarp(0)
    .strDataBase = objEnv.GetValue("Main")
    
    ' ******************************PROBLEMAS CON LAS FUNCIONES****************
    ' Lo siguiente no se hace por que las funciones no permiten utilizar ni
    ' el Distinct, ni el Group By.
'    .strTable = "pruebaAsistencia pA, carpetas c, users u, MB2000, MB0900"
'
'    SQL = "c.cCarpeta = pA.cCarpeta "
'    SQL = SQL & " AND MB2000.NRef = pA.nRef "
'    SQL = SQL & " AND MB0900.MB09_CODTEC=MB2000.MB09_CODTEC"
'    SQL = SQL & " AND u.cUser = MB2000.cUser"
'    SQL = SQL & " AND c.cCarpeta = " & strcCarpeta
'    SQL = SQL & " AND MB20_FECREALI = TO_DATE('" & strfecha & "','DD/MM/YYYY HH24:MI:SS')"
'    SQL = SQL & " AND MB2000.cUser = " & strcUser
'
'    .strWhere = SQL
    '************************************************************************************
    
    .strTable = "MB0900"

    SQL = " MB09_CODTEC IN "
    SQL = SQL & " (SELECT MB09_CODTEC FROM "
    SQL = SQL & "   pruebaAsistencia pA, carpetas c, users u, MB2000 "
    SQL = SQL & "   WHERE c.cCarpeta = pA.cCarpeta "
    SQL = SQL & "   AND MB2000.NRef = pA.nRef "
    SQL = SQL & "   AND u.cUser = MB2000.cUser"
    SQL = SQL & "   AND c.cCarpeta = " & strCCarpeta
    SQL = SQL & "   AND MB20_FECREALI = TO_DATE('" & strFecha & "','DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & "   AND MB2000.cUser = " & strCUser
    SQL = SQL & "   AND MB2000.MB34_CODESTTECASIST = " & constTECREALIZADA
    SQL = SQL & " )"
    'SQL = SQL & " AND (MB04_CODTITEC >= " & constBIOQUIMICAS & ")"
    SQL = SQL & " AND (MB04_CODTITEC >= " & constBIOQUIMICAS & " OR MB04_CODTITEC = " & constOBSERVDIRECTA & ")"

    .strWhere = SQL

    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("MB0900.MB09_DESIG", cwAscending)

    strKey = .strDataBase & .strTable

    Call .FormCreateFilterWhere(strKey, "Carpetas")
    Call .FormAddFilterWhere(strKey, "pa.cCarpeta", "C�digo Carpeta", cwNumeric, 3)
    Call .FormAddFilterWhere(strKey, "c.designacion", "Designaci�n Carpeta", cwString, 25)
    Call .FormAddFilterWhere(strKey, "MB0900.MB09_DESIG", "Designaci�n T�cnica", cwString, 25)
    Call .FormAddFilterWhere(strKey, "MB2000.MB20_FECREALI", "Fecha de Impresi�n", cwDate)
    Call .FormAddFilterWhere(strKey, "u.Ap1", "1er Apellido del usuario que realiza la hoja de trabajo", cwString, 25)
    Call .FormAddFilterWhere(strKey, "u.Ap2", "2� Apellido del usuario que realiza la hoja de trabajo", cwString, 25)

    Call .FormAddFilterOrder(strKey, "c.Designacion", "C�digo")
    Call .FormAddFilterOrder(strKey, "MB0900.MB09_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB2000.MB20_FECREALI", "Secci�n")

  End With

  With objMultiInfo
    Set .objFormContainer = fraResultados(0)
    Set .objFatherContainer = fraCarp(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssResultados(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys

    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB3300"

    Call .FormAddOrderField("MB3300.NREF", cwAscending)
    Call .FormAddOrderField("MB3300.MB20_CODTECASIST", cwAscending)
    Call .FormAddOrderField("MB3300.MB32_CODRESULT", cwAscending)

    Call .FormAddRelation("MB09_CODTEC", txtCarpetas(5))
'    Call .FormAddRelation("CCARPETA", txtCarpetas(0))

    SQL = "(MB3300.NREF,MB20_CODTECASIST) IN "
    SQL = SQL & " (SELECT PA.NREF,MB20_CODTECASIST FROM MB2000, pruebaAsistencia pA"
    SQL = SQL & "       WHERE MB2000.nRef = pA.nRef"
    SQL = SQL & "       AND MB2000.MB34_CODESTTECASIST = " & constTECREALIZADA
    SQL = SQL & "       AND MB2000.MB20_FECREALI = TO_DATE('" & strFecha & "','DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & "       AND MB2000.cUser = " & strCUser
    SQL = SQL & "       AND pA.cCarpeta = " & strCCarpeta
    SQL = SQL & "       )"

    .strWhere = SQL

  End With

  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "NRef", "NRef", cwString, 12)
    Call .GridAddColumn(objMultiInfo, "N� Ref.", "", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Cod Tec Asist", "MB20_CODTECASIST", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Cod Tec", "MB09_CODTEC", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Cod Tec Res", "MB09_CODTEC", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Cod Result", "MB32_CODResult", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "T�cnica", "", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Nombre Result.", "", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Tipo Origen", "", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Origen", "", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Cod Origen", "", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Resultado", "MB33_Result", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "cUnidad", "cUnidad", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "Unidad", "", cwString, 25)

    Call .FormCreateInfo(objMasterInfo)

    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    grdssResultados(0).Columns(3).Visible = False
    grdssResultados(0).Columns(5).Visible = False
    grdssResultados(0).Columns(6).Visible = False
    grdssResultados(0).Columns(7).Visible = False
    grdssResultados(0).Columns(8).Visible = False
    grdssResultados(0).Columns(9).Visible = False
    grdssResultados(0).Columns(11).Visible = False
    grdssResultados(0).Columns(15).Visible = False
    Call .FormChangeColor(objMultiInfo)

    
    ' se a�aden los links del detalle
    SQL = " SELECT "
    SQL = SQL & "   pA.cCarpeta, c.Designacion, u.nombre||' '||u.apellido1 as nomUser,"
    SQL = SQL & "   MB20_FECREALI"
    SQL = SQL & " FROM"
    SQL = SQL & "   pruebaAsistencia pA, carpetas c, users u, MB2000"
    SQL = SQL & " WHERE"
    SQL = SQL & "   c.cCarpeta = pA.cCarpeta "
    SQL = SQL & "   AND MB2000.NRef = pA.nRef "
    SQL = SQL & "   AND u.cUser = MB2000.cUser"
    SQL = SQL & "   AND MB2000.MB09_CODTEC=?"
    SQL = SQL & "   AND c.cCarpeta = ?"
    SQL = SQL & "   AND MB20_FECREALI = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & "   AND MB2000.cUser = ?"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtCarpetas(5)), "MB09_CODTEC", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpetas(5)), txtCarpetas(0), "cCarpeta")
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpetas(5)), txtCarpetas(1), "designacion")
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpetas(5)), txtCarpetas(2), "MB20_FECREALI")
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpetas(5)), txtCarpetas(3), "nomUser")

    
    ' se a�aden los links de la multil�nea
    ' T�cnica
    SQL = "SELECT MB09_desig FROM MB0900 WHERE MB09_codtec = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssResultados(0).Columns(6)), "MB09_CODTEC", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssResultados(0).Columns(6)), grdssResultados(0).Columns(9), "MB09_DESIG")

    ' Resultado
    SQL = "SELECT MB32_desig FROM MB3200 WHERE MB09_codtec = ? AND MB32_CODRESULT=?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssResultados(0).Columns(7)), "MB09_CODTEC", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssResultados(0).Columns(7)), grdssResultados(0).Columns(10), "MB32_DESIG")
        
    ' Origen
    SQL = "SELECT MB20_TiOrig, MB20_CODOrig FROM MB2000 WHERE NREF = ? AND MB20_CODTECASIST = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssResultados(0).Columns(3)), "NREF", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssResultados(0).Columns(3)), grdssResultados(0).Columns(11), "MB20_TiOrig")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssResultados(0).Columns(3)), grdssResultados(0).Columns(13), "MB20_CODOrig")
    
    ' Unidad
    SQL = "SELECT designacion FROM unidades WHERE cUnidad = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssResultados(0).Columns(15)), "cUnidad", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssResultados(0).Columns(15)), grdssResultados(0).Columns(16), "designacion")


    For i = 0 To 5
        .CtrlGetInfo(txtCarpetas(i)).blnReadOnly = True
    Next i
    For i = 3 To 13
        If i <> 11 Then
            .CtrlGetInfo(grdssResultados(0).Columns(i)).blnReadOnly = True
        End If
    Next i

    pAnchuraColumna ' Formatea la anchura de las columnas, ya que las funciones ponen la anchura en base a los datos de la BBDD
                    ' y no hacen caso a la anchura que se indica a mano.
    
    Call .WinRegister
    Call .WinStabilize
  End With

  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraCarp_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraCarp(intIndex), False, True)
End Sub

Private Sub fraResultados_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraResultados(intIndex), False, True)
End Sub

Private Sub grdssResultados_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    If grdssResultados(0).Columns(3).Text <> "" Then
        grdssResultados(0).Columns(4).Text = fNumeroMicro(grdssResultados(0).Columns(3).Text)
    End If
    
    If grdssResultados(0).Columns(11).Text <> "" Then
        grdssResultados(0).Columns(12).Text = fOrigenTecnica(grdssResultados(0).Columns(11).Text)
    End If
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    If strFormName = objMultiInfo.strName Then
        Select Case strCtrlName
            Case "grdssResultados(0).Cod Tec Res"
                aValues(2) = objMultiInfo.rdoCursor!MB32_CODResult
            Case "grdssResultados(0).NRef"
                aValues(2) = objMultiInfo.rdoCursor!MB20_CODTECASIST
        End Select
    ElseIf strFormName = objMasterInfo.strName Then
        Select Case strCtrlName
            Case "txtCarpetas(5)"
                aValues(2) = strCCarpeta
                aValues(3) = strFecha
                aValues(4) = strCUser
        End Select
    End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objMasterInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
 Case objMultiInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
  End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabCarp_MouseDown(intIndex As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabCarp(intIndex), False, True)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssCarp_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssCarp_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssCarp_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssCarp_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssResultados_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResultados_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResultados_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResultados_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtCarpetas_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtCarpetas_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtCarpetas_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
