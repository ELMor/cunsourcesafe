VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form frmP_Peticion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Solicitud de T�cnicas"
   ClientHeight    =   8400
   ClientLeft      =   1860
   ClientTop       =   2520
   ClientWidth     =   11970
   HelpContextID   =   30001
   Icon            =   "MB00155.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8400
   ScaleWidth      =   11970
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   48
      Top             =   0
      Width           =   11970
      _ExtentX        =   21114
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdPeticion 
      Caption         =   "Finalizar prueba"
      Height          =   375
      Index           =   4
      Left            =   10500
      TabIndex        =   35
      Top             =   7080
      Width           =   1320
   End
   Begin VB.CommandButton cmdPeticion 
      Caption         =   "Pruebas Hoy"
      Height          =   375
      Index           =   3
      Left            =   9060
      TabIndex        =   33
      Top             =   7620
      Width           =   1305
   End
   Begin VB.CommandButton cmdPeticion 
      Caption         =   "Buscar N� Ref."
      Height          =   375
      Index           =   2
      Left            =   9060
      TabIndex        =   32
      Top             =   7080
      Width           =   1305
   End
   Begin VB.CommandButton cmdResultados 
      Caption         =   "Problema"
      Height          =   375
      Index           =   2
      Left            =   9060
      TabIndex        =   31
      Top             =   6540
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.CommandButton cmdPeticion 
      Caption         =   "Finalizar placa"
      Enabled         =   0   'False
      Height          =   375
      Index           =   1
      Left            =   10500
      TabIndex        =   34
      Top             =   6540
      Width           =   1320
   End
   Begin VB.CommandButton cmdPeticion 
      Caption         =   "Paciente"
      Height          =   375
      Index           =   0
      Left            =   10500
      TabIndex        =   36
      Top             =   7620
      Width           =   1320
   End
   Begin VB.Frame fraResult 
      Caption         =   "Resultados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   57
      Top             =   6360
      Width           =   8835
      Begin VB.TextBox txtObservMuestra 
         Height          =   1335
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   87
         TabStop         =   0   'False
         Top             =   300
         Width           =   8595
      End
      Begin TabDlg.SSTab tabResCol 
         Height          =   1335
         Left            =   120
         TabIndex        =   67
         Top             =   300
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   2355
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         TabCaption(0)   =   "Identificaci�n"
         TabPicture(0)   =   "MB00155.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame3(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Observ. colonia"
         TabPicture(1)   =   "MB00155.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame3(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Observ. colonia/placa"
         TabPicture(2)   =   "MB00155.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame3(2)"
         Tab(2).ControlCount=   1
         Begin VB.Frame Frame3 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   2
            Left            =   -74940
            TabIndex        =   80
            Top             =   310
            Width           =   7100
            Begin VB.TextBox txtObserv 
               DataField       =   "mb30_observ"
               DataSource      =   "msrdcPlacas"
               Height          =   915
               Index           =   5
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   19
               Top             =   50
               Width           =   7035
            End
         End
         Begin VB.Frame Frame3 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   1
            Left            =   -74940
            TabIndex        =   79
            Top             =   310
            Width           =   7100
            Begin VB.TextBox txtObserv 
               DataField       =   "mb27_descrip"
               DataSource      =   "msrdcColonias"
               Height          =   915
               Index           =   4
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   18
               Top             =   50
               Width           =   7035
            End
         End
         Begin VB.Frame Frame3 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   0
            Left            =   60
            TabIndex        =   78
            Top             =   310
            Width           =   7100
            Begin SSDataWidgets_B.SSDBCombo cbossIdentificacion 
               DataField       =   "column 0"
               Height          =   315
               Index           =   0
               Left            =   900
               TabIndex        =   15
               Top             =   180
               Width           =   2415
               DataFieldList   =   "column 1"
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               BevelColorFace  =   12632256
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Designaci�n"
               Columns(1).Name =   "Designaci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4260
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo cbossIdentificacion 
               DataField       =   "column 0"
               Height          =   315
               Index           =   1
               Left            =   4560
               TabIndex        =   16
               Top             =   180
               Width           =   2355
               DataFieldList   =   "column 1"
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               BevelColorFace  =   12632256
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Designaci�n"
               Columns(1).Name =   "Designaci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4154
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo cbossIdentificacion 
               DataField       =   "column 0"
               Height          =   315
               Index           =   2
               Left            =   2400
               TabIndex        =   17
               Top             =   600
               Width           =   3015
               DataFieldList   =   "column 1"
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               BevelColorFace  =   12632256
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Designaci�n"
               Columns(1).Name =   "Designaci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5318
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Morfolog�a:"
               Height          =   255
               Index           =   15
               Left            =   3600
               TabIndex        =   83
               Top             =   240
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Microorganismo:"
               Height          =   255
               Index           =   2
               Left            =   1140
               TabIndex        =   82
               Top             =   600
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "G�nero:"
               Height          =   255
               Index           =   0
               Left            =   180
               TabIndex        =   81
               Top             =   240
               Width           =   675
            End
         End
      End
      Begin TabDlg.SSTab tabResTec 
         Height          =   1335
         Left            =   120
         TabIndex        =   66
         Top             =   300
         Visible         =   0   'False
         Width           =   7185
         _ExtentX        =   12674
         _ExtentY        =   2355
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Observ. Realiz."
         TabPicture(0)   =   "MB00155.frx":0060
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame2(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Observaciones"
         TabPicture(1)   =   "MB00155.frx":007C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame2(1)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   1
            Left            =   -74940
            TabIndex        =   77
            Top             =   310
            Width           =   7095
            Begin VB.TextBox txtObserv 
               DataField       =   "mb20_observ"
               DataSource      =   "msrdcTecnicas"
               Height          =   915
               Index           =   3
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   46
               Top             =   50
               Width           =   7035
            End
         End
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   0
            Left            =   60
            TabIndex        =   76
            Top             =   310
            Width           =   7095
            Begin VB.TextBox txtObserv 
               DataField       =   "mb20_reali"
               DataSource      =   "msrdcTecnicas"
               Height          =   915
               Index           =   2
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   45
               Top             =   50
               Width           =   7035
            End
         End
      End
      Begin TabDlg.SSTab tabResCultivo 
         Height          =   1335
         Left            =   120
         TabIndex        =   65
         Top             =   300
         Visible         =   0   'False
         Width           =   7185
         _ExtentX        =   12674
         _ExtentY        =   2355
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         TabCaption(0)   =   "Datos"
         TabPicture(0)   =   "MB00155.frx":0098
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Observ. Realiz."
         TabPicture(1)   =   "MB00155.frx":00B4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame1(1)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Observaciones"
         TabPicture(2)   =   "MB00155.frx":00D0
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame1(2)"
         Tab(2).ControlCount=   1
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   2
            Left            =   -74940
            TabIndex        =   75
            Top             =   310
            Width           =   7095
            Begin VB.TextBox txtObserv 
               DataField       =   "mb20_observ"
               DataSource      =   "msrdcCultivos"
               Height          =   915
               Index           =   1
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   26
               Top             =   50
               Width           =   7035
            End
         End
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   1
            Left            =   -74940
            TabIndex        =   74
            Top             =   310
            Width           =   7100
            Begin VB.TextBox txtObserv 
               DataField       =   "mb20_reali"
               DataSource      =   "msrdcCultivos"
               Height          =   915
               Index           =   0
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   25
               Top             =   50
               Width           =   7035
            End
         End
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Height          =   975
            Index           =   0
            Left            =   60
            TabIndex        =   70
            Top             =   310
            Width           =   6975
            Begin VB.TextBox txtOrigenPlaca 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Left            =   2460
               Locked          =   -1  'True
               TabIndex        =   71
               TabStop         =   0   'False
               Top             =   120
               Width           =   2895
            End
            Begin SSDataWidgets_B.SSDBCombo cbossCondiciones 
               DataField       =   "column 0"
               Height          =   315
               Left            =   2460
               TabIndex        =   24
               Top             =   540
               Width           =   2895
               DataFieldList   =   "column 1"
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               BevelColorFace  =   12632256
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Designaci�n"
               Columns(1).Name =   "Designaci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Origen placa:"
               Height          =   255
               Index           =   5
               Left            =   1320
               TabIndex        =   73
               Top             =   180
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cond. ambientales:"
               Height          =   255
               Index           =   4
               Left            =   1020
               TabIndex        =   72
               Top             =   600
               Width           =   1575
            End
         End
      End
      Begin VB.CommandButton cmdResultados 
         Caption         =   "Almacenar"
         Height          =   375
         Index           =   3
         Left            =   7500
         TabIndex        =   30
         Top             =   1200
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.Frame fraResultados 
         Caption         =   "Antibiograma"
         Height          =   1335
         Index           =   3
         Left            =   120
         TabIndex        =   60
         Top             =   300
         Visible         =   0   'False
         Width           =   7275
         Begin VB.CheckBox chkConc 
            Caption         =   "Informar concentr."
            Height          =   315
            Left            =   4980
            TabIndex        =   23
            Top             =   840
            Width           =   2175
         End
         Begin VB.CheckBox chkRecomendado 
            Caption         =   "Informar antibi�tico"
            Height          =   315
            Left            =   4980
            TabIndex        =   22
            Top             =   420
            Width           =   2055
         End
         Begin VB.TextBox txtConc 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1620
            MaxLength       =   7
            TabIndex        =   21
            Top             =   780
            Width           =   795
         End
         Begin SSDataWidgets_B.SSDBCombo cbossReac 
            DataField       =   "column 0"
            Height          =   315
            Left            =   1620
            TabIndex        =   20
            Top             =   360
            Width           =   2955
            DataFieldList   =   "column 1"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            BevelColorFace  =   12632256
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Designaci�n"
            Columns(1).Name =   "Designaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5212
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "mg/ml"
            Height          =   255
            Index           =   19
            Left            =   2520
            TabIndex        =   88
            Top             =   840
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Reacci�n:"
            Height          =   255
            Index           =   12
            Left            =   720
            TabIndex        =   62
            Top             =   360
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Concentraci�n:"
            Height          =   255
            Index           =   6
            Left            =   360
            TabIndex        =   61
            Top             =   840
            Width           =   1155
         End
      End
      Begin VB.Frame fraResultados 
         Caption         =   "T�cnicas"
         Height          =   1335
         Index           =   1
         Left            =   120
         TabIndex        =   58
         Top             =   300
         Visible         =   0   'False
         Width           =   7275
         Begin VB.TextBox txtUd 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   1080
            TabIndex        =   68
            TabStop         =   0   'False
            Top             =   960
            Width           =   1395
         End
         Begin VB.TextBox txtResultados 
            Height          =   735
            Left            =   1080
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   27
            Top             =   180
            Width           =   6075
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Unidad:"
            Height          =   255
            Index           =   17
            Left            =   360
            TabIndex        =   69
            Top             =   960
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Resultado:"
            Height          =   255
            Index           =   3
            Left            =   180
            TabIndex        =   59
            Top             =   240
            Width           =   915
         End
      End
      Begin VB.CommandButton cmdResultados 
         Caption         =   "Validar"
         Height          =   375
         Index           =   0
         Left            =   7500
         TabIndex        =   28
         Top             =   360
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.CommandButton cmdResultados 
         Caption         =   "Informar"
         Height          =   375
         Index           =   1
         Left            =   7500
         TabIndex        =   29
         Top             =   780
         Visible         =   0   'False
         Width           =   1200
      End
   End
   Begin VB.Frame fraMuestras 
      Caption         =   "Muestras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   120
      TabIndex        =   56
      Top             =   2460
      Width           =   11715
      Begin Threed.SSCommand cmdTipoVentana 
         Height          =   225
         Left            =   11370
         TabIndex        =   44
         ToolTipText     =   "Restaurar ventana"
         Top             =   180
         Width           =   300
         _Version        =   65536
         _ExtentX        =   529
         _ExtentY        =   397
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         AutoSize        =   2
         Picture         =   "MB00155.frx":00EC
      End
      Begin ComctlLib.TreeView tvwMuestras 
         Height          =   3495
         Left            =   120
         TabIndex        =   37
         Top             =   300
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   6165
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         BorderStyle     =   1
         Appearance      =   1
      End
      Begin TabDlg.SSTab tabMuestras 
         Height          =   3340
         Left            =   6795
         TabIndex        =   38
         Top             =   360
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   5900
         _Version        =   327681
         Style           =   1
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Protocolos"
         TabPicture(0)   =   "MB00155.frx":0108
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tvwProtocolos"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "T�cnicas"
         TabPicture(1)   =   "MB00155.frx":0124
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "tvwTecnicas"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Morfolog�as"
         TabPicture(2)   =   "MB00155.frx":0140
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "tvwMorfologias"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Colonias"
         TabPicture(3)   =   "MB00155.frx":015C
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "tvwColonias"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Antibi�ticos"
         TabPicture(4)   =   "MB00155.frx":0178
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "tvwAntibioticos"
         Tab(4).ControlCount=   1
         Begin ComctlLib.TreeView tvwColonias 
            Height          =   3015
            Left            =   -74940
            TabIndex        =   42
            Top             =   360
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   5318
            _Version        =   327682
            Indentation     =   529
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
         Begin ComctlLib.TreeView tvwMorfologias 
            Height          =   4335
            Left            =   -74940
            TabIndex        =   41
            Top             =   360
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   7646
            _Version        =   327682
            Indentation     =   529
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
         Begin ComctlLib.TreeView tvwProtocolos 
            Height          =   3015
            Left            =   60
            TabIndex        =   39
            Top             =   360
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   5318
            _Version        =   327682
            Indentation     =   529
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
         Begin ComctlLib.TreeView tvwTecnicas 
            Height          =   3015
            Left            =   -74940
            TabIndex        =   40
            Top             =   360
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   5318
            _Version        =   327682
            Indentation     =   529
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
         Begin ComctlLib.TreeView tvwAntibioticos 
            Height          =   4335
            Left            =   -74940
            TabIndex        =   43
            Top             =   360
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   7646
            _Version        =   327682
            Indentation     =   529
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
      End
   End
   Begin VB.Frame fraPruebaAsistencia 
      Caption         =   "Paciente/Prueba"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2025
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   420
      Width           =   11715
      Begin TabDlg.SSTab tabPruebaAsistencia 
         Height          =   1635
         Index           =   0
         Left            =   120
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   300
         Width           =   11460
         _ExtentX        =   20214
         _ExtentY        =   2884
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00155.frx":0194
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(13)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(10)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(9)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(7)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(14)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(16)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(18)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtPruebaAsistencia(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtPruebaAsistencia(4)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtPruebaAsistencia(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtPruebaAsistencia(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtPruebaAsistencia(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtPruebaAsistencia(6)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "chkPruebaAsistencia(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtPruebaAsistencia(7)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtPruebaAsistencia(0)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtPruebaAsistencia(8)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtPruebaAsistencia(9)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtPruebaAsistencia(10)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtPruebaAsistencia(11)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtPruebaAsistencia(12)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtPruebaAsistencia(13)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtPruebaAsistencia(14)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtPruebaAsistencia(15)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtPruebaAsistencia(16)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).ControlCount=   28
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPruebaAsistencia(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPetic"
            Height          =   285
            Index           =   16
            Left            =   2280
            TabIndex        =   92
            Top             =   1200
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPrueba"
            Height          =   285
            Index           =   15
            Left            =   2520
            TabIndex        =   91
            Top             =   1200
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPac"
            Height          =   285
            Index           =   14
            Left            =   2760
            TabIndex        =   90
            Top             =   1200
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "cTipoPrueba"
            Height          =   285
            Index           =   13
            Left            =   8460
            TabIndex        =   89
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "nRepeticion"
            Height          =   285
            Index           =   12
            Left            =   1800
            TabIndex        =   86
            TabStop         =   0   'False
            Top             =   1320
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "secuencia"
            Height          =   285
            Index           =   11
            Left            =   1800
            TabIndex        =   85
            TabStop         =   0   'False
            Top             =   1020
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "fnac"
            Height          =   285
            Index           =   10
            Left            =   6780
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Fecha Nacimiento|Fecha de nacimiento del paciente"
            Top             =   120
            Width           =   1035
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "caso"
            Height          =   285
            Index           =   9
            Left            =   840
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Caso|N� de caso"
            Top             =   840
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "historia"
            Height          =   285
            Index           =   8
            Left            =   840
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Historia|N� de historia del paciente"
            Top             =   480
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "NREF"
            Height          =   285
            Index           =   0
            Left            =   840
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "N� Ref.|N� de Referencia"
            Top             =   60
            Visible         =   0   'False
            Width           =   1395
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   7
            Left            =   840
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "N� Ref.|N� de Referencia"
            Top             =   120
            Width           =   915
         End
         Begin VB.CheckBox chkPruebaAsistencia 
            Alignment       =   1  'Right Justify
            Caption         =   "Urgente:"
            DataField       =   "urgenciaRealizacion"
            Height          =   255
            Index           =   0
            Left            =   9960
            TabIndex        =   10
            Tag             =   "Urgente|Prueba Urgente"
            Top             =   480
            Width           =   1005
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   705
            Index           =   6
            Left            =   3000
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Indicaciones|Indicaciones sobre la Prueba"
            Top             =   780
            Width           =   7995
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "paciente"
            Height          =   285
            Index           =   1
            Left            =   3000
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Paciente|Nombre y apellidos del paciente"
            Top             =   120
            Width           =   3015
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "cama"
            Height          =   285
            Index           =   2
            Left            =   840
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Cama|Cama"
            Top             =   1200
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "dptSolicitante"
            Height          =   285
            Index           =   3
            Left            =   3000
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Dpto. Solicitud|Dpto. que solicita la Prueba"
            Top             =   440
            Width           =   2775
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "drSolicitante"
            Height          =   285
            Index           =   4
            Left            =   6780
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Dr. Solicitud|Dr. que solicita la Prueba"
            Top             =   440
            Width           =   3075
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "desigPrueba"
            Height          =   285
            Index           =   5
            Left            =   8520
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Prueba solicitada|Prueba solicitada"
            Top             =   120
            Width           =   2415
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPruebaAsistencia 
            Height          =   1425
            Index           =   0
            Left            =   -74910
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   90
            Width           =   10935
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19288
            _ExtentY        =   2514
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "F. Nac:"
            Height          =   255
            Index           =   18
            Left            =   6180
            TabIndex        =   84
            Top             =   120
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Caso:"
            Height          =   255
            Index           =   16
            Left            =   300
            TabIndex        =   64
            Top             =   900
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia:"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   63
            Top             =   540
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Indicaciones:"
            Height          =   195
            Index           =   1
            Left            =   1920
            TabIndex        =   55
            Top             =   780
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Ref:"
            Height          =   255
            Index           =   7
            Left            =   180
            TabIndex        =   54
            Top             =   180
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Paciente:"
            Height          =   255
            Index           =   8
            Left            =   2220
            TabIndex        =   53
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cama:"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   52
            Top             =   1260
            Width           =   585
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dpto. Solic.:"
            Height          =   255
            Index           =   10
            Left            =   1980
            TabIndex        =   51
            Top             =   480
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr. Solic.:"
            Height          =   255
            Index           =   11
            Left            =   6000
            TabIndex        =   50
            Top             =   480
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Prueba:"
            Height          =   255
            Index           =   13
            Left            =   7860
            TabIndex        =   49
            Top             =   120
            Width           =   645
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   47
      Top             =   8115
      Width           =   11970
      _ExtentX        =   21114
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin MSRDC.MSRDC msrdcCultivos 
      Height          =   330
      Left            =   8400
      Top             =   0
      Visible         =   0   'False
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   1
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSRDC.MSRDC msrdcTecnicas 
      Height          =   330
      Left            =   9720
      Top             =   0
      Visible         =   0   'False
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   1
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSRDC.MSRDC msrdcColonias 
      Height          =   330
      Left            =   8940
      Top             =   0
      Visible         =   0   'False
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   1
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSRDC.MSRDC msrdcPlacas 
      Height          =   375
      Left            =   0
      Top             =   0
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   661
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   1
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmP_Peticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objPruebaAsistencia As New clsCWForm

Dim cCarpeta As String, desigCarpeta As String 'carpeta seleccionada
  
Dim whereInicial As String 'guarda el Where con las pruebas para leer hoy
Dim nReferencia As String 'guarda el n� de refencia de la prueba
Dim nodoResultados As Node 'nodo seleccionado para insertar resultados
Dim cUserPeticion As Integer 'usuario que valida o informa en la pantalla de Petici�n

'variables usadas para almacenar datos usados en la insercci�n de resultados
Dim IdentifMicro(0 To 2) As Integer 'cod. g�nero, cod. morfolog�a y cod. microorganismo
Dim CondAmbiental  As Integer   'cod. condici�n ambiental de la t�cnica/cultivo
Dim codPaso  As Integer 'n� de paso del cultivo por la estufa
Dim codReac As Integer 'cod. de la reacci�n/sensibilidad del microorganismo frente al antibi�tico

Dim admitido As Boolean 'para ver si un nodo se puede arrastrar y soltar

Dim accesoMantenimiento As Integer 'pantalla de mantenimiento a la que se accede
Private Function fArrastreValido(Source As Control) As Boolean
'chequea cuando un nodo arrastrado se puede soltar sobre el TreeView de las Muestras

    Dim nodo As Node
    Dim origen As String
    
    On Error Resume Next
    Set nodo = tvwMuestras.DropHighlight
    If Err <> 0 Then 'no se est� sobre ning�n nodo
        fArrastreValido = False
        Exit Function
    End If
    'On Error GoTo 0
    
'    si se arrastra una t�cnica de un protocolo, se se�ala el tvwTecnicas como el fuente _
'    para aprovechar los chequeos
    origen = Source.Name
    If Source.Name = tvwProtocolos.Name Then
        If Left$(nodo.Key, 1) = "C" Then
            origen = tvwTecnicas.Name
        End If
    End If
    
    Select Case origen
        Case tvwMorfologias.Name
            Select Case Left$(nodo.Key, 1)
                Case "X", "F", "M"
                    fArrastreValido = True
                Case "R"
                    If Left$(nodo.Parent.Parent.Key, 1) = "K" Then
                        fArrastreValido = True
                    End If
            End Select
            
        Case tvwAntibioticos.Name
            If Left$(nodo.Key, 1) = "T" Then
                If Val(Mid$(nodo.Key, InStr(nodo.Key, "I") + 1)) = constANTIBIOGRAMA Then
                    If Val(nodo.Tag) <> constTECVALIDADA And Val(nodo.Tag) <> constTECANULADA Then
                        fArrastreValido = True
                    End If
                End If
            End If
            
        Case tvwColonias.Name
            Select Case Left$(nodo.Key, 1)
                Case "C"
                    If Val(nodo.Tag) <> constTECANULADA And Val(nodo.Tag) <> constTECFINALIZADA Then
                        If Val(Mid$(nodo.Key, InStr(nodo.Key, "S") + 1)) = constRECIPPLACA Then
                            fArrastreValido = True
                        End If
                    End If
                Case "K"
                    If Val(nodo.Tag) <> constCOLANULADA And Val(Mid$(tvwColonias.SelectedItem.Key, 2)) > 0 Then
                        fArrastreValido = True
                    End If
            End Select
            
        Case tvwTecnicas.Name
            Select Case Left$(nodo.Key, 1)
                Case "M"
                    If fPosibleAplicarTecnica(constORIGENMUESTRA, CStr(Val(Mid$(Source.SelectedItem.Key, InStr(Source.SelectedItem.Key, "I") + 1)))) Then
                        fArrastreValido = True
                    End If
                Case "K"
                    If Val(nodo.Tag) <> constCOLPLACAANULADA Then
                        If fPosibleAplicarTecnica(constORIGENCOLONIA, CStr(Val(Mid$(Source.SelectedItem.Key, InStr(Source.SelectedItem.Key, "I") + 1)))) Then
                            fArrastreValido = True
                        End If
                    End If
                Case "C"
                    If Val(nodo.Tag) <> constTECANULADA And Val(nodo.Tag) <> constTECFINALIZADA Then
                        If fPosibleAplicarTecnica(constORIGENPLACA, CStr(Val(Mid$(Source.SelectedItem.Key, InStr(Source.SelectedItem.Key, "I") + 1)))) Then
                            fArrastreValido = True
                        End If
                    End If
            End Select
                        
        Case tvwProtocolos.Name
            Select Case Left$(nodo.Key, 1)
                Case "M"
                    fArrastreValido = True
                Case "K"
                    If Val(nodo.Tag) <> constCOLPLACAANULADA Then
                        fArrastreValido = True
                    End If
                Case "C"
                    If Val(nodo.Tag) <> constTECANULADA And Val(nodo.Tag) <> constTECFINALIZADA Then
                        fArrastreValido = True
                    End If
            End Select
    End Select
    
End Function

Private Function fBuscarCondAmbientales(codTec%) As Integer
'se buscan las condiciones ambientales en las que se debe colocar una t�cnica o cultivo _
    la primera vez que se realiza si no se ha pedido desde un protocolo

'primeramente se mira en la tabla MB1300 (tiempo-t�cnica) donde debe estar definido el _
    tiempo de incubaci�n en las condiciones ambientales m�s frecuentes. Si no se encuentra, _
    se elige la condici�n ambiental que primero se haya definido en el maestro (MB0200).
    
    Dim SQL As String
    Dim rsCondAmb As rdoResultset, qryCondAmb As rdoQuery
    Dim rsCondiciones As rdoResultset
    
    fBuscarCondAmbientales = 0
    
    SQL = "SELECT MB02_codCond FROM MB1300 WHERE MB09_codTec = ?"
    Set qryCondAmb = objApp.rdoConnect.CreateQuery("", SQL)
    qryCondAmb(0) = codTec
    Set rsCondAmb = qryCondAmb.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsCondAmb.EOF = False Then
        fBuscarCondAmbientales = rsCondAmb(0)
    Else
        SQL = "SELECT MIN(MB02_codCond) FROM MB0200"
        Set rsCondiciones = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
        If rsCondiciones.EOF = False Then
            fBuscarCondAmbientales = rsCondiciones(0)
        End If
        rsCondiciones.Close
    End If
    rsCondAmb.Close
    qryCondAmb.Close
    
End Function

Private Function fBuscarNumColonia()
'busca el n� de colonia siguiente para una referencia
    
    Dim SQL As String
    Dim rsCol As rdoResultset, qryCol As rdoQuery
    
    On Error Resume Next
    SQL = "SELECT MAX(MB27_CODCOL) FROM MB2700 WHERE NREF = ?"
    Set qryCol = objApp.rdoConnect.CreateQuery("SelNumCol", SQL)
    qryCol(0) = nReferencia
    Set rsCol = qryCol.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err = 0 Then
        If Not IsNull(rsCol(0)) Then
            fBuscarNumColonia = rsCol(0) + 1
        Else
            fBuscarNumColonia = 1
        End If
    Else
        fBuscarNumColonia = 0
    End If
    rsCol.Close
    qryCol.Close
    
End Function

Private Function fComprobarAntibiograma(CodCol%, cMicro%) As Boolean
'Comprueba la validez del antibiograma establecido para un microorganismo.

'Para que el antibiograma sea v�lido debe cumplir las restricciones de bacteria resistente _
    (sensibilidad del microorganismo frente a cada antibi�tico) y de relaci�n entre _
    antibi�ticos se�alada en los maestros MB2300 y MB2400. La funci�n tambi�n detecta _
    si existen incompatibilidades en los valores establecidos en estos maestros

'Si se detectan desviaciones, se pregunta al usuario sobre la acci�n a tomar _
    que puede ser: 1- cancelar (el microorganismo queda sin identificar), 2- validar el _
    microorganismo y corregir los resultados del antibiograma seg�n los est�ndares, _
    3- validar el microorganismo y dejar los resultados del antibiograma tal y como estaban

    Dim SQL As String
    Dim rsChequeo As rdoResultset, qryChequeo As rdoQuery
    Dim msg1$, msg2$, msg$
    Dim respuesta As Integer
    Dim reacStandar$, arReacStandar() As typeReaccionAntibiotico
    Dim dimension%, i%
    Dim res As Boolean
    Dim reacIncompat%, antibIncompat As String
    Dim resRecursiva As Boolean
    
    'se buscan los resultados introducidos en las distintas t�cnicas de antibiograma _
    realizadas sobre la colonia tratada
    SQL = "SELECT DISTINCT MB2900.MB07_codAnti, MB40_codReac, MB29_conc, MB29_recom,"
    SQL = SQL & " MB20_codTecAsist, MB07_desig, MB29_indInf_Conc"
    SQL = SQL & " FROM MB2900, MB0700"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB40_codReac IS NOT NULL"
    SQL = SQL & " AND MB0700.MB07_codAnti = MB2900.MB07_codAnti"
    Set qryChequeo = objApp.rdoConnect.CreateQuery("", SQL)

    qryChequeo(0) = nReferencia
    qryChequeo(1) = CodCol
    Set rsChequeo = qryChequeo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)

    'se comprueba la resistencia de los microorganismos frente a los antibi�ticos y la relaci�n _
    entre los antibi�ticos. Las funciones que realizan la comprobaci�n devuelven un string _
    con el mensaje de desviaci�n correspondiente (si hubiera desviaciones) y adem�s actualizan _
    los valores de las variables reacStandar y reacIncompat
    dimension = 0
    Do While rsChequeo.EOF = False
        reacStandar = ""
        reacIncompat = 0
        msg1 = msg1 & fComprobarBacteriaResistente(cMicro, rsChequeo(0), rsChequeo(1), reacStandar, reacIncompat)
        msg2 = msg2 & fComprobarRelacionAntibioticos(cMicro, CodCol, rsChequeo(0), rsChequeo(1), reacStandar, reacIncompat)
        'si la variable reacStandar no est� vac�a, contendr� el c�digo y el nombre de la _
        sensibilidad estandar esperada para el antibi�tico, que no corresponde con la actual
        If reacStandar <> "" Then
            'se anotan en la variable antibIncompat los antibi�ticos que presentan incompatibilidades _
            de resultados en los maestros. Las funciones anteriores devuelven un -1 en la _
            variable reacIncompat si el antibi�tico presenta incompatibilidad en los _
            resultados definidos en los maestros
            If reacIncompat = -1 Then
                antibIncompat = antibIncompat & " '" & rsChequeo(5) & "',"
            End If
            
            'Se anota en la matriz arReacStandar los resultados que se esperaban para _
            los antibi�ticos por si se quieren cambiar
            dimension = dimension + 1
            ReDim Preserve arReacStandar(1 To dimension)
            arReacStandar(dimension).cAnti = rsChequeo(0)
            arReacStandar(dimension).cReac = Val(reacStandar) 'c�digo reacci�n standar
            arReacStandar(dimension).desigReac = Mid$(reacStandar, InStr(reacStandar, "-") + 1) 'nombre reacci�n standar
            If Not IsNull(rsChequeo(2)) Then
                arReacStandar(dimension).conc = rsChequeo(2)
            End If
            arReacStandar(dimension).recom = rsChequeo(3)
            arReacStandar(dimension).infConc = rsChequeo(6)
            arReacStandar(dimension).cTecAsist = rsChequeo(4)
        End If
        rsChequeo.MoveNext
    Loop
    'si se ha detectado alg�n tipo de desviaci�n o de incompatibilidad, se muestra un aviso _
    para que el usuario tome una decisi�n al respecto
    If msg1 <> "" Then
        msg = "La sensibilidad definida para los microorganismo frente a antibi�ticos detecta desviaciones:" & Chr$(13) & Chr$(10) & msg1
        msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
    End If
    If msg2 <> "" Then
        msg = msg & "La relaci�n definida entre antibi�ticos detecta desviaciones:" & Chr$(13) & Chr$(10) & msg2
        msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
    End If
    If antibIncompat <> "" Then
        msg = msg & "AVISO: Existen incompatibilidades en las sensibilidades esperadas para"
        msg = msg & Left$(antibIncompat, Len(antibIncompat) - 1) & "." & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
    End If
    If msg <> "" Then
        msg = msg & "�Desea Ud. corregir estas desviaciones?"
        respuesta = MsgBox(msg, vbInformation + vbYesNoCancel, "Comprobaci�n antibiogramas")
        Select Case respuesta
            Case vbCancel 'cancelar todo: el microorganismo no quedar� identificado
                'La funci�n devuelve FALSE, es decir, no se debe validar el microorganismo
                Exit Function
            Case vbYes 'modificar los resultados del antibiograma de acuerdo con los est�ndares
                For i = 1 To UBound(arReacStandar)
                    'parametros: cMicro,cReac,cCol,cTecAsist,cAnti,concentracion,recomendado
                    If fUpdateResultAntibioticos(cMicro, arReacStandar(i).cReac, CodCol, arReacStandar(i).cTecAsist, arReacStandar(i).cAnti, arReacStandar(i).conc, arReacStandar(i).recom, arReacStandar(i).infConc) = False Then
                        MsgBox "Se ha producido un error en la actualizaci�n de los datos del antibiograma.", vbError
                        objApp.rdoConnect.RollbackTrans
                    Else
                        res = True
                        'parametros: cMicro,res,nodo del antibi�tico,designacion reaccion,concentracion,recomendado
                        'La variable 'res' se emplea ver si la acci�n sobre la base de datos ha sido correcta
                        'El 'nodo del antibi�tico' puede ser cualquiera de los posibles nodos _
                        del antibi�tico sobre la colonia
                        Call pTextoDatosAntibioticos(cMicro, res, tvwMuestras.Nodes("A" & arReacStandar(i).cAnti & "C" & arReacStandar(i).cTecAsist), arReacStandar(i).desigReac, arReacStandar(i).conc, arReacStandar(i).recom)
                        If res = False Then
                            MsgBox "Se ha producido un error en la actualizaci�n de los datos del antibiograma.", vbError
                            objApp.rdoConnect.RollbackTrans
                        Else
                            objApp.rdoConnect.CommitTrans
                        End If
                    End If
                Next i
                'se hace una llamada recursiva para ver si sigue habiendo incompatibilidades
                resRecursiva = fComprobarAntibiograma(CodCol, cMicro)
                If resRecursiva = False Then
                    Exit Function
                End If
            Case vbNo 'mantener los resultados de los antibiogramas
                'la funci�n devuelve TRUE, es decir, se puede validar el microorganismo
        End Select
    End If
    rsChequeo.Close
    qryChequeo.Close
    
    fComprobarAntibiograma = True
    
End Function
Private Function fComprobarBacteriaResistente(codMicro%, codAntibiotico%, codReaccion%, Optional reacStandar$, Optional reacIncompat%) As String
'Compara la sensibilidad de un microorganismo frente a un antibi�tico con la definida _
    en el maestro MB2400 (bacteria resistente) y devuelve un mensaje si ha detectado _
    desviaciones.

'Adem�s, tambi�n actualiza las variables reacStandar y reacIncompat seg�n lo comentado _
    posteriormente
    
    Dim SQL As String
    Dim rsBactResist As rdoResultset, qryBactResist As rdoQuery
    
    'se buscan la sensibilidad est�ndar definida en MB2400 para el microorganismo _
    (plenamente identificado) respecto al antibi�tico
    SQL = "SELECT MB2400.MB40_codReac,MB40_desig,MB07_desig"
    SQL = SQL & " FROM MB2400,MB4000,MB0700"
    SQL = SQL & " WHERE MB18_codMicro = ?"
    SQL = SQL & " AND MB24_indActiva = -1"
    SQL = SQL & " AND MB2400.MB07_codAnti = ?"
    SQL = SQL & " AND MB4000.MB40_codReac = MB2400.MB40_codReac"
    SQL = SQL & " AND MB0700.MB07_codAnti = MB2400.MB07_codAnti"
    Set qryBactResist = objApp.rdoConnect.CreateQuery("BactResist", SQL)
        
    qryBactResist(0) = codMicro
    qryBactResist(1) = codAntibiotico
    Set rsBactResist = qryBactResist.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
    If rsBactResist.EOF = False Then
        'NOTA: Las variables opcionales solo afectan cuando la llamada se ha hecho desde la funci�n _
        fComprobarAntibiograma. En la variable reacStandar quedar� anotado el c�digo y designaci�n _
        de la reacci�n esperada para el antibi�tico (por si luego se quiere cambiar). En _
        la reacIncompat quedar� el c�digo de la reacci�n esperada y si ha habido incompatibilidad _
        en los registros de la tablas MB2300 y/o MB2400 se anotar� un -1.
        On Error Resume Next
        If rsBactResist(0) <> codReaccion Then
            If reacStandar = "" Then
                reacStandar = rsBactResist(0) & "-" & rsBactResist(1)
            End If
            fComprobarBacteriaResistente = Chr$(13) & Chr$(10) & "  - Con el antibi�tico '" & rsBactResist(2) & "' la sensibilidad esperada es '" & rsBactResist(1) & "'."
        End If
        If reacIncompat <> 0 Then 'ya exist�a un valor de reacci�n anterior
            If reacIncompat <> rsBactResist(0) Then 'el nuevo valor es distinto
                reacIncompat = -1 'incompatibilidad
            End If
        Else
            'asigna el valor del c�digo de reacci�n est�ndar a la variable reacIncompat. _
            Posteriormente, al realizar la otra comprobaci�n (fComprobarRelacionAntibioticos) _
            si el nuevo valor est�ndar encontrado no corresponde con el de reacIncompat _
            se habr� detectado una incompatibilidad en los valores de los maestros
            reacIncompat = rsBactResist(0)
        End If
    End If
    
End Function

Private Function fComprobarResultadoMicro(cCol%, cMicro%, cGeMicro%, cMorf%) As Boolean
'Comprueba que los resultados de las t�cnicas realizadas sobre el microorganismo _
    est�n de acuerdo con los establecidos en el maestro de los resultados (MB2500). Tiene en _
    cuenta que el microorganismo puede estar identificado tambi�n a nivel de g�nero o de _
    morfolog�a

'En el caso de detectar desviaciones, muestra un aviso para que el usuario tome una decisi�n _
    al respecto

'Los valores que devuelve la funci�n son: _
    True    si no hay desviaciones o el usuario no las quiere corregir _
    False   si hay desviaciones y el usuario las quiere corregir
    
    Dim SQL As String
    Dim rsCompResMicro As rdoResultset, qryCompResMicro As rdoQuery
    
    fComprobarResultadoMicro = False
    
    SQL = "SELECT MB2500.MB09_codTec, MB2500.MB32_codResult, MB2500.MB25_result, MB2500.cUnidad"
    SQL = SQL & ", MB3300.MB33_result, MB3300.cUnidad"
    SQL = SQL & ", MB0900.MB09_desig, MB3200.MB32_desig"
    SQL = SQL & " FROM MB2500, MB3300, MB2000, MB0900, MB3200"
    SQL = SQL & " WHERE MB25_indActiva = -1"
    If cMicro <> -1 Then
        SQL = SQL & " AND MB2500.MB18_codMicro = ?"
    ElseIf cGeMicro <> -1 Then
        SQL = SQL & " AND MB2500.MB11_codGeMicro = ?"
    ElseIf cMorf <> -1 Then
        SQL = SQL & " AND MB2500.MB12_codMorf = ?"
    End If
    SQL = SQL & " AND MB3300.MB38_codEstResult <> ?"
    SQL = SQL & " AND MB3300.MB09_codTec = MB2500.MB09_codTec"
    SQL = SQL & " AND MB3300.MB32_codResult = MB2500.MB32_codResult"
    SQL = SQL & " AND MB3300.MB33_Result <> MB2500.MB25_Result"
    SQL = SQL & " AND MB2000.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB2000.MB20_codTecAsist = MB3300.MB20_codTecAsist"
    SQL = SQL & " AND MB2000.nRef = MB3300.nRef"
    SQL = SQL & " AND MB2000.nRef = ?"
    SQL = SQL & " AND MB2000.MB20_tiOrig = ?"
    SQL = SQL & " AND MB2000.MB20_codOrig = ?"
    SQL = SQL & " AND MB2000.MB34_codEstTecAsist <> ?"
    SQL = SQL & " AND MB0900.MB09_codTec = MB2500.MB09_codTec"
    SQL = SQL & " AND MB3200.MB09_codTec = MB2500.MB09_codTec"
    SQL = SQL & " AND MB3200.MB32_codResult = MB2500.MB32_codResult"
    Set qryCompResMicro = objApp.rdoConnect.CreateQuery("CompResMicro", SQL)
        
    If cMicro <> -1 Then
        qryCompResMicro(0) = cMicro
    ElseIf cGeMicro <> -1 Then
        qryCompResMicro(0) = cGeMicro
    ElseIf cMorf <> -1 Then
        qryCompResMicro(0) = cMorf
    End If
    qryCompResMicro(1) = constRESULTANULADO
    qryCompResMicro(2) = nReferencia
    qryCompResMicro(3) = constORIGENCOLONIA
    qryCompResMicro(4) = cCol
    qryCompResMicro(5) = constTECANULADA
    Set rsCompResMicro = qryCompResMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
   
    If rsCompResMicro.EOF = False Then
        SQL = "Se han detectado desviaciones en los resultados esperados y obtenidos al aplicar"
        SQL = SQL & " t�cnicas sobre el microorganismo:"
        Do While rsCompResMicro.EOF = False
            SQL = SQL & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
            SQL = SQL & " * T�cnica: " & rsCompResMicro(6) & Space(10) & "Resultado: " & rsCompResMicro(7)
            SQL = SQL & Chr$(13) & Chr$(10)
            SQL = SQL & Space(10) & "Esperado: " & rsCompResMicro(2) & Space(10) & "Obtenido: " & rsCompResMicro(4)
            rsCompResMicro.MoveNext
        Loop
        If MsgBox(SQL, vbInformation + vbOKCancel, "Comprobaci�n de resultados") = vbOK Then
            fComprobarResultadoMicro = True
        End If
    Else
        fComprobarResultadoMicro = True
    End If
    
    rsCompResMicro.Close
    qryCompResMicro.Close
    
End Function

Private Function fComprobarResultadoTec(cCol%, cTec%, cResult%, result$) As Boolean
'Comprueba que el resultado de la t�cnica realizada sobre el microorganismo _
    est� de acuerdo con lo establecido en el maestro de los resultados (MB2500). Esta _
    comprobaci�n se realiza s�lo si la colonia est� identificada

'En el caso de detectar desviaciones, muestra un aviso para que el usuario tome una decisi�n _
    al respecto

'Los valores que devuelve la funci�n son: _
    True    si la colonia no est� identificada, si no hay desviaciones o si el usuario no las quiere corregir _
    False   si hay desviaciones y el usuario las quiere corregir
    
    Dim SQL As String
    Dim rsCompResTec As rdoResultset, qryCompResTec As rdoQuery
    Dim cMicro As Integer, cGeMicro As Integer, cMorf As Integer
    
    fComprobarResultadoTec = False
    
    ' se mira si la colonia est� identificada
    Call pBuscarIdentifCol(cCol, cMicro, cGeMicro, cMorf)
    If cMicro = -1 And cGeMicro = -1 And cMorf = -1 Then
        fComprobarResultadoTec = True
        Exit Function
    End If
    
    'se compara el resultado obtenido con el esperado
    SQL = "SELECT MB2500.MB25_result, MB2500.cUnidad"
    SQL = SQL & ", MB0900.MB09_desig, MB3200.MB32_desig"
    SQL = SQL & " FROM MB2500, MB0900, MB3200"
    SQL = SQL & " WHERE MB25_indActiva = -1"
    If cMicro <> -1 Then
        SQL = SQL & " AND MB2500.MB18_codMicro = ?"
    ElseIf cGeMicro <> -1 Then
        SQL = SQL & " AND MB2500.MB11_codGeMicro = ?"
    ElseIf cMorf <> -1 Then
        SQL = SQL & " AND MB2500.MB12_codMorf = ?"
    End If
    SQL = SQL & " AND MB2500.MB09_codTec = ?"
    SQL = SQL & " AND MB2500.MB32_codResult = ?"
    SQL = SQL & " AND MB2500.MB25_Result <> ?"
    SQL = SQL & " AND MB0900.MB09_codTec = MB2500.MB09_codTec"
    SQL = SQL & " AND MB3200.MB09_codTec = MB2500.MB09_codTec"
    SQL = SQL & " AND MB3200.MB32_codResult = MB2500.MB32_codResult"
    Set qryCompResTec = objApp.rdoConnect.CreateQuery("CompResTec", SQL)
        
    If cMicro <> -1 Then
        qryCompResTec(0) = cMicro
    ElseIf cGeMicro <> -1 Then
        qryCompResTec(0) = cGeMicro
    ElseIf cMorf <> -1 Then
        qryCompResTec(0) = cMorf
    End If
    qryCompResTec(1) = cTec
    qryCompResTec(2) = cResult
    qryCompResTec(3) = result
    Set rsCompResTec = qryCompResTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsCompResTec.EOF = False Then
        SQL = "Se han detectado desviaciones en los resultados esperados y obtenidos al aplicar"
        SQL = SQL & " t�cnicas sobre el microorganismo:"
        SQL = SQL & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
        SQL = SQL & " * T�cnica: " & rsCompResTec(2) & Space(10) & "Resultado: " & rsCompResTec(3)
        SQL = SQL & Chr$(13) & Chr$(10)
        SQL = SQL & Space(10) & "Esperado: " & rsCompResTec(0) & Space(10) & "Obtenido: " & result
        If MsgBox(SQL, vbInformation + vbOKCancel, "Comprobaci�n de resultados") = vbOK Then
            fComprobarResultadoTec = True
        End If
    Else
        fComprobarResultadoTec = True
    End If
        
    rsCompResTec.Close
    qryCompResTec.Close
    
End Function

Private Function fComprobarMicroMorfo(cMuestra$) As String
'Realiza la comprobaci�n de correspondencia entre los microorganismos identificados en las _
    colonias y los encontrados por observaci�n directa (morfolog�as). La funci�n devuelve un _
    mensaje con las desviaciones encontradas

    Dim SQL As String, i As Integer, j As Integer
    Dim rsCorresp As rdoResultset, qryCorresp As rdoQuery
    Dim dimension As Integer, dimension1 As Integer
    Dim arMorf() As typeMorfologias
    Dim arCol() As typeColonias
    Dim segundaPasada As Boolean 'indica si habr� que hacer la segunda pasada
    Dim msg As String
        
    'se buscan las morfolog�as identificadas en las colonias. Solo las colonias identificadas _
    a nivel de morfolog�a o de microorganismo. No hay forma de asociar con morfolog�as las _
    identificadas a nivel de g�nero)
    SQL = "SELECT MB27_codCol, MB2700.MB12_codMorf, MB1800.MB12_codMorf"
    SQL = SQL & " FROM MB2700,MB1200,MB1800 "
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB36_codEstCol <> ?"
    SQL = SQL & " AND MB1200.MB12_codMorf (+)= MB2700.MB12_codMorf"
    SQL = SQL & " AND MB1800.MB18_codMicro (+)= MB2700.MB18_codMicro"
    Set qryCorresp = objApp.rdoConnect.CreateQuery("SelMorfCol", SQL)

    qryCorresp(0) = nReferencia
    qryCorresp(1) = constCOLANULADA
    Set rsCorresp = qryCorresp.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    dimension1 = 0
    Do While rsCorresp.EOF = False
        'se guarda en la matriz arCol todos los n� de colonia y los c�digos de la _
        morfolog�a correspondiente
        dimension1 = dimension1 + 1
        ReDim Preserve arCol(1 To dimension1)
        arCol(dimension1).cCol = rsCorresp(0)
        If Not IsNull(rsCorresp(1)) Then
            arCol(dimension1).cMorf = rsCorresp(1)
        ElseIf Not IsNull(rsCorresp(2)) Then
            arCol(dimension1).cMorf = rsCorresp(2)
        End If
        rsCorresp.MoveNext
    Loop
    
    'se buscan las morfolog�as encontradas por observaci�n directa ordenadas _
    por n� decreciente de resultados definidos en el maestro (MB2500)
    SQL = "SELECT MB2100.MB12_codMorf, MB12_desig, MB21_numLoc, count(MB2500.MB12_codMorf) numRes"
    SQL = SQL & " FROM MB2100,MB1200,MB2500"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND cMuestra = ?"
    SQL = SQL & " AND MB1200.MB12_codMorf = MB2100.MB12_codMorf"
    SQL = SQL & " AND MB2500.MB12_codMorf (+)= MB2100.MB12_codMorf"
    SQL = SQL & " AND MB25_indActiva (+)= -1"
    SQL = SQL & " GROUP BY MB2100.MB12_codMorf, MB12_desig, MB21_numLoc"
    SQL = SQL & " ORDER BY numRes DESC"
    Set qryCorresp = objApp.rdoConnect.CreateQuery("SelMorfologias", SQL)
        
    qryCorresp(0) = nReferencia
    qryCorresp(1) = cMuestra
    Set rsCorresp = qryCorresp.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    dimension = 0
    'En la matriz arMorf se van anotando las morfolog�as encontradas en observ. directa (OD) _
    (OD) y el n� de localizaciones de cada una de ellas. Por cada morfolog�a distinta, se busca _
    en la matriz arCol (morfolog�as identificadas) para ver correspondencias. Si alguna de _
    las morfolog�as de arCol corresponde con la de arMorf, la morfolog�a se anota como _
    asignada en arCol (campo asig = true) y en arMorf (campo numCol = numCol +1).
    'Al final, se pretende que en la matriz arMorf los campos numLoc y numCol tengan el _
    mismo valor para cada una de las morfolog�as, lo que significa que al menos, se han _
    identificado todas las morfolog�as encontradas en la OD. Si para alguna morfolog�a _
    el campo numCol es menor que numLoc, significa que todav�a quedan morfolog�as _
    encontradas en OD que no han sido identificadas en ninguna colonia. Habr� que realizar _
    una segunda pasada teniendo en cuenta los resultados de t�cnicas para ver si encontramos _
    alguna correspondencia m�s
    Do While rsCorresp.EOF = False
        dimension = dimension + 1
        ReDim Preserve arMorf(1 To dimension)
        arMorf(dimension).cMorf = rsCorresp(0)
        arMorf(dimension).desig = rsCorresp(1)
        arMorf(dimension).numLoc = rsCorresp(2)
        
        'se busca si la morfolog�a se corresponde con alguna de las colonias
        If dimension1 > 0 Then
            For i = 1 To dimension1
                If arCol(i).cMorf = arMorf(dimension).cMorf Then
                    arMorf(dimension).numCol = arMorf(dimension).numCol + 1
                    arCol(i).asig = True
                    'si todas las localizaciones de la morf. han sido asignadas no se sigue buscando
                    If arMorf(dimension).numCol = arMorf(dimension).numLoc Then
                        Exit For
                    End If
                End If
            Next i
            If arMorf(dimension).numCol < arMorf(dimension).numLoc Then
                segundaPasada = True
            End If
        End If
        rsCorresp.MoveNext
    Loop
    
    rsCorresp.Close
    qryCorresp.Close
    
    'se realiza una segunda pasada teniendo en cuenta los resultados de las t�cnicas:
    If segundaPasada = True Then
        'se crea un statement que compara los resultados de t�cnicas definidos para las _
        morfolog�as en el maestro (MB2500) con los resultados de t�cnicas realizadas sobre _
        las colonias. Devolver� alg�n registro si existe al menos un resultado distinto.
        SQL = "SELECT MB2500.MB09_codTec, MB2500.MB32_codResult, MB25_result, MB33_result"
        SQL = SQL & " FROM MB2500, MB3300, MB2000"
        SQL = SQL & " WHERE MB12_codMorf = ?"
        SQL = SQL & " AND MB25_indActiva = -1"
        SQL = SQL & " AND MB3300.nRef = ?"
        SQL = SQL & " AND MB3300.MB09_codTec = MB2500.MB09_codTec"
        SQL = SQL & " AND MB3300.MB32_codResult = MB2500.MB32_codResult"
        SQL = SQL & " AND MB33_result <> MB25_result"
        SQL = SQL & " AND MB38_codEstResult <> ?"
        SQL = SQL & " AND MB2000.MB20_codTecAsist = MB3300.MB20_codTecAsist"
        SQL = SQL & " AND MB2000.MB09_codTec = MB3300.MB09_codTec"
        SQL = SQL & " AND MB2000.nRef = MB3300.nRef"
        SQL = SQL & " AND cMuestra = ?"
        SQL = SQL & " AND MB20_tiOrig = ?"
        SQL = SQL & " AND MB20_codOrig = ?"
        SQL = SQL & " AND MB34_codEstTecAsist <> ?"
        Set qryCorresp = objApp.rdoConnect.CreateQuery("SelMorfCol", SQL)

        For i = 1 To UBound(arMorf)
            If arMorf(i).numCol < arMorf(i).numLoc Then 'se procesan las morfolog�as no asignadas
                For j = 1 To UBound(arCol)
                    If arCol(j).asig = False Then 'se procesan las colonias no asignadas
                        qryCorresp(0) = arMorf(i).cMorf
                        qryCorresp(1) = nReferencia
                        qryCorresp(2) = constRESULTANULADO
                        qryCorresp(3) = cMuestra
                        qryCorresp(4) = constORIGENCOLONIA
                        qryCorresp(5) = arCol(j).cCol
                        qryCorresp(6) = constTECANULADA
                        Set rsCorresp = qryCorresp.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                        
                        'si no se devuelve ning�n registro la colonia puede asignarse a _
                        UNA LOCALIZACI�N de la morfolog�a. La asignaci�n se realiza de la _
                        misma forma vista anteriormente
                        If rsCorresp.EOF = True Then
                            arCol(j).asig = True
                            arMorf(i).numCol = arMorf(i).numCol + 1
                            If arMorf(i).numCol = arMorf(i).numLoc Then 'la morfolog�a ha quedado completamente asignada
                                Exit For
                            End If
                        End If
                        rsCorresp.Close
                    End If
                Next j
            End If
        Next i
        
        qryCorresp.Close
    End If
    
    'se procesa la matriz arMorf para ver si la correspondencia entre morfolog�as encontradas _
    por OD y morfolog�as identificadas encolonias es completa
    If dimension > 0 Then 'dimension = ubound(arMorf)
        For i = 1 To UBound(arMorf)
            If arMorf(i).numCol < arMorf(i).numLoc Then
                msg = msg & Chr$(13) & Chr$(10)
                msg = msg & "Morfolog�a: " & arMorf(i).desig & Space(5) & "Esperadas: " & arMorf(i).numLoc
                msg = msg & Space(5) & "Identificadas: " & arMorf(i).numCol
            End If
        Next i
    End If
    
    fComprobarMicroMorfo = msg
 
End Function

Private Function fChangeNumColonia(cColMain%, cColIdentif%, arProceso() As Integer) As Boolean
'Gestiona las colonias cuando se ha identificado el mismo microorganismo en dos de ellas. _
    La gesti�n se realiza por cada placa en la que se encuentre alguna de las dos colonias

'La variable cColMain es el c�digo de la colonia primera, en la cual ya se hab�a _
    identififcado el microorganismo y cColIdentif contiene el c�digo de la nueva colonia _
    en la que se ha intentado identificar el mismo microorganismo
    
'En el caso de que cColIdentif no est� anulada (en la placa) se toman las siguientes _
    acciones: _
    1- Si no existe la colonia cColMain en la placa: cambiar n� colonia (cColIdentif --> cColMain) _
    2- Si existe la colonia cColMain en la placa: anular cColIdentif _
    3- Si la colonia cColMain est� anulada en la placa: se desanula cColMain y se anula _
    cColIdentif

'La funci�n devuelve True si las operaciones sobre la base de datos se realizaron correctamente _
    y False si se produjo alg�n error. Adem�s, en el primer caso, devuelve una matriz con _
    los datos correspondientes a los estados de ambas colonias y al cTecAsist de la colonia _
    cColIdentif que se utiliza posteriormente en pResultColonia para modificar los nodos
    
    Dim SQL As String
    Dim rsColPlaca As rdoResultset, qryColPlaca As rdoQuery
    Dim res%, dimension%
    
    fChangeNumColonia = True
    
    'se busca el posible estado de la colonia cColMain en las placas en las que existe la _
    colonia cColIdentif
    SQL = "SELECT MB1.MB20_codTecAsist, nvl(MB2.MB37_codEstPlaca,0),MB1.MB37_codEstPlaca"
    SQL = SQL & " FROM MB3000 MB1,MB3000 MB2"
    SQL = SQL & " WHERE MB1.nRef = ?"
    SQL = SQL & " AND MB1.MB27_codCol = ?"
    SQL = SQL & " AND MB2.nRef (+)= MB1.nRef"
    SQL = SQL & " AND MB2.MB20_codTecAsist (+)= MB1.MB20_codTecAsist"
    SQL = SQL & " AND MB2.Mb27_codCol (+)= ?"
    Set qryColPlaca = objApp.rdoConnect.CreateQuery("SelColPlaca", SQL)
        
    qryColPlaca(0) = nReferencia
    qryColPlaca(1) = cColIdentif
    qryColPlaca(2) = cColMain
    Set rsColPlaca = qryColPlaca.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsColPlaca.EOF = False
        If rsColPlaca(2) <> constCOLPLACAANULADA Then
            Select Case rsColPlaca(1)
            Case 0 'No existe la colonia cColMain en la placa: cambiar n� colonia (cColIdentif --> cColMain)
                res = fChangeNumColPlaca(cColIdentif, cColMain, rsColPlaca(0), False)
            Case constCOLPLACAASOCIADA 'Existe la colonia cColMain en la placa: anular cColIdentif
                res = fUpdateEstColoniaPlaca(rsColPlaca(0), cColIdentif, constCOLPLACAANULADA)
            Case constCOLPLACAANULADA 'La colonia cColMain est� anulada en la placa: se desanula cColMain y se anula cColIdentif
                res = fUpdateEstColoniaPlaca(rsColPlaca(0), cColIdentif, constCOLPLACAANULADA)
                res = fUpdateEstColoniaPlaca(rsColPlaca(0), cColMain, constCOLPLACAASOCIADA)
            End Select
            If res = False Then
                fChangeNumColonia = False
                rsColPlaca.Close
                qryColPlaca.Close
                Exit Function
            End If
        End If
        
        'se anotan los valores en la matriz arProceso
        dimension = dimension + 1
        ReDim Preserve arProceso(1 To 3, 1 To dimension)
        arProceso(1, dimension) = rsColPlaca(0)
        arProceso(2, dimension) = rsColPlaca(1)
        arProceso(3, dimension) = rsColPlaca(2)
        
        rsColPlaca.MoveNext
    Loop
    
    rsColPlaca.Close
    qryColPlaca.Close
    
End Function

Private Function fChangeNumColPlaca(cColOld%, cColNew%, cTecAsist%, colNueva As Boolean) As Boolean
'Cambia el n� de colonia en una placa. Afecta a las tablas MB2000, MB2900, MB3000 _
    (y  MB2700 si la colonia es nueva). Este cambio se realiza mediante llamadas a funciones _
    que afectan a cada una de las tablas

'La funci�n devuelve True si las operaciones sobre la base de datos se realizaron correctamente _
    y False si se produjo alg�n problema

    Dim qryChangeCol As rdoQuery
    Dim res As Boolean
    
    If colNueva = True Then
        If fInsertColonia(cColNew, constCOLLOCALIZADA) = False Then 'MB2700
            Exit Function
        End If
    End If
    If fUpdateNumCol_Placa(cColOld, cColNew, cTecAsist) = False Then 'MB3000
        Exit Function
    End If
    If fUpdateNumCol_Antibiograma(cColOld, cColNew, cTecAsist) = False Then 'MB2900
        Exit Function
    End If
    If fUpdateNumCol_TecAsist(cColOld, cColNew, cTecAsist) = False Then 'MB2000
        Exit Function
    End If
    fChangeNumColPlaca = True
    
End Function
Private Function fDatosTecnica(cTecAsist%) As Integer
'trae datos de campos largos de las t�cnicas y devuelve el estado de la t�cnica

'Se pone un On Error Resume Next ya que en caso de producirse un error, saldr� _
el mensaje del MSRDC

    Dim SQL As String
    
    On Error Resume Next
    SQL = "SELECT nRef, MB20_codTecAsist, MB34_codEstTecAsist, MB20_reali, MB20_observ"
    SQL = SQL & " FROM MB2000"
    SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
    SQL = SQL & " AND MB20_codTecAsist = " & cTecAsist
    msrdcTecnicas.SQL = SQL
    msrdcTecnicas.Refresh
    fDatosTecnica = msrdcTecnicas.Resultset(2)
    
End Function

Private Function fDatosCultivo(cTecAsist%) As Integer
'Trae datos de campos largos de cultivos y devuelve el estado del cultivo

'Se pone un On Error Resume Next ya que en caso de producirse un error, saldr� _
el mensaje del MSRDC

    Dim SQL As String
    
    On Error Resume Next
    SQL = "SELECT nRef, MB20_codTecAsist, MB34_codEstTecAsist, MB20_reali, MB20_observ"
    SQL = SQL & " FROM MB2000"
    SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
    SQL = SQL & " AND MB20_codTecAsist = " & cTecAsist
    msrdcCultivos.SQL = SQL
    msrdcCultivos.Refresh
    fDatosCultivo = msrdcCultivos.Resultset(2)
    
End Function


Private Function fDatosColonia(cCol%, cTecAsist%) As Integer
'Trae datos de campos largos de colonias y devuelve el estado de la colonia

'NOTA: despu�s de a�adir observaciones a una colonia nueva reci�n a�adida y validar, si _
pinchamos en otra colonia, se produce un error en el refresh del msrdcColonias (No se ha _
actualizado o eliminado ninguna fila). Se ha solucionado dicho error haci�ndo el Close y _
posterior Refresh del msrdcColonias en los eventos LostFocus y GotFocus de la caja de texto.

    Dim SQL As String
    
    On Error Resume Next
    SQL = "SELECT nRef, MB27_codCol, MB36_codEstCol, MB27_descrip"
    SQL = SQL & " FROM MB2700"
    SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
    SQL = SQL & " AND MB27_codCol = " & cCol
    msrdcColonias.SQL = SQL
    msrdcColonias.Refresh
    fDatosColonia = msrdcColonias.Resultset(2)
    
    SQL = "SELECT nRef, MB20_codTecAsist, MB27_codCol, MB30_observ"
    SQL = SQL & " FROM MB3000"
    SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
    SQL = SQL & " AND MB20_codTecAsist = " & cTecAsist
    SQL = SQL & " AND MB27_codCol = " & cCol
    msrdcPlacas.SQL = SQL
    msrdcPlacas.Refresh
    
End Function
Private Function fDeleteAntibioticos(cAnti%, cTecAsist%) As Boolean
'Elimina un antibi�tico de la tabla de Antibiogramas

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryAnti As rdoQuery
    
    fDeleteAntibioticos = False
    
    On Error Resume Next
    Err = 0
    objApp.rdoConnect.BeginTrans
    SQL = "DELETE FROM MB2900"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB07_codAnti = ?"
    Set qryAnti = objApp.rdoConnect.CreateQuery("DeleteAntibiotico", SQL)
    
    qryAnti(0) = nReferencia
    qryAnti(1) = cTecAsist
    qryAnti(2) = cAnti
    qryAnti.Execute
    
    If Err = 0 Then
        fDeleteAntibioticos = True
    End If
    
    qryAnti.Close
    
End Function

Private Function fDeleteMorfologias(cMorf%, nLoc%, cMuestra$) As Boolean
'Elimina UNA morfolog�a de la tabla Resultados-Muestra

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryMorf As rdoQuery
    
    fDeleteMorfologias = False
    
    On Error Resume Next
    If nLoc = 1 Then
        SQL = "DELETE FROM MB2100"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND cMuestra = ?"
        SQL = SQL & " AND MB12_codMorf = ?"
        Set qryMorf = objApp.rdoConnect.CreateQuery("DeleteMorfo", SQL)
        
        qryMorf(0) = nReferencia
        qryMorf(1) = cMuestra
        qryMorf(2) = cMorf
    Else
        SQL = "UPDATE MB2100 "
        SQL = SQL & " SET MB21_numLoc = ?"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND cMuestra = ?"
        SQL = SQL & " AND MB12_codMorf = ?"
        Set qryMorf = objApp.rdoConnect.CreateQuery("UpdateMorfo", SQL)
        
        qryMorf(0) = nLoc - 1
        qryMorf(1) = nReferencia
        qryMorf(2) = cMuestra
        qryMorf(3) = cMorf
    End If
    
    qryMorf.Execute
    
    If Err = 0 Then
        fDeleteMorfologias = True
    End If
    
    qryMorf.Close
    
End Function



Private Function fFinalizarPrueba() As Integer
'Da por concluida una prueba validando todas las t�cnicas que est�n pendientes y _
    finalizando las placas que no lo estuvieran

'La funci�n devuelve los siguientes valores: _
    0 - si se han producido errores en las operaciones sobre la base de datos _
    1 - si se ha finalizado la prueba correctamente _
    2 - si se ha cancelado la operaci�n
    
    Dim msgTec As String, msgMorf1 As String, msgMorf2 As String
    Dim pregunta As Boolean
    Dim nodo As Node, i As Integer
    Dim cTecnicas As String, cCultivos As String
    
    fFinalizarPrueba = 1
    
    'se mira si existen t�cnicas o cultivos solicitados y muestra un aviso por si el _
    usuario no quiere cancelar la operaci�n
    For i = 1 To tvwMuestras.Nodes.Count
        Set nodo = tvwMuestras.Nodes(i)
        If Left$(nodo.Key, 1) = "T" Or Left$(nodo.Key, 1) = "C" Then
            If Val(nodo.Tag) = constTECSOLICITADA Or Val(nodo.Tag) = constTECREINCUBADA Then
                msgTec = "Existen t�cnicas y/o cultivos solicitados para realizar."
                msgTec = msgTec & Chr$(13) & Chr$(10)
                Exit For
            End If
        End If
    Next i
    If msgTec <> "" Then
        pregunta = True
        If MsgBox(msgTec & "�Desea Ud. dar por finalizada la prueba?", vbQuestion + vbYesNo, "FinalizarPruebas") = vbNo Then
            fFinalizarPrueba = 2
            Exit Function
        End If
    End If
    
    'se mira si existe correspondencia entre los microorganismos identificados y las _
    morfolog�as encontradas por observaci�n directa
    For i = 1 To tvwMuestras.Nodes.Count
        Set nodo = tvwMuestras.Nodes(i)
        If Left$(nodo.Key, 1) = "M" Then
            msgMorf1 = fComprobarMicroMorfo(Mid$(nodo.Key, 2))
            If msgMorf1 <> "" Then
                msgMorf2 = msgMorf2 & Chr$(13) & Chr$(10)
                msgMorf2 = msgMorf2 & " * Muestra: " & Mid$(nodo.Key, 2) & msgMorf1
            End If
        End If
    Next i
    If msgMorf2 <> "" Then
        pregunta = True
        msgMorf1 = "Se han identificado menos microorganismos de los esperados:"
        msgMorf1 = msgMorf1 & Chr$(13) & Chr$(10) & msgMorf2
        msgMorf1 = msgMorf1 & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
        If MsgBox(msgMorf1 & "�Desea Ud. dar por finalizada la prueba?", vbQuestion + vbYesNo, "FinalizarPruebas") = vbNo Then
            fFinalizarPrueba = 2
            Exit Function
        End If
    End If
    
    'si los chequeos anteriores fueron correctos se pregunta confirmaci�n
    If pregunta = False Then
        If MsgBox("�Desea Ud. dar por finalizada la prueba?", vbQuestion + vbYesNo, "FinalizarPruebas") = vbNo Then
            fFinalizarPrueba = 2
            Exit Function
        End If
    End If
    
    'se anotan las t�cnicas/cultivos que hay que validar/finalizar
    For i = 1 To tvwMuestras.Nodes.Count
        Set nodo = tvwMuestras.Nodes(i)
        If Left$(nodo.Key, 1) = "T" Then
            If Val(nodo.Tag) < constTECVALIDADA Then
                cTecnicas = cTecnicas & Val(Mid$(nodo.Key, 2)) & ","
            End If
        ElseIf Left$(nodo.Key, 1) = "C" Then
            If Val(nodo.Tag) < constTECVALIDADA Then
                cCultivos = cCultivos & Val(Mid$(nodo.Key, 2)) & ","
            End If
        End If
    Next i
    
    'se validan/finalizan las t�cnicas/cultivos en la base de datos
    If cTecnicas <> "" Then
        cTecnicas = Left$(cTecnicas, Len(cTecnicas) - 1)
        'If fUpdateEstTecnicas(cTecnicas, constTECANULADA) = False Then
        If fUpdateEstTecnicas(cTecnicas, constTECVALIDADA) = False Then
            fFinalizarPrueba = 0
            Exit Function
        End If
    End If
    If cCultivos <> "" Then
        cCultivos = Left$(cCultivos, Len(cCultivos) - 1)
        If fUpdateEstCultivos(cCultivos, constTECFINALIZADA) = False Then
            fFinalizarPrueba = 0
            Exit Function
        End If
    End If
    
End Function




Private Sub pBuscarReacPorConcentracion(cAnti$, conc$)
'Busca la sensibilidad del antibi�tico en funci�n de la concentraci�n se�alada

'La subrutina actualiza los valores de la variable global codReac y del texto del combo _
    cbossReac que indican la sensibilidad del antibi�tico

    Dim SQL As String
    Dim rsRefAntib As rdoResultset, qryRefAntib As rdoQuery
    
    SQL = "SELECT MB4400.MB40_codReac, MB40_desig"
    SQL = SQL & " FROM MB4400, MB4000"
    SQL = SQL & " WHERE MB07_codAnti = ?"
    SQL = SQL & " AND (MB44_concIni <= ? OR MB44_concIni IS NULL)"
    SQL = SQL & " AND (MB44_concFin > ? OR MB44_concFin IS NULL)"
    SQL = SQL & " AND MB4000.MB40_codReac = MB4400.MB40_codReac"
    Set qryRefAntib = objApp.rdoConnect.CreateQuery("", SQL)
    qryRefAntib(0) = cAnti
    qryRefAntib(1) = conc
    qryRefAntib(2) = conc
    Set rsRefAntib = qryRefAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsRefAntib.EOF = False Then
        codReac = rsRefAntib(0)
        cbossReac.Text = rsRefAntib(1)
    End If
    rsRefAntib.Close
    qryRefAntib.Close
    
End Sub

Private Sub pBuscarReacPorDiametro(cAnti$, diam$)
'Busca la sensibilidad del antibi�tico en funci�n del di�metro se�alado

'La subrutina actualiza los valores de la variable global codReac y del texto del combo _
    cbossReac que indican la sensibilidad del antibi�tico
    
    Dim SQL As String
    Dim rsRefAntib As rdoResultset, qryRefAntib As rdoQuery
    
    SQL = "SELECT MB4400.MB40_codReac, MB40_desig"
    SQL = SQL & " FROM MB4400, MB4000"
    SQL = SQL & " WHERE MB07_codAnti = ?"
    SQL = SQL & " AND (MB44_diamIni <= ? OR MB44_diamIni IS NULL)"
    SQL = SQL & " AND (MB44_diamFin > ? OR MB44_diamFin IS NULL)"
    SQL = SQL & " AND MB4000.MB40_codReac = MB4400.MB40_codReac"
    Set qryRefAntib = objApp.rdoConnect.CreateQuery("", SQL)
    qryRefAntib(0) = cAnti
    qryRefAntib(1) = diam
    qryRefAntib(2) = diam
    Set rsRefAntib = qryRefAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsRefAntib.EOF = False Then
        codReac = rsRefAntib(0)
        cbossReac.Text = rsRefAntib(1)
    End If
    rsRefAntib.Close
    qryRefAntib.Close
    
End Sub
Private Sub pDatosMuestra(cMuestra$)
'Se traen a pantalla los datos de la muestra (comentarios a la extracci�n)

    Dim SQL As String
    Dim rsMuestra As rdoResultset, qryMuestra As rdoQuery
    
    SQL = "SELECT comentariosExtraccion FROM muestraAsistencia"
    SQL = SQL & " WHERE cMuestra = ?"
    Set qryMuestra = objApp.rdoConnect.CreateQuery("Muestra", SQL)
    qryMuestra(0) = cMuestra
    Set rsMuestra = qryMuestra.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not IsNull(rsMuestra(0)) Then
        txtObservMuestra.Text = rsMuestra(0)
    Else
        txtObservMuestra.Text = ""
    End If
    
End Sub

Public Sub pGenerarInformePorDefecto(nRef$, cPrueba$)
'Introduce un informe por defecto, gener�lmente cuando no ha habido crecimiento. El informe _
    se introduce como resultado de una t�cnica de resultados.
   
'Condiciones necesarias para realizar esta operaci�n:
'1- Que no se haya informado ninguna colonia. Pueden existir colonias validadas pero que _
    no resultan significativas.
'2- Que no se haya informado ning�n otro resultado (informe) del tipo de t�cnica de resultados. _
    Pueden existir informes validados pero no informados.
'3- Que existan informes a a�adir por defecto que cumplan las condiciones de prueba y/o muestra.

'Palabras a sustituir dentro del informe:
'1- %d%, %D% por el n� de d�as transcurridos desde la siembra de la primera placa
'2- %h%, %H% por el n� de horas transcurridos desde la siembra de la primera placa
'3- %s%, %S% por el n� de semanas transcurridos desde la siembra de la primera placa
    
    Dim SQL As String
    Dim rsInforme As rdoResultset, qryInforme As rdoQuery
    Dim rsFecReali As rdoResultset, qryFecReali As rdoQuery
    Dim textoInforme$, unidadTiempo$, tiempo$
    Dim i%, claveNodo$
    Dim nodo As Node
    Dim msg$
    
    'se mira si se ha informado alguna colonia
    SQL = "SELECT MB27_codCol FROM MB2700"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB36_codEstCol = ?"
    Set qryInforme = objApp.rdoConnect.CreateQuery("", SQL)
    qryInforme(0) = nRef
    qryInforme(1) = constCOLINFORMADA
    Set rsInforme = qryInforme.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsInforme.EOF = False Then 'existen colonias informadas
        Exit Sub 'no se a�ade ning�n informe
    End If
    rsInforme.Close
    qryInforme.Close
    
    'se mira si se ha informado alguna t�cnica de resultado con valor no nulo
    SQL = "SELECT MB3300.MB20_codTecAsist"
    SQL = SQL & " FROM MB3300, MB0900"
    SQL = SQL & " WHERE MB3300.nRef = ?"
    SQL = SQL & " AND MB3300.MB38_codEstResult = ?"
    SQL = SQL & " AND MB3300.MB33_result IS NOT NULL" 'ver si es necesaria esta condici�n
    SQL = SQL & " AND MB0900.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB0900.MB04_codTiTec = ?"
    Set qryInforme = objApp.rdoConnect.CreateQuery("", SQL)
    qryInforme(0) = nRef
    qryInforme(1) = constRESULTINFORMADO
    qryInforme(2) = constRESULTADOS
    Set rsInforme = qryInforme.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsInforme.EOF = False Then 'existen informes no nulos ya informados
        Exit Sub 'no se a�ade ning�n informe
    End If
    rsInforme.Close
    qryInforme.Close
    
    'se mira si hay alg�n informe a a�adir por defecto y en su caso, se anota en la variable _
    textoInforme, una vez hechas las modificaciones que fueran necesarias
    SQL = "SELECT MB45_descrip"
    SQL = SQL & " FROM MB4500"
    SQL = SQL & " WHERE (cPrueba = ? AND cTipoMuestra = ?)"
    SQL = SQL & " OR (cPrueba IS NULL AND cTipoMuestra = ?)"
    SQL = SQL & " OR (cPrueba = ? AND cTipoMuestra IS NULL)"
    SQL = SQL & " OR (cPrueba IS NULL AND cTipoMuestra IS NULL)"
    SQL = SQL & " ORDER BY cTipoMuestra, cPrueba"
    'el ORDER BY ordena los registros de forma que el primero es el que cumple la mayor _
    cantidad de restricciones de Muestra y Prueba
    Set qryInforme = objApp.rdoConnect.CreateQuery("", SQL)
    qryInforme(0) = cPrueba
    qryInforme(1) = tvwMuestras.Nodes.Item(1).Tag 'tipo de muestra
    qryInforme(2) = tvwMuestras.Nodes.Item(1).Tag 'tipo de muestra
    qryInforme(3) = cPrueba
    Set rsInforme = qryInforme.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsInforme.EOF = False Then 'existen informes a aplicar por defecto
        textoInforme = rsInforme(0)
        'se transforma el texto a insertar en el informe
        If InStr(UCase(textoInforme), "%D%") > 0 Then 'd�as
            unidadTiempo = Mid$(textoInforme, InStr(UCase(textoInforme), "%D%") + 1, 1)
        ElseIf InStr(UCase(textoInforme), "%H%") > 0 Then 'horas
            unidadTiempo = Mid$(textoInforme, InStr(UCase(textoInforme), "%H%") + 1, 1)
        ElseIf InStr(UCase(textoInforme), "%S%") > 0 Then 'semanas
            'se transforman los caracteres %S% a %WW% que es el indicador de semanas en el _
            DateDiff
            textoInforme = objGen.ReplaceStr(textoInforme, "%" & unidadTiempo & "%", "%WW%", 0)
            unidadTiempo = "WW"
        End If
        If unidadTiempo <> "" Then
            'se busca la fecha de siembra de la primera placa
            SQL = "SELECT MB26_fecEntra"
            SQL = SQL & " FROM MB2000, MB2600"
            SQL = SQL & " WHERE MB2000.nRef = ?"
            SQL = SQL & " AND MB2000.MB20_codPlaca = 1"
            SQL = SQL & " AND MB2600.nRef = MB2000.nRef"
            SQL = SQL & " AND MB2600.MB20_codTecAsist = MB2000.MB20_codTecAsist"
            SQL = SQL & " AND MB2600.MB26_codPaso = 1"
            Set qryFecReali = objApp.rdoConnect.CreateQuery("", SQL)
            qryFecReali(0) = nRef
            Set rsFecReali = qryFecReali.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If rsFecReali.EOF = False Then
                tiempo = DateDiff(unidadTiempo, rsFecReali(0), fFechaHoraActual)
                textoInforme = objGen.ReplaceStr(textoInforme, "%" & unidadTiempo & "%", tiempo, 0)
            End If
        End If
    Else 'no hay ning�n informe a aplicar por defecto
        Exit Sub 'no se a�ade ning�n informa
    End If
    rsInforme.Close
    qryInforme.Close
            
    'se a�ade una t�cnica del tipo resultados. Se busca en el tvwTecnicas la primera de las _
    t�cnicas de tipo resultado
    If tvwTecnicas.Nodes.Count > 0 Then
        For i = 1 To tvwTecnicas.Nodes.Count
            claveNodo = tvwTecnicas.Nodes.Item(i).Key
            If Left$(claveNodo, 1) = "T" And Val(Mid$(claveNodo, InStr(claveNodo, "I") + 1)) = constRESULTADOS Then
                'aviso previo
                msg = "No se ha realizado informe de la ausencia de colonias o no se ha informado su presencia. "
                msg = msg & "Se va a a�adir e INFORMAR el siguiente informe de resultados de la prueba:"
                msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
                msg = msg & "' " & textoInforme & " '"
                If MsgBox(msg, vbQuestion + vbYesNo, "A�adir Informe") = vbYes Then
                    Call pInsertarTecnicas(tvwMuestras.Nodes.Item(1), tvwTecnicas.Nodes.Item(i), False)
                    'a�adir el texto del resultado e informar
                    Set nodo = tvwMuestras.Nodes.Item("X" & Mid$(tvwMuestras.Nodes.Item(1).Key, 2)).Previous.Child
                    tvwMuestras_NodeClick nodo 'se traen los datos del resultado (unidad) a las cajas de texto
                    tvwMuestras.Tag = "SoloExpandir"                            '
                    Set tvwMuestras.SelectedItem = nodo                         'se selecciona el
                    Set tvwMuestras.DropHighlight = tvwMuestras.SelectedItem    'nodo de resultado
                    tvwMuestras.Tag = ""                                        '
                    Call pResultResultadoTecnica(nodo, textoInforme, constRESULTINFORMADO) 'se introduce e informa el resultado
                    Call pEstadoBotones("R", constRESULTINFORMADO, "M") 'se establece el estadod de los botones
                End If
                Exit For
            End If
        Next i
    Else
        Exit Sub 'no hay ninguna t�cnica definida para la secci�n
    End If
    
End Sub

Public Function fComprobarFinalizacionPrueba(nReferencia$) As Boolean
'se comprueba si la prueba est� finalizada. Se considera finalizada cuando no tiene ninguna _
    t�cnica pendiente (t�cnica o cultivo)

    Dim SQL As String
    Dim rsTecAsist As rdoResultset, qryTecAsist As rdoQuery
    
    SQL = "SELECT nRef FROM MB2000"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB34_codEstTecAsist < ?"
    Set qryTecAsist = objApp.rdoConnect.CreateQuery("", SQL)
    qryTecAsist(0) = nReferencia
    qryTecAsist(1) = constTECVALIDADA
    Set rsTecAsist = qryTecAsist.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsTecAsist.EOF = False Then
        fComprobarFinalizacionPrueba = False
    Else
        fComprobarFinalizacionPrueba = True
    End If
    rsTecAsist.Close
    qryTecAsist.Close
    
End Function


Private Sub pInsertarAntibRelacion(cAnti$, cReac$, cMicro$, nRef$, cCol$, cTecAsist$)
'busca los antibi�ticos relacionados con el que se acaba de validar y se introducen en el _
    antibiograma si no lo est�n. Si est�n pero no tienen valor, se actualizan.

    Dim SQL As String
    Dim rsAntibRel As rdoResultset, qryAntibRel As rdoQuery
    Dim rsAntibiograma As rdoResultset, qryAntibiograma As rdoQuery
    Dim arAntib() As typeReaccionAntibiotico
    Dim dimension%, i%
    Dim accion As String '"insert" o "update"
    Dim msg$
    
    'se buscan los antibi�ticos a a�adir o actualizar. Para hacerlo de una forma sencilla _
    se traen sin distinguir que acci�n (a�adir/actualizar) corresponde a cada uno. A la _
    hora de insertar, si la clave principal est� duplicada, se produce el error 23000 _
    que indica que hay que hacer una actualizaci�n en lugar de un insert. Los antibi�ticos _
    a�adidos o actualizar se anotan en la matriz arAntib() para luego a�adir o actualizar _
    los nodos
    SQL = "SELECT MB07_codAnti_2, MB40_codReac_2, MB07_desig, MB40_desig"
    SQL = SQL & " FROM MB2300, MB0700, MB4000"
    SQL = SQL & " WHERE MB07_codAnti_1 = ?"
    SQL = SQL & " AND MB40_codReac_1 = ?"
    SQL = SQL & " AND MB23_indActiva = -1"
    SQL = SQL & " AND (MB18_codMicro = ? OR MB18_codMicro IS NULL)"
    SQL = SQL & " AND MB07_codAnti_2 NOT IN("
    SQL = SQL & "       SELECT MB07_codAnti FROM MB2900"
    SQL = SQL & "       WHERE nRef = ?"
    SQL = SQL & "       AND MB27_codCol = ?"
    SQL = SQL & "       AND MB40_codReac IS NOT NULL)"
    SQL = SQL & " AND MB0700.MB07_codAnti = MB2300.MB07_codAnti_2"
    SQL = SQL & " AND MB4000.MB40_codReac = MB2300.MB40_codReac_2"
    Set qryAntibRel = objApp.rdoConnect.CreateQuery("", SQL)
    qryAntibRel(0) = cAnti
    qryAntibRel(1) = cReac
    qryAntibRel(2) = cMicro
    qryAntibRel(3) = nRef
    qryAntibRel(4) = cCol
    Set rsAntibRel = qryAntibRel.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsAntibRel.EOF = False Then 'hay antibi�ticos relacionados para insertar/actualizar en el antibiograma
        msg = "�Desea Ud. que los antibi�ticos y sensibilidades relacionadas sean a�adidos?"
        If MsgBox(msg, vbInformation + vbYesNo, "Antibi�ticos relacionados") = vbNo Then Exit Sub
        
        On Error Resume Next
        objApp.rdoConnect.BeginTrans
        accion = "insert" 'primeramente se intenta el insert; en caso de error 23000 se hace el update
        
        Do While rsAntibRel.EOF = False
            'se preparan los statements para el insert o la actualizaci�n
            If accion = "insert" Then
                SQL = "INSERT INTO MB2900 (MB40_codReac,cUser_Val,cUser_Inf,nRef,MB27_codCol,MB07_codAnti,MB20_codTecAsist)"
                SQL = SQL & " VALUES(?,?,?,?,?,?,?)"
            Else
                SQL = "UPDATE MB2900 SET MB40_codReac = ?, cUser_Val = ?, cUser_Inf = ?"
                SQL = SQL & " WHERE nRef = ?"
                SQL = SQL & " AND MB27_codCol = ?"
                SQL = SQL & " AND MB07_codAnti = ?"
                'si el microorganismo no est� identificado, s�lo se pone el resultado del _
                antibi�tico presente en la t�cnica actual. Si est� identificado, se pone el _
                resultado en todos los antibiogramas de la colonia que tengan ese antibi�tico
                If cMicro = -1 Then 'microorganismo no identificado
                    SQL = SQL & " AND MB20_codTecAsist = ?"
                End If
            End If
            Set qryAntibiograma = objApp.rdoConnect.CreateQuery("", SQL)
            qryAntibiograma(0) = rsAntibRel(1)
            qryAntibiograma(1) = cUserPeticion
            qryAntibiograma(2) = cUserPeticion
            qryAntibiograma(3) = nRef
            qryAntibiograma(4) = cCol
            qryAntibiograma(5) = rsAntibRel(0)
            If cMicro = -1 Or accion = "insert" Then
                qryAntibiograma(6) = cTecAsist
            End If
            qryAntibiograma.Execute
            
            If Err = 0 Then
                'se anotan los datos en la matriz arAntib
                dimension = dimension + 1
                ReDim Preserve arAntib(1 To dimension)
                arAntib(dimension).cAnti = rsAntibRel(0)
                arAntib(dimension).conc = rsAntibRel(2) 'desig antibi�tico
                arAntib(dimension).cTecAsist = cTecAsist
                arAntib(dimension).desigReac = rsAntibRel(3)
                arAntib(dimension).recom = -1
                arAntib(dimension).accion = accion
                'se procesa el siguiente antibi�tico
                accion = "insert"
                rsAntibRel.MoveNext
            ElseIf Val(Error) = 23000 Then 'SI EL ERROR ES EL DE CLAVE DUPLICADA
                'se ha producido un error en el insert. Se intenta el update. No se hace el _
                movenext para procesar el mismo antibi�tico
                Err = 0
                accion = "update"
            Else
                'si el error es otro, se cancela la operaci�n
                Exit Do
            End If
        Loop
        qryAntibiograma.Close
        
        If Err = 0 Then
            objApp.rdoConnect.CommitTrans
            For i = 1 To dimension 'se procesan todos los antibi�ticos a�adidos o actualizados
                If arAntib(i).accion = "insert" Then
                    'si se ha a�adido, se a�ade el nodo
                    Call pAddNodoAntibiotico(tvwMuestras, arAntib(i).cAnti, "", arAntib(i).desigReac, arAntib(i).conc, arAntib(i).recom, arAntib(i).cTecAsist)
                Else
                    'si se ha modificado, se modifica el texto del nodo
                    Call pTextoDatosAntibioticos(CInt(cMicro), True, tvwMuestras.Nodes.Item("A" & arAntib(i).cAnti & "C" & cTecAsist), arAntib(i).desigReac, "", arAntib(i).recom)
                End If
            Next i
        Else 'se cancela la operaci�n
            MsgBox "Se ha producido un error al a�adir los antibi�ticos relacionados.", vbError
            objApp.rdoConnect.RollbackTrans
        End If
    End If
    rsAntibRel.Close
    qryAntibRel.Close
    
End Sub
Private Sub pValidarTodosResultados(nodo As Node)
'Se validan todos los resultados de la t�cnica que est�n 'Solicitados' y no sean nulos.

'Si la t�cnica se realiza sobre una colonia, se comprueba la validez de cada resultado antes _
    de pasar a validarlo. Se utilizan las funciones fComprobarResultadoTec y _
    fComprobarResultadoMismaTec para realizar las comprobaciones. Si hay desviaciones, _
    el usuario decide que hacer (validar o no).
    
    Dim SQL As String
    Dim strResult As String
    Dim rsResult As rdoResultset, qryResult As rdoQuery
    Dim cTecAsist%, cTiTec%, cCol%
    Dim estadoRes As Boolean
    Dim qryValidaTec As rdoQuery
    Dim i%, nodoProcesado As Node
    
    'se obtienen algunos datos interesantes a partir del nodo
    cTecAsist = Val(Mid$(nodo.Key, InStr(nodo.Key, "T") + 1))
    cTiTec = Val(Mid$(nodo.Key, InStr(nodo.Key, "I") + 1))
    If Left$(nodo.Parent, 1) = "K" Then
        cCol = Val(Mid$(nodo.Parent.Key, InStr(nodo.Parent.Key, "K") + 1)) 't�cnica sobre colonia
    Else
        cCol = 0 't�cnica sobre muestra
    End If
    
    'se buscan los resultados de la t�cnica a validar y se introducen en forma de string _
    en la variable strResult
    SQL = "SELECT MB09_codTec, MB32_codResult, MB33_result"
    SQL = SQL & " FROM MB3300"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB38_codEstResult = " & constRESULTSOLICITADO
    SQL = SQL & " AND MB33_result IS NOT NULL"
    SQL = SQL & " ORDER BY MB32_codResult"
    Set qryResult = objApp.rdoConnect.CreateQuery("SelResult", SQL)
        
    qryResult(0) = nReferencia
    qryResult(1) = cTecAsist
    Set rsResult = qryResult.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
   
    If rsResult.EOF = False Then
        Do While rsResult.EOF = False
            'chequeos de resultados
            estadoRes = True
            If cCol > 0 Then
                ' se realiza el chequeo de validez del resultados respecto al maestro de _
                resultados de t�cnicas sobre microorganismos
                If fComprobarResultadoTec(cCol, rsResult(0), rsResult(1), rsResult(2)) = False Then
                    estadoRes = False
                Else
                    ' se realiza el chequeo de validez del resultados respecto a posibles resultados _
                    de la misma t�cnica sobre la colonia obtenidos anteriormente
                    If fComprobarResultadoMismaTec(cCol, rsResult(0), rsResult(1), rsResult(2), cTecAsist) = False Then
                        estadoRes = False
                    End If
                End If
            End If
            
            If estadoRes = True Then
                strResult = strResult & rsResult(1) & ","
            End If
            
            rsResult.MoveNext
        Loop
    Else
        'ANOTAR LA VALIDACI�N DE LA T�CNICA ??
    End If
    rsResult.Close
    qryResult.Close
    
    'se validan los resultados en la base de datos y la t�cnica si es el caso
    If strResult <> "" Then
        strResult = Left$(strResult, Len(strResult) - 1)
     
        On Error Resume Next
        objApp.rdoConnect.BeginTrans
        SQL = "UPDATE MB3300"
        SQL = SQL & " SET MB38_codEstResult = ?"
        SQL = SQL & " ,cUser_Val = ?"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND MB20_codTecAsist = ?"
        'no es necesario incluir el cTec
        SQL = SQL & " AND MB32_codResult IN (" & strResult & ")"
        Set qryResult = objApp.rdoConnect.CreateQuery("ValidarTodosResultados", SQL)
    
        qryResult(0) = constRESULTVALIDADO
        qryResult(1) = cUserPeticion
        qryResult(2) = nReferencia
        qryResult(3) = cTecAsist
        qryResult.Execute
        
        If Err <> 0 Then
            objApp.rdoConnect.RollbackTrans
            MsgBox "Se ha producido un error al validar los resultados.", vbError
            Exit Sub
        Else
            'se valida la t�cnica
            If fUpdateEstTecnicas(CStr(cTecAsist), constTECVALIDADA) = False Then
                objApp.rdoConnect.RollbackTrans
                MsgBox "Se ha producido un error al validar la t�cnica.", vbError
                Exit Sub
            Else
                objApp.rdoConnect.CommitTrans
                
                'se actualizan los nodos de los resultados y de la t�cnica
                For i = 1 To nodo.Children
                    If i = 1 Then
                        Set nodoProcesado = nodo.Child
                    Else
                        Set nodoProcesado = nodoProcesado.Next
                    End If
                    'se mira si hay que actualizar el nodo
                    If InStr(strResult, Val(Mid$(nodoProcesado.Key, InStr(nodoProcesado.Key, "R") + 1))) > 0 Then
                        Call pUpdateNodoResultTecnicaEstado(nodoProcesado, constRESULTVALIDADO)
                    End If
                Next i
                Call pUpdateNodoTecnicaEstado(nodo, constTECVALIDADA, True)
            End If
        End If
        qryValidaTec.Close
    End If

End Sub

Private Sub pInformarTodosResultados(nodo As Node)
'Se informan todos los resultados de la t�cnica que est�n validados.

    Dim SQL As String
    Dim qryInformarRes As rdoQuery
    Dim i%, nodoProcesado As Node
    Dim cTecAsist%
    Dim res%
    Dim cResult%, cResultados$
    
    cTecAsist = Val(Mid$(nodo.Key, InStr(nodo.Key, "T") + 1))
    
    On Error Resume Next
    objApp.rdoConnect.BeginTrans
    
    'se pasan los resultados a la base de datos de Laboratorio: se procesan todos los nodos _
    de resultados validados para ver cuales se han podido informar al Laboratorio correctamente. _
    Los resultados informados se anotan en el string cResultados
    cResultados = ""
    For i = 1 To nodo.Children
        If i = 1 Then
            Set nodoProcesado = nodo.Child
        Else
            Set nodoProcesado = nodoProcesado.Next
        End If
        If Val(nodoProcesado.Tag) = constRESULTVALIDADO Then
            cResult = Val(Mid$(nodoProcesado.Key, InStr(nodoProcesado.Key, "R") + 1))
            res = fInformarResultadoLabor(nReferencia, cTecAsist, objPruebaAsistencia.rdoCursor("historia"), objPruebaAsistencia.rdoCursor("caso"), objPruebaAsistencia.rdoCursor("secuencia"), objPruebaAsistencia.rdoCursor("nRepeticion"), cUserPeticion, cResult)
            Select Case res
                Case constCORRECTO
                    cResultados = cResultados & cResult & ","
                Case constINCORRECTO
                    MsgBox "Se ha producido un error al informar los resultados.", vbError
                    objApp.rdoConnect.RollbackTrans
                    Exit Sub
                Case constOTRO
            End Select
        End If
    Next i
    
    If cResultados <> "" Then
        'se anotan los resultados en la base de datos de Microbiolog�a
        cResultados = Left$(cResultados, Len(cResultados) - 1)
        
        SQL = "UPDATE MB3300"
        SQL = SQL & " SET MB38_codEstResult = ?"
        SQL = SQL & " ,cUser_Inf = ?"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND MB20_codTecAsist = ?"
        'no es necesario incluir el cTec
        'SQL = SQL & " AND MB38_codEstResult = ?"
        SQL = SQL & " AND MB32_codResult IN (" & cResultados & ")"
        Set qryInformarRes = objApp.rdoConnect.CreateQuery("InformarTodosResultados", SQL)
        
        qryInformarRes(0) = constRESULTINFORMADO
        qryInformarRes(1) = cUserPeticion
        qryInformarRes(2) = nReferencia
        qryInformarRes(3) = cTecAsist
        'qryInformarRes(4) = constRESULTVALIDADO
        qryInformarRes.Execute
        qryInformarRes.Close
        
        If Err <> 0 Then
            objApp.rdoConnect.RollbackTrans
            MsgBox "Se ha producido un error al informar los resultados.", vbError
            Exit Sub
        Else
            objApp.rdoConnect.CommitTrans
            'se modifica el texto de los nodos
            On Error GoTo 0
            For i = 1 To nodo.Children
                If i = 1 Then
                    Set nodoProcesado = nodo.Child
                Else
                    Set nodoProcesado = nodoProcesado.Next
                End If
                If Val(nodoProcesado.Tag) = constRESULTVALIDADO Then
                    Call pUpdateNodoResultTecnicaEstado(nodoProcesado, constRESULTINFORMADO)
                End If
            Next i
        End If
    End If
    
End Sub
Private Function fInsertColonia(numCol%, estado%) As Boolean
'se a�ade la colonia a la tabla de colonias

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery
    
    On Error Resume Next
    
    SQL = "INSERT INTO MB2700(nRef,MB27_codCol,MB27_fecDesar,MB36_codEstCol)"
    SQL = SQL & " VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?)"
    Set qryCol = objApp.rdoConnect.CreateQuery("InsertCol", SQL)
    
    qryCol(0) = nReferencia
    qryCol(1) = numCol
    qryCol(2) = fFechaHoraActual
    qryCol(3) = estado
    qryCol.Execute
    
    If Err = 0 Then fInsertColonia = True
    
    qryCol.Close
    
End Function

Private Function fInsertColPlaca(numCol%, cTecAsist%, estado%) As Boolean
'Se a�ade la colonia a la tabla colonia-placa

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery
    
    On Error Resume Next
    
    SQL = "INSERT INTO MB3000(nRef,MB27_codCol,MB20_codTecAsist,MB37_codEstPlaca,MB30_fecDesar)"
    SQL = SQL & " VALUES (?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
    Set qryCol = objApp.rdoConnect.CreateQuery("InsertColPlaca", SQL)
    
    qryCol(0) = nReferencia
    qryCol(1) = numCol
    qryCol(2) = cTecAsist
    qryCol(3) = estado
    qryCol(4) = fFechaHoraActual
    qryCol.Execute
    
    If Err = 0 Then fInsertColPlaca = True
    
    qryCol.Close
    
End Function


Private Function fInsertMorfologias(cMorf%, nLoc%, cMuestra$) As Boolean
'Se a�ade UNA morfolog�a a la tabla Resultados-Muestra

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryMorf As rdoQuery
    
    fInsertMorfologias = False
    
    On Error Resume Next
    If nLoc = 0 Then
        SQL = "INSERT INTO MB2100(MB21_numLoc,nRef,cMuestra,MB12_codMorf)"
        SQL = SQL & " VALUES(?,?,?,?)"
    Else
        SQL = "UPDATE MB2100 "
        SQL = SQL & " SET MB21_numLoc = ?"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND cMuestra = ?"
        SQL = SQL & " AND MB12_codMorf = ?"
    End If
    Set qryMorf = objApp.rdoConnect.CreateQuery("InsertUpdateMorf", SQL)
    
    qryMorf(0) = nLoc + 1
    qryMorf(1) = nReferencia
    qryMorf(2) = cMuestra
    qryMorf(3) = cMorf
    qryMorf.Execute
    
    If Err = 0 Then
        fInsertMorfologias = True
    End If
    
    qryMorf.Close
    
End Function


Private Function fComprobarMicroRepetido(cCol, cMicro%) As Integer
'Comprueba si el microorganismo que se acaba de identificar se hab�a identificado ya _
en alguna otra colonia. De ser as�, muestra un aviso para que el usuario tome una decisi�n.

'Los valores que puede devolver la funci�n son: _
    0       si el microorganismo no est� repetido _
    -1      si el microorganismo est� repetido y se quiere cancelar la identificaci�n _
    cCol    si el microorganimso est� repetido y se quiere aceptar la identificaci�n

'En este �ltimo caso, el c�digo de la colonia devuelto es el de la colonia en la que ya _
    se hab�a identificado el microorganismo. Posteriormente se emplear� para realizar las _
    operaciones necesarias al identificar la colonia (ver pResultColonias)
    
    Dim SQL As String
    Dim rsMicroRep As rdoResultset, qryMicroRep As rdoQuery
    
    SQL = "SELECT MB27_codCol, MB18_desig"
    SQL = SQL & " FROM MB2700, MB1800"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB2700.MB18_codMicro = ?"
    SQL = SQL & " AND MB27_codCol <> ?"
    SQL = SQL & " AND MB36_codEstCol <> ?"
    SQL = SQL & " AND MB2700.MB18_codMicro = MB1800.MB18_codMicro"
    Set qryMicroRep = objApp.rdoConnect.CreateQuery("SelColonias", SQL)
    
    qryMicroRep(0) = nReferencia
    qryMicroRep(1) = cMicro
    qryMicroRep(2) = cCol
    qryMicroRep(3) = constCOLANULADA
    Set rsMicroRep = qryMicroRep.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsMicroRep.EOF = False Then
        SQL = "El microorganismo '" & rsMicroRep(1) & "' ya ha sido identificado en la"
        SQL = SQL & " colonia n� " & rsMicroRep(0) & "."
        If MsgBox(SQL, vbInformation + vbOKCancel, "Identificaci�n de microorganismos") = vbOK Then
            fComprobarMicroRepetido = rsMicroRep(0)
        Else
            fComprobarMicroRepetido = -1
        End If
    Else
        fComprobarMicroRepetido = 0
    End If
    
    rsMicroRep.Close
    qryMicroRep.Close
    
End Function

Private Function fMorfologiasDelete(cMorf%, cMuestra$) As Boolean
'Se eliminan todas las localizaciones de una morfolog�a de la tabla Resultados-Muestra

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryMorf As rdoQuery
    
    On Error Resume Next
    SQL = "DELETE FROM MB2100"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND cMuestra = ?"
    SQL = SQL & " AND MB12_codMorf = ?"
    Set qryMorf = objApp.rdoConnect.CreateQuery("DeleteMorfo", SQL)
    
    qryMorf(0) = nReferencia
    qryMorf(1) = cMuestra
    qryMorf(2) = cMorf
    
    qryMorf.Execute
    
    If Err = 0 Then
        fMorfologiasDelete = True
    End If
    
    qryMorf.Close
    
End Function

Private Function fMorfologiasInsert(cMorf%, nLoc%, cMuestra$) As Boolean
'se a�aden morfolog�as en la tabla Resultado-Muestra

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryMorf As rdoQuery
    
    On Error Resume Next
    SQL = "INSERT INTO MB2100(MB21_numLoc,nRef,cMuestra,MB12_codMorf)"
    SQL = SQL & " VALUES(?,?,?,?)"
    Set qryMorf = objApp.rdoConnect.CreateQuery("InsertMorf", SQL)
    
    qryMorf(0) = nLoc
    qryMorf(1) = nReferencia
    qryMorf(2) = cMuestra
    qryMorf(3) = cMorf
    qryMorf.Execute
    
    If Err = 0 Then
        fMorfologiasInsert = True
    End If
    
    qryMorf.Close
    
End Function
Private Function fMorfologiasUpdate(cMorf%, nLoc%, cMuestra$) As Boolean
'se modifica el n� de morfolog�as en la tabla Resultado-Muestra

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryMorf As rdoQuery
    
    On Error Resume Next
    SQL = "UPDATE MB2100 "
    SQL = SQL & " SET MB21_numLoc = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND cMuestra = ?"
    SQL = SQL & " AND MB12_codMorf = ?"
    Set qryMorf = objApp.rdoConnect.CreateQuery("InsertUpdateMorf", SQL)
    
    qryMorf(0) = nLoc
    qryMorf(1) = nReferencia
    qryMorf(2) = cMuestra
    qryMorf(3) = cMorf
    qryMorf.Execute
    
    If Err = 0 Then
        fMorfologiasUpdate = True
    End If
    
    qryMorf.Close
    
End Function

Private Function fPosibleAplicarAntibiotico(cCol%, cAnti%) As Boolean
'Determina si un antibi�tico puede ser a�adido a un antibiograma: para ello se tiene en _
cuenta que si el microorganismo est� identificado plenamente y ya se le ha hecho alg�n _
antibiograma con ese antibi�tico no se podr� a�adir el antibi�tico de nuevo.

    Dim SQL As String
    Dim rsSelAntib As rdoResultset, qrySelAntib As rdoQuery
    
    SQL = "SELECT MB20_codTecAsist"
    SQL = SQL & " FROM MB2900, MB2700"
    SQL = SQL & " WHERE MB2900.nRef = ?"
    SQL = SQL & " AND MB2900.MB27_codCol = ?"
    SQL = SQL & " AND MB2900.MB07_codAnti = ?"
    SQL = SQL & " AND MB2700.nRef = MB2900.nRef"
    SQL = SQL & " AND MB2700.MB27_codCol = MB2900.MB27_codCol"
    SQL = SQL & " AND MB2700.MB18_codMicro IS NOT NULL"
    SQL = SQL & " AND MB2700.MB36_codEstCol <> ?"
    Set qrySelAntib = objApp.rdoConnect.CreateQuery("PosibleAplicarAntibiotico", SQL)
        
    qrySelAntib(0) = nReferencia
    qrySelAntib(1) = cCol
    qrySelAntib(2) = cAnti
    qrySelAntib(3) = constCOLANULADA
    Set rsSelAntib = qrySelAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsSelAntib.EOF = True Then
        fPosibleAplicarAntibiotico = True
    End If
   
    rsSelAntib.Close
    qrySelAntib.Close
    
End Function

Private Function fReincubar(numRef$, codTecAsist%, codPaso%, codCond%) As Boolean
'Anota en la base da datos la reincubaci�n de un cultivo. Se anota la salida de las _
    condiciones ambientales anteriores, se anota la entrada en las nuevas condiciones _
    ambientales y se actualiza la tabla MB2000 (t�cnica-asistencia) para indicar la _
    reincubaci�n

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryReincubar As rdoQuery
    
    fReincubar = False
    
    'Se anota la salida de las condiciones ambientales anteriores
    If fUpdateSeguiTec(CStr(codTecAsist)) = False Then
        Exit Function
    End If
    
    On Error Resume Next
    
    'Se anota la entrada en las nuevas condiciones ambientales. La fecha de entrada se _
    pone al sacar la hoja de trabajo
    SQL = "INSERT INTO MB2600 (NREF,MB20_CODTECASIST,MB26_CODPASO,MB02_CODCOND,MB35_CODESTSEGUI)"
    SQL = SQL & " VALUES (?,?,?,?,?)"
    Set qryReincubar = objApp.rdoConnect.CreateQuery("EntradaCondAmb", SQL)
        
    qryReincubar(0) = numRef
    qryReincubar(1) = codTecAsist
    qryReincubar(2) = codPaso + 1
    qryReincubar(3) = codCond
    qryReincubar(4) = constSEGUITECVALIDA
    qryReincubar.Execute
    
    If Err <> 0 Then
        qryReincubar.Close
        Exit Function
    End If
    
    'Se actualiza el estado de la tabla T�cnica-Asistencia para poder sacar las nuevas hojas _
    'de trabajo
    SQL = "UPDATE MB2000"
    SQL = SQL & " SET MB20_fecLect = ''"
    SQL = SQL & ", MB20_fecReali = ''"
    SQL = SQL & ", MB34_codEstTecAsist = ?"
    SQL = SQL & ", MB20_indSiembra = 0"
    SQL = SQL & " WHERE nRef =?"
    SQL = SQL & " AND MB20_codTecAsist= ?"
    Set qryReincubar = objApp.rdoConnect.CreateQuery("EstadoTecAsist", SQL)
    
    qryReincubar(0) = constTECREINCUBADA
    qryReincubar(1) = numRef
    qryReincubar(2) = codTecAsist
    qryReincubar.Execute
    
    If Err = 0 Then
        fReincubar = True
    End If
    
    qryReincubar.Close
    
End Function

Private Function fComprobarResultadoMismaTec(cCol%, cTec%, cResult%, result$, cTecAsist%) As Boolean
'Comprueba que el resultado de la t�cnica realizada sobre la colonia est� de acuerdo _
    con otros posibles resultados obtenidos anteriormente de la misma t�cnica sobre la _
    misma colonia

'En el caso de detectar desviaciones, muestra un aviso para que el usuario tome una decisi�n _
    al respecto

'Los valores que devuelve la funci�n son: _
    True    si no hay desviaciones o el usuario no las quiere corregir _
    False   si hay desviaciones y el usuario las quiere corregir
    Dim SQL As String
    Dim rsCompResTec As rdoResultset, qryCompResTec As rdoQuery
    
    fComprobarResultadoMismaTec = False
    
    'se compara el resultado obtenido con el esperado
    SQL = "SELECT DISTINCT MB33_result, MB3300.cUnidad"
    SQL = SQL & ", MB0900.MB09_desig, MB3200.MB32_desig"
    SQL = SQL & " FROM MB3300, MB2000, MB0900, MB3200"
    SQL = SQL & " WHERE MB3300.nRef = ?"
    SQL = SQL & " AND MB3300.MB09_codTec = ?"
    SQL = SQL & " AND MB3300.MB32_codResult = ?"
    SQL = SQL & " AND MB33_result <> ?"
    SQL = SQL & " AND MB38_codEstResult <> ?"
    SQL = SQL & " AND MB3300.MB20_codTecAsist <> ?"
    SQL = SQL & " AND MB2000.MB20_codTecAsist = MB3300.MB20_codTecAsist"
    SQL = SQL & " AND MB2000.nRef = MB3300.nRef"
    SQL = SQL & " AND MB2000.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB20_tiOrig = ?"
    SQL = SQL & " AND MB20_codOrig = ?"
    SQL = SQL & " AND MB34_codEstTecAsist <> ?"
    SQL = SQL & " AND MB0900.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB3200.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB3200.MB32_codResult = MB3300.MB32_codResult"
    Set qryCompResTec = objApp.rdoConnect.CreateQuery("CompResTec", SQL)
    
    qryCompResTec(0) = nReferencia
    qryCompResTec(1) = cTec
    qryCompResTec(2) = cResult
    qryCompResTec(3) = result
    qryCompResTec(4) = constRESULTANULADO
    qryCompResTec(5) = cTecAsist
    qryCompResTec(6) = constORIGENCOLONIA
    qryCompResTec(7) = cCol
    qryCompResTec(8) = constTECANULADA
    Set rsCompResTec = qryCompResTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsCompResTec.EOF = False Then
        SQL = "Se han encontrado valores distintos del resultado '" & rsCompResTec(3) & "'"
        SQL = SQL & " de la t�cnica '" & rsCompResTec(2) & "' para la Colonia " & cCol & ":"
        Do While rsCompResTec.EOF = False
            SQL = SQL & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
            SQL = SQL & " * Resultado: " & rsCompResTec(0)
            rsCompResTec.MoveNext
        Loop
        If MsgBox(SQL, vbInformation + vbOKCancel, "Comprobaci�n de resultados") = vbOK Then
            fComprobarResultadoMismaTec = True
        End If
    Else
        fComprobarResultadoMismaTec = True
    End If
    
    rsCompResTec.Close
    qryCompResTec.Close
    
End Function

Private Function fUpdateIdentifCol(estado%, cGeMicro%, cMorf%, cMicro%, CodCol%) As Boolean
'Se anota la identificaci�n de la colonia en cualquiera de sus tres niveles: microorganismo, _
    g�nero y morfolog�a

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryIdentifCol As rdoQuery
    
    fUpdateIdentifCol = False
    
    On Error Resume Next
    
    SQL = "UPDATE MB2700"
    SQL = SQL & " SET MB36_codEstCol = ?"
    SQL = SQL & ", MB11_codGeMicro = ?"
    SQL = SQL & ", MB12_codMorf = ?"
    SQL = SQL & ", MB18_codMicro = ?"
    SQL = SQL & ", MB27_fecIdent = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    Select Case estado
    Case constCOLVALIDADA
        SQL = SQL & ", cUser_Val = ?"
    Case constCOLINFORMADA
        SQL = SQL & ", cUser_Inf = ?"
    End Select
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    Set qryIdentifCol = objApp.rdoConnect.CreateQuery("UpdateCol", SQL)
        
    qryIdentifCol(0) = estado
    If cGeMicro <> -1 Then qryIdentifCol(1) = cGeMicro Else qryIdentifCol(1) = Null
    If cMorf <> -1 Then qryIdentifCol(2) = cMorf Else qryIdentifCol(2) = Null
    If cMicro <> -1 Then qryIdentifCol(3) = cMicro Else qryIdentifCol(3) = Null
    qryIdentifCol(4) = fFechaHoraActual()
    If estado = constCOLANULADA Then
        qryIdentifCol(5) = nReferencia
        qryIdentifCol(6) = CodCol
    Else
        qryIdentifCol(5) = cUserPeticion
        qryIdentifCol(6) = nReferencia
        qryIdentifCol(7) = CodCol
    End If
    qryIdentifCol.Execute
    
    If Err = 0 Then
        fUpdateIdentifCol = True
    End If
    
    qryIdentifCol.Close
    
End Function

Private Function fUpdateNumCol_Antibiograma(cColOld%, cColNew%, cTecAsist%) As Boolean
'Cambia el n� de colonia en un antibiograma

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery
    
    On Error Resume Next
    
    SQL = "UPDATE MB2900"
    SQL = SQL & " SET MB27_codCol = ?"
    SQL = SQL & " WHERE (nRef, MB20_codTecAsist, MB27_codCol) IN("
    SQL = SQL & "           SELECT nRef, MB20_codTecAsist, MB20_codOrig"
    SQL = SQL & "           FROM MB2000"
    SQL = SQL & "           WHERE nRef = ?"
    SQL = SQL & "           AND MB20_tiOrig = ?"
    SQL = SQL & "           AND MB20_codOrig = ?"
    SQL = SQL & "           AND MB20_codTecAsist_Orig = ?)"
    Set qryCol = objApp.rdoConnect.CreateQuery("UpdateNumColAntibiograma", SQL)
    
    qryCol(0) = cColNew
    qryCol(1) = nReferencia
    qryCol(2) = constORIGENCOLONIA
    qryCol(3) = cColOld
    qryCol(4) = cTecAsist
    qryCol.Execute
    
    If Err = 0 Then fUpdateNumCol_Antibiograma = True
    
    qryCol.Close
    
End Function

Private Function fUpdateNumCol_Placa(cColOld%, cColNew%, cTecAsist%) As Boolean
'Cambia el n� de colonia en una placa

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery
    
    On Error Resume Next
    
    SQL = "UPDATE MB3000"
    SQL = SQL & " SET MB27_codCol = ?,"
    SQL = SQL & " MB37_codEstPlaca = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    Set qryCol = objApp.rdoConnect.CreateQuery("UpdateNumColPlaca", SQL)
    
    qryCol(0) = cColNew
    qryCol(1) = constCOLPLACAASOCIADA
    qryCol(2) = nReferencia
    qryCol(3) = cTecAsist
    qryCol(4) = cColOld
    qryCol.Execute
    
    If Err = 0 Then fUpdateNumCol_Placa = True
    
    qryCol.Close
    
End Function

Private Function fUpdateNumCol_TecAsist(cColOld%, cColNew%, cTecAsist%) As Boolean
'Cambia el n� de colonia (c�digo de origen) en T�cnica-Asistencia

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery
    
    On Error Resume Next
    
    SQL = "UPDATE MB2000"
    SQL = SQL & " SET MB20_codOrig = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_tiOrig = ?"
    SQL = SQL & " AND MB20_codOrig = ?"
    SQL = SQL & " AND MB20_codTecAsist_Orig = ?"
    Set qryCol = objApp.rdoConnect.CreateQuery("UpdateNumColTecAsist", SQL)
    
    qryCol(0) = cColNew
    qryCol(1) = nReferencia
    qryCol(2) = constORIGENCOLONIA
    qryCol(3) = cColOld
    qryCol(4) = cTecAsist
    qryCol.Execute
    
    If Err = 0 Then fUpdateNumCol_TecAsist = True
    
    qryCol.Close
    
End Function
Private Function fUpdateResultAntibioticos(cMicro%, codReac%, CodCol%, codTecAsist%, codAntibiotico%, concentracion$, recom%, Optional infConc%) As Boolean
'Se anotan los datos del antibiotico en el antibiograma

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryResultAntib As rdoQuery
    
    fUpdateResultAntibioticos = False
    
    On Error Resume Next
    
    SQL = "UPDATE MB2900"
    SQL = SQL & " SET MB40_codReac = ?"
    SQL = SQL & " ,MB29_conc = ?"
    SQL = SQL & " ,MB29_recom = ?"
    SQL = SQL & " ,MB29_indInf_Conc = ?"
    SQL = SQL & " ,cUser_Val = ?"
    SQL = SQL & " ,cUser_Inf = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB07_codAnti = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    If cMicro = -1 Then
        SQL = SQL & " AND MB20_codTecAsist = ?"
    End If
    Set qryResultAntib = objApp.rdoConnect.CreateQuery("UpdateAntibiograma", SQL)
        
    If codReac <> -1 Then
        qryResultAntib(0) = codReac
    Else
        qryResultAntib(0) = Null
    End If
    qryResultAntib(1) = concentracion
    qryResultAntib(2) = recom
    qryResultAntib(3) = infConc
    qryResultAntib(4) = cUserPeticion
    qryResultAntib(5) = cUserPeticion
    qryResultAntib(6) = nReferencia
    qryResultAntib(7) = codAntibiotico
    qryResultAntib(8) = CodCol
    If cMicro = -1 Then
        qryResultAntib(9) = codTecAsist
    End If
    qryResultAntib.Execute
    
    If Err = 0 Then
        fUpdateResultAntibioticos = True
    End If
    
    qryResultAntib.Close
    
End Function

Private Function fValidaAntibiograma(cTecAsist%) As Boolean
'La t�cnica de antibiograma quedar� validada si tiene al menos introducido un resultado _
    de antibi�tico

'La funci�n devuelve True si la t�cnica de antibiograma debe quedar validada y False en _
    caso contrario
    
    Dim SQL As String
    Dim rsAntibVal As rdoResultset, qryAntibVal As rdoQuery
    
    fValidaAntibiograma = False
    
    SQL = "SELECT COUNT(*) FROM MB2900"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB40_codReac IS NOT NULL"
    Set qryAntibVal = objApp.rdoConnect.CreateQuery("AntibVal", SQL)
    
    qryAntibVal(0) = nReferencia
    qryAntibVal(1) = cTecAsist
    Set rsAntibVal = qryAntibVal.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsAntibVal(0) > 0 Then
        fValidaAntibiograma = True
    End If
    
    rsAntibVal.Close
    qryAntibVal.Close
    
End Function

Private Function fValidaAntibiograma2(cTecAsist%) As Boolean
' se comprueba si el antibiograma tiene introducidos todos los resultados de los antibi�ticos
    Dim SQL As String
    Dim rsAntibVal As rdoResultset, qryAntibVal As rdoQuery
    
    fValidaAntibiograma2 = False
    
    ' se buscan los antibi�ticos del antibiograma para los cuales no se han insertado todav�a _
    los resultados
    SQL = "SELECT COUNT(*) FROM MB2900"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB40_codReac IS NULL"
    Set qryAntibVal = objApp.rdoConnect.CreateQuery("AntibVal", SQL)
    
    qryAntibVal(0) = nReferencia
    qryAntibVal(1) = cTecAsist
    Set rsAntibVal = qryAntibVal.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsAntibVal(0) = 0 Then
        fValidaAntibiograma2 = True
    End If
    
    rsAntibVal.Close
    qryAntibVal.Close
    
End Function


Private Function fValidaTecnica(cTecAsist%, cTec%) As Integer
'Devuelve el estado de una t�cnica: anulada, validada o solicitada en funci�n del estado _
    de los resultados de la misma.

'La funci�n devuelve los siguientes valores: _
    * 0                 - si la t�cnica est� solicitada (al menos alg�n resultado como _
    solicitado y ninguno validado; el resto pueden estar anulados) _
    * constTECVALIDADA  - si la t�cnica est� validada (al menos un resultado como validado) _
    * constTECANULADA   - si la t�cnica est� anulada (todos los resultados anulados)
    
    Dim SQL As String
    Dim rsTecVal As rdoResultset, qryTecVal As rdoQuery
    
    ' resultados insertados
    SQL = "SELECT DISTINCT MB38_codEstResult FROM MB3300"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB09_codTec = ?"
    SQL = SQL & " ORDER BY MB38_codEstResult"
    Set qryTecVal = objApp.rdoConnect.CreateQuery("TecVal", SQL)
    
    qryTecVal(0) = nReferencia
    qryTecVal(1) = cTecAsist
    qryTecVal(2) = cTec
    Set rsTecVal = qryTecVal.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
'    ESTE C�DIGO ES PARA CUANDO LA T�CNICA SE CONSIDERA VALIDADA CUANDO TIENE TODOS LOS _
'    RESULTADOS INTRODUCIDOS
'    Do While rsTecVal.EOF = False
'        If rsTecVal(0) = constRESULTSOLICITADO Then
'            fValidaTecnica = 0 't�cnica sin validar ni anular
'            Exit Function
'        End If
'        If rsTecVal(0) <> constRESULTANULADO Then
'            fValidaTecnica = constTECVALIDADA 't�cnica validada
'        End If
'        rsTecVal.MoveNext
'    Loop
'
'    If fValidaTecnica = constTECVALIDADA Then
'        Exit Function
'    Else
'        fValidaTecnica = constTECANULADA 't�cnica anulada
'    End If

'    SE CONSIDERA LA T�CNICA VALIDADA CUANDO TIENE ALG�N RESULTADO VALIDADO O INFORMADO
'    SE CONSIDERA LA T�CNICA ANULADA CUANDO TODOS LOS RESULTADOS EST�N ANULADOS
    If rsTecVal.EOF = False Then
        fValidaTecnica = constTECANULADA
        Do While rsTecVal.EOF = False
            Select Case rsTecVal(0)
                Case constRESULTVALIDADO, constRESULTINFORMADO
                    fValidaTecnica = constTECVALIDADA
                    Exit Function
                Case constRESULTSOLICITADO
                    fValidaTecnica = 0
            End Select
            rsTecVal.MoveNext
        Loop
    End If
    
    rsTecVal.Close
    qryTecVal.Close
    
End Function



Private Sub pBotonLista(blnMaint As Boolean, intMantenimiento%)
'Controla la visibilidad del bot�n de acceso a mantenimientos

    If fAcceso(constMANTENIMIENTOPRUEBAS) = False Then
        blnMaint = False
        intMantenimiento = constMANTNINGUNO
    End If
    With stbStatusBar1.Panels(LoadResString(cwMsgStbPanForeign))
        If blnMaint = False Then
            Set .Picture = Nothing
            .Text = ""
        Else
            Set .Picture = objApp.imlImageList.ListImages(constcwImageForeign).Picture
            .Text = LoadResString(cwMsgStbPanForeignMsg)
        End If
        .Enabled = blnMaint
        .Bevel = IIf(blnMaint, sbrRaised, sbrInset)
        accesoMantenimiento = intMantenimiento
    End With
        
End Sub

Private Sub pBuscarIdentifCol(cCol%, cMicro%, cGeMicro%, cMorf%)
'Busca la identificaci�n de la colonia

'La subrutina devuelve los c�digos del microorganismo, g�nero y morfolog�a de la colonia _
    si est� identificada en esos tres niveles o -1 en el nivel en que no est� identificada
    
    Dim SQL As String
    Dim rsColIdent As rdoResultset, qryColIdent As rdoQuery
    
    SQL = "SELECT nvl(MB18_codMicro,-1), nvl(MB11_codGeMicro,-1), nvl(MB12_codMorf,-1)"
    SQL = SQL & " FROM MB2700"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    Set qryColIdent = objApp.rdoConnect.CreateQuery("CompResMicro", SQL)
        
    qryColIdent(0) = nReferencia
    qryColIdent(1) = cCol
    Set rsColIdent = qryColIdent.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    cMicro = rsColIdent(0)
    cGeMicro = rsColIdent(1)
    cMorf = rsColIdent(2)
    
    rsColIdent.Close
    qryColIdent.Close
    
End Sub

Private Sub pComprobarResultadoAntibiotico(codTecAsist%, CodCol%, codAntibiotico%, cReacc%, concentr$, recomend%, desigReacc$, msg0$)
'Detecta si la sensibilidad se�alada para el microorganismo frente al antibi�tico es _
    la misma que se ha podidido se�alar antes con otra t�cnica (otro codTecAsist)
    
'Si la sensibilidad es distinta que la se�alada anteriormente, la subrutina devuelve los _
    valores de reaccion, concentraci�n y recomendado existentes anteriormente, as� como un _
    mensaje que avisa de la desviaci�n
   
    Dim SQL As String
    Dim rsCompResMicro As rdoResultset, qryCompResMicro As rdoQuery
    
    ' se buscan resultados distintos del que se va a introducir
    SQL = "SELECT MB2900.MB40_codReac, MB29_conc, MB29_recom, MB40_desig"
    SQL = SQL & " FROM MB2900,MB4000"
    SQL = SQL & " WHERE MB2900.nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist <> ?"
    SQL = SQL & " AND MB07_codAnti = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB2900.MB40_codReac <> ?"
    SQL = SQL & " AND MB4000.MB40_codReac = MB2900.MB40_codReac"
    Set qryCompResMicro = objApp.rdoConnect.CreateQuery("CompResMicro", SQL)
    
    qryCompResMicro(0) = nReferencia
    qryCompResMicro(1) = codTecAsist
    qryCompResMicro(2) = codAntibiotico
    qryCompResMicro(3) = CodCol
    qryCompResMicro(4) = cReacc
    Set rsCompResMicro = qryCompResMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsCompResMicro.EOF = False Then
        'se anotan los valores del resultado guardado anteriormente por si se prefieren _
        poner estos
        cReacc = rsCompResMicro(0)
        If IsNull(rsCompResMicro(1)) Then
            concentr = ""
        Else
            concentr = rsCompResMicro(1)
        End If
        recomend = rsCompResMicro(2)
        desigReacc = rsCompResMicro(3)
        msg0 = "La sensibilidad del microorganismo frente al antibi�tico ya fue establecida"
        msg0 = msg0 & " anteriormente como '" & rsCompResMicro(3) & "'." & Chr$(13) & Chr$(10)
        msg0 = msg0 & "�Desea Ud. reemplazar el resultado anterior por el nuevo?"
    End If
    
    rsCompResMicro.Close
    qryCompResMicro.Close
    
End Sub

Private Function fComprobarRelacionAntibioticos(codMicro%, CodCol%, codAntibiotico%, codReaccion%, Optional reacStandar$, Optional reacIncompat%) As String
'Compara la sensibilidad de un microorganismo frente a un antibi�tico en relaci�n con _
    la sensibilidad frente a otros antibi�ticos definida en el maestro MB2300 _
    (relaci�n antibi�ticos) y devuelve un mensaje si ha detectado desviaciones.

'Adem�s, tambi�n actualiza las variables reacStandar y reacIncompat seg�n lo comentado _
    posteriormente
    
    Dim SQL As String
    Dim rsRelAntib As rdoResultset, qryRelAntib As rdoQuery
   
    'se buscan las relaciones entre sensibilidades de antibi�ticos para el antibi�tico _
    y el microorganismo que se procesa
    SQL = "SELECT MB2300.MB40_codReac_2, MB40_desig, MB07A.MB07_desig, MB07B.MB07_desig"
    SQL = SQL & " FROM MB2900,MB2300,MB4000,MB0700 MB07A,MB0700 MB07B"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB07_codAnti_1 = MB2900.MB07_codAnti"
    SQL = SQL & " AND MB40_codReac_1 = MB2900.MB40_codReac"
    SQL = SQL & " AND MB07_codAnti_2 = ?"
    SQL = SQL & " AND (MB18_codMicro = ? OR MB18_codMicro IS NULL)" 'se el campo es nulo, afecta a todos los microorganismos
    SQL = SQL & " AND MB23_indActiva = -1"
    SQL = SQL & " AND MB07A.MB07_codAnti = MB07_codAnti_1"
    SQL = SQL & " AND MB07B.MB07_codAnti = MB07_codAnti_2"
    SQL = SQL & " AND MB4000.MB40_codReac = MB40_codReac_2"
    Set qryRelAntib = objApp.rdoConnect.CreateQuery("RelAntib", SQL)
        
    qryRelAntib(0) = nReferencia
    qryRelAntib(1) = CodCol
    qryRelAntib(2) = codAntibiotico
    qryRelAntib(3) = codMicro
    Set rsRelAntib = qryRelAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            
    SQL = ""
    On Error Resume Next
    'NOTA: Las variables opcionales solo afectan cuando la llamada se ha hecho desde la funci�n _
    fComprobarAntibiograma. En la variable reacStandar quedar� anotado el c�digo y designaci�n _
    de la reacci�n esperada para el antibi�tico (por si luego se quiere cambiar). En _
    la reacIncompat quedar� el c�digo de la reacci�n esperada y si ha habido incompatibilidad _
    en los registros de la tablas MB2300 y/o MB2400 se anotar� un -1.
    Do While rsRelAntib.EOF = False
        If rsRelAntib(0) <> codReaccion Then
            If reacStandar = "" Then
                reacStandar = rsRelAntib(0) & "-" & rsRelAntib(1)
            End If
            SQL = SQL & Chr$(13) & Chr$(10) & "  - De la relaci�n con '" & rsRelAntib(2) & "' para el antibi�tico '" & rsRelAntib(3) & "' la sensibilidad esperada es '" & rsRelAntib(1) & "'."
        End If
        If reacIncompat <> 0 Then 'ya exist�a un valor de reacci�n anterior
            If reacIncompat <> rsRelAntib(0) Then 'el nuevo valor es distinto
                reacIncompat = -1 'incompatibilidad
            End If
        Else
            'asigna el valor del c�digo de reacci�n est�ndar a la variable reacIncompat. _
            Posteriormente, al realizar la otra comprobaci�n (fComprobarBacteriaResistente) _
            o en un paso posterior en este mismo bucle, si el nuevo valor est�ndar _
            encontrado no corresponde con el de reacIncompat se habr� detectado una _
            incompatibilidad en los valores de los maestros
            reacIncompat = rsRelAntib(0)
        End If
        rsRelAntib.MoveNext
    Loop
    fComprobarRelacionAntibioticos = SQL
    
    rsRelAntib.Close
    qryRelAntib.Close
    
End Function

Private Function fInsertAntibioticos(nodoEn As Node, nodoDesde As Node, arAnti() As typeAntibioticos, Optional cGrAntib%) As Integer
'Inserta un antibi�tico en el antibograma de una colonia

'La funci�n devuelve los siguientes valores: _
    0 - la operaci�n se ha realizado correctamente o no ha sido necesario realizarla _
    1 - el antibi�tico no se a�ade por existir ya en otro antibiograma y estar la colonia identificada _
    2 - se produjo un error al realizar las operaciones sobre la base de datos
'Adem�s, tambi�n se devuelve un matriz que contiene datos del antibi�tico que se ha a�adido

    Dim SQL As String, nodo As Node
    Dim qryAnti As rdoQuery
    Dim i%, dimension%
    Dim cCol%, cAnti%, cTecAsist%
    
    fInsertAntibioticos = 0 'El antibi�tico ya existe en el antibiograma o ha sido a�adido
        
    'Se mira si el antibiotico existe ya en el antibiograma
    For i = 1 To nodoEn.Children
        If i = 1 Then
            Set nodo = nodoEn.Child
        Else
            Set nodo = nodo.Next
        End If
        SQL = Left$(nodoDesde.Key, InStr(nodoDesde.Key, "G") - 1)
        If Left$(nodo.Key, InStr(nodo.Key, "C") - 1) = SQL Then
            Exit Function
        End If
    Next i
    
    'se obtiene el c�digo de la colonia y del antibi�tico
    cCol = Val(Mid$(nodoEn.Parent.Key, 2))
    cAnti = Val(Mid$(nodoDesde.Key, 2))
    cTecAsist = Val(Mid$(nodoEn.Key, 2))
    
    'se comprueba si el antibi�tico puede ser a�adido al antibiograma
    If fPosibleAplicarAntibiotico(cCol, cAnti) = False Then
        fInsertAntibioticos = 1 'El antibi�tico existe en alg�n antibiograma del microrg. ya identificado
        Exit Function
    End If
    
    'se genera el registro en la base de datos
    On Error Resume Next
    If cGrAntib = 0 Then
        SQL = "INSERT INTO MB2900 (nRef,MB27_codCol,MB07_codAnti,MB20_codTecAsist)"
        SQL = SQL & " VALUES (?,?,?,?)"
    Else
        SQL = "INSERT INTO MB2900 (nRef,MB27_codCol,MB07_codAnti,MB20_codTecAsist,MB31_codGrAnti)"
        SQL = SQL & " VALUES (?,?,?,?,?)"
    End If
    Set qryAnti = objApp.rdoConnect.CreateQuery("InsertAntibiotico", SQL)
    
    qryAnti(0) = nReferencia
    qryAnti(1) = cCol
    qryAnti(2) = cAnti
    qryAnti(3) = cTecAsist
    If cGrAntib > 0 Then
        qryAnti(4) = cGrAntib
    End If
    qryAnti.Execute
    
    If Err = 0 Then
        dimension = UBound(arAnti)
        dimension = dimension + 1
        ReDim Preserve arAnti(1 To dimension)
        arAnti(dimension).cAnti = cAnti
        arAnti(dimension).desig = nodoDesde.Text
    Else
        fInsertAntibioticos = 2 'El antibi�tico no se ha podido a�adir
    End If
    
    qryAnti.Close
        
End Function

Private Function fInsertGrAntibioticos(nodoEn As Node, nodoDesde As Node, arAnti() As typeAntibioticos) As Integer
'A�ade un grupo de antibi�ticos al antibiograma de una colonia mediante llamadas a la funci�n _
    fInsertarAntibioticos

'La funci�n devuelve los siguientes valores: _
    0 - la operaci�n se ha realizado correctamente _
    1 - alg�n antibi�tico no se a�ade por existir ya en otro antibiograma y estar la colonia identificada _
    2 - se produjo un error al realizar las operaciones sobre la base de datos

    Dim nodo As Node, i As Integer
    Dim res%
    Dim cGrAntib As Integer
    
    fInsertGrAntibioticos = 0 'grupo de antibi�ticos a�adio correctamente
    
    'se obtiene el grupo que se a�ade para anotarlo en MB2900
    cGrAntib = Val(Mid$(nodoDesde.Key, 2))
    
    For i = 1 To nodoDesde.Children
        If i = 1 Then
            Set nodo = nodoDesde.Child
        Else
            Set nodo = nodo.Next
        End If
        res = fInsertAntibioticos(nodoEn, nodo, arAnti(), cGrAntib)
        Select Case res
            Case 1
                fInsertGrAntibioticos = res 'antibi�tico no a�adido por estar en otro antibiograma del mismo microorg.
            Case 2
                fInsertGrAntibioticos = res 'error al a�adir antibi�ticos
                Exit Function
        End Select
    Next i
    
End Function


Private Sub pChangeNumColPlaca(nodoEn As Node, nodoDesde As Node)
'Cambia el n� de colonia en una placa

'********************************************************************
'* Casos en los que no se puede realizar la sustituci�n:            *
'* 1) Si la colonia a sustituir y la sustituta son la misma         *
'* 2) Si la colonia a sustituir est� anulada                        *
'* 3) Si la colonia a sustituir est� identificada                   *
'* 4) Si la colonia a sustituir est� localizada en una sola placa   *
'*        y se intenta cambiar por un n�meo nuevo -->(INACTIVO)     *
'* 5) Si la colonia sustituta est� anulada                          *
'* 6) Si la colonia sustituta ya existe en la placa                 *
'********************************************************************

    Dim cTecAsist% 'datos del cultivo
    Dim cColNew%, identifCol$, estado% 'datos de la colonia nueva
    Dim cColOld% 'datos de la colonia antig�a
    Dim cMicro%, cGeMicro%, cMorf% 'identificaci�n de la colonia
    Dim nodoExisteKey$
    Dim msg$
    
    'NOTA: HAY QUE VER QUE SUCEDE SI LA COLONIA SUSTITUIDA YA NO SE ENCUENTRA EN NINGUNA _
    PLACA. VER QUE HACER EN EL TVWCOLONIAS
    
    
    cColNew = Val(Mid$(nodoDesde.Key, 2)) 'ser� 0 si es Colonia Nueva
    cColOld = Val(Mid$(nodoEn.Key, 2))
    cTecAsist = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "C") + 1))
    
    '1)si es la misma colonia o es la colonia nueva no se hace nada
    If cColOld = cColNew Or cColNew = 0 Then
        Exit Sub
    End If
    
    '2)se mira si la colonia est� activa
    If Val(nodoEn.Tag) = constCOLANULADA Then
        MsgBox "La Colonia " & cColOld & " est� anulada. No se puede cambiar el n� de la colonia.", vbExclamation, "Cambiar n� colonia"
        Exit Sub
    End If
    
    '3)se mira si la colonia a sutituir est� identificada
    Call pBuscarIdentifCol(cColOld, cMicro, cGeMicro, cMorf)
    If cMicro <> -1 Or cGeMicro <> -1 Or cMorf <> -1 Then
        MsgBox "La Colonia " & cColOld & " est� identificada. No se puede cambiar el n� de la colonia.", vbExclamation, "Cambiar n� colonia"
        Exit Sub
    End If
    
    '4)chequeo inactivo: no se realiza por ahora
    '5)chequeo realizado al intentar arrastrar la colonia sustituta
    
    'se pregunta para seguir adelante
    If MsgBox("�Desea Ud. cambiar el n� de colonia " & cColOld & " por el " & cColNew & " en esta placa?", vbQuestion + vbYesNo, "Cambiar n� colonia") = vbNo Then
        Exit Sub
    End If
    
    ' se mira si la colonia es nueva o ya existe
    Select Case cColNew
        'Colonia nueva
        Case 0
            'se busca el pr�ximo n� de colonia
            cColNew = fBuscarNumColonia()
            If cColNew = 0 Then 'error
                MsgBox "Se ha producido un error en la b�squeda del n� de colonia.", vbError
                Exit Sub
            End If
            
            objApp.rdoConnect.BeginTrans
            If fChangeNumColPlaca(cColOld, cColNew, cTecAsist, True) = False Then
                objApp.rdoConnect.RollbackTrans
                MsgBox "Se ha producido un error al intentar cambiar el n� de colonia.", vbError
                Exit Sub
            Else
                objApp.rdoConnect.CommitTrans
                'nodo en tvwMuestras
                nodoEn.Text = "Colonia " & cColNew & ": Sin identificar"
                nodoEn.Key = "K" & cColNew & "C" & cTecAsist
                'nodo en tvwColonias
                Call pAddNodoColonia(tvwColonias, cColNew, "", constCOLLOCALIZADA)
                'estado de los botones y frames
                If tvwMuestras.SelectedItem = nodoEn Then
                    Call pDatosColonia(cColNew)
                    estado = fDatosColonia(cColNew, cTecAsist)
                    Call pEstadoBotones("K", estado)
                End If
            End If
            
        Case Else
            'se anota si la colonia ya existe  en la placa y si est� anulada o no
            nodoExisteKey = fExisteColPlaca(nodoEn.Parent, nodoDesde.Key)
            
            '6)si la colonia sustituta ya existe en la placa no se deja cambiar el n� de colonia
            If nodoExisteKey <> "" Then
                If Val(tvwMuestras.Nodes(nodoExisteKey).Tag) = constCOLPLACAANULADA Then
                    msg = "La Colonia " & cColNew & " ya existe en la placa. No se puede"
                    msg = msg & " cambiar el n� de colonia."
                    msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10) & "Recomendaci�n: "
                    msg = msg & "Volver activa la Colonia " & cColNew & " y anular la Colonia "
                    msg = msg & cColOld & "."
                    MsgBox msg, vbExclamation, "Cambiar n� colonia"
                Else
                    msg = "La Colonia " & cColNew & " ya existe en la placa. No se puede"
                    msg = msg & " cambiar el n� de colonia."
                    msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10) & "Recomendaci�n: "
                    msg = msg & "Anular la Colonia " & cColOld & "."
                    MsgBox msg, vbExclamation, "Cambiar n� colonia"
                End If
                Exit Sub
            End If
                
            'se realizan los chequeos de validez de resultados de las t�cnicas
            Call pBuscarIdentifCol(cColNew, cMicro, cGeMicro, cMorf)
            If cMicro <> -1 Or cGeMicro <> -1 Or cMorf <> -1 Then
                If fComprobarResultadoMicro(cColOld, cMicro, cGeMicro, cMorf) = False Then
                    Exit Sub
                End If
            End If
            
            'se realizan los chequeos de validez sobre el antibiograma si ya lo tiene definido
            If cMicro <> -1 Then 'microorganismo identificado completamente
                If fComprobarAntibiograma(cColOld, cMicro) = False Then
                    Exit Sub
                End If
            End If
        
            'se act�a sobre la base de datos
            objApp.rdoConnect.BeginTrans
            If fChangeNumColPlaca(cColOld, cColNew, cTecAsist, False) = False Then
                objApp.rdoConnect.RollbackTrans
                MsgBox "Se ha producido un error al intentar cambiar el n� de colonia.", vbError
                Exit Sub
            Else
                objApp.rdoConnect.CommitTrans
                'nodo en tvwMuestras
                If InStr(nodoDesde.Text, ":") > 0 Then
                    identifCol = Trim$(Mid$(nodoDesde.Text, InStr(nodoDesde.Text, ":")))
                    'identifCol = Trim$(Left$(identifCol, Len(identifCol) - Len(fEstadoColonia(Val(nodoDesde.Tag)))))
                    identifCol = Trim$(Left$(identifCol, Len(identifCol) - Len(fEstadoNodo(constESTADOCOL, Val(nodoDesde.Tag)))))
                Else
                    identifCol = ": Sin identificar"
                End If
                nodoEn.Text = "Colonia " & cColNew & identifCol
                nodoEn.Key = "K" & cColNew & "C" & cTecAsist
                'estado de los botones y frames
                If tvwMuestras.SelectedItem = nodoEn Then
                    Call pDatosColonia(cColNew)
                    estado = fDatosColonia(cColNew, cTecAsist)
                    Call pEstadoBotones("K", estado)
                End If
            End If
            
    End Select
        
End Sub
Private Sub pDatosAntibioticos(codTecAsist%, codAntibiotico%)
'Se traen a pantalla los datos de los antibi�ticos (sensibilidad, concentraci�n, _
    informaci�n del antibi�tico y de la concentraci�n)

    Dim SQL As String
    Dim rsResultados As rdoResultset, qryResultados As rdoQuery
    
    SQL = "SELECT MB2900.MB40_codReac, MB40_desig, MB29_conc, MB29_recom, MB29_indInf_Conc"
    SQL = SQL & " FROM MB2900, MB4000"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB07_codAnti = ?"
    SQL = SQL & " AND MB4000.MB40_codReac (+)= MB2900.MB40_codReac"
    Set qryResultados = objApp.rdoConnect.CreateQuery("Result", SQL)
        
    qryResultados(0) = nReferencia
    qryResultados(1) = codTecAsist
    qryResultados(2) = codAntibiotico
    Set rsResultados = qryResultados.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not IsNull(rsResultados(0)) Then
        cbossReac.Text = rsResultados(1)
        codReac = rsResultados(0)
    Else
        cbossReac.Text = ""
        codReac = -1
    End If
    
    If Not IsNull(rsResultados(2)) Then
        txtConc.Text = rsResultados(2)
    Else
        txtConc.Text = ""
    End If
    
    If rsResultados(3) = 0 Then
        chkRecomendado.Value = 0
    Else
        chkRecomendado.Value = 1
    End If
    
    If rsResultados(4) = 0 Then
        chkConc.Value = 0
    Else
        chkConc.Value = 1
    End If
    
    rsResultados.Close
    qryResultados.Close
    
End Sub

Private Sub pDatosColonia(CodCol%)
'Se traen a pantalla los datos de la colonia (identificaci�n)

    Dim SQL As String
    Dim rsResultados As rdoResultset, qryResultados As rdoQuery
    
    SQL = "SELECT MB2700.MB11_codGeMicro, MB2700.MB12_codMorf, MB2700.MB18_codMicro,"
    SQL = SQL & " MB11_desig, MB12_desig, MB18_desig"
    SQL = SQL & " FROM MB2700, MB1100, MB1200, MB1800"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB1100.MB11_codGeMicro (+) = MB2700.MB11_codGeMicro"
    SQL = SQL & " AND MB1200.MB12_codMorf (+)= MB2700.MB12_codMorf"
    SQL = SQL & " AND MB1800.MB18_codMicro (+)= MB2700.MB18_codMicro"
    Set qryResultados = objApp.rdoConnect.CreateQuery("Result", SQL)
        
    qryResultados(0) = nReferencia
    qryResultados(1) = CodCol
    Set rsResultados = qryResultados.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not IsNull(rsResultados(0)) Then
        cbossIdentificacion(0).Text = rsResultados(3)
        IdentifMicro(0) = rsResultados(0)
    Else
        cbossIdentificacion(0).Text = ""
        IdentifMicro(0) = -1
    End If
    
    If Not IsNull(rsResultados(1)) Then
        cbossIdentificacion(1).Text = rsResultados(4)
        IdentifMicro(1) = rsResultados(1)
    Else
        cbossIdentificacion(1).Text = ""
        IdentifMicro(1) = -1
    End If
    
    Call pLlenarComboMicro(IdentifMicro(0), IdentifMicro(1))
    
    If Not IsNull(rsResultados(2)) Then
        cbossIdentificacion(2).Text = rsResultados(5)
        IdentifMicro(2) = rsResultados(2)
    Else
        cbossIdentificacion(2).Text = ""
        IdentifMicro(2) = -1
    End If
    
    rsResultados.Close
    qryResultados.Close
    
End Sub

Private Sub pDatosCultivo(codTecAsist%)
'Se traen a pantalla los datos del cultivo (condicion ambiental actual y origen)

    Dim SQL As String
    Dim rsResultados As rdoResultset, qryResultados As rdoQuery
    Dim rsRecip As rdoResultset, qryRecip As rdoQuery

    ' se busca la condici�n ambiental actual del cultivo
    SQL = "SELECT MB2600.MB02_codCond, MB02_desig, MB26_codPaso"
    SQL = SQL & " FROM MB2600, MB0200, MB2000"
    SQL = SQL & " WHERE MB2600.nRef = ?"
    SQL = SQL & " AND MB2600.MB20_codTecAsist = ?"
    SQL = SQL & " AND MB35_codEstSegui = ?"
    SQL = SQL & " AND MB0200.MB02_codCond = MB2600.MB02_codCond"
    Set qryResultados = objApp.rdoConnect.CreateQuery("SelCondAmbCultivo", SQL)
    
    qryResultados(0) = nReferencia
    qryResultados(1) = codTecAsist
    qryResultados(2) = constSEGUITECVALIDA
    Set rsResultados = qryResultados.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If rsResultados.EOF = False Then
        cbossCondiciones.Text = rsResultados(1)
        CondAmbiental = rsResultados(0)
        codPaso = rsResultados(2)
    Else
        cbossCondiciones.Text = ""
    End If
    qryResultados.Close
        
    ' se busca el tipo de origen del cultivo (muestra, placa o tubo, colonia)
    SQL = "SELECT MB20_tiOrig, MB20_codOrig, MB20_codTecAsist_Orig"
    SQL = SQL & " FROM MB2000"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    Set qryResultados = objApp.rdoConnect.CreateQuery("SelDatosTecAsist", SQL)
        
    qryResultados(0) = nReferencia
    qryResultados(1) = codTecAsist
    Set rsResultados = qryResultados.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
    Select Case rsResultados(0)
        Case constORIGENMUESTRA
            SQL = " Muestra  "
        Case constORIGENPLACA
            SQL = "SELECT MB03_desig"
            SQL = SQL & " FROM MB0300,MB0900,MB2000"
            SQL = SQL & " WHERE MB0900.MB09_codTec = MB2000.MB09_codTec"
            SQL = SQL & " AND MB0300.MB03_codRecip = MB0900.MB03_codRecip"
            SQL = SQL & " AND nRef = ?"
            SQL = SQL & " AND MB20_codTecAsist = ?"
            Set qryRecip = objApp.rdoConnect.CreateQuery("Recip", SQL)
            
            qryRecip(0) = nReferencia
            qryRecip(1) = rsResultados(2)
            Set rsRecip = qryRecip.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            
            SQL = rsRecip(0) & "  "
            
            rsRecip.Close
            qryRecip.Close
        Case constORIGENCOLONIA
            SQL = " Colonia  "
    End Select
    txtOrigenPlaca.Text = SQL & rsResultados(1)
    
    rsResultados.Close
    qryResultados.Close
        
End Sub


Private Sub pDatosResultTecnicas(codTecAsist%, codTec%, codResult, estado%, cTiTec%)
'Se traen a pantalla los datos del resultado de la t�cnica (valor y unidad)

    Dim SQL As String
    Dim rsResultados As rdoResultset, qryResultados As rdoQuery
    Dim resultado As String
    
    SQL = "SELECT MB33_result, MB38_codEstResult, MB3300.cUnidad, designacion"
    SQL = SQL & " FROM MB3300, Unidades u"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB09_codTec = ?"
    SQL = SQL & " AND MB32_codResult = ?"
    SQL = SQL & " AND u.cUnidad = MB3300.cUnidad"
    Set qryResultados = objApp.rdoConnect.CreateQuery("Result", SQL)
        
    qryResultados(0) = nReferencia
    qryResultados(1) = codTecAsist
    qryResultados(2) = codTec
    qryResultados(3) = codResult
    Set rsResultados = qryResultados.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not IsNull(rsResultados(0)) Then
        'si el resultado es del tipo recuento hay que formatearlo previamente antes de _
        mostrarlo
        txtResultados.Text = fFormatearRecuento(rsResultados(0), cTiTec, False)
    Else
        txtResultados.Text = ""
    End If
    txtUd.Text = rsResultados(3)
    txtUd.Tag = rsResultados(2) 'se guarda el c�digo de la unidad en el Tag
    
    estado = rsResultados(1) 'se devuelve el estado del resultado

    rsResultados.Close
    qryResultados.Close
    
End Sub

Private Function fUpdateEstTecnicas(cTecAsist$, estado%) As Boolean
'Se cambia el estado de la t�cnica

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryTecAsist As rdoQuery
    
    fUpdateEstTecnicas = False
    
    On Error Resume Next
    
    SQL = "UPDATE MB2000 "
    SQL = SQL & " SET MB34_codEstTecAsist = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist IN (" & cTecAsist & ")"
    Set qryTecAsist = objApp.rdoConnect.CreateQuery("UpdateTecAsist", SQL)
    
    qryTecAsist(0) = estado
    qryTecAsist(1) = nReferencia
    qryTecAsist.Execute
    
    If Err = 0 Then
        fUpdateEstTecnicas = True
    End If
        
    qryTecAsist.Close
    
End Function
Private Function fUpdateEstCultivos(cTecAsist$, estado%) As Boolean
' se anula el cultivo y se anota la fecha de salida de las condiciones ambientales

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    fUpdateEstCultivos = False
    
    ' se cambia el estado del cultivo
    If fUpdateEstTecnicas(cTecAsist, estado) = False Then
        Exit Function
    End If
    
    ' se anota la fecha de salida de las condiciones ambientales
    If fUpdateSeguiTec(cTecAsist) = True Then
        fUpdateEstCultivos = True
    End If
        
End Function


Private Function fUpdateSeguiTec(cTecAsist$) As Boolean
'Anotar la fecha de salida de las condiciones ambientales

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qrySeguiTec As rdoQuery
    
    fUpdateSeguiTec = False
    
    On Error Resume Next
    
    SQL = "UPDATE MB2600"
    SQL = SQL & " SET MB35_codEstSegui = ?,"
    SQL = SQL & " MB26_fecSali = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist IN (" & cTecAsist & ")"
    SQL = SQL & " AND MB35_codEstSegui = ?"
    Set qrySeguiTec = objApp.rdoConnect.CreateQuery("SalidaCondAmb", SQL)

    qrySeguiTec(0) = constSEGUITECANTIGUA
    qrySeguiTec(1) = fFechaHoraActual()
    qrySeguiTec(2) = nReferencia
    qrySeguiTec(3) = constSEGUITECVALIDA
    qrySeguiTec.Execute
    
    If Err = 0 Then
        fUpdateSeguiTec = True
    End If
    
    qrySeguiTec.Close

End Function

Private Sub pEstadoBotones(letra$, estado%, Optional letraPadre$)
'Controla el estado de los botones y paneles de la pantalla en funci�n del nodo activo _
    en el TreeView de las Muestras (elemento y estado). Tambi�n hay que tener en cuanta _
    el tipo de acceso a la pantalla (Dr. o no)
    
tabResCol.Enabled = True 'colonias
fraResultados(1).Enabled = True 'resultados t�cnicas
tabResCultivo.Enabled = True 'cultivos
tabResTec.Enabled = True 't�cnicas
fraResultados(3).Enabled = True 'antibi�ticos

cmdResultados(0).Enabled = True 'validar, aceptar, reincubar
cmdResultados(1).Enabled = True 'informar
cmdResultados(2).Enabled = True 'problema
cmdResultados(3).Enabled = True 'almacenar

cmdPeticion(1).Enabled = False
        
Select Case letra
    Case "K" 'colonias
        txtObservMuestra.Visible = False
        tabResCol.Visible = True
        fraResultados(1).Visible = False
        tabResCultivo.Visible = False
        tabResTec.Visible = False
        fraResultados(3).Visible = False
    
        cmdResultados(0).Visible = True
        cmdResultados(0).Caption = "Validar"
        If tipoAccesoDoctor = True Then
            cmdResultados(1).Visible = True
        Else
            cmdResultados(1).Visible = False
        End If
        cmdResultados(2).Visible = True
        cmdResultados(3).Visible = True
        
        Frame3(0).Enabled = True
        Frame3(1).Enabled = True
        Frame3(2).Enabled = True
        tabResCol.ForeColor = vbBlack
        
        If estado = constCOLINFORMADA Or estado = constCOLANULADA Then
            cmdResultados(0).Enabled = False
            If estado = constCOLANULADA Then
                Frame3(0).Enabled = False
                Frame3(1).Enabled = False
                Frame3(2).Enabled = False
                tabResCol.ForeColor = vbGrayText
                cmdResultados(1).Enabled = False
                cmdResultados(3).Enabled = False
            ElseIf estado = constCOLINFORMADA And cbossIdentificacion(2).Text <> "" Then
                Frame3(0).Enabled = False
                Frame3(1).Enabled = False
                Frame3(2).Enabled = False
                tabResCol.ForeColor = vbGrayText
                cmdResultados(1).Enabled = False
            End If
        End If
        
    Case "R" 'resultados t�cnicas
        txtObservMuestra.Visible = False
        tabResCol.Visible = False
        fraResultados(1).Visible = True
        tabResCultivo.Visible = False
        tabResTec.Visible = False
        fraResultados(3).Visible = False
        
        cmdResultados(0).Visible = True
        cmdResultados(0).Caption = "Validar"
        'bot�n de informar visible para los resultados de t�cnicas sobre la muestra y para las _
        de recuento sobre la colonia
        If letraPadre = "M" And tipoAccesoDoctor = True Then
            cmdResultados(1).Visible = True
        Else
            If Val(Mid$(nodoResultados.Parent.Key, InStr(nodoResultados.Parent.Key, "I") + 1)) = constRECUENTO Then
                cmdResultados(1).Visible = True
            Else
                cmdResultados(1).Visible = False
            End If
        End If
        cmdResultados(2).Visible = False
        cmdResultados(3).Visible = False
        
        If estado = constRESULTINFORMADO Or estado = constRESULTANULADO Then
            fraResultados(1).Enabled = False
            cmdResultados(0).Enabled = False
            cmdResultados(1).Enabled = False
        End If
      
    Case "C" 'cultivos
        txtObservMuestra.Visible = False
        tabResCol.Visible = False
        fraResultados(1).Visible = False
        tabResCultivo.Visible = True
        tabResTec.Visible = False
        fraResultados(3).Visible = False
        
        cmdResultados(0).Visible = True
        cmdResultados(0).Caption = "Reincubar"
        cmdResultados(1).Visible = False
        cmdResultados(2).Visible = True
        cmdResultados(3).Visible = False
        
        Frame1(0).Enabled = True
        Frame1(1).Enabled = True
        Frame1(2).Enabled = True
        tabResCultivo.ForeColor = vbBlack
            
        If estado = constTECANULADA Or estado = constTECFINALIZADA Then
            Frame1(0).Enabled = False
            Frame1(1).Enabled = False
            Frame1(2).Enabled = False
            tabResCultivo.ForeColor = vbGrayText
            cmdResultados(0).Enabled = False
        ElseIf estado = constTECREINCUBADA Or estado = constTECSOLICITADA Then
            cmdResultados(0).Caption = "Aceptar" 'para cambiar las condiciones ambientales
            cmdPeticion(1).Enabled = True
        Else
            cmdPeticion(1).Enabled = True
        End If
        
    Case "T" 't�cnicas
        txtObservMuestra.Visible = False
        tabResCol.Visible = False
        fraResultados(1).Visible = False
        tabResCultivo.Visible = False
        tabResTec.Visible = True
        fraResultados(3).Visible = False
        
        cmdResultados(0).Caption = "Validar"
        'Bot�n Validar visible para validar todos los resultados a la vez excepto para _
        las t�cnicas de antibiograma
        'Bot�n Informar visible para informar todos los resultados validados de t�cnicas _
        realizadas sobre la muestra y las de recuento
        If letraPadre = "M" Then
            If tipoAccesoDoctor = True Then
                cmdResultados(1).Visible = True
            Else
                cmdResultados(1).Visible = False
            End If
            cmdResultados(0).Visible = True
        Else
            If Val(Mid$(nodoResultados.Key, InStr(nodoResultados.Key, "I") + 1)) = constANTIBIOGRAMA Then
                cmdResultados(0).Visible = False
            Else
                cmdResultados(0).Visible = True
            End If
            If Val(Mid$(nodoResultados.Key, InStr(nodoResultados.Key, "I") + 1)) = constRECUENTO Then
                cmdResultados(1).Visible = True
            Else
                cmdResultados(1).Visible = False
            End If
            'cmdResultados(1).Visible = False
        End If
        cmdResultados(2).Visible = True
        cmdResultados(3).Visible = False
        
        If estado = constTECANULADA Then
            Frame2(0).Enabled = False
            Frame2(1).Enabled = False
            tabResTec.ForeColor = vbGrayText
            cmdResultados(0).Enabled = False
            cmdResultados(1).Enabled = False
        Else
            Frame2(0).Enabled = True
            Frame2(1).Enabled = True
            tabResTec.ForeColor = vbBlack
            cmdResultados(0).Enabled = True
            cmdResultados(1).Enabled = True
        End If
        
    Case "A" 'antibi�ticos
        txtObservMuestra.Visible = False
        tabResCol.Visible = False
        fraResultados(1).Visible = False
        tabResCultivo.Visible = False
        tabResTec.Visible = False
        fraResultados(3).Visible = True
        
        cmdResultados(0).Visible = True
        cmdResultados(0).Caption = "Aceptar"
        cmdResultados(1).Visible = False
        cmdResultados(2).Visible = False
        cmdResultados(3).Visible = False
        
    Case Else
        tabResCol.Visible = False
        fraResultados(1).Visible = False
        tabResCultivo.Visible = False
        tabResTec.Visible = False
        fraResultados(3).Visible = False
        
        cmdResultados(0).Visible = False
        cmdResultados(1).Visible = False
        If letra = "M" Then 'muestras
            cmdResultados(2).Visible = True
            txtObservMuestra.Visible = True
        Else
            cmdResultados(2).Visible = False
            txtObservMuestra.Visible = False
        End If
        cmdResultados(3).Visible = False
    
End Select

End Sub

Private Function fExisteColPlaca(nodoParent As Node, nodoKey As String) As String
'Chequea si una colonia ya existe en una placa. En caso de existir devuelve el Key del nodo _
    de la colonia

    Dim i%
    Dim nodo As Node
    
    For i = 1 To nodoParent.Children
        If i = 1 Then
            Set nodo = nodoParent.Child
        Else
            Set nodo = nodo.Next
        End If
        If Left$(nodo.Key, Len(nodoKey)) = nodoKey Then
            fExisteColPlaca = nodo.Key
            Exit For
        End If
    Next i
    
End Function

Private Sub pFormatearVentana(tipoVentana$)
'Formatea el tama�o de los TreeViews

cmdTipoVentana.AutoSize = 2 'el bot�n ajustado a la imagen

Select Case tipoVentana
    Case constRESTAURAR_VENTANA
        'ventana peque�a
        fraMuestras.Top = 2460
        fraMuestras.Height = 3855
        
        tvwMuestras.Top = 300
        tvwMuestras.Height = 3495
        
        tabMuestras.Top = 450 '370
        tabMuestras.Height = 3340 '3420
        
        tvwAntibioticos.Top = 360
        tvwAntibioticos.Height = 2930 '3015
        
        tvwColonias.Top = 360
        tvwColonias.Height = 2930 '3015
        
        tvwMorfologias.Top = 360
        tvwMorfologias.Height = 2930 '3015
        
        tvwProtocolos.Top = 360
        tvwProtocolos.Height = 2930 '3015
        
        tvwTecnicas.Top = 360
        tvwTecnicas.Height = 2930 '3015
        
        'cmdTipoVentana.Caption = "+"
        cmdTipoVentana.Picture = frmPrincipal.imlImageList2.ListImages(constIconAumentar).Picture
        cmdTipoVentana.ToolTipText = constMAXIMIZAR_VENTANA
        
    Case constMAXIMIZAR_VENTANA
        'ventana grande
        fraMuestras.Top = 1140
        fraMuestras.Height = 5175
        
        tvwMuestras.Top = 300
        tvwMuestras.Height = 4815
        
        tabMuestras.Top = 450 '370
        tabMuestras.Height = 4660 '4740
        
        tvwAntibioticos.Top = 360
        tvwAntibioticos.Height = 4250 '4335
        
        tvwColonias.Top = 360
        tvwColonias.Height = 4250 '4335
        
        tvwMorfologias.Top = 360
        tvwMorfologias.Height = 4250 '4335
        
        tvwProtocolos.Top = 360
        tvwProtocolos.Height = 4250 '4335
        
        tvwTecnicas.Top = 360
        tvwTecnicas.Height = 4250 '4335
        
        'cmdTipoVentana.Caption = "-"
        cmdTipoVentana.Picture = frmPrincipal.imlImageList2.ListImages(constIconReducir).Picture
        cmdTipoVentana.ToolTipText = constRESTAURAR_VENTANA
End Select

End Sub

Private Sub pInsertarAntibioticos(nodoEn As Node, nodoDesde As Node)
'Inserta antibi�ticos en el antibiograma de la colonia

    Dim res%
    Dim arAnti() As typeAntibioticos
    Dim dimension%, i%
    Dim msg$
    
  If Left$(nodoEn.Key, 1) = "T" Then
     If Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "I") + 1)) = constANTIBIOGRAMA Then
        'se mira si la t�cnica est� activa
        If Val(nodoEn.Tag) = constTECANULADA Then
            MsgBox "La t�cnica est� anulada. No se puede a�adir el antibi�tico.", vbExclamation, "A�adir antibi�ticos"
            Exit Sub
        End If
        
        'se mira si la t�cnica est� validada
        If Val(nodoEn.Tag) = constTECVALIDADA Then
            MsgBox "La t�cnica est� validada. No se puede a�adir el antibi�tico.", vbExclamation, "A�adir antibi�ticos"
            Exit Sub
        End If
                
        'se a�ade el antibi�tico o grupo al antibiograma
         objApp.rdoConnect.BeginTrans
         Select Case Left$(nodoDesde.Key, 1)
         Case "G"
             'Clave del nodo arrastrado (NodoDesde):"G" & CodGrAnti
             res = fInsertGrAntibioticos(nodoEn, nodoDesde, arAnti())
             If res = 1 Then
                msg = "Alg�n antibi�tico del grupo no ha sido a�adido al antibiograma por"
                msg = msg & " encontrarse ya en otro antibiograma del mismo microorganismo."
                MsgBox msg, vbInformation, "A�adir antibi�ticos"
             End If
         Case "A"
             'Clave del nodo arrastrado (NodoDesde):"A" & CodAnti & "G" (& CodGrAnti)
             res = fInsertAntibioticos(nodoEn, nodoDesde, arAnti())
             If res = 1 Then
                msg = "El antibi�tico no ha sido a�adido al antibiograma por"
                msg = msg & " encontrarse ya en otro antibiograma del mismo microorganismo."
                MsgBox msg, vbInformation, "A�adir antibi�ticos"
             End If
         End Select
         
         Select Case res
         Case 2
             MsgBox "Se ha producido un error al a�adir antibi�ticos.", vbError
             objApp.rdoConnect.RollbackTrans
         Case 0, 1
             objApp.rdoConnect.CommitTrans
             
             'nodos de antibi�ticos
             On Error Resume Next 'por si no hay nada en arAnti()
             dimension = UBound(arAnti)
             On Error GoTo 0
             For i = 1 To dimension
                 Call pAddNodoAntibiotico(tvwMuestras, arAnti(i).cAnti, arAnti(i).desig, "", "", 1, Val(Mid$(nodoEn.Key, 2)))
             Next i
         End Select
     End If
  End If
  
End Sub
Private Sub pLlenarComboCondiciones()
' se llena el combo de las condiciones ambientales para la reincubaci�n de cultivos

    Dim SQL As String, rsCondiciones As rdoResultset
    
    SQL = "SELECT MB02_codCond, MB02_desig"
    SQL = SQL & " FROM MB0200"
    SQL = SQL & " WHERE MB02_indActiva = -1"
    SQL = SQL & " ORDER BY MB02_desig"
    Set rsCondiciones = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
    
    Do While rsCondiciones.EOF = False
        cbossCondiciones.AddItem rsCondiciones(0) & Chr$(9) & rsCondiciones(1)
        rsCondiciones.MoveNext
    Loop
    
    rsCondiciones.Close
    
End Sub

Private Sub pLlenarComboGeneroMicro()
' se llena el combo de los g�neros de microorganismos para la identificaci�n de colonias _
con los g�neros definidos para la secci�n

    Dim SQL As String
    Dim rsGeMicro As rdoResultset, qryGeMicro As rdoQuery
    
    'se vac�a el combo
    cbossIdentificacion(1).Text = ""
    IdentifMicro(0) = -1
    cbossIdentificacion(0).RemoveAll
    
    SQL = "SELECT MB11_codGeMicro, MB11_desig"
    SQL = SQL & " FROM MB1100,MB0800"
    SQL = SQL & " WHERE MB11_indActiva = -1"
    SQL = SQL & " AND MB0800.MB08_codTiMic = MB1100.MB08_codTiMic"
    SQL = SQL & " AND MB08_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = ?"
    SQL = SQL & " ORDER BY MB11_desig"
    Set qryGeMicro = objApp.rdoConnect.CreateQuery("GeMicro", SQL)
        
    qryGeMicro(0) = departamento
    Set rsGeMicro = qryGeMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsGeMicro.EOF = False
        cbossIdentificacion(0).AddItem rsGeMicro(0) & Chr$(9) & rsGeMicro(1)
        rsGeMicro.MoveNext
    Loop
    
    rsGeMicro.Close
    qryGeMicro.Close
    
End Sub

Private Sub pLlenarComboMicro(genero%, morfo%)
' se llena el combo de los microorganismos para la identificaci�n de colonias _
con los microorganismos definidos para la secci�n y que sean del g�nero y/o morfolog�a _
seleccionados

    Dim SQL As String
    Dim rsMicro As rdoResultset, qryMicro As rdoQuery
    
    'se vac�a el combo
    cbossIdentificacion(2).Text = ""
    IdentifMicro(2) = -1
    cbossIdentificacion(2).RemoveAll
    
    SQL = "SELECT MB18_codMicro, MB18_desig"
    SQL = SQL & " FROM MB1800,MB1100,MB0800"
    SQL = SQL & " WHERE MB18_indActiva = -1"
    SQL = SQL & " AND MB1100.MB11_codGeMicro = MB1800.MB11_codGeMicro"
    SQL = SQL & " AND MB11_indActiva = -1"
    SQL = SQL & " AND MB0800.MB08_codTiMic = MB1100.MB08_codTiMic"
    SQL = SQL & " AND MB08_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = ?"
    If genero <> -1 Then
        SQL = SQL & " AND MB1800.MB11_codGeMicro = ?"
    End If
    If morfo <> -1 Then
        SQL = SQL & " AND MB1800.MB12_codMorf = ?"
    End If
    SQL = SQL & " ORDER BY MB18_desig"
    Set qryMicro = objApp.rdoConnect.CreateQuery("Micro", SQL)
    
    qryMicro(0) = departamento
    If genero <> -1 Then
        qryMicro(1) = genero
        If morfo <> -1 Then
            qryMicro(2) = morfo
        End If
    Else
        If morfo <> -1 Then
            qryMicro(1) = morfo
        End If
    End If
    Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsMicro.EOF = False
        cbossIdentificacion(2).AddItem rsMicro(0) & Chr$(9) & rsMicro(1)
        rsMicro.MoveNext
    Loop
    
    rsMicro.Close
    qryMicro.Close
    
End Sub



Private Sub pLlenarComboMorfologiasTVMorfologias()
' se llenan el combo de las morfolog�as para la identificaci�n de colonias y el TreeView de _
las morfolog�as con las morfolog�as definidas para la secci�n
    
'**********************************************
'*  Clave de las morfologias: "F" & CodMorf  *
'**********************************************

    Dim SQL As String
    Dim rsMorfologias As rdoResultset, qryMorfologias As rdoQuery
    Dim nodo As Node
    
    'se vac�a el combo y el tvw
    cbossIdentificacion(1).Text = ""
    IdentifMicro(1) = -1
    cbossIdentificacion(1).RemoveAll
    tvwMorfologias.Nodes.Clear
    
    SQL = "SELECT MB1200.MB12_codMorf, MB12_desig"
    SQL = SQL & " FROM MB1200,MB0800"
    SQL = SQL & " WHERE MB12_indActiva = -1"
    SQL = SQL & " AND MB0800.MB08_codTiMic = MB1200.MB08_codTiMic"
    SQL = SQL & " AND MB08_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = ?"
    SQL = SQL & " ORDER BY MB12_desig"
    Set qryMorfologias = objApp.rdoConnect.CreateQuery("Morfologias", SQL)
        
    qryMorfologias(0) = departamento
    Set rsMorfologias = qryMorfologias.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsMorfologias.EOF = False
        'tvwMorfologias
        Set nodo = tvwMorfologias.Nodes.Add(, , "F" & rsMorfologias(0), rsMorfologias(1), constIconMorfologia)
        'comboMorfologias
        cbossIdentificacion(1).AddItem rsMorfologias(0) & Chr$(9) & rsMorfologias(1)
        rsMorfologias.MoveNext
    Loop
    
    rsMorfologias.Close
    qryMorfologias.Close
    
End Sub


Private Sub pLlenarComboReaccion()
' se llena el combo de los niveles de sensibilidad de los microorganismos ante los _
antibi�ticos para establecer el antibiograma

    Dim SQL As String, rsReaccion As rdoResultset
    
    SQL = "SELECT MB40_codReac, MB40_desig"
    SQL = SQL & " FROM MB4000"
    SQL = SQL & " WHERE MB40_indActiva = -1"
    SQL = SQL & " ORDER BY MB40_desig"
    Set rsReaccion = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
    
    Do While rsReaccion.EOF = False
        cbossReac.AddItem rsReaccion(0) & Chr$(9) & rsReaccion(1)
        rsReaccion.MoveNext
    Loop

    rsReaccion.Close
    
End Sub

Private Sub pRefrescarDatos()
    Dim dimension%, i%
    
    On Error Resume Next
    dimension = UBound(arMantenimientos)
    On Error GoTo 0
    
    For i = 1 To dimension
        Select Case arMantenimientos(i)
            Case constMANTPROTOCOLOS
                Call pTVProtocolos
            Case constMANTTECNICAS
                Call pTVTecnicas
            Case constMANTMORFOLOGIAS
                Call pLlenarComboMorfologiasTVMorfologias
                Call pLlenarComboMicro(IdentifMicro(0), IdentifMicro(1))
            Case constMANTANTIBIOTICOS
                Call pTVAntibioticos
            Case constMANTGENEROS
                Call pLlenarComboGeneroMicro
                Call pLlenarComboMicro(IdentifMicro(0), IdentifMicro(1))
            Case constMANTMICROORGANISMOS
                Call pLlenarComboMicro(IdentifMicro(0), IdentifMicro(1))
        End Select
    Next i
    
    Erase arMantenimientos()
    
End Sub

Private Sub pResultColonias(cCol%, estado%)
' se anota la identificaci�n de la colonia
    Dim SQL$
    Dim rsChequeo As rdoResultset, qryChequeo As rdoQuery
    Dim identifCol$
    Dim cColExist%
    Dim nodo As Node, i%
    Dim arProceso() As Integer
    Dim cColActual%
    
    ' se realizan los chequeos de validez de resultados de las t�cnicas
    If IdentifMicro(0) <> -1 Or IdentifMicro(1) <> -1 Or IdentifMicro(2) <> -1 Then
        If fComprobarResultadoMicro(cCol, IdentifMicro(2), IdentifMicro(0), IdentifMicro(1)) = False Then
            Exit Sub
        End If
    End If
        
    'se realizan los chequeos de validez sobre el antibiograma si ya lo tiene definido
    If IdentifMicro(2) <> -1 Then 'microorganismo identificado completamente
        If fComprobarAntibiograma(cCol, IdentifMicro(2)) = False Then
            Exit Sub
        End If
    End If
    
    ' se comprueba si el microorganismo hab�a sido identificado ya en otra colonia
    cColExist = 0
    If IdentifMicro(2) <> -1 Then
        cColExist = fComprobarMicroRepetido(cCol, IdentifMicro(2))
    End If
    
    'msrdcColonias.Resultset.Close 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
    Select Case cColExist
        Case -1 'Se ha cancelado la operaci�n
            'msrdcColonias.Refresh
            Exit Sub
        
        Case 0 'El microorganismo es nuevo
            'se identifica la colonia, validando o informando el resultado
            objApp.rdoConnect.BeginTrans
            If estado = constCOLINFORMADA Then
                'se anota el resultado en la base de datos de Laboratorio
                If fInformarColoniaLabor(nReferencia, cCol, IdentifMicro(2), IdentifMicro(0), IdentifMicro(1), objPruebaAsistencia.rdoCursor("historia"), objPruebaAsistencia.rdoCursor("caso"), objPruebaAsistencia.rdoCursor("secuencia"), objPruebaAsistencia.rdoCursor("nRepeticion"), cUserPeticion, False) = constINCORRECTO Then
                    MsgBox "Se ha producido un error en la identificaci�n de colonias.", vbError
                    objApp.rdoConnect.RollbackTrans
                    Exit Sub
                End If
            End If
            'se anota la identificaci�n en la base de datos de Micro
            If fUpdateIdentifCol(estado, IdentifMicro(0), IdentifMicro(1), IdentifMicro(2), cCol) = False Then
                MsgBox "Se ha producido un error en la identificaci�n de colonias.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                'nodos del tvwMuestras
                identifCol = fIdentifCol(cbossIdentificacion(2).Text, cbossIdentificacion(0).Text, cbossIdentificacion(1).Text)
                For i = 1 To tvwMuestras.Nodes.Count
                    Set nodo = tvwMuestras.Nodes(i)
                    If Left$(nodo.Key, Len(CStr(cCol)) + 1) = "K" & cCol Then
                        Call pUpdateNodoColPlaca(nodo, cCol, identifCol, Val(nodo.Tag))
                    End If
                Next i
                'nodo del tvwColonias
                Call pUpdateNodoColonia(tvwColonias.Nodes("K" & cCol), cCol, identifCol, estado)
                'estado del frame y de los botones
                Call pEstadoBotones(Left$(nodoResultados.Key, 1), estado)
            End If
            
        Case Is > 0 'El microorganismo fue identificado en la colonia cColExist
            identifCol = fIdentifCol(cbossIdentificacion(2).Text, cbossIdentificacion(0).Text, cbossIdentificacion(1).Text)
            objApp.rdoConnect.BeginTrans
            If estado = constCOLINFORMADA Then
                'se anota el resultado en la base de datos de Laboratorio
                If fInformarColoniaLabor(nReferencia, cCol, IdentifMicro(2), IdentifMicro(0), IdentifMicro(1), objPruebaAsistencia.rdoCursor("historia"), objPruebaAsistencia.rdoCursor("caso"), objPruebaAsistencia.rdoCursor("secuencia"), objPruebaAsistencia.rdoCursor("nRepeticion"), cUserPeticion, True) = constINCORRECTO Then
                    MsgBox "Se ha producido un error en la identificaci�n de colonias.", vbError
                    objApp.rdoConnect.RollbackTrans
                    Exit Sub
                End If
            End If
            If fUpdateIdentifCol(constCOLANULADA, IdentifMicro(0), IdentifMicro(1), IdentifMicro(2), cCol) = False Then
                MsgBox "Se ha producido un error en la identificaci�n de colonias.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                If fChangeNumColonia(cColExist, cCol, arProceso()) = False Then
                    MsgBox "Se ha producido un error en la identificaci�n de colonias.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    
                    'nodo de tvwColonias
                    Call pUpdateNodoColonia(tvwColonias.Nodes("K" & cCol), cCol, identifCol, constCOLANULADA)
                    
                    'nodos de tvwMuestras
                    For i = 1 To UBound(arProceso, 2)
                        Call pUpdateNodoColPlaca(tvwMuestras.Nodes("K" & cCol & "C" & arProceso(1, i)), cCol, identifCol, Val(tvwMuestras.Nodes("K" & cCol & "C" & arProceso(1, i)).Tag))
                        
                        If arProceso(3, i) <> constCOLPLACAANULADA Then
                            cColActual = cCol
                            Select Case arProceso(2, i)
                            Case 0
                                tvwMuestras.Nodes("K" & cCol & "C" & arProceso(1, i)).Text = "Colonia " & cColExist & ": " & identifCol
                                tvwMuestras.Nodes("K" & cCol & "C" & arProceso(1, i)).Key = "K" & cColExist & "C" & arProceso(1, i)
                                cColActual = cColExist
                            Case constCOLPLACAASOCIADA
                                Call pUpdateNodoColPlacaEstado(tvwMuestras.Nodes("K" & cCol & "C" & arProceso(1, i)), constCOLPLACAANULADA)
                            Case constCOLPLACAANULADA
                                Call pUpdateNodoColPlacaEstado(tvwMuestras.Nodes("K" & cCol & "C" & arProceso(1, i)), constCOLPLACAANULADA)
                                Call pUpdateNodoColPlacaEstado(tvwMuestras.Nodes("K" & cColExist & "C" & arProceso(1, i)), constCOLPLACAASOCIADA)
                            End Select
                            
                            'estado del frame y los botones
                            If tvwMuestras.Nodes("K" & cColActual & "C" & arProceso(1, i)) = nodoResultados Then
                                If arProceso(2, i) <> 0 Then
                                    Call pEstadoBotones("K", constCOLANULADA)
                                End If
                            End If
                        End If
                    Next i
                End If
            End If
    End Select
    'msrdcColonias.Refresh 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
    
End Sub

Private Sub pResultResultadoTecnica(nodoResult As Node, resultado$, estado%)
' se anotan los resultados de las t�cnicas
    Dim SQL$
    Dim rsResultados As rdoResultset, qryResultados As rdoQuery
    Dim res% 'respuesta
    Dim cUnidad%, unidad$ 'datos de la unidad del resultado
    Dim TextNodo$ 'texto a poner en el nodo
    Dim codTec%, codResult%, codTecAsist%, desig$ 'datos del resultado
    
    'se obtienen los datos del resultado del nodo
    codTec = Val(Mid$(nodoResult.Key, InStr(nodoResult.Key, "N") + 1))
    codResult = Val(Mid$(nodoResult.Key, 2))
    codTecAsist = Val(Mid$(nodoResult.Key, InStr(nodoResult.Key, "T") + 1))
 
    'RECUENTOS: se introducen los recuentos de colonias como resultado de la t�cnica de _
    recuento (tipo recuento). Aunque las t�cnicas van asociadas a colonia-placa, lo l�gico _
    es que el recuento s�lo se haga una vez por colonia (en una sola placa). En caso de _
    hacerse varios recuentos para la misma colonia, se supone que hay que sumarlos. El _
    recuento global (recuento de colonias sin especificar cuales) se anotar� en la t�cnica _
    de recuento pedida sobre la muestra
    
    'CHEQUEOS RESULTADOS INTRODUCIDOS: no se realizan para los resultados de recuentos
    
    If Val(Mid$(nodoResult.Parent.Key, InStr(nodoResult.Parent.Key, "I") + 1)) = constRECUENTO Then
        resultado = fFormatearRecuento(resultado$, Val(Mid$(nodoResult.Parent.Key, InStr(nodoResult.Parent.Key, "I") + 1)), True)
    Else
       If estado = constRESULTVALIDADO Or estado = constRESULTINFORMADO Then
           If Left$(nodoResult.Parent.Parent.Key, 1) = "K" Then
               ' se realiza el chequeo de validez del resultados respecto al maestro de _
               resultados de t�cnicas sobre microorganismos
               If fComprobarResultadoTec(Val(Mid$(nodoResult.Parent.Parent.Key, 2)), codTec, codResult, resultado) = False Then
                   Exit Sub
               End If
               
               ' se realiza el chequeo de validez del resultados respecto a posibles resultados _
               de la misma t�cnica sobre la colonia obtenidos anteriormente
               If fComprobarResultadoMismaTec(Val(Mid$(nodoResult.Parent.Parent.Key, 2)), codTec, codResult, resultado, codTecAsist) = False Then
                   Exit Sub
               End If
           End If
        End If
    End If
    
    'se obtienen los datos de la unidad del resultado
    cUnidad = txtUd.Tag
    If resultado = "" Then unidad = "" Else unidad = txtUd.Text
    
    On Error Resume Next
    Err = 0
    ' se actualiza el resultado en la base de datos
    objApp.rdoConnect.BeginTrans
    'se anotan los resultados en la base de datos de microbiolog�a
    SQL = "UPDATE MB3300"
    SQL = SQL & " SET MB38_codEstResult = ?"
    Select Case estado
        Case constRESULTVALIDADO
            SQL = SQL & ", cUser_Val = ?"
            SQL = SQL & ", MB33_result = ?"
            SQL = SQL & ", cUnidad = ?"
        Case constRESULTINFORMADO 'si se informa sin validar no se guarda el cUser_Val
            SQL = SQL & ", cUser_Inf = ?"
            SQL = SQL & ", MB33_result = ?"
            SQL = SQL & ", cUnidad = ?"
        Case constRESULTANULADO
    End Select
    SQL = SQL & " WHERE nref = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB09_codTec = ?"
    SQL = SQL & " AND MB32_codResult = ?"
    Set qryResultados = objApp.rdoConnect.CreateQuery("UpdateResult", SQL)

    qryResultados(0) = estado
    Select Case estado
    Case constRESULTANULADO
        qryResultados(1) = nReferencia
        qryResultados(2) = codTecAsist
        qryResultados(3) = codTec
        qryResultados(4) = codResult
    Case Else
        qryResultados(1) = cUserPeticion
        qryResultados(2) = resultado
        qryResultados(3) = cUnidad
        qryResultados(4) = nReferencia
        qryResultados(5) = codTecAsist
        qryResultados(6) = codTec
        qryResultados(7) = codResult
    End Select
    qryResultados.Execute
    
    If Err <> 0 Then
        MsgBox "Se ha producido un error en la actualizaci�n de resultados.", vbError
        objApp.rdoConnect.RollbackTrans
        Exit Sub
    End If
    
    'se pasan los resultados informados a laboratorio
    If estado = constRESULTINFORMADO Then
        res = fInformarResultadoLabor(nReferencia, codTecAsist, objPruebaAsistencia.rdoCursor("historia"), objPruebaAsistencia.rdoCursor("caso"), objPruebaAsistencia.rdoCursor("secuencia"), objPruebaAsistencia.rdoCursor("nRepeticion"), cUserPeticion, codResult)
        Select Case res
            Case constCORRECTO 'operaci�n correcta
            Case constINCORRECTO 'operaci�n incorrecta
                MsgBox "Se ha producido un error en la actualizaci�n de resultados.", vbError
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            Case constOTRO 'resultado ya informado al laboratorio
                estado = constRESULTADOVALIDADO
                objApp.rdoConnect.RollbackTrans
                Exit Sub
        End Select
    End If
    
    'se comprueba si la t�cnica pasar� a estar validada.
    res = fValidaTecnica(codTecAsist, codTec)
    If res <> 0 Then
        If fUpdateEstTecnicas(CStr(codTecAsist), res) = True Then
            ' se actualiza el estado del nodo de la t�cnica
            Call pUpdateNodoTecnicaEstado(nodoResult.Parent, res, True)
            res = 0 'para saber que no ha habido errores
        Else
            MsgBox "Se ha producido un error en la validaci�n de la t�cnica.", vbError
            objApp.rdoConnect.RollbackTrans
            Exit Sub
        End If
    End If
    
    If res = 0 Then
        objApp.rdoConnect.CommitTrans
        'se actualiza el nodo del resultado: se tiene en cuenta si es un resultado de recuento
        resultado = fFormatearRecuento(resultado$, Val(Mid$(nodoResult.Parent.Key, InStr(nodoResult.Parent.Key, "I") + 1)), False)
        txtResultados.Text = resultado
        desig = Mid$(nodoResult.Tag, InStr(nodoResult.Tag, "/") + 1)
        'TextNodo = Desig & ": " & resultado & Space(5) & fEstadoResultado(estado)
        TextNodo = desig & ": " & resultado & Space(2) & unidad
        If Len(TextNodo) > constMAX_LONG_RESULT Then
            TextNodo = Left$(TextNodo, constVISIBLE_LONG_RESULT) & "..."
        End If
        TextNodo = TextNodo & Space(5) & fEstadoNodo(constESTADORESULT, estado)
        nodoResult.Text = TextNodo
        nodoResult.Tag = estado & Mid$(nodoResult.Tag, InStr(nodoResult.Tag, "U"))
    End If
    
    rsResultados.Close
    qryResultados.Close
               
End Sub


Private Sub pResultReacAntibiotico(CodCol%, codTecAsist%, codAntibiotico%, concentracion$, recom%, infConc%)
' se anotan los datos de la reacci�n del antibi�tico frente al microorganismo
    Dim msg0 As String, msg1 As String, msg2 As String
    Dim cReacc As Integer, concentr As String, recomend As Integer, desigReacc As String
    Dim cMicro As Integer
    Dim res As Boolean
    
    ' se realizan los chequeos de validez de la sensibilidad si se ha identificado la colonia
    cMicro = -1
    Call pBuscarIdentifCol(CodCol, cMicro, -1, -1)
    If cMicro <> -1 Then 'colonia identificada
        'resultado microorganismo (misma sensibilidad siempre)
        cReacc = codReac
        Call pComprobarResultadoAntibiotico(codTecAsist, CodCol, codAntibiotico, cReacc, concentr, recomend, desigReacc, msg0)
        If msg0 <> "" Then
            If MsgBox(msg0, vbInformation + vbYesNo, "Resistencia de Microorganismos") = vbNo Then
            ' se cambian los valores por los que ten�a antes
                codReac = cReacc
                cbossReac.Text = desigReacc
                concentracion = concentr
                txtConc.Text = concentracion
                recom = recomend
                chkRecomendado.Value = -recom
            End If
        End If
            
        If codReac <> -1 Then
            'bacteria resistente
            msg1 = fComprobarBacteriaResistente(cMicro, codAntibiotico, codReac)
            If msg1 <> "" Then
                msg1 = "La sensibilidad definida para los microorganismo frente a antibi�ticos detecta desviaciones:" & Chr$(13) & Chr$(10) & msg1
                If MsgBox(msg1, vbInformation + vbOKCancel, "Resistencia de Microorganismos") = vbCancel Then
                    Exit Sub
                End If
            End If
            
            'relaci�n entre antibi�ticos
            msg2 = fComprobarRelacionAntibioticos(cMicro, CodCol, codAntibiotico, codReac)
            If msg2 <> "" Then
                msg2 = "La relaci�n definida entre antibi�ticos detecta desviaciones:" & Chr$(13) & Chr$(10) & msg2
                If MsgBox(msg2, vbInformation + vbOKCancel, "Relaci�n de Antibi�ticos") = vbCancel Then
                    Exit Sub
                End If
            End If
        End If
    End If
    
    'Se anotan los datos del antibiotico en el antibiograma
    objApp.rdoConnect.BeginTrans
    If fUpdateResultAntibioticos(cMicro, codReac, CodCol, codTecAsist, codAntibiotico, concentracion, recom, infConc) = False Then
        MsgBox "Se ha producido un error en la actualizaci�n de los datos del antibiograma.", vbError
        objApp.rdoConnect.RollbackTrans
    Else
        ' se actualizan los nodos de resultados de antibi�tico
        res = True
        Call pTextoDatosAntibioticos(cMicro, res, nodoResultados, cbossReac.Text, txtConc.Text, chkRecomendado.Value)
        
        If res = False Then
            MsgBox "Se ha producido un error en la actualizaci�n de los datos del antibiograma.", vbError
            objApp.rdoConnect.RollbackTrans
        Else
            objApp.rdoConnect.CommitTrans
            Call pInsertarAntibRelacion(CStr(codAntibiotico), CStr(codReac), CStr(cMicro), nReferencia, CStr(CodCol), CStr(codTecAsist))
        End If
    End If
    
End Sub

Private Sub pResultReincubar(cTecAsist%)
' se anota la reincubaci�n de un cultivo
    Dim SQL As String
    Dim TextNodo As String
    
    On Error Resume Next
    
    'msrdcCultivos.Resultset.Close 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
    objApp.rdoConnect.BeginTrans
    If fReincubar(nReferencia, cTecAsist, codPaso, CondAmbiental) = False Then
        objApp.rdoConnect.RollbackTrans
        MsgBox "Se ha producido un error en la reincubaci�n del cultivo.", vbError
        msrdcCultivos.Refresh 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
        Exit Sub
    Else
        objApp.rdoConnect.CommitTrans
'        codPaso = codPaso + 1
        ' se actualiza el estado del nodo del cultivo
        Call pUpdateNodoTecnicaEstado(nodoResultados, constTECREINCUBADA, False)
        
        Call pEstadoBotones("C", constTECREINCUBADA)
    End If
    'msrdcCultivos.Refresh'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
    
End Sub

Private Sub pReincubarAutomatico(numReferencia$)
' reincuba una placa en las mismas condiciones en las que estaba hasta ahora
    Dim SQL As String
    Dim rsReincAutom As rdoResultset, qryReincAutom As rdoQuery
    Dim res As Boolean
    
    objApp.rdoConnect.BeginTrans
    
    SQL = "SELECT MB2000.MB20_codTecAsist, MB26_codPaso, MB02_codCond"
    SQL = SQL & " FROM MB2000, MB2600"
    SQL = SQL & " WHERE MB20_codPlaca <> 0"
    SQL = SQL & " AND MB34_codEstTecAsist = ?"
    SQL = SQL & " AND MB2000.nRef = ?"
    SQL = SQL & " AND MB2600.nRef = MB2000.nRef"
    SQL = SQL & " AND MB2600.MB20_codTecAsist = MB2000.MB20_codTecAsist"
    SQL = SQL & " AND MB35_codEstSegui = ?"
    Set qryReincAutom = objApp.rdoConnect.CreateQuery("SelTecRealizada", SQL)
        
    qryReincAutom(0) = constTECREALIZADA
    qryReincAutom(1) = numReferencia
    qryReincAutom(2) = constSEGUITECVALIDA
    Set rsReincAutom = qryReincAutom.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    res = True
    If rsReincAutom.EOF = False Then
        SQL = "�Desea Ud. reincubar las placas le�das en las mismas condiciones actuales?"
        If MsgBox(SQL, vbQuestion + vbYesNo, "Reincubaci�n autom�tica") = vbYes Then
            Do While rsReincAutom.EOF = False
                If fReincubar(numReferencia, rsReincAutom(0), rsReincAutom(1), rsReincAutom(2)) = False Then
                    objApp.rdoConnect.RollbackTrans
                    MsgBox "Se ha producido un error en la reincubaci�n autom�tica de cultivos.", vbError
                    res = False
                    Exit Do
                End If
                
                rsReincAutom.MoveNext
            Loop
        End If
    End If
    If res = True Then
        objApp.rdoConnect.CommitTrans
    End If
    
    rsReincAutom.Close
    qryReincAutom.Close
    
End Sub

Private Sub pResultCambiarCondAmb(cTecAsist%)
' se anota el cambio de las condiciones ambientales. NO ES UNA REINCUBACI�N

    Dim SQL As String
    Dim qryCambiarCondAmb As rdoQuery
    
    objApp.rdoConnect.BeginTrans
    
    On Error Resume Next
    
    SQL = "UPDATE MB2600"
    SQL = SQL & " SET MB02_CODCOND = ?"
    SQL = SQL & " WHERE NREF = ?"
    SQL = SQL & " AND MB20_CODTECASIST = ?"
    SQL = SQL & " AND MB35_CODESTSEGUI = ?"
    Set qryCambiarCondAmb = objApp.rdoConnect.CreateQuery("CambiarConAmb", SQL)

    qryCambiarCondAmb(0) = CondAmbiental
    qryCambiarCondAmb(1) = nReferencia
    qryCambiarCondAmb(2) = cTecAsist
    qryCambiarCondAmb(3) = constSEGUITECVALIDA
    qryCambiarCondAmb.Execute
        
    ' se mira si no ha habido ning�n problema
    If Err <> 0 Then
        MsgBox "Se ha producido un error en el cambio de las condiciones ambientales.", vbError
        objApp.rdoConnect.RollbackTrans
    Else
        objApp.rdoConnect.CommitTrans
    End If
    
    qryCambiarCondAmb.Close
    
End Sub

Private Sub pTVAntibioticos()

'****************************************************************************
'*  Clave del texto de antibioticos: "W_Antibioticos"                       *
'*  Clave de los grupos: "G" & CodGrAnti                                    *
'*  Clave de los antibi�ticos: "A" & CodAnti & "G"                          *
'*  Clave de los antibi�ticos en grupos: "A" & CodAnti & "G" & CodGrAnti    *
'****************************************************************************
    Dim AnteriorAntibiotico As Integer
    Dim rsAntib As rdoResultset
    Dim SQL As String
    Dim nodo As Node
    
    'se vacia el tvw
    tvwAntibioticos.Nodes.Clear
    
    AnteriorAntibiotico = -1
    SQL = "SELECT MB0700.MB07_codAnti, MB07_desig, MB3100.MB31_codGrAnti, MB31_desig"
    SQL = SQL & " FROM MB0700,MB3100,MB1700"
    SQL = SQL & " WHERE MB07_indActiva = -1"
    SQL = SQL & " AND MB0700.cDptoSecc = " & departamento
    SQL = SQL & " AND MB1700.MB07_codAnti (+)= MB0700.MB07_codAnti "
    SQL = SQL & " AND MB3100.MB31_codGrAnti (+)= MB1700.MB31_codGrAnti"
    SQL = SQL & " AND MB31_indActiva (+)= -1"
    SQL = SQL & " AND MB3100.cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB07_desig, MB31_desig"
    Set rsAntib = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
    
    If rsAntib.EOF = False Then
        Set nodo = tvwAntibioticos.Nodes.Add(, , "W_Antibioticos", "Antibi�ticos", constIconTipo)
    End If
    
    While rsAntib.EOF = False
        If Not IsNull(rsAntib(2)) Then
            On Error Resume Next
            'Nodos de grupos de antibi�ticos
            Set nodo = tvwAntibioticos.Nodes.Add("W_Antibioticos", tvwPrevious, "G" & rsAntib(2), rsAntib(3), constIconGrAntibiotico)
            Err = 0
            On Error GoTo 0
            'Nodos de antibi�ticos en los grupos
            Set nodo = tvwAntibioticos.Nodes.Add("G" & rsAntib(2), tvwChild, "A" & rsAntib(0) & "G" & rsAntib(2), rsAntib(1), constIconAntibiotico)
        End If
            
        If AnteriorAntibiotico <> rsAntib(0) Then
            'Nodos de antibi�ticos
            Set nodo = tvwAntibioticos.Nodes.Add("W_Antibioticos", tvwChild, "A" & rsAntib(0) & "G", rsAntib(1), constIconAntibiotico)
            AnteriorAntibiotico = rsAntib(0)
        End If
        rsAntib.MoveNext
    Wend
    
    rsAntib.Close
    
End Sub
Private Sub pInsertarColonias(nodoEn As Node, nodoDesde As Node)
'A�ade una colonia a una placa si se cumplen las condiciones (solo se pueden a�adir colonias _
en cultivos no anulados cuyo recipiente = placa

    Dim res As Boolean, i%
    Dim nodoExisteKey$
    Dim cTecAsist%, cRecip%, cPlaca% 'datos del cultivo
    Dim cCol%, identifCol$ 'datos de la colonia
    Dim cMuestra As String
    Dim arTecProt() As typeTecProt
     
    cCol = Val(Mid$(nodoDesde.Key, 2)) 'ser� 0 si la colonia es nueva
    cTecAsist = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "C") + 1))
    cRecip = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "S") + 1))
    cPlaca = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "L") + 1))
    cMuestra = Mid$(tvwMuestras.Nodes(nodoEn.Key).Root.Key, 2)
    
    'se mira si el cultivo est� activo
    If Val(nodoEn.Tag) = constTECANULADA Or Val(nodoEn.Tag) = constTECFINALIZADA Then
        MsgBox "El cultivo est� anulado o finalizado. No se puede a�adir la colonia.", vbExclamation, "A�adir colonia"
        Exit Sub
    End If
        
    'si es un recipiente distinto de placa no se puede identificar la colonia
    If cRecip <> constRECIPPLACA Then
        MsgBox "No se pueden identificar colonias en recipientes que no sean placas.", vbExclamation, "Identificaci�n de colonias"
        Exit Sub
    End If
    
    'se mira si la colonia es nueva o ya existe
    Select Case cCol
        'Colonia nueva
        Case 0
            'se busca el pr�ximo n� de colonia
            cCol = fBuscarNumColonia()
            If cCol = 0 Then 'error
                MsgBox "Se ha producido un error en la b�squeda del n� de colonia.", vbError
                Exit Sub
            End If
            
            'se a�ade la colonia a la tabla de colonias y a colonia-placa
            objApp.rdoConnect.BeginTrans
            res = fInsertColonia(cCol, constCOLLOCALIZADA)
            If res = True Then 'sin error
                res = fInsertColPlaca(cCol, cTecAsist, constCOLPLACAASOCIADA)
            End If
            'se generan las t�cnicas asociadas a la colonia encontrada en el cultivo
            If res = True Then 'sin error
                res = fGenerarTecCult(nReferencia, CStr(cTecAsist), constORIGENCOLONIA, CStr(cCol), cMuestra, arTecProt())
            End If
            
            
            If res = False Then 'error
                MsgBox "Se ha producido un error al a�adir la colonia.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                'nodo de colonia en tvwColonias
                Call pAddNodoColonia(tvwColonias, cCol, "", constCOLLOCALIZADA)
                'nodo de colonia en tvwMuestras
                Call pAddNodoColPlaca(tvwMuestras, cCol, "", constCOLPLACAASOCIADA, cTecAsist, cPlaca, cRecip)
                'Se insertan los nodos de t�cnicas en tvwMuestras
                Call pAddNodoNuevasTecnicas(tvwMuestras, nReferencia, arTecProt(), "K" & cCol & "C" & cTecAsist, cMuestra)
            End If
                                
        'colonia ya existente
        Case Else
            'identificaci�n de la colonia
            If InStr(nodoDesde.Text, ":") > 0 Then
                identifCol = Trim$(Mid$(nodoDesde.Text, InStr(nodoDesde.Text, ":") + 1))
                'identifCol = Trim$(Left$(identifCol, Len(identifCol) - Len(fEstadoColonia(Val(nodoDesde.Tag)))))
                identifCol = Trim$(Left$(identifCol, Len(identifCol) - Len(fEstadoNodo(constESTADOCOL, Val(nodoDesde.Tag)))))
            Else
                identifCol = ""
            End If
                    
            'se mira si la colonia ya existe en la placa y si est� anulada o no
            nodoExisteKey = fExisteColPlaca(nodoEn, nodoDesde.Key)
            If nodoExisteKey <> "" Then 'la colonia existe en la placa
                If Val(tvwMuestras.Nodes(nodoExisteKey).Tag) <> constCOLPLACAANULADA Then 'la colonia no est� anulada
                    MsgBox "La Colonia " & cCol & " ya fue localizada en esta placa.", vbExclamation, "A�adir colonias"
                Else
                    objApp.rdoConnect.BeginTrans
                    If fUpdateEstColoniaPlaca(cTecAsist, cCol, constCOLPLACAASOCIADA) = False Then
                        MsgBox "Se ha producido un error al a�adir la colonia.", vbError
                        objApp.rdoConnect.RollbackTrans
                    Else
                        objApp.rdoConnect.CommitTrans
                        'se actualiza el nodo
                        Call pUpdateNodoColPlacaEstado(tvwMuestras.Nodes(nodoExisteKey), constCOLPLACAASOCIADA)
                        'cambiar el estado de los botones y el frame
                        If tvwMuestras.SelectedItem = tvwMuestras.Nodes(nodoExisteKey) Then
                            Call pEstadoBotones("K", Val(nodoDesde.Tag))
                        End If
                    End If
                End If
            Else
                objApp.rdoConnect.BeginTrans
                res = fInsertColPlaca(cCol, cTecAsist, constCOLPLACAASOCIADA)
                If res = True Then 'sin error
                    res = fGenerarTecCult(nReferencia, CStr(cTecAsist), constORIGENCOLONIA, CStr(cCol), cMuestra, arTecProt())
                End If
                
                If res = False Then
                    MsgBox "Se ha producido un error al a�adir la colonias.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    'se a�ade el nodo de colonia en el tvwMuestras
                    Call pAddNodoColPlaca(tvwMuestras, cCol, identifCol, constCOLPLACAASOCIADA, cTecAsist, cPlaca, cRecip)
                    'Se insertan los nodos de t�cnicas en tvwMuestras
                    Call pAddNodoNuevasTecnicas(tvwMuestras, nReferencia, arTecProt(), "K" & cCol & "C" & cTecAsist, cMuestra)
                End If
            End If
    End Select
    
End Sub

Private Sub pInsertarMorfologias(nodoEn As Node, nodoDesde As Node)
'Inserta una morfolog�a como resultado de una t�cnica
    
    Dim i%
    Dim nodo As Node
    Dim KeyNodo As String
    Dim cMorf%, numLoc%, desigMorf$ 'datos de la morfolog�a
    Dim cMuestra$
    Dim cCol%
    
    cMorf = Val(Mid$(nodoDesde.Key, 2))
            
    Select Case Left$(nodoEn.Key, 1)
'        'A�adir morfolog�a
'        Case "X", "F", "M"
'            'Se establece el nodo de texto de microorganismos como nodo referencia si no lo era
'            Select Case Left$(nodoEn.Key, 1)
'            Case "M"
'                Set nodoEn = tvwMuestras.Nodes("X" & Mid$(nodoEn.Key, 2))
'            Case "F"
'                Set nodoEn = nodoEn.Parent
'            End Select
'
'            'Se busca el n� de localizaciones del microorganismo(morfologia)
'            numLoc = 0
'            KeyNodo = nodoDesde.Key & nodoEn.Key 'clave del nodo a insertar
'            For i = 1 To nodoEn.Children
'                If i = 1 Then
'                    Set nodo = nodoEn.Child
'                Else
'                    Set nodo = nodo.Next
'                End If
'                If nodo.Key = KeyNodo Then
'                    numLoc = Val(nodo.Tag)
'                    Exit For
'                End If
'            Next i
'
'            'Se a�ade la morfolog�a
'            cMuestra = Mid$(nodoEn.Key, 2)
'
'            objApp.rdoConnect.BeginTrans
'            If fInsertMorfologias(cMorf, numLoc, cMuestra) = False Then
'                MsgBox "Se ha producido un error al a�adir morfolog�as.", vbError
'                objApp.rdoConnect.RollbackTrans
'            Else
'                objApp.rdoConnect.CommitTrans
'                If numLoc = 0 Then
'                    Call pAddNodoMorfologia(tvwMuestras, cMorf, nodoDesde.Text, 1, cMuestra)
'                Else
'                    nodo.Text = nodoDesde.Text & "   (" & numLoc + 1 & " localiz.)"
'                    nodo.Tag = numLoc + 1 & "/" & nodoDesde.Text
'                End If
'
'            End If
        
        'A�adir morfolog�a como resultado
        Case "R"
            'si el resultado est� anulado o ya est� informado, no se puede cambiar
            If Val(nodoEn.Tag) = constRESULTINFORMADO Or Val(nodoEn.Tag) = constRESULTANULADO Then
                Exit Sub
            Else
                If Left$(nodoEn.Parent.Parent.Key, 1) = "K" Then 'deber� ser una t�cnica sobre colonia
                    'se muestra el frame de los resultados de t�cnicas
                    Set tvwMuestras.SelectedItem = nodoEn
                    tvwMuestras_NodeClick nodoEn
                    'Set tvwMuestras.DropHighlight = nodoEn
                    
                    cCol = Val(Mid$(nodoEn.Parent.Parent.Key, 2))
                    desigMorf = nodoDesde.Text
                    
                    'se anota el resultado de la t�cnica
                    txtResultados.Text = desigMorf
                    Call pResultResultadoTecnica(nodoEn, desigMorf, constRESULTVALIDADO)
                
                    'se anota la identificaci�n de la colonia
                    objApp.rdoConnect.BeginTrans
                    If fUpdateIdentifCol(constCOLVALIDADA, -1, cMorf, -1, cCol) = False Then
                        MsgBox "Se ha producido un error en la identificaci�n de colonias.", vbError
                        objApp.rdoConnect.RollbackTrans
                    Else
                        objApp.rdoConnect.CommitTrans
                        
                        'NO HAY QUE PONER LA IDENTIFICACI�N DE LA COLONIA SI YA ES M�S CONCRETA
                        
                        'nodo del tvwMuestras
                        For i = 1 To tvwMuestras.Nodes.Count
                            If Left$(tvwMuestras.Nodes(i).Key, Len(CStr(cCol)) + 1) = "K" & cCol Then
                                Call pUpdateNodoColPlaca(tvwMuestras.Nodes(i), cCol, desigMorf, Val(tvwMuestras.Nodes(i).Tag))
                            End If
                        Next i
                        'nodo del tvwColonias
                        Call pUpdateNodoColonia(tvwColonias.Nodes("K" & cCol), cCol, desigMorf, constCOLVALIDADA)
                    End If
                End If
            End If
    End Select
    
End Sub

Private Sub pIndicarMorfologias(nodoEn As Node, nodoDesde As Node)
'Modifica el n� de localizaciones de una morfolog�a encontrada en la muestra _
    por observaci�n directa. Se pregunta el n� de localizaciones mediante un InputBox y se _
    hace un insert, un update o un delete en funci�n del n� de localizaciones actuales
    
    Dim i%
    Dim nodo As Node
    Dim KeyNodo As String
    Dim cMorf%, numLoc%, numActualLoc% 'datos de la morfolog�a
    Dim cMuestra$
    
    cMorf = Val(Mid$(nodoDesde.Key, 2))
            
    Select Case Left$(nodoEn.Key, 1)
        'A�adir morfolog�a
        Case "X", "F", "M"
            'Se establece el nodo de texto de microorganismos como nodo referencia si no lo era
            Select Case Left$(nodoEn.Key, 1)
            Case "M"
                Set nodoEn = tvwMuestras.Nodes("X" & Mid$(nodoEn.Key, 2))
            Case "F"
                Set nodoEn = nodoEn.Parent
            End Select
            
            'se utiliza la variabe cMuestra para recoger el n� de localizaciones introducidas por el ususario
            cMuestra = InputBox("�Cuantos microorganismos '" & nodoDesde.Text & "' se han localizado?", "Microorganismos", 1)
            If cMuestra = "" Then
                Exit Sub
            ElseIf cMuestra = "0" Then
                numLoc = 0
            Else
                If Val(cMuestra) = 0 Then
                    numLoc = 1
                Else
                    numLoc = Val(cMuestra)
                End If
            End If
            If numLoc < 0 Then numLoc = 1
                
            'Se busca el n� de localizaciones del microorganismo(morfologia)
            numActualLoc = 0
            KeyNodo = nodoDesde.Key & nodoEn.Key 'clave del nodo a insertar
            For i = 1 To nodoEn.Children
                If i = 1 Then
                    Set nodo = nodoEn.Child
                Else
                    Set nodo = nodo.Next
                End If
                If nodo.Key = KeyNodo Then
                    numActualLoc = Val(nodo.Tag)
                    Exit For
                End If
            Next i
            
            'Se a�ade, modifica o elimina la morfolog�a
            cMuestra = Mid$(nodoEn.Key, 2)
            
            objApp.rdoConnect.BeginTrans
            If numLoc > 0 And numActualLoc = 0 Then 'Insertar
                If fMorfologiasInsert(cMorf, numLoc, cMuestra) = False Then
                    MsgBox "Se ha producido un error al actualizar morfolog�as.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    Call pAddNodoMorfologia(tvwMuestras, cMorf, nodoDesde.Text, numLoc, cMuestra)
                End If
            ElseIf numLoc > 0 And numActualLoc > 0 Then 'Actualizar
                If fMorfologiasUpdate(cMorf, numLoc, cMuestra) = False Then
                    MsgBox "Se ha producido un error al actualizar morfolog�as.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    nodo.Text = nodoDesde.Text & "   (" & numLoc & " localiz.)"
                    nodo.Tag = numLoc & "/" & nodoDesde.Text
                End If
            ElseIf numLoc = 0 And numActualLoc > 0 Then 'Eliminar
                If fMorfologiasDelete(cMorf, cMuestra) = False Then
                    MsgBox "Se ha producido un error al actualizar morfolog�as.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    tvwMuestras.Nodes.Remove nodo.Key
                End If
            End If
            
    End Select
    
End Sub


Private Sub pInsertarProtocolos(nodoEn As Node, nodoDesde As Node)
'Inserta las t�cnicas de los protocolos seleccionados en el tvwMuestras y en la BD
    Dim SQL As String, res As Integer
    Dim nodo As Node
    Dim cProtocolo$, cMuestra$, cOrigen%, codTecOrig%
    Dim arTecProt() As typeTecProt, dimension As Integer, i As Integer
    'Dim rsTec As rdoResultset, qryTec As rdoQuery
    
    'Se generan las t�cnicas correspondientes al protocolo seleccionado
    'Clave del nodo arrastrado (NodoDesde): "P" & CodProt
    objApp.rdoConnect.BeginTrans
    cProtocolo = Val(Mid$(nodoDesde.Key, 2))
    
    Select Case Left$(nodoEn.Key, 1)
        Case "M" 'sobre muestra
            'Clave del nodo sobre el que se arrastra (NodoEn): "M" & cMuestra
            cMuestra = Trim$(Mid$(nodoEn.Key, 2))
            res = fGenerarTecProt(CStr(nReferencia), cProtocolo, constORIGENMUESTRA, cMuestra, cMuestra, arTecProt())
        
        Case "K" 'sobre colonia
            'se mira si la colonia est� activa
            If Val(nodoEn.Tag) = constCOLPLACAANULADA Then
                MsgBox "La colonia est� anulada. No se puede a�adir el protocolo.", vbExclamation, "A�aidr protocolo"
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            End If
            
            'Clave del nodo sobre el que se arrastra (NodoEn): "K" & CodCol & "C" & CodTecAsist
            cOrigen = Val(Mid$(nodoEn.Key, 2))
            codTecOrig = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "C") + 1))
            Set nodo = nodoEn.Root
            cMuestra = Trim$(Mid$(nodo.Key, 2))
            res = fGenerarTecProt(CStr(nReferencia), cProtocolo, constORIGENCOLONIA, CStr(cOrigen), cMuestra, arTecProt(), CStr(codTecOrig))
        
        Case "C" 'sobre cultivo
            'se mira si el cultivo est� activo
            If Val(nodoEn.Tag) = constTECANULADA Or Val(nodoEn.Tag) = constTECFINALIZADA Then
                MsgBox "El cultivo est� anulado o finalizado. No se puede a�adir el protocolo.", vbExclamation, "A�adir protocolo"
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            End If
            
            'Clave del nodo sobre el que se arrastra (NodoEn): "C" & CodTecAsist & "L" & CodPlaca & "S" & CodRecip
            cOrigen = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "L") + 1))
            codTecOrig = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "C") + 1))
            Set nodo = nodoEn.Root
            cMuestra = Trim$(Mid$(nodo.Key, 2))
            res = fGenerarTecProt(CStr(nReferencia), cProtocolo, constORIGENPLACA, CStr(cOrigen), cMuestra, arTecProt(), CStr(codTecOrig))
        
        Case Else
            objApp.rdoConnect.RollbackTrans
            Exit Sub
    End Select
    
    If res = False Then
        MsgBox "Se ha producido un error en la aplicaci�n de protocolos.", vbError
        objApp.rdoConnect.RollbackTrans
    Else
        objApp.rdoConnect.CommitTrans
        
        'Se insertan los nodos
        Call pAddNodoNuevasTecnicas(tvwMuestras, nReferencia, arTecProt(), nodoEn.Key, cMuestra)
    End If
    
End Sub


Private Sub pInsertarTecnicas(nodoEn As Node, nodoDesde As Node, Protocolo As Boolean)
'Inserta las t�cnicas seleccionadas en el tvwMuestras y en la BD
    Dim SQL As String, nodo As Node, res As Integer
    Dim cMuestra As String, cOrigen As Integer, codTecOrig As Integer
    Dim codTec As Integer, cCondiciones As Integer, numVeces As Integer, tipoTecnica As Integer
    Dim arTecProt() As typeTecProt, dimension As Integer, i As Integer
    Dim rsCondiciones As rdoResultset
    Dim cultivo As Boolean
    
    'Se generan las t�cnicas seleccionadas
    res = True
    objApp.rdoConnect.BeginTrans
    'Claves de los posibles nodos arrastrados (NodoDesde):
    '       "T" & CodTec (& "B" & CodCond & "N" & numVeces & "P" & CodProt) & "I" & codTiTec
    tipoTecnica = Val(Mid$(nodoDesde.Key, InStr(nodoDesde.Key, "I") + 1))
    If tipoTecnica = constCULTIVO Then cultivo = True Else cultivo = False
    codTec = Val(Mid$(nodoDesde.Key, 2))
    
    If Protocolo = True Then
        cCondiciones = Val(Mid$(nodoDesde.Key, InStr(nodoDesde.Key, "B") + 1))
        numVeces = Val(Mid$(nodoDesde.Key, InStr(nodoDesde.Key, "N") + 1))
    Else
'        SQL = "SELECT MIN(MB02_codCond) FROM MB0200"
'        Set rsCondiciones = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
'        If rsCondiciones.EOF = False Then
'            cCondiciones = rsCondiciones(0)
'        Else
'            MsgBox "No existe ninguna condici�n ambiental definida.", vbError
'            objApp.rdoConnect.RollbackTrans
'            Exit Sub
'        End If
        cCondiciones = fBuscarCondAmbientales(codTec)
        If cCondiciones = 0 Then
            MsgBox "No existe ninguna condici�n ambiental definida.", vbError
            objApp.rdoConnect.RollbackTrans
            Exit Sub
        End If
        numVeces = 1
    End If
    
    Select Case Left$(nodoEn.Key, 1)
        Case "M" 'sobre muestra
            If fPosibleAplicarTecnica(constORIGENMUESTRA, CStr(tipoTecnica)) Then
                'Clave del nodo sobre el que se arrastra (NodoEn): "M" & cMuestra
                cMuestra = Trim$(Mid$(nodoEn.Key, 2))
                res = fGenerarTecnica(CStr(nReferencia), CStr(codTec), CStr(cCondiciones), numVeces, constORIGENMUESTRA, cMuestra, cMuestra, cultivo, arTecProt())
            Else
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            End If
        
        Case "K" 'sobre colonia
            'se mira si la colonia est� activa
            If Val(nodoEn.Tag) = constCOLPLACAANULADA Then
                MsgBox "La colonia est� anulada. No se puede a�adir la t�cnica.", vbExclamation, "A�adir t�cnica"
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            End If
            
            If fPosibleAplicarTecnica(constORIGENCOLONIA, CStr(tipoTecnica)) Then
                'Clave del nodo sobre el que se arrastra (NodoEn): "K" & CodCol & "C" & CodTecAsist
                cOrigen = Val(Mid$(nodoEn.Key, 2))
                codTecOrig = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "C") + 1))
                Set nodo = nodoEn.Root
                cMuestra = Trim$(Mid$(nodo.Key, 2))
                res = fGenerarTecnica(CStr(nReferencia), CStr(codTec), CStr(cCondiciones), numVeces, constORIGENCOLONIA, CStr(cOrigen), cMuestra, cultivo, arTecProt(), CStr(codTecOrig))
            Else
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            End If
        
        Case "C" 'sobre cultivo
            'Clave del nodo sobre el que se arrastra (NodoEn): "C" & CodTecAsist & "L" & CodPlaca & "S" & CodRecip
            If cultivo = True Then
                'se mira si el cultivo est� activo
                If Val(nodoEn.Tag) = constTECANULADA Or Val(nodoEn.Tag) = constTECFINALIZADA Then
                    MsgBox "El cultivo est� anulado o finalizado. No se puede resembrar.", vbExclamation, "Resiembra"
                    objApp.rdoConnect.RollbackTrans
                    Exit Sub
                End If
            
                codTecOrig = Val(Mid$(nodoEn.Key, 2))
                cOrigen = Val(Mid$(nodoEn.Key, InStr(nodoEn.Key, "L") + 1))
                Set nodo = nodoEn.Root
                cMuestra = Trim$(Mid$(nodo.Key, 2))
                res = fGenerarTecnica(CStr(nReferencia), CStr(codTec), CStr(cCondiciones), numVeces, constORIGENPLACA, CStr(cOrigen), cMuestra, cultivo, arTecProt(), CStr(codTecOrig))
            Else
                objApp.rdoConnect.RollbackTrans
                Exit Sub
            End If
        
        Case Else
            objApp.rdoConnect.RollbackTrans
            Exit Sub
    End Select
    
    If res = False Then
        MsgBox "Se ha producido un error en la aplicaci�n de t�cnicas.", vbError
        objApp.rdoConnect.RollbackTrans
    Else
        objApp.rdoConnect.CommitTrans
        
        'Se insertan los nodos
        Call pAddNodoNuevasTecnicas(tvwMuestras, nReferencia, arTecProt(), nodoEn.Key, cMuestra)
    End If
    
End Sub


Private Sub pTVColonias()

    Dim rsCol As rdoResultset, qryCol As rdoQuery
    Dim SQL As String
    Dim nodo As Node
    Dim identif$
    
    'Se limpia el tvw
    tvwColonias.Nodes.Clear
    
    SQL = "SELECT MB27_codCol, nvl(MB18_desig,' '), nvl(MB11_desig,' '), nvl(MB12_desig,' '), MB36_codEstCol"
    SQL = SQL & " FROM MB2700, MB1800, MB1100, MB1200"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB1800.MB18_codMicro (+)= MB2700.MB18_codMicro"
    SQL = SQL & " AND MB1100.MB11_codGeMicro (+)= MB2700.MB11_codGeMicro"
    SQL = SQL & " AND MB1200.MB12_codMorf (+)= MB2700.MB12_codMorf"
    SQL = SQL & " ORDER BY MB27_codCol"
    Set qryCol = objApp.rdoConnect.CreateQuery("TVColonias", SQL)
    
    qryCol(0) = nReferencia
    Set rsCol = qryCol.OpenResultset(rdOpenForwardOnly)
    
    Set nodo = tvwColonias.Nodes.Add(, , "KNueva", "Colonia nueva", constIconColonia) 'nodo colonia nueva
    
    Do While rsCol.EOF = False
        ' se a�aden los nodos al tvwColonias
        identif = fIdentifCol(Trim$(rsCol(1)), Trim$(rsCol(2)), Trim$(rsCol(3)))
        Call pAddNodoColonia(tvwColonias, rsCol(0), identif, rsCol(4))
        
        rsCol.MoveNext
    Loop
    
    rsCol.Close
    qryCol.Close

End Sub
Private Sub pTVTecnicas()

'***************************************************************
'*  Clave de los grupos(tipos) de t�cnicas: "I" & CodTiTec     *
'*  Clave de las t�cnicas: "T" & CodTec & "I" & CodTiTec       *
'***************************************************************
    Dim AnteriorTipoTec As Integer
    Dim rsTec As rdoResultset, qryTec As rdoQuery
    Dim SQL As String
    Dim nodo As Node
    
    'Se limpia el tvw
    tvwTecnicas.Nodes.Clear
    
    'Preparaci�n del rdoQuery
    SQL = "SELECT MB0400.MB04_codTiTec, MB04_desig, MB0900.MB09_codTec, MB09_desig"
    SQL = SQL & " FROM MB0400,MB0900"
    SQL = SQL & " WHERE MB04_indActiva = -1"
    SQL = SQL & " AND MB0900.MB04_codTiTec = MB0400.MB04_codTiTec"
    SQL = SQL & " AND MB09_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = ?"
    SQL = SQL & " ORDER BY MB04_desig,MB0400.MB04_codTiTec,MB09_desig"
    Set qryTec = objApp.rdoConnect.CreateQuery("", SQL)
    
    'Se llena el tvw
    AnteriorTipoTec = -1 'Se utiliza para establecer los nodos de tipos de t�cnicas sin forzar
                         'el error por la repetici�n de la clave
    qryTec(0) = departamento
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly)
    
    Do While rsTec.EOF = False
        'tipos
        If AnteriorTipoTec <> rsTec(0) Then
'            If rsTec(0) = constCULTIVO Then 'cultivos
'                Set nodo = tvwTecnicas.Nodes.Add(, tvwFirst, "I" & rsTec(0), rsTec(1), constIconTipo)
'            Else
                Set nodo = tvwTecnicas.Nodes.Add(, , "I" & rsTec(0), rsTec(1), constIconTipo)
'            End If
            AnteriorTipoTec = rsTec(0)
        End If
        't�cnicas
        If rsTec(0) = constCULTIVO Then 'cultivos
            Set nodo = tvwTecnicas.Nodes.Add("I" & rsTec(0), tvwChild, "T" & rsTec(2) & "I" & rsTec(0), rsTec(3), constIconPlaca)
        Else
            Set nodo = tvwTecnicas.Nodes.Add("I" & rsTec(0), tvwChild, "T" & rsTec(2) & "I" & rsTec(0), rsTec(3), constIconTecnica)
        End If
        rsTec.MoveNext
    Loop
    
    rsTec.Close
    qryTec.Close
End Sub
Private Sub pTVProtocolos()

'***********************************************************************************************************
'*  Clave de los tipos de protocolos: "U" & CodTiProt
'*  Clave de los protocolos: "P" & CodProt                                                                 *
'*  Clave de las t�cnicas: "T" & CodTec & "B" & CodCond & "N" & numVeces & "P" & CodProt & "I" & CodTiTec  *
'***********************************************************************************************************
    Dim AnteriorTiProt As Integer
    Dim AnteriorProt As Integer
    Dim rsProt As rdoResultset, qryProt As rdoQuery
    Dim SQL As String, veces As String
    Dim nodo As Node
    Dim TextNodo As String, KeyNodo As String, relacNodo As String
     
    'Se vacia el tvw
    tvwProtocolos.Nodes.Clear
    
    'Preparaci�n del rdoQuery
    SQL = "SELECT MB0500.MB05_codProt, MB05_desig, MB0900.MB09_codTec, MB09_desig,"
    SQL = SQL & " MB14_numVeces, MB0200.MB02_codCond, MB02_desig, MB04_codTiTec,"
    SQL = SQL & " MB4200.MB42_codTiProt, MB42_desig"
    SQL = SQL & " FROM MB1400,MB0500,MB0900,MB0200,MB4200"
    SQL = SQL & " WHERE MB14_indActiva = -1"
    SQL = SQL & " AND MB0500.MB05_codProt = MB1400.MB05_codProt"
    SQL = SQL & " AND MB0900.MB09_codTec = MB1400.MB09_codTec"
    SQL = SQL & " AND MB0200.MB02_codCond = MB1400.MB02_codCond"
    SQL = SQL & " AND MB4200.MB42_codTiProt = MB0500.MB42_codTiProt"
    SQL = SQL & " AND MB42_indActiva = -1"
    SQL = SQL & " AND MB05_indActiva = -1"
    SQL = SQL & " AND MB09_indActiva = -1"
    SQL = SQL & " AND MB0500.cDptoSecc = ?"
    SQL = SQL & " AND MB02_indActiva = -1"
    SQL = SQL & " ORDER BY MB42_desig,MB4200.MB42_codTiProt,MB05_desig,MB0500.MB05_codProt,MB09_desig"
    Set qryProt = objApp.rdoConnect.CreateQuery("", SQL)
    
    'Se llena el tvw
    AnteriorTiProt = -1   'Se utiliza para establecer los nodos de tipos deprotocolos sin
                        'forzar el error por la repetici�n de la clave
    AnteriorProt = -1   'Se utiliza para establecer los nodos de protocolos sin forzar
                        'el error por la repetici�n de la clave
    qryProt(0) = departamento
    Set rsProt = qryProt.OpenResultset(rdOpenForwardOnly)
    Do While rsProt.EOF = False
        If AnteriorTiProt <> rsProt(8) Then
            Set nodo = tvwProtocolos.Nodes.Add(, , "U" & rsProt(8), rsProt(9), constIconTipo)
            AnteriorTiProt = rsProt(8)
        End If
        If AnteriorProt <> rsProt(0) Then
            Set nodo = tvwProtocolos.Nodes.Add("U" & rsProt(8), tvwChild, "P" & rsProt(0), rsProt(1), constIconProtocolo)
            AnteriorProt = rsProt(0)
        End If
        
        relacNodo = "P" & rsProt(0)
        KeyNodo = "T" & rsProt(2) & "B" & rsProt(5) & "N" & rsProt(4) & "P" & rsProt(0) & "I" & rsProt(7)
        If rsProt(4) = 1 Then veces = " vez" Else veces = " veces"
        TextNodo = rsProt(3) & Space(5) & rsProt(6) & Space(5) & "(" & rsProt(4) & veces & ")"
        
        If rsProt(7) = constCULTIVO Then
            Set nodo = tvwProtocolos.Nodes.Add(relacNodo, tvwChild, KeyNodo, TextNodo, constIconPlaca)
        Else
            Set nodo = tvwProtocolos.Nodes.Add(relacNodo, tvwChild, KeyNodo, TextNodo, constIconTecnica)
        End If
        rsProt.MoveNext
    Loop
    
    rsProt.Close
    qryProt.Close
    
End Sub
  

Private Sub pTextoDatosAntibioticos(codMicro%, res As Boolean, nodoRes As Node, reaccion$, concentracion$, recomendado%)
    Dim i As Integer, SQL As String
    Dim nodo As Node, nodoTecAntib As Node
    Dim codTecAsist As Integer
    Dim arAntibValidados() As Node, antibValidados As String
    Dim arAntibioticosResult() As Node
    Dim dimension1 As Integer, dimension2 As Integer
    Dim textoNodo As String
    Dim qryTecAsist As rdoQuery
    Dim cCol As Integer
    
    'En TextoNodo se obtiene el nombre del antibi�tico. Para mayor seguridad se puede obtener
    'de la base de datos con el nodoRes.key
    textoNodo = Mid$(nodoRes.Tag, 2) & ": "
'    If cbossReac.Text <> "" Then
'        textoNodo = textoNodo & cbossReac.Text
'    End If
'    If txtConc.Text <> "" Then
'        textoNodo = textoNodo & "    Conc.: " & txtConc.Text
'    End If
'    If chkRecomendado.Value = 0 Then
'        textoNodo = textoNodo & "    Infor.: NO"
'    Else
'        textoNodo = textoNodo & "    Infor.: SI"
'    End If
    textoNodo = textoNodo & reaccion
    If concentracion <> "" Then
        textoNodo = textoNodo & "    Conc.: " & concentracion
    End If
    If recomendado = 0 Then
        textoNodo = textoNodo & "    Infor.: NO"
    Else
        textoNodo = textoNodo & "    Infor.: SI"
    End If
    
    
    ' si el microorganismo est� identificado se cambia el texto de todos los nodos en los _
    que aparece el antibi�tico seleccionado en la colonia tratada; si no lo est�, se cambia s�lo en el nodo _
    que se est� introduciendo el resultado
    On Error Resume Next
                        
    If codMicro <> -1 Then 'microorg. identificado
        cCol = Val(Mid$(nodoRes.Parent.Parent.Key, 2))
        For i = 1 To tvwMuestras.Nodes.Count
            Set nodo = tvwMuestras.Nodes(i)
            If Left$(nodo.Key, 1) = "A" Then
                If Val(Mid$(nodo.Key, 2)) = Val(Mid$(nodoRes.Key, 2)) Then
                    'se mira si es de la colonia tratada
                    If cCol = Val(Mid$(nodo.Parent.Parent.Key, 2)) Then
                        ' se mira si la t�cnica de antibiograma est� validada
                        Set nodoTecAntib = nodo.Parent
                        codTecAsist = Val(Mid$(nodoTecAntib.Key, InStr(nodoTecAntib.Key, "T") + 1))
                        
                        ' se anotan las t�cnicas que han quedado validadas
                        If fValidaAntibiograma(codTecAsist) = True Then
                            dimension1 = UBound(arAntibValidados)
                            dimension1 = dimension1 + 1
                            ReDim Preserve arAntibValidados(1 To dimension1)
                            Set arAntibValidados(dimension1) = nodoTecAntib
                            antibValidados = antibValidados & codTecAsist & ","
                        End If
                        
                        ' se anotan los nodos de antibi�ticos que hay que cambiar de texto
                        dimension2 = UBound(arAntibioticosResult)
                        dimension2 = dimension2 + 1
                        ReDim Preserve arAntibioticosResult(1 To dimension2)
                        Set arAntibioticosResult(dimension2) = nodo
    '                   nodo.Text = Trim$(SQL)
                    End If
                End If
            End If
        Next i
    Else
        ' se mira si la t�cnica de antibiograma est� validada
        Set nodoTecAntib = nodoRes.Parent
        codTecAsist = Val(Mid$(nodoTecAntib.Key, InStr(nodoTecAntib.Key, "T") + 1))
        ' se anotan la t�cnica si han quedado validada
        If fValidaAntibiograma(codTecAsist) = True Then
            ReDim arAntibValidados(1 To 1)
            Set arAntibValidados(1) = nodoTecAntib
            antibValidados = antibValidados & codTecAsist & ","
        End If
        ' se anotan el nodo de antibi�tico que hay que cambiar de texto
        ReDim arAntibioticosResult(1 To 1)
        Set arAntibioticosResult(1) = nodoRes
'
'        nodoRes.Text = Trim$(SQL)
    End If
    
    ' se actualiza el estado de las t�cnicas que han quedado validadas
    If antibValidados <> "" Then
        Err = 0
        antibValidados = Left$(antibValidados, Len(antibValidados) - 1)
        SQL = "UPDATE MB2000"
        SQL = SQL & " SET MB34_codEstTecAsist = ?"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND MB20_codTecAsist IN (" & antibValidados & ")"
        Set qryTecAsist = objApp.rdoConnect.CreateQuery("TecAsist", SQL)
            
        qryTecAsist(0) = constTECVALIDADA
        qryTecAsist(1) = nReferencia
        qryTecAsist.Execute
        
        If Err = 0 Then
            For i = 1 To UBound(arAntibValidados)
                Call pUpdateNodoTecnicaEstado(arAntibValidados(i), constTECVALIDADA, True)
            Next i
        Else
            res = False
        End If
        
        qryTecAsist.Close
    End If
    
    If res = True Then
        For i = 1 To UBound(arAntibioticosResult)
            arAntibioticosResult(i).Text = textoNodo
        Next i
    End If
    
End Sub
Private Sub cbossCondiciones_Click()

'Se anota la condicion seleccionada
CondAmbiental = cbossCondiciones.Columns(0).Text

End Sub

Private Sub cbossIdentificacion_Click(Index As Integer)
    
    'Se anota la identificaci�n seleccionada
    IdentifMicro(Index) = cbossIdentificacion(Index).Columns(0).Text
    
    'Se llena el combo de microorg. en funci�n del g�nero o morfolog�a seleccionado
    If Index < 2 Then
        Call pLlenarComboMicro(IdentifMicro(0), IdentifMicro(1))
    End If
    
End Sub

Private Sub cbossIdentificacion_GotFocus(Index As Integer)
    Select Case Index
    Case 0
        Call pBotonLista(True, constMANTGENEROS)
    Case 1
        Call pBotonLista(True, constMANTMORFOLOGIAS)
    Case 2
        Call pBotonLista(True, constMANTMICROORGANISMOS)
    End Select
End Sub

Private Sub cbossIdentificacion_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 Then
        cbossIdentificacion(Index).Text = ""
        IdentifMicro(Index) = -1
        
        'Se llena el combo de microorg. en funci�n del g�nero o morfolog�a seleccionado
        If Index < 2 Then
            Call pLlenarComboMicro(IdentifMicro(0), IdentifMicro(1))
        End If
    End If
 
End Sub


Private Sub cbossIdentificacion_LostFocus(Index As Integer)
    Call pBotonLista(False, constMANTNINGUNO)
End Sub

Private Sub cbossReac_Click()

'Se anota la reacci�n seleccionada
codReac = cbossReac.Columns(0).Text

End Sub

Private Sub cbossReac_GotFocus()
    Call pBotonLista(True, constMANTREFSENSIBILIDADES)
End Sub

Private Sub cbossReac_KeyDown(KeyCode As Integer, Shift As Integer)
'Al pulsar ENTER se aceptan los valores establecidos para el antibi�tico
'Al pulsar SUPR se elimina el antibi�tico
    On Error Resume Next
    Select Case KeyCode
        Case 13 'validar sensibildad
            cmdResultados_Click (0)
        Case 46 'eliminar antibi�tico
            Call tvwMuestras_KeyDown(KeyCode, Shift)
    End Select
    
End Sub


Private Sub cbossReac_LostFocus()
    Call pBotonLista(False, constMANTNINGUNO)
End Sub

Private Sub cmdPeticion_Click(Index As Integer)
    Dim SQL As String
    Dim nodo As Node, i As Integer
    Dim numRef As String
    Dim msg, msg1 As String
    
    Select Case Index
        Case 0 'Paciente
            pVentanaPaciente (txtPruebaAsistencia(8).Text)
 
        Case 1 'Finalizar placa
            'si es la �ltima placa se chequea la correspondencia entre las morfolog�as _
            encontradas por observaci�n directa y las morfolog�as identificadas posteriormente
            If fUltimaPlacaTirada(nodoResultados) = True Then
                'parametros: cMuestra
                msg1 = fComprobarMicroMorfo(Mid$(nodoResultados.Root.Key, 2))
                If msg1 <> "" Then
                    msg = "El cultivo que va a finalizar es el �ltimo de los disponibles y se han"
                    msg = msg & " identificado menos microorganismos de los"
                    msg = msg & " esperados:" & Chr$(13) & Chr$(10) & msg1
                    msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
                End If
            End If
            msg = msg & "�Desea finalizar el cultivo?"

            If MsgBox(msg, vbQuestion + vbYesNo, "Finalizar placas") = vbYes Then
                'On Error Resume Next 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
                'msrdcCultivos.Resultset.Close 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
                'On Error GoTo 0 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
                objApp.rdoConnect.BeginTrans
                'parametros: cTecAsist, estado
                If fUpdateEstCultivos(CStr(Val(Mid$(nodoResultados.Key, 2))), constTECFINALIZADA) = False Then
                    MsgBox "Se ha producido un error en la anulaci�n del cultivo.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    ' se actualiza el estado del nodo del cultivo
                    Call pUpdateNodoTecnicaEstado(nodoResultados, constTECFINALIZADA, True)

                    Call pEstadoBotones("C", constTECFINALIZADA)
                End If
            End If
            
        Case 2 'Seleccionar n� ref
            numRef = fVentanaNRef()
            If numRef = "" Then
                Exit Sub
            End If
            
            SQL = "Proceso=" & constPROCESOSOLICITUD
            'la prueba localizada puede ser de cualquier carpeta del Dpto, independientemente _
            de la que se haya elegido al entrar en la pantalla
            SQL = SQL & " AND cCarpeta IN "
            SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
            SQL = SQL & " AND (estado = " & constPRUEBAREALIZANDO
            SQL = SQL & " OR estado = " & constPRUEBAVALIDADA & ")"
            SQL = SQL & " AND nRef = '" & numRef & "'"
            
            Screen.MousePointer = vbHourglass
            objPruebaAsistencia.strWhere = SQL
            Call objWinInfo.DataRefresh
            Screen.MousePointer = vbDefault
            
        Case 3 'pruebas hoy
            Screen.MousePointer = vbHourglass
            objPruebaAsistencia.strWhere = whereInicial
            Call objWinInfo.DataRefresh
            Screen.MousePointer = vbDefault
        
        Case 4 'finalizar prueba
            'On Error Resume Next 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
            'msrdcTecnicas.Resultset.Close 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
            'msrdcCultivos.Resultset.Close 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
            'On Error GoTo 0 'ERROR MSRDC SOLUCIONADO EN GOTFOCUS Y LOSTFOCUS DE TEXTBOX
            
            objApp.rdoConnect.BeginTrans
            i = fFinalizarPrueba()
            If i = 0 Then '0 - error en las operaciones sobre la base de datos
                MsgBox "Se ha producido un error en la finalizaci�n de la prueba.", vbError
                objApp.rdoConnect.RollbackTrans
            ElseIf i = 1 Then '1 - operaci�n de finalizaci�n correcta
                objApp.rdoConnect.CommitTrans
                
                'a�adir informe por defecto si no ha habido crecimiento
                Call pGenerarInformePorDefecto(nReferencia, objPruebaAsistencia.rdoCursor("cTipoPrueba"))
                
                'actualizaci�n de los nodos
                For i = 1 To tvwMuestras.Nodes.Count
                    Set nodo = tvwMuestras.Nodes(i)
                    'Las t�cnicas de la prueba acabada pasan a validadas
                    If Left$(nodo.Key, 1) = "T" Then
                        If Val(nodo.Tag) < constTECVALIDADA Then
                            'Call pUpdateNodoTecnicaEstado(nodo, constTECANULADA, True)
                            Call pUpdateNodoTecnicaEstado(nodo, constTECVALIDADA, True)
                        End If
                    End If
                    'Los cultivos de la prueba acabada pasan a finalizados
                    If Left$(nodo.Key, 1) = "C" Then
                        If Val(nodo.Tag) < constTECVALIDADA Then
                            Call pUpdateNodoTecnicaEstado(nodo, constTECFINALIZADA, True)
                        End If
                    End If
                Next i
            Else '2 - operaci�n cancelada
                objApp.rdoConnect.CommitTrans
            End If
            
    End Select
End Sub

Private Sub cmdResultados_Click(Index As Integer)
    Dim SQL As String
    Dim res As Integer, estado As Integer
    Dim nodo As Node
    
    Select Case Index
        Case 0
            Select Case Left$(nodoResultados.Key, 1)
                Case "K" 'Identificacion de microorganismos en colonias
                    SQL = "�Desea Ud. VALIDAR el resultado y RESTRINGIR el acceso al Dpto. actual?"
                    estado = constCOLVALIDADA
                    res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                    If res = vbYes Then
                        'parametros: codCol, estado
                        Call pResultColonias(Val(Mid$(nodoResultados.Key, 2)), estado)
                    End If
                
                Case "R" 'Resultados de t�cnicas
                    'no se muestra el mensaje para validar pues es muy repetitivo
                    'SQL = "�Desea Ud. VALIDAR el resultado y RESTRINGIR el acceso al Dpto. actual?"
                    res = vbYes
                    estado = constRESULTVALIDADO
                    If res = vbYes Then
                        'par�metros: nodo, resultado, estado
                        Call pResultResultadoTecnica(nodoResultados, txtResultados.Text, estado)
                        Call pEstadoBotones("R", estado)
                    End If
                    'se pasa al resultado siguiente de la misma t�cnica
                    On Error Resume Next
                    If Left$(nodoResultados.Next.Key, 1) = "R" Then
                        If Err = 0 Then
                            tvwMuestras_NodeClick nodoResultados.Next
                        End If
                    End If
                    
                Case "C" 'Reincubaci�n/cambio de condiciones ambientales de cultivos
                    ' se mira si es un cambio de condiciones ambientales (antes de sacar la hoja _
                    de trabajo) o se trata de una reincubaci�n
                    If cmdResultados(0).Caption = "Aceptar" Then
                        SQL = "�Desea Ud. cambiar las condiciones ambientales establecidas?"
                        res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                        If res = vbYes Then
                            'parametros: codTecAsist
                            Call pResultCambiarCondAmb(Val(Mid$(nodoResultados.Key, 2)))
                        End If
                    Else
                        SQL = "�Desea Ud. REINCUBAR el cultivo en las condiciones se�aladas?"
                        res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                        If res = vbYes Then
                            'parametros: codTecAsist
                            Call pResultReincubar(Val(Mid$(nodoResultados.Key, 2)))
                        End If
                    End If
                    
                Case "T"
                    'Al validar la t�cnica, se validan todos los resultados solicitados no nulos
                    SQL = "�Desea Ud. VALIDAR todos los resultados introducidos de la t�cnica?"
                    res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                    If res = vbYes Then
                        Call pValidarTodosResultados(nodoResultados)
                    End If
                                    
                Case "A" 'Reacci�n ante antibi�ticos
                    Set nodo = nodoResultados.Parent
                    Set nodo = nodo.Parent
                    'parametros: codCol, codTecAsist, codAntibiotico, concentracion, recom
                    Call pResultReacAntibiotico(Val(Mid$(nodo.Key, InStr(nodo.Key, "K") + 1)), Val(Mid$(nodoResultados.Key, InStr(nodoResultados.Key, "C") + 1)), Val(Mid$(nodoResultados.Key, InStr(nodoResultados.Key, "A") + 1)), txtConc.Text, -chkRecomendado.Value, -chkConc.Value)
                    'se pasa al antibi�tico siguiente de la misma t�cnica
                    On Error Resume Next
                    If Left$(nodoResultados.Next.Key, 1) = "A" Then
                        If Err = 0 Then
                            tvwMuestras_NodeClick nodoResultados.Next
                        End If
                    End If
            End Select
            tvwMuestras.DropHighlight = nodoResultados
                    
        Case 1
            Select Case Left$(nodoResultados.Key, 1)
                Case "K" 'Identificacion de microorganismos en colonias
                    SQL = "�Desea Ud. VALIDAR el resultado e INFORMAR al resto de Dptos.?"
                    estado = constCOLINFORMADA
                    res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                    If res = vbYes Then
                        'parametros: codCol, estado
                        Call pResultColonias(Val(Mid$(nodoResultados.Key, 2)), estado)
                    End If
                    
                Case "R" 'Resultados de t�cnicas
                    SQL = "�Desea Ud. VALIDAR el resultado e INFORMAR al resto de Dptos.?"
                    estado = constRESULTINFORMADO
                    res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                    If res = vbYes Then
                        'par�metros: nodo, resultado, estado
                        Call pResultResultadoTecnica(nodoResultados, txtResultados.Text, estado)
                        Call pEstadoBotones("R", estado)
                    End If
                    'se pasa al resultado siguiente de la misma t�cnica
                    On Error Resume Next
                    If Left$(nodoResultados.Next.Key, 1) = "R" Then
                        If Err = 0 Then
                            tvwMuestras_NodeClick nodoResultados.Next
                        End If
                    End If
           
                Case "C"
                
                Case "T"
                    'Al informar la t�cnica, se informan todos los resultados validados
                    SQL = "�Desea Ud. INFORMAR todos los resultados validados al resto de Dptos.?"
                    res = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
                    If res = vbYes Then
                        Call pInformarTodosResultados(nodoResultados)
                    End If
            End Select
            tvwMuestras.DropHighlight = nodoResultados
    
        Case 2
            Select Case Left$(nodoResultados.Key, 1)
            Case "K"
                Call pVentanaProblemas(nReferencia, , , , CStr(Val(Mid$(nodoResultados.Key, 2))))
            Case "T"
                Call pVentanaProblemas(nReferencia, , CStr(Val(Mid$(nodoResultados.Key, 2))))
            Case "C"
                Call pVentanaProblemas(nReferencia, , CStr(Val(Mid$(nodoResultados.Key, 2))), CStr(Val(Mid$(nodoResultados.Key, InStr(nodoResultados.Key, "L") + 1))))
            Case "M"
                Call pVentanaProblemas(nReferencia, Mid$(nodoResultados.Key, 2))
            End Select
            
        Case 3
            Call pVentanaAlmacenar(nReferencia, CStr(Val(Mid$(nodoResultados.Key, 2))))
            
    End Select
    
End Sub

Private Sub cmdTipoVentana_Click()
    pFormatearVentana (cmdTipoVentana.ToolTipText)
    SendKeys "{TAB}"
End Sub

Private Sub chkConc_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error Resume Next
    If KeyCode = 13 Then  'validar sensibildad
        cmdResultados_Click (0)
    End If
End Sub

Private Sub chkRecomendado_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error Resume Next
    If KeyCode = 13 Then  'validar sensibildad
        cmdResultados_Click (0)
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String, SQL As String
  Dim fecLect As String
  Dim i As Integer
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
    
  tvwMuestras.ImageList = frmPrincipal.imlImageList2
  tvwProtocolos.ImageList = frmPrincipal.imlImageList2
  tvwTecnicas.ImageList = frmPrincipal.imlImageList2
  tvwMorfologias.ImageList = frmPrincipal.imlImageList2
  tvwColonias.ImageList = frmPrincipal.imlImageList2
  tvwAntibioticos.ImageList = frmPrincipal.imlImageList2
  cmdTipoVentana.Picture = frmPrincipal.imlImageList2.ListImages(constIconAumentar).Picture
      
  'se obtiene la fecha de lectura de las pruebas (todo el d�a actual)
  fecLect = Format(DateAdd("d", 1, fFechaActual()), "dd/mm/yyyy")
  
  'se obtiene la carpeta (c�digo y nombre)
  If objPipe.PipeExist("cCarpeta") Then
      cCarpeta = objPipe.PipeGet("cCarpeta")
  Else
      cCarpeta = ""
  End If
  If objPipe.PipeExist("Carpeta") Then
      desigCarpeta = objPipe.PipeGet("Carpeta")
  Else
      desigCarpeta = ""
  End If
  
  'se obtiene el usuario que ha entrado en la pantalla de Petici�n
  If objPipe.PipeExist("MB_cUser") Then
    cUserPeticion = objPipe.PipeGet("MB_cUser")
  Else
    cUserPeticion = 0
  End If
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objPruebaAsistencia
    .strName = "PRUEBAASISTENCIA"
    Set .objFormContainer = fraPruebaAsistencia(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPruebaAsistencia(0)
    Set .grdGrid = grdssPruebaAsistencia(0)
    .intAllowance = cwAllowReadOnly
    .strDataBase = objEnv.GetValue("Main")
    'Se traen las pruebas en estado de 'realizada' y que tengan lectura para el d�a de _
    hoy o anterior
    .strTable = "PRUEBAASISTENCIA_2"
    SQL = "Proceso=" & constPROCESOSOLICITUD
    If cCarpeta = "" Then
        SQL = SQL & " AND cCarpeta IN "
        SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    Else
        SQL = SQL & " AND cCarpeta = " & cCarpeta
        If desigCarpeta <> "" Then
            Me.Caption = Me.Caption & " - " & desigCarpeta
        End If
    End If
    'NOTA LABOR:Para seguir mostrando pruebas que tienen alg�n resultado validado en Laboratorio
    'SQL = SQL & " AND pA.estado = " & constPRUEBAREALIZANDO
    SQL = SQL & " AND (estado = " & constPRUEBAREALIZANDO
    SQL = SQL & " OR estado = " & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND NRef IN "
    SQL = SQL & "       (SELECT NRef FROM MB2000 "
'    SQL = SQL & "        WHERE MB20_FECLECT <= TO_DATE('" & fFechaActual & "','DD/MM/YYYY')"
    SQL = SQL & "        WHERE MB20_FECLECT < TO_DATE('" & fecLect & "','DD/MM/YYYY')"
    SQL = SQL & "        AND MB34_CODESTTECASIST = " & constTECREALIZADA & ")"
    .strWhere = SQL
    whereInicial = SQL
        
    Call .FormAddOrderField("NRef", cwAscending)
      
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "PRUEBAASISTENCIA")
    Call .FormAddFilterWhere(strKey, "nRef", "N� Ref.", cwString)
    Call .FormAddFilterWhere(strKey, "historia", "Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "caso", "Caso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "paciente", "Paciente", cwString)
    Call .FormAddFilterWhere(strKey, "cama", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fnac", "Fecha Nac.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "desigPrueba", "Prueba", cwString)
    Call .FormAddFilterWhere(strKey, "dptSolicitud", "Dpto. Solicitud", cwString)
    Call .FormAddFilterWhere(strKey, "drSolicitud", "Dr. Solicitud", cwString)
    Call .FormAddFilterWhere(strKey, "urgenciaRealizacion", "Urgente", cwBoolean)
     
    Call .FormAddFilterOrder(strKey, "nRef", "N� Ref.")
    Call .FormAddFilterOrder(strKey, "historia", "historia")
    Call .FormAddFilterOrder(strKey, "caso", "caso")
    Call .FormAddFilterOrder(strKey, "paciente", "Paciente")
    Call .FormAddFilterOrder(strKey, "cama", "Cama")
    Call .FormAddFilterOrder(strKey, "fnac", "Fecha Nac.")
    Call .FormAddFilterOrder(strKey, "desigPrueba", "Prueba")
    Call .FormAddFilterOrder(strKey, "dptSolicitante", "Dpto. solicitud")
    Call .FormAddFilterOrder(strKey, "drSolicitante", "Dr. Solicitud")
    
    Call .objPrinter.Add("MB00101", "Informe de resultados informados", 1, crptToPrinter)
    Call .objPrinter.Add("MB00110", "Hoja de realizaci�n")
    
   End With

  With objWinInfo
    Call .FormAddInfo(objPruebaAsistencia, cwFormDetail)
    
    Call .FormCreateInfo(objPruebaAsistencia)
    'controles ReadOnly
    For i = 0 To txtPruebaAsistencia.Count - 1
        .CtrlGetInfo(txtPruebaAsistencia(i)).blnReadOnly = True
    Next i
    'datos no visibles en la tabla
    .CtrlGetInfo(txtPruebaAsistencia(11)).blnInGrid = False 'secuencia
    .CtrlGetInfo(txtPruebaAsistencia(12)).blnInGrid = False 'n� repetici�n
    .CtrlGetInfo(txtPruebaAsistencia(13)).blnInGrid = False 'c�d. prueba
    .CtrlGetInfo(txtPruebaAsistencia(14)).blnInGrid = False 'comPetic
    .CtrlGetInfo(txtPruebaAsistencia(15)).blnInGrid = False 'comPrueba
    .CtrlGetInfo(txtPruebaAsistencia(16)).blnInGrid = False 'comPac
    'informaci�n sobre b�squedas
    .CtrlGetInfo(txtPruebaAsistencia(0)).blnInFind = True 'num. ref.
    .CtrlGetInfo(txtPruebaAsistencia(1)).blnInFind = True 'paciente
    .CtrlGetInfo(txtPruebaAsistencia(2)).blnInFind = True 'cama
    .CtrlGetInfo(txtPruebaAsistencia(3)).blnInFind = True 'prueba
    .CtrlGetInfo(txtPruebaAsistencia(4)).blnInFind = True 'dpt
    .CtrlGetInfo(txtPruebaAsistencia(5)).blnInFind = True 'dr
    .CtrlGetInfo(txtPruebaAsistencia(8)).blnInFind = True 'historia
    .CtrlGetInfo(txtPruebaAsistencia(9)).blnInFind = True 'caso
    
    Call .FormChangeColor(objPruebaAsistencia)
    
    Call .WinRegister
    Call .WinStabilize
  End With

  'txtObservMuestra.BackColor = objApp.objColor.lngReadOnly
  txtObservMuestra.BackColor = &HC0C0C0
  
  'se llenan los combos que se emplean para la introducci�n de resultados
  Call pLlenarComboGeneroMicro
  Call pLlenarComboMorfologiasTVMorfologias 'llena tambi�n el tvwMorfologias
  Call pLlenarComboMicro(-1, -1)
  Call pLlenarComboCondiciones
  Call pLlenarComboReaccion
  'se llenan los treeviews
  Call pTVProtocolos
  Call pTVTecnicas
  Call pTVAntibioticos
  
  'se formatea el tama�o de los treeview
  cmdTipoVentana.ToolTipText = constRESTAURAR_VENTANA
  Call pFormatearVentana(cmdTipoVentana.ToolTipText)
  
  'se conecta el msrdc que trae datos campos largos (m�s de 250 caract�res)
  msrdcPlacas.Connect = objApp.rdoConnect.Connect
  msrdcColonias.Connect = objApp.rdoConnect.Connect
  msrdcCultivos.Connect = objApp.rdoConnect.Connect
  msrdcTecnicas.Connect = objApp.rdoConnect.Connect
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    
'    REINCUBACI�N AUTOM�TICA Y COMPROBACI�N DE FIN DE PRUEBA
    If txtPruebaAsistencia(0).Text <> "" Then
        'se llama al proceso de reincubaci�n autom�tica
        Call pReincubarAutomatico(txtPruebaAsistencia(0).Text)
        
        'se mira si la prueba est� finalizada
        If fComprobarFinalizacionPrueba(txtPruebaAsistencia(0).Text) = True Then
            'se mira si es necesario insertar el informe por defecto
            Call pGenerarInformePorDefecto(txtPruebaAsistencia(0).Text, txtPruebaAsistencia(13).Text)
            'se informa la finalizaci�n de la prueba
            Call pComentarioSeguimientoPrueba(txtPruebaAsistencia(8).Text, txtPruebaAsistencia(9).Text, txtPruebaAsistencia(11).Text, txtPruebaAsistencia(12).Text)
        End If
    End If
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
    
End Sub

Private Sub grdSSPruebaAsistencia_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    grdssPruebaAsistencia(Index).Columns(1).Text = Val(Mid$(grdssPruebaAsistencia(Index).Columns(1).Text, 6))
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    
   If objPruebaAsistencia.strName = strFormName Then
        If objGen.GetRowCount(objPruebaAsistencia.rdoCursor) = 0 Then
            MsgBox "No hay ninguna t�cnica para leer.", vbInformation, Me.Caption
            'se limpian los treeviews particulares de cada prueba y se ponen en estado inicial _
            los botones y frames
            tvwMuestras.Nodes.Clear
            tvwColonias.Nodes.Clear
            Call pEstadoBotones("1", -1)
            cmdPeticion(4).Enabled = False
        Else
            'Se completan y formatean los datos que no se han podido traer directamente
            'nRef
            txtPruebaAsistencia(7).Text = Val(Mid$(txtPruebaAsistencia(0).Text, 6))
            'Indicaciones
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPetic")) Then
                txtPruebaAsistencia(6).Text = "PETICI�N: " & objPruebaAsistencia.rdoCursor("comPetic")
            End If
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPrueba")) Then
                txtPruebaAsistencia(6).Text = txtPruebaAsistencia(6).Text & Chr$(13) & Chr$(10) & "PRUEBA: " & objPruebaAsistencia.rdoCursor("comPrueba")
            End If
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPac")) Then
                txtPruebaAsistencia(6).Text = txtPruebaAsistencia(6).Text & Chr$(13) & Chr$(10) & "PACIENTE: " & objPruebaAsistencia.rdoCursor("comPac")
            End If
            
            'si anota el n� de referencia en la variable. Si la prueba es la misma que hay _
            en pantalla no se refrescan los treeviews
            If txtPruebaAsistencia(0).Text = nReferencia Then
                Exit Sub
            Else
                nReferencia = txtPruebaAsistencia(0).Text
            End If
                
            'Se llena el tvwMuestras
            Screen.MousePointer = vbHourglass
            Call pTVMuestrasMuestras(tvwMuestras, nReferencia)
            
            'se llena el tvwColonias cada vez que se cambia la prueba
            Call pTVColonias
            
            'Se colocan los botones y frames en el estado correspondiente
            Call pEstadoBotones("1", -1)
            cmdPeticion(4).Enabled = True
            Screen.MousePointer = vbDefault
        End If
    End If
    
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)

'    REINCUBACI�N AUTOM�TICA y COMPROBACI�N DE FIN DE PRUEBA
    If txtPruebaAsistencia(0).Text <> "" Then
        'se llama al proceso de reincubaci�n autom�tica
        Call pReincubarAutomatico(txtPruebaAsistencia(0).Text)
        
        'se mira si la prueba est� finalizada
        If fComprobarFinalizacionPrueba(txtPruebaAsistencia(0).Text) = True Then
            'se mira si es necesario insertar el informe por defecto
            Call pGenerarInformePorDefecto(txtPruebaAsistencia(0).Text, txtPruebaAsistencia(13).Text)
            'se informa la finalizaci�n de la prueba al Laboratorio
            Call pComentarioSeguimientoPrueba(txtPruebaAsistencia(8).Text, txtPruebaAsistencia(9).Text, txtPruebaAsistencia(11).Text, txtPruebaAsistencia(12).Text)
        End If
    End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim SQLWhere As String
  Dim SQLOrder As String
  
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    Select Case intReport
        Case 1
            Screen.MousePointer = vbHourglass
            pImprimirInformeResultado (nReferencia)
            Screen.MousePointer = vbDefault
        Case 2
            blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
            SQLWhere = "WHERE pruebaAsistencia.nRef = '" & nReferencia & "'"
            SQLOrder = "ORDER BY pruebaAsistencia.NREF"
            Call objPrinter.ShowReport(SQLWhere, SQLOrder)
    End Select
    Set objPrinter = Nothing
    
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
    Dim acceso% 'guarda el valor de la variable accesoMantenimiento que cambia al llamar a _
                las pantallas de mantenimiento y perder el foco el elemento que lo ten�a.
    
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
  
    If accesoMantenimiento <> constMANTNINGUNO Then
        If panPanel.Index = constcwPosForeign Then
            acceso = accesoMantenimiento
            Select Case accesoMantenimiento
                Case constMANTREFSENSIBILIDADES
                    Call pVentanaRefSensibilidad(CStr(Val(Mid$(nodoResultados.Key, 2))), Mid$(nodoResultados.Tag, 2))
                    If objPipe.PipeExist("MB_codReac") Then
                        codReac = objPipe.PipeGet("MB_codReac")
                        objPipe.PipeRemove ("MB_codReac")
                        If objPipe.PipeExist("MB_desigReac") Then
                            cbossReac.Text = objPipe.PipeGet("MB_desigReac")
                            objPipe.PipeRemove ("MB_desigReac")
                        End If
                    End If
                Case constMANTMICROORGANISMOS
                    Erase arMantenimientos
                    frmA_Microorganismo.Show vbModal
                    Set frmA_Microorganismo = Nothing
                    Call pRefrescarDatos
                Case constMANTGENEROS
                    Erase arMantenimientos
                    frmA_FamiliaMicroorganismo.Show vbModal
                    Set frmA_FamiliaMicroorganismo = Nothing
                    Call pRefrescarDatos
                Case constMANTMORFOLOGIAS
                    Erase arMantenimientos
                    frmA_Morfologia.Show vbModal
                    Set frmA_Morfologia = Nothing
                    Call pRefrescarDatos
                Case constMANTPROTOCOLOS
                    Erase arMantenimientos
                    frmA_Protocolo.Show vbModal
                    Set frmA_Protocolo = Nothing
                    Call pRefrescarDatos
                Case constMANTTECNICAS
                    Erase arMantenimientos
                    frmA_Tecnica.Show vbModal
                    Set frmA_Tecnica = Nothing
                    Call pRefrescarDatos
                Case constMANTANTIBIOTICOS
                    Erase arMantenimientos
                    frmA_Antibiotico.Show vbModal
                    Set frmA_Antibiotico = Nothing
                    Call pRefrescarDatos
                Case constMANTAUTOTEXTOS
                    Call pVentanaAutotexto(1) 'acceso al mantenimiento de autotextos en el multil�inea
                    If objPipe.PipeExist("MB_autotexto") Then
                        Select Case Screen.ActiveForm.ActiveControl.Name
                            Case txtResultados.Name
                                If txtResultados.Text <> "" And Right$(txtResultados.Text, 1) <> " " Then
                                    txtResultados.Text = txtResultados.Text & " "
                                End If
                                txtResultados.Text = txtResultados.Text & objPipe.PipeGet("MB_autotexto")
                                txtResultados.SelStart = Len(txtResultados.Text)
                            Case txtConc.Name
                                If txtConc.Text <> "" And Right$(txtConc.Text, 1) <> " " Then
                                    txtConc.Text = txtConc.Text & " "
                                End If
                                txtConc.Text = txtConc.Text & objPipe.PipeGet("MB_autotexto")
                                txtConc.SelStart = Len(txtConc.Text)
                            Case txtObserv(0).Name
                                Select Case Screen.ActiveForm.ActiveControl.Index
                                    Case 0, 1
                                        msrdcCultivos.Refresh
                                    Case 2, 3
                                        msrdcTecnicas.Refresh
                                    Case 4
                                        msrdcColonias.Refresh
                                    Case 5
                                        msrdcPlacas.Refresh
                                End Select
                                If txtObserv(Screen.ActiveForm.ActiveControl.Index).Text <> "" And Right$(txtObserv(Screen.ActiveForm.ActiveControl.Index).Text, 1) <> " " Then
                                    txtObserv(Screen.ActiveForm.ActiveControl.Index).Text = txtObserv(Screen.ActiveForm.ActiveControl.Index).Text & " "
                                End If
                                txtObserv(Screen.ActiveForm.ActiveControl.Index).Text = txtObserv(Screen.ActiveForm.ActiveControl.Index).Text & objPipe.PipeGet("MB_autotexto")
                                txtObserv(Screen.ActiveForm.ActiveControl.Index).SelStart = Len(txtObserv(Screen.ActiveForm.ActiveControl.Index)) + 1
                                Select Case Screen.ActiveForm.ActiveControl.Index
                                    Case 0, 1
                                        msrdcCultivos.Resultset.Close
                                    Case 2, 3
                                        msrdcTecnicas.Resultset.Close
                                    Case 4
                                        msrdcColonias.Resultset.Close
                                    Case 5
                                        msrdcPlacas.Resultset.Close
                                End Select
                        End Select
                        Call objPipe.PipeRemove("MB_autotexto")
                    End If
            End Select
            Call pBotonLista(True, acceso)
        End If
    End If
  
End Sub

Private Sub tabMuestras_LostFocus()
    Call pBotonLista(False, constMANTNINGUNO)
End Sub

Private Sub tabMuestras_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    
    Select Case tabMuestras.Tab
        Case 0
            Call pBotonLista(True, constMANTPROTOCOLOS)
            'Call pBotonLista(False, constMANTNINGUNO)
        Case 1
            Call pBotonLista(True, constMANTTECNICAS)
            'Call pBotonLista(False, constMANTNINGUNO)
        Case 2
            Call pBotonLista(True, constMANTMORFOLOGIAS)
            'Call pBotonLista(False, constMANTNINGUNO)
        Case 3
            Call pBotonLista(False, constMANTNINGUNO)
        Case 4
            Call pBotonLista(True, constMANTANTIBIOTICOS)
            'Call pBotonLista(False, constMANTNINGUNO)
    End Select
     
End Sub



Private Sub tabResCol_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    On Error Resume Next
    Select Case tabResCol.Tab
        Case 1
            txtObserv(4).SetFocus
            txtObserv(4).SelStart = Len(txtObserv(4).Text) + 1
'            txtObserv(4).SelStart = 0
'            txtObserv(4).SelLength = Len(txtObserv(4).Text)
        Case 2
            txtObserv(5).SetFocus
            txtObserv(5).SelStart = Len(txtObserv(5).Text) + 1
'            txtObserv(5).SelStart = 0
'            txtObserv(5).SelLength = Len(txtObserv(5).Text)
    End Select
End Sub

Private Sub tabResCultivo_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    On Error Resume Next
    Select Case tabResCultivo.Tab
        Case 1
            txtObserv(0).SetFocus
            txtObserv(0).SelStart = Len(txtObserv(0).Text) + 1
'            txtObserv(0).SelStart = 0
'            txtObserv(0).SelLength = Len(txtObserv(0).Text)
        Case 2
            txtObserv(1).SetFocus
            txtObserv(1).SelStart = Len(txtObserv(1).Text) + 1
'            txtObserv(1).SelStart = 0
'            txtObserv(1).SelLength = Len(txtObserv(1).Text)
    End Select
End Sub

Private Sub tabResTec_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    On Error Resume Next
    Select Case tabResTec.Tab
        Case 0
            txtObserv(2).SetFocus
            txtObserv(2).SelStart = Len(txtObserv(2).Text) + 1
'            txtObserv(2).SelStart = 0
'            txtObserv(2).SelLength = Len(txtObserv(2).Text)
        Case 1
            txtObserv(3).SetFocus
            txtObserv(3).SelStart = Len(txtObserv(3).Text) + 1
'            txtObserv(3).SelStart = 0
'            txtObserv(3).SelLength = Len(txtObserv(3).Text)
    End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdSSPruebaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdSSPruebaAsistencia_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSPruebaAsistencia_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSPruebaAsistencia_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tabPruebaAsistencia_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabPruebaAsistencia(intIndex), False, True)
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraPruebaAsistencia_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraPruebaAsistencia(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub



Private Sub tvwAntibioticos_DragOver(Source As Control, X As Single, y As Single, State As Integer)
'se coloca el DragIcon correcto
    If Source.Name = tvwAntibioticos.Name Then
        If State = 0 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(Source.SelectedItem.Image).Picture
        ElseIf State = 1 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
        End If
    End If
    
End Sub

Private Sub tvwAntibioticos_KeyDown(KeyCode As Integer, Shift As Integer)
'arrastre de nodos con el ENTER
    If KeyCode = 13 Then
        If Left$(tvwAntibioticos.SelectedItem.Key, 1) <> "W" Then
            admitido = fArrastreValido(tvwAntibioticos)
            Call tvwMuestras_DragDrop(tvwAntibioticos, 0, 0)
        End If
    End If
    
End Sub

Private Sub tvwAntibioticos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Dim nodo As Node, comp As String
    
    Set tvwAntibioticos.DropHighlight = Nothing
    Set nodo = tvwAntibioticos.HitTest(X, y)
    
    'si no hay ninguna muestra en tvwMuestras no se permite arrastrar
    If tvwMuestras.Nodes.Count = 0 Then
        Set tvwAntibioticos.SelectedItem = nodo
        Exit Sub
    End If
    
    On Error Resume Next    '
    comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
    If Err = 0 Then        '
        Set tvwAntibioticos.SelectedItem = nodo
        Select Case Left$(comp, 1)
        Case "G", "A"
            tvwAntibioticos.Drag
            tvwAntibioticos.DragIcon = frmPrincipal.imlImageList2.ListImages(nodo.Image).Picture
        End Select
    End If
End Sub


Private Sub tvwColonias_DragOver(Source As Control, X As Single, y As Single, State As Integer)
'se coloca el DragIcon correcto
    If Source.Name = tvwColonias.Name Then
        If State = 0 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(Source.SelectedItem.Image).Picture
        ElseIf State = 1 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
        End If
    End If
    
End Sub

Private Sub tvwColonias_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim nodo As Node
    Dim cCol%
    Dim i%
    
    Select Case KeyCode
        Case 13 'arrastre de nodos con el ENTER
            admitido = fArrastreValido(tvwColonias)
            Call tvwMuestras_DragDrop(tvwColonias, 0, 0)
    
        Case 46    ' Tecla Suprimir
            Set nodo = tvwColonias.SelectedItem
            
            'se mira si la colonia se puede anular
            If nodo.Key = "KNueva" Then Exit Sub
            If Val(nodo.Tag) <> constCOLLOCALIZADA Then Exit Sub
            
            'se pregunta para anular la colonia
            cCol = Val(Mid$(nodo.Key, 2))
            If MsgBox("�Desea eliminar la Colonia " & cCol & "?", vbQuestion + vbYesNo, "Eliminar colonia") = vbYes Then
                objApp.rdoConnect.BeginTrans
                If fUpdateEstColonias(cCol) = False Then
                    MsgBox "Se ha producido un error en la eliminaci�n de colonias.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    'nodo del tvwColonias
                    Call pUpdateNodoColoniaEstado(nodo, constCOLANULADA)
                    'nodos del tvwMuestras
                    For i = 1 To tvwMuestras.Nodes.Count
                        Set nodo = tvwMuestras.Nodes(i)
                        If Left$(nodo.Key, Len(CStr(cCol)) + 1) = "K" & cCol Then
                            Call pUpdateNodoColPlacaEstado(nodo, constCOLPLACAANULADA)
                        End If
                    Next i
                    
                End If
            End If
    End Select
  
End Sub

Private Sub tvwColonias_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Dim nodo As Node, comp As String
    
    Set tvwColonias.DropHighlight = Nothing
    Set nodo = tvwColonias.HitTest(X, y)
    
    'si no hay ninguna muestra en tvwMuestras no se permite arrastrar
    If tvwMuestras.Nodes.Count = 0 Then
        Set tvwColonias.SelectedItem = nodo
        Exit Sub
    End If
    
    On Error Resume Next    '
    comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
    If Err = 0 Then        '
        ' se mira que la colonia no est� anulada
        If Val(nodo.Tag) = constCOLANULADA Then Exit Sub
        
        Set tvwColonias.SelectedItem = nodo
        tvwColonias.Drag
        tvwColonias.DragIcon = frmPrincipal.imlImageList2.ListImages(nodo.Image).Picture
    End If
    
End Sub


Private Sub tvwMorfologias_DragOver(Source As Control, X As Single, y As Single, State As Integer)
'se coloca el DragIcon correcto
    If Source.Name = tvwMorfologias.Name Then
        If State = 0 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(Source.SelectedItem.Image).Picture
        ElseIf State = 1 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
        End If
    End If
    
End Sub

Private Sub tvwMorfologias_KeyDown(KeyCode As Integer, Shift As Integer)
'arrastre de nodos con el ENTER
    If KeyCode = 13 Then
        admitido = fArrastreValido(tvwMorfologias)
        Call tvwMuestras_DragDrop(tvwMorfologias, 0, 0)
    End If
    
End Sub

Private Sub tvwMorfologias_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Dim nodo As Node, comp As String
    
    Set tvwMorfologias.DropHighlight = Nothing
    Set nodo = tvwMorfologias.HitTest(X, y)
    
    'si no hay ninguna muestra en tvwMuestras no se permite arrastrar
    If tvwMuestras.Nodes.Count = 0 Then
        Set tvwMorfologias.SelectedItem = nodo
        Exit Sub
    End If
    
    On Error Resume Next    '
    comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
    If Err = 0 Then        '
        Set tvwMorfologias.SelectedItem = nodo
        tvwMorfologias.Drag
        tvwMorfologias.DragIcon = frmPrincipal.imlImageList2.ListImages(nodo.Image).Picture
    End If
    
End Sub

Private Sub tvwMuestras_Collapse(ByVal Node As ComctlLib.Node)
 
    Set tvwMuestras.SelectedItem = Node
    tvwMuestras_NodeClick Node
    
End Sub


Private Sub tvwMuestras_DragDrop(Source As Control, X As Single, y As Single)
    Dim nodoEn As Node, nodoDesde As Node
    Dim compNodo As String
    
    'Estas 3 l�neas se pueden quitar si queremos obtener los mensajes de la causa por la que _
    no se puede soltar el elemento arrastrado sobre el nodo actual
    If admitido = True Then
         On Error Resume Next
         Set nodoEn = tvwMuestras.DropHighlight
         'se comprueba si el nodo existe
         compNodo = nodoEn.Text
         
         If Err = 0 Then
             On Error GoTo 0
             Select Case Source.Name
             Case "tvwMorfologias"
                 Set nodoDesde = tvwMorfologias.SelectedItem
                 Select Case Left$(nodoEn.Key, 1)
                     Case "M", "X", "F"
                         Call pIndicarMorfologias(nodoEn, nodoDesde)
                     Case "R"
                         Call pInsertarMorfologias(nodoEn, nodoDesde)
                 End Select
             Case "tvwColonias"
                 Set nodoDesde = tvwColonias.SelectedItem
                 Select Case Left$(nodoEn.Key, 1)
                 Case "C"
                     Call pInsertarColonias(nodoEn, nodoDesde)
                 Case "K"
                     Call pChangeNumColPlaca(nodoEn, nodoDesde)
                 End Select
             Case "tvwProtocolos"
                 Set nodoDesde = tvwProtocolos.SelectedItem
                 Select Case Left$(nodoDesde.Key, 1)
                 Case "P" 'protocolo
                   Call pInsertarProtocolos(nodoEn, nodoDesde)
                 Case "T", "C" 't�cnicas y cultivos
                   Call pInsertarTecnicas(nodoEn, nodoDesde, True)
                 End Select
             Case "tvwTecnicas"
                 Set nodoDesde = tvwTecnicas.SelectedItem
                 Call pInsertarTecnicas(nodoEn, nodoDesde, False)
             Case "tvwAntibioticos"
                 Set nodoDesde = tvwAntibioticos.SelectedItem
                 Call pInsertarAntibioticos(nodoEn, nodoDesde)
             End Select
        End If
    End If
    Set tvwMuestras.DropHighlight = tvwMuestras.SelectedItem
    
End Sub
Private Sub tvwMuestras_DragOver(Source As Control, X As Single, y As Single, State As Integer)
  Dim compNodo As String
    
    Set tvwMuestras.DropHighlight = tvwMuestras.HitTest(X, y)
        
    If State = 1 Then
        Set tvwMuestras.DropHighlight = tvwMuestras.SelectedItem
    End If
    
    On Error Resume Next
    compNodo = tvwMuestras.DropHighlight.Text
    If Err = 0 Then
        admitido = fArrastreValido(Source)
        If admitido = True Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(Source.SelectedItem.Image).Picture
        Else
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
        End If
    Else
        admitido = False
        Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
    End If
    
End Sub

Private Sub tvwMuestras_Expand(ByVal Node As ComctlLib.Node)

    If tvwMuestras.Tag = "" Then
        Set tvwMuestras.SelectedItem = Node
        tvwMuestras_NodeClick Node
    End If
    
End Sub

Private Sub tvwMuestras_GotFocus()
    
    On Error Resume Next
    Select Case Left$(nodoResultados.Key, 1)
        Case "R"
            txtResultados.SetFocus
        Case "A"
            cbossReac.SetFocus
'        Case "C" 'Si hacemos esto, no se puede anular la placa
'            Call tabResCultivo_MouseDown(0, 0, 0, 0)
    End Select

End Sub

Private Sub tvwMuestras_KeyDown(KeyCode As Integer, Shift As Integer)
  
  Dim nodo As Node, nodo1 As Node
  Dim numLoc As Integer
  Dim desig As String
  Dim msg As String, msg1 As String
  Dim i%
  
  If KeyCode = 46 Then    ' Tecla Suprimir
    Set nodo = tvwMuestras.SelectedItem
    desig = Trim$(Mid$(nodo.Tag, InStr(nodo.Tag, "/") + 1))
            
    Select Case Left(nodo.Key, 1)
    Case "F"
        msg = "�Desea eliminar el microorganismo '" & desig & "'?"
        If MsgBox(msg, vbQuestion + vbYesNo, "Eliminar microorganismos") = vbYes Then
            numLoc = Val(nodo.Tag)
            objApp.rdoConnect.BeginTrans
            'parametros: cMorf, nLoc, cMuestra
            If fDeleteMorfologias(Val(Mid$(nodo.Key, 2)), numLoc, Mid$(nodo.Key, InStr(nodo.Key, "X") + 1)) = False Then
                MsgBox "Se ha producido un error en la eliminaci�n de morfolog�as.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                'actualizaci�n de los nodos
                If numLoc = 1 Then
                    tvwMuestras.Nodes.Remove nodo.Key
                Else
                    nodo.Text = desig & "   (" & numLoc - 1 & " localiz.)"
                    nodo.Tag = numLoc - 1 & "/" & desig
                End If
            End If
        End If
    
    Case "A"
        msg = "�Desea eliminar el antibi�tico '" & desig & "'?"
        If MsgBox(msg, vbQuestion + vbYesNo, "Eliminar antibi�ticos") = vbYes Then
            objApp.rdoConnect.BeginTrans
            'par�metros: cAnti, cTecAsist
            If fDeleteAntibioticos(Val(Mid$(nodo.Key, 2)), Val(Mid$(nodo.Key, InStr(nodo.Key, "C") + 1))) = False Then
                MsgBox "Se ha producido un error en la eliminaci�n de antibi�ticos.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                'Se elimina el nodo del antibi�tico
                tvwMuestras.Nodes.Remove nodo.Key
                Call pEstadoBotones("1", -1) 'queda seleccionado el nodo de la t�cnica
            End If
        End If
    
    Case "T"
        'se mira si la t�cnica est� anulada o validada
        Select Case Val(nodo.Tag)
            Case constTECANULADA
                Exit Sub
            Case constTECVALIDADA
                For i = 1 To nodo.Children
                    If i = 1 Then Set nodo1 = nodo.Child Else Set nodo1 = nodo1.Next
                    If Val(nodo1.Tag) = constRESULTINFORMADO Then
                        MsgBox "La t�cnica tiene alg�n resultado informado. No se puede anular.", vbExclamation, "Anular t�cnica"
                        Exit Sub
                    End If
                Next i
        End Select
        
        msg = "�Desea anular la t�cnica '" & desig & "'?"
        If MsgBox(msg, vbQuestion + vbYesNo, "Anular t�cnicas") = vbYes Then
            objApp.rdoConnect.BeginTrans
            If fUpdateEstTecnicas(CStr(Val(Mid$(nodo.Key, 2))), constTECANULADA) = False Then
                MsgBox "Se ha producido un error en la anulaci�n de t�cnicas.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                ' se actualiza el estado del nodo de la t�cnica
                Call pUpdateNodoTecnicaEstado(nodo, constTECANULADA, True)
                
                Call pEstadoBotones("T", constTECANULADA)
            End If
        End If
    
    Case "C"
        'se mira si el cultivo est� anulado
        If Val(nodo.Tag) = constTECANULADA Or Val(nodo.Tag) = constTECFINALIZADA Then Exit Sub
        
        'si es la �ltima placa se chequea la correspondencia entre las morfolog�as _
        encontradas por observaci�n directa y las morfolog�as identificadas posteriormente
        If fUltimaPlacaTirada(nodo) = True Then
            'parametros: cMuestra
            Set nodo1 = nodo.Root
            msg1 = fComprobarMicroMorfo(Mid$(nodo1.Key, 2))
            If msg1 <> "" Then
                msg = "El cultivo que va a anular es el �ltimo de los disponibles y se han"
                msg = msg & " identificado menos microorganismos de los"
                msg = msg & " esperados:" & Chr$(13) & Chr$(10) & msg1
                msg = msg & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
            End If
        End If
        msg = msg & "�Desea anular el cultivo '" & desig & "'?"
            
        If MsgBox(msg, vbQuestion + vbYesNo, "Anular cultivos") = vbYes Then
            objApp.rdoConnect.BeginTrans
            'parametros: cTecAsist, estado
            If fUpdateEstCultivos(CStr(Val(Mid$(nodo.Key, 2))), constTECANULADA) = False Then
                MsgBox "Se ha producido un error en la anulaci�n del cultivo.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                ' se actualiza el estado del nodo del cultivo
                Call pUpdateNodoTecnicaEstado(nodo, constTECANULADA, True)
                
                Call pEstadoBotones("C", constTECANULADA)
            End If
        End If
    
    Case "K"
        'se mira si la colonia est� anulada
        If Val(nodo.Tag) = constCOLPLACAANULADA Then Exit Sub
        
        msg = "�Desea eliminar la Colonia " & Val(Mid$(nodo.Key, 2)) & " de esta placa?"
        If MsgBox(msg, vbQuestion + vbYesNo, "Eliminar colonia") = vbYes Then
            objApp.rdoConnect.BeginTrans
            'parametros: cTecAsist, cCol, estado
            If fUpdateEstColoniaPlaca(Val(Mid$(nodo.Key, InStr(nodo.Key, "C") + 1)), Val(Mid$(nodo.Key, 2)), constCOLPLACAANULADA) = False Then
                MsgBox "Se ha producido un error en la eliminaci�n de colonias.", vbError
                objApp.rdoConnect.RollbackTrans
            Else
                objApp.rdoConnect.CommitTrans
                'nodo del tvwMuestras
                Call pUpdateNodoColPlacaEstado(nodo, constCOLPLACAANULADA)
                'estado del frame y botones
                Call pEstadoBotones("K", constCOLANULADA)
            End If
        End If
        
    Case "R"
        'NOTA: no se podr� anular resultados ya que al pinchar en el nodo de resultados el foco _
        pasa directamente al TextBox del resultado (excepto si el resultado est� informado) _
        y por tanto pulsar Supr afecta al TextBox y no al tvwMuestras
'        ' se mira si se puede eliminar
'        Select Case Val(nodo.Tag)
'            Case constRESULTANULADO
'                Exit Sub
'            Case constRESULTVALIDADO
'                MsgBox "El resultado est� validado. No se puede anular.", vbExclamation, "Anular t�cnica"
'                Exit Sub
'            Case constRESULTINFORMADO
'                MsgBox "El resultado est� informado. No se puede anular.", vbExclamation, "Anular t�cnica"
'                Exit Sub
'        End Select
'
'        msg = "�Desea anular el resultado '" & desig & "'?"
'        If MsgBox(msg, vbQuestion + vbYesNo, "Anular resultados") = vbYes Then
'            'par�metros: nodo, resultado, estado
'            Call pResultResultadoTecnica(nodo, "", constRESULTANULADO)
'            Call pEstadoBotones("R", constRESULTANULADO)
'        End If

    End Select
    
  End If
  
End Sub
Private Function fUltimaPlacaTirada(nodo As Node) As Boolean
'Chequea si la placa tirada es la �ltima placa de la muestra
    
    Dim nodoPlaca As Node
    
    fUltimaPlacaTirada = True
    
    Set nodoPlaca = nodo.FirstSibling
    Do While nodoPlaca <> nodo.LastSibling
        Set nodoPlaca = nodoPlaca.Next
        If nodoPlaca <> nodo Then
            If Left$(nodoPlaca.Key, 1) = "C" Then
                If Val(nodoPlaca.Tag) <> constTECANULADA Or Val(nodoPlaca.Tag) <> constTECFINALIZADA Then
                    fUltimaPlacaTirada = False
                    Exit Function
                End If
            End If
        End If
    Loop

End Function
Private Function fUpdateEstColoniaPlaca(cTecAsist%, cCol%, estado%) As Boolean
'Se cambia el estado de la colonia-placa

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery
              
    fUpdateEstColoniaPlaca = False
    
    On Error Resume Next
              
    SQL = "UPDATE MB3000"
    SQL = SQL & " SET MB37_codEstPlaca = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    If cTecAsist <> -1 Then
        SQL = SQL & " AND MB20_codTecAsist = ?"
    End If
    Set qryCol = objApp.rdoConnect.CreateQuery("UpdateColPlaca", SQL)
    
    qryCol(0) = estado
    qryCol(1) = nReferencia
    qryCol(2) = cCol
    If cTecAsist <> -1 Then
        qryCol(3) = cTecAsist
    End If
    qryCol.Execute
    
    If Err = 0 Then
        fUpdateEstColoniaPlaca = True
    End If
    
    qryCol.Close
     
End Function

Private Function fUpdateEstColonias(cCol%) As Boolean
'Anula una colonia de la tabla de colonias (MB2700) y la anula tambi�n de todas las _
    placas (MB3000)

'La funci�n devuelve True si la operaci�n sobre la base de datos se realiz� correctamente y _
    False si se produjo alg�n error
    
    Dim SQL As String
    Dim qryCol As rdoQuery ',rsCol As rdoResultset
    
    fUpdateEstColonias = False
        
    'se anula la colonia de todas las placas
    'parametros: cTecAsist, estado
    If fUpdateEstColoniaPlaca(-1, cCol, constCOLPLACAANULADA) = False Then
        Exit Function
    End If
  
'    ' si la colonia estaba validada o informada habr� que indicar un problema
'    SQL = "SELECT MB36_codEstCol"
'    SQL = SQL & " FROM MB2700"
'    SQL = SQL & " WHERE nRef = ?"
'    SQL = SQL & " AND MB27_codCol = ?"
'    Set qryCol = objApp.rdoConnect.CreateQuery("Col2", SQL)
'
'    qryCol(0) = nReferencia
'    qryCol(1) = Val(Mid$(nodo.Key, 2))
'    Set rsCol = qryCol.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'
'    If rsCol(0) = constCOLLOCALIZADA Then
'        estado = constCOLANULADA
'    Else
'        Call pVentanaProblemas(CStr(nReferencia), , , , CStr(Val(Mid$(nodo.Key, 2))))
'        If objPipe.PipeGet("Probl") = False Then
'            MsgBox "No se puede anular la colonia si no se indica un problema.", vbExclamation, "Anular colonia"
'            Exit Function
'        Else
'            estado = constCOLPROBLEMA
'        End If
'    End If


    'Se anula la colonia de la tabla de colonias MB2700
    On Error Resume Next
    
    SQL = "UPDATE MB2700"
    SQL = SQL & " SET MB36_codEstCol = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    Set qryCol = objApp.rdoConnect.CreateQuery("UpdateCol", SQL)

    qryCol(0) = constCOLANULADA
    qryCol(1) = nReferencia
    qryCol(2) = cCol
    qryCol.Execute
    
    If Err = 0 Then
        fUpdateEstColonias = True
    End If

    qryCol.Close
    
End Function

Private Sub tvwMuestras_LostFocus()
  Set tvwMuestras.DropHighlight = tvwMuestras.SelectedItem
End Sub

Private Sub tvwMuestras_NodeClick(ByVal Node As ComctlLib.Node)
  Dim estado As Integer
  Dim letra As String
  
  '  Set tvwMuestras.DropHighlight = Nothing
  Set tvwMuestras.DropHighlight = tvwMuestras.SelectedItem

  'se anota el nodo para el cual se van a introducir resultados en una variable global
  Set nodoResultados = Node
  
  letra = Left$(Node.Key, 1)
  Select Case letra
    Case "K" 'colonias
      'Se traen los datos de la colonia. Parametros: cCol
      Call pDatosColonia(Val(Mid$(Node.Key, 2)))
      'Se traen los datos largos de la colonia. Par�metros: cCol, cTecAsist
      estado = fDatosColonia(Val(Mid$(Node.Key, 2)), Val(Mid$(Node.Key, InStr(Node.Key, "C") + 1)))
    
      'si la colonia-placa est� anulada, no se dejar� introducir resultados (identificaciones)
      If Val(Node.Tag) = constCOLPLACAANULADA Then
        estado = constCOLANULADA 'se se�ala el mismo estado que si estuviera anulada la colonia
      End If
    
    Case "R" 'Resultados de t�cnicas
      'parametros: codTecAsist, codTec, codResult, estado, codTiTec
      Call pDatosResultTecnicas(Val(Mid$(Node.Key, InStr(Node.Key, "T") + 1)), Val(Mid$(Node.Key, InStr(Node.Key, "N") + 1)), Val(Mid$(Node.Key, 2)), estado, Val(Mid$(Node.Parent.Key, InStr(Node.Parent.Key, "I") + 1)))
      
      ' si la t�cnica est� anulada no se podr� introducir resultado.
      If Val(Node.Parent.Tag) = constTECANULADA Then
        estado = constRESULTANULADO
      End If
    
    Case "C" 'cultivos
      'parametros: codTecAsist
      Call pDatosCultivo(Val(Mid$(Node.Key, 2)))
      'parametros: codTecAsist
      estado = fDatosCultivo(Val(Mid$(Node.Key, 2)))
      If Val(Node.Tag) = constTECANULADA Or Val(Node.Tag) = constTECFINALIZADA Then
        cmdPeticion(1).Enabled = False
      Else
        cmdPeticion(1).Enabled = True
      End If
      
    Case "T" 't�cnicas
      'parametros: codTecAsist
      estado = fDatosTecnica(Val(Mid$(Node.Key, 2)))
    
    Case "A" 'antibi�ticos
      'parametros: codTecAsist, codAntibiotico
      Call pDatosAntibioticos(Val(Mid$(Node.Key, InStr(Node.Key, "C") + 1)), Val(Mid$(Node.Key, 2)))

    Case "M" 'muestra
      Call pDatosMuestra(Mid$(Node.Key, 2))
      
  End Select

  'se muestra el frame y los botones correspondientes
  If letra = "R" Then
    Call pEstadoBotones(letra, estado, Left$(Node.Parent.Parent.Key, 1))
  ElseIf letra = "T" Then
    Call pEstadoBotones(letra, estado, Left$(Node.Parent.Key, 1))
  Else
    Call pEstadoBotones(letra, estado)
  End If
  
  'se realizan otras operaciones
  On Error Resume Next
  If letra = "R" Then
    txtResultados.SetFocus
    txtResultados.SelStart = 0
    txtResultados.SelLength = Len(txtResultados.Text)
  ElseIf letra = "A" Then
    cbossReac.SetFocus
'  ElseIf letra = "C" Then 'Si hacemos esto, no se puede anular la placa
'    Call tabResCultivo_MouseDown(0, 0, 0, 0)
  End If
  
End Sub
Private Sub tvwProtocolos_DragOver(Source As Control, X As Single, y As Single, State As Integer)
'se coloca el DragIcon correcto
    If Source.Name = tvwProtocolos.Name Then
        If State = 0 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(Source.SelectedItem.Image).Picture
        ElseIf State = 1 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
        End If
    End If

End Sub

Private Sub tvwProtocolos_KeyDown(KeyCode As Integer, Shift As Integer)
'arrastre de nodos con el ENTER
    If KeyCode = 13 Then
        If Left$(tvwProtocolos.SelectedItem.Key, 1) <> "U" Then
            admitido = fArrastreValido(tvwProtocolos)
            Call tvwMuestras_DragDrop(tvwProtocolos, 0, 0)
        End If
    End If
    
End Sub

Private Sub tvwProtocolos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Dim nodo As Node, comp As String
    
    Set tvwProtocolos.DropHighlight = Nothing
    Set nodo = tvwProtocolos.HitTest(X, y)
    
    'si no hay ninguna muestra en tvwMuestras no se permite arrastrar
    If tvwMuestras.Nodes.Count = 0 Then
        Set tvwProtocolos.SelectedItem = nodo
        Exit Sub
    End If
    
    On Error Resume Next    '
    comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
    If Err = 0 Then        '
        Set tvwProtocolos.SelectedItem = nodo
        Select Case Left$(comp, 1)
        Case "P", "T"
        tvwProtocolos.Drag
        tvwProtocolos.DragIcon = frmPrincipal.imlImageList2.ListImages(nodo.Image).Picture
        End Select
    End If
    
End Sub




Private Sub tvwTecnicas_DragOver(Source As Control, X As Single, y As Single, State As Integer)
'se coloca el DragIcon correcto
    If Source.Name = tvwTecnicas.Name Then
        If State = 0 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(Source.SelectedItem.Image).Picture
        ElseIf State = 1 Then
            Source.DragIcon = frmPrincipal.imlImageList2.ListImages(constIconStop).Picture
        End If
    End If
    
End Sub

Private Sub tvwTecnicas_KeyDown(KeyCode As Integer, Shift As Integer)
'arrastre de nodos con el ENTER
    If KeyCode = 13 Then
        If Left$(tvwTecnicas.SelectedItem.Key, 1) <> "I" Then
            admitido = fArrastreValido(tvwTecnicas)
            Call tvwMuestras_DragDrop(tvwTecnicas, 0, 0)
        End If
    End If
    
End Sub

Private Sub tvwTecnicas_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Dim nodo As Node, comp As String
    
    Set tvwTecnicas.DropHighlight = Nothing
    Set nodo = tvwTecnicas.HitTest(X, y)
    
    'si no hay ninguna muestra en tvwMuestras no se permite arrastrar
    If tvwMuestras.Nodes.Count = 0 Then
        Set tvwTecnicas.SelectedItem = nodo
        Exit Sub
    End If
    
    On Error Resume Next    '
    comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
    If Err = 0 Then        '
        Set tvwTecnicas.SelectedItem = nodo
        Select Case Left$(comp, 1)
        Case "T", "C"
            tvwTecnicas.Drag
            tvwTecnicas.DragIcon = frmPrincipal.imlImageList2.ListImages(nodo.Image).Picture
        End Select
    End If
End Sub


Private Sub txtConc_GotFocus()
    Call pBotonLista(True, constMANTREFSENSIBILIDADES)
    txtConc.SelStart = 0
    txtConc.SelLength = Len(txtConc.Text)
End Sub

Private Sub txtConc_KeyDown(KeyCode As Integer, Shift As Integer)
'Al pulsar ENTER se aceptan los valores establecidos para el antibi�tico
   
    On Error Resume Next
    Select Case KeyCode
        Case vbKeyF3
            Call pBuscarReacPorConcentracion(CStr(Val(Mid$(nodoResultados.Key, 2))), txtConc.Text)
        Case 13
            cmdResultados_Click (0)
    End Select
    
End Sub

Private Sub txtConc_KeyPress(KeyAscii As Integer)

    Select Case KeyAscii
        Case 1 To 7, 9 To 43, 45 To 47, Is > 58
            KeyAscii = 0
    End Select
    
End Sub

Private Sub txtConc_LostFocus()
    Call pBotonLista(False, constMANTNINGUNO)
End Sub

Private Sub txtObserv_GotFocus(Index As Integer)
    Call pBotonLista(True, constMANTAUTOTEXTOS)
    
    Select Case Index
        Case 0, 1
            msrdcCultivos.Refresh
        Case 2, 3
            msrdcTecnicas.Refresh
        Case 4
            msrdcColonias.Refresh
        Case 5
            msrdcPlacas.Refresh
    End Select
    
End Sub

Private Sub txtObserv_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    pAutotexto KeyCode
End Sub


Private Sub txtObserv_LostFocus(Index As Integer)
    Call pBotonLista(False, constMANTNINGUNO)
    
    On Error Resume Next
    Select Case Index
        Case 0, 1
            msrdcCultivos.Resultset.Close
        Case 2, 3
            msrdcTecnicas.Resultset.Close
        Case 4
            msrdcColonias.Resultset.Close
        Case 5
            msrdcPlacas.Resultset.Close
    End Select
    
End Sub

Private Sub txtResultados_GotFocus()
    Call pBotonLista(True, constMANTAUTOTEXTOS)
End Sub

Private Sub txtResultados_KeyDown(KeyCode As Integer, Shift As Integer)
    pAutotexto KeyCode
    
    On Error Resume Next
    If KeyCode = 13 Then
        If Shift <> 2 Then 'si shift=2 se ha pulsado Ctrl+ENTER --> retorno de carro
            cmdResultados_Click (0)
        End If
    End If

End Sub

Private Sub txtResultados_KeyPress(KeyAscii As Integer)
'El retorno de carro es Ctrl+ENTER (KeyAscii = 10) no ENTER (KeyAscii = 13)
    If KeyAscii = 13 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtResultados_LostFocus()
    Call pBotonLista(False, constMANTNINGUNO)
End Sub


