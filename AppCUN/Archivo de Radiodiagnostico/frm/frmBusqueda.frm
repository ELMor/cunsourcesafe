VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBusqueda 
   Caption         =   "Form1"
   ClientHeight    =   2910
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8190
   LinkTopic       =   "Form1"
   ScaleHeight     =   2910
   ScaleWidth      =   8190
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdButton 
      Caption         =   "&Buscar primero"
      Default         =   -1  'True
      Height          =   375
      Index           =   0
      Left            =   6480
      TabIndex        =   16
      Top             =   420
      Width           =   1545
   End
   Begin VB.CommandButton cmdButton 
      Caption         =   "Buscar &siguiente"
      Height          =   375
      Index           =   1
      Left            =   6480
      TabIndex        =   15
      Top             =   870
      Width           =   1545
   End
   Begin VB.CommandButton cmdButton 
      Cancel          =   -1  'True
      Caption         =   "&Cerrar"
      Height          =   375
      Index           =   2
      Left            =   6480
      TabIndex        =   14
      Top             =   1320
      Width           =   1545
   End
   Begin VB.CheckBox chkCase 
      Caption         =   "MAY�SCULAS/Min�sculas"
      Height          =   255
      Left            =   5760
      TabIndex        =   13
      Top             =   2145
      Width           =   2310
   End
   Begin VB.ComboBox cboSearch 
      Height          =   315
      ItemData        =   "frmBusqueda.frx":0000
      Left            =   5760
      List            =   "frmBusqueda.frx":000D
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   2460
      Width           =   2310
   End
   Begin VB.TextBox txtDiagnostico 
      BackColor       =   &H00FFFFFF&
      DataField       =   "RX06CODDIAGNOSTICO"
      Height          =   285
      Index           =   1
      Left            =   2880
      MaxLength       =   7
      TabIndex        =   6
      Tag             =   "C�digo de Diagn�stico"
      Top             =   960
      Width           =   810
   End
   Begin VB.TextBox txtDiagnostico 
      BackColor       =   &H00FFFFFF&
      DataField       =   "RX04CODAREA"
      Height          =   285
      Index           =   0
      Left            =   2340
      MaxLength       =   4
      TabIndex        =   5
      Tag             =   "C�digo de Localizaci�n"
      Top             =   960
      Width           =   510
   End
   Begin VB.CheckBox chkConfirmado 
      Caption         =   "Confirmado"
      DataField       =   "RX03INDCONFIRMADO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4800
      TabIndex        =   4
      Tag             =   "Confirmado?"
      Top             =   1020
      Width           =   1335
   End
   Begin VB.TextBox txtFicha 
      BackColor       =   &H00FFFFFF&
      DataField       =   "RX02DESORGANO"
      Height          =   285
      Index           =   0
      Left            =   2340
      MaxLength       =   300
      TabIndex        =   0
      Tag             =   "Descripci�n de T�cnica"
      Top             =   600
      Width           =   3690
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSTecnica 
      DataField       =   "RX05CODTECNICA"
      Height          =   285
      Left            =   2340
      TabIndex        =   1
      Tag             =   "T�cnica de Estudio"
      Top             =   240
      Width           =   3675
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1508
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3387
      Columns(1).Caption=   "T�cnica de Estudio"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6482
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSArea 
      DataField       =   "RX04CODAREA"
      Height          =   285
      Left            =   2340
      TabIndex        =   7
      Tag             =   "Localizaci�n"
      Top             =   1320
      Width           =   3735
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1323
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   18203
      Columns(1).Caption=   "Localizaci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSDiagnostico 
      DataField       =   "RX06CODDIAGNOSTICO"
      Height          =   285
      Left            =   2340
      TabIndex        =   8
      Tag             =   "Diagn�stico"
      Top             =   1680
      Width           =   3735
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1270
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   18230
      Columns(1).Caption=   "Diagn�stico"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblLabel1 
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   1560
      TabIndex        =   11
      Top             =   960
      Width           =   705
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Diagn�stico:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   1140
      TabIndex        =   10
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Localizaci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   1080
      TabIndex        =   9
      Top             =   1320
      Width           =   1155
   End
   Begin VB.Label lblLabel1 
      Caption         =   "T�cnica de Estudio:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   480
      TabIndex        =   3
      Top             =   240
      Width           =   1755
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Descripci�n de T�cnica:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   2160
   End
End
Attribute VB_Name = "frmBusqueda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdButton_Click(intIndex As Integer)
  Dim blnFound As Boolean
  Dim blnFirst As Boolean
  
  On Error GoTo cwIntError
  
  Select Case intIndex
    Case 0, 1
      If CheckValues Then
        Call objMouse.BusyOn
        blnFirst = (intIndex = 0)
        Select Case objApp.objActiveWin.objWinActiveForm.intFormType
          Case cwFormDetail
            If objApp.objActiveWin.objWinActiveForm.blnFilterOn Then
              blnFound = FindCursor(objApp.objActiveWin, blnFirst)
            Else
              blnFound = FindSQL(objApp.objActiveWin, blnFirst)
            End If
          Case cwFormMultiLine
            blnFound = FindGrid(objApp.objActiveWin, blnFirst)
        End Select
        If Not blnFound Then
          With objError
            Call .SetError(cwCodeMsg, "32") 'msgUsrSearchNotFound)
            Call .Raise
          End With
        Else
          Select Case objApp.objActiveWin.objWinActiveForm.intFormType
            Case cwFormDetail
              With objApp.objActiveWin
                Call .FormChangeStatus(cwModeSingleEdit)
                Call .DataRead
                Call .WinPrepareScr
              End With
            Case cwFormMultiLine
              With objApp.objActiveWin
                Call .FormChangeStatus(cwModeMultiLineEdit)
                Call .WinPrepareScr
              End With
          End Select
          Call cmdButton(1).SetFocus
        End If
        Call objMouse.BusyOff
      End If
    Case 2
      Call cmdButton(2).SetFocus
      Call Hide
  End Select
  
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "ButtonClick")
End Sub
Private Function FindCursor(ByVal objWin As clsCWWin, _
                            ByVal blnFirst As Boolean) As Boolean
  Dim vntMark   As Variant
  Dim blnFound  As Boolean
  Dim blnSearch As Boolean
  Dim objField  As clsCWFieldSearch
  Dim vntData1  As Variant
  Dim vntData2  As Variant

  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  With objWin.objWinActiveForm
    vntMark = .rdoCursor.Bookmark
    If blnFirst Then
      If .blnDirection Then
        Call .rdoCursor.MoveFirst
      Else
        Call .rdoCursor.MoveLast
      End If
    Else
      If .blnDirection Then
        Call .rdoCursor.MoveNext
      Else
        Call .rdoCursor.MovePrevious
      End If
    End If
    Do While ((.blnDirection And Not .rdoCursor.EOF) Or _
              (Not .blnDirection And Not .rdoCursor.BOF)) And _
             Not blnFound
      blnFound = True
    For Each objField In mcllFields
        vntData1 = .rdoCursor(objWin.CtrlGetField(objField.objControl))
        vntData1 = IIf(IsNull(vntData1), Empty, vntData1)
        If TypeName(objField.objControl) = cwComboBox Then
          vntData1 = CBool(vntData1)
        ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
          If IsDate(vntData1) Then
            vntData1 = CStr(CDate(vntData1))
          End If
        End If
        vntData1 = IIf(chkCase, vntData1, UCase(vntData1))
        If TypeName(objField.objControl) = cwComboBox Then
          vntData2 = IIf(objField.objControl.ListIndex = 0, "", CBool(objField.objControl.ListIndex - 2))
        ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
          If IsDate(objField.objControl.Date) Then
            vntData2 = CStr(CDate(objField.objControl.Date))
          Else
            vntData2 = ""
          End If
        Else
          vntData2 = objField.objControl
          vntData2 = IIf(IsNull(vntData2), Empty, vntData2)
        End If
        vntData2 = IIf(chkCase, vntData2, UCase(vntData2))
        If Not objGen.IsStrEmpty(CStr(vntData2)) Then
          Select Case cboSearch.ListIndex
            Case 0
              blnSearch = Left(vntData1, Len(Trim(vntData2))) = Trim(vntData2)
            Case 1
              blnSearch = InStr(vntData1, vntData2) > 0
            Case 2
              blnSearch = (vntData1 = vntData2)
          End Select
          If Not blnSearch Then
            blnFound = False
            Exit For
          End If
        End If
      Next
      If Not blnFound Then
        If .blnDirection Then
          Call .rdoCursor.MoveNext
        Else
          Call .rdoCursor.MovePrevious
        End If
      End If
    Loop
    If Not blnFound Then
      .rdoCursor.Bookmark = vntMark
    End If
    FindCursor = blnFound
  End With

cwResError:
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "FindCursor")
  Resume cwResError
End Function

Private Function FindSQL(ByVal objWin As clsCWWin, _
                         ByVal blnFirst As Boolean) As Boolean
  Dim objField   As clsCWFieldSearch
  Dim strMiWhere As String
  Dim strMiWher2 As String
  Dim strData1   As String
  Dim vntData2   As Variant
  Dim vntData3   As Variant
  Dim strControl As String
  Dim blnFound   As Boolean
  Dim cllValues  As New Collection
  Dim intType    As Integer
  Dim objCtrl    As clsCWCtrl
  Dim Cont       As Integer
  Dim blnCond    As Boolean
  Dim Col        As Integer
  Dim strParen1  As String
  Dim strParen2  As String
  
  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  With objWin.objWinActiveForm
    For Each objField In mcllFields
      If Not objGen.IsStrEmpty(objField.objControl) Then
        intType = mcllFields(objField.strField).intType
        Select Case intType
          Case cwNumeric, cwDecimal, cwDate
            strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
            strMiWhere = strMiWhere & objWin.CtrlGetField(objField.objControl) & _
                         "=" & objGen.ValueToSQL(objField.objControl, intType)
          Case cwBoolean
            If objField.objControl.ListIndex > 0 Then
              strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
              strMiWhere = strMiWhere & objWin.CtrlGetField(objField.objControl) & _
                           "=" & objGen.ValueToSQL(objField.objControl.ListIndex = 1, intType)
            End If
          Case cwString
            If objField.objControl.Tag <> "Lincado" Then
                strParen2 = ")"
                strParen1 = "("
            End If
            strControl = Trim(objField.objControl)
            'strData1 = IIf(chkCase, "", "{fn ucase(") & objWin.CtrlGetField(objField.objControl) & IIf(chkCase, "", ")}")
            strData1 = IIf(chkCase, "", "UPPER" & strParen1) & objWin.CtrlGetField(objField.objControl) & IIf(chkCase, "", strParen2)
            vntData2 = IIf(chkCase, strControl, UCase(strControl))
            strMiWhere = strMiWhere & IIf(Len(strMiWhere) = 0, "", " AND ")
            Select Case cboSearch.ListIndex
              Case 0
                strMiWher2 = strMiWhere & strData1 & " LIKE " & _
                             objGen.ValueToSQL(vntData2 & "%", cwString)
              Case 1
                strMiWher2 = strMiWhere & strData1 & " LIKE " & _
                             objGen.ValueToSQL("%" & vntData2 & "%", cwString)
              Case 2
                strMiWher2 = strMiWhere & strData1 & "=" & _
                             objGen.ValueToSQL(vntData2, cwString)
            End Select
           
            If objField.objControl.Tag = "Lincado" Then
                 strMiWher2 = strMiWher2 & ")"
            End If
           
          On Error Resume Next
          Set objCtrl = mcllCombo(objWin.CtrlGetField(objField.objControl))
          On Error GoTo cwIntError
                    
          'Salva: Para b�squedas con los combos
          
          If objCtrl Is Nothing = False Then
            With objCtrl.objControl
              Col = GetDBComboCol(objCtrl.objControl)
              strMiWhere = strMiWhere & .DataField & " IN ("
              .MoveFirst
              For Cont = 0 To .Rows
                vntData3 = IIf(chkCase, .Columns(Col).Value, UCase(.Columns(Col).Value))
                Select Case cboSearch.ListIndex
                Case 0
                    blnCond = (Left(vntData3, Len(vntData2)) = vntData2)
                Case 1
                    blnCond = (InStr(vntData3, vntData2) > 0)
                Case 2
                    blnCond = (vntData3 = vntData2)
                End Select
                
                If blnCond Then
                    If Mid(strMiWhere, Len(strMiWhere), 1) = "(" Then
                       strMiWhere = strMiWhere & .Columns(0).Value
                    Else
                       strMiWhere = strMiWhere & "," & .Columns(0).Value
                    End If
                End If
                .MoveNext
              Next
            End With
            If Mid(strMiWhere, Len(strMiWhere), 1) = "(" Then
              strMiWhere = strMiWhere & "-1)"
            Else
              strMiWhere = strMiWhere & ")"
            End If
          Else
            strMiWhere = strMiWhere & strMiWher2
          End If


        End Select
      End If
    Next
    
    Call objWin.DataCreateKeys(cllValues, False, True)
    Call objWin.DataRemoveCursor
    If blnFirst Then
      Call objWin.DataCreateCursor(cwGotoFirst, strMiWhere, False, True, cllValues)
    Else
      Call objWin.DataCreateCursor(cwGotoForzeNext, strMiWhere, False, True, cllValues)
    End If
    blnFound = objGen.GetRowCount(.rdoCursor) > 0
    If Not blnFound Then
      Call objWin.DataRemoveCursor
      Call objWin.DataCreateCursor(cwGotoActual, "", False, False, cllValues)
    End If
    Call objGen.RemoveCollection(cllValues)
    FindSQL = blnFound
  End With
  
cwResError:
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "FindSQL")
  Resume cwResError
End Function

Private Function CheckValues() As Boolean
  Dim objField As clsCWFieldSearch
  Dim blnRet   As Boolean
  
  blnRet = True
  For Each objField In mcllFields
    Select Case TypeName(objField.objControl)
      Case cwTextBox
        If Not objGen.IsStrEmpty(objField.objControl) Then
          Select Case mcllFields(objField.strField).intMask
            Case cwMaskInteger
              blnRet = objGen.ChangeType(objField.objControl, cwNumeric)
            Case cwMaskReal
              blnRet = objGen.ChangeType(objField.objControl, cwDecimal)
            Case Else
          End Select
          If Not blnRet Then
            Exit For
          End If
        End If
      Case Else
    End Select
  Next
  If Not blnRet Then
    With objError
      Call .SetError(cwCodeMsg, msgUsrFilterMaxSize, mcllFields(objField.strField).strSmallDesc)
      Call .Raise
    End With
  End If
  CheckValues = blnRet
End Function
Private Function FindGrid(ByVal objWin As clsCWWin, _
                          ByVal blnFirst As Boolean) As Boolean
  Dim vntMark      As Variant
  Dim lngPosition  As Long
  Dim blnFound     As Boolean
  Dim blnSearch    As Boolean
  Dim objField     As clsCWFieldSearch
  Dim vntData1     As Variant
  Dim vntData2     As Variant
  Dim blnNotSearch As Boolean

  On Error GoTo cwIntError
  
  Call objMouse.BusyOn
  
  With objWin.objWinActiveForm
    .grdGrid.Redraw = False
    vntMark = .grdGrid.Bookmark
    lngPosition = -1
    If blnFirst Then
      If .grdGrid.Rows > 0 Then
        Call .grdGrid.MoveFirst
      Else
        blnNotSearch = True
      End If
    Else
      If .grdGrid.AddItemRowIndex(.grdGrid.Bookmark) < .grdGrid.Rows - 1 Then
        Call .grdGrid.MoveNext
      Else
        blnNotSearch = True
      End If
    End If
    If Not blnNotSearch Then
      Do While lngPosition <> .grdGrid.AddItemRowIndex(.grdGrid.Bookmark) And Not blnFound
        lngPosition = .grdGrid.AddItemRowIndex(.grdGrid.Bookmark)
        blnFound = True
        For Each objField In mcllFields
          vntData1 = objWin.objWinActiveForm.cllControls(objField.strField).objControl.Value
          vntData1 = IIf(IsNull(vntData1), Empty, vntData1)
          If TypeName(objField.objControl) = cwComboBox Then
            vntData1 = CBool(vntData1)
          ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
            If IsDate(vntData1) Then
              vntData1 = CStr(CDate(vntData1))
            End If
          End If
          vntData1 = IIf(chkCase, vntData1, UCase(vntData1))
          If TypeName(objField.objControl) = cwComboBox Then
            vntData2 = IIf(objField.objControl.ListIndex = 0, "", CBool(objField.objControl.ListIndex - 2))
          ElseIf TypeName(objField.objControl) = cwSSDateCombo Then
            If IsDate(objField.objControl.Date) Then
              vntData2 = CStr(CDate(objField.objControl.Date))
            Else
              vntData2 = ""
            End If
          Else
            vntData2 = objField.objControl
            vntData2 = IIf(IsNull(vntData2), Empty, vntData2)
          End If
          vntData2 = IIf(chkCase, vntData2, UCase(vntData2))
          If Not objGen.IsStrEmpty(CStr(vntData2)) Then
            Select Case cboSearch.ListIndex
              Case 0
                blnSearch = Left(vntData1, Len(Trim(vntData2))) = Trim(vntData2)
              Case 1
                blnSearch = InStr(vntData1, vntData2) > 0
              Case 2
                blnSearch = (vntData1 = vntData2)
            End Select
            If Not blnSearch Then
              blnFound = False
              Exit For
            End If
          End If
        Next
        If Not blnFound Then
          Call .grdGrid.MoveNext
        End If
      Loop
    End If
    If blnFound Then
      vntMark = .grdGrid.Bookmark
    End If
    .grdGrid.Redraw = True
    .grdGrid.Bookmark = vntMark
    FindGrid = blnFound
  End With
  
cwResError:
  Call objMouse.BusyOff
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "FindGrid")
  Resume cwResError
End Function

