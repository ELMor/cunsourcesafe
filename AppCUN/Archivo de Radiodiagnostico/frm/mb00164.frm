VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmH_FechasLectura 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Fechas de Lectura de T�cnicas y Cultivos"
   ClientHeight    =   6780
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9765
   HelpContextID   =   30001
   Icon            =   "mb00164.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6780
   ScaleWidth      =   9765
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraPruebaAsistencia 
      Caption         =   "Paciente/Prueba"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2505
      Index           =   0
      Left            =   60
      TabIndex        =   14
      Top             =   480
      Width           =   9585
      Begin TabDlg.SSTab tabPruebaAsistencia 
         Height          =   2115
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   300
         Width           =   9360
         _ExtentX        =   16510
         _ExtentY        =   3731
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00164.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(18)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(16)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(14)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(8)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(9)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(10)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(11)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(13)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtPruebaAsistencia(12)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtPruebaAsistencia(11)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtPruebaAsistencia(10)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtPruebaAsistencia(9)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtPruebaAsistencia(8)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtPruebaAsistencia(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtPruebaAsistencia(7)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "chkPruebaAsistencia(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtPruebaAsistencia(6)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtPruebaAsistencia(1)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtPruebaAsistencia(2)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtPruebaAsistencia(3)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtPruebaAsistencia(4)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtPruebaAsistencia(5)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtPruebaAsistencia(15)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtPruebaAsistencia(14)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtPruebaAsistencia(13)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).ControlCount=   27
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00164.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPruebaAsistencia(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPac"
            Height          =   285
            Index           =   13
            Left            =   2580
            TabIndex        =   33
            Top             =   1680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPrueba"
            Height          =   285
            Index           =   14
            Left            =   2340
            TabIndex        =   32
            Top             =   1680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPetic"
            Height          =   285
            Index           =   15
            Left            =   2100
            TabIndex        =   31
            Top             =   1680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "desigPrueba"
            Height          =   285
            Index           =   5
            Left            =   2940
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Prueba solicitada|Prueba solicitada"
            Top             =   120
            Width           =   4815
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "drSolicitante"
            Height          =   285
            Index           =   4
            Left            =   2940
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Dr. Solicitud|Dr. que solicita la Prueba"
            Top             =   840
            Width           =   2715
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "dptSolicitante"
            Height          =   285
            Index           =   3
            Left            =   6660
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Dpto. Solicitud|Dpto. que solicita la Prueba"
            Top             =   840
            Width           =   2295
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "cama"
            Height          =   285
            Index           =   2
            Left            =   840
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Cama|Cama"
            Top             =   1200
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "paciente"
            Height          =   285
            Index           =   1
            Left            =   2940
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Paciente|Nombre y apellidos del paciente"
            Top             =   480
            Width           =   4155
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   765
            Index           =   6
            Left            =   2940
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Indicaciones|Indicaciones sobre la Prueba"
            Top             =   1200
            Width           =   6015
         End
         Begin VB.CheckBox chkPruebaAsistencia 
            Alignment       =   1  'Right Justify
            Caption         =   "Urgente:"
            DataField       =   "urgenciaRealizacion"
            Height          =   255
            Index           =   0
            Left            =   7920
            TabIndex        =   10
            Tag             =   "Urgente|Prueba Urgente"
            Top             =   120
            Width           =   1005
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   7
            Left            =   840
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "N� Ref.|N� de Referencia"
            Top             =   120
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "nRef"
            Height          =   285
            Index           =   0
            Left            =   840
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "N� Ref.|N� de Referencia"
            Top             =   0
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "historia"
            Height          =   285
            Index           =   8
            Left            =   840
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Historia|N� de historia del paciente"
            Top             =   480
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "caso"
            Height          =   285
            Index           =   9
            Left            =   840
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Caso|N� de caso"
            Top             =   840
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "fnac"
            Height          =   285
            Index           =   10
            Left            =   7920
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Fecha Nacimiento|Fecha de nacimiento del paciente"
            Top             =   480
            Width           =   1035
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "secuencia"
            Height          =   285
            Index           =   11
            Left            =   60
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   1740
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "nRepeticion"
            Height          =   285
            Index           =   12
            Left            =   60
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   1500
            Visible         =   0   'False
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPruebaAsistencia 
            Height          =   1905
            Index           =   0
            Left            =   -74910
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   90
            Width           =   8835
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15584
            _ExtentY        =   3360
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Prueba:"
            Height          =   255
            Index           =   13
            Left            =   2340
            TabIndex        =   30
            Top             =   120
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr. Solic.:"
            Height          =   255
            Index           =   11
            Left            =   2160
            TabIndex        =   29
            Top             =   900
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dpto. Solic.:"
            Height          =   255
            Index           =   10
            Left            =   5700
            TabIndex        =   28
            Top             =   900
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cama:"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   27
            Top             =   1260
            Width           =   585
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Paciente:"
            Height          =   255
            Index           =   8
            Left            =   2220
            TabIndex        =   26
            Top             =   480
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Ref:"
            Height          =   255
            Index           =   7
            Left            =   180
            TabIndex        =   25
            Top             =   180
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Indicaciones:"
            Height          =   195
            Index           =   1
            Left            =   1920
            TabIndex        =   24
            Top             =   1260
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia:"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   23
            Top             =   540
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Caso:"
            Height          =   255
            Index           =   16
            Left            =   300
            TabIndex        =   22
            Top             =   900
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            Caption         =   "F. Nac:"
            Height          =   255
            Index           =   18
            Left            =   7320
            TabIndex        =   21
            Top             =   540
            Width           =   540
         End
      End
   End
   Begin VB.Frame fraTecnicas 
      Caption         =   "T�cnicas/Cultivos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Index           =   0
      Left            =   60
      TabIndex        =   18
      Top             =   3060
      Width           =   9585
      Begin SSDataWidgets_B.SSDBGrid grdssTecnicas 
         Height          =   2940
         Index           =   0
         Left            =   180
         TabIndex        =   17
         Top             =   300
         Width           =   9315
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16431
         _ExtentY        =   5186
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   19
      Top             =   6495
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmH_FechasLectura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objPruebaAsistencia As New clsCWForm
Dim objMultiInfo As New clsCWForm

Private Sub chkPruebaAsistencia_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkPruebaAsistencia_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub


Private Sub chkPruebaAsistencia_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub


Private Sub Form_Load()
  Dim strKey As String, SQL As String
  Dim cCarpeta As String, desigCarpeta As String
  Dim i As Integer
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn

'  'se obtiene la carpeta
'  If objPipe.PipeExist("cCarpeta") Then
'      cCarpeta = objPipe.PipeGet("cCarpeta")
'  Else
'      cCarpeta = ""
'  End If
'  If objPipe.PipeExist("Carpeta") Then
'      desigCarpeta = objPipe.PipeGet("Carpeta")
'  Else
'      desigCarpeta = ""
'  End If

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objPruebaAsistencia
    .strName = "PRUEBAASISTENCIA"
    Set .objFormContainer = fraPruebaAsistencia(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPruebaAsistencia(0)
    Set .grdGrid = grdssPruebaAsistencia(0)
    .intAllowance = cwAllowReadOnly
    '.strDataBase = objEnv.GetValue("Main")
    
    'Se traen las pruebas que tienen t�cnicas o cultivos en estado de 'realizada'
    .strTable = "PRUEBAASISTENCIA_2"
    SQL = "Proceso=" & constPROCESOSOLICITUD
'    If cCarpeta = "" Then
        SQL = SQL & " AND cCarpeta IN "
        SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
'    Else
'        SQL = SQL & " AND cCarpeta = " & cCarpeta
'        If desigCarpeta <> "" Then
'            Me.Caption = Me.Caption & " - " & desigCarpeta
'        End If
'    End If
    'NOTA LABOR:Para seguir mostrando pruebas que tienen alg�n resultado validado en Laboratorio
    SQL = SQL & " AND (estado = " & constPRUEBAREALIZANDO
    SQL = SQL & " OR estado = " & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND NRef IN "
    SQL = SQL & "       (SELECT NRef FROM MB2000"
    SQL = SQL & "        WHERE MB34_CODESTTECASIST = " & constTECREALIZADA & ")"
    .strWhere = SQL
'    whereInicial = SQL
        
    Call .FormAddOrderField("NRef", cwAscending)
      
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "PRUEBAASISTENCIA")
    Call .FormAddFilterWhere(strKey, "nRef", "N� Ref.", cwString)
    Call .FormAddFilterWhere(strKey, "historia", "Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "caso", "Caso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "paciente", "Paciente", cwString)
    Call .FormAddFilterWhere(strKey, "cama", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fnac", "Fecha Nac.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "desigPrueba", "Prueba", cwString)
    Call .FormAddFilterWhere(strKey, "dptSolicitud", "Dpto. Solicitud", cwString)
    Call .FormAddFilterWhere(strKey, "drSolicitud", "Dr. Solicitud", cwString)
    Call .FormAddFilterWhere(strKey, "urgenciaRealizacion", "Urgente", cwBoolean)
     
    Call .FormAddFilterOrder(strKey, "nRef", "N� Ref.")
    Call .FormAddFilterOrder(strKey, "historia", "historia")
    Call .FormAddFilterOrder(strKey, "caso", "caso")
    Call .FormAddFilterOrder(strKey, "paciente", "Paciente")
    Call .FormAddFilterOrder(strKey, "cama", "Cama")
    Call .FormAddFilterOrder(strKey, "fnac", "Fecha Nac.")
    Call .FormAddFilterOrder(strKey, "desigPrueba", "Prueba")
    Call .FormAddFilterOrder(strKey, "dptSolicitante", "Dpto. solicitud")
    Call .FormAddFilterOrder(strKey, "drSolicitante", "Dr. Solicitud")
    
'    Call .objPrinter.Add("MB00101", "Informe de resultados informados", 1, crptToPrinter)
'    Call .objPrinter.Add("MB00110", "Hoja de realizaci�n")
    
   End With
   
   
  With objMultiInfo
    Set .objFormContainer = fraTecnicas(0)
    Set .objFatherContainer = fraPruebaAsistencia(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssTecnicas(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2000"
    .strWhere = "MB34_codEstTecAsist = " & constTECREALIZADA
    Call .FormAddOrderField("MB20_codTecAsist", cwAscending)
    Call .FormAddRelation("nRef", txtPruebaAsistencia(0))
    .intAllowance = cwAllowModify
    
  End With


  With objWinInfo
    Call .FormAddInfo(objPruebaAsistencia, cwFormDetail)
    
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "nRef", "nRef", cwString, 12)
    Call .GridAddColumn(objMultiInfo, "Cod Tec", "MB09_CODTEC", cwNumeric, 4)
    Call .GridAddColumn(objMultiInfo, "T�cnica", "", cwString, 20)
    Call .GridAddColumn(objMultiInfo, "Recip", "", cwString, 20)
    Call .GridAddColumn(objMultiInfo, "N�", "MB20_codPlaca", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Cultivo", "", cwString, 23)
    Call .GridAddColumn(objMultiInfo, "Tipo Origen", "MB20_tiOrig", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "Cod Origen", "MB20_codOrig", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Origen", "", cwString, 20)
    Call .GridAddColumn(objMultiInfo, "Fecha Solicitud", "MB20_fecReali", cwString, 20)
    Call .GridAddColumn(objMultiInfo, "Fecha Lectura", "MB20_fecLect", cwString, 20)
    
    Call .FormCreateInfo(objPruebaAsistencia)
    Call .FormChangeColor(objPruebaAsistencia)
    'controles ReadOnly
    For i = 0 To txtPruebaAsistencia.Count - 1
        .CtrlGetInfo(txtPruebaAsistencia(i)).blnReadOnly = True
    Next i
    'datos no visibles en la tabla
    .CtrlGetInfo(txtPruebaAsistencia(11)).blnInGrid = False 'secuencia
    .CtrlGetInfo(txtPruebaAsistencia(12)).blnInGrid = False 'n� repetici�n
    .CtrlGetInfo(txtPruebaAsistencia(13)).blnInGrid = False 'comPetic
    .CtrlGetInfo(txtPruebaAsistencia(14)).blnInGrid = False 'comPrueba
    .CtrlGetInfo(txtPruebaAsistencia(15)).blnInGrid = False 'comPac
    'informaci�n sobre b�squedas
    .CtrlGetInfo(txtPruebaAsistencia(0)).blnInFind = True 'num. ref.
    .CtrlGetInfo(txtPruebaAsistencia(1)).blnInFind = True 'paciente
    .CtrlGetInfo(txtPruebaAsistencia(2)).blnInFind = True 'cama
    .CtrlGetInfo(txtPruebaAsistencia(3)).blnInFind = True 'prueba
    .CtrlGetInfo(txtPruebaAsistencia(4)).blnInFind = True 'dpt
    .CtrlGetInfo(txtPruebaAsistencia(5)).blnInFind = True 'dr
    .CtrlGetInfo(txtPruebaAsistencia(8)).blnInFind = True 'historia
    .CtrlGetInfo(txtPruebaAsistencia(9)).blnInFind = True 'caso
        
    Call .FormChangeColor(objMultiInfo)
    grdssTecnicas(0).Columns(3).Visible = False
    grdssTecnicas(0).Columns(4).Visible = False
    .CtrlGetInfo(grdssTecnicas(0).Columns(5)).blnReadOnly = True
    grdssTecnicas(0).Columns(5).Width = 2000
    grdssTecnicas(0).Columns(6).Visible = False
    grdssTecnicas(0).Columns(7).Visible = False
    .CtrlGetInfo(grdssTecnicas(0).Columns(8)).blnReadOnly = True
    grdssTecnicas(0).Columns(8).Width = 1300
    grdssTecnicas(0).Columns(9).Visible = False
    grdssTecnicas(0).Columns(10).Visible = False
    .CtrlGetInfo(grdssTecnicas(0).Columns(11)).blnReadOnly = True
    grdssTecnicas(0).Columns(11).Width = 1500
    .CtrlGetInfo(grdssTecnicas(0).Columns(12)).blnReadOnly = True
    grdssTecnicas(0).Columns(12).Width = 1500
    .CtrlGetInfo(grdssTecnicas(0).Columns(13)).blnForeign = True
    grdssTecnicas(0).Columns(13).Width = 1500
    
    ' se a�aden los links de la multil�nea
    ' T�cnica
    SQL = "SELECT MB09_desig, MB03_desig FROM MB0900, MB0300 WHERE MB0900.MB09_codTec = ? AND MB0300.MB03_codRecip = MB0900.MB03_codRecip"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssTecnicas(0).Columns(4)), "MB09_CODTEC", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssTecnicas(0).Columns(4)), grdssTecnicas(0).Columns(5), "MB09_DESIG")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssTecnicas(0).Columns(4)), grdssTecnicas(0).Columns(6), "MB03_DESIG")

    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub fraPruebaAsistencia_Click(Index As Integer)
  Call objWinInfo.FormChangeActive(fraPruebaAsistencia(Index), False, True)
End Sub

Private Sub fraTecnicas_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraTecnicas(intIndex), False, True)
End Sub

Private Sub grdSSPruebaAsistencia_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdSSPruebaAsistencia_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub


Private Sub grdSSPruebaAsistencia_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub


Private Sub grdSSPruebaAsistencia_RowColChange(Index As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSPruebaAsistencia_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    grdssPruebaAsistencia(Index).Columns(1).Text = Val(Mid$(grdssPruebaAsistencia(Index).Columns(1).Text, 6))
End Sub


Private Sub grdssTecnicas_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    KeyCode = 0
End Sub

Private Sub grdssTecnicas_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    If grdssTecnicas(0).Columns(7).Text <> 0 Then
        grdssTecnicas(0).Columns(8).Text = grdssTecnicas(0).Columns(6).Text & " " & grdssTecnicas(0).Columns(7).Text
    End If
    If grdssTecnicas(0).Columns(9).Text <> "" Then
        grdssTecnicas(0).Columns(11).Text = fOrigenTecnica(grdssTecnicas(0).Columns(9).Text) & " " & grdssTecnicas(0).Columns(10).Text
    End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    Dim strFechaEnviada$
    Dim strFechaDevuelta$
    Dim strHora$
    
    strFechaEnviada = Format(grdssTecnicas(0).Columns(13).Text, "dd/mm/yyyy")
    strHora = Format(grdssTecnicas(0).Columns(13).Text, "hh:mm:ss")
    
    If objMultiInfo.strName = strFormName Then
        strFechaDevuelta = fVentanaCalendario(Format(grdssTecnicas(0).Columns(13).Text, "dd/mm/yyyy"), Format(grdssTecnicas(0).Columns(12).Text, "dd/mm/yyyy"), "")
        If Trim$(strFechaEnviada) <> Trim$(strFechaDevuelta) Then
            grdssTecnicas(0).Columns(13).Text = strFechaDevuelta & " " & strHora
            Call objWinInfo.CtrlDataChange
        End If
    End If
    
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    
   If objPruebaAsistencia.strName = strFormName Then
        If objGen.GetRowCount(objPruebaAsistencia.rdoCursor) > 0 Then
            'Se completan y formatean los datos que no se han podido traer directamente
            'nRef
            txtPruebaAsistencia(7).Text = Val(Mid$(txtPruebaAsistencia(0).Text, 6))
            'Indicaciones
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPetic")) Then
                txtPruebaAsistencia(6).Text = "PETICI�N: " & objPruebaAsistencia.rdoCursor("comPetic")
            End If
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPrueba")) Then
                txtPruebaAsistencia(6).Text = txtPruebaAsistencia(6).Text & Chr$(13) & Chr$(10) & "PRUEBA: " & objPruebaAsistencia.rdoCursor("comPrueba")
            End If
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPac")) Then
                txtPruebaAsistencia(6).Text = txtPruebaAsistencia(6).Text & Chr$(13) & Chr$(10) & "PACIENTE: " & objPruebaAsistencia.rdoCursor("comPac")
            End If
        End If
    End If
    
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  Select Case strFormName
'    Case objMasterInfo.strName
'        Call objWinInfo.FormPrinterDialog(True, "")
'        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'        intReport = objPrinter.Selected
'        If intReport > 0 Then
'          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                     objWinInfo.DataGetOrder(blnHasFilter, True))
'        End If
'        Set objPrinter = Nothing
'
' Case objMultiInfo.strName
'        Call objWinInfo.FormPrinterDialog(True, "")
'        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'        intReport = objPrinter.Selected
'        If intReport > 0 Then
'          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                     objWinInfo.DataGetOrder(blnHasFilter, True))
'        End If
'        Set objPrinter = Nothing
'
'  End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tabPruebaAsistencia_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabPruebaAsistencia(Index), False, True)
End Sub


Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub






Private Sub grdssTecnicas_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTecnicas_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTecnicas_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTecnicas_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub







Private Sub txtPruebaAsistencia_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtPruebaAsistencia_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub


Private Sub txtPruebaAsistencia_LostFocus(Index As Integer)
     Call objWinInfo.CtrlLostFocus
End Sub


