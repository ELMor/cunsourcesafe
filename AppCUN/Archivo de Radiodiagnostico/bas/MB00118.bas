Attribute VB_Name = "modIcons"
Option Explicit

' Constantes de los iconos de peticiones

Public Const constIconMuestra = 1
Public Const constIconTecnica = 2
Public Const constIconMicroorganismo = 3
Public Const constIconMorfologia = 4
Public Const constIconPlaca = 5
Public Const constIconColonia = 6
Public Const constIconResultado = 7
Public Const constIconAntibiotico = 8
Public Const constIconGrAntibiotico = 9
Public Const constIconTipo = 10
Public Const constIconProtocolo = 11
Public Const constIconAumentar = 12
Public Const constIconReducir = 13
Public Const constIconStop = 14
