Attribute VB_Name = "modFunciones_02"
Option Explicit


'constantes del resultado de una operacion sobre la base de datos
Public Const constCORRECTO = 0
Public Const constINCORRECTO = 1
Public Const constOTRO = 2
Public Function fFormatearRecuento(result$, cTiTec%, formatoBD As Boolean) As String
'Formatea el resultado de t�cnicas tipo recuento a un formato num�rico.

'Si el resultado introducido es > constMAXRECUENTO se anotar� como resultado _
constMAXRECUENTO + 1
    
    If cTiTec = constRECUENTO Then
        If formatoBD = True Then
            If (Left$(Trim$(result), 1) = ">" And Val(Mid$(Trim$(result), 2)) = constMAXRECUENTO) Or Val(result) > constMAXRECUENTO Then
                fFormatearRecuento = constMAXRECUENTO + 1
            Else
                fFormatearRecuento = Val(result)
            End If
        Else
            If result = CStr(constMAXRECUENTO + 1) Then
                fFormatearRecuento = ">" & constMAXRECUENTO
            Else
                fFormatearRecuento = result
            End If
        End If
    Else
        fFormatearRecuento = result
    End If

End Function
Public Function fEstadoNodo(tabla, estado) As String
Dim SQL As String
Dim rsEstado As rdoResultset, qryEstado As rdoQuery
Dim pipeName As String
Dim textoEstado As String

pipeName = "MB_" & tabla & "_" & estado

If objPipe.PipeExist(pipeName) = True Then
    fEstadoNodo = objPipe.PipeGet(pipeName)
Else
    Select Case tabla
    Case constESTADOTEC 'T�cnicas
        SQL = "SELECT MB34_desig"
        SQL = SQL & " FROM MB3400"
        SQL = SQL & " WHERE MB34_codEstTecAsist= ?"
        Set qryEstado = objApp.rdoConnect.CreateQuery("SelEstTec", SQL)
        qryEstado(0) = estado
        Set rsEstado = qryEstado.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rsEstado.EOF = False Then
            textoEstado = "(" & rsEstado(0) & ")"
        Else
            textoEstado = "(Estado no def.)"
        End If
        rsEstado.Close
        qryEstado.Close

    Case constESTADOPLACA
        If estado = constCOLPLACAASOCIADA Then 'no se va a poner ning�n texto si es el estado 1 (asociada)
            textoEstado = ""
        Else
            SQL = "SELECT MB37_desig"
            SQL = SQL & " FROM MB3700"
            SQL = SQL & " WHERE MB37_codEstPlaca= ?"
            Set qryEstado = objApp.rdoConnect.CreateQuery("SelEstPlaca", SQL)
            qryEstado(0) = estado
            Set rsEstado = qryEstado.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If rsEstado.EOF = False Then
                textoEstado = "(" & rsEstado(0) & ")"
            Else
                textoEstado = "(Estado no def.)"
            End If
            rsEstado.Close
            qryEstado.Close
        End If
        
    Case constESTADORESULT
        If estado = constRESULTSOLICITADO Then 'no se va a poner ning�n texto si es el estado 1 (solicitado)
            textoEstado = ""
        Else
            SQL = "SELECT MB38_desig"
            SQL = SQL & " FROM MB3800"
            SQL = SQL & " WHERE MB38_codEstResult= ?"
            Set qryEstado = objApp.rdoConnect.CreateQuery("SelEstResult", SQL)
            qryEstado(0) = estado
            Set rsEstado = qryEstado.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If rsEstado.EOF = False Then
                textoEstado = "(" & rsEstado(0) & ")"
            Else
                textoEstado = "(Estado no def.)"
            End If
            rsEstado.Close
            qryEstado.Close
        End If
        
    Case constESTADOCOL
        SQL = "SELECT MB36_desig"
        SQL = SQL & " FROM MB3600"
        SQL = SQL & " WHERE MB36_codEstCol= ?"
        Set qryEstado = objApp.rdoConnect.CreateQuery("SelEstCol", SQL)
        qryEstado(0) = estado
        Set rsEstado = qryEstado.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rsEstado.EOF = False Then
            textoEstado = "(" & rsEstado(0) & ")"
        Else
            textoEstado = "(Estado no def.)"
        End If
        rsEstado.Close
        qryEstado.Close

    End Select
    
    Call objPipe.PipeSet(pipeName, textoEstado)
    fEstadoNodo = textoEstado
End If

End Function

Public Function fIdentifCol(desigMicro$, desigGeMicro$, desigMorf$) As String
'devuelve la identificaci�n de la colonia
    
    If desigMicro <> "" Then
        fIdentifCol = desigMicro
    ElseIf desigGeMicro <> "" Then
        fIdentifCol = desigGeMicro
    ElseIf desigMorf <> "" Then
        fIdentifCol = desigMorf
    End If
    
End Function

Public Function fNextClave(campo$, tabla$) As String
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
    
End Function



Public Sub pAnotarMantenimiento(intMantenimiento%)
'anota un mantenimiento en la matriz de mantenimientos
    Dim dimension%, i%
    
    On Error Resume Next
    dimension = UBound(arMantenimientos)
    On Error GoTo 0
    
    For i = 1 To dimension
        If arMantenimientos(i) = intMantenimiento Then
            Exit Sub
        End If
    Next i
    dimension = dimension + 1
    ReDim Preserve arMantenimientos(1 To dimension)
    arMantenimientos(dimension) = intMantenimiento

End Sub

Public Sub pComboResultCargar(objWinInfo As clsCWWin, cbossResult As SSDBCombo, codTec$)
'Para actualizar la lista del combo de los resultados
    Dim SQL As String
    
    SQL = "SELECT MB32_codResult,MB32_desig"
    SQL = SQL & " FROM MB3200"
    SQL = SQL & " WHERE MB09_codTec = " & codTec
    SQL = SQL & " ORDER BY MB32_desig"
    cbossResult.RemoveAll
    objWinInfo.CtrlGetInfo(cbossResult).strSQL = SQL
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossResult))
        
End Sub

Public Sub pComboResultTexto(cbossResult As SSDBCombo, codTec$, codResult$)
'Para actualizar el texto del combo de los resultados
    Dim rsTecRes As rdoResultset, qryTecRes As rdoQuery
    Dim SQL As String
            
    SQL = "SELECT MB32_desig"
    SQL = SQL & " FROM MB3200"
    SQL = SQL & " WHERE MB09_codTec = ?"
    SQL = SQL & " AND MB32_codResult = ?"
    SQL = SQL & " ORDER BY MB32_desig"
    Set qryTecRes = objApp.rdoConnect.CreateQuery("Result", SQL)

    qryTecRes(0) = codTec
    qryTecRes(1) = codResult
    Set rsTecRes = qryTecRes.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    cbossResult.Text = rsTecRes(0)
    
    rsTecRes.Close
    qryTecRes.Close
    
End Sub



Public Sub pUpdateNodoColPlaca(nodo As Node, cCol%, identif$, estado%)
'actualiza un nodo colonia-placa
    Dim identifColonia$
    
    If identif = "" Then identifColonia = "Sin identificar" Else identifColonia = identif
    'nodo.Text = "Colonia " & cCol & ": " & identifColonia & RTrim$(Space(5) & fEstadoColPlaca(estado))
    nodo.Text = "Colonia " & cCol & ": " & identifColonia & RTrim$(Space(5) & fEstadoNodo(constESTADOPLACA, estado))
    nodo.Tag = estado
    
End Sub

Public Sub pUpdateNodoColPlacaEstado(nodo As Node, estado%)
'actualiza el estado un nodo colonia-placa
    
    If estado = constCOLPLACAANULADA Then
        If nodo.Tag <> constCOLPLACAANULADA Then
            'nodo.Text = nodo.Text & Space(5) & fEstadoColPlaca(estado)
            nodo.Text = nodo.Text & Space(5) & fEstadoNodo(constESTADOPLACA, estado)
        End If
    ElseIf estado = constCOLPLACAASOCIADA Then
        'nodo.Text = RTrim$(objGen.ReplaceStr(nodo.Text, fEstadoColPlaca(Val(nodo.Tag)), "", 1))
        nodo.Text = RTrim$(objGen.ReplaceStr(nodo.Text, fEstadoNodo(constESTADOPLACA, Val(nodo.Tag)), "", 1))
        'nodo.Text = rtrim$(Left$(nodo.Text, Len(nodo.Text) - Len(fEstadoColPlaca(constCOLPLACAANULADA))))
    End If
    nodo.Tag = estado
    
End Sub
Public Sub pUpdateNodoColonia(nodo As Node, cCol%, identif$, estado%)
'actualiza un nodo colonia-placa
    Dim identifColonia$
    
    If identif = "" Then identifColonia = "" Else identifColonia = ": " & identif
    'nodo.Text = "Colonia " & cCol & identifColonia & RTrim$(Space(5) & fEstadoColonia(estado))
    nodo.Text = "Colonia " & cCol & identifColonia & RTrim$(Space(5) & fEstadoNodo(constESTADOCOL, estado))
    nodo.Tag = estado
    
End Sub

Public Sub pUpdateNodoColoniaEstado(nodo As Node, estado%)
'actualiza el estado de un nodo colonia-placa

    'nodo.Text = objGen.ReplaceStr(nodo.Text, fEstadoColonia(Val(nodo.Tag)), fEstadoColonia(estado), 1)
    nodo.Text = objGen.ReplaceStr(nodo.Text, fEstadoNodo(constESTADOCOL, Val(nodo.Tag)), fEstadoNodo(constESTADOCOL, estado), 1)
    'nodo.Text = Left$(nodo.Text, Len(nodo.Text) - Len(fEstadoColonia(Val(nodo.Tag)))) & fEstadoColonia(estado)
    nodo.Tag = estado
    
End Sub
'Public Function fVentanaNRef() As String
''llama a la ventana de busqueda de un N� de ref y devuelve este.
'
'    frmG_BuscRef.Show vbModal
'    fVentanaNRef = objPipe.PipeGet("BuscNRef")
'
'End Function

'Sub pAccesoPeticion()
'
'    frmG_Identificacion.Show vbModal
'    If frmG_Identificacion.LoginSucceeded = True Then
'        Unload frmG_Identificacion
'        frmP_Peticion.Show vbModal
'        Set frmP_Peticion = Nothing
'    Else
'        Unload frmG_Identificacion
'    End If
'
'End Sub

Public Sub pUpdateNodoTecnicaEstado(nodo As Node, estado%, fecReali As Boolean)
'cambia el estado de un nodo t�cnica/cultivo
Dim strLeerNodo As String

    If Left$(nodo.Key, 1) = "C" And estado <> constTECREALIZADA Then
        strLeerNodo = "*  "
    End If
    If fecReali = True Then
        'nodo.Text = objGen.ReplaceStr(nodo.Text, fEstadoTecnica(Val(nodo.Tag)), fEstadoTecnica(estado), 1)
        nodo.Text = strLeerNodo & objGen.ReplaceStr(nodo.Text, fEstadoNodo(constESTADOTEC, Val(nodo.Tag)), fEstadoNodo(constESTADOTEC, estado), 1)
    Else
        'nodo.Text = Trim$(Left$(nodo.Text, InStr(nodo.Text, "     "))) & Space(10) & fEstadoTecnica(estado)
        nodo.Text = strLeerNodo & Trim$(Left$(nodo.Text, InStr(nodo.Text, "     "))) & Space(10) & fEstadoNodo(constESTADOTEC, estado)
    End If
    nodo.Tag = estado & Mid$(nodo.Tag, InStr(nodo.Tag, "/"))
    
End Sub


Public Sub pUpdateNodoResultTecnicaEstado(nodo As Node, estado%)
'cambia el estado de un nodo resultado de t�cnica

    If Val(nodo.Tag) = constRESULTSOLICITADO Then
        nodo.Text = nodo.Text & Space(5) & fEstadoNodo(constESTADORESULT, estado)
    Else
        nodo.Text = objGen.ReplaceStr(nodo.Text, fEstadoNodo(constESTADORESULT, Val(nodo.Tag)), fEstadoNodo(constESTADORESULT, estado), 1)
    End If
    nodo.Tag = estado & Mid$(nodo.Tag, InStr(nodo.Tag, "U"))
    
End Sub
Function fFechaActual() As String
    Dim SQL As String, rdo As rdoResultset
    
    SQL = "SELECT SYSDATE FROM DUAL"
    Set rdo = objApp.rdoConnect.OpenResultset(SQL)
    fFechaActual = Format$(rdo(0), "DD/MM/YYYY")
    rdo.Close
End Function

Function fFechaHoraActual() As String
    Dim SQL As String, rdo As rdoResultset
    
    SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rdo = objApp.rdoConnect.OpenResultset(SQL)
    fFechaHoraActual = rdo(0)
    rdo.Close
End Function

