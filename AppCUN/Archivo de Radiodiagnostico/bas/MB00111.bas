Attribute VB_Name = "modMontajeMesa"
Option Explicit

Public Sub pCargarTecnicasSobreMuestra(arTecPend() As typeTecMesa, origen1 As String, origen2 As String, cCarpeta As String, fechaAhora As String)
    ' Se cargan las t�cnicas que hay que ver hoy y que se realicen sobre la muestra
    
    Dim SQL  As String
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim dimension As Integer
    Dim fLect As String
    
    fLect = Format(DateAdd("d", 1, fechaAhora), "dd/mm/yyyy")
    
    ' ****************************CUANDO SE SACAN PRIMERO LAS DE MUESTRA Y **************
    ' ****************************DESPUES LAS DEL DIA ANTERIOR **************************
'    SQL = "SELECT MB2000.NRef, MB2000.MB20_codTecAsist, MB20_codPlaca,"
'    SQL = SQL & "   MB09_desig, MB02_desig"
'    SQL = SQL & " FROM MB2000,MB2600,MB0900, MB0200"
'    SQL = SQL & " WHERE     MB2600.NRef=MB2000.NRef"
'    SQL = SQL & "       AND MB2600.MB20_codTecAsist = MB2000.MB20_codTecAsist"
'    SQL = SQL & "       AND MB2600.MB35_codEstSegui = " & constSEGUITECVALIDA
'    SQL = SQL & "       AND MB0900.MB09_codTec = MB2000.MB09_codTec"
'    SQL = SQL & "       AND MB0200.MB02_codCond = MB2600.MB02_codCond"
'    SQL = SQL & "       AND MB2000.MB34_codEstTecAsist = ?"
'    SQL = SQL & "       AND MB2000.MB20_fecLect < TO_DATE(?,'DD/MM/YYYY')"
'    SQL = SQL & "       AND MB2000.MB20_tiOrig BETWEEN ? AND ?"
'    SQL = SQL & "       AND MB2000.NRef IN"
'    SQL = SQL & "           (SELECT nRef FROM pruebaAsistencia"
'    SQL = SQL & "            WHERE cCarpeta = ?"
'    SQL = SQL & "            AND estado = ?"
'    SQL = SQL & "           )"
'    SQL = SQL & "       AND MB2000.MB20_CodPlaca > 0"
'    SQL = SQL & " ORDER BY MB2000.nRef, MB2000.MB20_codTecAsist"
'
'    Set qryTec = objApp.rdoConnect.CreateQuery("", SQL)
'    qryTec(0) = constTECREALIZADA
'    qryTec(1) = Format$(fLect, "dd/mm/yyyy")
'    qryTec(2) = origen1
'    qryTec(3) = origen2
'    qryTec(4) = cCarpeta
'    qryTec(5) = constPRUEBAREALIZANDO
'    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
' *************************************************************************************************
        
    SQL = "SELECT MB2000.NRef, MB2000.MB20_codTecAsist, MB20_codPlaca,"
    SQL = SQL & "   MB09_desig, MB02_desig"
    SQL = SQL & " FROM MB2000,MB2600,MB0900, MB0200"
    SQL = SQL & " WHERE     MB2600.NRef=MB2000.NRef"
    SQL = SQL & "       AND MB2600.MB20_codTecAsist = MB2000.MB20_codTecAsist"
    SQL = SQL & "       AND MB2600.MB35_codEstSegui = " & constSEGUITECVALIDA
    SQL = SQL & "       AND MB0900.MB09_codTec = MB2000.MB09_codTec"
    SQL = SQL & "       AND MB0200.MB02_codCond = MB2600.MB02_codCond"
    SQL = SQL & "       AND MB2000.MB34_codEstTecAsist = ?"
    SQL = SQL & "       AND MB2000.MB20_fecLect < TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & "       AND MB2000.NRef IN"
    SQL = SQL & "           (SELECT nRef FROM pruebaAsistencia"
    SQL = SQL & "            WHERE cCarpeta = ?"
'    SQL = SQL & "            AND estado = ?"
    SQL = SQL & "            AND estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & "           )"
    SQL = SQL & "       AND MB2000.MB20_CodPlaca > 0"
    SQL = SQL & " ORDER BY MB2000.nRef, MB2000.MB20_codTecAsist"

    Set qryTec = objApp.rdoConnect.CreateQuery("", SQL)
    qryTec(0) = constTECREALIZADA
    qryTec(1) = Format$(fLect, "dd/mm/yyyy")
    qryTec(2) = cCarpeta
    'qryTec(3) = constPRUEBAREALIZANDO
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While Not rsTec.EOF
       On Error Resume Next
       dimension = UBound(arTecPend)
       dimension = dimension + 1
       ReDim Preserve arTecPend(1 To dimension)
       arTecPend(dimension).nRef = rsTec!nRef
       arTecPend(dimension).codTecAsist = rsTec!MB20_CODTECASIST
       arTecPend(dimension).codPlaca = rsTec!MB20_codPlaca
       arTecPend(dimension).desigTecnica = rsTec!MB09_Desig
       arTecPend(dimension).desigCondicion = rsTec!MB02_desig
       On Error GoTo 0
       rsTec.MoveNext
    Loop
    rsTec.Close
    qryTec.Close
End Sub
