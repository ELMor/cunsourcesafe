Attribute VB_Name = "modTvwPruebas"
Option Explicit
Public Sub pAntibioticoGenero(tvw As TreeView, numRef$, cCol%, cTec%, cTecAsist%)
'si la t�cnica es un antibiograma manual, se insertan los grupos de antibi�ticos indicados _
en la tabla MB3900 Antibiotico-G�nero
    Dim SQL As String
    Dim rsAnti As rdoResultset, qryAnti As rdoQuery
    Dim qryAntibiograma As rdoQuery
    Dim arAntibioticos() As typeAntibioticos
    Dim dimension%, i%
    
    SQL = "SELECT MB0700.MB07_codAnti, MB07_desig, MIN(MB1700.MB31_codGrAnti)"
    SQL = SQL & " FROM MB0700, MB1700, MB3900, MB2700, MB0900"
    SQL = SQL & " WHERE MB0900.MB09_codTec = ?"
    SQL = SQL & " AND MB0900.MB04_codTiTec = " & constANTIBIOGRAMA
    SQL = SQL & " AND MB0900.cAutoanalizador IS NULL"
    SQL = SQL & " AND MB0900.MB09_indDefecto = -1" 'aplicar antibi�ticos por defecto
    SQL = SQL & " AND MB2700.nRef = ?"
    SQL = SQL & " AND MB2700.MB27_codCol = ?"
    SQL = SQL & " AND MB2700.MB11_codGeMicro IS NOT NULL"
    SQL = SQL & " AND MB3900.MB11_codGeMicro = MB2700.MB11_codGeMicro"
    SQL = SQL & " AND MB1700.MB31_codGrAnti = MB3900.MB31_codGrAnti"
    SQL = SQL & " AND MB0700.MB07_codAnti = MB1700.MB07_codAnti"
    SQL = SQL & " GROUP BY MB0700.MB07_codAnti, MB07_desig"
    Set qryAnti = objApp.rdoConnect.CreateQuery("Anti", SQL)
        
    qryAnti(0) = cTec
    qryAnti(1) = numRef
    qryAnti(2) = cCol
    Set rsAnti = qryAnti.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    On Error Resume Next
    Err = 0
    dimension = 0
    SQL = "INSERT INTO MB2900 (nRef,MB20_codTecAsist,MB27_codCol,MB07_codAnti,MB31_codGrAnti)"
    SQL = SQL & " VALUES(?,?,?,?,?)"
    Set qryAntibiograma = objApp.rdoConnect.CreateQuery("Antibiograma", SQL)
        
    objApp.rdoConnect.BeginTrans
    While rsAnti.EOF = False
        'se insertan los antibi�ticos en el antibiograma
        qryAntibiograma(0) = numRef
        qryAntibiograma(1) = cTecAsist
        qryAntibiograma(2) = cCol
        qryAntibiograma(3) = rsAnti(0)
        qryAntibiograma(4) = rsAnti(2)
        qryAntibiograma.Execute
        
        dimension = dimension + 1
        ReDim Preserve arAntibioticos(1 To dimension)
        arAntibioticos(dimension).cAnti = rsAnti(0)
        arAntibioticos(dimension).desig = rsAnti(1)
        
        rsAnti.MoveNext
    Wend
    qryAntibiograma.Close
    
    If Err = 0 Then
        objApp.rdoConnect.CommitTrans
        'se a�aden los nodos de antibi�ticos
            For i = 1 To dimension
                Call pAddNodoAntibiotico(tvw, arAntibioticos(i).cAnti, arAntibioticos(i).desig, "", "", -1, cTecAsist)
            Next i
        Else
        objApp.rdoConnect.RollbackTrans
        MsgBox "Se ha producido un error al generar el antibiograma.", vbError
    End If
    rsAnti.Close
    qryAnti.Close
    
End Sub

Public Sub pTVMuestrasMuestras(tvwMuestras As TreeView, nReferencia$)
'*********************************************************************************************
'*  Clave de las muestras: "M" & cMuestra                                                    *
'*  Tag de las muestras: cTipoMuestra                                                        *
'*  Clave de las t�cnicas: "T" & CodTecAsist & "I" & CodTiTec                                *
'*  Clave de los cultivos: "C" & CodTecAsist & "L" & CodPlaca & "S" & CodRecip               *
'*  Tag de las t�cnicas y cultivos: estado & "/" & desig                                     *
'*  Clave de los resultados de t�cnicas: "R" & CodResult & "T" & CodTecAsist & "N" & CodTec  *
'*  Tag de los resultados de t�cnicas: estado & "/" & desig                                  *
'*  Clave de las colonias: "K" & CodCol & "C" & CodTecAsist                                  *
'*  Tag de las colonias: estado                                                              *
'*  Clave de los antibi�ticos: "A" & CodAnti & "C" & CodTecAsist(de la t�cnica antibiograma) *
'*  Tag de los antibi�ticos: "/" desig                                                       *
'*  Clave del texto de las morfolog�as(microorganismos): "X" & cMuestra                      *
'*  Clave de las morfolog�as(microorganismos): "F" & CodMorf & "X" & cMuestra                *
'*  Tag de las morfolog�as(microorganismos): numLoc & "/" & desig                            *
'*********************************************************************************************
    Dim SQL As String, i%
    Dim nodo As Node
    Dim rsMuestras As rdoResultset, qryMuestra As rdoQuery
    Dim TextNodo As String
    
    'Se limpia el tvw
    tvwMuestras.Nodes.Clear
    tvwMuestras.Tag = "LlenarTreeView"
    
    'se buscan las muestras que hay que cargar en el treeview
    SQL = "SELECT MB2000.cMuestra, tm.Designacion, ma.FechaExtraccion, ma.cTipoMuestra" ', ma.HoraExtraccion"
    SQL = SQL & " FROM MB2000 ,MuestraAsistencia ma, TiposMuestras tm"
    SQL = SQL & " WHERE NRef = ?"
    SQL = SQL & " AND MB2000.cMuestra = ma.cMuestra"
    SQL = SQL & " AND tm.cTipoMuestra = ma.cTipoMuestra"
    SQL = SQL & " GROUP BY MB2000.cMuestra, tm.Designacion, ma.FechaExtraccion, ma.cTipoMuestra"
    Set qryMuestra = objApp.rdoConnect.CreateQuery("Muestras", SQL)
    
    qryMuestra(0) = nReferencia
    Set rsMuestras = qryMuestra.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsMuestras.EOF = False
        TextNodo = rsMuestras(0) & Space(5) & rsMuestras(1) & Space(5) & rsMuestras(2)
        'nodos de las muestras
        Set nodo = tvwMuestras.Nodes.Add(, , "M" & rsMuestras(0), TextNodo, constIconMuestra)
        nodo.Tag = rsMuestras(3)
        
        'se buscan las morfolog�as que hay que cargar en el treeview
        Call pTVMuestrasMorfologias(tvwMuestras, nReferencia, rsMuestras(0))
        
        'se buscan las t�cnicas/cultivos que hay que cargar en el treeview
        Call pTVMuestrasTecnicas(tvwMuestras, nReferencia, rsMuestras(0))
        
        rsMuestras.MoveNext
    Loop
    

    'se buscan los antibi�ticos que hay que cargar en el treeview
    Call pTVMuestrasAntibioticos(tvwMuestras, nReferencia, -1, -1)
    
    rsMuestras.Close
    qryMuestra.Close

    tvwMuestras.Tag = "LlenarTreeView" 'para que no se ejecute nada en el evento Expand
    For i = 1 To tvwMuestras.Nodes.Count
        If Left$(tvwMuestras.Nodes(i).Key, 1) = "M" Or Left$(tvwMuestras.Nodes(i).Key, 1) = "X" Then
            tvwMuestras.Nodes(i).Expanded = True
        End If
    Next i
    tvwMuestras.Tag = ""
        
End Sub
Public Sub pTVMuestrasAntibioticos(tvwMuestras As TreeView, nReferencia$, codAnti%, codTecAsist%)
    
    Dim SQL As String
    Dim rsAnti As rdoResultset, qryAnti As rdoQuery
    Dim nAnti%, nTecAsist%, i%
    Dim reac$, concent$
    
    'se busca el/los antibi�ticos que hay que cargar en el treeview
    i = 1
    SQL = "SELECT MB2900.MB07_codAnti, MB07_desig, MB40_desig, MB29_conc, MB20_codTecAsist,"
    SQL = SQL & " MB29_recom"
    SQL = SQL & " FROM MB2900,MB0700,MB4000"
    SQL = SQL & " WHERE nRef = ?"
    If codAnti <> -1 Then
        SQL = SQL & " AND MB2900.MB07_codAnti = ?"
        nAnti = i
        i = i + 1
    End If
    If codTecAsist <> -1 Then
        SQL = SQL & " AND MB20_codTecAsist = ?"
        nTecAsist = i
    End If
    SQL = SQL & " AND MB0700.MB07_codAnti = MB2900.MB07_codAnti"
    SQL = SQL & " AND MB4000.MB40_codReac (+)= MB2900.MB40_codReac"
    SQL = SQL & " ORDER BY MB07_desig"
    Set qryAnti = objApp.rdoConnect.CreateQuery("", SQL)
    
    qryAnti(0) = nReferencia
    If codAnti > -1 Then
        qryAnti(nAnti) = codAnti
    End If
    If codTecAsist > -1 Then
        qryAnti(nTecAsist) = codTecAsist
    End If
    Set rsAnti = qryAnti.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsAnti.EOF = False
        If rsAnti(2) <> "" Then reac = rsAnti(2) Else reac = ""
        If rsAnti(3) <> "" Then concent = rsAnti(3) Else concent = ""
        'se a�aden los nodos
        Call pAddNodoAntibiotico(tvwMuestras, rsAnti(0), rsAnti(1), reac, concent, rsAnti(5), rsAnti(4))
        rsAnti.MoveNext
    Loop
    
    rsAnti.Close
    qryAnti.Close
    
End Sub

Public Sub pAddNodoAntibiotico(tvw As TreeView, cAnti%, desig$, reac$, concent$, recom%, cTecAsist%)
'a�ade un nodo colonia-placa

'*************************************************************************
'*  Clave: "A" & CodAnti & "C" & CodTecAsist(de la t�cnica antibiograma) *
'*  Tag: "/" desig                                                       *
'*************************************************************************
    Dim textoNodo$, relacNodo$, KeyNodo$
    Dim nodo As Node
    
    textoNodo = desig & ": "
    If reac <> "" Then
        textoNodo = textoNodo & reac
    End If
    If concent <> "" Then
        textoNodo = textoNodo & "    Conc.: " & concent
    End If
    If recom = 0 Then
        textoNodo = textoNodo & "    Infor.: NO"
    Else
        textoNodo = textoNodo & "    Infor.: SI"
    End If
    relacNodo = "T" & cTecAsist & "I" & constANTIBIOGRAMA
    KeyNodo = "A" & cAnti & "C" & cTecAsist
    
    Set nodo = tvw.Nodes.Add(relacNodo, tvwChild, KeyNodo, textoNodo, constIconAntibiotico)
    nodo.Tag = "/" & desig
    
    'Se desplega el nodo sobre el que se a�ade para ver lo a�adido
    If tvw.Tag = "" Then
        tvw.Tag = "A�adirNodo" 'para que no se ejecute nada en el evento Expand
        tvw.Nodes(relacNodo).Expanded = True
        tvw.Tag = ""
    End If
End Sub


Public Sub pTVMuestrasTecnicas(tvwMuestras As TreeView, nReferencia$, cMuestra$)
    
    Dim SQL As String
    Dim rsTec As rdoResultset, qryTec As rdoQuery
    Dim estadoTexto$
    Dim fecReali$, relacNodo$, relacion%
    Dim intLeerHoy As Integer
    Dim strFechaActual As String
    
    'fecha actual
    strFechaActual = fFechaActual
    
    'se buscan las t�cnicas/cultivos que hay que cargar en el treeview
    SQL = "SELECT MB0900.MB09_codTec, MB09_desig, MB04_codTiTec, MB20_codTecAsist,"
    SQL = SQL & " MB20_tiOrig, MB20_codOrig, MB20_fecReali, MB20_codPlaca,"
    SQL = SQL & " MB20_codTecAsist_Orig, MB0900.MB03_codRecip, MB03_desig,"
    SQL = SQL & " MB34_codEstTecAsist, MB20_fecLect"
    SQL = SQL & " FROM MB2000,MB0900,MB0300"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND cMuestra = ?"
    SQL = SQL & " AND MB2000.MB09_codTec = MB0900.MB09_codTec"
    SQL = SQL & " AND MB0300.MB03_codRecip = MB0900.MB03_codRecip"
    SQL = SQL & " ORDER BY MB20_codTecAsist"
    Set qryTec = objApp.rdoConnect.CreateQuery("Tecnicas", SQL)
    
    qryTec(0) = nReferencia
    qryTec(1) = cMuestra
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
    Do While rsTec.EOF = False
        If Not IsNull(rsTec!MB20_fecReali) Then fecReali = rsTec!MB20_fecReali Else fecReali = ""
        intLeerHoy = 0
        If rsTec!MB34_codEstTecAsist = constTECREALIZADA Then
            If Not IsNull(rsTec!MB20_FecLect) Then
                If CDate(Format(rsTec!MB20_FecLect, "dd/mm/yyyy")) <= CDate(Format(strFechaActual, "dd/mm/yyyy")) Then
                    intLeerHoy = 1
                End If
            End If
        End If
        
        'se a�aden los nodos
        Select Case rsTec!MB04_CODTITEC
            Case constCULTIVO 'CULTIVOS(PLACAS)
                Call pAddNodoCultivos(tvwMuestras, rsTec!MB20_CODTECASIST, rsTec!MB09_Desig, rsTec!MB03_desig, rsTec!MB20_CODPlaca, cMuestra, rsTec!MB03_codRecip, fecReali, rsTec!MB34_codEstTecAsist, rsTec!MB20_tiOrig, rsTec!MB20_codOrig, intLeerHoy)
                'se buscan las colonias que hay que cargar en el treeview
                Call pTVMuestrasColonias(tvwMuestras, nReferencia, rsTec!MB20_CODTECASIST, rsTec!MB20_CODPlaca, -1)
            Case Else 'T�CNICAS
                Select Case rsTec!MB20_tiOrig
                Case constORIGENMUESTRA
                    relacNodo = "X" & rsTec!MB20_codOrig
                    relacion = tvwPrevious
                Case constORIGENCOLONIA
                    relacNodo = "K" & rsTec!MB20_codOrig & "C" & rsTec!MB20_CODTecAsist_Orig
                    relacion = tvwChild
                End Select
                Call pAddNodoTecnica(tvwMuestras, rsTec!MB20_CODTECASIST, rsTec!MB04_CODTITEC, rsTec!MB09_Desig, fecReali, rsTec!MB34_codEstTecAsist, relacNodo$, relacion%)
                'se buscan los resultados que hay que cargar en el treeview
                Call pTVMuestrasResultTecnicas(tvwMuestras, nReferencia, rsTec!MB09_codTec, rsTec!MB20_CODTECASIST, rsTec!MB04_CODTITEC)
        End Select
        
        rsTec.MoveNext
    Loop
    
    rsTec.Close
    qryTec.Close
    
End Sub
Public Sub pAddNodoNuevasTecnicas(tvwMuestras As TreeView, nReferencia$, arTecProt() As typeTecProt, claveNodoEn$, cMuestra$)
'esta funci�n es necesaria ya que no se conocen los par�metros necesarios para llamar a las _
funciones pAddNodoTecnica/pAddNodoCultivo al generarse las t�cnicas en otra funci�n que no _
aporta estos par�metros

    Dim SQL$, i%
    Dim rsTec As rdoResultset, qryTec As rdoQuery
    Dim relacNodo$, relacion%
    Dim dimension%
    
    SQL = "SELECT MB09_desig, MB0900.MB03_codRecip, MB03_desig, MB04_codTiTec"
    SQL = SQL & " FROM MB0900, MB0300"
    SQL = SQL & " WHERE MB09_codTec = ?"
    SQL = SQL & " AND MB0300.MB03_CodRecip = MB0900.MB03_codRecip"
    Set qryTec = objApp.rdoConnect.CreateQuery("Tec", SQL)
    
    On Error Resume Next
    dimension = UBound(arTecProt)
    On Error GoTo 0
    
    For i = 1 To dimension
        qryTec(0) = arTecProt(i).codTec
        Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)

        Select Case arTecProt(i).codPlaca
            Case 0 'T�CNICAS
                Select Case Left$(claveNodoEn, 1)
                    Case "M" 'sobre muestra
                        relacNodo = "X" & cMuestra
                        relacion = tvwPrevious
                    Case "K" 'sobre colonia
                        relacNodo = claveNodoEn
                        relacion = tvwChild
                End Select
                Call pAddNodoTecnica(tvwMuestras, arTecProt(i).codTecAsist, rsTec(3), rsTec(0), "", constTECSOLICITADA, relacNodo, relacion)
                'si la t�cnica es de antibiograma manual y la colonia tiene identificado _
                el g�nero, se a�ade el grupo o grupos de antibi�ticos se�alados en MB3900
                Call pAntibioticoGenero(tvwMuestras, nReferencia, Val(Mid$(claveNodoEn, 2)), arTecProt(i).codTec, arTecProt(i).codTecAsist)
                
                'se buscan los resultados que hay que cargar en el treeview
                Call pTVMuestrasResultTecnicas(tvwMuestras, nReferencia, arTecProt(i).codTec, arTecProt(i).codTecAsist, rsTec(3))
            Case Else 'CULTIVOS(PLACAS)
                Call pAddNodoCultivos(tvwMuestras, arTecProt(i).codTecAsist, rsTec(0), rsTec(2), arTecProt(i).codPlaca, cMuestra, rsTec(1), "", constTECSOLICITADA, arTecProt(i).tiOrig, arTecProt(i).codOrig)
        End Select
        
        rsTec.Close
    Next i
    
    qryTec.Close
    
End Sub
Public Sub pAddNodoColonia(tvw As TreeView, cCol%, identif$, estado%)
'a�ade un nodo colonia
    
'**************************
'*  Clave: "K" & CodCol   *
'*  Tag: estado           *
'**************************
    Dim textoNodo$, relacNodo$, KeyNodo$
    Dim nodo As Node
    
    textoNodo = "Colonia " & cCol
    If identif <> "" Then
        textoNodo = textoNodo & ": " & identif
'    Else
'        textoNodo = textoNodo & ": Sin identificar"
    End If
    'textoNodo = textoNodo & Space(5) & fEstadoColonia(estado)
    textoNodo = textoNodo & Space(5) & fEstadoNodo(constESTADOCOL, estado)
    KeyNodo = "K" & cCol
    
    Set nodo = tvw.Nodes.Add(, , KeyNodo, textoNodo, constIconColonia)
    nodo.Tag = estado
    
End Sub


Public Sub pTVMuestrasResultTecnicas(tvwMuestras As TreeView, nReferencia$, codTec%, codTecAsist%, codTiTec%)
    Dim SQL As String
    Dim rsResult As rdoResultset, qryResult As rdoQuery
    Dim result$, unidad$, cResultado%
        
    'NOTA: al pedir t�cnicas, se a�aden los registros de resultados a la tabla MB3300, por _
    lo que en esta tabla estar�n todos los resultados definidos para la t�cnica
    SQL = "SELECT MB3300.MB32_codResult, MB32_desig, MB33_result, MB38_codEstResult,"
    SQL = SQL & " designacion, MB3200.cUnidad, cResultado"
    SQL = SQL & " FROM MB3300, MB3200, Unidades u"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB3300.MB09_codTec = ?"
    SQL = SQL & " AND MB3200.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB3200.MB32_codResult = MB3300.MB32_codResult"
    'SQL = SQL & " AND MB32_indActiva = -1"
    SQL = SQL & " AND u.cUnidad = MB3300.cUnidad"
    SQL = SQL & " ORDER BY MB32_desig"
    Set qryResult = objApp.rdoConnect.CreateQuery("Resultados", SQL)
    
    qryResult(0) = nReferencia
    qryResult(1) = codTecAsist
    qryResult(2) = codTec
    Set rsResult = qryResult.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    Do While rsResult.EOF = False
        If Not IsNull(rsResult(2)) Then
            result = rsResult(2)
            unidad = rsResult(4)
        Else
            result = ""
            unidad = ""
        End If
        If Not IsNull(rsResult(6)) Then
            cResultado = rsResult(6)
        Else
            cResultado = 0
        End If
        'se a�aden los nodos
        Call pAddNodoResultTec(tvwMuestras, rsResult(0), rsResult(1), result, codTecAsist, codTiTec, codTec, rsResult(3), unidad, rsResult(5), cResultado)
        rsResult.MoveNext
    Loop
    
    rsResult.Close
    qryResult.Close
    
End Sub

Public Sub pTVResumenAntibioticos(tvwMuestras As TreeView, nReferencia$, cCol As Integer)

    
    Dim nodo As Node, SQL As String
    Dim cAnti As Integer 'Chequea si hay dos antibi�ticos id�nticos (p.e. en distintas placas)
    Dim rsAntibioticos As rdoResultset, qryAntibiotico As rdoQuery
    Dim TextNodo As String
    
    SQL = "SELECT DISTINCT MB07_desig, MB40_desig, MB2900.MB07_codAnti"
    SQL = SQL & " FROM MB0700, MB4000, MB2900"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB0700.MB07_codAnti = MB2900.MB07_codAnti"
    SQL = SQL & " AND MB4000.MB40_codReac = MB2900.MB40_codReac"
    SQL = SQL & " ORDER BY MB07_desig, MB2900.MB07_codAnti"
    Set qryAntibiotico = objApp.rdoConnect.CreateQuery("Antibiotico", SQL)
    
    qryAntibiotico(0) = nReferencia
    qryAntibiotico(1) = cCol
    
    Set rsAntibioticos = qryAntibiotico.OpenResultset(rdOpenForwardOnly)
    Do While rsAntibioticos.EOF = False
        If cAnti <> rsAntibioticos(2) Then
            TextNodo = rsAntibioticos(0) & ": " & rsAntibioticos(1)
            Set nodo = tvwMuestras.Nodes.Add("K" & cCol, tvwChild, , TextNodo, constIconAntibiotico)
            cAnti = rsAntibioticos(2)
        End If
        rsAntibioticos.MoveNext
    Loop
    
    rsAntibioticos.Close
    qryAntibiotico.Close
    
End Sub

Public Sub pTVResumenColonias(tvwMuestras As TreeView, nReferencia$)
'*************************************
' Clave de las colonias:"K" & CodCol *
'*************************************
    
    Dim nodo As Node, SQL As String
    Dim rsColonias As rdoResultset, qryColonia As rdoQuery
    Dim desigColonia As String
    Dim TextNodo As String
    
    'rdoQuery de las Colonias no anuladas (consulta)
    SQL = "SELECT MB2700.MB27_codCol, nvl(MB18_desig,' '), nvl(MB11_desig,' '), "
    SQL = SQL & " nvl(MB12_desig,' '), MB3600.MB36_desig"
    SQL = SQL & " FROM MB2700, MB3600, MB1800, MB1100, MB1200"
    SQL = SQL & " WHERE nRef = ? "
    SQL = SQL & " AND MB2700.MB36_codEstCol <> " & constCOLANULADA
    SQL = SQL & " AND MB2700.MB36_codEstCol = MB3600.MB36_codEstCol"
    SQL = SQL & " AND MB1800.MB18_codMicro (+)= MB2700.MB18_codMicro"
    SQL = SQL & " AND MB1100.MB11_codGeMicro (+)= MB2700.MB11_codGeMicro"
    SQL = SQL & " AND MB1200.MB12_codMorf (+)= MB2700.MB12_codMorf"
    SQL = SQL & " ORDER BY MB27_codCol"
    Set qryColonia = objApp.rdoConnect.CreateQuery("Colonia", SQL)
    
    qryColonia(0) = nReferencia
    Set rsColonias = qryColonia.OpenResultset(rdOpenForwardOnly)
    
    'Si hay colonia, se pone el nodo
    If rsColonias.EOF = False Then
        Do While rsColonias.EOF = False
            desigColonia = fIdentifCol(Trim$(rsColonias(1)), Trim$(rsColonias(2)), Trim$(rsColonias(3)))
            If desigColonia = "" Then
                desigColonia = "Sin identificar"
            End If
            TextNodo = "Colonia " & rsColonias(0) & ": " & desigColonia & Space(5)
            TextNodo = TextNodo & "(" & rsColonias(4) & ")"
            Set nodo = tvwMuestras.Nodes.Add(, , "K" & rsColonias(0), TextNodo, constIconColonia)
            
            'Se ponen los nodos de los antibi�ticos
            Call pTVResumenAntibioticos(tvwMuestras, nReferencia, rsColonias(0))
            
            rsColonias.MoveNext
        Loop
    'Si no hay colonias, se avisa de este hecho
    Else
        TextNodo = "No se han detectado colonias"
        Set nodo = tvwMuestras.Nodes.Add(, , "vacioCol", TextNodo, constIconColonia)
    End If
    
    rsColonias.Close
    qryColonia.Close
    
End Sub

Public Sub pTVResumenTecnicas(tvwMuestras As TreeView, nReferencia$)
'se a�aden los nodos de las t�cnicas y los resultados NO ANULADOS

'*****************************************************************************
' Clave de las t�cnicas:"T" & codTecAsist                                    *
' Clave de los resultados:"R" & codresult & "T" & codTecAsist & "N" & codTec *
'*****************************************************************************
    
    Dim nodo As Node
    Dim SQL As String
    Dim rsTecnicas As rdoResultset, qryTecnica As rdoQuery
    Dim TextNodo, KeyNodo As String
    Dim cTecAsist As Integer
    
    'Se limpia el treeview
    tvwMuestras.Nodes.Clear
 
    SQL = "SELECT MB0900.MB09_Desig, MB2000.MB20_FecReali, MB3400.MB34_desig"
    SQL = SQL & ", MB2000.MB20_codTecAsist, MB0900.MB09_codTec, MB3300.MB32_codResult"
    SQL = SQL & " ,MB3300.MB33_result, MB3200.MB32_desig, MB3800.MB38_desig, u.Designacion"
    SQL = SQL & " ,MB3300.MB38_codEstResult"
    SQL = SQL & " FROM MB2000, MB3300, MB0900, MB3200, MB3400, MB3800, Unidades u"
    SQL = SQL & " WHERE MB2000.nRef = ?"
    SQL = SQL & " AND MB2000.MB20_tiOrig = " & constORIGENMUESTRA
    SQL = SQL & " AND MB2000.MB20_codPlaca = 0"
    SQL = SQL & " AND MB2000.MB34_codEstTecAsist <> " & constTECANULADA
    SQL = SQL & " AND MB3300.nRef = MB2000.nRef"
    SQL = SQL & " AND MB3300.MB20_codTecAsist = MB2000.MB20_codTecAsist"
    SQL = SQL & " AND MB3300.MB09_codTec = MB2000.MB09_codTec"
    SQL = SQL & " AND MB3300.MB38_codEstResult <> " & constRESULTANULADO
    SQL = SQL & " AND MB3200.MB09_codTec = MB3300.MB09_codTec"
    SQL = SQL & " AND MB3200.MB32_codResult = MB3300.MB32_codResult"
    SQL = SQL & " AND MB0900.MB09_codTec = MB2000.MB09_codTec"
    SQL = SQL & " AND MB3400.MB34_codEstTecAsist = MB2000.MB34_codEstTecAsist"
    SQL = SQL & " AND MB3800.MB38_codEstResult = MB3300.MB38_codEstResult"
    SQL = SQL & " AND u.cUnidad = MB3300.cUnidad"
    SQL = SQL & " ORDER BY MB2000.MB20_codTecAsist, MB3200.MB32_desig"
    Set qryTecnica = objApp.rdoConnect.CreateQuery("Tecnica", SQL)
    
    qryTecnica(0) = nReferencia
    Set rsTecnicas = qryTecnica.OpenResultset(rdOpenForwardOnly)
    
    'Si hay alguna t�cnica, se pone el nodo
    If rsTecnicas.EOF = False Then
        cTecAsist = 0
        Do While rsTecnicas.EOF = False
            'nodo de la t�cnica
            If rsTecnicas!MB20_CODTECASIST <> cTecAsist Then
                cTecAsist = rsTecnicas!MB20_CODTECASIST 'para s�lo poner una vez el nodo de la t�cnica
                TextNodo = rsTecnicas!MB09_Desig & Space(5) & rsTecnicas!MB20_fecReali
                TextNodo = TextNodo & Space(5) & "(" & rsTecnicas!MB34_desig & ")"
                KeyNodo = "T" & rsTecnicas!MB20_CODTECASIST
                Set nodo = tvwMuestras.Nodes.Add(, , KeyNodo, TextNodo, constIconTecnica)
            End If
            'nodo del resultado
            TextNodo = rsTecnicas!MB32_desig & ": "
            If Not IsNull(rsTecnicas!MB33_result) Then
                TextNodo = TextNodo & rsTecnicas!MB33_result & Space(2)
                TextNodo = TextNodo & rsTecnicas!Designacion
            End If
            If Len(TextNodo) > constMAX_LONG_RESULT Then
                TextNodo = Left$(TextNodo, constVISIBLE_LONG_RESULT) & "..."
            End If
            If rsTecnicas!MB38_codEstResult <> constRESULTSOLICITADO Then
                TextNodo = TextNodo & Space(5) & "(" & rsTecnicas!MB38_desig & ")"
            End If
            KeyNodo = "R" & rsTecnicas!MB32_CODResult & "T" & cTecAsist
            KeyNodo = KeyNodo & "N" & rsTecnicas!MB09_codTec
            Set nodo = tvwMuestras.Nodes.Add("T" & cTecAsist, tvwChild, KeyNodo, TextNodo, constIconResultado)
            
            rsTecnicas.MoveNext
        Loop
    'Si no hay t�cnicas, se avisa
    Else
        TextNodo = "No hay resultados de t�cnicas informados"
        Set nodo = tvwMuestras.Nodes.Add(, , "vacio", TextNodo, constIconTecnica)
    End If
    
    rsTecnicas.Close
    qryTecnica.Close
    
End Sub




Public Sub pAddNodoResultTec(tvw As TreeView, cResult%, desig$, result$, cTecAsist%, cTiTec%, cTec%, estado%, unidad$, cUnidad%, cResultado%)
'a�ade un nodo resultado de t�cnica
'cResult: c�digo del resultado en Micro
'desig: designaci�n del resultado
'result: valor del resultado
'cTecAsist: c�digo de t�cnica-asistencia de la t�cnica
'cTiTec: c�digo del tipo de t�cnica
'cTec: c�digo de la t�cnica
'estado: designaci�n del estado del resultado
'unidad: desiganci�n de la unidad del resultado
'cUnidad: c�digo de la unidad del resultado
'cResultado: c�digo del resultado en Laboratorio


'************************************************************************
'*  Clave: "R" & CodResult & "T" & CodTecAsist & "N" & CodTec           *
'*  Tag: estado & "U" & cUnidad & "R" & cResultado Labor & "/" & desig  *
'************************************************************************
    Dim textoNodo$, relacNodo$, KeyNodo$
    Dim nodo As Node
    Dim resultado As String
    
    
    relacNodo = "T" & cTecAsist & "I" & cTiTec
    resultado = fFormatearRecuento(result$, cTiTec, False)
    KeyNodo = "R" & cResult & "T" & cTecAsist & "N" & cTec
    'textoNodo = Desig & ": " & result & RTrim$(Space(5) & fEstadoResultado(estado))
    textoNodo = desig & ": " & resultado & Space(2) & unidad
    If Len(textoNodo) > constMAX_LONG_RESULT Then
        textoNodo = Left$(textoNodo, constVISIBLE_LONG_RESULT) & "..."
    End If
    textoNodo = textoNodo & Space(5) & fEstadoNodo(constESTADORESULT, estado)
    
    Set nodo = tvw.Nodes.Add(relacNodo, tvwChild, KeyNodo, textoNodo, constIconResultado)
    nodo.Tag = estado & "U" & cUnidad & "R" & cResultado & "/" & desig
    
End Sub


Public Sub pTVMuestrasColonias(tvwMuestras As TreeView, nReferencia$, codTecAsist%, codPlaca%, CodCol%)
    Dim SQL As String
    Dim rsCol As rdoResultset, qryCol As rdoQuery
    Dim identif$
    
    'se buscan la/las colonias que hay que cargar en el treeview
    SQL = "SELECT MB3000.MB27_codCol, MB18_desig, MB11_desig, MB12_desig,"
    SQL = SQL & " MB03_codRecip, MB37_codEstPlaca"
    SQL = SQL & " FROM MB2700,MB3000,MB2000,MB0900,MB1100,MB1200,MB1800"
    SQL = SQL & " WHERE MB3000.MB27_codCol = MB2700.MB27_codCol"
    SQL = SQL & " AND MB3000.nRef = MB2700.nRef"
    SQL = SQL & " AND MB3000.nRef = ?"
    SQL = SQL & " AND MB3000.MB20_codTecAsist = ?"
    If CodCol > -1 Then
        SQL = SQL & " AND MB3000.MB27_codCol = ?"
    End If
    SQL = SQL & " AND MB2000.MB20_codTecAsist = MB3000.MB20_codTecAsist"
    SQL = SQL & " AND MB2000.nRef = MB3000.nRef"
    SQL = SQL & " AND MB0900.MB09_codTec = MB2000.MB09_codTec"
    SQL = SQL & " AND MB1100.MB11_codGeMicro (+)= MB2700.MB11_codGeMicro"
    SQL = SQL & " AND MB1200.MB12_codMorf (+)= MB2700.MB12_codMorf"
    SQL = SQL & " AND MB1800.MB18_codMicro (+)= MB2700.MB18_codMicro"
    SQL = SQL & " ORDER BY MB3000.MB27_codCol"
    Set qryCol = objApp.rdoConnect.CreateQuery("", SQL)
    
    qryCol(0) = nReferencia
    qryCol(1) = codTecAsist
    If CodCol > -1 Then
        qryCol(2) = CodCol
    End If
    Set rsCol = qryCol.OpenResultset(rdOpenForwardOnly)
    
    Do While rsCol.EOF = False
        If Not IsNull(rsCol(1)) Then
            identif = rsCol(1)
        ElseIf Not IsNull(rsCol(2)) Then
            identif = rsCol(2)
        ElseIf Not IsNull(rsCol(3)) Then
            identif = rsCol(3)
        Else
            identif = ""
        End If
        'se a�aden los nodos
        Call pAddNodoColPlaca(tvwMuestras, rsCol(0), identif, rsCol(5), codTecAsist, codPlaca, rsCol(4))
        
        rsCol.MoveNext
    Loop
    
    rsCol.Close
    qryCol.Close
    
End Sub



Public Sub pAddNodoColPlaca(tvw As TreeView, cCol%, identif$, estado%, cTecAsist%, cPlaca%, cRecip%)
'a�ade un nodo colonia-placa

'*********************************************
'*  Clave: "K" & CodCol & "C" & CodTecAsist  *
'*  Tag: estado                              *
'*********************************************
    Dim textoNodo$, relacNodo$, KeyNodo$
    Dim nodo As Node
        
    textoNodo = "Colonia " & cCol & ": "
    If identif <> "" Then
        textoNodo = textoNodo & identif
    Else
        textoNodo = textoNodo & "Sin identificar"
    End If
    'textoNodo = textoNodo & Space(5) & fEstadoColPlaca(estado)
    textoNodo = textoNodo & Space(5) & fEstadoNodo(constESTADOPLACA, estado)
    relacNodo = "C" & cTecAsist & "L" & cPlaca & "S" & cRecip
    KeyNodo = "K" & cCol & "C" & cTecAsist
    
    Set nodo = tvw.Nodes.Add(relacNodo, tvwChild, KeyNodo, textoNodo, constIconColonia)
    nodo.Tag = estado
    
    'Se desplega el nodo sobre el que se a�ade para ver lo a�adido
    If tvw.Tag = "" Then
        tvw.Tag = "A�adirNodo" 'para que no se ejecute nada en el evento Expand
        tvw.Nodes(relacNodo).Expanded = True
        tvw.Tag = ""
    End If
    
End Sub


Public Sub pAddNodoTecnica(tvw As TreeView, cTecAsist%, cTiTec%, desigTec$, fecReali$, estado%, relacNodo$, relacion%)
'a�ade un nodo t�cnica

'************************************************
'*  Clave: "T" & CodTecAsist & "I" & CodTiTec   *
'*  Tag: estado & "/" & desig                   *
'************************************************
    Dim textoNodo$, KeyNodo$
    Dim nodo As Node
    
    KeyNodo = "T" & cTecAsist & "I" & cTiTec
    'textoNodo = desigTec & Space(5) & fecReali & Space(5) & fEstadoTecnica(estado)
    textoNodo = desigTec & Space(5) & fecReali & Space(5) & fEstadoNodo(constESTADOTEC, estado)
    
    Set nodo = tvw.Nodes.Add(relacNodo, relacion, KeyNodo, textoNodo, constIconTecnica)
    nodo.Tag = estado & "/" & desigTec
    
    'Se desplega el nodo sobre el que se a�ade para ver lo a�adido
    If tvw.Tag = "" Then
        tvw.Tag = "A�adirNodo" 'para que no se ejecute nada en el evento Expand
        tvw.Nodes(relacNodo).Expanded = True
        tvw.Tag = ""
    End If
    
End Sub


Public Sub pAddNodoCultivos(tvw As TreeView, cTecAsist%, desigTec$, desigPlaca$, cPlaca%, cMuestra$, cRecip%, fecReali$, estado%, tiOrig%, codOrig$, Optional intLeerHoy As Integer)
'a�ade un nodo cultivo

'***************************************************************
'*  Clave: "C" & CodTecAsist & "L" & CodPlaca & "S" & CodRecip *
'*  Tag: estado & "/" & desig                                  *
'***************************************************************
    Dim textoNodo$, relacNodo$, KeyNodo$
    Dim nodo As Node
    Dim origen$
    
    Select Case tiOrig
    Case constORIGENMUESTRA
        origen = "M"
    Case constORIGENPLACA
        origen = "P " & codOrig
    Case constORIGENCOLONIA
        origen = "C " & codOrig
    End Select
        
    'textoNodo = desigPlaca & " " & cPlaca & ": " & desigTec & Space(5) & fecReali & Space(5) & fEstadoTecnica(estado)
    If intLeerHoy = 0 Then
        textoNodo = "*  "
    End If
    textoNodo = textoNodo & desigPlaca & " " & cPlaca & " (" & origen & "): " & desigTec
'    textoNodo = desigPlaca & " " & cPlaca & " (" & origen & "): " & desigTec
'    If blnLeerHoy = False Then
'        textoNodo = textoNodo & Space(3) & "(Incubando)"
'    End If
    textoNodo = textoNodo & Space(5) & fecReali & Space(5) & fEstadoNodo(constESTADOTEC, estado)
    KeyNodo = "C" & cTecAsist & "L" & cPlaca & "S" & cRecip
    relacNodo = "M" & cMuestra
    
    Set nodo = tvw.Nodes.Add(relacNodo, tvwChild, KeyNodo, textoNodo, constIconPlaca)
    nodo.Tag = estado & "/" & desigTec
    
End Sub

Public Sub pTVMuestrasMorfologias(tvwMuestras As TreeView, nReferencia$, cMuestra$)
    Dim rsMorfo As rdoResultset, qryMorfo As rdoQuery
    Dim SQL As String
    Dim nodo As Node
    
    'se buscan las morfolog�as que hay que cargar en el treeview
    SQL = "SELECT MB2100.MB12_codMorf, MB21_numLoc, MB12_desig"
    SQL = SQL & " FROM MB2100,MB1200"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND cMuestra = ?"
    SQL = SQL & " AND MB1200.MB12_codMorf = MB2100.MB12_codMorf"
    Set qryMorfo = objApp.rdoConnect.CreateQuery("Morfologias", SQL)
    
    qryMorfo(0) = nReferencia
    qryMorfo(1) = cMuestra
    Set rsMorfo = qryMorfo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    'nodo de texto de 'microorganismos'
    Set nodo = tvwMuestras.Nodes.Add("M" & cMuestra, tvwChild, "X" & cMuestra, "Microorganismos:", constIconMicroorganismo)
     
    Do While rsMorfo.EOF = False
        'se a�aden los nodo
        Call pAddNodoMorfologia(tvwMuestras, rsMorfo(0), rsMorfo(2), rsMorfo(1), cMuestra)
        rsMorfo.MoveNext
    Loop
    
    rsMorfo.Close
    qryMorfo.Close
    
End Sub
Public Sub pAddNodoMorfologia(tvw As TreeView, cMorf%, desig$, numLoc%, cMuestra$)
'a�ade un nodo morfolog�a (microorganismo)

'********************************************
'*  Clave: "F" & CodMorf & "X" & cMuestra   *
'*  Tag: numLoc & "/" & desig               *
'********************************************
    Dim textoNodo$, relacNodo$, KeyNodo$
    Dim nodo As Node
    
    textoNodo = desig & "   (" & numLoc & " localiz.)"
    KeyNodo = "F" & cMorf & "X" & cMuestra
    relacNodo = "X" & cMuestra
    
    Set nodo = tvw.Nodes.Add(relacNodo, tvwChild, KeyNodo, textoNodo, constIconMorfologia)
    nodo.Tag = numLoc & "/" & desig
    
    'Se desplega el nodo sobre el que se a�ade para ver lo a�adido
    If tvw.Tag = "" Then
        tvw.Tag = "A�adirNodo" 'para que no se ejecute nada en el evento Expand
        tvw.Nodes(relacNodo).Expanded = True
        tvw.Tag = ""
    End If
    
End Sub


