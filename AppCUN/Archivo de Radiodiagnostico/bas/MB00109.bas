Attribute VB_Name = "publicConstants_P"
Option Explicit

'Titulaciones de los usuarios
Public Const constDOCTOR = 1

'Tipos de recipientes
Public Const constRECIPPLACA = 1

' Constantes de tipo de t�cnica
Public Const constCULTIVO = 1       ' T�cnicas correspondientes a cultivos
Public Const constIDENTIFICACION = 2       ' T�cnicas correspondientes a identificaci�n
Public Const constOBSERVDIRECTA = 3       ' T�cnicas correspondientes a observaci�n directa
Public Const constANTIBIOGRAMA = 4       ' T�cnicas correspondientes a antibiogramas
Public Const constRECUENTO = 5       ' T�cnicas correspondientes a recuentos
Public Const constRESULTADOS = 6       ' T�cnicas correspondientes a observaciones que se informan
Public Const constBIOQUIMICAS = 7       ' T�cnicas correspondientes a t�cnicas bioqu�micas

' Constantes de por qu� se aplica un protocolo
Public Const constAplicProtP = 1    ' Por prueba
Public Const constAplicProtM = 2    ' Por muestra
Public Const constAplicProtDpt = 4  ' Por solicitante

' Or�genes de las t�cnicas
Public Const constORIGENMUESTRA = 1
Public Const constORIGENPLACA = 2
Public Const constORIGENTECNICA = 3
Public Const constORIGENCOLONIA = 4

' Estado de las t�cnicas
Public Const constTECSOLICITADA = 1
Public Const constTECSOLICITAREALI = 2
Public Const constTECREALIZANDOSE = 3
Public Const constTECREALIZADA = 4
Public Const constTECREINCUBADA = 5
Public Const constTECVALIDADA = 6
Public Const constTECFINALIZADA = 8
Public Const constTECANULADA = 9

'Estado del seguimiento de t�cnicas
Public Const constSEGUITECVALIDA = 1
Public Const constSEGUITECANTIGUA = 2

'Estados de las colonias
Public Const constCOLLOCALIZADA = 1
Public Const constCOLVALIDADA = 3
Public Const constCOLINFORMADA = 5
Public Const constCOLANULADA = 9

'Estados de las colonias-palcas
Public Const constCOLPLACAASOCIADA = 1
Public Const constCOLPLACAANULADA = 9

'Estados de los resultados de las t�cnicas
Public Const constRESULTSOLICITADO = 1
Public Const constRESULTVALIDADO = 3
Public Const constRESULTINFORMADO = 5
Public Const constRESULTANULADO = 9

' Estados de las columnas de la mesa en el Montaje de la mesa
Public Const constColVacia = 1
Public Const constColIncompleta = 2
Public Const constColFalta = 3
Public Const constColColocado = 4

' Tipos de informes
Public Const constINFProblemas = 1
Public Const constINFColonias = 2
Public Const constINFMuestras = 3
Public Const constINFAislamientos = 4
Public Const constINFAntibiogramas = 5
Public Const constINFTecnicas = 6
Public Const constINFBionumeros = 7
Public Const constINFCrecimientos = 8
Public Const constINFEdades = 9
Public Const constINFDesglose = 10

' Constantes para la impresi�n de informes
Public Const constNAntibInforme = 40 ' N� m�ximo de antibi�ticos por elecci�n
Public Const constNColInforme = 5    ' N� m�ximo de colonias por elecci�n y antibi�tico

'Tablas de estados
Public Const constESTADOTEC = "TEC"
Public Const constESTADOCOL = "COL"
Public Const constESTADOPLACA = "PLACA"
Public Const constESTADORESULT = "RESULT"

'ToolTipText del bot�n Maximizar/Restaurar TreeView
Public Const constMAXIMIZAR_VENTANA = "Maximizar ventana"
Public Const constRESTAURAR_VENTANA = "Restaurar ventana"

'N� m�ximo de recuento de colonias
Public Const constMAXRECUENTO = 100000

'Longitud del resultado de las t�cnicas visible en el TreeView
Public Const constMAX_LONG_RESULT = 75
Public Const constVISIBLE_LONG_RESULT = 50

'Bot�n de mantenimiento de la barra de estado
Public Const constcwImageForeign = "i35"
Public Const constcwPosForeign = 2

'Acceso a mantenimientos
Public Const constMANTNINGUNO = 0
Public Const constMANTMICROORGANISMOS = 1
Public Const constMANTGENEROS = 2
Public Const constMANTMORFOLOGIAS = 3
Public Const constMANTPROTOCOLOS = 4
Public Const constMANTTECNICAS = 5
Public Const constMANTANTIBIOTICOS = 6
Public Const constMANTAUTOTEXTOS = 7
Public Const constMANTREFSENSIBILIDADES = 8

'constantes del estado del informe
Public Const constINFORMEDEFINITIVO = "Informe definitivo"
Public Const constINFORMEPROVISIONAL = "Informe provisional"

' C�digo de Resultadoque divide los de tipo t�cnica y los de microorganismos, morfolog�as y g�neros.
Public Const constcResultadoTecnica = 100

' Unidad por defecto
Public Const constUnidadDefecto = 3

' Relaci�n con Laboratorio
Public Const constcDptoSeccMicro = 8

Public Const constPROCESOSOLICITUD = 1
Public Const constPROCESOREALIZACION = 5
Public Const constPROCESOVALIDACION = 9
Public Const constPROCESOANULACION = 0

Public Const constPRESENTACIONFILAS = 0
Public Const constPRESENTACIONTABULAR = 1

Public Const constPRUEBASOLICITADA = 1
Public Const constPRUEBAIMPRESA = 2
Public Const constPRUEBAEXTRAIDA = 3
Public Const constPRUEBASOLICITUDREALIZ = 4
Public Const constPRUEBAREALIZANDO = 5
Public Const constPRUEBARESULTADO = 6
Public Const constPRUEBARESULTANALIZ = 7
Public Const constPRUEBAVALIDTEC = 8
Public Const constPRUEBAVALIDADA = 9
Public Const constPRUEBAINSERTADA = 10
Public Const constPRUEBAENVIOPROV = 11
Public Const constPRUEBAENVIADA = 12
Public Const constPRUEBAMANUAL = 80
Public Const constPRUEBAREPETIDA = 90
Public Const constPRUEBAANULADA = 99

Public Const constLISTAPREALIZANDO = 1
Public Const constLISTAPRESULTANAL = 2
Public Const constLISTAPRESULTADO = 3
Public Const constLISTAPVALIDTECNI = 4
Public Const constLISTAPVALIDMEDIC = 9


Public Const constMUESTRAPENDIENTE = 1
Public Const constMUESTRAEXTRAIDA = 2
Public Const constMUESTRAFINALIZADA = 3
Public Const constMUESTRAANULADAMUESTRA = 4
Public Const constMUESTRAANULADAPRUEBA = 5
Public Const constMUESTRAGUARDADA = 6

Public Const constFUNCIONLECTURA = 1
Public Const constFUNCIONACTUALIZACION = 2
Public Const constFUNCIONINSERCION = 3
Public Const constFUNCIONELIMINACION = 4

Public Const constMUESTRAMODOEXTRACCION = 1
Public Const constMUESTRAMODOPACIENTE = 2
Public Const constMUESTRAMODOVIVO = 3

' Limites de valores de resultados
Public Const constALERTASUPERIOR = 1
Public Const constREFSUPERIOR = 2
Public Const constREFINFERIOR = 3
Public Const constALERTAINFERIOR = 4
Public Const constVARIACIONPOS = 5
Public Const constVARIACIONNEG = 6

Public Const constASISTCERRADA = 5

' Dias de la semana
Public Const constLUNES = 1
Public Const constMARTES = 2
Public Const constMIERCOLES = 3
Public Const constJUEVES = 4
Public Const constVIERNES = 5
Public Const constSABADO = 6
Public Const constDOMINGO = 7
Public Const constLV = 8        ' Lunes a Viernes
Public Const constLD = 9        ' Lunes a Domingo
Public Const constFECHA = 10    ' Fecha determinada

' Tipos de Resultados
Public Const constRESULTADONUMERICO = 1
Public Const constRESULTADOALFANUMERICO = 2
Public Const constRESULTADOMEMO = 3
Public Const constRESULTADOIMAGEN = 4

' Sexo
Public Const constSEXOMASCULINO = 1
Public Const constSEXOFEMENINO = 2

' Constantes para Estado de ResultadoAsistencia
Global Const constRESULTADOINTRODUCIDO = 1
Global Const constRESULTADOVALIDADO = 2
Global Const constRESULTADOREPETIDO = 3
Global Const constRESULTADOANULADO = 4

' Funciones: Constantes para seguridad
Global Const constINTRODUCIRRESULTADOS = 6
Global Const constVALIDARINFORMES = 7
Global Const constENVIARINFORMES = 8
Global Const constCONSULTARESULTADOS = 10
Global Const constEXPLOTACION = 11
Global Const constRECEPEXTRACCIONES = 15
Global Const constVALIDACIONTECNICA = 16
Global Const constMANTENIMIENTOPRUEBAS = 17
Global Const constMODIFPRUEBASVALIDADAS = 26

'Autoanalizadores
Public Const constVITEK = 22
