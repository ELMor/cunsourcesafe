#include "indexcal.h"
#include <assert.h>


/////////////////////////////////////////////////////////////////////////////////////
// CLASS IndexedDay
/////////////////////////////////////////////////////////////////////////////////////
IndexedDay::IndexedDay(IlcUInt number, const char* name, IlcInt numberInWeek,
                       const Month* month):
                       Day(number, name, numberInWeek, month)
{
}


void IndexedDay::printOn(ostream& os) const
{
	Day::printOn(os);
}

/////////////////////////////////////////////////////////////////////////////////////
// CLASS IndexedMonth
/////////////////////////////////////////////////////////////////////////////////////
IndexedMonth::IndexedMonth(IlcUInt number, IlcUInt year, IlcUInt& indexMonth, IlcUInt& indexDay):
                Month(number, year, IlcFalse)
								
{
  _indexMonth= indexMonth++;
 
	createDays();
  indexDay += _numDays;
}

void IndexedMonth::createDays()
{
  for (IlcUInt i=0; i < _numDays; i++)
	  _days[i]= new IndexedDay(1 +i, getDayName(_startDayNameIndex +i),
                             (_startDayNameIndex +i) % _NUM_DAYS_WEEK, 
                             this);
}


/////////////////////////////////////////////////////////////////////////////////////
// CLASS IndexedCalendar
/////////////////////////////////////////////////////////////////////////////////////
IndexedCalendar::IndexedCalendar(Period period, TimeStep timeStep,
                                 IlcUInt timeStepPack):
				         Calendar(period, IlcFalse), _timeStep(timeStep),
                 _timeStepPack(timeStepPack), _indexDay(0), _indexMonth(0)
{
  createMonths();
  initializeIndexedDays();
	
	_firstIndexedDay= getGlobalIndexedDay(period.getDateFrom());
	_lastIndexedDay= getGlobalIndexedDay(period.getDateTo());
	
	_numDays= (_lastIndexedDay - _firstIndexedDay) +1;
	_timeMin= 0;
	_timeMax= (_numDays * (IlcInt)_timeStep) / _timeStepPack;
}

void IndexedCalendar::createMonths()
{
  if (_yearFrom == _yearTo)
	{
	  for (IlcUInt i= 0; i < _numMonths; i++)
      _months[i]= new IndexedMonth(i+ _monthFrom, _yearFrom, _indexMonth, _indexDay);
	}
	else
	{
		IlcUInt i;
		IlcUInt monthIndex= 0;
		for (i= 0; i < (_NUM_MONTHS_YEAR +1 - _monthFrom); i++)
    {
	    _months[monthIndex]= new IndexedMonth(i+ _monthFrom, _yearFrom, _indexMonth, _indexDay);
			monthIndex++;
		}

		IlcUInt yearIndex= _yearFrom;
  	yearIndex++;
		while ( yearIndex < _yearTo)
		{
      for (i= 0; i < _NUM_MONTHS_YEAR; i++)
      {
	      _months[monthIndex]= new IndexedMonth(i+ 1, yearIndex, _indexMonth, _indexDay);
			  monthIndex++;
		  }
	  yearIndex++;
		}

    for (i= 0; i < _monthTo; i++)
    {
	    _months[monthIndex]= new IndexedMonth(i+ 1, _yearTo, _indexMonth, _indexDay);
		  monthIndex++;
		}
	}
}


IlcUInt IndexedCalendar::getGlobalIndexedDay(Date d) const
{
	IlcInt day= d.getDay();
	IlcInt month= d.getMonth();
	IlcInt year= d.getYear();
	IlcUInt indexDay= 0;
  IlcUInt indexMonth= ((IndexedMonth*)getMonth(month, year))->getIndex();
	for (IlcUInt i=0; i < indexMonth; i++)
		indexDay += _months[i]->getNumDays();

	indexDay += day -1;
  return indexDay;
}

IlcUInt IndexedCalendar::getIndexedDay(Date d) const
{
	IlcInt day= d.getDay();
	IlcInt month= d.getMonth();
	IlcInt year= d.getYear();
	IlcUInt indexDay= 0;
  IlcUInt indexMonth= ((IndexedMonth*)getMonth(month, year))->getIndex();
	for (IlcUInt i=0; i < indexMonth; i++)
		indexDay += _months[i]->getNumDays();

	indexDay += day -1;
  return indexDay - _firstIndexedDay;
}


Date IndexedCalendar::getDate(IlcUInt timeUnit) const
{
  IlcUInt indexDay= (timeUnit*_timeStepPack) / (IlcInt)_timeStep;
  const Day* day= getDayFromIndex(indexDay);
  IlcInt dayNumber= day->getNumberInMonth();
  IlcInt monthNumber= day->getMonth()->getNumber();
  IlcInt yearNumber= day->getMonth()->getYear();

  IlcInt hour, minute;

  switch (_timeStep)
  {
  case DAYS: hour=0; minute=0; break;
  case HOURS: hour= (timeUnit*_timeStepPack % (IlcInt)_timeStep);                     
              minute=0; break;
  case MINUTES: 
                hour= (timeUnit*_timeStepPack % (IlcInt)_timeStep) / 60; 
                minute= (timeUnit*_timeStepPack % (IlcInt)_timeStep) % 60;
                break;
  };
	
	return Date(dayNumber, monthNumber, yearNumber, hour, minute);
}

void IndexedCalendar::initializeIndexedDays()
{
	_numDays= 0;
	IlcUInt i, j, index= 0;
	for (i=0; i < _numMonths; i++)
	  _numDays += _months[i]->getNumDays();

	_days= new const Day* [_numDays];
	for (i=0; i < _numMonths; i++)
		for (j=0; j < _months[i]->getNumDays(); j++)
		{
	    _days[index++] = _months[i]->getIndexDay(j);
		}
}

const Day* IndexedCalendar::getDayFromIndex(IlcUInt indexDay) const
{
	assert(indexDay >= 0 && 
		     (indexDay + _firstIndexedDay <= _lastIndexedDay));
  return _days[_firstIndexedDay+indexDay];
}

void IndexedCalendar::printOn(ostream& os) const
{
	
}

IlcUInt IndexedCalendar::getTimeUnit(Date date) const
{
  return getTimeUnit(Duration(getIndexedDay(date),
                              date.getHour(),
                              date.getMinute()));
}

IlcUInt IndexedCalendar::getTimeUnit(Duration duration) const
{
  IlcUInt timeUnit= (duration.getNumDays() * (IlcInt)_timeStep) /
                     _timeStepPack;

  switch (_timeStep)
  {
  case DAYS: break;
  case HOURS: timeUnit += duration.getNumHours() / _timeStepPack; 
              break; 
  case MINUTES: timeUnit += ((duration.getNumHours() * 60) + 
                              duration.getNumMinutes()) / 
                             _timeStepPack;
                break;
  };
	return timeUnit;
}


Duration IndexedCalendar::getDuration(Period period) const
{
  IlcUInt indexDay1= getIndexedDay(period.getDateFrom());
  IlcUInt indexDay2= getIndexedDay(period.getDateTo());
  IlcInt numDays= indexDay2 - indexDay1;

  IlcInt minuteFrom= period.getDateFrom().getMinute();
  IlcInt minuteTo= period.getDateTo().getMinute();

  IlcInt hourFrom= period.getDateFrom().getHour();
  IlcInt hourTo= period.getDateTo().getHour();
  
  IlcInt numHours;
  if (hourFrom > hourTo)
  {
    numDays--;
    numHours= (24 - hourFrom) + hourTo;
  }
  else 
    numHours= hourTo - hourFrom;

  IlcInt numMinutes;
  if (minuteFrom > minuteTo)
  {
    numHours--;
    numMinutes= (60 - minuteFrom) + minuteTo;
  }
  else 
    numMinutes= minuteTo - minuteFrom;
  
  return Duration(numDays, numHours, numMinutes);
}

void IndexedCalendar::setHoliday(Date date, IlcBool yn)
{ 
  IlcInt localIndex= getIndexedDay(date);
  ((Day*)getDayFromIndex(localIndex))->setHoliday(yn); 
}