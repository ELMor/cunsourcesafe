#include <weekindx.h>
#include <assert.h>

WeekIndexedCalendar::WeekIndexedCalendar(Period period, 
                                         TimeStep timeStep, IlcUInt tiemStepPack):
                     IndexedCalendar(period, timeStep, tiemStepPack)
{
  IlcUInt numDaysWeek= Month::getNumDaysWeek();
  _weekDayHoliday= new IlcBool[numDaysWeek];
  for (IlcUInt i=0; i < numDaysWeek; i++)
    _weekDayHoliday[i]= IlcFalse;
}

void WeekIndexedCalendar::setWeekDayHoliday(DayType dayInWeek, 
                                            IlcBool isHoliday)
{
  IlcUInt indexInWeek= (IlcUInt)dayInWeek;
  _weekDayHoliday[indexInWeek]= isHoliday;

  for (IlcUInt i=0; i < _numDays; i++)
  {
    Day* day= (Day*)getDayFromIndex(i);
    if (indexInWeek == day->getNumberInWeek())
      day->setHoliday(isHoliday);
  }
}

ostream& operator <<(ostream& os, const WeekIndexedCalendar& calendar)
{
  calendar.printOn(os);
	return os;
}
