#include <assert.h>
#include <calendar.h>

//////////////////////////////////////////////////////////////////////////////////////
// CLASS Day
//////////////////////////////////////////////////////////////////////////////////////
void Day::printOn(ostream& os) const
{
	os << _numberInMonth << '<' << _name << '>'; 
	if (_isHoliday) os << '*';

	os << endl;
}

const Date Day::getDate() const
{
  return Date(_numberInMonth, getMonth()->getNumber(), getMonth()->getYear(),
              0, 0);
}

Period Day::getPeriod(DayPeriod dayPeriod) const
{
  IlcInt monthNumber= _month->getNumber();
  IlcInt yearNumber= _month->getYear();
        
  Date dateBandFrom(_numberInMonth, monthNumber, yearNumber,
                    dayPeriod.getHourFrom(), 
                    dayPeriod.getMinuteFrom());

  Date dateBandTo(_numberInMonth, monthNumber, yearNumber,
                  dayPeriod.getHourTo(), 
                  dayPeriod.getMinuteTo());

  return Period(dateBandFrom, dateBandTo);
}

//////////////////////////////////////////////////////////////////////////////////////
// CLASS Month
//////////////////////////////////////////////////////////////////////////////////////
IlcUInt Month::_NUM_DAYS_WEEK= 7;
Month::Month(IlcUInt number, IlcUInt year, IlcBool create): 
       _number(number), _year(year)
{
				 
	if (ComputeMonthDays(_number, _year, _name, _numDays, _startDayNameIndex))
		{
		  _days= new Day* [_numDays];
			if (create) createDays();
		}
	else  cerr << "Wrong month or year number..." << endl;
}

void Month::createDays()
{
  for (IlcUInt i=0; i < _numDays; i++)
	  _days[i]= new Day(1 +i, getDayName(_startDayNameIndex +i), 
                      (_startDayNameIndex +i) % _NUM_DAYS_WEEK, 
                      this);
}

const char* Month::getDayName(IlcUInt dayIndex) const
{
	static char* dayNames[] = { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", 
		                          "Sabado" };
	dayIndex= dayIndex % _NUM_DAYS_WEEK;
	
	if (dayIndex == 0) dayIndex = _NUM_DAYS_WEEK;
  return strdup(dayNames[dayIndex-1]);
}


IlcBool Month::ComputeMonthDays(IlcUInt month, IlcUInt year, 
																char*&monthName, IlcUInt& numDaysMonth, IlcUInt& startDayNameIndex)
{
	static char* monthNames[]= { "January", "February", "March", "April", "May", "June",  "July", 
		                           "August",  "September", "October", "November", "December" };

  static IlcUInt monthdays[] ={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30};
  	
  if (year <= 1752) 
	{
		cerr << "Bad Year Argument : should be greater than 1752" << endl;
	  return IlcFalse;
  } 	
  
	if ((month < 1) || (month > 12)) 
	{
		cerr << "Bad Month Argument" << endl;
	  return IlcFalse;
  } 
  
  monthName= strdup(monthNames[month-1]);

  IlcUInt step  = 1;
  IlcBool isFeb29 = (!(year % 4) && ((year % 100) || !(year % 400)));
  if (month == 2)
    numDaysMonth = (isFeb29 ? 29 : 28) * step;
  else 
	{
	  numDaysMonth = (((month == 4)||(month == 6)||(month == 9)||(month == 11))? 30 : 31) * step;
  }
  
  IlcUInt days = (IlcUInt)((((int)year-1753) * 365) + 
			                    (((int)year-1753) /   4) -
			                    (((int)year-1701) / 100) +
			                    (((int)year-1601) / 400));
  
	if (isFeb29) monthdays[1] = 29;
  
	for (IlcUInt i = 1; i < month; i++)
	  days += monthdays[i-1];
  
	startDayNameIndex = (IlcUInt)((days+2) % _NUM_DAYS_WEEK);
  return IlcTrue;
}


void Month::setHoliday(IlcUInt dayNumber)
{
  if ((dayNumber >0) && (dayNumber <= _numDays))
  	_days[dayNumber-1]->setHoliday(IlcTrue);
}

void Month::setWeekEndHolidays()
{
	const IlcUInt SATURDAY= 7;
	const IlcUInt SUNDAY= 1;
  for (IlcUInt i=0; i < _numDays; i++)
	{
		IlcUInt index= (_startDayNameIndex +i) % _NUM_DAYS_WEEK;
		if (index == 0) index = _NUM_DAYS_WEEK;
  	if ((index == SATURDAY) || (index == SUNDAY))
		  _days[i]->setHoliday(IlcTrue);
	}
}


void Month::setSaturdayHolidays()
{
	const IlcUInt SATURDAY= 7;
  for (IlcUInt i=0; i < _numDays; i++)
	{
		IlcUInt index= (_startDayNameIndex +i) % _NUM_DAYS_WEEK;
		if (index == 0) index = _NUM_DAYS_WEEK;
  	if (index == SATURDAY)
		  _days[i]->setHoliday(IlcTrue);
	}
}

void Month::setSundayHolidays()
{
	const IlcUInt SUNDAY= 1;
  for (IlcUInt i=0; i < _numDays; i++)
	{
		IlcUInt index= (_startDayNameIndex +i) % _NUM_DAYS_WEEK;
		if (index == 0) index = _NUM_DAYS_WEEK;
  	if (index == SUNDAY)
		  _days[i]->setHoliday(IlcTrue);
	}
}

void Month::printOn(ostream& os) const
{
	os << _year << " " << _name << endl;
  for (IlcUInt i=0; i < _numDays; i++)
	  _days[i]->printOn(os);
	os << endl;
}

const Day* Month::getDay(IlcUInt numDay) const
{
	assert((numDay > 0) && (numDay <= _numDays));
	return _days[numDay-1];
}


//////////////////////////////////////////////////////////////////////////////////////
// CLASS Calendar
//////////////////////////////////////////////////////////////////////////////////////
IlcUInt Calendar::_NUM_MONTHS_YEAR= 12;

Calendar::Calendar(Period period, IlcBool create):
			    _monthFrom(period.getDateFrom().getMonth()), 
          _monthTo(period.getDateTo().getMonth()),
					_yearFrom(period.getDateFrom().getYear()), 
          _yearTo(period.getDateTo().getYear()), 
					_period(period)
{	
  if (_yearFrom == _yearTo)
	{
	  _numMonths= _monthTo +1 - _monthFrom;
	  _months= new Month* [_numMonths];

		if (create) createMonths();
	}
	else
	{
    _numMonths= _NUM_MONTHS_YEAR+1 - _monthFrom;
		IlcUInt numYears= _yearTo - _yearFrom;
		if (numYears > 1) 
			_numMonths += (numYears -1) * _NUM_MONTHS_YEAR;
    _numMonths += _monthTo;
		_months= new Month* [_numMonths];

		if (create) createMonths();
	}
}

void Calendar::createMonths()
{
  if (_yearFrom == _yearTo)
	{
	  for (IlcUInt i= 0; i < _numMonths; i++)
      _months[i]= new Month(i+ _monthFrom, _yearFrom);
	}
	else
	{
		IlcUInt i;
		IlcUInt monthIndex= 0;
		for (i= 0; i < (_NUM_MONTHS_YEAR +1 - _monthFrom); i++)
    {
	    _months[monthIndex]= new Month(i+ _monthFrom, _yearFrom);
			monthIndex++;
		}

		IlcUInt yearIndex= _yearFrom;
		yearIndex++;
		while ( yearIndex < _yearTo)
		{
      for (i= 0; i < _NUM_MONTHS_YEAR; i++)
      {
	      _months[monthIndex]= new Month(i+ 1, yearIndex);
			  monthIndex++;
		  }
			yearIndex++;
		}

    for (i= 0; i < _monthTo; i++)
    {
	    _months[monthIndex]= new Month(i+ 1, _yearTo);
		  monthIndex++;
		}
	}
}

void Calendar::setWeekEndHolidays()
{
  for (IlcUInt i= 0; i < _numMonths; i++)
	  _months[i]->setWeekEndHolidays();
}

void Calendar::setSaturdayHolidays()
{
	for (IlcUInt i= 0; i < _numMonths; i++)
	  _months[i]->setSaturdayHolidays();
}

void Calendar::setSundayHolidays()
{
	for (IlcUInt i= 0; i < _numMonths; i++)
	  _months[i]->setSundayHolidays();
}


void Calendar::printOn(ostream& os) const
{
	os << "Calendar: <YearFrom: " << _yearFrom << "> <MonthFrom: " 
		 << _monthFrom << "> <YearTo: " << _yearTo << "> <MonthTo: " 
		 << _monthTo << ">" << endl;

  for (IlcUInt i= 0; i < _numMonths; i++)
	  _months[i]->printOn(os);
}

const Month* Calendar::getMonth(IlcUInt numMonth, IlcUInt year) const
{
	assert((year >= _yearFrom) && (year <= _yearTo));
	if (year == _yearFrom)
	{
		assert((_monthFrom <= numMonth) && (numMonth > 0));
	}
  else if (year == _yearTo)
	{
	  assert((_monthTo >= numMonth) && (numMonth > 0));
	}
	
	IlcUInt monthIndex= 0;
	if (year == _yearFrom)
	  monthIndex += numMonth - _monthFrom;
	else 
	{
	  IlcUInt numYears= year - _yearFrom;
		monthIndex += _NUM_MONTHS_YEAR - _monthFrom;
    monthIndex += (numYears-1) * _NUM_MONTHS_YEAR;
    monthIndex += numMonth;
	}
	return _months[monthIndex];
}


const Day* Calendar::getDay(IlcUInt numDay, IlcUInt numMonth, IlcUInt year) const
{
  return getMonth(numMonth, year)->getDay(numDay);
}

const Day** Calendar::getDays(IlcUInt& numDays) const
{
	numDays= 0;
	IlcUInt i, j, index= 0;
	for (i=0; i < _numMonths; i++)
	  numDays += _months[i]->getNumDays();

	const Day** days= new const Day* [numDays];
	for (i=0; i < _numMonths; i++)
		for (j=0; j < _months[i]->getNumDays(); j++)
		{
	    days[index++] = _months[i]->getIndexDay(j);
		}
  
	return days;
}


ostream& operator <<(ostream& os, const Calendar& calendar)
{
	calendar.printOn(os);
	return os;
}

/////////////////////////////////////////////////////////////////////////////////////
// CLASS Date
/////////////////////////////////////////////////////////////////////////////////////
Date::Date(IlcUInt day, IlcUInt month, IlcUInt year, 
					 IlcUInt hour, IlcUInt minute, IlcUInt second):
	         _day(day), _month(month), _year(year), 
			     _hour(hour), _minute(minute), _second(0) 
{
	IlcBool isFeb29= (!(year % 4) && ((year % 100) || !(year % 400)));
	IlcUInt numDaysFeb= (isFeb29) ? 29 : 28;

  assert(hour >= 0 && hour <= 24);
  assert(minute >= 0 && minute < 60);
  assert(day > 0 && day < 32);
  assert(month > 0 && month < 13);
	switch (month)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: assert(day >0 && day < 32); break;
		case 2:  assert(day >0 && day <= numDaysFeb); break;
		case 4:
		case 6:
		case 9:
		case 11: assert(day >0 && day < 31); break;
    default: break;
	}
}

void Date::printOn(ostream& os) const
{
	os << "DATE <dd/mm/aaaa hh:mm:ss>: ";
  os << _day << "/" << _month << "/" << _year << " "
		 << _hour << ":" << _minute << endl << ":" << _second << endl;
}


IlcBool operator==(Date from, Date to)
{
	IlcInt monthFrom= from.getMonth();
	IlcInt monthTo= to.getMonth();
	IlcInt yearFrom= from.getYear();
	IlcInt yearTo= to.getYear();
	IlcInt dayFrom= from.getDay();
	IlcInt dayTo= to.getDay();
	IlcBool yn= IlcFalse;

	if (yearFrom == yearTo)
  {
    if (monthFrom == monthTo)
	  {
			if (dayFrom == dayTo) yn= IlcTrue;
			else yn= IlcFalse;
		}
		else yn= IlcFalse;
	}
	else yn= IlcFalse;

	return yn;
}


IlcBool operator<(Date from, Date to)
{
	IlcInt monthFrom= from.getMonth();
	IlcInt monthTo= to.getMonth();
	IlcInt yearFrom= from.getYear();
	IlcInt yearTo= to.getYear();
	IlcInt dayFrom= from.getDay();
	IlcInt dayTo= to.getDay();
	const IlcInt NUM_MONTHS_YEAR= 12;
	IlcBool yn= IlcFalse;

	if (yearFrom <= yearTo)
  {
		if (yearFrom == yearTo)
	  {
      if (monthFrom <= monthTo)
			{
				if (monthFrom == monthTo)
        {
					if (dayFrom < dayTo) yn= IlcTrue;
				  else yn= IlcFalse;
        }
        else yn= IlcTrue;
			}
			else yn= IlcFalse;
	  }
		else yn= IlcTrue;
	}
	else yn= IlcFalse;

	return yn;
}

ostream& operator <<(ostream& os, const Date& date)
{
	date.printOn(os);
	return os;
}


/////////////////////////////////////////////////////////////////////////////////////
// CLASS Duration
/////////////////////////////////////////////////////////////////////////////////////
IlcInt Duration::getDuration(const TimeStep ts, const IlcUInt tsp) const 
{
	IlcInt duration;
	switch (ts)
	{
		case DAYS:    duration= _numDays / tsp; break;
	  case HOURS:   duration= (_numDays*24 + _numHours) / tsp; break;
		case MINUTES: duration= (_numDays*24*60 + _numHours*60 + _numMinutes) / tsp; break;
		case SECONDS: duration= (_numDays*24*60*60 + _numHours*60*60 + _numMinutes*60 + _numSeconds) /tsp; break;
		default: break;
	}
	return duration;
}

void Duration::printOn(ostream& os) const
{
  os << "Days: " << _numDays << " Hours: " << _numHours
     << " Minutes: " << _numMinutes << endl;
}

ostream& operator <<(ostream& os, const Duration& duration)
{
  duration.printOn(os);
  return os;
}

/////////////////////////////////////////////////////////////////////////////////////
// CLASS Period
/////////////////////////////////////////////////////////////////////////////////////
void Period::printOn(ostream& os) const
{
	os << "From: " << _from << " to: " << _to << endl;
}


IlcInt Period::numMinutos()
{

  IlcInt min = 0;

  min += getDateTo().getMinute() - getDateFrom().getMinute();
  min += (getDateTo().getHour() - getDateFrom().getHour())*60;
  min += (getDateTo().getYear() - getDateFrom().getYear())*60*24*365;
  min += (getDateTo().getMonth() - getDateFrom().getMonth())*60*24*30;
  min += (getDateTo().getDay() - getDateFrom().getDay())*60*24;

  return (min);
}


/////////////////////////////////////////////////////////////////////////////////////
// CLASS DayPeriod
/////////////////////////////////////////////////////////////////////////////////////
DayPeriod::DayPeriod(IlcInt hourFrom, IlcInt minuteFrom, 
                     IlcInt hourTo, IlcInt minuteTo): 
                     _hourFrom(hourFrom), _minuteFrom(minuteFrom),
                     _hourTo(hourTo), _minuteTo(minuteTo)
{
  assert(_hourFrom >= 0 && _hourFrom <= 24);
  assert(_hourTo >= 0 && _hourTo <= 24);
  assert(_minuteFrom >= 0 && _minuteFrom <= 60);
  assert(_minuteTo >= 0 && _minuteTo <= 60);

  if (_hourFrom == _hourTo)
  {
    assert(_minuteFrom < _minuteTo);
  }
}


DayPeriod& 
DayPeriod::operator=(const DayPeriod& d)
{ 
	_hourFrom= d._hourFrom; 
	_hourTo= d._hourTo;
	_minuteFrom= d._minuteFrom; 
	_minuteTo= d._minuteTo;
  return *this;
}


Duration DayPeriod::getDuration() const 
{
  IlcInt numHours= _hourTo - _hourFrom;
  IlcInt numMinutes;
  if (_minuteFrom > _minuteTo)
  {
    numHours--;
    numMinutes= (60 - _minuteFrom) + _minuteTo;
  }
  else 
    numMinutes= _minuteTo - _minuteFrom;

  return Duration(0, numHours, numMinutes);
}

