#ifndef __WEEKINDEXEDCALENDAR_H_
#define __WEEKINDEXEDCALENDAR_H_

#include <indexcal.h>

/////////////////////////////////////////////////////////////////////////////////////
// CLASS WeekIndexedCalendar
/////////////////////////////////////////////////////////////////////////////////////
class WeekIndexedCalendar: public IndexedCalendar
{  
protected:
  IlcBool* _weekDayHoliday;	
public:
  WeekIndexedCalendar(Period period, TimeStep timeStep= MINUTES,
                      IlcUInt tiemStepPack= 1);

  void setWeekDayHoliday(DayType indexInWeek, 
                         IlcBool isHoliday);
};

ostream& operator <<(ostream& os, const WeekIndexedCalendar& calendar);


#endif // __WEEKINDEXEDCALENDAR_H_
