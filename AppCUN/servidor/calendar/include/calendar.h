#ifndef __CALENDAR_H_
#define __CALENDAR_H_

#include <iostream.h>
#include <string.h>
#include <tipos.h>

enum DayType { SABADO= 0, DOMINGO= 1, LUNES=2, MARTES=3, MIERCOLES=4,
               JUEVES= 5, VIERNES= 6 };

enum TimeStep { DAYS=1, HOURS=24, MINUTES=1440, SECONDS=0 };

/////////////////////////////////////////////////////////////////////////////////////
// CLASS Date
/////////////////////////////////////////////////////////////////////////////////////
class Date
{
  IlcUInt _day;
	IlcUInt _month;
	IlcUInt _year;
  IlcUInt _hour;
	IlcUInt _minute;
	IlcUInt _second;
public:
  Date():_day(0), _month(0), _year(0), _hour(0), _minute(0), _second(0) {}
	Date(IlcUInt day, IlcUInt month, IlcUInt year, 
		   IlcUInt hour=0, IlcUInt minute=0, IlcUInt second= 0);
	    
	void printOn(ostream& os) const;

	IlcUInt getDay() const { return _day; }
	IlcUInt getMonth() const { return _month; }
	IlcUInt getYear() const { return _year; }
	IlcUInt getHour() const { return _hour; }
	IlcUInt getMinute() const { return _minute; }
};
ostream& operator <<(ostream& os, const Date& date);
IlcBool operator<(Date from, Date to);
IlcBool operator==(Date from, Date to);

/////////////////////////////////////////////////////////////////////////////////////
// CLASS Duration
/////////////////////////////////////////////////////////////////////////////////////
class Duration
{
	IlcInt _numDays;
	IlcInt _numHours;
	IlcInt _numMinutes;
	IlcInt _numSeconds;
  IlcInt _ind;
public:
  Duration(): _numDays(0), _numHours(0),
		          _numMinutes(0), _numSeconds(0), _ind(0)
  {}
	
  Duration(IlcUInt numDays, IlcUInt numHours, 
		       IlcUInt numMinutes, IlcUInt numSeconds= 0, IlcInt indicador=0):
	         _numDays(numDays), _numHours(numHours),
		       _numMinutes(numMinutes), _numSeconds(numSeconds),
           _ind(indicador)
	{}

  Duration(const Duration& d)
  { _numDays= d._numDays; _numHours= d._numHours;
	  _numMinutes= d._numMinutes; _numSeconds= d._numSeconds;
    _ind = d._ind;
  }

  Duration& operator=(const Duration& d)
  { _numDays= d._numDays; _numHours= d._numHours;
	  _numMinutes= d._numMinutes; _numSeconds= d._numSeconds;
    _ind = d._ind;
    return *this;
  }

	IlcInt getDuration(const TimeStep ts, const IlcUInt timeStepPack) const;
  IlcInt getNumDays() const { return _numDays; }
	IlcInt getNumHours() const { return _numHours; }
	IlcInt getNumMinutes() const { return _numMinutes; }
  IlcInt getIndicador() const { return _ind; }
  void printOn(ostream& os) const;
};
ostream& operator <<(ostream& os, const Duration& duration);

/////////////////////////////////////////////////////////////////////////////////////
// CLASS Period
/////////////////////////////////////////////////////////////////////////////////////
class Period
{
	Date _from; 
	Date _to;   
public:
	Period(Date from, Date to): _from(from), _to(to) {}
	Date getDateFrom() const { return _from; }
	Date getDateTo() const { return _to; }
	void printOn(ostream& os) const;
  IlcInt  numMinutos();
};

/////////////////////////////////////////////////////////////////////////////////////
// CLASS DayPeriod
/////////////////////////////////////////////////////////////////////////////////////
class DayPeriod
{
  IlcInt _hourFrom;
  IlcInt _minuteFrom;
  IlcInt _hourTo; 
  IlcInt _minuteTo;
public:
  DayPeriod(IlcInt hourFrom, IlcInt minuteFrom, 
            IlcInt hourTo, IlcInt minuteTo);

	DayPeriod(){};

	DayPeriod(const DayPeriod& d)
  { 
		_hourFrom= d._hourFrom; 
		_hourTo= d._hourTo;
		_minuteFrom= d._minuteFrom; 
		_minuteTo= d._minuteTo;
  }

	DayPeriod& operator=(const DayPeriod& d);

  IlcInt getHourFrom() const { return _hourFrom; }
  IlcInt getMinuteFrom() const { return _minuteFrom; }
  IlcInt getHourTo() const { return _hourTo; }
  IlcInt getMinuteTo() const { return _minuteTo; }

  Duration getDuration() const;
	
};


class Month;

/////////////////////////////////////////////////////////////////////////////////////
// CLASS Day
/////////////////////////////////////////////////////////////////////////////////////
class Day
{
protected:
	const char* _name;
  const IlcUInt _numberInWeek;
	const IlcUInt _numberInMonth;
	IlcBool _isHoliday;
	const Month* _month;
public:
	Day(IlcUInt numberInMonth, const char* name, IlcInt numberInWeek,
      const Month* month): 
			_numberInMonth(numberInMonth), _numberInWeek(numberInWeek),
      _isHoliday(IlcFalse), _name(strdup(name)), _month(month) 
  {}

	void setHoliday(IlcBool yn) { _isHoliday= yn; }
	IlcBool isHoliday() const { return _isHoliday; }
	virtual void printOn(ostream& os) const;
	const Month* getMonth() const { return _month; }
	IlcUInt getNumberInMonth() const { return _numberInMonth; }
  IlcUInt getNumberInWeek() const { return _numberInWeek; }
  const char* getName() const { return _name; }
  const Date getDate() const;
  Period getPeriod(DayPeriod dayPeriod) const;
};


/////////////////////////////////////////////////////////////////////////////////////
// CLASS Month
/////////////////////////////////////////////////////////////////////////////////////
class Month
{
protected:
	char* _name;
	const IlcUInt _number;
	IlcUInt _numDays;
	Day** _days;
	IlcUInt _startDayNameIndex;
	IlcUInt _year;
  static IlcUInt _NUM_DAYS_WEEK;

	IlcBool ComputeMonthDays(IlcUInt month, IlcUInt year, 
		                       char*&monthName, IlcUInt& numDaysMonth, 
													 IlcUInt& startDayNameIndex);
	const char* getDayName(IlcUInt dayIndex) const;
	virtual void createDays();
public:
  Month(IlcUInt number, IlcUInt year, IlcBool create= IlcTrue);
	void setHoliday(IlcUInt dayNumber);
	void printOn(ostream& os) const;
	void setWeekEndHolidays();
	IlcUInt getNumDays() const { return _numDays; }
	Day* getIndexDay(IlcUInt i) { return _days[i]; } 
	IlcUInt getNumber() const { return _number; }
	IlcUInt getYear() const { return _year; }
	const char* getName() const { return _name; }
	const Day* getDay(IlcUInt numDay) const;
	void setSaturdayHolidays();
	void setSundayHolidays();

  static IlcUInt getNumDaysWeek() { return _NUM_DAYS_WEEK; }
};


/////////////////////////////////////////////////////////////////////////////////////
// CLASS Calendar
/////////////////////////////////////////////////////////////////////////////////////
class Calendar
{
protected:
  Month** _months;
	IlcUInt _numMonths;
	IlcUInt _monthFrom;
	IlcUInt _monthTo;
	IlcUInt _yearFrom;
	IlcUInt _yearTo;

	Period _period;

  static IlcUInt _NUM_MONTHS_YEAR;

	virtual void createMonths();
public:
  Calendar(Period period, IlcBool create= IlcTrue);

	virtual void printOn(ostream& os) const;
	void setWeekEndHolidays();
	void setSaturdayHolidays();
	void setSundayHolidays();
	IlcUInt getNumMonths() const { return _numMonths; }
	const Month* getMonth(IlcUInt numMonth, IlcUInt year) const;
	const Day* getDay(IlcUInt numDay, IlcUInt numMonth, IlcUInt year) const;
	const Day** getDays(IlcUInt& numDays) const;
  
  Period getPeriod() const { return _period; }

  static IlcUInt getNumMonthsYear() { return _NUM_MONTHS_YEAR; }
};

ostream& operator <<(ostream& os, const Calendar& calendar);

#endif // __CALENDAR_H_
