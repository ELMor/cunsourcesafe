// servidorView.cpp : implementation of the CServidorView class
//

#include "stdafx.h"
#include "servidor.h"

#include "servidorDoc.h"
#include "servidorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServidorView

IMPLEMENT_DYNCREATE(CServidorView, CView)

BEGIN_MESSAGE_MAP(CServidorView, CView)
	//{{AFX_MSG_MAP(CServidorView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServidorView construction/destruction

CServidorView::CServidorView()
{
	// TODO: add construction code here

}

CServidorView::~CServidorView()
{
}

BOOL CServidorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CServidorView drawing

void CServidorView::OnDraw(CDC* pDC)
{
	CServidorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CServidorView printing

BOOL CServidorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CServidorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CServidorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CServidorView diagnostics

#ifdef _DEBUG
void CServidorView::AssertValid() const
{
	CView::AssertValid();
}

void CServidorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CServidorDoc* CServidorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CServidorDoc)));
	return (CServidorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CServidorView message handlers
