// servidorDoc.cpp : implementation of the CServidorDoc class
//

#include "stdafx.h"
#include "servidor.h"

#include "servidorDoc.h"
#include <atlbase.h>

////////////////////////////////////////////////////////////////
#include <clinic.h>
#include <string.h>
#include <dataacss.h>
#include "Errores.h"

#include <ilsolver/ilm.h>
#include <ilsched/ilm.h>
////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServidorDoc

IMPLEMENT_DYNCREATE(CServidorDoc, CDocument)

BEGIN_MESSAGE_MAP(CServidorDoc, CDocument)
	//{{AFX_MSG_MAP(CServidorDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CServidorDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CServidorDoc)
	DISP_FUNCTION(CServidorDoc, "SetDiaDesde", SetDiaDesde, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetMesDesde", SetMesDesde, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetAnioDesde", SetAnioDesde, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetDiaHasta", SetDiaHasta, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetMesHasta", SetMesHasta, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetAnioHasta", SetAnioHasta, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetHoraDesde", SetHoraDesde, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetMinDesde", SetMinDesde, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetHoraHasta", SetHoraHasta, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetMinHasta", SetMinHasta, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetDiaSemana", SetDiaSemana, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetNumAct", SetNumAct, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetCodAct", SetCodAct, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetNumPhases", GetNumPhases, VT_I4, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDurationPhaseDias", GetDurationPhaseDias, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDurationPhaseHoras", GetDurationPhaseHoras, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDurationPhaseMin", GetDurationPhaseMin, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetNumRecursos", GetNumRecursos, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDurationRecursoDias", GetDurationRecursoDias, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDurationRecursoHoras", GetDurationRecursoHoras, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDurationRecursoMin", GetDurationRecursoMin, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetUsuarioBd", SetUsuarioBd, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CServidorDoc, "SetPasswdBd", SetPasswdBd, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CServidorDoc, "GetNumPhase", GetNumPhase, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetDescPhase", GetDescPhase, VT_BSTR, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "Confirm", Confirm, VT_I4, VTS_NONE)
	DISP_FUNCTION(CServidorDoc, "SetStartRecursoDia", SetStartRecursoDia, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartRecursoMes", SetStartRecursoMes, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartRecursoAnio", SetStartRecursoAnio, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartRecursoHora", SetStartRecursoHora, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartRecursoMin", SetStartRecursoMin, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartPhaseDia", SetStartPhaseDia, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartPhaseMes", SetStartPhaseMes, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartPhaseAnio", SetStartPhaseAnio, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartPhaseHora", SetStartPhaseHora, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetStartPhaseMin", SetStartPhaseMin, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartPhaseDia", GetStartPhaseDia, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartPhaseMes", GetStartPhaseMes, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartPhaseAnio", GetStartPhaseAnio, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartPhaseHora", GetStartPhaseHora, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartPhaseMin", GetStartPhaseMin, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartRecursoDia", GetStartRecursoDia, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartRecursoMes", GetStartRecursoMes, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartRecursoAnio", GetStartRecursoAnio, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartRecursoHora", GetStartRecursoHora, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetStartRecursoMin", GetStartRecursoMin, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "Run", Run, VT_I4, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetDiaPrefAct", SetDiaPrefAct, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetMesPrefAct", SetMesPrefAct, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetAnioPrefAct", SetAnioPrefAct, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetHoraPrefAct", SetHoraPrefAct, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetMinPrefAct", SetMinPrefAct, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetRecursoPreferente", SetRecursoPreferente, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetCodRecurso", GetCodRecurso, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetRecursoPreferente", GetRecursoPreferente, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetNumPhaseRecPref", GetNumPhaseRecPref, VT_I4, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "MoreSolutions", MoreSolutions, VT_I4, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "Initialize", Initialize, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CServidorDoc, "GetNumRecCapacity", GetNumRecCapacity, VT_I4, VTS_NONE)
	DISP_FUNCTION(CServidorDoc, "GetCodRecCapacity", GetCodRecCapacity, VT_I4, VTS_I4)
	DISP_FUNCTION(CServidorDoc, "GetLongDay", GetLongDay, VT_I4, VTS_NONE)
	DISP_FUNCTION(CServidorDoc, "GetCapacity", GetCapacity, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CServidorDoc, "SetServerBd", SetServerBd, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CServidorDoc, "Close", Close, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CServidorDoc, "GetObtenerFechaHasta", GetObtenerFechaHasta, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IServidor to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {BA1BADE7-7220-11D1-8276-00C04FB77FAE}
static const IID IID_IServidor =
{ 0xba1bade7, 0x7220, 0x11d1, { 0x82, 0x76, 0x0, 0xc0, 0x4f, 0xb7, 0x7f, 0xae } };

BEGIN_INTERFACE_MAP(CServidorDoc, CDocument)
	INTERFACE_PART(CServidorDoc, IID_IServidor, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServidorDoc construction/destruction

//Licencia ILOG.
static char *ilm_licenseSolver = \
"LICENSE syseca-bilbao\n\
RUNTIME Solver    4.200 NEVER 6AC6W12CMZAR Y pcwinnt";

static char *ilm_licenseScheduler = \
"LICENSE syseca-bilbao\n\
RUNTIME Scheduler 4.100 NEVER 8KWET276SWEF Y pcwinnt";


CServidorDoc::CServidorDoc()
{

	//Registrar licencia ILOG.
	IlcRegisterLicense(ilm_licenseSolver);
	IlcScheduleRegisterLicense(ilm_licenseScheduler);

	EnableAutomation();

	AfxOleLockApp();
}

CServidorDoc::~CServidorDoc()
{
	AfxOleUnlockApp();
}

BOOL CServidorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CServidorDoc serialization

void CServidorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CServidorDoc diagnostics

#ifdef _DEBUG
void CServidorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CServidorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CServidorDoc commands
DataBaseAccess* data_access = (DataBaseAccess*)NULL;
ClinicForm* form = (ClinicForm*)NULL;
ClinicForm* form_confir = (ClinicForm*)NULL;
unsigned int  v_diah = 0;
unsigned int  v_mesh = 0;
unsigned int  v_anioh = 0;
static  IlcInt steps;


long CServidorDoc::Run(long num_solutions) 
{
  //Resultado de la conexi�n.
  unsigned long v_result = GENE0000_NORMAL;
  long          v_err = GENE0000_NORMAL;

  //Fecha-hora sistema.
  char    v_fedia_sistema[C_GEN_LEN_FECHA + 1] = "";
  char    v_fehor_sistema[C_GEN_LEN_FEHOR + 1] = "";
  unsigned int  v_dia = 0;
  unsigned int  v_mes = 0;
  unsigned int  v_anio = 0;
  unsigned int  v_hora = 0;
  unsigned int  v_min = 0;

	//Grano de Tiempo (Par�metro de la Agenda)
	char	  v_grano_tiempo[C_GEN_LEN_GRANO_TIEMPO + 1] = "";

  const IlcInt NUM_MEDICAL_FACTS = 30;
	long	v_i = 0;

	if( steps == 0 )
	{
			data_access = new DataBaseAccess( getUsuarioBd(), getPasswdBd(),
                                        getServerBd(), v_result );

      if ( v_result == GENE0046_NO_CONNECT )
          return -3;
	}

  GetObtenerFechaHasta( getDiaHasta(), getMesHasta(), getAnioHasta());

  Period period(Date(getDiaDesde(),getMesDesde(),getAnioDesde()),
								Date(v_diah,v_mesh,v_anioh));


	data_access->setServidorDoc(this);

	gen_obtener_dato( "AG2500",
										"",
										"AG25GRANOTIEMPO",
										v_grano_tiempo );

	if( steps > 0 )
	{
		form->getClinicApplicationScheduler()->getManager().end();

	}

	if ( getHoraDesde() == 0 && getMinDesde() == 0 &&
													 getHoraHasta() == 0 && getMinHasta() == 0 )
	{ /* sin horas preferentes */

		form = new ClinicForm(NORMAL, period, atoi(v_grano_tiempo), diaSemana);

	} /* fin del if */
	else
	{ /* horas preferentes */

		DayPeriod* dayPeriod = new DayPeriod(	getHoraDesde(),getMinDesde(),
																					getHoraHasta(),getMinHasta());

		form = new ClinicForm(NORMAL, period, atoi(v_grano_tiempo), diaSemana, dayPeriod);

	} /* fin del else */

	data_access->setForm(form);

	data_access->initializeForm();

  MedicalFactGroup* mfg = new MedicalFactGroup(data_access->getClinicForm(),
																														NUM_MEDICAL_FACTS);

	for ( v_i = 0; v_i < contador; v_i++ )
	{
		if ( getDiaPrefAct(v_i) != 0 && getMesPrefAct(v_i) != 0 && 
																						getAnioPrefAct(v_i) != 0 )
		{ /* fecha-hora preferente para la actuaci�n en curso */
  
			Date*	fechap = new Date( getDiaPrefAct(v_i),getMesPrefAct(v_i),
															 getAnioPrefAct(v_i),
															 getHoraPrefAct(v_i),getMinPrefAct(v_i) );
			v_err = gen_read_fases_recursos( data_access->getClinicForm(), mfg, 
                                       getCodAct(v_i), getRecursoPref(v_i),
                                       fechap);
		  if ( v_err != GENE0000_NORMAL )
        return (v_err);

    } /* fin del if */
		else
		{ /* sin fecha preferente */

      gen_obtener_dato( "DUAL",
                        "",
                        "TO_CHAR(SYSDATE, 'DD-MM-YYYY hh24:MI')",
                        v_fehor_sistema );

      strncpy( v_fedia_sistema, v_fehor_sistema, C_GEN_LEN_FECHA );
      gen_desglosar_fechad( v_fedia_sistema, v_dia, v_mes, v_anio );
      gen_desglosar_fecha_hora( v_fehor_sistema, v_hora, v_min );

      if ( getDiaDesde() == (long)v_dia && getMesDesde() == (long)v_mes &&
            getAnioDesde() == (long)v_anio )
      {

        Date*	fechap = new Date( v_dia, v_mes, v_anio, v_hora, v_min );

			  v_err = gen_read_fases_recursos( data_access->getClinicForm(), mfg, 
                                         getCodAct(v_i), getRecursoPref(v_i),
                                         fechap);
      }
      else
      {
        v_err = gen_read_fases_recursos( data_access->getClinicForm(), mfg, 
                                         getCodAct(v_i), getRecursoPref(v_i) );
      }

      if ( v_err != GENE0000_NORMAL )
        return (v_err);

		} /* fin del else */

	} /* fin del for */

	data_access->getClinicForm()->addMedicalFactGroup(mfg);

	for ( v_i = 0; v_i < contador; v_i++ )
	{

    gen_read_iterations( getCodAct(v_i) );

  }

  if ( data_access->getClinicForm()->getNumPhasesMedicalFact() == -1 )
    return GENE0001_ERROR_NO_FASES;

  if ( data_access->getClinicForm()->getNumMedicalServiceFact() == -2 )
    return GENE0002_ERROR_NO_RECURSOS;

	data_access->getClinicForm()->getClinicApplicationScheduler()->defineProblem();

  form->searchAlgorithm();
  v_i = 0;
  
  while( ( v_i < num_solutions ) && ( form->solve() ) ) 
  {  
   v_i++;

  }  //fin del while 

	steps++;


	return v_i;
}


long CServidorDoc::getDiaDesde()
{
	return (diaDesde);
}


void CServidorDoc::SetDiaDesde(long diad) 
{
	diaDesde = diad;
}


void CServidorDoc::SetMesDesde(long mesd) 
{
	mesDesde = mesd;
}


long CServidorDoc::getMesDesde()
{
	return (mesDesde);
}


long CServidorDoc::getAnioDesde()
{
	return(anioDesde);
}

long CServidorDoc::getDiaHasta()
{
	return (diaHasta);
}


long CServidorDoc::getMesHasta()
{
	return (mesHasta);
}


long CServidorDoc::getAnioHasta()
{
	return (anioHasta);
}


void CServidorDoc::SetAnioDesde(long aniod) 
{
	anioDesde = aniod;
}


void CServidorDoc::SetDiaHasta(long diah) 
{
	diaHasta = diah;
}


void CServidorDoc::SetMesHasta(long mesh) 
{
	mesHasta = mesh;
}


void CServidorDoc::SetAnioHasta(long anioh) 
{
	anioHasta = anioh;
}

long CServidorDoc::getHoraDesde()
{
	return(horaDesde);
}


long CServidorDoc::getMinDesde()
{
	return(minDesde);
}

long CServidorDoc::getHoraHasta()
{
	return(horaHasta);
}

long CServidorDoc::getMinHasta()
{
	return(minHasta);
}

void CServidorDoc::SetHoraDesde(long horad) 
{
	horaDesde = horad;
}

void CServidorDoc::SetMinDesde(long mind) 
{
	minDesde = mind;
}

void CServidorDoc::SetHoraHasta(long horah) 
{
	horaHasta = horah;
}

void CServidorDoc::SetMinHasta(long minh) 
{
	minHasta = minh;
}

void CServidorDoc::SetDiaSemana(long indice) 
{
	diaSemana[indice] = 1;
}

long CServidorDoc::getNumAct()
{
	return (numAct);
}

void CServidorDoc::SetNumAct(long num) 
{
	numAct = num;
}

void CServidorDoc::SetCodAct(long codigo) 
{
	codAct[contador] = codigo;
	contador++;
}

long CServidorDoc::getCodAct(long cont)
{
	return (codAct[cont]);
}

long CServidorDoc::getDiaPrefAct(long cont)
{
	return (diaPref[cont]);
}

long CServidorDoc::getMesPrefAct(long cont)
{
	return (mesPref[cont]);
}

long CServidorDoc::getAnioPrefAct(long cont)
{
	return (anioPref[cont]);
}

long CServidorDoc::getHoraPrefAct(long cont)
{
	return (horaPref[cont]);
}

long CServidorDoc::getMinPrefAct(long cont)
{
	return (minPref[cont]);
}

void CServidorDoc::SetDiaPrefAct(long ind_act, long diap) 
{
	diaPref[ind_act] = diap;
}

void CServidorDoc::SetMesPrefAct(long ind_act, long mesp) 
{
	mesPref[ind_act] = mesp;
}

void CServidorDoc::SetAnioPrefAct(long ind_act, long aniop) 
{
	anioPref[ind_act] = aniop;
}

void CServidorDoc::SetHoraPrefAct(long ind_act, long horap) 
{
	horaPref[ind_act] = horap;
}

void CServidorDoc::SetMinPrefAct(long ind_act, long minp) 
{
	minPref[ind_act] = minp;
}

long CServidorDoc::GetNumPhases(long ind_act) 
{
	return (data_access->getClinicForm()->getMedicalFactGroup(0)->
					getMedicalFact(ind_act)->getCurrentPhase());
}

long CServidorDoc::GetStartPhaseDia(long ind_solution, long ind_act, long ind_phase) 
{
  return(data_access->getClinicForm()->getClinicAllSolutions()->
         getSolution(ind_solution)->getDateSolution(ind_act,ind_phase).
         getDay());
}

long CServidorDoc::GetStartPhaseMes(long ind_solution, long ind_act, long ind_phase) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase).
					getMonth());
}

long CServidorDoc::GetStartPhaseAnio(long ind_solution, long ind_act, long ind_phase) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase).
					getYear());
}

long CServidorDoc::GetStartPhaseHora(long ind_solution, long ind_act, long ind_phase) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase).
					getHour());
}

long CServidorDoc::GetStartPhaseMin(long ind_solution, long ind_act, long ind_phase) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase).
					getMinute());
}

long CServidorDoc::GetDurationPhaseDias(long ind_act, long ind_phase) 
{
	Duration duration = data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(ind_act)->getPhase(ind_phase)->
											getDuration();

	return (duration.getNumDays());
}

long CServidorDoc::GetDurationPhaseHoras(long ind_act, long ind_phase) 
{
	Duration duration = data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(ind_act)->getPhase(ind_phase)->
											getDuration();

	return (duration.getNumHours());
}

long CServidorDoc::GetDurationPhaseMin(long ind_act, long ind_phase) 
{
	Duration duration = data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(ind_act)->getPhase(ind_phase)->
											getDuration();

	return (duration.getNumMinutes());
}

long CServidorDoc::GetNumRecursos(long ind_act, long ind_phase) 
{
	return (data_access->getClinicForm()->getMedicalFactGroup(0)->
					getMedicalFact(ind_act)->getPhase(ind_phase)->
					getCurrentService());
}

long CServidorDoc::GetCodRecurso(long ind_solution, long ind_act, long ind_phase, 
                                 long ind_rec) 
{
  unsigned int  codrecursoSimple = 0;
  char          v_where[C_GEN_LEN_WHERE + 1] = "";
  char          codTipoRecurso[C_GEN_LEN_COD_RECURSO + 1] = "";

  if ( ind_solution >= 0 )
  { /* en caso de que haya soluci�n */

    return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getRecursoSolution(ind_act,ind_phase,ind_rec));

  } /* fin del if */
  else
  { /* si no hay soluci�n */

	  if ( data_access->getClinicForm()->getMedicalFactGroup(0)->
				  getMedicalFact(ind_act)->getPhase(ind_phase)->
				  getMedicalServiceReserve(ind_rec)->getMedicalService()->
				  getTypeMedicalService() == SINGLE )
	  {				
		  codrecursoSimple = data_access->getClinicForm()->getMedicalFactGroup(0)->
					  getMedicalFact(ind_act)->getPhase(ind_phase)->
					  getMedicalServiceReserve(ind_rec)->getMedicalService()->
					  getCodRecurso();
      
      sprintf( v_where, "AG11CODRECURSO = %d", codrecursoSimple );
      gen_obtener_dato( "AG1100",
                        v_where,
                        "AG14CODTIPRECU",
                        codTipoRecurso );

      return( atoi(codTipoRecurso) );
	  }
	  else
	  {
      return (
            data_access->getClinicForm()->getMedicalFactGroup(0)->
					  getMedicalFact(ind_act)->getPhase(ind_phase)->
					  getMedicalServiceReserve(ind_rec)->getMedicalService()->
					  getCodRecurso()
            );
	  }

  } /* fin del else */
}

long CServidorDoc::GetStartRecursoDia(long ind_solution, long ind_act, long ind_phase, 
                                      long ind_rec) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase,ind_rec).
					getDay());
}

long CServidorDoc::GetStartRecursoMes(long ind_solution, long ind_act, long ind_phase,
                                      long ind_rec) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase,ind_rec).
					getMonth());
}

long CServidorDoc::GetStartRecursoAnio(long ind_solution, long ind_act, long ind_phase,
                                       long ind_rec) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase,ind_rec).
					getYear());
}

long CServidorDoc::GetStartRecursoHora(long ind_solution, long ind_act, long ind_phase,
                                       long ind_rec) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase,ind_rec).
					getHour());
}

long CServidorDoc::GetStartRecursoMin(long ind_solution, long ind_act, long ind_phase, 
                                      long ind_rec) 
{
	return (data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getDateSolution(ind_act,ind_phase,ind_rec).
					getMinute());
}

long CServidorDoc::GetDurationRecursoDias(long ind_act, long ind_phase, long ind_rec) 
{
	Duration duration = data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(ind_act)->getPhase(ind_phase)->
											getMedicalServiceReserve(ind_rec)->
											getDuration();

	return (duration.getNumDays());
}

long CServidorDoc::GetDurationRecursoHoras(long ind_act, long ind_phase, long ind_rec) 
{
	Duration duration = data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(ind_act)->getPhase(ind_phase)->
											getMedicalServiceReserve(ind_rec)->
											getDuration();

	return (duration.getNumHours());
}

long CServidorDoc::GetDurationRecursoMin(long ind_act, long ind_phase, long ind_rec) 
{
	Duration duration = data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(ind_act)->getPhase(ind_phase)->
											getMedicalServiceReserve(ind_rec)->
											getDuration();

	return (duration.getNumMinutes());
}

long CServidorDoc::getRecursoPref(long cont)
{
	return (recPref[cont]);
}

void CServidorDoc::SetRecursoPreferente(long ind_act, long recurso_pref) 
{
	recPref[ind_act] = recurso_pref;
}

long CServidorDoc::GetRecursoPreferente(long ind_solution, long ind_act) 
{
  unsigned int  codrecSimplePref = 0;
  char          v_where[C_GEN_LEN_WHERE + 1] = "";
  char          codTipoRecurso[C_GEN_LEN_COD_RECURSO + 1] = "";

	if ( ind_solution >= 0 )
	{ //soluciones

	  return(data_access->getClinicForm()->getClinicAllSolutions()->
					getSolution(ind_solution)->getRecPrefSolution(ind_act));

  } //fin del if
  else
  { //sin soluciones

    return ( data_access->getClinicForm()->getMedicalFactGroup(0)->
					              getMedicalFact(ind_act)->getMedicalFactRecPref() );

  } //fin del else
}

char* CServidorDoc::getUsuarioBd()
{
	return( usuario_bd );
}

char* CServidorDoc::getPasswdBd()
{
	return( passwd_bd );
}

void CServidorDoc::SetUsuarioBd(LPCTSTR usuario) 
{
	strcpy( usuario_bd, usuario );
}

void CServidorDoc::SetPasswdBd(LPCTSTR passwd) 
{
	strcpy( passwd_bd, passwd );
}

long CServidorDoc::GetNumPhase(long ind_act, long ind_phase) 
{
	return (data_access->getClinicForm()->getMedicalFactGroup(0)->
					getMedicalFact(ind_act)->getPhase(ind_phase)->getCodFase());
}


BSTR CServidorDoc::GetDescPhase(long ind_act, long ind_phase) 
{
	CString strResult(data_access->getClinicForm()->getMedicalFactGroup(0)->
										getMedicalFact(ind_act)->getPhase(ind_phase)->getName()
										);

	return strResult.AllocSysString();
}

long CServidorDoc::Confirm() 
{
  //Resultado.
  long  v_err = GENE0000_NORMAL;

	//Grano de Tiempo (Par�metro de la Agenda)
	char	v_grano_tiempo[C_GEN_LEN_GRANO_TIEMPO + 1] = "";
  static  IlcInt stepsc;

  const IlcInt NUM_MEDICAL_FACTS = 30;
	long	v_i = 0, v_j = 0, v_k = 0;

  Period period(Date(getDiaDesde(),getMesDesde(),getAnioDesde()),
								Date(v_diah,v_mesh,v_anioh));

	gen_obtener_dato( "AG2500",
										"",
										"AG25GRANOTIEMPO",
										v_grano_tiempo );

  if( stepsc > 0 )
	{
		form_confir->getClinicApplicationScheduler()->getManager().end();

	}

	form_confir = new ClinicForm(CONFIRMACION, period, atoi(v_grano_tiempo));

	data_access->setForm(form_confir);

	data_access->initializeForm();

  MedicalFactGroup* mfg = new MedicalFactGroup(data_access->getClinicForm(),
																														NUM_MEDICAL_FACTS);

	for ( v_i = 0; v_i < cont_act; v_i++ )
	{
		v_err = gen_read_fases_recursos( data_access->getClinicForm(), mfg, 
														 getCodActConfirm(v_i), getRecursoPref(v_i) );

    if ( v_err != GENE0000_NORMAL )
        return (v_err);

	} /* fin del for */

	data_access->getClinicForm()->addMedicalFactGroup(mfg);

	for ( v_i = 0; v_i < cont_act; v_i++ )
	{ /* bucle por actuaciones */

		for ( v_j = 0; v_j < NumPhases[v_i]; v_j++ )
		{ /* bucle por fases para actuaci�n en curso */

			Date datePhase( diaPhase[v_i][v_j], 
											mesPhase[v_i][v_j], 
											anioPhase[v_i][v_j], 
											horaPhase[v_i][v_j], 
											minPhase[v_i][v_j] );

			data_access->getClinicForm()->getClinicApplicationScheduler()->
			addCst(data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(v_i)->getPhase(v_j)->
											getReserve().startsAt(datePhase)
						);

			for ( v_k = 0; v_k < NumRecursos[v_i][v_j]; v_k++ )
			{ /* bucle por recursos para fase y actuaci�n en curso */
	
				Date	dateRecurso( diaRec[v_i][v_j][v_k],
													 mesRec[v_i][v_j][v_k], 
													 anioRec[v_i][v_j][v_k],
													 horaRec[v_i][v_j][v_k], 
													 minRec[v_i][v_j][v_k] 
													 );

				data_access->getClinicForm()->getClinicApplicationScheduler()->
				addCst(data_access->getClinicForm()->
											getMedicalFactGroup(0)->
											getMedicalFact(v_i)->getPhase(v_j)->
											getMedicalServiceReserve(v_k)->
											getReserve().startsAt(dateRecurso)
						);

			} /* fin del for */

		} /* fin del for */

	} /* fin del for */

	data_access->getClinicForm()->getClinicApplicationScheduler()->defineProblem();

  if (form_confir->solve())
  {
		stepsc++;

	  form_confir->printSolution();

		return GENE0001_SOLUCION;
  }
	else
	{ /* no hay soluci�n */

		stepsc++;

		cout << "No hay solucion..." << endl;

		return GENE0000_NORMAL;

	} /* fin del else */
}

void CServidorDoc::SetStartRecursoDia(long act, long phase, long recurso, long dia) 
{
	if ( cont_rec == 0 || cdRec != recurso )
	{ /* Recursos sucesivos */

		cdRec = recurso;
		cont_rec++;

	} /* fin del if */

	Recurso[cont_act-1][cont_phase-1][cont_rec - 1] = recurso;
	diaRec[cont_act-1][cont_phase-1][cont_rec-1] = dia;
	NumRecursos[cont_act-1][cont_phase-1] = cont_rec;
}

void CServidorDoc::SetStartRecursoMes(long act, long phase, long recurso, long mes) 
{
	mesRec[cont_act-1][cont_phase-1][cont_rec-1] = mes;

}

void CServidorDoc::SetStartRecursoAnio(long act, long phase, long recurso, long anio) 
{
	anioRec[cont_act-1][cont_phase-1][cont_rec-1] = anio;

}

void CServidorDoc::SetStartRecursoHora(long act, long phase, long recurso, long hora) 
{
	horaRec[cont_act-1][cont_phase-1][cont_rec-1] = hora;

}

void CServidorDoc::SetStartRecursoMin(long act, long phase, long recurso, long min) 
{
	minRec[cont_act-1][cont_phase-1][cont_rec-1] = min;

}

void CServidorDoc::SetStartPhaseDia(long act, long phase, long dia) 
{

	if ( act != cdAct )
	{ /* Actuaciones sucesivas */

		cdAct = act;
    cdPhase = phase;
		cont_act++;
		cont_phase = 1;
		cont_rec = 0;

	} /* fin del if */

	else if ( phase != cdPhase )
	{ /* Fases sucesivas */

		cdPhase = phase;
		cont_phase++;
		cont_rec = 0;

	} /* fin del if */

	Actuacion[cont_act - 1] = act;
	Phase[cont_act - 1][cont_phase - 1] = phase;
	diaPhase[cont_act-1][cont_phase-1] = dia;

	NumPhases[cont_act - 1] = cont_phase;

}

void CServidorDoc::SetStartPhaseMes(long act, long phase, long mes) 
{
	mesPhase[cont_act-1][cont_phase-1] = mes;

}

void CServidorDoc::SetStartPhaseAnio(long act, long phase, long anio) 
{
	anioPhase[cont_act-1][cont_phase-1] = anio;

}

void CServidorDoc::SetStartPhaseHora(long act, long phase, long hora) 
{
	horaPhase[cont_act-1][cont_phase-1] = hora;

}

void CServidorDoc::SetStartPhaseMin(long act, long phase, long min) 
{
	minPhase[cont_act-1][cont_phase-1] = min;

}

long CServidorDoc::getCodActConfirm(long cont)
{
	return( Actuacion[cont] );
}

long CServidorDoc::getIndiceAct(long codAct)
{
	long	v_i = 0;

	for ( v_i = 0; v_i < cont_act; v_i++ )
	{ /* bucle por actuaciones */

		if ( Actuacion[v_i] == codAct )
			return v_i;

	} /* fin del for */

	return GENE0001_ERROR_NO_ACTUACION;
}

long CServidorDoc::getIndicePhase(long codAct, long codPhase)
{
	long indice = getIndiceAct( codAct );

	long	v_i = 0;

	for ( v_i = 0; v_i < NumPhases[indice]; v_i++ )
	{ /* bucle por fases */

		if ( Phase[indice][v_i] == codPhase )
			return v_i;

	} /* fin del for */

	return GENE0001_ERROR_NO_ACTUACION;

}

long CServidorDoc::GetNumPhaseRecPref(long ind_act) 
{

  long numPhases = data_access->getClinicForm()->getMedicalFactGroup(0)->
					getMedicalFact(ind_act)->getCurrentPhase();

	for(IlcInt i=0; i < numPhases; i++)
	{
		if( data_access->getClinicForm()->getMedicalFactGroup(0)->
        getMedicalFact(ind_act)->getPhase(i)->getMedicalServicePref() != 255 )
			return(i);
  }

	return( -1 );
}


long CServidorDoc::MoreSolutions(long num_solutions) 
{
  long	v_i = 0;

  while( ( v_i < num_solutions ) && ( data_access->getClinicForm()->solve() ) ) 
  {  

    v_i++;

  }  //fin del while 

	return v_i;
}

void CServidorDoc::Initialize() 
{
	for( int i = 0; i < 7; i ++ )
		diaSemana[i] = 0;

	contador = 0;

	horaDesde = 0;
	minDesde = 0;
	horaHasta = 0;
	minHasta = 0;

	for ( i = 0; i < 30; i++ )
	{
		diaPref[i] = 0;
		mesPref[i] = 0;
		anioPref[i] = 0;
		horaPref[i] = 0;
		minPref[i] = 0;
    recPref[i] = 0;
		NumPhases[i] = 0;
	}

	for ( i = 0; i < 30; i++ )
		for( int j = 0; j < 30; j++ )
			NumRecursos[i][j] = 0;

	cont_act = 0;
	cont_phase = 0;
	cont_rec = 0;

	cdAct = 0;
	cdPhase = 0;
	cdRec = 0;
}

long CServidorDoc::GetNumRecCapacity() 
{
  long  contador = 0;
  IlcInt numMedicalServices = data_access->getClinicForm()->
                              getClinicApplication()->getCurrentMedicalServices();

  for ( int i=0; i < numMedicalServices; i++ )
    if ( data_access->getClinicForm()->getClinicApplication()->
			 getMedicalService(i)->getTypeMedicalService() == SINGLE )
	  {				
      contador ++;
    }

  return(contador);
}



long CServidorDoc::GetCodRecCapacity(long ind_rec) 
{
  IlcInt numSingles = 0;
  IlcInt numMedicalServices = data_access->getClinicForm()->
                              getClinicApplication()->getCurrentMedicalServices();

  for ( int i=0; i < numMedicalServices; i++ )
  {
    if ( data_access->getClinicForm()->getClinicApplication()->
			   getMedicalService(i)->getTypeMedicalService() == SINGLE
        )
	  {	
      if ( numSingles == ind_rec )
      {
        break;
      }
      
      numSingles++;
    }
  }

  return(
         data_access->getClinicForm()->
         getClinicApplication()->getMedicalService(i)->getCodRecurso()
         );
}



long CServidorDoc::GetLongDay() 
{
	IlcInt interv = data_access->getClinicForm()->getCalendar()->getTimeStepPack();

	return ( C_GEN_MINUTOS_EN_DIA / interv );
}


long CServidorDoc::GetCapacity(long ind_rec, long dia, long mes, long anio, long ind)
{
  char  v_fecha_d[C_GEN_LEN_FECHA + 1] = "";
  unsigned long v_fecha_d_num = 0;
  char  v_fecha_h[C_GEN_LEN_FECHA + 1] = "";
  unsigned long v_fecha_h_num = 0;
  char  v_fecha_p[C_GEN_LEN_FECHA + 1] = "";
  unsigned long v_fecha_p_num = 0;

  getDate( getDiaDesde(), getMesDesde(), getAnioDesde(), v_fecha_d );
  v_fecha_d_num = gen_fecha_numero( v_fecha_d );

  getDate( v_diah, v_mesh, v_anioh, v_fecha_h );
  v_fecha_h_num = gen_fecha_numero( v_fecha_h );

  getDate( dia, mes, anio, v_fecha_p );
  v_fecha_p_num = gen_fecha_numero( v_fecha_p );


  if ( v_fecha_p_num >= v_fecha_d_num && v_fecha_p_num < v_fecha_h_num )
  { //Fecha seleccionada dentro del periodo de estudio.

    IlcInt timeUnitMin= data_access->getClinicForm()->getCalendar()->
                        getTimeUnit(Date(dia,mes,anio,0,0));

    MedicalServiceSingle* _service = (MedicalServiceSingle*)data_access->getClinicForm()->
                                      getClinicApplication()->getCodMedicalService(ind_rec);

    return(_service->getCapacity().getCapacityMax(timeUnitMin+ind));

  }
  else
  { //Fecha seleccionada no est� dentro del periodo de estudio.

    return 0;

  }

}


void CServidorDoc::getDate(int dia, int mes, int anio, char * fecha)
{
  char  dia_alf[C_GEN_LEN_DIA + 1] = "";
  char  mes_alf[C_GEN_LEN_DIA + 1] = "";
  char  anio_alf[C_GEN_LEN_ANIO + 1] = "";

	if ( dia < 10 )
		sprintf( dia_alf, "0%d", dia );	
	else
		sprintf( dia_alf, "%d", dia );
	if ( mes < 10 )
		sprintf( mes_alf, "0%d", mes );
	else
		sprintf( mes_alf, "%d", mes );
	sprintf( anio_alf, "%d", anio );
	strcpy( fecha, dia_alf );
	strcat( fecha, "-" );
	strcat( fecha, mes_alf );
	strcat( fecha, "-" );
	strcat( fecha, anio_alf );

}

void CServidorDoc::SetServerBd(LPCTSTR server) 
{
  strcpy( server_bd, server );
}

char* CServidorDoc::getServerBd()
{
  return( server_bd );
}

void CServidorDoc::Close() 
{

	if (steps > 0)
	{ //conexi�n realizada

		gen_disconnect_ora();

	}
}

void CServidorDoc::GetObtenerFechaHasta(long ae_diah, long ae_mesh, long ae_anioh) 
{
  //Declaraci�n de variables.
 	char	diah_alf[C_GEN_LEN_DIA + 1] = "";
	char	mesh_alf[C_GEN_LEN_MES + 1] = "";
	char	anioh_alf[C_GEN_LEN_ANIO + 1] = "";
  char  v_fecha_hasta[C_GEN_LEN_FECHA + 1] = "";
  char  v_dato[C_GEN_LEN_DATO + 1] = "";

  if ( ae_diah < 10 )
		sprintf( diah_alf, "0%d", ae_diah );	
	else
		sprintf( diah_alf, "%d", ae_diah );
	if ( ae_mesh < 10 )
		sprintf( mesh_alf, "0%d", ae_mesh );
	else
		sprintf( mesh_alf, "%d", ae_mesh );

	sprintf( anioh_alf, "%d", ae_anioh );
	strcpy( v_fecha_hasta, diah_alf );
	strcat( v_fecha_hasta, "-" );
	strcat( v_fecha_hasta, mesh_alf );
	strcat( v_fecha_hasta, "-" );
	strcat( v_fecha_hasta, anioh_alf );

  sprintf( v_dato,
			"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') + 1, 'DD-MM-YYYY')",
			v_fecha_hasta );

	gen_obtener_dato( "DUAL",
						"",
						v_dato,
						v_fecha_hasta );

	strncpy( diah_alf, v_fecha_hasta, C_GEN_LEN_DIA );
	v_diah = atoi( diah_alf );
	strncpy( mesh_alf, &v_fecha_hasta[3], C_GEN_LEN_MES );
  v_mesh = atoi( mesh_alf );
	strcpy( anioh_alf, &v_fecha_hasta[6] );
	v_anioh = atoi( anioh_alf );

}
