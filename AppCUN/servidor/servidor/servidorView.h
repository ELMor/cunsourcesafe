// servidorView.h : interface of the CServidorView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVIDORVIEW_H__BA1BADF3_7220_11D1_8276_00C04FB77FAE__INCLUDED_)
#define AFX_SERVIDORVIEW_H__BA1BADF3_7220_11D1_8276_00C04FB77FAE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CServidorView : public CView
{
protected: // create from serialization only
	CServidorView();
	DECLARE_DYNCREATE(CServidorView)

// Attributes
public:
	CServidorDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServidorView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CServidorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CServidorView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in servidorView.cpp
inline CServidorDoc* CServidorView::GetDocument()
   { return (CServidorDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVIDORVIEW_H__BA1BADF3_7220_11D1_8276_00C04FB77FAE__INCLUDED_)
