#include <clinic.h>
#include <string.h>
#include <strstrea.h>
#include /**/<ilsched/search.h>
#include <ostream.h>


IlcInt indice;
IlcInt indreserva;
IlcInt indact;
IlcInt contador[20];
IlcInt timeCita[200][20];
IlcInt timeSimple;
IlcInt indexGoal;
IlcInt TimeRef = 0;

IlcBool flag = IlcFalse;


////////////////////////////////////////////////////////////////////
// class ClinicSolution
////////////////////////////////////////////////////////////////////
ClinicSolution::ClinicSolution(ClinicForm* form,
                               ClinicAllSolutions* allSolutions):_form(form),
                                                                 _allSolutions(allSolutions)
                                                       
{
	const IlcInt maxArray= 1000;
	IlcInt index= 0;
	Date acts[maxArray];

  IlcInt  recs[maxArray];
  IlcInt indice = 0;

  IlcInt  recspref[maxArray];
  IlcInt indaux = 0;

	for (IlcInt i=0; i < form->getCurrentMedicalFactGroups(); i++)
	{
		IlcInt groupSize= form->getMedicalFactGroup(i)->getCurrentMedicalFacts();

		for (IlcInt j=0; j < groupSize; j++)
		{
			MedicalFact* mf= form->getMedicalFactGroup(i)->getMedicalFact(j);

      recspref[indaux++] = mf->getMedicalFactRecPref();

			IlcInt numPhases = mf->getCurrentPhase();
			for (IlcInt k=0; k < numPhases; k++)
			{
				IlcInt numReserves= mf->getPhase(k)->getCurrentService();

        acts[index++]= form->getCalendar()->getDate(mf->getPhase(k)->getReserve().getStartVariable().getValue());
				for (IlcInt w=0; w < numReserves; w++)
				{
	        acts[index++]= form->getCalendar()->getDate(mf->getPhase(k)->getMedicalServiceReserve(w)->getReserve().getStartVariable().getValue()); 

          if ( mf->getPhase(k)->getMedicalServiceReserve(w)->
               getMedicalService()->getTypeMedicalService() == SINGLE )
          {
            recs[indice++] = mf->getPhase(k)->getMedicalServiceReserve(w)->
                             getMedicalService()->getCodRecurso();
	        }
	        else
	        {

            recs[indice++] = form->getClinicApplication()->
                  getMedicalService(mf->getPhase(k)->getMedicalServiceReserve(w)->
                  getReserve().getName())->getCodRecurso();
	        }
				}
			}
		}
	}

	_solutions = new Date[index];

	for (i=0; i < index; i++)
	{
		_solutions[i] = acts[i];
	}

  _recs = new IlcInt[indice];

	for (i=0; i < indice; i++)
	{
		_recs[i] = recs[i];
	}

  _recspref = new IlcInt[indaux];

	for (i=0; i < indaux; i++)
	{
		_recspref[i] = recspref[i];
	}

}



ClinicSolution::~ClinicSolution()
{
  delete [] _solutions;
}


Date 
ClinicSolution::getDateSolution(IlcInt act,IlcInt phase,IlcInt rec)
{
  IlcInt posAct = 0;

	for (IlcInt i=0; i < _form->getCurrentMedicalFactGroups(); i++)
	{
		for (IlcInt j=0; j < act; j++)
		{
			MedicalFact* mf= _form->getMedicalFactGroup(i)->getMedicalFact(j);

			IlcInt numPhases= mf->getCurrentPhase();
			for (IlcInt k=0; k < numPhases; k++)
			{
				IlcInt numReserves= mf->getPhase(k)->getCurrentService();
				for (IlcInt w=0; w < numReserves; w++)
				{
          posAct++;
				}
        posAct++;
			}
		}
	}

  MedicalFact* mf= _form->getMedicalFactGroup(i-1)->getMedicalFact(act);

	for (IlcInt k=0; k < phase; k++)
		{
			IlcInt numReserves= mf->getPhase(k)->getCurrentService();
			for (IlcInt w=0; w < numReserves; w++)
			{
          posAct++;
			}
      posAct++;
		}

  if( rec != -1)
  {
    return(_solutions[posAct+rec+1]);
  }
  else
  {
    return(_solutions[posAct]);
  }

}

IlcInt 
ClinicSolution::getRecursoSolution(IlcInt act,IlcInt phase,IlcInt rec)
{
  IlcInt posAct = 0;

	for (IlcInt i=0; i < _form->getCurrentMedicalFactGroups(); i++)
	{
		for (IlcInt j=0; j < act; j++)
		{
			MedicalFact* mf= _form->getMedicalFactGroup(i)->getMedicalFact(j);

			IlcInt numPhases= mf->getCurrentPhase();
			for (IlcInt k=0; k < numPhases; k++)
			{
				IlcInt numReserves= mf->getPhase(k)->getCurrentService();
				for (IlcInt w=0; w < numReserves; w++)
				{
          posAct++;
				}
			}
		}
	}

  MedicalFact* mf= _form->getMedicalFactGroup(i-1)->getMedicalFact(act);

	for (IlcInt k=0; k < phase; k++)
		{
			IlcInt numReserves= mf->getPhase(k)->getCurrentService();
			for (IlcInt w=0; w < numReserves; w++)
			{
          posAct++;
			}
		}

  return(_recs[posAct+rec]);
}



IlcInt 
ClinicSolution::getRecPrefSolution(IlcInt act)
{
  IlcInt posAct = 0;

	for (IlcInt i=0; i < _form->getCurrentMedicalFactGroups(); i++)
	{
		for (IlcInt j=0; j < act; j++)
		{
      if ( _form->getMedicalFactGroup(i)->getMedicalFact(j)->
           getMedicalFactRecPref() != -1 )

        posAct++;
		}
	}

  return(_recspref[posAct]);
}

////////////////////////////////////////////////////////////////////
// class ClinicAllSolutions
////////////////////////////////////////////////////////////////////
ClinicAllSolutions::ClinicAllSolutions(ClinicForm* form):_numSolutions(60),
                                                         _currentSolutions(0),
                                                         _form(form)
{
	_solutions = new ClinicSolution*[_numSolutions];
}


ClinicAllSolutions::~ClinicAllSolutions()
{
  for(IlcInt i=0; i < _currentSolutions; i++)
    delete _solutions[i];
  delete [] _solutions;
}


void 
ClinicAllSolutions::addSolution()
{
	assert(_currentSolutions < _numSolutions);

  ClinicSolution* solution = new ClinicSolution(getClinicForm(),this);

  _solutions[_currentSolutions++]= solution;

}



ClinicSolution* 
ClinicAllSolutions::getSolution(IlcInt Index)
{
	assert(Index < _currentSolutions );

  return(_solutions[Index]);

}

////////////////////////////////////////////////////////////////////
// class ClinicClientBusy
////////////////////////////////////////////////////////////////////
ClinicClientBusy::ClinicClientBusy(Period* period):_period(period)
{
}



////////////////////////////////////////////////////////////////////
// class ClinicClient
////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa un cliente de la Cl�nica          */
/*                                                                */
/*                                                                */
/* AUTOR:    Salvador Pe�alver  (ILOG)                            */
/*                                                                */
/* FECHA:    16-10-97                                             */
/*                                                                */
/******************************************************************
*/
ClinicClient::ClinicClient(ClinicForm* form, const char* name,
													 IlcInt codClient,
                           IlcInt maxProperties,
													 IlcInt clinicClientBusy):
              _form(form), _name(name), _numProperties(maxProperties),
						  _currentProperty(0),_codClient(codClient),
							_numClinicClientBusy(clinicClientBusy),_currentClinicClientBusy(0)
{    
	_properties= new ClientProperty*[_numProperties];

	_clinicClientBusySet= new ClinicClientBusy*[_numClinicClientBusy];

  SchedulerCalendar schedule= _form->getClinicApplicationScheduler()->
                                         getSchedule();
	_capacity = UnaryResourceCalendar(schedule);
	schedule.getManager().add(_capacity.makeTimetableConstraint());

  _form->getClinicApplication()->addClinicClient(this);

  IlcInt maxSize= _form->getCalendar()->getTimeMax();
  _timeTable= _form->getClinicApplicationScheduler()->createIntArray(maxSize);

 
 const IlcInt NOT_USED= 0;
  for (IlcInt i=0; i < maxSize; i++)
    _timeTable[i]= NOT_USED;

}



void 
ClinicClient::printOn(ostream& os) const
{
	IlcInt timeUnitMin= _form->getClinicApplication()->getCalendar()->getTimeUnit(_form->getPeriod().getDateFrom());
  IlcInt timeUnitMax= _form->getClinicApplication()->getCalendar()->getTimeUnit(_form->getPeriod().getDateTo());

}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Baja la capacidad del tipo de d�a durante    */
/*                   un periodo de tiempo.                        */
/*                                                                */
/*                                                                */
/* AUTOR:    Salvador Pe�alver  (ILOG)                            */
/*                                                                */
/* FECHA:    16-10-97                                             */
/*                                                                */
/******************************************************************
*/

void ClinicClient::registerPreference(DayType dayType, DayPeriod* dayPeriod)
{
  IlcUInt indexInWeek= (IlcUInt)dayType;
  WeekIndexedCalendar* calendar= _form->getCalendar();
  const IlcInt USED= 1;
  
  for (IlcInt i=0; i < calendar->getNumDays(); i++)
  {
    Day* day= (Day*)calendar->getDayFromIndex(i);
    if (indexInWeek == day->getNumberInWeek())
    {
      Period period= day->getPeriod(*dayPeriod);
      IlcInt startTime= calendar->getTimeUnit(period.getDateFrom());
      IlcInt endTime= calendar->getTimeUnit(period.getDateTo());

      for (IlcInt j=startTime; j < endTime; j++)
        _timeTable[j]= USED;
    }
  }
}



void 
ClinicClient::setUpCitasPlanificadas()
{
	WeekIndexedCalendar* calendar= _form->getCalendar();
  const IlcInt NOT_USED= 0;
	Period* period;

	for(IlcInt i=0; i < getCurrentClinicClientBusy(); i++)
		{
			period = getClinicClientBusy(i)->getPeriod();
      IlcInt startTime= calendar->getTimeUnit(period->getDateFrom());
      IlcInt endTime= calendar->getTimeUnit(period->getDateTo());

      for (IlcInt j=startTime; j < endTime; j++)
        _timeTable[j]= NOT_USED;
		}
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Actualiza la capacidad del cliente.          */
/*                                                                */
/* AUTOR:    Salvador Pe�alver  (ILOG)                            */
/*                                                                */
/* FECHA:    16-10-97                                             */
/*                                                                */
/******************************************************************
*/
void ClinicClient::setUpCapacity()
{
  const IlcInt NOT_USED= 0;
  IlcInt i=0;

  while (i < _timeTable.getSize())
  {
    if (_timeTable[i] == NOT_USED)
    {
      IlcInt firstIndex= i;
      while (i < _timeTable.getSize() && _timeTable[i] == NOT_USED)
      {
        i++;
      }
      
      IlcInt lastIndex;
      if (i < _timeTable.getSize())
        lastIndex= i;
      else
        lastIndex= i-1;

      Period period(_form->getCalendar()->getDate(firstIndex), 
                    _form->getCalendar()->getDate(lastIndex));    
      _capacity.setCapacityMax(period, 0);
    }
    else
      i++;
  }
}



void 
ClinicClient::addClinicClientBusy(ClinicClientBusy* clinicClientBusy)
{
	assert( _currentClinicClientBusy < _numClinicClientBusy );
	_clinicClientBusySet[_currentClinicClientBusy++] = clinicClientBusy;
}



ClinicClientBusy* 
ClinicClient::getClinicClientBusy(IlcInt Index)
{
	assert( Index < _currentClinicClientBusy);
	return( _clinicClientBusySet[Index] );
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
ClinicClient::~ClinicClient()
{    
	delete [] _properties;
	delete [] _clinicClientBusySet;
}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva caracter�stica a un cliente  */
/*                   de la Cl�nica                                */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
void 
ClinicClient::addClientProperty(ClientProperty* property) 
{
	assert( _currentProperty < _numProperties );
	_properties[_currentProperty++] = property;
}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Obtiene la Propiedad que corresponde         */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
ClientProperty* 
ClinicClient::getClientProperty(const IlcInt codigo)
{
  ClientProperty* ms= 0;

  for (IlcInt i=0; i < _currentProperty; i++)
    if (codigo == _properties[i]->getCodigo())
    {
      ms= _properties[i];
      break;
    }
  return ms;
}



////////////////////////////////////////////////////////////////////
// class ClinicClientPreferences
////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Coloca las preferencias para cada d�a de     */
/*                   la semana.                                   */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
ClinicClientPreferences::ClinicClientPreferences():
                         _dayPreferenceSize(7) 
{
  _dayPreferences= new DayPeriod*[_dayPreferenceSize];
  for (IlcInt i=0; i < _dayPreferenceSize; i++)
  {
    _dayPreferences[i]= 0;
  }
}



ClinicClientPreferences::~ClinicClientPreferences()
{
  for (IlcInt i=0; i < _dayPreferenceSize; i++)
    delete _dayPreferences[i];
  delete [] _dayPreferences;
}



void ClinicClientPreferences::setDayPreference(DayType dayInWeek, 
                                               DayPeriod* preference)
{
  IlcInt indexDay= (IlcInt)dayInWeek;
  assert(indexDay < _dayPreferenceSize);
  _dayPreferences[indexDay]= preference;
}



DayPeriod* ClinicClientPreferences::getDayPreference(DayType dayInWeek) const
{
  IlcInt indexDay= (IlcInt)dayInWeek;
  assert(indexDay < _dayPreferenceSize);
  return _dayPreferences[indexDay];
}


///////////////////////////////////////////////////////////////////////////////
// Class ClinicForm.
///////////////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Crea la solicitud.                           */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
ClinicForm::ClinicForm(FormType tipo,
											 Period period,
											 IlcInt interv,
											 IlcInt *diaSemana,
											 DayPeriod* dayPeriod,
											 IlcInt maxMedicalFactGroups):
            _numMedicalFactGroups(maxMedicalFactGroups), 
						_currentMedicalFactGroups(0),
						_period(period),_tipo(tipo),
						_dayPeriod(dayPeriod)
{

  _appScheduler= new ClinicApplicationScheduler(this,interv);
	_medicalFactGroups = new MedicalFactGroup*[_numMedicalFactGroups];
  _app= new ClinicApplication(this);
  _allSolutions= new ClinicAllSolutions(this);

	if( dayPeriod == NULL )
		{
			_dayPeriod = new DayPeriod(0,0,24,0);
		}

	if( diaSemana == NULL )
		{
		_diaSemana = IlcIntArray(_appScheduler->getManager(),7,1,1,1,1,1,1,1);
		}
	else
		{
		_diaSemana = IlcIntArray(_appScheduler->getManager(),7);
			for(IlcInt i=0; i < 7; i++)
			{
				_diaSemana[i] = diaSemana[i];
			}
		}

	SetFormPreferences();

}



WeekIndexedCalendar* ClinicForm::getCalendar() const
{ 
	return _appScheduler->getWeekCalendar(); 
}



void ClinicForm::setUpClinicClientPreferences()
{
  _app->setUpClinicClientPreferences(_preferences);
}


IlcInt
ClinicForm::getNumMedicalServiceFact()
{
  if ( getClinicApplication()->getCurrentMedicalServices() == 0 )
    return -2;

  return 1;
}

/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
ClinicForm::~ClinicForm()
{    
 	delete [] _medicalFactGroups;
  delete _app;
  delete _appScheduler;
}


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Construye las preferencias de la solicitud.  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
void
ClinicForm::SetFormPreferences()
{    
	ClinicClientPreferences* preferences= new ClinicClientPreferences;

	//Lunes.
	if(_diaSemana[0])
		preferences->setDayPreference(LUNES, _dayPeriod);

	//Martes.
	if(_diaSemana[1])
		preferences->setDayPreference(MARTES, _dayPeriod);

	//Miercoles.
	if(_diaSemana[2])
		preferences->setDayPreference(MIERCOLES, _dayPeriod);

	//Jueves.
	if(_diaSemana[3])
		preferences->setDayPreference(JUEVES, _dayPeriod);

	//Viernes.
	if(_diaSemana[4])
		preferences->setDayPreference(VIERNES, _dayPeriod);

	//S�bado.
	if(_diaSemana[5])
		preferences->setDayPreference(SABADO, _dayPeriod);

	//Domingo.
	if(_diaSemana[6])
		preferences->setDayPreference(DOMINGO, _dayPeriod);

	_preferences = preferences;

}


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva actuaci�n a la Solicitud.    */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
void 
ClinicForm::addMedicalFactGroup(MedicalFactGroup* medicalFactGroup) 
{
	if( _currentMedicalFactGroups < _numMedicalFactGroups )
	{
		_medicalFactGroups[_currentMedicalFactGroups++]= medicalFactGroup;
	}
}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Obtiene la (i) Actuaci�n a planificar.       */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
MedicalFactGroup* 
ClinicForm::getMedicalFactGroup(IlcInt i) const 
{ 
	assert( i < _currentMedicalFactGroups );

	return _medicalFactGroups[i]; 
}



IlcInt
ClinicForm::ChooseBestTime(MedicalServiceReserve* reserve)
{
	IntervalActivityCalendar act = reserve->getReserve();

	return(	act.getStartVariable().getValue() );
}



IlcInt 
ClinicForm::obtenerIndice(IlcInt indact, IlcInt indreserva)
{
	IlcInt indequ = 0;

	for (IlcInt i = 0; i < indact; i++)
	{
		MedicalFact* mf = getMedicalFactGroup(0)->getMedicalFact(i);
		for (IlcInt j = 0; j < mf->getNumReserves(); j++)
			indequ++;

	}

	MedicalFact* mf = getMedicalFactGroup(0)->getMedicalFact(indact);
	for(IlcInt k = 0; k < indreserva; k++)
		indequ++;

	return indequ;
}



IlcBool ClinicForm::solve(IlcInt sols) 
{ 
	MedicalFact* mfFirst = getMedicalFactGroup(0)->getMedicalFact(0);	
	MedicalServiceReserve* reserve;
	IlcBool flag = IlcTrue;
	IlcInt indEquivalente;
	IlcInt indexContador;
	IlcInt groupSize;
	IlcInt i, j, x;


  indexContador = getClinicApplicationScheduler()->getIndexContador();
  groupSize= getMedicalFactGroup(0)->getCurrentMedicalFacts();

	//Reseuelve la solicitud.
  if(_app->schedulerBooking(sols))
  {
	  printSolution();

	  getClinicAllSolutions()->addSolution();

	  reserve = mfFirst->getMedicalFactReserveRecPref();

	  if ( reserve->getMedicalService()->getTypeMedicalService() == ALTERNATIVE )
	  {
		for ( x = indact; x >= 0; x-- )
		{
			MedicalFact* mf= getMedicalFactGroup(0)->getMedicalFact(x);

			if (indact != x)
			{
				indreserva = mf->getNumReserves() - 1;
				indice = 0;
			}
			indact = x;

			for ( j = indreserva; j >= 0; j-- )
			{
				if (indreserva != j)
				{
					indice = 0;
				}

				indreserva = j;

				indEquivalente = obtenerIndice( indact, indreserva );
					
				for ( i = indice+1; i < contador[indEquivalente]; i++ )
				{
				
					if ( timeCita[indEquivalente][i] == timeCita[indEquivalente][0] )
					{
						if ( timeCita[indEquivalente][i] != 1000000 )
						{
							flag = IlcFalse;
							indice++;
							i = contador[indEquivalente];
							j = -1;
							x = -1;
						}
					}
					else if ( timeCita[indEquivalente][i] != 1000000 )
					{
						indexGoal++;
					}
				}
			}
		}

		if (flag)
		{
			IlcInt BestTime = ChooseBestTime(reserve);

			getClinicApplicationScheduler()->getManager().restart();
		
			indact = groupSize - 1;

			MedicalFact* mf= getMedicalFactGroup(0)->getMedicalFact(groupSize-1);
			indreserva = mf->getNumReserves() - 1;

			for (j=0; j < indexContador; j++)
			{
				for (i = 0; i < contador[j]; i++)
				{
					timeCita[j][i] = 1000000;
				}			
				contador[j] = 0;
			}

			indice = 0;
			indexGoal = 0;
		}
		else
		{
			for (j=indEquivalente+1; j < indexContador; j++)
			{
				contador[j] = 0;
			}
		}
	}
	else
	{
		for ( x = indact; x >= 0; x-- )
		{
			MedicalFact* mf= getMedicalFactGroup(0)->getMedicalFact(x);

			if (indact != x)
			{
				indreserva = mf->getNumReserves() - 1;
				indice = 0;
			}
			indact = x;

			for ( j = indreserva; j >= 0; j-- )
			{
				if (indreserva != j)
				{
					indice = 0;
				}

				indreserva = j;

				indEquivalente = obtenerIndice( indact, indreserva );
					
				for ( i = indice+1; i < contador[indEquivalente]; i++ )
				{				
					if ( timeCita[indEquivalente][i] == timeCita[indEquivalente][0] )
					{
						if ( timeCita[indEquivalente][i] != 1000000 )
						{
							flag = IlcFalse;
							indice++;
							i = contador[indEquivalente];
							j = -1;
							x = -1;
						}
					}
					else if ( timeCita[indEquivalente][i] != 1000000 )
					{
						indexGoal++;

					}
				}
			}
		}

		if (flag)
		{
			getClinicApplicationScheduler()->getManager().restart();
		
			getClinicApplicationScheduler()->getManager().add(
				reserve->getReserve().getStartVariable() > timeSimple );

			indact = groupSize - 1;

			MedicalFact* mf= getMedicalFactGroup(0)->getMedicalFact(groupSize-1);
			indreserva = mf->getNumReserves() - 1;

			for (j=0; j < indexContador; j++)
			{
				for (i = 0; i < contador[j]; i++)
				{
					timeCita[j][i] = 1000000;
				}			
				contador[j] = 0;
			}

			indice = 0;
			indexGoal = 0;

			flag = IlcFalse;
		}
		else
		{
			for (j=indEquivalente+1; j < indexContador; j++)
			{
				contador[j] = 0;
			}
		}

	}		

	return 1;

  }
  else
  {
    return 0;
  }
}


void 
ClinicForm::searchAlgorithm()
{
	IlcInt i, j;

	for (j=0; j < 200; j++)
	{
		for (i = 0; i < 20; i++)
		{
			timeCita[j][i] = 1000000;
			contador[i] = 0;
		}			
	}

	indact = getMedicalFactGroup(0)->getCurrentMedicalFacts() - 1;

	MedicalFact* mf = getMedicalFactGroup(0)->getMedicalFact(indact);
	indreserva = mf->getNumReserves() - 1;

	indice = 0;
	indexGoal = 0;

	flag = IlcFalse;

	_app->searchAlgorithm();
}


void ClinicForm::printSolution() const 
{ 
  _app->printOn((ofstream)_app->getTraceFile());

  for (IlcInt i=0; i < _currentMedicalFactGroups; i++)
    _medicalFactGroups[i]->printOn((ofstream)_app->getTraceFile());

  _appScheduler->printOn((ofstream)_app->getTraceFile());
}


////////////////////////////////////////////////////////////////////
// class ClinicApplication
////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa el Entorno de la Aplicaci�n.      */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
ClinicApplication::ClinicApplication(ClinicForm* form, 
                                     IlcInt maxMedicalServices,
                                     IlcInt maxClinicClients,
                                     IlcInt maxProperties):
										_numMedicalServices(maxMedicalServices),
                    _currentMedicalServices(0),
                    _numClinicClients(maxClinicClients),
                    _currentClinicClient(0),
                    _form(form)                    
{
	//Reserva espacio en memoria.
	_medicalServices= new MedicalService*[_numMedicalServices];
  _clinicClients= new ClinicClient*[_numClinicClients];  
  _properties= new PropertySet(maxProperties);
}



ClinicClient* ClinicApplication::findClinicClient(const IlcInt codClinicClient)
{
  ClinicClient* client= 0;

  for (IlcInt i=0; i < _currentClinicClient; i++)
  {
    if (codClinicClient == _clinicClients[i]->getCodClient())
    {
      client= _clinicClients[i];
      break;
    }
  }

  return client;
}



MedicalService* ClinicApplication::findMedicalService(const IlcInt codMedicalService,
                                                      const IlcInt codMedicalFact,
                                                      const IlcInt codDpto,
                                                      const IlcInt numPhase,
																											MedicalServiceType type)
{
  MedicalService* ms= 0;

  if ( type == SINGLE )
  { //Recurso simple.

    for (IlcInt i=0; i < _currentMedicalServices; i++)
    {
        if (codMedicalService == _medicalServices[i]->getCodRecurso() && 
				    _medicalServices[i]->getTypeMedicalService() == type )
        {
          ms= _medicalServices[i];
          break;
        }
      }
  }
  else
  { //Recurso compuesto.

    MedicalServiceAlternative*  msa;

    for (IlcInt i=0; i < _currentMedicalServices; i++)
    {
      if ( _medicalServices[i]->getTypeMedicalService() == ALTERNATIVE )
      {
        msa = (MedicalServiceAlternative*)_medicalServices[i];

        if (codMedicalService == msa->getCodRecurso() && 
            codMedicalFact == msa->getCodMedicalFact() &&
            codDpto == msa->getCodDpto() &&
            numPhase == msa->getNumPhase() )
          {
            ms= _medicalServices[i];
            break;
          }
        }
      }
  }

  return ms;
}


void ClinicApplication::setUpClinicClientPreferences(ClinicClientPreferences* 
                                                     preferences)
{
  for (IlcInt i=0; i < _currentClinicClient; i++)
  {
    const IlcInt size= preferences->getDayPreferencesSize();
    for (IlcInt j=0; j < size; j++)
    {
      DayType dayType= (DayType)j;
      DayPeriod* period= preferences->getDayPreference(dayType);
      if (period)
      {
        _clinicClients[i]->registerPreference(dayType, period);
      }
    }

		_clinicClients[i]->setUpCitasPlanificadas();

    _clinicClients[i]->setUpCapacity();

  }
}


  
WeekIndexedCalendar* ClinicApplication::getCalendar() const 
{
	return _form->getClinicApplicationScheduler()->getWeekCalendar(); 
}


void ClinicApplication::addClinicClient(ClinicClient* client)
{
  assert(_currentClinicClient < _numClinicClients);

  _clinicClients[_currentClinicClient++]= client;
}



void ClinicApplication::printOn(ostream& os) const
{

  for (IlcInt i=0; i < _currentMedicalServices; i++)
  {
    _medicalServices[i]->printOn(os);
  }
}



void ClinicApplication::addProperty(Property* property)
{
  _properties->addProperty(property);
}


  
Property* ClinicApplication::getProperty(IlcInt codigo) const
{
  return _properties->getProperty(codigo);
}


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
ClinicApplication::~ClinicApplication() 
{ 
	delete [] _medicalServices;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Obtiene el Servicio Medico con Nombre(name). */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalService* 
ClinicApplication::getMedicalService(const char* name) const
{
  MedicalService* ms= 0;

  for (IlcInt i=0; i < _currentMedicalServices; i++)
    if (strcmp(name, _medicalServices[i]->getName()) == 0)
    {
      ms= _medicalServices[i];
      break;
    }
  return ms;
}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Obtiene el Servicio Medico con c�digo        */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalService* 
ClinicApplication::getCodMedicalService(IlcInt cod) const
{
  MedicalService* ms= 0;

  for (IlcInt i=0; i < _currentMedicalServices; i++)
    if ((cod == _medicalServices[i]->getCodRecurso()) && 
        (_medicalServices[i]->getTypeMedicalService() == SINGLE)
       )
    {
      ms= _medicalServices[i];
      break;
    }
  return ms;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Obtiene el (i) Servicio Medico.              */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalService* 
ClinicApplication::getMedicalService(IlcInt medicalService) const 
{ 
	if( medicalService < _currentMedicalServices )
	{
		return(_medicalServices[medicalService]); 
	}
	else
	{
		return( NULL );
	};
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: A�ade un Servicio Medico.                    */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
void
ClinicApplication::addMedicalService(MedicalService* medicalService) 
{ 
	if( _currentMedicalServices < _numMedicalServices )
	{
		_medicalServices[_currentMedicalServices++] = medicalService;
	}
}

/*
***********************************************************************/
/*                   SYSECA BILBAO                                    */
/*                                                                    */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                         */
/*                                                                    */
/* NOMBRE:   Clinic.cpp                                               */
/*                                                                    */
/* DESCRIP. FUNCION: Realiza la asignaci�n de Recursos y Actuaciones  */
/*                                                                    */
/* AUTOR:    Roberto Murga                                            */
/*                                                                    */
/* FECHA:    17-9-1997                                                */
/*                                                                    */
/**********************************************************************
*/
IlcBool 
ClinicApplication::schedulerBooking(IlcInt sols)
{
	return _form->getClinicApplicationScheduler()->schedulerBooking(sols);
}



void
ClinicApplication::searchAlgorithm()
{
  _form->getClinicApplicationScheduler()->searchAlgorithm();
}


////////////////////////////////////////////////////////////////////
// class ClinicApplicationScheduler
////////////////////////////////////////////////////////////////////
/*
***********************************************************************/
/*                   SYSECA BILBAO                                    */
/*                                                                    */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                         */
/*                                                                    */
/* NOMBRE:   Clinic.cpp                                               */
/*                                                                    */
/* DESCRIP. FUNCION: Realiza la asignaci�n de Recursos y Actuaciones  */
/*                                                                    */
/* AUTOR:    Roberto Murga                                            */
/*                                                                    */
/* FECHA:    17-9-1997                                                */
/*                                                                    */
/**********************************************************************
*/

ILCGOAL1(DefineProblem, ClinicForm*, form)
{
  form->setUpClinicClientPreferences();

  IlcGoal goal= form->getClinicApplicationScheduler()->getAssignGoal();
  return goal;
}


ClinicApplicationScheduler::ClinicApplicationScheduler(ClinicForm* form, IlcInt interv):
														                   _manager(IlcEdit), _form(form)
{ 
  getManager().useHeap(IlcTrue);

  _indexContador = 0;

  _schedule = SchedulerWeekCalendar(_manager, form->getPeriod(), interv);
}


void ClinicApplicationScheduler::defineProblem()
{
  _manager.add(DefineProblem(_manager, _form));
}


IlcInt OrderChooseActivity(IlcIntervalActivity act, ClinicForm* form,
						   MedicalServiceReserve* reserve)
{
	IlcInt indexReserve = reserve->getIndexReserve();

    IlcInt minutos = ((24*60)/(form->getCalendar()->getTimeStepPack()))-1;

		if(act.getStartVariable().isBound() &&
				act.getStartVariable().getValue() == 
					(IlcInt)form->getCalendar()->getTimeUnit(form->getPeriod().getDateTo())+minutos
			)
		{

			timeCita[indexReserve][contador[indexReserve]-1] = 
								act.getStartVariable().getValue();
			form->getClinicApplicationScheduler()->getManager().fail();

		}

		if (!act.getStartVariable().isBound())
			return 0;

	return -1;
}



IlcInt ChooseActivity(IlcActivityArray acts, ClinicForm* form)
{
    IlcInt minutos = ((24*60)/(form->getCalendar()->getTimeStepPack()))-1;

	for (IlcInt i=0; i < acts.getSize(); i++)
	{
		if(acts[i].getStartVariable().isBound() &&
				acts[i].getStartVariable().getValue() == 
				(IlcInt)form->getCalendar()->getTimeUnit(form->getPeriod().getDateTo())+
				minutos
			)
		{

			timeSimple = acts[i].getStartVariable().getValue();

			form->getClinicApplicationScheduler()->getManager().fail();

		}

		if (!acts[i].getStartVariable().isBound())
			return i;
	}

	return -1;
}


ILCGOAL3(OrderInstantiate, ClinicForm*, form,
						   IlcIntVar, var,
						   MedicalServiceReserve*, reserve)
{

	IlcInt indexReserve = reserve->getIndexReserve();

	if(var.isBound())
	{
		timeCita[indexReserve][contador[indexReserve]-1] = var.getValue();
			
		return IlcGoalFail(getManager());
	}
	else
	{
		IlcInt min = var.getMin();

		timeCita[indexReserve][contador[indexReserve]-1] = min;

		return IlcGoalFail(getManager());
	}
}


ILCGOAL3(ClinicaInstantiate, ClinicForm*, form,
							 IlcIntVar, var,
							 IlcInt, time)
{
	if(var.isBound())
	{		
		if ( flag == IlcFalse )
			timeSimple = var.getValue();

		return 0;
	}
	else
	{
		IlcInt min = var.getMin();
		var.setMax(min + 1);
		IlcInt max = var.getMax();

		if ( flag == IlcTrue )
		{
			timeSimple = min;
		}
			return IlcOr(var == min,
						 var != min);
	}
}


ILCGOAL3(SetOrderTimeActivities, IlcIntervalActivity, act, ClinicForm*, form,
		 MedicalServiceReserve*, reserve)
{
	IlcInt actIndex = OrderChooseActivity(act, form, reserve);


	return IlcAnd(OrderInstantiate(form->getClinicApplicationScheduler()->getManager(),
										form,
										act.getStartVariable(),
										reserve),
				   this);

}


void ChooseTimeRef(IlcActivityArray acts, ClinicForm* form)
{
	IlcInt bestTime = 1000000;

	for (IlcInt i=0; i < acts.getSize(); i++)
	{
		if(	acts[i].getStartVariable().getValue() < bestTime )
			bestTime = acts[i].getStartVariable().getValue();

	}

	TimeRef = bestTime;
}


ILCGOAL2(SetStartTimeActivities, IlcActivityArray, acts, ClinicForm*, form)
{

	IlcInt actIndex = ChooseActivity(acts, form);

	if (actIndex == -1)
  {

	ChooseTimeRef( acts, form );

	return 0;
  };

	return IlcAnd(ClinicaInstantiate(form->getClinicApplicationScheduler()->getManager(),
										form,
										acts[actIndex].getStartVariable(),
										1),
				   this);

}


ILCGOAL2(SetTimesOrder, ClinicForm*, form, MedicalServiceReserve*, reserve)
{
	IntervalActivityCalendar act;

	for (IlcInt i=0; i < form->getCurrentMedicalFactGroups(); i++)
	{
		IlcInt groupSize= form->getMedicalFactGroup(i)->getCurrentMedicalFacts();
		for (IlcInt j=0; j < groupSize; j++)
		{
			MedicalFact* mf= form->getMedicalFactGroup(i)->getMedicalFact(j);

			IlcInt numPhases= mf->getCurrentPhase();
			for (IlcInt k=0; k < numPhases; k++)
			{
				IlcInt numReserves= mf->getPhase(k)->getCurrentService();

				for (IlcInt w=0; w < numReserves; w++)
				{
					if ( reserve == mf->getPhase(k)->getMedicalServiceReserve(w) )
						act = mf->getPhase(k)->getMedicalServiceReserve(w)->getReserve(); 
				}
			}
		}
	}

	return SetOrderTimeActivities(getManager(), act, form, reserve);
}



ILCGOAL1(SetTimes, ClinicForm*, form)
{
	const IlcInt maxArray= 1000;
	IlcInt index= 0;
	IlcActivityArray acts(getManager(), maxArray);
	MedicalServiceReserve*	reserve;

	if ( indexGoal > 0 )
	{
		indexGoal--;
		return IlcGoalFail(getManager());

	}

	for (IlcInt i=0; i < form->getCurrentMedicalFactGroups(); i++)
	{
		IlcInt groupSize= form->getMedicalFactGroup(i)->getCurrentMedicalFacts();
		for (IlcInt j=0; j < groupSize; j++)
		{
			MedicalFact* mf= form->getMedicalFactGroup(i)->getMedicalFact(j);

			IlcInt numPhases= mf->getCurrentPhase();
			for (IlcInt k=0; k < numPhases; k++)
			{
				IlcInt numReserves= mf->getPhase(k)->getCurrentService();
				acts[index++]= mf->getPhase(k)->getReserve();

				for (IlcInt w=0; w < numReserves; w++)
				{
					acts[index++]= mf->getPhase(k)->getMedicalServiceReserve(w)->getReserve(); 

					if ( flag == IlcFalse )
					{
						reserve = mf->getPhase(k)->getMedicalServiceReserve(w);

						if ( reserve->getMedicalService()->getTypeMedicalService() == 
																	SINGLE )
							flag = IlcTrue;
					}

				}
			}
		}
	}

	IlcActivityArray actsToGenerate(getManager(), index);

	for (i=0; i < index; i++)
	{
		actsToGenerate[i]= acts[i];
	}

	return SetStartTimeActivities(getManager(), actsToGenerate, form);
}


void ClinicApplicationScheduler::searchAlgorithm()
{
  _manager.add(SetTimes(_manager, _form));
}



IlcBool ClinicApplicationScheduler::schedulerBooking(IlcInt sols)
{
  return _manager.nextSolution();

}



/*
*****************************************************************/
/*                   SYSECA BILBAO                              */
/*                                                              */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                   */
/*                                                              */
/* NOMBRE:   Clinic.cpp                                         */
/*                                                              */
/* DESCRIP. FUNCION: Imprime la Soluci�n al Problema Propuesto  */
/*                                                              */
/* AUTOR:    Roberto Murga                                      */
/*                                                              */
/* FECHA:    17-9-1997                                          */
/*                                                              */
/****************************************************************
*/
void ClinicApplicationScheduler::printOn(ostream& os) const
{
  _manager.printInformation(os);
}



/*
*****************************************************************/
/*                   SYSECA BILBAO                              */
/*                                                              */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                   */
/*                                                              */
/* NOMBRE:   Clinic.cpp                                         */
/*                                                              */
/* DESCRIP. FUNCION: Si el Recurso Posee suficiente Capacidad   */
/*                   como para ser seleccionado, comprueba las  */
/*                   dem�s Restricciones.                       */
/*                                                              */
/* AUTOR:    Roberto Murga                                      */
/*                                                              */
/* FECHA:    17-9-1997                                          */
/*                                                              */
/****************************************************************
*/
ILCGOAL2(AssignMedicalService, MedicalServiceReserve*, reserve,
                               MedicalServiceSingle*, mss)
{ 
  mss->setUpReserve(reserve);

  return 0;
}


ILCGOAL2(OrderMedicalService, MedicalServiceReserve*, reserve,
                              MedicalServiceSingle*, mss)
{
  IlcInt indexReserve = reserve->getIndexReserve();
  contador[indexReserve]++;

  mss->setUpReserve(reserve);

  return (SetTimesOrder(getManager(), mss->getClinicApplication()->getClinicForm(),
		  reserve));

}


ILCGOAL3(AssignAlternative, MedicalServiceAlternative*, msa, 
                            MedicalServiceReserve*, reserve, 
                            IlcIntVar, medicalServiceIndex)
{
  IlcInt index = medicalServiceIndex.getMin() % msa->getNumMedicalServices();
  IlcInt indaux = medicalServiceIndex.getMin();

  if ( indaux < msa->getNumMedicalServices() )
  {
	return IlcOr(OrderMedicalService(getManager(), reserve,
                                    msa->getMedicalServiceSingle(indaux)),
               IlcAnd(medicalServiceIndex != indaux, this));
  }

  if ( indaux == msa->getNumMedicalServices() )
  {
	  msa->setUpOrder(reserve);
  }
	
  if (indaux == medicalServiceIndex.getMax())
  {
    return (AssignMedicalService(getManager(), reserve,
                                msa->getMedicalServiceSingle(index)));
  }

	return IlcOr(AssignMedicalService(getManager(), reserve,
                                    msa->getMedicalServiceSingle(index)),
               IlcAnd(medicalServiceIndex != indaux, this));

}



void ClinicApplicationScheduler::assignReserve(MedicalServiceReserve* reserve, 
                                               MedicalServiceAlternative* msa)
{ 
  IlcIntVar medicalServiceIndex(_manager, 0, (msa->getNumMedicalServices()*2)-1);

	reserve->setIndexReserve();
	_indexContador++;

	reserve->getPhase()->getMedicalFact()->increaseNumReserves();

  if (_assignGoal.getImpl())
    _assignGoal= IlcAnd(_assignGoal, 
                        AssignAlternative(_manager, msa, reserve, 
                                          medicalServiceIndex));
  else
    _assignGoal= AssignAlternative(_manager, msa, reserve, 
                                   medicalServiceIndex);
}


IlcAnySet ClinicApplicationScheduler::createAnySet(IlcInt numMedicalFactTypes)
{ 
  IlcAnyArray arrayValues(_manager, numMedicalFactTypes);

	for (IlcInt i=0; i < arrayValues.getSize(); i++)
		arrayValues[i]= (IlcAny)i;

  return IlcAnySet(arrayValues); 
}



ILCDEMON2(SolveIterations, IntervalActivityCalendar, act, MedicalFact*, mf)
{
    IlcInt numIterac = mf->getMedicalFactIterations()->getCurrentMedicalFactAllIterations();
    for(IlcInt i = 0; i < numIterac; i++)
    {
      MedicalFact* act2 = mf->getMedicalFactGroup()->
                              getActuacion(mf->getMedicalFactIterations()->
                                                getIteration(i)->getCodigo()
                                           );

      if(act2->getAction().getStartVariable().isBound() == IlcFalse)
      {
        IlcInt dias = mf->getMedicalFactIterations()->getIteration(i)->getDias();
        IlcInt horas = mf->getMedicalFactIterations()->getIteration(i)->getHoras();
        IlcInt minutos = mf->getMedicalFactIterations()->getIteration(i)->getMinutos();

        Duration d(dias, horas, minutos);

        IlcInt numTimeUnits= d.getDuration(MINUTES,
                                           mf->getMedicalFactGroup()->
                                           getClinicForm()->getCalendar()->
                                           getTimeStepPack());

        mf->getMedicalFactGroup()->getClinicForm()->
            getClinicApplicationScheduler()->startsAfterEnd(act,act2->getAction(),
                                                            numTimeUnits);
      }
    }
}



ILCDEMON4(TestProperties, IntervalActivityCalendar, act, 
                          MedicalServiceSingle*, mss,
                          MedicalFact*, mf,
						  MedicalServiceReserve*, reserve)
{
  IlcInt index= act.getStartVariable().getValue();

  MedicalServiceCalendarBand* band= mss->getMedicalServiceCalendarBand(index);
  
  ClinicClient* client= (ClinicClient*)act.getObject();


	if(band == NULL )
		getManager().fail();

	if (!band->getPropertyConditionTree()->testClientProperties(client))
			getManager().fail();

  band->getVarQuota().removeValue(band->getVarQuota().getMax());

	MedicalFactTypesBand* typesBand = band->getMedicalFactTypesBand();

	IlcInt pos= typesBand->getIndexActuacion(mf->getState());

	if(pos >= 0)
	{
		typesBand->setQuota(pos);

	}	
}



void ClinicApplicationScheduler::whenValue(IntervalActivityCalendar act, 
                                           MedicalServiceSingle* mss,
                                           MedicalFact* mf,
										   MedicalServiceReserve* reserve)
{
  act.getStartVariable().whenValue(TestProperties(_manager, act, mss, mf, reserve));
}


void ClinicApplicationScheduler::whenValue(IntervalActivityCalendar act, 
                                           MedicalFact* mf)
{
  act.getStartVariable().whenValue(SolveIterations(_manager, act, mf));
}


void ClinicApplicationScheduler::setUpTimetable(DiscreteResourceCalendar res, 
                                                Period period,
                                                IlcUInt timeStepPack, 
                                                IlcInt capacity)
{
  IlcInt tMin= _schedule.getCalendar()->getTimeUnit(period.getDateFrom());
  IlcInt tMax= _schedule.getCalendar()->getTimeUnit(period.getDateTo());
  
  IlcUInt numTimeUnits= tMax - tMin;

  IlcDiscreteResource r= (IlcDiscreteResource)res;

  if (capacity == 0)
  {
    r.setCapacityMax(tMin, tMax, capacity);
    return;
  }

  if (timeStepPack == numTimeUnits)
  {
    r.setCapacityMax(tMin, tMin+1, capacity);

    r.setCapacityMax(tMin+1, tMax, 0);
  }
  else

	  if (timeStepPack > 1)
    {
      IlcInt numSteps= numTimeUnits / timeStepPack;
      IlcInt timeFrom= tMin;

      for (IlcInt i=0; i < numSteps; i++)
      {
        r.setCapacityMax(timeFrom+(timeStepPack*i), timeFrom+(timeStepPack*i)+1, capacity);

        r.setCapacityMax(timeFrom+(timeStepPack*i)+1,timeFrom+(timeStepPack*(i+1)) , 0);
      }
    }
    else
    {
      r.setCapacityMax(tMin, tMax, capacity);
    }
}




void ClinicApplicationScheduler::setUpTimetable(DiscreteResourceCalendar res, 
                                                Period period)
{
  IlcInt tMin= _schedule.getCalendar()->getTimeUnit(period.getDateFrom());
  IlcInt tMax= _schedule.getCalendar()->getTimeUnit(period.getDateTo());

	IlcInt capacity;

  IlcDiscreteResource r= (IlcDiscreteResource)res;

	for(IlcInt i=tMin; i < tMax; i++)
		{
			capacity = r.getCapacityMax(i);

			if(capacity > 0)
			{
				r.setCapacityMax(i, i+1, capacity-1);
			}
		}
}