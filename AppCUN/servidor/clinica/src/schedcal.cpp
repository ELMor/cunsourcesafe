#include <schedcal.h>
#include <string.h>
#include <assert.h>

/////////////////////////////////////////////////////////////////////////////////////
// CLASS IntervalActivityCalendar
/////////////////////////////////////////////////////////////////////////////////////
IntervalActivityCalendar::IntervalActivityCalendar(SchedulerCalendar scheduler, 
		                                               Duration duration,
													                         IlcBool boundDuration)  
{
  IlcInt d= duration.getDuration(scheduler.getCalendar()->getTimeStep(),
                                 scheduler.getCalendar()->getTimeStepPack());


	if (boundDuration)
	{
		IlcIntervalActivity act(scheduler, d);
		_impl= act.getImpl();
	}
	else
	{
    IlcIntVar dVar(IlcIntArray(scheduler.getManager(), 2, 0, d)); 
		IlcIntervalActivity act(scheduler, dVar);
		_impl= act.getImpl();
	}
}



void IntervalActivityCalendar::printOn(ostream& os) const
{
}



Duration IntervalActivityCalendar::getDuration() const 
{
  assert(getDurationVariable().isBound());

  IlcInt numTimeUnits= getDurationVariable().getValue();
  
  IlcInt timeStep= getCalendar()->getTimeStep();
  IlcInt timeStepPack= getCalendar()->getTimeStepPack();

  IlcInt days, hours, minutes;
  switch (timeStep)
  {
  case DAYS: hours=0; minutes=0; 
             days= numTimeUnits * timeStepPack;
             break;

  case HOURS: minutes=0; 
              hours= (numTimeUnits * timeStepPack) %24;
              days= (numTimeUnits * timeStepPack) / 24;
              break;

  case MINUTES: days= (numTimeUnits * timeStepPack) / 1440;
                hours= (numTimeUnits*timeStepPack - (days*1440)) / 60;
                minutes= (numTimeUnits*timeStepPack - (days*1440)) % 60;
                break;

  default: assert(0); break;
  }

  return Duration(days, hours, minutes);
}



ostream& operator <<(ostream& os, const IntervalActivityCalendar& act)
{
	act.printOn(os);
	return os;
}



/////////////////////////////////////////////////////////////////////////////////////
// CLASS DiscreteResourceCalendar
/////////////////////////////////////////////////////////////////////////////////////
IlcConstraint DiscreteResourceCalendar::makeTimetableConstraint(
                                        Period period,
                                        IlcUInt timeStepPack,
                                        IlcInt capacity)
{
  IlcInt tMin= getCalendar()->getTimeUnit(period.getDateFrom());
  IlcInt tMax= getCalendar()->getTimeUnit(period.getDateTo());
  
  assert((tMax - tMin) % timeStepPack == 0);
  
  return IlcDiscreteResource::makeTimetableConstraint(tMin, tMax, 
                                                      timeStepPack,
                                                      capacity); 
}
