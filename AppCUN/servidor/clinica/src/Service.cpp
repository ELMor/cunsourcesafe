#include <service.h>
#include <clinic.h>
#include <string.h>


////////////////////////////////////////////////////////////////////////
// class InternalTimeTable
////////////////////////////////////////////////////////////////////////
InternalTimetable::InternalTimetable(MedicalServiceSingle* mss, 
                                     IlcInt size): _mss(mss)
{
  ClinicApplicationScheduler* appScheduler= _mss->getClinicApplication()->
                                                 getClinicForm()->
                                            getClinicApplicationScheduler();

  _capacityUsed= appScheduler->createIntArray(size);

  const IlcInt NOT_USED= 0;
  for (IlcInt i=0; i < size; i++)
    _capacityUsed[i]= NOT_USED;

  _medicalServiceCalendarBand= appScheduler->createAnyArray(size);

  for (i=0; i < size; i++)
    _medicalServiceCalendarBand[i]= (IlcAny)0;
}



IlcBool InternalTimetable::isUsed(IlcInt index) const
{
  assert(index < _capacityUsed.getSize());

  const IlcInt USED= 1;
  return (_capacityUsed[index] == USED);
}
  


void InternalTimetable::setUsed(IlcInt index, 
                                MedicalServiceCalendarBand* band,
                                IlcInt inic,IlcInt comienzo)
{
  assert(index < _capacityUsed.getSize());
  const IlcInt USED= 1;
	MedicalServiceCalendarBand* band_aux = 0;

  _capacityUsed[index]= USED;

	if( band != NULL )
		{
      if( inic == 1 )
      {
			  band_aux = new MedicalServiceCalendarBand(band);
      }
      else
      {
        band_aux = (MedicalServiceCalendarBand*)_medicalServiceCalendarBand[comienzo];
      }
		}

  _medicalServiceCalendarBand[index]= (IlcAny)band_aux;
}



MedicalServiceCalendarBand* InternalTimetable::getMedicalServiceCalendarBand(IlcInt i) const
{ 
  assert(i < _medicalServiceCalendarBand.getSize()); 

  return (MedicalServiceCalendarBand*)_medicalServiceCalendarBand[i];
}



////////////////////////////////////////////////////////////////////////
// class MedicalServiceSingleBusy
////////////////////////////////////////////////////////////////////////
MedicalServiceSingleBusy::MedicalServiceSingleBusy(Period period,
																									 IlcInt actuacion):_period(period),
																																		 _actuacion(actuacion)
{

}



////////////////////////////////////////////////////////////////////////
// class MedicalService
////////////////////////////////////////////////////////////////////////
/*
********************************************************************/
/*                   SYSECA BILBAO                                 */
/*                                                                 */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                      */
/*                                                                 */
/* NOMBRE:   Service.cpp                                           */
/*                                                                 */
/* DESCRIP. FUNCION: Crea un Servicio que ofrece la Cl�nica.       */
/*                                                                 */
/* AUTOR:    Roberto Murga                                         */
/*                                                                 */
/* FECHA:    17-9-1997                                             */
/*                                                                 */
/*******************************************************************
*/	
MedicalService::MedicalService(ClinicApplication* app, const char* name, 
                               IlcInt codRecurso ):
															  _app(app), _codRecurso(codRecurso)
                  
{
  strcpy(_name,name);
}



const IndexedCalendar* MedicalService::getCalendar() const
{ 
  return _app->getClinicForm()->getClinicApplicationScheduler()->
               getSchedule().getCalendar(); 
}
                

////////////////////////////////////////////////////////////////////////
// class MedicalServiceSingle
////////////////////////////////////////////////////////////////////////
const IlcInt MAX_CAPACITY= 100;
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Construye un Servicio M�dico Simple.         */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalServiceSingle::MedicalServiceSingle(ClinicApplication* app, 
                                           const char* name, 
                                           ReserveType reserveType,
																					 IlcInt codRecurso,
																					 IlcInt capacidad,
                                           IlcInt maxProfiles,
																					 IlcInt maxCalendars,
																					 IlcInt maxBusy):
                      MedicalService(app,name,codRecurso), 
                      _numProfiles(maxProfiles),
                      _currentProfile(0),
                      _reserveType(reserveType),
                      _defaultCalendar(0),
											_numCalendars(maxCalendars),
											_currentCalendar(0),
											_capacidad(capacidad),
											_numMedicalServiceSingleBusy(maxBusy),
											_currentMedicalServiceSingleBusy(0),
                      _currentIntervalStates(0)
{
  ClinicApplicationScheduler* appScheduler= app->getClinicForm()->
                                            getClinicApplicationScheduler();
  _capacity= DiscreteResourceCalendar(appScheduler->getSchedule(), 
                                      _capacidad, IlcTrue);
  _capacity.setName(name);
  _capacity.setObject(this);

  _conditionsTree= new PropertyConditionTree();

  _possibleMedicalFacts = appScheduler->createIlcStateResource();

  _profiles= new MedicalServiceCalendarProfile*[_numProfiles];
	_calendars= new WeekIndexedCalendar*[_numCalendars];
	_medicalServiceSingleBusySet= new MedicalServiceSingleBusy*[maxBusy];

  IlcInt size= getCalendar()->getTimeMax();
  _internalTimetable= InternalTimetable(this, size);

  _numIntervalStates = size;
  _medicalServiceSingleIntervalStates= new IntervalStates*[_numIntervalStates];

  app->addMedicalService(this);

}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalServiceSingle::~MedicalServiceSingle() 
{ 
}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Crea una Reserva para un Servicio M�dico.    */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
void MedicalServiceSingle::requiresCapacity(MedicalServiceReserve* reserve)
{
  ClinicApplicationScheduler* appScheduler= _app->getClinicForm()->
                                            getClinicApplicationScheduler();

	IlcInt capacityRequired = reserve->getCapacity();
  appScheduler->requires(reserve->getReserve(), _capacity, capacityRequired);
}



void MedicalServiceSingle::requiresMedicalFact(MedicalServiceReserve* reserve)
{

  ClinicApplicationScheduler* appScheduler= _app->getClinicForm()->
                                            getClinicApplicationScheduler();

  IntervalActivityCalendar actStates= appScheduler->
  copyIntervalActivityCalendar(reserve->getPhase()->getReserve());
  
  appScheduler->startsAtStart(actStates, reserve->getReserve());

  appScheduler->requires(actStates, _possibleMedicalFacts, 
              reserve->getPhase()->getMedicalFact()->getState());
 
}



void MedicalServiceSingle::setDurationReserve(MedicalServiceReserve* reserve)
{
  IlcInt timeStepPack= getCalendar()->getTimeStepPack();

  Duration phaseDuration= reserve->getPhase()->getReserve().getDuration();

  switch (_reserveType)
  {
  case QUOTA:

  case FIX_INTERVAL: reserve->getReserve().
                              setDuration(Duration(0, 0, timeStepPack));
       break;
  
  case SEQUENTIAL_INTERVAL: reserve->getReserve().
                                     setDuration(phaseDuration);
       break; 

  default: assert(0); break;
  }
}



void MedicalServiceSingle::setUpReserve(MedicalServiceReserve* reserve)
{
  reserve->getReserve().setName(_name);

  setDurationReserve(reserve);

  requiresCapacity(reserve);
  requiresMedicalFact(reserve);  
  testClientProperties(reserve); 
}



MedicalServiceReserve* MedicalServiceSingle::createReserve(IlcInt delay, Phase* phase,
                                                           Duration duration,
																													 IlcInt capacity)
{
  MedicalServiceReserve* reserve= new MedicalServiceReserve(delay, 
		                                                        this, phase,
                                                            duration, capacity); 
  
  ClinicClient* client= phase->getMedicalFact()->getClinicClient();
	reserve->getReserve().setObject(client);
	 
  setUpReserve(reserve);
  return reserve;
}



void MedicalServiceSingle::addCalendar(WeekIndexedCalendar* calendar)
{
  assert(_currentCalendar < _numCalendars);

  _calendars[_currentCalendar++]= calendar;
}



void MedicalServiceSingle::addIntervalState(IlcAnySet states, IlcInt from, IlcInt to)
{
  assert( _currentIntervalStates < _numIntervalStates );

  IntervalStates* _interval = new IntervalStates(states,from,to);

  _medicalServiceSingleIntervalStates[_currentIntervalStates++]= _interval;
}



MedicalServiceSingleBusy* 
MedicalServiceSingle::getBusy(IlcInt Index)
{
	assert( Index < _currentMedicalServiceSingleBusy );

  return(_medicalServiceSingleBusySet[Index]);
}



IntervalStates* 
MedicalServiceSingle::getIntervalStates(IlcInt i)
{
  assert( i < _currentIntervalStates );

  return(_medicalServiceSingleIntervalStates[i]);
}



void MedicalServiceSingle::addBusy(MedicalServiceSingleBusy* _medicalServiceSingleBusy)
{
  assert(_currentMedicalServiceSingleBusy < _numMedicalServiceSingleBusy);

  _medicalServiceSingleBusySet[_currentMedicalServiceSingleBusy++]= _medicalServiceSingleBusy;
}



void MedicalServiceSingle::setUpIntervalStates()
{
  IlcInt i = 0;
  IlcInt j = 0;
  IlcInt _num = 0;

  for(i=0; i < getCurrentIntervalStates(); i++)
  {
    _num = _num + getIntervalStates(i)->getStates().getSize();
  }

  if ( _num == 0 )
  {
    IlcStateResource _auxilar(_capacity.getSchedule(),
                              IlcAnySet(_capacity.getManager(),
                              IlcAnyArray(_capacity.getManager(),1))
                              );
    _possibleMedicalFacts = _auxilar;

    return;
  }

  IlcAnyArray _allStates(_capacity.getManager(),_num);

  for(i=0; i < getCurrentIntervalStates(); i++)
  {
    for(IlcAnySetIterator iter(getIntervalStates(i)->getStates()); iter.ok(); ++iter)
    {
      _allStates[j++] = *iter;
    }
  }

  IlcAnySet _generalStates(_capacity.getManager(),_allStates);

  IlcStateResource _auxilar(_capacity.getSchedule(),_generalStates);

  _possibleMedicalFacts = _auxilar;

	for(i=0; i < getCurrentIntervalStates(); i++)
	{	
    _possibleMedicalFacts.setPossibleStates(getIntervalStates(i)->getFrom(), 
                                            getIntervalStates(i)->getTo(), 
                                            getIntervalStates(i)->getStates()
                                            );
  }
}



void MedicalServiceSingle::setUpBusy()
{
	IlcInt timeStepPack= 1;
	MedicalServiceCalendarBand* band;
	MedicalFactTypesBand* typesBand;
	IlcInt pos=0;

	for(IlcInt i=0; i < getCurrentMedicalServiceSingleBusy(); i++)
		{		
      _app->getClinicForm()->getClinicApplicationScheduler()->
                             setUpTimetable(_capacity, 
                                            getBusy(i)->getPeriod()
																						);

			IlcInt tMin= _app->getCalendar()->getTimeUnit(getBusy(i)->getPeriod().getDateFrom());
			IlcInt tMax= _app->getCalendar()->getTimeUnit(getBusy(i)->getPeriod().getDateTo());

			band = _internalTimetable.getMedicalServiceCalendarBand(tMin);
	
			if(band != NULL)
			{
        band->getVarQuota().removeValue(band->getVarQuota().getMax());

        
				typesBand = band->getMedicalFactTypesBand();

				pos= typesBand->getIndexActuacion(getBusy(i)->getActuacion());

				if(pos >= 0)
				{
					typesBand->setQuota(pos);

				}
			}
		}
}



void MedicalServiceSingle::setUpCalendar()
{
  IlcInt i = 0;

	const IndexedCalendar* calendar= getCalendar();

  _defaultCalendar= new WeekIndexedCalendar(calendar->getPeriod(), 
		                                        calendar->getTimeStep(),
		                                        calendar->getTimeStepPack());

	IlcInt numDays= calendar->getNumDays();

	for (i=0; i < numDays; i++)
	{
		const Day* day= calendar->getDayFromIndex(i);
		_defaultCalendar->setHoliday(day->getDate(), IlcTrue);
  }


  for (i=0; i < _currentCalendar; i++)
  {
    for (IlcInt j=0; j < _calendars[i]->getNumDays(); j++)
    {
      const Day* day= _calendars[i]->getDayFromIndex(j);

      if (day->isHoliday())
      {
        _defaultCalendar->setHoliday(day->getDate(), IlcTrue);
      }
			else 
      {
				_defaultCalendar->setHoliday(day->getDate(), IlcFalse);
      }
    }
  }

}



void MedicalServiceSingle::addProfile(MedicalServiceCalendarProfile* p)
{
  assert(_currentProfile < _numProfiles);

  _profiles[_currentProfile++]= p;
}


void MedicalServiceSingle::registerBand(Period validity, DayType dt,
                                        DayPeriod dayPeriod,
                                        IlcInt timeStepPack, 
                                        IlcInt capacity,
                                        MedicalServiceCalendarBand* band,
										Date* dateBand)
{
  IlcInt dayFromIndex= _defaultCalendar->getIndexedDay(validity.getDateFrom());
	IlcInt numDays= _defaultCalendar->getNumDays();
  IlcUInt indexInWeek= (IlcUInt)dt;
	IlcInt dayToIndex;

	if( dateBand == NULL )
		{
			dayToIndex= _defaultCalendar->getIndexedDay(validity.getDateTo());
		}
	else
		{
			dayToIndex= _defaultCalendar->getIndexedDay(*dateBand);
		}
 
  for (IlcInt i=dayFromIndex; i < dayToIndex; i++)
  {
    Day* day= (Day*)_defaultCalendar->getDayFromIndex(i);
    if (!day->isHoliday())
    {
      if (indexInWeek == day->getNumberInWeek())
      {  
        Period period= day->getPeriod(dayPeriod);

        _app->getClinicForm()->getClinicApplicationScheduler()->
                               setUpTimetable(_capacity, 
                                              period,
                                              timeStepPack, 
                                              capacity);

        registerMedicalFactTypes(period, band->getMedicalFactTypesBand()->
                                               getMedicalFactTypes());

        internalRegister(period, band);
      }
    }
  }
}



void MedicalServiceSingle::registerMedicalFactTypes(Period period,
                                                    IlcAnySet bandStates)
{
  IlcInt timeUnitMin= _defaultCalendar->getTimeUnit(period.getDateFrom());
  IlcInt timeUnitMax= _defaultCalendar->getTimeUnit(period.getDateTo());

  ClinicApplicationScheduler* appScheduler= _app->getClinicForm()->
                                            getClinicApplicationScheduler();

  addIntervalState(bandStates, timeUnitMin, timeUnitMax);

}



void MedicalServiceSingle::internalRegister(Period period,
                                            MedicalServiceCalendarBand* band)
{
  IlcInt timeUnitMin= _defaultCalendar->getTimeUnit(period.getDateFrom());
  IlcInt timeUnitMax= _defaultCalendar->getTimeUnit(period.getDateTo());

  for (IlcInt i=timeUnitMin; i < timeUnitMax; i++)
  {
    if( i == timeUnitMin )
    {
      _internalTimetable.setUsed(i, band);
    }
    else
    {
      _internalTimetable.setUsed(i, band,0,timeUnitMin);
    };
  }
}


void MedicalServiceSingle::setFechaDispo()
{
  IlcInt Index = 0;

  if (_currentProfile > 0 )
  {
	for(IlcInt i=0; i < _currentProfile; i++)
	{
		if(_profiles[i]->getValidity().getDateFrom() < 
				_profiles[Index]->getValidity().getDateFrom())
			Index = i;
	}

	_fechaDispo = _profiles[Index]->getValidity().getDateFrom();
	
  }
  else
	_fechaDispo = getClinicApplication()->getClinicForm()->getPeriod().getDateFrom();
}


void MedicalServiceSingle::setMinTotales()
{
  IlcInt Index = 0;
  for(IlcInt i=0; i < _currentMedicalServiceSingleBusy; i++)
  {
    Index += _medicalServiceSingleBusySet[i]->getPeriod().numMinutos();
  }

  _minTotales = Index;

}


void MedicalServiceSingle::setUpProfiles()
{
  IlcInt i= 0;

  while (i < _internalTimetable.getSize())
  {
    if (!_internalTimetable.isUsed(i))
    {
      IlcInt firstIndex= i;
      while (i < _internalTimetable.getSize() && 
             !_internalTimetable.isUsed(i))
      {
        i++;
      }
      
      IlcInt lastIndex;
      if (i < _internalTimetable.getSize())
        lastIndex= i;
      else
        lastIndex= i-1;

      Period period(_defaultCalendar->getDate(firstIndex), 
                    _defaultCalendar->getDate(lastIndex));
      
      IlcInt timeStepPack= 1;

      _app->getClinicForm()->getClinicApplicationScheduler()->
                             setUpTimetable(_capacity, 
                                            period,
                                            timeStepPack, 
                                            0);

      internalRegister(period);
    }
    else
      i++;
  }

  setFechaDispo();

}



static void PrintTimeTables(DiscreteResourceCalendar res, 
                            IlcInt timeMin, const IndexedCalendar* calendar, 
                            ostream& os)
{
}



void MedicalServiceSingle::testClientProperties(MedicalServiceReserve* reserve)
{
	ClinicApplicationScheduler* appScheduler= _app->getClinicForm()->
                                            getClinicApplicationScheduler();

  ClinicClient* clinicClient= reserve->getPhase()->getMedicalFact()->getClinicClient();

  if (!_conditionsTree->testClientProperties(clinicClient))
    appScheduler->fail();

  appScheduler->whenValue(reserve->getReserve(), this,
                          reserve->getPhase()->getMedicalFact(), reserve
                          );
}




void MedicalServiceSingle::printOn(ostream& os) const
{
}




////////////////////////////////////////////////////////////////////////
// class MedicalServiceAlternative
////////////////////////////////////////////////////////////////////////
/*
*************************************************************/
/*                   SYSECA BILBAO                          */
/*                                                          */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA               */
/*                                                          */
/* NOMBRE:   Service.cpp                                    */
/*                                                          */
/* DESCRIP. FUNCION: Crea un Servicio M�dico Alternativo.   */
/*                                                          */
/* AUTOR:    Roberto Murga                                  */
/*                                                          */
/* FECHA:    17-9-1997                                      */
/*                                                          */
/************************************************************
*/
MedicalServiceAlternative::MedicalServiceAlternative(ClinicApplication* app,
                                                     const char* name,
								                                     IlcInt numServices,
																										 IlcInt codRecurso,
                                                     IlcInt codMedicalFact,
                                                     IlcInt codDpto,
                                                     IlcInt numPhase): 
                           MedicalService(app, name, codRecurso), 
								           _numServices(numServices), 
									         _currentService(0),
                           _codMedicalFact(codMedicalFact),
                           _codDpto(codDpto),
                           _numPhase(numPhase)
{

  _services= new MedicalServiceSingle*[_numServices];

  app->addMedicalService(this);

}



MedicalServiceSingle* MedicalServiceAlternative::getMedicalServiceSingle(IlcInt index) const
{
  assert (index < _numServices);

  return _services[index];
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalServiceAlternative::~MedicalServiceAlternative() 
{ 
	delete [] _services;

}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade un nuevo Servicio M�dico Simple al     */
/*                   Servicio M�dico Alternativo.                 */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
void 
MedicalServiceAlternative::addMedicalService(MedicalServiceSingle* ms)
{ 
	if( _currentService < _numServices )
	{
		_services[_currentService++]= ms;
	}
}	


extern IlcInt contador[20];
extern IlcInt timeCita[200][20];


void 
MedicalServiceAlternative::setUpOrder(MedicalServiceReserve* reserve)
{
  MedicalServiceSingle* _services_aux;
  IlcInt				timeCita_aux;

  IlcInt indexReserve = reserve->getIndexReserve();

  IlcInt k = 0;
  IlcInt j = 0;
  IlcInt i = 0;


  for( i=0; i < _numServices - 1; i++)
  {
    k = i;

    for( j=i+1; j < _numServices; j++)
    {

      if( timeCita[indexReserve][j] < timeCita[indexReserve][k] )
        k = j;
    }


    _services_aux = _services[i];
    _services[i] = _services[k];
    _services[k] = _services_aux;

    timeCita_aux = timeCita[indexReserve][i];
    timeCita[indexReserve][i] = timeCita[indexReserve][k];
    timeCita[indexReserve][k] = timeCita_aux;

  }



}



/*
*********************************************************************/
/*                   SYSECA BILBAO                                  */
/*                                                                  */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                       */
/*                                                                  */
/* NOMBRE:   Service.cpp                                            */
/*                                                                  */
/* DESCRIP. FUNCION: Obtiene un nuevo Servicio M�dico Simple        */
/*                   que forma parte del Servio M�dico Alternativo. */
/*                                                                  */
/* AUTOR:    Roberto Murga                                          */
/*                                                                  */
/* FECHA:    17-9-1997                                              */
/*                                                                  */
/********************************************************************
*/
MedicalServiceSingle* 
MedicalServiceAlternative::getMedicalServiceSingle(const char* name) const
{
	for (IlcInt i=0; i < _currentService; i++)
	{
		if(strcmp(_services[i]->getName(),name) == 0)
		{
			return(_services[i]);
		}
	}

	return(NULL);
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Crea una Reserva para un Servicio M�dico.    */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalServiceReserve* 
MedicalServiceAlternative::createReserve(IlcInt delay, Phase* phase,
                                         Duration duration,
										 IlcInt capacity)
{
  MedicalServiceReserve* reserve= new MedicalServiceReserve(delay, 
															this, phase,
                                                            duration, capacity);

  reserve->getReserve().setName(_name);

	reserve->getReserve().setObject(phase->getMedicalFact()->getClinicClient());
  
	_app->getClinicForm()->getClinicApplicationScheduler()->
        assignReserve(reserve, this);

  return reserve;
}



////////////////////////////////////////////////////////////////////////
// class MedicalServiceReserve
////////////////////////////////////////////////////////////////////////
/*
****************************************************************/
/*                   SYSECA BILBAO                             */
/*                                                             */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                  */
/*                                                             */
/* NOMBRE:   Service.cpp                                       */
/*                                                             */
/* DESCRIP. FUNCION: Crea una Reserva para un Servicio M�dico. */
/*                   en una determinada fase.                  */
/*                                                             */
/* AUTOR:    Roberto Murga                                     */
/*                                                             */
/* FECHA:    17-9-1997                                         */
/*                                                             */
/***************************************************************
*/
MedicalServiceReserve::MedicalServiceReserve(IlcInt delay, 											
											                       MedicalService* ms,
											                       Phase* phase,
                                             Duration duration,
																						 IlcInt capacity): 
                       _phase(phase),_service(ms), _delay(delay),_capacity(capacity),
                       _duration(duration)
{
  ClinicApplicationScheduler* appScheduler= ms->getClinicApplication()->
                                                getClinicForm()->
                                                getClinicApplicationScheduler();

  _reserve= appScheduler->createActivity();
  
  appScheduler->conditionalPrecedence(phase->getReserve(), 
                                      _reserve, _delay);
}



void MedicalServiceReserve::printOn(ostream& os) const
{
  os << _reserve << endl;
}



void MedicalServiceReserve::setIndexReserve()
{
  _indexReserve = getMedicalService()->getClinicApplication()->
				  getClinicForm()->getClinicApplicationScheduler()->getIndexContador();
}