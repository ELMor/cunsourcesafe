#include <servical.h>
#include <clinic.h>

////////////////////////////////////////////////////////////////////////////////////
// CLASS MedicalFactTypesBand
/////////////////////////////////////////////////////////////////////////////////////
MedicalFactTypesBand::MedicalFactTypesBand(IlcAnySet medicalFactTypes,
																					 IlcAnyArray medicalFacts,
																					 Duration* maxTimes,
																					 IlcIntArray maxQuotas,
																					 IndexedCalendar* calendar):
                       _toScheduler(IlcTrue),
                       _medicalFactTypes(medicalFactTypes),
											 _calendar(calendar)
{
	_medicalFacts = medicalFacts;

	_maxTimes = maxTimes;

  _maxQuotas = IlcIntVarArray(_medicalFacts.getManager(),maxQuotas.getSize());

  for(IlcInt i=0; i < maxQuotas.getSize(); i++)
  {
    _maxQuotas[i] = IlcIntVar(_medicalFacts.getManager(),0,maxQuotas[i]);
  }

	_durations = IlcIntArray(_medicalFacts.getManager(),_medicalFacts.getSize());

	for(IlcInt pos=0; pos < _medicalFacts.getSize(); pos++)
		{
			_durations[pos]=(IlcUInt)_calendar->getTimeUnit(_maxTimes[pos]);
		}
}



MedicalFactTypesBand::MedicalFactTypesBand(const MedicalFactTypesBand* band)
{
	_calendar = band->_calendar;
	_medicalFactTypes = band->_medicalFactTypes;
	_medicalFacts = band->_medicalFacts;
  _toScheduler = band->_toScheduler;

  _maxQuotas = IlcIntVarArray(_medicalFacts.getManager(),
                              _medicalFacts.getSize());

  for(IlcInt i=0; i < _medicalFacts.getSize(); i++)
  {
    _maxQuotas[i] = IlcIntVar(_medicalFacts.getManager(),0,band->_maxQuotas[i].getMax());
  }

  _maxTimes = band->_maxTimes;
	_durations = band->_durations;
}



void 
MedicalFactTypesBand::setDuration(IlcInt pos,Period periodo)
{
	IlcUInt _unidadesCita;
	IlcUInt _unidadesActu;

	_unidadesCita = _calendar->getTimeUnit(_calendar->getDuration(periodo));

	_unidadesActu = _durations[pos];

	_durations[pos] = _unidadesActu - _unidadesCita;

}


void 
MedicalFactTypesBand::setQuota(IlcInt Index)
{
  _maxQuotas[Index].removeValue(_maxQuotas[Index].getMax());
}



IlcInt 
MedicalFactTypesBand::getIndexActuacion(IlcInt Index)
{
	IlcInt _tamanio = _medicalFacts.getSize();
	IlcInt pos;

	for(pos=0; pos < _tamanio; pos++)
		{
			if((IlcInt)_medicalFacts[pos] == Index)
			{
				return(pos);
			}
		}

	return(-1);
}


////////////////////////////////////////////////////////////////////////////////////
// CLASS MedicalServiceCalendarBand
/////////////////////////////////////////////////////////////////////////////////////
MedicalServiceCalendarBand::MedicalServiceCalendarBand(
                            MedicalServiceCalendarProfile* profile,
                            DayPeriod dayPeriod, 
                            IlcInt quota,
                            MedicalFactTypesBand* mfTypesBand,
                            PropertyConditionTree* tree,
                            ReserveType reserveType):
_profile(profile), _dayPeriod(dayPeriod), 
_medicalFactTypesBand(mfTypesBand), _tree(tree), 
_reserveType(reserveType)

{
  const IlcInt WEEK= 7;
  _weekDays= new IlcBool[WEEK];

  for (IlcInt i=0; i < WEEK; i++)
    _weekDays[i]= IlcFalse;

  _quota = IlcIntVar(mfTypesBand->getMedicalFacts().getManager(),0,quota);

}



MedicalServiceCalendarBand::MedicalServiceCalendarBand(const MedicalServiceCalendarBand* band)
{
	MedicalFactTypesBand* _medicalFactTypesBand_aux;
	_medicalFactTypesBand_aux = new MedicalFactTypesBand(band->_medicalFactTypesBand);

	_profile = band->_profile;
	_dayPeriod = band->_dayPeriod;

  _quota = IlcIntVar(band->getMedicalFactTypesBand()->getMedicalFacts().getManager(),
                     band->_quota.getMin(),
                     band->_quota.getMax()
                     );

  band->getMedicalFactTypesBand()->getMedicalFacts().getManager().add(_quota >= 0);

	_medicalFactTypesBand = _medicalFactTypesBand_aux;
	_tree = band->_tree;
	_weekDays = band->_weekDays;
}



void MedicalServiceCalendarBand::setWeekDay(DayType d)
{
  _weekDays[(IlcInt)d]= IlcTrue;
}


/////////////////////////////////////////////////////////////////////////////////////
// CLASS MedicalServiceCalendarProfile
/////////////////////////////////////////////////////////////////////////////////////
MedicalServiceCalendarProfile::MedicalServiceCalendarProfile(
                               MedicalServiceSingle* mss, 
                               Period validity,
                               IlcInt numBands):
                               _numBands(numBands),
                               _currentBand(0),
                               _mss(mss), 
                               _validity(validity)
{
  _bands= new MedicalServiceCalendarBand*[_numBands];
}



void MedicalServiceCalendarProfile::addBand(MedicalServiceCalendarBand* band,
																						Date* dateBand,
																						IlcInt interCita)
{
  assert(_currentBand < _numBands);

  _bands[_currentBand++]= band;
 
  DayPeriod dayPeriod= band->getDayPeriod();

  Duration d=  dayPeriod.getDuration();

  IlcInt numTimeUnits= d.getDuration(MINUTES, 
                                     _mss->getCalendar()->
                                           getTimeStepPack());
  IlcInt timeStepPack;
  IlcInt capacity;

  switch (band->getReserveType())
  {
    case QUOTA: timeStepPack= numTimeUnits;
                capacity= band->getQuota();
                break;

    case FIX_INTERVAL:
					   timeStepPack=	interCita;
                       capacity= 1;
                       break;
  
    case SEQUENTIAL_INTERVAL: timeStepPack= 1;
                              capacity= 1;
                              break; 

    default: assert(0); break;
  }
  
  const IlcInt WEEK= 7;

  for (IlcInt i=0; i < WEEK; i++)
  {
    if (band->getWeekDays(i))
    {
      if (band->isToScheduler())
        _mss->registerBand(_validity, (DayType)i, dayPeriod, 
                           timeStepPack, capacity, band, dateBand);
      else
        _mss->registerBand(_validity, (DayType)i, dayPeriod,
                           timeStepPack, 0, band, dateBand);
    }
  }
}


