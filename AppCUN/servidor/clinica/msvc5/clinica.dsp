# Microsoft Developer Studio Project File - Name="clinica" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=clinica - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "clinica.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "clinica.mak" CFG="clinica - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "clinica - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "clinica - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""$/AppCUN/Servidor", ADFAAAAA"
# PROP Scc_LocalPath "..\.."
CPP=cl.exe

!IF  "$(CFG)" == "clinica - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\clinica.lib"

!ELSEIF  "$(CFG)" == "clinica - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "clinica_"
# PROP BASE Intermediate_Dir "clinica_"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /Od /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\clinica.lib"
# ADD LIB32 /nologo /out:"..\clinica.lib"

!ENDIF 

# Begin Target

# Name "clinica - Win32 Release"
# Name "clinica - Win32 Debug"
# Begin Source File

SOURCE=..\src\Checkup.cpp
# End Source File
# Begin Source File

SOURCE=..\include\Checkup.h
# End Source File
# Begin Source File

SOURCE=..\src\Clinic.cpp
# End Source File
# Begin Source File

SOURCE=..\include\Clinic.h
# End Source File
# Begin Source File

SOURCE=..\src\property.cpp
# End Source File
# Begin Source File

SOURCE=..\include\property.h
# End Source File
# Begin Source File

SOURCE=..\src\schedcal.cpp
# End Source File
# Begin Source File

SOURCE=..\include\schedcal.h
# End Source File
# Begin Source File

SOURCE=..\src\servical.cpp
# End Source File
# Begin Source File

SOURCE=..\include\servical.h
# End Source File
# Begin Source File

SOURCE=..\src\Service.cpp
# End Source File
# Begin Source File

SOURCE=..\include\Service.h
# End Source File
# End Target
# End Project
