#ifndef __CLINIC_H_
#define __CLINIC_H_

#include /**/<ilsched/schedule.h>
#include /**/<ilsched/intact.h>
#include <service.h>
#include <checkup.h>
#include <property.h>
#include <fstream.h>
#include <ostream.h>

#include <schedcal.h>
#include <weekindx.h>

////////////////////////////////////////////////////////////////
// Forward Declaration
////////////////////////////////////////////////////////////////
class ClinicForm;
class ClinicApplication;
class ClinicApplicationScheduler;
class ClinicAllSolutions;

/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicSolution            */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class ClinicSolution
{  
  Date* _solutions;
  IlcInt _numDates;
  IlcInt _currentDates;
  IlcInt* _recs;
  IlcInt* _recspref;

  ClinicForm* _form;
  ClinicAllSolutions* _allSolutions;

public:
  ClinicSolution(ClinicForm* form,ClinicAllSolutions* allSolutions);
  ~ClinicSolution();

  Date getDateSolution(IlcInt act,IlcInt phase,IlcInt rec=-1);
  IlcInt getRecursoSolution(IlcInt act,IlcInt phase,IlcInt rec);
  IlcInt getRecPrefSolution(IlcInt act);
  ClinicForm* getClinicForm() const { return _form;}
  ClinicAllSolutions* getAllSolutions() const { return _allSolutions;}
};


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicAllSolutions        */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class ClinicAllSolutions
{
  ClinicSolution** _solutions;
  IlcInt _numSolutions;
  IlcInt _currentSolutions;

  ClinicForm* _form;

public:
	ClinicAllSolutions(ClinicForm* form);
  ~ClinicAllSolutions();

  void addSolution();
  ClinicSolution* getSolution(IlcInt Index);
  ClinicForm* getClinicForm() const { return _form;}
};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicClientBusy          */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class ClinicClientBusy
{
public:
	Period* _period;
	Period* getPeriod() const { return _period; }

	ClinicClientBusy(Period* period);
};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicClient              */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class ClinicClient
{
protected:
	const char* _name;
	IlcInt _codClient;

	ClinicForm* _form;

	ClientProperty** _properties;
	IlcInt _numProperties;
	IlcInt _currentProperty;

	ClinicClientBusy** _clinicClientBusySet;
	IlcInt _numClinicClientBusy;
	IlcInt _currentClinicClientBusy;

	UnaryResourceCalendar _capacity;
  IlcIntArray _timeTable;
	
public:
	ClinicClient(ClinicForm* form, const char* name, 
							 IlcInt codClient,
               IlcInt maxProperties= 32,
							 IlcInt clinicClientBusy= 100);
	~ClinicClient();

  ClinicForm* getClinicForm() const { return _form; }
  const char* getName() const { return _name; }
	IlcInt getCodClient() const { return _codClient; }

	IlcInt getNumProperties() const { return _numProperties; }
	void addClientProperty(ClientProperty* property);
	ClientProperty* getClientProperty(const IlcInt codigo);

	IlcInt getNumClinicClientBusy() const { return _numClinicClientBusy;}
	IlcInt getCurrentClinicClientBusy() const { return _currentClinicClientBusy;}
	void addClinicClientBusy(ClinicClientBusy* clinicClientBusy);
	ClinicClientBusy* getClinicClientBusy(IlcInt Index);

	UnaryResourceCalendar getCapacity() const { return _capacity; }
  void setUpCapacity();
	void setUpCitasPlanificadas();
  void registerPreference(DayType dayType, DayPeriod* dayPeriod);

	void printOn(ostream& os) const;
};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicClientPreferences   */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class ClinicClientPreferences
{
  DayPeriod** _dayPreferences;
  const IlcInt _dayPreferenceSize;
public:
  ClinicClientPreferences();
	~ClinicClientPreferences();
  void setDayPreference(DayType dayInWeek, DayPeriod* preference);
  DayPeriod* getDayPreference(DayType dayInWeek) const;
  const IlcInt getDayPreferencesSize() const { return _dayPreferenceSize; }
};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicForm                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
enum FormType { NORMAL, CONFIRMACION, LISTA_ESPERA };
class ClinicForm
{
  MedicalFactGroup** _medicalFactGroups;
  IlcInt _numMedicalFactGroups;
  IlcInt _currentMedicalFactGroups;

  Period _period;
	DayPeriod* _dayPeriod;
	IlcIntArray _diaSemana;

  ClinicApplication* _app;
  ClinicAllSolutions* _allSolutions;

  ClinicApplicationScheduler* _appScheduler;
  ClinicClientPreferences* _preferences;

	FormType _tipo;
  ofstream _traceFile;

protected:
	void SetFormPreferences();

public:
  ClinicForm(FormType tipo, Period period, IlcInt interv=15,
						 IlcInt *diaSemana = 0,
						 DayPeriod* dayPeriod = 0,
						 IlcInt maxMedicalFactGroups= 32 );
             
  ~ClinicForm();

  MedicalFactGroup* getMedicalFactGroup(IlcInt i) const;
  void addMedicalFactGroup(MedicalFactGroup* medicalFactGroup); 
	IlcInt getCurrentMedicalFactGroups() const { return _currentMedicalFactGroups; }

  void setUpClinicClientPreferences();
	ClinicClientPreferences* getFormPreferences() { return _preferences; }
  
  Period getPeriod() const { return _period; }
	DayPeriod* getDayPeriod() const { return _dayPeriod; }

  ClinicApplication* getClinicApplication() const { return _app; }
  ClinicAllSolutions* getClinicAllSolutions() const { return _allSolutions; }
  ClinicApplicationScheduler* getClinicApplicationScheduler() const 
  { return _appScheduler;}

  IlcInt getNumPhasesMedicalFact() const { return getMedicalFactGroup(0)->getNumPhasesMedicalFact(); }
  IlcInt getNumMedicalServiceFact();
                                           
  WeekIndexedCalendar* getCalendar() const;

	FormType getTipo() const { return _tipo; }
  ofstream getTraceFile() const { return _traceFile; }

  IlcBool solve(IlcInt sols = 1);
  void searchAlgorithm();
  void printSolution() const;
};


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase ClinicApplication.        */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class ClinicApplication
{
  MedicalService** _medicalServices;
  IlcInt _numMedicalServices;
  IlcInt _currentMedicalServices;

  ClinicClient** _clinicClients;
  IlcInt _numClinicClients;
  IlcInt _currentClinicClient;

  PropertySet* _properties;
  ClinicForm* _form;

public:
  ClinicApplication(ClinicForm* form, IlcInt maxMedicalServices= 256,
                                      IlcInt maxClinicClients= 32,
                                      IlcInt maxProperties= 32);
                    
  ~ClinicApplication();

  MedicalService* getMedicalService(const char* name) const;
  MedicalService* getMedicalService(IlcInt medicalService) const;
  MedicalService* getCodMedicalService(IlcInt cod) const;

  IlcInt getNumMedicalServices() const { return _numMedicalServices; }
  IlcInt getCurrentMedicalServices() const { return _currentMedicalServices; }
  void addMedicalService(MedicalService* medicalService);

  IlcBool schedulerBooking(IlcInt sols = 1);
  void searchAlgorithm();

  ClinicForm* getClinicForm() const { return _form; }
  void setUpClinicClientPreferences(ClinicClientPreferences* preferences);


  void addProperty(Property* property);
  Property* getProperty(IlcInt codigo) const;
  
  ofstream getTraceFile() const { return getClinicForm()->getTraceFile(); }

  void addClinicClient(ClinicClient* client);
  ClinicClient* findClinicClient(const IlcInt codClient);
  MedicalService* findMedicalService(const IlcInt codMedicalService,
                                     const IlcInt codMedicalFact = 0,
                                     const IlcInt codDpto = 0,
                                     const IlcInt numPhase = 0,
																		 MedicalServiceType type=SINGLE); 

  WeekIndexedCalendar* getCalendar() const;
  void printOn(ostream& os) const;
};


//---------------------------------------------------------------
// Class SchedulerCalendar
//---------------------------------------------------------------
class SchedulerWeekCalendar: public SchedulerCalendar
{
public:
	SchedulerWeekCalendar(): SchedulerCalendar() {}
  SchedulerWeekCalendar(IlcManager manager, Period period,IlcInt interv = 15):
                        SchedulerCalendar(new (manager.getHeap()) 
                        SchedulerCalendarI(manager, 
                        new WeekIndexedCalendar(period, MINUTES, interv)))
  {}
  
  WeekIndexedCalendar* getWeekCalendar() const 
	{ return (WeekIndexedCalendar*) 
           (((SchedulerCalendarI*)getImpl())->getCalendar()); }
};



/*
*********************************************************************/
/*                   SYSECA BILBAO                                  */
/*                                                                  */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                       */
/*                                                                  */
/* NOMBRE:   Clinic.h                                               */
/*                                                                  */
/* DESCRIPCION:                                                     */
/*           Implementación de la clase ClinicApplicationScheduler  */
/*                                                                  */
/* AUTOR:    Roberto Murga                                          */
/*                                                                  */
/* FECHA:    17-9-1997                                              */
/*                                                                  */
/********************************************************************
*/
class ClinicApplicationScheduler
{
	IlcManager _manager;
	SchedulerWeekCalendar _schedule;

  ClinicForm* _form;
  IlcGoal _assignGoal;

public:
  ClinicApplicationScheduler(ClinicForm* form, IlcInt interv=15);

  ~ClinicApplicationScheduler() { _manager.end(); }

  SchedulerCalendar getSchedule() const { return _schedule; }
  IlcManager getManager() const { return _manager; }

  void requires(IntervalActivityCalendar act, DiscreteResourceCalendar res, 
                IlcInt capacity)
  { _manager.add(act.requires(res, capacity)); }

  void requires(IntervalActivityCalendar act, IlcStateResource res, 
                IlcInt state)
  { _manager.add(act.requiresState(res, (IlcAny)state)); }

  void requires(IntervalActivityCalendar act, UnaryResourceCalendar res, IlcInt capacity = 1)
  { _manager.add(act.requires(res, capacity)); }

  void requires(IntervalActivityCalendar act, IlcAltResSet res, IlcInt capacity);
	
  void requires(IntervalActivityCalendar act, IlcStateResource res, IlcAny state)
	{ _manager.add(act.requiresState(res, state)); }


  void precedence(IntervalActivityCalendar act1, IntervalActivityCalendar act2,
									IlcInt delay=0)
  { _manager.add(act2.startsAtEnd(act1,delay)); }


  void startsAfterEnd(IntervalActivityCalendar act1, IntervalActivityCalendar act2,
									    IlcInt delay=0)
  { _manager.add(act2.startsAfterEnd(act1,delay)); }


  void conditionalPrecedence(IntervalActivityCalendar act1, 
                             IntervalActivityCalendar act2,
		                         IlcInt delay)
  { _manager.add(act2.startsAtStart(act1, delay)); }

	void precedenceConditional(IntervalActivityCalendar act1, 
                             IntervalActivityCalendar act2,
                             IlcInt delay = 0)
  { _manager.add(act2.startsAfterStart(act1,delay)); }
  
  void covers(IntervalActivityCalendar treatment, 
              IlcActivityArray reserves)
  { _manager.add(treatment.covers(reserves)); }

  void startsAtStart(IntervalActivityCalendar after, 
                     IntervalActivityCalendar before)
  { _manager.add(after.startsAtStart(before)); }

  IntervalActivityCalendar createActivity()
  { return IntervalActivityCalendar(_schedule); }

  IntervalActivityCalendar createActivity(Duration d)
  { return IntervalActivityCalendar(_schedule, d); }

  IlcActivityArray createActivityArray(IlcInt size)
  { return IlcActivityArray(_manager, size); }

  IlcIntArray createIntArray(IlcInt size)
  { return IlcIntArray(_manager, size); }

  IlcAnyArray createAnyArray(IlcInt size)
  { return IlcAnyArray(_manager, size); }

  IlcAltResSet createAltResSet(IlcInt numServices)
  { return IlcAltResSet(_schedule, numServices); }

  IlcStateResource createIlcStateResource(IlcAnySet states)
  { return IlcStateResource(_schedule, states); }

  IlcStateResource createIlcStateResource()
  { return IlcStateResource(); }

  IlcAnySet createAnySet(IlcInt numMedicalFactTypes);
  IlcAnySet createAnySet(IlcAnyArray medicalFactTypes)
  { return IlcAnySet(medicalFactTypes); }

  IlcIntVar createIntVar(IlcInt min, IlcInt max)
  { return IlcIntVar(_manager, min, max); }


  void defineProblem();
  IlcBool schedulerBooking(IlcInt sols = 1);
  void searchAlgorithm();
  
  void setWeekDayHoliday(DayType dayInWeek, IlcBool isHoliday)
  { _schedule.getWeekCalendar()->setWeekDayHoliday(dayInWeek, isHoliday); }

  void makeTimetableConstraint(DiscreteResourceCalendar res, 
                               Period p,
                               IlcUInt timeStepPack, IlcInt capacity)
  { _manager.add(res.makeTimetableConstraint(p,timeStepPack, capacity)); }


  void setUpTimetable(DiscreteResourceCalendar res, 
                      Period period,
                      IlcUInt timeStepPack, 
											IlcInt capacity);

	void setUpTimetable(DiscreteResourceCalendar res, 
                      Period period);

  void assignReserve(MedicalServiceReserve* reserve, 
                     MedicalServiceAlternative* msa);
  
  IlcGoal getAssignGoal() const { return _assignGoal; }
  IntervalActivityCalendar copyIntervalActivityCalendar(
                           IntervalActivityCalendar act)
  { return IntervalActivityCalendar(_schedule, act.getDuration()); }

  void fail() { _manager.fail(); }

  void whenValue(IntervalActivityCalendar act, MedicalServiceSingle* mss,MedicalFact* mf);
  void whenValue(IntervalActivityCalendar act, MedicalFact* mf);

  void addCst(IlcConstraint cst)
  { _manager.add(cst); }
    

  WeekIndexedCalendar* getWeekCalendar() const 
  { return _schedule.getWeekCalendar(); }
  
  void printOn(ostream& os) const;
  ClinicForm* getClinicForm() const { return _form; }

};



#endif // __CLINIC_H_