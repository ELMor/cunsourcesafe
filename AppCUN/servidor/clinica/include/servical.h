#ifndef __SERVICAL_H_
#define __SERVICAL_H_

#include <schedcal.h>
#include <property.h>

///////////////////////////////////////////////////////////////
// Forward declaration...
///////////////////////////////////////////////////////////////
class MedicalServiceSingle;
class MedicalServiceCalendarProfile;


///////////////////////////////////////////////////////////////
// class MedicalFactTypesBand
///////////////////////////////////////////////////////////////
class MedicalFactTypesBand
{
  IlcAnySet _medicalFactTypes;
	IlcAnyArray _medicalFacts;

  IlcBool _toScheduler;

  IlcIntVarArray _maxQuotas;
  Duration* _maxTimes;
  IlcIntArray _durations;

	IndexedCalendar* _calendar;
	
public:
	MedicalFactTypesBand(IlcAnySet medicalFactTypes,
											 IlcAnyArray medicalFacts,
                       Duration* maxTimes,
											 IlcIntArray maxQuotas,
											 IndexedCalendar* calendar);

	MedicalFactTypesBand(const MedicalFactTypesBand* band);

  IlcBool getToScheduler() const { return _toScheduler; }
  IlcAnySet getMedicalFactTypes() const { return _medicalFactTypes; }
	IlcAnyArray getMedicalFacts() const { return _medicalFacts; }

	IndexedCalendar* getCalendar() { return _calendar; }

	void setQuota(IlcInt Index);
	void setDuration(IlcInt pos,Period periodo);
	IlcInt getIndexActuacion(IlcInt Index);
};



/*
**************************************************************************/
/*                   SYSECA BILBAO                                       */
/*                                                                       */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                            */
/*                                                                       */
/* NOMBRE:   servical.h                                                  */
/*                                                                       */
/* DESCRIPCION: Implementación de la clase MedicalServiceCalendarBand    */
/*                                                                       */
/* AUTOR:    Roberto Murga                                               */
/*                                                                       */
/* FECHA:    14-10-1997                                                  */
/*                                                                       */
/*************************************************************************
*/
enum ReserveType { QUOTA, FIX_INTERVAL, SEQUENTIAL_INTERVAL };

class MedicalServiceCalendarBand
{
  MedicalServiceCalendarProfile* _profile;

  DayPeriod _dayPeriod;
  IlcIntVar _quota;
  IlcBool* _weekDays;

  MedicalFactTypesBand* _medicalFactTypesBand;
  PropertyConditionTree* _tree;

  ReserveType _reserveType;
public:
  MedicalServiceCalendarBand(MedicalServiceCalendarProfile* profile,
                             DayPeriod dayPeriod, 
                             IlcInt quota,
                             MedicalFactTypesBand* mfTypesBand,
                             PropertyConditionTree* tree,
                             ReserveType reserveType);

	MedicalServiceCalendarBand(const MedicalServiceCalendarBand* band);

  IlcInt getQuota() const { return _quota.getMax(); }
  IlcIntVar getVarQuota() { return _quota;}

  void setWeekDay(DayType d);
  IlcBool isToScheduler() const { return _medicalFactTypesBand->getToScheduler(); }
  DayPeriod getDayPeriod() const { return _dayPeriod; }

  IlcBool getWeekDays(IlcInt i) const { return _weekDays[i]; }
  MedicalFactTypesBand* getMedicalFactTypesBand() const { return _medicalFactTypesBand; }

  PropertyConditionTree* getPropertyConditionTree() const { return _tree; }

  ReserveType getReserveType() const { return _reserveType; }

};



/*
**************************************************************************/
/*                   SYSECA BILBAO                                       */
/*                                                                       */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                            */
/*                                                                       */
/* NOMBRE:   servical.h                                                  */
/*                                                                       */
/* DESCRIPCION:  Implementación de la clase MedicalServiceCalendarProfile*/
/*                                                                       */
/* AUTOR:    Roberto Murga                                               */
/*                                                                       */
/* FECHA:    14-10-1997                                                  */
/*                                                                       */
/*************************************************************************
*/

class MedicalServiceCalendarProfile
{
  MedicalServiceSingle* _mss;

  MedicalServiceCalendarBand** _bands;
  IlcInt _numBands;
  IlcInt _currentBand;

  Period _validity;
public:
  MedicalServiceCalendarProfile(MedicalServiceSingle* mss, 
                                Period validity,
                                IlcInt numBands);

  void addBand(MedicalServiceCalendarBand* band, Date* dateBand,
							 IlcInt interCita = 1);

	MedicalServiceSingle* getMedicalServiceSingle() const { return _mss; }

  Period getValidity() { return _validity; }
	
};

#endif // __SERVICAL_H_