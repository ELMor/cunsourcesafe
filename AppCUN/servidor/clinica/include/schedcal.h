#ifndef __SCHEDCALENDAR_H_
#define __SCHEDCALENDAR_H_

#include <indexcal.h>
#include /**/<ilsched/intact.h>
#include /**/<ilsched/breakact.h>
#include /**/<ilsched/breaks.h>
#include /**/<ilsched/unary.h>

//---------------------------------------------------------------
// Class SchedulerCalendarI
//---------------------------------------------------------------
class SchedulerCalendarI: public IlcScheduleI
{
	const IndexedCalendar* _calendar;
public:
  SchedulerCalendarI(IlcManager manager, const IndexedCalendar* calendar): 
	                   _calendar(calendar),
										 IlcScheduleI(manager.getImpl(),
                                 calendar->getTimeMin(), 
                                 calendar->getTimeMax(), IlcFalse)
                     {}

  const IndexedCalendar* getCalendar() const { return _calendar; }
};

//---------------------------------------------------------------
// Class SchedulerCalendar
//---------------------------------------------------------------
class SchedulerCalendar: public IlcSchedule
{
public:
	SchedulerCalendar(): IlcSchedule() {}
  SchedulerCalendar(SchedulerCalendarI* sched) { _impl= sched; }
  SchedulerCalendar(IlcManager manager, const IndexedCalendar* calendar): 
                    IlcSchedule(new (manager.getHeap()) 
                                     SchedulerCalendarI(manager, 
                                                        calendar))
  {}
  const IndexedCalendar* getCalendar() const 
	{ return ((SchedulerCalendarI*)getImpl())->getCalendar(); }
};

//---------------------------------------------------------------
// Class IntervalActivityCalendar
//---------------------------------------------------------------
class IntervalActivityCalendar: public IlcIntervalActivity
{
	const IndexedCalendar* getCalendar() const
  { return ((SchedulerCalendarI*)getSchedule().getImpl())->getCalendar(); }

public:
  IntervalActivityCalendar(): IlcIntervalActivity() {}

  IntervalActivityCalendar(SchedulerCalendar scheduler): 
                           IlcIntervalActivity(scheduler) {}

  IntervalActivityCalendar(SchedulerCalendar scheduler, 
		                       Duration duration,
													 IlcBool boundDuration= IlcTrue);

  IntervalActivityCalendar(IlcIntervalActivity act)
  { _impl= act.getImpl(); }

  IlcTimeBoundConstraint startsBefore(Date date)
  { return IlcIntervalActivity::startsBefore(getCalendar()->getTimeUnit(date)); }

  IlcTimeBoundConstraint startsAt(Date date)
  { return IlcIntervalActivity::startsAt(getCalendar()->getTimeUnit(date)); }
  
	IlcTimeBoundConstraint startsAfter(Date date)
  { return IlcIntervalActivity::startsAfter(getCalendar()->getTimeUnit(date)); }

  IlcTimeBoundConstraint endsBefore(Date date)
  { return IlcIntervalActivity::endsBefore(getCalendar()->getTimeUnit(date)); }
  
  IlcTimeBoundConstraint endsAt(Date date)
  { return IlcIntervalActivity::endsAt(getCalendar()->getTimeUnit(date)); }

  IlcTimeBoundConstraint endsAfter(Date date)
  { return IlcIntervalActivity::endsAfter(getCalendar()->getTimeUnit(date)); }
  
  Date getStartTime() const 
  {
  	if (getStartVariable().isBound())
	  return getCalendar()->getDate(getStartVariable().getValue());

	return Date(0,0,0,0,0);
  }
	
  Duration getDuration() const;
  void setDuration(Duration d)
  { IlcIntervalActivity::setDuration(getCalendar()->getTimeUnit(d)); }

  void printOn(ostream& os) const;
};

ostream& operator <<(ostream& os, const IntervalActivityCalendar& act);

/*
//---------------------------------------------------------------
// Class BreakableActivityCalendar
//---------------------------------------------------------------
class BreakableActivityCalendar: public IlcBreakableActivity
{
	const IndexedCalendar* getCalendar() const
  { return ((SchedulerCalendarI*)getSchedule().getImpl())->getCalendar(); }

public:
  BreakableActivityCalendar(SchedulerCalendar scheduler, 
		                        Duration duration): 
  IlcBreakableActivity(scheduler, 
		                   duration.getDuration(scheduler.getCalendar()->getTimeStep(),
                                            scheduler.getCalendar()->getTimeStepPack()))
  {}
	  IlcTimeBoundConstraint startsBefore(Date date)
  { return IlcIntervalActivity::startsBefore(getCalendar()->getTimeUnit(date)); }

  IlcTimeBoundConstraint startsAt(Date date)
  { return IlcIntervalActivity::startsAt(getCalendar()->getTimeUnit(date)); }
  
	IlcTimeBoundConstraint startsAfter(Date date)
  { return IlcIntervalActivity::startsAfter(getCalendar()->getTimeUnit(date)); }

  IlcTimeBoundConstraint endsBefore(Date date)
  { return IlcIntervalActivity::endsBefore(getCalendar()->getTimeUnit(date)); }
  
  IlcTimeBoundConstraint endsAt(Date date)
  { return IlcIntervalActivity::endsAt(getCalendar()->getTimeUnit(date)); }

  IlcTimeBoundConstraint endsAfter(Date date)
  { return IlcIntervalActivity::endsAfter(getCalendar()->getTimeUnit(date)); }
  
	void printOn(ostream& os) const;
};

ostream& operator <<(ostream& os, const BreakableActivityCalendar& act);

//---------------------------------------------------------------
// Class BreakListCalendarI
//---------------------------------------------------------------
class BreakListCalendarI: public IlcBreakListI
{
	const IndexedCalendar* _calendar;
public:
	BreakListCalendarI(SchedulerCalendar scheduler): 
			               IlcBreakListI(scheduler.getImpl()),
										 _calendar(scheduler.getCalendar())
	{}

  const IndexedCalendar* getCalendar() const { return _calendar; }
};

//---------------------------------------------------------------
// Class BreakListCalendar
//---------------------------------------------------------------
class BreakListCalendar: public IlcBreakList
{
	const IndexedCalendar* getCalendar() const
  { return ((BreakListCalendarI*)getImpl())->getCalendar(); }

public:
	BreakListCalendar(SchedulerCalendar scheduler):
			              IlcBreakList(new (getManager().getHeap()) 
                                 BreakListCalendarI(scheduler))
  {}
  void setSundayBreaks();
	void setSaturdayBreaks();
  void setWeekEndBreaks();
	void addBreak(Date from, Date to);
};
*/


//---------------------------------------------------------------
// Class UnaryResourceCalendar
//---------------------------------------------------------------
class UnaryResourceCalendar: public IlcUnaryResource
{
	const IndexedCalendar* getCalendar() const
  { return ((SchedulerCalendarI*)getSchedule().getImpl())->getCalendar(); }

public:
  UnaryResourceCalendar(): IlcUnaryResource() {}
  UnaryResourceCalendar(SchedulerCalendar scheduler, 
	                      IlcTransitionTimeObject obj,
						            IlcBool disjunctive = IlcTrue): 
	                      IlcUnaryResource(scheduler, obj, disjunctive)
	{}

  UnaryResourceCalendar(SchedulerCalendar scheduler):
                        IlcUnaryResource(scheduler) {}

  void setCapacityMax(Period period, IlcInt capacity)
  {
    IlcInt x= getCalendar()->getTimeUnit(period.getDateFrom());
    IlcInt y= getCalendar()->getTimeUnit(period.getDateTo());
    IlcUnaryResource::setCapacityMax(x, y, capacity); 
  }
};

//---------------------------------------------------------------
// Class UnaryResourceCalendar
//---------------------------------------------------------------
class DiscreteResourceCalendar: public IlcDiscreteResource
{
 	const IndexedCalendar* getCalendar() const
  { return ((SchedulerCalendarI*)getSchedule().getImpl())->getCalendar(); }

public:
  DiscreteResourceCalendar(): IlcDiscreteResource() {}
  DiscreteResourceCalendar(SchedulerCalendar scheduler, 
	                         IlcInt capacity,
                           IlcBool timeTable= IlcTrue):
	                         IlcDiscreteResource(scheduler, capacity, timeTable)
	{}

  DiscreteResourceCalendar(SchedulerCalendar scheduler):
                           IlcDiscreteResource(scheduler) {}

  void setCapacityMax(Period period, IlcInt capacity)
  {
    IlcInt x= getCalendar()->getTimeUnit(period.getDateFrom());
    IlcInt y= getCalendar()->getTimeUnit(period.getDateTo());
    IlcDiscreteResource::setCapacityMax(x, y, capacity); 
  }

  IlcConstraint makeTimetableConstraint(Period period,
                                        IlcUInt timeStepPack,
                                        IlcInt capacity);                           
};










#endif //__SCHEDCALENDAR_H_


