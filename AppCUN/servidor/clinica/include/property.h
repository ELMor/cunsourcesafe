#ifndef __PROPERTY_H_
#define __PROPERTY_H_

#include "string.h"
#include /**/<ilsched/discrete.h>
#include /**/<ilsched/intact.h>
#include /**/<ilsched/state.h>

//////////////////////////////////////////////////////////////////
// Class forwared 
//////////////////////////////////////////////////////////////////
class ClinicApplication;
class ClinicClient;
class MedicalServiceReserve;

/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION: Implementación de la clase Property               */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
enum DataTypeProperty { NUMERICO, CADENA };

class Property
{
	IlcInt _codigo;
	char _name[255];
	IlcIntVar _possibleValues;
  ClinicApplication* _app;

  DataTypeProperty _dataType;

public:
  Property(ClinicApplication* app, const IlcInt codigo, const char* name, 
           IlcInt numPossibleStates,DataTypeProperty dataType = NUMERICO);
  Property(const Property& p);

	char* getName() { return _name; }
	IlcInt getCodigo() const { return _codigo; }

	IlcInt getNumPossibleValues() const { return _possibleValues.getSize(); }
  IlcIntVar getPossibleValues() const { return _possibleValues; }
	IlcBool isIn(IlcInt value) const { return _possibleValues.isInDomain(value); }
  void setValues(IlcInt valueFrom, IlcInt valueTo, IlcBool inclusion);

  DataTypeProperty getDataTypeProperty() const { return _dataType; }
};


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION: Implementación de la clase PropertySet            */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class PropertySet
{
	Property** _properties;
  IlcInt _numProperties;
	IlcInt _currentProperty;

public:
	PropertySet(IlcInt numProperties);
	~PropertySet();

  IlcInt getNumProperties() { return _numProperties; }
  IlcInt getCurrentProperties() { return _currentProperty; }

	Property* getProperty( IlcInt codigo ) const;
	void addProperty(Property* property);
};



/*
*********************************************************************/
/*                   SYSECA BILBAO                                  */
/*                                                                  */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                       */
/*                                                                  */
/* NOMBRE:   Service.h                                              */
/*                                                                  */
/* DESCRIPCION: Implementación de la clase PropertyCondition        */
/*                                                                  */
/* AUTOR:    Roberto Murga                                          */
/*                                                                  */
/* FECHA:    17-9-1997                                              */
/*                                                                  */
/********************************************************************
*/
enum IncluType { EXCLUSION, INCLUSION };

class PropertyCondition
{
	Property* _property;
							
	IlcInt _valueFrom;
	IlcInt _valueTo;
	IncluType _inclusion;

  char _value[255];

public:
	PropertyCondition(Property* property, 
                    IlcInt valueFrom, IlcInt valueTo, 
                    IncluType inclusion);

  PropertyCondition(Property* property, 
                    IncluType inclusion,char* value);

	Property* getProperty() const { return _property; }

	IlcInt getValueFrom() const { return _valueFrom; }
	IlcInt getValueTo() const { return _valueTo; }

  const char* getValue() const { return _value; }
	IncluType getInclusion() const { return _inclusion; }
	void printon();

  IlcBool testClientProperties(ClinicClient* client) const;
};


/*
**************************************************************************/
/*                   SYSECA BILBAO                                       */
/*                                                                       */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                            */
/*                                                                       */
/* NOMBRE:   Service.h                                                   */
/*                                                                       */
/* DESCRIPCION:                                                          */
/*           Implementación de la clase PropertyConditionSet             */
/*                                                                       */
/* AUTOR:    Roberto Murga                                               */
/*                                                                       */
/* FECHA:    17-9-1997                                                   */
/*                                                                       */
/*************************************************************************
*/
class PropertyConditionSet
{
	PropertyCondition** _conditions;
	IlcInt _numConditions;
	IlcInt _currentCondition;

	ClinicApplication* _clinicApplication;

public:
  PropertyConditionSet(ClinicApplication* clinicApplication,
                       IlcInt maxConditions= 32);

	~PropertyConditionSet();

  IlcInt getNumPropertyConditions() const { return _numConditions; }

	IlcInt getCurrentPropertyCondition() const { return _currentCondition; }

	void addPropertyCondition(PropertyCondition* pc);
	PropertyCondition* getPropertyCondition(IlcInt Index) const;

  IlcBool testClientProperties(ClinicClient* client) const;
	void printon();
  
	ClinicApplication* getClinicApplication() const { return _clinicApplication; }
};


/*
**************************************************************************/
/*                   SYSECA BILBAO                                       */
/*                                                                       */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                            */
/*                                                                       */
/* NOMBRE:   property.h                                                  */
/*                                                                       */
/* DESCRIPCION:                                                          */
/*           Implementación de la clase PropertyConditionTree            */
/*                                                                       */
/* AUTOR:    Roberto Murga                                               */
/*                                                                       */
/* FECHA:    21-10-1997                                                  */
/*                                                                       */
/*************************************************************************
*/
class PropertyConditionTree
{
  PropertyConditionSet** _tree;
	IlcInt _numBranchs; 
  IlcInt _currentBranch;
public:
  PropertyConditionTree(IlcInt maxBranchs= 32): 
                        _numBranchs(maxBranchs), _currentBranch(0)
  { _tree= new PropertyConditionSet*[_numBranchs]; }
                       
  ~PropertyConditionTree();

  void addPropertyConditionSet(PropertyConditionSet* pcs);
  PropertyConditionSet* getPropertyConditionSet(IlcInt Index);

  IlcBool testClientProperties(ClinicClient* client) const;

	void printon();
};


/*
**************************************************************************/
/*                   SYSECA BILBAO                                       */
/*                                                                       */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                            */
/*                                                                       */
/* NOMBRE:   property.h                                                  */
/*                                                                       */
/* DESCRIPCION:                                                          */
/*           Implementación de la clase ClientProperty                   */
/*                                                                       */
/* AUTOR:    Roberto Murga                                               */
/*                                                                       */
/* FECHA:    21-10-1997                                                  */
/*                                                                       */
/*************************************************************************
*/
class ClientProperty
{
	IlcInt _codigo;
  const char* _name;

  IlcInt _value;
  char _valueCadena[255];

public:
  ClientProperty(const IlcInt codigo, const char* name, const char* valueCadena):
                _codigo(codigo), _name(name), _value(-1)
  { 
        strcpy(_valueCadena,valueCadena);
  }

	ClientProperty(const IlcInt codigo, const char* name, IlcInt value): 
                 _codigo(codigo), _name(name), _value(value)
                 {
                   strcpy(_valueCadena,"");
                 }

  const char* getName() const { return _name; }
	IlcInt getCodigo() const { return _codigo; }

  IlcInt getValue() const { return _value; }
  const char* getValueCadena() const { return _valueCadena; }

  DataTypeProperty getDataTypeProperty() const
  {  
    if(_value == -1)
      return(CADENA);
    else
      return(NUMERICO);
  }
};


#endif // __PROPERTY_H_