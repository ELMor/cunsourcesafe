#ifndef _BINDVAR_H
#define _BINDVAR_H

EXEC SQL BEGIN DECLARE SECTION;
	VARCHAR         username[20];
	VARCHAR         password[20];
	VARCHAR         cone_str[20];

  VARCHAR h_sentencia[3000+1];

  VARCHAR h_dato[1000];

  int		h_count;
 
  int 	h_dummy;

	VARCHAR	h_select[3000 + 1];
	VARCHAR	h_select_aux[3000 + 1];
	VARCHAR	h_select_temp[3000 + 1];
	VARCHAR	h_select_rest[3000 + 1];
	VARCHAR	h_select_ppal[3000 + 1];
	VARCHAR	h_select_rec[3000 + 1];
  VARCHAR	h_select_dpto[3000 + 1];

	typedef struct t_datos_cal
		{
		char	h_fecha_ini[10 + 1];
		char	h_fecha_fin[10 + 1];
		int		h_indlu;
		int		h_indma;
		int		h_indmi;
		int		h_indju;
		int		h_indvi;
		int		h_indsa;
		int		h_inddo;
		} t_datos_calen;

	t_datos_calen	h_datos_cal;

	typedef struct t_datos_dies
		{
		char	h_fecha_dia[10 + 1];
		int		h_indfes;
		} t_datos_diaesp;

	t_datos_diaesp	h_datos_diaesp;

	typedef struct t_datos_per
		{
		int		h_codigo_perfil;
		char	h_fecha_baja[10 + 1];
		char	h_fecha_ini[10 + 1];
		char	h_fecha_fin[10 + 1];
		} t_datos_perfil;

	t_datos_perfil	h_datos_per;

	typedef struct t_datos_pro
		{
		int		h_codigo_tiprest;
		char	h_descrip_tiprest[30 + 1];
		int		h_desde_tiprest;
		int		h_hasta_tiprest;
		} t_datos_properties;

	t_datos_properties	h_datos_pro;

	typedef struct t_datos_pro_alf
		{
		int		h_codigo_tiprest;
		char	h_descrip_tiprest[30 + 1];
		} t_datos_properties_alf;

	t_datos_properties_alf	h_datos_pro_alf;

	typedef struct t_datos_arb
		{
		int		h_level;
		int		h_cod_tiprest;
		char	h_desde_tiprest[20 + 1];
		char	h_hasta_tiprest[20 + 1];
		int		h_ind_inex;
		} t_datos_arbol;

	t_datos_arbol		h_datos_arb;

	typedef struct t_datos_fra
		{
		int		h_cod_franja;
		int		h_indlu;
		int		h_indma;
		int		h_indmi;
		int		h_indju;
		int		h_indvi;
		int		h_indsa;
		int		h_inddo;
		int		h_inihh;
		int		h_inimm;
		int		h_finhh;
		int		h_finmm;
    int		h_cod_act;
		int		h_num_citas;
    int   h_interv_cita;
		char	h_fecha_baja[10 + 1];
    char  h_modo_asig[1 + 1];
		} t_datos_franja;

	t_datos_franja	h_datos_franja;

	typedef struct t_datos_act
		{
		long	h_cod_dpto;
		long	h_cod_act;
		int		h_num_asig;
    char  h_fecini_vigencia[10 + 1];
    char  h_fecfin_vigencia[10 + 1];
		} t_datos_frjact;

	t_datos_frjact	h_datos_frjact;

	typedef struct t_datos_actdp
		{
		long		h_cod_dpto;
		long		h_cod_act;
		} t_datos_act_dpto;

	t_datos_act_dpto	h_datos_act;

	typedef struct t_datos_cit
		{
		unsigned long		h_numfase_cita;
		char						h_fecini_paciente[16 + 1];
		unsigned int		h_num_dias;
		unsigned int		h_num_horas;
		unsigned int		h_num_min;
		} t_datos_cita;

	t_datos_cita	h_datos_cita;

	typedef struct t_datos_citr
		{
		char						h_fecini_recurso[16 + 1];
		char						h_fecfin_recurso[16 + 1];
		unsigned long		h_cod_act;
		unsigned long		h_cod_dpto;
		} t_datos_citar;

	t_datos_citar	h_datos_citar;

	unsigned long			h_cod_recurso;

	typedef struct t_datos_fas
		{
		unsigned long		h_num_fase;
		char						h_desc_fase[30 + 1];
		unsigned long		h_num_fasep;
		unsigned long		h_dur_min;
		unsigned long		h_min_minutos;
		unsigned long		h_max_minutos;
		int							h_indicador;
		} t_datos_fases;

	t_datos_fases	h_datos_fases;

	typedef struct t_datos_r
		{
		unsigned long		h_cod_tiporec;
		unsigned long		h_num_unid;
		unsigned long		h_dur_min;
		long		        h_min_desfase;
		unsigned long		h_cod_rec;
		long						h_ind_pref;
    unsigned long		h_cod_dpto;
		} t_datos_rec;

	t_datos_rec	h_datos_rec;

	unsigned long		h_cod_trec;

	unsigned long		h_num_fase;

	unsigned long		h_num_unidades;

	typedef struct t_datos_i
		{
		unsigned long		h_codact_destino;
		unsigned long		h_num_minutos;
		} t_datos_iteration;

	t_datos_iteration	h_datos_iteration;

	typedef struct t_datos_rest_pac
		{
		int		h_cod_tiprest;
		char	h_valor[20 + 1];
		} t_datos_rest_paciente;

  t_datos_rest_paciente h_datos_rest_paciente;

  unsigned long	  h_cod_dpto;
  unsigned long	  h_cod_dpto_aux;
	unsigned long	  h_cod_act;

  unsigned long h_cod_actuacion;
  unsigned long h_cod_depto;
	unsigned long h_cod_act_pedi;
  unsigned long h_cod_persona;

EXEC SQL END DECLARE SECTION;

#endif