
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned long magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[53];
};
static const struct sqlcxp sqlfpn =
{
    52,
    "f:\\docume\\codigo\\vc5\\servidor\\dataccess\\src\\dserv.pc"
};


static const unsigned long sqlctx = 907689979;


static struct sqlexd {
   unsigned long    sqlvsn;
   unsigned long   arrsiz;
   unsigned long   iters;
   unsigned short   offset;
   unsigned short   selerr;
   unsigned short   sqlety;
   unsigned short   unused;
	 const    short   *cud;
   unsigned char    *sqlest;
	 const    char    *stmt;
   unsigned char  * *sqphsv;
   unsigned long   *sqphsl;
	    short  * *sqpind;
   unsigned long   *sqparm;
   unsigned long   * *sqparc;
   unsigned char    *sqhstv[17];
   unsigned long    sqhstl[17];
	    short   *sqindv[17];
   unsigned long    sqharm[17];
   unsigned long   *sqharc[17];
} sqlstm = {8,17};

// Prototypes
extern "C" {
  void sqlcx2(unsigned long , struct sqlexd *, const struct sqlcxp *);
  void sqlcte(unsigned long , struct sqlexd *, const struct sqlcxp *);
  void sqlbuf(char *);
  void sqlgs2(char *);
  void sqlora(const unsigned long, void *);
};

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(char *, int *); };

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{8,34,
2,0,0,1,0,0,27,74,0,3,3,0,1,0,1,9,0,0,1,9,0,0,1,9,0,0,
28,0,0,2,12,0,30,119,0,0,0,0,1,0,
42,0,0,3,0,0,17,406,0,1,1,0,1,0,1,9,0,0,
60,0,0,3,0,0,45,408,0,0,0,0,1,0,
74,0,0,3,0,0,13,414,0,1,0,0,1,0,2,9,0,0,
92,0,0,3,0,0,15,426,0,0,0,0,1,0,
106,0,0,4,0,0,17,540,0,1,1,0,1,0,1,9,0,0,
124,0,0,4,0,0,45,542,0,0,0,0,1,0,
138,0,0,4,0,0,13,546,0,1,0,0,1,0,2,3,0,0,
156,0,0,4,0,0,15,577,0,0,0,0,1,0,
170,0,0,5,0,0,17,680,0,1,1,0,1,0,1,9,0,0,
188,0,0,5,0,0,45,682,0,0,0,0,1,0,
202,0,0,5,0,0,13,686,0,1,0,0,1,0,2,3,0,0,
220,0,0,5,0,0,15,730,0,0,0,0,1,0,
234,0,0,6,0,0,24,824,0,1,1,0,1,0,1,9,0,0,
252,0,0,7,7,0,29,833,0,0,0,0,1,0,
266,0,0,8,0,0,17,1418,0,1,1,0,1,0,1,9,0,0,
284,0,0,8,0,0,45,1422,0,0,0,0,1,0,
298,0,0,8,0,0,13,1438,0,9,0,0,1,0,2,97,0,0,2,97,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,
3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,
348,0,0,9,0,0,17,1555,0,1,1,0,1,0,1,9,0,0,
366,0,0,9,0,0,45,1559,0,0,0,0,1,0,
380,0,0,9,0,0,13,1565,0,2,0,0,1,0,2,97,0,0,2,3,0,0,
402,0,0,9,0,0,15,1591,0,0,0,0,1,0,
416,0,0,8,0,0,15,1598,0,0,0,0,1,0,
430,0,0,10,0,0,17,1671,0,1,1,0,1,0,1,9,0,0,
448,0,0,10,0,0,45,1675,0,0,0,0,1,0,
462,0,0,10,0,0,13,1691,0,9,0,0,1,0,2,97,0,0,2,97,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,
3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,
512,0,0,11,0,0,17,1798,0,1,1,0,1,0,1,9,0,0,
530,0,0,11,0,0,45,1802,0,0,0,0,1,0,
544,0,0,11,0,0,13,1808,0,2,0,0,1,0,2,97,0,0,2,3,0,0,
566,0,0,11,0,0,15,1834,0,0,0,0,1,0,
580,0,0,10,0,0,15,1841,0,0,0,0,1,0,
594,0,0,12,0,0,17,2158,0,1,1,0,1,0,1,9,0,0,
612,0,0,12,0,0,45,2162,0,0,0,0,1,0,
626,0,0,12,0,0,13,2168,0,1,0,0,1,0,2,68,0,0,
644,0,0,12,0,0,15,2189,0,0,0,0,1,0,
658,0,0,13,0,0,17,2227,0,1,1,0,1,0,1,9,0,0,
676,0,0,13,0,0,45,2231,0,0,0,0,1,0,
690,0,0,13,0,0,13,2236,0,1,0,0,1,0,2,68,0,0,
708,0,0,13,0,0,15,2257,0,0,0,0,1,0,
722,0,0,14,0,0,17,2426,0,1,1,0,1,0,1,9,0,0,
740,0,0,14,0,0,45,2430,0,0,0,0,1,0,
754,0,0,14,0,0,13,2440,0,4,0,0,1,0,2,3,0,0,2,97,0,0,2,97,0,0,2,97,0,0,
784,0,0,14,0,0,15,2820,0,0,0,0,1,0,
798,0,0,15,0,0,17,3001,0,1,1,0,1,0,1,9,0,0,
816,0,0,15,0,0,45,3006,0,0,0,0,1,0,
830,0,0,15,0,0,13,3029,0,17,0,0,1,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,
3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,2,3,0,0,
2,97,0,0,2,97,0,0,
912,0,0,16,0,0,17,3135,0,1,1,0,1,0,1,9,0,0,
930,0,0,16,0,0,45,3139,0,0,0,0,1,0,
944,0,0,16,0,0,13,3145,0,5,0,0,1,0,2,3,0,0,2,3,0,0,2,3,0,0,2,97,0,0,2,97,0,0,
978,0,0,16,0,0,15,3166,0,0,0,0,1,0,
992,0,0,17,0,0,17,3218,0,1,1,0,1,0,1,9,0,0,
1010,0,0,17,0,0,45,3222,0,0,0,0,1,0,
1024,0,0,17,0,0,13,3227,0,1,0,0,1,0,2,68,0,0,
1042,0,0,17,0,0,15,3244,0,0,0,0,1,0,
1056,0,0,18,0,0,17,3277,0,1,1,0,1,0,1,9,0,0,
1074,0,0,18,0,0,45,3281,0,0,0,0,1,0,
1088,0,0,18,0,0,13,3287,0,2,0,0,1,0,2,68,0,0,2,68,0,0,
1110,0,0,19,0,0,17,3319,0,1,1,0,1,0,1,9,0,0,
1128,0,0,19,0,0,45,3323,0,0,0,0,1,0,
1142,0,0,19,0,0,13,3328,0,1,0,0,1,0,2,68,0,0,
1160,0,0,19,0,0,15,3349,0,0,0,0,1,0,
1174,0,0,18,0,0,15,3356,0,0,0,0,1,0,
1188,0,0,20,0,0,17,3403,0,1,1,0,1,0,1,9,0,0,
1206,0,0,20,0,0,45,3407,0,0,0,0,1,0,
1220,0,0,20,0,0,13,3412,0,1,0,0,1,0,2,68,0,0,
1238,0,0,20,0,0,15,3430,0,0,0,0,1,0,
1252,0,0,21,0,0,17,3464,0,1,1,0,1,0,1,9,0,0,
1270,0,0,21,0,0,45,3468,0,0,0,0,1,0,
1284,0,0,21,0,0,13,3474,0,2,0,0,1,0,2,68,0,0,2,68,0,0,
1306,0,0,22,0,0,17,3507,0,1,1,0,1,0,1,9,0,0,
1324,0,0,22,0,0,45,3511,0,0,0,0,1,0,
1338,0,0,22,0,0,13,3516,0,1,0,0,1,0,2,68,0,0,
1356,0,0,22,0,0,15,3537,0,0,0,0,1,0,
1370,0,0,21,0,0,15,3544,0,0,0,0,1,0,
1384,0,0,15,0,0,15,3717,0,0,0,0,1,0,
1398,0,0,23,0,0,17,3805,0,1,1,0,1,0,1,9,0,0,
1416,0,0,23,0,0,45,3809,0,0,0,0,1,0,
1430,0,0,23,0,0,13,3819,0,4,0,0,1,0,2,3,0,0,2,97,0,0,2,3,0,0,2,3,0,0,
1460,0,0,23,0,0,15,3846,0,0,0,0,1,0,
1474,0,0,24,0,0,17,3857,0,1,1,0,1,0,1,9,0,0,
1492,0,0,24,0,0,45,3861,0,0,0,0,1,0,
1506,0,0,24,0,0,13,3869,0,2,0,0,1,0,2,3,0,0,2,97,0,0,
1528,0,0,24,0,0,15,3883,0,0,0,0,1,0,
1542,0,0,25,0,0,17,3987,0,1,1,0,1,0,1,9,0,0,
1560,0,0,25,0,0,45,3991,0,0,0,0,1,0,
1574,0,0,25,0,0,13,4002,0,5,0,0,1,0,2,3,0,0,2,3,0,0,2,97,0,0,2,97,0,0,2,3,0,0,
1608,0,0,25,0,0,15,4202,0,0,0,0,1,0,
1622,0,0,26,0,0,17,4321,0,1,1,0,1,0,1,9,0,0,
1640,0,0,26,0,0,45,4325,0,0,0,0,1,0,
1654,0,0,26,0,0,13,4336,0,5,0,0,1,0,2,3,0,0,2,3,0,0,2,97,0,0,2,97,0,0,2,3,0,0,
1688,0,0,26,0,0,15,4530,0,0,0,0,1,0,
1702,0,0,27,0,0,17,4624,0,1,1,0,1,0,1,9,0,0,
1720,0,0,27,0,0,45,4628,0,0,0,0,1,0,
1734,0,0,27,0,0,13,4636,0,2,0,0,1,0,2,3,0,0,2,3,0,0,
1756,0,0,27,0,0,15,4654,0,0,0,0,1,0,
1770,0,0,28,0,0,17,4775,0,1,1,0,1,0,1,9,0,0,
1788,0,0,28,0,0,45,4779,0,0,0,0,1,0,
1802,0,0,28,0,0,13,4790,0,5,0,0,1,0,2,68,0,0,2,97,0,0,2,68,0,0,2,68,0,0,2,68,0,
0,
1836,0,0,28,0,0,15,4895,0,0,0,0,1,0,
1850,0,0,29,0,0,17,5017,0,1,1,0,1,0,1,9,0,0,
1868,0,0,29,0,0,45,5021,0,0,0,0,1,0,
1882,0,0,29,0,0,13,5031,0,4,0,0,1,0,2,97,0,0,2,97,0,0,2,68,0,0,2,68,0,0,
1912,0,0,29,0,0,15,5130,0,0,0,0,1,0,
1926,0,0,30,0,0,17,5264,0,1,1,0,1,0,1,9,0,0,
1944,0,0,30,0,0,45,5268,0,0,0,0,1,0,
1958,0,0,30,0,0,13,5270,0,4,0,0,1,0,2,68,0,0,2,68,0,0,2,68,0,0,2,68,0,0,
1988,0,0,30,0,0,15,5280,0,0,0,0,1,0,
2002,0,0,31,0,0,17,5331,0,1,1,0,1,0,1,9,0,0,
2020,0,0,31,0,0,45,5335,0,0,0,0,1,0,
2034,0,0,31,0,0,13,5346,0,7,0,0,1,0,2,68,0,0,2,97,0,0,2,68,0,0,2,68,0,0,2,68,0,
0,2,68,0,0,2,3,0,0,
2076,0,0,32,0,0,17,5379,0,1,1,0,1,0,1,9,0,0,
2094,0,0,32,0,0,45,5383,0,0,0,0,1,0,
2108,0,0,32,0,0,13,5389,0,2,0,0,1,0,2,68,0,0,2,68,0,0,
2130,0,0,32,0,0,15,5407,0,0,0,0,1,0,
2144,0,0,33,0,0,17,5435,0,1,1,0,1,0,1,9,0,0,
2162,0,0,33,0,0,45,5439,0,0,0,0,1,0,
2176,0,0,33,0,0,13,5451,0,7,0,0,1,0,2,68,0,0,2,68,0,0,2,68,0,0,2,3,0,0,2,68,0,
0,2,3,0,0,2,68,0,0,
2218,0,0,33,0,0,15,5576,0,0,0,0,1,0,
2232,0,0,31,0,0,15,5711,0,0,0,0,1,0,
2246,0,0,34,0,0,17,5894,0,1,1,0,1,0,1,9,0,0,
2264,0,0,34,0,0,45,5898,0,0,0,0,1,0,
2278,0,0,34,0,0,13,5904,0,2,0,0,1,0,2,68,0,0,2,68,0,0,
2300,0,0,34,0,0,15,5942,0,0,0,0,1,0,
2314,0,0,35,0,0,17,6038,0,1,1,0,1,0,1,9,0,0,
2332,0,0,35,0,0,45,6042,0,0,0,0,1,0,
2346,0,0,35,0,0,13,6050,0,2,0,0,1,0,2,3,0,0,2,97,0,0,
2368,0,0,35,0,0,15,6102,0,0,0,0,1,0,
};


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Clinic.h>
#include <dataacss.h>

#include "Errores.h"
#include "Dserv.h"

/* EXEC SQL INCLUDE "BindVar.h";
 */ 
#ifndef _BINDVAR_H
#define _BINDVAR_H

/* EXEC SQL BEGIN DECLARE SECTION; */ 
	/* VARCHAR         username[20]; */ 
struct { unsigned short len; unsigned char arr[20]; } username;
	/* VARCHAR         password[20]; */ 
struct { unsigned short len; unsigned char arr[20]; } password;
	/* VARCHAR         cone_str[20]; */ 
struct { unsigned short len; unsigned char arr[20]; } cone_str;

  /* VARCHAR h_sentencia[3000+1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_sentencia;

  /* VARCHAR h_dato[1000]; */ 
struct { unsigned short len; unsigned char arr[1000]; } h_dato;

  int		h_count;
 
  int 	h_dummy;

	/* VARCHAR	h_select[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select;
	/* VARCHAR	h_select_aux[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select_aux;
	/* VARCHAR	h_select_temp[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select_temp;
	/* VARCHAR	h_select_rest[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select_rest;
	/* VARCHAR	h_select_ppal[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select_ppal;
	/* VARCHAR	h_select_rec[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select_rec;
  /* VARCHAR	h_select_dpto[3000 + 1]; */ 
struct { unsigned short len; unsigned char arr[3001]; } h_select_dpto;

	typedef struct t_datos_cal
		{
		char	h_fecha_ini[10 + 1];
		char	h_fecha_fin[10 + 1];
		int		h_indlu;
		int		h_indma;
		int		h_indmi;
		int		h_indju;
		int		h_indvi;
		int		h_indsa;
		int		h_inddo;
		} t_datos_calen;

	t_datos_calen	h_datos_cal;

	typedef struct t_datos_dies
		{
		char	h_fecha_dia[10 + 1];
		int		h_indfes;
		} t_datos_diaesp;

	t_datos_diaesp	h_datos_diaesp;

	typedef struct t_datos_per
		{
		int		h_codigo_perfil;
		char	h_fecha_baja[10 + 1];
		char	h_fecha_ini[10 + 1];
		char	h_fecha_fin[10 + 1];
		} t_datos_perfil;

	t_datos_perfil	h_datos_per;

	typedef struct t_datos_pro
		{
		int		h_codigo_tiprest;
		char	h_descrip_tiprest[30 + 1];
		int		h_desde_tiprest;
		int		h_hasta_tiprest;
		} t_datos_properties;

	t_datos_properties	h_datos_pro;

	typedef struct t_datos_pro_alf
		{
		int		h_codigo_tiprest;
		char	h_descrip_tiprest[30 + 1];
		} t_datos_properties_alf;

	t_datos_properties_alf	h_datos_pro_alf;

	typedef struct t_datos_arb
		{
		int		h_level;
		int		h_cod_tiprest;
		char	h_desde_tiprest[20 + 1];
		char	h_hasta_tiprest[20 + 1];
		int		h_ind_inex;
		} t_datos_arbol;

	t_datos_arbol		h_datos_arb;

	typedef struct t_datos_fra
		{
		int		h_cod_franja;
		int		h_indlu;
		int		h_indma;
		int		h_indmi;
		int		h_indju;
		int		h_indvi;
		int		h_indsa;
		int		h_inddo;
		int		h_inihh;
		int		h_inimm;
		int		h_finhh;
		int		h_finmm;
    int		h_cod_act;
		int		h_num_citas;
    int   h_interv_cita;
		char	h_fecha_baja[10 + 1];
    char  h_modo_asig[1 + 1];
		} t_datos_franja;

	t_datos_franja	h_datos_franja;

	typedef struct t_datos_act
		{
		long	h_cod_dpto;
		long	h_cod_act;
		int		h_num_asig;
    char  h_fecini_vigencia[10 + 1];
    char  h_fecfin_vigencia[10 + 1];
		} t_datos_frjact;

	t_datos_frjact	h_datos_frjact;

	typedef struct t_datos_actdp
		{
		long		h_cod_dpto;
		long		h_cod_act;
		} t_datos_act_dpto;

	t_datos_act_dpto	h_datos_act;

	typedef struct t_datos_cit
		{
		unsigned long		h_numfase_cita;
		char						h_fecini_paciente[16 + 1];
		unsigned int		h_num_dias;
		unsigned int		h_num_horas;
		unsigned int		h_num_min;
		} t_datos_cita;

	t_datos_cita	h_datos_cita;

	typedef struct t_datos_citr
		{
		char						h_fecini_recurso[16 + 1];
		char						h_fecfin_recurso[16 + 1];
		unsigned long		h_cod_act;
		unsigned long		h_cod_dpto;
		} t_datos_citar;

	t_datos_citar	h_datos_citar;

	unsigned long			h_cod_recurso;

	typedef struct t_datos_fas
		{
		unsigned long		h_num_fase;
		char						h_desc_fase[30 + 1];
		unsigned long		h_num_fasep;
		unsigned long		h_dur_min;
		unsigned long		h_min_minutos;
		unsigned long		h_max_minutos;
		int							h_indicador;
		} t_datos_fases;

	t_datos_fases	h_datos_fases;

	typedef struct t_datos_r
		{
		unsigned long		h_cod_tiporec;
		unsigned long		h_num_unid;
		unsigned long		h_dur_min;
		long		        h_min_desfase;
		unsigned long		h_cod_rec;
		long						h_ind_pref;
    unsigned long		h_cod_dpto;
		} t_datos_rec;

	t_datos_rec	h_datos_rec;

	unsigned long		h_cod_trec;

	unsigned long		h_num_fase;

	unsigned long		h_num_unidades;

	typedef struct t_datos_i
		{
		unsigned long		h_codact_destino;
		unsigned long		h_num_minutos;
		} t_datos_iteration;

	t_datos_iteration	h_datos_iteration;

	typedef struct t_datos_rest_pac
		{
		int		h_cod_tiprest;
		char	h_valor[20 + 1];
		} t_datos_rest_paciente;

  t_datos_rest_paciente h_datos_rest_paciente;

  unsigned long	  h_cod_dpto;
  unsigned long	  h_cod_dpto_aux;
	unsigned long	  h_cod_act;

  unsigned long h_cod_actuacion;
  unsigned long h_cod_depto;
	unsigned long h_cod_act_pedi;
  unsigned long h_cod_persona;

/* EXEC SQL END DECLARE SECTION; */ 

#endif/* EXEC SQL INCLUDE "sqlca.h";
 */ 
/* Copyright (c) 1985,1986 by Oracle Corporation. */

/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:

    #define SQLCA_STORAGE_CLASS extern

  will define the SQLCA as an extern.

  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  New rules for defining SQLCA_INIT, SQLCA_STORAGE_CLASS, and DLL in OS/2:
  Users should not define SQLCA_STORAGE_CLASS if defining DLL.
  SQLCA_STORAGE_CLASS is primarily used for single-threaded programs
  and for internal development.

MODIFIED
  Okamura    08/15/89 - OS/2: users must define SQLMT for multi-threaded case
  Okamura    06/23/89 - OS/2: modify for multi-threaded case
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/

#ifndef SQLCA
#define SQLCA 1

struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ long    sqlabc;
         /* b4  */ long    sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ long    sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifdef SQLMT
   extern struct sqlca *sqlcamt();             /* For multi-threaded version */
#  define sqlca (*sqlcamt())
#else /* SQLMT */

#ifdef SQLCA_STORAGE_CLASS
  SQLCA_STORAGE_CLASS struct sqlca sqlca
# ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
# endif /* SQLCA_INIT */
         ;

#else /* SQLCA_STORAGE_CLASS */
   struct sqlca sqlca                         /* For single-threaded version */

#  ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#  endif /* SQLCA_INIT */
         ;
#endif /* SQLCA_STORAGE_CLASS */

#endif /* SQLMT */

/* end SQLCA */
#endif /* SQLCA */

extern DataBaseAccess* data_access;

/*
 *******************************************************************************
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_connect_ora
 *
 * TIPO:        Funci�n 
 *
 * ARGUMENTOS:
 *
 *      Nombre          	Tipo            Descripci�n
 *      ----------------- 	-------------- ---------------------------------------
 *		usuario				char*			Usuario de conexi�n a la B.D.
 *		passwd				char*			Password de conexi�n a la B.D.
 *		servidor			char*			Connection string de conexi�n a la 
 *											B.D.
 *
 * DESCRIPCI�N:
 *     Se conecta a la B.D.
 *
 * RESULTADOS:
 *      Ninguno
 *
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   06/02/1997
 * AUTOR:               Marian Bezanilla.
 *
 *******************************************************************************
 */
unsigned long gen_connect_ora( 
								char*	usuario,
								char*	passwd,
								char*	servidor
							 )
{
	//Declaraci�n de variables.
	unsigned long	v_result = GENE0000_NORMAL;

	strcpy((char *)username.arr, usuario);
	username.len = (short) strlen((char *)username.arr);

	strcpy((char *)password.arr, passwd);
	password.len = (short) strlen((char *)password.arr);

	strcpy((char *)cone_str.arr, servidor);
	cone_str.len = (short) strlen((char *)cone_str.arr);

	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
												v_result); */ 

    /* EXEC SQL CONNECT :username IDENTIFIED BY :password USING :cone_str; */ 
{
    struct sqlexd sqlstm={8,3};
    sqlstm.iters = (unsigned int  )10;
    sqlstm.offset = (unsigned int  )2;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&username;
    sqlstm.sqhstl[0] = (unsigned int  )22;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqhstv[1] = (unsigned char  *)&password;
    sqlstm.sqhstl[1] = (unsigned int  )22;
    sqlstm.sqindv[1] = (         short *)0;
    sqlstm.sqharm[1] = (unsigned int  )0;
    sqlstm.sqhstv[2] = (unsigned char  *)&cone_str;
    sqlstm.sqhstl[2] = (unsigned int  )22;
    sqlstm.sqindv[2] = (         short *)0;
    sqlstm.sqharm[2] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	return ( v_result );

}

/*
 *******************************************************************************
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_disconnect_ora
 *
 * TIPO:        Funci�n 
 *
 * ARGUMENTOS:
 *
 *      Nombre          	Tipo            Descripci�n
 *      ----------------- 	-------------- ---------------------------------------
 *
 * DESCRIPCI�N:
 *     Se desconecta de la B.D.
 *
 * RESULTADOS:
 *      Ninguno
 *
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   06/02/1997
 * AUTOR:               Marian Bezanilla.
 *
 *******************************************************************************
 */
void gen_disconnect_ora()
{
	//Declaraci�n de variables.
	unsigned long	v_result = GENE0000_NORMAL;

	/* EXEC SQL COMMIT WORK RELEASE; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )28;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

	
}


/*
 *******************************************************************************
 *							K Y A T   -   S Y S E C A
 *
 * PROYECTO:	CLINICA
 *
 * NOMBRE:		gen_error_ora
 *
 * TIPO			funcion
 *
 * ARGUMENTOS:
 *
 *		Nombre			Tipo			Descripcion
 *		---------------	---------------	---------------------------------------
 *		ae_condi		unsigned int	Condicion de error
 *		as_error		unsigned long&	Codigo de error CLINICA
 *
 * DESCRIPCION:
 *		Servicio que gestina los errorres como consecuencia de operaciones  
 *		con la base de datos Oracle. El argumento de entrada ae_condi indi-
 *		ca la condicion por la cual sea producido la llamada a la funcion.
 *		Estas condiciones seran por error, por warning y por registro no
 *		encontrado. Devuelve como argumento de salida as_error con el
 *		valor correspondiente al error detectado  con el fin que la funcion 
 *		donde realmente se realiza la operacion con oracle tenga conocimiento 
 *		del mismo.  
 *
 * RESULTADOS:
 *		ninguno - void
 *		
 * ERRORES:
 *
 *		Nombre				 Descripcion
 *		-------------------- --------------------------------------------------
 *
 * CONTEXTO:
 *		Ninguno.
 *
 * FECHA DE CREACI�N:	12/04/1996	
 * AUTOR:				Jose J. Fuentes
 *
 *******************************************************************************
*/
/*
 *******************************************************************************
 *
 * 			DISE�O DETALLADO
 *
 *	Determinar la condicion y el error Oracle detectado.
 *
 *  Traducir el codigo de error Oracle al codigo de error de CLINICA. 
 *
 *  Devolver el codigo de error CLINICA
 * 
 *
 *******************************************************************************
*/

void gen_error_ora( 
				   unsigned long	ae_condi,
			       unsigned long&	as_error
			   	  )
{ /* inicio funcion */

/*
 *******************************************************************************
 *
 * Declaracion de variables locales  
 *
 *******************************************************************************
*/

	/* Codigo de error de CLINICA */
	unsigned long	v_err_clinica = GENE0000_NORMAL;

	static char	  v_funcion[]="gen_error_ora";	// Nombre de la funci�n

/*
 *******************************************************************************
 *
 * Determinar la condicion y el error Oracle detectado
 *
 *******************************************************************************
*/
	
	switch (ae_condi)
	{

	/*
 	 ***************************************************************************
 	 *
 	 *  Condicion de error
 	 *
 	 ***************************************************************************
	*/
		case GEN_ERROR_ORA_AE_CONDI_ERROR:

			switch (sqlca.sqlcode)
			{
				case 0:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0000_NORMAL;
					break;

				case -54:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0057_REGISTRO_BLOQUEADO;
					break;

				case -903:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0043_TABLA_NO_EXISTE;
					break;

				case -933:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0048_SENTEN_ERROR;
					break;

				case -1405:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0000_NORMAL;
					break;

				case -1034:
				case -1017:
				case -3114:
				case -3121:
				case -12154:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0046_NO_CONNECT;
					break;

				default:
					v_err_clinica = GENE0014_ORAERRORNOTIP;
					break;

			} /* fin switch de error*/	
			break;

	/*
 	 ***************************************************************************
 	 *
 	 *  Condicion de warning
 	 *
 	 ***************************************************************************
	*/
		case GEN_ERROR_ORA_AE_CONDI_WARNING:
			/* Detectar y traducir el warning Oracle a  CLINICA */
			switch (sqlca.sqlcode)
			{
				case 0:
					/* Asignar codigo de error CLINICA */
					v_err_clinica = GENE0000_NORMAL;
					break;

				default:
					/* Warning Oracle no tipificada */
					v_err_clinica = GENE0015_ORAWARNINGNOTIP;
					break;

			} /* fin switch de warning*/	
			break;


	/*
 	 ***************************************************************************
 	 *
 	 *  Condicion de no encontrado
 	 *
 	 ***************************************************************************
	*/
		case GEN_ERROR_ORA_AE_CONDI_NOT_FOUND:
			/* Asignar codigo de error CLINICA - registro no encontrado*/
			v_err_clinica = GENE0013_REGNOTFOUND;
			break;

	/*
 	 ***************************************************************************
 	 *
 	 * Por defecto 
 	 *
 	 ***************************************************************************
	*/
		default:
			break;

	} /* fin swicth de condicion */	

	as_error = v_err_clinica;

} /* fin de gen_error_ora */





/*
 *******************************************************************************
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_obtener_dato
 *
 * TIPO:        Funci�n 
 *
 * ARGUMENTOS:
 *
 *      Nombre          	Tipo            Descripci�n
 *      ----------------- 	-------------- ---------------------------------------
 *      ae_tabla        	char*           Tabla
 *      ae_where        	char*           Condici�n de b�squeda
 *      ae_campo        	char*           Campos a recuperar
 *      as_dato         	char*           Campo
 *
 * DESCRIPCI�N:
 *     Recupera un dato de la tabla
 *
 * RESULTADOS:
 *      Ninguno
 *
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   06/02/1997
 * AUTOR:               Marian Bezanilla.
 *
 *******************************************************************************
 */
/*
 *******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 *******************************************************************************
 */

unsigned long gen_obtener_dato( 
								char*	ae_tabla,
                        		char*   ae_where,
                        		char*   ae_campo,
                        		char*   as_dato
								)

{ /* inicio funci�n */

/*
 *******************************************************************************
 *
 *  Declaraci�n de variables y bind variables
 *
 *******************************************************************************
*/
 	unsigned long v_result = GENE0000_NORMAL;

    if ( strcmp( ae_where , "") )
    {
        sprintf( (char*)h_sentencia.arr, " SELECT %s FROM %s WHERE %s",
                                    ae_campo,ae_tabla, ae_where );

    }/* fin if */
    else
    {
        sprintf( (char*)h_sentencia.arr, " SELECT %s FROM %s",
                                    ae_campo,ae_tabla );
    }/* fin else */
 
    h_sentencia.len=strlen((char *)h_sentencia.arr);
    h_sentencia.arr[h_sentencia.len]='\0';
 
 
    /* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
												v_result); */ 
 
    /* EXEC SQL WHENEVER NOT FOUND CONTINUE; */ 
 
    /* EXEC SQL PREPARE SENTENCIAUNO FROM :h_sentencia; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )42;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_sentencia;
    sqlstm.sqhstl[0] = (unsigned int  )3003;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

    /* EXEC SQL DECLARE sql_cursor_dato CURSOR FOR SENTENCIAUNO; */ 
    /* EXEC SQL OPEN sql_cursor_dato; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )60;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
    strcpy((char *)h_dato.arr,"");
    h_dato.len=strlen((char *)h_dato.arr);
 
 
    /* EXEC SQL FETCH sql_cursor_dato INTO :h_dato; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )74;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_dato;
    sqlstm.sqhstl[0] = (unsigned int  )1002;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
    if (v_result != GENE0000_NORMAL )
	{
        return (v_result);
    } /* fin if */
    else
    {
        h_dato.arr[h_dato.len]='\0';
        strcpy(as_dato, (char*)h_dato.arr);
    }/* fin else */
 
    /* EXEC SQL CLOSE sql_cursor_dato; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )92;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


    return (v_result);
 
}/* fin funci�n gen_obtener_dato */

	

/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_count_registros
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre          Tipo            Descripci�n
 *      --------------- -------------- ---------------------------------------
 *      ae_tabla        char*           Tabla
 *      ae_where        char*           Condici�n de b�squeda
 *      as_count        unsigned int    Count de los registros
 *
 *
 * DESCRIPCI�N:
 *     Calcula el n�mero de registros existentes en una tabla para la
 *     condici�n de b�squeda especificada
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   03/10/1996
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 *  Efectuar acceso a la tabla
 *
 ******************************************************************************
 */
unsigned long
gen_count_registros ( 
 						char* 			ae_tabla,
                        char* 			ae_where,
                        unsigned int&	as_count
					)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *  Inicializar variables
 *
 *******************************************************************************
 */
	unsigned long v_result = GENE0000_NORMAL;
 
/*
 *******************************************************************************
 *
 *	Preparar la sentencia a ejecutar
 *
 *******************************************************************************
 */
    if ( strcmp( ae_where , "") )
    {
		sprintf( (char*)h_sentencia.arr, " SELECT COUNT(*) FROM %s WHERE %s ",
                                        ae_tabla, ae_where );

    }/* fin if */
    else
    {
		sprintf( (char*)h_sentencia.arr, " SELECT COUNT(*) FROM %s ",
                                        ae_tabla );

    }/* fin else */

    h_sentencia.len = strlen( (char *)h_sentencia.arr );
    h_sentencia.arr[h_sentencia.len] = '\0';
 
/*
 *******************************************************************************
 *
 *	Control de errores posibles
 *
 *******************************************************************************
 */
    /* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
 
    /* EXEC SQL WHENEVER NOT FOUND CONTINUE; */ 
 
/*
 *******************************************************************************
 *
 *	Declarar cursor, abrirlo y guardar resultado en h_count
 *
 *******************************************************************************
 */
    /* EXEC SQL PREPARE SENTENCIADOS FROM :h_sentencia; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )106;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_sentencia;
    sqlstm.sqhstl[0] = (unsigned int  )3003;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

    /* EXEC SQL DECLARE sql_cursor_sentencia CURSOR FOR SENTENCIADOS; */ 
    /* EXEC SQL OPEN sql_cursor_sentencia; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )124;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
    h_count = 0;
 
    /* EXEC SQL FETCH sql_cursor_sentencia INTO :h_count; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )138;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_count;
    sqlstm.sqhstl[0] = (unsigned int  )4;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
/*
 *******************************************************************************
 *
 *	Controlar el posible error
 *
 *******************************************************************************
 */
    if (v_result != GENE0000_NORMAL)
	{ /* error */

		/* retornar c�digo de error */
        return (v_result);

    }  /* fin if */
    else
    { /* no error */

		/* Dejar resultado en par�metro de salida */
        as_count = h_count;

    }/* fin else */
 
/*
 *******************************************************************************
 *
 *	Cerrar el cursor
 *
 *******************************************************************************
 */
    /* EXEC SQL CLOSE sql_cursor_sentencia; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )156;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


/*
 *******************************************************************************
 *
 *	Retornar c�digo de ejecuci�n correcta
 *
 *******************************************************************************
 */
    return( v_result );
 
}/* fin funci�n gen_count_registros */
 
 

/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_existe_registro
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre           Tipo            Descripci�n
 *      ---------------  -------------   -------------------------------------
 *      ae_sentencia_sql char*           Sentencia de b�squeda
 *
 *
 * DESCRIPCI�N:
 *		Comprueba la existencia de un rgistro, �nicamente devuelve la existen-
 *		cia � no del registro.
 *
 * RESULTADOS:
 *      Existencia � no del registro.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   07/10/1996
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
unsigned long
gen_existe_registro ( 
 						char* 			ae_sentencia_sql
					)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declarar variables
 *
 *******************************************************************************
 */
	unsigned long v_result = GENE0000_NORMAL;
 
/*
 *******************************************************************************
 *
 *  Cargar en variable h_sentencia el par�metro de entrada ae_sentencia_sql
 *
 *******************************************************************************
 */
 	strcpy(( char * )h_sentencia.arr, ae_sentencia_sql );
    h_sentencia.len = strlen(( char * )h_sentencia.arr );
    h_sentencia.arr[h_sentencia.len] = '\0';

/*
 *******************************************************************************
 *
 *	Control de errores posibles
 *
 *******************************************************************************
 */
    /* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
 
    /* EXEC SQL WHENEVER NOT FOUND CONTINUE; */ 
 
/*
 *******************************************************************************
 *
 *	Declarar cursor, abrirlo y controlar la existencia del registro
 *
 *******************************************************************************
 */
    /* EXEC SQL PREPARE SENTENCIA FROM :h_sentencia; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )170;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_sentencia;
    sqlstm.sqhstl[0] = (unsigned int  )3003;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

    /* EXEC SQL DECLARE sql_cursor_existencia CURSOR FOR SENTENCIA; */ 
    /* EXEC SQL OPEN sql_cursor_existencia; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )188;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
    h_dummy = -1;
 
    /* EXEC SQL FETCH sql_cursor_existencia INTO :h_dummy; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )202;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_dummy;
    sqlstm.sqhstl[0] = (unsigned int  )4;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
/*
 *******************************************************************************
 *
 *	Controlar el posible error
 *
 *******************************************************************************
 */
    if (v_result != GENE0000_NORMAL)
	{ /* error */

		/* retornar c�digo de error */
        return (v_result);

    }  /* fin if */
 
/*
 *******************************************************************************
 *
 *	Controlar la existencia del registro
 *
 *******************************************************************************
 */
 	if ( h_dummy != -1 )
   	{ /* Registro existe */

        return (GENE0037_REGISTRO_EXISTE);

    }/*fin if*/
    else
    { /* Registro no existe */

        return (GENE0033_REGISTRO_NO_EXISTE);

    }/*fin else*/

/*
 *******************************************************************************
 *
 *	Cerrar el cursor
 *
 *******************************************************************************
 */
    /* EXEC SQL CLOSE sql_cursor_existencia; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )220;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


}/* fin funci�n gen_existe_registro */
 

 
/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_ejecutar_sentencia
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre           Tipo            Descripci�n
 *      ---------------  -------------   -------------------------------------
 *      ae_sentencia_sql char*           Sentencia INSERT, DELETE � UPDATE
 *
 *
 * DESCRIPCI�N:
 *		Ejecutar la sentencia SQL (Insert, Update � Delete) pasada como par�-
 *		metro. Grabar los cambios.
 *
 * RESULTADOS:
 *      Ejecuci�n correcta � no.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   07/10/1996
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
unsigned long
gen_ejecutar_sentencia ( 
 							char* 			ae_sentencia_sql
						)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declarar variables
 *
 *******************************************************************************
 */
	unsigned long v_result = GENE0000_NORMAL;
 
/*
 *******************************************************************************
 *
 *  Cargar en variable h_sentencia el par�metro de entrada ae_sentencia_sql
 *
 *******************************************************************************
 */
 	strcpy(( char * )h_sentencia.arr, ae_sentencia_sql );
    h_sentencia.len = strlen(( char * )h_sentencia.arr );
    h_sentencia.arr[h_sentencia.len] = '\0';

/*
 *******************************************************************************
 *
 *	Control de errores posibles
 *
 *******************************************************************************
 */
    /* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
 
    /* EXEC SQL WHENEVER NOT FOUND CONTINUE; */ 
 
/*
 *******************************************************************************
 *
 *	Ejecutar la sentencia SQL.
 *
 *******************************************************************************
 */
    /* EXEC SQL EXECUTE IMMEDIATE :h_sentencia; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )234;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_sentencia;
    sqlstm.sqhstl[0] = (unsigned int  )3003;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


/*
 *******************************************************************************
 *
 *	Guardar los cambios.
 *
 *******************************************************************************
 */
    /* EXEC SQL COMMIT; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )252;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

 
/*
 *******************************************************************************
 *
 *	Retornar el posible error
 *
 *******************************************************************************
 */
     return (v_result);

}/* fin funci�n gen_ejecutar_sentencia */
 
  
 
/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_fecha_numero
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre          Tipo            Descripci�n
 *      --------------- -------------- ---------------------------------------
 *      ae_fecha	    char*          Fecha en cadena
 *
 *
 * DESCRIPCI�N:
 *		Tranformar una fecha en formato string a formato n�mero (YYYYMMDD)
 *
 * RESULTADOS:
 *      La fecha en formato YYYYMMDD.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   02/10/1996
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 *	Transformar fecha en formato cadena a formato n�mero (YYYYMMDD)
 *
 ******************************************************************************
 */
unsigned long
gen_fecha_numero ( 
                    char*	 ae_fecha 
				)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *  Inicializar variables
 *
 *******************************************************************************
 */
    unsigned long 	v_fecha_num = 0;
    char    		v_dia[C_GEN_LEN_DIA + 1] = "";
    char    		v_mes[C_GEN_LEN_MES + 1] = "";
    char    		v_year[C_GEN_LEN_ANIO + 1] = "";
    char    		v_fecha[C_GEN_LEN_FECHA + 1] = "";
 
    strncpy( v_dia, ae_fecha, C_GEN_LEN_DIA );
 
    strncpy( v_mes, ae_fecha + 3, C_GEN_LEN_MES );
 
    strncpy( v_year, ae_fecha + 6, C_GEN_LEN_ANIO );
 
    sprintf( v_fecha, "%s%s%s", v_year, v_mes, v_dia );
 
    v_fecha_num = atoi( v_fecha );
 
    return ( v_fecha_num );
 
}/* fin funci�n gen_fecha_numero */
 
 
/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_desglosar_fecha_hora
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre          Tipo            Descripci�n
 *      --------------- -------------- ---------------------------------------
 *      ae_fehor	    char*			Fecha en cadena, 'DD-MM-YYYY hh24:mi'
 *		as_hora			unsigned int&	Horas en entero.
 *		as_min			unsigned int&	Minutos en entero.
 *
 * DESCRIPCI�N:
 *		Desglosar una fecha en formato string ('DD-MM-YYYY hh24:mi') en 
 *		2 enteros con las horas y los minutos.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   26/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_desglosar_fecha_hora( 
							char*			ae_fehor,
							unsigned int&	as_hora,
							unsigned int&	as_min
						)
{ /* inicio funci�n /*
/*
 ******************************************************************************
 *
 * Declaraci�n de variables.
 *
 ******************************************************************************
 */
	char	hora_alf[C_GEN_LEN_DIA + 1] = "";
	char	min_alf[C_GEN_LEN_MES + 1] = "";

	strncpy( hora_alf, &ae_fehor[11], C_GEN_LEN_DIA );
	as_hora = atoi( hora_alf );
	strncpy( min_alf, &ae_fehor[14], C_GEN_LEN_MES );
	as_min = atoi( min_alf );

} /* fin de gen_desglosar_fecha_hora */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_desglosar_fechad
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre          Tipo            Descripci�n
 *      --------------- -------------- ---------------------------------------
 *      ae_fecha	    char*			Fecha en cadena, formato 'DD-MM-YYYY'
 *		as_dia			unsigned int&	Dia en entero.
 *		as_mes			unsigned int&	Mes en entero.
 *		as_anio			unsigned int&	A�o en entero.
 *
 * DESCRIPCI�N:
 *		Desglosar una fecha en formato string ('DD-MM-YYYY') en 3 enteros con
 *		los valores correspondientes de dia, mes y a�o.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   26/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_desglosar_fechad( 
						char*			ae_fecha,
						unsigned int&	as_dia,
						unsigned int&	as_mes,
						unsigned int&	as_anio
					)
{ /* inicio funci�n */
/*
 ******************************************************************************
 *
 * Declaraci�n de variables.
 *
 ******************************************************************************
 */
	char	dia_alf[C_GEN_LEN_DIA + 1] = "";
	char	mes_alf[C_GEN_LEN_MES + 1] = "";
	char	anio_alf[C_GEN_LEN_ANIO + 1] = "";

	strncpy( dia_alf, ae_fecha, C_GEN_LEN_DIA );
	as_dia = atoi( dia_alf );
	strncpy( mes_alf, &ae_fecha[3], C_GEN_LEN_MES );
	as_mes = atoi( mes_alf );
	strncpy( anio_alf, &ae_fecha[6], C_GEN_LEN_ANIO );
	as_anio = atoi( anio_alf );

} /* fin de gen_desglosar_fechad */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_desglosar_fechah
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre          Tipo            Descripci�n
 *      --------------- -------------- ---------------------------------------
 *      ae_fecha	    char*			Fecha en cadena, formato 'DD-MM-YYYY'
 *		as_dia			unsigned int&	Dia en entero.
 *		as_mes			unsigned int&	Mes en entero.
 *		as_anio			unsigned int&	A�o en entero.
 *
 * DESCRIPCI�N:
 *		Desglosar una fecha en formato string ('DD-MM-YYYY') en 3 enteros con
 *		los valores correspondientes de dia, mes y a�o. Adem�s para fechas que
 *		corresponden a fechas finales de un periodo, le sumamos un d�a, para
 *		que en el periodo se incluya el propio d�a.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   26/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_desglosar_fechah( 
						char*			ae_fecha,
						unsigned int&	as_dia,
						unsigned int&	as_mes,
						unsigned int&	as_anio
					)
{ /* inicio funci�n */
/*
 ******************************************************************************
 *
 * Declaraci�n de variables.
 *
 ******************************************************************************
 */
 	char	v_dato[C_GEN_LEN_DATO + 1] = "";
	char	dia_alf[C_GEN_LEN_DIA + 1] = "";
	char	mes_alf[C_GEN_LEN_MES + 1] = "";
	char	anio_alf[C_GEN_LEN_ANIO + 1] = "";

/*
 ******************************************************************************
 *
 * Transformaci�n de la cadena.
 *
 ******************************************************************************
 */
	sprintf( v_dato,
			"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') + 1, 'DD-MM-YYYY')",
			ae_fecha );

	gen_obtener_dato( "DUAL",
						"",
						v_dato,
						ae_fecha );

	strncpy( dia_alf, ae_fecha, C_GEN_LEN_DIA );
	as_dia = atoi( dia_alf );
	strncpy( mes_alf, &ae_fecha[3], C_GEN_LEN_MES );
	as_mes = atoi( mes_alf );
	strcpy( anio_alf, &ae_fecha[6] );
	as_anio = atoi( anio_alf );

} /* fin de gen_desglosar_fechah */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_period
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------
 *      _form			ClinicForm*				Puntero a la solicitud
 *		as_fecha_d		char*					Fecha desde del periodo
 *		as_fecha_h		char*					Fecha hasta del periodo
 *
 *
 * DESCRIPCI�N:
 *		Lee de la solicitud el periodo de estudio y obtiene la fecha desde y
 *		la fecha hasta del mismo en formato 'dd-mm-yyyy'.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   13/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
 void gen_read_period( 
						ClinicForm*	_form, 
						char*		as_fecha_d,
						char*		as_fecha_h
					)
{ /* inicio funci�n */

	//Declaraci�n de variables.
	char	diad_alf[C_GEN_LEN_DIA + 1] = "";
	char	mesd_alf[C_GEN_LEN_MES + 1] = "";
	char	aniod_alf[C_GEN_LEN_ANIO + 1] = "";
	char	diah_alf[C_GEN_LEN_DIA + 1] = "";
	char	mesh_alf[C_GEN_LEN_MES + 1] = "";
	char	anioh_alf[C_GEN_LEN_ANIO + 1] = "";

	const IlcInt dia_d = _form->getPeriod().getDateFrom().getDay();
	if ( dia_d < 10 )
		sprintf( diad_alf, "0%d", dia_d );	
	else
		sprintf( diad_alf, "%d", dia_d );
	const IlcInt mes_d = _form->getPeriod().getDateFrom().getMonth();
	if ( mes_d < 10 )
		sprintf( mesd_alf, "0%d", mes_d );
	else
		sprintf( mesd_alf, "%d", mes_d );
	const IlcInt anio_d = _form->getPeriod().getDateFrom().getYear();
	sprintf( aniod_alf, "%d", anio_d );
	strcpy( as_fecha_d, diad_alf );
	strcat( as_fecha_d, "-" );
	strcat( as_fecha_d, mesd_alf );
	strcat( as_fecha_d, "-" );
	strcat( as_fecha_d, aniod_alf );

	const IlcInt dia_h = _form->getPeriod().getDateTo().getDay();
	if ( dia_h < 10 )
		sprintf( diah_alf, "0%d", dia_h );
	else
		sprintf( diah_alf, "%d", dia_h );
	const IlcInt mes_h = _form->getPeriod().getDateTo().getMonth();
	if ( mes_h < 10 )
		sprintf( mesh_alf, "0%d", mes_h );
	else
		sprintf( mesh_alf, "%d", mes_h );
	const IlcInt anio_h = _form->getPeriod().getDateTo().getYear();
	sprintf( anioh_alf, "%d", anio_h );
	strcpy( as_fecha_h, diah_alf );
	strcat( as_fecha_h, "-" );
	strcat( as_fecha_h, mesh_alf );
	strcat( as_fecha_h, "-" );
	strcat( as_fecha_h, anioh_alf );

} /* fin de gen_read_period */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_calendar
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *      mss				MedicalServiceSingle*   Puntero al recurso simple
 *		tStep			TimeStep
 *		timeStepPack	IlcInt
 *		ae_fecha_d		char*					Fecha desde periodo estudio
 *		ae_fecha_h		char*					Fecha hasta periodo estudio
 *
 *
 * DESCRIPCI�N:
 *		Construye el calendario de un recurso, teniendo en cuenta el calen-
 *		dario principal y el propio del recurso si lo tiene.
 *		Se considerar�n los peridos de vigencia dentro del periodo de 
 *		estudio, sus d�as festivos y sus d�as especiales.
 *
 * RESULTADOS:
 *      C�digo de error � no existencia de calendarios para recurso actual.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   13/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
long gen_read_calendar( 
						MedicalServiceSingle*	mss,
						TimeStep				tStep,
						IlcInt					timeStepPack,
						char*					ae_fecha_d,
						char*					ae_fecha_h
						)
{ /* inicio funci�n */

/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long	v_result = GENE0000_NORMAL;
	long			v_err = GENE0000_NORMAL;
	unsigned long	v_fecini_num = 0;
	unsigned long	v_fecfin_num = 0;
	unsigned long	v_fecd_num = 0;
	unsigned long	v_fech_num = 0;
	unsigned long	v_fecha_baja_num = 0;
	unsigned int	diad = 0;
	unsigned int	mesd = 0;
	unsigned int	aniod = 0;
	unsigned int	diah = 0;
	unsigned int	mesh = 0;
	unsigned int	anioh = 0;
	char	v_codcalprin[C_GEN_LEN_CODCAL + 1] = "";
	char	v_codcalrec[C_GEN_LEN_CODCAL + 1] = "";
	char	v_where[C_GEN_LEN_WHERE + 1] = "";
	char	v_period_d[C_GEN_LEN_FECHA + 1] = "";
	char	v_period_h[C_GEN_LEN_FECHA + 1] = "";
	char	v_fecha_baja[C_GEN_LEN_FECHA + 1] = "";
	char	v_dato[C_GEN_LEN_DATO + 1] = "";

/*
 *******************************************************************************
 *
 *	Control de errores de acceso a la B.D.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 
	
	const IlcInt codRecurso = mss->getCodRecurso();

	v_fecd_num = gen_fecha_numero( ae_fecha_d );
	v_fech_num = gen_fecha_numero( ae_fecha_h );

	strcpy( v_where, "AG02INDCALPRIN = -1" );
	gen_obtener_dato( "AG0200",
						v_where,
						"AG02CODCALENDA",
						v_codcalprin );

	sprintf( v_where, "AG11CODRECURSO = %d", codRecurso );
	gen_obtener_dato( "AG1100",
						v_where,
						"AG02CODCALENDA",
						v_codcalrec );

	if ( !strcmp( v_codcalprin, v_codcalrec ))
	{ //el calendario de recurso es el calendario principal.

		sprintf( (char*)h_select.arr,
			"SELECT TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),\
			AG08INDLUNFEST,AG08INDMARFEST,AG08INDMIEFEST,AG08INDJUEFEST,AG08INDVIEFEST,\
			AG08INDSABFEST,AG08INDDOMFEST FROM AG0800 WHERE AG02CODCALENDA=%s AND \
			((TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') <= TO_DATE( \'%s\','dd-mm-yyyy') \
			AND TO_DATE( \'%s\','dd-mm-yyyy') < TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
			AND TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') < \
			TO_DATE( \'%s\','dd-mm-yyyy')) OR \
			(TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
			<= TO_DATE(\'%s\','dd-mm-yyyy') AND TO_DATE(\'%s\','dd-mm-yyyy') < \
			TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
			TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
			>= TO_DATE(\'%s\','dd-mm-yyyy')) OR \
			(TO_DATE(\'%s\','dd-mm-yyyy') < TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
			TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') < \
			TO_DATE(\'%s\','dd-mm-yyyy')) OR \
			(TO_DATE(\'%s\','dd-mm-yyyy') < \
			TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
			TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') < \
			TO_DATE(\'%s\','dd-mm-yyyy') AND TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
			>= TO_DATE(\'%s\','dd-mm-yyyy')))",
			v_codcalprin, ae_fecha_d, ae_fecha_d, ae_fecha_h,
			ae_fecha_d, ae_fecha_d, ae_fecha_h,
			ae_fecha_d, ae_fecha_h,
			ae_fecha_d, ae_fecha_h, ae_fecha_h );
		
		//A�adir fin de cadena a h_select.
		h_select.len = strlen((char*)h_select.arr );
		h_select.arr[h_select.len] = '\0';

		//Preparar sentencia.
		/* EXEC SQL PREPARE SENTENCIACAL FROM :h_select; */ 
{
  struct sqlexd sqlstm={8,1};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )266;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
  sqlstm.sqhstl[0] = (unsigned int  )3003;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Declarar y abrir cursor para sentencia anterior.
		/* EXEC SQL DECLARE sql_cursor_cal CURSOR FOR SENTENCIACAL; */ 
		/* EXEC SQL OPEN sql_cursor_cal; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )284;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Bucle para recuperar los registros del cursor.
		while(1)
		{

			strcpy( h_datos_cal.h_fecha_ini, "" );
			strcpy( h_datos_cal.h_fecha_fin, "" );
			h_datos_cal.h_indlu = 0;
			h_datos_cal.h_indma = 0;
			h_datos_cal.h_indmi = 0;
			h_datos_cal.h_indju = 0;
			h_datos_cal.h_indvi = 0;
			h_datos_cal.h_indsa = 0;
			h_datos_cal.h_inddo = 0;

			/* EXEC SQL FETCH sql_cursor_cal INTO :h_datos_cal; */ 
{
   struct sqlexd sqlstm={8,9};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )298;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)h_datos_cal.h_fecha_ini;
   sqlstm.sqhstl[0] = (unsigned int  )11;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqhstv[1] = (unsigned char  *)h_datos_cal.h_fecha_fin;
   sqlstm.sqhstl[1] = (unsigned int  )11;
   sqlstm.sqindv[1] = (         short *)0;
   sqlstm.sqharm[1] = (unsigned int  )0;
   sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_cal.h_indlu;
   sqlstm.sqhstl[2] = (unsigned int  )4;
   sqlstm.sqindv[2] = (         short *)0;
   sqlstm.sqharm[2] = (unsigned int  )0;
   sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_cal.h_indma;
   sqlstm.sqhstl[3] = (unsigned int  )4;
   sqlstm.sqindv[3] = (         short *)0;
   sqlstm.sqharm[3] = (unsigned int  )0;
   sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_cal.h_indmi;
   sqlstm.sqhstl[4] = (unsigned int  )4;
   sqlstm.sqindv[4] = (         short *)0;
   sqlstm.sqharm[4] = (unsigned int  )0;
   sqlstm.sqhstv[5] = (unsigned char  *)&h_datos_cal.h_indju;
   sqlstm.sqhstl[5] = (unsigned int  )4;
   sqlstm.sqindv[5] = (         short *)0;
   sqlstm.sqharm[5] = (unsigned int  )0;
   sqlstm.sqhstv[6] = (unsigned char  *)&h_datos_cal.h_indvi;
   sqlstm.sqhstl[6] = (unsigned int  )4;
   sqlstm.sqindv[6] = (         short *)0;
   sqlstm.sqharm[6] = (unsigned int  )0;
   sqlstm.sqhstv[7] = (unsigned char  *)&h_datos_cal.h_indsa;
   sqlstm.sqhstl[7] = (unsigned int  )4;
   sqlstm.sqindv[7] = (         short *)0;
   sqlstm.sqharm[7] = (unsigned int  )0;
   sqlstm.sqhstv[8] = (unsigned char  *)&h_datos_cal.h_inddo;
   sqlstm.sqhstl[8] = (unsigned int  )4;
   sqlstm.sqindv[8] = (         short *)0;
   sqlstm.sqharm[8] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode == 1403) break;
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			if ( v_result != GENE0000_NORMAL )
			{
				return (v_result);
			}

			v_fecini_num = gen_fecha_numero( h_datos_cal.h_fecha_ini );
			v_fecfin_num = gen_fecha_numero( h_datos_cal.h_fecha_fin );
			
			if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
				&& v_fecfin_num < v_fech_num )
			{ /* Periodo que empieza antes del periodo de estudio y finaliza
				antes que dicho periodo */

				//Construir periodo para calendario en curso.
				//Fecha desde.
				strcpy( v_period_d, ae_fecha_d );
				gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

				//Fecha hasta.
				strcpy( v_period_h, h_datos_cal.h_fecha_fin );
				gen_desglosar_fechah( v_period_h, diah, mesh, anioh );

			} /* fin del if */

			if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
				&& v_fecfin_num >= v_fech_num )
			{ /* Periodo que empieza antes del periodo de estudio y finaliza
				despu�s que dicho periodo */

				//Construir periodo para calendario en curso.
				//Fecha desde.
				strcpy( v_period_d, ae_fecha_d );
				gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

				//Fecha hasta.
				strcpy( v_period_h, ae_fecha_h );
				gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

			} /* fin del if */

			if ( v_fecd_num < v_fecini_num && v_fecfin_num < v_fech_num )
			{ /* Periodo comprendido dentro del periodo de estudio */

				//Construir periodo para calendario en curso.
				//Fecha desde.
				strcpy( v_period_d, h_datos_cal.h_fecha_ini ); 
				gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

				//Fecha hasta.
				strcpy( v_period_h, h_datos_cal.h_fecha_fin );
				gen_desglosar_fechah( v_period_h, diah, mesh, anioh );

			} /* fin del if */

			if ( v_fecd_num < v_fecini_num && v_fecini_num < v_fech_num
				&& v_fecfin_num >= v_fech_num )
			{ /* Periodo que comienza en periodo de estudio y finaliza 
				despu�s que el periodo de estudio */

				//Construir periodo para calendario en curso.
				//Fecha desde.
				strcpy( v_period_d, h_datos_cal.h_fecha_ini );
				gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

				//Fecha hasta.
				strcpy( v_period_h, ae_fecha_h );
				gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

			} /* fin del if */

			Period p1(Date(diad,mesd,aniod), Date(diah,mesh,anioh));
			WeekIndexedCalendar* calendar = new WeekIndexedCalendar(p1,
													 tStep, timeStepPack);

			if ( h_datos_cal.h_indlu == -1 )
			{
				calendar->setWeekDayHoliday(LUNES, IlcTrue);
			}
			if ( h_datos_cal.h_indma == -1 )
			{
				calendar->setWeekDayHoliday(MARTES, IlcTrue);
			}
			if ( h_datos_cal.h_indmi == -1 )
			{
				calendar->setWeekDayHoliday(MIERCOLES, IlcTrue);
			}
			if ( h_datos_cal.h_indju == -1 )
			{
				calendar->setWeekDayHoliday(JUEVES, IlcTrue);
			}
			if ( h_datos_cal.h_indvi == -1 )
			{
				calendar->setWeekDayHoliday(VIERNES, IlcTrue);
			}
			if ( h_datos_cal.h_indsa == -1 )
			{			
				calendar->setWeekDayHoliday(SABADO, IlcTrue);
			}
			if ( h_datos_cal.h_inddo == -1 )
			{			
				calendar->setWeekDayHoliday(DOMINGO, IlcTrue);
			}

			sprintf( (char*)h_select_aux.arr,
				"SELECT TO_CHAR(AG03FECDIAESPE,'DD-MM-YYYY'),AG03INDFESTIVO FROM AG0300 \
				WHERE AG02CODCALENDA=%s AND TO_DATE(TO_CHAR(AG03FECDIAESPE,'DD-MM-YYYY'),'DD-MM-YYYY') \
				BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND TO_DATE( \'%s\','DD-MM-YYYY') \
				AND ( AG03FECBAJA IS NULL OR AG03FECBAJA > AG03FECDIAESPE )",
				v_codcalprin, v_period_d, v_period_h );

			//A�adir fin de cadena a h_select_aux.
			h_select_aux.len = strlen((char*)h_select_aux.arr );
			h_select_aux.arr[h_select_aux.len] = '\0';

			//Preparar sentencia.
			/* EXEC SQL PREPARE SENTENCIADIA FROM :h_select_aux; */ 
{
   struct sqlexd sqlstm={8,1};
   sqlstm.stmt = "";
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )348;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)&h_select_aux;
   sqlstm.sqhstl[0] = (unsigned int  )3003;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			//Declarar y abrir cursor para sentencia anterior.
			/* EXEC SQL DECLARE sql_cursor_dia CURSOR FOR SENTENCIADIA; */ 
			/* EXEC SQL OPEN sql_cursor_dia; */ 
{
   struct sqlexd sqlstm={8,0};
   sqlstm.stmt = "";
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )366;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			//Bucle para recuperar los registros del cursor.
			while(1)
			{

				/* EXEC SQL FETCH sql_cursor_dia INTO :h_datos_diaesp; */ 
{
    struct sqlexd sqlstm={8,2};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )380;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)h_datos_diaesp.h_fecha_dia;
    sqlstm.sqhstl[0] = (unsigned int  )11;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_diaesp.h_indfes;
    sqlstm.sqhstl[1] = (unsigned int  )4;
    sqlstm.sqindv[1] = (         short *)0;
    sqlstm.sqharm[1] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode == 1403) break;
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				if ( v_result != GENE0000_NORMAL )
				{
					return (v_result);
				}

				gen_desglosar_fechad( h_datos_diaesp.h_fecha_dia,
															diah, mesh, anioh );

				if ( h_datos_diaesp.h_indfes == -1 )
				{ /* D�a especial que siendo no festivo pasa a serlo */

					calendar->setHoliday(Date(diah,mesh,anioh));

				} /* fin del if */
				else
				{ /* D�a especial que siendo festivo pasa a no serlo */
				
					calendar->setHoliday(Date(diah,mesh,anioh), IlcFalse);

				} /* fin del else */
				
			} /* fin del while */
			
			//Cerrar el cursor.
			/* EXEC SQL CLOSE sql_cursor_dia; */ 
{
   struct sqlexd sqlstm={8,0};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )402;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			mss->addCalendar(calendar);

		} /* fin del while */
			
		//Cerrar el cursor.
		/* EXEC SQL CLOSE sql_cursor_cal; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )416;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( sqlca.sqlerrd[2] == 0 )
		{ //Ning�n calendario

			v_err = GENE0004_ERROR_NO_CALENDARIO;
			return (v_err);

		} /* fin del if */

	} /* fin del if */
	else
	{ //el calendario de recurso es diferente del principal

		sprintf( v_where, "AG02CODCALENDA = %s", v_codcalrec );

		gen_obtener_dato( "AG0200",
						  v_where,
						  "TO_CHAR(AG02FECBAJA,'DD-MM-YYYY')",
						  v_fecha_baja );

		v_fecha_baja_num = gen_fecha_numero( v_fecha_baja );

		if ( v_fecd_num < v_fecha_baja_num && v_fecha_baja_num <= v_fech_num )
		{ /* Fecha baja dentro del periodo de estudio */

			sprintf( v_dato,
				"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') - 1, 'DD-MM-YYYY')",
				v_fecha_baja );
			gen_obtener_dato( "DUAL",
								"",
								v_dato,
								v_fecha_baja );

			strcpy( ae_fecha_h, v_fecha_baja );

		} /* fin del if */

		if (( v_fecha_baja_num == 0 || v_fecha_baja_num > v_fech_num ) ||
			( v_fecd_num < v_fecha_baja_num && v_fecha_baja_num <= v_fech_num ))
		{ /* calendario no de baja � de baja en periodo de estudio */

			sprintf( (char*)h_select.arr,
				"SELECT TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),\
				AG08INDLUNFEST,AG08INDMARFEST,AG08INDMIEFEST,AG08INDJUEFEST,AG08INDVIEFEST,\
				AG08INDSABFEST,AG08INDDOMFEST FROM AG0800 WHERE AG02CODCALENDA=%s AND \
				((TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') <= TO_DATE( \'%s\','dd-mm-yyyy') \
				AND TO_DATE( \'%s\','dd-mm-yyyy') < TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
				AND TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') < \
				TO_DATE( \'%s\','dd-mm-yyyy')) OR \
				(TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
				<= TO_DATE(\'%s\','dd-mm-yyyy') AND TO_DATE(\'%s\','dd-mm-yyyy') < \
				TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
				TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
				>= TO_DATE(\'%s\','dd-mm-yyyy')) OR \
				(TO_DATE(\'%s\','dd-mm-yyyy') < TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
				TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') < \
				TO_DATE(\'%s\','dd-mm-yyyy')) OR \
				(TO_DATE(\'%s\','dd-mm-yyyy') < \
				TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
				TO_DATE(TO_CHAR(AG08FECINIPERI,'dd-mm-yyyy'),'dd-mm-yyyy') < \
				TO_DATE(\'%s\','dd-mm-yyyy') AND TO_DATE(TO_CHAR(AG08FECFINPERI,'dd-mm-yyyy'),'dd-mm-yyyy') \
				>= TO_DATE(\'%s\','dd-mm-yyyy')))",
				v_codcalrec, ae_fecha_d, ae_fecha_d, ae_fecha_h,
				ae_fecha_d, ae_fecha_d, ae_fecha_h,
				ae_fecha_d, ae_fecha_h,
				ae_fecha_d, ae_fecha_h, ae_fecha_h );
	
			//A�adir fin de cadena a h_select.
			h_select.len = strlen((char*)h_select.arr );
			h_select.arr[h_select.len] = '\0';

			//Preparar sentencia.
			/* EXEC SQL PREPARE SENTENCIACALR FROM :h_select; */ 
{
   struct sqlexd sqlstm={8,1};
   sqlstm.stmt = "";
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )430;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
   sqlstm.sqhstl[0] = (unsigned int  )3003;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			//Declarar y abrir cursor para sentencia anterior.
			/* EXEC SQL DECLARE sql_cursor_calr CURSOR FOR SENTENCIACALR; */ 
			/* EXEC SQL OPEN sql_cursor_calr; */ 
{
   struct sqlexd sqlstm={8,0};
   sqlstm.stmt = "";
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )448;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			//Bucle para recuperar los registros del cursor.
			while(1)
			{

				strcpy( h_datos_cal.h_fecha_ini, "" );
				strcpy( h_datos_cal.h_fecha_fin, "" );
				h_datos_cal.h_indlu = 0;
				h_datos_cal.h_indma = 0;
				h_datos_cal.h_indmi = 0;
				h_datos_cal.h_indju = 0;
				h_datos_cal.h_indvi = 0;
				h_datos_cal.h_indsa = 0;
				h_datos_cal.h_inddo = 0;

				/* EXEC SQL FETCH sql_cursor_calr INTO :h_datos_cal; */ 
{
    struct sqlexd sqlstm={8,9};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )462;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)h_datos_cal.h_fecha_ini;
    sqlstm.sqhstl[0] = (unsigned int  )11;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqhstv[1] = (unsigned char  *)h_datos_cal.h_fecha_fin;
    sqlstm.sqhstl[1] = (unsigned int  )11;
    sqlstm.sqindv[1] = (         short *)0;
    sqlstm.sqharm[1] = (unsigned int  )0;
    sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_cal.h_indlu;
    sqlstm.sqhstl[2] = (unsigned int  )4;
    sqlstm.sqindv[2] = (         short *)0;
    sqlstm.sqharm[2] = (unsigned int  )0;
    sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_cal.h_indma;
    sqlstm.sqhstl[3] = (unsigned int  )4;
    sqlstm.sqindv[3] = (         short *)0;
    sqlstm.sqharm[3] = (unsigned int  )0;
    sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_cal.h_indmi;
    sqlstm.sqhstl[4] = (unsigned int  )4;
    sqlstm.sqindv[4] = (         short *)0;
    sqlstm.sqharm[4] = (unsigned int  )0;
    sqlstm.sqhstv[5] = (unsigned char  *)&h_datos_cal.h_indju;
    sqlstm.sqhstl[5] = (unsigned int  )4;
    sqlstm.sqindv[5] = (         short *)0;
    sqlstm.sqharm[5] = (unsigned int  )0;
    sqlstm.sqhstv[6] = (unsigned char  *)&h_datos_cal.h_indvi;
    sqlstm.sqhstl[6] = (unsigned int  )4;
    sqlstm.sqindv[6] = (         short *)0;
    sqlstm.sqharm[6] = (unsigned int  )0;
    sqlstm.sqhstv[7] = (unsigned char  *)&h_datos_cal.h_indsa;
    sqlstm.sqhstl[7] = (unsigned int  )4;
    sqlstm.sqindv[7] = (         short *)0;
    sqlstm.sqharm[7] = (unsigned int  )0;
    sqlstm.sqhstv[8] = (unsigned char  *)&h_datos_cal.h_inddo;
    sqlstm.sqhstl[8] = (unsigned int  )4;
    sqlstm.sqindv[8] = (         short *)0;
    sqlstm.sqharm[8] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode == 1403) break;
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				if ( v_result != GENE0000_NORMAL )
				{
					return (v_result);
				}

				v_fecini_num = gen_fecha_numero( h_datos_cal.h_fecha_ini );
				v_fecfin_num = gen_fecha_numero( h_datos_cal.h_fecha_fin );

				v_fech_num = gen_fecha_numero( ae_fecha_h );

				if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
					&& v_fecfin_num < v_fech_num )
				{ /* Periodo que empieza antes del periodo de estudio y finaliza
					antes que dicho periodo */

					strcpy( v_period_d, ae_fecha_d );
					gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

					strcpy( v_period_h, h_datos_cal.h_fecha_fin );
					gen_desglosar_fechah( v_period_h, diah, mesh, anioh );

				} /* fin del if */

				if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
					&& v_fecfin_num >= v_fech_num )
				{ /* Periodo que empieza antes del periodo de estudio y finaliza
					despu�s que dicho periodo */

					strcpy( v_period_d, ae_fecha_d );
					gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

					strcpy( v_period_h, ae_fecha_h );
					gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

				} /* fin del if */

				if ( v_fecd_num < v_fecini_num && v_fecfin_num < v_fech_num )
				{ /* Periodo comprendido dentro del periodo de estudio */

					strcpy( v_period_d, h_datos_cal.h_fecha_ini );
					gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

					strcpy( v_period_h, h_datos_cal.h_fecha_fin );
					gen_desglosar_fechah( v_period_h, diah, mesh, anioh );

				} /* fin del if */

				if ( v_fecd_num < v_fecini_num && v_fecini_num < v_fech_num
					&& v_fecfin_num >= v_fech_num )
				{ /* Periodo que comienza en periodo de estudio y finaliza 
					despu�s que el periodo de estudio */

					strcpy( v_period_d, h_datos_cal.h_fecha_ini );
					gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

					strcpy( v_period_h, ae_fecha_h );
					gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

				} /* fin del if */

				Period p1(Date(diad,mesd,aniod), Date(diah,mesh,anioh));
				WeekIndexedCalendar* calendar = new WeekIndexedCalendar(p1,
                                                 tStep, timeStepPack);

				if ( h_datos_cal.h_indlu == -1 )
				{
					calendar->setWeekDayHoliday(LUNES, IlcTrue);
				}
				if ( h_datos_cal.h_indma == -1 )
				{
					calendar->setWeekDayHoliday(MARTES, IlcTrue);
				}
				if ( h_datos_cal.h_indmi == -1 )
				{
					calendar->setWeekDayHoliday(MIERCOLES, IlcTrue);
				}
				if ( h_datos_cal.h_indju == -1 )
				{
					calendar->setWeekDayHoliday(JUEVES, IlcTrue);
				}
				if ( h_datos_cal.h_indvi == -1 )
				{
					calendar->setWeekDayHoliday(VIERNES, IlcTrue);
				}
				if ( h_datos_cal.h_indsa == -1 )
				{			
					calendar->setWeekDayHoliday(SABADO, IlcTrue);
				}
				if ( h_datos_cal.h_inddo == -1 )
				{			
					calendar->setWeekDayHoliday(DOMINGO, IlcTrue);
				}

				sprintf( (char*)h_select_aux.arr,
					"SELECT TO_CHAR(AG03FECDIAESPE,'DD-MM-YYYY'),AG03INDFESTIVO FROM AG0300 \
					WHERE AG02CODCALENDA=%s AND TO_DATE(TO_CHAR(AG03FECDIAESPE,'DD-MM-YYYY'),'DD-MM-YYYY') \
					BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND TO_DATE( \'%s\','DD-MM-YYYY') \
					AND ( AG03FECBAJA IS NULL OR AG03FECBAJA > AG03FECDIAESPE )",
					v_codcalrec, v_period_d, v_period_h );

				//A�adir fin de cadena a h_select_aux.
				h_select_aux.len = strlen((char*)h_select_aux.arr );
				h_select_aux.arr[h_select_aux.len] = '\0';

				//Preparar sentencia.
				/* EXEC SQL PREPARE SENTENCIADIAR FROM :h_select_aux; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )512;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_select_aux;
    sqlstm.sqhstl[0] = (unsigned int  )3003;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				//Declarar y abrir cursor para sentencia anterior.
				/* EXEC SQL DECLARE sql_cursor_diar CURSOR FOR SENTENCIADIAR; */ 
				/* EXEC SQL OPEN sql_cursor_diar; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )530;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				//Bucle para recuperar los registros del cursor.
				while(1)
				{

					/* EXEC SQL FETCH sql_cursor_diar INTO :h_datos_diaesp; */ 
{
     struct sqlexd sqlstm={8,2};
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )544;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlstm.sqhstv[0] = (unsigned char  *)h_datos_diaesp.h_fecha_dia;
     sqlstm.sqhstl[0] = (unsigned int  )11;
     sqlstm.sqindv[0] = (         short *)0;
     sqlstm.sqharm[0] = (unsigned int  )0;
     sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_diaesp.h_indfes;
     sqlstm.sqhstl[1] = (unsigned int  )4;
     sqlstm.sqindv[1] = (         short *)0;
     sqlstm.sqharm[1] = (unsigned int  )0;
     sqlstm.sqphsv = sqlstm.sqhstv;
     sqlstm.sqphsl = sqlstm.sqhstl;
     sqlstm.sqpind = sqlstm.sqindv;
     sqlstm.sqparm = sqlstm.sqharm;
     sqlstm.sqparc = sqlstm.sqharc;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode == 1403) break;
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


					if ( v_result != GENE0000_NORMAL )
					{
						return (v_result);
					}

					gen_desglosar_fechad( h_datos_diaesp.h_fecha_dia,
														diah, mesh, anioh );

					if ( h_datos_diaesp.h_indfes == -1 )
					{ /* D�a especial que siendo no festivo pasa a serlo */

						calendar->setHoliday(Date(diah,mesh,anioh));

					} /* fin del if */
					else
					{ /* D�a especial que siendo festivo pasa a no serlo */
			
						calendar->setHoliday(Date(diah,mesh,anioh), IlcFalse);

					} /* fin del else */
			
				} /* fin del while */
		
				//Cerrar el cursor.
				/* EXEC SQL CLOSE sql_cursor_diar; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )566;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				mss->addCalendar(calendar);

			} /* fin del while */
		
			//Cerrar el cursor.
			/* EXEC SQL CLOSE sql_cursor_calr; */ 
{
   struct sqlexd sqlstm={8,0};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )580;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			if ( sqlca.sqlerrd[2] == 0 )
			{ //Ning�n calendario

				v_err = GENE0004_ERROR_NO_CALENDARIO;
				return (v_err);

			} /* fin del if */

		} /* fin del if */
		else
		{ //calendario del recurso actual est� de baja

			v_err = GENE0004_ERROR_NO_CALENDARIO;
			return (v_err);

		} /* fin del else */

	} /* fin del else */

	return (v_err);

} /* fin de funci�n gen_read_calendar */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_acotar_period_rec
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *		codRecurso		IlcInt					C�digo del Recurso
 *		ae_period_fechadchar*					Fecha desde periodo estudio
 *		ae_period_fechahchar*					Fecha hasta periodo estudio
 *		as_fecha_d		char*					Fecha desde periodo estudio
 *												para recurso actual, teniendo
 *												en cuenta su periodo de vigencia
 *		as_fecha_h		char*					Fecha hasta periodo estudio
 *												para recurso actual, teniendo
 *												en cuenta su periodo de vigencia
 *
 * DESCRIPCI�N:
 *		Acota el periodo de estudio para el recurso actual, teniendo en cuenta
 *		su periodo de vigencia.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   27/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_acotar_period_rec( 
						const IlcInt	codRecurso,
						char*			ae_period_fechad,
						char*			ae_period_fechah,
						char*			as_fecha_d,
						char*			as_fecha_h
						)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long	v_result = GENE0000_NORMAL;
	char			v_where[C_GEN_LEN_WHERE + 1] = "";
	char			v_fecha_ini[C_GEN_LEN_FECHA + 1] = "";
	char			v_fecha_fin[C_GEN_LEN_FECHA + 1] = "";
	char			v_dato[C_GEN_LEN_DATO + 1] = "";
	unsigned long	v_fecha_ini_num = 0;
	unsigned long	v_fecha_fin_num = 0;
	unsigned long	v_period_fechad_num = 0;
	unsigned long	v_period_fechah_num = 0;

	v_period_fechad_num = gen_fecha_numero( ae_period_fechad );
	v_period_fechah_num = gen_fecha_numero( ae_period_fechah );

	sprintf( v_where, "AG11CODRECURSO = %d", codRecurso );

	gen_obtener_dato( "AG1100",
						v_where,
						"TO_CHAR(AG11FECINIVREC,'DD-MM-YYYY')",
						v_fecha_ini );

	v_fecha_ini_num = gen_fecha_numero( v_fecha_ini );

	gen_obtener_dato( "AG1100",
						v_where,
						"TO_CHAR(AG11FECFINVREC,'DD-MM-YYYY')",
						v_fecha_fin );

	sprintf( v_dato,
		"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') + 1, 'DD-MM-YYYY')",
		v_fecha_fin );
	gen_obtener_dato( "DUAL",
						"",
						v_dato,
						v_fecha_fin );

	v_fecha_fin_num = gen_fecha_numero( v_fecha_fin );

	if ( v_fecha_fin_num == 0 )
	{ /* sin fecha fin de vigencia del recurso */

		strcpy( as_fecha_h, ae_period_fechah );

		if ( v_fecha_ini_num <= v_period_fechad_num )
		{ /* Vigencia que empieza antes del periodo de estudio */

			strcpy( as_fecha_d, ae_period_fechad );

		} /* fin del if */

		if ( v_period_fechad_num < v_fecha_ini_num )
		{ /* vigencia posterior al comienzo de periodo de estudio */

			strcpy( as_fecha_d, v_fecha_ini );

		} /* fin del if */

	} /* fin del if */

	if ( v_fecha_ini_num <= v_period_fechad_num && v_period_fechad_num < 
		v_fecha_fin_num && v_fecha_fin_num < v_period_fechah_num )
	{ /* Vigencia que empieza antes del periodo de estudio y finaliza
		antes que dicho periodo */

		strcpy( as_fecha_d, ae_period_fechad );

		strcpy( as_fecha_h, v_fecha_fin );

	} /* fin del if */

	if ( v_fecha_ini_num <= v_period_fechad_num && v_period_fechad_num < 
		v_fecha_fin_num && v_fecha_fin_num >= v_period_fechah_num )
	{ /* Vigencia que empieza antes del periodo de estudio y finaliza
		despu�s que dicho periodo */

		strcpy( as_fecha_d, ae_period_fechad );

		strcpy( as_fecha_h, ae_period_fechah );

	} /* fin del if */

	if ( v_period_fechad_num < v_fecha_ini_num && v_fecha_fin_num < 
		v_period_fechah_num && v_fecha_fin_num != 0 )
	{ /* Vigencia comprendida dentro del periodo de estudio */

		strcpy( as_fecha_d, v_fecha_ini );

		strcpy( as_fecha_h, v_fecha_fin );

	} /* fin del if */

	if ( v_period_fechad_num < v_fecha_ini_num && v_fecha_ini_num <
		 v_period_fechah_num && v_fecha_fin_num >= v_period_fechah_num )
	{ /* Vigencia que comienza en periodo de estudio y finaliza 
		despu�s que el periodo de estudio */

		strcpy( as_fecha_d, v_fecha_ini );

		strcpy( as_fecha_h, ae_period_fechah );

	} /* fin del if */

} /* fin de gen_acotar_period_rec */


/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_recursos_simples
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre		Tipo							Descripci�n
 *      ------		-----------------------			----------------
 *      msa			MedicalServiceAlternative*		Puntero al recurso alternat.
 *		codTypeRec	const IlcInt					C�digo Tipo recurso
 *
 * DESCRIPCI�N:
 *		Construye los recursos simples planificables asociados a un tipo de 
 *		recurso, con sus perfiles, franjas y restricciones.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   05/12/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
long
gen_read_recursos_simples( 
							MedicalServiceAlternative*	msa,
							const IlcInt				codTypeRec,
                            const IlcInt                codMedicalFact,
                            const IlcInt                codDpto, 
                            const IlcInt                numPhase

						)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long			v_result = GENE0000_NORMAL;
	long					v_err = GENE0000_NORMAL;
	MedicalServiceSingle*	mss;
	char					v_where[C_GEN_LEN_WHERE + 1] = "";
	unsigned int			v_count_rec = 0;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

	if ( codDpto != 0 )
		sprintf( v_where, "PR01CODACTUACION= %d AND PR1100.AD02CODDPTO = %d \
						   AND PR05NUMFASE = %d AND PR1100.AD02CODDPTO = \
						   AG1100.AD02CODDPTO \
						   AND PR1100.AG11CODRECURSO=AG1100.AG11CODRECURSO AND \
						   AG11INDPLANIFI = -1 AND AG14CODTIPRECU=%d AND \
						   ( AG11FECFINVREC IS NULL OR AG11FECFINVREC > SYSDATE )",
				 codMedicalFact, codDpto, numPhase, codTypeRec );
	else
		sprintf( v_where, "PR01CODACTUACION= %d AND PR05NUMFASE = %d AND \
						   PR1100.AG11CODRECURSO=AG1100.AG11CODRECURSO AND \
						   AG11INDPLANIFI = -1 AND AG14CODTIPRECU=%d AND \
						   ( AG11FECFINVREC IS NULL OR AG11FECFINVREC > SYSDATE)",
				 codMedicalFact, numPhase, codTypeRec );

	gen_count_registros( "PR1100,AG1100",
						 v_where,
						 v_count_rec );

	if ( v_count_rec == 0 )
	{ /* no hay recursos especificados para fase actual */

		if ( codDpto != 0 )
			sprintf( (char*)h_select.arr,
				"SELECT AG11CODRECURSO FROM AG1100 WHERE AG14CODTIPRECU = %d AND \
				AG11INDPLANIFI=-1 AND AD02CODDPTO = %d AND \
				( AG11FECFINVREC IS NULL OR \
				AG11FECFINVREC > SYSDATE)",
				codTypeRec, codDpto );
		else
			sprintf( (char*)h_select.arr,
				"SELECT AG11CODRECURSO FROM AG1100 WHERE AG14CODTIPRECU = %d AND \
				AG11INDPLANIFI=-1 AND ( AG11FECFINVREC IS NULL OR \
				AG11FECFINVREC > SYSDATE)",
				codTypeRec);

		//A�adir fin de cadena a h_select.
		h_select.len = strlen((char*)h_select.arr );
		h_select.arr[h_select.len] = '\0';

		//Preparar sentencia.
		/* EXEC SQL PREPARE SENTENCIAREC FROM :h_select; */ 
{
  struct sqlexd sqlstm={8,1};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )594;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
  sqlstm.sqhstl[0] = (unsigned int  )3003;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Declarar y abrir cursor para sentencia anterior.
		/* EXEC SQL DECLARE sql_cursor_rec CURSOR FOR SENTENCIAREC; */ 
		/* EXEC SQL OPEN sql_cursor_rec; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )612;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Bucle para recuperar los registros del cursor.
		while(1)
		{		

			/* EXEC SQL FETCH sql_cursor_rec INTO :h_cod_recurso; */ 
{
   struct sqlexd sqlstm={8,1};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )626;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_recurso;
   sqlstm.sqhstl[0] = (unsigned int  )4;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode == 1403) break;
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			if ( v_result != GENE0000_NORMAL )
			{
				return (v_result);
			}

			mss = data_access->readMedicalService( h_cod_recurso, v_err );

			if ( v_err != GENE0000_NORMAL )
			{
				return (v_err);

			}

			//A�ade el recurso simple al recurso alternativo.
			msa->addMedicalService(mss);

		} /* fin del while */
			
		//Cerrar el cursor.
		/* EXEC SQL CLOSE sql_cursor_rec; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )644;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( sqlca.sqlerrd[2] == 0 )
		{ //sin recursos simples que cumplan los requisitos de la petici�n.

			v_err = GENE0005_ERROR_NO_RECURSOS;
			return (v_err);

		} /* fin del if */

	} /* fin del if */
	else
	{
		if ( codDpto != 0 )
			sprintf( (char*)h_select_aux.arr,
				"SELECT DISTINCT PR1100.AG11CODRECURSO FROM PR1100,AG1100 WHERE \
				PR01CODACTUACION = %d AND PR1100.AD02CODDPTO = %d AND \
				PR1100.AD02CODDPTO = AG1100.AD02CODDPTO AND PR05NUMFASE=%d \
				AND PR1100.AG11CODRECURSO=AG1100.AG11CODRECURSO AND \
				AG11INDPLANIFI = -1 AND AG14CODTIPRECU=%d AND \
				( AG11FECFINVREC IS NULL OR AG11FECFINVREC \
				> SYSDATE )",
				codMedicalFact, codDpto, numPhase, codTypeRec );
		else
			sprintf( (char*)h_select_aux.arr,
				"SELECT DISTINCT PR1100.AG11CODRECURSO FROM PR1100,AG1100 WHERE \
				PR01CODACTUACION = %d AND PR05NUMFASE=%d AND \
				PR1100.AG11CODRECURSO=AG1100.AG11CODRECURSO AND \
				AG11INDPLANIFI = -1 AND AG14CODTIPRECU=%d AND \
				( AG11FECFINVREC IS NULL OR AG11FECFINVREC \
				> SYSDATE )",
				codMedicalFact, numPhase, codTypeRec );

		//A�adir fin de cadena a h_select_aux.
		h_select_aux.len = strlen((char*)h_select_aux.arr );
		h_select_aux.arr[h_select_aux.len] = '\0';

		//Preparar sentencia.
		/* EXEC SQL PREPARE SENTENCIAUX FROM :h_select_aux; */ 
{
  struct sqlexd sqlstm={8,1};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )658;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_select_aux;
  sqlstm.sqhstl[0] = (unsigned int  )3003;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Declarar y abrir cursor para sentencia anterior.
		/* EXEC SQL DECLARE sql_cursor_aux CURSOR FOR SENTENCIAUX; */ 
		/* EXEC SQL OPEN sql_cursor_aux; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )676;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Bucle para recuperar los registros del cursor.
		while(1)
		{							
			/* EXEC SQL FETCH sql_cursor_aux INTO :h_cod_recurso; */ 
{
   struct sqlexd sqlstm={8,1};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )690;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_recurso;
   sqlstm.sqhstl[0] = (unsigned int  )4;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode == 1403) break;
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

					
			if ( v_result != GENE0000_NORMAL )
			{

				return (v_result);

			} /* fin del if */

			mss = data_access->readMedicalService( h_cod_recurso, v_err );

			if ( v_err == GENE0000_NORMAL )
			{ // recurso correcto.

				msa->addMedicalService(mss);

			} /* fin del if */

		} /* fin del while */
			
		//Cerrar el cursor.
		/* EXEC SQL CLOSE sql_cursor_aux; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )708;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	} /* fin del else */

	return (v_err);

} /* fin de gen_read_recursos_simples */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_profiles_bands
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *      mss				MedicalServiceSingle*   Puntero al recurso simple
 *		app				ClinicApplication*		Puntero a datos generales
 *												de la cl�nica.
 *		appScheduler	ClinicApplicationScheduler* Puntero a datos generales
 *												de la cl�nica, utilizados por
 *												el scheduler.
 *		codRecurso		IlcInt					C�digo del Recurso
 *		ae_fecha_d		char*					Fecha desde periodo estudio
 *		ae_fecha_h		char*					Fecha hasta periodo estudio
 *
 *
 * DESCRIPCI�N:
 *		Construye los perfiles de un recurso, teniendo en cuenta el perfil
 *		principal y los propios del recurso si los tiene.
 *		Se considerar�n los periodos de vigencia dentro del periodo de 
 *		estudio.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   17/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void gen_read_profiles_bands( 
								MedicalServiceSingle*		mss,
								ClinicApplication*			app,
								ClinicApplicationScheduler*	appScheduler,
								const IlcInt				codRecurso,
								char*						ae_fecha_d,
								char*						ae_fecha_h
							)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
 	MedicalServiceCalendarProfile*	profile;
	unsigned long	v_result = GENE0000_NORMAL;
	char			v_codperfilprin[C_GEN_LEN_CODPERFIL + 1] = "";
	char			v_where[C_GEN_LEN_WHERE + 1] = "";
	char			v_sentencia[C_GEN_LEN_SENTENCIA + 1] = "";
	unsigned int	v_count_bands = 0;
	unsigned int	v_count_act = 0;
	IlcInt			numBands;
	unsigned long	v_fecini_num = 0;
	unsigned long	v_fecfin_num = 0;
	unsigned long	v_fecd_num = 0;
	unsigned long	v_fech_num = 0;
	unsigned long	v_fecha_baja_num = 0;
	unsigned long	v_period_dant_num = 0;
	unsigned long	v_period_d_num = 0;
	unsigned int	diad = 0;
	unsigned int	mesd = 0;
	unsigned int	aniod = 0;
	unsigned int	diah = 0;
	unsigned int	mesh = 0;
	unsigned int	anioh = 0;	
	int				v_codperfil = 0;
	char	v_period_d[C_GEN_LEN_FECHA + 1] = "";
	char	v_period_h[C_GEN_LEN_FECHA + 1] = "";
	char	v_period_dant[C_GEN_LEN_FECHA + 1] = "";
	char	v_dato[C_GEN_LEN_DATO + 1] = "";

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

	sprintf( v_where, "AG11CODRECURSO = %d AND AG07INDPERGENE = -1",
		codRecurso );
	gen_obtener_dato( "AG0700",
						v_where,
						"AG07CODPERFIL",
						v_codperfilprin );

	v_codperfil = atoi( v_codperfilprin );

	v_fecd_num = gen_fecha_numero( ae_fecha_d );
	v_fech_num = gen_fecha_numero( ae_fecha_h );

	strcpy( v_period_dant, ae_fecha_d );
	v_period_dant_num = gen_fecha_numero( v_period_dant );

	sprintf( (char*)h_select.arr,
		"SELECT AG0700.AG07CODPERFIL, TO_CHAR(AG07FECBAJA,'DD-MM-YYYY'), TO_CHAR(AG09FECINVIPER,'DD-MM-YYYY'),\
		TO_CHAR(AG09FECFIVIPER,'DD-MM-YYYY') FROM AG0700,AG0900 WHERE \
		AG0700.AG11CODRECURSO=%d AND AG0900.AG11CODRECURSO=%d AND \
		AG0700.AG07CODPERFIL=AG0900.AG07CODPERFIL AND AG07INDPERGENE != -1 AND \
		((AG09FECFIVIPER IS NULL AND TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') \
		< TO_DATE(\'%s\','dd-mm-yyyy')) OR \
		(TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') <= \
		TO_DATE( \'%s\','dd-mm-yyyy') AND TO_DATE( \'%s\','dd-mm-yyyy') < \
		TO_DATE(TO_CHAR(AG09FECFIVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') \
		AND TO_DATE(TO_CHAR(AG09FECFIVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') < \
		TO_DATE( \'%s\','dd-mm-yyyy')) OR (TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') \
		<= TO_DATE(\'%s\','dd-mm-yyyy') AND TO_DATE(\'%s\','dd-mm-yyyy') < \
		TO_DATE(TO_CHAR(AG09FECFIVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
		TO_DATE(TO_CHAR(AG09FECFIVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') \
		>= TO_DATE(\'%s\','dd-mm-yyyy')) OR (TO_DATE(\'%s\','dd-mm-yyyy') < \
		TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
		TO_DATE(TO_CHAR(AG09FECFIVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') < \
		TO_DATE(\'%s\','dd-mm-yyyy')) OR (TO_DATE(\'%s\','dd-mm-yyyy') < \
		TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') AND \
		TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') < \
		TO_DATE(\'%s\','dd-mm-yyyy') AND TO_DATE(TO_CHAR(AG09FECFIVIPER,'dd-mm-yyyy'),'dd-mm-yyyy') \
		>= TO_DATE(\'%s\','dd-mm-yyyy'))) AND ( AG07FECBAJA IS NULL \
		OR AG07FECBAJA > TO_DATE(TO_CHAR(AG09FECINVIPER,'dd-mm-yyyy'),'dd-mm-yyyy')) \
		ORDER BY AG09FECINVIPER",
		codRecurso, codRecurso, ae_fecha_h,
		ae_fecha_d, ae_fecha_d, ae_fecha_h,
		ae_fecha_d, ae_fecha_d, ae_fecha_h,
		ae_fecha_d, ae_fecha_h,
		ae_fecha_d, ae_fecha_h, ae_fecha_h );

	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAPER FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )722;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_per CURSOR FOR SENTENCIAPER; */ 
	/* EXEC SQL OPEN sql_cursor_per; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )740;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{		
		h_datos_per.h_codigo_perfil = 0;
		strcpy( h_datos_per.h_fecha_baja, "" );
		strcpy( h_datos_per.h_fecha_ini, "" );
		strcpy( h_datos_per.h_fecha_fin, "" );

		/* EXEC SQL FETCH sql_cursor_per INTO :h_datos_per; */ 
{
  struct sqlexd sqlstm={8,4};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )754;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_per.h_codigo_perfil;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_per.h_fecha_baja;
  sqlstm.sqhstl[1] = (unsigned int  )11;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)h_datos_per.h_fecha_ini;
  sqlstm.sqhstl[2] = (unsigned int  )11;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)h_datos_per.h_fecha_fin;
  sqlstm.sqhstl[3] = (unsigned int  )11;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{
		}

		v_fecha_baja_num = gen_fecha_numero( h_datos_per.h_fecha_baja );

		v_fecini_num = gen_fecha_numero( h_datos_per.h_fecha_ini );
		v_fecfin_num = gen_fecha_numero( h_datos_per.h_fecha_fin );

		if ( v_fecini_num < v_fecha_baja_num && ( v_fecha_baja_num <= v_fecfin_num 
			|| v_fecfin_num == 0 ) && v_fecha_baja_num != 0 )
		{ /* Fecha baja dentro del periodo de vigencia */

			if ( v_fecha_baja_num > v_fech_num )
			{

				strcpy( h_datos_per.h_fecha_fin, ae_fecha_h );

			} /* fin del if */
			else
			{
				strcpy( h_datos_per.h_fecha_fin, h_datos_per.h_fecha_baja );

			} /* fin del else */

			sprintf( v_dato,
				"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') - 1, 'DD-MM-YYYY')",
				h_datos_per.h_fecha_fin );
			gen_obtener_dato( "DUAL",
								"",
								v_dato,
								h_datos_per.h_fecha_fin );

			v_fecfin_num = gen_fecha_numero( h_datos_per.h_fecha_fin );

		} /* fin del if */

		if ( v_fecfin_num == 0 )
		{ /* Fecha fin vigencia perfil es nula */

			strcpy( v_period_h, ae_fecha_h );
			gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

			if ( v_fecini_num <= v_fecd_num )
			{ /* Periodo que empieza antes del periodo de estudio */

				strcpy( v_period_d, ae_fecha_d );
				gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

			} /* fin del if */

			if ( v_fecd_num < v_fecini_num )
			{ /* Periodo dentro del periodo de estudio */

				//Fecha desde.
				strcpy( v_period_d, h_datos_per.h_fecha_ini ); 
				gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

			} /* fin del if */

			sprintf( v_where, 
				"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND \
				( AG04FECBAJA IS NULL OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
				codRecurso, h_datos_per.h_codigo_perfil, v_period_d );

			gen_count_registros( "AG0400",
								v_where,
								v_count_bands );

			if ( v_count_bands != 0 )
			{ /* alguna franja */
				
				numBands = v_count_bands;
				profile = new MedicalServiceCalendarProfile(mss, 
												 Period(Date(diad,mesd,aniod),
														Date(diah,mesh,anioh)),
												 numBands);

				gen_read_bands( app, appScheduler, profile,
								codRecurso, h_datos_per.h_codigo_perfil,
								v_period_d, v_period_h );

				mss->addProfile(profile);

			} /* fin del if */

			v_period_d_num = gen_fecha_numero( v_period_d );
			if ( v_period_d_num > v_period_dant_num )
			{ /* hay hueco */

				sprintf( v_where, 
					"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL \
					OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
					codRecurso, v_codperfil, v_period_dant );

				gen_count_registros( "AG0400",
										v_where,
										v_count_bands );

				if ( v_count_bands != 0 )
				{  /* alguna franja */

					//Fecha desde.
					gen_desglosar_fechad( v_period_dant, diad, mesd, aniod );

					//Fecha hasta.
					gen_desglosar_fechad( v_period_d, diah, mesh, anioh );

					numBands = v_count_bands;
					profile = new MedicalServiceCalendarProfile(mss,
														   Period(Date(diad,mesd,aniod),
																  Date(diah,mesh,anioh)),
														   numBands);

					gen_read_bands( app, appScheduler, profile,
									codRecurso, v_codperfil,
									v_period_dant, v_period_d );

					mss->addProfile(profile);

				} /* fin del if */

			} /* fin del if */

			strcpy( v_period_dant, v_period_h );
			v_period_dant_num = gen_fecha_numero( v_period_dant );

		} /* fin del if */

		if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
			&& v_fecfin_num < v_fech_num )
		{ /* Periodo que empieza antes del periodo de estudio y finaliza
			antes que dicho periodo */

			strcpy( v_period_d, ae_fecha_d );
			gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

			//Fecha hasta.
			strcpy( v_period_h, h_datos_per.h_fecha_fin );
			gen_desglosar_fechah( v_period_h, diah, mesh, anioh );

			sprintf( v_where, 
				"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
				codRecurso, h_datos_per.h_codigo_perfil, v_period_d );

			gen_count_registros( "AG0400",
							v_where,
							v_count_bands );

			if ( v_count_bands != 0 )
			{ /* alguna franja */
			
				numBands = v_count_bands;
				profile = new MedicalServiceCalendarProfile(mss, 
                                             Period(Date(diad,mesd,aniod),
                                                    Date(diah,mesh,anioh)),
                                             numBands);

				gen_read_bands( app, appScheduler, profile,
								codRecurso, h_datos_per.h_codigo_perfil,
								v_period_d, v_period_h );

				mss->addProfile(profile);

			} /* fin del if */

			strcpy( v_period_dant, v_period_h );
			v_period_dant_num = gen_fecha_numero( v_period_dant );

		} /* fin del if */

		if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
			&& v_fecfin_num >= v_fech_num )
		{ /* Periodo que empieza antes del periodo de estudio y finaliza
			despu�s que dicho periodo */

			//Fecha desde.
			strcpy( v_period_d, ae_fecha_d );
			gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

			//Fecha hasta.
			strcpy( v_period_h, ae_fecha_h );
			gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

			sprintf( v_where, 
				"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
				codRecurso, h_datos_per.h_codigo_perfil, v_period_d );

			gen_count_registros( "AG0400",
							v_where,
							v_count_bands );

			if ( v_count_bands != 0 )
			{ /* alguna franja */

				numBands = v_count_bands;
				profile = new MedicalServiceCalendarProfile(mss, 
                                             Period(Date(diad,mesd,aniod),
                                                    Date(diah,mesh,anioh)),
                                             numBands);

				gen_read_bands( app, appScheduler, profile,
								codRecurso, h_datos_per.h_codigo_perfil,
								v_period_d, v_period_h );

				mss->addProfile(profile);

			} /* fin del if */

			strcpy( v_period_dant, v_period_h );
			v_period_dant_num = gen_fecha_numero( v_period_dant );

		} /* fin del if */

		if ( v_fecd_num < v_fecini_num && v_fecfin_num < v_fech_num &&
			v_fecfin_num != 0 )
		{ /* Periodo comprendido dentro del periodo de estudio */

			//Fecha desde.
			strcpy( v_period_d, h_datos_per.h_fecha_ini ); 
			gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

			//Fecha hasta.
			strcpy( v_period_h, h_datos_per.h_fecha_fin );
			gen_desglosar_fechah( v_period_h, diah, mesh, anioh );

			sprintf( v_where, 
				"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
				codRecurso, h_datos_per.h_codigo_perfil, v_period_d );

			gen_count_registros( "AG0400",
							v_where,
							v_count_bands );

			if ( v_count_bands != 0 )
			{ /* alguna franja */

				numBands = v_count_bands;
				profile = new MedicalServiceCalendarProfile(mss, 
                                             Period(Date(diad,mesd,aniod),
                                                    Date(diah,mesh,anioh)),
                                             numBands);

				gen_read_bands( app, appScheduler, profile,
								codRecurso, h_datos_per.h_codigo_perfil,
								v_period_d, v_period_h );

				mss->addProfile(profile);

			} /* fin del if */

			v_period_d_num = gen_fecha_numero( v_period_d );
			if ( v_period_d_num > v_period_dant_num )
			{ /* hay hueco */

				sprintf( v_where, 
					"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL \
					OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
					codRecurso, v_codperfil, v_period_dant );

				gen_count_registros( "AG0400",
										v_where,
										v_count_bands );

				if ( v_count_bands != 0 )
				{  /* alguna franja */

					//Fecha desde.
					gen_desglosar_fechad( v_period_dant, diad, mesd, aniod );

					//Fecha hasta.
					gen_desglosar_fechad( v_period_d, diah, mesh, anioh );

					numBands = v_count_bands;
					profile = new MedicalServiceCalendarProfile(mss,
														   Period(Date(diad,mesd,aniod),
																  Date(diah,mesh,anioh)),
														   numBands);

					gen_read_bands( app, appScheduler, profile,
									codRecurso, v_codperfil,
									v_period_dant, v_period_d );

					mss->addProfile(profile);

				} /* fin del if */

			} /* fin del if */

			strcpy( v_period_dant, v_period_h );
			v_period_dant_num = gen_fecha_numero( v_period_dant );

		} /* fin del if */

		if ( v_fecd_num < v_fecini_num && v_fecini_num < v_fech_num
			&& v_fecfin_num >= v_fech_num )
		{ /* Periodo que comienza en periodo de estudio y finaliza 
			despu�s que el periodo de estudio */

			//Fecha desde.
			strcpy( v_period_d, h_datos_per.h_fecha_ini );
			gen_desglosar_fechad( v_period_d, diad, mesd, aniod );

			//Fecha hasta.
			strcpy( v_period_h, ae_fecha_h );
			gen_desglosar_fechad( v_period_h, diah, mesh, anioh );

			sprintf( v_where, 
				"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
				codRecurso, h_datos_per.h_codigo_perfil, v_period_d );

			gen_count_registros( "AG0400",
							v_where,
							v_count_bands );

			if ( v_count_bands != 0 )
			{ /* alguna franja */

				numBands = v_count_bands;
				profile = new MedicalServiceCalendarProfile(mss, 
                                             Period(Date(diad,mesd,aniod),
                                                    Date(diah,mesh,anioh)),
				                             numBands);

				gen_read_bands( app, appScheduler, profile,
								codRecurso, h_datos_per.h_codigo_perfil,
								v_period_d, v_period_h );

				mss->addProfile(profile);

			} /* fin del if */

			v_period_d_num = gen_fecha_numero( v_period_d );
			if ( v_period_d_num > v_period_dant_num )
			{ /* hay hueco */

				sprintf( v_where, 
					"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL \
					OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
					codRecurso, v_codperfil, v_period_dant );

				gen_count_registros( "AG0400",
										v_where,
										v_count_bands );

				if ( v_count_bands != 0 )
				{  /* alguna franja */

					//Fecha desde.
					gen_desglosar_fechad( v_period_dant, diad, mesd, aniod );

					//Fecha hasta.
					gen_desglosar_fechad( v_period_d, diah, mesh, anioh );

					numBands = v_count_bands;
					profile = new MedicalServiceCalendarProfile(mss,
														   Period(Date(diad,mesd,aniod),
																  Date(diah,mesh,anioh)),
														   numBands);

					gen_read_bands( app, appScheduler, profile,
									codRecurso, v_codperfil,
									v_period_dant, v_period_d );

					mss->addProfile(profile);

				} /* fin del if */

			} /* fin del if */

			strcpy( v_period_dant, v_period_h );
			v_period_dant_num = gen_fecha_numero( v_period_dant );

		} /* fin del if */
				
	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_per; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )784;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	if ( v_period_dant_num < v_fech_num )
	{ /* hay hueco al final del periodo de estudio */

		sprintf( v_where, 
			"AG11CODRECURSO = %d AND AG07CODPERFIL = %d AND ( AG04FECBAJA IS NULL \
			OR AG04FECBAJA > TO_DATE( \'%s\','DD-MM-YYYY'))",
			codRecurso, v_codperfil, v_period_dant );

		gen_count_registros( "AG0400",
								v_where,
								v_count_bands );

		if ( v_count_bands != 0 )
		{  /* alguna franja */

			//Fecha desde.
			gen_desglosar_fechad( v_period_dant, diad, mesd, aniod );

			//Fecha hasta.
			gen_desglosar_fechad( ae_fecha_h, diah, mesh, anioh );

			numBands = v_count_bands;
			profile = new MedicalServiceCalendarProfile(mss,
											   Period(Date(diad,mesd,aniod),
													  Date(diah,mesh,anioh)),
											   numBands);

			gen_read_bands( app, appScheduler, profile,
							codRecurso, v_codperfil,
							v_period_dant, ae_fecha_h );

			mss->addProfile(profile);

		} /* fin del if */

	} /* fin del if */

} /* fin de funci�n gen_read_profiles_bands */




/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_bands
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *		app				ClinicApplication*		Puntero a datos generales
 *												de la cl�nica.
 *		appScheduler	ClinicApplicationScheduler* Puntero a datos generales
 *												de la cl�nica, utilizados por
 *												el scheduler.
 *		profile			MedicalServiceCalendarProfile*	Puntero al perfil
 *												del recurso.
 *		codRecurso		IlcInt					C�digo del Recurso
 *		codPerfil		int						C�digo del Perfil
 *		periodd			char*					Fecha inicio del periodo vigen-
 *												cia actual.
 *		periodh			char*					Fecha fin del periodo vigen-
 *												cia actual.
 *
 *
 * DESCRIPCI�N:
 *		Construye las bandas de un recurso y perfil dados. Para un mismo perfil
 *		pueden exitir distintos periodos de vigencia, y para cada uno de ellos
 *		se leer�n las restricciones por franja y las actuaciones permitidas
 *		en dicha franja.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   24/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void gen_read_bands( 
					ClinicApplication*				app,
					ClinicApplicationScheduler*		appScheduler,
					MedicalServiceCalendarProfile*	profile,
					const IlcInt					codRecurso,
					const int						codPerfil,
					char*							periodd,
					char*							periodh
					)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
 	MedicalServiceCalendarBand*	band;
	MedicalFactTypesBand*		mftBand;
	PropertyConditionTree*		tree;
	unsigned long	v_result = GENE0000_NORMAL;
	unsigned long	periodd_num = 0;
	unsigned long	periodh_num = 0;
	char			v_where[C_GEN_LEN_WHERE + 1] = "";
	unsigned int	v_count_act = 0;
	Date*			dateBand;
	IlcInt			numMFTypes;
	IlcAnyArray		mfTypes;
	IlcIntArray		maxQuotas;
	IlcInt			quota;
	unsigned long	v_fecha_baja_num = 0;	
	unsigned int	diab = 0;
	unsigned int	mesb = 0;
	unsigned int	aniob = 0;	
	unsigned int	indice = 0;
	char			v_sentencia[C_GEN_LEN_SENTENCIA + 1] = "";
	unsigned long	v_dias = 0;
	unsigned long	v_horas = 0;
	unsigned long	v_min = 0;
	int				minTotales = 0;
	int				numIntervalos = 0;
	int				horaFin = 0;
	int				minFin = 0;
	ReserveType		reserveType;
	char			typeRec[C_GEN_LEN_COD_RECURSO + 1] = "";
	unsigned int	v_tpo_reserva = 0;


/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

	periodd_num = gen_fecha_numero( periodd );

	periodh_num = gen_fecha_numero( periodh );

	sprintf( (char*)h_select_aux.arr,
		"SELECT DISTINCT AG04CODFRANJA,AG04INDLUNFRJA,AG04INDMARFRJA,AG04INDMIEFRJA, \
		AG04INDJUEFRJA,AG04INDVIEFRJA,AG04INDSABFRJA,AG04INDDOMFRJA, \
		AG04HORINFRJHH,AG04HORINFRJMM,AG04HORFIFRJHH,AG04HORFIFRJMM,NVL(AG0400.PR12CODACTIVIDAD,0),\
		AG04NUMCITADMI,NVL(AG04INTERVCITA,0),TO_CHAR(AG04FECBAJA,'DD-MM-YYYY'), \
		NVL(AG04MODASIGCITA,'0') FROM AG0400,PR1200 WHERE AG11CODRECURSO=%d \
		AND AG07CODPERFIL=%d AND (AG0400.PR12CODACTIVIDAD IS NULL \
		OR AG0400.PR12CODACTIVIDAD IN (SELECT PR12CODACTIVIDAD FROM PR1200 \
		WHERE  PR12INDPLANIFIC = -1 ))",
		codRecurso, codPerfil );

	//A�adir fin de cadena a h_select_aux.
	h_select_aux.len = strlen((char*)h_select_aux.arr );
	h_select_aux.arr[h_select_aux.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAFRJA FROM :h_select_aux; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )798;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select_aux;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_frja CURSOR FOR SENTENCIAFRJA; */ 

	/* EXEC SQL OPEN sql_cursor_frja; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )816;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{		
		h_datos_franja.h_cod_franja = 0;
		h_datos_franja.h_indlu = 0;
		h_datos_franja.h_indma = 0;
		h_datos_franja.h_indmi = 0;
		h_datos_franja.h_indju = 0;
		h_datos_franja.h_indvi = 0;
		h_datos_franja.h_indsa = 0;
		h_datos_franja.h_inddo = 0;
		h_datos_franja.h_inihh = 0;
		h_datos_franja.h_inimm = 0;
		h_datos_franja.h_finhh = 0;
		h_datos_franja.h_finmm = 0;
		h_datos_franja.h_cod_act = 0;
		h_datos_franja.h_num_citas = 0;
		h_datos_franja.h_interv_cita = 0;
		strcpy( h_datos_franja.h_fecha_baja, "" );
		strcpy( h_datos_franja.h_modo_asig, "" );

		/* EXEC SQL FETCH sql_cursor_frja INTO :h_datos_franja; */ 
{
  struct sqlexd sqlstm={8,17};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )830;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_franja.h_cod_franja;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_franja.h_indlu;
  sqlstm.sqhstl[1] = (unsigned int  )4;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_franja.h_indma;
  sqlstm.sqhstl[2] = (unsigned int  )4;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_franja.h_indmi;
  sqlstm.sqhstl[3] = (unsigned int  )4;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_franja.h_indju;
  sqlstm.sqhstl[4] = (unsigned int  )4;
  sqlstm.sqindv[4] = (         short *)0;
  sqlstm.sqharm[4] = (unsigned int  )0;
  sqlstm.sqhstv[5] = (unsigned char  *)&h_datos_franja.h_indvi;
  sqlstm.sqhstl[5] = (unsigned int  )4;
  sqlstm.sqindv[5] = (         short *)0;
  sqlstm.sqharm[5] = (unsigned int  )0;
  sqlstm.sqhstv[6] = (unsigned char  *)&h_datos_franja.h_indsa;
  sqlstm.sqhstl[6] = (unsigned int  )4;
  sqlstm.sqindv[6] = (         short *)0;
  sqlstm.sqharm[6] = (unsigned int  )0;
  sqlstm.sqhstv[7] = (unsigned char  *)&h_datos_franja.h_inddo;
  sqlstm.sqhstl[7] = (unsigned int  )4;
  sqlstm.sqindv[7] = (         short *)0;
  sqlstm.sqharm[7] = (unsigned int  )0;
  sqlstm.sqhstv[8] = (unsigned char  *)&h_datos_franja.h_inihh;
  sqlstm.sqhstl[8] = (unsigned int  )4;
  sqlstm.sqindv[8] = (         short *)0;
  sqlstm.sqharm[8] = (unsigned int  )0;
  sqlstm.sqhstv[9] = (unsigned char  *)&h_datos_franja.h_inimm;
  sqlstm.sqhstl[9] = (unsigned int  )4;
  sqlstm.sqindv[9] = (         short *)0;
  sqlstm.sqharm[9] = (unsigned int  )0;
  sqlstm.sqhstv[10] = (unsigned char  *)&h_datos_franja.h_finhh;
  sqlstm.sqhstl[10] = (unsigned int  )4;
  sqlstm.sqindv[10] = (         short *)0;
  sqlstm.sqharm[10] = (unsigned int  )0;
  sqlstm.sqhstv[11] = (unsigned char  *)&h_datos_franja.h_finmm;
  sqlstm.sqhstl[11] = (unsigned int  )4;
  sqlstm.sqindv[11] = (         short *)0;
  sqlstm.sqharm[11] = (unsigned int  )0;
  sqlstm.sqhstv[12] = (unsigned char  *)&h_datos_franja.h_cod_act;
  sqlstm.sqhstl[12] = (unsigned int  )4;
  sqlstm.sqindv[12] = (         short *)0;
  sqlstm.sqharm[12] = (unsigned int  )0;
  sqlstm.sqhstv[13] = (unsigned char  *)&h_datos_franja.h_num_citas;
  sqlstm.sqhstl[13] = (unsigned int  )4;
  sqlstm.sqindv[13] = (         short *)0;
  sqlstm.sqharm[13] = (unsigned int  )0;
  sqlstm.sqhstv[14] = (unsigned char  *)&h_datos_franja.h_interv_cita;
  sqlstm.sqhstl[14] = (unsigned int  )4;
  sqlstm.sqindv[14] = (         short *)0;
  sqlstm.sqharm[14] = (unsigned int  )0;
  sqlstm.sqhstv[15] = (unsigned char  *)h_datos_franja.h_fecha_baja;
  sqlstm.sqhstl[15] = (unsigned int  )11;
  sqlstm.sqindv[15] = (         short *)0;
  sqlstm.sqharm[15] = (unsigned int  )0;
  sqlstm.sqhstv[16] = (unsigned char  *)h_datos_franja.h_modo_asig;
  sqlstm.sqhstl[16] = (unsigned int  )2;
  sqlstm.sqindv[16] = (         short *)0;
  sqlstm.sqharm[16] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{
		
		} /* fin del if */

		v_tpo_reserva = 0;
		if ( !strcmp( h_datos_franja.h_modo_asig, "0" ))
		{ //no tiene modo asignaci�n particular

			reserveType = profile->getMedicalServiceSingle()->getReserveType();

		} /* fin del if */
		else
		{ /* asignaci�n particular por franja */

			if ( !strcmp( h_datos_franja.h_modo_asig, "1" ))
			{ /* Por cantidad */

				reserveType = QUOTA;

			} /* fin del if */
			else if ( !strcmp( h_datos_franja.h_modo_asig, "2" ))
			{ /* Secuencial */

				reserveType = SEQUENTIAL_INTERVAL;

			} /* fin del else if */
			else if ( !strcmp( h_datos_franja.h_modo_asig, "3" ))
			{ /* Intervalos predeterminados */

				reserveType = FIX_INTERVAL;

			} /* fin del else if */

		} /* fin del else */

		DayPeriod dayPeriod(h_datos_franja.h_inihh,
							h_datos_franja.h_inimm,
							h_datos_franja.h_finhh,
							h_datos_franja.h_finmm);
				
		minTotales = dayPeriod.getDuration().getNumHours()*C_GEN_MINUTOS_EN_HORA +
					 dayPeriod.getDuration().getNumMinutes();

		v_fecha_baja_num = gen_fecha_numero( h_datos_franja.h_fecha_baja );

		indice = 0;
			
		sprintf( v_where,
			"AG11CODRECURSO=%d AND AG07CODPERFIL=%d AND AG04CODFRANJA=%d AND \
			( ( AG01FECINVGACF BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
			TO_DATE( \'%s\','DD-MM-YYYY')) OR ( AG01FECINVGACF < TO_DATE( \'%s\', \
			'DD-MM-YYYY') AND ( AG01FECFIVGACF > TO_DATE( \'%s\', 'DD-MM-YYYY') OR \
			AG01FECFIVGACF IS NULL) )) AND AD02CODDPTO IN (SELECT AD02CODDPTO \
			FROM AD0200 WHERE AG0100.AD02CODDPTO=AD0200.AD02CODDPTO AND \
			( AD02FECINICIO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND TO_DATE( \'%s\', \
			'DD-MM-YYYY')) OR ( AD02FECINICIO < TO_DATE( \'%s\', 'DD-MM-YYYY') AND \
			( AD02FECFIN > TO_DATE( \'%s\', 'DD-MM-YYYY') OR AD02FECFIN IS NULL) ))",
			codRecurso, codPerfil, h_datos_franja.h_cod_franja,
			periodd, periodh, periodd, periodd, periodd, periodh, periodd, periodd );

		gen_count_registros( "AG0100",
								v_where,
								v_count_act );


		if ( v_fecha_baja_num == 0 || 
			( v_fecha_baja_num != 0 && v_fecha_baja_num > periodd_num ))
		{ /* hay actuaciones para franja en curso */
			tree = gen_read_tree_band( app, codRecurso, codPerfil,
											h_datos_franja.h_cod_franja );


			if ( v_count_act != 0 )
			{ /* Alguna Actuaci�n/Dpto vigente para franja en curso */

				numMFTypes = v_count_act;

				Duration* maxTimes= new Duration[numMFTypes];
  
				mfTypes= appScheduler->createAnyArray(numMFTypes);

				maxQuotas= appScheduler->createIntArray(numMFTypes);

				sprintf( (char*)h_select_temp.arr,
					"SELECT AD02CODDPTO,PR01CODACTUACION,AG01NUMASIGADM, \
					TO_CHAR(AG01FECINVGACF,'DD-MM-YYYY'), TO_CHAR(AG01FECFIVGACF,'DD-MM-YYYY') \
					FROM AG0100 WHERE AG11CODRECURSO=%d AND AG07CODPERFIL=%d AND \
					AG04CODFRANJA=%d AND ( ( AG01FECINVGACF BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
					TO_DATE( \'%s\','DD-MM-YYYY')) OR ( AG01FECINVGACF < TO_DATE( \'%s\', \
					'DD-MM-YYYY') AND ( AG01FECFIVGACF > TO_DATE( \'%s\', 'DD-MM-YYYY') OR \
					AG01FECFIVGACF IS NULL) )) AND AD02CODDPTO IN (SELECT AD02CODDPTO \
					FROM AD0200 WHERE AG0100.AD02CODDPTO=AD0200.AD02CODDPTO AND \
					( AD02FECINICIO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND TO_DATE( \'%s\', \
					'DD-MM-YYYY')) OR ( AD02FECINICIO < TO_DATE( \'%s\', 'DD-MM-YYYY') AND \
					( AD02FECFIN > TO_DATE( \'%s\', 'DD-MM-YYYY') OR AD02FECFIN IS NULL) ))",
					codRecurso, codPerfil, h_datos_franja.h_cod_franja,								
					periodd, periodh, periodd, periodd, periodd, periodh, periodd, periodd );

				//A�adir fin de cadena a h_select_temp.
				h_select_temp.len = strlen((char*)h_select_temp.arr );
				h_select_temp.arr[h_select_temp.len] = '\0';

				//Preparar sentencia.
				/* EXEC SQL PREPARE SENTENCIAFRJACT FROM :h_select_temp; */ 
{
    struct sqlexd sqlstm={8,1};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )912;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_select_temp;
    sqlstm.sqhstl[0] = (unsigned int  )3003;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				//Declarar y abrir cursor para sentencia anterior.
				/* EXEC SQL DECLARE sql_cursor_frjact CURSOR FOR SENTENCIAFRJACT; */ 
				/* EXEC SQL OPEN sql_cursor_frjact; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.stmt = "";
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )930;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				//Bucle para recuperar los registros del cursor.
				while(1)
				{		

					/* EXEC SQL FETCH sql_cursor_frjact INTO :h_datos_frjact; */ 
{
     struct sqlexd sqlstm={8,5};
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )944;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_frjact.h_cod_dpto;
     sqlstm.sqhstl[0] = (unsigned int  )4;
     sqlstm.sqindv[0] = (         short *)0;
     sqlstm.sqharm[0] = (unsigned int  )0;
     sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_frjact.h_cod_act;
     sqlstm.sqhstl[1] = (unsigned int  )4;
     sqlstm.sqindv[1] = (         short *)0;
     sqlstm.sqharm[1] = (unsigned int  )0;
     sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_frjact.h_num_asig;
     sqlstm.sqhstl[2] = (unsigned int  )4;
     sqlstm.sqindv[2] = (         short *)0;
     sqlstm.sqharm[2] = (unsigned int  )0;
     sqlstm.sqhstv[3] = (unsigned char  *)h_datos_frjact.h_fecini_vigencia;
     sqlstm.sqhstl[3] = (unsigned int  )11;
     sqlstm.sqindv[3] = (         short *)0;
     sqlstm.sqharm[3] = (unsigned int  )0;
     sqlstm.sqhstv[4] = (unsigned char  *)h_datos_frjact.h_fecfin_vigencia;
     sqlstm.sqhstl[4] = (unsigned int  )11;
     sqlstm.sqindv[4] = (         short *)0;
     sqlstm.sqharm[4] = (unsigned int  )0;
     sqlstm.sqphsv = sqlstm.sqhstv;
     sqlstm.sqphsl = sqlstm.sqhstl;
     sqlstm.sqpind = sqlstm.sqindv;
     sqlstm.sqparm = sqlstm.sqharm;
     sqlstm.sqparc = sqlstm.sqharc;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode == 1403) break;
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


					if ( v_result != GENE0000_NORMAL )
					{

					} /* fin del if */

					IlcInt valor_mfTypes = ( h_datos_frjact.h_cod_act * 1000 ) + 
											 h_datos_frjact.h_cod_dpto;

					mfTypes[indice] = (IlcAny)valor_mfTypes;
					maxQuotas[indice] = h_datos_frjact.h_num_asig;

					gen_obtener_duracion( minTotales, v_dias, v_horas, v_min );
					maxTimes[indice]= Duration(v_dias,v_horas,v_min);

					indice++;

				} /* fin del while */
				
				//Cerrar el cursor.
				/* EXEC SQL CLOSE sql_cursor_frjact; */ 
{
    struct sqlexd sqlstm={8,0};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )978;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


				mftBand= new MedicalFactTypesBand(
									appScheduler->createAnySet(mfTypes), mfTypes,
									maxTimes, maxQuotas, app->getCalendar());

			} /* fin del if */
			else
			{ /* sin actuaciones especificadas por franja */

				sprintf( v_where, "AG11CODRECURSO = %d", codRecurso );
				gen_obtener_dato( "AG1100",
								  v_where,
								  "AG14CODTIPRECU",
								  typeRec );

				if ( h_datos_franja.h_cod_act == 0 )
				{
					sprintf( v_where,
						"AG14CODTIPRECU=%s AND ( ( PR01FECINICO BETWEEN \
						TO_DATE( \'%s\','DD-MM-YYYY') AND TO_DATE( \'%s\','DD-MM-YYYY')) \
						OR ( PR01FECINICO < TO_DATE( \'%s\','DD-MM-YYYY') AND \
						( PR01FECFIN > TO_DATE( \'%s\', 'DD-MM-YYYY') OR \
						PR01FECFIN IS NULL) )) AND AD02CODDPTO IN (SELECT AD02CODDPTO \
						FROM AD0200 WHERE AG0501J.AD02CODDPTO=AD0200.AD02CODDPTO AND \
						( AD02FECINICIO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
						TO_DATE( \'%s\','DD-MM-YYYY')) OR ( AD02FECINICIO < \
						TO_DATE( \'%s\', 'DD-MM-YYYY') AND ( AD02FECFIN > \
						TO_DATE( \'%s\', 'DD-MM-YYYY') OR AD02FECFIN IS NULL) ))",
						typeRec, periodd, periodh, periodd, periodd, periodd, 
						periodh, periodd, periodd );

					gen_count_registros( "AG0501J",
										 v_where,
										 v_count_act );

					numMFTypes = v_count_act;

					sprintf( (char*)h_select_temp.arr,
						"SELECT PR01CODACTUACION FROM AG0501J WHERE AG14CODTIPRECU=%s \
						AND (( PR01FECINICO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
						TO_DATE( \'%s\','DD-MM-YYYY')) OR ( PR01FECINICO < \
						TO_DATE( \'%s\', 'DD-MM-YYYY') AND ( PR01FECFIN > \
						TO_DATE( \'%s\', 'DD-MM-YYYY') OR PR01FECFIN IS NULL) )) \
						AND AD02CODDPTO IS NULL",
						typeRec, periodd, periodh, periodd, periodd);

					//A�adir fin de cadena a h_select_temp.
					h_select_temp.len = strlen((char*)h_select_temp.arr );
					h_select_temp.arr[h_select_temp.len] = '\0';

					//Preparar sentencia.
					/* EXEC SQL PREPARE SENTENCIADPTOAUX1 FROM :h_select_temp; */ 
{
     struct sqlexd sqlstm={8,1};
     sqlstm.stmt = "";
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )992;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlstm.sqhstv[0] = (unsigned char  *)&h_select_temp;
     sqlstm.sqhstl[0] = (unsigned int  )3003;
     sqlstm.sqindv[0] = (         short *)0;
     sqlstm.sqharm[0] = (unsigned int  )0;
     sqlstm.sqphsv = sqlstm.sqhstv;
     sqlstm.sqphsl = sqlstm.sqhstl;
     sqlstm.sqpind = sqlstm.sqindv;
     sqlstm.sqparm = sqlstm.sqharm;
     sqlstm.sqparc = sqlstm.sqharc;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


					//Declarar y abrir cursor para sentencia anterior.
					/* EXEC SQL DECLARE sql_cursor_dpto_aux1 CURSOR FOR SENTENCIADPTOAUX1; */ 
					/* EXEC SQL OPEN sql_cursor_dpto_aux1; */ 
{
     struct sqlexd sqlstm={8,0};
     sqlstm.stmt = "";
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )1010;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


					//Bucle para recuperar los registros del cursor.
					while(1)
					{
						/* EXEC SQL FETCH sql_cursor_dpto_aux1 INTO :h_cod_act; */ 
{
      struct sqlexd sqlstm={8,1};
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1024;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_act;
      sqlstm.sqhstl[0] = (unsigned int  )4;
      sqlstm.sqindv[0] = (         short *)0;
      sqlstm.sqharm[0] = (unsigned int  )0;
      sqlstm.sqphsv = sqlstm.sqhstv;
      sqlstm.sqphsl = sqlstm.sqhstl;
      sqlstm.sqpind = sqlstm.sqindv;
      sqlstm.sqparm = sqlstm.sqharm;
      sqlstm.sqparc = sqlstm.sqharc;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode == 1403) break;
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						if ( v_result != GENE0000_NORMAL )
						{

						} /* fin del if */

						sprintf( v_where, "PR01CODACTUACION=%d", h_cod_act );
						gen_count_registros( "PR0200",
											 v_where,
											 v_count_act );

						numMFTypes += v_count_act;

					} /* fin del while */

					//Cerrar el cursor.
					/* EXEC SQL CLOSE sql_cursor_dpto_aux1; */ 
{
     struct sqlexd sqlstm={8,0};
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )1042;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}
					

					if ( numMFTypes > 0 )
					{ /* Alguna Actuaci�n/Dpto vigente para franja en curso */

						Duration* maxTimes= new Duration[numMFTypes];
  
						mfTypes= appScheduler->createAnyArray(numMFTypes);

						maxQuotas= appScheduler->createIntArray(numMFTypes);

						sprintf( (char*)h_select_temp.arr,
							"SELECT NVL(AD02CODDPTO,0),PR01CODACTUACION FROM \
							AG0501J WHERE AG14CODTIPRECU=%s AND \
							( ( PR01FECINICO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
							TO_DATE( \'%s\','DD-MM-YYYY')) OR ( PR01FECINICO < \
							TO_DATE( \'%s\','DD-MM-YYYY') AND ( PR01FECFIN > \
							TO_DATE( \'%s\', 'DD-MM-YYYY') OR PR01FECFIN IS NULL) )) AND \
							(AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0200 WHERE \
							AG0501J.AD02CODDPTO=AD0200.AD02CODDPTO AND \
							( AD02FECINICIO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
							TO_DATE( \'%s\','DD-MM-YYYY')) OR ( AD02FECINICIO < \
							TO_DATE( \'%s\', 'DD-MM-YYYY') AND ( AD02FECFIN > \
							TO_DATE( \'%s\', 'DD-MM-YYYY') OR AD02FECFIN IS \
							NULL) )) OR AD02CODDPTO IS NULL)",
							typeRec, periodd, periodh, periodd, periodd, periodd, 
							periodh, periodd, periodd );

						//A�adir fin de cadena a h_select_temp.
						h_select_temp.len = strlen((char*)h_select_temp.arr );
						h_select_temp.arr[h_select_temp.len] = '\0';

						//Preparar sentencia.
						/* EXEC SQL PREPARE SENTENCIAFRJACT1 FROM :h_select_temp; */ 
{
      struct sqlexd sqlstm={8,1};
      sqlstm.stmt = "";
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1056;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlstm.sqhstv[0] = (unsigned char  *)&h_select_temp;
      sqlstm.sqhstl[0] = (unsigned int  )3003;
      sqlstm.sqindv[0] = (         short *)0;
      sqlstm.sqharm[0] = (unsigned int  )0;
      sqlstm.sqphsv = sqlstm.sqhstv;
      sqlstm.sqphsl = sqlstm.sqhstl;
      sqlstm.sqpind = sqlstm.sqindv;
      sqlstm.sqparm = sqlstm.sqharm;
      sqlstm.sqparc = sqlstm.sqharc;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						//Declarar y abrir cursor para sentencia anterior.
						/* EXEC SQL DECLARE sql_cursor_frjact1 CURSOR FOR SENTENCIAFRJACT1; */ 
						/* EXEC SQL OPEN sql_cursor_frjact1; */ 
{
      struct sqlexd sqlstm={8,0};
      sqlstm.stmt = "";
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1074;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						//Bucle para recuperar los registros del cursor.
						while(1)
						{		

							/* EXEC SQL FETCH sql_cursor_frjact1 INTO :h_cod_dpto,
																   :h_cod_act; */ 
{
       struct sqlexd sqlstm={8,2};
       sqlstm.iters = (unsigned int  )1;
       sqlstm.offset = (unsigned int  )1088;
       sqlstm.cud = sqlcud0;
       sqlstm.sqlest = (unsigned char  *)&sqlca;
       sqlstm.sqlety = (unsigned short)0;
       sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_dpto;
       sqlstm.sqhstl[0] = (unsigned int  )4;
       sqlstm.sqindv[0] = (         short *)0;
       sqlstm.sqharm[0] = (unsigned int  )0;
       sqlstm.sqhstv[1] = (unsigned char  *)&h_cod_act;
       sqlstm.sqhstl[1] = (unsigned int  )4;
       sqlstm.sqindv[1] = (         short *)0;
       sqlstm.sqharm[1] = (unsigned int  )0;
       sqlstm.sqphsv = sqlstm.sqhstv;
       sqlstm.sqphsl = sqlstm.sqhstl;
       sqlstm.sqpind = sqlstm.sqindv;
       sqlstm.sqparm = sqlstm.sqharm;
       sqlstm.sqparc = sqlstm.sqharc;
       sqlcex(sqlctx, &sqlstm, &sqlfpn);
       if (sqlca.sqlcode == 1403) break;
       if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


							if ( v_result != GENE0000_NORMAL )
							{

							} /* fin del if */

							if ( h_cod_dpto != 0 )
							{
								IlcInt valor_mfTypes = ( h_cod_act * 1000 ) + h_cod_dpto;

								mfTypes[indice] = (IlcAny)valor_mfTypes;
								maxQuotas[indice] = h_datos_franja.h_num_citas;

								gen_obtener_duracion( minTotales, v_dias, v_horas, v_min );
								maxTimes[indice]= Duration(v_dias,v_horas,v_min);

								indice++;
								
							} /* fin del if */
							else
							{
								sprintf( (char*)h_select_dpto.arr,
									"SELECT AD02CODDPTO FROM PR0200 WHERE \
									PR01CODACTUACION=%d", h_cod_act );

								//A�adir fin de cadena a h_select_dpto.
								h_select_dpto.len = strlen((char*)h_select_dpto.arr );
								h_select_dpto.arr[h_select_dpto.len] = '\0';

								//Preparar sentencia.
								/* EXEC SQL PREPARE SENTENCIADPTO FROM :h_select_dpto; */ 
{
        struct sqlexd sqlstm={8,1};
        sqlstm.stmt = "";
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )1110;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)0;
        sqlstm.sqhstv[0] = (unsigned char  *)&h_select_dpto;
        sqlstm.sqhstl[0] = (unsigned int  )3003;
        sqlstm.sqindv[0] = (         short *)0;
        sqlstm.sqharm[0] = (unsigned int  )0;
        sqlstm.sqphsv = sqlstm.sqhstv;
        sqlstm.sqphsl = sqlstm.sqhstl;
        sqlstm.sqpind = sqlstm.sqindv;
        sqlstm.sqparm = sqlstm.sqharm;
        sqlstm.sqparc = sqlstm.sqharc;
        sqlcex(sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


								//Declarar y abrir cursor para sentencia anterior.
								/* EXEC SQL DECLARE sql_cursor_dpto CURSOR FOR SENTENCIADPTO; */ 
								/* EXEC SQL OPEN sql_cursor_dpto; */ 
{
        struct sqlexd sqlstm={8,0};
        sqlstm.stmt = "";
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )1128;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)0;
        sqlcex(sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


								//Bucle para recuperar los registros del cursor.
								while(1)
								{		
									/* EXEC SQL FETCH sql_cursor_dpto INTO :h_cod_dpto; */ 
{
         struct sqlexd sqlstm={8,1};
         sqlstm.iters = (unsigned int  )1;
         sqlstm.offset = (unsigned int  )1142;
         sqlstm.cud = sqlcud0;
         sqlstm.sqlest = (unsigned char  *)&sqlca;
         sqlstm.sqlety = (unsigned short)0;
         sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_dpto;
         sqlstm.sqhstl[0] = (unsigned int  )4;
         sqlstm.sqindv[0] = (         short *)0;
         sqlstm.sqharm[0] = (unsigned int  )0;
         sqlstm.sqphsv = sqlstm.sqhstv;
         sqlstm.sqphsl = sqlstm.sqhstl;
         sqlstm.sqpind = sqlstm.sqindv;
         sqlstm.sqparm = sqlstm.sqharm;
         sqlstm.sqparc = sqlstm.sqharc;
         sqlcex(sqlctx, &sqlstm, &sqlfpn);
         if (sqlca.sqlcode == 1403) break;
         if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


									if ( v_result != GENE0000_NORMAL )
									{

									} /* fin del if */

									IlcInt valor_mfTypes = ( h_cod_act * 1000 ) + h_cod_dpto;


									mfTypes[indice] = (IlcAny)valor_mfTypes;
									maxQuotas[indice] = h_datos_franja.h_num_citas;

									gen_obtener_duracion( minTotales, v_dias, v_horas, v_min );
									maxTimes[indice]= Duration(v_dias,v_horas,v_min);

									indice++;

								} /* fin del while */
						
								//Cerrar el cursor.
								/* EXEC SQL CLOSE sql_cursor_dpto; */ 
{
        struct sqlexd sqlstm={8,0};
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )1160;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)0;
        sqlcex(sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


							} /* fin del else */

						} /* fin del while */
						
						//Cerrar el cursor.
						/* EXEC SQL CLOSE sql_cursor_frjact1; */ 
{
      struct sqlexd sqlstm={8,0};
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1174;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						mftBand= new MedicalFactTypesBand(
											appScheduler->createAnySet(mfTypes), mfTypes,
											maxTimes, maxQuotas, app->getCalendar());

					} /* fin del if */
				} /* fin del if */
				else
				{
					sprintf( v_where,
						"AG14CODTIPRECU=%s AND PR12CODACTIVIDAD = %d AND \
						( ( PR01FECINICO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
						TO_DATE( \'%s\','DD-MM-YYYY')) OR ( PR01FECINICO < \
						TO_DATE( \'%s\','DD-MM-YYYY') AND ( PR01FECFIN > \
						TO_DATE( \'%s\', 'DD-MM-YYYY') OR PR01FECFIN IS NULL) )) \
						AND AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0200 WHERE \
						AG0501J.AD02CODDPTO=AD0200.AD02CODDPTO AND ( AD02FECINICIO \
						BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND TO_DATE( \'%s\', \
						'DD-MM-YYYY')) OR ( AD02FECINICIO < \
						TO_DATE( \'%s\', 'DD-MM-YYYY') AND ( AD02FECFIN > \
						TO_DATE( \'%s\', 'DD-MM-YYYY') OR AD02FECFIN IS NULL) ))",
						typeRec, h_datos_franja.h_cod_act, periodd, periodh, periodd,
						periodd, periodd, periodh, periodd, periodd );

					gen_count_registros( "AG0501J",
										 v_where,
										 v_count_act );

					numMFTypes = v_count_act;

					sprintf( (char*)h_select_temp.arr,
						"SELECT PR01CODACTUACION FROM AG0501J WHERE \
						AG14CODTIPRECU=%s AND PR12CODACTIVIDAD = %d AND \
						(( PR01FECINICO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') AND \
						TO_DATE( \'%s\','DD-MM-YYYY')) OR \
						( PR01FECINICO < TO_DATE( \'%s\', 'DD-MM-YYYY') AND \
						( PR01FECFIN > TO_DATE( \'%s\', 'DD-MM-YYYY') OR \
						PR01FECFIN IS NULL) )) AND AD02CODDPTO IS NULL",
						typeRec, h_datos_franja.h_cod_act, periodd, periodh, periodd,
						periodd);

					//A�adir fin de cadena a h_select_temp.
					h_select_temp.len = strlen((char*)h_select_temp.arr );
					h_select_temp.arr[h_select_temp.len] = '\0';

					//Preparar sentencia.
					/* EXEC SQL PREPARE SENTENCIADPTOAUX2 FROM :h_select_temp; */ 
{
     struct sqlexd sqlstm={8,1};
     sqlstm.stmt = "";
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )1188;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlstm.sqhstv[0] = (unsigned char  *)&h_select_temp;
     sqlstm.sqhstl[0] = (unsigned int  )3003;
     sqlstm.sqindv[0] = (         short *)0;
     sqlstm.sqharm[0] = (unsigned int  )0;
     sqlstm.sqphsv = sqlstm.sqhstv;
     sqlstm.sqphsl = sqlstm.sqhstl;
     sqlstm.sqpind = sqlstm.sqindv;
     sqlstm.sqparm = sqlstm.sqharm;
     sqlstm.sqparc = sqlstm.sqharc;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


					//Declarar y abrir cursor para sentencia anterior.
					/* EXEC SQL DECLARE sql_cursor_dpto_aux2 CURSOR FOR SENTENCIADPTOAUX2; */ 
					/* EXEC SQL OPEN sql_cursor_dpto_aux2; */ 
{
     struct sqlexd sqlstm={8,0};
     sqlstm.stmt = "";
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )1206;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


					//Bucle para recuperar los registros del cursor.
					while(1)
					{
						/* EXEC SQL FETCH sql_cursor_dpto_aux2 INTO :h_cod_act; */ 
{
      struct sqlexd sqlstm={8,1};
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1220;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_act;
      sqlstm.sqhstl[0] = (unsigned int  )4;
      sqlstm.sqindv[0] = (         short *)0;
      sqlstm.sqharm[0] = (unsigned int  )0;
      sqlstm.sqphsv = sqlstm.sqhstv;
      sqlstm.sqphsl = sqlstm.sqhstl;
      sqlstm.sqpind = sqlstm.sqindv;
      sqlstm.sqparm = sqlstm.sqharm;
      sqlstm.sqparc = sqlstm.sqharc;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode == 1403) break;
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						if ( v_result != GENE0000_NORMAL )
						{

						} /* fin del if */

						sprintf( v_where, "PR01CODACTUACION=%d", h_cod_act );
						gen_count_registros( "PR0200",
											 v_where,
											 v_count_act );


						numMFTypes += v_count_act;

					} /* fin del while */

					//Cerrar el cursor.
					/* EXEC SQL CLOSE sql_cursor_dpto_aux2; */ 
{
     struct sqlexd sqlstm={8,0};
     sqlstm.iters = (unsigned int  )1;
     sqlstm.offset = (unsigned int  )1238;
     sqlstm.cud = sqlcud0;
     sqlstm.sqlest = (unsigned char  *)&sqlca;
     sqlstm.sqlety = (unsigned short)0;
     sqlcex(sqlctx, &sqlstm, &sqlfpn);
     if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}
					

					if ( numMFTypes > 0 )
					{ /* Alguna Actuaci�n/Dpto vigente para franja en curso */

						Duration* maxTimes= new Duration[numMFTypes];
  
						mfTypes= appScheduler->createAnyArray(numMFTypes);

						maxQuotas= appScheduler->createIntArray(numMFTypes);

						sprintf( (char*)h_select_temp.arr,
							"SELECT NVL(AD02CODDPTO,0),PR01CODACTUACION FROM \
							AG0501J WHERE AG14CODTIPRECU=%s AND PR12CODACTIVIDAD = %d \
							AND (( PR01FECINICO BETWEEN TO_DATE( \'%s\','DD-MM-YYYY') \
							AND TO_DATE( \'%s\','DD-MM-YYYY')) OR \
							( PR01FECINICO < TO_DATE( \'%s\','DD-MM-YYYY') AND \
							( PR01FECFIN > TO_DATE( \'%s\', 'DD-MM-YYYY') OR \
							PR01FECFIN IS NULL) )) AND (AD02CODDPTO IN (SELECT \
							AD02CODDPTO FROM AD0200 WHERE AG0501J.AD02CODDPTO=\
							AD0200.AD02CODDPTO AND ( AD02FECINICIO BETWEEN \
							TO_DATE( \'%s\','DD-MM-YYYY') AND \
							TO_DATE( \'%s\','DD-MM-YYYY')) OR ( AD02FECINICIO < \
							TO_DATE( \'%s\', 'DD-MM-YYYY') AND ( AD02FECFIN > \
							TO_DATE( \'%s\', 'DD-MM-YYYY') OR AD02FECFIN IS NULL) )) \
							OR AD02CODDPTO IS NULL)",
							typeRec, h_datos_franja.h_cod_act, periodd, periodh, periodd, 
							periodd, periodd, periodh, periodd, periodd );

						//A�adir fin de cadena a h_select_temp.
						h_select_temp.len = strlen((char*)h_select_temp.arr );
						h_select_temp.arr[h_select_temp.len] = '\0';

						//Preparar sentencia.
						/* EXEC SQL PREPARE SENTENCIAFRJACT2 FROM :h_select_temp; */ 
{
      struct sqlexd sqlstm={8,1};
      sqlstm.stmt = "";
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1252;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlstm.sqhstv[0] = (unsigned char  *)&h_select_temp;
      sqlstm.sqhstl[0] = (unsigned int  )3003;
      sqlstm.sqindv[0] = (         short *)0;
      sqlstm.sqharm[0] = (unsigned int  )0;
      sqlstm.sqphsv = sqlstm.sqhstv;
      sqlstm.sqphsl = sqlstm.sqhstl;
      sqlstm.sqpind = sqlstm.sqindv;
      sqlstm.sqparm = sqlstm.sqharm;
      sqlstm.sqparc = sqlstm.sqharc;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						//Declarar y abrir cursor para sentencia anterior.
						/* EXEC SQL DECLARE sql_cursor_frjact2 CURSOR FOR SENTENCIAFRJACT2; */ 
						/* EXEC SQL OPEN sql_cursor_frjact2; */ 
{
      struct sqlexd sqlstm={8,0};
      sqlstm.stmt = "";
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1270;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						//Bucle para recuperar los registros del cursor.
						while(1)
						{		

							/* EXEC SQL FETCH sql_cursor_frjact2 INTO :h_cod_dpto,
																   :h_cod_act; */ 
{
       struct sqlexd sqlstm={8,2};
       sqlstm.iters = (unsigned int  )1;
       sqlstm.offset = (unsigned int  )1284;
       sqlstm.cud = sqlcud0;
       sqlstm.sqlest = (unsigned char  *)&sqlca;
       sqlstm.sqlety = (unsigned short)0;
       sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_dpto;
       sqlstm.sqhstl[0] = (unsigned int  )4;
       sqlstm.sqindv[0] = (         short *)0;
       sqlstm.sqharm[0] = (unsigned int  )0;
       sqlstm.sqhstv[1] = (unsigned char  *)&h_cod_act;
       sqlstm.sqhstl[1] = (unsigned int  )4;
       sqlstm.sqindv[1] = (         short *)0;
       sqlstm.sqharm[1] = (unsigned int  )0;
       sqlstm.sqphsv = sqlstm.sqhstv;
       sqlstm.sqphsl = sqlstm.sqhstl;
       sqlstm.sqpind = sqlstm.sqindv;
       sqlstm.sqparm = sqlstm.sqharm;
       sqlstm.sqparc = sqlstm.sqharc;
       sqlcex(sqlctx, &sqlstm, &sqlfpn);
       if (sqlca.sqlcode == 1403) break;
       if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


							if ( v_result != GENE0000_NORMAL )
							{

							} /* fin del if */

							if ( h_cod_dpto != 0 )
							{
								IlcInt valor_mfTypes = ( h_cod_act * 1000 ) + h_cod_dpto;


								mfTypes[indice] = (IlcAny)valor_mfTypes;
								maxQuotas[indice] = h_datos_franja.h_num_citas;

								gen_obtener_duracion( minTotales, v_dias, v_horas, v_min );
								maxTimes[indice]= Duration(v_dias,v_horas,v_min);

								indice++;

							} /* fin del if */
							else
							{
								sprintf( (char*)h_select_dpto.arr,
									"SELECT AD02CODDPTO FROM PR0200 WHERE \
									PR01CODACTUACION=%d", h_cod_act );

								//A�adir fin de cadena a h_select_dpto.
								h_select_dpto.len = strlen((char*)h_select_dpto.arr );
								h_select_dpto.arr[h_select_dpto.len] = '\0';

								//Preparar sentencia.
								/* EXEC SQL PREPARE SENTENCIADPTO1 FROM :h_select_dpto; */ 
{
        struct sqlexd sqlstm={8,1};
        sqlstm.stmt = "";
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )1306;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)0;
        sqlstm.sqhstv[0] = (unsigned char  *)&h_select_dpto;
        sqlstm.sqhstl[0] = (unsigned int  )3003;
        sqlstm.sqindv[0] = (         short *)0;
        sqlstm.sqharm[0] = (unsigned int  )0;
        sqlstm.sqphsv = sqlstm.sqhstv;
        sqlstm.sqphsl = sqlstm.sqhstl;
        sqlstm.sqpind = sqlstm.sqindv;
        sqlstm.sqparm = sqlstm.sqharm;
        sqlstm.sqparc = sqlstm.sqharc;
        sqlcex(sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


								//Declarar y abrir cursor para sentencia anterior.
								/* EXEC SQL DECLARE sql_cursor_dpto1 CURSOR FOR SENTENCIADPTO1; */ 
								/* EXEC SQL OPEN sql_cursor_dpto1; */ 
{
        struct sqlexd sqlstm={8,0};
        sqlstm.stmt = "";
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )1324;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)0;
        sqlcex(sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


								//Bucle para recuperar los registros del cursor.
								while(1)
								{		
									/* EXEC SQL FETCH sql_cursor_dpto1 INTO :h_cod_dpto; */ 
{
         struct sqlexd sqlstm={8,1};
         sqlstm.iters = (unsigned int  )1;
         sqlstm.offset = (unsigned int  )1338;
         sqlstm.cud = sqlcud0;
         sqlstm.sqlest = (unsigned char  *)&sqlca;
         sqlstm.sqlety = (unsigned short)0;
         sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_dpto;
         sqlstm.sqhstl[0] = (unsigned int  )4;
         sqlstm.sqindv[0] = (         short *)0;
         sqlstm.sqharm[0] = (unsigned int  )0;
         sqlstm.sqphsv = sqlstm.sqhstv;
         sqlstm.sqphsl = sqlstm.sqhstl;
         sqlstm.sqpind = sqlstm.sqindv;
         sqlstm.sqparm = sqlstm.sqharm;
         sqlstm.sqparc = sqlstm.sqharc;
         sqlcex(sqlctx, &sqlstm, &sqlfpn);
         if (sqlca.sqlcode == 1403) break;
         if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


									if ( v_result != GENE0000_NORMAL )
									{

									} /* fin del if */

									IlcInt valor_mfTypes = ( h_cod_act * 1000 ) + h_cod_dpto;


									mfTypes[indice] = (IlcAny)valor_mfTypes;
									maxQuotas[indice] = h_datos_franja.h_num_citas;

									gen_obtener_duracion( minTotales, v_dias, v_horas, v_min );
									maxTimes[indice]= Duration(v_dias,v_horas,v_min);

									indice++;

								} /* fin del while */
						
								//Cerrar el cursor.
								/* EXEC SQL CLOSE sql_cursor_dpto1; */ 
{
        struct sqlexd sqlstm={8,0};
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )1356;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)0;
        sqlcex(sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


							} /* fin del else */

						} /* fin del while */
						
						//Cerrar el cursor.
						/* EXEC SQL CLOSE sql_cursor_frjact2; */ 
{
      struct sqlexd sqlstm={8,0};
      sqlstm.iters = (unsigned int  )1;
      sqlstm.offset = (unsigned int  )1370;
      sqlstm.cud = sqlcud0;
      sqlstm.sqlest = (unsigned char  *)&sqlca;
      sqlstm.sqlety = (unsigned short)0;
      sqlcex(sqlctx, &sqlstm, &sqlfpn);
      if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


						mftBand= new MedicalFactTypesBand(
											appScheduler->createAnySet(mfTypes), mfTypes,
											maxTimes, maxQuotas, app->getCalendar());

					} /* fin del if */
				} /* fin del else */

			} /* fin del else */

 			if ( numMFTypes > 0 )
			{ /* Alguna Actuaci�n/Dpto vigente para franja en curso */

				quota = h_datos_franja.h_num_citas;

				if ( v_tpo_reserva > 0 )
				{
					IlcInt auxiliar = minTotales * v_tpo_reserva / 100;
					minTotales = minTotales - auxiliar;
					
					if ( reserveType == FIX_INTERVAL )
					{ // Asignaci�n del recurso en Intervalos fijos 

						numIntervalos = minTotales / h_datos_franja.h_interv_cita;
						minTotales = numIntervalos * h_datos_franja.h_interv_cita;
					}
					
					gen_obtener_duracion( minTotales, v_dias, v_horas, v_min );
					horaFin = h_datos_franja.h_inihh + v_horas;
					minFin = h_datos_franja.h_inimm + v_min;

					if ( minFin >= C_GEN_MINUTOS_EN_HORA )
					{
						horaFin++;
						minFin -= C_GEN_MINUTOS_EN_HORA;
					}

					band = new MedicalServiceCalendarBand(
									profile,
									DayPeriod(h_datos_franja.h_inihh,
										h_datos_franja.h_inimm,
										horaFin,
										minFin), 
									quota,
									mftBand,
									tree,	//Arbol de restricciones por franja.
									reserveType); //Modo asignaci�n por franja.

				}
				else
				{

					band = new MedicalServiceCalendarBand(
									profile,
									DayPeriod(h_datos_franja.h_inihh,
										h_datos_franja.h_inimm,
										h_datos_franja.h_finhh,
										h_datos_franja.h_finmm), 
									quota,
									mftBand,
									tree, //Arbol de restricciones por franja.
									reserveType); //Modo asignaci�n por franja.

				}

				if ( h_datos_franja.h_indlu == -1 )
				{ /* Lunes entra en la franja */

					band->setWeekDay(LUNES);

				} /* fin del if */
				if ( h_datos_franja.h_indma == -1 )
				{ /* Martes entra en la franja */

					band->setWeekDay(MARTES);

				} /* fin del if */
				if ( h_datos_franja.h_indmi == -1 )
				{ /* Mi�rcoles entra en la franja */

					band->setWeekDay(MIERCOLES);

				} /* fin del if */
				if ( h_datos_franja.h_indju == -1 )
				{ /* Jueves entra en la franja */

					band->setWeekDay(JUEVES);

				} /* fin del if */
				if ( h_datos_franja.h_indvi == -1 )
				{ /* Viernes entra en la franja */

					band->setWeekDay(VIERNES);

				} /* fin del if */
				if ( h_datos_franja.h_indsa == -1 )
				{ /* S�bado entra en la franja */

					band->setWeekDay(SABADO);

				} /* fin del if */
				if ( h_datos_franja.h_inddo == -1 )
				{ /* Domingo entra en la franja */

					band->setWeekDay(DOMINGO);

				} /* fin del if */
				if ( v_fecha_baja_num == 0 || v_fecha_baja_num > periodh_num )
				{ /* fecha de baja nula � mayor que fecha fin del per. de vig. */

					if ( reserveType == FIX_INTERVAL )
					{ /* Asignaci�n del recurso en Intervalos fijos */

						gen_obtener_duracion( h_datos_franja.h_interv_cita,
											  v_dias, v_horas, v_min );

						Duration d =  Duration(v_dias,v_horas,v_min);

						IlcInt numTimeUnits= d.getDuration(MINUTES, 
															profile->getMedicalServiceSingle()->getCalendar()->
															getTimeStepPack());

						profile->addBand(band, NULL, numTimeUnits);

					} /* fin del if */
					else
					{ /* Otro modo de asignaci�n */

						profile->addBand(band, NULL );

					} /* fin del else */

				} /* fin del if */
				else
				{ /* fecha de baja dentro del per. de vigencia */

					gen_desglosar_fechad( h_datos_franja.h_fecha_baja,
														diab, mesb, aniob );	

					dateBand = new Date( diab, mesb, aniob );

					if ( reserveType == FIX_INTERVAL )
					{ /* Asignaci�n del recurso en Intervalos fijos */

						gen_obtener_duracion( h_datos_franja.h_interv_cita,
											  v_dias, v_horas, v_min );

						Duration d =  Duration(v_dias,v_horas,v_min);

						IlcInt numTimeUnits= d.getDuration(MINUTES, 
															profile->getMedicalServiceSingle()->getCalendar()->
															getTimeStepPack());

						profile->addBand(band, dateBand, numTimeUnits);

					} /* fin del if */
					else
					{ /* Otro modo de asignaci�n */

						profile->addBand(band, dateBand );

					} /* fin del else */

				} /* fin del else */
  
			} /* fin del if */

		} /* fin del if */

	} /* fin del while */
			
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_frja; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1384;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


} /* fin de funci�n gen_read_bands */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_properties
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *		app				ClinicApplication*		Puntero a datos generales
 *												de la cl�nica
 *
 * DESCRIPCI�N:
 *		Lectura de los Tipos de Restricci�n de la cl�nica, de momento s�lo
 *		los de tipo Num�rico.
 *
 * RESULTADOS:
 *      Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   18/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void gen_read_properties(
							ClinicApplication* app
						)
{ /* inicio funci�n */

/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long v_result = GENE0000_NORMAL;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	

	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 


	sprintf( (char*)h_select.arr,
		"SELECT AG1600.AG16CODTIPREST,AG16DESTIPREST,MAX(TO_NUMBER(AG17VALDESDETRE)), \
		NVL(MAX(TO_NUMBER(AG17VALHASTATRE)),0) FROM AG1600,AG1700 WHERE \
		AG18CODTIPDATO='1' AND AG16TABLAVALOR IS NULL AND AG1700.AG16CODTIPREST = \
		AG1600.AG16CODTIPREST GROUP BY AG1600.AG16CODTIPREST,AG16DESTIPREST" );

	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAPRO FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1398;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_pro CURSOR FOR SENTENCIAPRO; */ 
	/* EXEC SQL OPEN sql_cursor_pro; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1416;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{		
		h_datos_pro.h_codigo_tiprest = 0;
		strcpy( h_datos_pro.h_descrip_tiprest, "" );
		h_datos_pro.h_desde_tiprest = 0;
		h_datos_pro.h_hasta_tiprest = 0;
		
		/* EXEC SQL FETCH sql_cursor_pro INTO :h_datos_pro; */ 
{
  struct sqlexd sqlstm={8,4};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1430;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_pro.h_codigo_tiprest;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_pro.h_descrip_tiprest;
  sqlstm.sqhstl[1] = (unsigned int  )31;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_pro.h_desde_tiprest;
  sqlstm.sqhstl[2] = (unsigned int  )4;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_pro.h_hasta_tiprest;
  sqlstm.sqhstl[3] = (unsigned int  )4;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{
		}


		if ( h_datos_pro.h_hasta_tiprest == 0 )
		{ /* no hay valor hasta */

			Property* property= new Property(app, h_datos_pro.h_codigo_tiprest,
				 h_datos_pro.h_descrip_tiprest, h_datos_pro.h_desde_tiprest );
			app->addProperty(property);

		} /* fin del if */
		else
		{ /* hay valor hasta */

			Property* property= new Property(app, h_datos_pro.h_codigo_tiprest,
				 h_datos_pro.h_descrip_tiprest, h_datos_pro.h_hasta_tiprest );
			app->addProperty(property);

		} /* fin del else */

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_pro; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1460;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	sprintf( (char*)h_select.arr,
		"SELECT AG16CODTIPREST,AG16DESTIPREST FROM AG1600 WHERE \
		(AG18CODTIPDATO='2' AND AG16TABLAVALOR IS NULL) OR AG16TABLAVALOR IS NOT NULL" );

	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAPROALF FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1474;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_pro_alf CURSOR FOR SENTENCIAPROALF; */ 
	/* EXEC SQL OPEN sql_cursor_pro_alf; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1492;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{		
		h_datos_pro_alf.h_codigo_tiprest = 0;
		strcpy( h_datos_pro_alf.h_descrip_tiprest, "" );
		
		/* EXEC SQL FETCH sql_cursor_pro_alf INTO :h_datos_pro_alf; */ 
{
  struct sqlexd sqlstm={8,2};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1506;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_pro_alf.h_codigo_tiprest;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_pro_alf.h_descrip_tiprest;
  sqlstm.sqhstl[1] = (unsigned int  )31;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{
		}


		Property* property= new Property(app, h_datos_pro_alf.h_codigo_tiprest,
				  h_datos_pro_alf.h_descrip_tiprest, 255, CADENA );
		app->addProperty(property);

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_pro_alf; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1528;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


} /* fin de gen_read_properties */


/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_tree
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *		app				ClinicApplication*		Puntero a datos generales
 *												de la cl�nica
 *		codRecurso		const IlcInt			C�digo del recurso actual
 *
 * DESCRIPCI�N:
 *		Lectura del �rbol de restricciones de un recurso.
 *
 * RESULTADOS:
 *      PropertyConditionTree*, puntero al �rbol de restricciones del recurso.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   19/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
PropertyConditionTree*
gen_read_tree(
				ClinicApplication*		app,
				const IlcInt			codRecurso
			)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long			v_result = GENE0000_NORMAL;
	PropertyConditionTree*	tree;
	PropertyConditionSet*	pcSet;
	PropertyCondition*		pc;
	PropertyCondition*		pc_aux;
	Property*				property;	
	IlcInt					stateFrom;
	IlcInt					stateTo;
	IncluType				inclusion;
	t_datos_nodo			v_nodo_ant;
	unsigned long			i = 0;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	

	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

    tree= new PropertyConditionTree();

	v_nodo_ant.v_level_ant = 0;
	v_nodo_ant.v_cod_tiprest_ant = 0;
	strcpy( v_nodo_ant.v_desde_tiprest_ant, "" );
	strcpy( v_nodo_ant.v_hasta_tiprest_ant, "" );
	v_nodo_ant.v_ind_inex_ant = 0;

	sprintf( (char*)h_select.arr,
		"SELECT AG13LEVEL,AG16CODTIPREST,AG13VALDESDERES,AG13VALHASTARES, \
		AG13INDINCEXCL FROM AG1301 WHERE AG11CODRECURSO=%d",
		codRecurso );
    
	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAARB FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1542;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_arb CURSOR FOR SENTENCIAARB; */ 
	/* EXEC SQL OPEN sql_cursor_arb; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1560;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		h_datos_arb.h_level = 0;		
		h_datos_arb.h_cod_tiprest = 0;
		strcpy( h_datos_arb.h_desde_tiprest, "" );
		strcpy( h_datos_arb.h_hasta_tiprest, "" );
		h_datos_arb.h_ind_inex = 0;
		
		/* EXEC SQL FETCH sql_cursor_arb INTO :h_datos_arb; */ 
{
  struct sqlexd sqlstm={8,5};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1574;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_arb.h_level;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_arb.h_cod_tiprest;
  sqlstm.sqhstl[1] = (unsigned int  )4;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)h_datos_arb.h_desde_tiprest;
  sqlstm.sqhstl[2] = (unsigned int  )21;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)h_datos_arb.h_hasta_tiprest;
  sqlstm.sqhstl[3] = (unsigned int  )21;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_arb.h_ind_inex;
  sqlstm.sqhstl[4] = (unsigned int  )4;
  sqlstm.sqindv[4] = (         short *)0;
  sqlstm.sqharm[4] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

		
		if ( v_result != GENE0000_NORMAL )
		{
		}


		if ( h_datos_arb.h_ind_inex == 0 )
		{ /* inclusi�n */

			inclusion = INCLUSION;

		} /* fin del if */
		else
		{ /* exclusi�n */

			inclusion = EXCLUSION;

		} /* fin del else */

		if ( v_nodo_ant.v_level_ant >= h_datos_arb.h_level )
		{ /* hoja del �rbol de restricciones */

			//A�ade la rama al �rbol.
			tree->addPropertyConditionSet(pcSet);			

		} /* fin del if */
		
		if ( h_datos_arb.h_level == 1 )
		{ /* Primer nivel de la jerarqu�a */			

			pcSet= new PropertyConditionSet(app);

			property= app->getProperty(h_datos_arb.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				stateFrom = atoi(h_datos_arb.h_desde_tiprest);

				if (atoi( h_datos_arb.h_hasta_tiprest) == 0)
				{ /* sin valor hasta */

					stateTo = stateFrom;

				}/* fin del if */
				else
				{ /* con valor hasta */

					stateTo = atoi(h_datos_arb.h_hasta_tiprest);

				} /* fin del else */
				
				pcSet->addPropertyCondition(new PropertyCondition(property,
														  stateFrom, stateTo, 
														  inclusion));

			} /* fin del if */
			else
			{ //Tipo CADENA.

				for ( i = 0; i < strlen(h_datos_arb.h_desde_tiprest); i++ )
				{
					if ( h_datos_arb.h_desde_tiprest[i] == ' ' )
					{
						h_datos_arb.h_desde_tiprest[i] = '\0';
						break;

					} /* fin del if */
				} /* fin del for */


				pcSet->addPropertyCondition(new PropertyCondition(property,
												inclusion,
												h_datos_arb.h_desde_tiprest));
			} /* fin del else */

		} /* fin del if */
		else if ( h_datos_arb.h_level == 2 )
		{ /* Segundo nivel de la jerarqu�a */			
			
			pc = pcSet->getPropertyCondition(0);

			pcSet = new PropertyConditionSet(app);

			pcSet->addPropertyCondition( pc );

			property= app->getProperty(h_datos_arb.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				stateFrom = atoi(h_datos_arb.h_desde_tiprest);

				if ( atoi(h_datos_arb.h_hasta_tiprest) == 0 )
				{ /* sin valor hasta */

					stateTo = stateFrom;

				}/* fin del if */
				else
				{ /* con valor hasta */

					stateTo = atoi(h_datos_arb.h_hasta_tiprest);

				} /* fin del else */

				pcSet->addPropertyCondition(new PropertyCondition(property,
												stateFrom, stateTo, 
												inclusion));
					
			} /* fin del if */
			else
			{ //Tipo CADENA.

				for ( i = 0; i < strlen(h_datos_arb.h_desde_tiprest); i++ )
				{
					if ( h_datos_arb.h_desde_tiprest[i] == ' ' )
					{
						h_datos_arb.h_desde_tiprest[i] = '\0';
						break;

					} /* fin del if */

				} /* fin del for */

				pcSet->addPropertyCondition(new PropertyCondition(property,
												inclusion,
												h_datos_arb.h_desde_tiprest));
			} /* fin del else */

		} /* fin del else if */
		else if ( h_datos_arb.h_level == 3 )
		{ /* Tercer nivel de la jerarqu�a */			
			
			pc = pcSet->getPropertyCondition(0);
					
			pc_aux = pcSet->getPropertyCondition(1);
			
			pcSet = new PropertyConditionSet(app);

			pcSet->addPropertyCondition( pc );

			pcSet->addPropertyCondition( pc_aux );

			property= app->getProperty(h_datos_arb.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				stateFrom = atoi(h_datos_arb.h_desde_tiprest);

				if ( atoi(h_datos_arb.h_hasta_tiprest) == 0 )
				{ /* sin valor hasta */

					stateTo = stateFrom;

				}/* fin del if */
				else
				{ /* con valor hasta */

					stateTo = atoi(h_datos_arb.h_hasta_tiprest);

				} /* fin del else */
				
				pcSet->addPropertyCondition(new PropertyCondition(property,
														  stateFrom, stateTo, 
														  inclusion));

			} /* fin del if */
			else
			{ //Tipo CADENA.

				for ( i = 0; i < strlen(h_datos_arb.h_desde_tiprest); i++ )
				{
					if ( h_datos_arb.h_desde_tiprest[i] == ' ' )
					{
						h_datos_arb.h_desde_tiprest[i] = '\0';
						break;

					} /* fin del if */

				} /* fin del for */

				pcSet->addPropertyCondition(new PropertyCondition(property,
												inclusion,
												h_datos_arb.h_desde_tiprest));
			} /* fin del else */

		} /* fin del else if */

		v_nodo_ant.v_level_ant = h_datos_arb.h_level;
		v_nodo_ant.v_cod_tiprest_ant = h_datos_arb.h_cod_tiprest;
		strcpy( v_nodo_ant.v_desde_tiprest_ant, h_datos_arb.h_desde_tiprest );
		strcpy( v_nodo_ant.v_hasta_tiprest_ant, h_datos_arb.h_hasta_tiprest );
		v_nodo_ant.v_ind_inex_ant = h_datos_arb.h_ind_inex;

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_arb; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1608;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	if ( sqlca.sqlerrd[2] > 0 )
	{ /* alg�n elemento en la fetch */

		tree->addPropertyConditionSet(pcSet);

	} /* fin del if */

	return(tree);	
  
} /* fin de funci�n gen_read_tree */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_tree_band
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *		app				ClinicApplication*		Puntero a datos generales
 *												de la cl�nica
 *		codRecurso		const IlcInt			C�digo del recurso actual
 *		codPerfil		const int				C�digo del perfil actual
 *		codFranja		const int				C�dgio de la franja actual
 *
 * DESCRIPCI�N:
 *		Lectura del �rbol de restricciones de una franja.
 *
 * RESULTADOS:
 *      PropertyConditionTree*, puntero al �rbol de restricciones de la franja.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   21/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
PropertyConditionTree*
gen_read_tree_band(
					ClinicApplication*	app,								
					const IlcInt		codRecurso,
					const int			codPerfil,
					const int			codFranja
				)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long			v_result = GENE0000_NORMAL;
	PropertyConditionTree*	tree;
	PropertyConditionSet*	pcSet;
	PropertyCondition*		pc;
	PropertyCondition*		pc_aux;
	Property*				property;	
	IlcInt					stateFrom;
	IlcInt					stateTo;
	IncluType				inclusion;
	t_datos_nodo			v_nodo_ant;
	unsigned long			i = 0;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 
	
    tree = new PropertyConditionTree();


	v_nodo_ant.v_level_ant = 0;
	v_nodo_ant.v_cod_tiprest_ant = 0;
	strcpy( v_nodo_ant.v_desde_tiprest_ant, "" );
	strcpy( v_nodo_ant.v_hasta_tiprest_ant, "" );
	v_nodo_ant.v_ind_inex_ant = 0;

	sprintf( (char*)h_select_rest.arr,
		"SELECT AG12LEVEL,AG16CODTIPREST,AG12VALDESDERES,AG12VALHASTARES, \
		AG12INDINCEXCL FROM AG1201 WHERE AG11CODRECURSO=%d AND AG07CODPERFIL=%d \
		AND AG04CODFRANJA=%d",
		codRecurso, codPerfil, codFranja );
    
	//A�adir fin de cadena a h_select_rest.
	h_select_rest.len = strlen((char*)h_select_rest.arr );
	h_select_rest.arr[h_select_rest.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAARBB FROM :h_select_rest; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1622;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select_rest;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_arbb CURSOR FOR SENTENCIAARBB; */ 
	/* EXEC SQL OPEN sql_cursor_arbb; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1640;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		h_datos_arb.h_level = 0;		
		h_datos_arb.h_cod_tiprest = 0;
		strcpy( h_datos_arb.h_desde_tiprest, "" );
		strcpy( h_datos_arb.h_hasta_tiprest, "" );
		h_datos_arb.h_ind_inex = 0;
		
		/* EXEC SQL FETCH sql_cursor_arbb INTO :h_datos_arb; */ 
{
  struct sqlexd sqlstm={8,5};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1654;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_arb.h_level;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_arb.h_cod_tiprest;
  sqlstm.sqhstl[1] = (unsigned int  )4;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)h_datos_arb.h_desde_tiprest;
  sqlstm.sqhstl[2] = (unsigned int  )21;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)h_datos_arb.h_hasta_tiprest;
  sqlstm.sqhstl[3] = (unsigned int  )21;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_arb.h_ind_inex;
  sqlstm.sqhstl[4] = (unsigned int  )4;
  sqlstm.sqindv[4] = (         short *)0;
  sqlstm.sqharm[4] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{

		} /* fin del if */

		
		if ( h_datos_arb.h_ind_inex == 0 )
		{ /* inclusi�n */

			inclusion = INCLUSION;

		} /* fin del if */
		else
		{ /* exclusi�n */

			inclusion = EXCLUSION;

		} /* fin del else */

		if ( v_nodo_ant.v_level_ant >= h_datos_arb.h_level )
		{ /* hoja del �rbol de restricciones */

			tree->addPropertyConditionSet(pcSet);			

		} /* fin del if */
		
		if ( h_datos_arb.h_level == 1 )
		{ /* Primer nivel de la jerarqu�a */			

			pcSet= new PropertyConditionSet(app);

			property= app->getProperty(h_datos_arb.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				stateFrom = atoi(h_datos_arb.h_desde_tiprest);

				if (atoi( h_datos_arb.h_hasta_tiprest) == 0)
				{ /* sin valor hasta */

					stateTo = stateFrom;

				}/* fin del if */
				else
				{ /* con valor hasta */

					stateTo = atoi(h_datos_arb.h_hasta_tiprest);

				} /* fin del else */
				
				pcSet->addPropertyCondition(new PropertyCondition(property,
														  stateFrom, stateTo, 
														  inclusion));
			} /* fin del if */
			else
			{ //Tipo CADENA.

				for ( i = 0; i < strlen(h_datos_arb.h_desde_tiprest); i++ )
				{
					if ( h_datos_arb.h_desde_tiprest[i] == ' ' )
					{
						h_datos_arb.h_desde_tiprest[i] = '\0';
						break;

					} /* fin del if */
				} /* fin del for */

				pcSet->addPropertyCondition(new PropertyCondition(property,
												inclusion,
												h_datos_arb.h_desde_tiprest));
			} /* fin del else */

		} /* fin del if */
		else if ( h_datos_arb.h_level == 2 )
		{ /* Segundo nivel de la jerarqu�a */			
			
			pc = pcSet->getPropertyCondition(0);

			pcSet = new PropertyConditionSet(app);

			pcSet->addPropertyCondition( pc );

			property= app->getProperty(h_datos_arb.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				stateFrom = atoi(h_datos_arb.h_desde_tiprest);

				if ( atoi(h_datos_arb.h_hasta_tiprest) == 0 )
				{ /* sin valor hasta */

					stateTo = stateFrom;

				}/* fin del if */
				else
				{ /* con valor hasta */

					stateTo = atoi(h_datos_arb.h_hasta_tiprest);

				} /* fin del else */

				pcSet->addPropertyCondition(new PropertyCondition(property,
														  stateFrom, stateTo, 
														  inclusion));
			} /* fin del if */
			else
			{ //Tipo CADENA.

				for ( i = 0; i < strlen(h_datos_arb.h_desde_tiprest); i++ )
				{
					if ( h_datos_arb.h_desde_tiprest[i] == ' ' )
					{
						h_datos_arb.h_desde_tiprest[i] = '\0';
						break;

					} /* fin del if */
				} /* fin del for */

				pcSet->addPropertyCondition(new PropertyCondition(property,
												inclusion,
												h_datos_arb.h_desde_tiprest));
			} /* fin del else */

		} /* fin del else if */
		else if ( h_datos_arb.h_level == 3 )
		{ /* Tercer nivel de la jerarqu�a */			
			
			pc = pcSet->getPropertyCondition(0);
					
			pc_aux = pcSet->getPropertyCondition(1);
			
			pcSet = new PropertyConditionSet(app);

			pcSet->addPropertyCondition( pc );

			pcSet->addPropertyCondition( pc_aux );

			property= app->getProperty(h_datos_arb.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				stateFrom = atoi(h_datos_arb.h_desde_tiprest);

				if ( atoi(h_datos_arb.h_hasta_tiprest) == 0 )
				{ /* sin valor hasta */

					stateTo = stateFrom;

				}/* fin del if */
				else
				{ /* con valor hasta */

					stateTo = atoi(h_datos_arb.h_hasta_tiprest);

				} /* fin del else */
				
				pcSet->addPropertyCondition(new PropertyCondition(property,
														  stateFrom, stateTo, 
														  inclusion));
			} /* fin del if */
			else
			{ //Tipo CADENA.

				for ( i = 0; i < strlen(h_datos_arb.h_desde_tiprest); i++ )
				{
					if ( h_datos_arb.h_desde_tiprest[i] == ' ' )
					{
						h_datos_arb.h_desde_tiprest[i] = '\0';
						break;

					} /* fin del if */
				} /* fin del for */

				pcSet->addPropertyCondition(new PropertyCondition(property,
												inclusion,
												h_datos_arb.h_desde_tiprest));
			} /* fin del else */

		} /* fin del else if */

		v_nodo_ant.v_level_ant = h_datos_arb.h_level;
		v_nodo_ant.v_cod_tiprest_ant = h_datos_arb.h_cod_tiprest;
		strcpy( v_nodo_ant.v_desde_tiprest_ant, h_datos_arb.h_desde_tiprest );
		strcpy( v_nodo_ant.v_hasta_tiprest_ant, h_datos_arb.h_hasta_tiprest );
		v_nodo_ant.v_ind_inex_ant = h_datos_arb.h_ind_inex;

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_arbb; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1688;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	if ( sqlca.sqlerrd[2] > 0 )
	{ /* alg�n elemento en la fetch */

		tree->addPropertyConditionSet(pcSet);

	} /* fin del if */

	return(tree);	
  
} /* fin de funci�n gen_read_tree_band */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_act_dep
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------
 *		states			IlcAnyArray				Posibles estados (actuaciones
 *												por Departamento), que se pue-
 *												den realizar en la cl�nica
 * DESCRIPCI�N:
 *		Lectura de las posibles actuaciones por departamento, posible estados,
 *		que se pueden realizar en la cl�nica..
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   28/11/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_read_act_dep( 
					IlcAnyArray	states
				)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long	v_result = GENE0000_NORMAL;
	unsigned int	indice = 0;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 


	strcpy( (char*)h_select.arr,
		"SELECT AD02CODDPTO,PR01CODACTUACION FROM PR0200" );
    
	//A�adir fin de cadena a h_select_rest.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIACT FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1702;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_act CURSOR FOR SENTENCIACT; */ 
	/* EXEC SQL OPEN sql_cursor_act; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1720;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		h_datos_act.h_cod_act = 0;
		h_datos_act.h_cod_dpto = 0;
		
		/* EXEC SQL FETCH sql_cursor_act INTO :h_datos_act; */ 
{
  struct sqlexd sqlstm={8,2};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1734;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_act.h_cod_dpto;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_act.h_cod_act;
  sqlstm.sqhstl[1] = (unsigned int  )4;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{

		} /* fin del if */

		IlcInt valor_state = (h_datos_act.h_cod_act*1000) +
							 h_datos_act.h_cod_dpto;
		

		states[indice] = (IlcAny)valor_state;

		indice++;

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_act; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1756;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


} /* fin de funci�n gen_read_act_dep */




/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_citas_paciente
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------------
 *		client			ClinicClient*			Puntero al paciente en curso
 *		codClient		const IlcInt			C�digo del cliente del que se
 *												leen las citas concertadas en
 *												periodo de estudio
 *		ae_fecha_d		char*					Fecha inicio periodo de estudio
 *		ae_fecha_h		char*					Fecha fin periodo de estudio
 *
 * DESCRIPCI�N:
 *		Lectura de las citas concertadas en periodo de estudio para el paciente
 *		en curso.
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   01/12/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_read_citas_paciente(
							ClinicClient*	client,
							const IlcInt	codClient,
							char*			ae_fecha_d,
							char*			ae_fecha_h
						)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long	v_result = GENE0000_NORMAL;
	char			v_dato[C_GEN_LEN_DATO + 1] = "";
	char			v_fecfin_paciente[C_GEN_LEN_FEHOR + 1] = "";
	char			v_fecfin[C_GEN_LEN_FECHA + 1] = "";
	char			v_fecini[C_GEN_LEN_FECHA + 1] = "";
	char			v_fecha_d[C_GEN_LEN_FECHA + 1] = "";
	unsigned long	v_fecini_num = 0;
	unsigned long	v_fecfin_num = 0;
	unsigned long	v_fecd_num = 0;
	unsigned long	v_fech_num = 0;
	unsigned int	v_diad, v_mesd, v_aniod, v_horad, v_mind;
	unsigned int	v_diah, v_mesh, v_anioh, v_horah, v_minh;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 


	sprintf( v_dato,
			"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') - 1, 'DD-MM-YYYY')",
			ae_fecha_d );
	gen_obtener_dato( "DUAL",
						"",
						v_dato,
						v_fecha_d );

	v_fecd_num = gen_fecha_numero( ae_fecha_d );
	v_fech_num = gen_fecha_numero( ae_fecha_h );

	sprintf( (char*)h_select.arr,
		"SELECT DISTINCT CI15NUMFASECITA,TO_CHAR(CI15FECCONCPAC,'DD-MM-YYYY hh24:MI'), \
		CI15NUMDIASPAC,CI15NUMHORAPAC,CI15NUMMINUPAC FROM CI0101J,PR0431J WHERE \
		CI21CODPERSONA=%d AND (PR0431J.CI01SITCITA='1' OR PR0431J.CI01SITCITA='3') AND \
		CI0101J.CI31NUMSOLICIT=PR0431J.CI31NUMSOLICIT AND \
		CI0101J.CI01NUMCITA=PR0431J.CI01NUMCITA AND PR0431J.CI01SITCITA=CI0101J.CI01SITCITA \
		AND CI15FECCONCPAC IS NOT NULL AND CI15FECCONCPAC >= TO_DATE( \'%s\','DD-MM-YYYY') \
		AND CI15FECCONCPAC < TO_DATE( \'%s\','DD-MM-YYYY')",
		codClient, v_fecha_d, ae_fecha_h );
    
	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIACITAS FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1770;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_citas CURSOR FOR SENTENCIACITAS; */ 
	/* EXEC SQL OPEN sql_cursor_citas; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1788;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		h_datos_cita.h_numfase_cita = 0;
		strcpy( h_datos_cita.h_fecini_paciente, "" );
		h_datos_cita.h_num_dias = 0;
		h_datos_cita.h_num_horas = 0;
		h_datos_cita.h_num_min = 0;
		
		/* EXEC SQL FETCH sql_cursor_citas INTO :h_datos_cita; */ 
{
  struct sqlexd sqlstm={8,5};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1802;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_cita.h_numfase_cita;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_cita.h_fecini_paciente;
  sqlstm.sqhstl[1] = (unsigned int  )17;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_cita.h_num_dias;
  sqlstm.sqhstl[2] = (unsigned int  )4;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_cita.h_num_horas;
  sqlstm.sqhstl[3] = (unsigned int  )4;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_cita.h_num_min;
  sqlstm.sqhstl[4] = (unsigned int  )4;
  sqlstm.sqindv[4] = (         short *)0;
  sqlstm.sqharm[4] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{

		} /* fin del if */

		sprintf( v_dato,
			"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY HH24:MI') + %d + %d/24 + %d/1440, \
			'DD-MM-YYYY HH24:MI')",
			h_datos_cita.h_fecini_paciente, h_datos_cita.h_num_dias,
			h_datos_cita.h_num_horas, h_datos_cita.h_num_min );

		gen_obtener_dato( "DUAL",
							"",
							v_dato,
							v_fecfin_paciente);


		strncpy( v_fecini, h_datos_cita.h_fecini_paciente, C_GEN_LEN_FECHA );
		strncpy( v_fecfin, v_fecfin_paciente, C_GEN_LEN_FECHA );

		v_fecini_num = gen_fecha_numero(v_fecini);
		v_fecfin_num = gen_fecha_numero(v_fecfin);

		if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
			&& v_fecfin_num < v_fech_num )
		{ /* Cita que empieza antes del periodo de estudio y finaliza
			antes que dicho periodo */

			//Fecha hora desde.
			gen_desglosar_fechad( ae_fecha_d, v_diad, v_mesd, v_aniod );

			//Fecha hora hasta.
			gen_desglosar_fechad( v_fecfin, v_diah, v_mesh, v_anioh );
			gen_desglosar_fecha_hora( v_fecfin_paciente, v_horah, v_minh );

			Period*	period = new Period( 
						Date(v_diad, v_mesd, v_aniod ),
						Date( v_diah, v_mesh, v_anioh, v_horah, v_minh ) );

			client->addClinicClientBusy( new ClinicClientBusy( period ));

		} /* fin del if */

		if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
			&& v_fecfin_num >= v_fech_num )
		{ /* Cita que empieza antes del periodo de estudio y finaliza
			despu�s que dicho periodo */

			//Fecha desde.
			gen_desglosar_fechad( ae_fecha_d, v_diad, v_mesd, v_aniod );

			//Fecha hasta.
			gen_desglosar_fechad( ae_fecha_h, v_diah, v_mesh, v_anioh );

			Period*	period = new Period( 
							Date( v_diad, v_mesd, v_aniod ),
							Date( v_diah, v_mesh, v_anioh ) );
			client->addClinicClientBusy( new ClinicClientBusy( period ));

		} /* fin del if */

		if ( v_fecd_num <= v_fecini_num && v_fecfin_num < v_fech_num )
		{ /* Cita comprendido dentro del periodo de estudio */

			//Fecha desde.
			gen_desglosar_fechad( v_fecini, v_diad, v_mesd, v_aniod );
			gen_desglosar_fecha_hora( h_datos_cita.h_fecini_paciente,
														 v_horad, v_mind );

			//Fecha hasta.
			gen_desglosar_fechad( v_fecfin, v_diah, v_mesh, v_anioh );
			gen_desglosar_fecha_hora( v_fecfin_paciente, v_horah, v_minh );

			Period*	period = new Period( 
						Date( v_diad, v_mesd, v_aniod, v_horad, v_mind ),
						Date( v_diah, v_mesh, v_anioh, v_horah, v_minh ) );
			client->addClinicClientBusy( new ClinicClientBusy( period ));

		} /* fin del if */

		if ( v_fecd_num < v_fecini_num && v_fecini_num < v_fech_num
			&& v_fecfin_num >= v_fech_num )
		{ /* Cita que comienza en periodo de estudio y finaliza 
			despu�s que el periodo de estudio */

			//Fecha desde.
			gen_desglosar_fechad( v_fecini, v_diad, v_mesd, v_aniod );
			gen_desglosar_fecha_hora( h_datos_cita.h_fecini_paciente,
														 v_horad, v_mind );

			//Fecha hasta.
			gen_desglosar_fechad( ae_fecha_h, v_diah, v_mesh, v_anioh );

			Period*	period = new Period( 
						Date( v_diad, v_mesd, v_aniod, v_horad, v_mind ),
						Date( v_diah, v_mesh, v_anioh ) );
			client->addClinicClientBusy( new ClinicClientBusy( period ));

		} /* fin del if */

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_citas; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1836;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	if (sqlca.sqlerrd[2] == 0 )
	{
	}

} /* fin de gen_read_citas_paciente */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_citas_recurso
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------------
 *		mss				MedicalServiceSingle*	Puntero al recurso actual
 *		codRecurso		const IlcInt			C�digo del recurso del que se
 *												leen las citas concertadas en
 *												periodo de estudio
 *		ae_fecha_d		char*					Fecha inicio periodo de estudio
 *		ae_fecha_h		char*					Fecha fin periodo de estudio
 *
 * DESCRIPCI�N:
 *		Lectura de las citas concertadas en periodo de estudio para el recurso
 *		actual.
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   03/12/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_read_citas_recurso(
						MedicalServiceSingle*	mss,
						const IlcInt			codRecurso,
						char*					ae_fecha_d,
						char*					ae_fecha_h
						)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long	v_result = GENE0000_NORMAL;
	char			v_dato[C_GEN_LEN_DATO + 1] = "";
	char			v_fecha_d[C_GEN_LEN_FECHA + 1] = "";
	char			v_fecini[C_GEN_LEN_FECHA + 1] = "";
	char			v_fecfin[C_GEN_LEN_FECHA + 1] = "";
	unsigned long	v_fecini_num = 0;
	unsigned long	v_fecfin_num = 0;
	unsigned long	v_fecd_num = 0;
	unsigned long	v_fech_num = 0;
	unsigned int	v_diad, v_mesd, v_aniod, v_horad, v_mind;
	unsigned int	v_diah, v_mesh, v_anioh, v_horah, v_minh;
	IlcInt			v_act_dpto = 0;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 


	sprintf( v_dato,
			"TO_CHAR(TO_DATE(\'%s\','DD-MM-YYYY') - 1, 'DD-MM-YYYY')",
			ae_fecha_d );
	gen_obtener_dato( "DUAL",
						"",
						v_dato,
						v_fecha_d );

	v_fecd_num = gen_fecha_numero( ae_fecha_d );
	v_fech_num = gen_fecha_numero( ae_fecha_h );

	sprintf( (char*)h_select.arr,
		"SELECT DISTINCT TO_CHAR(CI27FECINOCUREC,'DD-MM-YYYY HH24:MI'), \
		TO_CHAR(CI27FECFIOCUREC,'DD-MM-YYYY HH24:MI'),PR01CODACTUACION, AD02CODDPTO_ACT \
		FROM CI2702J WHERE AG11CODRECURSO=%d AND (CI01SITCITA='1' OR CI01SITCITA='3') \
		AND CI27FECINOCUREC >= TO_DATE(\'%s\','DD-MM-YYYY') AND \
		CI27FECINOCUREC < TO_DATE(\'%s\','DD-MM-YYYY')",
		codRecurso, v_fecha_d, ae_fecha_h );
    
	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIACITASR FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1850;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_citasr CURSOR FOR SENTENCIACITASR; */ 
	/* EXEC SQL OPEN sql_cursor_citasr; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1868;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		strcpy( h_datos_citar.h_fecini_recurso, "" );
		strcpy( h_datos_citar.h_fecfin_recurso, "" );
		h_datos_citar.h_cod_act = 0;
		h_datos_citar.h_cod_dpto = 0;
		
		/* EXEC SQL FETCH sql_cursor_citasr INTO :h_datos_citar; */ 
{
  struct sqlexd sqlstm={8,4};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )1882;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)h_datos_citar.h_fecini_recurso;
  sqlstm.sqhstl[0] = (unsigned int  )17;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_citar.h_fecfin_recurso;
  sqlstm.sqhstl[1] = (unsigned int  )17;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_citar.h_cod_act;
  sqlstm.sqhstl[2] = (unsigned int  )4;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_citar.h_cod_dpto;
  sqlstm.sqhstl[3] = (unsigned int  )4;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{

		} /* fin del if */


		v_act_dpto = (h_datos_citar.h_cod_act*1000)+h_datos_citar.h_cod_dpto;

		strncpy( v_fecini, h_datos_citar.h_fecini_recurso, C_GEN_LEN_FECHA );
		strncpy( v_fecfin, h_datos_citar.h_fecfin_recurso, C_GEN_LEN_FECHA );

		v_fecini_num = gen_fecha_numero(v_fecini);
		v_fecfin_num = gen_fecha_numero(v_fecfin);

		if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
			&& v_fecfin_num < v_fech_num )
		{ /* Cita que empieza antes del periodo de estudio y finaliza
			antes que dicho periodo */

			//Fecha hora desde.
			gen_desglosar_fechad( ae_fecha_d, v_diad, v_mesd, v_aniod );

			//Fecha hora hasta.
			gen_desglosar_fechad( v_fecfin, v_diah, v_mesh, v_anioh );
			gen_desglosar_fecha_hora( h_datos_citar.h_fecfin_recurso,
													 v_horah, v_minh );

			Period	period( Date(v_diad,v_mesd,v_aniod),
							Date(v_diah,v_mesh,v_anioh,v_horah,v_minh) );

			mss->addBusy( new MedicalServiceSingleBusy(period,v_act_dpto));

		} /* fin del if */

		if ( v_fecini_num <= v_fecd_num && v_fecd_num < v_fecfin_num
			&& v_fecfin_num >= v_fech_num )
		{ /* Cita que empieza antes del periodo de estudio y finaliza
			despu�s que dicho periodo */

			//Fecha desde.
			gen_desglosar_fechad( ae_fecha_d, v_diad, v_mesd, v_aniod );

			//Fecha hasta.
			gen_desglosar_fechad( ae_fecha_h, v_diah, v_mesh, v_anioh );

			Period	period( Date(v_diad,v_mesd,v_aniod),
							Date(v_diah,v_mesh,v_anioh) );

			mss->addBusy( new MedicalServiceSingleBusy(period,v_act_dpto));

		} /* fin del if */

		if ( v_fecd_num <= v_fecini_num && v_fecfin_num < v_fech_num )
		{ /* Cita comprendido dentro del periodo de estudio */

			//Construir cita en curso.
			//Fecha desde.
			gen_desglosar_fechad( v_fecini, v_diad, v_mesd, v_aniod );
			gen_desglosar_fecha_hora( h_datos_citar.h_fecini_recurso,
														 v_horad, v_mind );

			//Fecha hasta.
			gen_desglosar_fechad( v_fecfin, v_diah, v_mesh, v_anioh );
			gen_desglosar_fecha_hora( h_datos_citar.h_fecfin_recurso,
														 v_horah, v_minh );

			Period	period( Date(v_diad,v_mesd,v_aniod,v_horad,v_mind),
							Date(v_diah,v_mesh,v_anioh,v_horah,v_minh) );

			mss->addBusy( new MedicalServiceSingleBusy(period,v_act_dpto));

		} /* fin del if */

		if ( v_fecd_num < v_fecini_num && v_fecini_num < v_fech_num
			&& v_fecfin_num >= v_fech_num )
		{ /* Cita que comienza en periodo de estudio y finaliza 
			despu�s que el periodo de estudio */

			//Construir cita en curso.
			//Fecha desde.
			gen_desglosar_fechad( v_fecini, v_diad, v_mesd, v_aniod );
			gen_desglosar_fecha_hora( h_datos_citar.h_fecini_recurso,
														 v_horad, v_mind );

			//Fecha hasta.
			gen_desglosar_fechad( ae_fecha_h, v_diah, v_mesh, v_anioh );

			Period	period(	Date(v_diad,v_mesd,v_aniod,v_horad,v_mind),
							Date(v_diah,v_mesh,v_anioh) );

			mss->addBusy( new MedicalServiceSingleBusy(period,v_act_dpto));

		} /* fin del if */

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_citasr; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1912;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	if (sqlca.sqlerrd[2] == 0 )
	{
	}

} /* fin de gen_read_citas_recurso */



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_fases_recursos
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------------
 *		_form			CliniForm*				Puntero a la solicitud
 *		mfg				MedicalFactGroup*		Puntero a grupo de actuaciones
 *		cod_act_planif	unsigned int			C�digo Actuaci�n planificada.
 *		recursoPref		long					C�digo Recurso Preferente.
 *		fechaPref		Date*					Puntero a la fecha y hora prefe-
 *												rentes para realizar la actuaci�n
 *												en curso.
 *
 * DESCRIPCI�N:
 *		Lectura de las fases con sus interacciones para cada una de las ac-
 *		tuaciones planificadas. por cada una de las fases se leer�n los
 *		recursos necesarios.
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   15/12/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
long
gen_read_fases_recursos( 
							ClinicForm*			_form,
							MedicalFactGroup*	mfg,
							unsigned int		cod_act_planif,
							long                recursoPref,
							Date*				fechaPref
						)
{ /* inicio funci�n */

/*
 ******************************************************************************
 *
 * Declaraci�n de variables.
 *
 ******************************************************************************
 */
	unsigned long	v_result = GENE0000_NORMAL;
	unsigned long	codActPedida = 0;
	long	v_err = GENE0000_NORMAL;
	char	v_where[C_GEN_LEN_WHERE + 1] = "";
	char	v_num_peticion[C_GEN_LEN_COD_ACTUACION + 1] = "";
	char*	v_desc_act = new char[C_GEN_LEN_DESC_RECURSO + 1];
	char*	v_desc_fase = new char[C_GEN_LEN_DESC_RECURSO + 1];
	unsigned int	v_num_fases = 0;
	unsigned int	v_num_recsimples = 0;
	unsigned int	v_num_recalter = 0;
	unsigned int	v_num_aux = 0;
	IlcInt	mfType, numPhases, numServices, codDpto, codPersona;
	Duration*		durMin;
	Duration*		durMax;
	Phase*			phase_aux;
	Phase*			phase;
	unsigned long	v_dias = 0;
	unsigned long	v_horas = 0;
	unsigned long	v_min = 0;
	unsigned long	v_diad = 0;
	unsigned long	v_horad = 0;
	unsigned long	v_mind = 0;
	long			ind_act = 0;
	long			ind_phase = 0;
	long			v_k = 0;
	char			v_min_desfase[C_GEN_LEN_DESFASE + 1] = "";
	char			v_dur_min[C_GEN_LEN_DESFASE + 1] = "";
	char			v_cod_tipo[C_GEN_LEN_COD_RECURSO + 1]  = "";
	long			recurso_confir = 0;
	char			v_num_unidades[C_GEN_LEN_UNIDADES_RECURSO + 1] = "";
	Duration*		duration;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

	if ( _form->getTipo() == CONFIRMACION )
		ind_act = data_access->getServidorDoc()->getIndiceAct( cod_act_planif );

	/* EXEC SQL WHENEVER NOT FOUND CONTINUE; */ 

	sprintf( (char*)h_select_ppal.arr,
		"SELECT PR01CODACTUACION,AD02CODDPTO,PR03NUMACTPEDI,CI21CODPERSONA \
			 FROM PR0400 WHERE PR04NUMACTPLAN = %d", cod_act_planif );
    
	//A�adir fin de cadena a h_select_ppal.
	h_select_ppal.len = strlen((char*)h_select_ppal.arr );
	h_select_ppal.arr[h_select_ppal.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAPLANIF FROM :h_select_ppal; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1926;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select_ppal;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_actplanif CURSOR FOR SENTENCIAPLANIF; */ 
	/* EXEC SQL OPEN sql_cursor_actplanif; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1944;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	/* EXEC SQL FETCH sql_cursor_actplanif INTO :h_cod_actuacion,:h_cod_depto,
												 :h_cod_act_pedi,:h_cod_persona; */ 
{
 struct sqlexd sqlstm={8,4};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1958;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_actuacion;
 sqlstm.sqhstl[0] = (unsigned int  )4;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqhstv[1] = (unsigned char  *)&h_cod_depto;
 sqlstm.sqhstl[1] = (unsigned int  )4;
 sqlstm.sqindv[1] = (         short *)0;
 sqlstm.sqharm[1] = (unsigned int  )0;
 sqlstm.sqhstv[2] = (unsigned char  *)&h_cod_act_pedi;
 sqlstm.sqhstl[2] = (unsigned int  )4;
 sqlstm.sqindv[2] = (         short *)0;
 sqlstm.sqharm[2] = (unsigned int  )0;
 sqlstm.sqhstv[3] = (unsigned char  *)&h_cod_persona;
 sqlstm.sqhstl[3] = (unsigned int  )4;
 sqlstm.sqindv[3] = (         short *)0;
 sqlstm.sqharm[3] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	if ( v_result != GENE0000_NORMAL )
	{
		return (v_result);

	} /* fin del if */
			
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_actplanif; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )1988;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	mfType = h_cod_actuacion;
	codDpto = h_cod_depto;
	codActPedida = h_cod_act_pedi;
	codPersona = h_cod_persona;


	sprintf( v_where, "PR03NUMACTPEDI = %d", codActPedida );
	gen_obtener_dato( "PR0300",
						v_where,
						"PR09NUMPETICION",
						v_num_peticion );


	data_access->readClinicClient(atoi(v_num_peticion), codPersona);

	sprintf( v_where, "PR01CODACTUACION = %d", mfType );
	gen_obtener_dato( "PR0100",
						v_where,
						"PR01DESCORTA",
						v_desc_act );

	sprintf( v_where, "PR03NUMACTPEDI = %d", codActPedida );
	gen_count_registros( "PR0600",
							v_where,
							v_num_fases );
	numPhases = v_num_fases;

	MedicalFact* mf = new MedicalFact(mfg, v_desc_act, 
							mfType, numPhases, codPersona, codDpto, fechaPref);

	mf->setCodMedicalFact( mfType );

	MedicalFactAllIterations*	mfallIter = new MedicalFactAllIterations(mf);

	mf->setMedicalFactIterations(mfallIter);

 	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

	sprintf( (char*)h_select_ppal.arr,
		"SELECT PR06NUMFASE,PR06DESFASE,NVL(PR06NUMFASE_PRE,0),PR06NUMMINOCUPAC,\
		NVL(PR06NUMMINFPRE,99999),NVL(PR06NUMMAXFPRE,99999),NVL(PR06INDINIFIN,0) \
		FROM PR0600 WHERE PR03NUMACTPEDI = %d ORDER BY 3",
		codActPedida );
    
	//A�adir fin de cadena a h_select_ppal.
	h_select_ppal.len = strlen((char*)h_select_ppal.arr );
	h_select_ppal.arr[h_select_ppal.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAFASES FROM :h_select_ppal; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2002;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select_ppal;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_fases CURSOR FOR SENTENCIAFASES; */ 
	/* EXEC SQL OPEN sql_cursor_fases; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2020;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		phase_aux = phase;
		
		v_num_recsimples = 0;
		v_num_recalter = 0;
		v_num_aux = 0;

		/* EXEC SQL FETCH sql_cursor_fases INTO :h_datos_fases; */ 
{
  struct sqlexd sqlstm={8,7};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )2034;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_fases.h_num_fase;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_fases.h_desc_fase;
  sqlstm.sqhstl[1] = (unsigned int  )31;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_fases.h_num_fasep;
  sqlstm.sqhstl[2] = (unsigned int  )4;
  sqlstm.sqindv[2] = (         short *)0;
  sqlstm.sqharm[2] = (unsigned int  )0;
  sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_fases.h_dur_min;
  sqlstm.sqhstl[3] = (unsigned int  )4;
  sqlstm.sqindv[3] = (         short *)0;
  sqlstm.sqharm[3] = (unsigned int  )0;
  sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_fases.h_min_minutos;
  sqlstm.sqhstl[4] = (unsigned int  )4;
  sqlstm.sqindv[4] = (         short *)0;
  sqlstm.sqharm[4] = (unsigned int  )0;
  sqlstm.sqhstv[5] = (unsigned char  *)&h_datos_fases.h_max_minutos;
  sqlstm.sqhstl[5] = (unsigned int  )4;
  sqlstm.sqindv[5] = (         short *)0;
  sqlstm.sqharm[5] = (unsigned int  )0;
  sqlstm.sqhstv[6] = (unsigned char  *)&h_datos_fases.h_indicador;
  sqlstm.sqhstl[6] = (unsigned int  )4;
  sqlstm.sqindv[6] = (         short *)0;
  sqlstm.sqharm[6] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{
			return (v_result);

		} /* fin del if */

		sprintf( v_where, "PR03NUMACTPEDI = %d AND PR06NUMFASE = %d",
								codActPedida, h_datos_fases.h_num_fase );
		gen_obtener_dato( "PR0600",
						v_where,
						"PR06DESFASE",
						v_desc_fase );

		sprintf( v_where, "PR03NUMACTPEDI = %d AND PR06NUMFASE = %d AND \
				 AG11CODRECURSO IS NOT NULL",
				 codActPedida, h_datos_fases.h_num_fase );

		gen_count_registros( "PR1400",
							 v_where,
							 v_num_recsimples );

 		sprintf( (char*)h_select_temp.arr,
			"SELECT AG14CODTIPRECU,PR14NUMUNIREC FROM PR1400 WHERE \
			PR03NUMACTPEDI = %d AND PR06NUMFASE=%d AND AG11CODRECURSO IS NULL",
			codActPedida, h_datos_fases.h_num_fase );
    
		//A�adir fin de cadena a h_select_temp.
		h_select_temp.len = strlen((char*)h_select_temp.arr );
		h_select_temp.arr[h_select_temp.len] = '\0';

		//Preparar sentencia.
		/* EXEC SQL PREPARE SENTENCIATREC FROM :h_select_temp; */ 
{
  struct sqlexd sqlstm={8,1};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )2076;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_select_temp;
  sqlstm.sqhstl[0] = (unsigned int  )3003;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Declarar y abrir cursor para sentencia anterior.
		/* EXEC SQL DECLARE sql_cursor_trecu CURSOR FOR SENTENCIATREC; */ 
		/* EXEC SQL OPEN sql_cursor_trecu; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.stmt = "";
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )2094;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		//Bucle para recuperar los registros del cursor.
		while(1)
		{
			
			/* EXEC SQL FETCH sql_cursor_trecu INTO :h_cod_trec, :h_num_unidades; */ 
{
   struct sqlexd sqlstm={8,2};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )2108;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)&h_cod_trec;
   sqlstm.sqhstl[0] = (unsigned int  )4;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqhstv[1] = (unsigned char  *)&h_num_unidades;
   sqlstm.sqhstl[1] = (unsigned int  )4;
   sqlstm.sqindv[1] = (         short *)0;
   sqlstm.sqharm[1] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode == 1403) break;
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			if ( v_result != GENE0000_NORMAL )
			{
				return (v_result);

			} /* fin del if */

			sprintf( v_where, "AG14CODTIPRECU = %d", h_cod_trec );
			gen_count_registros( "AG1100",
								v_where,
								v_num_aux );

			v_num_recalter += v_num_aux*h_num_unidades;

		} /* fin del while */
			
		//Cerrar el cursor.
		/* EXEC SQL CLOSE sql_cursor_trecu; */ 
{
  struct sqlexd sqlstm={8,0};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )2130;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		numServices = v_num_recsimples + v_num_recalter;

		gen_obtener_duracion( h_datos_fases.h_dur_min, v_dias, 
												v_horas, v_min );

		phase = new Phase(mf, v_desc_fase, 
						Duration(v_dias,v_horas,v_min), numServices,
						h_datos_fases.h_num_fase );

		if ( _form->getTipo() == NORMAL )
		{ /* Solicitud normal */

			sprintf( (char*)h_select_rec.arr,
				"SELECT PR1400.AG14CODTIPRECU,PR14NUMUNIREC,NVL(PR14NUMMINOCU,0), \
				NVL(PR14NUMMINDESREC,0), NVL(AG11CODRECURSO,0),NVL(PR1400.PR14INDRECPREFE,0), \
				NVL(PR1400.AD02CODDPTO,0) FROM PR1400,PR1300 WHERE PR03NUMACTPEDI = %d \
				AND PR06NUMFASE=%d AND PR06NUMFASE=PR05NUMFASE AND \
				PR01CODACTUACION=%d AND PR14NUMNECESID=PR13NUMNECESID AND \
				PR13INDPLANIF=-1 ORDER BY PR1400.PR14INDRECPREFE",
				codActPedida, h_datos_fases.h_num_fase, mfType );
    
			//A�adir fin de cadena a h_select_rec.
			h_select_rec.len = strlen((char*)h_select_rec.arr );
			h_select_rec.arr[h_select_rec.len] = '\0';

			//Preparar sentencia.
			/* EXEC SQL PREPARE SENTENCIARECP FROM :h_select_rec; */ 
{
   struct sqlexd sqlstm={8,1};
   sqlstm.stmt = "";
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )2144;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlstm.sqhstv[0] = (unsigned char  *)&h_select_rec;
   sqlstm.sqhstl[0] = (unsigned int  )3003;
   sqlstm.sqindv[0] = (         short *)0;
   sqlstm.sqharm[0] = (unsigned int  )0;
   sqlstm.sqphsv = sqlstm.sqhstv;
   sqlstm.sqphsl = sqlstm.sqhstl;
   sqlstm.sqpind = sqlstm.sqindv;
   sqlstm.sqparm = sqlstm.sqharm;
   sqlstm.sqparc = sqlstm.sqharc;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			//Declarar y abrir cursor para sentencia anterior.
			/* EXEC SQL DECLARE sql_cursor_recu CURSOR FOR SENTENCIARECP; */ 
			/* EXEC SQL OPEN sql_cursor_recu; */ 
{
   struct sqlexd sqlstm={8,0};
   sqlstm.stmt = "";
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )2162;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


			//Bucle para recuperar los registros del cursor.
			while(1)
			{
				h_datos_rec.h_cod_tiporec = 0;
				h_datos_rec.h_num_unid = 0;
				h_datos_rec.h_dur_min = 0;
				h_datos_rec.h_min_desfase = 0;
				h_datos_rec.h_cod_rec = 0;
				h_datos_rec.h_cod_dpto = 0;
				
				/* EXEC SQL FETCH sql_cursor_recu INTO :h_datos_rec; */ 
{
    struct sqlexd sqlstm={8,7};
    sqlstm.iters = (unsigned int  )1;
    sqlstm.offset = (unsigned int  )2176;
    sqlstm.cud = sqlcud0;
    sqlstm.sqlest = (unsigned char  *)&sqlca;
    sqlstm.sqlety = (unsigned short)0;
    sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_rec.h_cod_tiporec;
    sqlstm.sqhstl[0] = (unsigned int  )4;
    sqlstm.sqindv[0] = (         short *)0;
    sqlstm.sqharm[0] = (unsigned int  )0;
    sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_rec.h_num_unid;
    sqlstm.sqhstl[1] = (unsigned int  )4;
    sqlstm.sqindv[1] = (         short *)0;
    sqlstm.sqharm[1] = (unsigned int  )0;
    sqlstm.sqhstv[2] = (unsigned char  *)&h_datos_rec.h_dur_min;
    sqlstm.sqhstl[2] = (unsigned int  )4;
    sqlstm.sqindv[2] = (         short *)0;
    sqlstm.sqharm[2] = (unsigned int  )0;
    sqlstm.sqhstv[3] = (unsigned char  *)&h_datos_rec.h_min_desfase;
    sqlstm.sqhstl[3] = (unsigned int  )4;
    sqlstm.sqindv[3] = (         short *)0;
    sqlstm.sqharm[3] = (unsigned int  )0;
    sqlstm.sqhstv[4] = (unsigned char  *)&h_datos_rec.h_cod_rec;
    sqlstm.sqhstl[4] = (unsigned int  )4;
    sqlstm.sqindv[4] = (         short *)0;
    sqlstm.sqharm[4] = (unsigned int  )0;
    sqlstm.sqhstv[5] = (unsigned char  *)&h_datos_rec.h_ind_pref;
    sqlstm.sqhstl[5] = (unsigned int  )4;
    sqlstm.sqindv[5] = (         short *)0;
    sqlstm.sqharm[5] = (unsigned int  )0;
    sqlstm.sqhstv[6] = (unsigned char  *)&h_datos_rec.h_cod_dpto;
    sqlstm.sqhstl[6] = (unsigned int  )4;
    sqlstm.sqindv[6] = (         short *)0;
    sqlstm.sqharm[6] = (unsigned int  )0;
    sqlstm.sqphsv = sqlstm.sqhstv;
    sqlstm.sqphsl = sqlstm.sqhstl;
    sqlstm.sqpind = sqlstm.sqindv;
    sqlstm.sqparm = sqlstm.sqharm;
    sqlstm.sqparc = sqlstm.sqharc;
    sqlcex(sqlctx, &sqlstm, &sqlfpn);
    if (sqlca.sqlcode == 1403) break;
    if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}

				
				if ( v_result != GENE0000_NORMAL )
				{
					return (v_result);

				} /* fin del if */


				if ( h_datos_rec.h_min_desfase >= 0 )
				{					
					gen_obtener_duracion( h_datos_rec.h_min_desfase, v_dias, 
										  v_horas, v_min );
					duration = new Duration(v_dias, v_horas, v_min);
				}
				else
				{
					gen_obtener_duracion( -h_datos_rec.h_min_desfase, v_dias, 
										  v_horas, v_min );
					duration = new Duration(v_dias, v_horas, v_min, 0, -1);
				}

				gen_obtener_duracion( h_datos_rec.h_dur_min, v_diad, 
									  v_horad, v_mind );

				if ( h_datos_rec.h_ind_pref == -1 )
				{ /* tipo recurso preferente */

					phase->setMedicalServicePref();

				} /* fin del if */

				if ( recursoPref == 0 || h_datos_rec.h_ind_pref != -1 )
				{ /* sin recurso preferente seleccionado */

					if ( h_datos_rec.h_cod_rec != 0 )
					{ /* recurso simple */

						sprintf( v_where, "AG11CODRECURSO = %d", h_datos_rec.h_cod_rec );
						gen_obtener_dato( "AG1100",
											v_where,
											"AG11NUMUNIDREC",
											v_num_unidades );

						if ( atoi(v_num_unidades) > 0 )
						{
							data_access->readMedicalService(h_datos_rec.h_cod_rec, v_err);

							if ( v_err == GENE0000_NORMAL )
							{ //recurso planificable y vigente.

								phase->addMedicalServiceReserve(_form->getClinicApplication()->
													findMedicalService(h_datos_rec.h_cod_rec), 
													*duration,
													Duration(v_diad,v_horad,v_mind));
							}
							else if ( v_err == GENE0004_ERROR_NO_CALENDARIO )
								return ( v_err );

						} /* fin del if */

					} /* fin del if */
					else
					{ /* tipo de recurso */

						v_err = data_access->readMedicalServiceType(
															h_datos_rec.h_cod_tiporec,
															mfType, h_datos_rec.h_cod_dpto, 
															h_datos_fases.h_num_fase );

						if ( v_err == GENE0000_NORMAL )
						{ //alg�n recurso simple planificable y vigente
						
							for ( unsigned long i = 0; i < h_datos_rec.h_num_unid; i++ )
							{

								phase->addMedicalServiceReserve(_form->getClinicApplication()->
														findMedicalService(h_datos_rec.h_cod_tiporec,
																mfType,
																h_datos_rec.h_cod_dpto,
																h_datos_fases.h_num_fase,
																ALTERNATIVE),
														*duration,
														Duration(v_diad,v_horad,v_mind));

							} /* fin del for */

						}
						else if ( v_err == GENE0004_ERROR_NO_CALENDARIO )
							return ( v_err );

					} /* fin del else */

				} /* fin del if */
				else if ( recursoPref != 0 && h_datos_rec.h_ind_pref == -1 )
				{ /* con recurso preferente */

					sprintf( v_where, "AG11CODRECURSO = %d", recursoPref );
					gen_obtener_dato( "AG1100",
									  v_where,
									  "AG11NUMUNIDREC",
									  v_num_unidades );

					if ( atoi(v_num_unidades) > 0 )
					{
						data_access->readMedicalService(recursoPref, v_err);

						if ( v_err == GENE0000_NORMAL )
						{ //recurso planificable

							phase->addMedicalServiceReserve(_form->getClinicApplication()->
											findMedicalService(recursoPref), 
											*duration,
											Duration(v_diad,v_horad,v_mind));
						} /* fin del if */
						else if ( v_err == GENE0004_ERROR_NO_CALENDARIO )
							return ( v_err );

					} /* fin del if */

				} /* fin del else if */

			} /* fin del while */
				
			//Cerrar el cursor.
			/* EXEC SQL CLOSE sql_cursor_recu; */ 
{
   struct sqlexd sqlstm={8,0};
   sqlstm.iters = (unsigned int  )1;
   sqlstm.offset = (unsigned int  )2218;
   sqlstm.cud = sqlcud0;
   sqlstm.sqlest = (unsigned char  *)&sqlca;
   sqlstm.sqlety = (unsigned short)0;
   sqlcex(sqlctx, &sqlstm, &sqlfpn);
   if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		} /* fin del if */
		else if ( _form->getTipo() == CONFIRMACION )
		{ /* Solicitud de Confirmaci�n */

			ind_phase = data_access->getServidorDoc()->getIndicePhase( 
							cod_act_planif, h_datos_fases.h_num_fase );

			for ( v_k = 0; v_k < data_access->getServidorDoc()->
							NumRecursos[ind_act][ind_phase]; v_k++ )
			{ /* bucle por recursos a confirmar */

				recurso_confir = data_access->getServidorDoc()->
								Recurso[ind_act][ind_phase][v_k];
				data_access->readMedicalService( recurso_confir, v_err );

				if ( v_err != GENE0000_NORMAL )
							return (v_err);

				sprintf( v_where, "AG11CODRECURSO = %d", recurso_confir );
				gen_obtener_dato( "AG1100",
									v_where,
									"AG14CODTIPRECU",
									v_cod_tipo );

				sprintf( v_where,
					"PR03NUMACTPEDI = %d AND PR06NUMFASE=%d AND ( AG11CODRECURSO \
					=  %d OR AG14CODTIPRECU = %s)",
					codActPedida, h_datos_fases.h_num_fase,
					recurso_confir, v_cod_tipo );

				gen_obtener_dato( "PR1400",
									v_where,
									"PR14NUMMINDESREC",
									v_min_desfase );

				if ( atoi(v_min_desfase) >= 0 )
				{					
					gen_obtener_duracion( atoi(v_min_desfase), v_dias, 
										  v_horas, v_min );
					duration = new Duration(v_dias, v_horas, v_min);
				}
				else
				{
					gen_obtener_duracion( -atoi(v_min_desfase), v_dias, 
										  v_horas, v_min );
					duration = new Duration(v_dias, v_horas, v_min, 0, -1);
				}

				gen_obtener_dato( "PR1400",
									v_where,
									"PR14NUMMINOCU",
									v_dur_min );

				gen_obtener_duracion( atoi(v_dur_min), v_diad, 
									  v_horad, v_mind );

				phase->addMedicalServiceReserve(_form->getClinicApplication()->
					findMedicalService(recurso_confir), 
					*duration,
					Duration(v_diad,v_horad,v_mind));

			} /* fin del for */

		} /* fin del else if */

		if ( h_datos_fases.h_min_minutos == 99999 &&
							h_datos_fases.h_max_minutos == 99999 )
		{ //los dos campos vac�os

			durMin = NULL;
			durMax = NULL;

		}
		else if ( h_datos_fases.h_min_minutos < 99999 &&
							h_datos_fases.h_max_minutos == 99999 )
		{ /* Minutos m�nimos de desfase distinto de null y m�ximo null */
			
			gen_obtener_duracion( h_datos_fases.h_min_minutos, v_dias, v_horas, v_min );
			durMin = new Duration( v_dias, v_horas, v_min );

			durMax = NULL;

		} /* fin del else */
		else if ( h_datos_fases.h_min_minutos < 99999 &&
							h_datos_fases.h_max_minutos < 99999 )
		{ /* los dos campos con valor */

			gen_obtener_duracion( h_datos_fases.h_min_minutos, v_dias, v_horas, v_min );
			durMin = new Duration( v_dias, v_horas, v_min );

			gen_obtener_duracion( h_datos_fases.h_max_minutos, v_dias, v_horas, v_min );
			durMax = new Duration( v_dias, v_horas, v_min );
		}
		else if ( h_datos_fases.h_min_minutos == 99999 &&
							h_datos_fases.h_max_minutos < 99999 )
		{ /* Minutos m�ximos de desfase distinto de null y m�nimo null */

			durMin = new Duration( 0, 0, 0 );

			gen_obtener_duracion( h_datos_fases.h_max_minutos, v_dias, v_horas, v_min );
			durMax = new Duration( v_dias, v_horas, v_min );

		} /* fin del else */

		if ( h_datos_fases.h_indicador == 0 )
		{ /* respecto del final */

			h_datos_fases.h_indicador = 1;

		} /* fin del if */
		else
		{ /* respecto al comienzo */

			h_datos_fases.h_indicador = 0;

		} /* fin del else */

		if ( h_datos_fases.h_num_fasep == 0 )
		{ /* Primera fase de la actuaci�n */

			mf->addPhase(phase, NULL, h_datos_fases.h_indicador, durMin, durMax);

		} /* fin del if */
		else
		{ /* sucesivas fases */

			mf->addPhase(phase, phase_aux, h_datos_fases.h_indicador, durMin, durMax);

		} /* fin del else */

	} /* fin del while */
			
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_fases; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2232;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	mfg->addMedicalFact(mf);

	return (v_result);

} /* fin de gen_read_fases_recursos */


/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_obtener_duracion
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------------
 *		minutos			unsigned long			Minutos de desfase enter fases
 *		dias			unsigned long&			D�as contenidos
 *		horas			unsigned long&			Horas contenidas
 *		min				unsigned long&			Minutos contenidos
 *
 * DESCRIPCI�N:
 *		Halla la equivalencia entre minutos totales y d�as, horas y minutos.
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   18/12/1997
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_obtener_duracion(	
						unsigned long	minutos,
						unsigned long&	dias,
						unsigned long&	horas,
						unsigned long&	min
					 )
{ /* inicio funci�n */

	//Declaraci�n de variables.
	unsigned long	aux_dias = 0;
	unsigned long	aux_horas = 0;
	unsigned long	aux_min = 0;

	aux_dias = minutos/C_GEN_MINUTOS_EN_DIA;
	dias = aux_dias;

	aux_min = minutos - ( aux_dias * C_GEN_MINUTOS_EN_DIA );
	aux_horas = aux_min/C_GEN_MINUTOS_EN_HORA;
	horas = aux_horas;

	aux_min = minutos - ( aux_dias * C_GEN_MINUTOS_EN_DIA ) - 
									( aux_horas * C_GEN_MINUTOS_EN_HORA );
	min = aux_min;

} /* fin de gen_obtener_duracion*/



/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_iterations
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------------
 *		cod_act_planif	unsigned int			C�digo Actuaci�n planificada
 *
 * DESCRIPCI�N:
 *		Lectura de las interacciones con otras actuaciones, siendo las actua-
 *		ci�n origen la actuaci�n en curso.
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   03/02/1998
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_read_iterations(
						unsigned int	cod_act_planif
					)
{ /* inicio funci�n */
/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
 	unsigned long	v_result = GENE0000_NORMAL;
	char			v_where[C_GEN_LEN_WHERE + 1] = "";
	char			v_cod_act_pedida[C_GEN_LEN_COD_ACTUACION + 1] = "";
	char			v_cod_act_origen[C_GEN_LEN_COD_ACTUACION + 1] = "";
	char			v_cod_act_destino[C_GEN_LEN_COD_ACTUACION + 1] = "";
	char			v_cod_persona[C_GEN_LEN_COD_PACIENTE + 1] = "";
	unsigned long	v_dias = 0;
	unsigned long	v_horas = 0;
	unsigned long	v_minutos = 0;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 

	sprintf( v_where, "PR04NUMACTPLAN = %d", cod_act_planif );
	gen_obtener_dato( "PR0400",
						v_where,
						"PR01CODACTUACION",
						v_cod_act_origen );

	gen_obtener_dato( "PR0400",
						v_where,
						"PR03NUMACTPEDI",
						v_cod_act_pedida );

	MedicalFactAllIterations* mfallIter = data_access->getClinicForm()->
				getMedicalFactGroup(0)->getActuacion(atoi(v_cod_act_origen))->
				getMedicalFactIterations();

	sprintf( (char*)h_select.arr,
		"SELECT PR03NUMACTPEDI_DES,PR39NUMMININTER FROM PR3900 WHERE PR03NUMACTPEDI = %s",
		v_cod_act_pedida );
    
	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAINTER FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2246;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_inter CURSOR FOR SENTENCIAINTER; */ 
	/* EXEC SQL OPEN sql_cursor_inter; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2264;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{

		/* EXEC SQL FETCH sql_cursor_inter INTO :h_datos_iteration; */ 
{
  struct sqlexd sqlstm={8,2};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )2278;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_iteration.h_codact_destino;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)&h_datos_iteration.h_num_minutos;
  sqlstm.sqhstl[1] = (unsigned int  )4;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{

		} /* fin del if */

		sprintf( v_where, "PR03NUMACTPEDI = %d",
										h_datos_iteration.h_codact_destino );
		gen_obtener_dato( "PR0400",
						v_where,
						"PR01CODACTUACION",
						v_cod_act_destino );

		gen_obtener_dato( "PR0400",
						v_where,
						"CI21CODPERSONA",
						v_cod_persona );

		if ( data_access->getClinicForm()->getMedicalFactGroup(0)->
			 getCodMedicalFactClient( atoi(v_cod_act_destino), 
									  atoi(v_cod_persona) ) == 1 )
		{
			gen_obtener_duracion( h_datos_iteration.h_num_minutos, v_dias,
								  v_horas, v_minutos );

			MedicalFactIteration* mfIter = new MedicalFactIteration(
												v_dias, v_horas, v_minutos,
												h_datos_iteration.h_codact_destino,
												mfallIter
													);

			mfallIter->addIteration( mfIter );
		}

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_inter; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2300;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	mfallIter->printOn();

} /* fin de gen_read_iterations */


/*
 ******************************************************************************
 *
 *                              K Y A T  -  S Y S E C A
 *
 * PROYECTO:    CLINICA
 *
 * NOMBRE:      gen_read_property_client
 *
 * TIPO:        Funci�n
 *
 * ARGUMENTOS:
 *
 *      Nombre			Tipo					Descripci�n
 *      --------------- ----------------------- ----------------------------
 *		client			ClinicClient*			Paciente sobre el que se leen
 *												las restrcciones.
 *		codPeticion		const IlcInt			C�digo de la petici�n.
 *		v_fecha_h		char*					Fecha fin del periodo de estudio.
 *
 * DESCRIPCI�N:
 *		Lectura de las restricciones para una petici�n y un paciente dados.
 *
 * RESULTADOS:
 *		Ninguno.
 *
 * ERRORES:
 *      Nombre          Descripci�n
 *      --------------- ------------------------------------------------------
 *      Ninguno.
 *
 * CONTEXTO:
 *
 * FECHA DE CREACI�N:   13/02/1998
 * AUTOR:               Marian Bezanilla
 *
 ******************************************************************************
 */
/*
 ******************************************************************************
 *
 * DISE�O DETALLADO:
 *
 ******************************************************************************
 */
void
gen_read_property_client( 
							ClinicClient*	client,
							const IlcInt	codPeticion, 
							char*			v_fecha_h
                         )
{ /* inicio funci�n */

/*
 *******************************************************************************
 *
 *	Declaraci�n de variables.
 *
 *******************************************************************************
 */	
	unsigned long	v_result = GENE0000_NORMAL;
	char			v_where[C_GEN_LEN_WHERE + 1] = "";
	char*			v_desc_tiprest = new char[C_GEN_LEN_DESC_RECURSO + 1];
	Property*		property;

/*
 *******************************************************************************
 *
 *	Control de errores posibles.
 *
 *******************************************************************************
 */	
	/* EXEC SQL WHENEVER SQLERROR DO gen_error_ora( GEN_ERROR_ORA_AE_CONDI_ERROR,
                                        v_result); */ 
	/* EXEC SQL WHENEVER NOT FOUND DO break; */ 


	sprintf( (char*)h_select.arr,
		"SELECT PR4700.AG16CODTIPREST,PR47VALOR FROM PR4700,AG1600 WHERE \
		PR09NUMPETICION = %d AND PR4700.AG16CODTIPREST= \
		AG1600.AG16CODTIPREST AND (AG16FECBAJA IS NULL OR AG16FECBAJA >= \
		TO_DATE(\'%s\','DD-MM-YYYY'))",
		codPeticion, v_fecha_h );
    
	//A�adir fin de cadena a h_select.
	h_select.len = strlen((char*)h_select.arr );
	h_select.arr[h_select.len] = '\0';

	//Preparar sentencia.
	/* EXEC SQL PREPARE SENTENCIAREST FROM :h_select; */ 
{
 struct sqlexd sqlstm={8,1};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2314;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlstm.sqhstv[0] = (unsigned char  *)&h_select;
 sqlstm.sqhstl[0] = (unsigned int  )3003;
 sqlstm.sqindv[0] = (         short *)0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Declarar y abrir cursor para sentencia anterior.
	/* EXEC SQL DECLARE sql_cursor_rest CURSOR FOR SENTENCIAREST; */ 
	/* EXEC SQL OPEN sql_cursor_rest; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.stmt = "";
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2332;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


	//Bucle para recuperar los registros del cursor.
	while(1)
	{
		h_datos_rest_paciente.h_cod_tiprest = 0;
		strcpy(h_datos_rest_paciente.h_valor, "" );
		
		/* EXEC SQL FETCH sql_cursor_rest INTO :h_datos_rest_paciente; */ 
{
  struct sqlexd sqlstm={8,2};
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )2346;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)0;
  sqlstm.sqhstv[0] = (unsigned char  *)&h_datos_rest_paciente.h_cod_tiprest;
  sqlstm.sqhstl[0] = (unsigned int  )4;
  sqlstm.sqindv[0] = (         short *)0;
  sqlstm.sqharm[0] = (unsigned int  )0;
  sqlstm.sqhstv[1] = (unsigned char  *)h_datos_rest_paciente.h_valor;
  sqlstm.sqhstl[1] = (unsigned int  )21;
  sqlstm.sqindv[1] = (         short *)0;
  sqlstm.sqharm[1] = (unsigned int  )0;
  sqlstm.sqphsv = sqlstm.sqhstv;
  sqlstm.sqphsl = sqlstm.sqhstl;
  sqlstm.sqpind = sqlstm.sqindv;
  sqlstm.sqparm = sqlstm.sqharm;
  sqlstm.sqparc = sqlstm.sqharc;
  sqlcex(sqlctx, &sqlstm, &sqlfpn);
  if (sqlca.sqlcode == 1403) break;
  if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


		if ( v_result != GENE0000_NORMAL )
		{

		} /* fin del if */

		sprintf( v_where, " AG16CODTIPREST = %d", h_datos_rest_paciente.h_cod_tiprest );
		gen_obtener_dato( "AG1600",
						  v_where,
						  "AG16DESTIPREST",
						  v_desc_tiprest );

		if ( client->getClientProperty(h_datos_rest_paciente.h_cod_tiprest) == 0 )
		{
			property = data_access->getClinicForm()->getClinicApplication()->
									getProperty(h_datos_rest_paciente.h_cod_tiprest);

			if ( property->getDataTypeProperty() == NUMERICO )
			{ //Tipo NUMERICO

				client->addClientProperty(new ClientProperty(
										  h_datos_rest_paciente.h_cod_tiprest, 
										  v_desc_tiprest,
										  atoi(h_datos_rest_paciente.h_valor)));

			} /* fin del if */
			else
			{ //Tipo CADENA

				for ( unsigned int i = 0; i < strlen(h_datos_rest_paciente.h_valor); i++ )
				{
					if ( h_datos_rest_paciente.h_valor[i] == ' ' )
					{
						h_datos_rest_paciente.h_valor[i] = '\0';
						break;

					} /* fin del if */
				} /* fin del for */

				client->addClientProperty(new ClientProperty(
										  h_datos_rest_paciente.h_cod_tiprest, 
										  v_desc_tiprest,
										  h_datos_rest_paciente.h_valor));

			} /* fin del else */

		} /* fin del if */

	} /* fin del while */
		
	//Cerrar el cursor.
	/* EXEC SQL CLOSE sql_cursor_rest; */ 
{
 struct sqlexd sqlstm={8,0};
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )2368;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)0;
 sqlcex(sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) gen_error_ora(GEN_ERROR_ORA_AE_CONDI_ERROR,v_result);
}


} /* fin de gen_read_property_client */

