Attribute VB_Name = "DEF001"
Option Explicit

Public objsecurity As clsCWSecurity
Public objmouse As clsCWMouse

Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
'gintprodbuscado es un array que guarda los c�digos de los productos de un protocolo
Public gintprodbuscado()
'Public gintcamabuscada()
Public gintpricipioactivobuscado()
'gintprodtotal guarda el n�mero total de productos seleccionados
Public gintprodtotal As Integer
'Public gintcamatotal
Public gintpricipioactivototal As Integer
Public gintbuscargruprot As Integer
Public gintbuscargruprod As Integer
Public gstrLlamador As String
Public gstrLista As String
Public glngPaciente As Long
Public gintfirmarOM As Integer
Public glngpeticion
Public gintservicio As Long
Public gintlinea As Integer
Public gintbusprod As Integer
Public gvntdatafr0105(1 To 9) As Variant
Public gintcodhojaquiro As Integer
Public gintcodhojaanest As Integer
Public gvntdataPedFarm(1 To 2) As Variant
Public gintfirmarStPlanta As Integer
Public glngnumpeticion
'glngselpaciente recoge el c�digo de paciente seleccionado
'antes de redactar la orden m�dica
Public glngselpaciente As Long
Public gintredactarorden As Integer
Public ginteventloadRedOM As Integer 'Para saber que el la primera llamada en frmRedactarOMPRN
Public gstrLlamadorProd As String
Public glngcodpeticion
Public glngcodprod
Public blnImpresoraSeleccionada As Boolean
Public gstrBoton As String
Public gdispensarestupefacientes As String
Public gEstadoDeLaPeticion As String
Public gstrInstAdmin As String
Public Const gstrversion = "19990806"


Sub Main()
End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub

'Funci�n que devuelve el separador decimal que se corresponde con
'el de la configuraci�n regional de la m�quina.
Public Function SeparadorDec() As String

If Format("12,345", "00.000") = "12,345" Then
  SeparadorDec = ","
Else
  SeparadorDec = "."
End If

End Function

Public Function Texto_Etiqueta(cadena As String, x As Integer, y As Integer, rot As Integer, letra As String, horMult As Integer, vertMult As Integer, TipoImg As String) As String
 
Texto_Etiqueta = "A" & x & "," & y & "," & rot & "," & letra & "," & horMult & "," & vertMult & "," & TipoImg & "," & Chr(34) & transfCadena(cadena) & Chr(34)

End Function

Public Function transfCadena(cadena As String) As String
Dim i As Integer

  For i = 1 To Len(cadena)
    Select Case Mid(cadena, i, 1)
      Case "�"
        Mid(cadena, i, 1) = Chr$(160)
      Case "�"
        Mid(cadena, i, 1) = Chr$(130)
      Case "�"
        Mid(cadena, i, 1) = Chr$(161)
      Case "�"
        Mid(cadena, i, 1) = Chr$(162)
      Case "�"
        Mid(cadena, i, 1) = Chr$(163)
      Case "�"
        Mid(cadena, i, 1) = Chr$(168)
      Case "�"
        Mid(cadena, i, 1) = Chr$(164)
      Case "�"
        Mid(cadena, i, 1) = Chr$(165)
      Case "�"
        Mid(cadena, i, 1) = Chr$(129)
      Case "�"
        Mid(cadena, i, 1) = Chr$(166)
      Case "�"
        Mid(cadena, i, 1) = Chr$(167)
    End Select
  Next

  transfCadena = cadena

End Function

Public Function formatear(cadena As String, espacios As Integer, izq As Boolean, cortar As Boolean) As String
Dim i As Integer
Dim strMask As String
Dim strAux As String
  
  strAux = cadena
  If cortar Then
    If Len(strAux) > espacios Then
      strAux = Left$(strAux, espacios)
    End If
  End If
  strMask = ""
  For i = 1 To espacios
    strMask = strMask & "@"
  Next i
  If izq Then
    formatear = Format(strAux, "!" & strMask)
  Else
    formatear = Format(strAux, strMask)
  End If

End Function

