VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmDispEstupef 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. CONTROLAR ESTUPEFACIENTES. Dispensar Estupefacientes."
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0107.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame FrameBusEst 
      Height          =   2895
      Left            =   4320
      TabIndex        =   36
      Top             =   2280
      Visible         =   0   'False
      Width           =   6855
      Begin VB.CommandButton cmdSalirBusEst 
         Caption         =   "Salir"
         Height          =   375
         Left            =   5760
         TabIndex        =   38
         Top             =   1440
         Width           =   855
      End
      Begin VB.CommandButton cmdAceptarBusEst 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   5760
         TabIndex        =   37
         Top             =   960
         Width           =   855
      End
      Begin SSDataWidgets_B.SSDBGrid GridBusEst 
         Height          =   2535
         Left            =   120
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   240
         Visible         =   0   'False
         Width           =   5490
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   9684
         _ExtentY        =   4471
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Origen"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   975
      Index           =   1
      Left            =   7560
      TabIndex        =   31
      Top             =   1680
      Width           =   2535
      Begin VB.OptionButton optorigen 
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   195
         Index           =   0
         Left            =   600
         TabIndex        =   33
         Top             =   360
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton optorigen 
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   195
         Index           =   1
         Left            =   600
         TabIndex        =   32
         Top             =   600
         Width           =   1335
      End
   End
   Begin VB.CommandButton cmdVerPeticion 
      Caption         =   "Visualizar Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6720
      TabIndex        =   25
      Top             =   7680
      Width           =   1815
   End
   Begin VB.CommandButton cmdDispensar 
      Caption         =   "DISPENSAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   24
      Top             =   7680
      Width           =   1935
   End
   Begin VB.CommandButton cmdfiltrar 
      Caption         =   "FILTRAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10440
      TabIndex        =   23
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Caption         =   "Estado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   1095
      Left            =   7560
      TabIndex        =   22
      Top             =   480
      Width           =   2535
      Begin VB.OptionButton optestado 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Dispensadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   375
         Index           =   1
         Left            =   480
         TabIndex        =   11
         Top             =   600
         Width           =   1695
      End
      Begin VB.OptionButton optestado 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Validadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   375
         Index           =   0
         Left            =   480
         TabIndex        =   10
         Top             =   240
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "B�squeda por "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   2175
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   7215
      Begin VB.CommandButton cmdBuscarEstupefacientes 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   220
         Left            =   1260
         TabIndex        =   40
         Top             =   1420
         Width           =   330
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00E0E0E0&
         DataField       =   "CI22NOMBRE"
         Height          =   330
         Index           =   2
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   1
         Tag             =   "Paciente"
         ToolTipText     =   "Paciente"
         Top             =   600
         Width           =   3300
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         DataField       =   "CI22NUMHISTORIA"
         Height          =   330
         Index           =   1
         Left            =   120
         MaxLength       =   7
         TabIndex        =   0
         Tag             =   "Historia"
         ToolTipText     =   "Historia"
         Top             =   600
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   2
         Left            =   3120
         TabIndex        =   6
         Top             =   1800
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Estupefaciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   1
         Left            =   1320
         TabIndex        =   5
         Top             =   1800
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   1800
         Width           =   1095
      End
      Begin VB.TextBox txtproducto 
         Height          =   330
         Left            =   120
         MaxLength       =   6
         TabIndex        =   2
         Tag             =   "C�digo Producto"
         ToolTipText     =   "C�digo Producto"
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Frame Frame5 
         Caption         =   "Intervalo"
         ForeColor       =   &H00C00000&
         Height          =   1815
         Left            =   5160
         TabIndex        =   15
         Top             =   240
         Width           =   1935
         Begin SSCalendarWidgets_A.SSDateCombo dtcinicio 
            Height          =   330
            Left            =   120
            TabIndex        =   8
            Tag             =   "Fecha Inicio"
            ToolTipText     =   "Fecha Inicio"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcfin 
            Height          =   330
            Left            =   120
            TabIndex        =   9
            Tag             =   "Fecha Fin"
            ToolTipText     =   "Fecha Fin"
            Top             =   1320
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   17
            Top             =   360
            Width           =   870
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   16
            Top             =   1080
            Width           =   705
         End
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   3
         Left            =   4320
         TabIndex        =   7
         Top             =   1800
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.TextBox txtdescproducto 
         BackColor       =   &H00E0E0E0&
         Height          =   330
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   3
         Tag             =   "C�digo Producto"
         ToolTipText     =   "C�digo Producto"
         Top             =   1320
         Width           =   3255
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Paciente"
         Height          =   195
         Index           =   17
         Left            =   1680
         TabIndex        =   21
         Top             =   360
         Width           =   630
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   21
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   525
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d.Estupefaciente"
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   1395
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Estupefaciente"
         Height          =   195
         Index           =   2
         Left            =   1680
         TabIndex        =   18
         Top             =   1080
         Width           =   1065
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   12
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tab1 
      Height          =   4740
      Left            =   120
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   2880
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   8361
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "A nombre de Pacientes"
      TabPicture(0)   =   "FR0107.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "A nombre de Servicios"
      TabPicture(1)   =   "FR0107.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1(2)"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Caption         =   "Peticiones Validadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   4095
         Index           =   2
         Left            =   -74880
         TabIndex        =   34
         Top             =   480
         Width           =   11385
         Begin SSDataWidgets_B.SSDBGrid Grid1 
            Height          =   3615
            Index           =   2
            Left            =   120
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   360
            Width           =   11130
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   26
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "NuevoRegistro"
            Columns(0).Name =   "NuevoRegistro"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   11
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   3200
            Columns(1).Caption=   "EntradaManual?"
            Columns(1).Name =   "EntadaManual?"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   11
            Columns(1).FieldLen=   256
            Columns(1).Style=   2
            Columns(2).Width=   2170
            Columns(2).Caption=   "CodigoLibro"
            Columns(2).Name =   "CodigoLibro"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2487
            Columns(3).Caption=   "Fecha"
            Columns(3).Name =   "Fecha"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   7
            Columns(3).FieldLen=   256
            Columns(4).Width=   2778
            Columns(4).Caption=   "N�Recetario"
            Columns(4).Name =   "N�Recetario"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   2196
            Columns(5).Caption=   "N�Receta"
            Columns(5).Name =   "N�Receta"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   2037
            Columns(6).Caption=   "L�nea"
            Columns(6).Name =   "L�nea"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   1746
            Columns(7).Caption=   "C�dEstu"
            Columns(7).Name =   "C�dEstu"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   2646
            Columns(8).Caption=   "Estupefaciente"
            Columns(8).Name =   "Estupefaciente"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   2037
            Columns(9).Caption=   "Entrada"
            Columns(9).Name =   "Entrada"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   1720
            Columns(10).Caption=   "Salida"
            Columns(10).Name=   "Salida"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   2302
            Columns(11).Caption=   "CodProv"
            Columns(11).Name=   "CodProv"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   2699
            Columns(12).Caption=   "Proveedor"
            Columns(12).Name=   "Proveedor"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   2302
            Columns(13).Caption=   "C�dM�dico"
            Columns(13).Name=   "C�dM�dico"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Caption=   "M�dico"
            Columns(14).Name=   "M�dico"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   2302
            Columns(15).Caption=   "N�Colegiado"
            Columns(15).Name=   "N�Colegiado"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(16).Width=   2302
            Columns(16).Caption=   "C�dPaciente"
            Columns(16).Name=   "C�dPaciente"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(17).Width=   1826
            Columns(17).Caption=   "Historia"
            Columns(17).Name=   "Historia"
            Columns(17).DataField=   "Column 17"
            Columns(17).DataType=   8
            Columns(17).FieldLen=   256
            Columns(18).Width=   2170
            Columns(18).Caption=   "NombrePac"
            Columns(18).Name=   "NombrePac"
            Columns(18).DataField=   "Column 18"
            Columns(18).DataType=   8
            Columns(18).FieldLen=   256
            Columns(19).Width=   2566
            Columns(19).Caption=   "PriApelPac"
            Columns(19).Name=   "PriApelPac"
            Columns(19).DataField=   "Column 19"
            Columns(19).DataType=   8
            Columns(19).FieldLen=   256
            Columns(20).Width=   2646
            Columns(20).Caption=   "SegApelPac"
            Columns(20).Name=   "SegApelPac"
            Columns(20).DataField=   "Column 20"
            Columns(20).DataType=   8
            Columns(20).FieldLen=   256
            Columns(21).Width=   1984
            Columns(21).Caption=   "Cama"
            Columns(21).Name=   "Cama"
            Columns(21).DataField=   "Column 21"
            Columns(21).DataType=   8
            Columns(21).FieldLen=   256
            Columns(22).Width=   3200
            Columns(22).Caption=   "C�dServicio"
            Columns(22).Name=   "C�dServicio"
            Columns(22).DataField=   "Column 22"
            Columns(22).DataType=   8
            Columns(22).FieldLen=   256
            Columns(23).Width=   2064
            Columns(23).Caption=   "Servicio"
            Columns(23).Name=   "Servivio"
            Columns(23).DataField=   "Column 23"
            Columns(23).DataType=   8
            Columns(23).FieldLen=   256
            Columns(24).Width=   1588
            Columns(24).Caption=   "Saldos"
            Columns(24).Name=   "Saldos"
            Columns(24).DataField=   "Column 24"
            Columns(24).DataType=   8
            Columns(24).FieldLen=   256
            Columns(25).Width=   3200
            Columns(25).Caption=   "Observaciones"
            Columns(25).Name=   "Observaciones"
            Columns(25).DataField=   "Column 25"
            Columns(25).DataType=   8
            Columns(25).FieldLen=   256
            _ExtentX        =   19632
            _ExtentY        =   6376
            _StockProps     =   79
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Peticiones Validadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   4095
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   480
         Width           =   11385
         Begin SSDataWidgets_B.SSDBGrid Grid1 
            Height          =   3615
            Index           =   0
            Left            =   120
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   360
            Width           =   11130
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   26
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "NuevoRegistro"
            Columns(0).Name =   "NuevoRegistro"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   11
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   3200
            Columns(1).Caption=   "EntradaManual?"
            Columns(1).Name =   "EntadaManual?"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   11
            Columns(1).FieldLen=   256
            Columns(1).Style=   2
            Columns(2).Width=   2170
            Columns(2).Caption=   "CodigoLibro"
            Columns(2).Name =   "CodigoLibro"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2487
            Columns(3).Caption=   "Fecha"
            Columns(3).Name =   "Fecha"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   7
            Columns(3).FieldLen=   256
            Columns(4).Width=   2778
            Columns(4).Caption=   "N�Recetario"
            Columns(4).Name =   "N�Recetario"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   2196
            Columns(5).Caption=   "N�Receta"
            Columns(5).Name =   "N�Receta"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   2037
            Columns(6).Caption=   "L�nea"
            Columns(6).Name =   "L�nea"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   1746
            Columns(7).Caption=   "C�dEstu"
            Columns(7).Name =   "C�dEstu"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   2646
            Columns(8).Caption=   "Estupefaciente"
            Columns(8).Name =   "Estupefaciente"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   2037
            Columns(9).Caption=   "Entrada"
            Columns(9).Name =   "Entrada"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   1720
            Columns(10).Caption=   "Salida"
            Columns(10).Name=   "Salida"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   2302
            Columns(11).Caption=   "CodProv"
            Columns(11).Name=   "CodProv"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   2699
            Columns(12).Caption=   "Proveedor"
            Columns(12).Name=   "Proveedor"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   2302
            Columns(13).Caption=   "C�dM�dico"
            Columns(13).Name=   "C�dM�dico"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Caption=   "M�dico"
            Columns(14).Name=   "M�dico"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   2302
            Columns(15).Caption=   "N�Colegiado"
            Columns(15).Name=   "N�Colegiado"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(16).Width=   2302
            Columns(16).Caption=   "C�dPaciente"
            Columns(16).Name=   "C�dPaciente"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(17).Width=   1826
            Columns(17).Caption=   "Historia"
            Columns(17).Name=   "Historia"
            Columns(17).DataField=   "Column 17"
            Columns(17).DataType=   8
            Columns(17).FieldLen=   256
            Columns(18).Width=   2170
            Columns(18).Caption=   "NombrePac"
            Columns(18).Name=   "NombrePac"
            Columns(18).DataField=   "Column 18"
            Columns(18).DataType=   8
            Columns(18).FieldLen=   256
            Columns(19).Width=   2566
            Columns(19).Caption=   "PriApelPac"
            Columns(19).Name=   "PriApelPac"
            Columns(19).DataField=   "Column 19"
            Columns(19).DataType=   8
            Columns(19).FieldLen=   256
            Columns(20).Width=   2646
            Columns(20).Caption=   "SegApelPac"
            Columns(20).Name=   "SegApelPac"
            Columns(20).DataField=   "Column 20"
            Columns(20).DataType=   8
            Columns(20).FieldLen=   256
            Columns(21).Width=   1984
            Columns(21).Caption=   "Cama"
            Columns(21).Name=   "Cama"
            Columns(21).DataField=   "Column 21"
            Columns(21).DataType=   8
            Columns(21).FieldLen=   256
            Columns(22).Width=   3200
            Columns(22).Caption=   "C�dServicio"
            Columns(22).Name=   "C�dServicio"
            Columns(22).DataField=   "Column 22"
            Columns(22).DataType=   8
            Columns(22).FieldLen=   256
            Columns(23).Width=   2064
            Columns(23).Caption=   "Servicio"
            Columns(23).Name=   "Servivio"
            Columns(23).DataField=   "Column 23"
            Columns(23).DataType=   8
            Columns(23).FieldLen=   256
            Columns(24).Width=   1588
            Columns(24).Caption=   "Saldos"
            Columns(24).Name=   "Saldos"
            Columns(24).DataField=   "Column 24"
            Columns(24).DataType=   8
            Columns(24).FieldLen=   256
            Columns(25).Width=   3200
            Columns(25).Caption=   "Observaciones"
            Columns(25).Name=   "Observaciones"
            Columns(25).DataField=   "Column 25"
            Columns(25).DataType=   8
            Columns(25).FieldLen=   256
            _ExtentX        =   19632
            _ExtentY        =   6376
            _StockProps     =   79
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4425
         Index           =   3
         Left            =   -74880
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   240
         Width           =   10455
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   18441
         _ExtentY        =   7805
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1305
         Index           =   4
         Left            =   -74880
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   120
         Width           =   9855
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17383
         _ExtentY        =   2302
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmDispEstupef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDispEstupef (FR0107.FRM)                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: MAYO DEL 2000                                                 *
'* DESCRIPCION: Dispensar Estupefacientes                               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdAceptarBusEst_Click()
Dim mvarBkmrk As Variant
Dim mintisel As Integer
Dim numerolineas As Integer

FrameBusEst.Visible = False
GridBusEst.Visible = False

numerolineas = GridBusEst.Rows
For mintisel = 0 To numerolineas - 1
  mvarBkmrk = GridBusEst.SelBookmarks(mintisel)
  txtproducto.Text = GridBusEst.Columns(1).CellValue(mvarBkmrk)
  txtdescproducto.Text = GridBusEst.Columns(2).CellValue(mvarBkmrk)
Next mintisel
txtproducto.SetFocus
GridBusEst.RemoveAll
End Sub

Private Sub cmdBuscarEstupefacientes_Click()
Dim i As Integer
Dim stra As String
Dim rsta As rdoResultset

Screen.MousePointer = vbHourglass
cmdBuscarEstupefacientes.Enabled = False

FrameBusEst.Left = 240
FrameBusEst.Top = 2130
FrameBusEst.Visible = True
GridBusEst.Visible = True
FrameBusEst.ZOrder (0)
GridBusEst.ZOrder (0)
GridBusEst.Columns.Add (0)
GridBusEst.Columns.RemoveAll
For i = 0 To 2
  Call GridBusEst.Columns.Add(i)
Next i
GridBusEst.Columns(0).Caption = "C�digo"
GridBusEst.Columns(0).Locked = True
GridBusEst.Columns(0).Visible = False
GridBusEst.Columns(1).Caption = "Interno"
GridBusEst.Columns(1).Locked = True
GridBusEst.Columns(1).Width = 1200
GridBusEst.Columns(2).Caption = "Descripci�n"
GridBusEst.Columns(2).Locked = True
GridBusEst.Columns(2).Width = 6000
stra = "SELECT * FROM FR7300 WHERE FR73INDESTUPEFACI=-1 " & _
     " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) ORDER BY FR73DESPRODUCTO"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  GridBusEst.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                        rsta.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                        rsta.rdoColumns("FR73DESPRODUCTO").Value
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing

cmdBuscarEstupefacientes.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdDispensar_Click()
Dim strupdate As String
Dim rsta As rdoResultset
Dim stra As String
Dim TodaviaSinDispensar As Boolean
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant

Me.Enabled = False
Screen.MousePointer = vbHourglass
cmdDispensar.Enabled = False

If optorigen(0).Value = True Then 'Petici�n a Paciente
    's�lo se puede dispensar si la l�nea no lo est�
    If optestado(0).Value = True Then
        TodaviaSinDispensar = False
        'movimientos de almac�n y facturaci�n
        If Grid1(0).SelBookmarks.Count = 0 Then
          Call MsgBox("No ha seleccionado ninguna fila para dispensar", vbInformation, "Aviso")
          cmdDispensar.Enabled = True
          Me.Enabled = True
          Screen.MousePointer = vbDefault
          Exit Sub
        End If
        Call Movimientos_Almacen_y_Facturacion_Peticion_a_Paciente
        mintNTotalSelRows = Grid1(0).SelBookmarks.Count
        For mintisel = 0 To mintNTotalSelRows - 1
          mvarBkmrk = Grid1(0).SelBookmarks(mintisel)
          'se cambia el estado de la fila a Dispensada
          strupdate = "UPDATE FR1300 SET FR13INDDISPENSADO=-1 " & _
             "WHERE FR13CODLIBROESTUPEF=" & Grid1(0).Columns("CodigoLibro").CellValue(mvarBkmrk)
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
          'se mira si todas las l�neas de la petici�n est�n ya dispensadas para cambiar el
          'estado de la petici�n a 5=Dispensada
          stra = "SELECT * FROM FR1300 WHERE FR13NUMRECETAOFICIAL=" & Grid1(0).Columns("N�Receta").CellValue(mvarBkmrk)
          stra = stra & " AND FR13INDPRN=-1"
          stra = stra & " AND (FR13INDENTRADAMANUAL=0 OR FR13INDENTRADAMANUAL IS NULL)"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          Do While Not rsta.EOF
            If IsNull(rsta.rdoColumns("FR13INDDISPENSADO").Value) Then
              TodaviaSinDispensar = True
              Exit Do
            End If
          rsta.MoveNext
          Loop
          rsta.Close
          Set rsta = Nothing
          If TodaviaSinDispensar = False Then 'petici�n dispensada
            strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=5,FR66FECDISPEN=SYSDATE WHERE " & _
                      " FR66CODPETICION=" & Grid1(0).Columns("N�Receta").CellValue(mvarBkmrk)
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else 'petici�n dispensada parcial
            strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=9 WHERE " & _
                      " FR66CODPETICION=" & Grid1(0).Columns("N�Receta").CellValue(mvarBkmrk)
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          End If
        Next mintisel
        Call cmdFiltrar_Click  'se refresca el grid
    Else
      Call MsgBox("La l�nea ya est� dispensada, debe seleccionar una Validada", vbInformation, "Aviso")
    End If
End If
'Petici�n de Servicio
If optorigen(1).Value = True Then 'Petici�n a Paciente
    's�lo se puede dispensar si la l�nea no lo est�
    If optestado(0).Value = True Then
        TodaviaSinDispensar = False
        'movimientos de almac�n y facturaci�n
        If Grid1(2).SelBookmarks.Count = 0 Then
          Call MsgBox("No ha seleccionado ninguna fila para dispensar", vbInformation, "Aviso")
          cmdDispensar.Enabled = True
          Me.Enabled = True
          Screen.MousePointer = vbDefault
          Exit Sub
        End If
        Call Movimientos_Almacen_y_Facturacion_Peticion_de_Servicio
        mintNTotalSelRows = Grid1(2).SelBookmarks.Count
        For mintisel = 0 To mintNTotalSelRows - 1
          mvarBkmrk = Grid1(2).SelBookmarks(mintisel)
          'se cambia el estado de la fila a Dispensada
          strupdate = "UPDATE FR1300 SET FR13INDDISPENSADO=-1 " & _
             "WHERE FR13CODLIBROESTUPEF=" & Grid1(2).Columns("CodigoLibro").CellValue(mvarBkmrk)
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
          'se mira si todas las l�neas de la petici�n est�n ya dispensadas para cambiar el
          'estado de la petici�n a 5=Dispensada
          stra = "SELECT * FROM FR1300 WHERE FR13NUMRECETAOFICIAL=" & Grid1(2).Columns("N�Receta").CellValue(mvarBkmrk)
          stra = stra & " AND (FR13INDPRN=0 OR FR13INDPRN IS NULL) "
          stra = stra & " AND (FR13INDENTRADAMANUAL=0 OR FR13INDENTRADAMANUAL IS NULL)"
            
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          Do While Not rsta.EOF
            If IsNull(rsta.rdoColumns("FR13INDDISPENSADO").Value) Then
              TodaviaSinDispensar = True
              Exit Do
            End If
          rsta.MoveNext
          Loop
          rsta.Close
          Set rsta = Nothing
          If TodaviaSinDispensar = False Then
            'Dispensada
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=5,FR55FECDISPEN=SYSDATE WHERE " & _
                      " FR55CODNECESUNID=" & Grid1(2).Columns("N�Receta").CellValue(mvarBkmrk)
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else 'Dispensada
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=9,FR55FECDISPEN=SYSDATE WHERE " & _
                      " FR55CODNECESUNID=" & Grid1(2).Columns("N�Receta").CellValue(mvarBkmrk)
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          End If
        Next mintisel
        Call cmdFiltrar_Click  'se refresca el grid
    Else
      Call MsgBox("La l�nea ya est� dispensada, debe seleccionar una Validada", vbInformation, "Aviso")
    End If
End If

cmdDispensar.Enabled = True
Me.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdFiltrar_Click()
Dim strWhere As String
Dim stra As String
Dim rsta As rdoResultset
Dim strpersona As String
Dim rstpersona As rdoResultset
Dim rstprod As rdoResultset
Dim strprod  As String
Dim dpto As String
Dim strDpto As String
Dim rstdpto As rdoResultset
Dim strdepartamento As String
Dim codigodepartamento As String
Dim strdoctor As String
Dim rstdoctor As rdoResultset
Dim codigodoctor As String
Dim doctor As String
Dim numcolegiado As String
Dim strpac As String
Dim rstpac As rdoResultset
Dim codigopaciente As String
Dim historia As String
Dim nombre As String
Dim priapel As String
Dim segapel As String
Dim rstprov As rdoResultset
Dim strprov As String
Dim codproveedor As String
Dim proveedor As String
Dim cama As String
Dim numrecetaoficial As String
Dim numrecetario As String
Dim linea As String
Dim entrada As String
Dim salida As String
Dim saldos As String
Dim Observaciones As String
Dim codigoestupefaciente As String
Dim estupefaciente As String
Dim rstestu As rdoResultset
Dim strestu As String
Dim rstSTAT As rdoResultset
Dim strSTAT As String


Screen.MousePointer = vbHourglass
cmdfiltrar.Enabled = False

If Option1(0).Value = True Then 'Historia
  If IsNumeric(Text1(1).Text) Then
    strpersona = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rstpersona = objApp.rdoConnect.OpenResultset(strpersona)
    strWhere = strWhere & " AND FR1300.CI21CODPERSONA=" & rstpersona.rdoColumns("CI21CODPERSONA").Value
    rstpersona.Close
    Set rstpersona = Nothing
  Else
    Grid1(0).RemoveAll
    Grid1(2).RemoveAll
    cmdfiltrar.Enabled = True
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
Else
   If Option1(1).Value = True Then 'Estupefaciente
      If Trim(txtproducto.Text) <> "" Then
        strprod = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & "'" & txtproducto.Text & "'"
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If Not rstprod.EOF Then
          strWhere = strWhere & " AND FR1300.FR73CODPRODUCTO=" & rstprod.rdoColumns("FR73CODPRODUCTO").Value
        End If
        rstprod.Close
        Set rstprod = Nothing
      End If
    Else
      If Option1(2).Value = True Then 'Fechas
          'fecha inicio/fecha fin
          If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
            strWhere = strWhere & " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
            strWhere = strWhere & " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('31/12/9999','DD/MM/YYYY')"
          End If
          If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
            strWhere = strWhere & " AND TRUNC(FR1300.FR65FECPETICION) BETWEEN " & _
                     "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
          End If
      End If
   End If
End If
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Grid1(0).RemoveAll
Grid1(2).RemoveAll
If optestado(0).Value = True Then  'validadas
    stra = "SELECT FR1300.*,GCFN06(FR1300.AD15CODCAMA) CAMA,FR7300.FR73CODINTFAR,FR7300.FR73DESPRODUCTO,"
    stra = stra & "CI2200.CI22NUMHISTORIA"
    stra = stra & " FROM FR1300,FR7300,CI2200 "
    stra = stra & " WHERE FR1300.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
    stra = stra & " AND (FR13INDENTRADAMANUAL=0 OR FR13INDENTRADAMANUAL IS NULL)"
    stra = stra & " AND (FR13INDDISPENSADO=0 OR FR13INDDISPENSADO IS NULL)"
    stra = stra & " AND (FR13INDPETICANULADA=0 OR FR13INDPETICANULADA IS NULL)"
    If optorigen(0).Value = True Then 'Petici�n a Paciente
      stra = stra & " AND FR1300.FR13INDPRN=-1"
    Else   ' A servicio
      stra = stra & " AND (FR1300.FR13INDPRN=0 OR FR1300.FR13INDPRN IS NULL)"
    End If
    stra = stra & " AND FR1300.CI21CODPERSONA=CI2200.CI21CODPERSONA(+)" & strWhere
    stra = stra & " ORDER BY FR1300.FR13FECPETICION,"
    stra = stra & " FR1300.FR13NUMRECETAOFICIAL,FR1300.FR13NUMLINEA DESC"
End If
If optestado(1).Value = True Then  'dispensadas
    stra = "SELECT FR1300.*,GCFN06(FR1300.AD15CODCAMA) CAMA,FR7300.FR73CODINTFAR,FR7300.FR73DESPRODUCTO,"
    stra = stra & "CI2200.CI22NUMHISTORIA"
    stra = stra & " FROM FR1300,FR7300,CI2200"
    stra = stra & " WHERE FR1300.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
    stra = stra & " AND (FR13INDENTRADAMANUAL=0 OR FR13INDENTRADAMANUAL IS NULL)"
    stra = stra & " AND (FR13INDPETICANULADA=0 OR FR13INDPETICANULADA IS NULL)"
    If optorigen(0).Value = True Then 'Petici�n a Paciente
      stra = stra & " AND FR1300.FR13INDPRN=-1"
    Else  'A servicio
      stra = stra & " AND (FR1300.FR13INDPRN=0 OR FR1300.FR13INDPRN IS NULL)"
    End If
    stra = stra & " AND FR1300.FR13INDDISPENSADO=-1"
    stra = stra & " AND FR1300.CI21CODPERSONA=CI2200.CI21CODPERSONA(+)" & strWhere
    stra = stra & " ORDER BY FR1300.FR13FECPETICION,"
    stra = stra & " FR1300.FR13NUMRECETAOFICIAL,FR1300.FR13NUMLINEA DESC"
End If
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  'cama
  If Not IsNull(rsta.rdoColumns("CAMA").Value) Then
    cama = rsta.rdoColumns("CAMA").Value
  Else
    cama = " "
  End If
  
  'Departamento solicitante
  If Not IsNull(rsta.rdoColumns("AD02CODDPTO").Value) Then
    codigodepartamento = rsta.rdoColumns("AD02CODDPTO").Value
    strDpto = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rsta.rdoColumns("AD02CODDPTO").Value
    Set rstdpto = objApp.rdoConnect.OpenResultset(strDpto)
    If Not IsNull(rstdpto.rdoColumns("AD02DESDPTO").Value) Then
      strdepartamento = rstdpto.rdoColumns("AD02DESDPTO").Value
    Else
      strdepartamento = " "
    End If
    rstdpto.Close
    Set rstdpto = Nothing
  Else
    codigodepartamento = " "
    strdepartamento = " "
  End If
  
  'Estupefaciente
  If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
    codigoestupefaciente = rsta.rdoColumns("FR73CODPRODUCTO").Value
    strestu = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO").Value
    Set rstestu = objApp.rdoConnect.OpenResultset(strestu)
    If Not IsNull(rstestu.rdoColumns("FR73DESPRODUCTO").Value) Then
      estupefaciente = rstestu.rdoColumns("FR73DESPRODUCTO").Value
    Else
      estupefaciente = " "
    End If
    rstestu.Close
    Set rstestu = Nothing
  Else
    codigoestupefaciente = " "
    estupefaciente = " "
  End If
  
  'M�dico
  If Not IsNull(rsta.rdoColumns("SG02COD").Value) Then
    codigodoctor = rsta.rdoColumns("SG02COD").Value
    strdoctor = "SELECT SG02APE1,SG02NUMCOLEGIADO FROM SG0200 WHERE SG02COD=" & "'" & rsta.rdoColumns("SG02COD").Value & "'"
    Set rstdoctor = objApp.rdoConnect.OpenResultset(strdoctor)
    If Not IsNull(rstdoctor.rdoColumns("SG02APE1").Value) Then
      doctor = rstdoctor.rdoColumns("SG02APE1").Value
    Else
      doctor = " "
    End If
    If Not IsNull(rstdoctor.rdoColumns("SG02NUMCOLEGIADO").Value) Then
      numcolegiado = rstdoctor.rdoColumns("SG02NUMCOLEGIADO").Value
    Else
      numcolegiado = " "
    End If
    rstdoctor.Close
    Set rstdoctor = Nothing
  Else
    codigodoctor = " "
    doctor = " "
    numcolegiado = " "
  End If
  
  'Paciente
  If Not IsNull(rsta.rdoColumns("CI21CODPERSONA").Value) Then
    codigopaciente = rsta.rdoColumns("CI21CODPERSONA").Value
    strpac = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value
    Set rstpac = objApp.rdoConnect.OpenResultset(strpac)
    If Not IsNull(rstpac.rdoColumns("CI22NUMHISTORIA").Value) Then
      historia = rstpac.rdoColumns("CI22NUMHISTORIA").Value
    Else
      historia = " "
    End If
    If Not IsNull(rstpac.rdoColumns("CI22NOMBRE").Value) Then
      nombre = rstpac.rdoColumns("CI22NOMBRE").Value
    Else
      nombre = " "
    End If
    If Not IsNull(rstpac.rdoColumns("CI22PRIAPEL").Value) Then
      priapel = rstpac.rdoColumns("CI22PRIAPEL").Value
    Else
      priapel = " "
    End If
    If Not IsNull(rstpac.rdoColumns("CI22SEGAPEL").Value) Then
      segapel = rstpac.rdoColumns("CI22SEGAPEL").Value
    Else
      segapel = " "
    End If
    rstpac.Close
    Set rstpac = Nothing
  Else
    codigopaciente = " "
    historia = " "
    nombre = " "
    priapel = " "
    segapel = " "
  End If
  
  'Proveedor
  If Not IsNull(rsta.rdoColumns("FR79CODPROVEEDOR").Value) Then
    codproveedor = rsta.rdoColumns("FR79CODPROVEEDOR").Value
    strprov = "SELECT FR79PROVEEDOR FROM FR7900 WHERE " & _
              "FR79CODPROVEEDOR=" & rsta.rdoColumns("FR79CODPROVEEDOR").Value
    Set rstprov = objApp.rdoConnect.OpenResultset(strprov)
    If Not IsNull(rstprov.rdoColumns("FR79PROVEEDOR").Value) Then
      proveedor = rstprov.rdoColumns("FR79PROVEEDOR").Value
    Else
      proveedor = " "
    End If
    rstprov.Close
    Set rstprov = Nothing
  Else
    codproveedor = " "
    proveedor = " "
  End If
  'N�mero del Recetario
  If Not IsNull(rsta.rdoColumns("FR13NUMRECETARIO").Value) Then
    numrecetario = rsta.rdoColumns("FR13NUMRECETARIO").Value
  Else
    numrecetario = " "
  End If
  'N�mero de Receta oficial
  If Not IsNull(rsta.rdoColumns("FR13NUMRECETAOFICIAL").Value) Then
    numrecetaoficial = rsta.rdoColumns("FR13NUMRECETAOFICIAL").Value
  Else
    numrecetaoficial = " "
  End If
  'L�nea
  If Not IsNull(rsta.rdoColumns("FR13NUMLINEA").Value) Then
    linea = rsta.rdoColumns("FR13NUMLINEA").Value
  Else
    linea = " "
  End If
  'Saldos
  If Not IsNull(rsta.rdoColumns("FR13SALDOS").Value) Then
    saldos = rsta.rdoColumns("FR13SALDOS").Value
  Else
    saldos = " "
  End If
  'Observaciones
  If Not IsNull(rsta.rdoColumns("FR13OBSERVACIONES").Value) Then
    Observaciones = rsta.rdoColumns("FR13OBSERVACIONES").Value
  Else
    Observaciones = " "
  End If
  'Entrada
  If Not IsNull(rsta.rdoColumns("FR13ENTRADA").Value) Then
    entrada = rsta.rdoColumns("FR13ENTRADA").Value
    entrada = " "
  End If
  'Salida
  If Not IsNull(rsta.rdoColumns("FR13SALIDA").Value) Then
    salida = rsta.rdoColumns("FR13SALIDA").Value
  Else
    salida = " "
  End If
  If optorigen(0).Value = True Then  'Petici�n a Paciente
      'las l�neas que tienen clicado PERF y STAT no se dispensan ya que habr�n ido
      'a Fabricaci�n con su etiqueta correspondiente.
      strSTAT = "SELECT * FROM FR2800 WHERE FR66CODPETICION="
      strSTAT = strSTAT & rsta.rdoColumns("FR13NUMRECETAOFICIAL").Value
      strSTAT = strSTAT & " AND FR28NUMLINEA=" & rsta.rdoColumns("FR13NUMLINEA").Value
      strSTAT = strSTAT & " AND (FR28INDPERF=0 OR FR28INDPERF IS NULL)"
      strSTAT = strSTAT & " AND (FR28INDCOMIENINMED=0 OR FR28INDCOMIENINMED IS NULL)"
      strSTAT = strSTAT & " AND (FR28INDBLOQUEADA=0 OR FR28INDBLOQUEADA IS NULL)"
      Set rstSTAT = objApp.rdoConnect.OpenResultset(strSTAT)
      If Not rstSTAT.EOF Then  'l�neas no Stat y no Perf
            Grid1(0).AddItem 0 & Chr(vbKeyTab) & _
                  rsta.rdoColumns("FR13INDENTRADAMANUAL").Value & Chr(vbKeyTab) & _
                  rsta.rdoColumns("FR13CODLIBROESTUPEF").Value & Chr(vbKeyTab) & _
                  rsta.rdoColumns("FR13FECPETICION").Value & Chr(vbKeyTab) & _
                  numrecetario & Chr(vbKeyTab) & _
                  numrecetaoficial & Chr(vbKeyTab) & _
                  linea & Chr(vbKeyTab) & _
                  codigoestupefaciente & Chr(vbKeyTab) & _
                  estupefaciente & Chr(vbKeyTab) & _
                  entrada & Chr(vbKeyTab) & _
                  salida & Chr(vbKeyTab) & _
                  codproveedor & Chr(vbKeyTab) & proveedor & Chr(vbKeyTab) & _
                  codigodoctor & Chr(vbKeyTab) & _
                  doctor & Chr(vbKeyTab) & numcolegiado & Chr(vbKeyTab) & _
                  codigopaciente & Chr(vbKeyTab) & _
                  historia & Chr(vbKeyTab) & _
                  nombre & Chr(vbKeyTab) & priapel & Chr(vbKeyTab) & segapel & Chr(vbKeyTab) & _
                  cama & Chr(vbKeyTab) & codigodepartamento & Chr(vbKeyTab) & _
                  strdepartamento & Chr(vbKeyTab) & _
                  saldos & Chr(vbKeyTab) & _
                  Observaciones & Chr(vbKeyTab)
      End If
      rstSTAT.Close
      Set rstSTAT = Nothing
  Else  'Petici�n a Servicio
    Grid1(2).AddItem 0 & Chr(vbKeyTab) & _
                       rsta.rdoColumns("FR13INDENTRADAMANUAL").Value & Chr(vbKeyTab) & _
                       rsta.rdoColumns("FR13CODLIBROESTUPEF").Value & Chr(vbKeyTab) & _
                       rsta.rdoColumns("FR13FECPETICION").Value & Chr(vbKeyTab) & _
                       numrecetario & Chr(vbKeyTab) & _
                       numrecetaoficial & Chr(vbKeyTab) & _
                       linea & Chr(vbKeyTab) & _
                       codigoestupefaciente & Chr(vbKeyTab) & _
                       estupefaciente & Chr(vbKeyTab) & _
                       entrada & Chr(vbKeyTab) & _
                       salida & Chr(vbKeyTab) & _
                       codproveedor & Chr(vbKeyTab) & proveedor & Chr(vbKeyTab) & _
                       codigodoctor & Chr(vbKeyTab) & _
                       doctor & Chr(vbKeyTab) & numcolegiado & Chr(vbKeyTab) & _
                       codigopaciente & Chr(vbKeyTab) & _
                       historia & Chr(vbKeyTab) & _
                       nombre & Chr(vbKeyTab) & priapel & Chr(vbKeyTab) & segapel & Chr(vbKeyTab) & _
                       cama & Chr(vbKeyTab) & codigodepartamento & Chr(vbKeyTab) & _
                       strdepartamento & Chr(vbKeyTab) & _
                       saldos & Chr(vbKeyTab) & _
                       Observaciones & Chr(vbKeyTab)
  End If
rsta.MoveNext
Wend

cmdfiltrar.Enabled = True
Screen.MousePointer = vbDefault
End Sub


Private Sub cmdSalirBusEst_Click()
FrameBusEst.Visible = False
GridBusEst.Visible = False
GridBusEst.RemoveAll
txtproducto.SetFocus
End Sub

Private Sub cmdVerPeticion_Click()
cmdVerPeticion.Enabled = False
If optorigen(0).Value = True Then 'Petici�n a Paciente
  If Grid1(0).Rows > 0 Then
   If IsNumeric(Grid1(0).Columns("N�Receta").Value) Then
       gdispensarestupefacientes = "Dispensar Estupefacientes"
       glngpeticion = Grid1(0).Columns("N�Receta").Value
       Call objsecurity.LaunchProcess("FR0190")
       gdispensarestupefacientes = ""
   End If
  End If
End If
If optorigen(1).Value = True Then 'Petici�n de Servicio
  If Grid1(2).Rows > 0 Then
   If IsNumeric(Grid1(2).Columns("N�Receta").Value) Then
       gdispensarestupefacientes = "Dispensar Estupefacientes"
       glngnumpeticion = Grid1(2).Columns("N�Receta").Value
       Call objsecurity.LaunchProcess("FR0108")
       gdispensarestupefacientes = ""
   End If
  End If
End If
cmdVerPeticion.Enabled = True
End Sub

Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
End Sub

Private Sub optestado_Click(Index As Integer)
If optestado(0).Value = True Then
  Frame1(0).Caption = "Peticiones Validadas"
  Grid1(0).RemoveAll
  Frame1(2).Caption = "Peticiones Validadas"
  Grid1(2).RemoveAll
  cmdDispensar.Enabled = True
End If
If optestado(1).Value = True Then
  Frame1(0).Caption = "Peticiones Dispensadas"
  Grid1(0).RemoveAll
  Frame1(2).Caption = "Peticiones Dispensadas"
  Grid1(2).RemoveAll
  cmdDispensar.Enabled = False
End If
End Sub

Private Sub optorigen_Click(Index As Integer)
If optorigen(0).Value = True Then  'paciente
  tab1.Tab = 0
End If
If optorigen(1).Value = True Then  'servicio
  tab1.Tab = 1
End If
End Sub

Private Sub tab1_Click(PreviousTab As Integer)
If tab1.Tab = 0 Then
  optorigen(0).Value = True
End If
If tab1.Tab = 1 Then
  optorigen(1).Value = True
End If
End Sub

Private Sub Text1_Change(Index As Integer)
If Index = 1 Then
  Grid1(0).RemoveAll
  Grid1(2).RemoveAll
  Text1(2).Text = ""
End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If IsNumeric(Text1(1).Text) Then
    stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
      strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
    End If
    If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
      strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
    End If
    If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
      strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
    End If
    Text1(2).Text = strpaciente
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub txtproducto_Change()
  Grid1(0).RemoveAll
  Grid1(2).RemoveAll
  txtdescproducto.Text = ""
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If KeyAscii = 13 Then
  If IsNumeric(Text1(1).Text) Then
      stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
        strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
      End If
      Text1(2).Text = strpaciente
      rsta.Close
      Set rsta = Nothing
  End If
End If
End Sub


Private Sub txtproducto_KeyPress(KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strestupefaciente As String

If KeyAscii = 13 Then
  If Trim(txtproducto.Text) <> "" Then
      stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & txtproducto.Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("FR73DESPRODUCTO").Value) Then
          strestupefaciente = rsta.rdoColumns("FR73DESPRODUCTO").Value
        End If
        txtdescproducto = strestupefaciente
      End If
      rsta.Close
      Set rsta = Nothing
  End If
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim stra As String
    Dim rsta As rdoResultset
    Dim strpaciente As String
  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  '  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
   ' intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    'intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
   ' Call objWinInfo.WinDeRegister
   ' Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim i As Integer
Dim rsta As rdoResultset
Dim stra As String
Dim rstfec As rdoResultset
Dim strfec As String
Dim rstestu As rdoResultset
Dim strmensaje As String
Dim strmensajeerrores As String
Dim codigoestupefaciente As String
Dim codigoproveedor As String
Dim codigomedico As String
Dim codigoservicio As String
Dim codigopaciente As String
Dim strinsert As String
Dim fecha As String
Dim numrecetario As String
Dim numrecetaoficial As String
Dim linea As String
Dim entrada As String
Dim salida As String

Select Case btnButton.Index
  Case 21:
          Call Grid1(0).MoveFirst
  Case 22:
          Call Grid1(0).MovePrevious
  Case 23:
          Call Grid1(0).MoveNext
  Case 24:
          Call Grid1(0).MoveLast
  Case 26:
          Call Inicializar_Grid
          Call Grid1(0).Refresh
  Case 30:
          Unload Me
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Select Case intIndex
  Case 40:
          Call Grid1(0).MoveFirst
  Case 50:
          Call Grid1(0).MovePrevious
  Case 60:
          Call Grid1(0).MoveNext
  Case 70:
          Call Grid1(0).MoveLast
End Select
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
          Call Inicializar_Grid
          Call Grid1(0).Refresh
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    'Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub

Private Sub Inicializar_Grid()
Dim i As Integer

For i = 0 To 25
      Grid1(0).Columns(i).Locked = True
Next i
For i = 0 To 25
      Grid1(2).Columns(i).Locked = True
Next i

Grid1(0).Columns("NuevoRegistro").Visible = False
Grid1(0).Columns("EntradaManual?").Visible = False
Grid1(0).Columns("CodigoLibro").Visible = False
Grid1(0).Columns("N�Recetario").Visible = False
Grid1(0).Columns("Entrada").Visible = False
Grid1(0).Columns("CodProv").Visible = False
Grid1(0).Columns("Proveedor").Visible = False
Grid1(0).Columns("C�dPaciente").Visible = False
Grid1(0).Columns("C�dEstu").Visible = False
Grid1(0).Columns("C�dM�dico").Visible = False
Grid1(0).Columns("C�dServicio").Visible = False
Grid1(0).Columns("Observaciones").Visible = False
Grid1(0).Columns("Saldos").Visible = False
Grid1(2).Columns("Historia").Visible = False
Grid1(2).Columns("NombrePac").Visible = False
Grid1(2).Columns("PriApelPac").Visible = False
Grid1(2).Columns("SegApelPac").Visible = False
Grid1(2).Columns("Cama").Visible = False

Grid1(2).Columns("NuevoRegistro").Visible = False
Grid1(2).Columns("EntradaManual?").Visible = False
Grid1(2).Columns("CodigoLibro").Visible = False
Grid1(2).Columns("N�Recetario").Visible = False
Grid1(2).Columns("Entrada").Visible = False
Grid1(2).Columns("CodProv").Visible = False
Grid1(2).Columns("Proveedor").Visible = False
Grid1(2).Columns("C�dPaciente").Visible = False
Grid1(2).Columns("C�dEstu").Visible = False
Grid1(2).Columns("C�dM�dico").Visible = False
Grid1(2).Columns("C�dServicio").Visible = False
Grid1(2).Columns("Observaciones").Visible = False
Grid1(2).Columns("Saldos").Visible = False

Grid1(0).Columns("Fecha").Width = 1050
Grid1(0).Columns("N�Receta").Width = 1000
Grid1(0).Columns("L�nea").Width = 600
Grid1(0).Columns("Estupefaciente").Width = 3800
Grid1(0).Columns("Salida").Width = 700
Grid1(0).Columns("M�dico").Width = 1000
Grid1(0).Columns("N�Colegiado").Width = 1100
Grid1(0).Columns("Historia").Width = 800
Grid1(0).Columns("NombrePac").Width = 1500
Grid1(0).Columns("PriApelPac").Width = 1500
Grid1(0).Columns("SegApelPac").Width = 1500
Grid1(0).Columns("Cama").Width = 700
Grid1(0).Columns("Servicio").Width = 2000

Grid1(2).Columns("Fecha").Width = 1050
Grid1(2).Columns("N�Receta").Width = 1000
Grid1(2).Columns("L�nea").Width = 600
Grid1(2).Columns("Estupefaciente").Width = 3800
Grid1(2).Columns("Salida").Width = 700
Grid1(2).Columns("M�dico").Width = 1000
Grid1(2).Columns("N�Colegiado").Width = 1100
Grid1(2).Columns("Historia").Width = 800
Grid1(2).Columns("NombrePac").Width = 1500
Grid1(2).Columns("PriApelPac").Width = 1500
Grid1(2).Columns("SegApelPac").Width = 1500
Grid1(2).Columns("Cama").Width = 700
Grid1(2).Columns("Servicio").Width = 2000

End Sub

Private Sub txtproducto_LostFocus()
Dim stra As String
Dim rsta As rdoResultset
Dim strestupefaciente As String

If Trim(txtproducto.Text) <> "" Then
    stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & txtproducto.Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
      If Not IsNull(rsta.rdoColumns("FR73DESPRODUCTO").Value) Then
        strestupefaciente = rsta.rdoColumns("FR73DESPRODUCTO").Value
      End If
      txtdescproducto = strestupefaciente
    End If
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub Movimientos_Almacen_y_Facturacion_Peticion_a_Paciente()
Dim rsta As rdoResultset
Dim stra As String
Dim strIns As String
Dim strdetalle As String
Dim rstdetalle As rdoResultset
Dim strFR04 As String
Dim qryFR04  As rdoQuery
Dim rdoFR04 As rdoResultset
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset
Dim strAlmFar As String
Dim strAlmDes As String
Dim strIns80 As String
Dim strIns35 As String
Dim strSql As String
Dim rdoSql As rdoResultset
Dim strCod80 As String
Dim strCod35 As String
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim marca As Variant
Dim investigacion As String
Dim dptocargo As String

mintNTotalSelRows = Grid1(0).SelBookmarks.Count
For mintisel = 0 To mintNTotalSelRows - 1
  marca = Grid1(0).SelBookmarks(mintisel)
  
  'Movimientos de Facturaci�n
  '--------------------------
  stra = "SELECT * FROM FR6600 WHERE FR66CODPETICION=" & Grid1(0).Columns("N�Receta").CellValue(marca)
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    'Se realiza el insert en FR6500
    If IsNull(rsta.rdoColumns("FR66INDINTERCIENT").Value) Then
         investigacion = "null"
    Else
         investigacion = rsta.rdoColumns("FR66INDINTERCIENT").Value
    End If
    If IsNull(rsta.rdoColumns("AD02CODDPTO_CRG").Value) Then
         dptocargo = "null"
    Else
         dptocargo = rsta.rdoColumns("AD02CODDPTO_CRG").Value
    End If
    
    strIns = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,"
    strIns = strIns & "FR65HORA,FR65CANTIDAD,FR73CODPRODUCTO_DIL,"
    strIns = strIns & "FR65CANTIDADDIL,FR65DESPRODUCTO,FR65DOSIS,FR93CODUNIMEDIDA,"
    strIns = strIns & "FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO,"
    strIns = strIns & "FR65INDINTERCIENT,AD02CODDPTO_CRG)"
    strIns = strIns & " VALUES (FR65CODIGO_SEQUENCE.NEXTVAL" & ","
    strIns = strIns & Grid1(0).Columns("C�dPaciente").CellValue(marca) & ","
    strIns = strIns & Grid1(0).Columns("C�dEstu").CellValue(marca) & ","
    strIns = strIns & "TRUNC(SYSDATE),TO_CHAR(SYSDATE,'HH24')" & ","
    strIns = strIns & objGen.ReplaceStr(Grid1(0).Columns("Salida").CellValue(marca), ",", ".", 1) & ","
    strIns = strIns & "NULL" & ","
    strIns = strIns & "NULL" & ","
    strIns = strIns & "'" & Grid1(0).Columns("Estupefaciente").CellValue(marca) & "'" & ","
    'dosis,UM
    strdetalle = "SELECT * FROM FR2800 WHERE " & _
                 " FR66CODPETICION=" & Grid1(0).Columns("N�Receta").CellValue(marca) & _
                 " AND FR28NUMLINEA=" & Grid1(0).Columns("L�nea").CellValue(marca)
    Set rstdetalle = objApp.rdoConnect.OpenResultset(strdetalle)
    If Not rstdetalle.EOF Then
      If Not IsNull(rstdetalle.rdoColumns("FR28DOSIS").Value) Then
         strIns = strIns & objGen.ReplaceStr(rstdetalle.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
      Else
         strIns = strIns & "0" & ","
      End If
      If Not IsNull(rstdetalle.rdoColumns("FR93CODUNIMEDIDA").Value) Then
         strIns = strIns & "'" & rstdetalle.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
      Else
         strIns = strIns & "NULL" & ","
      End If
    Else
        strIns = strIns & "0" & ","
        strIns = strIns & "NULL" & ","
    End If
    rstdetalle.Close
    Set rstdetalle = Nothing
    
    strIns = strIns & Grid1(0).Columns("N�Receta").CellValue(marca) & ","
    strIns = strIns & 1 & ","
    strIns = strIns & rsta.rdoColumns("AD01CODASISTENCI").Value & ","
    strIns = strIns & rsta.rdoColumns("AD07CODPROCESO").Value & ","
    strIns = strIns & investigacion & "," & dptocargo & ")"
    
    objApp.rdoConnect.Execute strIns, 64
    objApp.rdoConnect.Execute "commit", 64
    
    'Movimientos de Almacenes
    '------------------------
    'Almac�n de Destino
    strFR04 = "SELECT * FROM FR0400 WHERE AD02CODDPTO = ?"
    Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
    qryFR04(0) = rsta.rdoColumns("AD02CODDPTO").Value
    Set rdoFR04 = qryFR04.OpenResultset()
    If Not IsNull(rdoFR04.rdoColumns("FR04CODALMACEN").Value) Then
      strAlmDes = rdoFR04.rdoColumns("FR04CODALMACEN").Value
    Else
     strAlmDes = 999 'Almac�n ficticio
    End If
    qryFR04.Close
    Set qryFR04 = Nothing
    Set rdoFR04 = Nothing
    
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 28
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strAlmFar = "0"
    Else
      strAlmFar = rdoFRH2.rdoColumns(0).Value
    End If
    qryFRH2.Close
    Set qryFRH2 = Nothing
    Set rdoFRH2 = Nothing
  
    strSql = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod80 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
  
    'Se realiza el insert en FR8000: salidas de almac�n
    If Grid1(0).Columns("C�dEstu").CellValue(marca) <> "999999999" Then
      strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
      strIns80 = strIns80 & "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR73CODPRODUCTO,"
      strIns80 = strIns80 & "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN)"
      strIns80 = strIns80 & "VALUES ("
      strIns80 = strIns80 & strCod80 & "," & strAlmFar & "," & strAlmDes & ",9,SYSDATE,"
      strIns80 = strIns80 & Grid1(0).Columns("C�dEstu").CellValue(marca) & ","
      strIns80 = strIns80 & objGen.ReplaceStr(Grid1(0).Columns("Salida").CellValue(marca), ",", ".", 1) & ","
      strIns80 = strIns80 & "1,'NE',"
      strIns80 = strIns80 & objGen.ReplaceStr(Grid1(0).Columns("Salida").CellValue(marca), ",", ".", 1)
      strIns80 = strIns80 & ")"
      objApp.rdoConnect.Execute strIns80, 64
    End If
    'Se realiza el insert en FR3500
    strSql = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod35 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    'Se realiza el insert en FR3500: entradas de almac�n
    If Grid1(0).Columns("C�dEstu").CellValue(marca) <> "999999999" Then
      strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,"
      strIns35 = strIns35 & "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,"
      strIns35 = strIns35 & "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,"
      strIns35 = strIns35 & "FR93CODUNIMEDIDA,FR35UNIENT)"
      strIns35 = strIns35 & "VALUES ("
      strIns35 = strIns35 & strCod35 & "," & strAlmFar & "," & strAlmDes & ","
      strIns35 = strIns35 & "9,SYSDATE,"
      strIns35 = strIns35 & Grid1(0).Columns("C�dEstu").CellValue(marca) & ","
      strIns35 = strIns35 & objGen.ReplaceStr(Grid1(0).Columns("Salida").CellValue(marca), ",", ".", 1) & ","
      strIns35 = strIns35 & "1,'NE',"
      strIns35 = strIns35 & objGen.ReplaceStr(Grid1(0).Columns("Salida").CellValue(marca), ",", ".", 1)
      strIns35 = strIns35 & ")"
      objApp.rdoConnect.Execute strIns35, 64
    End If
 End If
 rsta.Close
 Set rsta = Nothing
Next mintisel
End Sub

Private Sub Movimientos_Almacen_y_Facturacion_Peticion_de_Servicio()
Dim rsta As rdoResultset
Dim stra As String
Dim strIns As String
Dim strdetalle As String
Dim rstdetalle As rdoResultset
Dim strFR04 As String
Dim qryFR04  As rdoQuery
Dim rdoFR04 As rdoResultset
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset
Dim strAlmFar As String
Dim strAlmDes As String
Dim strIns80 As String
Dim strIns35 As String
Dim strSql As String
Dim rdoSql As rdoResultset
Dim strCod80 As String
Dim strCod35 As String
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim marca As Variant
Dim investigacion As String
Dim dptocargo As String


mintNTotalSelRows = Grid1(2).SelBookmarks.Count
For mintisel = 0 To mintNTotalSelRows - 1
  marca = Grid1(2).SelBookmarks(mintisel)
  
  'Movimientos de Facturaci�n
  '--------------------------
  stra = "SELECT * FROM FR5500 WHERE FR55CODNECESUNID=" & Grid1(2).Columns("N�Receta").CellValue(marca)
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    If IsNull(rsta.rdoColumns("FR55INDINTERCIENT").Value) Then
         investigacion = "null"
    Else
         investigacion = rsta.rdoColumns("FR55INDINTERCIENT").Value
    End If
    If IsNull(rsta.rdoColumns("AD02CODDPTO_CRG").Value) Then
         dptocargo = "null"
    Else
         dptocargo = rsta.rdoColumns("AD02CODDPTO_CRG").Value
    End If
    'Se realiza el insert en FRK100
    strIns = "INSERT INTO FRK100 ("
    strIns = strIns & "FRK1CODMOV,FRK1FECMOV,"
    strIns = strIns & "AD02CODDPTO,FR73CODPRODUCTO,"
    strIns = strIns & "FRK1CANTIDAD,FRK1TAMENVASE,"
    strIns = strIns & "FRK1INDMOD,FRK1INDFAC,"
    strIns = strIns & "FRK1CODNECESUNID,FRK1INDINTERCIENT,AD02CODDPTO_CRG"
    strIns = strIns & ") VALUES ("
    strIns = strIns & "FRK1CODMOV_SEQUENCE.NEXTVAL" & ","
    strIns = strIns & "SYSDATE,"
    strIns = strIns & rsta.rdoColumns("AD02CODDPTO").Value & ","
    strIns = strIns & Grid1(2).Columns("C�dEstu").CellValue(marca) & ","
    strIns = strIns & Grid1(2).Columns("Salida").CellValue(marca) & ","
    'tama�o del envase
    strdetalle = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & Grid1(2).Columns("C�dEstu").CellValue(marca)
    Set rstdetalle = objApp.rdoConnect.OpenResultset(strdetalle)
    If Not rstdetalle.EOF Then
      If IsNull(rstdetalle.rdoColumns("FR73TAMENVASE").Value) Then
            strIns = strIns & "1,"
      Else
            strIns = strIns & rstdetalle.rdoColumns("FR73TAMENVASE").Value & ","
      End If
    End If
    rstdetalle.Close
    Set rstdetalle = Nothing
    strIns = strIns & "0,0," & Grid1(2).Columns("N�Receta").CellValue(marca) & ","
    strIns = strIns & investigacion & "," & dptocargo & ")"
  
    objApp.rdoConnect.Execute strIns, 64
    objApp.rdoConnect.Execute "commit", 64
    
    'Movimientos de Almacenes
    '------------------------
    'Almac�n de Destino
    strFR04 = "SELECT * FROM FR0400 WHERE AD02CODDPTO = ?"
    Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
    qryFR04(0) = rsta.rdoColumns("AD02CODDPTO").Value
    Set rdoFR04 = qryFR04.OpenResultset()
    If Not IsNull(rdoFR04.rdoColumns("FR04CODALMACEN").Value) Then
      strAlmDes = rdoFR04.rdoColumns("FR04CODALMACEN").Value
    Else
     strAlmDes = 999 'Almac�n ficticio
    End If
    qryFR04.Close
    Set qryFR04 = Nothing
    Set rdoFR04 = Nothing
    
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 28
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strAlmFar = "0"
    Else
      strAlmFar = rdoFRH2.rdoColumns(0).Value
    End If
    qryFRH2.Close
    Set qryFRH2 = Nothing
    Set rdoFRH2 = Nothing
  
    strSql = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod80 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
  
    'Se realiza el insert en FR8000: salidas de almac�n
    If Grid1(2).Columns("C�dEstu").CellValue(marca) <> "999999999" Then
      strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
      strIns80 = strIns80 & "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR73CODPRODUCTO,"
      strIns80 = strIns80 & "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN)"
      strIns80 = strIns80 & "VALUES ("
      strIns80 = strIns80 & strCod80 & "," & strAlmFar & "," & strAlmDes & ",9,SYSDATE,"
      strIns80 = strIns80 & Grid1(2).Columns("C�dEstu").CellValue(marca) & ","
      strIns80 = strIns80 & objGen.ReplaceStr(Grid1(2).Columns("Salida").CellValue(marca), ",", ".", 1) & ","
      strIns80 = strIns80 & "1,'NE',"
      strIns80 = strIns80 & objGen.ReplaceStr(Grid1(2).Columns("Salida").CellValue(marca), ",", ".", 1)
      strIns80 = strIns80 & ")"
      objApp.rdoConnect.Execute strIns80, 64
    End If
    'Se realiza el insert en FR3500
    strSql = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod35 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    'Se realiza el insert en FR3500: entradas de almac�n
    If Grid1(2).Columns("C�dEstu").CellValue(marca) <> "999999999" Then
      strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,"
      strIns35 = strIns35 & "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,"
      strIns35 = strIns35 & "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,"
      strIns35 = strIns35 & "FR93CODUNIMEDIDA,FR35UNIENT)"
      strIns35 = strIns35 & "VALUES ("
      strIns35 = strIns35 & strCod35 & "," & strAlmFar & "," & strAlmDes & ","
      strIns35 = strIns35 & "9,SYSDATE,"
      strIns35 = strIns35 & Grid1(2).Columns("C�dEstu").CellValue(marca) & ","
      strIns35 = strIns35 & objGen.ReplaceStr(Grid1(2).Columns("Salida").CellValue(marca), ",", ".", 1) & ","
      strIns35 = strIns35 & "1,'NE',"
      strIns35 = strIns35 & objGen.ReplaceStr(Grid1(2).Columns("Salida").CellValue(marca), ",", ".", 1)
      strIns35 = strIns35 & ")"
      objApp.rdoConnect.Execute strIns35, 64
    End If
 End If
 rsta.Close
 Set rsta = Nothing
Next mintisel
End Sub



