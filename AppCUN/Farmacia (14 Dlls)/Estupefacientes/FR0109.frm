VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmMonLibEstupef 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. CONTROLAR ESTUPEFACIENTES. Monitorizar Libro de Estupefacientes."
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0109.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame FrameBusEst 
      Height          =   2895
      Left            =   4320
      TabIndex        =   27
      Top             =   2280
      Visible         =   0   'False
      Width           =   6855
      Begin VB.CommandButton cmdAceptarBusEst 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   5760
         TabIndex        =   29
         Top             =   960
         Width           =   855
      End
      Begin VB.CommandButton cmdSalirBusEst 
         Caption         =   "Salir"
         Height          =   375
         Left            =   5760
         TabIndex        =   28
         Top             =   1440
         Width           =   855
      End
      Begin SSDataWidgets_B.SSDBGrid GridBusEst 
         Height          =   2535
         Left            =   120
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   240
         Visible         =   0   'False
         Width           =   5490
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   9684
         _ExtentY        =   4471
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdfiltrar 
      Caption         =   "FILTRAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   12
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Libro de Estupefacientes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5295
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   2880
      Width           =   11745
      Begin VB.Frame FrameBuscador 
         Height          =   2655
         Left            =   1680
         TabIndex        =   23
         Top             =   1560
         Visible         =   0   'False
         Width           =   8175
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "Aceptar"
            Height          =   375
            Left            =   6960
            TabIndex        =   26
            Top             =   840
            Width           =   855
         End
         Begin VB.CommandButton cmdSalir 
            Caption         =   "Salir"
            Height          =   375
            Left            =   6960
            TabIndex        =   25
            Top             =   1440
            Width           =   855
         End
         Begin SSDataWidgets_B.SSDBGrid GridBuscador 
            Height          =   2295
            Left            =   120
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   240
            Visible         =   0   'False
            Width           =   6690
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   11800
            _ExtentY        =   4048
            _StockProps     =   79
         End
      End
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   4815
         Index           =   0
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   27
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   27
         Columns(0).Width=   3200
         Columns(0).Caption=   "NuevoRegistro"
         Columns(0).Name =   "NuevoRegistro"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   3200
         Columns(1).Caption=   "EntradaManual?"
         Columns(1).Name =   "EntadaManual?"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   11
         Columns(1).FieldLen=   256
         Columns(1).Style=   2
         Columns(2).Width=   2170
         Columns(2).Caption=   "CodigoLibro"
         Columns(2).Name =   "CodigoLibro"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2487
         Columns(3).Caption=   "Fecha"
         Columns(3).Name =   "Fecha"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   7
         Columns(3).FieldLen=   256
         Columns(4).Width=   2778
         Columns(4).Caption=   "N�Recetario"
         Columns(4).Name =   "N�Recetario"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2196
         Columns(5).Caption=   "N�Receta"
         Columns(5).Name =   "N�Receta"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   2037
         Columns(6).Caption=   "L�nea"
         Columns(6).Name =   "L�nea"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   1746
         Columns(7).Caption=   "C�dEstu"
         Columns(7).Name =   "C�dEstu"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   2646
         Columns(8).Caption=   "Estupefaciente"
         Columns(8).Name =   "Estupefaciente"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   2037
         Columns(9).Caption=   "Entrada"
         Columns(9).Name =   "Entrada"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   1720
         Columns(10).Caption=   "Salida"
         Columns(10).Name=   "Salida"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   2302
         Columns(11).Caption=   "CodProv"
         Columns(11).Name=   "CodProv"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   2699
         Columns(12).Caption=   "Proveedor"
         Columns(12).Name=   "Proveedor"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   2302
         Columns(13).Caption=   "C�dM�dico"
         Columns(13).Name=   "C�dM�dico"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Caption=   "M�dico"
         Columns(14).Name=   "M�dico"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   2302
         Columns(15).Caption=   "N�Colegiado"
         Columns(15).Name=   "N�Colegiado"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   2302
         Columns(16).Caption=   "C�dPaciente"
         Columns(16).Name=   "C�dPaciente"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(17).Width=   1826
         Columns(17).Caption=   "Historia"
         Columns(17).Name=   "Historia"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(18).Width=   2170
         Columns(18).Caption=   "NombrePac"
         Columns(18).Name=   "NombrePac"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(19).Width=   2566
         Columns(19).Caption=   "PriApelPac"
         Columns(19).Name=   "PriApelPac"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(20).Width=   2646
         Columns(20).Caption=   "SegApelPac"
         Columns(20).Name=   "SegApelPac"
         Columns(20).DataField=   "Column 20"
         Columns(20).DataType=   8
         Columns(20).FieldLen=   256
         Columns(21).Width=   1984
         Columns(21).Caption=   "Cama"
         Columns(21).Name=   "Cama"
         Columns(21).DataField=   "Column 21"
         Columns(21).DataType=   8
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Caption=   "C�dServicio"
         Columns(22).Name=   "C�dServicio"
         Columns(22).DataField=   "Column 22"
         Columns(22).DataType=   8
         Columns(22).FieldLen=   256
         Columns(23).Width=   2064
         Columns(23).Caption=   "Servicio Solicitante"
         Columns(23).Name=   "Servivio"
         Columns(23).DataField=   "Column 23"
         Columns(23).DataType=   8
         Columns(23).FieldLen=   256
         Columns(24).Width=   1588
         Columns(24).Caption=   "Saldos"
         Columns(24).Name=   "Saldos"
         Columns(24).DataField=   "Column 24"
         Columns(24).DataType=   8
         Columns(24).FieldLen=   256
         Columns(25).Width=   3200
         Columns(25).Caption=   "Observaciones"
         Columns(25).Name=   "Observaciones"
         Columns(25).DataField=   "Column 25"
         Columns(25).DataType=   8
         Columns(25).FieldLen=   256
         Columns(26).Width=   3200
         Columns(26).Caption=   "Estupefaciente?"
         Columns(26).Name=   "Estupefaciente?"
         Columns(26).DataField=   "Column 26"
         Columns(26).DataType=   11
         Columns(26).FieldLen=   256
         Columns(26).Style=   2
         _ExtentX        =   20267
         _ExtentY        =   8493
         _StockProps     =   79
         Caption         =   "LIBRO DE ESTUPEFACIENTES"
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "B�squeda por "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   2295
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   10095
      Begin VB.CommandButton cmdBuscarEstupefacientes 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   220
         Left            =   1370
         TabIndex        =   31
         Top             =   1430
         Width           =   330
      End
      Begin VB.TextBox txtdescproducto 
         BackColor       =   &H00E0E0E0&
         Height          =   330
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   21
         Tag             =   "C�digo Producto"
         ToolTipText     =   "C�digo Producto"
         Top             =   1320
         Width           =   3945
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   3
         Left            =   4320
         TabIndex        =   20
         Top             =   1800
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.Frame Frame5 
         Caption         =   "Intervalo"
         ForeColor       =   &H00C00000&
         Height          =   1815
         Left            =   6000
         TabIndex        =   15
         Top             =   240
         Width           =   1935
         Begin SSCalendarWidgets_A.SSDateCombo dtcinicio 
            Height          =   330
            Left            =   120
            TabIndex        =   16
            Tag             =   "Fecha Inicio"
            ToolTipText     =   "Fecha Inicio"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcfin 
            Height          =   330
            Left            =   120
            TabIndex        =   17
            Tag             =   "Fecha Fin"
            ToolTipText     =   "Fecha Fin"
            Top             =   1320
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   19
            Top             =   1080
            Width           =   705
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   18
            Top             =   360
            Width           =   870
         End
      End
      Begin VB.TextBox txtproducto 
         Height          =   330
         Left            =   120
         MaxLength       =   6
         TabIndex        =   13
         Tag             =   "C�digo Producto"
         ToolTipText     =   "C�digo Producto"
         Top             =   1320
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   1800
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Estupefaciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   1
         Left            =   1320
         TabIndex        =   6
         Top             =   1800
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   2
         Left            =   3120
         TabIndex        =   5
         Top             =   1800
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         DataField       =   "CI22NUMHISTORIA"
         Height          =   330
         Index           =   1
         Left            =   120
         MaxLength       =   7
         TabIndex        =   4
         Tag             =   "Historia"
         ToolTipText     =   "Historia"
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00E0E0E0&
         DataField       =   "CI22NOMBRE"
         Height          =   330
         Index           =   2
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   3
         Tag             =   "Paciente"
         ToolTipText     =   "Paciente"
         Top             =   600
         Width           =   3945
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Estupefaciente"
         Height          =   195
         Index           =   2
         Left            =   1800
         TabIndex        =   22
         Top             =   1080
         Width           =   1065
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d.Estupefaciente"
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   14
         Top             =   1080
         Width           =   1395
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   21
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   525
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Paciente"
         Height          =   195
         Index           =   17
         Left            =   1800
         TabIndex        =   8
         Top             =   360
         Width           =   630
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10920
      Top             =   960
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmMonLibEstupef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: FR0109.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: MAYO DEL 2000                                                 *
'* DESCRIPCI�N: Libro de Estupefacientes                                *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim buscador As String
Dim LineaNoGuardada As Boolean


Private Sub cmdaceptar_Click()
Dim mvarBkmrk As Variant
Dim mintisel As Integer
Dim numerolineas As Integer

FrameBuscador.Visible = False
GridBuscador.Visible = False

numerolineas = GridBuscador.Rows
For mintisel = 0 To numerolineas - 1
  mvarBkmrk = GridBuscador.SelBookmarks(mintisel)
  Select Case buscador
    Case "Estupefacientes"
        Grid1(0).Columns("C�dEstu").Value = GridBuscador.Columns(0).CellValue(mvarBkmrk)
        Grid1(0).Columns("Estupefaciente").Value = GridBuscador.Columns(2).CellValue(mvarBkmrk)
    Case "Proveedores"
        Grid1(0).Columns("CodProv").Value = GridBuscador.Columns(0).CellValue(mvarBkmrk)
        Grid1(0).Columns("Proveedor").Value = GridBuscador.Columns(1).CellValue(mvarBkmrk)
    Case "M�dicos"
        Grid1(0).Columns("C�dM�dico").Value = GridBuscador.Columns(0).CellValue(mvarBkmrk)
        Grid1(0).Columns("M�dico").Value = GridBuscador.Columns(1).CellValue(mvarBkmrk)
        Grid1(0).Columns("N�Colegiado").Value = GridBuscador.Columns(2).CellValue(mvarBkmrk)
    Case "Servicios"
        Grid1(0).Columns("C�dServicio").Value = GridBuscador.Columns(0).CellValue(mvarBkmrk)
        Grid1(0).Columns("Servicio Solicitante").Value = GridBuscador.Columns(1).CellValue(mvarBkmrk)
  End Select
  buscador = ""
Next mintisel

GridBuscador.RemoveAll
End Sub

Private Sub cmdAceptarBusEst_Click()
Dim mvarBkmrk As Variant
Dim mintisel As Integer
Dim numerolineas As Integer

FrameBusEst.Visible = False
GridBusEst.Visible = False

numerolineas = GridBusEst.Rows
For mintisel = 0 To numerolineas - 1
  mvarBkmrk = GridBusEst.SelBookmarks(mintisel)
  txtproducto.Text = GridBusEst.Columns(1).CellValue(mvarBkmrk)
  txtdescproducto.Text = GridBusEst.Columns(2).CellValue(mvarBkmrk)
Next mintisel
txtproducto.SetFocus
GridBusEst.RemoveAll
End Sub

Private Sub cmdBuscarEstupefacientes_Click()
Dim i As Integer
Dim stra As String
Dim rsta As rdoResultset

Screen.MousePointer = vbHourglass
cmdBuscarEstupefacientes.Enabled = False

FrameBusEst.Left = 240
FrameBusEst.Top = 2160
FrameBusEst.Visible = True
GridBusEst.Visible = True
FrameBusEst.ZOrder (0)
GridBusEst.ZOrder (0)
GridBusEst.Columns.Add (0)
GridBusEst.Columns.RemoveAll
'For i = 0 To 2
'  Call GridBusEst.Columns.Add(i)
'Next i
Call GridBusEst.Columns.Add(0)
GridBusEst.Columns(0).Visible = False
GridBusEst.Columns(0).Caption = "C�digo"
GridBusEst.Columns(0).Locked = True
Call GridBusEst.Columns.Add(1)
GridBusEst.Columns(1).Caption = "Interno"
GridBusEst.Columns(1).Locked = True
GridBusEst.Columns(1).Width = 1200
Call GridBusEst.Columns.Add(2)
GridBusEst.Columns(2).Caption = "Descripci�n"
GridBusEst.Columns(2).Locked = True
GridBusEst.Columns(2).Width = 6000
stra = "SELECT * FROM FR7300 WHERE FR73INDESTUPEFACI=-1 " & _
     " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) ORDER BY FR73DESPRODUCTO"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  GridBusEst.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                        rsta.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                        rsta.rdoColumns("FR73DESPRODUCTO").Value
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing

cmdBuscarEstupefacientes.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdFiltrar_Click()
Dim strWhere As String
Dim stra As String
Dim rsta As rdoResultset
Dim strpersona As String
Dim rstpersona As rdoResultset
Dim rstprod As rdoResultset
Dim strprod  As String
Dim dpto As String
Dim strDpto As String
Dim rstdpto As rdoResultset
Dim strdepartamento As String
Dim codigodepartamento As String
Dim strdoctor As String
Dim rstdoctor As rdoResultset
Dim codigodoctor As String
Dim doctor As String
Dim numcolegiado As String
Dim strpac As String
Dim rstpac As rdoResultset
Dim codigopaciente As String
Dim historia As String
Dim nombre As String
Dim priapel As String
Dim segapel As String
Dim rstprov As rdoResultset
Dim strprov As String
Dim codproveedor As String
Dim proveedor As String
Dim cama As String
Dim numrecetaoficial As String
Dim numrecetario As String
Dim linea As String
Dim entrada As String
Dim salida As String
Dim saldos As String
Dim Observaciones As String
Dim codigoestupefaciente As String
Dim estupefaciente As String
Dim rstestu As rdoResultset
Dim strestu As String


Screen.MousePointer = vbHourglass
cmdfiltrar.Enabled = False

If Option1(0).Value = True Then 'Historia
  If IsNumeric(Text1(1).Text) Then
    strpersona = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rstpersona = objApp.rdoConnect.OpenResultset(strpersona)
    strWhere = strWhere & " AND FR1300.CI21CODPERSONA=" & rstpersona.rdoColumns("CI21CODPERSONA").Value
    rstpersona.Close
    Set rstpersona = Nothing
  Else
    Grid1(0).RemoveAll
    cmdfiltrar.Enabled = True
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
Else
   If Option1(1).Value = True Then 'Estupefaciente
      If Trim(txtproducto.Text) <> "" Then
        strprod = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & "'" & txtproducto.Text & "'"
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If Not rstprod.EOF Then
          strWhere = strWhere & " AND FR1300.FR73CODPRODUCTO=" & rstprod.rdoColumns("FR73CODPRODUCTO").Value
        End If
        rstprod.Close
        Set rstprod = Nothing
      End If
    Else
      If Option1(2).Value = True Then 'Fechas
          'fecha inicio/fecha fin
          If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
            strWhere = strWhere & " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
            strWhere = strWhere & " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('31/12/9999','DD/MM/YYYY')"
          End If
          If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
            strWhere = strWhere & " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
                     "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
          End If
      End If
   End If
End If
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Grid1(0).RemoveAll
stra = "SELECT FR1300.*,GCFN06(FR1300.AD15CODCAMA) CAMA,FR7300.FR73CODINTFAR,FR7300.FR73DESPRODUCTO,"
stra = stra & "CI2200.CI22NUMHISTORIA"
stra = stra & " FROM FR1300,FR7300,CI2200 "
stra = stra & " WHERE FR1300.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
stra = stra & " AND FR1300.CI21CODPERSONA=CI2200.CI21CODPERSONA(+)" & strWhere
stra = stra & " AND FR1300.FR13INDESTUPEFACIENTE=-1"  'para que no se vean los diluyentes ni los productos_2 de las mezclas
stra = stra & " ORDER BY FR1300.FR13FECPETICION DESC"

Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  'cama
  If Not IsNull(rsta.rdoColumns("CAMA").Value) Then
    cama = rsta.rdoColumns("CAMA").Value
  Else
    cama = " "
  End If
  
  'Departamento solicitante
  If Not IsNull(rsta.rdoColumns("AD02CODDPTO").Value) Then
    codigodepartamento = rsta.rdoColumns("AD02CODDPTO").Value
    strDpto = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rsta.rdoColumns("AD02CODDPTO").Value
    Set rstdpto = objApp.rdoConnect.OpenResultset(strDpto)
    If Not IsNull(rstdpto.rdoColumns("AD02DESDPTO").Value) Then
      strdepartamento = rstdpto.rdoColumns("AD02DESDPTO").Value
    Else
      strdepartamento = " "
    End If
    rstdpto.Close
    Set rstdpto = Nothing
  Else
    codigodepartamento = " "
    strdepartamento = " "
  End If
  
  'Estupefaciente
  If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
    codigoestupefaciente = rsta.rdoColumns("FR73CODPRODUCTO").Value
    strestu = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO").Value
    Set rstestu = objApp.rdoConnect.OpenResultset(strestu)
    If Not IsNull(rstestu.rdoColumns("FR73DESPRODUCTO").Value) Then
      estupefaciente = rstestu.rdoColumns("FR73DESPRODUCTO").Value
    Else
      estupefaciente = " "
    End If
    rstestu.Close
    Set rstestu = Nothing
  Else
    codigoestupefaciente = " "
    estupefaciente = " "
  End If
  
  'M�dico
  If Not IsNull(rsta.rdoColumns("SG02COD").Value) Then
    codigodoctor = rsta.rdoColumns("SG02COD").Value
    strdoctor = "SELECT SG02APE1,SG02NUMCOLEGIADO FROM SG0200 WHERE SG02COD=" & "'" & rsta.rdoColumns("SG02COD").Value & "'"
    Set rstdoctor = objApp.rdoConnect.OpenResultset(strdoctor)
    If Not IsNull(rstdoctor.rdoColumns("SG02APE1").Value) Then
      doctor = rstdoctor.rdoColumns("SG02APE1").Value
    Else
      doctor = " "
    End If
    If Not IsNull(rstdoctor.rdoColumns("SG02NUMCOLEGIADO").Value) Then
      numcolegiado = rstdoctor.rdoColumns("SG02NUMCOLEGIADO").Value
    Else
      numcolegiado = " "
    End If
    rstdoctor.Close
    Set rstdoctor = Nothing
  Else
    codigodoctor = " "
    doctor = " "
    numcolegiado = " "
  End If
  
  'Paciente
  If Not IsNull(rsta.rdoColumns("CI21CODPERSONA").Value) Then
    codigopaciente = rsta.rdoColumns("CI21CODPERSONA").Value
    strpac = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value
    Set rstpac = objApp.rdoConnect.OpenResultset(strpac)
    If Not IsNull(rstpac.rdoColumns("CI22NUMHISTORIA").Value) Then
      historia = rstpac.rdoColumns("CI22NUMHISTORIA").Value
    Else
      historia = " "
    End If
    If Not IsNull(rstpac.rdoColumns("CI22NOMBRE").Value) Then
      nombre = rstpac.rdoColumns("CI22NOMBRE").Value
    Else
      nombre = " "
    End If
    If Not IsNull(rstpac.rdoColumns("CI22PRIAPEL").Value) Then
      priapel = rstpac.rdoColumns("CI22PRIAPEL").Value
    Else
      priapel = " "
    End If
    If Not IsNull(rstpac.rdoColumns("CI22SEGAPEL").Value) Then
      segapel = rstpac.rdoColumns("CI22SEGAPEL").Value
    Else
      segapel = " "
    End If
    rstpac.Close
    Set rstpac = Nothing
  Else
    codigopaciente = " "
    historia = " "
    nombre = " "
    priapel = " "
    segapel = " "
  End If
  
  'Proveedor
  If Not IsNull(rsta.rdoColumns("FR79CODPROVEEDOR").Value) Then
    codproveedor = rsta.rdoColumns("FR79CODPROVEEDOR").Value
    strprov = "SELECT FR79PROVEEDOR FROM FR7900 WHERE " & _
              "FR79CODPROVEEDOR=" & rsta.rdoColumns("FR79CODPROVEEDOR").Value
    Set rstprov = objApp.rdoConnect.OpenResultset(strprov)
    If Not IsNull(rstprov.rdoColumns("FR79PROVEEDOR").Value) Then
      proveedor = rstprov.rdoColumns("FR79PROVEEDOR").Value
    Else
      proveedor = " "
    End If
    rstprov.Close
    Set rstprov = Nothing
  Else
    codproveedor = " "
    proveedor = " "
  End If
  'N�mero del Recetario
  If Not IsNull(rsta.rdoColumns("FR13NUMRECETARIO").Value) Then
    numrecetario = rsta.rdoColumns("FR13NUMRECETARIO").Value
  Else
    numrecetario = " "
  End If
  'N�mero de Receta oficial
  If Not IsNull(rsta.rdoColumns("FR13NUMRECETAOFICIAL").Value) Then
    numrecetaoficial = rsta.rdoColumns("FR13NUMRECETAOFICIAL").Value
  Else
    numrecetaoficial = " "
  End If
  'L�nea
  If Not IsNull(rsta.rdoColumns("FR13NUMLINEA").Value) Then
    linea = rsta.rdoColumns("FR13NUMLINEA").Value
  Else
    linea = " "
  End If
  'Saldos
  If Not IsNull(rsta.rdoColumns("FR13SALDOS").Value) Then
    saldos = rsta.rdoColumns("FR13SALDOS").Value
  Else
    saldos = " "
  End If
  'Observaciones
  If Not IsNull(rsta.rdoColumns("FR13OBSERVACIONES").Value) Then
    Observaciones = rsta.rdoColumns("FR13OBSERVACIONES").Value
  Else
    Observaciones = " "
  End If
  'Entrada
  If Not IsNull(rsta.rdoColumns("FR13ENTRADA").Value) Then
    entrada = rsta.rdoColumns("FR13ENTRADA").Value
  Else
    entrada = " "
  End If
  'Salida
  If Not IsNull(rsta.rdoColumns("FR13SALIDA").Value) Then
    salida = rsta.rdoColumns("FR13SALIDA").Value
  Else
    salida = " "
  End If
    
  Grid1(0).AddItem 0 & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR13INDENTRADAMANUAL").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR13CODLIBROESTUPEF").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR13FECPETICION").Value & Chr(vbKeyTab) & _
                   numrecetario & Chr(vbKeyTab) & _
                   numrecetaoficial & Chr(vbKeyTab) & _
                   linea & Chr(vbKeyTab) & _
                   codigoestupefaciente & Chr(vbKeyTab) & _
                   estupefaciente & Chr(vbKeyTab) & _
                   entrada & Chr(vbKeyTab) & _
                   salida & Chr(vbKeyTab) & _
                   codproveedor & Chr(vbKeyTab) & proveedor & Chr(vbKeyTab) & _
                   codigodoctor & Chr(vbKeyTab) & _
                   doctor & Chr(vbKeyTab) & numcolegiado & Chr(vbKeyTab) & _
                   codigopaciente & Chr(vbKeyTab) & _
                   historia & Chr(vbKeyTab) & _
                   nombre & Chr(vbKeyTab) & priapel & Chr(vbKeyTab) & segapel & Chr(vbKeyTab) & _
                   cama & Chr(vbKeyTab) & codigodepartamento & Chr(vbKeyTab) & _
                   strdepartamento & Chr(vbKeyTab) & _
                   saldos & Chr(vbKeyTab) & _
                   Observaciones & Chr(vbKeyTab)
rsta.MoveNext
Wend

cmdfiltrar.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdSalir_Click()
  FrameBuscador.Visible = False
  GridBuscador.Visible = False
  GridBuscador.RemoveAll
End Sub

Private Sub cmdSalirBusEst_Click()
FrameBusEst.Visible = False
GridBusEst.Visible = False
GridBusEst.RemoveAll
txtproducto.SetFocus
End Sub

Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
End Sub


Private Sub Grid1_DblClick(Index As Integer)
Dim i As Integer
Dim rsta As rdoResultset
Dim stra As String
Dim numcolegiado

Screen.MousePointer = vbHourglass
'la fila debe tener la columna clicada que indica "Nuevo Registro"
If Grid1(0).Columns(0).Value = -1 Then
    'seg�n en qu� columna se est� se va a un buscador o a otro
    If Grid1(0).Col = 8 Then  'estupefacientes
      buscador = "Estupefacientes"
      FrameBuscador.Visible = True
      GridBuscador.Visible = True
      FrameBuscador.ZOrder (0)
      GridBuscador.ZOrder (0)
      GridBuscador.Columns.Add (0)
      GridBuscador.Columns.RemoveAll
      For i = 0 To 2
        Call GridBuscador.Columns.Add(i)
      Next i
      GridBuscador.Columns(0).Caption = "C�digo"
      GridBuscador.Columns(0).Locked = True
      GridBuscador.Columns(0).Visible = False
      GridBuscador.Columns(1).Caption = "Interno"
      GridBuscador.Columns(1).Locked = True
      GridBuscador.Columns(1).Width = 1200
      GridBuscador.Columns(2).Caption = "Descripci�n"
      GridBuscador.Columns(2).Locked = True
      GridBuscador.Columns(2).Width = 6000
      stra = "SELECT * FROM FR7300 WHERE FR73INDESTUPEFACI=-1 " & _
           " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) ORDER BY FR73DESPRODUCTO"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      While Not rsta.EOF
        GridBuscador.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                              rsta.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                              rsta.rdoColumns("FR73DESPRODUCTO").Value
      rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
    End If
    
    If Grid1(0).Col = 12 Then  'proveedores
      buscador = "Proveedores"
      FrameBuscador.Visible = True
      GridBuscador.Visible = True
      FrameBuscador.ZOrder (0)
      GridBuscador.ZOrder (0)
      GridBuscador.Columns.Add (0)
      GridBuscador.Columns.RemoveAll
      For i = 0 To 1
        Call GridBuscador.Columns.Add(i)
      Next i
      GridBuscador.Columns(0).Caption = "C�digo"
      GridBuscador.Columns(0).Locked = True
      GridBuscador.Columns(0).Visible = False
      GridBuscador.Columns(1).Caption = "Proveedor"
      GridBuscador.Columns(1).Locked = True
      GridBuscador.Columns(1).Width = 6500
      stra = "SELECT * FROM FR7900 ORDER BY FR79PROVEEDOR"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      While Not rsta.EOF
        GridBuscador.AddItem rsta.rdoColumns("FR79CODPROVEEDOR").Value & Chr(vbKeyTab) & _
                              rsta.rdoColumns("FR79PROVEEDOR").Value
      rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
    End If
    
    If Grid1(0).Col = 14 Then  'm�dicos
      buscador = "M�dicos"
      FrameBuscador.Visible = True
      GridBuscador.Visible = True
      FrameBuscador.ZOrder (0)
      GridBuscador.ZOrder (0)
      GridBuscador.Columns.Add (0)
      GridBuscador.Columns.RemoveAll
      For i = 0 To 1
        Call GridBuscador.Columns.Add(i)
      Next i
      GridBuscador.Columns(0).Caption = "C�digo"
      GridBuscador.Columns(0).Locked = True
      GridBuscador.Columns(0).Visible = True
      GridBuscador.Columns(0).Width = 1000
      GridBuscador.Columns(1).Caption = "Dr."
      GridBuscador.Columns(1).Locked = True
      GridBuscador.Columns(1).Width = 3000
      GridBuscador.Columns(2).Caption = "N�Colegiado"
      GridBuscador.Columns(2).Locked = True
      GridBuscador.Columns(2).Width = 1100
      stra = "SELECT * FROM SG0200 ORDER BY SG02APE1"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      While Not rsta.EOF
        If IsNull(rsta.rdoColumns("SG02NUMCOLEGIADO").Value) Then
          numcolegiado = " "
        Else
          numcolegiado = rsta.rdoColumns("SG02NUMCOLEGIADO").Value
        End If
        GridBuscador.AddItem rsta.rdoColumns("SG02COD").Value & Chr(vbKeyTab) & _
                              rsta.rdoColumns("SG02APE1").Value & Chr(vbKeyTab) & numcolegiado
      rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
    End If
    
    If Grid1(0).Col = 23 Then  'servicios
      buscador = "Servicios"
      FrameBuscador.Visible = True
      GridBuscador.Visible = True
      FrameBuscador.ZOrder (0)
      GridBuscador.ZOrder (0)
      GridBuscador.Columns.Add (0)
      GridBuscador.Columns.RemoveAll
      For i = 0 To 1
        Call GridBuscador.Columns.Add(i)
      Next i
      GridBuscador.Columns(0).Caption = "C�digo"
      GridBuscador.Columns(0).Locked = True
      GridBuscador.Columns(0).Visible = True
      GridBuscador.Columns(0).Width = 1100
      GridBuscador.Columns(1).Caption = "Servicio"
      GridBuscador.Columns(1).Locked = True
      GridBuscador.Columns(1).Width = 4000
      stra = "SELECT * FROM AD0200 WHERE " & _
             " TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(AD02FECINICIO,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(AD02FECFIN,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) ORDER BY AD02DESDPTO"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      While Not rsta.EOF
        GridBuscador.AddItem rsta.rdoColumns("AD02CODDPTO").Value & Chr(vbKeyTab) & _
                            rsta.rdoColumns("AD02DESDPTO").Value
      rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
    End If
End If
Screen.MousePointer = vbDefault
End Sub

Private Sub Grid1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim i As Integer
'para que no deje escribir en Estupefaciente,Proveedor,M�dico,N�Colegiado,
'Historia,Nombre,Apellido1,Apellido2,Servicio,Saldos,Cama
If Grid1(0).Col = 8 Or Grid1(0).Col = 12 Or Grid1(0).Col = 14 Or Grid1(0).Col = 15 Or _
   Grid1(0).Col = 17 Or Grid1(0).Col = 18 Or Grid1(0).Col = 19 Or Grid1(0).Col = 20 Or _
   Grid1(0).Col = 23 Or Grid1(0).Col = 24 Or Grid1(0).Col = 21 Then
      For i = 0 To 25
        Grid1(0).Columns(i).Locked = True
      Next i
Else
      For i = 0 To 25
        Grid1(0).Columns(i).Locked = False
      Next i
End If
'entrada,salida
If Grid1(0).Col = 9 Or Grid1(0).Col = 10 Then
 Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
 End Select
End If
'n� de receta, n� de l�nea
If Grid1(0).Col = 5 Or Grid1(0).Col = 6 Then
 Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case 8
    Case Else
      KeyAscii = 0
 End Select
End If

End Sub

Private Sub Grid1_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim i As Integer
If Grid1(0).Columns(0).Value = 0 Then
    For i = 0 To 25
      Grid1(0).Columns(i).Locked = True
    Next i
Else
    For i = 0 To 25
      Grid1(0).Columns(i).Locked = False
    Next i
End If
End Sub



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean

Call objWinInfo.FormPrinterDialog(True, "")
Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
intReport = objPrinter.Selected
If intReport > 0 Then
  Select Case intReport
    Case 1
      Call Imprimir("FR1392.RPT", 0)
  End Select
End If
Set objPrinter = Nothing
End Sub

Private Sub Text1_Change(Index As Integer)
If Index = 1 Then
  Grid1(0).RemoveAll
  Text1(2).Text = ""
End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If IsNumeric(Text1(1).Text) Then
    stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
      strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
    End If
    If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
      strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
    End If
    If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
      strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
    End If
    Text1(2).Text = strpaciente
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub txtproducto_Change()
  Grid1(0).RemoveAll
  txtdescproducto.Text = ""
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If KeyAscii = 13 Then
  If IsNumeric(Text1(1).Text) Then
      stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
        strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
      End If
      Text1(2).Text = strpaciente
      rsta.Close
      Set rsta = Nothing
  End If
End If
End Sub



Private Sub txtproducto_KeyPress(KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strestupefaciente As String

If KeyAscii = 13 Then
  If Trim(txtproducto.Text) <> "" Then
      stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & txtproducto.Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("FR73DESPRODUCTO").Value) Then
          strestupefaciente = rsta.rdoColumns("FR73DESPRODUCTO").Value
        End If
        txtdescproducto = strestupefaciente
      End If
      rsta.Close
      Set rsta = Nothing
  End If
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim stra As String
    Dim rsta As rdoResultset
    Dim strpaciente As String
  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  '  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
   ' intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    'intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
   ' Call objWinInfo.WinDeRegister
   ' Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim i As Integer
Dim rsta As rdoResultset
Dim stra As String
Dim rstfec As rdoResultset
Dim strfec As String
Dim rstestu As rdoResultset
Dim strmensaje As String
Dim strmensajeerrores As String
Dim codigoestupefaciente As String
Dim codigoproveedor As String
Dim codigomedico As String
Dim codigoservicio As String
Dim codigopaciente As String
Dim strinsert As String
Dim fecha As String
Dim numrecetario As String
Dim numrecetaoficial As String
Dim linea As String
Dim entrada As String
Dim salida As String
Dim respuesta As Integer
Dim salir As Boolean
Dim Observaciones As String

Select Case btnButton.Index
  Case 2:
          Grid1(0).Update
          tlbToolbar1.Buttons(4).Enabled = True
          'se hace un nuevo registro
          Grid1(0).AddNew
          Grid1(0).Columns("NuevoRegistro").Value = -1 'registro nuevo
          Grid1(0).Columns("EntradaManual?").Value = -1
          Grid1(0).Columns("Estupefaciente?").Value = -1
          '+++++++++++++++++++++++++++++++++++
          stra = "SELECT * FROM SG0200 WHERE SG02COD=" & _
                       "'" & objsecurity.strUser & "'"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          Grid1(0).Columns("C�dM�dico").Value = objsecurity.strUser
          If Not rsta.EOF Then
            If Not IsNull(rsta.rdoColumns("SG02APE1").Value) Then
                Grid1(0).Columns("M�dico").Value = rsta.rdoColumns("SG02APE1").Value
            End If
            If Not IsNull(rsta.rdoColumns("SG02NUMCOLEGIADO").Value) Then
                Grid1(0).Columns("N�Colegiado").Value = rsta.rdoColumns("SG02NUMCOLEGIADO").Value
            End If
          End If
          stra = "SELECT AD0300.AD02CODDPTO,AD0200.AD02DESDPTO " & _
                       " FROM AD0300,AD0200 WHERE AD0300.SG02COD=" & _
                       "'" & objsecurity.strUser & "'" & _
                       " AND AD0300.AD02CODDPTO=AD0200.AD02CODDPTO"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Grid1(0).Columns("C�dServicio").Value = rsta.rdoColumns("AD02CODDPTO").Value
            Grid1(0).Columns("Servicio Solicitante").Value = rsta.rdoColumns("AD02DESDPTO").Value
          End If
          rsta.Close
          Set rsta = Nothing
          '++++++++++++++++++++++++++
          Grid1(0).Col = 4
          stra = "SELECT FR13CODLIBROESTUPEF_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          Grid1(0).Columns("CodigoLibro").Value = rsta.rdoColumns(0).Value
          strfec = "SELECT SYSDATE FROM DUAL"
          Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
          Grid1(0).Columns("Fecha").Value = rstfec.rdoColumns(0).Value
  Case 4:
          LineaNoGuardada = False
          'se recorre el grid y se hace insert de los registros nuevos
          Grid1(0).MoveFirst
          For i = 0 To Grid1(0).Rows - 1
            If Grid1(0).Columns(0).Value = -1 Then
              '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              strmensaje = ""
              strmensajeerrores = ""
              'estupefaciente
              If Trim(Grid1(0).Columns("C�dEstu").Value) = "" Then
                strmensaje = strmensaje & "El campo Estupefaciente es obligatorio" & Chr(13)
              Else
                stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                        Trim(Grid1(0).Columns("C�dEstu").Value)
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta.EOF Then
                  strmensajeerrores = strmensajeerrores & "Estupefaciente err�neo" & Chr(13)
                Else
                  codigoestupefaciente = rsta.rdoColumns("FR73CODPRODUCTO").Value
                End If
                rsta.Close
                Set rsta = Nothing
              End If
              'Proveedor
              If Trim(Grid1(0).Columns("CodProv").Value) = "" Then
                codigoproveedor = "NULL"
              Else
                stra = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & _
                        Trim(Grid1(0).Columns("CodProv").Value)
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta.EOF Then
                  strmensajeerrores = strmensajeerrores & "Proveedor err�neo" & Chr(13)
                Else
                  codigoproveedor = rsta.rdoColumns("FR79CODPROVEEDOR").Value
                End If
                rsta.Close
                Set rsta = Nothing
              End If
              'Paciente
              If Trim(Grid1(0).Columns("C�dPaciente").Value) = "" Then
                codigopaciente = "NULL"
              Else
                stra = "SELECT * FROM CI2200 WHERE CI21CODPERSONA=" & _
                        Trim(Grid1(0).Columns("C�dPaciente").Value)
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta.EOF Then
                  strmensajeerrores = strmensajeerrores & "Paciente err�neo" & Chr(13)
                Else
                  codigopaciente = rsta.rdoColumns("CI21CODPERSONA").Value
                End If
                rsta.Close
                Set rsta = Nothing
              End If
              'M�dico
              If Trim(Grid1(0).Columns("C�dM�dico").Value) = "" Then
                codigomedico = "NULL"
              Else
                stra = "SELECT * FROM SG0200 WHERE SG02COD=" & _
                       "'" & Trim(Grid1(0).Columns("C�dM�dico").Value) & "'"
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta.EOF Then
                  strmensajeerrores = strmensajeerrores & "M�dico err�neo" & Chr(13)
                Else
                  codigomedico = rsta.rdoColumns("SG02COD").Value
                End If
                rsta.Close
                Set rsta = Nothing
              End If
              'Servicio
              If Trim(Grid1(0).Columns("C�dServicio").Value) = "" Then
                codigoservicio = "NULL"
              Else
                stra = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & _
                        Trim(Grid1(0).Columns("C�dServicio").Value)
                Set rsta = objApp.rdoConnect.OpenResultset(stra)
                If rsta.EOF Then
                  strmensajeerrores = strmensajeerrores & "Servicio err�neo" & Chr(13)
                Else
                 codigoservicio = rsta.rdoColumns("AD02CODDPTO").Value
                End If
                rsta.Close
                Set rsta = Nothing
              End If
              'fecha
              If Trim(Grid1(0).Columns("Fecha").Value) = "" Then
                strmensaje = strmensaje & "El campo Fecha es obligatorio" & Chr(13)
              Else
                If IsDate(Grid1(0).Columns("Fecha").Value) Then
                  fecha = Grid1(0).Columns("Fecha").Value
                Else
                  strmensajeerrores = strmensajeerrores & "Fecha err�nea" & Chr(13)
                End If
              End If
              'N�mero del Recetario
              If Trim(Grid1(0).Columns("N�Recetario").Value) = "" Then
                  numrecetario = "NULL"
              Else
                If Len(Grid1(0).Columns("N�Recetario").Value) > 10 Then
                  strmensajeerrores = strmensajeerrores & "N�Recetario s�lo admite 10 caracteres" & Chr(13)
                Else
                  numrecetario = Grid1(0).Columns("N�Recetario").Value
                End If
              End If
              'N�mero Receta
              If Trim(Grid1(0).Columns("N�Receta").Value) = "" Then
                  numrecetaoficial = "NULL"
              Else
                If IsNumeric(Grid1(0).Columns("N�Receta").Value) Then
                 If Len(Grid1(0).Columns("N�Receta").Value) > 9 Then
                  strmensajeerrores = strmensajeerrores & "N�Receta s�lo admite 9 d�gitos" & Chr(13)
                Else
                  numrecetaoficial = Grid1(0).Columns("N�Receta").Value
                End If
                Else
                  strmensajeerrores = strmensajeerrores & "N� de Receta no num�rico" & Chr(13)
                End If
              End If
              'L�nea
              If Trim(Grid1(0).Columns("L�nea").Value) = "" Then
                  linea = "NULL"
              Else
                If IsNumeric(Grid1(0).Columns("L�nea").Value) Then
                 If Len(Grid1(0).Columns("L�nea").Value) > 3 Then
                  strmensajeerrores = strmensajeerrores & "L�nea s�lo admite 3 d�gitos" & Chr(13)
                 Else
                  linea = Grid1(0).Columns("L�nea").Value
                 End If
                Else
                  strmensajeerrores = strmensajeerrores & "Linea no num�rica" & Chr(13)
                End If
              End If
              'Entrada
              If Trim(Grid1(0).Columns("Entrada").Value) = "" Then
                  entrada = "NULL"
              Else
                If IsNumeric(Grid1(0).Columns("Entrada").Value) Then
                  entrada = objGen.ReplaceStr(Grid1(0).Columns("Entrada").Value, ",", ".", 1)
                Else
                  strmensajeerrores = strmensajeerrores & "Entrada err�nea" & Chr(13)
                End If
              End If
              'Salida
              If Trim(Grid1(0).Columns("Salida").Value) = "" Then
                  salida = "NULL"
              Else
                If IsNumeric(Grid1(0).Columns("Salida").Value) Then
                  salida = objGen.ReplaceStr(Grid1(0).Columns("Salida").Value, ",", ".", 1)
                Else
                  strmensajeerrores = strmensajeerrores & "Salida err�nea" & Chr(13)
                End If
              End If
              'Observaciones
              If Trim(Grid1(0).Columns("Observaciones").Value) = "" Then
                  Observaciones = "NULL"
              Else
                If Len(Grid1(0).Columns("Observaciones").Value) > 250 Then
                 strmensajeerrores = strmensajeerrores & "Observaciones s�lo admite 250 caracteres" & Chr(13)
                Else
                  Observaciones = objGen.ReplaceStr(Grid1(0).Columns("Observaciones").Value, ",", ".", 1)
                End If
              End If
              
              strmensajeerrores = strmensajeerrores & strmensaje
              If strmensajeerrores <> "" Then
                Call MsgBox(strmensajeerrores, vbInformation, "Aviso")
                LineaNoGuardada = True
                tlbToolbar1.Buttons(4).Enabled = True
                Exit Sub
              End If
              
              strinsert = "INSERT INTO FR1300(FR13CODLIBROESTUPEF,FR73CODPRODUCTO,FR13FECPETICION,"
              strinsert = strinsert & "FR13NUMRECETARIO,FR13NUMRECETAOFICIAL,FR13NUMLINEA,"
              strinsert = strinsert & "FR13ENTRADA,FR13SALIDA,FR79CODPROVEEDOR,"
              strinsert = strinsert & "SG02COD,CI21CODPERSONA,AD02CODDPTO,"
              strinsert = strinsert & "AD15CODCAMA,FR13SALDOS,FR13OBSERVACIONES,FR13INDENTRADAMANUAL,"
              strinsert = strinsert & "FR13INDESTUPEFACIENTE)"
              strinsert = strinsert & " VALUES ("
              strinsert = strinsert & Grid1(0).Columns("CodigoLibro").Value & ","
              strinsert = strinsert & codigoestupefaciente & ","
              'strinsert = strinsert & "TO_DATE('" & fecha & "','DD/MM/YYYY')" & ","
              strinsert = strinsert & "TO_DATE('" & fecha & "','DD/MM/YYYY HH24:MI:SS')" & ","
              strinsert = strinsert & "'" & numrecetario & "'" & ","
              strinsert = strinsert & numrecetaoficial & ","
              strinsert = strinsert & linea & ","
              strinsert = strinsert & entrada & ","
              strinsert = strinsert & salida & ","
              strinsert = strinsert & codigoproveedor & ","
              If codigomedico = "NULL" Then
                  strinsert = strinsert & codigomedico & ","
              Else
                  strinsert = strinsert & "'" & codigomedico & "'" & ","
              End If
              strinsert = strinsert & codigopaciente & ","
              strinsert = strinsert & codigoservicio & ","
              strinsert = strinsert & "NULL,"
              strinsert = strinsert & "NULL,"
              If Observaciones = "NULL" Then
                  strinsert = strinsert & Observaciones & ","
              Else
                  strinsert = strinsert & "'" & Observaciones & "'" & ","
              End If
              strinsert = strinsert & "-1,-1" & ")"
              
              objApp.rdoConnect.Execute strinsert, 64
              objApp.rdoConnect.Execute "Commit", 64
              tlbToolbar1.Buttons(4).Enabled = False
              '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              'el registro ya est� guardado, ya no se puede modificar
              Grid1(0).Columns("NuevoRegistro").Value = 0
              Call Actualizar_Saldos
            End If
            Grid1(0).MoveNext
          Next i
          Call cmdFiltrar_Click
  Case 6:
          Call Imprimir("FR1392.RPT", 0)
  Case 21:
          Call Grid1(0).MoveFirst
  Case 22:
          Call Grid1(0).MovePrevious
  Case 23:
          Call Grid1(0).MoveNext
  Case 24:
          Call Grid1(0).MoveLast
  Case 26:
          Call Inicializar_Grid
          Call cmdFiltrar_Click
  Case 30:
          LineaNoGuardada = False
          'si se ha hecho nuevo se comprueba si se han guardado los cambios
          Grid1(0).MoveFirst
          respuesta = 0
          For i = 0 To Grid1(0).Rows - 1
              If Grid1(0).Columns("NuevoRegistro").Value = -1 Then
                    respuesta = MsgBox("            �Desea salvar los cambios realizados?            " & Chr(13), _
                                vbYesNoCancel, "Aviso")
                    If respuesta = 2 Then 'cancelar
                    salir = False
                      Exit For
                    End If
                    If respuesta = 7 Then 'no
                      salir = True
                      Exit For
                    End If
                    If respuesta = 6 Then 'si
                      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
                      If LineaNoGuardada = True Then
                        salir = False
                        Exit For
                      Else
                        salir = True
                        Exit For
                      End If
                    End If
              Else
'                    Unload Me
              End If
          Grid1(0).MoveNext
          Next i
          If salir = True Or respuesta = 0 Then
            Unload Me
          End If
          LineaNoGuardada = False
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Select Case intIndex
  Case 40:
          Call Grid1(0).MoveFirst
  Case 50:
          Call Grid1(0).MovePrevious
  Case 60:
          Call Grid1(0).MoveNext
  Case 70:
          Call Grid1(0).MoveLast
End Select
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
          Call Inicializar_Grid
          Call Grid1(0).Refresh
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    'Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub Inicializar_Toolbar()
   'tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   'tlbToolbar1.Buttons.Item(4).Enabled = False
   'tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub

Private Sub Inicializar_Grid()
Dim i As Integer

Grid1(0).Columns("NuevoRegistro").Visible = False
Grid1(0).Columns("EntradaManual?").Visible = False
Grid1(0).Columns("CodigoLibro").Visible = False
Grid1(0).Columns("C�dPaciente").Visible = False
Grid1(0).Columns("CodProv").Visible = False
Grid1(0).Columns("C�dEstu").Visible = False
Grid1(0).Columns("C�dM�dico").Visible = False
Grid1(0).Columns("C�dServicio").Visible = False
Grid1(0).Columns("Estupefaciente?").Visible = False

'Grid1(0).Columns("Fecha").Width = 1050
Grid1(0).Columns("Fecha").Width = 1700
Grid1(0).Columns("N�Recetario").Width = 1000
Grid1(0).Columns("N�Receta").Width = 1000
Grid1(0).Columns("L�nea").Width = 600
Grid1(0).Columns("Estupefaciente").Width = 3800
Grid1(0).Columns("Entrada").Width = 900
Grid1(0).Columns("Salida").Width = 900
Grid1(0).Columns("M�dico").Width = 1000
Grid1(0).Columns("N�Colegiado").Width = 1100
Grid1(0).Columns("Historia").Width = 800
Grid1(0).Columns("NombrePac").Width = 1500
Grid1(0).Columns("PriApelPac").Width = 1500
Grid1(0).Columns("SegApelPac").Width = 1500
Grid1(0).Columns("Cama").Width = 700
Grid1(0).Columns("Servicio Solicitante").Width = 2000
Grid1(0).Columns("Saldos").Width = 1000
Grid1(0).Columns("Observaciones").Width = 10000

End Sub

Private Sub txtproducto_LostFocus()
Dim stra As String
Dim rsta As rdoResultset
Dim strestupefaciente As String

If Trim(txtproducto.Text) <> "" Then
    stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & txtproducto.Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
      If Not IsNull(rsta.rdoColumns("FR73DESPRODUCTO").Value) Then
        strestupefaciente = rsta.rdoColumns("FR73DESPRODUCTO").Value
      End If
      txtdescproducto = strestupefaciente
    End If
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub Imprimir(strListado As String, intDes As Integer)
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  Dim strpersona As String
  Dim rstpersona As rdoResultset
  Dim rstprod As rdoResultset
  Dim strprod  As String
  Dim dia_ini
  Dim mes_ini
  Dim a�o_ini
  Dim dia_fin
  Dim mes_fin
  Dim a�o_fin
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  'se crea la where
  strWhere = strWhere & "-1=-1"
  If Option1(0).Value = True Then 'Historia
    If IsNumeric(Text1(1).Text) Then
      strpersona = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
      Set rstpersona = objApp.rdoConnect.OpenResultset(strpersona)
      strWhere = strWhere & " AND {FR1300.CI21CODPERSONA}=" & rstpersona.rdoColumns("CI21CODPERSONA").Value
      rstpersona.Close
      Set rstpersona = Nothing
    End If
  Else
     If Option1(1).Value = True Then 'Estupefaciente
        If Trim(txtproducto.Text) <> "" Then
          strprod = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & "'" & txtproducto.Text & "'"
          Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
          If Not rstprod.EOF Then
            strWhere = strWhere & " AND {FR1300.FR73CODPRODUCTO}=" & rstprod.rdoColumns("FR73CODPRODUCTO").Value
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      Else
        If Option1(2).Value = True Then 'Fechas
            'fecha inicio/fecha fin
            If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
                dia_ini = Day(dtcinicio.Date)
                mes_ini = Month(dtcinicio.Date)
                a�o_ini = Year(dtcinicio.Date)
                dia_fin = Day(dtcfin.Date)
                mes_fin = Month(dtcfin.Date)
                a�o_fin = Year(dtcfin.Date)
                strWhere = strWhere & " AND {FR1300.FR13FECPETICION} IN "
                strWhere = strWhere & "DATE(" & a�o_ini & "," & mes_ini & "," & dia_ini & ")"
                strWhere = strWhere & " TO "
                strWhere = strWhere & "DATE(" & a�o_fin & "," & mes_fin & "," & dia_fin & ")"
            End If
            If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
                dia_ini = Day(dtcinicio.Date)
                mes_ini = Month(dtcinicio.Date)
                a�o_ini = Year(dtcinicio.Date)
                dia_fin = 31
                mes_fin = 12
                a�o_fin = 9999
                strWhere = strWhere & " AND {FR1300.FR13FECPETICION} IN "
                strWhere = strWhere & "DATE(" & a�o_ini & "," & mes_ini & "," & dia_ini & ")"
                strWhere = strWhere & " TO "
                strWhere = strWhere & "DATE(" & a�o_fin & "," & mes_fin & "," & dia_fin & ")"
            End If
            If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
                dia_ini = 1
                mes_ini = 1
                a�o_ini = 1900
                dia_fin = Day(dtcfin.Date)
                mes_fin = Month(dtcfin.Date)
                a�o_fin = Year(dtcfin.Date)
                strWhere = strWhere & " AND {FR1300.FR13FECPETICION} IN "
                strWhere = strWhere & "DATE(" & a�o_ini & "," & mes_ini & "," & dia_ini & ")"
                strWhere = strWhere & " TO "
                strWhere = strWhere & "DATE(" & a�o_fin & "," & mes_fin & "," & dia_fin & ")"
            End If
        End If
     End If
  End If
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1

Err_imp1:
  
End Sub

Private Sub Actualizar_Saldos()
Dim rsta As rdoResultset
Dim stra As String
Dim fecha_max As Date
Dim codigo_libro As Variant
Dim saldo As Variant
Dim entrada
Dim salida
Dim nuevo_saldo
Dim Incremento
Dim strupdate As String
Dim saldo_actual

'se coge la fecha justo anterior para tomarla de referencia para calcular el saldo
stra = "SELECT FR13CODLIBROESTUPEF,FR13FECPETICION,FR13SALDOS,FR73CODPRODUCTO FROM FR1300 WHERE"
stra = stra & " TO_CHAR(FR13FECPETICION,'DD/MM/YYYY HH24:MI:SS')="
stra = stra & " (SELECT TO_CHAR(MAX(FR13FECPETICION),'DD/MM/YYYY HH24:MI:SS')"
stra = stra & " FROM FR1300 WHERE"
stra = stra & " TO_DATE(TO_CHAR(FR13FECPETICION,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') <"
stra = stra & " TO_DATE('" & Grid1(0).Columns("fecha").Value & "','DD/MM/YYYY HH24:MI:SS')"
stra = stra & " AND FR13INDESTUPEFACIENTE=-1 AND FR73CODPRODUCTO=" & Grid1(0).Columns("C�dEstu").Value
stra = stra & " AND FR13CODLIBROESTUPEF<>" & Grid1(0).Columns("CodigoLibro").Value & ")"
stra = stra & " AND FR13INDESTUPEFACIENTE=-1 AND FR73CODPRODUCTO=" & Grid1(0).Columns("C�dEstu").Value
stra = stra & " AND FR13CODLIBROESTUPEF<>" & Grid1(0).Columns("CodigoLibro").Value
Set rsta = objApp.rdoConnect.OpenResultset(stra)

If Trim(Grid1(0).Columns("Entrada").Value) = "" And Trim(Grid1(0).Columns("Salida").Value) = "" Then
  Incremento = 0
End If
If Trim(Grid1(0).Columns("Entrada").Value) <> "" And Trim(Grid1(0).Columns("Salida").Value) <> "" Then
  Incremento = CCur(Grid1(0).Columns("Entrada").Value) - CCur(Grid1(0).Columns("Salida").Value)
End If
If Trim(Grid1(0).Columns("Entrada").Value) = "" And Trim(Grid1(0).Columns("Salida").Value) <> "" Then
  Incremento = -CCur(Grid1(0).Columns("Salida").Value)
End If
If Trim(Grid1(0).Columns("Entrada").Value) <> "" And Trim(Grid1(0).Columns("Salida").Value) = "" Then
  Incremento = CCur(Grid1(0).Columns("Entrada").Value)
End If
If Not rsta.EOF Then
  If Not IsNull(rsta.rdoColumns("FR13SALDOS").Value) Then
    nuevo_saldo = rsta.rdoColumns("FR13SALDOS").Value + Incremento
  Else
    nuevo_saldo = 0 + Incremento
  End If
Else
  nuevo_saldo = 0 + Incremento
End If
rsta.Close
Set rsta = Nothing

strupdate = "UPDATE FR1300 SET FR13SALDOS=" & objGen.ReplaceStr(nuevo_saldo, ",", ".", 1)
strupdate = strupdate & " WHERE FR13CODLIBROESTUPEF=" & Grid1(0).Columns("CodigoLibro").Value
objApp.rdoConnect.Execute strupdate, 64
objApp.rdoConnect.Execute "Commit", 64

'se cogen los registros con fecha mayor al nuevo insertado para modificarles el saldo
stra = "SELECT * FROM FR1300 "
stra = stra & "WHERE TO_DATE(TO_CHAR(FR13FECPETICION,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') "
stra = stra & ">"
stra = stra & " TO_DATE('" & Grid1(0).Columns("fecha").Value & "','DD/MM/YYYY HH24:MI:SS')"
stra = stra & " AND FR13INDESTUPEFACIENTE=-1"
stra = stra & " AND FR73CODPRODUCTO=" & Grid1(0).Columns("C�dEstu").Value
stra = stra & " ORDER BY FR13CODLIBROESTUPEF ASC"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  If Not rsta.rdoColumns("FR13SALDOS").Value Then
    saldo_actual = rsta.rdoColumns("FR13SALDOS").Value
  Else
    saldo_actual = 0
  End If
  strupdate = "UPDATE FR1300 SET FR13SALDOS=" & objGen.ReplaceStr(saldo_actual + Incremento, ",", ".", 1)
  strupdate = strupdate & " WHERE FR13CODLIBROESTUPEF=" & rsta.rdoColumns("FR13CODLIBROESTUPEF").Value
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
End Sub
