VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmEstuVerPedidoFarmacia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. PEDIR SERVICIO FARMACIA. Visualizar Pedido Farmacia. Estupefacientes"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   10320
      TabIndex        =   34
      Top             =   6960
      Width           =   1575
      Begin VB.CommandButton cmdAnular 
         Caption         =   "Anular"
         Height          =   375
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   10320
      TabIndex        =   32
      Top             =   5880
      Width           =   1575
      Begin VB.CommandButton cmdValidar 
         Caption         =   "Validar"
         Height          =   375
         Left            =   120
         TabIndex        =   33
         Top             =   285
         Width           =   1335
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   10320
      TabIndex        =   30
      Top             =   3600
      Width           =   1575
      Begin VB.CommandButton cmdEstupefacientes 
         Caption         =   "Estupefacientes"
         Height          =   375
         Left            =   120
         TabIndex        =   31
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   10320
      TabIndex        =   28
      Top             =   4680
      Width           =   1575
      Begin VB.CommandButton cmdFirmarEnviar 
         Caption         =   "Firmar y Enviar"
         Height          =   375
         Left            =   120
         TabIndex        =   29
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n Estupefacientes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   11460
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   0
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(16)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(19)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(23)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(46)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(6)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(5)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(4)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(7)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(8)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "chkCheck1(0)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtcolegiado"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(61)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(62)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "chkCheck1(15)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR55INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   5280
            TabIndex        =   38
            Tag             =   "Inter�s Cient�fico?"
            Top             =   1800
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   62
            Left            =   7800
            TabIndex        =   37
            Tag             =   "Dpto.Cargo"
            Top             =   1800
            Width           =   2685
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO_CRG"
            Height          =   330
            Index           =   61
            Left            =   7320
            TabIndex        =   36
            Tag             =   "C�d.Dpto.Cargo"
            Top             =   1800
            Width           =   360
         End
         Begin VB.TextBox txtcolegiado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Left            =   3840
            Locked          =   -1  'True
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   750
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Petici�n de Estupefacientes"
            DataField       =   "FR55INDESTUPEFACIENTE"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   7800
            TabIndex        =   25
            Tag             =   "Alergico?"
            Top             =   360
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   8
            Left            =   7080
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Descripci�n Secci�n"
            Top             =   1080
            Width           =   2775
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD41CODSECCION"
            Height          =   330
            Index           =   7
            Left            =   5640
            TabIndex        =   21
            Tag             =   "C�digo Secci�n"
            Top             =   1080
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   1320
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   2175
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   5
            Left            =   120
            TabIndex        =   4
            Tag             =   "C�digo Servicio"
            Top             =   1080
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_FIR"
            Height          =   330
            Index           =   3
            Left            =   120
            TabIndex        =   6
            Tag             =   "C�d. Peticionario"
            Top             =   1800
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   1320
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   1080
            Width           =   3975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�d. Necesidad"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   2040
            TabIndex        =   1
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   2520
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1905
            Index           =   2
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   240
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   3360
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   5640
            TabIndex        =   3
            Tag             =   "Fecha Petici�n"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Departamento de Cargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   46
            Left            =   7320
            TabIndex        =   39
            Top             =   1560
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Colegiado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   3840
            TabIndex        =   27
            Top             =   1560
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Secci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   7080
            TabIndex        =   24
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Secci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   5640
            TabIndex        =   23
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "M�dico que firma"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   20
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   19
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   5640
            TabIndex        =   18
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   17
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   2040
            TabIndex        =   16
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Necesidades de la Unidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   2
      Left            =   120
      TabIndex        =   13
      Top             =   3360
      Width           =   10095
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4095
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   9810
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Alerta"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0108.frx":0000
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   17304
         _ExtentY        =   7223
         _StockProps     =   79
         Caption         =   "NECESIDADES DE LA UNIDAD"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmEstuVerPedidoFarmacia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEstuVerPedidoFarmacia(FR0108.FRM)                         *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: ABRIL DEL 2000                                                *
'* DESCRIPCION: Visualizar Pedido de Planta para Estupefacientes        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnError As Boolean

Private Sub cmdAnular_Click()
Dim intMsg As Integer
Dim strupdate As String
Dim strdelete As String
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
    
On Error GoTo err
Screen.MousePointer = vbHourglass
Me.Enabled = False
cmdAnular.Enabled = False

If IsNumeric(txtText1(1).Text) And _
      (txtText1(1).Text = 1 Or txtText1(1).Text = 3) Then 'redactada o enviada
    If IsNumeric(txtText1(0).Text) Then
        mensaje = MsgBox("�Est� seguro que desea Anular la Petici�n?", vbYesNo, "Aviso")
        If mensaje = 6 Then 'SI
           strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=8"
           strupdate = strupdate & " WHERE FR55CODNECESUNID=" & txtText1(0).Text
           objApp.rdoConnect.Execute strupdate, 64
           objWinInfo.DataRefresh
           Call MsgBox("Petici�n Anulada", vbInformation, "Aviso")
        End If
    End If
    cmdAnular.Enabled = True
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    Exit Sub
Else
  If IsNumeric(txtText1(0).Text) Then  'dispensada parcial, validada
      intMsg = MsgBox("�Est� seguro que desea Anular la Petici�n?", vbQuestion + vbYesNo, "Aviso")
      If intMsg = vbYes Then
          strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=8" & _
                    " WHERE FR55CODNECESUNID=" & txtText1(0).Text
          objApp.rdoConnect.Execute strupdate, 64
          objWinInfo.DataRefresh
          If txtText1(1).Text = 4 Then
              'se clican las l�neas del libro de esa petici�n como anuladas
              strupdate = "UPDATE FR1300 SET FR13INDPETICANULADA=-1 WHERE"
              strupdate = " FR13NUMRECETAOFICIAL=" & txtText1(0).Text
              strupdate = strupdate & " AND FR13INDPRN=0"
              strupdate = strupdate & " AND (FR13INDDISPENSADO=0 OR FR13INDDISPENSADO IS NULL)"
              strupdate = strupdate & " AND FR13INDENTRADAMANUAL=0"
              objApp.rdoConnect.Execute strupdate, 64
              Call Rellenar_Libro_Estupefacientes_Petici�n_Anulada
              Call MsgBox("Petici�n Anulada", vbInformation, "Aviso")
          End If
      End If
  End If
End If
err:
  cmdAnular.Enabled = True
  Me.Enabled = True
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdValidar_Click()
Dim i As Integer
Dim strinsert As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
If IsNumeric(txtText1(0).Text) Then
  If tlbToolbar1.Buttons(4).Enabled = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  End If
  If (txtText1(1).Text = 3) Then 'enviada
    strinsert = "UPDATE FR5500 SET FR26CODESTPETIC=4 " & _
                " WHERE FR55CODNECESUNID=" & _
                  txtText1(0).Text
    objApp.rdoConnect.Execute strinsert, 64
    objApp.rdoConnect.Execute "Commit", 64
    Screen.MousePointer = vbHourglass
    'se rellena el Libro de Estupefacientes
    Call Rellenar_Libro_Estupefacientes
    Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
    objWinInfo.DataRefresh
    Screen.MousePointer = vbDefault
    Call MsgBox("La Petici�n est� Validada", vbInformation, "Aviso")
    
  Else
    Call MsgBox("La Petici�n debe estar Enviada", vbInformation, "Aviso")
  End If
End If
  
Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub

Private Sub cmdEstupefacientes_Click()
Dim v As Integer
  Dim i As Integer
  Dim noinsertar As Boolean
  Dim mensaje As String
  Dim strinsert As String

  If mblnError Then
    Exit Sub
  End If
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  End If
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Exit Sub
  End If
  
  Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
  
  If (Len(Trim(dtcDateCombo1(0).Text)) = 0) Or _
     (Not IsNumeric(txtText1(0).Text)) Or _
     (Not IsNumeric(txtText1(5).Text)) Then
    Exit Sub
  End If
  
  cmdEstupefacientes.Enabled = False
  If IsNumeric(txtText1(0).Text) Then
    noinsertar = True
    If grdDBGrid1(1).Rows > 0 Then
      For i = 0 To grdDBGrid1(1).Rows - 1
        If Len(Trim(grdDBGrid1(1).Columns("Cantidad").Value)) = 0 Then
          MsgBox "Se debe introducir la cantidad ", vbInformation, "Redactar Pedido Farmacia"
          cmdEstupefacientes.Enabled = True
          Exit Sub
        End If
        grdDBGrid1(1).MoveNext
      Next i
    End If
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.DataSave
    gstrBoton = "Estupefacientes"
    Call objsecurity.LaunchProcess("FR0360")
    gstrBoton = ""
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2)
          grdDBGrid1(1).Columns(7).Value = gintprodbuscado(v, 7)
          grdDBGrid1(1).Columns(10).Value = gintprodbuscado(v, 6)
          grdDBGrid1(1).Columns(11).Value = gintprodbuscado(v, 3)
          grdDBGrid1(1).Columns(12).Value = gintprodbuscado(v, 4)
          grdDBGrid1(1).Columns(9).Value = gintprodbuscado(v, 5)
          grdDBGrid1(1).Columns(14).Value = 1 'cantidad
          '*********************************************************************
          strinsert = "INSERT INTO FR2000(FR55CODNECESUNID,FR20NUMLINEA,FR73CODPRODUCTO,"
          strinsert = strinsert & "FR93CODUNIMEDIDA,FR20CANTNECESQUIR,FR20INDMOD) VALUES ("
          strinsert = strinsert & grdDBGrid1(1).Columns(3).Value & ","
          strinsert = strinsert & grdDBGrid1(1).Columns(4).Value & ","
          strinsert = strinsert & grdDBGrid1(1).Columns(5).Value & ","
          If grdDBGrid1(1).Columns(12).Value = "" Then 'U.M
            strinsert = strinsert & "NULL" & ","
          Else
            strinsert = strinsert & "'" & grdDBGrid1(1).Columns(12).Value & "'" & ","
          End If
          strinsert = strinsert & grdDBGrid1(1).Columns(14).Value & ","
          strinsert = strinsert & 0 & ")" '?
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          '********************************************************************+
      Next v
    End If
    objWinInfo.objWinActiveForm.blnChanged = False
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
    gintprodtotal = 0
  End If
  cmdEstupefacientes.Enabled = True

End Sub

Private Sub Form_Activate()
Dim rsta As rdoResultset
Dim stra As String

'se refresca el n� de colegiado
If txtText1(3).Text <> "" Then
  stra = "SELECT SG02NUMCOLEGIADO FROM SG0200 " & _
       " WHERE SG02COD=" & "'" & txtText1(3).Text & "'"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
   If Not IsNull(rsta.rdoColumns("SG02NUMCOLEGIADO").Value) Then
      txtcolegiado.Text = rsta.rdoColumns("SG02NUMCOLEGIADO").Value
   End If
  Else
      txtcolegiado.Text = ""
  End If
  rsta.Close
  Set rsta = Nothing
End If
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "Necesidad Unidad" And strCtrlName = "txttext1(7)" Then  'SECCION
    aValues(2) = txtText1(5).Text  'DEPARTAMENTO
End If
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

If strFormName = "Detalle Necesidad" Then
  Select Case txtText1(1).Text
    Case 1:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
        objWinInfo.DataRefresh
      End If
      'cmdEstupefacientes.Enabled = True
      'cmdFirmarEnviar.Enabled = True
    Case 3:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      'cmdEstupefacientes.Enabled = True
      'cmdFirmarEnviar.Enabled = True
    Case Else:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      'cmdEstupefacientes.Enabled = False
      'cmdFirmarEnviar.Enabled = False
  End Select
End If

End Sub




Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim blnDptoCorrecto As Boolean
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset

  If objWinInfo.objWinActiveForm.strName <> "Detalle Necesidad" Then
    If txtText1(5).Text <> "" Then
      blnDptoCorrecto = False
      'If txttext1(3).Text <> "" Then
      '  sqlstr = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & txttext1(3).Text & "'"
      '  sqlstr = sqlstr & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
      '  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      '  While Not rsta.EOF
      '    If txttext1(5).Text = rsta.rdoColumns(0).Value Then
      '      blnDptoCorrecto = True
      '    End If
      '    rsta.MoveNext
      '  Wend
      '  rsta.Close
      '  Set rsta = Nothing
      'Else
        sqlstr = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "'"
        sqlstr = sqlstr & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        While Not rsta.EOF
          If txtText1(5).Text = rsta.rdoColumns(0).Value Then
            blnDptoCorrecto = True
          End If
          rsta.MoveNext
        Wend
        rsta.Close
        Set rsta = Nothing
      'End If
      If blnDptoCorrecto = False Then
        Call MsgBox("El Peticionario no pertenece al servicio", vbExclamation, "Aviso")
        blnCancel = True
      End If
      If blnDptoCorrecto = True Then
        SQL = "SELECT COUNT(*)"
        SQL = SQL & " FROM AD4100"
        SQL = SQL & " WHERE AD02CODDPTO = ?"
        SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = txtText1(5).Text
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rs(0).Value = 0 Then
        Else
          If txtText1(7).Text = "" Then
            MsgBox "El Servicio tiene secciones, es Obligatorio elegir una SECCION.", vbExclamation
            blnCancel = True
          End If
        End If
        rs.Close
        Set rs = Nothing
      End If
    End If
  End If

  If IsNumeric(txtText1(5).Text) And IsNumeric(txtText1(7).Text) And txtText1(7).Visible = True Then
    sqlstr = "SELECT COUNT(*) FROM AD4100 WHERE AD02CODDPTO=" & txtText1(5).Text
    sqlstr = sqlstr & " AND " & "AD41CODSECCION=" & txtText1(7).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.rdoColumns(0).Value = 0 Then
      Call MsgBox("La secci�n es incorrecta", vbExclamation, "Aviso")
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  If objWinInfo.objWinActiveForm.strName = "Detalle Necesidad" Then
    If IsNumeric(grdDBGrid1(1).Columns(14).Value) = True Then
      If CDec(grdDBGrid1(1).Columns(14).Value) <= 0 Then
        Call MsgBox("Hay datos incorrectos en la cantidad pedida", vbExclamation, "Aviso")
        blnCancel = True
        'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(10), 0)
      End If
    Else
      Call MsgBox("Hay datos incorrectos en la cantidad pedida", vbExclamation, "Aviso")
      blnCancel = True
    End If
  End If
  mblnError = blnCancel
End Sub



Private Sub cmdfirmarenviar_Click()
  Dim intMsg As Integer
  Dim strupdate As String
  Dim stra As String
  Dim rsta As rdoResultset
   
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  End If
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Exit Sub
  End If
  
  Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
  Select Case txtText1(1).Text
    Case 1:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
        objWinInfo.DataRefresh
      End If
      cmdEstupefacientes.Enabled = True
      cmdFirmarEnviar.Enabled = True
    Case 2:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      cmdEstupefacientes.Enabled = True
      cmdFirmarEnviar.Enabled = True
    Case Else:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      cmdEstupefacientes.Enabled = False
      cmdFirmarEnviar.Enabled = False
      Exit Sub
  End Select
  
  If txtText1(1).Text = 3 Then
    MsgBox "La Petici�n ya est� firmada.", vbCritical, "Aviso"
    Exit Sub
  End If
   
    cmdFirmarEnviar.Enabled = False
    If IsNumeric(txtText1(0).Text) Then
        intMsg = MsgBox("�Est� seguro que desea firmar y enviar la Petici�n?", vbQuestion + vbYesNo, "Aviso")
        If intMsg = vbYes Then
            cmdFirmarEnviar.Enabled = True
            strupdate = "UPDATE FR5500 SET FR55FECENVIO=SYSDATE,FR26CODESTPETIC=3, "
            strupdate = strupdate & " SG02COD_FIR=" & "'" & objsecurity.strUser & "'"
            strupdate = strupdate & " WHERE FR55CODNECESUNID=" & txtText1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
            
            stra = "SELECT SG02NUMCOLEGIADO FROM SG0200 " & _
                   " WHERE SG02COD=" & "'" & objsecurity.strUser & "'"
            Set rsta = objApp.rdoConnect.OpenResultset(stra)
            If Not rsta.EOF Then
             If Not IsNull(rsta.rdoColumns("SG02NUMCOLEGIADO").Value) Then
              txtcolegiado.Text = rsta.rdoColumns("SG02NUMCOLEGIADO").Value
             End If
            End If
            rsta.Close
            Set rsta = Nothing
           
            Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
            objWinInfo.DataRefresh
            cmdFirmarEnviar.Enabled = True
        Else
          cmdFirmarEnviar.Enabled = True
        End If
    Else
      cmdFirmarEnviar.Enabled = True
    End If
   cmdFirmarEnviar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String

  
    Set objWinInfo = New clsCWWin
        Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
      With objMasterInfo
        Set .objFormContainer = fraframe1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(2)
        
        .strName = "Necesidad Unidad"
        .blnAskPrimary = False
        .intAllowance = cwAllowReadOnly
          
        .strTable = "FR5500"
        .strWhere = "FR55CODNECESUNID=" & glngnumpeticion
        
        Call .FormAddOrderField("FR55FECPETICION", cwDescending)
        Call .FormAddOrderField("FR55CODNECESUNID", cwDescending)
        
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
        Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
        Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
        Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")
    
      End With
    With objMultiInfo
        .strName = "Detalle Necesidad"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = fraframe1(1)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR2000"
        
        If gEstadoDeLaPeticion = "Dispensada Parcial" Then
          's�lo se visualizan las l�nea que no han sido dispensadas a�n
          .strWhere = "FR20NUMLINEA NOT IN (SELECT FR13NUMLINEA FROM FR1300 WHERE" & _
                                     " FR13NUMRECETAOFICIAL=" & glngnumpeticion & _
                                     " AND FR13INDPRN=0 AND " & _
                                     " FR13INDDISPENSADO=-1 AND FR13INDENTRADAMANUAL=0)" & _
                                     " AND FR13INDENTRADAMANUAL=0"
        End If

        .intAllowance = cwAllowReadOnly
        .intCursorSize = 0
        
        Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
        
        strKey = .strDataBase & .strTable
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Necesidades")
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida", cwString)
        Call .FormAddFilterWhere(strKey, "FR20CANTNECESQUIR", "Cantidad", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d. Producto")
        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida")
        Call .FormAddFilterOrder(strKey, "FR20CANTNECESQUIR", "Cantidad")
        
        Call .FormAddRelation("FR55CODNECESUNID", txtText1(0))
    End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
        Call .GridAddColumn(objMultiInfo, "C�d. Necesidad", "FR55CODNECESUNID", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "L�nea", "FR20NUMLINEA", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "C�d.Int", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
        Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
        Call .GridAddColumn(objMultiInfo, "F.F", "", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "Dosis", "", cwDecimal, 9)
        Call .GridAddColumn(objMultiInfo, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "Descripci�n U.M", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Cantidad", "FR20CANTNECESQUIR", cwNumeric, 11)
        Call .GridAddColumn(objMultiInfo, "Mod", "FR20INDMOD", cwBoolean, 1)
    
        Call .FormCreateInfo(objMasterInfo)
    
        ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).intKeyNo = 1
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(txtText1(0)).intKeyNo = 1

        Call .FormChangeColor(objMultiInfo)
    
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnReadOnly = True
        .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
        .CtrlGetInfo(txtText1(3)).blnInFind = True
        .CtrlGetInfo(txtText1(5)).blnInFind = True
        .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
        
        .CtrlGetInfo(chkCheck1(0)).blnInGrid = False

        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "SG02COD_FIR", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "SG02APE1")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "AD02DESDPTO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "AD02DESDPTO")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR26DESESTADOPET")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "AD41CODSECCION", "SELECT AD41DESSECCION FROM AD4100 WHERE AD41CODSECCION= ? AND AD02CODDPTO=?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(8), "AD41DESSECCION")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(8), "FR73DESPRODUCTO")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(10), "FRH7CODFORMFAR")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(11), "FR73DOSIS")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73REFERENCIA")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(9), "FR73TAMENVASE")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), grdDBGrid1(1).Columns(13), "FR93DESUNIMEDIDA")
    
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnReadOnly = True
        '.CtrlGetInfo(txttext1(3)).blnForeign = True
        .CtrlGetInfo(txtText1(5)).blnForeign = True
        .CtrlGetInfo(txtText1(7)).blnForeign = True
        
        .CtrlGetInfo(txtText1(3)).blnReadOnly = True
        
        .CtrlGetInfo(txtcolegiado).blnNegotiated = False
        
        Call .WinRegister
        Call .WinStabilize
    End With
    If gintfirmarStPlanta = 1 Then
        grdDBGrid1(1).Columns(0).Width = 0
        grdDBGrid1(1).Columns(4).Width = 700
        grdDBGrid1(1).Columns(5).Width = 1100
        grdDBGrid1(1).Columns(6).Width = 700
        grdDBGrid1(1).Columns(8).Width = 3200
        grdDBGrid1(1).Columns(9).Width = 800
        grdDBGrid1(1).Columns(10).Width = 500
        grdDBGrid1(1).Columns(11).Width = 1000
        grdDBGrid1(1).Columns(7).Width = 1300
        grdDBGrid1(1).Columns(12).Width = 700
        grdDBGrid1(1).Columns(13).Width = 1300
        grdDBGrid1(1).Columns(14).Width = 900
    Else
        grdDBGrid1(1).Columns(0).Width = 0
        grdDBGrid1(1).Columns(4).Width = 600
        grdDBGrid1(1).Columns(5).Width = 1000
        grdDBGrid1(1).Columns(6).Width = 700
        grdDBGrid1(1).Columns(8).Width = 3200
        grdDBGrid1(1).Columns(9).Width = 800
        grdDBGrid1(1).Columns(10).Width = 500
        grdDBGrid1(1).Columns(11).Width = 1000
        grdDBGrid1(1).Columns(7).Width = 1300
        grdDBGrid1(1).Columns(12).Width = 600
        grdDBGrid1(1).Columns(13).Width = 1300
        grdDBGrid1(1).Columns(14).Width = 800
        grdDBGrid1(1).RemoveAll
    End If
    ''grdDBGrid1(1).Columns(2).Visible = False
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(4).Visible = False
    grdDBGrid1(1).Columns(5).Visible = False
    grdDBGrid1(1).Columns(13).Visible = False
    grdDBGrid1(1).Columns("Mod").Visible = False
    
    txtcolegiado.Locked = True
    txtcolegiado.BackColor = &HC0C0C0
    
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strdelete As String
Dim stra As String
Dim rsta As rdoResultset
    
  intCancel = objWinInfo.WinExit
  If intCancel = 0 Then
    'si la petici�n no tiene productos elimina dicha petici�n
    If IsNumeric(txtText1(0).Text) Then
      stra = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
        strdelete = "DELETE FR5500 WHERE FR55CODNECESUNID=" & txtText1(0).Text
        objApp.rdoConnect.Execute strdelete, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If

End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    Dim sqlstr As String
    Dim rsta As rdoResultset
    
    If Index = 1 Then
        If IsNumeric(grdDBGrid1(1).Columns(5).Value) Then
            sqlstr = "SELECT count(*) from fra300 where fr73codproducto=" & grdDBGrid1(1).Columns(5).Value
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            If rsta(0).Value > 0 Then
                For i = 3 To 10
                    grdDBGrid1(1).Columns(i).CellStyleSet "Alerta"
                Next i
            End If
          End If
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txttext1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = " WHERE AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
                 
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txttext1(7)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD4100"

     Set objField = .AddField("AD41CODSECCION")
     objField.strSmallDesc = "C�digo Secci�n"

     Set objField = .AddField("AD41DESSECCION")
     objField.strSmallDesc = "Descripci�n Secci�n"
     
     If IsNumeric(txtText1(5).Text) Then
      .strWhere = "where AD02CODDPTO=" & txtText1(5).Text
     Else
       Call MsgBox("Debe introducir el Servicio antes de ver sus Secciones", vbInformation, "Aviso")
       Exit Sub
     End If

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(7), .cllValues("AD41CODSECCION"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim marca As Variant
Dim strdelete As String
Dim stra As String

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    'si la petici�n no tiene productos elimina dicha petici�n
    If IsNumeric(txtText1(0).Text) Then
      stra = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
          strdelete = "DELETE FR5500 WHERE FR55CODNECESUNID=" & txtText1(0).Text
          objApp.rdoConnect.Execute strdelete, 64
          objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If
    txtcolegiado = ""
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR55CODNECESUNID_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 1) 'FR26CODESTPETIC, REDACTADA
    rsta.Close
    Set rsta = Nothing
    'txtText1(0).SetFocus
    
    sqlstr = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "'"
    sqlstr = sqlstr & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If Not rsta.EOF Then
      Call objWinInfo.CtrlSet(txtText1(5), rsta.rdoColumns(0).Value)
    End If
    rsta.Close
    Set rsta = Nothing
    dtcDateCombo1(0).Date = Date
    chkCheck1(0).Value = 1
    
    'Call objWinInfo.CtrlGotFocus
    'Call objWinInfo.CtrlLostFocus
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle Necesidad" Then
      If IsNumeric(txtText1(0).Text) Then
        marca = grdDBGrid1(1).Bookmark
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR2000 WHERE FR55CODNECESUNID=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        'marca = grdDBGrid1(1).Bookmark
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          'linea = 1 + grdDBGrid1(1).Row
          If IsNumeric(grdDBGrid1(1).Columns(4).CellValue(marca)) Then
            linea = 1 + grdDBGrid1(1).Columns(4).CellValue(marca)
          Else
            linea = 1
          End If
        Else
          'linea = rstlinea.rdoColumns(0).Value + 1 + grdDBGrid1(1).Row
          If rstlinea.rdoColumns(0).Value > grdDBGrid1(1).Columns(4).CellValue(marca) Then
            linea = rstlinea.rdoColumns(0).Value + 1
          Else
            linea = grdDBGrid1(1).Columns(4).CellValue(marca) + 1
          End If
        End If
        grdDBGrid1(1).Columns(4).Value = linea
        grdDBGrid1(1).Col = 5
        'SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Grabar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Borrar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 80 'Imprimir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Deshacer
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(14))
  Case 30 'Cortar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(10))
  Case 40 'Copiar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(11))
  Case 50 'Pegar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(12))
  Case Else
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  End Select
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Poner Filtro
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(18))
  Case 20 'Quitar Filtro
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(19))
  Case Else
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  End Select
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Localizar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16))
  Case 40 'Primera
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Refrescar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  Case 20 'Mantenimiento
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(28))
  Case Else
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End Select
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Dim vntDatos(1 To 2) As Variant
    
    Call objWinInfo.GridDblClick
    If grdDBGrid1(1).Rows > 0 Then
        vntDatos(1) = "FrmRedPedStPlanta"
        vntDatos(2) = grdDBGrid1(1).Columns(5).Value
        'mantenimiento alerta de inmovilizaci�n
        'Call objsecurity.LaunchProcess("FR0003", vntDatos)
    End If
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim stra As String
Dim rsta As rdoResultset
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 1 And IsNumeric(grdDBGrid1(1).Columns(5).Value) And grdDBGrid1(1).Columns(12).Value = "" Then
      stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(12), rsta(0).Value)
      rsta.Close
      Set rsta = Nothing
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
If intIndex = 1 Then
  If grdDBGrid1(1).Columns(14).Value <> "" Then
    'se controla que en la columna cantidad no se metan n�meros negativos
    If grdDBGrid1(1).Columns(14).Value = "-" Then
        Call MsgBox("El campo Cantidad no admite valores negativos", vbInformation, "Aviso")
        grdDBGrid1(1).Columns(14).Value = ""
    End If
    If IsNumeric(grdDBGrid1(1).Columns(14).Value) Then
      If CDec(grdDBGrid1(1).Columns(14).Value) = 0 Then
        Call MsgBox("El campo Cantidad no admite el valor 0", vbInformation, "Aviso")
        grdDBGrid1(1).Columns(14).Value = ""
      End If
    Else
      MsgBox "El campo Cantidad debe ser num�rico", vbInformation, "Aviso"
      grdDBGrid1(1).Columns(14).Value = ""
    End If
  End If
End If
    Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rsta As rdoResultset
Dim stra As String

Call objWinInfo.CtrlDataChange

If intIndex = 5 Then
  If IsNumeric(txtText1(5).Text) Then
    stra = "SELECT COUNT(*) FROM AD4100 WHERE AD02CODDPTO=" & txtText1(5).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value = 0 Then
        txtText1(7).Visible = False
        txtText1(8).Visible = False
        lblLabel1(1).Visible = False
        lblLabel1(2).Visible = False
        txtText1(7).Text = ""
    Else
        txtText1(7).Visible = True
        txtText1(8).Visible = True
        lblLabel1(1).Visible = True
        lblLabel1(2).Visible = True
    End If
  End If
End If

If intIndex = 1 Then 'estado
  If IsNumeric(txtText1(1).Text) Then
    If gstrLlamador = "Consultar Peticiones de estupefacientes" Then
      Select Case txtText1(1).Text
          Case 1 'redactada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            Frame1.Visible = True
            cmdFirmarEnviar.Visible = True
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = True
            cmdAnular.Visible = True
            Frame4.Top = 6000
          Case 3 'enviada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Top = 4800
            frame3.Left = 10320
            cmdValidar.Top = 360
            cmdValidar.Left = 120
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = True
            Frame4.Top = 5000
            cmdAnular.Visible = True
          Case 4 'validada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = True
            cmdAnular.Visible = False
            Frame4.Top = 4800
            Frame4.Left = 10320
          Case 9 'parcialmente dispensada
            fraframe1(2).Caption = "Necesidades de la Unidad. Estupefacientes no Dispensados"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = False
            Frame4.Visible = True
            cmdAnular.Visible = False
            Frame4.Top = 4800
            Frame4.Left = 10320
          Case 5 'dispensada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = False
            cmdAnular.Visible = False
          Case 8 'anulada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = False
            cmdAnular.Visible = False
          Case Else
            fraframe1(2).Caption = "Necesidades de la Unidad"
      End Select
    End If
    If gstrLlamador <> "Consultar Peticiones de estupefacientes" Then
      Select Case txtText1(1).Text
          Case 1 'redactada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            Frame1.Visible = True
            cmdFirmarEnviar.Visible = True
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = True
            cmdAnular.Visible = True
            Frame4.Top = 6000
          Case 3 'enviada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Top = 4800
            frame3.Left = 10320
            cmdValidar.Top = 360
            cmdValidar.Left = 120
            frame3.Visible = True
            cmdValidar.Visible = True
            Frame4.Visible = True
            Frame4.Top = 6000
            cmdAnular.Visible = True
          Case 4 'validada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = True
            cmdAnular.Visible = True
            Frame4.Top = 4800
            Frame4.Left = 10320
          Case 9 'parcialmente dispensada
            fraframe1(2).Caption = "Necesidades de la Unidad. Estupefacientes no Dispensados"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = False
            cmdAnular.Visible = False
            Frame4.Top = 4800
            Frame4.Left = 10320
          Case 5 'dispensada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = False
            cmdAnular.Visible = False
          Case 8 'anulada
            fraframe1(2).Caption = "Necesidades de la Unidad"
            cmdEstupefacientes.Visible = False
            Frame2.Visible = False
            Frame1.Visible = False
            cmdFirmarEnviar.Visible = False
            frame3.Visible = False
            cmdValidar.Visible = False
            Frame4.Visible = False
            cmdAnular.Visible = False
          Case Else
            fraframe1(2).Caption = "Necesidades de la Unidad"
      End Select
    End If
  End If
End If

End Sub

Private Sub Rellenar_Libro_Estupefacientes()
Dim rsta As rdoResultset
Dim stra As String
Dim strinsert As String
Dim rstsecuencia As rdoResultset
Dim strsecuencia As String
Dim i As Long

stra = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
    strsecuencia = "SELECT FR13CODLIBROESTUPEF_SEQUENCE.nextval FROM DUAL"
    Set rstsecuencia = objApp.rdoConnect.OpenResultset(strsecuencia)
    'se hace la entrada en el libro de estupefacientes
    strinsert = "INSERT INTO FR1300(FR13CODLIBROESTUPEF,FR73CODPRODUCTO,FR13FECPETICION,"
    strinsert = strinsert & "FR13NUMRECETAOFICIAL,FR13NUMLINEA,FR13SALIDA,"
    strinsert = strinsert & "SG02COD,AD02CODDPTO,"
    strinsert = strinsert & "FR13SALDOS,FR13INDENTRADAMANUAL,"
    strinsert = strinsert & "FR13INDPRN,FR13INDESTUPEFACIENTE)"
    strinsert = strinsert & " VALUES "
    'strinsert = strinsert & "(FR13CODLIBROESTUPEF_SEQUENCE.nextval,"
    strinsert = strinsert & "(" & rstsecuencia.rdoColumns(0).Value & ","
    strinsert = strinsert & rsta.rdoColumns("FR73CODPRODUCTO").Value & ","
    strinsert = strinsert & "SYSDATE" & ","
    strinsert = strinsert & txtText1(0).Text & ","
    strinsert = strinsert & rsta.rdoColumns("FR20NUMLINEA").Value & ","
    strinsert = strinsert & objGen.ReplaceStr(rsta.rdoColumns("FR20CANTNECESQUIR").Value, ",", ".", 1) & "," 'cantidad
    strinsert = strinsert & "'" & txtText1(3).Text & "'" & "," 'm�dico que ha firmado y enviado
    strinsert = strinsert & txtText1(5).Text & "," 'servicio
    strinsert = strinsert & "NULL," 'saldos
    strinsert = strinsert & 0 & ","
    strinsert = strinsert & 0 & ","   'no es una petici�n a paciente(FR0196) sino de un Servicio
    strinsert = strinsert & -1 & ")"  'es un estupefaciente
    objApp.rdoConnect.Execute strinsert, 64
    objApp.rdoConnect.Execute "Commit", 64
    Call Actualizar_Saldos(rstsecuencia.rdoColumns(0).Value)
    For i = 0 To 30000000
    Next i
    rstsecuencia.Close
    Set rstsecuencia = Nothing
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
End Sub


Private Sub Actualizar_Saldos(CodigoLibro)
Dim rsta As rdoResultset
Dim stra As String
Dim fecha_max As Date
Dim codigo_libro As Variant
Dim saldo As Variant
Dim entrada
Dim salida
Dim nuevo_saldo
Dim Incremento
Dim strupdate As String
Dim saldo_actual
Dim strlibro As String
Dim rstlibro As rdoResultset


strlibro = "SELECT * FROM FR1300 WHERE FR13CODLIBROESTUPEF=" & CodigoLibro
Set rstlibro = objApp.rdoConnect.OpenResultset(strlibro)

'se coge la fecha justo anterior para tomarla de referencia para calcular el saldo
stra = "SELECT FR13CODLIBROESTUPEF,FR13FECPETICION,FR13SALDOS,FR73CODPRODUCTO FROM FR1300 WHERE"
stra = stra & " TO_CHAR(FR13FECPETICION,'DD/MM/YYYY HH24:MI:SS')="
stra = stra & " (SELECT TO_CHAR(MAX(FR13FECPETICION),'DD/MM/YYYY HH24:MI:SS')"
stra = stra & " FROM FR1300 WHERE"
stra = stra & " TO_DATE(TO_CHAR(FR13FECPETICION,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') <"
stra = stra & " TO_DATE('" & rstlibro.rdoColumns("FR13FECPETICION").Value & "','DD/MM/YYYY HH24:MI:SS')"
stra = stra & " AND FR13INDESTUPEFACIENTE=-1 AND FR73CODPRODUCTO=" & rstlibro.rdoColumns("FR73CODPRODUCTO").Value
stra = stra & " AND FR13CODLIBROESTUPEF<>" & CodigoLibro & ")"
stra = stra & " AND FR13INDESTUPEFACIENTE=-1 AND FR73CODPRODUCTO=" & rstlibro.rdoColumns("FR73CODPRODUCTO").Value
stra = stra & " AND FR13CODLIBROESTUPEF<>" & CodigoLibro
Set rsta = objApp.rdoConnect.OpenResultset(stra)

If IsNull(rstlibro.rdoColumns("FR13ENTRADA").Value) And IsNull(rstlibro.rdoColumns("FR13SALIDA").Value) Then
  Incremento = 0
End If
If Not IsNull(rstlibro.rdoColumns("FR13ENTRADA").Value) And Not IsNull(rstlibro.rdoColumns("FR13SALIDA").Value) Then
  Incremento = CCur(rstlibro.rdoColumns("FR13ENTRADA").Value) - CCur(rstlibro.rdoColumns("FR13SALIDA").Value)
End If
If IsNull(rstlibro.rdoColumns("FR13ENTRADA").Value) And Not IsNull(rstlibro.rdoColumns("FR13SALIDA").Value) Then
  Incremento = -CCur(rstlibro.rdoColumns("FR13SALIDA").Value)
End If
If Not IsNull(rstlibro.rdoColumns("FR13ENTRADA").Value) And IsNull(rstlibro.rdoColumns("FR13SALIDA").Value) Then
  Incremento = CCur(rstlibro.rdoColumns("FR13ENTRADA").Value)
End If

If Not rsta.EOF Then
  If Not IsNull(rsta.rdoColumns("FR13SALDOS").Value) Then
    nuevo_saldo = rsta.rdoColumns("FR13SALDOS").Value + Incremento
  Else
    nuevo_saldo = 0 + Incremento
  End If
Else
  nuevo_saldo = 0 + Incremento
End If
rsta.Close
Set rsta = Nothing

strupdate = "UPDATE FR1300 SET FR13SALDOS=" & objGen.ReplaceStr(nuevo_saldo, ",", ".", 1)
strupdate = strupdate & " WHERE FR13CODLIBROESTUPEF=" & CodigoLibro
objApp.rdoConnect.Execute strupdate, 64
objApp.rdoConnect.Execute "Commit", 64

'se cogen los registros con fecha mayor al nuevo insertado para modificarles el saldo
stra = "SELECT * FROM FR1300 "
stra = stra & "WHERE TO_DATE(TO_CHAR(FR13FECPETICION,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') "
stra = stra & ">"
stra = stra & " TO_DATE('" & rstlibro.rdoColumns("FR13FECPETICION").Value & "','DD/MM/YYYY HH24:MI:SS')"
stra = stra & " AND FR13INDESTUPEFACIENTE=-1"
stra = stra & " AND FR73CODPRODUCTO=" & rstlibro.rdoColumns("FR73CODPRODUCTO").Value
stra = stra & " ORDER BY FR13CODLIBROESTUPEF ASC"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  If Not rsta.rdoColumns("FR13SALDOS").Value Then
    saldo_actual = rsta.rdoColumns("FR13SALDOS").Value
  Else
    saldo_actual = 0
  End If
  strupdate = "UPDATE FR1300 SET FR13SALDOS=" & objGen.ReplaceStr(saldo_actual + Incremento, ",", ".", 1)
  strupdate = strupdate & " WHERE FR13CODLIBROESTUPEF=" & rsta.rdoColumns("FR13CODLIBROESTUPEF").Value
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
rstlibro.Close
Set rstlibro = Nothing
End Sub


Private Sub Rellenar_Libro_Estupefacientes_Petici�n_Anulada()
Dim rsta As rdoResultset
Dim stra As String
Dim strinsert As String
Dim rstsecuencia As rdoResultset
Dim strsecuencia As String
Dim i As Long

stra = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
    strsecuencia = "SELECT FR13CODLIBROESTUPEF_SEQUENCE.nextval FROM DUAL"
    Set rstsecuencia = objApp.rdoConnect.OpenResultset(strsecuencia)
    'se hace la entrada en el libro de estupefacientes
    strinsert = "INSERT INTO FR1300(FR13CODLIBROESTUPEF,FR73CODPRODUCTO,FR13FECPETICION,"
    strinsert = strinsert & "FR13NUMRECETAOFICIAL,FR13NUMLINEA,FR13ENTRADA,"
    strinsert = strinsert & "SG02COD,AD02CODDPTO,"
    strinsert = strinsert & "FR13SALDOS,FR13INDENTRADAMANUAL,"
    strinsert = strinsert & "FR13INDPRN,FR13INDESTUPEFACIENTE,FR13INDPETICANULADA)"
    strinsert = strinsert & " VALUES "
    strinsert = strinsert & "(" & rstsecuencia.rdoColumns(0).Value & ","
    strinsert = strinsert & rsta.rdoColumns("FR73CODPRODUCTO").Value & ","
    strinsert = strinsert & "SYSDATE" & ","
    strinsert = strinsert & txtText1(0).Text & ","
    strinsert = strinsert & rsta.rdoColumns("FR20NUMLINEA").Value & ","
    strinsert = strinsert & objGen.ReplaceStr(rsta.rdoColumns("FR20CANTNECESQUIR").Value, ",", ".", 1) & "," 'cantidad
    strinsert = strinsert & "'" & txtText1(3).Text & "'" & "," 'm�dico que ha firmado y enviado
    strinsert = strinsert & txtText1(5).Text & "," 'servicio
    strinsert = strinsert & "NULL," 'saldos
    strinsert = strinsert & 0 & ","
    strinsert = strinsert & 0 & ","   'no es una petici�n a paciente(FR0196) sino de un Servicio
    strinsert = strinsert & -1 & ","  'es un estupefaciente
    strinsert = strinsert & -1 & ")"  'l�nea de una petici�n anulada
    objApp.rdoConnect.Execute strinsert, 64
    objApp.rdoConnect.Execute "Commit", 64
    Call Actualizar_Saldos(rstsecuencia.rdoColumns(0).Value)
    For i = 0 To 30000000
    Next i
    rstsecuencia.Close
    Set rstsecuencia = Nothing
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
End Sub
