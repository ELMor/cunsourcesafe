VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmBusMedFRPRD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Buscar Medicamento"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0092.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   80
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Index           =   0
      Left            =   0
      TabIndex        =   88
      Top             =   2040
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   5340
         Index           =   0
         Left            =   240
         TabIndex        =   89
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   9419
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0092.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(14)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(15)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(16)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(17)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(18)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(20)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(21)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(22)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(23)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(24)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(25)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(26)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(27)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(28)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(5)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(6)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(11)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(12)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "lblLabel1(56)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "SSTab1"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "Command2"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(4)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "chkCheck1(0)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(7)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(12)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(13)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(14)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(5)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(16)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(18)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(19)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(20)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(21)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(22)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(23)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(24)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(25)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(26)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(27)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(0)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(11)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(6)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtText1(10)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(8)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).Control(47)=   "txtText1(9)"
         Tab(0).Control(47).Enabled=   0   'False
         Tab(0).Control(48)=   "txtText1(17)"
         Tab(0).Control(48).Enabled=   0   'False
         Tab(0).ControlCount=   49
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0092.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODINTFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   17
            Left            =   5880
            TabIndex        =   15
            Tag             =   "C�d.Int.Farm."
            Top             =   360
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODINTFARSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   7080
            TabIndex        =   16
            Tag             =   "C.S."
            Top             =   360
            Width           =   345
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73VOLUMEN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Volumen mL"
            Top             =   360
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DOSIS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH7CODFORMFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "F.F. Forma Farmac�utica"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8760
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "U.M. Unidad de Medida"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   0
            Left            =   1560
            TabIndex        =   13
            Top             =   0
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   4200
            TabIndex        =   34
            Tag             =   "Moneda"
            Top             =   2160
            Width           =   570
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73BONIFICACION"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   6960
            TabIndex        =   37
            Tag             =   "Tanto por ciento de Bonificaci�n"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECOFR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   2160
            TabIndex        =   33
            Tag             =   "Precio Oferta de Compra"
            Top             =   2160
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PREULTENT"
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   120
            TabIndex        =   32
            Tag             =   "Precio de la �ltima entrada"
            Top             =   2160
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73TAMPEDI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   5040
            TabIndex        =   24
            Tag             =   "Tama�o del pedido en d�as"
            Top             =   960
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DIASSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   3600
            TabIndex        =   23
            Tag             =   "D�as de Seguridad"
            Top             =   960
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73RAPPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   6000
            TabIndex        =   36
            Tag             =   "Tanto por ciento de Rappel"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73EXISTENCIAS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   20
            Left            =   4080
            TabIndex        =   30
            Tag             =   "Existencias"
            Top             =   1560
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMAX"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   2160
            TabIndex        =   29
            Tag             =   "Stock m�ximo"
            Top             =   1560
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMIN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   18
            Left            =   120
            TabIndex        =   28
            Tag             =   "Stock m�nimo"
            Top             =   1560
            Width           =   1905
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSANUAL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   16
            Left            =   9720
            TabIndex        =   27
            Tag             =   "Consumo Anual"
            Top             =   960
            Width           =   1170
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Medicamentos"
            Top             =   360
            Width           =   5580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSMES"
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   8520
            TabIndex        =   26
            Tag             =   "Consumo mensual"
            Top             =   960
            Width           =   1170
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSDIARIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   7440
            TabIndex        =   25
            Tag             =   "Consumo Diario"
            Top             =   960
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   5040
            TabIndex        =   35
            Tag             =   "Tanto por ciento de Descuento"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PTEBONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   1800
            TabIndex        =   22
            Tag             =   "Pendiente Bonificar"
            Top             =   960
            Width           =   1650
         End
         Begin VB.CheckBox chkCheck1 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Consumo Regular"
            DataField       =   "FR73INDCONSREGULAR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   7440
            TabIndex        =   31
            Tag             =   "Consumo Regular"
            Top             =   1560
            Width           =   1905
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CANTPEND"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   120
            TabIndex        =   21
            Tag             =   "Cantidad pendiente de recibir"
            Top             =   960
            Width           =   1650
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Traer Medicamento"
            Height          =   375
            Left            =   8160
            TabIndex        =   38
            Top             =   2160
            Width           =   1935
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2655
            Left            =   120
            TabIndex        =   90
            Top             =   2520
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   4683
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Proveedores"
            TabPicture(0)   =   "FR0092.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Frame3"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "txtText1(3)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "txtText1(2)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "txtText1(1)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).ControlCount=   4
            TabCaption(1)   =   "Informaci�n Adicional"
            TabPicture(1)   =   "FR0092.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "SSTab2"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_C"
               Height          =   330
               HelpContextID   =   30104
               Index           =   1
               Left            =   840
               TabIndex        =   46
               Tag             =   "C�d.Prov.C"
               Top             =   1320
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_B"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   840
               TabIndex        =   43
               Tag             =   "C�d.Prov.B"
               Top             =   960
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_A"
               Height          =   330
               HelpContextID   =   30104
               Index           =   3
               Left            =   840
               TabIndex        =   40
               Tag             =   "C�d.Prov.A"
               Top             =   600
               Width           =   930
            End
            Begin VB.Frame Frame3 
               Caption         =   "Producto de"
               ForeColor       =   &H00FF0000&
               Height          =   2055
               Left            =   240
               TabIndex        =   91
               Top             =   360
               Width           =   10215
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   0
                  Left            =   360
                  TabIndex        =   39
                  Top             =   240
                  Value           =   -1  'True
                  Width           =   255
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   1
                  Left            =   360
                  TabIndex        =   42
                  Top             =   600
                  Width           =   255
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   2
                  Left            =   360
                  TabIndex        =   45
                  Top             =   960
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   15
                  Left            =   1560
                  TabIndex        =   41
                  Tag             =   "Proveedor A"
                  Top             =   240
                  Width           =   8490
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   28
                  Left            =   1560
                  TabIndex        =   44
                  Tag             =   "Proveedor B"
                  Top             =   600
                  Width           =   8490
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   29
                  Left            =   1560
                  TabIndex        =   47
                  Tag             =   "Proveedor C"
                  Top             =   960
                  Width           =   8490
               End
               Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                  Bindings        =   "FR0092.frx":007C
                  Height          =   330
                  Index           =   0
                  Left            =   600
                  TabIndex        =   48
                  Top             =   1560
                  Visible         =   0   'False
                  Width           =   2130
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Row.Count       =   3
                  Row(0)          =   "Directo a Proveedor"
                  Row(1)          =   "Delegado 1"
                  Row(2)          =   "Delegado 2"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   3731
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  _ExtentX        =   3757
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
            End
            Begin TabDlg.SSTab SSTab2 
               Height          =   2175
               Left            =   -74880
               TabIndex        =   78
               Top             =   360
               Width           =   10335
               _ExtentX        =   18230
               _ExtentY        =   3836
               _Version        =   327681
               Style           =   1
               Tabs            =   4
               TabsPerRow      =   4
               TabHeight       =   520
               TabCaption(0)   =   "Directo a Proveedor"
               TabPicture(0)   =   "FR0092.frx":008E
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(1)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(3)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(7)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(29)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(30)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(31)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "lblLabel1(32)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "lblLabel1(33)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "lblLabel1(34)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtProv1(30)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtProv1(31)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtProv1(32)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtProv1(33)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "txtProv1(34)"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).Control(14)=   "txtProv1(35)"
               Tab(0).Control(14).Enabled=   0   'False
               Tab(0).Control(15)=   "txtProv1(36)"
               Tab(0).Control(15).Enabled=   0   'False
               Tab(0).Control(16)=   "txtProv1(37)"
               Tab(0).Control(16).Enabled=   0   'False
               Tab(0).Control(17)=   "txtProv1(38)"
               Tab(0).Control(17).Enabled=   0   'False
               Tab(0).ControlCount=   18
               TabCaption(1)   =   "Delegado 1"
               TabPicture(1)   =   "FR0092.frx":00AA
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "txtProv1(57)"
               Tab(1).Control(1)=   "txtProv1(47)"
               Tab(1).Control(2)=   "txtProv1(46)"
               Tab(1).Control(3)=   "txtProv1(45)"
               Tab(1).Control(4)=   "txtProv1(44)"
               Tab(1).Control(5)=   "txtProv1(43)"
               Tab(1).Control(6)=   "txtProv1(42)"
               Tab(1).Control(7)=   "txtProv1(41)"
               Tab(1).Control(8)=   "txtProv1(40)"
               Tab(1).Control(9)=   "txtProv1(39)"
               Tab(1).Control(10)=   "lblLabel1(53)"
               Tab(1).Control(11)=   "lblLabel1(43)"
               Tab(1).Control(12)=   "lblLabel1(42)"
               Tab(1).Control(13)=   "lblLabel1(41)"
               Tab(1).Control(14)=   "lblLabel1(40)"
               Tab(1).Control(15)=   "lblLabel1(39)"
               Tab(1).Control(16)=   "lblLabel1(38)"
               Tab(1).Control(17)=   "lblLabel1(37)"
               Tab(1).Control(18)=   "lblLabel1(36)"
               Tab(1).Control(19)=   "lblLabel1(35)"
               Tab(1).ControlCount=   20
               TabCaption(2)   =   "Delegado 2"
               TabPicture(2)   =   "FR0092.frx":00C6
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "txtProv1(48)"
               Tab(2).Control(1)=   "txtProv1(49)"
               Tab(2).Control(2)=   "txtProv1(50)"
               Tab(2).Control(3)=   "txtProv1(51)"
               Tab(2).Control(4)=   "txtProv1(52)"
               Tab(2).Control(5)=   "txtProv1(53)"
               Tab(2).Control(6)=   "txtProv1(55)"
               Tab(2).Control(7)=   "txtProv1(56)"
               Tab(2).Control(8)=   "txtProv1(58)"
               Tab(2).Control(9)=   "txtProv1(54)"
               Tab(2).Control(10)=   "lblLabel1(44)"
               Tab(2).Control(11)=   "lblLabel1(45)"
               Tab(2).Control(12)=   "lblLabel1(46)"
               Tab(2).Control(13)=   "lblLabel1(47)"
               Tab(2).Control(14)=   "lblLabel1(48)"
               Tab(2).Control(15)=   "lblLabel1(49)"
               Tab(2).Control(16)=   "lblLabel1(50)"
               Tab(2).Control(17)=   "lblLabel1(51)"
               Tab(2).Control(18)=   "lblLabel1(52)"
               Tab(2).Control(19)=   "lblLabel1(54)"
               Tab(2).ControlCount=   20
               TabCaption(3)   =   "Comentarios"
               TabPicture(3)   =   "FR0092.frx":00E2
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "txtProv1(59)"
               Tab(3).ControlCount=   1
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   57
                  Left            =   -74880
                  TabIndex        =   58
                  Tag             =   "Nombre2"
                  Top             =   540
                  Width           =   4605
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   47
                  Left            =   -70320
                  TabIndex        =   66
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   46
                  Left            =   -74880
                  TabIndex        =   64
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   45
                  Left            =   -68280
                  TabIndex        =   67
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3360
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   44
                  Left            =   -72360
                  TabIndex        =   65
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   43
                  Left            =   -66360
                  TabIndex        =   63
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   42
                  Left            =   -68280
                  TabIndex        =   62
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   1845
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   41
                  Left            =   -72360
                  TabIndex        =   61
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   40
                  Left            =   -70200
                  TabIndex        =   59
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   5295
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   39
                  Left            =   -74880
                  TabIndex        =   60
                  Tag             =   "Distrito1"
                  Top             =   1140
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   38
                  Left            =   4440
                  TabIndex        =   56
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   37
                  Left            =   120
                  TabIndex        =   54
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   36
                  Left            =   6600
                  TabIndex        =   57
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3240
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   35
                  Left            =   2280
                  TabIndex        =   55
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   34
                  Left            =   7080
                  TabIndex        =   53
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   2780
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   33
                  Left            =   4200
                  TabIndex        =   52
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   2800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   32
                  Left            =   120
                  TabIndex        =   51
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   31
                  Left            =   120
                  TabIndex        =   49
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   6015
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   30
                  Left            =   6240
                  TabIndex        =   50
                  Tag             =   "Distrito1"
                  Top             =   540
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   48
                  Left            =   -74880
                  TabIndex        =   68
                  Tag             =   "Nombre2"
                  Top             =   540
                  Width           =   4605
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   49
                  Left            =   -70320
                  TabIndex        =   75
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   50
                  Left            =   -74880
                  TabIndex        =   73
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   51
                  Left            =   -68280
                  TabIndex        =   76
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3360
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   52
                  Left            =   -72360
                  TabIndex        =   74
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   53
                  Left            =   -66360
                  TabIndex        =   92
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   55
                  Left            =   -72360
                  TabIndex        =   71
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   56
                  Left            =   -70200
                  TabIndex        =   69
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   5295
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   58
                  Left            =   -74880
                  TabIndex        =   70
                  Tag             =   "Distrito1"
                  Top             =   1140
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  Height          =   1530
                  Index           =   59
                  Left            =   -74880
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   77
                  Tag             =   "Comentarios"
                  Top             =   480
                  Width           =   10080
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   54
                  Left            =   -68280
                  TabIndex        =   72
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   1845
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   53
                  Left            =   -74880
                  TabIndex        =   121
                  Top             =   360
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   43
                  Left            =   -74880
                  TabIndex        =   120
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   42
                  Left            =   -70320
                  TabIndex        =   119
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   41
                  Left            =   -68280
                  TabIndex        =   118
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   40
                  Left            =   -72360
                  TabIndex        =   117
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   39
                  Left            =   -66360
                  TabIndex        =   116
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   38
                  Left            =   -68280
                  TabIndex        =   115
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   37
                  Left            =   -72360
                  TabIndex        =   114
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   36
                  Left            =   -74880
                  TabIndex        =   113
                  Top             =   960
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   35
                  Left            =   -70200
                  TabIndex        =   112
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   34
                  Left            =   120
                  TabIndex        =   111
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   33
                  Left            =   4440
                  TabIndex        =   110
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   32
                  Left            =   6600
                  TabIndex        =   109
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   2280
                  TabIndex        =   108
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   30
                  Left            =   7080
                  TabIndex        =   107
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   29
                  Left            =   4200
                  TabIndex        =   106
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   7
                  Left            =   120
                  TabIndex        =   105
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   3
                  Left            =   6240
                  TabIndex        =   104
                  Top             =   360
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   1
                  Left            =   120
                  TabIndex        =   103
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   44
                  Left            =   -74880
                  TabIndex        =   102
                  Top             =   360
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   45
                  Left            =   -74880
                  TabIndex        =   101
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   46
                  Left            =   -70320
                  TabIndex        =   100
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   47
                  Left            =   -68280
                  TabIndex        =   99
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   48
                  Left            =   -72360
                  TabIndex        =   98
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   -66360
                  TabIndex        =   97
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   -68280
                  TabIndex        =   96
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   -72360
                  TabIndex        =   95
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   52
                  Left            =   -74880
                  TabIndex        =   94
                  Top             =   960
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   54
                  Left            =   -70200
                  TabIndex        =   93
                  Top             =   360
                  Width           =   825
               End
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5025
            Index           =   0
            Left            =   -74880
            TabIndex        =   122
            TabStop         =   0   'False
            Top             =   90
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   8864
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Interno Far."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   5880
            TabIndex        =   145
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   9360
            TabIndex        =   143
            Top             =   120
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   8760
            TabIndex        =   142
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   8160
            TabIndex        =   141
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   7560
            TabIndex        =   140
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Bonif."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   6960
            TabIndex        =   139
            Top             =   1920
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   27
            Left            =   2160
            TabIndex        =   138
            Top             =   1920
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Rappel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   26
            Left            =   6000
            TabIndex        =   137
            Top             =   1920
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tama�o Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   5040
            TabIndex        =   136
            Top             =   720
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dias Seguridad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   3600
            TabIndex        =   135
            Top             =   720
            Width           =   1305
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Desc."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   5040
            TabIndex        =   134
            Top             =   1920
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Existencias"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   4080
            TabIndex        =   133
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�ximo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   2160
            TabIndex        =   132
            Top             =   1320
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�nimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   120
            TabIndex        =   131
            Top             =   1320
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Consumo A."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   9720
            TabIndex        =   130
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Consumo M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   8520
            TabIndex        =   129
            Top             =   720
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Consumo D."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   7440
            TabIndex        =   128
            Top             =   720
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pendiente Bonificar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   1800
            TabIndex        =   127
            Top             =   720
            Width           =   1680
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pendiente Entregar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   120
            TabIndex        =   126
            Top             =   720
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   125
            Top             =   120
            Width           =   1230
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Ultima Entrada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   124
            Top             =   1920
            Width           =   1860
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Mon."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   4200
            TabIndex        =   123
            Top             =   1920
            Width           =   435
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1575
      Left            =   0
      TabIndex        =   81
      Top             =   480
      Width           =   11655
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   0
         Left            =   6120
         TabIndex        =   9
         Tag             =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   1185
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   7320
         TabIndex        =   10
         Tag             =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   345
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   0
         Tag             =   "Texto a buscar"
         Top             =   480
         Width           =   5130
      End
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   5280
         TabIndex        =   82
         Top             =   240
         Width           =   5295
         Begin VB.OptionButton Option2 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3960
            TabIndex        =   4
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Prin.Activo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   1320
            TabIndex        =   2
            Top             =   240
            Width           =   1335
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   1
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Grp.Terap."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2640
            TabIndex        =   3
            Top             =   240
            Width           =   1335
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Buscar"
         Default         =   -1  'True
         Height          =   495
         Left            =   10680
         TabIndex        =   12
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   1800
         TabIndex        =   7
         Tag             =   "C�d. Grp. Terap�utico"
         Top             =   1080
         Width           =   1905
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Productos Bajo Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   8160
         TabIndex        =   11
         Top             =   1080
         Width           =   2265
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   3840
         TabIndex        =   8
         Tag             =   "Desviaci�n Mayor que"
         Top             =   1080
         Width           =   1905
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0092.frx":00FE
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   5
         Tag             =   "ABC(C)"
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0092.frx":0110
         Height          =   330
         Index           =   2
         Left            =   960
         TabIndex        =   6
         Tag             =   "ABC(CE)"
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Interno"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   6120
         TabIndex        =   144
         Top             =   840
         Width           =   1005
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   55
         Left            =   120
         TabIndex        =   87
         Top             =   240
         Width           =   1290
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(C)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   86
         Top             =   840
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(CE)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   960
         TabIndex        =   85
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Grp. Terap�utico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   1800
         TabIndex        =   84
         Top             =   840
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desviaci�n Mayor que"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   3840
         TabIndex        =   83
         Top             =   840
         Width           =   1905
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   79
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmBusMedFRPRD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA, COMPRAS                                          *
'* NOMBRE: FrmBusMed (FR0092.FRM)                                       *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Buscar Medicamentos                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Command1_Click()
Dim strTextoABuscar As String
Dim strCodGrpTerap As String
Dim strDesvMayQue As String
Dim strProdBajoStock As String
Dim strABC_C As String
Dim strABC_CE As String
Dim strclausulawhere As String
Dim strCodSeguridad As String
Dim strCodIntFar As String
  
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strDesvMayQue = txtBusq1(17).Text
  strProdBajoStock = chkBusq1(4).Value
  strABC_C = cboBusq1(1).Value
  strABC_CE = cboBusq1(2).Value
  strCodIntFar = txtBusq1(0).Text
  strCodSeguridad = txtBusq1(1).Text
  
  
   If strCodGrpTerap <> "" Then
      If Left(strCodGrpTerap, 1) = "Q" Then
         MsgBox "El Grupo Terape�tico no ha de comenzar por Q (Medicamentos)", vbInformation
         Exit Sub
      End If
   End If
  
   Command1.Enabled = False
   
   strclausulawhere = " -1 = -1 "
   If strTextoABuscar <> "" Then
      If Option2(0).Value = True Then
         strclausulawhere = strclausulawhere & " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & strTextoABuscar & "%') "
      ElseIf Option2(1).Value = True Then
         strclausulawhere = strclausulawhere & " AND (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400,FR6800 WHERE FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV AND UPPER(FR68DESPRINCACTIV) LIKE UPPER('" & strTextoABuscar & "%') )" & _
                 " OR " & _
                 "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400 WHERE FR68CODPRINCACTIV IN (SELECT FR68CODPRINCACTIV FROM FRH400 WHERE UPPER(FRH4DESSINONIMO) LIKE UPPER('" & strTextoABuscar & "%')))" & _
                 ") "
      ElseIf Option2(2).Value = True Then
         strclausulawhere = strclausulawhere & " AND FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE UPPER(FR00DESGRPTERAP) LIKE UPPER('" & strTextoABuscar & "%')) "
      ElseIf Option2(3).Value = True Then
          strclausulawhere = strclausulawhere & " AND ( "
          strclausulawhere = strclausulawhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('" & strTextoABuscar & "%')) "
          strclausulawhere = strclausulawhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('" & strTextoABuscar & "%')) "
          strclausulawhere = strclausulawhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('" & strTextoABuscar & "%')) "
          strclausulawhere = strclausulawhere & " ) "
      End If
   End If
  
  'If Option2(3).Value = False Then
  '   ' Proveedor
  '  If gstrllamador = "FrmGenPetOfe" Then
  '     strclausulawhere = strclausulawhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
  '  End If
  'End If
  
   If strCodGrpTerap <> "" Then
      strclausulawhere = strclausulawhere & " AND FR00CODGRPTERAP LIKE '" & strCodGrpTerap & "%' "
   Else
      strclausulawhere = strclausulawhere & " AND (FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL) "
   End If
  
   If strProdBajoStock = 1 Then
      strclausulawhere = strclausulawhere & " AND FR73INDCTRLLOTE=-1 "
   End If
  
   If strABC_C <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73ABCCONS='" & strABC_C & "' "
   End If

   If strABC_CE <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73ABCCONS='" & strABC_CE & "' "
   End If

   If strDesvMayQue <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73DESVIACION>" & strDesvMayQue & " "
   End If
   
  If strCodIntFar <> "" Then
    strclausulawhere = strclausulawhere & " AND FR73CODINTFAR LIKE '" & strCodIntFar & "%' "
  End If
  
  If strCodSeguridad <> "" Then
    strclausulawhere = strclausulawhere & " AND FR73CODINTFARSEG=" & strCodSeguridad & " "
  End If

   objWinInfo.objWinActiveForm.strWhere = strclausulawhere
   Call objWinInfo.DataRefresh

   Command1.Enabled = True
   
End Sub

Private Sub Form_Activate()
   gintprodbusFRPRD = ""
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Medicamentos"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR7300"
    
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    .strWhere = " (FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL) "
    'If gstrllamador = "FrmGenPetOfe" Then
    '  .strWhere = .strWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
    'End If
    
    Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno Medicamento", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento", cwString)
    Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno Medicamento")
    Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
      
    .CtrlGetInfo(txtText1(0)).blnInGrid = False

    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    .CtrlGetInfo(txtProv1(59)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(31)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(30)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(32)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(33)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(34)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(37)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(35)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(38)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(36)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(57)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(40)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(39)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(41)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(42)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(43)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(46)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(44)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(47)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(45)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(48)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(56)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(58)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(55)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(54)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(53)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(50)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(52)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(49)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(51)).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "FR79CODPROVEEDOR_A", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(15), "FR79PROVEEDOR")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR_B", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(28), "FR79PROVEEDOR")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR79CODPROVEEDOR_C", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(29), "FR79PROVEEDOR")
    
    Call .WinRegister
    Call .WinStabilize
  End With

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Medicamentos" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = ""
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
End Sub

Private Sub Option1_Click(Index As Integer)
Dim rstp As rdoResultset
Dim strp As String
Dim rdoQ As rdoQuery
  Select Case Index
  Case 0
    If txtText1(3).Text <> "" Then
      'bind variables
      strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strp)
      rdoQ(0) = txtText1(3).Text
      Set rstp = rdoQ.OpenResultset(strp)
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      rstp.Close
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 1
    If txtText1(2).Text <> "" Then
      'bind variables
      'strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(2).Text
      strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strp)
      rdoQ(0) = txtText1(2).Text
      Set rstp = rdoQ.OpenResultset(strp)
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      rstp.Close
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 2
    If txtText1(1).Text <> "" Then
      'bind variables
      'strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
      strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strp)
      rdoQ(0) = txtText1(1).Text
      Set rstp = rdoQ.OpenResultset(strp)
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      rstp.Close
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Vesi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rstp As rdoResultset
Dim strp As String
Dim rdoQ As rdoQuery

  gintIndice = intIndex
  Call objWinInfo.CtrlDataChange


  Select Case intIndex
  Case 3
    If txtText1(3).Text <> "" Then
      'bind variables
      'strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(3).Text
      strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strp)
      rdoQ(0) = txtText1(3).Text
      Set rstp = rdoQ.OpenResultset(strp)
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      rstp.Close
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 2
    If txtText1(2).Text <> "" Then
      'bind variables
      'strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(2).Text
      strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strp)
      rdoQ(0) = txtText1(2).Text
      Set rstp = rdoQ.OpenResultset(strp)
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      rstp.Close
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 1
    If txtText1(1).Text <> "" Then
      'bind variables
      'strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
      strp = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strp)
      rdoQ(0) = txtText1(1).Text
      Set rstp = rdoQ.OpenResultset(strp)
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      rstp.Close
      Set rstp = Nothing
    End If
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Command2_Click()
  gintprodbusFRPRD = txtText1(0).Text
  Unload Me
End Sub
