VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************
'LISTADOS

Const FRRepProveedores                As String = "FR0261" 'Proveedores
Const FRRepProtocolos                 As String = "FR0281" 'Protocolos
Const FRRepProdEspecialidad           As String = "FR0351" 'Producto: Especialidad
Const FRRepProdDatosClin              As String = "FR0352" 'Producto: Datos Cl�nicos
Const FRRepProdCompras                As String = "FR0353" 'Producto: Compras
Const FRRepProdAlmacen                As String = "FR0354" 'Producto: Almac�n
Const FRRepProdMaestro                As String = "FR0355" 'Producto: Maestro
Const FRRepProcesoFabric              As String = "FRR411" 'Proceso de Fabricaci�n
Const FRRepGrpProd                    As String = "FRR321" 'Grupos de Productos
Const FRRepMaestroConsAnual           As String = "FR0349" 'Grupos de Productos
Const FRRepProdMaestroUbi             As String = "FR0359" 'Producto: Maestro(por ubicaci�n)
'MANTENIMIENTO

Const PRWinMantProveedor              As String = "FR0026"
Const PRWinBusGrp                     As String = "FR0156"
Const PRWinBuscarProductos            As String = "FR0029"
'Const PRWinBuscarProtocolos           As String = "FR0031"
Const PRWinMantGrupoProducto          As String = "FR0032"
Const PRWinMantPrincAct               As String = "FR0054"
Const PRWinBuscarActividades          As String = "FR0065"
Const FRWinInv                        As String = "FR8842"


'DEFINIR
Const PRWinDefProducto                As String = "FR0035"
Const PRWinBuscarPrincipiosActivos    As String = "FR0053"
Const PRWinBuscadorPrincipiosActivos  As String = "FR0066"
Const PRWinDefSinonimo                As String = "FR0038"
Const PRWinDefSustituto               As String = "FR0036"
Const PRWinDefSustAlergia             As String = "FR0037"
Const PRWinDefProcFab                 As String = "FR0041"
Const PRWinDefProtocolo               As String = "FR0067"
Const PRWinBusGrpTeraPRD              As String = "FR0072"
Const PRWinBusProductosPRD            As String = "FR0073"

Const PRWINBusMedFRPRD                As String = "FR0092"
Const PRWINBusMatFRPRD                As String = "FR0093"

Const PRWINVerMasInformacion          As String = "FR0184"
Const PRWINBusProtocolosPRD           As String = "FR0185"
Const PRWINRestriccionesPRD           As String = "FR0186"
Const PRWINBusGrpProdPRD              As String = "FR0187"
Const PRWINConcatenarInsAdminPRD      As String = "FR0188"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    'MANTENIMIENTO
    Case PRWinMantProveedor
      Load frmMantProveedor
      'Call objsecurity.AddHelpContext(528)
      Call frmMantProveedor.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantProveedor
      Set frmMantProveedor = Nothing
    Case PRWinBuscarProductos
      Load frmBuscarProductos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarProductos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarProductos
      Set frmBuscarProductos = Nothing
    
    'Case PRWinBuscarProtocolos
    '  Load frmBuscarProtocolos
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmBuscarProtocolos.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmBuscarProtocolos
    '  Set frmBuscarProtocolos = Nothing
    
    Case PRWinMantGrupoProducto
      Load frmMantGrupoProducto
      'Call objsecurity.AddHelpContext(528)
      Call frmMantGrupoProducto.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantGrupoProducto
      Set frmMantGrupoProducto = Nothing
    Case PRWinMantPrincAct
      Load frmMantPrincAct
      'Call objsecurity.AddHelpContext(528)
      Call frmMantPrincAct.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantPrincAct
      Set frmMantPrincAct = Nothing
    Case PRWinBuscarActividades
       Load frmBuscarActividades
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarActividades.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBuscarActividades
      Set frmBuscarActividades = Nothing
    'DEFINIR
    Case PRWinDefProducto
      Load frmDefProducto
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProducto.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProducto
      Set frmDefProducto = Nothing
    Case PRWinBuscarPrincipiosActivos
      Load frmBuscarPrincipiosActivos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarPrincipiosActivos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarPrincipiosActivos
      Set frmBuscarPrincipiosActivos = Nothing
    Case PRWinBuscadorPrincipiosActivos
      Load frmBuscadorPrincipiosActivos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscadorPrincipiosActivos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscadorPrincipiosActivos
      Set frmBuscadorPrincipiosActivos = Nothing
    Case PRWinDefSinonimo
      Load frmDefSinonimo
      'Call objsecurity.AddHelpContext(528)
      Call frmDefSinonimo.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefSinonimo
      Set frmDefSinonimo = Nothing
    Case PRWinDefSustituto
      Load frmDefSustituto
      'Call objsecurity.AddHelpContext(528)
      Call frmDefSustituto.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefSustituto
      Set frmDefSustituto = Nothing
    Case PRWinDefSustAlergia
      Load frmDefSustAlergia
      'Call objsecurity.AddHelpContext(528)
      Call frmDefSustAlergia.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefSustAlergia
      Set frmDefSustAlergia = Nothing
    Case PRWinDefProcFab
      Load frmDefProcFab
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProcFab.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProcFab
      Set frmDefProcFab = Nothing
    Case PRWinDefProtocolo
      Load frmDefProtocolos
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProtocolos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProtocolos
      Set frmDefProtocolos = Nothing
    Case PRWinBusGrp
      Load frmBuscarGrTerapeuticos
      Call frmBuscarGrTerapeuticos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarGrTerapeuticos
      Set frmBuscarGrTerapeuticos = Nothing
    Case PRWinBusGrpTeraPRD
      Load frmBusGrpTeraPRD
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTeraPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpTeraPRD
      Set frmBusGrpTeraPRD = Nothing
    Case PRWinBusProductosPRD
      Load frmBusProductosPRD
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProductosPRD
      Set frmBusProductosPRD = Nothing
      
    Case PRWINBusMedFRPRD
      Load FrmBusMedFRPRD
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusMedFRPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmBusMedFRPRD
      Set FrmBusMedFRPRD = Nothing
    
    Case PRWINBusMatFRPRD
      Load FrmBusMatFRPRD
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusMatFRPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmBusMatFRPRD
      Set FrmBusMatFRPRD = Nothing
      
    Case PRWINVerMasInformacion
      Load frmVerMasInformacion
      'Call objsecurity.AddHelpContext(528)
      Call frmVerMasInformacion.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerMasInformacion
      Set frmVerMasInformacion = Nothing
      
    Case PRWINBusProtocolosPRD
      Load frmBusProtocolosPRD
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProtocolosPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProtocolosPRD
      Set frmBusProtocolosPRD = Nothing
      
    Case PRWINRestriccionesPRD
      Load frmRestriccionesPRD
      'Call objsecurity.AddHelpContext(528)
      Call frmRestriccionesPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRestriccionesPRD
      Set frmRestriccionesPRD = Nothing
      
    Case PRWINBusGrpProdPRD
      Load frmBusGrpProdPRD
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProdPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProdPRD
      Set frmBusGrpProdPRD = Nothing
      
    Case PRWINConcatenarInsAdminPRD
      Load frmConcatenarInsAdminPRD
      'Call objsecurity.AddHelpContext(528)
      Call frmConcatenarInsAdminPRD.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmConcatenarInsAdminPRD
      Set frmConcatenarInsAdminPRD = Nothing
    Case FRWinInv
      Load frmRealInvent
      'Call objsecurity.AddHelpContext(528)
      Call frmRealInvent.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRealInvent
      Set frmRealInvent = Nothing

    
  End Select
  Call ERR.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 120, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
  aProcess(23, 1) = PRWinMantProveedor
  aProcess(23, 2) = "Mantenimiento Proveedor"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinBuscarProductos
  aProcess(26, 2) = "Buscar Productos"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow
  
  aProcess(28, 1) = FRWinInv
  aProcess(28, 2) = "Realizar Inventario"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow
  
  aProcess(29, 1) = PRWinMantGrupoProducto
  aProcess(29, 2) = "Definir Grupos de Productos"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow
  
  aProcess(32, 1) = PRWinMantPrincAct
  aProcess(32, 2) = "Mantenimiento Principios Activos"
  aProcess(32, 3) = True
  aProcess(32, 4) = cwTypeWindow
  

  
  
    
  
  'DEFINIR
  aProcess(40, 1) = PRWinDefProcFab
  aProcess(40, 2) = "Definici�n de Proceso de Fabricaci�n"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeWindow
  
  aProcess(44, 1) = PRWinDefProducto
  aProcess(44, 2) = "Definir Productos"
  aProcess(44, 3) = True
  aProcess(44, 4) = cwTypeWindow
  
  aProcess(45, 1) = PRWinBuscarPrincipiosActivos
  aProcess(45, 2) = "Buscar Principios Activos"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeWindow
 
  aProcess(46, 1) = PRWinDefSinonimo
  aProcess(46, 2) = "Definici�n Sin�nimos"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeWindow
  
  aProcess(47, 1) = PRWinDefSustituto
  aProcess(47, 2) = "Definici�n Sustitutos"
  aProcess(47, 3) = False
  aProcess(47, 4) = cwTypeWindow
  
  aProcess(48, 1) = PRWinDefSustAlergia
  aProcess(48, 2) = "Definici�n Sustituci�n Alergias"
  aProcess(48, 3) = False
  aProcess(48, 4) = cwTypeWindow
  
  aProcess(58, 1) = PRWinBuscarActividades
  aProcess(58, 2) = "Buscar Actividades Proveedor"
  aProcess(58, 3) = False
  aProcess(58, 4) = cwTypeWindow
  
  aProcess(61, 1) = PRWinDefProtocolo
  aProcess(61, 2) = "Definici�n Protocolos"
  aProcess(61, 3) = True
  aProcess(61, 4) = cwTypeWindow
  
  aProcess(62, 1) = PRWinBuscadorPrincipiosActivos
  aProcess(62, 2) = "Buscador Principios Activos"
  aProcess(62, 3) = False
  aProcess(62, 4) = cwTypeWindow
  
  aProcess(66, 1) = PRWinBusGrpTeraPRD
  aProcess(66, 2) = "Buscador Grupos Terape�ticos"
  aProcess(66, 3) = False
  aProcess(66, 4) = cwTypeWindow
  
  aProcess(67, 1) = PRWinBusProductosPRD
  aProcess(67, 2) = "Buscador Productos"
  aProcess(67, 3) = False
  aProcess(67, 4) = cwTypeWindow

  aProcess(72, 1) = PRWinBusGrp
  aProcess(72, 2) = "Buscar Grupos Terap�uticos"
  aProcess(72, 3) = False
  aProcess(72, 4) = cwTypeWindow

  aProcess(73, 1) = PRWINBusMedFRPRD
  aProcess(73, 2) = "Buscar Medicamentos Productos"
  aProcess(73, 3) = False
  aProcess(73, 4) = cwTypeWindow

  aProcess(74, 1) = PRWINBusMatFRPRD
  aProcess(74, 2) = "Buscar Material Productos"
  aProcess(74, 3) = False
  aProcess(74, 4) = cwTypeWindow
  
  aProcess(75, 1) = PRWINVerMasInformacion
  aProcess(75, 2) = "Ver M�s Informaci�n"
  aProcess(75, 3) = False
  aProcess(75, 4) = cwTypeWindow
  
  aProcess(76, 1) = PRWINBusProtocolosPRD
  aProcess(76, 2) = "Buscar Protocolos"
  aProcess(76, 3) = False
  aProcess(76, 4) = cwTypeWindow
  
  aProcess(77, 1) = PRWINRestriccionesPRD
  aProcess(77, 2) = "Restricciones"
  aProcess(77, 3) = False
  aProcess(77, 4) = cwTypeWindow
  
  aProcess(78, 1) = PRWINBusGrpProdPRD
  aProcess(78, 2) = "Buscar Grupos de Productos"
  aProcess(78, 3) = False
  aProcess(78, 4) = cwTypeWindow
  
  aProcess(79, 1) = PRWINConcatenarInsAdminPRD
  aProcess(79, 2) = "Instruccciones de Administraci�n"
  aProcess(79, 3) = False
  aProcess(79, 4) = cwTypeWindow
  'Informes
  
  aProcess(93, 1) = FRRepProveedores
  aProcess(93, 2) = "Proveedores"
  aProcess(93, 3) = False
  aProcess(93, 4) = cwTypeReport
  
  aProcess(94, 1) = FRRepProtocolos
  aProcess(94, 2) = "Protocolos"
  aProcess(94, 3) = False
  aProcess(94, 4) = cwTypeReport
  
  aProcess(95, 1) = FRRepProdEspecialidad
  aProcess(95, 2) = "Producto: Especialidad"
  aProcess(95, 3) = False
  aProcess(95, 4) = cwTypeReport
  
  aProcess(96, 1) = FRRepProdDatosClin
  aProcess(96, 2) = "Producto: Datos Cl�nicos"
  aProcess(96, 3) = False
  aProcess(96, 4) = cwTypeReport
  
  aProcess(97, 1) = FRRepProdCompras
  aProcess(97, 2) = "Producto: Compras"
  aProcess(97, 3) = False
  aProcess(97, 4) = cwTypeReport
  
  aProcess(98, 1) = FRRepProdAlmacen
  aProcess(98, 2) = "Producto: Almac�n"
  aProcess(98, 3) = False
  aProcess(98, 4) = cwTypeReport

  aProcess(102, 1) = FRRepProcesoFabric
  aProcess(102, 2) = "Proceso de Fabricaci�n"
  aProcess(102, 3) = False
  aProcess(102, 4) = cwTypeReport

  aProcess(104, 1) = FRRepGrpProd
  aProcess(104, 2) = "Grupos de Productos"
  aProcess(104, 3) = False
  aProcess(104, 4) = cwTypeReport
  
  aProcess(105, 1) = FRRepProdMaestro
  aProcess(105, 2) = "Maestro de Productos"
  aProcess(105, 3) = False
  aProcess(105, 4) = cwTypeReport
  
  aProcess(106, 1) = FRRepMaestroConsAnual
  aProcess(106, 2) = "Consumo Anual de Comprimidos"
  aProcess(106, 3) = False
  aProcess(106, 4) = cwTypeReport
  
  aProcess(107, 1) = FRRepProdMaestroUbi
  aProcess(107, 2) = "Maestro de Productos por Ubicaci�n"
  aProcess(107, 3) = False
  aProcess(107, 4) = cwTypeReport

  
End Sub

