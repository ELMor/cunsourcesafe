VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBusGrpTeraPRD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Grupo Terape�tico."
   ClientHeight    =   8220
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11805
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0072.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8220
   ScaleWidth      =   11805
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   7200
      TabIndex        =   6
      Top             =   7320
      Width           =   2295
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Index           =   2
      Left            =   4920
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   6975
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6135
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   6810
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         MaxSelectedRows =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   12012
         _ExtentY        =   10821
         _StockProps     =   79
         Caption         =   "MEDICAMENTOS"
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Grupos Terape�ticos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7200
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6495
         Index           =   0
         Left            =   360
         TabIndex        =   3
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   11456
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         OLEDragMode     =   1
         OLEDropMode     =   1
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7935
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusGrpTeraPRD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusGrpTera(FR0153.FRM)                                    *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: FEBRERO  1999                                                 *
'* DESCRIPCION: Buscar  Grupos Terapeuticos                             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
'* NOTAS :                                                              *
'* Se utiliza en definir protocolos,                                    *
'* los grids solo deja escoger 1                                        *
'* pues solo se puede coger un medicamento a la vez                        *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
' Declara variables globales.
Dim Enarbol As Boolean
Dim EnPostWrite As Boolean
Dim indrag As Boolean 'Indicador de operaci�n de arrastrar y colocar.
Dim nodX As Object ' Elemento que se arrastra.
Dim arbolvacio As Boolean
Dim blnnoactivate As Boolean

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub rellenar_TreeView()
Dim strgrupo As String
Dim rstgrupo As rdoResultset
Dim vntPadre As Variant
Dim vntNodoActual As Variant
Dim vntNodoActualDesc As Variant
   
  
   tvwItems(0).Nodes.Clear
   
  If gstrLlamadorProd = "frmDefProtocolosGT" Then
   strgrupo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP NOT LIKE 'Q%' ORDER BY LENGTH(FR00CODGRPTERAP), FR00CODGRPTERAP"
  Else
   strgrupo = "SELECT * FROM FR0000 ORDER BY LENGTH(FR00CODGRPTERAP), FR00CODGRPTERAP"
  End If
   
   Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
  
   While rstgrupo.EOF = False
      arbolvacio = False
      vntNodoActual = rstgrupo("FR00CODGRPTERAP").Value
      vntNodoActualDesc = rstgrupo("FR00DESGRPTERAP").Value
      vntPadre = rstgrupo("FR00CODGRPTERAP_PAD").Value
      Debug.Print vntPadre & "  " & vntNodoActual
      Call tvwItems(0).Nodes.Add(vntPadre, tvwChild, vntNodoActual, vntNodoActual & ".-" & vntNodoActualDesc)
      rstgrupo.MoveNext
   Wend

End Sub



Private Sub cmdaceptar_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim rstrest As rdoResultset
Dim strrest As String
Dim strinsert As String
    
'Guardamos el n�mero de filas seleccionadas
mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
ReDim gintprodbuscado(mintNTotalSelRows, 8)
gintprodtotal = mintNTotalSelRows
For mintisel = 0 To mintNTotalSelRows - 1
    'Guardamos el n�mero de fila que est� seleccionada
     mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
     gintprodbuscado(mintisel, 0) = grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk) 'c�digo
     gintprodbuscado(mintisel, 1) = grdDBGrid1(1).Columns(4).CellValue(mvarBkmrk) 'c�digo interno
     gintprodbuscado(mintisel, 2) = grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk) 'descripci�n
     gintprodbuscado(mintisel, 3) = grdDBGrid1(1).Columns(6).CellValue(mvarBkmrk) 'Dosis
     gintprodbuscado(mintisel, 4) = grdDBGrid1(1).Columns(7).CellValue(mvarBkmrk) 'U.M
     'gintprodbuscado(mintisel, 5) = grdDBGrid1(1).Columns(8).CellValue(mvarBkmrk) 'Forma
     gintprodbuscado(mintisel, 6) = grdDBGrid1(1).Columns(8).CellValue(mvarBkmrk) 'Volumen
     gintprodbuscado(mintisel, 7) = grdDBGrid1(1).Columns(9).CellValue(mvarBkmrk) 'Via
     
     '----------------------------------------------------------------------------------
       ''para cada producto hay que mirar si es de uso restringido
       'stra = "SELECT FR73INDUSOREST FROM FR7300 WHERE FR73CODPRODUCTO=" & _
       '        grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
       'Set rsta = objApp.rdoConnect.OpenResultset(stra)
       'If rsta.rdoColumns(0).Value = -1 Then
       '   'si es de uso restringido se llama a la pantalla de restricciones,pero antes
       '   'se vuelca el contenido de FRA900 en FRA400
       '       stra = "SELECT * FROM FRA900 WHERE FR73CODPRODUCTO=" & _
       '              grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
       '       Set rsta = objApp.rdoConnect.OpenResultset(stra)
       '       While Not rsta.EOF
       '           strrest = "SELECT FRA9CODREST FROM FRA400 WHERE FR73CODPRODUCTO=" & _
       '                     grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk) & " AND " & _
       '                     "FR66CODPETICION=" & glngcodpeticion & " AND " & _
       '                     "FRA9CODREST=" & rsta.rdoColumns("FRA9CODREST").Value
       '           Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
       '           If rstrest.EOF Then
       '             strinsert = "INSERT INTO FRA400(FR73CODPRODUCTO,FR66CODPETICION," & _
       '                       "FRA9CODREST,FRA4DESCREST,FRA4INDSELECCIONADO) VALUES(" & _
       '                       grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk) & "," & _
       '                       glngcodpeticion & "," & _
       '                       rsta.rdoColumns("FRA9CODREST").Value & "," & _
       '                       "'" & rsta.rdoColumns("FRA9DESREST").Value & "'" & "," & _
       '                       "0" & ")"
       '             objApp.rdoConnect.Execute strinsert, 64
       '             objApp.rdoConnect.Execute "Commit", 64
       '           End If
       '           rstrest.Close
       '           Set rstrest = Nothing
       '       rsta.MoveNext
       '       Wend
       '       rsta.Close
       '       Set rsta = Nothing
       '       glngcodprod = grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
       '       Call objsecurity.LaunchProcess("FR0182")
       'End If
       '---------------------------------------------------------------------------------
     
Next mintisel

Unload Me

End Sub

Private Sub Form_Activate()
If blnnoactivate = False Then
    blnnoactivate = True
    grdDBGrid1(1).Columns(0).Visible = False
    If arbolvacio = False Then
      tvwItems(0).Nodes(1).Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Productos"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR7300"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    
        strKey = .strDataBase & .strTable
        
        If gstrLlamadorProd = "frmDefProtocolosGT" Then
          .strWhere = " (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
                      " and (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL ) "
        End If
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Productos")
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Medicamento", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�d.Int", cwString)
        Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento", cwString)
        Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwDecimal)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA_USU", "U.M", cwString)
        'Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR_USU", "Forma Far.", cwString)
        Call .FormAddFilterWhere(strKey, "FR73VOLUMEN", "Volumen", cwDecimal)
        Call .FormAddFilterWhere(strKey, "FR34CODVIA", "V�a", cwString)
        
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Medicamento")
        Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento")

    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Medicamento", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "C.I", "FR73CODINTFAR", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Medicamento", "FR73DESPRODUCTO", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "Dosis", "FR73DOSIS", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "U.M", "FR93CODUNIMEDIDA_USU", cwString, 5)
        'Call .GridAddColumn(objMultiInfo, "Forma Far.", "FRH7CODFORMFAR_USU", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "Volumen", "FR73VOLUMEN", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)

        Call .FormCreateInfo(objMultiInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
  arbolvacio = True
    
  Call rellenar_TreeView
  
  grdDBGrid1(1).Columns(3).Visible = False
  grdDBGrid1(1).Columns(4).Width = 700  'c�d.interno
  grdDBGrid1(1).Columns(5).Width = 3000 'medicamento
  grdDBGrid1(1).Columns(6).Width = 600 'dosis
  grdDBGrid1(1).Columns(7).Width = 500 'U.M
  grdDBGrid1(1).Columns(8).Width = 750 'volumen
  grdDBGrid1(1).Columns(9).Width = 650 'v�a

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch


  If strCtrl = "grdDBGrid1(1).C�d. Grupo Terape�tico Padre" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR0000"
     .strOrder = "ORDER BY FR00CODGRPTERAP ASC"
     .strWhere = "WHERE FR00CODGRPTERAP LIKE '" & Left(grdDBGrid1(1).Columns(3).Value, 1) & "%'"
         
     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�digo Grupo"
         
     Set objField = .AddField("FR00DESGRPTERAP")
     objField.strSmallDesc = "Descripci�n Grupo"
         
     If .Search Then
        Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), .cllValues("FR00CODGRPTERAP"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  Select Case btnButton.Index
  Case 21 'Primero
    If arbolvacio = False Then
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
    Exit Sub
  Case 22 'Anterior
    If arbolvacio = False Then
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    End If
    Exit Sub
  Case 23 'Siguiente
    If arbolvacio = False Then
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    End If
    Exit Sub
  Case 24 'Ultimo
    If arbolvacio = False Then
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
    Exit Sub
  Case Else 'Otro boton
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)

  Select Case intIndex
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_DragDrop(Index As Integer, Source As Control, x As Single, y As Single)
    If tvwItems(0).DropHighlight Is Nothing Then
        Set tvwItems(0).DropHighlight = Nothing
        indrag = False
        Exit Sub
    Else
        If nodX = tvwItems(0).DropHighlight Then Exit Sub

Cls
        Print nodX.Text & " colocado en " & tvwItems(0).DropHighlight.Text
        Set tvwItems(0).DropHighlight = Nothing
        indrag = False
    End If
End Sub



Private Sub tvwItems_DragOver(Index As Integer, Source As Control, x As Single, y As Single, State As Integer)
    If indrag = True Then
        ' Establece las coordenadas del mouse en
        ' DropHighlight.
        Set tvwItems(0).DropHighlight = tvwItems(0).HitTest(x, y)
    End If
End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  If EnPostWrite = True Then
    EnPostWrite = False
    Exit Sub
  End If
        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)
End Sub


Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim aux As Variant
Dim aux2 As Variant

On Error GoTo ERR:
    Enarbol = True
    
    aux2 = Me.MousePointer
    Me.MousePointer = vbHourglass
    'aux = Right(Node.Key, Len(Node.Key) - 4)
    'While InStr(aux, "/") <> 0
    '    aux = Right(aux, Len(aux) - InStr(aux, "/"))
    'Wend
    
    'objWinInfo.objWinActiveForm.strWhere = "FR00CODGRPTERAP='" & aux & "'"
    If gstrLlamadorProd = "frmDefProtocolosGT" Then
      objWinInfo.objWinActiveForm.strWhere = "FR00CODGRPTERAP LIKE '" & Node.Key & "%'"
      objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " and (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
                  " and (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL ) "
    Else
      objWinInfo.objWinActiveForm.strWhere = "FR00CODGRPTERAP='" & Node.Key & "'"
    End If
    objWinInfo.DataRefresh
ERR:
    Me.MousePointer = aux2
    Enarbol = False

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub




