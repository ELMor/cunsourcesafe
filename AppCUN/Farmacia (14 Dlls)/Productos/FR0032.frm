VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmMantGrupoProducto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definir Grupos de Productos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdbuscar 
      Caption         =   "Seleccionar Productos"
      Height          =   375
      Left            =   10080
      TabIndex        =   20
      Top             =   5280
      Width           =   1815
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Grupo de Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   240
      TabIndex        =   8
      Top             =   480
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   0
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0032.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(14)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(5)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(4)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0032.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FRI7CODTIPGRP"
            Height          =   330
            Index           =   4
            Left            =   6240
            TabIndex        =   6
            Tag             =   "Tipo de Grupo"
            Top             =   1800
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   3
            Left            =   7080
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Descripci�n del Grupo"
            Top             =   1800
            Width           =   3720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   5
            Left            =   720
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   4
            Tag             =   "C�digo Servicio"
            Top             =   1800
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR41DESGRUPPROD"
            Height          =   330
            Index           =   1
            Left            =   1800
            MultiLine       =   -1  'True
            TabIndex        =   1
            Tag             =   "Descripci�n Grupo Producto"
            Top             =   360
            Width           =   9000
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR41CODGRUPPROD"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Grupo Producto"
            Top             =   360
            Width           =   1140
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2025
            Index           =   2
            Left            =   -74880
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   3572
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR41FECINIVIGGP"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   2
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1080
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR41FECFINVIGGP"
            Height          =   330
            Index           =   1
            Left            =   2640
            TabIndex        =   3
            Tag             =   "Fecha Fin Vigencia"
            Top             =   1080
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   6240
            TabIndex        =   22
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   21
            Top             =   1560
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   19
            Top             =   840
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   2640
            TabIndex        =   18
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Grupo Prod"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   240
            TabIndex        =   17
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Grupo Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1800
            TabIndex        =   16
            Top             =   120
            Width           =   2535
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Index           =   0
      Left            =   240
      TabIndex        =   10
      Top             =   3360
      Width           =   9735
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4185
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   9480
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16722
         _ExtentY        =   7382
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantGrupoProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantGrupoProducto (FR0032.FRM)                            *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: mantenimiento Grupo Productos                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


'Private Sub cboSSDBCombo1_CloseUp(Index As Integer)
'txtText1(4).Text = cboSSDBCombo1(0).Text
'End Sub

Private Sub cmdbuscar_Click()
Dim i As Integer
'Dim rsta As rdoResultset
'Dim stra As String
'Dim rstgrupo As rdoResultset
'Dim strgrupo As String
Dim v As Integer
Dim j As Integer
Dim insertarfila As Boolean
Dim mensaje As String
Dim noinsertar As Boolean
'Dim rstServicio As rdoResultset
'Dim strServicio As String
'Dim Servicio As Integer

If txtText1(0).Text <> "" And tlbToolbar1.Buttons(4).Enabled = False Then
Else
  MsgBox "Debe de tener definido un Grupo de Productos para poder a�adirle Productos", vbInformation, "Aviso"
  Exit Sub
End If

cmdbuscar.Enabled = False

Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
Call objsecurity.LaunchProcess("FR0029")
Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
noinsertar = True
insertarfila = True
'si el servicio introducido es Farmacia(c�digo 1) y hay valores en Tiempo de
'estabilidad y Tipo de medida
'If txtText1(2).Text = 1 And txtText1(3).Text <> "" And _
'   txtText1(4).Text <> "" And txtText1(0).Text <> "" Then
If txtText1(0).Text <> "" Then
  For v = 0 To gintprodtotal - 1
    'strServicio = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=3"
    'Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
    'Servicio = rstServicio("FRH2PARAMGEN").Value
    'rstServicio.Close
    'Set rstServicio = Nothing
    '
    'stra = "SELECT * FROM FR0900 WHERE FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
    'Set rsta = objApp.rdoConnect.OpenResultset(stra)
    'Do While Not rsta.EOF
    '    strgrupo = "SELECT count(*) FROM FR4100 WHERE " & _
    '             "FR41CODGRUPPROD='" & rsta.rdoColumns("FR41CODGRUPPROD").Value & "' AND " & _
    '             "FR41CODGRUPPROD<>'" & txtText1(0).Text & "' AND " & _
    '             "AD02CODDPTO=" & Servicio
    '             '& " AND " & _
    '             '"FR41NUMESTAB IS NOT NULL AND FR41TIPMEDIDA IS NOT NULL"
    '    Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
    '    If rstgrupo.rdoColumns(0).Value > 0 Then 'existe un grupo
    '        insertarfila = False
    '    Else
    '        insertarfila = True
    '    End If
    '    rstgrupo.Close
    '    Set rstgrupo = Nothing
    'rsta.MoveNext
    'Loop
    If insertarfila = True Then
      'se inserta el producto en el grid
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(0).Rows > 0 Then
        grdDBGrid1(0).MoveFirst
        For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(3).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
               'no se inserta el producto pues ya est� insertado en el grid
                noinsertar = False
                Exit For
            Else
                noinsertar = True
            End If
        grdDBGrid1(0).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
        grdDBGrid1(0).Columns(3).Value = gintprodbuscado(v, 0)
        grdDBGrid1(0).Columns(4).Value = gintprodbuscado(v, 1)
        grdDBGrid1(0).Columns(5).Value = txtText1(0).Text
      Else
        mensaje = MsgBox("El producto: " & gintprodbuscado(v, 1) & " ya est� guardado." & _
                       Chr(13), vbInformation)
      End If
      noinsertar = True
    Else
      mensaje = MsgBox("El producto: " & gintprodbuscado(v, 1) & " ya forma parte de un grupo." & _
                       Chr(13), vbInformation)
    End If
    insertarfila = True
    'rsta.Close
    'Set rsta = Nothing
  Next v
  
  gintprodtotal = 0

End If


cmdbuscar.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Grupos de Productos"
      
    .strTable = "FR4100"
    
'    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR4100.FR41CODGRUPPROD", cwAscending)
    
    Call .objPrinter.Add("FRR321", "Grupos de Productos")
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Grupos de Productos")
    Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "C�digo Grupo Producto", cwString)
    Call .FormAddFilterWhere(strKey, "FR41DESGRUPPROD", "Descripci�n Grupo Producto", cwString)
    Call .FormAddFilterWhere(strKey, "FR41FECINIVIGGP", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR41FECFINVIGGP", "Fecha Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR41INDPRIVGP", "Indicador Privado", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR41NUMESTAB", "Tiempo Estabilidad", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR41TIPMEDIDA", "Tipo Medida", cwString)

    Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Producto")
    Call .FormAddFilterOrder(strKey, "FR41DESGRUPPROD", "Descripci�n Grupo Producto")
    Call .FormAddFilterOrder(strKey, "FR41FECINIVIGGP", "Fecha Inicio Vigencia")
    Call .FormAddFilterOrder(strKey, "FR41FECFINVIGGP", "Fecha Fin Vigencia")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")
    'Call .FormAddFilterOrder(strKey, "FR41INDPRIVGP", "Indicador Privado")


  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Productos"
    
    .strTable = "FR0900"
    
    .blnHasMaint = True
    .intCursorSize = 0
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR41CODGRUPPROD", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d.Producto", cwNumeric)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d. Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "C�d. Grupo Producto", "FR41CODGRUPPROD", cwString, 10)
    
    Call .GridAddColumn(objMultiInfo, "C�d.Int.Farmacia", "", cwString, 6) ' FR73CODINTFAR
    Call .GridAddColumn(objMultiInfo, "Seguridad", "", cwNumeric, 1) ' FR73CODINTFARSEG
    Call .GridAddColumn(objMultiInfo, "F.F.", "", cwString, 3) ' FRH7CODFORMFAR
    Call .GridAddColumn(objMultiInfo, "D�sis", "", cwString, 3) ' FR73DOSIS
    Call .GridAddColumn(objMultiInfo, "U.M.", "", cwString, 5) ' FR93CODUNIMEDIDA
    Call .GridAddColumn(objMultiInfo, "Via", "", cwString, 3) ' FR34CODVIA
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15) ' FR73REFERENCIA
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL))) AND AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "AD02DESDPTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "FRI7CODTIPGRP", "SELECT FRI7DESTIPGRP FROM FRI700 WHERE FRI7CODTIPGRP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(3), "FRI7DESTIPGRP")

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(4), "FR73DESPRODUCTO")
    
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(7), "FR73CODINTFARSEG")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(8), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(9), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(10), "FR93CODUNIMEDIDA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(11), "FR34CODVIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(12), "FR73REFERENCIA")
        
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    
    grdDBGrid1(0).Columns(2).Width = 0
    grdDBGrid1(0).Columns(2).Visible = False
    grdDBGrid1(0).Columns(5).Visible = False
    
    ' C�digo Producto
    grdDBGrid1(0).Columns(3).Visible = False
    ' Descripci�n Producto
    grdDBGrid1(0).Columns(4).Width = 20 * 120
    ' Cod int farm
    grdDBGrid1(0).Columns(6).Width = 16 * 120
    ' Seguridad
    grdDBGrid1(0).Columns(7).Width = 2 * 120
    
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
   
   Dim q%
   For q% = 2 To grdDBGrid1(0).Cols - 1
      Debug.Print q% & " - " & grdDBGrid1(0).Columns(q%).Caption
   Next q%

   
   

  'txtText1(4).ZOrder (0)
  'cboSSDBCombo1(0).AddItem "Minutos"
  'cboSSDBCombo1(0).AddItem "Horas"
  'cboSSDBCombo1(0).AddItem "D�as"
  'cboSSDBCombo1(0).AddItem "Semanas"
  'cboSSDBCombo1(0).AddItem "Meses"
  'cboSSDBCombo1(0).AddItem "A�os"
Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

  If txtText1(0).Text <> "" Then
    cmdbuscar.Enabled = True
  Else
    cmdbuscar.Enabled = False
  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Grupos de Productos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "Grupos de Productos" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
     
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
   If strFormName = "Grupos de Productos" And strCtrl = "txtText1(4)" Then
      Set objSearch = New clsCWSearch
      With objSearch
         .strTable = "FRI700"
'     .strWhere = "WHERE  AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
'     .strOrder = "ORDER BY AD02CODDPTO ASC"
     
         Set objField = .AddField("FRI7CODTIPGRP")
         objField.strSmallDesc = "C�digo de Tipo de Grupo"
         
         Set objField = .AddField("FRI7DESTIPGRP")
         objField.strSmallDesc = "Descripci�n de Tipo de Grupo"
         
         If .Search Then
            Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FRI7CODTIPGRP"))
         End If
      End With
      Set objSearch = Nothing
  End If
 
 
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)

Call objsecurity.LaunchProcess("FR0035")

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim sqlstr As String

  If txtText1(2).Text <> "" Then
    If Not IsNumeric(txtText1(2).Text) Then
        MsgBox "El C�d.Servicio es incorrecto.", vbExclamation
        blnCancel = True
        Exit Sub
    Else
      sqlstr = "SELECT * FROM AD0200 WHERE  AD02CODDPTO=" & txtText1(2).Text
      sqlstr = sqlstr & " AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If rsta.EOF = True Then
        MsgBox "El C�d.Servicio es incorrecto.", vbExclamation
        blnCancel = True
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
'Dim rsta As rdoResultset
'Dim sqlstr As String

If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Productos" Then
    MsgBox "S�lo puede a�adir productos mediante el buscador", vbExclamation
   Exit Sub
End If

'If btnButton.Index = 2 Then
'        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'        sqlstr = "SELECT FR41CODGRUPPROD_SEQUENCE.nextval FROM dual"
'        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
'        'SendKeys ("{TAB}")
'        rsta.Close
'        Set rsta = Nothing
'Else
'        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'End If

   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)


End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
'Dim rsta As rdoResultset
'Dim sqlstr As String

If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Productos" Then
    MsgBox "S�lo puede a�adir productos mediante el buscador", vbExclamation
   Exit Sub
End If

'If intIndex = 10 Then
'        sqlstr = "SELECT FR41CODGRUPPROD_SEQUENCE.nextval FROM dual"
'        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
'        'txtText1(0).Text = rsta.rdoColumns(0).Value
'        'SendKeys ("{TAB}")
'        rsta.Close
'        Set rsta = Nothing
'Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
'End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
Dim mensaje As String
If intIndex = 1 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
        dtcDateCombo1(1).Text = ""
    End If
  End If
End If
If intIndex = 0 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
        dtcDateCombo1(0).Text = ""
    End If
  End If
End If
  
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
Dim mensaje As String
If intIndex = 1 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
        dtcDateCombo1(1).Text = ""
    End If
  End If
End If
If intIndex = 0 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
        dtcDateCombo1(0).Text = ""
    End If
  End If
End If
Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


