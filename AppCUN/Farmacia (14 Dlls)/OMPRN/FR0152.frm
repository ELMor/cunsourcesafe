VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmSelPaciente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Seleccionar Paciente"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Procesos/Asistencias"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Index           =   2
      Left            =   120
      TabIndex        =   17
      Tag             =   "Actuaciones Asociadas"
      Top             =   3360
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3495
         Index           =   1
         Left            =   120
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "PRN"
         stylesets(0).BackColor=   65535
         stylesets(0).Picture=   "FR0152.frx":0000
         stylesets(1).Name=   "OM"
         stylesets(1).BackColor=   16776960
         stylesets(1).Picture=   "FR0152.frx":001C
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20055
         _ExtentY        =   6165
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Farmacia"
      Height          =   615
      Left            =   9120
      Picture         =   "FR0152.frx":0038
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton cmdPRN 
      Caption         =   "PRN"
      Height          =   375
      Left            =   6120
      TabIndex        =   7
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdOManteriores 
      Caption         =   "OM"
      Height          =   375
      Left            =   3240
      TabIndex        =   6
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   11685
      Begin TabDlg.SSTab tabTab1 
         Height          =   2100
         Index           =   0
         Left            =   240
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   480
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   3704
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0152.frx":05BA
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(7)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "IdPersona1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(11)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(6)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0152.frx":05D6
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   9480
            TabIndex        =   14
            Tag             =   "Edad"
            Top             =   360
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   11
            Left            =   10200
            TabIndex        =   13
            Tag             =   "Cod Cama"
            Top             =   360
            Visible         =   0   'False
            Width           =   840
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   10
            Left            =   9000
            TabIndex        =   10
            Tag             =   "Departamento"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1560
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   9
            Left            =   9000
            TabIndex        =   9
            Tag             =   "Categoria"
            Top             =   240
            Visible         =   0   'False
            Width           =   840
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   8
            Left            =   9000
            TabIndex        =   8
            Tag             =   "Cod Dpto"
            Top             =   960
            Visible         =   0   'False
            Width           =   1560
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22FECNACIM"
            Height          =   330
            Index           =   7
            Left            =   9000
            TabIndex        =   0
            Top             =   600
            Visible         =   0   'False
            Width           =   1560
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1785
            Index           =   0
            Left            =   -74760
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   240
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   3149
            _StockProps     =   79
            Caption         =   "PACIENTES"
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1455
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Width           =   10305
            _ExtentX        =   18177
            _ExtentY        =   2566
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Edad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   9480
            TabIndex        =   16
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   10200
            TabIndex        =   15
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSelPaciente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmSelPaciente (FR0152.FRM)                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: FEBRERO DE 1999                                               *
'* DESCRIPCION: selecci�n de paciente                                   *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdPRN_Click()
  cmdPRN.Enabled = False
  If grdDBGrid1(1).Rows > 0 Then
    If Len(Trim(grdDBGrid1(1).Columns("Asistencia").Value)) > 0 Then
      If Len(Trim(grdDBGrid1(1).Columns("Proceso").Value)) > 0 Then
        'Se puede redactar OM
      Else
        MsgBox "El paciente no tiene Proceso", vbInformation, "Seleccionar Paciente"
        cmdOManteriores.Enabled = True
        Exit Sub
      End If
    Else
      MsgBox "El paciente no tiene Asistencia", vbInformation, "Seleccionar Paciente"
      cmdOManteriores.Enabled = True
      Exit Sub
    End If
  Else
    MsgBox "El paciente no tiene Proceso ni Asistencia", vbInformation, "Seleccionar Paciente"
    cmdOManteriores.Enabled = True
    Exit Sub
  End If
  
  If IdPersona1.Text <> "" Then
    'se pasa el c�digo de paciente
    glngselpaciente = IdPersona1.Text
    gblnSelPaciente = True
    gstrLlamadorProd = "SelPacServEspeciales"
    Call objsecurity.LaunchProcess("FR0171")
    gstrLlamadorProd = ""
    gblnSelPaciente = False
  Else
    Call MsgBox("No hay ning�n paciente seleccionado", vbCritical, "Aviso")
  End If
  cmdPRN.Enabled = True
End Sub



Private Sub Command1_Click()
  Call Farmacia(IdPersona1.Text)
  
End Sub
Private Sub Farmacia(ByVal lngCodPersona As Long)
  Dim vntDatos(1)
  Dim strCat As String
  Dim rstCat As rdoResultset
  
  vntDatos(1) = lngCodPersona         'Codigo Paciente
  strCat = "select * from sg0200 where sg02cod='" & objsecurity.strUser & "'"
  Set rstCat = objApp.rdoConnect.OpenResultset(strCat)
  If Not rstCat.EOF Then
    Select Case rstCat("AD30CODCATEGORIA").Value
      Case 1, 2, 3, 5, 7, 8, 18, 19, 21:    'Categor�a Doctor
        gblnSelPaciente = True
        Call objsecurity.LaunchProcess("FR0115", vntDatos)
        gblnSelPaciente = False
      Case 6, 15:                           'Categor�a Enfermera/Auxiliar
        gblnSelPaciente = True
        Call objsecurity.LaunchProcess("FR0171", vntDatos)
        gblnSelPaciente = False
    End Select
  End If
  rstCat.Close
  Set rstCat = Nothing
End Sub





Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim selectDpto As String
Dim rdoDpto As rdoResultset
Dim selectCama As String
Dim rdoCama As rdoResultset

If Index = 1 Then
  If grdDBGrid1(1).Rows > 0 Then
    selectDpto = "SELECT AD0500.AD02CODDPTO,AD02DESDPTO,SG0200.SG02NOM || ',' || SG0200.SG02APE1 || ',' || SG0200.SG02APE2 DOCTORRESP,SG0200.SG02COD FROM AD0500,AD0200,SG0200 WHERE "
    selectDpto = selectDpto & " AD0200.AD02CODDPTO=AD0500.AD02CODDPTO"
    selectDpto = selectDpto & " AND AD0500.SG02COD=SG0200.SG02COD"
    selectDpto = selectDpto & " AND AD0500.AD07CODPROCESO="
    selectDpto = selectDpto & grdDBGrid1(1).Columns("Proceso").Value
    selectDpto = selectDpto & " AND AD0500.AD05FECFINRESPON IS NULL "
    selectDpto = selectDpto & " AND AD0500.AD01CODASISTENCI="
    selectDpto = selectDpto & grdDBGrid1(1).Columns("Asistencia").Value
    Set rdoDpto = objApp.rdoConnect.OpenResultset(selectDpto)
    If Not rdoDpto.EOF Then
      grdDBGrid1(1).Columns("Dpto.").Value = rdoDpto(0).Value
      grdDBGrid1(1).Columns("Dpto.Responsable").Value = rdoDpto(1).Value
      grdDBGrid1(1).Columns("Doctor Responsable").Value = rdoDpto(2).Value
      grdDBGrid1(1).Columns("Doctor").Value = rdoDpto(3).Value
    Else
      grdDBGrid1(1).Columns("Dpto.").Value = ""
      grdDBGrid1(1).Columns("Dpto.Responsable").Value = ""
      grdDBGrid1(1).Columns("Doctor Responsable").Value = ""
      grdDBGrid1(1).Columns("Doctor").Value = ""
    End If
    rdoDpto.Close
    Set rdoDpto = Nothing
    
    selectCama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE "
    selectCama = selectCama & " AD07CODPROCESO="
    selectCama = selectCama & grdDBGrid1(1).Columns("Proceso").Value
    selectCama = selectCama & " AND AD01CODASISTENCI="
    selectCama = selectCama & grdDBGrid1(1).Columns("Asistencia").Value
    selectCama = selectCama & " AND AD14CODESTCAMA=2"
    Set rdoCama = objApp.rdoConnect.OpenResultset(selectCama)
    If Not rdoCama.EOF Then
      grdDBGrid1(1).Columns("Cama").Value = rdoCama(0).Value
    Else
      grdDBGrid1(1).Columns("Cama").Value = ""
    End If
    rdoCama.Close
    Set rdoCama = Nothing
  End If
End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmdOManteriores_Click()
  cmdOManteriores.Enabled = False
  If grdDBGrid1(1).Rows > 0 Then
    If Len(Trim(grdDBGrid1(1).Columns("Asistencia").Value)) > 0 Then
      If Len(Trim(grdDBGrid1(1).Columns("Proceso").Value)) > 0 Then
        'Se puede redactar OM
      Else
        MsgBox "El paciente no tiene Proceso", vbInformation, "Seleccionar Paciente"
        cmdOManteriores.Enabled = True
        Exit Sub
      End If
    Else
      MsgBox "El paciente no tiene Asistencia", vbInformation, "Seleccionar Paciente"
      cmdOManteriores.Enabled = True
      Exit Sub
    End If
  Else
    MsgBox "El paciente no tiene Proceso ni Asistencia", vbInformation, "Seleccionar Paciente"
    cmdOManteriores.Enabled = True
    Exit Sub
  End If
    
  If IdPersona1.Text <> "" Then
    gintredactarorden = 1
    'se pasa el c�digo de paciente
    glngselpaciente = IdPersona1.Text
    gblnSelPaciente = True
    gstrLlamador = "frmSelPaciente"
    Call objsecurity.LaunchProcess("FR0115")
    'Call objsecurity.LaunchProcess("FR0111")
    gstrLlamador = ""
    gblnSelPaciente = False
  Else
    Call MsgBox("No hay ning�n paciente seleccionado", vbCritical, "Aviso")
  End If
cmdOManteriores.Enabled = True
End Sub

Private Sub Form_Activate()
Dim strCat As String
Dim rstCat As rdoResultset


    'If gintredactarorden = 1 Then
    '    'gintredactarorden = 0
    '    'se llama a Redactar Orden M�dica
    '    Call objsecurity.LaunchProcess("FR0111")
    '    Unload Me
    'Else
    cmdOManteriores.Enabled = True
    cmdPRN.Enabled = True
      strCat = "select * from sg0200 where sg02cod='" & objsecurity.strUser & "'"
      Set rstCat = objApp.rdoConnect.OpenResultset(strCat)
      If rstCat.EOF = False Then
        txtText1(9).Text = rstCat("AD30CODCATEGORIA").Value
        'Select Case txtText1(9).Text
        '  Case 1, 2, 3, 5, 7, 8, 18, 19, 21:
        '    cmdOManteriores.Enabled = True
        '    cmdPRN.Enabled = True
        '  Case 6, 15:
        '    cmdOManteriores.Enabled = False
        '    cmdPRN.Enabled = True
        '  Case Else:
        '    cmdOManteriores.Enabled = False
        '    cmdPRN.Enabled = False
        'End Select
      End If
    'End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String

  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                              Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Pacientes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strTable = "FR2200J" 's�lo pacientes con cama ocupada
    '.strTable = "FR2201J" 'pacientes con proceso y asistencia con o sin cama    .intAllowance = cwAllowReadOnly
    .strTable = "CI2200"
    .intAllowance = cwAllowReadOnly
    '.strWhere = " ad15codcama in (select ad1500.ad15codcama from ad1500,ad0300" & _
              " where ad1500.ad02coddpto=ad0300.ad02coddpto and" & _
              " ad0300.sg02cod='" & objsecurity.strUser & "')"
    '.strWhere = " ci21codpersona in (select ci21codpersona from ad0100,ad0500 where " & _
                " ad0100.AD01CODASISTENCI=ad0500.AD01CODASISTENCI and " & _
                "ad0100.AD01FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
                "((ad0100.AD01FECFIN IS NULL) OR (ad0100.AD01FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
                " AND ad0500.AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "'))"
              '" where ad1500.ad02coddpto=ad0300.ad02coddpto and" & _
              '" ad0300.sg02cod='" & objsecurity.strUser & "')"
'    .strWhere = "AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "')"
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Pacientes")
    'Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    'Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Apellido 1�", cwString)
    'Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Apellido 2�", cwString)
    'Call .FormAddFilterWhere(strKey, "CI22DNI", "DNI", cwString)
    'Call .FormAddFilterWhere(strKey, "CI22FECNACIM", "Fecha de Nacimiento", cwDate)
    'Call .FormAddFilterWhere(strKey, "CI22NUMHISTORIA", "Historia", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "CI22NUMSEGSOC", "N� Seguridad social", cwString)
    'Call .FormAddFilterWhere(strKey, "CI22DESPROVI", "Provincia", cwString)
    'Call .FormAddFilterWhere(strKey, "CI22DESLOCALID", "Localidad", cwString)
    'Call .FormAddFilterWhere(strKey, "CI19CODPAIS", "Pa�s", cwString)
    'Call .FormAddFilterWhere(strKey, "CI30DESSEXO", "Sexo", cwString)
 
  End With
   
    With objMultiInfo
      .strName = "Proc/Asis"
      Set .objFormContainer = fraFrame1(2)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = Nothing
      Set .grdGrid = grdDBGrid1(1)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
      .strTable = "AD0803J"
      .intAllowance = cwAllowReadOnly
      
      '.strWhere = "AD34CODESTADO=1 AND AD07CODPROCESO IN (SELECT AD07CODPROCESO FROM AD0800 WHERE AD34CODESTADO=1)"
      
      .strWhere = "AD01CODASISTENCI IN (SELECT AD01CODASISTENCI FROM AD0100 " & _
                                       " WHERE TO_DATE(TO_CHAR(AD01FECINICIO,'DD/MM/YYYY'),'DD/MM/YYYY')<=(SELECT TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') FROM DUAL))"
      
      
      Call .FormAddOrderField("AD08FECINICIO", cwDescending)
      Call .FormAddRelation("CI21CODPERSONA", IdPersona1)
  
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "OM")
      'Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
  End With
 
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

    'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
    Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo, "Dpto.", "", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Dpto.Responsable", "", cwString, 20)
    Call .GridAddColumn(objMultiInfo, "Doctor", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Doctor Responsable", "", cwString, 20)
    Call .GridAddColumn(objMultiInfo, "Cama", "", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Fec.Inicio", "AD08FECINICIO", cwDate)
    Call .GridAddColumn(objMultiInfo, "Fec.Fin", "AD08FECFIN", cwDate)
    
    Call .FormCreateInfo(objDetailInfo)
    Call .FormChangeColor(objMultiInfo)

    '.CtrlGetInfo(txtText1(3)).intKeyNo = 1
    
    '.CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(0)).blnInFind = True
'    .CtrlGetInfo(txtText1(1)).blnInFind = True
'    .CtrlGetInfo(txtText1(2)).blnInFind = True
'    .CtrlGetInfo(txtText1(4)).blnInFind = True
'    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
    .CtrlGetInfo(txtText1(6)).blnNegotiated = False
    .CtrlGetInfo(txtText1(9)).blnNegotiated = False
    .CtrlGetInfo(txtText1(7)).blnInGrid = False
    '.CtrlGetInfo(txtText1(3)).blnInGrid = False
'    .CtrlGetInfo(txtText1(1)).blnInGrid = False
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(10), "AD02DESDPTO")

    .CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    .CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo

    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "AD07CODPROCESO", "SELECT * FROM AD0800 WHERE AD07CODPROCESO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(4), "AD01CODASISTENCI")

    
    Call .WinRegister
    Call .WinStabilize
  End With
gintredactarorden = 0
'grdDBGrid1(0).Columns(2).Width = 2000
'grdDBGrid1(0).Columns(3).Width = 2000
'grdDBGrid1(0).Columns(4).Width = 2000
'grdDBGrid1(0).Columns(5).Width = 0
  
  grdDBGrid1(1).Columns("Doctor").Visible = False
  IdPersona1.blnSearchButton = True
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))  'abrir
  Call IdPersona1.Busqueda

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Select Case strCtrl
    Case "IdPersona1"                  'nuevo
      IdPersona1.SearchPersona         'nuevo
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index <> 30 Then
    If txtText1(7).Text <> "" Then
        txtText1(6).Text = DateDiff("d", txtText1(7).Text, Date) \ 365
    Else
        txtText1(6).Text = ""
    End If
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 ' Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 7 Then
    If txtText1(7).Text <> "" Then
        txtText1(6).Text = DateDiff("d", txtText1(7).Text, Date) \ 365
    Else
        txtText1(6).Text = ""
    End If
  End If
End Sub


