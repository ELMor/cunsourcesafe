VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmLECTOROM 
   Caption         =   "LECTOR"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6900
   LinkTopic       =   "Form1"
   ScaleHeight     =   7065
   ScaleWidth      =   6900
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "TRAER"
      Height          =   495
      Left            =   2160
      TabIndex        =   2
      Top             =   6480
      Width           =   2175
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   5295
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   6495
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   5
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "C�d.Prod."
      Columns(0).Name =   "C�d.Prod."
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   1482
      Columns(1).Caption=   "Cod.Int."
      Columns(1).Name =   "Cod.Int."
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   423
      Columns(2).Caption=   "S"
      Columns(2).Name =   "S"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   6059
      Columns(3).Caption=   "Producto"
      Columns(3).Name =   "Producto"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1508
      Columns(4).Caption=   "Cantidad"
      Columns(4).Name =   "Cantidad"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   2
      Columns(4).FieldLen=   256
      _ExtentX        =   11456
      _ExtentY        =   9340
      _StockProps     =   79
      Caption         =   "Materiales y Medicamentos"
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   1095
   End
End
Attribute VB_Name = "frmLECTOROM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String

'Guardamos el n�mero de filas seleccionadas
SSDBGrid1.MoveFirst
mintNTotalSelRows = SSDBGrid1.Rows
ReDim gintprodbuscado(mintNTotalSelRows, 8)
gintprodtotal = mintNTotalSelRows
For mintisel = 0 To mintNTotalSelRows - 1
    'Guardamos el n�mero de fila que est� seleccionada
     gintprodbuscado(mintisel, 0) = "" & SSDBGrid1.Columns(0).Value & "" 'c�digo
     gintprodbuscado(mintisel, 1) = SSDBGrid1.Columns(1).Value 'c�d.interno
     gintprodbuscado(mintisel, 2) = SSDBGrid1.Columns(3).Value 'descripci�n
     gintprodbuscado(mintisel, 3) = SSDBGrid1.Columns(4).Value 'dosis
     'gintprodbuscado(mintisel, 4) = SSDBGrid1.Columns(7).CellValue(mvarBkmrk) 'unidad medida
     'gintprodbuscado(mintisel, 5) = SSDBGrid1.Columns(5).CellValue(mvarBkmrk) 'forma
     SSDBGrid1.MoveNext
Next mintisel
Unload Me

End Sub

Private Sub Form_Activate()
Text1.SetFocus
End Sub



Private Sub SSDBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
Dim auxstr As String
Dim auxstr2 As String
Dim stra As String
Dim rsta As rdoResultset
Dim i As Integer
Dim listaerrores As String
Dim respuesta

SSDBGrid1.MoveLast

If KeyAscii = 13 Then
  KeyAscii = 0
i = 1
If Text1.Text <> "" Then
  auxstr = Text1
  If Len(auxstr) >= 9 Then
    If IsNumeric(Left(Text1.Text, 3)) And Left(Text1.Text, 3) > 90 Then
      respuesta = MsgBox("La cantidad introducida es mayor de 100." & Chr(13) & "�Desea continuar?", vbYesNo, "Aviso")
      If respuesta = 7 Then 'no
        Text1.Text = ""
        Exit Sub
      End If
    End If
    stra = "SELECT * FROM FR7300 WHERE FR73CODINTFAR='" & Mid(auxstr, 4, 6) & "'" & _
      " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"
    '& _           " AND FR73CODINTFARSEG=" & Mid(auxstr, 10, 1)
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
      SSDBGrid1.AddItem rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & Mid(auxstr, 4, 6) & _
      Chr(vbKeyTab) & rsta("FR73CODINTFARSEG").Value & _
      Chr(vbKeyTab) & rsta("FR73DESPRODUCTO").Value & _
      Chr(vbKeyTab) & Format(Mid(auxstr, 1, 3), "###")

      'grdDBGrid1(3).Columns(5).Value = rsta("FR73CODPRODUCTO").Value  'cod.producto
      'grdDBGrid1(3).Columns(6).Value = Mid(auxstr2, 4, 6) 'cod.interno
      'grdDBGrid1(3).Columns(8).Value = rsta("FR73DESPRODUCTO").Value 'des.producto
      'grdDBGrid1(3).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
      'grdDBGrid1(3).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
      'grdDBGrid1(3).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
      'grdDBGrid1(3).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
      'grdDBGrid1(3).Columns(12).Value = Format(Mid(auxstr2, 1, 3), "###")
      'grdDBGrid1(3).Columns(17).Value = rsta.rdoColumns("FR73INDPRODSAN").Value
      'grdDBGrid1(3).Update
    Else
      MsgBox "El c�digo: " & Text1.Text & " no existe o est� fuera de vigencia", vbExclamation, "Aviso"
    End If
    rsta.Close
    Set rsta = Nothing
  Else
  End If
End If
Text1.Text = ""

End If

SSDBGrid1.MoveLast

End Sub
