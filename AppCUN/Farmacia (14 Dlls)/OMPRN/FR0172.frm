VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmCesta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA.Cesta"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Ver Cestas en Estado:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   6480
      TabIndex        =   11
      Top             =   480
      Width           =   5295
      Begin VB.OptionButton Option1 
         Caption         =   "Dispensadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   2
         Left            =   3360
         TabIndex        =   14
         Top             =   480
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Enviadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   13
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Redactadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   12
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "&Quitar de la Cesta"
      Height          =   375
      Left            =   5160
      TabIndex        =   10
      Top             =   7560
      Width           =   1575
   End
   Begin VB.Frame frame1 
      Caption         =   "Ordenaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   6255
      Begin VB.OptionButton optOrd 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   1
         Left            =   600
         TabIndex        =   9
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton optOrd 
         Caption         =   "M�dico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   3
         Left            =   2160
         TabIndex        =   7
         Top             =   480
         Width           =   1095
      End
      Begin VB.OptionButton optOrd 
         Caption         =   "Enfermera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   4
         Left            =   3360
         TabIndex        =   6
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton optOrd 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   5
         Left            =   4680
         TabIndex        =   5
         Top             =   480
         Width           =   1215
      End
      Begin VB.OptionButton optOrd 
         Caption         =   "Cama"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   2
         Left            =   3000
         TabIndex        =   8
         Top             =   480
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Cesta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Tag             =   "Actuaciones Asociadas"
      Top             =   1560
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5295
         Index           =   1
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "NoDispensada"
         stylesets(0).BackColor=   16776960
         stylesets(0).Picture=   "FR0172.frx":0000
         stylesets(1).Name=   "Dispensada"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0172.frx":001C
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20055
         _ExtentY        =   9340
         _StockProps     =   79
         Caption         =   "PRODUCTOS"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmCesta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmCesta(FR0172.FRM)                                         *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: AGOSTO DE 1999                                                *
'* DESCRIPCION: Ver Situacion Cesta                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mstrCodServ As String




Private Sub cmdQuitar_Click()
  'Se quita el pedido de la cesta
  Dim intNTotalSelRows As Integer
  Dim intisel As Integer
  Dim varBkmrk As Variant
  Dim dblCodPet As Double
  Dim strFR28 As String
  Dim qryFR28 As rdoQuery
  Dim strFR66 As String
  Dim qryFR66 As rdoQuery
  Dim rstFR66 As rdoResultset
  Dim strCant As String
  Dim strNumLin As String
  Dim blnFR66 As Boolean
  Dim blnFR28 As Boolean
  Dim strIns As String
  Dim strCodPrd
  Dim dblCant As DBEngine
  Dim dblCantidad As Double
  blnFR66 = False
  blnFR28 = False
  Screen.MousePointer = vbHourglass
  cmdQuitar.Enabled = False
  If grdDBGrid1(1).Rows > 0 Then
    'Guardamos el n�mero de filas seleccionadas
    intNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
    strFR66 = "SELECT * FROM FR6600 WHERE FR66CODPETICION= ? "
        
    strFR28 = "DELETE FR2800 " & _
              " WHERE FR66CODPETICION = ? " & _
              "   AND FR28NUMLINEA = ? "
    
    For intisel = 0 To intNTotalSelRows - 1
      'Guardamos el n�mero de fila que est� seleccionada
      varBkmrk = grdDBGrid1(1).SelBookmarks(intisel)
      dblCodPet = grdDBGrid1(1).Columns("Pet").CellValue(varBkmrk) 'c�digo petici�n
      strNumLin = grdDBGrid1(1).Columns("Linea").CellValue(varBkmrk) 'N� L�nea
      strCant = grdDBGrid1(1).Columns("Cantidad").CellValue(varBkmrk) 'Cantidad
      strCodPrd = grdDBGrid1(1).Columns("Cod Prod").CellValue(varBkmrk) 'Cod Prod
      If IsNumeric(dblCodPet) Then
        'On Error GoTo Err_cmdQuitar_Click
        Set qryFR66 = objApp.rdoConnect.CreateQuery("", strFR66)
        qryFR66(0) = dblCodPet
        blnFR66 = True
        Set rstFR66 = qryFR66.OpenResultset()
        If Not rstFR66.EOF Then
          If Not IsNull(rstFR66.rdoColumns("AD07CODPROCESO").Value) And _
             Not IsNull(rstFR66.rdoColumns("AD01CODASISTENCI").Value) Then
             'Se hace el abono en FR6500
             dblCantidad = strCant
             dblCantidad = dblCantidad * -1
             strCant = objGen.ReplaceStr(dblCantidad, ",", ".", 1)
             strIns = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA," & _
                "FR65CANTIDAD,FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO," & _
                "FR65INDINTERCIENT,AD02CODDPTO_CRG) VALUES ("
             strIns = strIns & "FR65CODIGO_SEQUENCE.NEXTVAL,"
             strIns = strIns & rstFR66.rdoColumns("CI21CODPERSONA").Value & " ," & strCodPrd & ",SYSDATE,TO_CHAR(SYSDATE,'HH24')," '& _
             strIns = strIns & strCant & " ,"
             strIns = strIns & strCant & " ,"
             strIns = strIns & dblCodPet & ",0," & rstFR66.rdoColumns("AD01CODASISTENCI").Value & "," & rstFR66.rdoColumns("AD07CODPROCESO").Value & ","
             If IsNull(rstFR66.rdoColumns("FR66INDINTERCIENT").Value) Then
                  strIns = strIns & "null" & ","
             Else
                  strIns = strIns & rstFR66.rdoColumns("FR66INDINTERCIENT").Value & ","
             End If
             If IsNull(rstFR66.rdoColumns("AD02CODDPTO_CRG").Value) Then
                  strIns = strIns & "null"
             Else
                  strIns = strIns & rstFR66.rdoColumns("AD02CODDPTO_CRG").Value
             End If
             strIns = strIns & ")"
             objApp.rdoConnect.Execute strIns, 64
             'Se borra la l�nea de la petici�n
             Set qryFR28 = objApp.rdoConnect.CreateQuery("", strFR28)
             qryFR28(0) = dblCodPet
             qryFR28(1) = strNumLin
             qryFR28.Execute
             blnFR28 = True
             Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
             Screen.MousePointer = vbHourglass
             objWinInfo.DataRefresh
             Screen.MousePointer = vbDefault
             MsgBox "La Petici�n ha sido quitada", vbInformation, "Quitar de la Cesta"
          Else
            MsgBox "No es posible quitar la petici�n", vbInformation, "Quitar de la Cesta"
          End If
        Else
          MsgBox "No es posible quitar la petici�n", vbInformation, "Quitar de la Cesta"
        End If
        
        'strFR66 = "UPDATE FR6600 SET FR66INDCESTA= ? WHERE FR66CODPETICION= ? "
        'Set qryFR66 = objApp.rdoConnect.CreateQuery("", strFR66)
        'qryFR66(0) = 0
        'qryFR66(1) = dblCodPet
        'qryFR66.Execute
        'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
        'objWinInfo.DataRefresh
      Else
        MsgBox "Debe seleccionar la l�nea a quitar", vbInformation, "Quitar de la Cesta"
      End If
    Next intisel
    If blnFR66 Then
      qryFR66.Close
      Set qryFR66 = Nothing
      Set rstFR66 = Nothing
    End If
    If blnFR28 Then
      qryFR28.Close
      Set qryFR28 = Nothing
    End If
    
  Else
    MsgBox "No hay pedidos para quitar", vbInformation, "Quitar de la Cesta"
  End If
  Screen.MousePointer = vbDefault
  cmdQuitar.Enabled = True
  
Err_cmdQuitar_Click:
  Screen.MousePointer = vbDefault
  cmdQuitar.Enabled = True
End Sub

Private Sub Form_Activate()
  optOrd(1).Value = True
  If gstrLlamadorPRN = "PRN" Or gstrLlamadorPRN = "PRNServEspecial" Then
    cmdQuitar.Visible = False
    Frame2.Visible = False
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim strservicio As String
    Dim rstservicio As rdoResultset
    Dim servicio As Integer
    
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Cesta"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strTable = "FR2802J"
        .strTable = "FR2804J"
        .intAllowance = cwAllowReadOnly
        If gstrLlamadorPRN = "Cesta" Or gstrLlamadorPRN = "PRN" Then
          servicio = frmPedirPRN.txtText1(16).Text '215
        Else
            If gstrLlamadorPRN = "CestaServEspecial" Or gstrLlamadorPRN = "PRNServEspecial" Then
                servicio = frmPedirServEspeciales.txtText1(16).Text '215
            End If
        End If
        
        mstrCodServ = servicio
        
        If gstrLlamadorPRN = "Cesta" Or gstrLlamadorPRN = "CestaServEspecial" Then
          .strWhere = " FR66INDCESTA=-1 AND FR26CODESTPETIC=10 AND AD02CODDPTO=" & servicio
          Me.Caption = "FARMACIA.Cestas"
          fraFrame1(2).Caption = "Cesta"
        Else
          .strWhere = " AD02CODDPTO=" & servicio & _
                      " AND (FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL)"
          Me.Caption = "FARMACIA.Prns"
          fraFrame1(2).Caption = "PRNs"
        End If
        'Call .FormAddOrderField("FR66CODPETICION ", cwDescending)
        'Call .FormAddOrderField("FR28NUMLINEA ", cwAscending)
        '.blnHasMaint = True
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        If gstrLlamadorPRN = "Cesta" Or gstrLlamadorPRN = "CestaServEspecial" Then
          Call .FormCreateFilterWhere(strKey, "Cesta")
        Else
          Call .FormCreateFilterWhere(strKey, "PRN")
        End If
        Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�d Pet", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d Prod", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR28REFERENCIA", "Referencia", cwString)
        Call .FormAddFilterWhere(strKey, "FR28DOSIS", "Dosis", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "UM", cwString)
        Call .FormAddFilterWhere(strKey, "FR28CANTIDAD", "Cantidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Pet (1,3,5)", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR66FECREDACCION", "Fecha", cwDate)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR66CODPETICION", "C�d Pet")
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d Prod")
        Call .FormAddFilterOrder(strKey, "FR28REFERENCIA", "Referencia")
        Call .FormAddFilterOrder(strKey, "FR28DOSIS", "Dosis")
        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "Referencia")
        Call .FormAddFilterOrder(strKey, "FR28CANTIDAD", "Cantidad")
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "Est", "FR26CODESTPETIC", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Estado", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Pet", "FR66CODPETICION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Linea", "FR28NUMLINEA", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Fecha", "FR66FECREDACCION", cwDate)
        Call .GridAddColumn(objMultiInfo, "Cod Prod", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Codigo", "FR73CODINTFAR", cwDate)
        Call .GridAddColumn(objMultiInfo, "Referencia", "FR28REFERENCIA", cwString, 15)
        Call .GridAddColumn(objMultiInfo, "Producto", "FR73DESPRODUCTO", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Tam Env", "FR73TAMENVASE", cwDecimal, 10)
        Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "Cantidad", "FR28CANTIDAD", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Paciente", "CI21CODPERSONA", cwNumeric, 7)
        'Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
        Call .GridAddColumn(objMultiInfo, "Historia", "CI22NUMHISTORIA", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Nombre", "CI22NOMBRE", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "1�Apellido", "CI22PRIAPEL", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "2�Apellido", "CI22SEGAPEL", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Doc", "SG02COD_MED", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Doctor", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Enf", "SG02COD_ENF", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Enfermera", "", cwString, 25)
        
        Call .FormCreateInfo(objMultiInfo)
        'Se indica que campos son obligatorios y cuales son clave primaria
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(4), "FR26DESESTADOPET")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(22)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE  SG02COD= ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(22)), grdDBGrid1(1).Columns(23), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(24)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE  SG02COD= ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(24)), grdDBGrid1(1).Columns(25), "SG02APE1")
        
        
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(8), "FR73DESPRODUCTO")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(10), "FRH7CODFORMFAR")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(9), "FR73TAMENVASE")
        
        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "FR66CODPETICION", "SELECT * FROM FR6600 WHERE FR66CODPETICION = ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(14), "CI21CODPERSONA")
        
        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), grdDBGrid1(1).Columns(17), "CI22NOMBRE")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), grdDBGrid1(1).Columns(18), "CI22PRIAPEL")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), grdDBGrid1(1).Columns(19), "CI22SEGAPEL")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), grdDBGrid1(1).Columns(16), "CI22NUMHISTORIA")
        
        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE FR2200J.CI21CODPERSONA= ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), grdDBGrid1(1).Columns(15), "AD15CODCAMA")
        
        Call .WinRegister
        Call .WinStabilize
    End With
    'grdDBGrid1(1).Columns(7).Width = 1200
    'grdDBGrid1(1).Columns(9).Width = 800
    'grdDBGrid1(1).Columns(11).Width = 800
    'grdDBGrid1(1).Columns(8).Width = 3500
    
    grdDBGrid1(1).Columns(4).Width = 1000
    grdDBGrid1(1).Columns(5).Width = 600
    'grdDBGrid1(1).Columns(4).Width = 800
    'grdDBGrid1(1).Columns(5).Width = 800
    grdDBGrid1(1).Columns(7).Width = 1000
    grdDBGrid1(1).Columns(9).Width = 800
    grdDBGrid1(1).Columns(10).Width = 1000
    grdDBGrid1(1).Columns(11).Width = 2500
    grdDBGrid1(1).Columns(12).Width = 500
    grdDBGrid1(1).Columns(13).Width = 400
    grdDBGrid1(1).Columns(14).Width = 500
    grdDBGrid1(1).Columns(15).Width = 400
    grdDBGrid1(1).Columns(16).Width = 600
    'grdDBGrid1(1).Columns(14).Width = 800
    'CAMAgrdDBGrid1(1).Columns(18).Width = 800
    grdDBGrid1(1).Columns(18).Width = 1000
    grdDBGrid1(1).Columns(19).Width = 3500
    grdDBGrid1(1).Columns(20).Width = 3500
    grdDBGrid1(1).Columns(21).Width = 3500
    
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(6).Visible = False
    grdDBGrid1(1).Columns(8).Visible = False
    grdDBGrid1(1).Columns(17).Visible = False
    grdDBGrid1(1).Columns(22).Visible = False
    grdDBGrid1(1).Columns(24).Visible = False
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
'Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Actuaciones" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
'End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    Call objsecurity.LaunchProcess("FR0054")
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


Private Sub Option1_Click(Index As Integer)
  Screen.MousePointer = vbHourglass
  Select Case Index
    Case 0 'Redactadas
      cmdQuitar.Enabled = True
      Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      objWinInfo.objWinActiveForm.strWhere = " FR66INDCESTA=-1  AND FR26CODESTPETIC=1 AND AD02CODDPTO=" & mstrCodServ
      Call objWinInfo.DataRefresh
    Case 1 'Enviadas
      cmdQuitar.Enabled = True
      Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      objWinInfo.objWinActiveForm.strWhere = "  FR66INDCESTA=-1  AND FR26CODESTPETIC=3 AND AD02CODDPTO=" & mstrCodServ
      Call objWinInfo.DataRefresh
    Case 2 'Dispensadas
      cmdQuitar.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      objWinInfo.objWinActiveForm.strWhere = " FR66INDCESTA=-1  AND FR26CODESTPETIC=5 AND AD02CODDPTO=" & mstrCodServ
      Call objWinInfo.DataRefresh
  End Select
  Screen.MousePointer = vbDefault
End Sub

Private Sub optOrd_Click(Index As Integer)
  Select Case Index
    Case 1:
        Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR66FECREDACCION", cwDescending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR66CODPETICION", cwDescending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR28NUMLINEA", cwAscending)
        Call objWinInfo.DataRefresh
    Case 2:
        Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        'Call objWinInfo.objWinActiveForm.FormAddOrderField("AD15CODCAMA", cwAscending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR66CODPETICION", cwDescending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR28NUMLINEA", cwAscending)
        Call objWinInfo.DataRefresh
    Case 3:
        Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("SG02COD_MED", cwAscending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR66CODPETICION", cwDescending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR28NUMLINEA", cwAscending)
        Call objWinInfo.DataRefresh
    Case 4:
        Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("SG02COD_ENF", cwAscending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR66CODPETICION", cwDescending)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("FR28NUMLINEA", cwAscending)
        Call objWinInfo.DataRefresh
    Case 5:
        Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        Call objWinInfo.objWinActiveForm.FormAddOrderField("CI22NUMHISTORIA", cwAscending)
        'Call objWinInfo.objWinActiveForm.FormAddOrderField("FR28NUMLINEA", cwAscending)
        Call objWinInfo.DataRefresh
  End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    
    If Index = 1 Then
        If grdDBGrid1(1).Columns(3).Value = 5 Or grdDBGrid1(1).Columns(5).Value = 4 Then
          For i = 3 To 21
              grdDBGrid1(1).Columns(i).CellStyleSet "Dispensada"
          Next i
        Else
          For i = 3 To 21
              grdDBGrid1(1).Columns(i).CellStyleSet "NoDispensada"
          Next i
        End If
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub




