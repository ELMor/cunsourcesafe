VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmRedactarOMPRN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar OM / PRN"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   93
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdfirmarenviar 
      Caption         =   "Firmar / Enviar "
      Height          =   375
      Left            =   10320
      TabIndex        =   297
      Top             =   2520
      Width           =   1455
   End
   Begin VB.CommandButton cmdcopiarlinea 
      Caption         =   "Copiar L�nea"
      Height          =   375
      Left            =   10200
      TabIndex        =   292
      Top             =   3600
      Width           =   1695
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   9960
      Top             =   2880
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdInstAdm 
      Caption         =   "Instr.Administraci�n"
      Height          =   375
      Left            =   10200
      TabIndex        =   87
      Top             =   5520
      Width           =   1695
   End
   Begin VB.CommandButton cmdMed2 
      Caption         =   "Medicamento 2"
      Height          =   375
      Left            =   10200
      TabIndex        =   83
      Top             =   6360
      Width           =   1695
   End
   Begin VB.CommandButton cmdSolDil 
      Caption         =   "Soluci�n para diluir"
      Height          =   375
      Left            =   10200
      TabIndex        =   82
      Top             =   6000
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdMedNoForm 
      Caption         =   "Medic. No Formulario"
      Height          =   375
      Left            =   10200
      TabIndex        =   88
      Top             =   7200
      Width           =   1695
   End
   Begin VB.Frame Frame2 
      Caption         =   "Periodicidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   360
      TabIndex        =   134
      Top             =   3960
      Width           =   8535
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   29
         Left            =   6480
         TabIndex        =   165
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   30
         Left            =   7200
         TabIndex        =   164
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   22
         Left            =   1200
         TabIndex        =   163
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   23
         Left            =   2040
         TabIndex        =   162
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   24
         Left            =   2760
         TabIndex        =   161
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   25
         Left            =   3480
         TabIndex        =   160
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   26
         Left            =   4200
         TabIndex        =   159
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   27
         Left            =   4920
         TabIndex        =   158
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   28
         Left            =   5760
         TabIndex        =   157
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   15
         Left            =   3480
         TabIndex        =   156
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   16
         Left            =   4200
         TabIndex        =   155
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   17
         Left            =   4920
         TabIndex        =   154
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   18
         Left            =   5760
         TabIndex        =   153
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   19
         Left            =   6480
         TabIndex        =   152
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   20
         Left            =   7200
         TabIndex        =   151
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   21
         Left            =   480
         TabIndex        =   150
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   8
         Left            =   5760
         TabIndex        =   149
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   9
         Left            =   6480
         TabIndex        =   148
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   10
         Left            =   7200
         TabIndex        =   147
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   11
         Left            =   480
         TabIndex        =   146
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   12
         Left            =   1200
         TabIndex        =   145
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   13
         Left            =   1965
         TabIndex        =   144
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   14
         Left            =   2760
         TabIndex        =   143
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   142
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   2
         Left            =   1230
         TabIndex        =   141
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   3
         Left            =   1965
         TabIndex        =   140
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   4
         Left            =   2760
         TabIndex        =   139
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   5
         Left            =   3480
         TabIndex        =   138
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   6
         Left            =   4200
         TabIndex        =   137
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   7
         Left            =   4920
         TabIndex        =   136
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CommandButton cmdAceptarPer 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   3360
         TabIndex        =   135
         TabStop         =   0   'False
         Top             =   3120
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(4)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   2760
         TabIndex        =   291
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   2760
         TabIndex        =   290
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   29
         Left            =   7200
         TabIndex        =   223
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   28
         Left            =   6480
         TabIndex        =   222
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   5760
         TabIndex        =   221
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   4920
         TabIndex        =   220
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   4200
         TabIndex        =   219
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   3480
         TabIndex        =   218
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   2760
         TabIndex        =   217
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   2040
         TabIndex        =   216
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   1200
         TabIndex        =   215
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   480
         TabIndex        =   214
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   7200
         TabIndex        =   213
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   6480
         TabIndex        =   212
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   5760
         TabIndex        =   211
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   4920
         TabIndex        =   210
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   4200
         TabIndex        =   209
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   3480
         TabIndex        =   208
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   2760
         TabIndex        =   207
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   1965
         TabIndex        =   206
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   1200
         TabIndex        =   205
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   480
         TabIndex        =   204
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   7185
         TabIndex        =   203
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   6480
         TabIndex        =   202
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   5760
         TabIndex        =   201
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   4920
         TabIndex        =   200
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   4200
         TabIndex        =   199
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   3465
         TabIndex        =   198
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   1965
         TabIndex        =   197
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   1230
         TabIndex        =   196
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   195
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(29)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   36
         Left            =   6480
         TabIndex        =   194
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(30)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   35
         Left            =   7200
         TabIndex        =   193
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(22)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   34
         Left            =   1200
         TabIndex        =   192
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(23)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   33
         Left            =   2040
         TabIndex        =   191
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(24)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   32
         Left            =   2760
         TabIndex        =   190
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(25)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   31
         Left            =   3480
         TabIndex        =   189
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(26)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   30
         Left            =   4200
         TabIndex        =   188
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(27)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   29
         Left            =   4920
         TabIndex        =   187
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(28)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   28
         Left            =   5760
         TabIndex        =   186
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(15)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   3480
         TabIndex        =   185
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(16)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   4200
         TabIndex        =   184
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(17)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   4920
         TabIndex        =   183
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(18)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   5760
         TabIndex        =   182
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(19)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   6480
         TabIndex        =   181
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(20)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   7200
         TabIndex        =   180
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(21)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   480
         TabIndex        =   179
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(8)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   5760
         TabIndex        =   178
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(9)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   6480
         TabIndex        =   177
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(10)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   7200
         TabIndex        =   176
         Top             =   720
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(11)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   480
         TabIndex        =   175
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(12)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   1200
         TabIndex        =   174
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(13)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   1965
         TabIndex        =   173
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(14)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   2760
         TabIndex        =   172
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   480
         TabIndex        =   171
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(2)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   1230
         TabIndex        =   170
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(3)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   1965
         TabIndex        =   169
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(5)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   3480
         TabIndex        =   168
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(6)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   4200
         TabIndex        =   167
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(7)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   4920
         TabIndex        =   166
         Top             =   720
         Width           =   240
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Prescripci�n M�dica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   0
      TabIndex        =   102
      Top             =   480
      Width           =   10185
      Begin TabDlg.SSTab tabTab1 
         Height          =   2655
         Index           =   0
         Left            =   120
         TabIndex        =   103
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   4683
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0111.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(13)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(28)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "tab1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(26)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(25)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(15)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkCheck1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chkCheck1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chkOMPRN(6)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0111.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkOMPRN 
            Caption         =   "PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   7680
            TabIndex        =   9
            Tag             =   "PRN?"
            Top             =   600
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   1
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   7680
            TabIndex        =   8
            Tag             =   "Orden M�dica?"
            Top             =   360
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5400
            TabIndex        =   4
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR66INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   5400
            TabIndex        =   5
            Tag             =   "Alergico?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n Volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5400
            TabIndex        =   6
            Tag             =   "Restricci�n de Volumen?"
            Top             =   600
            Width           =   2175
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   7680
            TabIndex        =   7
            Tag             =   "Inter�s Cient�fico?"
            Top             =   120
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   3120
            TabIndex        =   2
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   3480
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   104
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2385
            Index           =   2
            Left            =   -74880
            TabIndex        =   105
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   4207
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tab1 
            Height          =   1815
            Left            =   120
            TabIndex        =   106
            TabStop         =   0   'False
            Top             =   720
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   3201
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0111.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(42)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(41)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(40)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(39)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(38)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(11)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(7)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(9)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(86)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(87)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(7)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(6)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "Text1"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(29)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(33)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(32)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(46)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(23)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(3)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(2)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(4)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(5)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(59)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(60)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).ControlCount=   26
            TabCaption(1)   =   "M�dico"
            TabPicture(1)   =   "FR0111.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(12)"
            Tab(1).Control(1)=   "txtText1(20)"
            Tab(1).Control(2)=   "txtText1(9)"
            Tab(1).Control(3)=   "txtText1(8)"
            Tab(1).Control(4)=   "txtText1(22)"
            Tab(1).Control(5)=   "txtText1(11)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "txtText1(10)"
            Tab(1).Control(7)=   "dtcDateCombo1(1)"
            Tab(1).Control(8)=   "dtcDateCombo1(2)"
            Tab(1).Control(9)=   "lblLabel1(15)"
            Tab(1).Control(10)=   "lblLabel1(18)"
            Tab(1).Control(11)=   "lblLabel1(5)"
            Tab(1).Control(12)=   "lblLabel1(20)"
            Tab(1).Control(13)=   "lblLabel1(6)"
            Tab(1).Control(14)=   "lblLabel1(10)"
            Tab(1).Control(15)=   "lblLabel1(0)"
            Tab(1).ControlCount=   16
            TabCaption(2)   =   "Servicio"
            TabPicture(2)   =   "FR0111.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(62)"
            Tab(2).Control(1)=   "txtText1(61)"
            Tab(2).Control(2)=   "txtText1(17)"
            Tab(2).Control(3)=   "txtText1(16)"
            Tab(2).Control(4)=   "txtText1(24)"
            Tab(2).Control(5)=   "txtText1(14)"
            Tab(2).Control(6)=   "txtText1(15)"
            Tab(2).Control(7)=   "txtText1(19)"
            Tab(2).Control(8)=   "txtText1(21)"
            Tab(2).Control(9)=   "lblLabel1(46)"
            Tab(2).Control(10)=   "lblLabel1(1)"
            Tab(2).Control(11)=   "lblLabel1(12)"
            Tab(2).Control(12)=   "lblLabel1(3)"
            Tab(2).Control(13)=   "lblLabel1(4)"
            Tab(2).ControlCount=   14
            TabCaption(3)   =   "Observaciones"
            TabPicture(3)   =   "FR0111.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(18)"
            Tab(3).Control(1)=   "lblLabel1(24)"
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Validaci�n/Cierre"
            TabPicture(4)   =   "FR0111.frx":00A8
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(62)"
            Tab(4).Control(1)=   "lblLabel1(85)"
            Tab(4).Control(2)=   "dtcDateCombo1(5)"
            Tab(4).Control(3)=   "dtcDateCombo1(0)"
            Tab(4).ControlCount=   4
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   62
               Left            =   -69720
               TabIndex        =   303
               Tag             =   "Dpto.Cargo"
               Top             =   1320
               Visible         =   0   'False
               Width           =   3405
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   61
               Left            =   -70200
               TabIndex        =   302
               Tag             =   "C�d.Dpto.Cargo"
               Top             =   1320
               Visible         =   0   'False
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD01CODASISTENCI"
               Height          =   330
               Index           =   60
               Left            =   7680
               TabIndex        =   289
               Tag             =   "Asistencia"
               Top             =   1380
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD07CODPROCESO"
               Height          =   330
               Index           =   59
               Left            =   6000
               TabIndex        =   288
               Tag             =   "Proceso"
               Top             =   1380
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1170
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   41
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   9165
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   3840
               TabIndex        =   20
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1380
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   1800
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1380
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   9120
               TabIndex        =   22
               Tag             =   "C�digo Paciente"
               Top             =   780
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1380
               Width           =   1600
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   120
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Historia Paciente"
               Top             =   660
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   3840
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   660
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66PESOPAC"
               Height          =   330
               Index           =   32
               Left            =   5160
               MaxLength       =   3
               TabIndex        =   15
               Tag             =   "Peso"
               Top             =   660
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66ALTUPAC"
               Height          =   330
               Index           =   33
               Left            =   6240
               MaxLength       =   3
               TabIndex        =   16
               Tag             =   "Altura Cm"
               Top             =   660
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   29
               Left            =   7320
               TabIndex        =   17
               Tag             =   "Superficie Corporal"
               Top             =   660
               Width           =   600
            End
            Begin VB.TextBox Text1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   1800
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "Edad"
               Top             =   660
               Width           =   420
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD02CODDPTO_MED"
               Height          =   330
               Index           =   12
               Left            =   -70920
               TabIndex        =   25
               Tag             =   "Departamento"
               Top             =   660
               Width           =   1275
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   -67320
               TabIndex        =   27
               Tag             =   "Hora Firma M�dico"
               Top             =   660
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   -72720
               TabIndex        =   24
               Tag             =   "Apellido Doctor"
               Top             =   660
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   -74760
               TabIndex        =   23
               Tag             =   "C�digo Doctor"
               Top             =   660
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -73680
               TabIndex        =   33
               Tag             =   "Desc.Departamento"
               Top             =   660
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   32
               Tag             =   "C�d.Departamento"
               Top             =   660
               Width           =   1035
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -70920
               TabIndex        =   30
               Tag             =   "C�digo Urgencia"
               Top             =   1380
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -70080
               TabIndex        =   31
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   1380
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   -72840
               TabIndex        =   29
               Tag             =   "Hora Redacci�n"
               Top             =   1380
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   6
               Left            =   2520
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   660
               Width           =   1140
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   9120
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   420
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   -74760
               TabIndex        =   34
               Tag             =   "M�dico que firma"
               Top             =   1380
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_ENF"
               Height          =   330
               Index           =   14
               Left            =   -66960
               TabIndex        =   37
               Tag             =   "M�dico que env�a"
               Top             =   600
               Visible         =   0   'False
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   -66960
               TabIndex        =   38
               Tag             =   "Apellido Doctor"
               Top             =   960
               Visible         =   0   'False
               Width           =   765
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   -74040
               TabIndex        =   35
               Tag             =   "Apellido Doctor"
               Top             =   1380
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   21
               Left            =   -72120
               TabIndex        =   36
               Tag             =   "N� Colegiado"
               Top             =   1380
               Width           =   720
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   107
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   108
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   -69360
               TabIndex        =   26
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   660
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   28
               Tag             =   "Fecha Redacci�n"
               Top             =   1380
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECVALIDACION"
               Height          =   330
               Index           =   0
               Left            =   -74640
               TabIndex        =   39
               Tag             =   "Fecha Validaci�n"
               Top             =   840
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECCIERRE"
               Height          =   330
               Index           =   5
               Left            =   -72120
               TabIndex        =   40
               Tag             =   "Fecha Cierre"
               Top             =   840
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento de Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   46
               Left            =   -70200
               TabIndex        =   304
               Top             =   1080
               Visible         =   0   'False
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Asistencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   87
               Left            =   7680
               TabIndex        =   301
               Top             =   1140
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   86
               Left            =   6000
               TabIndex        =   300
               Top             =   1140
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Cierre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   85
               Left            =   -72120
               TabIndex        =   299
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Validaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   62
               Left            =   -74640
               TabIndex        =   298
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   130
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   3840
               TabIndex        =   129
               Top             =   1140
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   1800
               TabIndex        =   128
               Top             =   1140
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   120
               TabIndex        =   127
               Top             =   1140
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   120
               TabIndex        =   126
               Top             =   420
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   38
               Left            =   3840
               TabIndex        =   125
               Top             =   420
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   39
               Left            =   5160
               TabIndex        =   124
               Top             =   420
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Altura Cm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   6240
               TabIndex        =   123
               Top             =   420
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Superficie Corporal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   7320
               TabIndex        =   122
               Top             =   420
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Edad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   42
               Left            =   1800
               TabIndex        =   121
               Top             =   420
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -70920
               TabIndex        =   120
               Top             =   420
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67320
               TabIndex        =   119
               Top             =   420
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -69360
               TabIndex        =   118
               Top             =   420
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74760
               TabIndex        =   117
               Top             =   420
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   116
               Top             =   420
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -70920
               TabIndex        =   115
               Top             =   1140
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -72840
               TabIndex        =   114
               Top             =   1140
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74760
               TabIndex        =   113
               Top             =   1140
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   2520
               TabIndex        =   112
               Top             =   420
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74760
               TabIndex        =   111
               Top             =   1140
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que env�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -67320
               TabIndex        =   110
               Top             =   360
               Visible         =   0   'False
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Colegiado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -72120
               TabIndex        =   109
               Top             =   1140
               Width           =   1335
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   133
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1320
            TabIndex        =   132
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3120
            TabIndex        =   131
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.CommandButton cmdInformacion 
      Caption         =   "Informaci�n"
      Height          =   375
      Left            =   10200
      TabIndex        =   89
      Top             =   7680
      Width           =   1695
   End
   Begin VB.CommandButton cmdAlergia 
      Caption         =   "Alergias"
      Height          =   375
      Left            =   10320
      TabIndex        =   42
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton cmdenviar 
      Caption         =   "Enviar"
      Height          =   375
      Left            =   11160
      TabIndex        =   47
      Top             =   3000
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdfirmar 
      Caption         =   "Firmar"
      Height          =   375
      Left            =   10440
      TabIndex        =   46
      Top             =   3000
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdsitcarro 
      Caption         =   "Situaci�n Carro"
      Height          =   375
      Left            =   10320
      TabIndex        =   45
      Top             =   2040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton cmdbuscarprot 
      Caption         =   "Protocolos"
      Height          =   375
      Left            =   10200
      TabIndex        =   84
      Top             =   6720
      Width           =   1695
   End
   Begin VB.CommandButton cmdHCpaciente 
      Caption         =   "Historia Cl�nica"
      Height          =   375
      Left            =   10320
      TabIndex        =   44
      Top             =   1560
      Width           =   1455
   End
   Begin VB.CommandButton cmdperfil 
      Caption         =   "Perfil FTP"
      Height          =   375
      Left            =   10320
      TabIndex        =   43
      Top             =   1080
      Width           =   1455
   End
   Begin VB.CommandButton cmdbuscargruprot 
      Caption         =   "Grupos Terap�uticos"
      Height          =   375
      Left            =   10200
      TabIndex        =   85
      Top             =   4560
      Width           =   1695
   End
   Begin VB.CommandButton cmdbuscargruprod 
      Caption         =   "Grupo Medicamentos"
      Height          =   375
      Left            =   10200
      TabIndex        =   86
      Top             =   5040
      Width           =   1695
   End
   Begin VB.CommandButton cmdbuscarprod 
      Caption         =   "Medicamentos"
      Height          =   375
      Left            =   10200
      TabIndex        =   81
      Top             =   4080
      Width           =   1695
   End
   Begin VB.Frame fraFrame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4560
      Index           =   0
      Left            =   0
      TabIndex        =   92
      Top             =   3480
      Width           =   10095
      Begin TabDlg.SSTab tabTab1 
         Height          =   4215
         Index           =   1
         Left            =   120
         TabIndex        =   91
         TabStop         =   0   'False
         Top             =   240
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   7435
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0111.frx":00C4
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Frame1"
         Tab(0).Control(1)=   "tab2"
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0111.frx":00E0
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "lblLabel1(55)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "lblLabel1(54)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "lblLabel1(53)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "lblLabel1(44)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "grdDBGrid1(6)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).ControlCount=   5
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3600
            Left            =   -74760
            TabIndex        =   249
            Top             =   480
            Width           =   9135
            Begin VB.TextBox txtText1 
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   35
               Left            =   5400
               TabIndex        =   51
               Tag             =   "FF"
               Top             =   240
               Width           =   615
            End
            Begin VB.Frame Frame3 
               BorderStyle     =   0  'None
               Height          =   375
               Left            =   8640
               TabIndex        =   296
               Top             =   3240
               Width           =   495
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR28INSTRADMIN"
               Height          =   940
               Index           =   39
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   71
               Tag             =   "Inst.Admin.|Instrucciones para administraci�n"
               Top             =   2040
               Width           =   5205
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28CANTDISP"
               Height          =   330
               Index           =   58
               Left            =   3480
               TabIndex        =   287
               Tag             =   "Cant.Dispens."
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Index           =   1
               Left            =   4080
               TabIndex        =   79
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.CommandButton cmdWord 
               Caption         =   "W"
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Left            =   4680
               TabIndex        =   80
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR28PATHFORMAG"
               Height          =   330
               Index           =   57
               Left            =   360
               TabIndex        =   78
               Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
               Top             =   2520
               Visible         =   0   'False
               Width           =   4725
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28REFERENCIA"
               Height          =   330
               Index           =   56
               Left            =   2760
               TabIndex        =   285
               Tag             =   "Referencia"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR28DESPRODUCTO"
               Height          =   330
               Index           =   55
               Left            =   120
               TabIndex        =   50
               Tag             =   "Medic.No Form.|""Medic. No Formulario"""
               Top             =   120
               Visible         =   0   'False
               Width           =   5295
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   13
               Left            =   6480
               TabIndex        =   49
               Tag             =   "Medicamento"
               Top             =   2280
               Visible         =   0   'False
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28UBICACION"
               Height          =   330
               Index           =   54
               Left            =   2520
               TabIndex        =   284
               Tag             =   "Ubicaci�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28OPERACION"
               Height          =   330
               Index           =   53
               Left            =   3000
               TabIndex        =   283
               Tag             =   "Operaci�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO_2"
               Height          =   330
               Index           =   52
               Left            =   9000
               TabIndex        =   90
               Tag             =   "C�digo Medicamento 2"
               Top             =   3240
               Width           =   150
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   51
               Left            =   3960
               TabIndex        =   66
               Tag             =   "Medicamento 2"
               Top             =   3000
               Visible         =   0   'False
               Width           =   1380
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28DOSIS_2"
               Height          =   330
               Index           =   50
               Left            =   6120
               TabIndex        =   68
               Tag             =   "Dosis2"
               Top             =   3240
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA_2"
               Height          =   330
               Index           =   49
               Left            =   7080
               TabIndex        =   69
               Tag             =   "UM2"
               Top             =   3240
               Width           =   660
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRH7CODFORMFAR_2"
               Height          =   330
               Index           =   48
               Left            =   5400
               TabIndex        =   67
               Tag             =   "FF2"
               Top             =   3240
               Width           =   615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VOLUMEN_2"
               Height          =   330
               Index           =   47
               Left            =   7800
               TabIndex        =   70
               Tag             =   "Vol.Prod.2|Volumen(ml) Producto 2"
               Top             =   3240
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VELPERFUSION"
               Height          =   330
               Index           =   82
               Left            =   5400
               TabIndex        =   72
               Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
               Top             =   2040
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VOLTOTAL"
               Height          =   330
               Index           =   44
               Left            =   7800
               TabIndex        =   75
               Tag             =   "Vol.Total|Volumen Total (ml)"
               Top             =   2040
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VOLUMEN"
               Height          =   330
               Index           =   38
               Left            =   7800
               TabIndex        =   54
               Tag             =   "Volum.|Volumen(ml)"
               Top             =   240
               Width           =   732
            End
            Begin VB.TextBox txtMinutos 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   7080
               TabIndex        =   65
               Tag             =   "Tiempo Infusi�n(min)"
               Top             =   1440
               Width           =   375
            End
            Begin VB.TextBox txtHoras 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   6600
               TabIndex        =   64
               Tag             =   "Tiempo Infusi�n(hor)"
               Top             =   1440
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28tiemmininf"
               Height          =   330
               Index           =   43
               Left            =   3720
               TabIndex        =   254
               Tag             =   "T.Inf.(min)|Tiempo Infusi�n(min)"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28CANTIDADDIL"
               Height          =   330
               Index           =   42
               Left            =   5520
               TabIndex        =   63
               Tag             =   "Vol.Dil.|Volumen(ml)"
               Top             =   1440
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   41
               Left            =   4080
               TabIndex        =   62
               Tag             =   "Descripci�n Producto Diluir"
               Top             =   1200
               Visible         =   0   'False
               Width           =   1425
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO_DIL"
               Height          =   330
               Index           =   40
               Left            =   8760
               TabIndex        =   61
               Tag             =   "C�digo Producto diluir"
               Top             =   3240
               Width           =   150
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "PERF"
               DataField       =   "FR28INDPERF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   8280
               TabIndex        =   77
               Tag             =   "PERF|Mezcla IV en Farmacia"
               Top             =   1280
               Width           =   855
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "STAT"
               DataField       =   "fr28indcomieninmed"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   8280
               TabIndex        =   76
               Tag             =   "STAT|Pedir la primera dosis inmediatamente"
               Top             =   800
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28horafin"
               Height          =   330
               Index           =   36
               Left            =   7320
               TabIndex        =   74
               Tag             =   "H.F.|Hora Fin"
               Top             =   2640
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28horainicio"
               Height          =   330
               Index           =   37
               Left            =   7440
               TabIndex        =   60
               Tag             =   "H.I.|Hora Inicio"
               Top             =   840
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   31
               Left            =   7080
               TabIndex        =   53
               Tag             =   "UM"
               Top             =   240
               Width           =   660
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28DOSIS"
               Height          =   330
               Index           =   30
               Left            =   6120
               TabIndex        =   52
               Tag             =   "Dosis"
               Top             =   240
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28CANTIDAD"
               Height          =   330
               Index           =   28
               Left            =   0
               TabIndex        =   55
               Tag             =   "Cantidad"
               Top             =   840
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   27
               Left            =   8880
               TabIndex        =   48
               Tag             =   "C�digo Medicamento"
               Top             =   3240
               Width           =   150
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "PRN"
               DataField       =   "FR28INDDISPPRN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   1440
               TabIndex        =   253
               Tag             =   "Prn?"
               Top             =   2760
               Visible         =   0   'False
               Width           =   735
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Cambiar V�a"
               DataField       =   "FR28INDVIAOPC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   3000
               TabIndex        =   252
               Tag             =   "Cambiar V�a?"
               Top             =   2760
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28numlinea"
               Height          =   405
               Index           =   45
               Left            =   2040
               TabIndex        =   251
               Tag             =   "Num.Linea"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr66codpeticion"
               Height          =   405
               Index           =   34
               Left            =   1680
               TabIndex        =   250
               Tag             =   "C�digo Petici�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "fr28fecinicio"
               Height          =   330
               Index           =   3
               Left            =   5520
               TabIndex        =   59
               Tag             =   "Inicio|Fecha Inicio Vigencia"
               Top             =   840
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "fr28fecfin"
               Height          =   330
               Index           =   4
               Left            =   5400
               TabIndex        =   73
               Tag             =   "Fin|Fecha Fin"
               Top             =   2640
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "fr34codvia"
               Height          =   330
               Index           =   1
               Left            =   2760
               TabIndex        =   57
               Tag             =   "V�a|C�digo V�a"
               Top             =   840
               Width           =   765
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0111.frx":00FC
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0111.frx":0118
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1349
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "frg4codfrecuencia"
               Height          =   330
               Index           =   0
               Left            =   960
               TabIndex        =   56
               Tag             =   "Frec.|Cod.Frecuencia"
               Top             =   840
               Width           =   1725
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0111.frx":0134
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0111.frx":0150
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3043
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "frh5codperiodicidad"
               Height          =   330
               Index           =   3
               Left            =   3600
               TabIndex        =   58
               Tag             =   "Period.|Cod. Periodicidad"
               Top             =   840
               Width           =   1845
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0111.frx":016C
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0111.frx":0188
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3254
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin MSComDlg.CommonDialog CommonDialog1 
               Left            =   3480
               Top             =   2520
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   327681
            End
            Begin SSDataWidgets_B.SSDBCombo cboproducto 
               Height          =   315
               Left            =   0
               TabIndex        =   293
               TabStop         =   0   'False
               Top             =   240
               Width           =   5295
               DataFieldList   =   "Column 1"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               FieldSeparator  =   ";"
               DefColWidth     =   9
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2646
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8811
               Columns(1).Caption=   "Medicamento"
               Columns(1).Name =   "Medicamento"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9340
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboproductodil 
               Height          =   315
               Left            =   0
               TabIndex        =   294
               TabStop         =   0   'False
               Top             =   1440
               Width           =   5295
               DataFieldList   =   "Column 1"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               FieldSeparator  =   ";"
               DefColWidth     =   9
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2646
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8811
               Columns(1).Caption=   "Medicamento"
               Columns(1).Name =   "Medicamento"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9340
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboproductomez 
               Height          =   315
               Left            =   0
               TabIndex        =   295
               TabStop         =   0   'False
               Top             =   3240
               Width           =   5295
               DataFieldList   =   "Column 1"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               FieldSeparator  =   ";"
               DefColWidth     =   9
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2646
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8811
               Columns(1).Caption=   "Medicamento"
               Columns(1).Name =   "Medicamento"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9340
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Archivo Inf. F�rmula Magistral"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   84
               Left            =   360
               TabIndex        =   286
               Top             =   2280
               Visible         =   0   'False
               Width           =   2550
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento 2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   83
               Left            =   0
               TabIndex        =   282
               Top             =   3000
               Width           =   1305
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   82
               Left            =   6120
               TabIndex        =   281
               Top             =   3000
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   81
               Left            =   7080
               TabIndex        =   280
               Top             =   3000
               Width           =   300
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   80
               Left            =   5400
               TabIndex        =   279
               Top             =   3000
               Width           =   225
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Med2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   79
               Left            =   0
               TabIndex        =   278
               Top             =   2400
               Visible         =   0   'False
               Width           =   1125
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   78
               Left            =   7800
               TabIndex        =   277
               Top             =   3000
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "ml/hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   77
               Left            =   6420
               TabIndex        =   276
               Top             =   2085
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vel.Perfus."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   52
               Left            =   5400
               TabIndex        =   275
               Top             =   1800
               Width           =   945
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vol.Total(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   51
               Left            =   7800
               TabIndex        =   274
               Top             =   1800
               Width           =   1080
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Instrucciones para administraci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   50
               Left            =   0
               TabIndex        =   273
               Top             =   1800
               Width           =   2865
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   49
               Left            =   7800
               TabIndex        =   272
               Top             =   0
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Sol"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   48
               Left            =   1200
               TabIndex        =   271
               Top             =   2040
               Visible         =   0   'False
               Width           =   930
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   47
               Left            =   0
               TabIndex        =   270
               Top             =   2040
               Visible         =   0   'False
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   37
               Left            =   5400
               TabIndex        =   269
               Top             =   0
               Width           =   225
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tiempo Infusi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   36
               Left            =   6600
               TabIndex        =   268
               Top             =   1200
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   5520
               TabIndex        =   267
               Top             =   1200
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Soluci�n para Diluir"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   34
               Left            =   0
               TabIndex        =   266
               Top             =   1200
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   7320
               TabIndex        =   265
               Top             =   2400
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   33
               Left            =   5520
               TabIndex        =   264
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   32
               Left            =   7440
               TabIndex        =   263
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   31
               Left            =   5400
               TabIndex        =   262
               Top             =   2400
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Periodicidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   3600
               TabIndex        =   261
               Top             =   600
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               Caption         =   "V�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   2760
               TabIndex        =   260
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Frecuencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   960
               TabIndex        =   259
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   7080
               TabIndex        =   258
               Top             =   0
               Width           =   300
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   6120
               TabIndex        =   257
               Top             =   0
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   0
               TabIndex        =   256
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   0
               TabIndex        =   255
               Top             =   0
               Width           =   1140
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   95
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   5
            Left            =   -74880
            TabIndex        =   96
            TabStop         =   0   'False
            Top             =   120
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   5477
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3465
            Index           =   6
            Left            =   420
            TabIndex        =   97
            Top             =   600
            Width           =   8895
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   6
            stylesets(0).Name=   "Estupefacientes"
            stylesets(0).BackColor=   14671839
            stylesets(0).Picture=   "FR0111.frx":01A4
            stylesets(1).Name=   "FormMagistral"
            stylesets(1).BackColor=   8454016
            stylesets(1).Picture=   "FR0111.frx":01C0
            stylesets(2).Name=   "Medicamentos"
            stylesets(2).BackColor=   16777215
            stylesets(2).Picture=   "FR0111.frx":01DC
            stylesets(3).Name=   "MezclaIV2M"
            stylesets(3).BackColor=   8454143
            stylesets(3).Picture=   "FR0111.frx":01F8
            stylesets(4).Name=   "Fluidoterapia"
            stylesets(4).BackColor=   16776960
            stylesets(4).Picture=   "FR0111.frx":0214
            stylesets(5).Name=   "PRNenOM"
            stylesets(5).BackColor=   12615935
            stylesets(5).Picture=   "FR0111.frx":0230
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15690
            _ExtentY        =   6112
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab2 
            Height          =   4000
            Left            =   -74880
            TabIndex        =   224
            TabStop         =   0   'False
            Top             =   120
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   7064
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Medicamento"
            TabPicture(0)   =   "FR0111.frx":024C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(64)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Mezcla IV 2M"
            TabPicture(1)   =   "FR0111.frx":0268
            Tab(1).ControlEnabled=   0   'False
            Tab(1).ControlCount=   0
            TabCaption(2)   =   "PRN en OM"
            TabPicture(2)   =   "FR0111.frx":0284
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(27)"
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "Flu�doterapia"
            TabPicture(3)   =   "FR0111.frx":02A0
            Tab(3).ControlEnabled=   0   'False
            Tab(3).ControlCount=   0
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   9
               Left            =   -74880
               TabIndex        =   225
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   10
               Left            =   -74880
               TabIndex        =   226
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00808080&
               Caption         =   "Form Magistral"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   58
               Left            =   -69025
               TabIndex        =   248
               Top             =   90
               Width           =   1050
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H000000FF&
               Caption         =   "Estupefacientes"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   59
               Left            =   -70440
               TabIndex        =   247
               Top             =   90
               Width           =   1170
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00FFFF80&
               Caption         =   "Flu�doterapia"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   60
               Left            =   -71520
               TabIndex        =   246
               Top             =   90
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FFFF&
               Caption         =   "PRN en OM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   61
               Left            =   -72555
               TabIndex        =   245
               Top             =   90
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FF80&
               Caption         =   "Mezcla IV 2M"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   63
               Left            =   -73725
               TabIndex        =   244
               Top             =   90
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   64
               Left            =   6600
               TabIndex        =   243
               Top             =   1260
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   65
               Left            =   -74880
               TabIndex        =   242
               Top             =   480
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   66
               Left            =   -70920
               TabIndex        =   241
               Top             =   600
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   67
               Left            =   -67320
               TabIndex        =   240
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   68
               Left            =   -69360
               TabIndex        =   239
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   69
               Left            =   -74760
               TabIndex        =   238
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   70
               Left            =   -74760
               TabIndex        =   237
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   71
               Left            =   -70920
               TabIndex        =   236
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   72
               Left            =   -72840
               TabIndex        =   235
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   73
               Left            =   -74760
               TabIndex        =   234
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   74
               Left            =   -74760
               TabIndex        =   233
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que env�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   75
               Left            =   -69720
               TabIndex        =   232
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Colegiado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   76
               Left            =   -72120
               TabIndex        =   231
               Top             =   1320
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Mezcla IV 2M"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   22
               Left            =   1275
               TabIndex        =   230
               Top             =   -510
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "PRN en OM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   27
               Left            =   -72550
               TabIndex        =   229
               Top             =   -810
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Flu�doterapia"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   43
               Left            =   3480
               TabIndex        =   228
               Top             =   -510
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Estupefacientes"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   45
               Left            =   4600
               TabIndex        =   227
               Top             =   -510
               Width           =   1170
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   44
            Left            =   420
            TabIndex        =   101
            Top             =   210
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H0080FFFF&
            Caption         =   "Mezcla IV 2M"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   53
            Left            =   1935
            TabIndex        =   100
            Top             =   210
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFC0FF&
            Caption         =   "PRN en OM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   54
            Left            =   3345
            TabIndex        =   99
            Top             =   210
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFF80&
            Caption         =   "Flu�doterapia"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   55
            Left            =   4620
            TabIndex        =   98
            Top             =   210
            Width           =   1275
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   94
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRedactarOMPRN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedactarOMPRN (FR0111.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: redactar Orden M�dica                                   *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim noformulario As Boolean

Dim vntAux As Variant
Dim intDia(30) As Integer
Dim intNumDia As Integer
Dim mstrOperacion As String 'saber que tipo de linea estamos creando en el hijo
'mstrOperacion = "/" 'Medicamento
'mstrOperacion = "M" 'Mezcla IV 2M
'mstrOperacion = "P" 'PRN en OM
'mstrOperacion = "F" 'Fluidoterapia
'mstrOperacion = "E" 'Estupefaciente
'mstrOperacion = "L" 'Form.Magistral
Dim intCambioAlgo As Integer 'Para saber si cambian las horas y minutos, por no ser campos CW
Dim blnDetalle As Boolean 'Para saber si estoy en modo detalle en el hijo
Dim blnBoton As Boolean 'Para saber si he pulsado un boton
Dim intBotonMasInf As Integer 'Para dar informacion en funcion del campo en el que este el cursor
Dim blnFormActRest As Boolean
Dim blnObligatorio As Boolean
Dim strOrigen As String 'Para saber si el evento change se produce por modificaciones en el campo o no;
                        'vale V cuando se ha modificado manualmente la velocidad de perfusi�n,
                        'y vale T cuando se ha modificado manualmente el tiempo de perfusi�n
Dim blnNoVerAgen As Boolean 'True: No hay que mostrar la agenda
Dim blnNoPrimAct As Boolean
Dim FormMagistral As Boolean
Dim blnInload As Boolean

Private Function blnErrorFecFin()
  'Devuelte TRUE si alguna de las fechas de fin est� mal
  Dim intNumLin As Integer
  Dim strSQL As String
  Dim qrySQL As rdoQuery
  Dim rstSQL As rdoResultset
  Dim mvarBkmrk As Variant
  Dim mintisel As Integer
  Dim blnError As Boolean
  Dim blnCerrarSQL As Boolean
  
  blnCerrarSQL = False
  blnError = False
  strSQL = "SELECT TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')-TO_DATE(?,'DD/MM/YYYY') FROM DUAL"
  Set qrySQL = objApp.rdoConnect.CreateQuery("", strSQL)
  
  intNumLin = grdDBGrid1(6).Rows
  For mintisel = 0 To intNumLin - 1
    mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
    If Not IsNull(grdDBGrid1(6).Columns("Fin").CellValue(mvarBkmrk)) Then
      'On Error GoTo ErrorFec
      qrySQL(0) = Format(grdDBGrid1(6).Columns("Fin").CellValue(mvarBkmrk), "dd/mm/yyyy")
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        blnCerrarSQL = True
        If rstSQL.rdoColumns(0).Value > 0 Then
          blnError = True
        End If
      Else
        blnError = True
      End If
    End If
  Next mintisel
     
  If blnCerrarSQL Then
    qrySQL.Close
    Set qrySQL = Nothing
    Set rstSQL = Nothing
  End If
  blnErrorFecFin = blnError
  Exit Function
ErrorFec:
  blnError = True
  blnErrorFecFin = blnError
  
End Function
Private Sub RealizarApuntes()
  'Al realizar la dispensaci�n se almacenan los movimientos, tanto de salida como de entrada
  'FR8000 es la tabla de salidas de almac�n: sacamos del almac�n de Farmacia y lo pasamos al almac�n del servicio
  'FR3500 es la tabla de entradas de almac�n: metemos en el almac�n del Servicio lo sacado de Farmacia
  'FR6500 esla tabla en la que se almacenan lo entregado a un servicio
  
  
  Dim strCodDpto As String 'Para guardar el c�digo del Servicio
  Dim strNumNec As String 'Para guardar el n�mero de necesidad
  Dim strFR19 As String
  Dim qryFR19 As rdoQuery
  Dim rdoFR19 As rdoResultset
  Dim strIns As String
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rdoFR73 As rdoResultset
  Dim strTamEnv As String
  Dim strCant As String
  Dim strUpd As String
  Dim qryUpd As rdoQuery
  Dim rdoUpd As rdoResultset
  Dim strCP As String
  Dim strDptoFar As String
  Dim strIns80 As String
  Dim strIns35 As String
  Dim strFRH2 As String
  Dim qryFRH2 As rdoQuery
  Dim rdoFRH2 As rdoResultset
  Dim strAlmFar As String
  Dim blnEntroEnWhile As Boolean
  Dim strSQL As String
  Dim rdoSql As rdoResultset
  Dim strCodMov As String
  Dim strCod80 As String
  Dim strCod35 As String
  Dim strFR04 As String
  Dim qryFR04  As rdoQuery
  Dim rdoFR04 As rdoResultset
  Dim strAlmDes As String
  Dim strCodNec As String
  Dim strCantDil As String
  Dim strCant2 As String
  Dim strhora As String
  Dim strSqlHora As String
  Dim strFR66 As String
  Dim rstFR66 As rdoResultset
  
  'On Error GoTo Err_Rea_Apu
  'Screen.MousePointer = vbHourglass
  blnEntroEnWhile = False
  strCodDpto = txtText1(16).Text
  strNumNec = txtText1(0).Text
    
  strFR73 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?"
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  
  strFR66 = "SELECT * FROM FR6600 WHERE FR66CODPETICION=" & strNumNec
  Set rstFR66 = objApp.rdoConnect.OpenResultset(strFR66)
  
  strFR04 = "SELECT * FROM FR0400 WHERE AD02CODDPTO = ?"
  Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
  qryFR04(0) = strCodDpto
  Set rdoFR04 = qryFR04.OpenResultset()
  If Not rdoFR04.EOF Then
    If Not IsNull(rdoFR04.rdoColumns("FR04CODALMACEN").Value) Then
      strAlmDes = rdoFR04.rdoColumns("FR04CODALMACEN").Value
    Else
     strAlmDes = 999 'Almac�n ficticio
    End If
  Else
     strAlmDes = 999 'Almac�n ficticio
  End If
  qryFR04.Close
  Set qryFR04 = Nothing
  Set rdoFR04 = Nothing
  
  'Se obtiene el almac�n principal de Farmacia
  strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
  Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
  qryFRH2(0) = 28
  Set rdoFRH2 = qryFRH2.OpenResultset()
  If IsNull(rdoFRH2.rdoColumns(0).Value) Then
    strAlmFar = "0"
  Else
    strAlmFar = rdoFRH2.rdoColumns(0).Value
  End If
  qryFRH2.Close
  Set qryFRH2 = Nothing
  Set rdoFRH2 = Nothing
  
  strFR19 = "SELECT * " & _
            "  FROM FR2800 " & _
            " WHERE FR66CODPETICION = ? " & _
            "   AND FR28INDCOMIENINMED = ? "
            
  Set qryFR19 = objApp.rdoConnect.CreateQuery("", strFR19)
  qryFR19(0) = strNumNec
  'qryFR19(1) = 0
  qryFR19(1) = -1
  
  
  'Se tienen las l�neas dispensadas en ese momento
  Set rdoFR19 = qryFR19.OpenResultset()
  While Not rdoFR19.EOF
    blnEntroEnWhile = True
    
    
    If Not IsNull(rdoFR19.rdoColumns("FR28CANTIDAD").Value) Then
      strCant = CCur(rdoFR19.rdoColumns("FR28CANTIDAD").Value)
    Else
      strCant = 0
    End If
    strCant = objGen.ReplaceStr(strCant, ",", ".", 1)
    
    strCodNec = strNumNec
    
    
    'Se realiza el insert en FR6500
    strIns = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA," & _
                "FR65CANTIDAD,FR73CODPRODUCTO_DIL," & _
                "FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2," & _
                "FR93CODUNIMEDIDA_2,FR65DESPRODUCTO,FR65DOSIS,FR93CODUNIMEDIDA," & _
                "FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO," & _
                "FR65INDINTERCIENT,AD02CODDPTO_CRG) VALUES ("
    strIns = strIns & "FR65CODIGO_SEQUENCE.NEXTVAL,"
    strIns = strIns & txtText1(2).Text & ","
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    Else
      strIns = strIns & "999999999" & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    End If
    strIns = strIns & strCant & ","
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      qryFR73(0) = rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value
      Set rdoFR73 = qryFR73.OpenResultset()
      If Not rdoFR73.EOF Then
        If Not IsNull(rdoFR73.rdoColumns("FR73VOLUMEN").Value) Then
          If rdoFR73.rdoColumns("FR73VOLUMEN").Value > 0 Then
            strIns = strIns & objGen.ReplaceStr(Format(CCur(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value) / CCur(rdoFR73.rdoColumns("FR73VOLUMEN").Value), "##0.00"), ",", ".", 1) & ","
            strCantDil = objGen.ReplaceStr(Format(CCur(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value) / CCur(rdoFR73.rdoColumns("FR73VOLUMEN").Value), "##0.00"), ",", ".", 1)
          Else
            strIns = strIns & objGen.ReplaceStr(1, ",", ".", 1) & ","
            strCantDil = objGen.ReplaceStr(1, ",", ".", 1)
          End If
        Else
          strIns = strIns & objGen.ReplaceStr(1, ",", ".", 1) & ","
          strCantDil = objGen.ReplaceStr(1, ",", ".", 1)
        End If
      Else
        strIns = strIns & objGen.ReplaceStr(1, ",", ".", 1) & ","
        strCantDil = objGen.ReplaceStr(1, ",", ".", 1)
      End If
    Else
      strIns = strIns & "0,"
      strCantDil = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS_2").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1) & ","
       If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value) Then
         qryFR73(0) = rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value
         Set rdoFR73 = qryFR73.OpenResultset()
         If Not rdoFR73.EOF Then
           If Not IsNull(rdoFR73.rdoColumns("FR73DOSIS").Value) Then
             If rdoFR73.rdoColumns("FR73DOSIS").Value > 0 Then
               strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / rdoFR73.rdoColumns("FR73DOSIS").Value, "##0.00")
             Else
               strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / 1, "##0.00")
             End If
           Else
             strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / 1, "##0.00")
           End If
         Else
           strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / 1, "##0.00")
         End If
       Else
        strCant2 = 0
       End If
    Else
      strIns = strIns & "0,"
      strCant2 = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DESPRODUCTO").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR28DESPRODUCTO").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
    Else
      strIns = strIns & "0,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    strIns = strIns & txtText1(0).Text & ",1," & txtText1(60) & "," & txtText1(59) & ","
    If IsNull(rstFR66.rdoColumns("FR66INDINTERCIENT").Value) Then
         strIns = strIns & "null" & ","
    Else
         strIns = strIns & rstFR66.rdoColumns("FR66INDINTERCIENT").Value & ","
    End If
    If IsNull(rstFR66.rdoColumns("AD02CODDPTO_CRG").Value) Then
         strIns = strIns & "null"
    Else
         strIns = strIns & rstFR66.rdoColumns("AD02CODDPTO_CRG").Value
    End If
    strIns = strIns & ")"
    objApp.rdoConnect.Execute strIns, 64
    
'    strSQL = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
'    Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
'    strCod80 = rdoSql.rdoColumns(0).Value
'    rdoSql.Close
'    Set rdoSql = Nothing
'
'    'Se realiza el insert en FR8000: salidas de almac�n
'    If (Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO").Value)) Then
'      If rdoFR19.rdoColumns("FR73CODPRODUCTO").Value <> "999999999" Then
'        strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
'                   "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN) VALUES (" & _
'                   strCod80 & "," & strAlmFar & "," & strAlmDes & ",7,SYSDATE," & _
'                   rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & "," & objGen.ReplaceStr(strCant, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCant, ",", ".", 1) & ")"
'        objApp.rdoConnect.Execute strIns80, 64
'       End If
'    End If
'
'    If (Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value)) Then
'      If rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value <> "999999999" Then
'        strSQL = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
'        Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
'        strCod80 = rdoSql.rdoColumns(0).Value
'        rdoSql.Close
'        Set rdoSql = Nothing
'        strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
'                   "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN) VALUES (" & _
'                   strCod80 & "," & strAlmFar & "," & strAlmDes & ",7,SYSDATE," & _
'                   rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value & "," & objGen.ReplaceStr(strCantDil, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCantDil, ",", ".", 1) & ")"
'        objApp.rdoConnect.Execute strIns80, 64
'       End If
'    End If
'    If (Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value)) Then
'      If rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value <> "999999999" Then
'        strSQL = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
'        Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
'        strCod80 = rdoSql.rdoColumns(0).Value
'        rdoSql.Close
'        Set rdoSql = Nothing
'        strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
'                   "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN) VALUES (" & _
'                   strCod80 & "," & strAlmFar & "," & strAlmDes & ",7,SYSDATE," & _
'                   rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value & "," & objGen.ReplaceStr(strCant2, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCant2, ",", ".", 1) & ")"
'        objApp.rdoConnect.Execute strIns80, 64
'       End If
'    End If
'
'    strSQL = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
'    Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
'    strCod35 = rdoSql.rdoColumns(0).Value
'    rdoSql.Close
'    Set rdoSql = Nothing
'    'Se realiza el insert en FR3500: entradas de almac�n
'    If (Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO").Value)) Then
'      If rdoFR19.rdoColumns("FR73CODPRODUCTO").Value <> "999999999" Then
'        strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
'                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD," & _
'                   "FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & strCod35 & "," & _
'                   strAlmFar & "," & strAlmDes & ",7,SYSDATE," & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & "," & objGen.ReplaceStr(strCant, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCant, ",", ".", 1) & ")"
'        objApp.rdoConnect.Execute strIns35, 64
'      End If
'    End If
'    If (Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value)) Then
'      If rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value <> "999999999" Then
'        strSQL = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
'        Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
'        strCod35 = rdoSql.rdoColumns(0).Value
'        rdoSql.Close
'        Set rdoSql = Nothing
'        strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
'                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD," & _
'                   "FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & strCod35 & "," & _
'                   strAlmFar & "," & strAlmDes & ",7,SYSDATE," & rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value & "," & objGen.ReplaceStr(strCantDil, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCantDil, ",", ".", 1) & ")"
'        objApp.rdoConnect.Execute strIns35, 64
'       End If
'    End If
'    If (Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value)) Then
'      If rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value <> "999999999" Then
'        strSQL = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
'        Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
'        strCod35 = rdoSql.rdoColumns(0).Value
'        rdoSql.Close
'        Set rdoSql = Nothing
'        strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
'                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD," & _
'                   "FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & strCod35 & "," & _
'                   strAlmFar & "," & strAlmDes & ",7,SYSDATE," & rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value & "," & objGen.ReplaceStr(strCant2, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCant2, ",", ".", 1) & ")"
'        objApp.rdoConnect.Execute strIns35, 64
'       End If
'    End If
    rdoFR19.MoveNext
  Wend
  
  If blnEntroEnWhile Then
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rdoFR73 = Nothing
  End If
  
  qryFR19.Close
  Set qryFR19 = Nothing
  Set rdoFR19 = Nothing
Err_Rea_Apu:
  'Screen.MousePointer = vbDefault
End Sub

Private Sub Imprimir(strListado As String, intDes As Integer, strCodPRN As String)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{AD0200.AD02CODDPTO}={FR6601J.AD02CODDPTO} AND " & _
             "{FR6601J.FR66CODPETICION}={FR2800.FR66CODPETICION} AND " & _
             "{FR2800.FR73CODPRODUCTO}={FR7300.FR73CODPRODUCTO} AND " & _
             "{FR6601J.FR66CODPETICION}=" & strCodPRN
             
             
  CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

Private Sub Generar_PRN()
  '1.- Genera un PRN para las l�neas de la OM que est�n en modo STAT
  '2.- Genera la etiqueta
  '3.- Quita el STAT
  Dim strCodPet As String
  Dim strFR28 As String
  Dim qryFR28 As rdoQuery
  Dim rdoFR28 As rdoResultset
  Dim strInsFR66 As String
  Dim strSQL As String
  Dim rdoSql As rdoResultset
  Dim strInsFR28 As String
  Dim strUpdFR28 As String
  Dim qryUpdFR28 As rdoQuery
  Dim strmedidadil As String
  Dim curDosis2 As Currency
  Dim curCantidad2 As Currency
  Dim auxProd2 As String
  Dim strProd2 As String
  Dim qryProd2 As rdoQuery
  Dim rstProd2 As rdoResultset
  Dim auxProd1 As String
  Dim strProd1 As String
  Dim qryProd1 As rdoQuery
  Dim rstProd1 As rdoResultset
  Dim curDosis1 As Currency
  Dim curCantidad1 As Currency
  Dim vntAuxCantidad1 As Variant
  Dim vntAuxCantidad2 As Variant
  Dim mintisel As Integer
  Dim mintNTotalSelRows As Integer
  Dim mvarBkmrk As Variant
  Dim strlinea As String
  Dim strpaciente As String
  Dim qrypaciente As rdoQuery
  Dim rstpaciente As rdoResultset
  Dim Auxcama As String
  Dim strcama As String
  Dim qrycama As rdoQuery
  Dim rstcama As rdoResultset
  Dim auxDpto As String
  Dim strdpto As String
  Dim qrydpto As rdoQuery
  Dim rstdpto As rdoResultset
  Dim auxSolucion As String
  Dim strSolucion As String
  Dim qrySolucion As rdoQuery
  Dim rstSolucion As rdoResultset
  Dim strcaducidad As String
  Dim strfecha As String
  Dim rstfecha As rdoResultset
  Dim Auxfecha As String
  Dim auxVia As String
  Dim strVia As String
  Dim qryVia As rdoQuery
  Dim rstVia As rdoResultset
  Dim strHoras As String
  Dim strMinutos As String
  Dim strHAdm As String
  Dim strInstrucctiones As String
  Dim i As Integer
  Dim auxInstrucctiones As String
  Dim auxVolTotal As String
  Dim auxstr As String
  Dim auxcant As String
  Dim crlf As String
  Dim Incremento As Integer
  Dim contLinea As Integer
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rdoFR73 As rdoResultset
  Dim blnHaySTAT As Boolean
  Dim strUbi As String
  
  blnHaySTAT = False
  Me.Enabled = False
  strCodPet = txtText1(0).Text
  strFR28 = "SELECT * FROM FR2800 WHERE FR66CODPETICION = ? AND FR28INDCOMIENINMED = ? "
  Set qryFR28 = objApp.rdoConnect.CreateQuery("", strFR28)
  qryFR28(0) = strCodPet
  qryFR28(1) = -1
  Set rdoFR28 = qryFR28.OpenResultset()
  While Not rdoFR28.EOF
    blnHaySTAT = True
    strSQL = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSQL)
    
    strInsFR66 = "INSERT INTO FR6600 (FR66CODPETICION,FR66INDPACIDIABET,FR66INDRESTVOLUM,FR66OBSERV," & _
                 "SG02COD_MED,AD02CODDPTO_MED,FR66FECFIRMMEDI,FR66HORAFIRMMEDI," & _
                 "AD02CODDPTO,CI21CODPERSONA,FR66CODOMORIPRN,FR66INDINTERCIENT," & _
                 "AD02CODDPTO_CRG,FR66FECREDACCION,FR66HORAREDACCI,FR26CODESTPETIC," & _
                 "FR66PESOPAC,FR66ALTUPAC,FR66INDALERGIA,AD07CODPROCESO,AD01CODASISTENCI,FR66INDCESTA,FR66INDOM) " & _
                 " VALUES ("
    strInsFR66 = strInsFR66 & rdoSql.rdoColumns(0).Value & ","
    If chkCheck1(0).Value = 1 Then
      strInsFR66 = strInsFR66 & "-1" & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If chkCheck1(7).Value = 1 Then
      strInsFR66 = strInsFR66 & "-1" & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If Len(Trim(txtText1(18).Text)) > 0 Then
      strInsFR66 = strInsFR66 & "'" & txtText1(18).Text & "',"
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    If Len(Trim(txtText1(8).Text)) > 0 Then
      strInsFR66 = strInsFR66 & "'" & txtText1(8).Text & "',"
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    If Len(Trim(txtText1(12).Text)) > 0 Then
      strInsFR66 = strInsFR66 & txtText1(12).Text & ","
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    strInsFR66 = strInsFR66 & "SYSDATE" & ","
    If IsNumeric(txtText1(20).Text) Then
      strInsFR66 = strInsFR66 & objGen.ReplaceStr(txtText1(20).Text, ",", ".", 1) & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If IsNumeric(txtText1(16).Text) Then
      strInsFR66 = strInsFR66 & txtText1(16).Text & ","
    Else
      strInsFR66 = strInsFR66 & "215" & ","
    End If
    If IsNumeric(txtText1(2).Text) Then
      strInsFR66 = strInsFR66 & txtText1(2).Text & ","
    Else
      strInsFR66 = strInsFR66 & "1" & ","
    End If
    If IsNumeric(txtText1(0).Text) Then
      strInsFR66 = strInsFR66 & txtText1(0).Text & ","
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    If chkCheck1(15).Value = 1 Then
      strInsFR66 = strInsFR66 & "-1" & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If IsNumeric(txtText1(12).Text) Then
      strInsFR66 = strInsFR66 & txtText1(12).Text & ","
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    strInsFR66 = strInsFR66 & "SYSDATE" & ","
    If IsNumeric(txtText1(20).Text) Then
      strInsFR66 = strInsFR66 & objGen.ReplaceStr(txtText1(20).Text, ",", ".", 1) & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    strInsFR66 = strInsFR66 & "3" & ","
    If IsNumeric(txtText1(32).Text) Then
      strInsFR66 = strInsFR66 & txtText1(32).Text & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If IsNumeric(txtText1(33).Text) Then
      strInsFR66 = strInsFR66 & txtText1(33).Text & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If chkCheck1(2).Value = 1 Then
      strInsFR66 = strInsFR66 & "-1" & ","
    Else
      strInsFR66 = strInsFR66 & "0" & ","
    End If
    If IsNumeric(txtText1(59).Text) Then
      strInsFR66 = strInsFR66 & txtText1(59).Text & ","
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    If IsNumeric(txtText1(60).Text) Then
      strInsFR66 = strInsFR66 & txtText1(60).Text & ","
    Else
      strInsFR66 = strInsFR66 & "null" & ","
    End If
    strInsFR66 = strInsFR66 & "0,0)"
    objApp.rdoConnect.Execute strInsFR66, 64
    
    strInsFR28 = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR28DOSIS," & _
                 "FR93CODUNIMEDIDA,FR28VOLUMEN,FR28TIEMINFMIN,FR34CODVIA,FRG4CODFRECUENCIA," & _
                 "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,FR28INDBLOQUEADA," & _
                 "FR28INDESTOM,FRH7CODFORMFAR,FR28OPERACION,FR28CANTIDAD,FR28FECINICIO," & _
                 "FR28HORAINICIO,FR28FECFIN,FR28HORAFIN,FR28INDVIAOPC,FR73CODPRODUCTO_DIL, " & _
                 "FR28CANTIDADDIL,FR28TIEMMININF,FR28REFERENCIA,FR28UBICACION,FR28INDPERF, " & _
                 "FR28INSTRADMIN,FR28VELPERFUSION,FR28VOLTOTAL,FR73CODPRODUCTO_2," & _
                 "FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28DESPRODUCTO," & _
                 "FR28PATHFORMAG,FR28VOLUMEN_2,FR28DIASADM) VALUES ("
                 
    strInsFR28 = strInsFR28 & rdoSql.rdoColumns(0).Value & ",1,"
    If Not IsNull(rdoFR28.rdoColumns("FR73CODPRODUCTO").Value) Then
      strInsFR28 = strInsFR28 & rdoFR28.rdoColumns("FR73CODPRODUCTO").Value & ","
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28DOSIS").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR93CODUNIMEDIDA").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR93CODUNIMEDIDA").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28VOLUMEN").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28VOLUMEN").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28TIEMINFMIN").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28TIEMINFMIN").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR34CODVIA").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR34CODVIA").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FRG4CODFRECUENCIA").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FRG4CODFRECUENCIA").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28INDDISPPRN").Value) Then
      strInsFR28 = strInsFR28 & rdoFR28.rdoColumns("FR28INDDISPPRN").Value & ",0,'Diario',0,0,"
    Else
      strInsFR28 = strInsFR28 & "-1,0,'Diario',0,0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FRH7CODFORMFAR").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FRH7CODFORMFAR").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28OPERACION").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR28OPERACION").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "'P',"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28CANTIDAD").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28CANTIDAD").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    strInsFR28 = strInsFR28 & "SYSDATE,0,SYSDATE,null,"
    If Not IsNull(rdoFR28.rdoColumns("FR28INDVIAOPC").Value) Then
      strInsFR28 = strInsFR28 & rdoFR28.rdoColumns("FR28INDVIAOPC").Value & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strInsFR28 = strInsFR28 & rdoFR28.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28CANTIDADDIL").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28TIEMMININF").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28TIEMMININF").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28REFERENCIA").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR28REFERENCIA").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28UBICACION").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR28UBICACION").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28INDPERF").Value) Then
      strInsFR28 = strInsFR28 & rdoFR28.rdoColumns("FR28INDPERF").Value & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28INSTRADMIN").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR28INSTRADMIN").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28VELPERFUSION").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28VELPERFUSION").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28VOLTOTAL").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28VOLTOTAL").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR73CODPRODUCTO_2").Value) Then
      strInsFR28 = strInsFR28 & rdoFR28.rdoColumns("FR73CODPRODUCTO_2").Value & ","
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FRH7CODFORMFAR_2").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FRH7CODFORMFAR_2").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28DOSIS_2").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR93CODUNIMEDIDA_2").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28DESPRODUCTO").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR28DESPRODUCTO").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28PATHFORMAG").Value) Then
      strInsFR28 = strInsFR28 & "'" & rdoFR28.rdoColumns("FR28PATHFORMAG").Value & "',"
    Else
      strInsFR28 = strInsFR28 & "null,"
    End If
    If Not IsNull(rdoFR28.rdoColumns("FR28VOLUMEN_2").Value) Then
      strInsFR28 = strInsFR28 & objGen.ReplaceStr(rdoFR28.rdoColumns("FR28VOLUMEN_2").Value, ",", ".", 1) & ","
    Else
      strInsFR28 = strInsFR28 & "0,"
    End If
    strInsFR28 = strInsFR28 & "null)"
    objApp.rdoConnect.Execute strInsFR28, 64
    rdoFR28.MoveNext
  Wend
 
    
  If (blnHaySTAT) Or (chkOMPRN(6).Value = 1) Then
    blnImpresoraSeleccionada = False
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    If blnImpresoraSeleccionada = False Then
      MsgBox "No se ha seleccionado ninguna impresora", vbInformation, "PRN"
      cmdenviar.Enabled = True
      Exit Sub
    End If
    
    strFR73 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?"
    Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
    
    'Para cada l�nea del grid
    mintNTotalSelRows = grdDBGrid1(6).Rows
    For mintisel = 0 To mintNTotalSelRows - 1
     
     If ((grdDBGrid1(6).Columns("STAT").CellValue(mvarBkmrk) = True) Or (chkOMPRN(6).Value = 1)) And (grdDBGrid1(6).Columns("PERF").CellValue(mvarBkmrk) = False) Then
      mvarBkmrk = grdDBGrid1(6).RowBookmark(mintisel)
      crlf = Chr$(13) & Chr$(10)
      Incremento = 30
      contLinea = 0
  
      strlinea = crlf
      strlinea = strlinea & "N" & crlf
      strlinea = strlinea & "I8,1,034" & crlf
      'strlinea = strlinea & "Q599,24" & crlf
      strlinea = strlinea & "Q250,0" & crlf
      strlinea = strlinea & "R0,0" & crlf
      strlinea = strlinea & "S2" & crlf
      strlinea = strlinea & "D5" & crlf
      strlinea = strlinea & "ZB" & crlf

      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
    
      strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio de Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
    
      strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
      contLinea = contLinea + 1
    
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
        
      strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
      Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
      qrydpto(0) = txtText1(0).Text 'grdDBGrid1(0).Columns("C�digo Petici�n").CellValue(mvarBkmrk)
      Set rstdpto = qrydpto.OpenResultset(strdpto)
      If Not rstdpto.EOF Then
        auxDpto = rstdpto(0)
      Else
        auxDpto = ""
      End If
      rstdpto.Close
      qrydpto.Close
  
      If Len(Trim(txtText1(46).Text)) >= 0 Then
        strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= GCFN07(?) "
        Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
        qrycama(0) = txtText1(46).Text 'grdDBGrid1(0).Columns("Cama").CellValue(mvarBkmrk)
        Set rstcama = qrycama.OpenResultset(strcama)
        If Not rstcama.EOF Then
          Auxcama = rstcama(0)
        Else
          Auxcama = ""
        End If
        rstcama.Close
        qrycama.Close
      Else
        Auxcama = "SIN CAMA"
      End If

      strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
      Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
      qrypaciente(0) = txtText1(2).Text 'grdDBGrid1(0).Columns("Persona").CellValue(mvarBkmrk)
      Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
      If Not rstpaciente.EOF Then
        If Not IsNull(rstpaciente(0)) Then
          strlinea = strlinea & Texto_Etiqueta(formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxDpto, 20, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          strlinea = strlinea & Texto_Etiqueta(formatear("", 30, True, True) & " " & formatear(auxDpto, 20, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        End If
        If Not IsNull(rstpaciente(1)) Then
          strlinea = strlinea & Texto_Etiqueta(formatear(rstpaciente(1), 30, True, True) & " " & Auxcama, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          strlinea = strlinea & Texto_Etiqueta(formatear("", 30, True, True) & " " & Auxcama, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        End If
      End If
      rstpaciente.Close
      qrypaciente.Close
  
  
      strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
      Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
      Auxfecha = rstfecha(0).Value
  
      auxVia = ""
      
      strHoras = ""
      strMinutos = ""
      
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      qryFR73(0) = grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
      Set rdoFR73 = qryFR73.OpenResultset()
      If IsNull(rdoFR73.rdoColumns("FRH9UBICACION").Value) Then
        strUbi = ""
      Else
        strUbi = rdoFR73.rdoColumns("FRH9UBICACION").Value
      End If
      If grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk) = "999999999" Then
        auxProd1 = strUbi & " " & rdoFR73.rdoColumns("FR73CODINTFAR").Value & " " & _
                   grdDBGrid1(6).Columns("Medic.No Form").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(6).Columns("Cantidad").CellValue(mvarBkmrk)
      Else
        auxProd1 = strUbi & " " & rdoFR73.rdoColumns("FR73CODINTFAR").Value & " " & _
                   grdDBGrid1(6).Columns("Medicamento").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(6).Columns("Cantidad").CellValue(mvarBkmrk)
        
        
        If Not IsNull(rdoFR73.rdoColumns("FR73INDPSICOT").Value) Then
          If rdoFR73.rdoColumns("FR73INDPSICOT").Value = -1 Then
            auxProd1 = auxProd1 & " (PSIC)"
          End If
        End If
        If Not IsNull(rdoFR73.rdoColumns("FR73INDESTUPEFACI").Value) Then
          If rdoFR73.rdoColumns("FR73INDESTUPEFACI").Value = -1 Then
            auxProd1 = auxProd1 & " (ESTU)"
          End If
        End If
      End If
      
      auxProd1 = auxProd1 & " " & grdDBGrid1(6).Columns("Dosis").CellValue(mvarBkmrk)
      auxProd1 = auxProd1 & " " & grdDBGrid1(6).Columns("UM").CellValue(mvarBkmrk)
      auxProd1 = auxProd1 & " " & grdDBGrid1(6).Columns("Volum.").CellValue(mvarBkmrk)
      strlinea = strlinea & Texto_Etiqueta("  " & auxProd1, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
  
      If Len(Trim(grdDBGrid1(6).Columns("C�digo Producto diluir").CellValue(mvarBkmrk))) > 0 Then
        qryFR73(0) = grdDBGrid1(6).Columns("C�digo Producto diluir").CellValue(mvarBkmrk)
        Set rdoFR73 = qryFR73.OpenResultset()
      End If
      
      If IsNull(rdoFR73.rdoColumns("FRH9UBICACION").Value) Then
        strUbi = ""
      Else
        strUbi = rdoFR73.rdoColumns("FRH9UBICACION").Value
      End If
      
      auxProd2 = strUbi & " " & rdoFR73.rdoColumns("FR73CODINTFAR").Value & " " & _
                grdDBGrid1(6).Columns("Descripci�n Producto Diluir").CellValue(mvarBkmrk) & " " & _
                grdDBGrid1(6).Columns("Vol.Dil.").CellValue(mvarBkmrk) & " " & _
                grdDBGrid1(6).Columns("T.Inf.(min)").CellValue(mvarBkmrk) & "(DIL)"

      strlinea = strlinea & Texto_Etiqueta("  " & auxProd2, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      If Len(Trim(grdDBGrid1(6).Columns("C�digo Medicamento 2").CellValue(mvarBkmrk))) > 0 Then
        qryFR73(0) = grdDBGrid1(6).Columns("C�digo Medicamento 2").CellValue(mvarBkmrk)
        Set rdoFR73 = qryFR73.OpenResultset()
      End If
      If IsNull(rdoFR73.rdoColumns("FRH9UBICACION").Value) Then
        strUbi = ""
      Else
        strUbi = rdoFR73.rdoColumns("FRH9UBICACION").Value
      End If
      auxProd2 = strUbi & " " & rdoFR73.rdoColumns("FR73CODINTFAR").Value & " " & _
                grdDBGrid1(6).Columns("Medicamento 2").CellValue(mvarBkmrk) & " " & _
                grdDBGrid1(6).Columns("Dosis2").CellValue(mvarBkmrk) & " " & _
                grdDBGrid1(6).Columns("Vol.Prod.2").CellValue(mvarBkmrk) & " " & _
                grdDBGrid1(6).Columns("UM2").CellValue(mvarBkmrk) & "(MEDIC 2)"

      strlinea = strlinea & Texto_Etiqueta("  " & auxProd2, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      strInstrucctiones = grdDBGrid1(6).Columns("Inst.Admin.").CellValue(mvarBkmrk)
      i = 0
      auxInstrucctiones = ""
      While strInstrucctiones <> "" And i <> 5
        If InStr(strInstrucctiones, Chr(13)) > 0 Then
          auxInstrucctiones = Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)) - 1)
          strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
          strlinea = strlinea & Texto_Etiqueta(formatear(auxInstrucctiones, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          If Chr(10) <> strInstrucctiones Then
            auxInstrucctiones = strInstrucctiones
            strInstrucctiones = ""
            strlinea = strlinea & Texto_Etiqueta(formatear(auxInstrucctiones, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          Else
            strInstrucctiones = ""
          End If
        End If
        i = i + 1
      Wend
    
      strlinea = strlinea & "P1" & crlf
      strlinea = strlinea & " " & crlf
    
      Printer.Print strlinea
      Printer.EndDoc
     End If
    Next mintisel  'Fin para l�nea de cada grid
  
    '++++++
    'Call Imprimir("FR1711.RPT", 1, rdoSql.rdoColumns(0).Value)
    
  'rdoFR73.Close
  qryFR73.Close
  Set qryFR73 = Nothing
  Set rdoFR73 = Nothing
  
  strUpdFR28 = "UPDATE FR2800 " & _
               "   SET FR28INDCOMIENINMED = ? " & _
               " WHERE FR66CODPETICION = ? "
  Set qryUpdFR28 = objApp.rdoConnect.CreateQuery("", strUpdFR28)
  qryUpdFR28(0) = 0
  qryUpdFR28(1) = strCodPet
  qryUpdFR28.Execute
  
  
  qryUpdFR28.Close
  Set qryUpdFR28 = Nothing
  qryFR28.Close
 End If
 Set qryFR28 = Nothing
 Set rdoFR28 = Nothing
  'rdoSQL.Close
 Set rdoSql = Nothing
 Me.Enabled = True
End Sub

Private Sub chkOMPRN_Click(Index As Integer)

If objWinInfo.intWinStatus = 1 Then
  If chkOMPRN(6).Value = 1 Then
    If chkCheck1(12).Value <> 0 Then
      Call objWinInfo.CtrlSet(chkCheck1(12), 0)
    End If
  Else
    If chkCheck1(12).Value <> 1 Then
      Call objWinInfo.CtrlSet(chkCheck1(12), 1)
    End If
  End If
End If

End Sub

Private Sub cmdcopiarlinea_Click()
Dim strinsert As String
Dim linea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset

Screen.MousePointer = vbHourglass
cmdcopiarlinea.Enabled = False
If IsNumeric(txtText1(0).Text) And IsNumeric(txtText1(45).Text) Then
  
  strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
              txtText1(0).Text
  Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
  linea = rstlinea.rdoColumns(0).Value + 1
  rstlinea.Close
  Set rstlinea = Nothing

  strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR28DOSIS,"
  strinsert = strinsert & "FR93CODUNIMEDIDA,FR28VOLUMEN,FR28TIEMINFMIN,FR34CODVIA,FRG4CODFRECUENCIA,"
  strinsert = strinsert & "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,FR28INDBLOQUEADA,"
  strinsert = strinsert & "FR28FECBLOQUEO,SG02CODPERSBLOQ,FR28INDSN,FR28INDESTOM,FRH7CODFORMFAR,"
  strinsert = strinsert & "FR28OPERACION,FR28CANTIDAD,FR28FECINICIO,FR28HORAINICIO,FR28FECFIN,"
  strinsert = strinsert & "FR28HORAFIN,FR28INDVIAOPC,FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,"
  strinsert = strinsert & "FR28TIEMMININF,FR28REFERENCIA,FR28CANTDISP,FR28UBICACION,FR28INDPERF,"
  strinsert = strinsert & "FR28INSTRADMIN,FR28VELPERFUSION,FR28VOLTOTAL,FR73CODPRODUCTO_2,"
  strinsert = strinsert & "FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28DESPRODUCTO,"
  strinsert = strinsert & "FR28PATHFORMAG,FR28VOLUMEN_2,FR28DIASADM,FR28CANTPEDORI" & ")"
  strinsert = strinsert & " "
  strinsert = strinsert & "SELECT " & txtText1(0).Text & "," & linea & ","
  strinsert = strinsert & "FR73CODPRODUCTO,FR28DOSIS,"
  strinsert = strinsert & "FR93CODUNIMEDIDA,FR28VOLUMEN,FR28TIEMINFMIN,FR34CODVIA,FRG4CODFRECUENCIA,"
  strinsert = strinsert & "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,FR28INDBLOQUEADA,"
  strinsert = strinsert & "FR28FECBLOQUEO,SG02CODPERSBLOQ,FR28INDSN,FR28INDESTOM,FRH7CODFORMFAR,"
  strinsert = strinsert & "FR28OPERACION,FR28CANTIDAD,FR28FECINICIO,FR28HORAINICIO,FR28FECFIN,"
  strinsert = strinsert & "FR28HORAFIN,FR28INDVIAOPC,FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,"
  strinsert = strinsert & "FR28TIEMMININF,FR28REFERENCIA,FR28CANTDISP,FR28UBICACION,FR28INDPERF,"
  strinsert = strinsert & "FR28INSTRADMIN,FR28VELPERFUSION,FR28VOLTOTAL,FR73CODPRODUCTO_2,"
  strinsert = strinsert & "FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28DESPRODUCTO,"
  strinsert = strinsert & "FR28PATHFORMAG,FR28VOLUMEN_2,FR28DIASADM,FR28CANTPEDORI"
  strinsert = strinsert & " FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text & " AND "
  strinsert = strinsert & " FR28NUMLINEA=" & txtText1(45).Text
  
  objApp.rdoConnect.Execute strinsert, 64
  objApp.rdoConnect.Execute "Commit", 64
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
End If
cmdcopiarlinea.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer

If txtText1(34).Text = "" Then
   MsgBox "Debe hacer nuevo registro primero con el bot�n de F�rmula Magistral.", vbInformation, "Aviso"
  Exit Sub
End If


If Index = 0 Then
Else
    On Error Resume Next
    pathFileName = Dir(txtText1(57).Text)
    finpath = InStr(1, txtText1(57).Text, pathFileName, 1)
    pathFileName = Left(txtText1(57).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(57).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If ERR.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), "")
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), strOpenFileName)
    End If
End If

End Sub

Private Sub cmdfirmarenviar_Click()
Dim strFR32Peticion As String
Dim qryFR32Peticion As rdoQuery
Dim rstFR32Peticion As rdoResultset
Dim strupdate As String
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim hora As Variant
Dim rstb As rdoResultset
Dim strb As String
Dim VolTotal As Currency
Dim strSql28 As String
Dim qrySql28 As rdoQuery
Dim rstSql28 As rdoResultset
Dim strDiasAdm As String
Dim intInd As Integer
Dim intCont As Integer
Dim intLon As Integer
Dim strfec As String
Dim intFec As Integer
Dim strFR73CODPRODUCTO As String
Dim strFR28DOSIS As String
Dim strFRG4CODFRECUENCIA As String
Dim strFR93CODUNIMEDIDA As String
Dim strFR34CODVIA As String
Dim strFRH7CODFORMFAR As String
Dim strFR73CODPRODUCTO_DIL As String
Dim strFR28OPERACION As String
Dim strFR28HORAINICIO As String
Dim strFR28INDDISPPRN As String
Dim strFR28INDCOMIENINMED As String
Dim strFR28INDPERF As String
Dim strFR28CANTIDAD As String
Dim strFR28CANTIDADDIL As String
Dim strFR28TIEMMININF As String
Dim strFR28INDVIAOPC As String
Dim strFR28REFERENCIA As String
Dim strFR28UBICACION As String
Dim strFR28VELPERFUSION As String
Dim strFR28INSTRADMIN As String
Dim strFR28VOLTOTAL As String
Dim strFR73CODPRODUCTO_2 As String
Dim strFRH7CODFORMFAR_2 As String
Dim strFR28DOSIS_2 As String
Dim strFR93CODUNIMEDIDA_2 As String
Dim strFR28VOLUMEN_2 As String
Dim strFR28DESPRODUCTO As String
Dim strFR28PATHFORMAG As String
Dim strFR28VOLUMEN As String
Dim blnPim As Boolean
  
cmdfirmarenviar.Enabled = False

blnNoVerAgen = True
If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If Campos_Obligatorios = True Then
      cmdfirmarenviar.Enabled = True
      blnNoVerAgen = False
      Exit Sub
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    If tlbToolbar1.Buttons(4).Enabled = True Then
      cmdfirmarenviar.Enabled = True
      Exit Sub
    End If
  End If
End If
'++++++++++++++++++++++++++++++++
If Campos_Obligatorios = True Then
      blnNoVerAgen = False
      cmdfirmarenviar.Enabled = True
      Exit Sub
End If
'+++++++++++++++++++++++++++++++++

tabTab1(1).Tab = 1
Me.Enabled = False

Screen.MousePointer = vbHourglass

If Peticion_Correcta = False Then
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdfirmarenviar.Enabled = True
  Exit Sub
End If

If txtText1(25).Text = 1 Then 'redactada
  If chkCheck1(7).Value = 1 Then
    strb = "SELECT SUM(FR28VOLTOTAL) FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
    Set rstb = objApp.rdoConnect.OpenResultset(strb)
    If rstb.EOF Then
      VolTotal = 0
    Else
      VolTotal = rstb(0).Value
    End If
    rstb.Close
    Set rstb = Nothing
    If MsgBox("El Paciente tiene restricciones de Volumen, " & Chr(13) & "el Volumen total de los medicamentos a administrarle es de :" & Chr(13) & VolTotal & " mililitros", vbOKCancel, "Aviso") = vbCancel Then
      cmdfirmarenviar.Enabled = True
      Exit Sub
    End If
  End If
    
  'Se generan las tuplas correspondientes a la periodicidad agenda
  strSql28 = "SELECT * " & _
             "  FROM FR2800 " & _
             " WHERE FR66CODPETICION= ? " & _
             "   AND FRH5CODPERIODICIDAD = ? "
  Set qrySql28 = objApp.rdoConnect.CreateQuery("", strSql28)
  qrySql28(0) = txtText1(0).Text
  qrySql28(1) = "Agenda"
  Set rstSql28 = qrySql28.OpenResultset()
  While Not rstSql28.EOF
    If IsNull(rstSql28.rdoColumns("FR28DIASADM").Value) Then
      strDiasAdm = ""
    Else
      strDiasAdm = rstSql28.rdoColumns("FR28DIASADM").Value
    End If
    If Len(Trim(strDiasAdm)) > 0 Then
      'Hay que realizar los insert de los d�as de administraci�n
      intLon = Len(strDiasAdm) - 1
      strfec = ""
      blnPim = True
      For intInd = 1 To intLon
        
        'Se obtiene un n�mero con la fecha
        
        For intCont = 1 To intLon
          If IsNumeric(Left(strDiasAdm, 1)) Then
            strfec = strfec & Left(strDiasAdm, 1)
            strDiasAdm = Right(strDiasAdm, Len(strDiasAdm) - 1)
          Else
            'Se ha llegado a un asterisco (separador)
            If Len(Trim(strDiasAdm)) > 0 Then
              strDiasAdm = Right(strDiasAdm, Len(strDiasAdm) - 1)
              If IsNumeric(strfec) Then
                intFec = strfec
                '''''
                If IsNull(rstSql28.rdoColumns("FR73CODPRODUCTO").Value) Then
                  strFR73CODPRODUCTO = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR73CODPRODUCTO").Value)) > 0 Then
                    strFR73CODPRODUCTO = rstSql28.rdoColumns("FR73CODPRODUCTO").Value
                  Else
                    strFR73CODPRODUCTO = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28DOSIS").Value) Then
                  strFR28DOSIS = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28DOSIS").Value)) > 0 Then
                    strFR28DOSIS = objGen.ReplaceStr(rstSql28.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
                  Else
                    strFR28DOSIS = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FRG4CODFRECUENCIA").Value) Then
                  strFRG4CODFRECUENCIA = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FRG4CODFRECUENCIA").Value)) > 0 Then
                    strFRG4CODFRECUENCIA = "'" & rstSql28.rdoColumns("FRG4CODFRECUENCIA").Value & "'"
                  Else
                    strFRG4CODFRECUENCIA = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                  strFR93CODUNIMEDIDA = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR93CODUNIMEDIDA").Value)) > 0 Then
                    strFR93CODUNIMEDIDA = "'" & rstSql28.rdoColumns("FR93CODUNIMEDIDA").Value & "'"
                  Else
                    strFR93CODUNIMEDIDA = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR34CODVIA").Value) Then
                  strFR34CODVIA = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR34CODVIA").Value)) > 0 Then
                    strFR34CODVIA = "'" & rstSql28.rdoColumns("FR34CODVIA").Value & "'"
                  Else
                    strFR34CODVIA = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FRH7CODFORMFAR").Value) Then
                  strFRH7CODFORMFAR = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FRH7CODFORMFAR").Value)) > 0 Then
                    strFRH7CODFORMFAR = "'" & rstSql28.rdoColumns("FRH7CODFORMFAR").Value & "'"
                  Else
                    strFRH7CODFORMFAR = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
                  strFR73CODPRODUCTO_DIL = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR73CODPRODUCTO_DIL").Value)) > 0 Then
                    strFR73CODPRODUCTO_DIL = rstSql28.rdoColumns("FR73CODPRODUCTO_DIL").Value
                  Else
                    strFR73CODPRODUCTO_DIL = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28OPERACION").Value) Then
                  strFR28OPERACION = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28OPERACION").Value)) > 0 Then
                    strFR28OPERACION = "'" & rstSql28.rdoColumns("FR28OPERACION").Value & "'"
                  Else
                    strFR28OPERACION = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28HORAINICIO").Value) Then
                  strFR28HORAINICIO = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28HORAINICIO").Value)) > 0 Then
                    If blnPim = True Then
                      strFR28HORAINICIO = rstSql28.rdoColumns("FR28HORAINICIO").Value
                      blnPim = False
                    Else
                      strFR28HORAINICIO = "1"
                    End If
                  Else
                    strFR28HORAINICIO = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28INDDISPPRN").Value) Then
                  strFR28INDDISPPRN = "0"
                Else
                  If (rstSql28.rdoColumns("FR28INDDISPPRN").Value = -1) Or _
                     (rstSql28.rdoColumns("FR28INDDISPPRN").Value = 1) Then
                    strFR28INDDISPPRN = "-1"
                  Else
                    strFR28INDDISPPRN = "0"
                  End If
                End If
                
                If rstSql28.rdoColumns("FR28INDCOMIENINMED").Value = 0 Then
                  strFR28INDCOMIENINMED = "0"
                Else
                  If (rstSql28.rdoColumns("FR28INDCOMIENINMED").Value = -1) Or _
                     (rstSql28.rdoColumns("FR28INDCOMIENINMED").Value = 1) Then
                    strFR28INDCOMIENINMED = "-1"
                  Else
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28INDPERF").Value) Then
                  strFR28INDPERF = "0"
                Else
                  If (rstSql28.rdoColumns("FR28INDPERF").Value = -1) Or _
                     (rstSql28.rdoColumns("FR28INDPERF").Value = 1) Then
                    strFR28INDPERF = "-1"
                  Else
                    strFR28INDPERF = "0"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28CANTIDAD").Value) Then
                  strFR28CANTIDAD = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28CANTIDAD").Value)) > 0 Then
                    strFR28CANTIDAD = objGen.ReplaceStr(rstSql28.rdoColumns("FR28CANTIDAD").Value, ",", ".", 1)
                  Else
                    strFR28CANTIDAD = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28CANTIDADDIL").Value) Then
                  strFR28CANTIDADDIL = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28CANTIDADDIL").Value)) > 0 Then
                    strFR28CANTIDADDIL = rstSql28.rdoColumns("FR28CANTIDADDIL").Value
                  Else
                    strFR28CANTIDADDIL = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28TIEMMININF").Value) Then
                  strFR28TIEMMININF = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28TIEMMININF").Value)) > 0 Then
                    strFR28TIEMMININF = rstSql28.rdoColumns("FR28TIEMMININF").Value
                  Else
                    strFR28TIEMMININF = "null"
                  End If
                End If
                
                If rstSql28.rdoColumns("FR28INDVIAOPC").Value = 0 Then
                  strFR28INDVIAOPC = "0"
                Else
                  If (rstSql28.rdoColumns("FR28INDVIAOPC").Value = -1) Or _
                     (rstSql28.rdoColumns("FR28INDVIAOPC").Value = 1) Then
                    strFR28INDVIAOPC = "-1"
                  Else
                    strFR28INDVIAOPC = "0"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28REFERENCIA").Value) Then
                  strFR28REFERENCIA = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28REFERENCIA").Value)) > 0 Then
                    strFR28REFERENCIA = "'" & rstSql28.rdoColumns("FR28REFERENCIA").Value & "'"
                  Else
                    strFR28REFERENCIA = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28UBICACION").Value) Then
                  strFR28UBICACION = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28UBICACION").Value)) > 0 Then
                    strFR28UBICACION = "'" & rstSql28.rdoColumns("FR28UBICACION").Value & "'"
                  Else
                    strFR28UBICACION = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28VELPERFUSION").Value) Then
                  strFR28VELPERFUSION = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28VELPERFUSION").Value)) > 0 Then
                    strFR28VELPERFUSION = objGen.ReplaceStr(rstSql28.rdoColumns("FR28VELPERFUSION").Value, ",", ".", 1)
                  Else
                    strFR28VELPERFUSION = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28INSTRADMIN").Value) Then 'FR28INSTRADMIN
                  strFR28INSTRADMIN = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28INSTRADMIN").Value)) > 0 Then
                    strFR28INSTRADMIN = "'" & rstSql28.rdoColumns("FR28INSTRADMIN").Value & "'"
                  Else
                    strFR28INSTRADMIN = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28VOLTOTAL").Value) Then 'FR28VOLTOTAL
                  strFR28VOLTOTAL = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28VOLTOTAL").Value)) > 0 Then
                    strFR28VOLTOTAL = objGen.ReplaceStr(rstSql28.rdoColumns("FR28VOLTOTAL").Value, ",", ".", 1)
                  Else
                    strFR28VOLTOTAL = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR73CODPRODUCTO_2").Value) Then 'FR73CODPRODUCTO_2
                  strFR73CODPRODUCTO_2 = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR73CODPRODUCTO_2").Value)) > 0 Then
                    strFR73CODPRODUCTO_2 = rstSql28.rdoColumns("FR73CODPRODUCTO_2").Value
                  Else
                    strFR73CODPRODUCTO_2 = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FRH7CODFORMFAR_2").Value) Then 'FRH7CODFORMFAR_2
                  strFRH7CODFORMFAR_2 = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FRH7CODFORMFAR_2").Value)) > 0 Then
                    strFRH7CODFORMFAR_2 = "'" & rstSql28.rdoColumns("FRH7CODFORMFAR_2").Value & "'"
                  Else
                    strFRH7CODFORMFAR_2 = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28DOSIS_2").Value) Then 'FR28DOSIS_2
                  strFR28DOSIS_2 = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28DOSIS_2").Value)) > 0 Then
                    strFR28DOSIS_2 = objGen.ReplaceStr(rstSql28.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1)
                  Else
                    strFR28DOSIS_2 = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then 'FR93CODUNIMEDIDA_2
                  strFR93CODUNIMEDIDA_2 = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR93CODUNIMEDIDA_2").Value)) > 0 Then
                    strFR93CODUNIMEDIDA_2 = "'" & rstSql28.rdoColumns("FR93CODUNIMEDIDA_2").Value & "'"
                  Else
                    strFR93CODUNIMEDIDA_2 = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28VOLUMEN_2").Value) Then 'FR28VOLUMEN_2
                  strFR28VOLUMEN_2 = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28VOLUMEN_2").Value)) > 0 Then
                    strFR28VOLUMEN_2 = rstSql28.rdoColumns("FR28VOLUMEN_2").Value
                  Else
                    strFR28VOLUMEN_2 = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28DESPRODUCTO").Value) Then 'FR28DESPRODUCTO
                  strFR28DESPRODUCTO = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28DESPRODUCTO").Value)) > 0 Then
                    strFR28DESPRODUCTO = "'" & rstSql28.rdoColumns("FR28DESPRODUCTO").Value & "'"
                  Else
                    strFR28DESPRODUCTO = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28VOLUMEN").Value) Then 'FR28VOLUMEN
                  strFR28VOLUMEN = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28VOLUMEN").Value)) > 0 Then
                    strFR28VOLUMEN = rstSql28.rdoColumns("FR28VOLUMEN").Value
                  Else
                    strFR28VOLUMEN = "null"
                  End If
                End If
                
                If IsNull(rstSql28.rdoColumns("FR28PATHFORMAG").Value) Then 'FR28PATHFORMAG
                  strFR28PATHFORMAG = "null"
                Else
                  If Len(Trim(rstSql28.rdoColumns("FR28PATHFORMAG").Value)) > 0 Then
                    strFR28PATHFORMAG = "'" & rstSql28.rdoColumns("FR28PATHFORMAG").Value & "'"
                  Else
                    strFR28PATHFORMAG = "null"
                  End If
                End If
                '''''
                Call insertar(intFec, rstSql28.rdoColumns("FR66CODPETICION").Value, rstSql28.rdoColumns("FR28NUMLINEA").Value, strFR73CODPRODUCTO, _
                            strFR28DOSIS, strFRG4CODFRECUENCIA, strFR93CODUNIMEDIDA, strFR34CODVIA, strFRH7CODFORMFAR, _
                            strFR73CODPRODUCTO_DIL, strFR28OPERACION, strFR28HORAINICIO, strFR28INDDISPPRN, strFR28INDCOMIENINMED, strFR28INDPERF, _
                            strFR28CANTIDAD, strFR28CANTIDADDIL, strFR28TIEMMININF, strFR28INDVIAOPC, strFR28REFERENCIA, strFR28UBICACION, _
                            strFR28VELPERFUSION, strFR28INSTRADMIN, strFR28VOLTOTAL, strFR73CODPRODUCTO_2, strFRH7CODFORMFAR_2, strFR28DOSIS_2, _
                            strFR93CODUNIMEDIDA_2, strFR28VOLUMEN_2, strFR28DESPRODUCTO, strFR28PATHFORMAG, strFR28VOLUMEN)
                strfec = ""
              End If
            End If
          End If
        Next intCont
      Next intInd
    Else
      MsgBox "Existe una petici�n de medicamento con periodicidad agenda en la que no se han seleccionado los d�as de administraci�n", vbInformation, "No es Posible Firmar"
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      cmdfirmarenviar.Enabled = True
      Exit Sub
    End If
    rstSql28.MoveNext
  Wend
  qrySql28.Close
  Set qrySql28 = Nothing
  Set rstSql28 = Nothing
    
    '*************************************************************************
  'se cambia el estado y se modifica qu�n env�a la orden
  stra = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  hora = rsta.rdoColumns(0).Value
  hora = hora & "."
  stra = ""
  stra = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  hora = hora & rsta.rdoColumns(0).Value
  strupdate = "UPDATE FR6600 SET FR66FECFIRMMEDI=SYSDATE," & _
              "FR66HORAFIRMMEDI=" & hora & "," & _
              "SG02COD_FEN=" & "'" & objsecurity.strUser & "'" & _
              ",FR26CODESTPETIC=3,FR66FECENVIO=SYSDATE" & _
              " WHERE FR66CODPETICION=" & txtText1(0).Text
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
    
  'Se genera un PRN para las l�neas STAT y la etiqueta correspondiente
  strFR32Peticion = "SELECT * FROM FR3200 WHERE FR66CODPETICION=? AND FR32INDCOMIENINMED=-1"
  Set qryFR32Peticion = objApp.rdoConnect.CreateQuery("", strFR32Peticion)
  qryFR32Peticion(0) = txtText1(0).Text
  Set rstFR32Peticion = qryFR32Peticion.OpenResultset()
  If Not rstFR32Peticion.EOF Or (chkOMPRN(6).Value = 1) Then
    Call RealizarApuntes
    Call Generar_PRN
  End If
  qryFR32Peticion.Close
  Set rstFR32Peticion = Nothing
    
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.objWinActiveForm.blnChanged = False
  objWinInfo.DataRefresh
  mensaje = MsgBox("La Orden M�dica ha sido Firmada y Enviada", vbInformation, "Farmacia")

    '************************************************************************+
  Else
    mensaje = MsgBox("La Orden M�dica ya  ha sido Firmada y Enviada", vbInformation, "Farmacia")
  End If

  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdfirmarenviar.Enabled = True
End Sub

Private Sub cmdHCpaciente_Click()
  blnNoVerAgen = True
  blnNoVerAgen = False
End Sub

Private Sub cmdInformacion_Click()

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

cmdInformacion.Enabled = False
  
  Select Case intBotonMasInf
  Case 41 'solucion para diluir
    If txtText1(40).Text <> "" Then
      glngMasInfProd = txtText1(40).Text
      Call objsecurity.LaunchProcess("FR0200")
    Else
      Call MsgBox("No hay soluci�n para diluir.", vbInformation, "Aviso")
      cmdInformacion.Enabled = True
      blnNoVerAgen = False
      Exit Sub
    End If
  Case 51 'producto2
    If txtText1(52).Text <> "" Then
      glngMasInfProd = txtText1(52).Text
      Call objsecurity.LaunchProcess("FR0200")
    Else
      Call MsgBox("No hay Medicamento 2.", vbInformation, "Aviso")
      cmdInformacion.Enabled = True
      blnNoVerAgen = False
      Exit Sub
    End If
  Case Else
    If txtText1(27).Text <> "" Then
      glngMasInfProd = txtText1(27).Text
      Call objsecurity.LaunchProcess("FR0200")
    Else
      Call MsgBox("No hay Medicamento.", vbInformation, "Aviso")
      cmdInformacion.Enabled = True
      blnNoVerAgen = False
      Exit Sub
    End If
  End Select

glngMasInfProd = 0
cmdInformacion.Enabled = True
blnNoVerAgen = False
End Sub

Private Sub cmdInstAdm_Click()
Dim mensaje As String
Dim strupdate As String

blnNoVerAgen = True
If mstrOperacion = "P" Then
  cmdInstAdm.Enabled = False
  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      If txtText1(34).Text = "" Then
        mensaje = MsgBox("Debe hacer nuevo registro primero.", vbInformation, "Aviso")
        cmdInstAdm.Enabled = True
        blnNoVerAgen = False
        Exit Sub
      End If
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = "frmRedactarOMPRN"
      gstrInstAdmin = ""
      Call objsecurity.LaunchProcess("FR0180")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gstrInstAdmin <> "" Then
        txtText1(39).Text = gstrInstAdmin
      End If
      gstrInstAdmin = ""
      gstrLlamadorProd = ""
    End If
  End If
  cmdInstAdm.Enabled = True
Else
  'If tlbToolbar1.Buttons(4).Enabled = True Then
  '  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  'End If
  'If tlbToolbar1.Buttons(4).Enabled = True Then
  '  Exit Sub
  'End If
  If Campos_Obligatorios = True Then
    blnNoVerAgen = False
    Exit Sub
  ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  End If
  
  cmdInstAdm.Enabled = False
  
    If txtText1(0).Text <> "" Then
      If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
        mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                  "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
      Else
        If txtText1(34).Text = "" Then
          mensaje = MsgBox("Debe hacer nuevo registro primero.", vbInformation, "Aviso")
          cmdInstAdm.Enabled = True
          blnNoVerAgen = False
          Exit Sub
        End If
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        gstrLlamadorProd = "frmRedactarOMPRN"
        gstrInstAdmin = ""
        Call objsecurity.LaunchProcess("FR0180")
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        If gstrInstAdmin <> "" Then
          strupdate = "UPDATE FR2800 SET "
          strupdate = strupdate & "FR28INSTRADMIN='" & gstrInstAdmin & "'"
          strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
          strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
          
          objApp.rdoConnect.Execute strupdate, 64
          
          Call objWinInfo.DataRefresh
        End If
        gstrInstAdmin = ""
        gstrLlamadorProd = ""
      End If
    End If
  
  cmdInstAdm.Enabled = True
End If
blnNoVerAgen = False
End Sub

Private Sub cmdMed2_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.objWinActiveForm.blnChanged = False
End If

cmdMed2.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      gstrLlamadorProd = "frmRedactarOMPRNmed2"
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = ""
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
            strupdate = "UPDATE FR2800 SET "
            strupdate = strupdate & "FR73CODPRODUCTO_2=" & gintprodbuscado(v, 0)
            If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strupdate = strupdate & ",FRH7CODFORMFAR_2='" & rstProducto("FRH7CODFORMFAR").Value & "'"
            End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              strupdate = strupdate & ",FR28VOLUMEN_2='" & rstProducto("FR73VOLUMEN").Value & "'"
            End If
            'If Not IsNull(rstProducto("FR73DOSIS").Value) Then
            '  strupdate = strupdate & ",FR28DOSIS_2=" & rstProducto("FR73DOSIS").Value
            'End If
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strupdate = strupdate & ",FR28DOSIS_2=1"
            Else
              If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                If txtText1(32).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                  cmdMed2.Enabled = True
                  blnBoton = False
                  gintprodtotal = 0
                  blnNoVerAgen = False
                  Exit Sub
                Else
                  strupdate = strupdate & ",FR28DOSIS_2=" & rstProducto("FR73DOSIS").Value * txtText1(32).Text
                End If
              Else
                If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                  If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                    cmdMed2.Enabled = True
                    blnBoton = False
                    gintprodtotal = 0
                    blnNoVerAgen = False
                    Exit Sub
                  Else
                    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                    intPeso = txtText1(32).Text
                    intAltura = txtText1(33).Text
                    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                    vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                    strupdate = strupdate & ",FR28DOSIS_2=" & vntMasa
                    rstPotPeso.Close
                    rstPotAlt.Close
                    rstFactor.Close
                    Set rstPotPeso = Nothing
                    Set rstPotAlt = Nothing
                    Set rstFactor = Nothing
                  End If
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                  strupdate = strupdate & ",FR28DOSIS_2=" & vntAux
                End If
              End If
            End If

            If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strupdate = strupdate & ",FR93CODUNIMEDIDA_2='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
            End If
            VolTotal = 0
            If txtText1(38).Text = "" Then
              Vol1 = 0
            Else
              Vol1 = CCur(txtText1(38).Text)
            End If
            If txtText1(42).Text = "" Then
              Vol2 = 0
            Else
              Vol2 = CCur(txtText1(42).Text)
            End If
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol3 = 0
            Else
              Vol3 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
            If txtText1(43).Text = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(txtText1(43).Text) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
            strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
            Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.DataRefresh

          End If
        Next v
      End If
      gintprodtotal = 0
    End If
  End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
cmdMed2.Enabled = True
blnNoVerAgen = False
End Sub

Private Sub cmdMedNoForm_Click()
Dim strfec As String
Dim rstfec As rdoResultset
Dim mensaje As String

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
noformulario = True

blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  noformulario = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If


blnBoton = True
cmdMedNoForm.Enabled = False



  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else

      tab2.Tab = 0
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
      Call objWinInfo.CtrlSet(txtText1(27), 999999999)
          
      strfec = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'),TO_CHAR(SYSDATE,'HH24') FROM DUAL"
      Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
      Call objWinInfo.CtrlSet(dtcDateCombo1(3), rstfec.rdoColumns(0).Value)
      Call objWinInfo.CtrlSet(txtText1(37), rstfec.rdoColumns(1).Value)
      Call objWinInfo.CtrlSet(cboDBCombo1(3), "Diario")
      rstfec.Close
      Set rstfec = Nothing
      
      txtText1(55).SetFocus
    End If
  End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
cmdMedNoForm.Enabled = True
blnBoton = False
blnNoVerAgen = False
noformulario = False
End Sub

Private Sub cmdSolDil_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim strinsert As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  'objWinInfo.DataSave
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.objWinActiveForm.blnChanged = False
End If

cmdSolDil.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
        gstrLlamadorProd = "frmRedactarOMPRNsolFlui"
      Else
        gstrLlamadorProd = "frmRedactarOMPRNsoldil"
        If (UCase(txtText1(35).Text) = "VI") Or (UCase(txtText1(35).Text) = "AMP") Or (UCase(txtText1(35).Text) = "SOL") Then
          'Se puede diluir
        Else
          'No se puede diluir
          MsgBox "Solo se pueden diluir las siguientes FF: VI, SOL y AMP", vbInformation, "Seleccionar Diluyente"
          cmdSolDil.Enabled = True
          blnNoVerAgen = False
          Exit Sub
        End If
      End If
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
'      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = ""
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
              
            If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
              strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                txtText1(0).Text
              Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
              If IsNull(rstlinea.rdoColumns(0).Value) Then
                linea = 1
              Else
                linea = rstlinea.rdoColumns(0).Value + 1
              End If
            
              '??????????????????????????-
              strinsert = "INSERT INTO FR2800"
              strinsert = strinsert & " (FR66CODPETICION,FR28NUMLINEA,"
              strinsert = strinsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
              strinsert = strinsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
              strinsert = strinsert & "FR28OPERACION,FR28FECINICIO,"
              strinsert = strinsert & "FR28HORAINICIO,"
              strinsert = strinsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
              strinsert = strinsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
              'strinsert = strinsert & "FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2"
              strinsert = strinsert & "FRH5CODPERIODICIDAD"
              strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
              strinsert = strinsert & "null," 'la U.M. es del medicamento
              
              If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
                VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
              Else
                VolTotal = 0
                strinsert = strinsert & "NULL," & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              strinsert = strinsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
              
              If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                If txtText1(43).Text = "" Then
                  TiemMinInf = 0
                Else
                  TiemMinInf = CCur(txtText1(43).Text) / 60
                End If
                If TiemMinInf <> 0 Then
                  VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                  vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                Else
                  VolPerf = 0
                  strinsert = strinsert & VolPerf & ","
                End If
              End If
              strinsert = strinsert & "'Diario')"
              'If Not IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
              'Else
              '  strinsert = strinsert & "NULL,"
              'End If
              'If Not IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
              'Else
              '  strinsert = strinsert & "NULL)"
              'End If
              
              objApp.rdoConnect.Execute strinsert, 64
              
              rstProducto.Close
              Set rstProducto = Nothing
              Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
              
            Else
              strupdate = "UPDATE FR2800 SET "
              strupdate = strupdate & "FR73CODPRODUCTO_DIL=" & gintprodbuscado(v, 0)
              'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
              If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strupdate = strupdate & ",FR28CANTIDADDIL=" & vntAux
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strupdate = strupdate & ",FR28TIEMMININF=" & rstProducto("FR73TIEMINF").Value
              End If
              VolTotal = 0
              If txtText1(38).Text = "" Then
                Vol1 = 0
              Else
                Vol1 = CCur(txtText1(38).Text)
              End If
              If IsNull(rstProducto("FR73VOLUMEN").Value) Then
                Vol2 = 0
              Else
                Vol2 = CCur(rstProducto("FR73VOLUMEN").Value)
              End If
              If txtText1(47).Text = "" Then
                Vol3 = 0
              Else
                Vol3 = CCur(txtText1(47).Text)
              End If
              VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
              vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
              strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
              If txtText1(43).Text = "" Then
                TiemMinInf = 0
              Else
                TiemMinInf = CCur(txtText1(43).Text) / 60
              End If
              If TiemMinInf <> 0 Then
                VolPerf = Format(VolTotal / TiemMinInf, "0.00")
              Else
                VolPerf = 0
              End If
              vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
              strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
              
              strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
              strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
              
              objApp.rdoConnect.Execute strupdate, 64
              
              rstProducto.Close
              Set rstProducto = Nothing
              Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
              objWinInfo.objWinActiveForm.blnChanged = False
              Call objWinInfo.DataRefresh
            End If
          End If
        Next v
      End If
      gintprodtotal = 0
    End If
  End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
cmdSolDil.Enabled = True
blnNoVerAgen = False
End Sub


Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strpathword As String
  
On Error GoTo error_shell
  
'Par�metros generales
'5,Ubicaci�n de Word,C:\Archivos de programa\Microsoft Office\Office\winword.exe,TEXTO
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=5"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strpathword = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el Path del Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 5 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\Microsoft Office\Office\winword.exe", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  If txtText1(57).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Exit Sub
  End If
  
  Shell strpathword & " " & txtText1(57).Text, vbMaximizedFocus

error_shell:

  If ERR.Number <> 0 Then
    MsgBox "Error N�mero: " & ERR.Number & Chr(13) & ERR.Description, vbCritical
  End If

End Sub

Private Sub lblLabel1_DblClick(Index As Integer)
    
    Select Case Index
    Case 44
        If tab2.Tab = 0 Then
          blnDetalle = True
          tab2.Tab = 1
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 0
    Case 53
        If tab2.Tab = 1 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 1
    Case 54
        If tab2.Tab = 2 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 2
    Case 55
        If tab2.Tab = 3 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 3
    'Case 56
    '    If tab2.Tab = 4 Then
    '      blnDetalle = True
    '      tab2.Tab = 0
    '    End If
    '    tabTab1(1).Tab = 0
    '    tab2.Tab = 4
    'Case 57
    '    If tab2.Tab = 5 Then
    '      blnDetalle = True
    '      tab2.Tab = 0
    '    End If
    '    tabTab1(1).Tab = 0
    '    tab2.Tab = 5
    '    txtText1(57).SetFocus
    End Select
    
    Call presentacion_pantalla

End Sub


Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "OM" And strCtrlName = "txtText1(60)" Then
    aValues(2) = txtText1(59).Text  'proceso
End If
End Sub

Private Sub tab2_Click(PreviousTab As Integer)
Dim auxtab As Integer
Dim cboproductolleno As Boolean
Dim cboproductodillleno As Boolean
Dim cboproductomezlleno As Boolean



If blnObligatorio = False Then
  If objWinInfo.objWinActiveForm.strName <> "Detalle OM" Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  End If
  If tab2.Tab <> PreviousTab And blnDetalle Then
    'If tlbToolbar1.Buttons(4).Enabled = True Then
      'Guardar
      If Campos_Obligatorios = True Then
        'tab2.SetFocus
        'Call objWinInfo.CtrlGotFocus
        blnObligatorio = True
        tab2.Tab = PreviousTab
        Exit Sub
      'Else
      ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
        auxtab = tab2.Tab
        Call objWinInfo.DataSave
        blnObligatorio = False
        tab2.Tab = auxtab
      End If
    'Else
      blnObligatorio = False
      'Vaciar pantalla
      objWinInfo.intWinStatus = cwModeSingleEmpty
      Call objWinInfo.WinStabilize
    'End If
  End If
  Call presentacion_pantalla
  'If tabTab1(1).Tab = 0 And tab2.Tab = 4 And blnDetalle Then
   ' txtText1(57).SetFocus
  'ElseIf tabTab1(1).Tab = 0 And blnDetalle Then
   If tabTab1(1).Tab = 0 And blnDetalle Then
    If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
      txtText1(35).SetFocus
    End If
   End If
Else
  blnObligatorio = False
End If

End Sub

Private Sub tab2_DblClick()
  
    Call presentacion_pantalla

End Sub






Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If

End Sub


Private Sub cmdAceptarPer_Click()
  Dim intDiaSemana As Integer
  Dim intsuma As Integer
  Dim i As Integer
  Dim strDiasAdm As String
  Dim strUpd28 As String
  Dim qryUpd28 As rdoQuery
  
  tlbToolbar1.Enabled = True
  Frame2.Visible = False
  cmdsitcarro.Enabled = True
  cmdfirmar.Enabled = True
  cmdenviar.Enabled = True
  cmdbuscargruprot.Enabled = True
  cmdbuscarprot.Enabled = True
  cmdbuscargruprod.Enabled = True
  cmdbuscarprod.Enabled = True
  cmdperfil.Enabled = True
  cmdHCpaciente.Enabled = True
  cmdAlergia.Enabled = True
  fraFrame1(0).Enabled = True
  fraFrame1(1).Enabled = True
  strDiasAdm = ""
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  For i = 1 To 30
    If Check1(i).Value = 1 Then
      'Call insertar(i - 1)
      strDiasAdm = strDiasAdm & (i - 1) & "*"
      Check1(i).Value = 0
    End If
  Next i
  
  If Len(strDiasAdm) > 0 Then
    strUpd28 = "UPDATE FR2800 " & _
               "   SET FR28DIASADM = ? " & _
               " WHERE FR66CODPETICION = ? " & _
               "   AND FR28NUMLINEA = ? "
    Set qryUpd28 = objApp.rdoConnect.CreateQuery("", strUpd28)
    qryUpd28(0) = strDiasAdm
    qryUpd28(1) = txtText1(0).Text
    qryUpd28(2) = txtText1(45).Text
    qryUpd28.Execute
  End If
  
  'objWinInfo.DataRefresh
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
End Sub


Private Sub cmdAlergia_Click()
  
  cmdAlergia.Enabled = False
  If chkCheck1(2).Value = 1 Then
    blnNoVerAgen = True
    gPaciente = txtText1(2).Text
    Call objsecurity.LaunchProcess("FR0155")
    gPaciente = ""
    blnNoVerAgen = False
  Else
    Call MsgBox("El Paciente no es al�rgico", vbExclamation, "Farmacia")
  End If
  cmdAlergia.Enabled = True

End Sub

Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim blnIffiv As Boolean
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

  blnBoton = True
  cmdbuscargruprod.Enabled = False
  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      gstrLlamadorProd = "frmRedactarOMPRNGM"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0167")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = ""
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
            txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If IsNull(rstlinea.rdoColumns(0).Value) Then
            linea = 1
          Else
            linea = rstlinea.rdoColumns(0).Value + 1
          End If
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            If IsNull(rstProducto("FR73INDINFIV").Value) Then
              blnIffiv = False
            Else
              If rstProducto("FR73INDINFIV").Value = -1 Then
                blnIffiv = True
              Else
                blnIffiv = False
              End If
            End If
            If blnIffiv = False Then
              strinsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                          "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR28VOLUMEN,FR28VOLTOTAL,FR34CODVIA," & _
                          "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                          "FR28HORAINICIO,FR28INDVIAOPC,FR28CANTIDADDIL,FR28CANTIDAD,FR28CANTDISP,FR28TIEMMININF"
              strinsert = strinsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
              strinsert = strinsert & ",FRH5CODPERIODICIDAD"
              ',FR93CODUNIMEDIDA_PERF1 , FR93CODUNIMEDIDA_PERF2
              strinsert = strinsert & ") VALUES " & _
                          "(" & _
                          txtText1(0).Text & "," & _
                          linea & "," & _
                          gintprodbuscado(v, 0) & ","
              If IsNull(rstProducto("FR73DOSIS").Value) Then
                strinsert = strinsert & "1,"
              Else
                If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                  If txtText1(32).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                    cmdbuscargruprod.Enabled = True
                    blnBoton = False
                    gintprodtotal = 0
                    blnNoVerAgen = False
                    Exit Sub
                  Else
                    strinsert = strinsert & rstProducto("FR73DOSIS").Value * txtText1(32).Text & ","
                  End If
                Else
                  If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                    If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                      Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                      cmdbuscargruprod.Enabled = True
                      blnBoton = False
                      gintprodtotal = 0
                      blnNoVerAgen = False
                      Exit Sub
                    Else
                      strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                      Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                      strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                      Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                      strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                      Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                      intPeso = txtText1(32).Text
                      intAltura = txtText1(33).Text
                      vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                      vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                      vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                      vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                      strinsert = strinsert & vntMasa & ","
                      rstPotPeso.Close
                      rstPotAlt.Close
                      rstFactor.Close
                      Set rstPotPeso = Nothing
                      Set rstPotAlt = Nothing
                      Set rstFactor = Nothing
                    End If
                  Else
                    vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                    strinsert = strinsert & vntAux & ","
                  End If
                End If
              End If
              If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              End If
              If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
              End If
              If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL
                strinsert = strinsert & "null,null,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
              End If
              If IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              End If
              If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                  strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
              End If
              'If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Or mstrOperacion <> "=" Then
              'If IsNull(rstProducto("FR73CODPRODUCTO_REC").Value) Then
              If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
              End If
              If IsNull(rstProducto("FR73INDESTUPEFACI").Value) Then
                strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
              Else
                If rstProducto("FR73INDESTUPEFACI").Value = -1 Then
                  strinsert = strinsert & "'" & "E" & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                Else
                  strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                End If
              End If
              If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              End If
              'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
              If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
              End If
              strinsert = strinsert & "1,1," 'FR28CANTIDAD,FR28CANTDISP
              'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
              If IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              End If
              If IsNull(rstProducto("FR73REFERENCIA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
              End If
              If IsNull(rstProducto("FRH9UBICACION").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
              End If
              If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                strinsert = strinsert & "null,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              End If
              strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
              'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
              '  strinsert = strinsert & "null,"
              'Else
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
              'End If
              'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
              '  strinsert = strinsert & "null)"
              'Else
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
              'End If
            Else 'IV
              strinsert = "INSERT INTO FR2800"
              strinsert = strinsert & " (FR66CODPETICION,FR28NUMLINEA,"
              strinsert = strinsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
              strinsert = strinsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
              strinsert = strinsert & "FR28OPERACION,FR28FECINICIO,"
              strinsert = strinsert & "FR28HORAINICIO,"
              strinsert = strinsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
              strinsert = strinsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
              'strinsert = strinsert & "FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2"
              strinsert = strinsert & "FRH5CODPERIODICIDAD"
              strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
              strinsert = strinsert & "null," 'la U.M. es del medicamento
              'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
              '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
              '  strinsert = strinsert & vntAux & "," & vntAux & ","
              '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
              'Else
              '  VolTotal = 0
              '  strinsert = strinsert & "NULL," & "NULL,"
              'End If
              If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
                VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
              Else
                VolTotal = 0
                strinsert = strinsert & "NULL," & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              strinsert = strinsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
              
              If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                If txtText1(43).Text = "" Then
                  TiemMinInf = 0
                Else
                  TiemMinInf = CCur(txtText1(43).Text) / 60
                End If
                If TiemMinInf <> 0 Then
                  VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                  vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                Else
                  VolPerf = 0
                  strinsert = strinsert & VolPerf & ","
                End If
              End If
              strinsert = strinsert & "'Diario')"
            End If
            
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            
            rstlinea.Close
            Set rstlinea = Nothing
            
            rstProducto.Close
            Set rstProducto = Nothing
            'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
          End If
        Next v
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      End If
      gintprodtotal = 0
    End If
  End If
'tabTab1(1).Tab = 1
'objWinInfo.DataMoveLast
'tabTab1(1).Tab = 0
If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
  txtText1(35).SetFocus
End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
cmdbuscargruprod.Enabled = True
blnBoton = False
blnNoVerAgen = False
End Sub

Private Sub cmdbuscargruprot_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim blnIffiv As Boolean
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

blnNoVerAgen = True

blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.objWinActiveForm.blnChanged = False
End If

blnBoton = True
cmdbuscargruprot.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      gstrLlamadorProd = "frmRedactarOMPRNGT"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0153")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = ""
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If IsNull(rstlinea.rdoColumns(0).Value) Then
            linea = 1
          Else
            linea = rstlinea.rdoColumns(0).Value + 1
          End If
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                          gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            If IsNull(rstProducto("FR73INDINFIV").Value) Then
              blnIffiv = False
            Else
              If rstProducto("FR73INDINFIV").Value = -1 Then
                blnIffiv = True
              Else
                blnIffiv = False
              End If
            End If
            If blnIffiv = False Then
              strinsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                          "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR28VOLUMEN,FR28VOLTOTAL,FR34CODVIA," & _
                          "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                          "FR28HORAINICIO,FR28INDVIAOPC,FR28CANTIDADDIL,FR28CANTIDAD,FR28CANTDISP,FR28TIEMMININF"
              strinsert = strinsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
              ',FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
              strinsert = strinsert & ",FRH5CODPERIODICIDAD"
              strinsert = strinsert & ") VALUES " & _
                          "(" & _
                          txtText1(0).Text & "," & _
                          linea & "," & _
                          gintprodbuscado(v, 0) & ","
              If IsNull(rstProducto("FR73DOSIS").Value) Then
                strinsert = strinsert & "1,"
              Else
                If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                  If txtText1(32).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                    cmdbuscargruprot.Enabled = True
                    blnBoton = False
                    gintprodtotal = 0
                    blnNoVerAgen = False
                    Exit Sub
                  Else
                    strinsert = strinsert & rstProducto("FR73DOSIS").Value * txtText1(32).Text & ","
                  End If
                Else
                  If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                    If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                      Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                      cmdbuscargruprot.Enabled = True
                      blnBoton = False
                      gintprodtotal = 0
                      blnNoVerAgen = False
                      Exit Sub
                    Else
                      strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                      Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                      strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                      Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                      strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                      Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                      intPeso = txtText1(32).Text
                      intAltura = txtText1(33).Text
                      vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                      vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                      vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                      vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                      strinsert = strinsert & vntMasa & ","
                      rstPotPeso.Close
                      rstPotAlt.Close
                      rstFactor.Close
                      Set rstPotPeso = Nothing
                      Set rstPotAlt = Nothing
                      Set rstFactor = Nothing
                    End If
                  Else
                    vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                    strinsert = strinsert & vntAux & ","
                  End If
                End If
              End If
              If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              End If
              If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
              End If
              If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL
                strinsert = strinsert & "null,null,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
              End If
              If IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              End If
              If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
              End If
              'If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Or mstrOperacion <> "=" Then
              If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
                strinsert = strinsert & "null," '/',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
              Else
                strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & "," '/',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
              End If
              If IsNull(rstProducto("FR73INDESTUPEFACI").Value) Then
                strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
              Else
                If rstProducto("FR73INDESTUPEFACI").Value = -1 Then
                  strinsert = strinsert & "'" & "E" & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                Else
                  strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                End If
              End If
              If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              End If
              'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
              If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
              End If
              strinsert = strinsert & "1,1," 'FR28CANTIDAD,FR28CANTDISP
              'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
              If IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              End If
              If IsNull(rstProducto("FR73REFERENCIA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
              End If
              If IsNull(rstProducto("FRH9UBICACION").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
              End If
              If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                strinsert = strinsert & "null,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              End If
              strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
              'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
              '  strinsert = strinsert & "null,"
              'Else
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
              'End If
              'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
              '  strinsert = strinsert & "null)"
              'Else
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
              'End If
            Else 'IV
              strinsert = "INSERT INTO FR2800"
              strinsert = strinsert & " (FR66CODPETICION,FR28NUMLINEA,"
              strinsert = strinsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
              strinsert = strinsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
              strinsert = strinsert & "FR28OPERACION,FR28FECINICIO,"
              strinsert = strinsert & "FR28HORAINICIO,"
              strinsert = strinsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
              strinsert = strinsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
              'strinsert = strinsert & "FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2"
              strinsert = strinsert & "FRH5CODPERIODICIDAD"
              strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
              strinsert = strinsert & "null," 'la U.M. es del medicamento
              'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
              '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
              '  strinsert = strinsert & vntAux & "," & vntAux & ","
              '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
              'Else
              '  VolTotal = 0
              '  strinsert = strinsert & "NULL," & "NULL,"
              'End If
              If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
                VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
              Else
                VolTotal = 0
                strinsert = strinsert & "NULL," & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              strinsert = strinsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
              
              If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                If txtText1(43).Text = "" Then
                  TiemMinInf = 0
                Else
                  TiemMinInf = CCur(txtText1(43).Text) / 60
                End If
                If TiemMinInf <> 0 Then
                  VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                  vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                Else
                  VolPerf = 0
                  strinsert = strinsert & VolPerf & ","
                End If
              End If
              strinsert = strinsert & "'Diario')"
            End If
            
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
                    
            rstlinea.Close
            Set rstlinea = Nothing
            
            rstProducto.Close
            Set rstProducto = Nothing
            'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
          End If
        Next v
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        objWinInfo.objWinActiveForm.blnChanged = False
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      End If
      gintprodtotal = 0
    End If
  End If

'tabTab1(1).Tab = 1
'objWinInfo.DataMoveLast
'tabTab1(1).Tab = 0
If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
  txtText1(35).SetFocus
End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
cmdbuscargruprot.Enabled = True
blnBoton = False
blnNoVerAgen = False
End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
'Dim strIgAnt As String
'Dim rstIgAnt As rdoResultset
'Dim blnMas As Boolean
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strfec As String
Dim rstfec As rdoResultset
Dim blnIffiv As Boolean

blnNoVerAgen = True


    If Campos_Obligatorios = True Then
      blnNoVerAgen = False
      Exit Sub
    ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
blnBoton = True
cmdbuscarprod.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case mstrOperacion
  Case "/" 'Medicamento
      'gstrLlamadorProd = "Producto"
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "M" 'Mezcla IV 2M
      'gstrLlamadorProd = "Producto"
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "P" 'PRN en OM
      'gstrLlamadorProd = "Producto"
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      gblnEsPRN = True
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
      gblnEsPRN = False
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "F" 'Fluidoterapia
    '??????????????????????????
      gstrLlamadorProd = "frmRedactarOMPRNFlui"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "E" 'Estupefaciente
      gstrLlamadorProd = "frmRedactarOMPRNEstu"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0165")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "L" 'Form.Magistral
      'gstrLlamadorProd = "frmRedactarOMPRNFoMa"
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      'glngcodpeticion = txtText1(0).Text
      'Call objsecurity.LaunchProcess("FR0165")
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      blnBoton = True
      cmdbuscarprod.Enabled = False
      FormMagistral = True
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
      FormMagistral = False
      'Call objWinInfo.CtrlSet(txtText1(31), "NE")
      'Call objWinInfo.CtrlSet(txtText1(28), 1)
      
      strfec = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'),TO_CHAR(SYSDATE,'HH24') FROM DUAL"
      Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
      Call objWinInfo.CtrlSet(dtcDateCombo1(3), rstfec.rdoColumns(0).Value)
      Call objWinInfo.CtrlSet(txtText1(37), rstfec.rdoColumns(1).Value)
      rstfec.Close
      Set rstfec = Nothing
      
      txtText1(57).SetFocus
      cmdbuscarprod.Enabled = True
      blnBoton = False
      blnNoVerAgen = False
      Exit Sub
  End Select

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If gintprodtotal > 0 Then
        If gstrLlamadorProd <> "frmRedactarOMPRNFlui" Then
          For v = 0 To gintprodtotal - 1
            strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
            txtText1(0).Text
            Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
            If IsNull(rstlinea.rdoColumns(0).Value) Then
              linea = 1
            Else
              linea = rstlinea.rdoColumns(0).Value + 1
            End If
            If gintprodbuscado(v, 0) <> "" Then
              strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
              gintprodbuscado(v, 0)
              Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
              If IsNull(rstProducto("FR73INDINFIV").Value) Then
                blnIffiv = False
              Else
                If rstProducto("FR73INDINFIV").Value = -1 Then
                  blnIffiv = True
                Else
                  blnIffiv = False
                End If
              End If
              If blnIffiv = False Then
                strinsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                            "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR28VOLUMEN,FR28VOLTOTAL,FR34CODVIA," & _
                            "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                            "FR28HORAINICIO,FR28INDVIAOPC,FR28CANTIDADDIL,FR28CANTIDAD,FR28CANTDISP,FR28TIEMMININF"
                strinsert = strinsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
                ',FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
                strinsert = strinsert & ",FRH5CODPERIODICIDAD"
                strinsert = strinsert & ") VALUES " & _
                            "(" & _
                            txtText1(0).Text & "," & _
                            linea & "," & _
                            gintprodbuscado(v, 0) & ","
                If IsNull(rstProducto("FR73DOSIS").Value) Then
                  strinsert = strinsert & "1,"
                Else
                  If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                    If txtText1(32).Text = "" Then
                      Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                      cmdbuscarprod.Enabled = True
                      blnBoton = False
                      gintprodtotal = 0
                      gstrLlamadorProd = ""
                      blnNoVerAgen = False
                      Exit Sub
                    Else
                      strinsert = strinsert & rstProducto("FR73DOSIS").Value * txtText1(32).Text & ","
                    End If
                  Else
                    If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                      If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                        Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                        cmdbuscarprod.Enabled = True
                        blnBoton = False
                        gintprodtotal = 0
                        gstrLlamadorProd = ""
                        blnNoVerAgen = False
                        Exit Sub
                      Else
                        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                        intPeso = txtText1(32).Text
                        intAltura = txtText1(33).Text
                        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                        vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                        strinsert = strinsert & vntMasa & ","
                        rstPotPeso.Close
                        rstPotAlt.Close
                        rstFactor.Close
                        Set rstPotPeso = Nothing
                        Set rstPotAlt = Nothing
                        Set rstFactor = Nothing
                      End If
                    Else
                      vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                      strinsert = strinsert & vntAux & ","
                    End If
                  End If
                End If
                'If blnMas Then
                '  If IsNull(rstIgAnt("FRG4CODFRECUENCIA").Value) Then
                '    strInsert = strInsert & "null,"
                '  Else
                '    strInsert = strInsert & "'" & rstIgAnt("FRG4CODFRECUENCIA").Value & "',"
                '  End If
                'Else
                  If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                    strinsert = strinsert & "null,"
                  Else
                    strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
                  End If
                'End If
                If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
                End If
                If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL
                  strinsert = strinsert & "null,null,"
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & "," & vntAux & ","
                End If
                'If blnMas Then
                '  If IsNull(rstIgAnt("FR34CODVIA").Value) Then
                '    strInsert = strInsert & "null,"
                '  Else
                '    strInsert = strInsert & "'" & rstIgAnt("FR34CODVIA").Value & "',"
                '  End If
                'Else
                  If IsNull(rstProducto("FR34CODVIA").Value) Then
                    strinsert = strinsert & "null,"
                  Else
                    strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
                  End If
                'End If
                If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
                End If
                'If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
                End If
                'If blnMas Then
                '  If IsNull(rstIgAnt("FR28FECINICIO").Value) Then
                '    strInsert = strInsert & "'" & mstrOperacion & "',null,"
                '  Else
                '    strInsert = strInsert & "'" & mstrOperacion & "'," & "to_date('" & rstIgAnt("FR28FECINICIO").Value & "','dd/mm/yyyy')," ',"
                '  End If
                '  If IsNull(rstIgAnt("FR28HORAINICIO").Value) Then
                '    strInsert = strInsert & "null,"
                '  Else
                '    strInsert = strInsert & "'" & rstIgAnt("FR28HORAINICIO").Value & "',"
                '  End If
                'Else
                '  strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                'End If
                If IsNull(rstProducto("FR73INDESTUPEFACI").Value) Then
                  strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                Else
                  If rstProducto("FR73INDESTUPEFACI").Value = -1 Then
                    strinsert = strinsert & "'" & "E" & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                  Else
                    strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                  End If
                End If
                If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
                End If
                'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
                End If
                strinsert = strinsert & "1,1," 'FR28CANTIDAD,FR28CANTDISP
                'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73TIEMINF").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
                End If
                If IsNull(rstProducto("FR73REFERENCIA").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
                End If
                If IsNull(rstProducto("FRH9UBICACION").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
                End If
                If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                End If
                strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
                'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
                '  strinsert = strinsert & "null,"
                'Else
                '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
                'End If
                'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
                '  strinsert = strinsert & "null)"
                'Else
                '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
                'End If
              Else 'IV
                strinsert = "INSERT INTO FR2800"
                strinsert = strinsert & " (FR66CODPETICION,FR28NUMLINEA,"
                strinsert = strinsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
                strinsert = strinsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
                strinsert = strinsert & "FR28OPERACION,FR28FECINICIO,"
                strinsert = strinsert & "FR28HORAINICIO,"
                strinsert = strinsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
                strinsert = strinsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
                strinsert = strinsert & "FRH5CODPERIODICIDAD"
                strinsert = strinsert & ") VALUES " & _
                          "(" & _
                          txtText1(0).Text & "," & _
                          linea & "," & _
                          gintprodbuscado(v, 0) & ","
                strinsert = strinsert & "null," 'la U.M. es del medicamento
                'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
                '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
                '  strinsert = strinsert & vntAux & "," & vntAux & ","
                '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
                'Else
                '  VolTotal = 0
                '  strinsert = strinsert & "NULL," & "NULL,"
                'End If
                If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & "," & vntAux & ","
                  VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
                Else
                  VolTotal = 0
                  strinsert = strinsert & "NULL," & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                  strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
                Else
                  strinsert = strinsert & "NULL,"
                End If
                strinsert = strinsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
                
                If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                  strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
                Else
                  strinsert = strinsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                  strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
                Else
                  strinsert = strinsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                  strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
                Else
                  strinsert = strinsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                Else
                  If txtText1(43).Text = "" Then
                    TiemMinInf = 0
                  Else
                    TiemMinInf = CCur(txtText1(43).Text) / 60
                  End If
                  If TiemMinInf <> 0 Then
                    VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                    vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                    strinsert = strinsert & vntAux & ","
                  Else
                    VolPerf = 0
                    strinsert = strinsert & VolPerf & ","
                  End If
                End If
                strinsert = strinsert & "'Diario')"
              End If

              objApp.rdoConnect.Execute strinsert, 64
              objApp.rdoConnect.Execute "Commit", 64
              
              If mstrOperacion = "P" Then
                strupdate = "UPDATE FR2800 SET FR28INDDISPPRN=-1"
                strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
                strupdate = strupdate & " AND FR28NUMLINEA=" & linea
                objApp.rdoConnect.Execute strupdate, 64
                objApp.rdoConnect.Execute "Commit", 64
              End If
              
              rstlinea.Close
              Set rstlinea = Nothing
              rstProducto.Close
              Set rstProducto = Nothing
              'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
            End If
          Next v
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))  'irene
          If mstrOperacion = "P" Then
            'MsgBox "Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso"
            Call objWinInfo.CtrlDataChange
            txtText1(39).SetFocus
          End If
        Else
          '???????????????????????????-
          For v = 0 To gintprodtotal - 1
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
            strupdate = "UPDATE FR2800 SET "
            strupdate = strupdate & "FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
            If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strupdate = strupdate & ",FRH7CODFORMFAR='" & rstProducto("FRH7CODFORMFAR").Value & "'"
            End If
            'If Not IsNull(rstProducto("FR73DOSIS").Value) Then
            '  strupdate = strupdate & ",FR28DOSIS=" & rstProducto("FR73DOSIS").Value
            'End If
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strupdate = strupdate & ",FR28DOSIS=1"
            Else
              If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                If txtText1(32).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                  cmdbuscarprod.Enabled = True
                  blnBoton = False
                  gintprodtotal = 0
                  blnNoVerAgen = False
                  Exit Sub
                Else
                  strupdate = strupdate & ",FR28DOSIS=" & rstProducto("FR73DOSIS").Value * txtText1(32).Text
                End If
              Else
                If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                  If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                    cmdbuscarprod.Enabled = True
                    blnBoton = False
                    gintprodtotal = 0
                    blnNoVerAgen = False
                    Exit Sub
                  Else
                    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                    intPeso = txtText1(32).Text
                    intAltura = txtText1(33).Text
                    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                    vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                    strupdate = strupdate & ",FR28DOSIS=" & vntMasa
                    rstPotPeso.Close
                    rstPotAlt.Close
                    rstFactor.Close
                    Set rstPotPeso = Nothing
                    Set rstPotAlt = Nothing
                    Set rstFactor = Nothing
                  End If
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                  strupdate = strupdate & ",FR28DOSIS=" & vntAux
                End If
              End If
            End If
            
            If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strupdate = strupdate & ",FR93CODUNIMEDIDA='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
            End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL?
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strupdate = strupdate & ",FR28VOLUMEN=" & vntAux
            End If
            If Not IsNull(rstProducto("FR73REFERENCIA").Value) Then
              strupdate = strupdate & ",FR28REFERENCIA='" & rstProducto("FR73REFERENCIA").Value & "'"
            End If
            If Not IsNull(rstProducto("FRH9UBICACION").Value) Then
              strupdate = strupdate & ",FR28UBICACION='" & rstProducto("FRH9UBICACION").Value & "'"
            End If
            
            VolTotal = 0
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol1 = 0
            Else
              Vol1 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            If txtText1(42).Text = "" Then
              Vol2 = 0
            Else
              Vol2 = CCur(txtText1(42).Text)
            End If
            If txtText1(47).Text = "" Then
              Vol3 = 0
            Else
              Vol3 = CCur(txtText1(47).Text)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
            If txtText1(43).Text = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(txtText1(43).Text) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
            strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
          Next v
          Call objWinInfo.DataRefresh
          '???????????????????????????-
        End If
      End If
      gintprodtotal = 0
    End If
  End If

'tabTab1(1).Tab = 1
'objWinInfo.DataMoveLast
'tabTab1(1).Tab = 0
gstrLlamadorProd = ""
If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
  txtText1(35).SetFocus
End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
  cmdbuscarprod.Enabled = True
  blnBoton = False
  blnNoVerAgen = False
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdbuscarprot_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim dtcFecha As Date
Dim dtcDias As Date
Dim strFR73 As String
Dim qryFR73 As rdoQuery
Dim rdoFR73 As rdoResultset
Dim strCant As String
Dim strVol As String
Dim strDosisOLD As String
Dim strDosisNEW As String

blnNoVerAgen = True
If Campos_Obligatorios = True Then
  blnNoVerAgen = False
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

blnBoton = True
cmdbuscarprot.Enabled = False
  'strFR73 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?"
  'Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0118")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          strProducto = "SELECT * FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
                        gintprodbuscado(v, 6) & " AND FR11NUMLINEA=" & gintprodbuscado(v, 5)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          intNumDia = 1
          intDia(0) = 0
          If IsNull(rstProducto("FRH5CODPERIODICIDAD").Value) = False Then
            If rstProducto("FRH5CODPERIODICIDAD").Value = "Agenda" Then
              If IsNull(rstProducto("fr11diasadm").Value) = False Then
                If rstProducto("fr11diasadm").Value <> "" Then
                  Call Agenda(rstProducto("fr11diasadm").Value)
                End If
              End If
            End If
          End If
          
          For i = 1 To intNumDia
            strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                      txtText1(0).Text
            Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
            If IsNull(rstlinea.rdoColumns(0).Value) Then
              linea = 1
            Else
              linea = rstlinea.rdoColumns(0).Value + 1
            End If
            dtcFecha = DateAdd("d", intDia(i - 1), Date)
            If IsNull(rstProducto("fr11numdias").Value) = False Then
              dtcDias = DateAdd("d", rstProducto("fr11numdias").Value, Date)
            End If

            strinsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                        "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR34CODVIA," & _
                        "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                        "FR28HORAINICIO,FR28INDVIAOPC,FR28TIEMINFMIN,FRH5CODPERIODICIDAD," & _
                        "FR28CANTIDAD,FR28CANTDISP,FR28CANTIDADDIL,FR28TIEMMININF,FR28FECFIN,FR28HORAFIN"
            strinsert = strinsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
            ',FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
            strinsert = strinsert & ",FR28INDCOMIENINMED"
            strinsert = strinsert & ",FR28VOLUMEN"
            strinsert = strinsert & ",FR28INDPERF"
            strinsert = strinsert & ",FR28INSTRADMIN"
            strinsert = strinsert & ",FR28VOLTOTAL"
            strinsert = strinsert & ",FR73CODPRODUCTO_2"
            strinsert = strinsert & ",FRH7CODFORMFAR_2"
            strinsert = strinsert & ",FR28DOSIS_2"
            strinsert = strinsert & ",FR93CODUNIMEDIDA_2"
            strinsert = strinsert & ",FR28DESPRODUCTO"
            strinsert = strinsert & ",FR28PATHFORMAG"
            strinsert = strinsert & ",FR28VOLUMEN_2"
            strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & ","
            If IsNull(rstProducto("FR73CODPRODUCTO").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73CODPRODUCTO").Value & ","
            End If
            If IsNull(rstProducto("FR11DOSIS").Value) Then
              strinsert = strinsert & "1,"
              strDosisOLD = 1
              strDosisNEW = 1
            Else
              If rstProducto("FR11DOSISPESO").Value = -1 Then
                If txtText1(32).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                  cmdbuscarprot.Enabled = True
                  blnBoton = False
                  gintprodtotal = 0
                  blnNoVerAgen = False
                  Exit Sub
                Else
                  strDosisOLD = rstProducto("FR11DOSIS").Value
                  strDosisNEW = rstProducto("FR11DOSIS").Value * CCur(txtText1(32).Text)
                  strinsert = strinsert & rstProducto("FR11DOSIS").Value * txtText1(32).Text & ","
                  'qryFR73(0) = rstProducto("FR73CODPRODUCTO").Value
                  'Set rdoFR73 = qryFR73.OpenResultset()
                  'strCant
                  'strVol
                End If
              Else
                If rstProducto("FR11DOSISSUP").Value = -1 Then
                  If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                    cmdbuscarprot.Enabled = True
                    blnBoton = False
                    gintprodtotal = 0
                    blnNoVerAgen = False
                    Exit Sub
                  Else
                    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                    intPeso = txtText1(32).Text
                    intAltura = txtText1(33).Text
                    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR11DOSIS").Value
                    strDosisOLD = rstProducto("FR11DOSIS").Value
                    strDosisNEW = rstProducto("FR11DOSIS").Value * CCur(txtText1(29).Text) 'CCur(vntMasa)
                    vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                    strinsert = strinsert & vntMasa & ","
                    rstPotPeso.Close
                    rstPotAlt.Close
                    rstFactor.Close
                    Set rstPotPeso = Nothing
                    Set rstPotAlt = Nothing
                    Set rstFactor = Nothing
                    'strCant
                    'strVol
                  End If
                Else
                  strDosisOLD = rstProducto("FR11DOSIS").Value
                  strDosisNEW = rstProducto("FR11DOSIS").Value
                  vntAux = objGen.ReplaceStr(rstProducto("FR11DOSIS").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                End If
              End If
            End If
            If IsNull(rstProducto("FRG4CODFRECUENCIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA").Value & "',"
            End If
            If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
            End If
            If IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            End If
            If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
            End If
            If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
            End If
            If IsNull(rstProducto("FR11OPERACION").Value) Then
              If i = 0 Then
                strinsert = strinsert & "null,to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
              Else
                strinsert = strinsert & "NULL,to_date('" & dtcFecha & "','dd/mm/yyyy'),0,"
              End If
            Else
              If i = 1 Then
                strinsert = strinsert & "'" & rstProducto("FR11OPERACION").Value & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
              Else
                strinsert = strinsert & "'" & rstProducto("FR11OPERACION").Value & "',to_date('" & dtcFecha & "','dd/mm/yyyy'),0,"
              End If
            End If
            If IsNull(rstProducto("FR11INDVIAOPC").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR11INDVIAOPC").Value & ","
            End If
            If IsNull(rstProducto("FR11TIEMMININF").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR11TIEMMININF").Value & ","
            End If
            If IsNull(rstProducto("FRH5CODPERIODICIDAD").Value) Then
              strinsert = strinsert & "'Diario',"
            Else
              If i = 1 Then
                strinsert = strinsert & "'" & rstProducto("FRH5CODPERIODICIDAD").Value & "',"
              Else
                strinsert = strinsert & "'Diario',"
              End If
            End If
            If IsNull(rstProducto("FR11CANTIDAD").Value) Then 'FR28CANTDISP
              strinsert = strinsert & "0,0," '0 en vez de null, es obligatorio, FR28CANTDISP
            Else
              vntAux = objGen.ReplaceStr(Format(((CCur(strDosisNEW) * (rstProducto("FR11CANTIDAD").Value)) / CCur(strDosisOLD)), "######0.00"), ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","  'FR28CANTDISP
            End If
            If IsNull(rstProducto("FR11CANTIDADDIL").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR11CANTIDADDIL").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            If IsNull(rstProducto("FR11TIEMMININF").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR11TIEMMININF").Value & ","
            End If
            If i = 1 Then
              If IsNull(rstProducto("fr11numdias").Value) = False Then
                strinsert = strinsert & "to_date('" & dtcDias & "','dd/mm/yyyy'),0,"
              Else
                strinsert = strinsert & "null,null,"
              End If
            Else
              strinsert = strinsert & "to_date('" & dtcFecha & "','dd/mm/yyyy'),0,"
            End If
            If IsNull(rstProducto("FR11REFERENCIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR11REFERENCIA").Value & "',"
            End If
            If IsNull(rstProducto("FR11UBICACION").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR11UBICACION").Value & "',"
            End If
            If IsNull(rstProducto("FR11VELPERFUSION").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR11VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
            '  strinsert = strinsert & "null,"
            'Else
            '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
            'End If
            'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
            '  strinsert = strinsert & "null)"
            'Else
            '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
            'End If
            If IsNull(rstProducto("FR11INDCOMIENINMED").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR11INDCOMIENINMED").Value & ","
            End If
            If IsNull(rstProducto("FR11VOLUMEN").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(Format((CCur(strDosisNEW) * rstProducto("FR11VOLUMEN").Value), "######0.00"), ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            If IsNull(rstProducto("FR11INDPERF").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR11INDPERF").Value & ","
            End If
            If IsNull(rstProducto("FR11INSTRADMIN").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR11INSTRADMIN").Value & "',"
            End If
            If IsNull(rstProducto("FR11VOLTOTAL").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR11VOLTOTAL").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            If IsNull(rstProducto("FR73CODPRODUCTO_2").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73CODPRODUCTO_2").Value & ","
            End If
            If IsNull(rstProducto("FRH7CODFORMFAR_2").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR_2").Value & "',"
            End If
            If IsNull(rstProducto("FR11DOSIS_2").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR11DOSIS_2").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            If IsNull(rstProducto("FR93CODUNIMEDIDA_2").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_2").Value & "',"
            End If
            If IsNull(rstProducto("FR11DESPRODUCTO").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR11DESPRODUCTO").Value & "',"
            End If
            If IsNull(rstProducto("FR11PATHFORMAG").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR11PATHFORMAG").Value & "',"
            End If
            If IsNull(rstProducto("FR11VOLUMEN_2").Value) Then
              strinsert = strinsert & "null)"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR11VOLUMEN_2").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ")"
            End If
            
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            rstlinea.Close
            Set rstlinea = Nothing
          Next i
          rstProducto.Close
          Set rstProducto = Nothing
          'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
        Next v
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      End If
      gintprodtotal = 0
    End If
  End If

'tabTab1(1).Tab = 1
'objWinInfo.DataMoveLast
'tabTab1(1).Tab = 0
If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
  txtText1(35).SetFocus
End If
  Screen.MousePointer = vbHourglass
  'tabTab1(1).Tab = 1
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Screen.MousePointer = vbDefault
cmdbuscarprot.Enabled = True
blnBoton = False
blnNoVerAgen = False
End Sub

Private Sub cmdenviar_Click()
Dim strupdate As String
Dim mensaje As String
Dim rstb As rdoResultset
Dim strb As String
Dim VolTotal As Currency
Dim strFR32Peticion As String
Dim qryFR32Peticion As rdoQuery
Dim rstFR32Peticion As rdoResultset

'On Error GoTo Enviar_Err
cmdenviar.Enabled = False

  If txtText1(25).Text = 2 Then 'firmada
    If chkCheck1(7).Value = 1 Then
      strb = "SELECT SUM(FR28VOLTOTAL) FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
      Set rstb = objApp.rdoConnect.OpenResultset(strb)
      If rstb.EOF Then
        VolTotal = 0
      Else
        VolTotal = rstb(0).Value
      End If
      rstb.Close
      Set rstb = Nothing
      If MsgBox("El Paciente tiene restricciones de Volumen, " & Chr(13) & "el Volumen total de los medicamentos a administrarle es de :" & Chr(13) & VolTotal & " mililitros", vbOKCancel, "Aviso") = vbCancel Then
        cmdenviar.Enabled = True
        Exit Sub
      End If
    End If
    'se cambia el estado y se modifica qu�n env�a la orden
    strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=3,FR66FECENVIO=SYSDATE," & _
                "SG02COD_ENF=" & "'" & objsecurity.strUser & "'" & _
                " WHERE FR66CODPETICION=" & txtText1(0).Text
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    'Se genera un PRN para las l�neas STAT y la etiqueta correspondiente
    
    strFR32Peticion = "SELECT * FROM FR3200 WHERE FR66CODPETICION=? AND FR32INDCOMIENINMED=-1"
    Set qryFR32Peticion = objApp.rdoConnect.CreateQuery("", strFR32Peticion)
    qryFR32Peticion(0) = txtText1(0).Text
    Set rstFR32Peticion = qryFR32Peticion.OpenResultset()
    If Not rstFR32Peticion.EOF Or (chkOMPRN(6).Value = 1) Then
      Call RealizarApuntes
      Call Generar_PRN
    End If
    qryFR32Peticion.Close
    Set rstFR32Peticion = Nothing
    
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.objWinActiveForm.blnChanged = False
    objWinInfo.DataRefresh
    mensaje = MsgBox("La Orden M�dica ha sido enviada", vbInformation, "Farmacia")
  Else
    If txtText1(25).Text = 3 Then
      mensaje = MsgBox("La Orden M�dica ya ha sido enviada", vbInformation, "Farmacia")
    Else
      mensaje = MsgBox("La Orden M�dica no est� a�n firmada para ser enviada", vbInformation, "Farmacia")
    End If
  End If

Enviar_Err:
cmdenviar.Enabled = True


End Sub


Private Sub cmdfirmar_Click()
  Dim strupdate As String
  Dim mensaje As String
  Dim rsta As rdoResultset
  Dim stra As String
  Dim hora As Variant
  Dim rstb As rdoResultset
  Dim strb As String
  Dim VolTotal As Currency
  Dim strSql28 As String
  Dim qrySql28 As rdoQuery
  Dim rstSql28 As rdoResultset
  Dim strDiasAdm As String
  Dim intInd As Integer
  Dim intCont As Integer
  Dim intLon As Integer
  Dim strfec As String
  Dim intFec As Integer
  Dim strFR73CODPRODUCTO As String
  Dim strFR28DOSIS As String
  Dim strFRG4CODFRECUENCIA As String
  Dim strFR93CODUNIMEDIDA As String
  Dim strFR34CODVIA As String
  Dim strFRH7CODFORMFAR As String
  Dim strFR73CODPRODUCTO_DIL As String
  Dim strFR28OPERACION As String
  Dim strFR28HORAINICIO As String
  Dim strFR28INDDISPPRN As String
  Dim strFR28INDCOMIENINMED As String
  Dim strFR28INDPERF As String
  Dim strFR28CANTIDAD As String
  Dim strFR28CANTIDADDIL As String
  Dim strFR28TIEMMININF As String
  Dim strFR28INDVIAOPC As String
  Dim strFR28REFERENCIA As String
  Dim strFR28UBICACION As String
  Dim strFR28VELPERFUSION As String
  Dim strFR28INSTRADMIN As String
  Dim strFR28VOLTOTAL As String
  Dim strFR73CODPRODUCTO_2 As String
  Dim strFRH7CODFORMFAR_2 As String
  Dim strFR28DOSIS_2 As String
  Dim strFR93CODUNIMEDIDA_2 As String
  Dim strFR28VOLUMEN_2 As String
  Dim strFR28DESPRODUCTO As String
  Dim strFR28PATHFORMAG As String
  Dim strFR28VOLUMEN As String
  Dim blnPim As Boolean
  
  cmdfirmar.Enabled = False
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataSave
  tabTab1(1).Tab = 1
  Me.Enabled = False
'jrc  If blnErrorFecFin Then
'jrc    cmdfirmar.Enabled = True
'jrc    MsgBox "No se puede Firmar: Errores en las fechas de Fin", vbExclamation, "Firma"
'jrc    Me.Enabled = True
'jrc    Exit Sub
'jrc  End If
  Screen.MousePointer = vbHourglass
  
  If Peticion_Correcta = False Then
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    cmdfirmar.Enabled = True
    Exit Sub
  End If

  If txtText1(25).Text = 1 Then 'redactada
    If chkCheck1(7).Value = 1 Then
      strb = "SELECT SUM(FR28VOLTOTAL) FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
      Set rstb = objApp.rdoConnect.OpenResultset(strb)
      If rstb.EOF Then
        VolTotal = 0
      Else
        VolTotal = rstb(0).Value
      End If
      rstb.Close
      Set rstb = Nothing
      If MsgBox("El Paciente tiene restricciones de Volumen, " & Chr(13) & "el Volumen total de los medicamentos a administrarle es de :" & Chr(13) & VolTotal & " mililitros", vbOKCancel, "Aviso") = vbCancel Then
        cmdfirmar.Enabled = True
        Exit Sub
      End If
    End If
    
    'Se generan las tuplas correspondientes a la periodicidad agenda
    strSql28 = "SELECT * " & _
               "  FROM FR2800 " & _
               " WHERE FR66CODPETICION= ? " & _
               "   AND FRH5CODPERIODICIDAD = ? "
    Set qrySql28 = objApp.rdoConnect.CreateQuery("", strSql28)
    qrySql28(0) = txtText1(0).Text
    qrySql28(1) = "Agenda"
    Set rstSql28 = qrySql28.OpenResultset()
    While Not rstSql28.EOF
      If IsNull(rstSql28.rdoColumns("FR28DIASADM").Value) Then
        strDiasAdm = ""
      Else
        strDiasAdm = rstSql28.rdoColumns("FR28DIASADM").Value
      End If
      If Len(Trim(strDiasAdm)) > 0 Then
        'Hay que realizar los insert de los d�as de administraci�n
        intLon = Len(strDiasAdm) - 1
        strfec = ""
        blnPim = True
        For intInd = 1 To intLon
          
          'Se obtiene un n�mero con la fecha
          
          For intCont = 1 To intLon
            If IsNumeric(Left(strDiasAdm, 1)) Then
              strfec = strfec & Left(strDiasAdm, 1)
              strDiasAdm = Right(strDiasAdm, Len(strDiasAdm) - 1)
            Else
              'Se ha llegado a un asterisco (separador)
              If Len(Trim(strDiasAdm)) > 0 Then
                strDiasAdm = Right(strDiasAdm, Len(strDiasAdm) - 1)
                If IsNumeric(strfec) Then
                  intFec = strfec
                  '''''
                  If IsNull(rstSql28.rdoColumns("FR73CODPRODUCTO").Value) Then
                    strFR73CODPRODUCTO = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR73CODPRODUCTO").Value)) > 0 Then
                      strFR73CODPRODUCTO = rstSql28.rdoColumns("FR73CODPRODUCTO").Value
                    Else
                      strFR73CODPRODUCTO = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28DOSIS").Value) Then
                    strFR28DOSIS = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28DOSIS").Value)) > 0 Then
                      strFR28DOSIS = objGen.ReplaceStr(rstSql28.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
                    Else
                      strFR28DOSIS = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FRG4CODFRECUENCIA").Value) Then
                    strFRG4CODFRECUENCIA = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FRG4CODFRECUENCIA").Value)) > 0 Then
                      strFRG4CODFRECUENCIA = "'" & rstSql28.rdoColumns("FRG4CODFRECUENCIA").Value & "'"
                    Else
                      strFRG4CODFRECUENCIA = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                    strFR93CODUNIMEDIDA = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR93CODUNIMEDIDA").Value)) > 0 Then
                      strFR93CODUNIMEDIDA = "'" & rstSql28.rdoColumns("FR93CODUNIMEDIDA").Value & "'"
                    Else
                      strFR93CODUNIMEDIDA = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR34CODVIA").Value) Then
                    strFR34CODVIA = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR34CODVIA").Value)) > 0 Then
                      strFR34CODVIA = "'" & rstSql28.rdoColumns("FR34CODVIA").Value & "'"
                    Else
                      strFR34CODVIA = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FRH7CODFORMFAR").Value) Then
                    strFRH7CODFORMFAR = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FRH7CODFORMFAR").Value)) > 0 Then
                      strFRH7CODFORMFAR = "'" & rstSql28.rdoColumns("FRH7CODFORMFAR").Value & "'"
                    Else
                      strFRH7CODFORMFAR = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
                    strFR73CODPRODUCTO_DIL = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR73CODPRODUCTO_DIL").Value)) > 0 Then
                      strFR73CODPRODUCTO_DIL = rstSql28.rdoColumns("FR73CODPRODUCTO_DIL").Value
                    Else
                      strFR73CODPRODUCTO_DIL = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28OPERACION").Value) Then
                    strFR28OPERACION = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28OPERACION").Value)) > 0 Then
                      strFR28OPERACION = "'" & rstSql28.rdoColumns("FR28OPERACION").Value & "'"
                    Else
                      strFR28OPERACION = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28HORAINICIO").Value) Then
                    strFR28HORAINICIO = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28HORAINICIO").Value)) > 0 Then
                      If blnPim = True Then
                        strFR28HORAINICIO = rstSql28.rdoColumns("FR28HORAINICIO").Value
                        blnPim = False
                      Else
                        strFR28HORAINICIO = "1"
                      End If
                    Else
                      strFR28HORAINICIO = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28INDDISPPRN").Value) Then
                    strFR28INDDISPPRN = "0"
                  Else
                    If (rstSql28.rdoColumns("FR28INDDISPPRN").Value = -1) Or _
                       (rstSql28.rdoColumns("FR28INDDISPPRN").Value = 1) Then
                      strFR28INDDISPPRN = "-1"
                    Else
                      strFR28INDDISPPRN = "0"
                    End If
                  End If
                  
                  If rstSql28.rdoColumns("FR28INDCOMIENINMED").Value = 0 Then
                    strFR28INDCOMIENINMED = "0"
                  Else
                    If (rstSql28.rdoColumns("FR28INDCOMIENINMED").Value = -1) Or _
                       (rstSql28.rdoColumns("FR28INDCOMIENINMED").Value = 1) Then
                      strFR28INDCOMIENINMED = "-1"
                    Else
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28INDPERF").Value) Then
                    strFR28INDPERF = "0"
                  Else
                    If (rstSql28.rdoColumns("FR28INDPERF").Value = -1) Or _
                       (rstSql28.rdoColumns("FR28INDPERF").Value = 1) Then
                      strFR28INDPERF = "-1"
                    Else
                      strFR28INDPERF = "0"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28CANTIDAD").Value) Then
                    strFR28CANTIDAD = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28CANTIDAD").Value)) > 0 Then
                      strFR28CANTIDAD = objGen.ReplaceStr(rstSql28.rdoColumns("FR28CANTIDAD").Value, ",", ".", 1)
                    Else
                      strFR28CANTIDAD = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28CANTIDADDIL").Value) Then
                    strFR28CANTIDADDIL = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28CANTIDADDIL").Value)) > 0 Then
                      strFR28CANTIDADDIL = rstSql28.rdoColumns("FR28CANTIDADDIL").Value
                    Else
                      strFR28CANTIDADDIL = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28TIEMMININF").Value) Then
                    strFR28TIEMMININF = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28TIEMMININF").Value)) > 0 Then
                      strFR28TIEMMININF = rstSql28.rdoColumns("FR28TIEMMININF").Value
                    Else
                      strFR28TIEMMININF = "null"
                    End If
                  End If
                  
                  If rstSql28.rdoColumns("FR28INDVIAOPC").Value = 0 Then
                    strFR28INDVIAOPC = "0"
                  Else
                    If (rstSql28.rdoColumns("FR28INDVIAOPC").Value = -1) Or _
                       (rstSql28.rdoColumns("FR28INDVIAOPC").Value = 1) Then
                      strFR28INDVIAOPC = "-1"
                    Else
                      strFR28INDVIAOPC = "0"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28REFERENCIA").Value) Then
                    strFR28REFERENCIA = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28REFERENCIA").Value)) > 0 Then
                      strFR28REFERENCIA = "'" & rstSql28.rdoColumns("FR28REFERENCIA").Value & "'"
                    Else
                      strFR28REFERENCIA = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28UBICACION").Value) Then
                    strFR28UBICACION = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28UBICACION").Value)) > 0 Then
                      strFR28UBICACION = "'" & rstSql28.rdoColumns("FR28UBICACION").Value & "'"
                    Else
                      strFR28UBICACION = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28VELPERFUSION").Value) Then
                    strFR28VELPERFUSION = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28VELPERFUSION").Value)) > 0 Then
                      strFR28VELPERFUSION = objGen.ReplaceStr(rstSql28.rdoColumns("FR28VELPERFUSION").Value, ",", ".", 1)
                    Else
                      strFR28VELPERFUSION = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28INSTRADMIN").Value) Then 'FR28INSTRADMIN
                    strFR28INSTRADMIN = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28INSTRADMIN").Value)) > 0 Then
                      strFR28INSTRADMIN = "'" & rstSql28.rdoColumns("FR28INSTRADMIN").Value & "'"
                    Else
                      strFR28INSTRADMIN = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28VOLTOTAL").Value) Then 'FR28VOLTOTAL
                    strFR28VOLTOTAL = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28VOLTOTAL").Value)) > 0 Then
                      strFR28VOLTOTAL = objGen.ReplaceStr(rstSql28.rdoColumns("FR28VOLTOTAL").Value, ",", ".", 1)
                    Else
                      strFR28VOLTOTAL = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR73CODPRODUCTO_2").Value) Then 'FR73CODPRODUCTO_2
                    strFR73CODPRODUCTO_2 = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR73CODPRODUCTO_2").Value)) > 0 Then
                      strFR73CODPRODUCTO_2 = rstSql28.rdoColumns("FR73CODPRODUCTO_2").Value
                    Else
                      strFR73CODPRODUCTO_2 = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FRH7CODFORMFAR_2").Value) Then 'FRH7CODFORMFAR_2
                    strFRH7CODFORMFAR_2 = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FRH7CODFORMFAR_2").Value)) > 0 Then
                      strFRH7CODFORMFAR_2 = "'" & rstSql28.rdoColumns("FRH7CODFORMFAR_2").Value & "'"
                    Else
                      strFRH7CODFORMFAR_2 = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28DOSIS_2").Value) Then 'FR28DOSIS_2
                    strFR28DOSIS_2 = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28DOSIS_2").Value)) > 0 Then
                      strFR28DOSIS_2 = objGen.ReplaceStr(rstSql28.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1)
                    Else
                      strFR28DOSIS_2 = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then 'FR93CODUNIMEDIDA_2
                    strFR93CODUNIMEDIDA_2 = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR93CODUNIMEDIDA_2").Value)) > 0 Then
                      strFR93CODUNIMEDIDA_2 = "'" & rstSql28.rdoColumns("FR93CODUNIMEDIDA_2").Value & "'"
                    Else
                      strFR93CODUNIMEDIDA_2 = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28VOLUMEN_2").Value) Then 'FR28VOLUMEN_2
                    strFR28VOLUMEN_2 = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28VOLUMEN_2").Value)) > 0 Then
                      strFR28VOLUMEN_2 = rstSql28.rdoColumns("FR28VOLUMEN_2").Value
                    Else
                      strFR28VOLUMEN_2 = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28DESPRODUCTO").Value) Then 'FR28DESPRODUCTO
                    strFR28DESPRODUCTO = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28DESPRODUCTO").Value)) > 0 Then
                      strFR28DESPRODUCTO = "'" & rstSql28.rdoColumns("FR28DESPRODUCTO").Value & "'"
                    Else
                      strFR28DESPRODUCTO = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28VOLUMEN").Value) Then 'FR28VOLUMEN
                    strFR28VOLUMEN = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28VOLUMEN").Value)) > 0 Then
                      strFR28VOLUMEN = rstSql28.rdoColumns("FR28VOLUMEN").Value
                    Else
                      strFR28VOLUMEN = "null"
                    End If
                  End If
                  
                  If IsNull(rstSql28.rdoColumns("FR28PATHFORMAG").Value) Then 'FR28PATHFORMAG
                    strFR28PATHFORMAG = "null"
                  Else
                    If Len(Trim(rstSql28.rdoColumns("FR28PATHFORMAG").Value)) > 0 Then
                      strFR28PATHFORMAG = "'" & rstSql28.rdoColumns("FR28PATHFORMAG").Value & "'"
                    Else
                      strFR28PATHFORMAG = "null"
                    End If
                  End If
                  '''''
                  Call insertar(intFec, rstSql28.rdoColumns("FR66CODPETICION").Value, rstSql28.rdoColumns("FR28NUMLINEA").Value, strFR73CODPRODUCTO, _
                              strFR28DOSIS, strFRG4CODFRECUENCIA, strFR93CODUNIMEDIDA, strFR34CODVIA, strFRH7CODFORMFAR, _
                              strFR73CODPRODUCTO_DIL, strFR28OPERACION, strFR28HORAINICIO, strFR28INDDISPPRN, strFR28INDCOMIENINMED, strFR28INDPERF, _
                              strFR28CANTIDAD, strFR28CANTIDADDIL, strFR28TIEMMININF, strFR28INDVIAOPC, strFR28REFERENCIA, strFR28UBICACION, _
                              strFR28VELPERFUSION, strFR28INSTRADMIN, strFR28VOLTOTAL, strFR73CODPRODUCTO_2, strFRH7CODFORMFAR_2, strFR28DOSIS_2, _
                              strFR93CODUNIMEDIDA_2, strFR28VOLUMEN_2, strFR28DESPRODUCTO, strFR28PATHFORMAG, strFR28VOLUMEN)
                  strfec = ""
                End If
              End If
            End If
          Next intCont
        Next intInd
      Else
        MsgBox "Existe una petici�n de medicamento con periodicidad agenda en la que no se han seleccionado los d�as de administraci�n", vbInformation, "No es Posible Firmar"
        Me.Enabled = True
        Screen.MousePointer = vbDefault
        cmdfirmar.Enabled = True
        Exit Sub
      End If
      rstSql28.MoveNext
    Wend
    qrySql28.Close
    Set qrySql28 = Nothing
    Set rstSql28 = Nothing
    
    stra = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    hora = rsta.rdoColumns(0).Value
    hora = hora & "."
    stra = ""
    stra = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    hora = hora & rsta.rdoColumns(0).Value
    strupdate = "UPDATE FR6600 SET FR66FECFIRMMEDI=SYSDATE," & _
                "FR66HORAFIRMMEDI=" & hora & "," & _
                "SG02COD_FEN=" & "'" & objsecurity.strUser & "'" & _
                ",FR26CODESTPETIC=2" & _
                " WHERE FR66CODPETICION=" & txtText1(0).Text
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataRefresh
    mensaje = MsgBox("La Orden M�dica ha sido firmada", vbInformation, "Farmacia")
  Else
    mensaje = MsgBox("La Orden M�dica ya  ha sido firmada", vbInformation, "Farmacia")
  End If

  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdfirmar.Enabled = True
End Sub


Private Sub cmdperfil_Click()
Dim mensaje As String

  blnNoVerAgen = True
  cmdperfil.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(2).Text <> "" Then
      glngPaciente = txtText1(2).Text
      Call objsecurity.LaunchProcess("FR0160")
    Else
      mensaje = MsgBox("No hay ning�n paciente.", vbInformation, "Aviso")
    End If
  End If

  cmdperfil.Enabled = True
  blnNoVerAgen = False
End Sub

Private Sub cmdsitcarro_Click()
Dim mensaje As String

  blnNoVerAgen = True
  cmdsitcarro.Enabled = False

  If txtText1(16).Text = "" Then
    mensaje = MsgBox("No hay ning�n servicio seleccionado", vbInformation, "Aviso")
  Else
    If txtText1(17).Text <> "" Then
      gintservicio = txtText1(16).Text
      frmVerSitCarro.txtServicio.Text = txtText1(17).Text
      Call objsecurity.LaunchProcess("FR0114")
    Else
      mensaje = MsgBox("El servicio no es v�lido", vbInformation, "Aviso")
    End If
  End If

cmdsitcarro.Enabled = True
blnNoVerAgen = False
End Sub

Private Sub Form_Activate()
Dim sqlstr As String
Dim rsta As rdoResultset
Dim FR66CODPETICION As Variant
Dim fr66indom As Variant
Dim ci21codpersona As Variant
Dim fr26codestpetic As Variant
Dim strad1500 As String
Dim rstad1500 As rdoResultset
Dim ad02coddpto As Variant
Dim sg02cod_med As Variant
Dim strfec As String
Dim rstfec As rdoResultset
Dim hora As Variant
Dim fr66horaredacci As Variant
Dim strinsert As String
Dim mensaje As String
Dim strpaciente As String
Dim rstpaciente As rdoResultset
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim blnInsertar As Boolean
Dim strfri600 As String
Dim rstfri600 As rdoResultset
Dim intAlergico As Integer
Dim strpeso As String
Dim rstpeso As rdoResultset
Dim rstausorest As rdoResultset
Dim strusorest As String
Dim rstrest As rdoResultset
Dim strrest As String
Dim strfecnac As String
Dim rstfecnac As rdoResultset
Dim edad As Integer
Dim auxProceso, auxAsistencia As Currency
Dim strProcAsis As String
Dim rstProcAsis As rdoResultset
Dim strserviciomed As String
Dim rstserviciomed As rdoResultset
Dim v As Integer
Dim qryInsert As rdoQuery
Dim IralUltimoalCopiarLineas As Boolean


If gintfirmarOM = 1 Then
  If gblnActivateRedactarOM = False Then
    gblnActivateRedactarOM = True
  Else
    Exit Sub
  End If
End If

  strOrigen = "T"
  
  If gblnSelPaciente = True Then
    Me.Enabled = False
    If blnNoPrimAct = False Then
      If gstrLlamador = "frmSelPaciente" Then
        Call Nueva_OM(frmSelPaciente.IdPersona1.Text, frmSelPaciente.grdDBGrid1(1).Columns("Proceso").Value, frmSelPaciente.grdDBGrid1(1).Columns("Asistencia").Value, frmSelPaciente.grdDBGrid1(1).Columns("Dpto.").Value, frmSelPaciente.grdDBGrid1(1).Columns("Doctor").Value)
      Else
        If gstrLlamador = "frmSelPacienteServEspeciales" Then
          Call Nueva_OM(frmSelPacienteServEspeciales.IdPersona1.Text, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Proceso").Value, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Asistencia").Value, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Dpto.").Value, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Doctor").Value)
        End If
      End If
      blnNoPrimAct = True
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
    End If
    Me.Enabled = False
    Call presentacion_pantalla
    Me.Enabled = False
    If tabTab1(1).Tab = 0 Then
      blnDetalle = True
    Else
      blnDetalle = False
    End If
    On Error Resume Next
    grdDBGrid1(6).Columns("Medicamento").Position = 1
    grdDBGrid1(6).Columns("FF").Position = 2
    grdDBGrid1(6).Columns("Dosis").Position = 3
    grdDBGrid1(6).Columns("UM").Position = 4
    grdDBGrid1(6).Columns("Volum.").Position = 5
    grdDBGrid1(6).Columns("Cantidad").Position = 6
    grdDBGrid1(6).Columns("Frec.").Position = 7
    grdDBGrid1(6).Columns("V�a").Position = 8
    grdDBGrid1(6).Columns("Period.").Position = 9
    grdDBGrid1(6).Columns("Inicio").Position = 10
    grdDBGrid1(6).Columns("H.I.").Position = 11
    grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Position = 12
    grdDBGrid1(6).Columns("Vol.Dil.").Position = 13
    grdDBGrid1(6).Columns("T.Inf.(min)").Position = 14
    grdDBGrid1(6).Columns("Inst.Admin.").Position = 15
    grdDBGrid1(6).Columns("Fin").Position = 16
    grdDBGrid1(6).Columns("H.F.").Position = 17
    grdDBGrid1(6).Columns("Perfusi�n").Position = 18
    grdDBGrid1(6).Columns("Vol.Total").Position = 19
    grdDBGrid1(6).Columns("STAT").Position = 20
    grdDBGrid1(6).Columns("PERF").Position = 21
    grdDBGrid1(6).Columns("Medicamento 2").Position = 22
    grdDBGrid1(6).Columns("FF2").Position = 23
    grdDBGrid1(6).Columns("Dosis2").Position = 24
    grdDBGrid1(6).Columns("UM2").Position = 25
    grdDBGrid1(6).Columns("Medic.No Form.").Position = 26
    grdDBGrid1(6).Columns("Arch.Inf.F.M.").Position = 27
    grdDBGrid1(6).Columns("C�digo Medicamento").Visible = False
    grdDBGrid1(6).Columns("Medicamento").Width = 2235
    grdDBGrid1(6).Columns("Medic.No Form.").Width = 2235
    grdDBGrid1(6).Columns("Arch.Inf.F.M.").Width = 2235
    grdDBGrid1(6).Columns("FF").Width = 390
    grdDBGrid1(6).Columns("Dosis").Width = 945
    grdDBGrid1(6).Columns("UM").Width = 510
    grdDBGrid1(6).Columns("Cantidad").Width = 915
    grdDBGrid1(6).Columns("Frec.").Width = 929
    grdDBGrid1(6).Columns("V�a").Width = 600
    grdDBGrid1(6).Columns("Period.").Width = 1065
    grdDBGrid1(6).Columns("Inicio").Width = 1095
    grdDBGrid1(6).Columns("H.I.").Width = 540
    grdDBGrid1(6).Columns("C�digo Producto diluir").Visible = False
    grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Width = 2160
    grdDBGrid1(6).Columns("Vol.Dil.").Width = 825
    grdDBGrid1(6).Columns("Inst.Admin.").Width = 2500
    grdDBGrid1(6).Columns("C�digo Medicamento 2").Visible = False
    grdDBGrid1(6).Columns("Medicamento 2").Width = 1815
    grdDBGrid1(6).Columns("FF2").Width = 390
    grdDBGrid1(6).Columns("Dosis2").Width = 1289
    grdDBGrid1(6).Columns("UM2").Width = 645
    grdDBGrid1(6).Columns("Fin").Width = 1095
    grdDBGrid1(6).Columns("H.F.").Width = 540
    grdDBGrid1(6).Columns("Perfusi�n").Width = 1170
    grdDBGrid1(6).Columns("Vol.Total").Width = 1170
    grdDBGrid1(6).Columns("STAT").Width = 525
    grdDBGrid1(6).Columns("PERF").Width = 645
    grdDBGrid1(6).Columns("C�digo Petici�n").Visible = False
    grdDBGrid1(6).Columns("Num.Linea").Visible = False
    grdDBGrid1(6).Columns("Cambiar V�a?").Visible = False
    grdDBGrid1(6).Columns("Prn?").Visible = False
    grdDBGrid1(6).Columns("T.Inf.(min)").Width = 825
    grdDBGrid1(6).Columns("Operaci�n").Visible = False
    grdDBGrid1(6).Columns("Ubicaci�n").Visible = False
    grdDBGrid1(6).Columns("Referencia").Width = 1100
    grdDBGrid1(6).Columns("Referencia").Visible = False
    grdDBGrid1(6).Columns("Volum.").Width = 825
    grdDBGrid1(6).Columns("Cant.Dispens.").Visible = False
    '*******************************************************************************
    ' Exit Sub   'ahora viene de OM anteriores
  End If 'If gblnSelPaciente = True Then

  If blnFormActRest = False Then
    Me.Enabled = False 'Para que no toquen en ninguna parte de la
                       'pantalla hasta que este cargada
    'se viene de la pantalla frmFirmarOMPRNPOM(FR0169.frm)
    If gintfirmarOM = 1 Then
      'gintfirmarOM = 0  esta instrucci�n va ahora en Form_Unload
      objWinInfo.objWinActiveForm.strWhere = "FR66CODPETICION=" & glngpeticion & _
                     " AND (FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
                     " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
      objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
      objWinInfo.DataRefresh
      grdDBGrid1(6).Columns("Medicamento").Position = 1
      grdDBGrid1(6).Columns("FF").Position = 2
      grdDBGrid1(6).Columns("Dosis").Position = 3
      grdDBGrid1(6).Columns("UM").Position = 4
      grdDBGrid1(6).Columns("Volum.").Position = 5
      grdDBGrid1(6).Columns("Cantidad").Position = 6
      grdDBGrid1(6).Columns("Frec.").Position = 7
      grdDBGrid1(6).Columns("V�a").Position = 8
      grdDBGrid1(6).Columns("Period.").Position = 9
      grdDBGrid1(6).Columns("Inicio").Position = 10
      grdDBGrid1(6).Columns("H.I.").Position = 11
      grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Position = 12
      grdDBGrid1(6).Columns("Vol.Dil.").Position = 13
      grdDBGrid1(6).Columns("T.Inf.(min)").Position = 14
      grdDBGrid1(6).Columns("Inst.Admin.").Position = 15
      grdDBGrid1(6).Columns("Fin").Position = 16
      grdDBGrid1(6).Columns("H.F.").Position = 17
      grdDBGrid1(6).Columns("Perfusi�n").Position = 18
      grdDBGrid1(6).Columns("Vol.Total").Position = 19
      grdDBGrid1(6).Columns("STAT").Position = 20
      grdDBGrid1(6).Columns("PERF").Position = 21
      grdDBGrid1(6).Columns("Medicamento 2").Position = 22
      grdDBGrid1(6).Columns("FF2").Position = 23
      grdDBGrid1(6).Columns("Dosis2").Position = 24
      grdDBGrid1(6).Columns("UM2").Position = 25
      grdDBGrid1(6).Columns("Medic.No Form.").Position = 26
      grdDBGrid1(6).Columns("Arch.Inf.F.M.").Position = 27
      grdDBGrid1(6).Columns("C�digo Medicamento").Visible = False
      grdDBGrid1(6).Columns("Medicamento").Width = 2235
      grdDBGrid1(6).Columns("Medic.No Form.").Width = 2235
      grdDBGrid1(6).Columns("Arch.Inf.F.M.").Width = 2235
      grdDBGrid1(6).Columns("FF").Width = 390
      grdDBGrid1(6).Columns("Dosis").Width = 945
      grdDBGrid1(6).Columns("UM").Width = 510
      grdDBGrid1(6).Columns("Cantidad").Width = 915
      grdDBGrid1(6).Columns("Frec.").Width = 929
      grdDBGrid1(6).Columns("V�a").Width = 600
      grdDBGrid1(6).Columns("Period.").Width = 1065
      grdDBGrid1(6).Columns("Inicio").Width = 1095
      grdDBGrid1(6).Columns("H.I.").Width = 540
      grdDBGrid1(6).Columns("C�digo Producto diluir").Visible = False
      grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Width = 2160
      grdDBGrid1(6).Columns("Vol.Dil.").Width = 825
      grdDBGrid1(6).Columns("Inst.Admin.").Width = 2500
      grdDBGrid1(6).Columns("C�digo Medicamento 2").Visible = False
      grdDBGrid1(6).Columns("Medicamento 2").Width = 1815
      grdDBGrid1(6).Columns("FF2").Width = 390
      grdDBGrid1(6).Columns("Dosis2").Width = 1289
      grdDBGrid1(6).Columns("UM2").Width = 645
      grdDBGrid1(6).Columns("Fin").Width = 1095
      grdDBGrid1(6).Columns("H.F.").Width = 540
      grdDBGrid1(6).Columns("Perfusi�n").Width = 1170
      grdDBGrid1(6).Columns("Vol.Total").Width = 1170
      grdDBGrid1(6).Columns("STAT").Width = 525
      grdDBGrid1(6).Columns("PERF").Width = 645
      grdDBGrid1(6).Columns("C�digo Petici�n").Visible = False
      grdDBGrid1(6).Columns("Num.Linea").Visible = False
      grdDBGrid1(6).Columns("Cambiar V�a?").Visible = False
      grdDBGrid1(6).Columns("Prn?").Visible = False
      grdDBGrid1(6).Columns("T.Inf.(min)").Width = 825
      grdDBGrid1(6).Columns("Operaci�n").Visible = False
      grdDBGrid1(6).Columns("Ubicaci�n").Visible = False
      grdDBGrid1(6).Columns("Referencia").Width = 1100
      grdDBGrid1(6).Columns("Referencia").Visible = False
      grdDBGrid1(6).Columns("Volum.").Width = 825
      grdDBGrid1(6).Columns("Cant.Dispens.").Visible = False
      If blnInload = True Then
        blnInload = False
      End If
      Me.Enabled = True
      Exit Sub
    Else
      'If ginteventloadRedOM = 0 Then
      '  objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR (FR66INDOM=0 AND FR66CODOMORIPRN IS NOT NULL)) AND CI21CODPERSONA=" & glngselpaciente
      'End If
    End If 'If gintfirmarOM = 1 Then
    If gblnSelPaciente = False Then
      'If ginteventloadRedOM = 0 Then
      If blnInload = True Then
        sqlstr = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        FR66CODPETICION = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
        'por defecto se clica que sea Orden M�dica
        fr66indom = -1
        ci21codpersona = glngselpaciente 'paciente
        fr26codestpetic = 1 'estado REDACTADA
        strProcAsis = "select ad1500.AD07CODPROCESO,ad1500.AD01CODASISTENCI "
        strProcAsis = strProcAsis & " From ad1500, ad0800, ad0100, ad0700 "
        strProcAsis = strProcAsis & " Where ad1500.AD07CODPROCESO = ad0800.AD07CODPROCESO "
        strProcAsis = strProcAsis & " and ad1500.AD07CODPROCESO=ad0700.AD07CODPROCESO"
        strProcAsis = strProcAsis & " and ad1500.AD01CODASISTENCI=ad0800.AD01CODASISTENCI"
        strProcAsis = strProcAsis & " and ad1500.AD01CODASISTENCI=ad0100.AD01CODASISTENCI"
        strProcAsis = strProcAsis & " and ad0700.ci21codpersona=" & glngselpaciente
        strProcAsis = strProcAsis & " and ad0100.ci21codpersona=" & glngselpaciente
        strProcAsis = strProcAsis & " and ad0800.AD34CODESTADO=1"
        strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TRUNC(AD0800.AD08FECINICIO) AND NVL(TRUNC(AD0800.AD08FECFIN),TO_DATE('31/12/9999','DD/MM/YYYY'))"
        strProcAsis = strProcAsis & " ORDER BY AD0800.AD08FECINICIO DESC "
        Set rstProcAsis = objApp.rdoConnect.OpenResultset(strProcAsis)
        If rstProcAsis.EOF Then
          strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
          Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
          If rstpaciente.EOF Then
            mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
          Else
            mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                      rstpaciente("CI22PRIAPEL").Value & _
                      rstpaciente("CI22SEGAPEL").Value & _
                      " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                      Chr(13) & " No tiene asignado PROCESO-ASISTENCIA a CAMA." & _
                      Chr(13), vbCritical)
          End If
          rstpaciente.Close
          Set rstpaciente = Nothing
          If blnInload = True Then
            blnInload = False
          End If
          Me.Enabled = True
          Unload Me
          Exit Sub
        Else
          If IsNull(rstProcAsis("AD07CODPROCESO").Value) Or IsNull(rstProcAsis("AD01CODASISTENCI").Value) Then
            strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
            Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
            If rstpaciente.EOF Then
              mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
            Else
              mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                        rstpaciente("CI22PRIAPEL").Value & _
                        rstpaciente("CI22SEGAPEL").Value & _
                        " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                        Chr(13) & " No tiene asignado PROCESO-ASISTENCIA a CAMA." & _
                        Chr(13), vbCritical)
            End If
            rstpaciente.Close
            Set rstpaciente = Nothing
            If blnInload = True Then
              blnInload = False
            End If
            Me.Enabled = True
            Unload Me
            Exit Sub
          Else
            auxProceso = rstProcAsis("AD07CODPROCESO").Value
            auxAsistencia = rstProcAsis("AD01CODASISTENCI").Value
          End If
        End If 'If rstProcAsis.EOF Then
        rstProcAsis.Close
        Set rstProcAsis = Nothing
        strad1500 = "SELECT AD02CODDPTO FROM AD0500 WHERE " & _
                    "AD01CODASISTENCI=" & auxAsistencia & _
                    " AND AD07CODPROCESO=" & auxProceso
        Set rstad1500 = objApp.rdoConnect.OpenResultset(strad1500)
        If Not rstad1500.EOF Then
          ad02coddpto = rstad1500.rdoColumns(0).Value  'servicio
        Else
          'si el paciente no tiene cama se mete como servicio el del m�dico que hace Login
          strserviciomed = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=" & _
                           "'" & objsecurity.strUser & "'" & " AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
          Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
          If Not rstserviciomed.EOF Then
            ad02coddpto = rstserviciomed.rdoColumns(0).Value
          Else
            Call MsgBox("El m�dico " & objsecurity.strUser & " no est� asignado a ning�n servicio", vbInformation, "Aviso")
            If blnInload = True Then
              blnInload = False
            End If
            Me.Enabled = True
            Unload Me
            Exit Sub
          End If
          rstserviciomed.Close
          Set rstserviciomed = Nothing
        End If
        rstad1500.Close
        Set rstad1500 = Nothing
        sg02cod_med = objsecurity.strUser
  
        strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
        Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
        hora = rstfec.rdoColumns(0).Value & "."
        Set rstfec = Nothing
        fr66horaredacci = hora
        strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
        Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
        hora = hora & rstfec.rdoColumns(0).Value
        rstfec.Close
        Set rstfec = Nothing
        fr66horaredacci = hora
  
        strpeso = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE " & _
                  "AD01CODASISTENCI=" & auxAsistencia
        Set rstpeso = objApp.rdoConnect.OpenResultset(strpeso)
        strfri600 = "select count(*) from fri600 where ci21codpersona=" & ci21codpersona
        Set rstfri600 = objApp.rdoConnect.OpenResultset(strfri600)
        If rstfri600(0).Value > 0 Then
          intAlergico = 1
        Else
          intAlergico = 0
        End If
        rstfri600.Close
        Set rstfri600 = Nothing
  
        strinsert = "INSERT INTO FR6600 (FR66CODPETICION,SG02COD_MED,AD02CODDPTO,"
        strinsert = strinsert & "CI21CODPERSONA,FR66FECREDACCION,FR66HORAREDACCI,FR26CODESTPETIC,FR66INDOM,"
        strinsert = strinsert & "FR66PESOPAC,FR66ALTUPAC,FR66INDALERGIA,"
        strinsert = strinsert & "AD07CODPROCESO,AD01CODASISTENCI"
        strinsert = strinsert & ") VALUES "
        strinsert = strinsert & "("
        strinsert = strinsert & FR66CODPETICION & ","
        strinsert = strinsert & "'" & sg02cod_med & "'" & ","
        strinsert = strinsert & ad02coddpto & ","
        strinsert = strinsert & ci21codpersona & ","
        strinsert = strinsert & "SYSDATE" & ","
        strinsert = strinsert & fr66horaredacci & ","
        strinsert = strinsert & fr26codestpetic & ","
        strinsert = strinsert & fr66indom & ","
        If Not rstpeso.EOF Then
          If IsNull(rstpeso.rdoColumns(0).Value) Then
            strinsert = strinsert & "NULL" & ","
          Else
            strinsert = strinsert & objGen.ReplaceStr(rstpeso.rdoColumns(0).Value, ",", ".", 1) & ","
          End If
          If IsNull(rstpeso.rdoColumns(1).Value) Then
            strinsert = strinsert & "NULL"
          Else
            strinsert = strinsert & rstpeso.rdoColumns(1).Value
          End If
        Else
          strinsert = strinsert & "NULL,NULL"
        End If
        strinsert = strinsert & "," & intAlergico
        strinsert = strinsert & "," & auxProceso
        strinsert = strinsert & "," & auxAsistencia
        strinsert = strinsert & ")"
        rstpeso.Close
        Set rstpeso = Nothing
      
        Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
        qryInsert.Execute
        qryInsert.Close
        Set qryInsert = Nothing
        'objApp.rdoConnect.Execute strinsert, 64
        'objApp.rdoConnect.Execute "Commit", 64
  
        'que vaya al �ltimo registro porque como el insert se hace a pelo si no se quedar�a
        'apuntando siempre al primer registro(es decir,a la primera orden m�dica)
        'JRC, va al ultimo pues esta ordenada en orden inverso
        objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR (FR66INDOM=0 AND FR66CODOMORIPRN IS NOT NULL)) AND CI21CODPERSONA=" & glngselpaciente _
                 & " AND AD02CODDPTO=" & ad02coddpto & _
                 " AND (FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
                 " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        Call objWinInfo.DataRefresh
        Me.Enabled = False
      End If 'If ginteventloadRedOM = 0 Then
    End If 'gblnSelPaciente = False Then
    '--------------------------------------------------------
    'gintredactarorden se ha puesto a 1 al seleccionar un paciente en frmselPaciente e ir
    'a ver sus �rdenes m�dicas anteriores
    If gintredactarorden = 1 Then 'Copiar lineas de OM anteriores
      IralUltimoalCopiarLineas = True
      gintredactarorden = 0
      'si se ha entrado a Ordenes M�dicas Anteriores desde la pantalla de Seleccionar Paciente
      'tiene que guardar los medicamentos que se traen
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If
        rstlinea.Close
        Set rstlinea = Nothing
        If gintprodbuscado(v, 0) <> "" Then
          strProducto = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & gintprodbuscado(v, 0) & _
                        " and fr28numlinea=" & gintprodbuscado(v, 1)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          blnInsertar = True
        Else
          blnInsertar = False
        End If
        If blnInsertar Then
          strinsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                      "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR28VOLUMEN,FR34CODVIA," & _
                      "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                      "FR28HORAINICIO," & _
                      "FR28TIEMINFMIN,FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD," & _
                      "FR28INDBLOQUEADA,FR28FECBLOQUEO,SG02CODPERSBLOQ,FR28INDSN," & _
                      "FR28INDESTOM,FR28CANTIDAD,FR28FECFIN,FR28HORAFIN,FR28CANTIDADDIL,FR28TIEMMININF,FR28INDVIAOPC"
          strinsert = strinsert & ",FR28REFERENCIA,FR28CANTDISP,FR28UBICACION"
          strinsert = strinsert & ",FR28INDPERF,FR28INSTRADMIN,FR28VELPERFUSION"
          strinsert = strinsert & ",FR28VOLTOTAL,FR73CODPRODUCTO_2,FRH7CODFORMFAR_2"
          strinsert = strinsert & ",FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28DESPRODUCTO"
          strinsert = strinsert & ") VALUES "
          strinsert = strinsert & "(" & _
                      txtText1(0).Text & "," & _
                      rstProducto("fr28numlinea").Value & ","
          If IsNull(rstProducto("FR73CODPRODUCTO").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR73CODPRODUCTO").Value & ","
          End If
          If IsNull(rstProducto("FR28DOSIS").Value) Then
            strinsert = strinsert & "1,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28DOSIS").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FRG4CODFRECUENCIA").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA").Value & "',"
          End If
          If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
          End If
          If IsNull(rstProducto("FR28VOLUMEN").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28VOLUMEN").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR34CODVIA").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
          End If
          If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
          End If
          If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
            strinsert = strinsert & "NULL,"
          Else
            strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
          End If
          If IsNull(rstProducto("FR28OPERACION").Value) Then
            strinsert = strinsert & "NULL,to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
          Else
            strinsert = strinsert & "'" & rstProducto("FR28OPERACION").Value & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
          End If
          If IsNull(rstProducto("FR28TIEMINFMIN").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28TIEMINFMIN").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR28INDDISPPRN").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDDISPPRN").Value & ","
          End If
          If IsNull(rstProducto("FR28INDCOMIENINMED").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDCOMIENINMED").Value & ","
          End If
          If IsNull(rstProducto("FRH5CODPERIODICIDAD").Value) Then
            strinsert = strinsert & "'Diario',"
          Else
            strinsert = strinsert & "'" & rstProducto("FRH5CODPERIODICIDAD").Value & "',"
          End If
          If IsNull(rstProducto("FR28INDBLOQUEADA").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDBLOQUEADA").Value & ","
          End If
          If IsNull(rstProducto("FR28FECBLOQUEO").Value) Then
            strinsert = strinsert & "null,"
          Else
            If Len(rstProducto("FR28FECBLOQUEO").Value) < 11 Then
              strinsert = strinsert & "to_date('" & rstProducto("FR28FECBLOQUEO").Value & "','dd/mm/yyyy'),"
            Else
              strinsert = strinsert & "to_date('" & rstProducto("FR28FECBLOQUEO").Value & "','dd/mm/yyyy hh24:mi:ss'),"
            End If
          End If
          If IsNull(rstProducto("SG02CODPERSBLOQ").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("SG02CODPERSBLOQ").Value & "',"
          End If
          If IsNull(rstProducto("FR28INDSN").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDSN").Value & ","
          End If
          If IsNull(rstProducto("FR28INDESTOM").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDESTOM").Value & ","
          End If
          If IsNull(rstProducto("FR28CANTIDAD").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28CANTIDAD").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR28FECFIN").Value) Then
            strinsert = strinsert & "null,"
          Else
            If Len(rstProducto("FR28FECFIN").Value) < 11 Then
              strinsert = strinsert & "to_date('" & rstProducto("FR28FECFIN").Value & "','dd/mm/yyyy'),"
            Else
              strinsert = strinsert & "to_date('" & rstProducto("FR28FECFIN").Value & "','dd/mm/yyyy hh24:mi:ss'),"
            End If
          End If
          If IsNull(rstProducto("FR28HORAFIN").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28HORAFIN").Value & ","
          End If
          If IsNull(rstProducto("FR28CANTIDADDIL").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28CANTIDADDIL").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR28TIEMMININF").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28TIEMMININF").Value & ","
          End If
          If IsNull(rstProducto("FR28INDVIAOPC").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDVIAOPC").Value & ","
          End If
          If IsNull(rstProducto("FR28REFERENCIA").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FR28REFERENCIA").Value & "',"
          End If
          If IsNull(rstProducto("FR28CANTDISP").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28CANTDISP").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR28UBICACION").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FR28UBICACION").Value & "',"
          End If
          If IsNull(rstProducto("FR28INDPERF").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR28INDPERF").Value & ","
          End If
          If IsNull(rstProducto("FR28INSTRADMIN").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FR28INSTRADMIN").Value & "',"
          End If
          If IsNull(rstProducto("FR28VELPERFUSION").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28VELPERFUSION").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR28VOLTOTAL").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28VOLTOTAL").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR73CODPRODUCTO_2").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & rstProducto("FR73CODPRODUCTO_2").Value & ","
          End If
          If IsNull(rstProducto("FRH7CODFORMFAR_2").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR_2").Value & "',"
          End If
          If IsNull(rstProducto("FR28DOSIS_2").Value) Then
            strinsert = strinsert & "null,"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR28DOSIS_2").Value, ",", ".", 1)
            strinsert = strinsert & vntAux & ","
          End If
          If IsNull(rstProducto("FR93CODUNIMEDIDA_2").Value) Then
            strinsert = strinsert & "null,"
          Else
            strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_2").Value & "',"
          End If
          If IsNull(rstProducto("FR28DESPRODUCTO").Value) Then
            strinsert = strinsert & "null)"
          Else
            strinsert = strinsert & "'" & rstProducto("FR28DESPRODUCTO").Value & "')"
          End If
          rstProducto.Close
          Set rstProducto = Nothing
          
          Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
          qryInsert.Execute
          qryInsert.Close
          Set qryInsert = Nothing
      
          'objApp.rdoConnect.Execute strinsert, 64
          'objApp.rdoConnect.Execute "Commit", 64
          '----------------------------------------------------------------------------------
          'para cada producto hay que mirar si es de uso restringido
          If gintprodbuscado(v, 2) <> "" Then
            strusorest = "SELECT FR73INDUSOREST FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                          gintprodbuscado(v, 2)
            Set rstausorest = objApp.rdoConnect.OpenResultset(strusorest)
            If rstausorest.rdoColumns(0).Value = -1 Then
              'si es de uso restringido se llama a la pantalla de restricciones,pero antes
              'se vuelca el contenido de FRA900 en FRA400
              strusorest = "SELECT * FROM FRA900 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 2)
              Set rstausorest = objApp.rdoConnect.OpenResultset(strusorest)
              While Not rstausorest.EOF
                strrest = "SELECT FRA9CODREST FROM FRA400 WHERE FR73CODPRODUCTO=" & _
                          gintprodbuscado(v, 2) & " AND " & _
                          "FR66CODPETICION=" & txtText1(0).Text & " AND " & _
                          "FRA9CODREST=" & rstausorest.rdoColumns("FRA9CODREST").Value
                Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
                If rstrest.EOF Then
                  strinsert = "INSERT INTO FRA400(FR73CODPRODUCTO,FR66CODPETICION," & _
                            "FRA9CODREST,FRA4DESCREST,FRA4INDSELECCIONADO) VALUES(" & _
                            gintprodbuscado(v, 2) & "," & _
                            txtText1(0).Text & "," & _
                            rstausorest.rdoColumns("FRA9CODREST").Value & "," & _
                            "'" & rstausorest.rdoColumns("FRA9DESREST").Value & "'" & "," & _
                            "0" & ")"
                  objApp.rdoConnect.Execute strinsert, 64
                  objApp.rdoConnect.Execute "Commit", 64
                End If
                rstrest.Close
                Set rstrest = Nothing
                rstausorest.MoveNext
              Wend
              rstausorest.Close
              Set rstausorest = Nothing
              glngcodpeticion = txtText1(0).Text
              glngcodprod = gintprodbuscado(v, 2)
              blnFormActRest = True
              Call objsecurity.LaunchProcess("FR0182")
              Me.Enabled = False
              blnFormActRest = False
            End If
          End If
          '---------------------------------------------------------------------------------
        End If 'If blnInsertar Then
      Next v
    End If
    gintprodtotal = 0

    'se calcula la edad
    strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & glngselpaciente
    Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
    If Not rstfecnac.EOF Then
      If Not IsNull(rstfecnac.rdoColumns(0).Value) Then
        edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
        Text1.Text = edad
      Else
        Text1.Text = ""
      End If
    Else
        Text1.Text = ""
    End If
    rstfecnac.Close
    Set rstfecnac = Nothing
    Text1.Locked = True
    ''''''''''''''''''''''''''''''''''''''
  End If 'If blnFormActRest = False Then

'  If ginteventloadRedOM = 0 And blnFormActRest = False Then
   If blnInload = True And blnFormActRest = False Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
    Me.Enabled = False
    Call presentacion_pantalla
    Me.Enabled = False
    blnDetalle = False
    grdDBGrid1(6).Columns("Medicamento").Position = 1
    grdDBGrid1(6).Columns("FF").Position = 2
    grdDBGrid1(6).Columns("Dosis").Position = 3
    grdDBGrid1(6).Columns("UM").Position = 4
    grdDBGrid1(6).Columns("Volum.").Position = 5
    grdDBGrid1(6).Columns("Cantidad").Position = 6
    grdDBGrid1(6).Columns("Frec.").Position = 7
    grdDBGrid1(6).Columns("V�a").Position = 8
    grdDBGrid1(6).Columns("Period.").Position = 9
    grdDBGrid1(6).Columns("Inicio").Position = 10
    grdDBGrid1(6).Columns("H.I.").Position = 11
    grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Position = 12
    grdDBGrid1(6).Columns("Vol.Dil.").Position = 13
    grdDBGrid1(6).Columns("T.Inf.(min)").Position = 14
    grdDBGrid1(6).Columns("Inst.Admin.").Position = 15
    grdDBGrid1(6).Columns("Fin").Position = 16
    grdDBGrid1(6).Columns("H.F.").Position = 17
    grdDBGrid1(6).Columns("Perfusi�n").Position = 18
    grdDBGrid1(6).Columns("Vol.Total").Position = 19
    grdDBGrid1(6).Columns("STAT").Position = 20
    grdDBGrid1(6).Columns("PERF").Position = 21
    grdDBGrid1(6).Columns("Medicamento 2").Position = 22
    grdDBGrid1(6).Columns("FF2").Position = 23
    grdDBGrid1(6).Columns("Dosis2").Position = 24
    grdDBGrid1(6).Columns("UM2").Position = 25
    grdDBGrid1(6).Columns("Medic.No Form.").Position = 26
    grdDBGrid1(6).Columns("Arch.Inf.F.M.").Position = 27
    grdDBGrid1(6).Columns("C�digo Medicamento").Visible = False
    grdDBGrid1(6).Columns("Medicamento").Width = 2235
    grdDBGrid1(6).Columns("Medic.No Form.").Width = 2235
    grdDBGrid1(6).Columns("Arch.Inf.F.M.").Width = 2235
    grdDBGrid1(6).Columns("FF").Width = 390
    grdDBGrid1(6).Columns("Dosis").Width = 945
    grdDBGrid1(6).Columns("UM").Width = 510
    grdDBGrid1(6).Columns("Cantidad").Width = 915
    grdDBGrid1(6).Columns("Frec.").Width = 929
    grdDBGrid1(6).Columns("V�a").Width = 600
    grdDBGrid1(6).Columns("Period.").Width = 1065
    grdDBGrid1(6).Columns("Inicio").Width = 1095
    grdDBGrid1(6).Columns("H.I.").Width = 540
    grdDBGrid1(6).Columns("C�digo Producto diluir").Visible = False
    grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Width = 2160
    grdDBGrid1(6).Columns("Vol.Dil.").Width = 825
    grdDBGrid1(6).Columns("Inst.Admin.").Width = 2500
    grdDBGrid1(6).Columns("C�digo Medicamento 2").Visible = False
    grdDBGrid1(6).Columns("Medicamento 2").Width = 1815
    grdDBGrid1(6).Columns("FF2").Width = 390
    grdDBGrid1(6).Columns("Dosis2").Width = 1289
    grdDBGrid1(6).Columns("UM2").Width = 645
    grdDBGrid1(6).Columns("Fin").Width = 1095
    grdDBGrid1(6).Columns("H.F.").Width = 540
    grdDBGrid1(6).Columns("Perfusi�n").Width = 1170
    grdDBGrid1(6).Columns("Vol.Total").Width = 1170
    grdDBGrid1(6).Columns("STAT").Width = 525
    grdDBGrid1(6).Columns("PERF").Width = 645
    grdDBGrid1(6).Columns("C�digo Petici�n").Visible = False
    grdDBGrid1(6).Columns("Num.Linea").Visible = False
    grdDBGrid1(6).Columns("Cambiar V�a?").Visible = False
    grdDBGrid1(6).Columns("Prn?").Visible = False
    grdDBGrid1(6).Columns("T.Inf.(min)").Width = 825
    grdDBGrid1(6).Columns("Operaci�n").Visible = False
    grdDBGrid1(6).Columns("Ubicaci�n").Visible = False
    grdDBGrid1(6).Columns("Referencia").Width = 1100
    grdDBGrid1(6).Columns("Referencia").Visible = False
    grdDBGrid1(6).Columns("Volum.").Width = 825
    grdDBGrid1(6).Columns("Cant.Dispens.").Visible = False
  End If

  'ginteventloadRedOM = 1
  Me.Enabled = True

  If tabTab1(1).Tab = 0 Then
    blnDetalle = True
  Else
    blnDetalle = False
  End If
End If

If blnInload = True Then
  blnInload = False
End If

If IralUltimoalCopiarLineas = True Then
  IralUltimoalCopiarLineas = True
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataMoveLast
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "OM"
    
    .strTable = "FR6600"
    .strWhere = "(FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    
    Call .FormAddOrderField("FR66CODPETICION", cwDescending)
    
    'If gblnSelPaciente = True Then
    '  .intAllowance = cwAllowModify + cwAllowDelete
    'End If
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "FR66FECVALIDACION", "Fecha Validaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR66FECCIERRE", "Fecha Cierre", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDALERGIA", "Alergia?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
  End With

  With objDetailInfo
    .strName = "Detalle OM"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(6)
    .strTable = "FR2800" 'Perfil FTP
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR28CANTIDAD", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR28DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "frg4codfrecuencia", "Frecuencia", cwString)
    Call .FormAddFilterWhere(strKey, "fr34codvia", "V�a", cwString)
    Call .FormAddFilterWhere(strKey, "frh5codperiodicidad", "Periodicidad", cwString)
    Call .FormAddFilterWhere(strKey, "fr28fecinicio", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "fr28horainicio", "Hora Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fr28fecfin", "Fecha Fin", cwDate)
    Call .FormAddFilterWhere(strKey, "fr28horafin", "Hora Fin", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_DIL", "Cod.Producto Diluy.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fr28indcomieninmed", "Comienzo Inmediato?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "fr28indsn", "S.N?", cwBoolean)
  End With

  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
  
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    'productos
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(35)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(31)).blnInFind = True
    .CtrlGetInfo(txtText1(38)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(37)).blnInFind = True
    .CtrlGetInfo(txtText1(41)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(txtText1(36)).blnInFind = True
    
    .CtrlGetInfo(Text1).blnNegotiated = False
    .CtrlGetInfo(txtText1(29)).blnNegotiated = False
    .CtrlGetInfo(txtHoras).blnNegotiated = False
    .CtrlGetInfo(txtMinutos).blnNegotiated = False
    .CtrlGetInfo(cboproducto).blnNegotiated = False
    .CtrlGetInfo(cboproductodil).blnNegotiated = False
    .CtrlGetInfo(cboproductomez).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(29)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(14)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(5)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(59)).blnReadOnly = True
    .CtrlGetInfo(txtText1(60)).blnReadOnly = True
    
    '.CtrlGetInfo(txtOperacion).blnNegotiated = False
     .CtrlGetInfo(Text1).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(3)).blnNegotiated = False
    .CtrlGetInfo(txtText1(3)).blnReadOnly = True
    .CtrlGetInfo(txtText1(4)).blnNegotiated = False
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
    .CtrlGetInfo(txtText1(5)).blnNegotiated = False
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    .CtrlGetInfo(txtText1(23)).blnNegotiated = False
    .CtrlGetInfo(txtText1(23)).blnReadOnly = True
    .CtrlGetInfo(txtText1(7)).blnNegotiated = False
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    .CtrlGetInfo(txtText1(6)).blnNegotiated = False
    .CtrlGetInfo(txtText1(6)).blnReadOnly = True
    .CtrlGetInfo(txtText1(19)).blnNegotiated = False
    .CtrlGetInfo(txtText1(19)).blnReadOnly = True
    .CtrlGetInfo(txtText1(21)).blnNegotiated = False
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    .CtrlGetInfo(txtText1(15)).blnNegotiated = False
    .CtrlGetInfo(txtText1(15)).blnReadOnly = True
    .CtrlGetInfo(txtText1(26)).blnNegotiated = False
    .CtrlGetInfo(txtText1(26)).blnReadOnly = True
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "CI22PRIAPEL")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "CI22SEGAPEL")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "CI22NUMHISTORIA")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(7), "CI30CODSEXO")
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(60)), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(60)), txtText1(46), "GCFN06(AD15CODCAMA)")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "CI30CODSEXO", "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "CI30DESSEXO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "SG02COD_FEN", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(19), "SG02APE1")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(21), "SG02NUMCOLEGIADO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "AD02DESDPTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    '**********************************************************************************
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(27)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(27)), txtText1(13), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(40)), "FR73CODPRODUCTO_DIL", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(40)), txtText1(41), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(52)), "FR73CODPRODUCTO_2", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(51), "FR73DESPRODUCTO")
    .CtrlGetInfo(txtText1(47)).blnReadOnly = True
    
    '.CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    '.CtrlGetInfo(txtText1(25)).blnForeign = True
    .CtrlGetInfo(txtText1(61)).blnForeign = True
        
    '.CtrlGetInfo(txtText1(27)).blnForeign = True
    '.CtrlGetInfo(txtText1(40)).blnForeign = True
    .CtrlGetInfo(txtText1(31)).blnForeign = True
    '.CtrlGetInfo(txtText1(81)).blnForeign = True
    '.CtrlGetInfo(txtText1(80)).blnForeign = True
    .CtrlGetInfo(txtText1(35)).blnForeign = True
    
    .CtrlGetInfo(txtText1(48)).blnForeign = True
    .CtrlGetInfo(txtText1(49)).blnForeign = True
     
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(txtText1(44)).blnReadOnly = True
    
    .CtrlGetInfo(dtcDateCombo1(3)).blnMandatory = True
    .CtrlGetInfo(txtText1(37)).blnMandatory = True
    .CtrlGetInfo(txtText1(38)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(35)).blnReadOnly = True 'FF
    
    .CtrlGetInfo(txtText1(59)).blnInGrid = False 'Proceso
    .CtrlGetInfo(txtText1(60)).blnInGrid = False 'Asistencia
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT FR34CODVIA,FR34DESVIA FROM FR3400 ORDER BY FR34CODVIA"
    
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT FRG4CODFRECUENCIA,FRG4DESFRECUENCIA FROM FRG400 ORDER BY FRG4CODFRECUENCIA"
    
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT FRH5CODPERIODICIDAD,FRH5DESPERIODICIDAD FROM FRH500 ORDER BY FRH5CODPERIODICIDAD"
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  'ginteventloadRedOM = 0
  Frame2.Visible = False
  mstrOperacion = "/"
  blnFormActRest = False
  blnObligatorio = False
  blnInload = True

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset

  intCancel = objWinInfo.WinExit
  If intCancel = 0 Then
    If IsNumeric(txtText1(0).Text) Then
        stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If rsta.EOF Then
            strDelete = "DELETE FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
            objApp.rdoConnect.Execute strDelete, 64
            objApp.rdoConnect.Execute "Commit", 64
        End If
        rsta.Close
        Set rsta = Nothing
    End If
  End If

End Sub

Private Sub Form_Unload(intCancel As Integer)
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset

If IsNumeric(txtText1(0).Text) Then
    stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then
        strDelete = "DELETE FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
    rsta.Close
    Set rsta = Nothing
End If
gintfirmarOM = 0
gblnActivateRedactarOM = False
Call objWinInfo.WinDeRegister
Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
Dim v As Integer

  If Index = 6 Then
  v = 37
  Select Case grdDBGrid1(6).Columns("Operaci�n").Value
  Case "/"  'Medicamento
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Medicamentos"
    Next i
  Case "M"  'Mezcla IV 2M
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "MezclaIV2M"
    Next i
  Case "P"  'PRN en OM
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "PRNenOM"
    Next i
  Case "F" 'Fluidoterapia
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Fluidoterapia"
    Next i
  'Case "E" 'Estupefaciente
  '  For i = 0 To v
  '    grdDBGrid1(6).Columns(i).CellStyleSet "Estupefacientes"
  '  Next i
  'Case "L" 'Form.Magistral
    'For i = 0 To v
    '  grdDBGrid1(6).Columns(i).CellStyleSet "FormMagistral"
    'Next i
  End Select
  End If
  
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR2200J" 'pacientes con cama ocupada

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"

     Set objField = .AddField("DESCCAMA")
     objField.strSmallDesc = "Cama"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<TRUNC(SYSDATE) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>SYSDATE))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(61)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<TRUNC(SYSDATE) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>SYSDATE))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(61), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
 
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(27), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(41)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = " WHERE FR73INDPRODREC=-1"
     .strWhere = " WHERE FR73INDINFIV=-1"
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
    
     Set objField = .AddField("FR73VOLUMEN")
     objField.strSmallDesc = "Volumen"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     'Set objField = .AddField("FR73VOLINFIV")
     'objField.strSmallDesc = "Volumen Infusi�n IV"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(40), .cllValues("FR73CODPRODUCTO"))
      'Call objWinInfo.CtrlSet(txtText1(42), .cllValues("FR73VOLINFIV"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(31)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(31), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(49)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(49), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Forma Farmace�tica" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
     
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(18), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(35)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(35), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(48)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(48), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If

End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

Me.Enabled = False

  If strFormName = "Detalle OM" Then
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
          objWinInfo.DataRefresh
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
          objWinInfo.DataRefresh
        End If
      End If
    End If
  Else
    If tabTab1(1).Tab <> 1 Then
      tabTab1(1).Tab = 1
      objWinInfo.DataMoveFirst
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    End If
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAdd Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowAdd
          'objWinInfo.DataRefresh
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
          'objWinInfo.DataRefresh
        End If
      End If
    End If
  End If

  If blnInload = False Then
    Me.Enabled = True
  End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'txtOperacion.Text = cboDBCombo1(4).Text
'On Error GoTo Error
  
  If strFormName = "Detalle OM" Then
  '  If (cboDBCombo1(1).Text = "IV") Or (cboDBCombo1(1).Text = "PC") Or (cboDBCombo1(1).Text = "PIN") Then
  '    'Hay que activar la velocidad de perfusi�n
  '    objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  '  Else
  '    'Hay que desactivar la velocidad de perfusi�n
  '    objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = True
  '  End If
    If chkCheck1(5).Value = 1 Then
      cboDBCombo1(1).Enabled = True
      objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = False
    Else
      'cboDBCombo1(1).Enabled = False
      'objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = True
    End If
  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  End If
  
  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If objWinInfo.intWinStatus = 1 Then
      If Not objWinInfo.objWinActiveForm.rdoCursor.EOF Then
        If objWinInfo.objWinActiveForm.rdoCursor("FR66INDOM").Value = 0 Then
        'If chkCheck1(12).Value = 0 Then
        chkOMPRN(6).Value = 1
        End If
      End If
    End If
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAdd Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowAdd
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
        End If
      End If
    End If
  
  Else
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
          objWinInfo.DataRefresh
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
          objWinInfo.DataRefresh
        End If
      End If
    End If
    
    If objWinInfo.intWinStatus = 1 Then
      If grdDBGrid1(6).Rows > 0 Then
        Select Case grdDBGrid1(6).Columns("Operaci�n").Value
        Case "/"  'Medicamento
          tab2.Tab = 0
        Case "M"  'Mezcla IV 2M
          tab2.Tab = 1
        Case "P"  'PRN en OM
          tab2.Tab = 2
        Case "F" 'Fluidoterapia
          tab2.Tab = 3
        'Case "E" 'Estupefaciente
        '  tab2.Tab = 4
        'Case "L" 'Form.Magistral
        '  tab2.Tab = 4
        End Select
        Call presentacion_pantalla
      End If
    End If
  End If

  If strFormName = "Detalle OM" Then
    'Campos Linkados
    Dim srtSQL As String
    Dim qrySQL As rdoQuery
    Dim rstSQL As rdoResultset
    
    txtText1(3).Text = ""
    txtText1(4).Text = ""
    txtText1(5).Text = ""
    txtText1(23).Text = ""
    txtText1(7).Text = ""
    If txtText1(2).Text <> "" Then
      srtSQL = "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(2).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("CI22NOMBRE").Value) Then
          txtText1(3).Text = rstSQL("CI22NOMBRE").Value
        End If
        If Not IsNull(rstSQL("CI22PRIAPEL").Value) Then
          txtText1(4).Text = rstSQL("CI22PRIAPEL").Value
        End If
        If Not IsNull(rstSQL("CI22SEGAPEL").Value) Then
          txtText1(5).Text = rstSQL("CI22SEGAPEL").Value
        End If
        If Not IsNull(rstSQL("CI22NUMHISTORIA").Value) Then
          txtText1(23).Text = rstSQL("CI22NUMHISTORIA").Value
        End If
        If Not IsNull(rstSQL("CI30CODSEXO").Value) Then
          txtText1(7).Text = rstSQL("CI30CODSEXO").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(6).Text = ""
    If txtText1(7).Text <> "" Then
      srtSQL = "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(7).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("CI30DESSEXO").Value) Then
          txtText1(6).Text = rstSQL("CI30DESSEXO").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(19).Text = ""
    txtText1(21).Text = ""
    If txtText1(24).Text <> "" Then
      srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(24).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("SG02APE1").Value) Then
          txtText1(19).Text = rstSQL("SG02APE1").Value
        End If
        If Not IsNull(rstSQL("SG02NUMCOLEGIADO").Value) Then
          txtText1(21).Text = rstSQL("SG02NUMCOLEGIADO").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(15).Text = ""
    If txtText1(14).Text <> "" Then
      srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(14).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("SG02APE1").Value) Then
          txtText1(15).Text = rstSQL("SG02APE1").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(26).Text = ""
    If txtText1(25).Text <> "" Then
      srtSQL = "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(25).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("FR26DESESTADOPET").Value) Then
          txtText1(26).Text = rstSQL("FR26DESESTADOPET").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
  End If

'Error:
'  Exit Sub

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
  If strFormName = "Detalle OM" And cboDBCombo1(3).Text = "Agenda" And blnNoVerAgen = False Then
    Call Periodicidad
    blnNoVerAgen = False
  End If
  intCambioAlgo = 0

End Sub


Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim strDelete As String

  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If txtText1(25).Text <> 1 Then
      blnCancel = True
      Call MsgBox("No se puede borrar una petici�n " & txtText1(26).Text, _
                   vbInformation, "Aviso")
      Exit Sub
    End If
    If txtText1(0).Text <> "" Then
      stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        blnCancel = True
        Call MsgBox("No se puede borrar una Petici�n hasta borrar todos sus Medicamentos.", _
                     vbInformation, "Aviso")
        rsta.Close
        Set rsta = Nothing
        Exit Sub
      Else
        strDelete = "DELETE FROM FRA400 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  Else 'Detalle OM
  End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim rsta As rdoResultset
  Dim stra As String
  Dim mensaje As String
  Dim dblHoraFin As Double
  Dim dblHoraInicio As Double
  Dim blnFechaFinMenor As Boolean
  Dim qryA As rdoQuery
  
  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If txtText1(2).Text <> "" Then
      stra = "SELECT CI21CODPERSONA FROM FR2200J " & _
             " WHERE CI21CODPERSONA=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
        mensaje = MsgBox("El paciente es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
      End If
      rsta.Close
      Set rsta = Nothing
      
      'si est� clicado Inter�s Cient�fico debe haber metido el Dpto. de Cargo
      If chkCheck1(15).Value = 1 Then
        If Trim(txtText1(61).Text) = "" Then
          mensaje = MsgBox("El Departamento de Cargo es obligatorio", vbInformation, "Aviso")
          blnCancel = True
        End If
      End If
      
      stra = "SELECT COUNT(*) " & _
             "  FROM FR2800 " & _
             " WHERE FR66CODPETICION = ? " & _
             "   AND (FR28FECFIN IS NOT NULL) " & _
             "   AND ((FR28FECINICIO > FR28FECFIN) " & _
             "        OR ((FR28FECINICIO = FR28FECFIN) AND ((FR28HORAFIN IS NULL) OR (ABS(FR28HORAFIN) > ABS(FR28HORAINICIO)))))"
      Set qryA = objApp.rdoConnect.CreateQuery("", stra)
      qryA(0) = txtText1(0).Text
      Set rsta = qryA.OpenResultset()
      If (rsta.rdoColumns(0).Value > 0) Then
        mensaje = MsgBox("Hay medicamentos con las fecha de fin anterior a la fecha de inicio", vbInformation, "Aviso")
        blnCancel = True
      End If
      qryA.Close
      Set qryA = Nothing
      Set rsta = Nothing
    End If
  Else 'Detalle
    txtText1(58).Text = txtText1(28).Text
    Select Case txtText1(53).Text 'Campos Obligatorios
    Case "/" 'Medicamento
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
          Exit Sub
          End If
        End If
    Case "M"  'Mezcla IV 2M
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(52).Text <> "" Then
        If txtText1(48).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(48).SetFocus
          Exit Sub
        End If
        If txtText1(50).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(50).SetFocus
          Exit Sub
        End If
        If txtText1(49).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(49).SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
          Exit Sub
          End If
        End If
    Case "P"  'PRN en OM
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(39).Text = "" Then
        mensaje = MsgBox("Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(39).SetFocus
        Exit Sub
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    Case "F"  'Fluidoterapia
      If txtText1(40).Text = "" Then
        mensaje = MsgBox("El campo Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtText1(42).Text = "" Then
        mensaje = MsgBox("El campo Volumen de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtHoras.Text = "" Then
        mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtHoras.SetFocus
        Exit Sub
      End If
      If txtMinutos.Text = "" Then
        mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtMinutos.SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(27).Text <> "" Then
        If txtText1(35).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(35).SetFocus
          Exit Sub
        End If
        If txtText1(30).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(30).SetFocus
          Exit Sub
        End If
        If txtText1(31).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(31).SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    Case "E" 'Estupefaciente
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Estupefaciente es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    Case "L"  'Form.Magistral
      If txtText1(55).Text = "" Then
        mensaje = MsgBox("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(57).Text = "" Then
        mensaje = MsgBox("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(57).SetFocus
        Exit Sub
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    End Select
    End If
  
  
  If strFormName = "Detalle OM" Then
   'Se comprueba que la fecha de inicio de la l�nea de detalle de la OM se menor
    'que la fecha de fin
    If (dtcDateCombo1(4).Text <> "") Then
      'Hay fecha de fin
      'If dtcDateCombo1(4).Date < dtcDateCombo1(3).Date Then
      blnFechaFinMenor = False
      Select Case DateDiff("yyyy", dtcDateCombo1(4).Date, dtcDateCombo1(3).Date)
        Case Is > 0
          blnFechaFinMenor = True
        Case Is < 0
          blnFechaFinMenor = False
        Case 0
          Select Case DateDiff("y", dtcDateCombo1(4).Date, dtcDateCombo1(3).Date)
            Case Is > 0
              blnFechaFinMenor = True
            Case Is < 0
              blnFechaFinMenor = False
            Case 0
              blnFechaFinMenor = False
          End Select
      End Select
      
      If blnFechaFinMenor Then
        mensaje = MsgBox("La fecha de fin no puede ser menor que la fecha de inicio", vbInformation, "Aviso")
        blnCancel = True
        'dtcDateCombo1(4).SetFocus
      Else
        If dtcDateCombo1(4).Date = dtcDateCombo1(3).Date Then
          If IsNumeric(txtText1(36).Text) Then
            'Hay hora de fin y hay que compararla con la hora de inicio
            If IsNumeric(txtText1(37).Text) Then
              dblHoraFin = txtText1(36).Text
              dblHoraInicio = txtText1(37).Text
              'If dblHoraFin <> 0 Then
                If dblHoraFin < dblHoraInicio Then
                  mensaje = MsgBox("La hora de Fin debe ser mayor que la hora de Inicio", vbInformation, "Aviso")
                  blnCancel = True
                  txtText1(37).SetFocus
                End If
              'End If
            Else
              mensaje = MsgBox("Debe introducir la hora de inicio", vbInformation, "Aviso")
              blnCancel = True
              txtText1(37).SetFocus
            End If
          End If
        End If
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  
  If Index = 1 Then
    If tabTab1(1).Tab = 0 Then
      blnDetalle = True
    Else
      blnDetalle = False
    End If
  End If
  
  If Index = 1 Then
    If objWinInfo.objWinActiveForm.strName <> "Detalle OM" Then
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    End If
    If grdDBGrid1(6).Rows > 0 Then
      If grdDBGrid1(6).Enabled = True Then
      Select Case grdDBGrid1(6).Columns("Operaci�n").Value
      Case "/"  'Medicamento
        tab2.Tab = 0
      Case "M"  'Mezcla IV 2M
        tab2.Tab = 1
      Case "P"  'PRN en OM
        tab2.Tab = 2
      Case "F" 'Fluidoterapia
        tab2.Tab = 3
      'Case "E" 'Estupefaciente
      '  tab2.Tab = 4
      'Case "L" 'Form.Magistral
      '  tab2.Tab = 4
      End Select
      End If
    End If
    Call presentacion_pantalla
  End If
    
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstfec As rdoResultset
Dim strfec As String
Dim hora As Variant
Dim auxProceso, auxAsistencia As Currency
Dim strpeso As String
Dim rstpeso As rdoResultset
Dim strad1500 As String
Dim rstad1500 As rdoResultset
Dim strProcAsis As String
Dim rstProcAsis As rdoResultset
Dim strpaciente As String
Dim rstpaciente As rdoResultset
Dim mensaje  As String
Dim stra As String
Dim strDelete As String


  If btnButton.Index = 30 Then 'Salir
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End If

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "OM" Then
    
    If gblnSelPaciente = True Then
      If gstrLlamador = "frmSelPaciente" Then
          glngselpaciente = frmSelPaciente.IdPersona1.Text
          auxProceso = frmSelPaciente.grdDBGrid1(1).Columns("Proceso").Value
          auxAsistencia = frmSelPaciente.grdDBGrid1(1).Columns("Asistencia").Value
      Else
          If gstrLlamador = "frmSelPacienteServEspeciales" Then
              glngselpaciente = frmSelPacienteServEspeciales.IdPersona1.Text
              auxProceso = frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Proceso").Value
              auxAsistencia = frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Asistencia").Value
          End If
      End If
    Else
      strProcAsis = "select ad1500.AD07CODPROCESO,ad1500.AD01CODASISTENCI "
      strProcAsis = strProcAsis & " From ad1500, ad0800, ad0100, ad0700 "
      strProcAsis = strProcAsis & " Where ad1500.AD07CODPROCESO = ad0800.AD07CODPROCESO "
      strProcAsis = strProcAsis & " and ad1500.AD07CODPROCESO=ad0700.AD07CODPROCESO"
      strProcAsis = strProcAsis & " and ad1500.AD01CODASISTENCI=ad0800.AD01CODASISTENCI"
      strProcAsis = strProcAsis & " and ad1500.AD01CODASISTENCI=ad0100.AD01CODASISTENCI"
      strProcAsis = strProcAsis & " and ad0700.ci21codpersona=" & glngselpaciente
      strProcAsis = strProcAsis & " and ad0100.ci21codpersona=" & glngselpaciente
      strProcAsis = strProcAsis & " and ad0800.AD34CODESTADO=1"
      'strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(ad0100.AD01FECINICIO,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(ad0100.AD01FECFIN,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      'strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(ad0700.AD07FECHORAINICI,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(ad0700.AD07FECHORAFIN,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TRUNC(AD0800.AD08FECINICIO) AND NVL(TRUNC(AD0800.AD08FECFIN),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      strProcAsis = strProcAsis & " ORDER BY AD0800.AD08FECINICIO DESC "
      
  
      Set rstProcAsis = objApp.rdoConnect.OpenResultset(strProcAsis)
      
      If rstProcAsis.EOF Then
        strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
        Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
        If rstpaciente.EOF Then
          mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
        Else
          mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                    rstpaciente("CI22PRIAPEL").Value & _
                    rstpaciente("CI22SEGAPEL").Value & _
                    " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                    Chr(13) & " No tiene asignado PROCESO-ASISTENCIA a CAMA." & _
                    Chr(13), vbCritical)
        End If
        rstpaciente.Close
        Set rstpaciente = Nothing
        Exit Sub
      Else
        If IsNull(rstProcAsis("AD07CODPROCESO").Value) Or IsNull(rstProcAsis("AD01CODASISTENCI").Value) Then
          strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
          Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
          If rstpaciente.EOF Then
            mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
          Else
            mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                      rstpaciente("CI22PRIAPEL").Value & _
                      rstpaciente("CI22SEGAPEL").Value & _
                      " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                      Chr(13) & " No tiene asignado PROCESO-ASISTENCIA a CAMA." & _
                      Chr(13), vbCritical)
          End If
          rstpaciente.Close
          Set rstpaciente = Nothing
          Exit Sub
        Else
          auxProceso = rstProcAsis("AD07CODPROCESO").Value
          auxAsistencia = rstProcAsis("AD01CODASISTENCI").Value
        End If
      End If
      rstProcAsis.Close
      Set rstProcAsis = Nothing
    End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    'por defecto se clica que sea Orden M�dica
    Call objWinInfo.CtrlSet(chkCheck1(12), 1)
    'fecha y hora de redacci�n
    strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(2), rstfec.rdoColumns(0).Value)
    strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = rstfec.rdoColumns(0).Value & ","
    strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = hora & rstfec.rdoColumns(0).Value
    Call objWinInfo.CtrlSet(txtText1(10), hora)
    rstfec.Close
    Set rstfec = Nothing
    Call objWinInfo.CtrlSet(txtText1(2), glngselpaciente)  'paciente
    Call objWinInfo.CtrlSet(txtText1(25), 1)  'estado REDACTADA
    Call objWinInfo.CtrlSet(txtText1(26), "REDACTADA")
    'se selecciona el servicio ----------------------------------
    strad1500 = "SELECT AD02CODDPTO FROM AD0500 WHERE " & _
              "AD01CODASISTENCI=" & auxAsistencia & _
              " AND AD07CODPROCESO=" & auxProceso
    Set rstad1500 = objApp.rdoConnect.OpenResultset(strad1500)
    If Not rstad1500.EOF Then
      'ad02coddpto = rstad1500.rdoColumns(0).Value  'servicio
      Call objWinInfo.CtrlSet(txtText1(16), rstad1500.rdoColumns(0).Value)    'servicio
    Else
      'si el paciente no tiene cama se mete como servicio el del m�dico que hace Login
      Dim strserviciomed As String
      Dim rstserviciomed As rdoResultset
      strserviciomed = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=" & _
                       "'" & objsecurity.strUser & "'" & " AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
      Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
      If Not rstserviciomed.EOF Then
        'ad02coddpto = rstserviciomed.rdoColumns(0).Value
        Call objWinInfo.CtrlSet(txtText1(16), rstserviciomed.rdoColumns(0).Value)    'servicio
      Else
        Call MsgBox("El m�dico " & objsecurity.strUser & " no est� asignado a ning�n servicio", vbInformation, "Aviso")
        'Exit Sub
      End If
      rstserviciomed.Close
      Set rstserviciomed = Nothing
    End If
    'Call objWinInfo.CtrlSet(txtText1(16), rstad1500.rdoColumns(0).Value)    'servicio
    rstad1500.Close
    Set rstad1500 = Nothing
    '------------------------------------------------------------
    txtText1(8).Text = objsecurity.strUser
    txtText1(59).Text = auxProceso
    txtText1(60).Text = auxAsistencia
    
    strpeso = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE " & _
              "AD01CODASISTENCI=" & auxAsistencia
    Set rstpeso = objApp.rdoConnect.OpenResultset(strpeso)
    If Not rstpeso.EOF Then
      If Not IsNull(rstpeso.rdoColumns(0).Value) Then
        txtText1(32).Text = rstpeso.rdoColumns(0).Value
      End If
      If Not IsNull(rstpeso.rdoColumns(1).Value) Then
        txtText1(33).Text = rstpeso.rdoColumns(1).Value
      End If
    End If
    rstpeso.Close
    Set rstpeso = Nothing
    
    'SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
      'If noformulario = False And tab2.Tab <> 4 Then
      If noformulario = False Then
        Exit Sub
      End If
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If
        txtText1(53).Text = mstrOperacion
        Select Case tab2.Tab
          Case 0
            mstrOperacion = "/" 'Medicamento
          Case 1
            mstrOperacion = "M" 'Mezcla IV 2M
          Case 2
            mstrOperacion = "P" 'PRN en OM
          Case 3
            mstrOperacion = "F" 'Fluidoterapia
          'Case 4
          '  mstrOperacion = "E" 'Estupefaciente
          'Case 4
          '  mstrOperacion = "L" 'Form.Magistral
        End Select
        txtText1(53).Text = mstrOperacion
        txtText1(45).Text = linea
        'SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      If btnButton.Index = 4 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
        If txtText1(27).Text <> "" Then
         'If Campos_Obligatorios = True Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
         'End If
        Else
          'If Campos_Obligatorios = True Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Exit Sub
          'End If
        End If
      Else
        If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
          blnDetalle = False
          Select Case btnButton.Index
          Case 21 'Primero
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 22 'Anterior
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 23 'Siguiente
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 24 'Ultimo
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case Else 'Otro boton
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          End Select
          If tabTab1(1).Tab = 0 Then
            blnDetalle = True
          Else
            blnDetalle = False
          End If
        Else
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    End If
  End If
  

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
      
    Select Case grdDBGrid1(6).Columns("Operaci�n").Value
    Case "/"  'Medicamento
      tab2.Tab = 0
    Case "M"  'Mezcla IV 2M
      tab2.Tab = 1
    Case "P"  'PRN en OM
      tab2.Tab = 2
    Case "F" 'Fluidoterapia
      tab2.Tab = 3
    'Case "E" 'Estupefaciente
    '  tab2.Tab = 4
    'Case "L" 'Form.Magistral
    '  tab2.Tab = 4
    End Select
    Call presentacion_pantalla

End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    If intIndex = 6 Then
      blnDetalle = False
    End If
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 6 Then
      Call presentacion_pantalla
      If tabTab1(1).Tab = 0 Then
        blnDetalle = True
      Else
        blnDetalle = False
      End If
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  Select Case intIndex
  Case 0
    If chkCheck1(0).Value = 0 Then
      chkCheck1(0).ForeColor = vbBlack
    Else
      chkCheck1(0).ForeColor = vbRed
    End If
  Case 2
    If chkCheck1(2).Value = 0 Then
      chkCheck1(2).ForeColor = vbBlack
    Else
      chkCheck1(2).ForeColor = vbRed
    End If
  Case 7
    If chkCheck1(7).Value = 0 Then
      chkCheck1(7).ForeColor = vbBlack
    Else
      chkCheck1(7).ForeColor = vbRed
    End If
  End Select
  
  Select Case intIndex
    Case 1:
        If chkCheck1(1).Value = 1 Then
            chkCheck1(3).Value = 0
            chkCheck1(4).Value = 0
            chkCheck1(3).Enabled = False
            chkCheck1(4).Enabled = False
        Else
            chkCheck1(3).Enabled = True
            chkCheck1(4).Enabled = True
        End If
    Case 5:
        If chkCheck1(5).Value = 1 Then
            cboDBCombo1(1).Enabled = True
            objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = False
        Else
            'cboDBCombo1(1).Enabled = False
            'objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = True
        End If
    Case 12:
      If chkCheck1(12).Value = 1 Then
        If chkOMPRN(6).Value <> 0 Then
          chkOMPRN(6).Value = 0
        End If
        If txtText1(1).Text <> "" Then
          Call objWinInfo.CtrlSet(txtText1(1), "")
        End If
      Else
        If chkOMPRN(6).Value <> 1 Then
          chkOMPRN(6).Value = 1
        End If
        If txtText1(1).Text = "" Then
          Call objWinInfo.CtrlSet(txtText1(1), txtText1(0))
        End If
      End If
    Case 15:
     If chkCheck1(15).Value = 1 Then
      txtText1(61).Visible = True
      txtText1(62).Visible = True
      lblLabel1(46).Visible = True
      txtText1(61).BackColor = &HFFFF00
      If IsNumeric(txtText1(16).Text) Then
        txtText1(61).Text = txtText1(16).Text
        txtText1(61).SetFocus
        SendKeys ("{TAB}")
      End If
     Else
      txtText1(61).Visible = False
      txtText1(62).Visible = False
      lblLabel1(46).Visible = False
      txtText1(61).Text = ""
      txtText1(62).Text = ""
      txtText1(61).BackColor = &HFFFFFF
     End If
  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  'If (cboDBCombo1(1).Text = "IV") Or (cboDBCombo1(1).Text = "PC") Or (cboDBCombo1(1).Text = "PIN") Then
  ' 'Hay que activar la velocidad de perfusi�n
  '  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  'Else
  ' 'Hay que desactivar la velocidad de perfusi�n
  '  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = True
  'End If
End Sub

Private Sub txtHoras_Change()

  strOrigen = "T"
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtHoras.Text > 999 Then
        Beep
        txtHoras.Text = 100
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          If txtHoras.Text > 999 Then
            Beep
            txtHoras.Text = 100
          End If
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtHoras_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
    intCambioAlgo = 1
    strOrigen = "T"
End Sub

Private Sub txtMinutos_Change()
  
  strOrigen = "T"
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtMinutos.Text > 59 Then
        Beep
        txtMinutos.Text = 59
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          If txtMinutos.Text > 59 Then
            Beep
            txtMinutos.Text = 59
          End If
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtMinutos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
    intCambioAlgo = 1
    strOrigen = "T"
End Sub


Private Sub txtText1_DblClick(Index As Integer)

  If objWinInfo.CtrlGetInfo(txtText1(Index)).blnForeign Then
    If stbStatusBar1.Panels(2).Text = "Lista" Then
      Call objWinInfo_cwForeign(objWinInfo.objWinActiveForm.strName, "txtText1(" & Index & ")")
    End If
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  intBotonMasInf = intIndex
  
  If intIndex = 51 Then
    If (tab2.Tab = 1) Or (tab2.Tab = 3) Then
      cmdMed2.Top = cmdbuscarprod.Top
      If txtText1(27).Text <> "" Then
        cmdMed2.Visible = True
        cmdbuscarprod.Visible = False
      Else
        cmdMed2.Visible = False
        cmdbuscarprod.Visible = True
      End If
      cmdSolDil.Visible = False
    End If
  ElseIf intIndex = 41 Then
    cmdSolDil.Top = cmdbuscarprod.Top
    If tab2.Tab <> 3 Then
      If txtText1(27).Text <> "" Then
        cmdSolDil.Visible = True
        cmdbuscarprod.Visible = False
      Else
        cmdSolDil.Visible = False
        cmdbuscarprod.Visible = True
      End If
    End If
    cmdMed2.Visible = False
    If tab2.Tab = 3 Then
      cmdSolDil.Caption = "Soluci�n Intravenosa"
      cmdSolDil.Visible = True
      cmdbuscarprod.Visible = False
    Else
      cmdSolDil.Caption = "Soluci�n para diluir"
    End If
  ElseIf intIndex = 55 Then
    If tab2.Tab <> 4 Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
    Else
      lblLabel1(16).Caption = "F�rmula Magistral"
    End If
  ElseIf intIndex = 57 Then
    cmdbuscarprod.Caption = "Form. Magistrales"
  Else
    'lblLabel1(16).Caption = "Medicamento"
    cmdMed2.Visible = False
    If tab2.Tab = 3 And txtText1(40).Text = "" Then
      cmdSolDil.Caption = "Soluci�n Intravenosa"
      cmdSolDil.Top = cmdbuscarprod.Top
      cmdSolDil.Visible = True
      cmdbuscarprod.Visible = False
    Else
      cmdSolDil.Visible = False
      cmdbuscarprod.Visible = True
    End If
  End If
  'If intIndex = 41 Then
  '  Call objWinInfo_cwForeign("Detalle OM", "txtText1(41)")
  'End If
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  If Index = 32 Then
    If IsNumeric(txtText1(32).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  'ALTURA
  If Index = 33 Then
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
    
    If IsNumeric(txtText1(33).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  If (Index = 82) Then
    strOrigen = "V"
  End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim rstDosis As rdoResultset
Dim strDosis As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
'Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
'Dim TiemMinInf As Currency
'Dim dblTiempo As Double


  Call objWinInfo.CtrlLostFocus
  If intIndex = 32 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  If intIndex = 33 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  
  If intIndex = 28 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(30), Format(txtText1(28).Text * rstDosis("fr73dosis").Value, "######0.00"))
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                Call objWinInfo.CtrlSet(txtText1(38), Format(txtText1(28).Text * rstDosis("fr73volumen").Value, "###0.00"))
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(42), Format(txtText1(28).Text * rstDosis("fr73volumen").Value, "###0.00"))
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  If intIndex = 30 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              If rstDosis("fr73dosis").Value <> 0 Then
                Call objWinInfo.CtrlSet(txtText1(28), Format(txtText1(30).Text / rstDosis("fr73dosis").Value, "##0.00"))
              End If
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), Format((txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value, "###0.00"))
                End If
              End If
            End If
        End If
      End If
    Else 'I.V.
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              'Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), Format((txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value, "###0.00"))
                End If
              End If
            End If
        End If
      End If
    End If
  End If

  If intIndex = 50 Then
    If mstrOperacion = "M" Then
      If Trim(txtText1(52).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(52).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(50).Text) And IsNull(rstDosis("fr73volumen").Value) = False Then
              If rstDosis("fr73dosis").Value <> 0 And IsNumeric(txtText1(50).Text) Then
                Call objWinInfo.CtrlSet(txtText1(47), (txtText1(50).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
              End If
            End If
        End If
      End If
    End If
  End If
  
  If intIndex = 42 Then
    If mstrOperacion <> "F" Then
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              If rstDosis("fr73volumen").Value <> 0 And IsNumeric(txtText1(42).Text) Then
                Call objWinInfo.CtrlSet(txtText1(28), Format(txtText1(42).Text / rstDosis("fr73volumen").Value, , "##0.00"))
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rstPotPeso As rdoResultset
Dim rstFactor As rdoResultset
Dim rstPotAlt As rdoResultset
Dim strPotPeso As String
Dim strPotAlt As String
Dim strFactor As String
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim dblTiempo As Double

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 43 Then
    If IsNumeric(txtText1(43).Text) Then
      If (intCambioAlgo = 0) And (strOrigen = "T") Then
        txtHoras.Text = txtText1(43).Text \ 60
        txtMinutos.Text = txtText1(43).Text Mod 60
      End If
    Else
        txtHoras.Text = ""
        txtMinutos.Text = ""
    End If
  End If
  
  If (intIndex = 82) And (strOrigen = "V") Then
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    'Se ha modificado la velocidad de perfusi�n
    If (VolTotal > 0) And IsNumeric(txtText1(82).Text) Then
      If txtText1(82).Text > 0 Then
        'Se puede calcular el tiempo Tiempo = VolTotal/ (VelPerf * 60)
        dblTiempo = (VolTotal / txtText1(82).Text) * 60
        txtHoras.Text = dblTiempo \ 60
        txtMinutos.Text = dblTiempo Mod 60
      End If
    Else
      'No hacer nada, de momento
    End If
  End If
  
  If intIndex = 27 Then
    If txtText1(27).Text = "999999999" Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
      txtText1(55).Top = 240
      txtText1(55).Left = 0
      txtText1(55).Visible = True
      cboproducto.Visible = False
    Else
      lblLabel1(16).Caption = "Medicamento"
      txtText1(55).Top = -1240
      txtText1(55).Left = 0
      cboproducto.Visible = True
      txtText1(55).Visible = False
      If txtText1(35).Visible = True And txtText1(35).Enabled = True Then
        txtText1(35).SetFocus
      End If
    End If
  End If
   
  'PESO
  If intIndex = 32 Then
    If Not IsNumeric(txtText1(32).Text) Then
        txtText1(32).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'ALTURA
  If intIndex = 33 Then
    If Not IsNumeric(txtText1(33).Text) Then
        txtText1(33).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'HORA (frame Medicamentos)
  If intIndex = 36 Then
    If Not IsNumeric(txtText1(36).Text) Then
        txtText1(36).Text = ""
    Else
        If txtText1(36).Text > 23 Then
            txtText1(36).Text = ""
                Call MsgBox("Hora fuera de rango. Para indicar las 24 horas adelante un dia la Fecha in y ponga en la Hora Fin un 0 (cero)", vbCritical, "Aviso")
        End If
    End If
  End If

  Select Case intIndex
  Case 38, 42, 47, 43, 44 ', 82
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    If txtText1(43).Text = "" Then
      TiemMinInf = 0
    Else
      TiemMinInf = CCur(txtText1(43).Text) / 60
    End If
    If TiemMinInf <> 0 Then
      VolPerf = Format(VolTotal / TiemMinInf, "0.00")
    Else
      VolPerf = 0
    End If
    If txtText1(44).Text <> "" Then
      If VolTotal <> CCur(txtText1(44)) Then
        txtText1(44).Text = Format(VolTotal, "###0.00")
      End If
    Else
      If VolTotal <> 0 Then
        txtText1(44).Text = Format(VolTotal, "###0.00")
      End If
    End If
    If txtText1(82).Text <> "" Then
      If VolPerf <> CCur(txtText1(82)) Then
        txtText1(82).Text = Format(VolPerf, "#####0.00")
      End If
    Else
      If VolPerf <> 0 Then
        txtText1(82).Text = Format(VolPerf, "#####0.00")
      End If
    End If
  End Select
  
  If intIndex = 13 Then
    cboproducto.Text = txtText1(13).Text
  End If
  If intIndex = 41 Then
    cboproductodil.Text = txtText1(41).Text
  End If
  If intIndex = 51 Then
    cboproductomez.Text = txtText1(51).Text
  End If
  
'Campos Linkados
Dim srtSQL As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Select Case intIndex
Case 2
  txtText1(3).Text = ""
  txtText1(4).Text = ""
  txtText1(5).Text = ""
  txtText1(23).Text = ""
  txtText1(7).Text = ""
  If txtText1(2).Text <> "" Then
    srtSQL = "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(2).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("CI22NOMBRE").Value) Then
        txtText1(3).Text = rstSQL("CI22NOMBRE").Value
      End If
      If Not IsNull(rstSQL("CI22PRIAPEL").Value) Then
        txtText1(4).Text = rstSQL("CI22PRIAPEL").Value
      End If
      If Not IsNull(rstSQL("CI22SEGAPEL").Value) Then
        txtText1(5).Text = rstSQL("CI22SEGAPEL").Value
      End If
      If Not IsNull(rstSQL("CI22NUMHISTORIA").Value) Then
        txtText1(23).Text = rstSQL("CI22NUMHISTORIA").Value
      End If
      If Not IsNull(rstSQL("CI30CODSEXO").Value) Then
        txtText1(7).Text = rstSQL("CI30CODSEXO").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 7
  txtText1(6).Text = ""
  If txtText1(7).Text <> "" Then
    srtSQL = "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(7).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("CI30DESSEXO").Value) Then
        txtText1(6).Text = rstSQL("CI30DESSEXO").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 24
  txtText1(19).Text = ""
  txtText1(21).Text = ""
  If txtText1(24).Text <> "" Then
    srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(24).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("SG02APE1").Value) Then
        txtText1(19).Text = rstSQL("SG02APE1").Value
      End If
      If Not IsNull(rstSQL("SG02NUMCOLEGIADO").Value) Then
        txtText1(21).Text = rstSQL("SG02NUMCOLEGIADO").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 14
  txtText1(15).Text = ""
  If txtText1(14).Text <> "" Then
    srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(14).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("SG02APE1").Value) Then
        txtText1(15).Text = rstSQL("SG02APE1").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 25
  txtText1(26).Text = ""
  If txtText1(25).Text <> "" Then
    srtSQL = "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(25).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("FR26DESESTADOPET").Value) Then
        txtText1(26).Text = rstSQL("FR26DESESTADOPET").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
End Select

End Sub

Private Sub presentacion_pantalla()

  Select Case tab2.Tab
  Case 0, 2
    lblLabel1(16).Top = 0 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 240
    cboproducto.Top = 240
    'txtText1(13).Visible = True
    cboproducto.Visible = True
    lblLabel1(37).Top = 0
    lblLabel1(37).Visible = True
    txtText1(35).Top = 240
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 0 'dosis
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 240
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 0
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 240
    txtText1(31).Visible = True
    lblLabel1(49).Top = 0
    lblLabel1(49).Visible = True
    txtText1(38).Top = 240
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 600
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 600
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 840
    txtText1(37).Visible = True
    
    lblLabel1(34).Top = 1200 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n para Diluir"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 1440
    cboproductodil.Top = 1440
    'txtText1(41).Visible = True
    cboproductodil.Visible = True
    lblLabel1(35).Top = 1200
    lblLabel1(35).Visible = True
    txtText1(42).Top = 1440
    txtText1(42).Visible = True
    lblLabel1(36).Top = 1200
    lblLabel1(36).Visible = True
    txtHoras.Top = 1440
    txtHoras.Visible = True
    txtMinutos.Top = 1440
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 1800 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2040
    txtText1(39).Visible = True
    lblLabel1(52).Top = 1800
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2040
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 1800
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2040
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2080
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2040
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 1800
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2040
    txtText1(44).Visible = True
    
    lblLabel1(31).Left = 5400
    lblLabel1(31).Top = 2400 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    dtcDateCombo1(4).Left = 5400
    dtcDateCombo1(4).Top = 2640
    dtcDateCombo1(4).Visible = True
    lblLabel1(30).Left = 7320
    lblLabel1(30).Top = 2400
    lblLabel1(30).Visible = True
    txtText1(36).Left = 7320
    txtText1(36).Top = 2640
    txtText1(36).Visible = True
    
    lblLabel1(79).Top = 3000 'Linea Prod2....
    'lblLabel1(79).Visible = False
    'txtText1(52).Top = 3240
    'txtText1(52).Visible = False
    lblLabel1(83).Top = 3000
    lblLabel1(83).Visible = False
    txtText1(51).Top = 3240
    cboproductomez.Top = 3240
    txtText1(51).Visible = False
    cboproductomez.Visible = False
    lblLabel1(80).Top = 3000
    lblLabel1(80).Visible = False
    txtText1(48).Top = 3240
    txtText1(48).Visible = False
    lblLabel1(82).Top = 3000
    lblLabel1(82).Visible = False
    txtText1(50).Top = 3240
    txtText1(50).Visible = False
    lblLabel1(81).Top = 3000
    lblLabel1(81).Visible = False
    txtText1(49).Top = 3240
    txtText1(49).Visible = False
    lblLabel1(78).Top = 3000
    lblLabel1(78).Visible = False
    txtText1(47).Top = 3240
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 1280
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
    
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
      cboproducto.Visible = False
    Else
      txtText1(55).Visible = False
      cboproducto.Visible = True
    End If
    
  Case 3 'Fluidoterapia
    lblLabel1(16).Top = 1200 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 1440
    cboproducto.Top = 1440
    'txtText1(13).Visible = True
    cboproducto.Visible = True
    lblLabel1(37).Top = 1200
    lblLabel1(37).Visible = True
    txtText1(35).Top = 1440
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 1200 'dosis
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 1440
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 1200
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 1440
    txtText1(31).Visible = True
    lblLabel1(49).Top = 1200
    lblLabel1(49).Visible = True
    txtText1(38).Top = 1440
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 600
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 600
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 840
    txtText1(37).Visible = True
    
    lblLabel1(34).Top = 0 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n Intravenosa"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 240
    cboproductodil.Top = 240
    'txtText1(41).Visible = True
    cboproductodil.Visible = True
    lblLabel1(35).Top = 0
    lblLabel1(35).Visible = True
    txtText1(42).Top = 240
    txtText1(42).Visible = True
    lblLabel1(36).Top = 0
    lblLabel1(36).Visible = True
    txtHoras.Top = 240
    txtHoras.Visible = True
    txtMinutos.Top = 240
    txtMinutos.Visible = True
    
    'lblLabel1(50).Top = 1800 'Linea Instr.Admin. y Vel.Perf....
    'lblLabel1(50).Visible = True
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    'txtText1(39).Top = 2040
    'txtText1(39).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True

    'lblLabel1(52).Top = 1800
    'lblLabel1(52).Visible = True
    lblLabel1(52).Top = 2400
    lblLabel1(52).Visible = True
    'txtText1(82).Top = 2040
    'txtText1(82).Visible = True
    txtText1(82).Top = 2640
    txtText1(82).Visible = True

    'lblLabel1(62).Top = 1800
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2040
    'txtText1(81).Visible = True
    'lblLabel1(77).Top = 2080
    'lblLabel1(77).Visible = True
    lblLabel1(77).Top = 2680
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2040
    'txtText1(80).Visible = True
    'lblLabel1(51).Top = 1800
    'lblLabel1(51).Visible = True
    lblLabel1(51).Top = 2400
    lblLabel1(51).Visible = True
    'txtText1(44).Top = 2040
    'txtText1(44).Visible = True
    txtText1(44).Top = 2640
    txtText1(44).Visible = True

    'lblLabel1(31).Left = 5400
    'lblLabel1(31).Top = 2400 'Linea Fecha fin...
    'lblLabel1(31).Visible = True
    lblLabel1(31).Left = 5400
    lblLabel1(31).Top = 3000 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    'dtcDateCombo1(4).Left = 5400
    'dtcDateCombo1(4).Top = 2640
    'dtcDateCombo1(4).Visible = True
    dtcDateCombo1(4).Left = 5400
    dtcDateCombo1(4).Top = 3240
    dtcDateCombo1(4).Visible = True

    'lblLabel1(30).Left = 7320
    'lblLabel1(30).Top = 2400
    'lblLabel1(30).Visible = True
    lblLabel1(30).Left = 7320
    lblLabel1(30).Top = 3000
    lblLabel1(30).Visible = True
    'txtText1(36).Left = 7320
    'txtText1(36).Top = 2640
    'txtText1(36).Visible = True
    txtText1(36).Left = 7320
    txtText1(36).Top = 3240
    txtText1(36).Visible = True

    lblLabel1(79).Top = 3000 'Linea Prod2....
    'lblLabel1(79).Visible = False
    'txtText1(52).Top = 3240
    'txtText1(52).Visible = False
    'lblLabel1(83).Top = 3000
    'lblLabel1(83).Visible = False
    lblLabel1(83).Top = 1800
    lblLabel1(83).Visible = True
    txtText1(51).Top = 2040
    cboproductomez.Top = 2040
    'txtText1(51).Visible = True
    cboproductomez.Visible = True

    'lblLabel1(80).Top = 3000
    'lblLabel1(80).Visible = False
    lblLabel1(80).Top = 1800
    lblLabel1(80).Visible = True
    'txtText1(48).Top = 3240
    'txtText1(48).Visible = False
    txtText1(48).Top = 2040
    txtText1(48).Visible = True

    'lblLabel1(82).Top = 3000
    'lblLabel1(82).Visible = False
    lblLabel1(82).Top = 1800
    lblLabel1(82).Visible = True
    'txtText1(50).Top = 3240
    'txtText1(50).Visible = False
    txtText1(50).Top = 2040
    txtText1(50).Visible = True

    'lblLabel1(81).Top = 3000
    'lblLabel1(81).Visible = False
    lblLabel1(81).Top = 1800
    lblLabel1(81).Visible = True
    'txtText1(49).Top = 3240
    'txtText1(49).Visible = False
    txtText1(49).Top = 2040
    txtText1(49).Visible = True

    'lblLabel1(78).Top = 3000
    'lblLabel1(78).Visible = False
    lblLabel1(78).Top = 1800
    lblLabel1(78).Visible = True
    'txtText1(47).Top = 3240
    'txtText1(47).Visible = False
    txtText1(47).Top = 2040
    txtText1(47).Visible = True

    chkCheck1(3).Top = 400
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 880
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
    
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
      cboproducto.Visible = False
    Else
      txtText1(55).Visible = False
      cboproducto.Visible = True
    End If
    
  Case 1 'Mezcla IV 2M
    lblLabel1(16).Top = 0 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 240
    cboproducto.Top = 240
    'txtText1(13).Visible = True
    cboproducto.Visible = True
    lblLabel1(37).Top = 0
    lblLabel1(37).Visible = True
    txtText1(35).Top = 240
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 0 'dosis
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 240
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 0
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 240
    txtText1(31).Visible = True
    lblLabel1(49).Top = 0
    lblLabel1(49).Visible = True
    txtText1(38).Top = 240
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 600
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 600
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 840
    txtText1(37).Visible = True
    
    lblLabel1(34).Top = 1200 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n para Diluir"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 1440
    cboproductodil.Top = 1440
    'txtText1(41).Visible = True
    cboproductodil.Visible = True
    lblLabel1(35).Top = 1200
    lblLabel1(35).Visible = True
    txtText1(42).Top = 1440
    txtText1(42).Visible = True
    lblLabel1(36).Top = 1200
    lblLabel1(36).Visible = True
    txtHoras.Top = 1440
    txtHoras.Visible = True
    txtMinutos.Top = 1440
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True
    lblLabel1(52).Top = 2400
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2640
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 2400
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2640
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2680
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2640
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 2400
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2640
    txtText1(44).Visible = True
    
    lblLabel1(31).Left = 5400
    lblLabel1(31).Top = 3000 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    dtcDateCombo1(4).Left = 5400
    dtcDateCombo1(4).Top = 3240
    dtcDateCombo1(4).Visible = True
    lblLabel1(30).Left = 7320
    lblLabel1(30).Top = 3000
    lblLabel1(30).Visible = True
    txtText1(36).Left = 7320
    txtText1(36).Top = 3240
    txtText1(36).Visible = True
    
    'lblLabel1(79).Top = 1800
    'lblLabel1(79).Visible = True
    'txtText1(52).Top = 2040
    'txtText1(52).Visible = True
    lblLabel1(83).Top = 1800 'Linea Prod2....
    lblLabel1(83).Visible = True
    txtText1(51).Top = 2040
    cboproductomez.Top = 2040
    'txtText1(51).Visible = True
    cboproductomez.Visible = True
    lblLabel1(80).Top = 1800
    lblLabel1(80).Visible = True
    txtText1(48).Top = 2040
    txtText1(48).Visible = True
    lblLabel1(82).Top = 1800
    lblLabel1(82).Visible = True
    txtText1(50).Top = 2040
    txtText1(50).Visible = True
    lblLabel1(81).Top = 1800
    lblLabel1(81).Visible = True
    txtText1(49).Top = 2040
    txtText1(49).Visible = True
    lblLabel1(78).Top = 1800
    lblLabel1(78).Visible = True
    txtText1(47).Top = 2040
    txtText1(47).Visible = True
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 1280
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
  
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
      cboproducto.Visible = False
    Else
      txtText1(55).Visible = False
      cboproducto.Visible = True
    End If
  
  Case 5 'Form.Magistral
'  Case 4 'Form.Magistral
'    lblLabel1(84).Left = 0
'    lblLabel1(84).Top = 1200
'    lblLabel1(84).Visible = True
'    txtText1(57).Left = 0
'    txtText1(57).Top = 1440
'    txtText1(57).Visible = True
'    cmdExplorador(1).Left = 4920
'    cmdExplorador(1).Top = 1240
'    cmdExplorador(1).Visible = True
'    cmdWord.Left = 5520
'    cmdWord.Top = 1240
'    cmdWord.Visible = True
'
'    'lblLabel1(16).Visible = False 'Linea Medicamento....
'    txtText1(13).Visible = False
'    cboproducto.Visible = False
'    lblLabel1(37).Visible = False
'    txtText1(35).Visible = False
'    lblLabel1(19).Top = 600
'    lblLabel1(19).Left = 0
'    lblLabel1(19).Visible = True 'dosis
'    txtText1(30).Top = 840
'    txtText1(30).Left = 0
'    txtText1(30).Visible = True
'    lblLabel1(21).Top = 600
'    lblLabel1(21).Left = 1000
'    lblLabel1(21).Visible = True
'    txtText1(31).Top = 840
'    txtText1(31).Left = 1000
'    txtText1(31).Visible = True
'    lblLabel1(49).Visible = False
'    txtText1(38).Visible = False
'
'    lblLabel1(17).Visible = False 'Linea Cantidad...
'    txtText1(28).Visible = False
'    lblLabel1(23).Left = 1960
'    lblLabel1(23).Visible = True
'    cboDBCombo1(0).Left = 1960
'    cboDBCombo1(0).Visible = True
'    lblLabel1(25).Left = 3760
'    lblLabel1(25).Visible = True
'    cboDBCombo1(1).Left = 3760
'    cboDBCombo1(1).Visible = True
'    lblLabel1(29).Left = 4600
'    lblLabel1(29).Visible = True
'    cboDBCombo1(3).Left = 4600
'    cboDBCombo1(3).Visible = True
'
'    lblLabel1(33).Left = 5520
'    lblLabel1(33).Top = 0 'Linea Fecha Inicio...
'    lblLabel1(33).Visible = True
'    dtcDateCombo1(3).Left = 5520
'    dtcDateCombo1(3).Top = 240
'    dtcDateCombo1(3).Visible = True
'    lblLabel1(32).Left = 7440
'    lblLabel1(32).Top = 0
'    lblLabel1(32).Visible = True
'    txtText1(37).Left = 7440
'    txtText1(37).Top = 240
'    txtText1(37).Visible = True
'
'    lblLabel1(34).Visible = False 'Linea Sol.Dil....
'    txtText1(41).Visible = False
'    cboproductodil.Visible = False
'    lblLabel1(35).Visible = False
'    txtText1(42).Visible = False
'    lblLabel1(36).Visible = False
'    txtHoras.Visible = False
'    txtMinutos.Visible = False
'
'    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
'    lblLabel1(50).Visible = True
'    txtText1(39).Top = 2640
'    txtText1(39).Visible = True
'    lblLabel1(52).Visible = False
'    txtText1(82).Visible = False
'    'lblLabel1(62).Visible = False
'    'txtText1(81).Visible = False
'    lblLabel1(77).Visible = False
'    'txtText1(80).Visible = False
'    lblLabel1(51).Visible = False
'    txtText1(44).Visible = False
'
'    lblLabel1(31).Left = 0
'    lblLabel1(31).Top = 1800 'Linea Fecha fin...
'    lblLabel1(31).Visible = True
'    dtcDateCombo1(4).Left = 0
'    dtcDateCombo1(4).Top = 2040
'    dtcDateCombo1(4).Visible = True
'    lblLabel1(30).Left = 1920
'    lblLabel1(30).Top = 1800
'    lblLabel1(30).Visible = True
'    txtText1(36).Left = 1920
'    txtText1(36).Top = 2040
'    txtText1(36).Visible = True
'
'    'lblLabel1(79).Top = 1800
'    'lblLabel1(79).Visible = False
'    'txtText1(52).Top = 2040
'    'txtText1(52).Visible = False
'    lblLabel1(83).Visible = False 'Linea Prod2....
'    txtText1(51).Visible = False
'    cboproductomez.Visible = False
'    lblLabel1(80).Visible = False
'    txtText1(48).Visible = False
'    lblLabel1(82).Visible = False
'    txtText1(50).Visible = False
'    lblLabel1(81).Visible = False
'    txtText1(49).Visible = False
'    lblLabel1(78).Visible = False
'    txtText1(47).Visible = False
'
'    chkCheck1(3).Top = 800
'    chkCheck1(3).Visible = True
'    chkCheck1(4).Visible = False
'
'    txtText1(55).Top = 240
'    txtText1(55).Left = 0
'    txtText1(55).Visible = True
'    lblLabel1(16).Top = 0
'    lblLabel1(16).Caption = "F�rmula Magistral"
  End Select

  Select Case tab2.Tab 'Campos Obligatorios
  Case 0
    txtText1(55).BackColor = txtText1(37).BackColor 'obligatorio
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 1
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 2
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = txtText1(37).BackColor
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 3
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = vbWhite
    txtText1(30).BackColor = vbWhite
    txtText1(31).BackColor = vbWhite
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = txtText1(37).BackColor
    txtHoras.BackColor = txtText1(37).BackColor
    txtMinutos.BackColor = txtText1(37).BackColor
    txtText1(57).BackColor = vbWhite
  'Case 4
  '  txtText1(55).BackColor = vbWhite
  '  txtText1(35).BackColor = txtText1(37).BackColor
  '  txtText1(30).BackColor = txtText1(37).BackColor
  '  txtText1(31).BackColor = txtText1(37).BackColor
  '  txtText1(28).BackColor = txtText1(37).BackColor
  '  cboDBCombo1(0).BackColor = txtText1(37).BackColor
  '  cboDBCombo1(1).BackColor = txtText1(37).BackColor
  '  cboDBCombo1(3).BackColor = txtText1(37).BackColor
  '  txtText1(39).BackColor = vbWhite
  '  txtText1(42).BackColor = vbWhite
  '  txtHoras.BackColor = vbWhite
  '  txtMinutos.BackColor = vbWhite
  '  txtText1(57).BackColor = vbWhite
'  Case 4
'    txtText1(55).BackColor = txtText1(37).BackColor
'    txtText1(35).BackColor = vbWhite
'    txtText1(30).BackColor = txtText1(37).BackColor
'    txtText1(31).BackColor = txtText1(37).BackColor
'    txtText1(28).BackColor = vbWhite
'    cboDBCombo1(0).BackColor = txtText1(37).BackColor
'    cboDBCombo1(1).BackColor = txtText1(37).BackColor
'    cboDBCombo1(3).BackColor = txtText1(37).BackColor
'    txtText1(39).BackColor = vbWhite
'    txtText1(42).BackColor = vbWhite
'    txtHoras.BackColor = vbWhite
'    txtMinutos.BackColor = vbWhite
'    txtText1(57).BackColor = txtText1(37).BackColor
  End Select

  Select Case tab2.Tab
  Case 0
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "/" 'Medicamento
    If txtText1(55).Visible = True Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
    Else
      lblLabel1(16).Caption = "Medicamento"
    End If
  Case 1
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = True
    mstrOperacion = "M" 'Mezcla IV 2M
    lblLabel1(16).Caption = "Medicamento"
  Case 2
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "P" 'PRN en OM
    lblLabel1(16).Caption = "Medicamento"
  Case 3
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "F" 'Fluidoterapia
    lblLabel1(34).Caption = "Electrolito"
    lblLabel1(16).Caption = "Electrolito 2"
  'Case 4
  '  cmdbuscarprod.Caption = "Estupefacientes"
  '  'cmdMed2.Visible = False
  '  mstrOperacion = "E" 'Estupefaciente
  '  lblLabel1(16).Caption = "Estupefaciente"
'  Case 4
'    cmdbuscarprod.Caption = "Form. Magistrales"
'    'cmdMed2.Visible = False
'    mstrOperacion = "L" 'Form.Magistral
'    lblLabel1(16).Caption = "F�rmula Magistral"
  End Select
  
  If tabTab1(1).Tab = 0 Then
    'botones habilitados
    If tab2.Tab = 0 Or tab2.Tab = 1 Or tab2.Tab = 2 Then
      cmdbuscarprod.Enabled = True
      cmdbuscarprot.Enabled = True
      cmdbuscargruprot.Enabled = True
      cmdbuscargruprod.Enabled = True
      cmdMedNoForm.Enabled = True
      cmdSolDil.Enabled = True
      cmdMed2.Enabled = True
      cmdInformacion.Enabled = True
      cmdInstAdm.Enabled = True
    Else
      cmdbuscarprod.Enabled = True
      cmdbuscarprot.Enabled = True
      cmdbuscargruprot.Enabled = False
      cmdbuscargruprod.Enabled = False
      cmdMedNoForm.Enabled = True
      cmdSolDil.Enabled = True
      cmdMed2.Enabled = True
      cmdInstAdm.Enabled = True
      If tab2.Tab <> 5 Then
        cmdInformacion.Enabled = True
      Else
        cmdInformacion.Enabled = False
      End If
    End If
  Else
    'botones deshabilitados
    cmdbuscarprod.Enabled = False
    cmdbuscarprot.Enabled = False
    cmdbuscargruprot.Enabled = False
    cmdbuscargruprod.Enabled = False
    cmdMedNoForm.Enabled = False
    cmdSolDil.Enabled = False
    cmdMed2.Enabled = False
    cmdInformacion.Enabled = False
    cmdInstAdm.Enabled = False
  End If

End Sub


Private Sub Agenda(strdias As String)
Dim intPos As Integer

  intNumDia = 1
  While strdias <> ""
    intPos = InStr(1, strdias, ",")
    If intPos <> 0 Then
      intDia(intNumDia) = Val(Left(strdias, intPos - 1))
      intNumDia = intNumDia + 1
    Else
      intDia(intNumDia) = Val(strdias)
      intNumDia = intNumDia + 1
    End If
    intPos = InStr(1, strdias, ",")
    If intPos <> 0 Then
      strdias = Right(strdias, Len(strdias) - intPos)
    Else
      strdias = ""
    End If
  Wend

End Sub

Private Sub insertar(intsuma As Integer, strCodPet As String, strNumLin As String, _
                     strFR73CODPRODUCTO As String, strFR28DOSIS As String, strFRG4CODFRECUENCIA As String, _
                     strFR93CODUNIMEDIDA As String, strFR34CODVIA As String, strFRH7CODFORMFAR As String, _
                     strFR73CODPRODUCTO_DIL As String, strFR28OPERACION As String, strFR28HORAINICIO As String, _
                     strFR28INDDISPPRN As String, strFR28INDCOMIENINMED As String, strFR28INDPERF As String, _
                     strFR28CANTIDAD As String, strFR28CANTIDADDIL As String, strFR28TIEMMININF As String, _
                     strFR28INDVIAOPC As String, strFR28REFERENCIA As String, strFR28UBICACION As String, _
                     strFR28VELPERFUSION As String, strFR28INSTRADMIN As String, strFR28VOLTOTAL As String, _
                     strFR73CODPRODUCTO_2 As String, strFRH7CODFORMFAR_2 As String, strFR28DOSIS_2 As String, _
                     strFR93CODUNIMEDIDA_2 As String, strFR28VOLUMEN_2 As String, strFR28DESPRODUCTO As String, _
                     strFR28PATHFORMAG As String, strFR28VOLUMEN As String)
  Dim strlinea As String
  Dim rstlinea As rdoResultset
  Dim strinsert As String
  Dim dtfecha As Date
  Dim linea As Integer
  Dim lineaactual As Integer
  Dim lineaaux As Integer
  Dim lineaact As Integer
  Dim strDelete As String
    
  lineaactual = strNumLin
  lineaaux = strNumLin
  lineaact = lineaactual
  
  While lineaact <= lineaaux
    strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
    strCodPet
    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
    
    If IsNull(rstlinea.rdoColumns(0).Value) Then
      linea = 1
    Else
      linea = rstlinea.rdoColumns(0).Value + 1
    End If
    dtfecha = DateAdd("d", intsuma, Date)

    strinsert = "INSERT INTO FR2800 ("
    strinsert = strinsert & "FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,"
    strinsert = strinsert & "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR34CODVIA,"
    strinsert = strinsert & "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,"
    strinsert = strinsert & "FR28FECINICIO,FR28HORAINICIO,"
    strinsert = strinsert & "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,"
    strinsert = strinsert & "FR28INDSN,FR28CANTIDAD,FR28CANTDISP,FR28FECFIN,FR28HORAFIN,"
    strinsert = strinsert & "FR28CANTIDADDIL,FR28TIEMMININF,FR28INDVIAOPC"
    strinsert = strinsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
    'strinsert = strinsert & ",FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
    strinsert = strinsert & ",FR28VOLUMEN,FR28INDPERF"
    strinsert = strinsert & ",FR28INSTRADMIN,FR28VOLTOTAL,FR73CODPRODUCTO_2"
    strinsert = strinsert & ",FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28VOLUMEN_2"
    strinsert = strinsert & ",FR28DESPRODUCTO,FR28PATHFORMAG"
    strinsert = strinsert & ") VALUES " & _
      "(" & _
      strCodPet & "," & _
      linea & ","
      strinsert = strinsert & strFR73CODPRODUCTO & ","
      vntAux = objGen.ReplaceStr(strFR28DOSIS, ",", ".", 1)
      strinsert = strinsert & vntAux & ","
      strinsert = strinsert & strFRG4CODFRECUENCIA & ","
      strinsert = strinsert & strFR93CODUNIMEDIDA & ","
      strinsert = strinsert & strFR34CODVIA & ","
      strinsert = strinsert & strFRH7CODFORMFAR & ","
      strinsert = strinsert & strFR73CODPRODUCTO_DIL & ","
      strinsert = strinsert & strFR28OPERACION & ",to_date('" & dtfecha & "','dd/mm/yyyy'),"
      strinsert = strinsert & strFR28HORAINICIO & ","
      strinsert = strinsert & strFR28INDDISPPRN & ","
      strinsert = strinsert & strFR28INDCOMIENINMED & ",'Diario',"
      strinsert = strinsert & strFR28INDPERF & ","
      If UCase(strFR28CANTIDAD) = "NULL" Then 'FR28CANTIDAD,FR28CANTDISP
        strinsert = strinsert & "null,null,"
      Else
        vntAux = objGen.ReplaceStr(strFR28CANTIDAD, ",", ".", 1)
        strinsert = strinsert & vntAux & "," & vntAux & ","
      End If
      strinsert = strinsert & "to_date('" & dtfecha + 1 & "','dd/mm/yyyy'),"
      strinsert = strinsert & "0,"
      strinsert = strinsert & strFR28CANTIDADDIL & ","
      strinsert = strinsert & strFR28TIEMMININF & ","
      strinsert = strinsert & strFR28INDVIAOPC & ","
      strinsert = strinsert & strFR28REFERENCIA & ","
      strinsert = strinsert & strFR28UBICACION & ","
      vntAux = objGen.ReplaceStr(strFR28VELPERFUSION, ",", ".", 1)
      strinsert = strinsert & vntAux & ","
      vntAux = objGen.ReplaceStr(strFR28VOLUMEN, ",", ".", 1)
      strinsert = strinsert & vntAux & ","
      strinsert = strinsert & strFR28INDPERF & ","
      strinsert = strinsert & strFR28INSTRADMIN & ","
      vntAux = objGen.ReplaceStr(strFR28VOLTOTAL, ",", ".", 1)
      strinsert = strinsert & vntAux & ","
      strinsert = strinsert & strFR73CODPRODUCTO_2 & ","
      strinsert = strinsert & strFRH7CODFORMFAR_2 & ","
      vntAux = objGen.ReplaceStr(strFR28DOSIS_2, ",", ".", 1)
      strinsert = strinsert & vntAux & ","
      strinsert = strinsert & strFR93CODUNIMEDIDA_2 & ","
      vntAux = objGen.ReplaceStr(strFR28VOLUMEN_2, ",", ".", 1)
      strinsert = strinsert & vntAux & ","
      strinsert = strinsert & strFR28DESPRODUCTO & ","
      strinsert = strinsert & strFR28PATHFORMAG & ")"
    
    
    objApp.rdoConnect.Execute strinsert, 64
    objApp.rdoConnect.Execute "Commit", 64
  
    rstlinea.Close
    Set rstlinea = Nothing
  
    lineaact = lineaact + 1
    If lineaaux <> lineaactual Then
      objWinInfo.DataMoveNext
    End If
  Wend
  
  If lineaactual = lineaaux Then
    strDelete = "delete fr2800 where FR66CODPETICION=" & strCodPet & _
      " AND FR28NUMLINEA=" & lineaactual
    objApp.rdoConnect.Execute strDelete, 64
    objApp.rdoConnect.Execute "Commit", 64
  Else
    While strNumLin <> lineaactual
      objWinInfo.DataMovePrevious
      strDelete = "delete fr2800 where FR66CODPETICION=" & strCodPet & _
        " AND FR28NUMLINEA=" & strNumLin
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
    Wend
  End If

End Sub

Private Sub Periodicidad()
Dim fecha As Date
Dim i As Integer
Dim dia As String
Dim mes As String
    
  Frame2.Visible = True
  Call Frame2.ZOrder(0)
  cmdsitcarro.Enabled = False
  cmdfirmar.Enabled = False
  cmdenviar.Enabled = False
  cmdbuscargruprot.Enabled = False
  cmdbuscarprot.Enabled = False
  cmdbuscargruprod.Enabled = False
  cmdbuscarprod.Enabled = False
  cmdperfil.Enabled = False
  cmdHCpaciente.Enabled = False
  cmdAlergia.Enabled = False
  fraFrame1(0).Enabled = False
  fraFrame1(1).Enabled = False
  tlbToolbar1.Enabled = False
    
  For i = 0 To 29
    fecha = DateAdd("d", i, Date)
    dia = DatePart("d", fecha)
    mes = DatePart("m", fecha)
    Label2(i).Caption = dia & "/" & mes
  Next i

End Sub

Private Function Peticion_Correcta() As Boolean
Dim strselect As String
Dim rstselect As rdoResultset
Dim strOperacion As String
Dim blnIncorrecta As Boolean
Dim blnFecFin As Boolean
Dim IntLinea As Integer
Dim mensaje As String
Dim strSQL As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
    
  If txtText1(0).Text = "" Then
    mensaje = MsgBox("No hay Orden M�dica para firmar.", vbInformation, "Aviso")
    Peticion_Correcta = False
    Exit Function
  End If
    
  IntLinea = 0
  blnIncorrecta = False
  strselect = "SELECT * FROM FR2800 "
  strselect = strselect & " WHERE FR66CODPETICION=" & txtText1(0).Text
  Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
  While Not rstselect.EOF And blnIncorrecta = False
    blnFecFin = False
    IntLinea = IntLinea + 1
    Select Case rstselect("FR28OPERACION").Value 'Campos Obligatorios
    Case "/" 'Medicamento
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "M" 'Mezcla IV 2M
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_2")) Then
        If IsNull(rstselect("FRH7CODFORMFAR_2")) Then
          blnIncorrecta = True  '("El campo Forma Farmace�tica del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28DOSIS_2")) Then
          blnIncorrecta = True  '("El campo Dosis del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR93CODUNIMEDIDA_2")) Then
          blnIncorrecta = True  '("El campo Unidad de Medida del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "P" 'PRN en OM
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FR28INSTRADMIN")) Then
        blnIncorrecta = True  '("Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso")
      End If
    Case "F" 'Fluidoterapia
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        blnIncorrecta = True  '("El campo Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDADDIL")) Then
        blnIncorrecta = True  '("El campo Volumen de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28TIEMMININF")) Then
        blnIncorrecta = True  '("El campo Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO")) Then
        If IsNull(rstselect("FRH7CODFORMFAR")) Then
          blnIncorrecta = True  '("El campo Forma Farmace�tica del Medicamento es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28DOSIS")) Then
          blnIncorrecta = True  '("El campo Dosis del Medicamento es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
          blnIncorrecta = True  '("El campo Unidad de Medida del Medicamento es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "E" 'Estupefaciente
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Estupefaciente es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "L" 'Form.Magistral
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DESPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28PATHFORMAG")) Then
        blnIncorrecta = True  '("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
      End If
    End Select
    
    If Not IsNull(rstselect("fr28fecfin").Value) Then
        strSQL = "SELECT TRUNC(SYSDATE)-TO_DATE(?,'DD/MM/YYYY') FROM DUAL"
        Set qrySQL = objApp.rdoConnect.CreateQuery("", strSQL)
        qrySQL(0) = Format(rstselect("fr28fecfin").Value, "dd/mm/yyyy")
        Set rstSQL = qrySQL.OpenResultset()
        If Not rstSQL.EOF Then
          If rstSQL.rdoColumns(0).Value > 0 Then
            blnIncorrecta = True
            blnFecFin = True
          End If
        End If
        qrySQL.Close
        Set rstSQL = Nothing
    End If
    
    rstselect.MoveNext
  Wend
  
  If IntLinea = 0 Then
    mensaje = MsgBox("La Orden M�dica no tiene Medicamentos.", vbCritical, "Error")
    Peticion_Correcta = False
  ElseIf blnIncorrecta = False Then
    Peticion_Correcta = True
  Else
    If blnFecFin = False Then
      mensaje = MsgBox("La linea : " & IntLinea & " de la Orden M�dica tiene campos obligatorios no informados.", vbCritical, "Error")
    Else
      mensaje = MsgBox("La Fecha Fin de la linea : " & IntLinea & " de la Orden M�dica es anterior a hoy.", vbCritical, "Error")
    End If
    Peticion_Correcta = False
  End If
  
  rstselect.Close
  Set rstselect = Nothing

End Function

Private Function Campos_Obligatorios() As Boolean
Dim blnIncorrecta As Boolean
Dim mensaje As String
Dim auxControl As Control
    
  mensaje = ""
  blnIncorrecta = False
  Select Case txtText1(53).Text 'Campos Obligatorios
  Case "/" 'Medicamento
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
  Case "M" 'Mezcla IV 2M
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
    If txtText1(52).Text <> "" Then
      If txtText1(48).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(48)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica del Medicamento 2 es obligatorio."
      End If
      If txtText1(50).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(50)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Dosis del Medicamento 2 es obligatorio."
      End If
      If txtText1(49).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(49)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Unidad de Medida del Medicamento 2 es obligatorio."
      End If
    End If
  Case "P" 'PRN en OM
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
    If txtText1(39).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(39)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n."
    End If
  Case "F" 'Fluidoterapia
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    If txtText1(40).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(42)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(42).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(42)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtHoras.Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtHoras
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtMinutos.Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtMinutos
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(27).Text <> "" Then
      If txtText1(35).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(35)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica del Medicamento es obligatorio."
      End If
      If txtText1(30).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(30)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Dosis del Medicamento es obligatorio."
      End If
      If txtText1(31).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(31)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Unidad de Medida del Medicamento es obligatorio."
      End If
    End If
  Case "E" 'Estupefaciente
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Estupefaciente es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
  Case "L" 'Form.Magistral
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    If txtText1(55).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(55)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Descripci�n de la F�rmula Magistral es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(57).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(57)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Archivo Inf. de la F�rmula Magistral es obligatorio."
    End If
  End Select
  
  If blnIncorrecta = False Then
    Campos_Obligatorios = False
  Else
    Campos_Obligatorios = True
    MsgBox mensaje, vbInformation, "Aviso"
    auxControl.SetFocus
  End If

End Function

Private Sub Nueva_OM(auxPaciente, auxProc, auxAsist, auxDpto, auxDoctor)
Dim sqlSeq As String
Dim rstSeq As rdoResultset
Dim gSPcodpeticion
Dim ad02coddpto
Dim sg02cod_med
Dim strfec As String
Dim rstfec As rdoResultset
Dim hora
Dim fr66horaredacci
Dim strpeso As String
Dim rstpeso As rdoResultset
Dim strfri600 As String
Dim rstfri600 As rdoResultset
Dim intAlergico As Integer
Dim strinsert As String
Dim strfecnac As String
Dim rstfecnac As rdoResultset
Dim edad
Dim qryInsert As rdoQuery

    glngselpaciente = auxPaciente
    
    sqlSeq = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
    Set rstSeq = objApp.rdoConnect.OpenResultset(sqlSeq)
    gSPcodpeticion = rstSeq.rdoColumns(0).Value
    rstSeq.Close
    Set rstSeq = Nothing
  
    If auxDpto = "" Then
      'si el paciente no tiene cama se mete como servicio el del m�dico que hace Login
      Dim strserviciomed As String
      Dim rstserviciomed As rdoResultset
      strserviciomed = "SELECT AD02CODDPTO FROM AD0500 WHERE  " & _
                "AD01CODASISTENCI=" & auxAsist & _
                " AND AD07CODPROCESO=" & auxProc
      Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
      If Not rstserviciomed.EOF Then
        ad02coddpto = rstserviciomed.rdoColumns(0).Value
      Else
        strserviciomed = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=" & _
                         "'" & objsecurity.strUser & "'" & " AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
        Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
        If Not rstserviciomed.EOF Then
          ad02coddpto = rstserviciomed.rdoColumns(0).Value
        Else
          Call MsgBox("El m�dico " & objsecurity.strUser & " no est� asignado a ning�n servicio", vbInformation, "Aviso")
        End If
      End If
      rstserviciomed.Close
      Set rstserviciomed = Nothing
    Else
      ad02coddpto = auxDpto
    End If
    If auxDoctor = "" Then
      sg02cod_med = objsecurity.strUser
    Else
      sg02cod_med = auxDoctor
    End If
  
    strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = rstfec.rdoColumns(0).Value & "."
    strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = hora & rstfec.rdoColumns(0).Value
    rstfec.Close
    Set rstfec = Nothing
    fr66horaredacci = hora
  
    strpeso = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE " & _
              "AD01CODASISTENCI=" & auxAsist
    Set rstpeso = objApp.rdoConnect.OpenResultset(strpeso)
    
    strfri600 = "select count(*) from fri600 where ci21codpersona=" & auxPaciente
    Set rstfri600 = objApp.rdoConnect.OpenResultset(strfri600)
    If rstfri600(0).Value > 0 Then
      intAlergico = 1
    Else
      intAlergico = 0
    End If
    rstfri600.Close
    Set rstfri600 = Nothing
  
    strinsert = "INSERT INTO FR6600 (FR66CODPETICION,SG02COD_MED,AD02CODDPTO,"
    strinsert = strinsert & "CI21CODPERSONA,FR66FECREDACCION,FR66HORAREDACCI,FR26CODESTPETIC,FR66INDOM,"
    strinsert = strinsert & "FR66PESOPAC,FR66ALTUPAC,FR66INDALERGIA,"
    strinsert = strinsert & "AD07CODPROCESO,AD01CODASISTENCI"
    strinsert = strinsert & ") VALUES "
    strinsert = strinsert & "("
    strinsert = strinsert & gSPcodpeticion & ","
    strinsert = strinsert & "'" & sg02cod_med & "'" & ","
    strinsert = strinsert & ad02coddpto & ","
    strinsert = strinsert & auxPaciente & ","
    strinsert = strinsert & "SYSDATE" & ","
    strinsert = strinsert & fr66horaredacci & ","
    strinsert = strinsert & "1,"
    strinsert = strinsert & "-1," 'indicador de OM
    If Not rstpeso.EOF Then
      If IsNull(rstpeso.rdoColumns(0).Value) Then
        strinsert = strinsert & "NULL" & ","
      Else
        strinsert = strinsert & objGen.ReplaceStr(rstpeso.rdoColumns(0).Value, ",", ".", 1) & ","
      End If
      If IsNull(rstpeso.rdoColumns(1).Value) Then
        strinsert = strinsert & "NULL"
      Else
        strinsert = strinsert & objGen.ReplaceStr(rstpeso.rdoColumns(1).Value, ",", ".", 1)
      End If
    Else
      strinsert = strinsert & "NULL,NULL"
    End If
    strinsert = strinsert & "," & intAlergico
    strinsert = strinsert & "," & auxProc
    strinsert = strinsert & "," & auxAsist
    strinsert = strinsert & ")"
    rstpeso.Close
    Set rstpeso = Nothing
  
    Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
    qryInsert.Execute
    qryInsert.Close
    Set qryInsert = Nothing
    'objApp.rdoConnect.Execute strinsert, 64
    'objApp.rdoConnect.Execute "Commit", 64
  
    'se calcula la edad
    strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & auxPaciente
    Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
    If Not rstfecnac.EOF Then
      If Not IsNull(rstfecnac.rdoColumns(0).Value) Then
        edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
        Text1.Text = edad
      Else
        Text1.Text = ""
      End If
    Else
        Text1.Text = ""
    End If
    rstfecnac.Close
    Set rstfecnac = Nothing
    
    Text1.Locked = True
  
    objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR (FR66INDOM=0 AND FR66CODOMORIPRN IS NOT NULL)) AND CI21CODPERSONA=" & auxPaciente _
             & " AND AD02CODDPTO=" & ad02coddpto & " AND FR66CODPETICION=" & gSPcodpeticion & _
             " AND (FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
             " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    Call objWinInfo.DataRefresh

End Sub

Private Sub cboproducto_Change()
Dim rsta As rdoResultset
Dim stra As String
cboproducto.RemoveAll
If Trim(cboproducto.Text) <> "" Then
  'stra = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE " & _
         " UPPER(FR73DESPRODUCTO) LIKE UPPER('" & Trim(cboproducto.Text) & "%')" & _
         " AND FR73INDVISIBLE=-1" & _
         " AND (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
         " AND (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL )" & _
         " AND (FR73INDPRN <> -1) OR ( FR73INDPRN IS NULL)"
 stra = "SELECT * FROM FR7300 WHERE "
 stra = stra & " UPPER(FR73DESPRODUCTO) LIKE UPPER('" & Trim(cboproducto.Text) & "%')"
 stra = stra & " AND FR73INDVISIBLE=-1"
 stra = stra & " AND (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL )"
 stra = stra & " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      
  If cmdbuscarprod.Caption = "Estupefacientes" Then
      stra = stra & " AND FR73INDESTUPEFACI=-1"
  End If
  
  stra = stra & " ORDER BY FR73DESPRODUCTO"
  
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    cboproducto.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & ";" & rsta.rdoColumns("FR73DESPRODUCTO").Value
  rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
End If
End Sub

Private Sub cboproducto_CloseUp()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strfec As String
Dim rstfec As rdoResultset
Dim blnIffiv As Boolean

If IsNumeric(cboproducto.Columns(0).Text) Then

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case mstrOperacion
  Case "/" 'Medicamento
      'gstrLlamadorProd = "Producto"
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "M" 'Mezcla IV 2M
      'gstrLlamadorProd = "Producto"
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "P" 'PRN en OM
      'gstrLlamadorProd = "Producto"
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      gblnEsPRN = True
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      gblnEsPRN = False
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "F" 'Fluidoterapia
      gstrLlamadorProd = "frmRedactarOMPRNFlui"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "E" 'Estupefaciente
      gstrLlamadorProd = "frmRedactarOMPRNEstu"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "L" 'Form.Magistral
      strfec = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'),TO_CHAR(SYSDATE,'HH24') FROM DUAL"
      Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
      Call objWinInfo.CtrlSet(dtcDateCombo1(3), rstfec.rdoColumns(0).Value)
      Call objWinInfo.CtrlSet(txtText1(37), rstfec.rdoColumns(1).Value)
      rstfec.Close
      Set rstfec = Nothing
      
      txtText1(57).SetFocus
      Exit Sub
  End Select

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If IsNumeric(cboproducto.Columns(0).Text) Then
        If gstrLlamadorProd <> "frmRedactarOMPRNFlui" Then
          For v = 0 To 0
            strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
            txtText1(0).Text
            Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
            If IsNull(rstlinea.rdoColumns(0).Value) Then
              linea = 1
            Else
              linea = rstlinea.rdoColumns(0).Value + 1
            End If
            If cboproducto.Columns(0).Text <> "" Then
              strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
              cboproducto.Columns(0).Text
              Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
              If IsNull(rstProducto("FR73INDINFIV").Value) Then
                blnIffiv = False
              Else
                If rstProducto("FR73INDINFIV").Value = -1 Then
                  blnIffiv = True
                Else
                  blnIffiv = False
                End If
              End If
              If blnIffiv = False Then
                strinsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                            "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR28VOLUMEN,FR28VOLTOTAL,FR34CODVIA," & _
                            "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                            "FR28HORAINICIO,FR28INDVIAOPC,FR28CANTIDADDIL,FR28CANTIDAD,FR28CANTDISP,FR28TIEMMININF"
                strinsert = strinsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
                ',FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
                strinsert = strinsert & ",FRH5CODPERIODICIDAD"
                strinsert = strinsert & ") VALUES " & _
                            "(" & _
                            txtText1(0).Text & "," & _
                            linea & "," & _
                            cboproducto.Columns(0).Text & ","
                If IsNull(rstProducto("FR73DOSIS").Value) Then
                  strinsert = strinsert & "1,"
                Else
                  If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                    If txtText1(32).Text = "" Then
                      Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                      gintprodtotal = 0
                      gstrLlamadorProd = ""
                      Exit Sub
                    Else
                      strinsert = strinsert & rstProducto("FR73DOSIS").Value * txtText1(32).Text & ","
                    End If
                  Else
                    If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                      If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                        Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                        gintprodtotal = 0
                        gstrLlamadorProd = ""
                        Exit Sub
                      Else
                        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                        intPeso = txtText1(32).Text
                        intAltura = txtText1(33).Text
                        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                        vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                        strinsert = strinsert & vntMasa & ","
                        rstPotPeso.Close
                        rstPotAlt.Close
                        rstFactor.Close
                        Set rstPotPeso = Nothing
                        Set rstPotAlt = Nothing
                        Set rstFactor = Nothing
                      End If
                    Else
                      vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                      strinsert = strinsert & vntAux & ","
                    End If
                  End If
                End If

                  If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                    strinsert = strinsert & "null,"
                  Else
                    strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
                  End If
                'End If
                If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
                End If
                If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL
                  strinsert = strinsert & "null,null,"
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & "," & vntAux & ","
                End If
                  If IsNull(rstProducto("FR34CODVIA").Value) Then
                    strinsert = strinsert & "null,"
                  Else
                    strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
                  End If
                'End If
                If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
                End If
                If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
                End If
                If IsNull(rstProducto("FR73INDESTUPEFACI").Value) Then
                  strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                Else
                  If rstProducto("FR73INDESTUPEFACI").Value = -1 Then
                    strinsert = strinsert & "'" & "E" & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                  Else
                    strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                  End If
                End If
                If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
                End If
                'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
                End If
                strinsert = strinsert & "1,1," 'FR28CANTIDAD,FR28CANTDISP
                'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73TIEMINF").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
                End If
                If IsNull(rstProducto("FR73REFERENCIA").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
                End If
                If IsNull(rstProducto("FRH9UBICACION").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
                End If
                If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                  strinsert = strinsert & "null,"
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                End If
                strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
              Else 'IV
                strinsert = "INSERT INTO FR2800"
                strinsert = strinsert & " (FR66CODPETICION,FR28NUMLINEA,"
                strinsert = strinsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
                strinsert = strinsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
                strinsert = strinsert & "FR28OPERACION,FR28FECINICIO,"
                strinsert = strinsert & "FR28HORAINICIO,"
                strinsert = strinsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
                strinsert = strinsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
                strinsert = strinsert & "FRH5CODPERIODICIDAD"
                strinsert = strinsert & ") VALUES " & _
                          "(" & _
                          txtText1(0).Text & "," & _
                          linea & "," & _
                          cboproducto.Columns(0).Text & ","
                strinsert = strinsert & "null," 'la U.M. es del medicamento
                If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & "," & vntAux & ","
                  VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
                Else
                  VolTotal = 0
                  strinsert = strinsert & "NULL," & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                  strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
                Else
                  strinsert = strinsert & "NULL,"
                End If
                strinsert = strinsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
                
                If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                  strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
                Else
                  strinsert = strinsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                  strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
                Else
                  strinsert = strinsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                  strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
                Else
                  strinsert = strinsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                Else
                  If txtText1(43).Text = "" Then
                    TiemMinInf = 0
                  Else
                    TiemMinInf = CCur(txtText1(43).Text) / 60
                  End If
                  If TiemMinInf <> 0 Then
                    VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                    vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                    strinsert = strinsert & vntAux & ","
                  Else
                    VolPerf = 0
                    strinsert = strinsert & VolPerf & ","
                  End If
                End If
                strinsert = strinsert & "'Diario')"
              End If

              objApp.rdoConnect.Execute strinsert, 64
              objApp.rdoConnect.Execute "Commit", 64
              
              If mstrOperacion = "P" Then
                strupdate = "UPDATE FR2800 SET FR28INDDISPPRN=-1"
                strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
                strupdate = strupdate & " AND FR28NUMLINEA=" & linea
                objApp.rdoConnect.Execute strupdate, 64
                objApp.rdoConnect.Execute "Commit", 64
              End If
              
              rstlinea.Close
              Set rstlinea = Nothing
              rstProducto.Close
              Set rstProducto = Nothing
            End If
          Next v
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))  'irene
          If mstrOperacion = "P" Then
            Call objWinInfo.CtrlDataChange
            txtText1(39).SetFocus
          End If
        Else
          '???????????????????????????-
          For v = 0 To gintprodtotal - 1
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      cboproducto.Columns(0).Text
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
            strupdate = "UPDATE FR2800 SET "
            strupdate = strupdate & "FR73CODPRODUCTO=" & cboproducto.Columns(0).Text
            If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strupdate = strupdate & ",FRH7CODFORMFAR='" & rstProducto("FRH7CODFORMFAR").Value & "'"
            End If
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strupdate = strupdate & ",FR28DOSIS=1"
            Else
              If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                If txtText1(32).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                  gintprodtotal = 0
                  Exit Sub
                Else
                  strupdate = strupdate & ",FR28DOSIS=" & rstProducto("FR73DOSIS").Value * txtText1(32).Text
                End If
              Else
                If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                  If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                    gintprodtotal = 0
                    Exit Sub
                  Else
                    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                    intPeso = txtText1(32).Text
                    intAltura = txtText1(33).Text
                    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                    vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                    strupdate = strupdate & ",FR28DOSIS=" & vntMasa
                    rstPotPeso.Close
                    rstPotAlt.Close
                    rstFactor.Close
                    Set rstPotPeso = Nothing
                    Set rstPotAlt = Nothing
                    Set rstFactor = Nothing
                  End If
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                  strupdate = strupdate & ",FR28DOSIS=" & vntAux
                End If
              End If
            End If
            
            If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strupdate = strupdate & ",FR93CODUNIMEDIDA='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
            End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL?
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strupdate = strupdate & ",FR28VOLUMEN=" & vntAux
            End If
            If Not IsNull(rstProducto("FR73REFERENCIA").Value) Then
              strupdate = strupdate & ",FR28REFERENCIA='" & rstProducto("FR73REFERENCIA").Value & "'"
            End If
            If Not IsNull(rstProducto("FRH9UBICACION").Value) Then
              strupdate = strupdate & ",FR28UBICACION='" & rstProducto("FRH9UBICACION").Value & "'"
            End If
            
            VolTotal = 0
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol1 = 0
            Else
              Vol1 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            If txtText1(42).Text = "" Then
              Vol2 = 0
            Else
              Vol2 = CCur(txtText1(42).Text)
            End If
            If txtText1(47).Text = "" Then
              Vol3 = 0
            Else
              Vol3 = CCur(txtText1(47).Text)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
            If txtText1(43).Text = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(txtText1(43).Text) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
            strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
          Next v
          Call objWinInfo.DataRefresh
        End If
      End If
      gintprodtotal = 0
    End If
  End If

gstrLlamadorProd = ""
If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
  txtText1(35).SetFocus
End If
Screen.MousePointer = vbHourglass
Screen.MousePointer = vbDefault
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
End If
End Sub


Private Sub cboproductodil_Change()
Dim rsta As rdoResultset
Dim stra As String
cboproductodil.RemoveAll
If Trim(cboproductodil.Text) <> "" Then
  'stra = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE " & _
         " UPPER(FR73DESPRODUCTO) LIKE UPPER('" & Trim(cboproductodil.Text) & "%')" & _
         " AND (FR73INDINFIV = -1 or FR73INDINFIV=0 or FR73INDINFIV is null)" & _
         " AND FR73INDVISIBLE=-1" & _
         " AND (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
         " AND (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL )" & _
         " AND FR73INDINFIV=-1" & _
         " AND (FR73INDPRN <> -1) OR ( FR73INDPRN IS NULL)"
  stra = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE " & _
         " UPPER(FR73DESPRODUCTO) LIKE UPPER('" & Trim(cboproductodil.Text) & "%')" & _
         " AND (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL )" & _
         " AND FR73INDINFIV=-1" & _
         " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))" & _
         " ORDER BY FR73DESPRODUCTO"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    cboproductodil.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & ";" & rsta.rdoColumns("FR73DESPRODUCTO").Value
  rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
End If
End Sub

Private Sub cboproductodil_CloseUp()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim strinsert As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

If IsNumeric(cboproductodil.Columns(0).Text) Then
If tab2.Tab <> 3 Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.objWinActiveForm.blnChanged = False
End If

If txtText1(0).Text <> "" Then
  If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
    mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
              "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
  Else
    If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
      gstrLlamadorProd = "frmRedactarOMPRNsolFlui"
    Else
      gstrLlamadorProd = "frmRedactarOMPRNsoldil"
      If (UCase(txtText1(35).Text) = "VI") Or (UCase(txtText1(35).Text) = "AMP") Or (UCase(txtText1(35).Text) = "SOL") Then
        'Se puede diluir
      Else
        'No se puede diluir
        MsgBox "Solo se pueden diluir las siguientes FF: VI, SOL y AMP", vbInformation, "Seleccionar Diluyente"
        cboproductodil.Text = ""
        Exit Sub
      End If
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    glngcodpeticion = txtText1(0).Text
    gstrLlamadorProd = ""
    If True Then
      For v = 0 To 0
        If cboproductodil.Columns(0).Text <> "" Then
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                    cboproductodil.Columns(0).Text
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
          If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
            strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
              txtText1(0).Text
            Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
            If IsNull(rstlinea.rdoColumns(0).Value) Then
              linea = 1
            Else
              linea = rstlinea.rdoColumns(0).Value + 1
            End If
            strinsert = "INSERT INTO FR2800"
            strinsert = strinsert & " (FR66CODPETICION,FR28NUMLINEA,"
            strinsert = strinsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
            strinsert = strinsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
            strinsert = strinsert & "FR28OPERACION,FR28FECINICIO,"
            strinsert = strinsert & "FR28HORAINICIO,"
            strinsert = strinsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
            strinsert = strinsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
            strinsert = strinsert & "FRH5CODPERIODICIDAD"
            strinsert = strinsert & ") VALUES " & _
                      "(" & _
                      txtText1(0).Text & "," & _
                      linea & "," & _
                      cboproductodil.Columns(0).Text & ","
            strinsert = strinsert & "null," 'la U.M. es del medicamento
            
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","
              VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
            Else
              VolTotal = 0
              strinsert = strinsert & "NULL," & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
              strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            strinsert = strinsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
            
            If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
              strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            Else
              If txtText1(43).Text = "" Then
                TiemMinInf = 0
              Else
                TiemMinInf = CCur(txtText1(43).Text) / 60
              End If
              If TiemMinInf <> 0 Then
                VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                VolPerf = 0
                strinsert = strinsert & VolPerf & ","
              End If
            End If
            strinsert = strinsert & "'Diario')"
            
            objApp.rdoConnect.Execute strinsert, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
            
          Else
            strupdate = "UPDATE FR2800 SET "
            strupdate = strupdate & "FR73CODPRODUCTO_DIL=" & cboproductodil.Columns(0).Text
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strupdate = strupdate & ",FR28CANTIDADDIL=" & vntAux
            End If
            If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
              strupdate = strupdate & ",FR28TIEMMININF=" & rstProducto("FR73TIEMINF").Value
            End If
            VolTotal = 0
            If txtText1(38).Text = "" Then
              Vol1 = 0
            Else
              Vol1 = CCur(txtText1(38).Text)
            End If
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol2 = 0
            Else
              Vol2 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            If txtText1(47).Text = "" Then
              Vol3 = 0
            Else
              Vol3 = CCur(txtText1(47).Text)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
            If txtText1(43).Text = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(txtText1(43).Text) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
            strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
            Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.DataRefresh
          End If
        End If
      Next v
    End If
    gintprodtotal = 0
  End If
End If
Screen.MousePointer = vbHourglass
Screen.MousePointer = vbDefault
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
End If
End Sub

Private Sub cboproductodil_GotFocus()
cmdSolDil.Top = cmdbuscarprod.Top
If tab2.Tab <> 3 Then
  If txtText1(27).Text <> "" Then
    cmdSolDil.Visible = True
    cmdbuscarprod.Visible = False
  Else
    cmdSolDil.Visible = False
    cmdbuscarprod.Visible = True
  End If
End If
cmdMed2.Visible = False
If tab2.Tab = 3 Then
  cmdSolDil.Caption = "Soluci�n Intravenosa"
  cmdSolDil.Visible = True
  cmdbuscarprod.Visible = False
Else
  cmdSolDil.Caption = "Soluci�n para diluir"
End If
End Sub
Private Sub cboproductomez_Change()
Dim rsta As rdoResultset
Dim stra As String
cboproductomez.RemoveAll
If Trim(cboproductomez.Text) <> "" Then
  'stra = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE " & _
         " UPPER(FR73DESPRODUCTO) LIKE UPPER('" & Trim(cboproductomez.Text) & "%')" & _
         " AND FR73INDVISIBLE=-1" & _
         " AND (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
         " AND (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL )" & _
         " AND (FR73INDPRN <> -1) OR ( FR73INDPRN IS NULL)"
  stra = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE " & _
         " UPPER(FR73DESPRODUCTO) LIKE UPPER('" & Trim(cboproductomez.Text) & "%')" & _
         " AND (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL )" & _
         " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))" & _
         " ORDER BY FR73DESPRODUCTO"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    cboproductomez.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & ";" & rsta.rdoColumns("FR73DESPRODUCTO").Value
  rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
End If
End Sub

Private Sub cboproductomez_CloseUp()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String

If IsNumeric(cboproductomez.Columns(0).Text) Then

Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.objWinActiveForm.blnChanged = False


If txtText1(0).Text <> "" Then
  If txtText1(25).Text <> 1 Then 'estado distinto a OM Redactada
    mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
              "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
  Else
    gstrLlamadorProd = "frmRedactarOMPRNmed2"
    glngcodpeticion = txtText1(0).Text
    gstrLlamadorProd = ""
    If True Then
      For v = 0 To 0
        If cboproductomez.Columns(0).Text <> "" Then
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                    cboproductomez.Columns(0).Text
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          
          strupdate = "UPDATE FR2800 SET "
          strupdate = strupdate & "FR73CODPRODUCTO_2=" & cboproductomez.Columns(0).Text
          If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
            strupdate = strupdate & ",FRH7CODFORMFAR_2='" & rstProducto("FRH7CODFORMFAR").Value & "'"
          End If
          If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
            strupdate = strupdate & ",FR28VOLUMEN_2='" & rstProducto("FR73VOLUMEN").Value & "'"
          End If
          If IsNull(rstProducto("FR73DOSIS").Value) Then
            strupdate = strupdate & ",FR28DOSIS_2=1"
          Else
            If rstProducto("FR73INDDOSISPESO").Value = -1 Then
              If txtText1(32).Text = "" Then
                Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                blnBoton = False
                gintprodtotal = 0
                Exit Sub
              Else
                strupdate = strupdate & ",FR28DOSIS_2=" & rstProducto("FR73DOSIS").Value * txtText1(32).Text
              End If
            Else
              If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                  blnBoton = False
                  gintprodtotal = 0
                  Exit Sub
                Else
                  strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                  Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                  strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                  Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                  strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                  Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                  intPeso = txtText1(32).Text
                  intAltura = txtText1(33).Text
                  vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                  vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                  vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                  vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                  strupdate = strupdate & ",FR28DOSIS_2=" & vntMasa
                  rstPotPeso.Close
                  rstPotAlt.Close
                  rstFactor.Close
                  Set rstPotPeso = Nothing
                  Set rstPotAlt = Nothing
                  Set rstFactor = Nothing
                End If
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                strupdate = strupdate & ",FR28DOSIS_2=" & vntAux
              End If
            End If
          End If

          If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
            strupdate = strupdate & ",FR93CODUNIMEDIDA_2='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
          End If
          VolTotal = 0
          If txtText1(38).Text = "" Then
            Vol1 = 0
          Else
            Vol1 = CCur(txtText1(38).Text)
          End If
          If txtText1(42).Text = "" Then
            Vol2 = 0
          Else
            Vol2 = CCur(txtText1(42).Text)
          End If
          If IsNull(rstProducto("FR73VOLUMEN").Value) Then
            Vol3 = 0
          Else
            Vol3 = CCur(rstProducto("FR73VOLUMEN").Value)
          End If
          VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
          vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
          strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
          If txtText1(43).Text = "" Then
            TiemMinInf = 0
          Else
            TiemMinInf = CCur(txtText1(43).Text) / 60
          End If
          If TiemMinInf <> 0 Then
            VolPerf = Format(VolTotal / TiemMinInf, "0.00")
          Else
            VolPerf = 0
          End If
          vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
          strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
          
          strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
          strupdate = strupdate & " AND FR28NUMLINEA=" & txtText1(45).Text
          If IsNumeric(txtText1(45).Text) Then
            objApp.rdoConnect.Execute strupdate, 64
          End If
          rstProducto.Close
          Set rstProducto = Nothing
          Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
          objWinInfo.objWinActiveForm.blnChanged = False
          Call objWinInfo.DataRefresh

        End If
      Next v
    End If
    gintprodtotal = 0
  End If
End If
Screen.MousePointer = vbHourglass
Screen.MousePointer = vbDefault
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
End If
End Sub
Private Sub cboproductomez_GotFocus()
  
If (tab2.Tab = 1) Or (tab2.Tab = 3) Then
  cmdMed2.Top = cmdbuscarprod.Top
  If txtText1(27).Text <> "" Then
    cmdMed2.Visible = True
    cmdbuscarprod.Visible = False
  Else
    cmdMed2.Visible = False
    cmdbuscarprod.Visible = True
  End If
  cmdSolDil.Visible = False
End If
End Sub

