VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmDefIntMedica 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definir Interacciones Medicamentosas"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.TextBox txttiempo 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFF00&
      Height          =   330
      Left            =   240
      MaxLength       =   9
      TabIndex        =   13
      ToolTipText     =   "Tiempo (en Horas)"
      Top             =   4080
      Width           =   1100
   End
   Begin VB.TextBox txtdesc 
      BackColor       =   &H00FFFF00&
      Height          =   615
      Left            =   4320
      MaxLength       =   2000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   15
      ToolTipText     =   "Descripci�n"
      Top             =   4080
      Width           =   7455
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Interacciones Medicamentosas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   1
      Left            =   120
      TabIndex        =   16
      Tag             =   "Actuaciones Asociadas"
      Top             =   4800
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2775
         Index           =   1
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20055
         _ExtentY        =   4895
         _StockProps     =   79
         Caption         =   "INTERACCIONES MEDICAMENTOSAS"
      End
   End
   Begin VB.CommandButton cmdinteracciones 
      Caption         =   "Interacciones"
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5520
      TabIndex        =   6
      Top             =   1800
      Width           =   855
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Principios Activos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   12
      Left            =   9480
      TabIndex        =   12
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   2295
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2775
         Index           =   12
         Left            =   120
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   360
         Width           =   2010
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   3545
         _ExtentY        =   4895
         _StockProps     =   79
         Caption         =   "PRINCIPIOS ACTIVOS"
      End
   End
   Begin VB.Frame frame1 
      Caption         =   "Principios Destino"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   13
      Left            =   6480
      TabIndex        =   7
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   2295
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   2775
         Index           =   13
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   2055
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   2
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   2
         Columns(0).Width=   1852
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   6218
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         _ExtentX        =   3625
         _ExtentY        =   4895
         _StockProps     =   79
         Caption         =   "PRINCIPIOS DESTINO"
      End
   End
   Begin VB.CommandButton cmdrellenar1 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   8880
      TabIndex        =   8
      Top             =   1320
      Width           =   495
   End
   Begin VB.CommandButton cmdrellenar1 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   8880
      TabIndex        =   9
      Top             =   1800
      Width           =   495
   End
   Begin VB.CommandButton cmdrellenar1 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   8880
      TabIndex        =   10
      Top             =   2280
      Width           =   495
   End
   Begin VB.CommandButton cmdrellenar1 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   8880
      TabIndex        =   11
      Top             =   2760
      Width           =   495
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Principios Activos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   10
      Left            =   120
      TabIndex        =   19
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   2295
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2775
         Index           =   10
         Left            =   120
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   360
         Width           =   2010
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   3545
         _ExtentY        =   4895
         _StockProps     =   79
         Caption         =   "PRINCIPIOS ACTIVOS"
      End
   End
   Begin VB.Frame frame1 
      Caption         =   "Principios Origen"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   11
      Left            =   3120
      TabIndex        =   5
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   2295
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   2775
         Index           =   11
         Left            =   120
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   360
         Width           =   2010
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   2
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   2
         Columns(0).Width=   1852
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   6218
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         _ExtentX        =   3545
         _ExtentY        =   4895
         _StockProps     =   79
         Caption         =   "PRINCIPIOS ORIGEN"
      End
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   2520
      TabIndex        =   1
      Top             =   1320
      Width           =   495
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   2520
      TabIndex        =   2
      Top             =   1800
      Width           =   495
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   2520
      TabIndex        =   3
      Top             =   2280
      Width           =   495
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   2520
      TabIndex        =   4
      Top             =   2760
      Width           =   495
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cbotipo 
      Height          =   330
      Left            =   2400
      TabIndex        =   14
      ToolTipText     =   "Tipo de Interacci�n"
      Top             =   4080
      Width           =   1575
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1958
      Columns(0).Caption=   "C�d.Tipo"
      Columns(0).Name =   "C�d.Tipo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7911
      Columns(1).Caption=   "Desc.Tipo"
      Columns(1).Name =   "Desc.Tipo"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2778
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
   End
   Begin VB.Label Label4 
      Caption         =   "Horas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1440
      TabIndex        =   28
      Top             =   4200
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "Descripci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4320
      TabIndex        =   27
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Tipo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2400
      TabIndex        =   26
      Top             =   3840
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Tiempo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   25
      Top             =   3840
      Width           =   735
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Tipo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   735
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefIntMedica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefIntMedica (FR0110.FRM)                                 *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE 1998                                                  *
'* DESCRIPCION: definir interacciones medicamentosas                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cmdinteracciones_Click()
Dim i As Integer
Dim j As Integer
Dim mensaje As String

cmdinteracciones.Enabled = False

If Grid1(11).Rows > 0 And Grid1(13).Rows > 0 Then
If txtdesc.Text <> "" And txttiempo.Text <> "" And cbotipo.Text <> "" Then
    Grid1(11).MoveFirst
    For i = 0 To Grid1(11).Rows - 1
        Grid1(13).MoveFirst
        For j = 0 To Grid1(13).Rows - 1
          If (Grid1(11).Columns(0).Value <> Grid1(13).Columns(0).Value) Then
            Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), Grid1(11).Columns(0).Value)
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), Grid1(11).Columns(1).Value)
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(6), Grid1(13).Columns(0).Value)
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(7), Grid1(13).Columns(1).Value)
            If txttiempo.Text <> "" Then
                Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(8), txttiempo.Text) 'tiempo
            End If
            If cbotipo.Text <> "" Then
                Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(9), cbotipo.Text) 'tipo
            End If
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(11), txtdesc.Text) 'descripci�n
          End If
        Grid1(13).MoveNext
        Next j
     Grid1(11).MoveNext
     Next i
 'Guardar
 Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
 Else
    'txtdesc.Text <> "" And txttiempo.Text <> "" And cbotipo.Text <> "" Then
    If txtdesc.Text = "" Then
        mensaje = "El campo Descripci�n es obligatorio." & Chr(13)
    End If
    If txttiempo.Text = "" Then
        mensaje = mensaje & "El campo Tiempo es obligatorio." & Chr(13)
    End If
    If cbotipo.Text = "" Then
        mensaje = mensaje & "El campo Tipo es obligatorio."
    End If
    Call MsgBox(mensaje, vbInformation, "Aviso")
 End If
Else
    mensaje = MsgBox("Debe seleccionar Principios Origen y Destino", vbInformation, "Aviso")
End If
cmdinteracciones.Enabled = True
End Sub

Private Sub cmdrellenar_Click(Index As Integer)
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mvarfilatotal As Variant
Dim filas() As Integer
Dim filasordenadas() As Integer
Dim i As Integer
Dim j As Integer
Dim v As Integer
Dim max As Integer
Dim insertar As Integer


Select Case Index
Case 0
  insertar = 0
  'se mira que no se meta el mismo almac�n 2 veces
  Grid1(11).MoveFirst
  If Grid1(11).Rows > 0 Then
        For mintisel = 0 To Grid1(11).Rows - 1
            If Grid1(11).Columns(0).Value <> grdDBGrid1(10).Columns(3).Value Then
              insertar = 1
            Else
              insertar = 0
              Exit For
            End If
        Grid1(11).MoveNext
        Next mintisel
        If insertar = 1 Then
           insertar = 0
           Grid1(11).AddNew
           Grid1(11).Columns(0).Value = grdDBGrid1(10).Columns(3).Value
           Grid1(11).Columns(1).Value = grdDBGrid1(10).Columns(4).Value
           Grid1(11).Update
       End If
  Else
        Grid1(11).AddNew
        Grid1(11).Columns(0).Value = grdDBGrid1(10).Columns(3).Value
        Grid1(11).Columns(1).Value = grdDBGrid1(10).Columns(4).Value
        Grid1(11).Update
  End If

Case 1
  'se mira que no se meta el mismo almac�n 2 veces
  insertar = 0
  mintNTotalSelRows = grdDBGrid1(10).SelBookmarks.Count
  For mintisel = 0 To mintNTotalSelRows - 1
    Grid1(11).MoveFirst
    If Grid1(11).Rows > 0 Then
        For i = 0 To Grid1(11).Rows - 1
            mvarBkmrk = grdDBGrid1(10).SelBookmarks(mintisel)
            If Grid1(11).Columns(0).Value <> grdDBGrid1(10).Columns(3).CellValue(mvarBkmrk) Then
                insertar = 1
            Else
                insertar = 0
                Exit For
            End If
        Grid1(11).MoveNext
        Next i
        If insertar = 1 Then
                insertar = 0
                Grid1(11).AddNew
                Grid1(11).Columns(0).Value = grdDBGrid1(10).Columns(3).CellValue(mvarBkmrk)
                Grid1(11).Columns(1).Value = grdDBGrid1(10).Columns(4).CellValue(mvarBkmrk)
                Grid1(11).Update
        End If
    Else
        mvarBkmrk = grdDBGrid1(10).SelBookmarks(mintisel)
        Grid1(11).AddNew
        Grid1(11).Columns(0).Value = grdDBGrid1(10).Columns(3).CellValue(mvarBkmrk)
        Grid1(11).Columns(1).Value = grdDBGrid1(10).Columns(4).CellValue(mvarBkmrk)
        Grid1(11).Update
    End If
  Next mintisel

Case 2
  If Grid1(11).Rows = 0 Or Grid1(11).Rows = 1 Then
    Grid1(11).RemoveAll
  Else
    mvarBkmrk = Grid1(11).Bookmark
    mvarfilatotal = Grid1(11).AddItemRowIndex(mvarBkmrk)
    'If mvarfilatotal = Grid1(11).Rows - 1 Then
    '    mvarfilatotal = mvarfilatotal - 1
    'End If
    On Error Resume Next
    Call Grid1(11).RemoveItem(mvarfilatotal)
    On Error GoTo 0
  End If
Case 3
        mintNTotalSelRows = Grid1(11).SelBookmarks.Count
        ReDim filas(mintNTotalSelRows)
        ReDim filasordenadas(mintNTotalSelRows)
        For mintisel = 0 To mintNTotalSelRows - 1
          mvarBkmrk = Grid1(11).SelBookmarks(mintisel)
          mvarfilatotal = Grid1(11).AddItemRowIndex(mvarBkmrk)
          filas(mintisel) = mvarfilatotal
        Next mintisel
        'se ordena el array <filas> de mayor a menor en el array <filasordenadas>
        For j = 0 To mintNTotalSelRows - 1
            max = filas(0)
            For i = 0 To mintNTotalSelRows - 1
                If filas(i) >= max Then
                    max = filas(i)
                    v = i
                End If
            Next i
            filas(v) = 0
            filasordenadas(j) = max
        Next j
        For mintisel = 0 To mintNTotalSelRows - 1
          Call Grid1(11).RemoveItem(filasordenadas(mintisel))
        Next mintisel
End Select
End Sub

Private Sub cmdrellenar1_Click(Index As Integer)
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mvarfilatotal As Variant
Dim filas() As Integer
Dim filasordenadas() As Integer
Dim i As Integer
Dim j As Integer
Dim v As Integer
Dim max As Integer
Dim insertar As Integer


Select Case Index
'Case 0
'  Grid1(13).AddNew
'  Grid1(13).Columns(0).Value = grdDBGrid1(12).Columns(3).Value
'  Grid1(13).Columns(1).Value = grdDBGrid1(12).Columns(4).Value
'  Grid1(13).Update
'Case 1
'  mintNTotalSelRows = grdDBGrid1(12).SelBookmarks.Count
'  For mintisel = 0 To mintNTotalSelRows - 1
'    Grid1(13).AddNew
'    mvarBkmrk = grdDBGrid1(12).SelBookmarks(mintisel)
'    Grid1(13).Columns(0).Value = grdDBGrid1(12).Columns(3).CellValue(mvarBkmrk)
'    Grid1(13).Columns(1).Value = grdDBGrid1(12).Columns(4).CellValue(mvarBkmrk)
'    Grid1(13).Update
'  Next mintisel
Case 0
  insertar = 0
  'se mira que no se meta el mismo almac�n 2 veces
  Grid1(13).MoveFirst
  If Grid1(13).Rows > 0 Then
        For mintisel = 0 To Grid1(13).Rows - 1
            If Grid1(13).Columns(0).Value <> grdDBGrid1(12).Columns(3).Value Then
              insertar = 1
            Else
              insertar = 0
              Exit For
            End If
        Grid1(13).MoveNext
        Next mintisel
        If insertar = 1 Then
           insertar = 0
           Grid1(13).AddNew
           Grid1(13).Columns(0).Value = grdDBGrid1(12).Columns(3).Value
           Grid1(13).Columns(1).Value = grdDBGrid1(12).Columns(4).Value
           Grid1(13).Update
       End If
  Else
        Grid1(13).AddNew
        Grid1(13).Columns(0).Value = grdDBGrid1(12).Columns(3).Value
        Grid1(13).Columns(1).Value = grdDBGrid1(12).Columns(4).Value
        Grid1(13).Update
  End If

Case 1
  'se mira que no se meta el mismo almac�n 2 veces
  insertar = 0
  mintNTotalSelRows = grdDBGrid1(12).SelBookmarks.Count
  For mintisel = 0 To mintNTotalSelRows - 1
    Grid1(13).MoveFirst
    If Grid1(13).Rows > 0 Then
        For i = 0 To Grid1(13).Rows - 1
            mvarBkmrk = grdDBGrid1(12).SelBookmarks(mintisel)
            If Grid1(13).Columns(0).Value <> grdDBGrid1(12).Columns(3).CellValue(mvarBkmrk) Then
                insertar = 1
            Else
                insertar = 0
                Exit For
            End If
        Grid1(13).MoveNext
        Next i
        If insertar = 1 Then
                insertar = 0
                Grid1(13).AddNew
                Grid1(13).Columns(0).Value = grdDBGrid1(12).Columns(3).CellValue(mvarBkmrk)
                Grid1(13).Columns(1).Value = grdDBGrid1(12).Columns(4).CellValue(mvarBkmrk)
                Grid1(13).Update
        End If
    Else
        mvarBkmrk = grdDBGrid1(12).SelBookmarks(mintisel)
        Grid1(13).AddNew
        Grid1(13).Columns(0).Value = grdDBGrid1(12).Columns(3).CellValue(mvarBkmrk)
        Grid1(13).Columns(1).Value = grdDBGrid1(12).Columns(4).CellValue(mvarBkmrk)
        Grid1(13).Update
    End If
  Next mintisel
Case 2
  If Grid1(13).Rows = 0 Or Grid1(13).Rows = 1 Then
    Grid1(13).RemoveAll
  Else
    mvarBkmrk = Grid1(13).Bookmark
    mvarfilatotal = Grid1(13).AddItemRowIndex(mvarBkmrk)
    'If mvarfilatotal = Grid1(13).Rows - 1 Then
    '    mvarfilatotal = mvarfilatotal - 1
    'End If
    On Error Resume Next
    Call Grid1(13).RemoveItem(mvarfilatotal)
    On Error GoTo 0
  End If
Case 3
        mintNTotalSelRows = Grid1(13).SelBookmarks.Count
        ReDim filas(mintNTotalSelRows)
        ReDim filasordenadas(mintNTotalSelRows)
        For mintisel = 0 To mintNTotalSelRows - 1
          mvarBkmrk = Grid1(13).SelBookmarks(mintisel)
          mvarfilatotal = Grid1(13).AddItemRowIndex(mvarBkmrk)
          filas(mintisel) = mvarfilatotal
        Next mintisel
        'se ordena el array <filas> de mayor a menor en el array <filasordenadas>
        For j = 0 To mintNTotalSelRows - 1
            max = filas(0)
            For i = 0 To mintNTotalSelRows - 1
                If filas(i) >= max Then
                    max = filas(i)
                    v = i
                End If
            Next i
            filas(v) = 0
            filasordenadas(j) = max
        Next j
        For mintisel = 0 To mintNTotalSelRows - 1
          Call Grid1(13).RemoveItem(filasordenadas(mintisel))
        Next mintisel
End Select
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

Call objWinInfo.FormChangeActive(fraframe1(10), False, True)
objWinInfo.DataRefresh
Call objWinInfo.FormChangeActive(fraframe1(12), False, True)
objWinInfo.DataRefresh
Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
objWinInfo.DataRefresh
Call objWinInfo.FormChangeActive(fraframe1(1), False, True)

stra = "select FR78CODTIPINTER,FR78DESTIPINTER from FR7800"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While (Not rsta.EOF)
    Call cbotipo.AddItem(rsta.rdoColumns("FR78CODTIPINTER").Value & ";" & rsta.rdoColumns("FR78DESTIPINTER").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing

'se ocultan los c�digos de los principios activos
Grid1(11).Columns(0).Visible = False
Grid1(13).Columns(0).Visible = False

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Interacciones"
      
    .strTable = "FR4600"
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR46CODINTERACCIO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Interacciones")
    Call .FormAddFilterWhere(strKey, "FR46CODINTERACCIO", "C�digo Interacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR46DESLARGAINTER", "Descripci�n Interacci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR68CODPRINCACTIV_ORI", "Principio Origen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR68CODPRINCACTIV_DES", "Principio Destino", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR78CODTIPINTER", "Tipo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR46TIEMPINTERACC", "Tiempo", cwNumeric)

    Call .FormAddFilterOrder(strKey, "FR46CODINTERACCIO", "C�digo Interacci�n")
    Call .FormAddFilterOrder(strKey, "FR46DESLARGAINTER", "Descripci�n Interacci�n")
    Call .FormAddFilterOrder(strKey, "FR68CODPRINCACTIV_ORI", "Principio Origen")
    Call .FormAddFilterOrder(strKey, "FR68CODPRINCACTIV_DES", "Principio Destino")
    Call .FormAddFilterOrder(strKey, "FR78CODTIPINTER", "Tipo")
    Call .FormAddFilterOrder(strKey, "FR46TIEMPINTERACC", "Tiempo")
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(10)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(10)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Principios Activos Origen"
    
    .strTable = "FR6800"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR68DESPRINCACTIV", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Principios Origen")
    Call .FormAddFilterWhere(strKey, "FR68CODPRINCACTIV", "C�digo Interacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR68DESPRINCACTIV", "Descripci�n Interacci�n", cwString)
  End With
  
  With objMultiInfo1
    Set .objFormContainer = fraframe1(12)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(12)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Principios Activos Destino"
    
    .strTable = "FR6800"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR68DESPRINCACTIV", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Principios Destino")
    Call .FormAddFilterWhere(strKey, "FR68CODPRINCACTIV", "C�digo Interacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR68DESPRINCACTIV", "Descripci�n Interacci�n", cwString)
    
  End With
  
  
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMasterInfo, "C�d.Interacci�n", "FR46CODINTERACCIO", cwNumeric, 9)
    Call .GridAddColumn(objMasterInfo, "C�d.Origen", "FR68CODPRINCACTIV_ORI", cwNumeric, 9)
    Call .GridAddColumn(objMasterInfo, "Desc.Principio Origen", "", cwString, 50)
    Call .GridAddColumn(objMasterInfo, "C�d.Destino", "FR68CODPRINCACTIV_DES", cwNumeric, 9)
    Call .GridAddColumn(objMasterInfo, "Desc.Principio Destino", "", cwString, 50)
    Call .GridAddColumn(objMasterInfo, "Tiempo", "FR46TIEMPINTERACC", cwNumeric, 9)
    Call .GridAddColumn(objMasterInfo, "Tipo", "FR78CODTIPINTER", cwNumeric, 7)
    Call .GridAddColumn(objMasterInfo, "Desc.Tipo", "", cwString, 30)
    Call .GridAddColumn(objMasterInfo, "Descripci�n", "FR46DESLARGAINTER", cwString, 2000)
  
  
    Call .GridAddColumn(objMultiInfo, "C�digo", "FR68CODPRINCACTIV", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n", "FR68DESPRINCACTIV", cwString, 50)
    
    Call .GridAddColumn(objMultiInfo1, "C�digo", "FR68CODPRINCACTIV", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "Descripci�n", "FR68DESPRINCACTIV", cwString, 50)
   
    Call .FormCreateInfo(objMasterInfo)
    Call .FormCreateInfo(objMultiInfo)
    Call .FormCreateInfo(objMultiInfo1)
    
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnMandatory = True 'origen
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnMandatory = True 'destino
    .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnMandatory = True 'descripci�n
    
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(10).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(10).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(12).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(12).Columns(4)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnForeign = True
   
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "FR68CODPRINCACTIV", "SELECT * FROM FR6800 WHERE FR68CODPRINCACTIV = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "FR68DESPRINCACTIV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR68CODPRINCACTIV", "SELECT * FROM FR6800 WHERE FR68CODPRINCACTIV = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(7), "FR68DESPRINCACTIV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "FR78CODTIPINTER", "SELECT * FROM FR7800 WHERE FR78CODTIPINTER = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "FR78DESTIPINTER")
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  grdDBGrid1(10).Columns(3).Visible = False
  grdDBGrid1(12).Columns(3).Visible = False
  
  grdDBGrid1(1).Columns(3).Width = 1000
  grdDBGrid1(1).Columns(4).Width = 1000
  grdDBGrid1(1).Columns(5).Width = 2000
  grdDBGrid1(1).Columns(6).Width = 1000
  grdDBGrid1(1).Columns(7).Width = 2000
  grdDBGrid1(1).Columns(8).Width = 1000
  grdDBGrid1(1).Columns(9).Width = 1000
  grdDBGrid1(1).Columns(10).Width = 1500

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch
  
  If strCtrl = "grdDBGrid1(1).C�d.Origen" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR6800"
     .strOrder = "ORDER BY FR68CODPRINCACTIV ASC"
         
     Set objField = .AddField("FR68CODPRINCACTIV")
     objField.strSmallDesc = "C�digo Principio Activo"
         
     Set objField = .AddField("FR68DESPRINCACTIV")
     objField.strSmallDesc = "Descripci�n Principio Activo"
         
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), .cllValues("FR68CODPRINCACTIV"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "grdDBGrid1(1).C�d.Destino" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR6800"
     .strOrder = "ORDER BY FR68CODPRINCACTIV ASC"
         
     Set objField = .AddField("FR68CODPRINCACTIV")
     objField.strSmallDesc = "C�digo Principio Activo"
         
     Set objField = .AddField("FR68DESPRINCACTIV")
     objField.strSmallDesc = "Descripci�n Principio Activo"
         
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(6), .cllValues("FR68CODPRINCACTIV"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "grdDBGrid1(1).Tipo" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7800"
     .strOrder = "ORDER BY FR78CODTIPINTER ASC"
         
     Set objField = .AddField("FR78CODTIPINTER")
     objField.strSmallDesc = "C�digo Tipo de Interacci�n"
         
     Set objField = .AddField("FR78DESTIPINTER")
     objField.strSmallDesc = "Descripci�n Tipo de Interacci�n"
         
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(9), .cllValues("FR78CODTIPINTER"))
     End If
   End With
   Set objSearch = Nothing
 End If
 Call grdDBGrid1_Change(1)
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Interacciones" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR46CODINTERACCIO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(3), rsta.rdoColumns(0).Value)
    grdDBGrid1(1).Col = 4
    'SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String

If intIndex = 10 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    sqlstr = "SELECT FR46CODINTERACCIO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(3), rsta.rdoColumns(0).Value)
    grdDBGrid1(1).Col = 4
    'SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
Dim mensaje As String

If intIndex = 1 Then
    If grdDBGrid1(1).Columns(4).Value = grdDBGrid1(1).Columns(6).Value Then
      mensaje = MsgBox("Un principio activo no puede interactuar consigo mismo.", vbInformation, "Aviso")
      grdDBGrid1(1).Columns(4).Value = ""
      grdDBGrid1(1).Columns(5).Value = ""
      grdDBGrid1(1).Columns(6).Value = ""
      grdDBGrid1(1).Columns(7).Value = ""
      Call objWinInfo.CtrlDataChange
    Else
      Call objWinInfo.CtrlDataChange
    End If
Else
    Call objWinInfo.CtrlDataChange
End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub txttiempo_Change()
If Not IsNumeric(txttiempo.Text) And txttiempo.Text <> "" Then
    Call MsgBox("El campo Tiempo contiene datos no num�ricos", vbInformation, "Aviso")
    txttiempo.Text = ""
End If
End Sub

Private Sub txttiempo_KeyPress(KeyAscii As Integer)
If KeyAscii = 46 Or KeyAscii = 44 Then
    txttiempo.Text = ""
End If
End Sub
