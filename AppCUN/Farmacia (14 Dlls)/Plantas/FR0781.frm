VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form FrmRedPedFMPlanta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. PEDIR SERVICIO FARMACIA. Redactar Pedido Farmacia .F�rmula Magistral"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0781.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdFMnueva 
      Caption         =   "F�rm.Magistral"
      Height          =   495
      Left            =   10080
      TabIndex        =   38
      Top             =   4800
      Width           =   1575
   End
   Begin VB.CommandButton cmdFMnormalizada 
      Caption         =   "F.M. Normalizada"
      Height          =   495
      Left            =   10080
      TabIndex        =   37
      Top             =   3960
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   480
      Width           =   11700
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   0
         Left            =   120
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0781.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(16)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(19)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(23)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(28)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(46)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(6)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(5)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(4)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(8)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "chkCheck1(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(61)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(62)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "chkCheck1(15)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0781.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR55INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   5760
            TabIndex        =   10
            Tag             =   "Inter�s Cient�fico?"
            Top             =   1800
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   62
            Left            =   8280
            TabIndex        =   53
            Tag             =   "Dpto.Cargo"
            Top             =   1800
            Visible         =   0   'False
            Width           =   2685
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO_CRG"
            Height          =   330
            Index           =   61
            Left            =   7800
            TabIndex        =   52
            Tag             =   "C�d.Dpto.Cargo"
            Top             =   1800
            Visible         =   0   'False
            Width           =   360
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "FM"
            DataField       =   "FR55INDFM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   7920
            TabIndex        =   51
            Tag             =   "Form.Magistral?"
            Top             =   360
            Value           =   1  'Checked
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   8
            Left            =   6960
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Descripci�n Secci�n"
            Top             =   1080
            Width           =   3855
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD41CODSECCION"
            Height          =   330
            Index           =   7
            Left            =   5640
            TabIndex        =   6
            Tag             =   "C�digo Secci�n"
            Top             =   1080
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   1680
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   3735
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   5
            Left            =   120
            TabIndex        =   4
            Tag             =   "C�digo Servicio"
            Top             =   1080
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   3
            Left            =   120
            TabIndex        =   8
            Tag             =   "C�d. Peticionario"
            Top             =   1800
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   1680
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   1080
            Width           =   3735
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�d. Necesidad"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   2040
            TabIndex        =   1
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   2520
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1905
            Index           =   2
            Left            =   -74880
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19076
            _ExtentY        =   3360
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   5640
            TabIndex        =   3
            Tag             =   "Fecha Petici�n"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Departamento de Cargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   46
            Left            =   7800
            TabIndex        =   54
            Top             =   1560
            Visible         =   0   'False
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Secci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   6960
            TabIndex        =   28
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Secci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   5640
            TabIndex        =   27
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   26
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona Peticionaria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   1680
            TabIndex        =   24
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   23
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   1680
            TabIndex        =   21
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   5640
            TabIndex        =   20
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   19
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   2040
            TabIndex        =   18
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   9960
      TabIndex        =   22
      Top             =   5880
      Width           =   1935
      Begin VB.CommandButton cmdFirmarEnviar 
         Caption         =   "&Firmar y Enviar"
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "F�rmula Magistral"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   2
      Left            =   120
      TabIndex        =   16
      Top             =   3360
      Width           =   9735
      Begin TabDlg.SSTab tabTab1 
         Height          =   4095
         Index           =   1
         Left            =   120
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   360
         Width           =   9495
         _ExtentX        =   16748
         _ExtentY        =   7223
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0781.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0781.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(6)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3840
            Left            =   240
            TabIndex        =   30
            Top             =   120
            Width           =   8415
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   11
               Left            =   3240
               TabIndex        =   49
               Tag             =   "C�d.Producto"
               Top             =   2760
               Visible         =   0   'False
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR20NUMLINEA"
               Height          =   330
               Index           =   10
               Left            =   1560
               TabIndex        =   47
               Tag             =   "Linea"
               Top             =   2760
               Visible         =   0   'False
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR55CODNECESUNID"
               Height          =   330
               Index           =   9
               Left            =   0
               TabIndex        =   45
               Tag             =   "C�d. Necesidad"
               Top             =   2760
               Visible         =   0   'False
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR20CANTNECESQUIR"
               Height          =   330
               Index           =   28
               Left            =   5400
               TabIndex        =   32
               Tag             =   "Cantidad"
               Top             =   600
               Width           =   855
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Index           =   1
               Left            =   4920
               TabIndex        =   35
               Top             =   2880
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.CommandButton cmdWord 
               Caption         =   "W"
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   370
               Left            =   4200
               TabIndex        =   36
               Top             =   1680
               Width           =   495
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR20PATHFORMAG"
               Height          =   350
               Index           =   57
               Left            =   0
               TabIndex        =   34
               Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
               Top             =   1680
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR20DESFORMAG"
               Height          =   330
               Index           =   55
               Left            =   0
               TabIndex        =   31
               Tag             =   "Form.Magistral"
               Top             =   600
               Width           =   5295
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   49
               Left            =   6360
               TabIndex        =   33
               Tag             =   "U.M.P."
               Top             =   600
               Width           =   540
            End
            Begin MSComDlg.CommonDialog CommonDialog1 
               Left            =   6120
               Top             =   1560
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   327681
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   3240
               TabIndex        =   50
               Top             =   2520
               Visible         =   0   'False
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Linea"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   1560
               TabIndex        =   48
               Top             =   2520
               Visible         =   0   'False
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Necesidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   0
               TabIndex        =   46
               Top             =   2520
               Visible         =   0   'False
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Archivo Inf. F�rmula Magistral"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   84
               Left            =   0
               TabIndex        =   41
               Top             =   1440
               Width           =   2550
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cantidad   preparar"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   17
               Left            =   5400
               TabIndex        =   40
               Top             =   360
               Width           =   1650
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "F�rmula Magistral"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   0
               TabIndex        =   39
               Top             =   360
               Width           =   1500
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   5
            Left            =   -74880
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   120
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   5477
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3825
            Index           =   6
            Left            =   -74820
            TabIndex        =   44
            Top             =   120
            Width           =   8775
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   6
            stylesets(0).Name=   "Estupefacientes"
            stylesets(0).BackColor=   14671839
            stylesets(0).Picture=   "FR0781.frx":007C
            stylesets(1).Name=   "FormMagistral"
            stylesets(1).BackColor=   8454016
            stylesets(1).Picture=   "FR0781.frx":0098
            stylesets(2).Name=   "Medicamentos"
            stylesets(2).BackColor=   16777215
            stylesets(2).Picture=   "FR0781.frx":00B4
            stylesets(3).Name=   "MezclaIV2M"
            stylesets(3).BackColor=   8454143
            stylesets(3).Picture=   "FR0781.frx":00D0
            stylesets(4).Name=   "PRNenOM"
            stylesets(4).BackColor=   12615935
            stylesets(4).Picture=   "FR0781.frx":00EC
            stylesets(5).Name=   "Fluidoterapia"
            stylesets(5).BackColor=   16776960
            stylesets(5).Picture=   "FR0781.frx":0108
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15478
            _ExtentY        =   6747
            _StockProps     =   79
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   12
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRedPedFMPlanta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedPedStPlanta (FR0131.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: 10 DE NOVIEMBRE DE 1998                                       *
'* DESCRIPCION: Redactar Pedido Stock de Planta                         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnError As Boolean
Dim strBoton As String
Dim CantidadBloqueada As Boolean


Private Sub chkCheck1_Click(Index As Integer)
If Index = 15 Then
  If chkCheck1(15).Value = 1 Then
   txtText1(61).Visible = True
   txtText1(62).Visible = True
   lblLabel1(46).Visible = True
   txtText1(61).BackColor = &HFFFF00
   If IsNumeric(txtText1(5).Text) Then
      txtText1(61).Text = txtText1(5).Text
      txtText1(61).SetFocus
      SendKeys ("{TAB}")
   End If
  Else
   txtText1(61).Visible = False
   txtText1(62).Visible = False
   lblLabel1(46).Visible = False
   txtText1(61).Text = ""
   txtText1(62).Text = ""
   txtText1(61).BackColor = &HFFFFFF
  End If
End If

End Sub

Private Sub chkCheck1_GotFocus(Index As Integer)
'If Index = 15 Then
'  If txtText1(1).Text = 3 Then
'    chkCheck1(15).Enabled = False
'    Call MsgBox("El PRN est� enviado. No puede modificarlo", vbInformation, "Aviso")
'  End If
'End If

End Sub

Private Sub cmdFMnormalizada_Click()
Dim mensaje As String

  strBoton = "cmdFMnormalizada"

  cmdFMnormalizada.Enabled = False
  If txtText1(0).Text <> "" Then
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  End If
      
  strBoton = ""
  cmdFMnormalizada.Enabled = True

End Sub

Private Sub cmdFMnueva_Click()
Dim mensaje As String

  strBoton = "cmdFMnueva"

  cmdFMnueva.Enabled = False
  If txtText1(0).Text <> "" Then
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  End If
      
  strBoton = ""
  cmdFMnueva.Enabled = True


End Sub

Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strplantilla As String
Dim strubiplantilla As String
Dim wword As Word.Application
Dim strUbiDocumento As String
Dim strUbicacion As String
Dim parametroubicacionfichero As Integer
Dim parametroubicacionplantilla As Integer

parametroubicacionfichero = 36
parametroubicacionplantilla = 37

On Error GoTo error_shell
  
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=" & parametroubicacionplantilla
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strplantilla = rsta(0).Value
  End If
  rsta.Close
  Set rsta = Nothing
  
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=" & parametroubicacionfichero
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strUbicacion = rsta(0).Value
  End If
  rsta.Close
  Set rsta = Nothing
  
  strubiplantilla = strplantilla
  
  If txtText1(57).Text <> "" Then
      strUbiDocumento = strUbicacion & txtText1(57).Text
      
      Set wword = New Word.Application
      If Dir(strUbiDocumento) = "" Then
        wword.Documents.Add strubiplantilla
        wword.ActiveDocument.SaveAs strUbiDocumento
      Else
        wword.Documents.Open strUbiDocumento
      End If
      wword.Visible = True
  End If

error_shell:

  If err.Number <> 0 Then
    MsgBox "Error N�mero: " & err.Number & Chr(13) & err.Description, vbCritical
  End If

End Sub

Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer



If Index = 0 Then
Else
    On Error Resume Next
    pathFileName = Dir(txtText1(57).Text)
    finpath = InStr(1, txtText1(57).Text, pathFileName, 1)
    pathFileName = Left(txtText1(57).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(57).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If err.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), "")
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), strOpenFileName)
    End If
End If

End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "Necesidad Unidad" And strCtrlName = "txtText1(7)" Then  'SECCION
    aValues(2) = txtText1(5).Text  'DEPARTAMENTO
End If
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

If strFormName = "Detalle Necesidad" Then
  Select Case txtText1(1).Text
    Case 1:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
        objWinInfo.DataRefresh
      End If
      cmdFirmarEnviar.Enabled = True
    Case 2:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      cmdFirmarEnviar.Enabled = True
    Case Else:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      cmdFirmarEnviar.Enabled = False
  End Select
End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  
If strFormName <> "Detalle Necesidad" Then
  Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  Select Case txtText1(1).Text
    Case 1:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
        objWinInfo.DataRefresh
      End If
      cmdFirmarEnviar.Enabled = True
    Case 2:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      cmdFirmarEnviar.Enabled = True
    Case Else:
      If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        objWinInfo.DataRefresh
      End If
      cmdFirmarEnviar.Enabled = False
  End Select
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim blnDptoCorrecto As Boolean
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim mensaje As String

  If objWinInfo.objWinActiveForm.strName <> "Detalle Necesidad" Then
    If txtText1(5).Text <> "" Then
      'si est� clicado Inter�s Cient�fico debe haber metido el Dpto. de Cargo
      If chkCheck1(15).Value = 1 Then
        If Trim(txtText1(61).Text) = "" Then
          mensaje = MsgBox("El Departamento de Cargo es obligatorio", vbInformation, "Aviso")
          blnCancel = True
        End If
      End If
      blnDptoCorrecto = False
      If txtText1(3).Text <> "" Then
        sqlstr = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & txtText1(3).Text & "'"
        sqlstr = sqlstr & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        While Not rsta.EOF
          If txtText1(5).Text = rsta.rdoColumns(0).Value Then
            blnDptoCorrecto = True
          End If
          rsta.MoveNext
        Wend
        rsta.Close
        Set rsta = Nothing
      Else
        sqlstr = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "'"
        sqlstr = sqlstr & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        While Not rsta.EOF
          If txtText1(5).Text = rsta.rdoColumns(0).Value Then
            blnDptoCorrecto = True
          End If
          rsta.MoveNext
        Wend
        rsta.Close
        Set rsta = Nothing
      End If
      If blnDptoCorrecto = False Then
        Call MsgBox("El Peticionario no pertenece al servicio", vbExclamation, "Aviso")
        blnCancel = True
      End If
      If blnDptoCorrecto = True Then
        SQL = "SELECT COUNT(*)"
        SQL = SQL & " FROM AD4100"
        SQL = SQL & " WHERE AD02CODDPTO = ?"
        SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = txtText1(5).Text
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rs(0).Value = 0 Then
        Else
          If txtText1(7).Text = "" Then
            MsgBox "El Servicio tiene secciones, es Obligatorio elegir una SECCION.", vbExclamation
            blnCancel = True
          End If
        End If
        rs.Close
        Set rs = Nothing
      End If
    End If
  Else 'Detalle
    If txtText1(57) = "" And txtText1(11).Text = "999999999" Then
      Call MsgBox("El Archivo Inf. F�rmula Magistral es obligatorio.", vbExclamation, "Aviso")
      blnCancel = True
    End If
  End If

  If IsNumeric(txtText1(5).Text) And IsNumeric(txtText1(7).Text) And txtText1(7).Visible = True Then
    sqlstr = "SELECT COUNT(*) FROM AD4100 WHERE AD02CODDPTO=" & txtText1(5).Text
    sqlstr = sqlstr & " AND " & "AD41CODSECCION=" & txtText1(7).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.rdoColumns(0).Value = 0 Then
      Call MsgBox("La secci�n es incorrecta", vbExclamation, "Aviso")
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  mblnError = blnCancel
End Sub


Private Sub cmdfirmarenviar_Click()
Dim intMsg As Integer
Dim strupdate As String
Dim strselect As String
Dim strInsert As String
Dim bCrearOFNueva As Boolean
Dim rsta As rdoResultset
Dim strSelectFR6900 As String
Dim rstFR6900 As rdoResultset
Dim strSelectFR7000 As String
Dim rstFR7000 As rdoResultset
Dim strSelectFR7100 As String
Dim rstFR7100 As rdoResultset
Dim auxcant
   
If tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If
If tlbToolbar1.Buttons(4).Enabled = True Then
  Exit Sub
End If

Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
Select Case txtText1(1).Text
  Case 1:
    If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
      objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
      objWinInfo.DataRefresh
    End If
    cmdFirmarEnviar.Enabled = True
  Case 2:
    If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
      objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
      objWinInfo.DataRefresh
    End If
    cmdFirmarEnviar.Enabled = True
  Case Else:
    If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
      objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
      objWinInfo.DataRefresh
    End If
    cmdFirmarEnviar.Enabled = False
    Exit Sub
End Select

If txtText1(1).Text = 3 Then
  MsgBox "La Petici�n ya est� firmada.", vbCritical, "Aviso"
  Exit Sub
End If
   
 cmdFirmarEnviar.Enabled = False
 If IsNumeric(txtText1(0).Text) Then
    intMsg = MsgBox("�Est� seguro que desea firmar y enviar la Petici�n?", vbQuestion + vbYesNo, "Aviso")
    If intMsg = vbYes Then
      Screen.MousePointer = vbHourglass
      If IsNumeric(txtText1(11).Text) Then 'existe la FM
         '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ' Mirar si existe una orden de fabricaci�n para este c�digo de petici�n
        strselect = "SELECT COUNT(FR55CODNECESUNID) FROM FR5900 WHERE " & _
                 " FR55CODNECESUNID=" & txtText1(0).Text & _
                 " AND FR20NUMLINEA=" & txtText1(10).Text
        Set rsta = objApp.rdoConnect.OpenResultset(strselect)
        If IsNull(rsta.rdoColumns(0).Value) Then
          ' Ok, NO EXISTE -> CREAR UNA NUEVA
        bCrearOFNueva = True
        ElseIf rsta.rdoColumns(0).Value = 0 Then
          ' Ok, NO EXISTE -> CREAR UNA NUEVA
        bCrearOFNueva = True
        Else
          ' Ok, SI EXISTE -> HACER REFERENCIA A LA QUE EXISTE
         bCrearOFNueva = False
        End If
        rsta.Close
        Set rsta = Nothing
        If bCrearOFNueva Then
        ' Crear una OF nueva por cada forma de hacer un producto
        strSelectFR6900 = " SELECT FR69CODPROCFABRIC, FR73CODPRODUCTO, FR69DESPROCESO FROM FR6900 " & _
                          " WHERE FR73CODPRODUCTO = " & txtText1(11).Text
        Set rstFR6900 = objApp.rdoConnect.OpenResultset(strSelectFR6900)
        If rstFR6900.EOF Then
           strInsert = "insert into fr5900 " & _
                       "  (FR59CODORDFABR, " & _
                       "  FR59OBSORDEN,    " & _
                       "  FR73CODPRODUCTO, " & _
                       "  FR93CODUNIMEDIDA, " & _
                       "  FR59CANTFABRICAR, " & _
                       "  FR37CODESTOF, " & _
                       "  FR55CODNECESUNID, " & _
                       "  FR20NUMLINEA, " & _
                       "  FR59DESPROCESO,FR59INDPAC) "
           strInsert = strInsert & _
                       "Values " & _
                       "  (FR59CODORDFABR_SEQUENCE.nextval," & _
                       "  NULL," & _
                          txtText1(11).Text & "," & _
                       "'" & txtText1(49).Text & "'" & "," & _
                          objGen.ReplaceStr(txtText1(28).Text, ",", ".", 1) & "," & _
                       "1," & _
                          txtText1(0).Text & "," & _
                          txtText1(10).Text & "," & _
                       "NULL" & _
                       ",-1)"
           objApp.rdoConnect.Execute strInsert, 64
           objApp.rdoConnect.Execute "Commit", 64
        End If
        While Not rstFR6900.EOF
           strselect = "SELECT FR59CODORDFABR_SEQUENCE.nextval FROM dual"
           Set rsta = objApp.rdoConnect.OpenResultset(strselect)
           strInsert = "insert into fr5900 " & _
                       "  (FR59CODORDFABR, " & _
                       "  FR59OBSORDEN,    " & _
                       "  FR73CODPRODUCTO, " & _
                       "  FR93CODUNIMEDIDA, " & _
                       "  FR59CANTFABRICAR, " & _
                       "  FR37CODESTOF, " & _
                       "  FR55CODNECESUNID, " & _
                       "  FR20NUMLINEA, " & _
                       "  FR59DESPROCESO,FR59INDPAC) "
           strInsert = strInsert & _
                       "Values " & _
                       "  (" & rsta.rdoColumns(0).Value & "," & _
                       "  NULL," & _
                          txtText1(11).Text & "," & _
                       "'" & txtText1(49).Text & "'" & "," & _
                          objGen.ReplaceStr(txtText1(28).Text, ",", ".", 1) & "," & _
                       "1," & _
                          txtText1(0).Text & "," & _
                          txtText1(10).Text & "," & _
                       "'" & rstFR6900.rdoColumns("FR69DESPROCESO").Value & _
                       "',-1)"
           objApp.rdoConnect.Execute strInsert, 64
           objApp.rdoConnect.Execute "Commit", 64
        
           strSelectFR7000 = "SELECT * " & _
                             " FROM FR7000 " & _
                             " WHERE FR69CODPROCFABRIC = " & rstFR6900.rdoColumns("FR69CODPROCFABRIC").Value
        
           Set rstFR7000 = objApp.rdoConnect.OpenResultset(strSelectFR7000)
        
           ' Crear una nueva Operaci�n por cada Operaci�n/Producto
           While Not rstFR7000.EOF ' Recorrer la tabla Proceso Operacion
              strSelectFR7100 = "SELECT * "
              strSelectFR7100 = strSelectFR7100 & "  FROM FR7100 WHERE "
              strSelectFR7100 = strSelectFR7100 & "  FR69CODPROCFABRIC = " & rstFR7000.rdoColumns("FR69CODPROCFABRIC").Value & " And "
              strSelectFR7100 = strSelectFR7100 & "  FR70CODOPERACION = " & rstFR7000.rdoColumns("FR70CODOPERACION").Value
        
              Set rstFR7100 = objApp.rdoConnect.OpenResultset(strSelectFR7100)
              While Not rstFR7100.EOF ' Recorrer la tabla Proceso Producto
                 strInsert = "insert into fr2400 " & _
                             "  (FR59CODORDFABR, " & _
                             "  FR70CODOPERACION, " & _
                             "  FR70DESOPERACION ," & _
                             "  FR24TIEMPPREV, " & _
                             "  SG02COD, " & _
                             "  FR24TIEMPREAL, " & _
                             "  FR73CODPRODUCTO, " & _
                             "  FR24CANTNECESARIA, " & _
                             "  FR93CODUNIMEDIDA, " & _
                             "  FR37CODESTOF)" & _
                             "Values " & _
                             "  (" & rsta.rdoColumns(0).Value & "," & _
                             rstFR7000.rdoColumns("FR70CODOPERACION").Value & "," & _
                             "'" & rstFR7000.rdoColumns("FR70DESOPERACION").Value & "'" & "," & _
                             rstFR7000.rdoColumns("FR70TIEMPOPREPACI").Value & "," & _
                             "'" & objsecurity.strUser & "'" & ","
                 If Not IsNull(rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value) Then
                    strInsert = strInsert & rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value & ","
                 Else
                    strInsert = strInsert & " NULL,"
                 End If
                 strInsert = strInsert & rstFR7100.rdoColumns("FR73CODPRODUCTO").Value & ","
                 auxcant = txtText1(28).Text * rstFR7100.rdoColumns("FR71CANTNECESARIA").Value
                 strInsert = strInsert & objGen.ReplaceStr(auxcant, ",", ".", 1) & ","
                 If Not IsNull(rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                    strInsert = strInsert & "'" & rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
                 Else
                    strInsert = strInsert & " NULL,"
                 End If
                 strInsert = strInsert & "  1)"
                 objApp.rdoConnect.Execute strInsert, 64
                 objApp.rdoConnect.Execute "Commit", 64
                 rstFR7100.MoveNext
              Wend
              rstFR7100.Close
              Set rstFR7100 = Nothing
              rstFR7000.MoveNext
           Wend
           rstFR7000.Close
           Set rstFR7000 = Nothing
           rstFR6900.MoveNext
        Wend
        rstFR6900.Close
        Set rstFR6900 = Nothing
        End If
      End If 'existe la FM
      strupdate = "UPDATE FR5500 SET FR55FECENVIO=SYSDATE,FR26CODESTPETIC=3,SG02COD_FIR=" & "'" & objsecurity.strUser & "'" & " WHERE FR55CODNECESUNID=" & txtText1(0).Text
      objApp.rdoConnect.Execute strupdate, 64
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      objWinInfo.DataRefresh
      Call MsgBox("La petici�n ha sido Firmada y Enviada", vbInformation, "Aviso")
      cmdFirmarEnviar.Enabled = True
      Screen.MousePointer = vbDefault
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     Else
       cmdFirmarEnviar.Enabled = True
     End If
 Else
   cmdFirmarEnviar.Enabled = True
 End If
cmdFirmarEnviar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String

  
    Set objWinInfo = New clsCWWin
    If gintfirmarStPlanta = 1 Then 'Se le llama desde la pantalla Firmar Pedido Farmacia
        Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    Else
        Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    End If
    
      With objMasterInfo
        Set .objFormContainer = fraFrame1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(2)
        
        .strName = "Necesidad Unidad"
        .blnAskPrimary = False
          
        .strTable = "FR5500"
        If gintfirmarStPlanta = 1 Then 'Se le llama desde la pantalla Firmar Pedido Farmacia
            .strWhere = "FR55CODNECESUNID=" & glngnumpeticion & " AND FR55INDFM=-1 "
            .intAllowance = cwAllowReadOnly
        Else
            '.strWhere = "FR26CODESTPETIC in (1,2,3,4,5,9) AND " & _
                      "AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "') "
            .strWhere = "FR26CODESTPETIC in (1,2,3,4,5,9) AND FR55INDFM=-1 "
            'Red,Fir,Env,Valid,Disp Parc
        End If
        
        
        Call .FormAddOrderField("FR55FECPETICION", cwDescending)
        Call .FormAddOrderField("FR55CODNECESUNID", cwDescending)
        
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
        Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
        Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
        Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")
    
      End With
    With objMultiInfo
        .strName = "Detalle Necesidad"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = fraFrame1(1)
        Set .tabMainTab = tabTab1(1)
        Set .grdGrid = grdDBGrid1(6)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        
        .strTable = "FR2000"
        
        .blnAskPrimary = False
        'If gintfirmarStPlanta = 1 Then 'Se le llama desde la pantalla Firmar Pedido Farmacia
        '    .intAllowance = cwAllowReadOnly
        '    'cmdbuscarprod.Enabled = False
        '    cmdbuscargruprod.Enabled = False
        'End If
    
        '.intAllowance = cwAllowReadOnly
        '.intCursorSize = 0
        
        Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
        
        strKey = .strDataBase & .strTable
        
        'Se establecen los campos por los que se puede filtrar
        'Call .FormCreateFilterWhere(strKey, "Necesidades")
        'Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida", cwString)
        'Call .FormAddFilterWhere(strKey, "FR20CANTNECESQUIR", "Cantidad", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        'Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d. Producto")
        'Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida")
        'Call .FormAddFilterOrder(strKey, "FR20CANTNECESQUIR", "Cantidad")
        
        Call .FormAddRelation("FR55CODNECESUNID", txtText1(0))
    End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormDetail)
    
        Call .FormCreateInfo(objMasterInfo)
        
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(txtText1(0)).intKeyNo = 1

        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnReadOnly = True
        .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
        .CtrlGetInfo(txtText1(3)).blnInFind = True
        .CtrlGetInfo(txtText1(5)).blnInFind = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
        
        .CtrlGetInfo(txtText1(49)).blnForeign = True

        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "SG02APE1")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "AD02DESDPTO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "AD02DESDPTO")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR26DESESTADOPET")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "AD41CODSECCION", "SELECT AD41DESSECCION FROM AD4100 WHERE AD41CODSECCION= ? AND AD02CODDPTO=?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(8), "AD41DESSECCION")
    
        .CtrlGetInfo(txtText1(3)).blnForeign = True
        .CtrlGetInfo(txtText1(5)).blnForeign = True
        .CtrlGetInfo(txtText1(7)).blnForeign = True
        .CtrlGetInfo(txtText1(61)).blnForeign = True
        
        .CtrlGetInfo(txtText1(3)).blnMandatory = True
        .CtrlGetInfo(txtText1(55)).blnMandatory = True
        .CtrlGetInfo(txtText1(57)).blnReadOnly = True
        
        .CtrlGetInfo(txtText1(11)).blnForeign = True
        
        
        
        Call .WinRegister
        Call .WinStabilize
    End With

strBoton = ""

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset
    
  intCancel = objWinInfo.WinExit
  If intCancel = 0 Then
    'si la petici�n no tiene productos elimina dicha petici�n
    If IsNumeric(txtText1(0).Text) Then
      stra = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
        strDelete = "DELETE FR5500 WHERE FR55CODNECESUNID=" & txtText1(0).Text
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If

End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  'Dim intReport As Integer
  'Dim objPrinter As clsCWPrinter
  'Dim blnHasFilter As Boolean
  '
  'If strFormName = "Estupefacientes" Then
  '  Call objWinInfo.FormPrinterDialog(True, "")
  '  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  '  intReport = objPrinter.Selected
  '  If intReport > 0 Then
  '    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
  '    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
  '                               objWinInfo.DataGetOrder(blnHasFilter, True))
  '  End If
  '  Set objPrinter = Nothing
  'End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txttext1(3)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Peticionario"

     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

      .strWhere = "where SG02COD IN (SELECT SG02COD FROM AD0300 WHERE AD02CODDPTO=" & txtText1(5).Text & " AND (AD03FECFIN > SYSDATE OR AD03FECFIN IS NULL))"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(3), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txttext1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = " WHERE AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
                 
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txttext1(61)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = " WHERE AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
                 
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(61), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txtText1(7)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD4100"

     Set objField = .AddField("AD41CODSECCION")
     objField.strSmallDesc = "C�digo Secci�n"

     Set objField = .AddField("AD41DESSECCION")
     objField.strSmallDesc = "Descripci�n Secci�n"
     
     If IsNumeric(txtText1(5).Text) Then
      .strWhere = "where AD02CODDPTO=" & txtText1(5).Text
     Else
       Call MsgBox("Debe introducir el Servicio antes de ver sus Secciones", vbInformation, "Aviso")
       Exit Sub
     End If

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(7), .cllValues("AD41CODSECCION"))
     End If
   End With
   Set objSearch = Nothing
 End If
 If CantidadBloqueada = False Then
  If strFormName = "Detalle Necesidad" And strCtrl = "txtText1(49)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo U.M."

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n U.M."
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(49), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
  End If
 End If
 If strFormName = "Detalle Necesidad" And strCtrl = "txtText1(11)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = " WHERE FR73INDFABRICABLE=-1 "
     .strWhere = .strWhere & " AND TRUNC(SYSDATE) BETWEEN TRUNC(FR73FECINIVIG) AND NVL(TRUNC(FR73FECFINVIG),TO_DATE('31/12/9999','DD/MM/YYYY'))"
     
     .strOrder = " ORDER BY FR73DESPRODUCTO"
 
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
     objField.blnInDialog = False
     objField.blnInGrid = False
 
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Medicamento"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farm."
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Uni.Medida"
     
     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "V�a"
     
     Set objField = .AddField("FRG4CODFRECUENCIA_USU")
     objField.strSmallDesc = "Frecuencia"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
     
     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�d.Grupo Terape�tico"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(11), .cllValues("FR73CODPRODUCTO"))
      Call objWinInfo.CtrlSet(txtText1(55), .cllValues("FR73DESPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim marca As Variant
Dim strDelete As String
Dim stra As String
Dim strPathW As String
Dim rstaPathW As rdoResultset
Dim strFRH2pathword As String

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    'si la petici�n no tiene productos elimina dicha petici�n
    If IsNumeric(txtText1(0).Text) Then
      stra = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
          strDelete = "DELETE FR5500 WHERE FR55CODNECESUNID=" & txtText1(0).Text
          objApp.rdoConnect.Execute strDelete, 64
          objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If

    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR55CODNECESUNID_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 1) 'FR26CODESTPETIC, REDACTADA
    rsta.Close
    Set rsta = Nothing
    'txtText1(0).SetFocus
    
    sqlstr = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "'"
    sqlstr = sqlstr & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If Not rsta.EOF Then
      Call objWinInfo.CtrlSet(txtText1(5), rsta.rdoColumns(0).Value)
    End If
    rsta.Close
    Set rsta = Nothing
    Call objWinInfo.CtrlSet(txtText1(3), objsecurity.strUser)  'CODUSER
    dtcDateCombo1(0).Date = Date
    
    chkCheck1(0).Value = vbChecked
    
    'Call objWinInfo.CtrlGotFocus
    'Call objWinInfo.CtrlLostFocus
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle Necesidad" Then
      If IsNumeric(txtText1(0).Text) Then
        strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR2000 WHERE FR55CODNECESUNID=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          MsgBox "S�lo puede pedir una F�rmula Magistral", vbInformation, "Aviso"
          'linea = rstlinea.rdoColumns(0).Value + 1
          Exit Sub
        End If
        rstlinea.Close
        Set rstlinea = Nothing
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        If strBoton = "cmdFMnueva" Then
          txtText1(10).Text = linea
          txtText1(11).Text = "999999999"
          txtText1(57).Text = txtText1(3).Text & "_" & txtText1(0).Text & "_SER.DOC"
          txtText1(55).Locked = False
          txtText1(55).SetFocus
          CantidadBloqueada = False
          txtText1(49).BackColor = &HFFFF00
        Else
          txtText1(10).Text = linea
          'While txtText1(11).Text = ""
            Call objWinInfo_cwForeign("Detalle Necesidad", "txtText1(11)")
          'Wend
          If Trim(txtText1(11).Text) = "" Then
            objWinInfo.objWinActiveForm.blnChanged = False
          Else
            'se rellena la cantidad y dosis de la FM
            stra = "SELECT FR73DOSIS,FR93CODUNIMEDIDA FROM FR7300 WHERE "
            stra = stra & " FR73CODPRODUCTO=" & txtText1(11).Text
            Set rsta = objApp.rdoConnect.OpenResultset(stra)
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                txtText1(28) = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                txtText1(49) = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            rsta.Close
            Set rsta = Nothing
            txtText1(49).Locked = True
            txtText1(49).BackColor = &HC0C0C0
            CantidadBloqueada = True
          End If
          
          txtText1(55).Locked = True
        End If
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Grabar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Borrar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 80 'Imprimir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Deshacer
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(14))
  Case 30 'Cortar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(10))
  Case 40 'Copiar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(11))
  Case 50 'Pegar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(12))
  Case Else
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  End Select
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Poner Filtro
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(18))
  Case 20 'Quitar Filtro
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(19))
  Case Else
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  End Select
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Localizar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16))
  Case 40 'Primera
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Select Case intIndex
  Case 10 'Refrescar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  Case 20 'Mantenimiento
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(28))
  Case Else
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End Select
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtText1_DblClick(Index As Integer)
If CantidadBloqueada = False Then
  If objWinInfo.CtrlGetInfo(txtText1(Index)).blnForeign Then
      If stbStatusBar1.Panels(2).Text = "Lista" Then
        Call objWinInfo_cwForeign(objWinInfo.objWinActiveForm.strName, "txtText1(" & Index & ")")
      End If
  End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)

If CantidadBloqueada = True Then
    If (intIndex = 49 Or intIndex = 55) Then
      txtText1(49).Locked = True
      txtText1(55).Locked = True
    End If
Else
    txtText1(49).Locked = False
    txtText1(55).Locked = False
End If

Call objWinInfo.CtrlGotFocus
   
End Sub
Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rsta As rdoResultset
Dim stra As String

Call objWinInfo.CtrlDataChange

If intIndex = 5 Then
  If IsNumeric(txtText1(5).Text) Then
    stra = "SELECT COUNT(*) FROM AD4100 WHERE AD02CODDPTO=" & txtText1(5).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value = 0 Then
        txtText1(7).Visible = False
        txtText1(8).Visible = False
        lblLabel1(1).Visible = False
        lblLabel1(2).Visible = False
        txtText1(7).Text = ""
    Else
        txtText1(7).Visible = True
        txtText1(8).Visible = True
        lblLabel1(1).Visible = True
        lblLabel1(2).Visible = True
    End If
  End If
End If

End Sub


