VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************
Const PRWinRegAdministr               As String = "FR0137"
Const PRWinVerPerfilFTP               As String = "FR0106"
Const PRWinFirmarOMPRNPLA             As String = "FR0121"
Const PRWinRedPedStPlanta             As String = "FR0131"
Const PRWinFirmarPedStPta             As String = "FR0132"
Const PRWinBuscarProdPLA              As String = "FR0164"
Const PRWinBuscarGruProdPLA           As String = "FR0120"
Const FRWinVisPRN                     As String = "FR0724"
Const FRWinVisOM                      As String = "FR0778"
Const FRWinValPedPta                  As String = "FR0735"
Const FRWinVerPedPta                  As String = "FR0736"
Const FRWinConsPetFarm                As String = "FR0725"
Const FRWinConsOM                     As String = "FR0779"
Const FRWinRedPedStPlantaEstupefac    As String = "FR0780"
Const FRWinRedPedFMPlanta             As String = "FR0781"

'informes
Const FRRepFaltasCestas               As String = "FR1243"
Const FRRepFaltasPRNs                 As String = "FR1244"
Const FRRepRepFaltasPla               As String = "FR1352"
Const FRRepFaltasCestasSec            As String = "FR1246"
Const FRRepRepFaltasPlaSec            As String = "FR1354"



Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean
Dim intTipoVar As Integer

  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
  Case PRWinRegAdministr
      Load FrmRegAdministr
      'Call objsecurity.AddHelpContext(528)
      Call FrmRegAdministr.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload FrmRegAdministr
      Set FrmRegAdministr = Nothing
  Case PRWinVerPerfilFTP
      Load frmVerPerfilFTP
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTP.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTP
      Set frmVerPerfilFTP = Nothing
      glngPaciente = 0
  Case PRWinFirmarOMPRNPLA
      Load frmFirmarOMPRNPLA
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarOMPRNPLA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarOMPRNPLA
      Set frmFirmarOMPRNPLA = Nothing
  Case PRWinRedPedStPlanta
      Load FrmRedPedStPlanta
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedPedStPlanta.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmRedPedStPlanta
      Set FrmRedPedStPlanta = Nothing
      gblnNoDesdeMenu = False
  Case PRWinFirmarPedStPta
      Load frmFirmarPedStPta
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarPedStPta.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmFirmarPedStPta
      Set frmFirmarPedStPta = Nothing
   Case PRWinBuscarProdPLA
      Load frmBusProductosPLA
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosPLA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusProductosPLA
      Set frmBusProductosPLA = Nothing
    Case PRWinBuscarGruProdPLA
      Load frmBusGrpProdPLA
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProdPLA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProdPLA
      Set frmBusGrpProdPLA = Nothing
    Case FRWinVisOM
      Load frmVisOM
      Call frmVisOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVisOM
      Set frmVisOM = Nothing
    Case FRWinVisPRN
      Load frmVisPRN
      Call frmVisPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVisPRN
      Set frmVisPRN = Nothing
    Case FRWinValPedPta
      Load frmValPedPtaFarma
      Call frmValPedPtaFarma.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmValPedPtaFarma
      Set frmValPedPtaFarma = Nothing
    Case FRWinVerPedPta
      Load FrmVerPedPtaNoVal
      Call FrmVerPedPtaNoVal.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload FrmVerPedPtaNoVal
      Set FrmVerPedPtaNoVal = Nothing
    Case FRWinConsPetFarm
      intTipoVar = VarType(vntData)
      
      If (intTipoVar <> 10) Then
        If (vntData(1) = "VisPRNOM") Then
          gstrLlamadorProd = vntData(1)
          glngpeticion = vntData(2)
        End If
      End If
    
      Load frmEstPetFarm
      Call frmEstPetFarm.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetFarm
      Set frmEstPetFarm = Nothing
    Case FRWinConsOM
      Load frmEstPetFarmOM
      Call frmEstPetFarmOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetFarmOM
      Set frmEstPetFarmOM = Nothing
      
    Case FRWinRedPedStPlantaEstupefac
      Load FrmRedPedStPlantaEstupefac
      Call FrmRedPedStPlantaEstupefac.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload FrmRedPedStPlantaEstupefac
      Set FrmRedPedStPlantaEstupefac = Nothing
      
    Case FRWinRedPedFMPlanta
      Load FrmRedPedFMPlanta
      Call FrmRedPedFMPlanta.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload FrmRedPedFMPlanta
      Set FrmRedPedFMPlanta = Nothing
      
      
  End Select
  Call err.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 21, 1 To 4) As Variant
  
  aProcess(1, 1) = PRWinRegAdministr
  aProcess(1, 2) = "Registrar Administraci�n"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinVerPerfilFTP
  aProcess(2, 2) = "Consultar Perfil FTP"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinFirmarOMPRNPLA
  aProcess(3, 2) = "Firmar OM/PRN"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinRedPedStPlanta
  aProcess(4, 2) = "Redactar Pedido Farmacia"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinFirmarPedStPta
  aProcess(5, 2) = "Firmar Pedido Farmacia"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinBuscarProdPLA
  aProcess(6, 2) = "Buscar Productos"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinBuscarGruProdPLA
  aProcess(7, 2) = "Buscar Grupo de Productos"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = FRWinVisPRN
  aProcess(8, 2) = "Consultar PRN y Cestas"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = FRWinVisOM
  aProcess(9, 2) = "Consultar OM"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = FRWinValPedPta
  aProcess(10, 2) = "Consultar Peticiones de Planta"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = FRWinVerPedPta
  aProcess(11, 2) = "Ver Peticion de Planta"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = FRWinConsPetFarm
  aProcess(12, 2) = "Ver PRN/Cesta"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = FRWinConsOM
  aProcess(13, 2) = "Ver OM"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = FRWinRedPedStPlantaEstupefac
  aProcess(14, 2) = "Redactar Pedido Farmacia Estupefacientes"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = FRWinRedPedFMPlanta
  aProcess(15, 2) = "Redactar Pedido F�rmula Magistral"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
  
  
  
  'informes
  aProcess(16, 1) = FRRepFaltasCestas
  aProcess(16, 2) = "Faltas de Cestas Dpto./Fecha"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeReport

  aProcess(17, 1) = FRRepFaltasPRNs
  aProcess(17, 2) = "Faltas de PRNs Dpto./Fecha"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeReport
  
  aProcess(18, 1) = FRRepRepFaltasPla
  aProcess(18, 2) = "Faltas de Plantas Dpto./Fecha"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeReport

  aProcess(19, 1) = FRRepFaltasCestasSec
  aProcess(19, 2) = "Faltas de Cestas Dpto./Sec/Fecha"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeReport
  
  aProcess(20, 1) = FRRepRepFaltasPlaSec
  aProcess(20, 2) = "Faltas de Plantas Dpto./Sec/Fecha"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeReport

End Sub
