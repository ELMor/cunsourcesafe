VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

Const PRWinDefCarro                   As String = "FR0040"
Const PRWinBuscarCamas                As String = "FR0051"




Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PRWinDefCarro
      Load frmDefCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmDefCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefCarro
      Set frmDefCarro = Nothing
    Case PRWinBuscarCamas
      Load frmBuscarCamas
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarCamas.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarCamas
      Set frmBuscarCamas = Nothing
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 3, 1 To 4) As Variant
      
  aProcess(1, 1) = PRWinDefCarro
  aProcess(1, 2) = "Definici�n de Carro"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinBuscarCamas
  aProcess(2, 2) = "Buscar Camas"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
End Sub
