Attribute VB_Name = "DEF001"
Option Explicit

Public objsecurity As clsCWSecurity
Public objmouse As clsCWMouse

Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
'gintprodbuscado es un array que guarda los c�digos de los productos de un protocolo
Public gintprodbuscado()
'Public gintcamabuscada()
Public gintpricipioactivobuscado()
'gintprodtotal guarda el n�mero total de productos seleccionados
Public gintprodtotal As Integer
'Public gintcamatotal
Public gintpricipioactivototal As Integer
Public gintbuscargruprot As Integer
Public gintbuscargruprod As Integer
Public gstrllamador As String
Public gstrLista As String
Public glngPaciente As Long
Public gintfirmarOM As Integer
Public glngpeticion As Long
Public gintservicio As Long
Public gintlinea As Integer
Public gintbusprod As Integer
Public gvntdatafr0105(1 To 9) As Variant
Public gintprincact As Integer

Public gintCodigoGrupo As Long
Public Const gstrversion = "20000315"



Sub Main()
End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub



'Funci�n que devuelve el separador decimal que se corresponde con
'el de la configuraci�n regional de la m�quina.
Public Function SeparadorDec() As String

If Format("12,345", "00.000") = "12,345" Then
  SeparadorDec = ","
Else
  SeparadorDec = "."
End If

End Function

Public Function intCuentaCaracteres(ByVal strCaracter As String, ByVal strCadena As String) As Integer
  Dim intCuenta As Integer
  Dim intPos As Variant
  Dim strSubCadena As String
  Dim Fin As Boolean
  
  Fin = False
  intCuenta = 0
  intPos = 0
  strSubCadena = strCadena
  While Not Fin
    intPos = InStr(strSubCadena, strCaracter)
    If intPos = 0 Then
      Fin = True
    Else
      intCuenta = intCuenta + 1
      strSubCadena = Right(strSubCadena, Len(strSubCadena) - intPos)
    End If
  Wend
  intCuentaCaracteres = intCuenta
End Function

