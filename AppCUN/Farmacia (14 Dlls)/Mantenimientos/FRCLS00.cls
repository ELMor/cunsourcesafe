VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************



'************************************************************************************
'LISTADOS
Const FRRepActProv                    As String = "FRR011" 'Actividades de Proveedor
Const FRRepAlergias                   As String = "FRR021" 'Alergias
Const FRRepCarros                     As String = "FRR051" 'Carros
Const FRRepCentroCoste                As String = "FRR061" 'Centro de Coste
Const FRRepEstPetOMPRN                As String = "FRR071" 'Estado Petici�n OM/PRN
Const FRRepTipObserv                  As String = "FR0081" 'Tipo Observaci�n
Const FRRepTipVia                     As String = "FR0091" 'Tipo V�a
Const FRRepTipInterMed                As String = "FRR111" 'Tipo Interacci�n Medicamentosa
Const FRRepABCProd                    As String = "FRR131" 'ABC Producto
Const FRRepEstCarro                   As String = "FRR151" 'Estados del Carro
Const FRRepABCProv                    As String = "FR0161" 'ABC Proveedor
Const FRRepTipIVA                     As String = "FR0171" 'Tipo de IVA
Const FRRepTipMov                     As String = "FR0181" 'Tipo de Movimiento
Const FRRepTipUrg                     As String = "FR0191" 'Tipo de Urgencias
Const FRRepUniMed                     As String = "FRR201" 'Unidades de Medida
Const FRRepTipAler                    As String = "FR0221" 'Tipo de Alerta
Const FRRepEstOfeProv                 As String = "FRR231" 'Estado Oferta Proveedor
Const FRRepProgCarro                  As String = "FRR241" 'Programaci�n Carro
Const FRRepParamGen                   As String = "FRR451" 'Par�metros Generales
Const FRRepNaturaleza                 As String = "FRR751" 'Naturaleza de las Interacciones
Const FRRepPeriodicidad               As String = "FRR681" 'Periodicidad
Const FRRepQuirofano                  As String = "FRR591" 'Quir�fano
Const FRRepUbicacion                  As String = "FRR711" 'Ubicaciones
Const FRRepConvUni                    As String = "FRR561" 'Conversi�n entre Unidades
Const FRRepMonedas                    As String = "FRR691" 'Monedas
Const FRRepFrecuencias                As String = "FRR571" 'Frecuencias de Administraci�n
Const FRRepFormasFarmaceuticas        As String = "FRR701" 'Formas Farmac�uticas

'MANTENIMIENTO
Const PRWinMantActProv                As String = "FR0001"
Const PRWinMantAlergia                As String = "FR0002"
Const PRWinMantCarro                  As String = "FR0005"
Const PRWinMantCentroCoste            As String = "FR0006"
Const PRWinMantEstaPeticion           As String = "FR0007"
Const PRWinMantTipoObserv             As String = "FR0008"
Const PRWinMantTipoVia                As String = "FR0009"
Const PRWinMantTipoInter              As String = "FR0011"
Const PRWinMantTipoABCProd            As String = "FR0013"
Const PRWinMantEstadoCarro            As String = "FR0015"
Const PRWinMantABCProveedor           As String = "FR0016"
Const PRWinMantIVA                    As String = "FR0017"
Const PRWinMantMovimiento             As String = "FR0018"
Const PRWinMantUrgencias              As String = "FR0019"
Const PRWinMantUnidadesMedida         As String = "FR0020"
Const PRWinMantPedidoCompra           As String = "FR0021"
Const PRWinMantAlerta                 As String = "FR0022"
Const PRWinMantEstadoOferta           As String = "FR0023"
Const PRWinMantProgramacionCarro      As String = "FR0024"
Const PRWinMantObservFarma            As String = "FR0027"
Const PRWinMantEnvase                 As String = "FR0033"
Const PRWinMantExpediente             As String = "FR0034"
Const PRWinMantEstOF                  As String = "FR0055"
Const PRWinMantConvUnidad             As String = "FR0056"
Const PRWinMantFrecuencias            As String = "FR0057"
Const PRWinDefGrpTerap                As String = "FR0058"
Const PRWinMantQuirofano              As String = "FR0059"
Const PRWinMantOrigDest               As String = "FR0060"
'Const PRWinMantCGCF                   As String = "FR0062"
Const PRWinMantPeriod                 As String = "FR0068"
Const PRWinMantMonedas                As String = "FR0069"
Const PRWinMantFormFarma              As String = "FR0070"
Const PRWinMantUbicaciones            As String = "FR0071"
Const PRWinMantAcciones               As String = "FR0074"
Const PRWinMantNaturaleza             As String = "FR0075"
Const PRWinMantTipoGrp                As String = "FR0077"
Const PRWinMantTipoProtQuirof         As String = "FR0078"
Const PRWinMantInstAdmin              As String = "FR0079"
'Const PRWinActCGCF                    As String = "FR0039"


'DEFINIR
Const PRWinDefProgCarro               As String = "FR0052"

'DEFINIR PAR�METROS
Const PRWinDefParamGen                As String = "FR0045"




Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    'MANTENIMIENTO
      
    Case PRWinMantAlergia
      Load frmMantAlergia
      'Call objsecurity.AddHelpContext(528)
      Call frmMantAlergia.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAlergia
      Set frmMantAlergia = Nothing
    Case PRWinMantEstOF
      Load frmMantEstOF
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstOF.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstOF
      Set frmMantEstOF = Nothing
    Case PRWinMantCarro
      Load frmMantCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmMantCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantCarro
      Set frmMantCarro = Nothing
    Case PRWinMantCentroCoste
      Load frmMantCentroCoste
      'Call objsecurity.AddHelpContext(528)
      Call frmMantCentroCoste.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantCentroCoste
      Set frmMantCentroCoste = Nothing
    Case PRWinMantEstaPeticion
      Load frmMantEstaPeticion
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstaPeticion.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstaPeticion
      Set frmMantEstaPeticion = Nothing
    Case PRWinMantTipoObserv
      Load frmMantTipoObserv
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoObserv.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoObserv
      Set frmMantTipoObserv = Nothing
    Case PRWinMantTipoVia
      Load frmMantTipoVia
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoVia.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoVia
      Set frmMantTipoVia = Nothing
    Case PRWinMantTipoInter
      Load frmMantTipoInter
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoInter.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoInter
      Set frmMantTipoInter = Nothing
    Case PRWinMantTipoABCProd
      Load frmMantTipoABCProd
      'Call objsecurity.AddHelpContext(528)
      Call frmMantTipoABCProd.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoABCProd
      Set frmMantTipoABCProd = Nothing
    Case PRWinMantEstadoCarro
      Load frmMantEstadoCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstadoCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstadoCarro
      Set frmMantEstadoCarro = Nothing
    Case PRWinMantABCProveedor
      Load frmMantABCProveedor
      'Call objsecurity.AddHelpContext(528)
      Call frmMantABCProveedor.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantABCProveedor
      Set frmMantABCProveedor = Nothing
    Case PRWinMantIVA
      Load frmMantIVA
      'Call objsecurity.AddHelpContext(528)
      Call frmMantIVA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantIVA
      Set frmMantIVA = Nothing
    Case PRWinMantMovimiento
      Load frmMantMovimiento
      'Call objsecurity.AddHelpContext(528)
      Call frmMantMovimiento.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantMovimiento
      Set frmMantMovimiento = Nothing
    Case PRWinMantUrgencias
      Load frmMantUrgencias
      'Call objsecurity.AddHelpContext(528)
      Call frmMantUrgencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantUrgencias
      Set frmMantUrgencias = Nothing
    Case PRWinMantUnidadesMedida
      Load frmMantUnidadesMedida
      'Call objsecurity.AddHelpContext(528)
      Call frmMantUnidadesMedida.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantUnidadesMedida
      Set frmMantUnidadesMedida = Nothing
    Case PRWinMantPedidoCompra
      Load frmMantPedidoCompra
      'Call objsecurity.AddHelpContext(528)
      Call frmMantPedidoCompra.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantPedidoCompra
      Set frmMantPedidoCompra = Nothing
    Case PRWinMantAlerta
      Load frmMantAlerta
      'Call objsecurity.AddHelpContext(528)
      Call frmMantAlerta.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAlerta
      Set frmMantAlerta = Nothing
    Case PRWinMantEstadoOferta
      Load frmMantEstadoOferta
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEstadoOferta.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEstadoOferta
      Set frmMantEstadoOferta = Nothing
    Case PRWinMantProgramacionCarro
      Load frmMantProgramacionCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmMantProgramacionCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantProgramacionCarro
      Set frmMantProgramacionCarro = Nothing
    Case PRWinMantObservFarma
      Load frmMantObservFarma
      'Call objsecurity.AddHelpContext(528)
      Call frmMantObservFarma.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantObservFarma
      Set frmMantObservFarma = Nothing
    Case PRWinMantEnvase
      Load frmMantEnvase
      'Call objsecurity.AddHelpContext(528)
      Call frmMantEnvase.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantEnvase
      Set frmMantEnvase = Nothing
    Case PRWinMantExpediente
      Load frmMantExpediente
      'Call objsecurity.AddHelpContext(528)
      Call frmMantExpediente.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantExpediente
      Set frmMantExpediente = Nothing
    Case PRWinMantQuirofano
       Load frmMantQuirofano
      'Call objsecurity.AddHelpContext(528)
      Call frmMantQuirofano.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantQuirofano
      Set frmMantQuirofano = Nothing
    Case PRWinDefGrpTerap
       Load frmDefGrpTerap
      'Call objsecurity.AddHelpContext(528)
      Call frmDefGrpTerap.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefGrpTerap
      Set frmDefGrpTerap = Nothing
    Case PRWinMantFrecuencias
       Load frmMantFrecuencias
      'Call objsecurity.AddHelpContext(528)
      Call frmMantFrecuencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantFrecuencias
      Set frmMantFrecuencias = Nothing
    Case PRWinMantConvUnidad
       Load frmMantConvUnidad
      'Call objsecurity.AddHelpContext(528)
      Call frmMantConvUnidad.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantConvUnidad
      Set frmMantConvUnidad = Nothing
    Case PRWinMantOrigDest
       Load frmMantOrigDest
      'Call objsecurity.AddHelpContext(528)
      Call frmMantOrigDest.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantOrigDest
      Set frmMantOrigDest = Nothing
     'Case PRWinMantCGCF
     '  Load frmMantCGCF
     ' 'Call objsecurity.AddHelpContext(528)
     ' Call frmMantCGCF.Show(vbModal)
     ' Call objsecurity.RemoveHelpContext
     ' Unload frmMantCGCF
     ' Set frmMantCGCF = Nothing
    Case PRWinMantPeriod
       Load frmMantPeriod
      'Call objsecurity.AddHelpContext(528)
      Call frmMantPeriod.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantPeriod
      Set frmMantPeriod = Nothing
    Case PRWinMantMonedas
       Load frmMantMonedas
      'Call objsecurity.AddHelpContext(528)
      Call frmMantMonedas.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantMonedas
      Set frmMantMonedas = Nothing
    Case PRWinMantFormFarma
       Load frmMantFormFarma
      'Call objsecurity.AddHelpContext(528)
      Call frmMantFormFarma.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantFormFarma
      Set frmMantFormFarma = Nothing
    Case PRWinMantUbicaciones
       Load frmMantUbicaciones
      'Call objsecurity.AddHelpContext(528)
      Call frmMantUbicaciones.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantUbicaciones
      Set frmMantUbicaciones = Nothing
    Case PRWinMantAcciones
      Load frmMantAcciones
      Call frmMantAcciones.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAcciones
      Set frmMantAcciones = Nothing
    Case PRWinMantNaturaleza
      Load frmMantNaturaleza
      Call frmMantNaturaleza.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantNaturaleza
      Set frmMantNaturaleza = Nothing
    Case PRWinMantInstAdmin
      Load frmMantInstAdmin
      Call frmMantInstAdmin.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantInstAdmin
      Set frmMantInstAdmin = Nothing
    Case PRWinMantTipoGrp
      Load frmMantTipoGrp
      Call frmMantTipoGrp.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoGrp
      Set frmMantTipoGrp = Nothing
    Case PRWinMantTipoProtQuirof
      Load frmMantTipoProtQuirof
      Call frmMantTipoProtQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantTipoProtQuirof
      Set frmMantTipoProtQuirof = Nothing
    'DEFINIR
    Case PRWinDefProgCarro
      Load frmDefProgCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProgCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefProgCarro
      Set frmDefProgCarro = Nothing
    'Case PRWinActCGCF
    '  Load frmActCGCF
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmActCGCF.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmActCGCF
    '  Set frmActCGCF = Nothing
    'DEFINIR PAR�METROS
    Case PRWinDefParamGen
      Load frmDefParamGen
      'Call objsecurity.AddHelpContext(528)
      Call frmDefParamGen.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefParamGen
      Set frmDefParamGen = Nothing
    
  End Select
  Call ERR.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 115, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
  aProcess(2, 1) = PRWinMantAlergia
  aProcess(2, 2) = "Mantenimiento Alergia"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow

  'aProcess(3, 1) = PRWinMantAlmacen
  'aProcess(3, 2) = "Mantenimiento Almac�n"
  'aProcess(3, 3) = True
  'aProcess(3, 4) = cwTypeWindow

  aProcess(4, 1) = PRWinMantCarro
  aProcess(4, 2) = "Mantenimiento Carro"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow

  aProcess(5, 1) = PRWinMantCentroCoste
  aProcess(5, 2) = "Mantenimiento Centro Coste"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow

  aProcess(6, 1) = PRWinMantEstaPeticion
  aProcess(6, 2) = "Mantenimiento Estado Petici�n"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow

  aProcess(7, 1) = PRWinMantTipoObserv
  aProcess(7, 2) = "Mantenimiento Tipo Observaci�n"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow

  aProcess(8, 1) = PRWinMantTipoVia
  aProcess(8, 2) = "Mantenimiento Tipo V�a"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow

  aProcess(10, 1) = PRWinMantTipoInter
  aProcess(10, 2) = "Mantenimiento Tipo Interacci�n"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = PRWinMantTipoABCProd
  aProcess(11, 2) = "Mantenimiento Tipo ABC Producto"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinMantEstadoCarro
  aProcess(13, 2) = "Mantenimiento Estado Carro"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWinMantABCProveedor
  aProcess(14, 2) = "Mantenimiento Tipo ABC Proveedor"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWinMantIVA
  aProcess(15, 2) = "Mantenimiento Tipo IVA"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinMantMovimiento
  aProcess(16, 2) = "Mantenimiento Tipo Movimiento"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = PRWinMantUrgencias
  aProcess(17, 2) = "Mantenimiento Tipo Urgencias"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinMantUnidadesMedida
  aProcess(18, 2) = "Mantenimiento Unidades de Medida "
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinMantPedidoCompra
  aProcess(19, 2) = "Mantenimiento Estado Pedido de Compra"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinMantAlerta
  aProcess(20, 2) = "Mantenimiento Tipo de Alerta"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinMantEstadoOferta
  aProcess(21, 2) = "Mantenimiento Estado Oferta"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinMantProgramacionCarro
  aProcess(22, 2) = "Mantenimiento Programaci�n Carro"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow

  aProcess(24, 1) = PRWinMantObservFarma
  aProcess(24, 2) = "Mantenimiento Observaciones Farmacia"
  aProcess(24, 3) = True
  aProcess(24, 4) = cwTypeWindow
   
  aProcess(30, 1) = PRWinMantEnvase
  aProcess(30, 2) = "Mantenimiento Envases"
  aProcess(30, 3) = True
  aProcess(30, 4) = cwTypeWindow
  
  aProcess(31, 1) = PRWinMantExpediente
  aProcess(31, 2) = "Mantenimiento Expedientes"
  aProcess(31, 3) = True
  aProcess(31, 4) = cwTypeWindow
  
  aProcess(33, 1) = PRWinMantEstOF
  aProcess(33, 2) = "Mantenimiento Estado Orden Fabricaci�n"
  aProcess(33, 3) = True
  aProcess(33, 4) = cwTypeWindow
  
  aProcess(34, 1) = PRWinMantQuirofano
  aProcess(34, 2) = "Mantenimiento Quir�fanos"
  aProcess(34, 3) = True
  aProcess(34, 4) = cwTypeWindow
  
  aProcess(35, 1) = PRWinMantConvUnidad
  aProcess(35, 2) = "Mantenimiento Conversi�n de Unidad"
  aProcess(35, 3) = True
  aProcess(35, 4) = cwTypeWindow
  
  aProcess(36, 1) = PRWinMantFrecuencias
  aProcess(36, 2) = "Mantenimiento Frecuencias"
  aProcess(36, 3) = True
  aProcess(36, 4) = cwTypeWindow
  
  aProcess(37, 1) = PRWinMantOrigDest
  aProcess(37, 2) = "Mantenimiento Origen Destino"
  aProcess(37, 3) = True
  aProcess(37, 4) = cwTypeWindow

  'DEFINIR
  
  aProcess(41, 1) = PRWinDefGrpTerap
  aProcess(41, 2) = "Definir Grupos Terape�ticos"
  aProcess(41, 3) = True
  aProcess(41, 4) = cwTypeWindow

  aProcess(43, 1) = PRWinDefProgCarro
  aProcess(43, 2) = "Definici�n Programaci�n Carro"
  aProcess(43, 3) = False
  aProcess(43, 4) = cwTypeWindow
  
  'aProcess(49, 1) = PRWinActCGCF
  'aProcess(49, 2) = "Actualizar CGCF"
  'aProcess(49, 3) = True
  'aProcess(49, 4) = cwTypeWindow
  
  'aProcess(52, 1) = PRWinMantCGCF
  'aProcess(52, 2) = "Mantenimiento CGCF"
  'aProcess(52, 3) = True
  'aProcess(52, 4) = cwTypeWindow
  
  'DEFINIR PAR�METROS
  aProcess(53, 1) = PRWinDefParamGen
  aProcess(53, 2) = "Definir Par�metros Generales"
  aProcess(53, 3) = True
  aProcess(53, 4) = cwTypeWindow
  
  
  'MANTENIMIENTOS
  aProcess(60, 1) = PRWinMantPeriod
  aProcess(60, 2) = "Mantenimiento Periodicidades"
  aProcess(60, 3) = True
  aProcess(60, 4) = cwTypeWindow

  aProcess(63, 1) = PRWinMantMonedas
  aProcess(63, 2) = "Mantenimiento Monedas"
  aProcess(63, 3) = True
  aProcess(63, 4) = cwTypeWindow
  
  aProcess(64, 1) = PRWinMantFormFarma
  aProcess(64, 2) = "Mantenimiento Formas Farmace�ticas"
  aProcess(64, 3) = True
  aProcess(64, 4) = cwTypeWindow
  
  aProcess(65, 1) = PRWinMantUbicaciones
  aProcess(65, 2) = "Mantenimiento Ubicaciones"
  aProcess(65, 3) = True
  aProcess(65, 4) = cwTypeWindow

  aProcess(68, 1) = PRWinMantAcciones
  aProcess(68, 2) = "Mantenimiento Acciones"
  aProcess(68, 3) = True
  aProcess(68, 4) = cwTypeWindow
  
  aProcess(69, 1) = PRWinMantNaturaleza
  aProcess(69, 2) = "Mantenimiento Naturaleza Interacciones"
  aProcess(69, 3) = True
  aProcess(69, 4) = cwTypeWindow
  
  aProcess(71, 1) = PRWinMantTipoGrp
  aProcess(71, 2) = "Mantenimiento Tipos de Grupos"
  aProcess(71, 3) = True
  aProcess(71, 4) = cwTypeWindow

  aProcess(73, 1) = PRWinMantTipoProtQuirof
  aProcess(73, 2) = "Mantenimiento Tipos de Protocolos de Quir�fano"
  aProcess(73, 3) = True
  aProcess(73, 4) = cwTypeWindow
  
  aProcess(112, 1) = PRWinMantInstAdmin
  aProcess(112, 2) = "Mantenimiento Instrucciones de Admninistraci�n"
  aProcess(112, 3) = True
  aProcess(112, 4) = cwTypeWindow
  
  
  
  
  'Informes
  aProcess(74, 1) = FRRepActProv
  aProcess(74, 2) = "Actividades de Proveedor"
  aProcess(74, 3) = False
  aProcess(74, 4) = cwTypeReport
  
  aProcess(75, 1) = FRRepAlergias
  aProcess(75, 2) = "Alergias"
  aProcess(75, 3) = False
  aProcess(75, 4) = cwTypeReport

  aProcess(77, 1) = FRRepCarros
  aProcess(77, 2) = "Carros"
  aProcess(77, 3) = False
  aProcess(77, 4) = cwTypeReport
  
  aProcess(78, 1) = FRRepCentroCoste
  aProcess(78, 2) = "Centros de Coste"
  aProcess(78, 3) = False
  aProcess(78, 4) = cwTypeReport

  aProcess(79, 1) = FRRepEstPetOMPRN
  aProcess(79, 2) = "Estado de la Petici�n de OM/PRN"
  aProcess(79, 3) = False
  aProcess(79, 4) = cwTypeReport

  aProcess(80, 1) = FRRepTipObserv
  aProcess(80, 2) = "Tipos de Observaci�n"
  aProcess(80, 3) = False
  aProcess(80, 4) = cwTypeReport

  aProcess(81, 1) = FRRepTipVia
  aProcess(81, 2) = "Tipos de V�a"
  aProcess(81, 3) = False
  aProcess(81, 4) = cwTypeReport

  aProcess(82, 1) = FRRepTipInterMed
  aProcess(82, 2) = "Tipos Interacci�n Medicamentosa"
  aProcess(82, 3) = False
  aProcess(82, 4) = cwTypeReport

  aProcess(83, 1) = FRRepABCProd
  aProcess(83, 2) = "ABC del Producto"
  aProcess(83, 3) = False
  aProcess(83, 4) = cwTypeReport

  aProcess(84, 1) = FRRepEstCarro
  aProcess(84, 2) = "Estados del Carro"
  aProcess(84, 3) = False
  aProcess(84, 4) = cwTypeReport

  aProcess(85, 1) = FRRepABCProv
  aProcess(85, 2) = "ABC Proveedor"
  aProcess(85, 3) = False
  aProcess(85, 4) = cwTypeReport

  aProcess(86, 1) = FRRepTipIVA
  aProcess(86, 2) = "Tipo de IVA"
  aProcess(86, 3) = False
  aProcess(86, 4) = cwTypeReport

  aProcess(87, 1) = FRRepTipMov
  aProcess(87, 2) = "Tipos de Movimiento"
  aProcess(87, 3) = False
  aProcess(87, 4) = cwTypeReport

  aProcess(88, 1) = FRRepTipUrg
  aProcess(88, 2) = "Tipo de Urgencia"
  aProcess(88, 3) = False
  aProcess(88, 4) = cwTypeReport
  
  aProcess(89, 1) = FRRepUniMed
  aProcess(89, 2) = "Unidades de Medida"
  aProcess(89, 3) = False
  aProcess(89, 4) = cwTypeReport
  
  aProcess(90, 1) = FRRepTipAler
  aProcess(90, 2) = "Tipo de Alerta"
  aProcess(90, 3) = False
  aProcess(90, 4) = cwTypeReport

  aProcess(91, 1) = FRRepEstOfeProv
  aProcess(91, 2) = "Estado Oferta Proveedor"
  aProcess(91, 3) = False
  aProcess(91, 4) = cwTypeReport
  
  aProcess(92, 1) = FRRepProgCarro
  aProcess(92, 2) = "Programaci�n del Carro"
  aProcess(92, 3) = False
  aProcess(92, 4) = cwTypeReport
  
  aProcess(100, 1) = FRRepParamGen
  aProcess(100, 2) = "Par�metros Genreales"
  aProcess(100, 3) = False
  aProcess(100, 4) = cwTypeReport

  aProcess(103, 1) = FRRepNaturaleza
  aProcess(103, 2) = "Naturaleza de las Interacciones"
  aProcess(103, 3) = False
  aProcess(103, 4) = cwTypeReport

  aProcess(105, 1) = FRRepPeriodicidad
  aProcess(105, 2) = "Periodicidad"
  aProcess(105, 3) = False
  aProcess(105, 4) = cwTypeReport
  
  aProcess(106, 1) = FRRepQuirofano
  aProcess(106, 2) = "Quir�fanos"
  aProcess(106, 3) = False
  aProcess(106, 4) = cwTypeReport

  aProcess(107, 1) = FRRepUbicacion
  aProcess(107, 2) = "Ubicaciones"
  aProcess(107, 3) = False
  aProcess(107, 4) = cwTypeReport

  aProcess(108, 1) = FRRepConvUni
  aProcess(108, 2) = "Conversi�n de Unidades"
  aProcess(108, 3) = False
  aProcess(108, 4) = cwTypeReport

  aProcess(109, 1) = FRRepMonedas
  aProcess(109, 2) = "Monedas"
  aProcess(109, 3) = False
  aProcess(109, 4) = cwTypeReport
  
  aProcess(110, 1) = FRRepFrecuencias
  aProcess(110, 2) = "Frecuencias de Administraci�n"
  aProcess(110, 3) = False
  aProcess(110, 4) = cwTypeReport

  aProcess(111, 1) = FRRepFormasFarmaceuticas
  aProcess(111, 2) = "Formas Farmac�uticas"
  aProcess(111, 3) = False
  aProcess(111, 4) = cwTypeReport

  
End Sub

