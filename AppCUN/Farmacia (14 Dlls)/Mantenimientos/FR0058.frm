VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmDefGrpTerap 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Grupo Terape�tico."
   ClientHeight    =   8220
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11805
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0058.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8220
   ScaleWidth      =   11805
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Grupos Terape�ticos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7200
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6495
         Index           =   0
         Left            =   360
         TabIndex        =   5
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   11456
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         OLEDragMode     =   1
         OLEDropMode     =   1
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Grupo Terape�tico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7215
      Index           =   2
      Left            =   5040
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   6735
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6495
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   480
         Width           =   6450
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   11377
         _ExtentY        =   11456
         _StockProps     =   79
         Caption         =   "GRUPO TERAPEUTICO"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   7935
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefGrpTerap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefProtocolos (FR0058.FRM)                                *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: ENERO DE 1999                                                 *
'* DESCRIPCION: Definir Grupos Terapeuticos                             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
' Declara variables globales.
Dim Enarbol As Boolean
Dim Enabrir As Boolean
Dim EnPostWrite As Boolean

Dim indrag As Boolean 'Indicador de operaci�n de arrastrar y colocar.
Dim nodX As Object ' Elemento que se arrastra.

Dim gstrCursor As String
Dim glngTamano As Long
Dim engrid As Boolean
Dim AuxNodoActual As String

Private Sub Rellenar_Componentes(ByVal vntpadre As Variant, _
                                 ByVal vntgrupo As Variant, _
                                 ByVal vntdesgrupo As Variant)
  'Dim strRelprod As String
  'Dim rstRelprod As rdoResultset
  'Dim strdesProd As String
  'Dim rstdesProd As rdoResultset
  'Dim vntProd As Variant
  'Dim vntdesProd As Variant
  'Dim vntpadre As Variant
  Dim vntcompHijo As Variant
  Dim vntdesCompHijo As Variant
  Dim qrygrupo As rdoQuery
  Dim strgrupo As String
  Dim rstgrupo As rdoResultset
  
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'strgrupo = "SELECT * FROM FR0000 WHERE fr00codgrpterap_pad='" & vntgrupo & "' order by fr00codgrpterap"
    strgrupo = "SELECT * FROM FR0000 WHERE fr00codgrpterap_pad=? order by fr00codgrpterap"
    Set qrygrupo = objApp.rdoConnect.CreateQuery("", strgrupo)
    qrygrupo(0) = vntgrupo
    Set rstgrupo = qrygrupo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    'Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
    
    If vntpadre = "grup" Then
        vntpadre = vntpadre & vntgrupo
    Else
        vntpadre = vntpadre & "/" & vntgrupo
    End If
    If rstgrupo.EOF = False Then
        While rstgrupo.EOF = False
            vntcompHijo = rstgrupo("fr00codgrpterap").Value
            vntdesCompHijo = rstgrupo("fr00desgrpterap").Value
            Call tvwItems(0).Nodes.Add(vntpadre, tvwChild, vntpadre & "/" & vntcompHijo, vntcompHijo & ".-" & vntdesCompHijo)
            Call Rellenar_Componentes(vntpadre, vntcompHijo, vntdesCompHijo)
            rstgrupo.MoveNext
        Wend
        rstgrupo.Close
        Set rstgrupo = Nothing
    End If
    'strRelprod = "SELECT * FROM FR1100 WHERE FR75codprotocolo=" & vntprotocolo & _
    '             " and frh6codcompon=" & vntcomp & " order by fr73codproducto"
    'Set rstRelprod = objApp.rdoConnect.OpenResultset(strRelprod)
    'While rstRelprod.EOF = False
    '    vntProd = rstRelprod("fr73codproducto").Value
    '    strdesProd = "SELECT fr73desproducto FROM FR7300 where fr73codproducto=" & vntProd
    '    Set rstdesProd = objApp.rdoConnect.OpenResultset(strdesProd)
    '    vntdesProd = rstdesProd(0).Value
    '    Call tvwItems(0).Nodes.Add("comp" & vntprotocolo & "/" & vntcomp, tvwChild, "prod" & vntprotocolo & "/" & vntcomp & "/" & vntProd, vntProd & ".-" & vntdesProd)
    '    rstRelprod.MoveNext
    'Wend
End Sub
Private Sub rellenar_TreeView()
  Dim strRelGrupo As String
  Dim rstRelGrupo As rdoResultset
  Dim rstesProd As rdoResultset
  Dim vntprotocolo As Variant
  Dim vntdesProtocolo As Variant
  Dim vntgrupo As Variant
  Dim vntdesgrupo As Variant
  Dim vntpadre As Variant
  
    tvwItems(0).Nodes.Clear
    strRelGrupo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP_PAD is null order by FR00CODGRPTERAP"
    Set rstRelGrupo = objApp.rdoConnect.OpenResultset(strRelGrupo)
    While rstRelGrupo.EOF = False
        vntgrupo = rstRelGrupo("FR00CODGRPTERAP").Value
        vntdesgrupo = rstRelGrupo("FR00DESGRPTERAP").Value
        Call tvwItems(0).Nodes.Add(, , "grup" & vntgrupo, vntgrupo & ".-" & vntdesgrupo)
        tvwItems(0).Nodes("grup" & vntgrupo).Selected = True
        vntpadre = "grup"
        Call Rellenar_Componentes(vntpadre, vntgrupo, vntdesgrupo)
        rstRelGrupo.MoveNext
    Wend
    rstRelGrupo.Close
    Set rstRelGrupo = Nothing
 ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'If tvwItems(0).Nodes.Count > 0 Then
    '    tvwItems(0
    'End If
    'tabTab1(0).Enabled = True
    'tabTab1(2).Enabled = False
    'tabTab1(1).Enabled = False

End Sub



Private Sub Form_Activate()
    
    grdDBGrid1(1).Columns(0).Visible = False
    If grdDBGrid1(1).Rows > 0 Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Grupos Terape�ticos"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "FR0000"
        
        Call .FormAddOrderField("FR00CODGRPTERAP", cwAscending)
    
        strKey = .strDataBase & .strTable
        
        .intCursorSize = 0
        
        'Call .FormCreateFilterWhere(strKey, "Grupos Terape�ticos")
        'Call .FormAddFilterWhere(strKey, "FR00CODGRPTERAP", "C�d. Grupo Terape�tico", cwString)
        'Call .FormAddFilterWhere(strKey, "FR00DESGRPTERAP", "Descripci�n Grupo Terape�tico", cwString)
        'Call .FormAddFilterWhere(strKey, "FR00DESGRPTERAP_PAD", "C�d. Grupo Terape�tico Padre", cwString)
        
        'Call .FormAddFilterOrder(strKey, "FR00CODGRPTERAP", "C�d. Grupo Terape�tico")
        'Call .FormAddFilterOrder(strKey, "FR00DESGRPTERAP", "Descripci�n Grupo Terape�tico")
    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�d. Grupo Terape�tico", "FR00CODGRPTERAP", cwString, 10)
        Call .GridAddColumn(objMultiInfo, "Descripci�n  Grupo Terape�tico", "FR00DESGRPTERAP", cwString, 75)
        Call .GridAddColumn(objMultiInfo, "C�d. Grupo Terape�tico Padre", "FR00CODGRPTERAP_PAD", cwString, 10)
  
        Call .FormCreateInfo(objMultiInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnForeign = True
        
        
        Call .WinRegister
        Call .WinStabilize
    End With

    AuxNodoActual = ""

    grdDBGrid1(1).Columns(3).Width = 2000
    Me.Enabled = False
    Call rellenar_TreeView
    Me.Enabled = True
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Click(Index As Integer)
Dim strNodo As String
Dim qryNodo As rdoQuery
Dim rstNodo As rdoResultset
Dim SelectNodo As String
Dim strPadre As String
Dim Fin As Boolean
Dim pos As Integer

  engrid = True
      
  If grdDBGrid1(1).Rows > 0 Then
    If grdDBGrid1(1).Columns(0).Value = "Le�do" Then
      If grdDBGrid1(1).Columns(5).Value = "" Then
        tvwItems(0).Nodes("grup" & grdDBGrid1(1).Columns(3).Value).EnsureVisible
        tvwItems(0).Nodes("grup" & grdDBGrid1(1).Columns(3).Value).Selected = True
      Else
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & grdDBGrid1(1).Columns(3).Value & "'"
        strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP=?"
        Set qryNodo = objApp.rdoConnect.CreateQuery("", strNodo)
        qryNodo(0) = grdDBGrid1(1).Columns(3).Value
        Set rstNodo = qryNodo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        'Set rstNodo = objApp.rdoConnect.OpenResultset(strNodo)
        If rstNodo.EOF Then
          Exit Sub
        Else
          strPadre = rstNodo("FR00CODGRPTERAP_PAD").Value
          SelectNodo = strPadre & "/" & grdDBGrid1(1).Columns(3).Value
          Fin = False
        End If
        rstNodo.Close
        Set rstNodo = Nothing
        While Not Fin
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
          'strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
          strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP=?"
          Set qryNodo = objApp.rdoConnect.CreateQuery("", strNodo)
          qryNodo(0) = strPadre
          Set rstNodo = qryNodo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
          'Set rstNodo = objApp.rdoConnect.OpenResultset(strNodo)
          If rstNodo.EOF Then
            Fin = True
          Else
            If IsNull(rstNodo("FR00CODGRPTERAP_PAD").Value) Then
              'pos = InStr(Right(SelectNodo, Len(SelectNodo) - 1), "/")
              'If strPadre = Left(Right(SelectNodo, Len(SelectNodo) - 1), pos - 1) Then
              '  SelectNodo = Right(SelectNodo, Len(SelectNodo) - 1)
              'Else
              '  SelectNodo = strPadre & SelectNodo
              'End If
              Fin = True
            Else
              strPadre = rstNodo("FR00CODGRPTERAP_PAD").Value
              SelectNodo = strPadre & "/" & SelectNodo
              Fin = False
            End If
          End If
          rstNodo.Close
          Set rstNodo = Nothing
        Wend
        SelectNodo = "grup" & SelectNodo
        
        tvwItems(0).Nodes(SelectNodo).EnsureVisible
        tvwItems(0).Nodes(SelectNodo).Selected = True
      End If
    End If
  End If

End Sub

Private Sub grdDBGrid1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)

  engrid = True

End Sub

Private Sub grdDBGrid1_LostFocus(Index As Integer)
  engrid = False
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch


  If strCtrl = "grdDBGrid1(1).C�d. Grupo Terape�tico Padre" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR0000"
     .strOrder = "ORDER BY FR00CODGRPTERAP ASC"
     .strWhere = "WHERE FR00CODGRPTERAP LIKE '" & Left(grdDBGrid1(1).Columns(3).Value, 1) & "%'" & _
                 " AND FR00CODGRPTERAP<>'" & grdDBGrid1(1).Columns(3).Value & "'"
         
     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�digo Grupo"
         
     Set objField = .AddField("FR00DESGRPTERAP")
     objField.strSmallDesc = "Descripci�n Grupo"
         
     If .Search Then
        Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), .cllValues("FR00CODGRPTERAP"))
        Call objWinInfo.CtrlDataChange
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim strDelete As String
    Dim STR$
    Dim RD As rdoResultset
    Dim QY As rdoQuery
    Dim QYUP As rdoQuery
    Dim Codfamilia$
    On Error GoTo ERR
    If blnError = False Then
        strDelete = "DELETE FROM FR0000 WHERE FR00CODGRPTERAP='" & grdDBGrid1(1).Columns(3).Value & "'"
        Codfamilia = grdDBGrid1(1).Columns(3).Value
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "commit", 64
        objWinInfo.objWinActiveForm.blnChanged = False
        Call rellenar_TreeView
        objWinInfo.DataRefresh
        If Len(Codfamilia) = 3 And Left(Codfamilia, 1) <> "Q" Then
            STR = "SELECT FAMILYDBOID FROM FRP100 WHERE FR00CODGRPTERAP=?"
            Set QY = objApp.rdoConnect.CreateQuery("", STR)
            QY(0) = Codfamilia
            Set RD = QY.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                Do While Not RD.EOF
                    STR = "UPDATE FAMILIES SET ISDELETED=? WHERE FAMILYDBOID=?"
                    Set QYUP = objApp.rdoConnect.CreateQuery("", STR)
                    QYUP(0) = "T"
                    QYUP(1) = RD!FAMILYDBOID
                    QYUP.Execute
                    RD.MoveNext
                Loop
            QYUP.Close
            RD.Close
            QY.Close
        End If

        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
    End If
    Exit Sub
ERR:
    Call MsgBox("Imposible borrar el Grupo", vbInformation, "Aviso")
'    Dim strDelete As String
'    On Error GoTo ERR
'    If blnError = False Then
'        strDelete = "DELETE FROM FR0000 WHERE FR00CODGRPTERAP='" & grdDBGrid1(1).Columns(3).Value & "'"
'
'        objApp.rdoConnect.Execute strDelete, 64
'        objApp.rdoConnect.Execute "commit", 64
'        objWinInfo.objWinActiveForm.blnChanged = False
'        Call rellenar_TreeView
'        objWinInfo.DataRefresh
'        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
'    End If
'    Exit Sub
'ERR:
'    Call MsgBox("Imposible borrar el Grupo", vbInformation, "Aviso")
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim strNodo  As String
Dim strPadre As String
Dim Fin As Boolean
Dim strSelect As String
Dim qrySelect As rdoQuery
Dim rstSelect As rdoResultset
Dim Codfamilia$
Dim objOrdenMedica As New PI00100.clsFarmacia
  If blnError = False Then
    Fin = False
    If grdDBGrid1(1).Columns(5).Value = "" Then
      strNodo = "grup" & grdDBGrid1(1).Columns(3).Value
      strPadre = ""
    Else
      'strNodo = "grup" & grdDBGrid1(1).Columns(5).Value & "/" & grdDBGrid1(1).Columns(3).Value
      strNodo = grdDBGrid1(1).Columns(5).Value & "/" & grdDBGrid1(1).Columns(3).Value
      strPadre = grdDBGrid1(1).Columns(5).Value
      If strPadre = "" Then
        Fin = True
      End If
      While Not Fin
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'strSelect = "SELECT FR00CODGRPTERAP_PAD FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
        strSelect = "SELECT FR00CODGRPTERAP_PAD FROM FR0000 WHERE FR00CODGRPTERAP=?"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        qrySelect(0) = strPadre
        Set rstSelect = qrySelect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        'Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        If rstSelect.EOF Then
          Fin = True
          strNodo = "grup" & strNodo
        Else
          If IsNull(rstSelect(0).Value) Then
            Fin = True
            strNodo = "grup" & strNodo
          Else
            strPadre = rstSelect(0).Value
            strNodo = strPadre & "/" & strNodo
          End If
        End If
      Wend
      rstSelect.Close
      Set rstSelect = Nothing
    End If
   Codfamilia = Left(grdDBGrid1(1).Columns(3).Value, 3)
        If Len(Codfamilia) = 3 And Left(Codfamilia, 1) <> _
            "Q" Then 'SOLO SI SON FAMILIAS DE 2� NIVEL
            Set objOrdenMedica.objClsCW = objCW
            objOrdenMedica.strUser = objsecurity.strUser
            Call objOrdenMedica.Familia(Codfamilia)
        End If
  AuxNodoActual = strNodo
  
  End If
  
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)

  If grdDBGrid1(1).Columns(3).Value = grdDBGrid1(1).Columns(5).Value Then
    MsgBox "El C�d.Grupo Terapeutico Padre es incorrecto.", vbExclamation
    blnCancel = True
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim objOrdenMedica As New PI00100.clsFarmacia
  Dim Codfamilia As String
  Dim Longitud%
  Dim posicion%
  Dim STR$
  Dim QY As rdoQuery
  Dim RD As rdoResultset
  Dim QYUP As rdoQuery
  engrid = False
  ERR = 0
  AuxNodoActual = ""

  Select Case btnButton.Index
  Case 4 'Guardar
      ERR = objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If grdDBGrid1(1).Rows > 0 Then
        Call rellenar_TreeView
        tvwItems(0).Nodes(AuxNodoActual).EnsureVisible
        tvwItems(0).Nodes(AuxNodoActual).Selected = True
        AuxNodoActual = ""
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
      'PASO a PICIS
'      posicion = InStr(1, tvwItems(0).SelectedItem, ".")
'      Longitud = Len(tvwItems(0).SelectedItem)
'      Codfamilia = Left(tvwItems(0).SelectedItem, Longitud - (Longitud - posicion + 1))
'        If (ERR = 0 Or ERR = 4) And Len(Codfamilia) = 3 And Left(Codfamilia, 1) <> _
'            "Q" Then 'SOLO SI SON FAMILIAS DE 2� NIVEL
'            Set objOrdenMedica.objClsCW = objCW
'            objOrdenMedica.strUser = objsecurity.strUser
'            Call objOrdenMedica.Familia(Codfamilia)
'        End If
  Case 21 'Primero
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If grdDBGrid1(1).Rows > 0 Then
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
    Exit Sub
  Case 22 'Anterior
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If grdDBGrid1(1).Rows > 0 Then
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    End If
    Exit Sub
  Case 23 'Siguiente
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If grdDBGrid1(1).Rows > 0 Then
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    End If
    Exit Sub
  Case 24 'Ultimo
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If grdDBGrid1(1).Rows > 0 Then
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    End If
    Exit Sub
  Case Else 'Otro boton
    If btnButton.Index = 8 Then
        posicion = InStr(1, tvwItems(0).SelectedItem, ".")
        Longitud = Len(tvwItems(0).SelectedItem)
        Codfamilia = Left(tvwItems(0).SelectedItem, Longitud - (Longitud - posicion + 1))
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

  End Select


'*
'
'  engrid = False'
'
'  AuxNodoActual = ""
'
'  Select Case btnButton.Index
'  Case 4 'Guardar
'      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'      If grdDBGrid1(1).Rows > 0 Then
'        Call rellenar_TreeView
'        tvwItems(0).Nodes(AuxNodoActual).EnsureVisible
'        tvwItems(0).Nodes(AuxNodoActual).Selected = True
'        AuxNodoActual = ""
'        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
'      End If
'  Case 21 'Primero
'    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'    If grdDBGrid1(1).Rows > 0 Then
'      tvwItems(0).SelectedItem.FirstSibling.Selected = True
'      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
'    End If
'    Exit Sub
'  Case 22 'Anterior
'    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'    If grdDBGrid1(1).Rows > 0 Then
'      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
'        tvwItems(0).SelectedItem.Previous.Selected = True
'        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
'      End If
'    End If
'    Exit Sub
'  Case 23 'Siguiente
'    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'    If grdDBGrid1(1).Rows > 0 Then
'      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
'        tvwItems(0).SelectedItem.Next.Selected = True
'        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
'      End If
'    End If
'    Exit Sub
'  Case 24 'Ultimo
'    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'    If grdDBGrid1(1).Rows > 0 Then
'      tvwItems(0).SelectedItem.LastSibling.Selected = True
'      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
'    End If
'    Exit Sub
'  Case Else 'Otro boton
'    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'    Exit Sub
'  End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  If intIndex = 40 Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)

  Select Case intIndex
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_DragDrop(Index As Integer, Source As Control, x As Single, y As Single)
    If tvwItems(0).DropHighlight Is Nothing Then
        Set tvwItems(0).DropHighlight = Nothing
        indrag = False
        Exit Sub
    Else
        If nodX = tvwItems(0).DropHighlight Then Exit Sub

Cls
        Print nodX.Text & " colocado en " & tvwItems(0).DropHighlight.Text
        Set tvwItems(0).DropHighlight = Nothing
        indrag = False
    End If
End Sub



Private Sub tvwItems_DragOver(Index As Integer, Source As Control, x As Single, y As Single, State As Integer)
    If indrag = True Then
        ' Establece las coordenadas del mouse en
        ' DropHighlight.
        Set tvwItems(0).DropHighlight = tvwItems(0).HitTest(x, y)
    End If
End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  If EnPostWrite = True Then
    EnPostWrite = False
    Exit Sub
  End If
        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)
End Sub


Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim aux As Variant
Dim aux2 As Variant

On Error GoTo ERR:
    Enarbol = True
    If engrid = False Then
      grdDBGrid1(1).Redraw = False
      aux2 = Me.MousePointer
      Me.MousePointer = vbHourglass
      aux = Right(Node.Key, Len(Node.Key) - 4)
      While InStr(aux, "/") <> 0
          aux = Right(aux, Len(aux) - InStr(aux, "/"))
      Wend
      'Call objWinInfo.DataRefreshGoFirst
      'Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
      grdDBGrid1(1).MoveFirst
      While grdDBGrid1(1).Columns("C�d. Grupo Terape�tico").Value <> aux
        Call grdDBGrid1(1).MoveNext
      Wend
      grdDBGrid1(1).Redraw = True
    End If
    'While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR00CODGRPTERAP").Value <> aux
    '  Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
    'Wend
ERR:
    Me.MousePointer = aux2
    
    Enarbol = False
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  engrid = True
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim qryNodo As rdoQuery
Dim strNodo As String
Dim rstNodo As rdoResultset
Dim SelectNodo As String
Dim strPadre As String
Dim Fin As Boolean
Dim pos As Integer

Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)

If engrid = True Then
  If grdDBGrid1(1).Rows > 0 Then
    If grdDBGrid1(1).Columns(0).Value = "Le�do" Then
      If grdDBGrid1(1).Columns(5).Value = "" Then
        tvwItems(0).Nodes("grup" & grdDBGrid1(1).Columns(3).Value).EnsureVisible
        tvwItems(0).Nodes("grup" & grdDBGrid1(1).Columns(3).Value).Selected = True
      Else
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & grdDBGrid1(1).Columns(3).Value & "'"
        strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP=?"
        Set qryNodo = objApp.rdoConnect.CreateQuery("", strNodo)
        qryNodo(0) = grdDBGrid1(1).Columns(3).Value
        Set rstNodo = qryNodo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        'Set rstNodo = objApp.rdoConnect.OpenResultset(strNodo)
        If rstNodo.EOF Then
          Exit Sub
        Else
          strPadre = rstNodo("FR00CODGRPTERAP_PAD").Value
          SelectNodo = strPadre & "/" & grdDBGrid1(1).Columns(3).Value
          Fin = False
        End If
        rstNodo.Close
        Set rstNodo = Nothing
        While Not Fin
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
          'strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP='" & strPadre & "'"
          strNodo = "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP=?"
          Set qryNodo = objApp.rdoConnect.CreateQuery("", strNodo)
          qryNodo(0) = strPadre
          Set rstNodo = qryNodo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
          'Set rstNodo = objApp.rdoConnect.OpenResultset(strNodo)
          If rstNodo.EOF Then
            Fin = True
          Else
            If IsNull(rstNodo("FR00CODGRPTERAP_PAD").Value) Then
              'pos = InStr(Right(SelectNodo, Len(SelectNodo) - 1), "/")
              'If strPadre = Left(Right(SelectNodo, Len(SelectNodo) - 1), pos - 1) Then
              '  SelectNodo = Right(SelectNodo, Len(SelectNodo) - 1)
              'Else
              '  SelectNodo = strPadre & SelectNodo
              'End If
              Fin = True
            Else
              strPadre = rstNodo("FR00CODGRPTERAP_PAD").Value
              SelectNodo = strPadre & "/" & SelectNodo
              Fin = False
            End If
          End If
          rstNodo.Close
          Set rstNodo = Nothing
        Wend
        SelectNodo = "grup" & SelectNodo
        
        tvwItems(0).Nodes(SelectNodo).EnsureVisible
        tvwItems(0).Nodes(SelectNodo).Selected = True
      End If
    End If
  End If
End If

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


