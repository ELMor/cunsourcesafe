VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmValNecQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. VALIDAR PEDIDO FARMACIA. Validar Pedido Quir�fano."
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0130.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdPrepQui 
      Caption         =   "PREPARAR"
      Enabled         =   0   'False
      Height          =   375
      Left            =   6600
      TabIndex        =   15
      Top             =   7560
      Width           =   1335
   End
   Begin VB.CommandButton cmdRecQui 
      Caption         =   "Recoger Quir�fanos"
      Height          =   375
      Left            =   2880
      TabIndex        =   14
      Top             =   7560
      Width           =   1575
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Peticiones de Quir�fano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5535
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Top             =   1920
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5055
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11250
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19844
         _ExtentY        =   8916
         _StockProps     =   79
         Caption         =   "PETICIONES SIN DISPENSAR"
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Quir�fano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   1455
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   11535
      Begin VB.CommandButton Command1 
         Caption         =   "FILTRAR"
         Default         =   -1  'True
         Height          =   495
         Left            =   6360
         TabIndex        =   12
         Top             =   480
         Width           =   1215
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         ForeColor       =   &H00FF0000&
         Height          =   1095
         Left            =   3360
         TabIndex        =   8
         Top             =   270
         Width           =   2775
         Begin VB.OptionButton Option3 
            Caption         =   "Enviadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800080&
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   13
            Top             =   0
            Value           =   -1  'True
            Width           =   1575
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Pend.+Disp.Parciales"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800080&
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   11
            Top             =   810
            Width           =   2535
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Pendientes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800080&
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   10
            Top             =   270
            Width           =   1575
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Disp.Parcial"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800080&
            Height          =   435
            Index           =   2
            Left            =   120
            TabIndex        =   9
            Top             =   440
            Width           =   1455
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         ForeColor       =   &H00FF0000&
         Height          =   855
         Left            =   1200
         TabIndex        =   5
         Top             =   240
         Width           =   1815
         Begin VB.OptionButton Option2 
            Caption         =   "Anestesia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   7
            Top             =   600
            Width           =   1335
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Quir�fano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   1575
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmValNecQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0130.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Validar Pedido Farmacia                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdPrepQui_Click()

If grdDBGrid1(1).Rows = 0 Then
  Screen.MousePointer = vbDefault
  Exit Sub
End If

cmdPrepQui.Enabled = False
Screen.MousePointer = vbHourglass
Me.Enabled = False
    
    'ALMACENAR EL ESTADO DE LA PETICI�N
    gstrEst = ""
    If Option3(3).Value = True Then
      gstrEst = "12" '4
    End If
    If Option3(2).Value = True Then
      gstrEst = "9"
    End If
    If Option3(0).Value = True Then
      gstrEst = "12,9"
    End If

    If Option2(0).Value = True Then
      gstrQA = "0"
    Else
      gstrQA = "1"
    End If
    Screen.MousePointer = vbDefault
    'Call objsecurity.LaunchProcess("FR0148")
    Call objsecurity.LaunchProcess("FR0158")
    gstrEst = ""
    gstrQA = ""

Screen.MousePointer = vbDefault
Me.Enabled = True
cmdPrepQui.Enabled = True

End Sub

Private Sub cmdRecQui_Click()
Dim strinsertar As String
Dim strupdate As String

If grdDBGrid1(1).Rows = 0 Then
  Screen.MousePointer = vbDefault
  Exit Sub
End If

cmdRecQui.Enabled = False
Screen.MousePointer = vbHourglass
Me.Enabled = False
    
    strinsertar = "INSERT INTO FRK500 (FRK3CODNECESUNID,FRK4NUMLINEA,FRK5CODNECESVAL,"
    strinsertar = strinsertar & "FR73CODPRODUCTO,FR93CODUNIMEDIDA,FRK5CANTNECESQUIR,SG02COD_VAL,"
    strinsertar = strinsertar & "FRK5FECVALIDACION,FR93CODUNIMEDIDA_SUM,FRK5CANTSUMIFARM,"
    strinsertar = strinsertar & "FRK5INDQUIANE,FRK5INDTRATADO,FRK5INDFAC,FRK5INDBLOQ,FRK5CANTULTFARM)"
    strinsertar = strinsertar & " SELECT  FRK400.FRK3CODNECESUNID,"
    strinsertar = strinsertar & "FRK400.FRK4NUMLINEA,"
    strinsertar = strinsertar & "FRK5CODNECESVAL_SEQUENCE.nextval,"
    strinsertar = strinsertar & "FRK400.FR73CODPRODUCTO,"
    strinsertar = strinsertar & "FRK400.FR93CODUNIMEDIDA,"
    strinsertar = strinsertar & "FRK400.FRK4CANTNECESQUIR,"
    strinsertar = strinsertar & "'" & objsecurity.strUser & "',"
    strinsertar = strinsertar & "SYSDATE,"
    strinsertar = strinsertar & "FRK400.FR93CODUNIMEDIDA,"
    strinsertar = strinsertar & "FRK400.FRK4CANTNECESQUIR,"
    strinsertar = strinsertar & "FRK400.FRK4INDQUIANE,"
    'strinsertar = strinsertar & "0,0,0,FRK400.FRK4CANTNECESQUIR" 'jrc 25/01/2000
    strinsertar = strinsertar & "0,0,0,0" 'jrc 22/02/2000
    strinsertar = strinsertar & " FROM FRK400,FRK300,FRK500"
    strinsertar = strinsertar & " WHERE FRK400.FRK3CODNECESUNID=FRK300.FRK3CODNECESUNID"
    strinsertar = strinsertar & " AND FR26CODESTPETIC=3"
    strinsertar = strinsertar & " AND FRK400.FRK3CODNECESUNID=FRK500.FRK3CODNECESUNID(+)"
    strinsertar = strinsertar & " AND FRK400.FRK4NUMLINEA=FRK500.FRK4NUMLINEA(+)"
    strinsertar = strinsertar & " AND FRK500.FRK3CODNECESUNID IS NULL"
    strinsertar = strinsertar & " AND FRK500.FRK4NUMLINEA IS NULL"
    If Option2(0).Value = True Then
      strinsertar = strinsertar & " AND FRK300.FRK3INDQUIANE=0"
    Else
      strinsertar = strinsertar & " AND FRK300.FRK3INDQUIANE=-1"
    End If
    objApp.rdoConnect.Execute strinsertar, 64
    
    strupdate = "UPDATE FRK300 SET FR26CODESTPETIC=12 WHERE "
    strupdate = strupdate & " FR26CODESTPETIC=3"
    If Option2(0).Value = True Then
      strupdate = strupdate & " AND FRK3INDQUIANE=0"
    Else
      strupdate = strupdate & " AND FRK3INDQUIANE=-1"
    End If
    objApp.rdoConnect.Execute strupdate, 64

objWinInfo.DataRefresh
Screen.MousePointer = vbDefault
Me.Enabled = True
cmdRecQui.Enabled = True


End Sub

Private Sub Command1_Click()
  Dim strLis As String
  Dim auxQA As String
  
  Screen.MousePointer = vbHourglass
  strLis = ""
  If Option3(1).Value = True Then
    strLis = "3"
  End If
  If Option3(3).Value = True Then
    strLis = "12"
  End If
  If Option3(2).Value = True Then
    strLis = "9"
  End If
  If Option3(0).Value = True Then
    strLis = "12,9"
  End If
  
  If Len(Trim(strLis)) = 0 Then
    strLis = "0"
  End If
  
  If Option2(0).Value = True Then
    auxQA = "0"
  Else
    auxQA = "-1"
  End If
  
  objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC in (" & strLis & ") AND FRK3INDQUIANE=" & auxQA
  
  objWinInfo.DataRefresh
  Screen.MousePointer = vbDefault

End Sub



Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)

  If Index = 1 Then
    grdDBGrid1(Index).Columns("Quir�fano").Value = "QUIROFANO PARA DISPENSACION"
  End If

End Sub

Private Sub Option2_Click(Index As Integer)
  grdDBGrid1(1).RemoveAll
End Sub

Private Sub Option3_Click(Index As Integer)

  If Index = 1 Then
    cmdRecQui.Enabled = True
    cmdPrepQui.Enabled = False
  Else
    cmdRecQui.Enabled = False
    cmdPrepQui.Enabled = True
  End If
  
  grdDBGrid1(1).RemoveAll
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Pedidos a Firmar"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "FRK300"
        .intAllowance = cwAllowReadOnly
        '.strWhere = "FR26CODESTPETIC in (3,4,5,9) AND FRK3CODNECESUNID IS NULL"
        .strWhere = "-1=0"
        
        Call .FormAddOrderField("FRK3FECPETICION", cwDescending)
        Call .FormAddOrderField("FRK3CODNECESUNID", cwDescending)
        
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "Pedidos Farmacia")
        Call .FormAddFilterWhere(strKey, "FRK3CODNECESUNID", "C�digo Petici�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Dispensa", cwString)
        Call .FormAddFilterWhere(strKey, "FRK3FECPETICION", "Fecha Petici�n", cwDate)
        Call .FormAddFilterWhere(strKey, "FR97CODQUIROFANO", "C�digo Quir�fano", cwNumeric)
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FRK3CODNECESUNID", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Estado", "FR26CODESTPETIC", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Desc.Estado", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Fecha", "FRK3FECPETICION", cwDate)
        Call .GridAddColumn(objMultiInfo, "C�d.Quir.", "FR97CODQUIROFANO", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "Quir�fano", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Dispensa", "SG02COD_PDS", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Dispensado por ", "", cwString, 30)
        
        Call .GridAddColumn(objMultiInfo, "Quiro?", "FRK3INDQUIANE", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "Tipo Necesidad", "", cwString, 30)
        
        
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "FR26DESESTADOPET")
                
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "SG02APE1")
                
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "FRK3INDQUIANE", "SELECT DECODE(FRK3INDQUIANE,-1,'Necesidad Alm.Anestesia','Necesidad Alm.Quir�fano') TIPONECES FROM FRK300 WHERE FRK3CODNECESUNID = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(12), "TIPONECES")
   
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns(0).Visible = False
    grdDBGrid1(1).Columns(4).Visible = False 'c�d.estado
    'grdDBGrid1(1).Columns(5).Visible = False 'desc.estado
    grdDBGrid1(1).Columns(9).Visible = False
    grdDBGrid1(1).Columns(10).Visible = False
    
    grdDBGrid1(1).Columns(3).Width = 1000 'c�d petici�n
    grdDBGrid1(1).Columns(5).Width = 1600 'estado
    grdDBGrid1(1).Columns(6).Width = 1200 'fecha
    grdDBGrid1(1).Columns(9).Width = 1200 'doctor
    grdDBGrid1(1).Columns(10).Width = 2900
    grdDBGrid1(1).Columns(7).Width = 1200 'cod.quir.
    grdDBGrid1(1).Columns(8).Width = 2900 'quirofano
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    
Call objWinInfo.GridDblClick

End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


