Attribute VB_Name = "DEF001"
Option Explicit

Public objsecurity As clsCWSecurity
Public objmouse As clsCWMouse

Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
'gintprodbuscado es un array que guarda los c�digos de los productos de un protocolo
Public gintprodbuscado()
'gintprodtotal guarda el n�mero total de productos seleccionados
Public gintprodtotal As Integer
Public gintbuscargruprod As Integer
Public gstrLlamador As String
Public gintservicio As Long
Public gstrLlamadorProd As String
Public gstrEst As String
Public gstrQA As String 'para saber si hay que dispensar peticion qui. de anestesia o de quirofano
Public gblnSelProd As Boolean
Public Const gstrversion = "20000125"

Sub Main()
End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub

'Funci�n que devuelve el separador decimal que se corresponde con
'el de la configuraci�n regional de la m�quina.
Public Function SeparadorDec() As String

If Format("12,345", "00.000") = "12,345" Then
  SeparadorDec = ","
Else
  SeparadorDec = "."
End If

End Function

Private Function Saltar_Coment()
  'JMRL 19991125
  Dim strLetra As String
  
  strLetra = " "
  While (Not EOF(1)) And (strLetra <> "*")
    strLetra = Input(1, 1)
  Wend
  If strLetra = "*" Then
    Saltar_Coment = ""
  Else
    Saltar_Coment = strLetra
  End If
End Function
Private Sub Leer_Palabra(strPalabra)
  'JMRL 19991125
  Dim strLetra As String
  
  strLetra = ""
  While (Not EOF(1)) And (strLetra <> ";")
    strLetra = Input(1, 1)
    strPalabra = strPalabra & strLetra
  Wend
End Sub
Public Sub LeerCWPrint(ByRef strDNS As String, ByRef strUser As String, _
                          ByRef strPass As String, ByRef strPATH As String)
  'JMRL 19991125
  Dim strLetra As String
  Dim strFicEntrada As String
  
  strFicEntrada = "CWPrint.ini"
    
  Open strFicEntrada For Input As #1
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  
  strDNS = Saltar_Coment
  
  While Not EOF(1)
    Call Leer_Palabra(strDNS)
    Call Leer_Palabra(strUser)
    Call Leer_Palabra(strPass)
    Call Leer_Palabra(strPATH)
  Wend
  Close #1
  strDNS = Left(strDNS, Len(strDNS) - 1)
  strUser = Left(strUser, Len(strUser) - 1)
  strPass = Left(strPass, Len(strPass) - 1)
  strPATH = Left(strPATH, Len(strPATH) - 1)
  
End Sub



