VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form FrmRegDev 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Cargos y Abonos"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   HelpContextID   =   30001
   Icon            =   "FR8800.frx":0000
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Prod.No Formulario"
      Height          =   1575
      Left            =   4320
      TabIndex        =   15
      Top             =   5280
      Width           =   4935
      Begin VB.CommandButton Command2 
         Caption         =   "Traer"
         Height          =   375
         Left            =   3120
         TabIndex        =   21
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox txtDescripcion 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   240
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   360
         Width           =   4485
      End
      Begin VB.TextBox txtPrecio 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   240
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   1080
         Width           =   1845
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Precio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   240
         TabIndex        =   53
         Top             =   840
         Width           =   555
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   23
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7455
      Left            =   120
      TabIndex        =   26
      Top             =   480
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   13150
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "A paciente"
      TabPicture(0)   =   "FR8800.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel1(6)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblLabel1(7)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLabel1(12)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cboServAlm"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "dtcFecDesde(1)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "dtcFecDesde(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "fraframe1(3)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "fraframe1(2)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmdFiltrar"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "optFactAlm(0)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "optFactAlm(1)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "optFactAlm(2)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "fraframe1(0)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "A Servicio"
      TabPicture(1)   =   "FR8800.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Command1"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "cmdProdNoForm2"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Frame3"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "frame7(1)"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      Begin VB.Frame frame7 
         Caption         =   "Cargos y Abonos al Servicio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5115
         Index           =   1
         Left            =   -74880
         TabIndex        =   27
         Top             =   1560
         Width           =   11145
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid2 
            Height          =   4665
            Index           =   0
            Left            =   120
            TabIndex        =   28
            Top             =   360
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowColumnMoving=   2
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   19076
            _ExtentY        =   8229
            _StockProps     =   79
            BackColor       =   12632256
         End
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   615
         Left            =   -74880
         TabIndex        =   62
         Top             =   6720
         Width           =   3135
         Begin VB.OptionButton optFactAlm2 
            Caption         =   "Facturar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   0
            Left            =   0
            TabIndex        =   65
            Top             =   120
            Width           =   1095
         End
         Begin VB.OptionButton optFactAlm2 
            Caption         =   "Almac�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   1
            Left            =   1080
            TabIndex        =   64
            Top             =   120
            Width           =   1095
         End
         Begin VB.OptionButton optFactAlm2 
            Caption         =   "Ambos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   2
            Left            =   2160
            TabIndex        =   63
            Top             =   120
            Value           =   -1  'True
            Width           =   975
         End
      End
      Begin VB.Frame fraframe1 
         Caption         =   "Cargos y Abonos a Paciente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2850
         Index           =   0
         Left            =   120
         TabIndex        =   22
         Top             =   4440
         Width           =   11265
         Begin VB.CommandButton cmdleerlector 
            Caption         =   "LECTOR"
            Height          =   915
            Left            =   10200
            Picture         =   "FR8800.frx":0044
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   1800
            Width           =   975
         End
         Begin VB.CommandButton cmdProdNoFormPac 
            Caption         =   "&Prod.No Formulario"
            Height          =   615
            Left            =   10200
            TabIndex        =   17
            Tag             =   "Buscar el paciente o el servicio al que se realizar� el abono"
            Top             =   1080
            Width           =   975
         End
         Begin VB.CommandButton cmdBusProdPac 
            Caption         =   "&Buscar Productos"
            Height          =   615
            Left            =   10200
            TabIndex        =   16
            Tag             =   "Buscar el paciente o el servicio al que se realizar� el abono"
            Top             =   360
            Width           =   975
         End
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   2415
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Top             =   360
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   21
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowColumnMoving=   2
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   21
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod.Abo."
            Columns(0).Name =   "Cod.Abo."
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "Proceso"
            Columns(1).Name =   "Proceso"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Asistencia"
            Columns(2).Name =   "Asistencia"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "Persona"
            Columns(3).Name =   "Persona"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1588
            Columns(4).Caption=   "Fecha"
            Columns(4).Name =   "Fecha"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   12632256
            Columns(5).Width=   873
            Columns(5).Caption=   "Hora"
            Columns(5).Name =   "Hora"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(5).HasBackColor=   -1  'True
            Columns(5).BackColor=   12632256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "C�d.Producto"
            Columns(6).Name =   "C�d.Producto"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   1244
            Columns(7).Caption=   "CodInt"
            Columns(7).Name =   "CodInt"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(7).HasBackColor=   -1  'True
            Columns(7).BackColor=   12632256
            Columns(8).Width=   1773
            Columns(8).Caption=   "Referencia"
            Columns(8).Name =   "Referencia"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(8).HasBackColor=   -1  'True
            Columns(8).BackColor=   12632256
            Columns(9).Width=   5292
            Columns(9).Caption=   "Producto"
            Columns(9).Name =   "Producto"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Locked=   -1  'True
            Columns(9).HasBackColor=   -1  'True
            Columns(9).BackColor=   12632256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "Envase"
            Columns(10).Name=   "Envase"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   1402
            Columns(11).Caption=   "Cantidad"
            Columns(11).Name=   "Cantidad"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).HasBackColor=   -1  'True
            Columns(11).BackColor=   16776960
            Columns(12).Width=   1402
            Columns(12).Caption=   "Precio"
            Columns(12).Name=   "Precio"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Locked=   -1  'True
            Columns(12).HasBackColor=   -1  'True
            Columns(12).BackColor=   12632256
            Columns(13).Width=   5292
            Columns(13).Caption=   "Desc.Prod. No Form."
            Columns(13).Name=   "Desc.Prod. No Form."
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(13).Locked=   -1  'True
            Columns(13).HasBackColor=   -1  'True
            Columns(13).BackColor=   12632256
            Columns(14).Width=   3200
            Columns(14).Caption=   "CodInt_dil"
            Columns(14).Name=   "CodInt_dil"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(14).Locked=   -1  'True
            Columns(14).HasBackColor=   -1  'True
            Columns(14).BackColor=   12632256
            Columns(15).Width=   3200
            Columns(15).Caption=   "Producto_dil"
            Columns(15).Name=   "Producto_dil"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(15).Locked=   -1  'True
            Columns(15).HasBackColor=   -1  'True
            Columns(15).BackColor=   12632256
            Columns(16).Width=   3200
            Columns(16).Caption=   "Cant_dil"
            Columns(16).Name=   "Cant_dil"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(16).Locked=   -1  'True
            Columns(16).HasBackColor=   -1  'True
            Columns(16).BackColor=   12632256
            Columns(17).Width=   3200
            Columns(17).Caption=   "CodInt_mez"
            Columns(17).Name=   "CodInt_mez"
            Columns(17).DataField=   "Column 17"
            Columns(17).DataType=   8
            Columns(17).FieldLen=   256
            Columns(17).Locked=   -1  'True
            Columns(17).HasBackColor=   -1  'True
            Columns(17).BackColor=   12632256
            Columns(18).Width=   3200
            Columns(18).Caption=   "Producto_mez"
            Columns(18).Name=   "Producto_mez"
            Columns(18).DataField=   "Column 18"
            Columns(18).DataType=   8
            Columns(18).FieldLen=   256
            Columns(18).Locked=   -1  'True
            Columns(18).HasBackColor=   -1  'True
            Columns(18).BackColor=   12632256
            Columns(19).Width=   3200
            Columns(19).Caption=   "Cant_mez"
            Columns(19).Name=   "Cant_mez"
            Columns(19).DataField=   "Column 19"
            Columns(19).DataType=   8
            Columns(19).FieldLen=   256
            Columns(19).Locked=   -1  'True
            Columns(19).HasBackColor=   -1  'True
            Columns(19).BackColor=   12632256
            Columns(20).Width=   3200
            Columns(20).Visible=   0   'False
            Columns(20).Caption=   "Nuevo"
            Columns(20).Name=   "Nuevo"
            Columns(20).DataField=   "Column 20"
            Columns(20).DataType=   8
            Columns(20).FieldLen=   256
            Columns(20).Locked=   -1  'True
            _ExtentX        =   17595
            _ExtentY        =   4260
            _StockProps     =   79
         End
      End
      Begin VB.OptionButton optFactAlm 
         Caption         =   "Ambos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   2
         Left            =   2280
         TabIndex        =   59
         Top             =   4080
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton optFactAlm 
         Caption         =   "Almac�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   1
         Left            =   1200
         TabIndex        =   58
         Top             =   4080
         Width           =   1095
      End
      Begin VB.OptionButton optFactAlm 
         Caption         =   "Facturar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   57
         Top             =   4080
         Width           =   1095
      End
      Begin VB.CommandButton cmdProdNoForm2 
         Caption         =   "&Prod.No Formulario"
         Height          =   375
         Left            =   -68760
         TabIndex        =   56
         Tag             =   "Buscar el paciente o el servicio al que se realizar� el abono"
         Top             =   6840
         Width           =   1575
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Buscar Productos"
         Height          =   375
         Left            =   -70920
         TabIndex        =   51
         Tag             =   "Buscar el paciente o el servicio al que se realizar� el abono"
         Top             =   6840
         Width           =   1575
      End
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "FILTRAR"
         Default         =   -1  'True
         Height          =   375
         Left            =   10080
         TabIndex        =   45
         Top             =   4080
         Width           =   1215
      End
      Begin VB.Frame fraframe1 
         Caption         =   "Pacientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   11325
         Begin TabDlg.SSTab tabTab1 
            Height          =   1500
            Index           =   1
            Left            =   120
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   360
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   2646
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR8800.frx":0886
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "IdPersona1"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR8800.frx":08A2
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).ControlCount=   1
            Begin idperson.IdPersona IdPersona1 
               Height          =   1335
               Left            =   120
               TabIndex        =   0
               Top             =   120
               Width           =   10065
               _ExtentX        =   17754
               _ExtentY        =   2355
               BackColor       =   12648384
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Datafield       =   "CI21CodPersona"
               MaxLength       =   7
               blnAvisos       =   0   'False
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   2
               Left            =   -74880
               TabIndex        =   1
               TabStop         =   0   'False
               Top             =   120
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   2302
               _StockProps     =   79
               Caption         =   "PACIENTES"
            End
         End
      End
      Begin VB.Frame fraframe1 
         Caption         =   "Procesos/Asistencias"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Tag             =   "Actuaciones Asociadas"
         Top             =   2280
         Width           =   11295
         Begin TabDlg.SSTab tabTab1 
            Height          =   1325
            Index           =   0
            Left            =   120
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   300
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   2328
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR8800.frx":08BE
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(5)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(4)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(3)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(2)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(1)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(23)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(0)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(10)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "dtcDateCombo1(1)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "dtcDateCombo1(0)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txttext1(4)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txttext1(1)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txttext1(3)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txttext1(0)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txttext1(2)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txttext1(5)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txttext1(6)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txttext1(8)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).ControlCount=   18
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR8800.frx":08DA
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(3)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD07CODPROCESO"
               Height          =   330
               Index           =   8
               Left            =   8160
               TabIndex        =   55
               Tag             =   "Proceso3"
               Top             =   720
               Visible         =   0   'False
               Width           =   1100
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD07CODPROCESO"
               Height          =   330
               Index           =   6
               Left            =   8160
               TabIndex        =   54
               Tag             =   "Proceso2"
               Top             =   240
               Visible         =   0   'False
               Width           =   1100
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   3960
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Dpto."
               Top             =   840
               Width           =   525
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Index           =   2
               Left            =   7200
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   250
               Width           =   765
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD07CODPROCESO"
               Height          =   315
               Index           =   0
               Left            =   120
               TabIndex        =   4
               Tag             =   "Proceso"
               Top             =   250
               Width           =   1100
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD01CODASISTENCI"
               Height          =   315
               Index           =   3
               Left            =   1440
               TabIndex        =   5
               Tag             =   "Asistencia"
               Top             =   250
               Width           =   1100
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Index           =   1
               Left            =   120
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Doctor Responsable"
               Top             =   840
               Width           =   3525
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Index           =   4
               Left            =   4560
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Dpto. Responsable"
               Top             =   840
               Width           =   3525
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1095
               Index           =   3
               Left            =   -74880
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   120
               Width           =   10530
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               stylesets.count =   2
               stylesets(0).Name=   "PRN"
               stylesets(0).BackColor=   65535
               stylesets(0).Picture=   "FR8800.frx":08F6
               stylesets(1).Name=   "OM"
               stylesets(1).BackColor=   16776960
               stylesets(1).Picture=   "FR8800.frx":0912
               SelectTypeRow   =   3
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   18574
               _ExtentY        =   1931
               _StockProps     =   79
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "AD08FECINICIO"
               Height          =   315
               Index           =   0
               Left            =   3000
               TabIndex        =   10
               Tag             =   "Fec.Inicio"
               Top             =   250
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "AD08FECFIN"
               Height          =   315
               Index           =   1
               Left            =   5040
               TabIndex        =   11
               Tag             =   "Fec.Fin"
               Top             =   250
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dpto."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   3960
               TabIndex        =   52
               Top             =   600
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   40
               Top             =   70
               Width           =   705
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fec.Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   23
               Left            =   3000
               TabIndex        =   39
               Top             =   70
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Asistencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   1440
               TabIndex        =   38
               Top             =   70
               Width           =   885
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fec.Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   5040
               TabIndex        =   37
               Top             =   70
               Width           =   645
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   7200
               TabIndex        =   36
               Top             =   70
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Doctor Responsable"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   120
               TabIndex        =   35
               Top             =   600
               Width           =   1740
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dpto. Responsable"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   4560
               TabIndex        =   34
               Top             =   600
               Width           =   1635
            End
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Criterios de B�squeda"
         ForeColor       =   &H00C00000&
         Height          =   1215
         Index           =   1
         Left            =   -74880
         TabIndex        =   29
         Top             =   360
         Width           =   11175
         Begin VB.CommandButton cmdFilServicio 
            Caption         =   "FILTRAR"
            Height          =   375
            Left            =   8160
            TabIndex        =   50
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox txtServicio 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1200
            Locked          =   -1  'True
            TabIndex        =   31
            TabStop         =   0   'False
            Tag             =   "Servicio"
            Top             =   480
            Width           =   3165
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   4920
            TabIndex        =   30
            Tag             =   "El abono se realizar� al Servicio"
            Top             =   480
            Value           =   -1  'True
            Visible         =   0   'False
            Width           =   1335
         End
         Begin SSDataWidgets_B.SSDBCombo cboservicio 
            Height          =   315
            Left            =   120
            TabIndex        =   32
            TabStop         =   0   'False
            Tag             =   "C�digo Servicio"
            Top             =   480
            Width           =   855
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1508
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFecDesde 
            Height          =   330
            Index           =   2
            Left            =   1080
            TabIndex        =   46
            Top             =   840
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFecDesde 
            Height          =   330
            Index           =   3
            Left            =   4080
            TabIndex        =   48
            Top             =   840
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fec.Hasta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   3120
            TabIndex        =   49
            Top             =   960
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fec.Desde"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   47
            Top             =   960
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   120
            TabIndex        =   33
            Top             =   240
            Width           =   705
         End
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecDesde 
         Height          =   330
         Index           =   0
         Left            =   5640
         TabIndex        =   41
         Top             =   4080
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecDesde 
         Height          =   330
         Index           =   1
         Left            =   8160
         TabIndex        =   43
         Top             =   4080
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboServAlm 
         Height          =   315
         Left            =   3240
         TabIndex        =   60
         TabStop         =   0   'False
         Top             =   4080
         Width           =   735
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1296
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dpto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   12
         Left            =   4080
         TabIndex        =   61
         Top             =   4200
         Width           =   480
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   7560
         TabIndex        =   44
         Top             =   4200
         Width           =   510
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   5040
         TabIndex        =   42
         Top             =   4200
         Width           =   555
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRegDev"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmRegDev (FR8800.FRM)                                       *
'* AUTOR: JESUS M� RODILLA LARA                                         *
'* FECHA: 17/12/1999                                                    *
'* DESCRIPCION: Registrar Devoluciones                                  *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
'Dim mstrCodMov As String
Dim glstrWhereSer As String


Private Sub RealizarApuntes2(strCodDpto, strCodPrd, strCant, tipo As String)
'Al realizar la dispensaci�n se almacenan los movimientos, tanto de salida como de entrada
'FR8000 es la tabla de salidas de almac�n: sacamos del almac�n de Farmacia y lo pasamos al almac�n del servicio
'FR3500 es la tabla de entradas de almac�n: metemos en el almac�n del Servicio lo sacado de Farmacia
'FR6500 esla tabla en la que se almacenan lo entregado a un servicio
'Dim strCodDpto As String 'Para guardar el c�digo del Servicio
  Dim strNumNec As String 'Para guardar el n�mero de necesidad
  Dim strFR19 As String
  Dim qryFR19 As rdoQuery
  Dim rdoFR19 As rdoResultset
  Dim strIns As String
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rdoFR73 As rdoResultset
  Dim strTamEnv As String
  'Dim strCant As String
  Dim strUpd As String
  Dim qryUpd As rdoQuery
  Dim rdoUpd As rdoResultset
  Dim strCP As String
  Dim strDptoFar As String
  Dim strIns80 As String
  Dim strIns35 As String
  Dim strFRH2 As String
  Dim qryFRH2 As rdoQuery
  Dim rdoFRH2 As rdoResultset
  Dim strAlmOri As String
  Dim blnEntroEnWhile As Boolean
  Dim strSql As String
  Dim rdoSql As rdoResultset
  Dim strCodMov As String
  Dim strCod80 As String
  Dim strCod35 As String
  Dim strFR04 As String
  Dim qryFR04  As rdoQuery
  Dim rdoFR04 As rdoResultset
  Dim strAlmDes As String
  Dim strCodNec As String
  'Dim strCodPrd As String
  
    'On Error GoTo ErrReAp2
      
    If IdPersona1.Text = "*" Then
      Exit Sub
    End If
    'Screen.MousePointer = vbHourglass
  
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 28
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      If tipo = "cargo" Then
        strAlmOri = "0"
      Else
        strAlmDes = "0"
      End If
    Else
      If tipo = "cargo" Then
        strAlmOri = rdoFRH2.rdoColumns(0).Value
      Else
        strAlmDes = rdoFRH2.rdoColumns(0).Value
      End If
    End If
    qryFRH2.Close
    Set qryFRH2 = Nothing
    Set rdoFRH2 = Nothing
    
    
    strSql = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod80 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
      
    'strCodDpto = txttext1(5).Text
    'strCodPrd = grdDBGrid1(0).Columns("C�d.Producto").Value
    'strCant = grdDBGrid1(0).Columns("Cantidad").Value
    
    strFR04 = "SELECT FR04CODALMACEN FROM FR0400 WHERE AD02CODDPTO=?"
    Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
    qryFR04(0) = strCodDpto
    Set rdoFR04 = qryFR04.OpenResultset()
    If Not rdoFR04.EOF Then
      If Not IsNull(rdoFR04(0).Value) Then
        If tipo = "cargo" Then
          strAlmDes = rdoFR04(0).Value
        Else
          strAlmOri = rdoFR04(0).Value
        End If
      Else
        If tipo = "cargo" Then
          strAlmDes = 999 'Almac�n ficticio
        Else
          strAlmOri = 999 'Almac�n ficticio
        End If
      End If
    Else
      If tipo = "cargo" Then
        strAlmDes = 999 'Almac�n ficticio
      Else
        strAlmOri = 999 'Almac�n ficticio
      End If
    End If
    qryFR04.Close
    Set qryFR04 = Nothing
    Set rdoFR04 = Nothing
    
    'Se realiza rl insert en FR8000: salidas de alamc�n
    strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
                "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN) VALUES (" & _
                strCod80 & "," & strAlmOri & "," & strAlmDes & ",4,SYSDATE," & _
                strCodPrd & "," & objGen.ReplaceStr(strCant, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCant, ",", ".", 1) & ")"
    objApp.rdoConnect.Execute strIns80, 64

    strSql = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod35 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    'Se realiza el insert en FR3500: entradas de almac�n
    strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
               "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD," & _
               "FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & strCod35 & "," & _
               strAlmOri & "," & strAlmDes & ",4,SYSDATE," & strCodPrd & "," & objGen.ReplaceStr(strCant, ",", ".", 1) & ",1,'NE'," & objGen.ReplaceStr(strCant, ",", ".", 1) & ")"
    objApp.rdoConnect.Execute strIns35, 64
  
  
  'Screen.MousePointer = vbDefault
  Exit Sub
ErrReAp2:
  Screen.MousePointer = vbDefault
  MsgBox "No existe el almac�n del Servicio: " & strCodDpto, vbInformation, "Realizar Apuntes"
End Sub


Private Sub RealizarApuntes1()
'Al realizar la dispensaci�n se almacenan los movimientos, tanto de salida como de entrada
'FR8000 es la tabla de salidas de almac�n: sacamos del almac�n de Farmacia y lo pasamos al almac�n del servicio
'FR3500 es la tabla de entradas de almac�n: metemos en el almac�n del Servicio lo sacado de Farmacia
'FRK100 esla tabla en la que se almacenan lo entregado a un servicio
Dim strCodDpto As String 'Para guardar el c�digo del Servicio
Dim strNumNec As String 'Para guardar el n�mero de necesidad
Dim strFR19 As String
Dim qryFR19 As rdoQuery
Dim rdoFR19 As rdoResultset
Dim strIns As String
Dim strFR73 As String
Dim qryFR73 As rdoQuery
Dim rdoFR73 As rdoResultset
Dim strTamEnv As String
Dim strCant As String
Dim strUpd As String
Dim qryUpd As rdoQuery
Dim rdoUpd As rdoResultset
Dim strCP As String
Dim strDptoFar As String
Dim strIns80 As String
Dim strIns35 As String
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset
Dim strAlmFar As String
Dim blnEntroEnWhile As Boolean
Dim strSql As String
Dim rdoSql As rdoResultset
Dim strCodMov As String
Dim strCod80 As String
Dim strCod35 As String
Dim strFR04 As String
Dim qryFR04  As rdoQuery
Dim rdoFR04 As rdoResultset
Dim strAlmDes As String
Dim strCodNec As String
Dim strCodPrd As String
  
    On Error GoTo ErrReAp1
  
    Screen.MousePointer = vbHourglass
  
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 28
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strAlmFar = "0"
    Else
      strAlmFar = rdoFRH2.rdoColumns(0).Value
    End If
    qryFRH2.Close
    Set qryFRH2 = Nothing
    Set rdoFRH2 = Nothing
    
    
    strSql = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod80 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
      
    'strCodDpto = objWinInfo.objWinActiveForm.rdoCursor!AD02CODDPTO
    strCodPrd = objWinInfo.objWinActiveForm.rdoCursor!FR73CODPRODUCTO
    
    strFR04 = "SELECT FR04CODALMACEN FROM FR0400 WHERE AD02CODDPTO=?"
    Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
    qryFR04(0) = objWinInfo.objWinActiveForm.rdoCursor!AD02CODDPTO
    Set rdoFR04 = qryFR04.OpenResultset()
    If Not rdoFR04.EOF Then
      If Not IsNull(rdoFR04(0).Value) Then
        strAlmDes = rdoFR04(0).Value
      Else
        strAlmDes = 999 'Almac�n ficticio
      End If
    Else
      strAlmDes = 999 'Almac�n ficticio
    End If
    qryFR04.Close
    Set qryFR04 = Nothing
    Set rdoFR04 = Nothing
    
    strCant = objWinInfo.objWinActiveForm.rdoCursor!FRK1CANTIDAD
    
    'Se realiza rl insert en FR8000: salidas de alamc�n
    strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
                "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN) VALUES (" & _
                strCod80 & "," & strAlmDes & "," & strAlmFar & ",4,SYSDATE," & _
                strCodPrd & "," & strCant & ",1,'NE'," & strCant & ")"
    objApp.rdoConnect.Execute strIns80, 64
    
    strSql = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod35 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    'Se realiza el insert en FR3500: entradas de almac�n
    strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
               "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD," & _
               "FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & strCod35 & "," & _
               strAlmDes & "," & strAlmFar & ",4,SYSDATE," & strCodPrd & "," & strCant & ",1,'NE'," & strCant & ")"
    objApp.rdoConnect.Execute strIns35, 64
    
  
  
  Screen.MousePointer = vbDefault
  Exit Sub
ErrReAp1:
  Screen.MousePointer = vbDefault
  MsgBox "No existe el almac�n del Servicio: " & strCodDpto, vbInformation, "Realizar Apuntes"
End Sub

Private Sub auxDBGrid1_KeyPress(Index As Integer, KeyAscii As Integer)

If auxDBGrid1(0).Col = 11 Then
Select Case KeyAscii
Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8, Asc(","), Asc("-")
  If auxDBGrid1(0).Columns("Nuevo").Value <> "" Then
    tlbToolbar1.Buttons(4).Enabled = True
  End If
Case Else
  KeyAscii = 0
End Select
End If

End Sub

Private Sub auxDBGrid1_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
  
  If auxDBGrid1(0).Columns("Nuevo").Value = "" Then
    auxDBGrid1(0).Columns("Cantidad").Locked = True
  Else
    auxDBGrid1(0).Columns("Cantidad").Locked = False
  End If

End Sub


Private Sub auxDBGrid2_KeyPress(Index As Integer, KeyAscii As Integer)
If auxDBGrid2(0).Col = 5 Then
Select Case KeyAscii
Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8, Asc(","), Asc("-")
  If auxDBGrid2(0).Columns("Nuevo").Value <> "" Then
    tlbToolbar1.Buttons(4).Enabled = True
  End If
Case Else
  KeyAscii = 0
End Select
End If

End Sub

Private Sub auxDBGrid2_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
  
  If auxDBGrid2(0).Columns("Nuevo").Value = "" Then
    auxDBGrid2(0).Columns(5).Locked = True
  Else
    auxDBGrid2(0).Columns(5).Locked = False
  End If

End Sub

Private Sub cmdBusProdPac_Click()
'Se realiza la busqueda del paciente o del servicio
Dim intV As Integer
Dim strIns65 As String
Dim codproducto As Currency
  
  If dtcDateCombo1(1).Text <> "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia no Cerrado", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If IdPersona1.Text = "" Then
    MsgBox "Debe seleccionar un Paciente", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If txttext1(0).Text = "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If txttext1(3).Text = "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If txttext1(5).Text = "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia con Departamento", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  
  'objWinInfo.DataSave
  cmdBusProdPac.Enabled = False
  
  Call objWinInfo.FormChangeActive(fraframe1(3), False, True)
  gblnSelProd = False
  gstrLlamador = "frmRegDev"
  Call objsecurity.LaunchProcess("FR0119")
  gstrLlamador = ""
  Me.Enabled = False
  If gblnSelProd = False Then
    'No ha seleccionado ning�n producto
    MsgBox "No ha seleccionado ning�n producto", vbInformation, "Abono de Productos"
  Else
      'Se pueden hacer los inserts
      For intV = 0 To gintprodtotal - 1
        ''Se realiza el insert en FR6500
        'strIns65 = "INSERT INTO FR6500 ("
        'strIns65 = strIns65 & "FR65CODIGO,AD07CODPROCESO,AD01CODASISTENCI"
        'strIns65 = strIns65 & ",CI21CODPERSONA,FR65FECHA,FR65HORA"
        'strIns65 = strIns65 & ",FR73CODPRODUCTO,FR65CANTIDAD"
        'strIns65 = strIns65 & ") VALUES ("
        'strIns65 = strIns65 & "FR65CODIGO_SEQUENCE.NEXTVAL,"
        'strIns65 = strIns65 & txttext1(0).Text & ","
        'strIns65 = strIns65 & txttext1(3).Text & ","
        'strIns65 = strIns65 & IdPersona1.Text & ","
        'strIns65 = strIns65 & "TRUNC(SYSDATE),"
        'strIns65 = strIns65 & "TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
        'strIns65 = strIns65 & gintprodbuscado(intV, 0) & ","
        'strIns65 = strIns65 & 0
        'strIns65 = strIns65 & ")"
        'objApp.rdoConnect.Execute strIns65, 64
      'Next intV
      'objWinInfo.DataRefresh
      'Call Actualizar_Grid
        codproducto = gintprodbuscado(intV, 0)
        Call insertar_en_grid("1", codproducto, 0, 0, "")
      Next intV
      auxDBGrid1(0).MoveLast
      auxDBGrid1(0).Col = 11
      tlbToolbar1.Buttons(4).Enabled = True
      Call objWinInfo.FormChangeActive(fraframe1(3), False, True)
  End If
''''''''''''''''''''''''''''''''''''''''''''''''''
  
  cmdBusProdPac.Enabled = True
  Me.Enabled = True

End Sub

Private Sub cmdFilServicio_Click()
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    End If
  End If

  If cboservicio.Text = "" Then
    MsgBox "Debe seleccionar el Servicio.", vbInformation, "Abono de Productos"
    Exit Sub
  End If
    
  glstrWhereSer = ""
    
  'Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
    If IsDate(dtcFecDesde(2).Date) And IsDate(dtcFecDesde(3).Date) Then
      If DateDiff("d", dtcFecDesde(2).Date, dtcFecDesde(3).Date) < 0 Then
        MsgBox "La fecha hasta debe ser mayor que la fecha desde", vbInformation, "Aviso"
        'objWinInfo.objWinActiveForm.strWhere = "-1=0"
        glstrWhereSer = "-1=0"
      Else
        'objWinInfo.objWinActiveForm.strWhere = "TRUNC(FRK1FECMOV) BETWEEN TO_DATE('" & dtcFecDesde(2).Date & "','DD/MM/YYYY') AND NVL(TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
        glstrWhereSer = "TRUNC(FRK100.FRK1FECMOV) BETWEEN TO_DATE('" & dtcFecDesde(2).Date & "','DD/MM/YYYY') AND NVL(TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
      End If
    ElseIf IsDate(dtcFecDesde(2).Date) Then
      'objWinInfo.objWinActiveForm.strWhere = "TRUNC(FRK1FECMOV) BETWEEN TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY') AND TO_DATE('31/12/9999','DD/MM/YYYY') "
      glstrWhereSer = "TRUNC(FRK100.FRK1FECMOV) BETWEEN TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY') AND TO_DATE('31/12/9999','DD/MM/YYYY') "
    ElseIf IsDate(dtcFecDesde(3).Date) Then
      'objWinInfo.objWinActiveForm.strWhere = "TRUNC(FRK1FECMOV) <= NVL(TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
      glstrWhereSer = "TRUNC(FRK100.FRK1FECMOV) <= NVL(TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
    End If
    If glstrWhereSer = "" Then
      'objWinInfo.objWinActiveForm.strWhere = "1=1"
      glstrWhereSer = "1=1"
    End If
    
    If cboservicio.Text <> "" Then
      'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND AD02CODDPTO =" & cboservicio.Text
      glstrWhereSer = glstrWhereSer & " AND FRK100.AD02CODDPTO =" & cboservicio.Text
    End If
    
    'Call objWinInfo.DataRefresh
    Call Refrescar_Grid

End Sub

Private Sub cmdFiltrar_Click()
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    End If
  End If

    'Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    If IsDate(dtcFecDesde(0).Date) And IsDate(dtcFecDesde(1).Date) Then
      If DateDiff("d", dtcFecDesde(0).Date, dtcFecDesde(1).Date) < 0 Then
        MsgBox "La fecha hasta debe ser mayor que la fecha desde", vbInformation, "Aviso"
        'objWinInfo.objWinActiveForm.strWhere = "-1=0"
        Exit Sub
      Else
        'objWinInfo.objWinActiveForm.strWhere = "TRUNC(FR65FECHA) BETWEEN TO_DATE('" & dtcFecDesde(0).Date & "','DD/MM/YYYY') AND NVL(TO_DATE('" & dtcFecDesde(1).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
      End If
    ElseIf IsDate(dtcFecDesde(0).Date) Then
      'objWinInfo.objWinActiveForm.strWhere = "TRUNC(FR65FECHA) BETWEEN TO_DATE('" & dtcFecDesde(0).Date & "','DD/MM/YYYY') AND TO_DATE('31/12/9999','DD/MM/YYYY') "
    ElseIf IsDate(dtcFecDesde(1).Date) Then
      'objWinInfo.objWinActiveForm.strWhere = "TRUNC(FR65FECHA) <= NVL(TO_DATE('" & dtcFecDesde(1).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
    Else
      'objWinInfo.objWinActiveForm.strWhere = ""
    End If
    'Call objWinInfo.DataRefresh
    Call Actualizar_Grid
    Call objWinInfo.FormChangeActive(fraframe1(3), False, True)

End Sub

Private Sub cmdleerlector_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim rstprod As rdoResultset
Dim strprod As String
Dim strInsert As String
Dim strIns65 As String
Dim codproducto As Currency
Dim cantidad As Currency

If dtcDateCombo1(1).Text <> "" Then
  MsgBox "Debe seleccionar un Proceso-Asistencia no Cerrado", vbInformation, "Aviso"
  Frame2.Visible = False
  Exit Sub
End If
If IdPersona1.Text = "" Then
  MsgBox "Debe seleccionar un Paciente", vbInformation, "Aviso"
  Frame2.Visible = False
  Exit Sub
End If
If txttext1(0).Text = "" Then
  MsgBox "Debe seleccionar un Proceso-Asistencia", vbInformation, "Aviso"
  Frame2.Visible = False
  Exit Sub
End If
If txttext1(3).Text = "" Then
  MsgBox "Debe seleccionar un Proceso-Asistencia", vbInformation, "Aviso"
  Frame2.Visible = False
  Exit Sub
End If
If txttext1(5).Text = "" Then
  MsgBox "Debe seleccionar un Proceso-Asistencia con Departamento", vbInformation, "Aviso"
  Frame2.Visible = False
  Exit Sub
End If

'objWinInfo.DataSave
cmdleerlector.Enabled = False

''''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
If objWinInfo.objWinActiveForm.strName <> "Proc/Asis" Then
  Call objWinInfo.FormChangeActive(fraframe1(3), False, True)
End If
gblnSelProd = False
Load frmLECTORDSE
Call frmLECTORDSE.Show(vbModal)
Me.Enabled = False
If gintprodtotal <= 0 Then
  'No ha seleccionado ning�n producto
  MsgBox "No ha seleccionado ning�n producto", vbInformation, "Abono de Productos"
Else
    'Se pueden hacer los inserts
    For v = 0 To gintprodtotal - 1
      ''Se realiza el insert en FR6500
      'strIns65 = "INSERT INTO FR6500 ("
      'strIns65 = strIns65 & "FR65CODIGO,AD07CODPROCESO,AD01CODASISTENCI"
      'strIns65 = strIns65 & ",CI21CODPERSONA,FR65FECHA,FR65HORA"
      'strIns65 = strIns65 & ",FR73CODPRODUCTO,FR65CANTIDAD"
      'strIns65 = strIns65 & ") VALUES ("
      'strIns65 = strIns65 & "FR65CODIGO_SEQUENCE.NEXTVAL,"
      'strIns65 = strIns65 & txttext1(0).Text & ","
      'strIns65 = strIns65 & txttext1(3).Text & ","
      'strIns65 = strIns65 & IdPersona1.Text & ","
      'strIns65 = strIns65 & "TRUNC(SYSDATE),"
      'strIns65 = strIns65 & "TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
      'strIns65 = strIns65 & gintprodbuscado(v, 0) & ","
      'strIns65 = strIns65 & gintprodbuscado(v, 3)
      'strIns65 = strIns65 & ")"
      'objApp.rdoConnect.Execute strIns65, 64
      codproducto = gintprodbuscado(v, 0)
      cantidad = gintprodbuscado(v, 3)
      Call insertar_en_grid("2", codproducto, cantidad, 0, "")
    Next v
    auxDBGrid1(0).MoveLast
    auxDBGrid1(0).Col = 11
    tlbToolbar1.Buttons(4).Enabled = True
    Call objWinInfo.FormChangeActive(fraframe1(3), False, True)
    'Next v
    'Call Actualizar_Grid
End If

cmdleerlector.Enabled = True
Me.Enabled = True
End Sub

Private Sub cmdProdNoForm2_Click()
  
  'Call objWinInfo.FormChangeActive(fraframe1(1), True, False)
  
  If cboservicio.Text = "" Then
    MsgBox "Debe seleccionar el Servicio al que se realiza el abono", vbInformation, "Abono de Productos"
    Exit Sub
  End If
  
  Frame2.Visible = True
  txtDescripcion.Text = ""
  txtPrecio.Text = ""
  txtDescripcion.Locked = False
  txtPrecio.Locked = False
  txtDescripcion.TabStop = True
  txtPrecio.TabStop = True
  
  txtDescripcion.SetFocus


End Sub

Private Sub Command2_Click()
Dim strIns65, strInsK1 As String
Dim codproducto As Currency
Dim descripcion As String
Dim precio  As Currency
  
  If txtPrecio.Text = "" And txtDescripcion.Text = "" Then
    Frame2.Visible = False
    Exit Sub
  End If
  
  If txtPrecio.Text = "" Or txtPrecio.Text = "0" Then
    MsgBox "Debe poner un precio", vbInformation, "Aviso"
    Exit Sub
  End If
  strIns65 = strIns65 & "'" & txtDescripcion.Text & "'"
  If txtDescripcion.Text = "" Then
    MsgBox "Debe poner un descripci�n", vbInformation, "Aviso"
    Exit Sub
  End If
  
  If SSTab1.Tab = 0 Then
    Screen.MousePointer = vbHourglass
    Me.Enabled = False
    'Hay un paciente con los datos necesarios
    'Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    
    'Se realiza el insert en FR6500
    'strIns65 = "INSERT INTO FR6500 ("
    'strIns65 = strIns65 & "FR65CODIGO,AD07CODPROCESO,AD01CODASISTENCI"
    'strIns65 = strIns65 & ",CI21CODPERSONA,FR65FECHA,FR65HORA"
    'strIns65 = strIns65 & ",FR73CODPRODUCTO,FR65CANTIDAD,FR65PRECIO,FR65DESPRODUCTO"
    'strIns65 = strIns65 & ") VALUES ("
    'strIns65 = strIns65 & "FR65CODIGO_SEQUENCE.NEXTVAL,"
    'strIns65 = strIns65 & txttext1(0).Text & ","
    'strIns65 = strIns65 & txttext1(3).Text & ","
    'strIns65 = strIns65 & IdPersona1.Text & ","
    'strIns65 = strIns65 & "TRUNC(SYSDATE),"
    'strIns65 = strIns65 & "TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
    'strIns65 = strIns65 & 999999999 & ","
    'strIns65 = strIns65 & 0 & ","
    'strIns65 = strIns65 & objGen.ReplaceStr(txtPrecio.Text, ",", ".", 1) & ","
    'strIns65 = strIns65 & "'" & txtDescripcion.Text & "'"
    'strIns65 = strIns65 & ")"
    'objApp.rdoConnect.Execute strIns65, 64
    codproducto = 999999999
    precio = txtPrecio.Text
    descripcion = txtDescripcion.Text
    Call insertar_en_grid("3", codproducto, 0, precio, descripcion)
    Call objWinInfo.FormChangeActive(fraframe1(3), False, True)
    'Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    'objWinInfo.DataRefresh
    
    auxDBGrid1(0).MoveLast
    auxDBGrid1(0).Col = 11
    tlbToolbar1.Buttons(4).Enabled = True
    Frame2.Visible = False
    'Call Actualizar_Grid
    Me.Enabled = True
    Screen.MousePointer = vbDefault
  Else
    Screen.MousePointer = vbHourglass
    Me.Enabled = False
    'Hay un paciente con los datos necesarios
    'Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
    ''Se realiza el insert en FRK100
    'strInsK1 = "INSERT INTO FRK100 (FRK1CODMOV,FRK1FECMOV,AD02CODDPTO,FR73CODPRODUCTO," & _
    '           "FRK1CANTIDAD,FRK1TAMENVASE,FRK1INDMOD,FRK1INDFAC,FRK1PRECIO,FRK1DESPRODUCTO) VALUES ("
    'strInsK1 = strInsK1 & "FRK1CODMOV_SEQUENCE.NEXTVAL,SYSDATE,"
    'strInsK1 = strInsK1 & cboservicio.Text & ","
    'strInsK1 = strInsK1 & "999999999,0,1"
    'strInsK1 = strInsK1 & ",0,0,"
    'strInsK1 = strInsK1 & objGen.ReplaceStr(txtPrecio.Text, ",", ".", 1) & ","
    'strInsK1 = strInsK1 & "'" & txtDescripcion.Text & "'"
    'strInsK1 = strInsK1 & ")"
    'objApp.rdoConnect.Execute strInsK1, 64
    codproducto = 999999999
    precio = txtPrecio.Text
    descripcion = txtDescripcion.Text
    Call insertar_en_grid_servicio("3", codproducto, 0, precio, descripcion)
    tlbToolbar1.Buttons(4).Enabled = True
  
    'Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
    'objWinInfo.DataRefresh
    auxDBGrid2(0).MoveLast
    auxDBGrid2(0).Col = 5
    Frame2.Visible = False
    Me.Enabled = True
    Screen.MousePointer = vbDefault
  End If

End Sub

Private Sub cmdProdNoFormPac_Click()
  
  If dtcDateCombo1(1).Text <> "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia no Cerrado", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If IdPersona1.Text = "" Then
    MsgBox "Debe seleccionar un Paciente", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If txttext1(0).Text = "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If txttext1(3).Text = "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  If txttext1(5).Text = "" Then
    MsgBox "Debe seleccionar un Proceso-Asistencia con Departamento", vbInformation, "Aviso"
    Frame2.Visible = False
    Exit Sub
  End If
  
  If objWinInfo.objWinActiveForm.strName <> "Proc/Asis" Then
    Call objWinInfo.FormChangeActive(fraframe1(3), False, False)
  End If
  
  Frame2.Visible = True
  txtDescripcion.Text = ""
  txtPrecio.Text = ""
  txtDescripcion.Locked = False
  txtPrecio.Locked = False
  txtDescripcion.TabStop = True
  txtPrecio.TabStop = True
  
  txtDescripcion.SetFocus

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



Private Sub cboservicio_Click()
  txtServicio.Text = cboservicio.Columns(1).Value
End Sub

Private Sub cboservicio_CloseUp()
  txtServicio.Text = cboservicio.Columns(1).Value
End Sub



Private Sub Command1_Click()
Dim intV As Integer
Dim codproducto As Currency
  
  'objWinInfo.DataSave
  Command1.Enabled = False
    
    'Se realiza el abono al servicio
    SSTab1.Tab = 1
    If cboservicio.Text = "" Then
      MsgBox "Debe seleccionar el Servicio al que se realiza el abono", vbInformation, "Abono de Productos"
    Else
      'Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
      'Se llama al buscador de productos
      gblnSelProd = False
      gstrLlamador = "frmRegDev"
      Call objsecurity.LaunchProcess("FR0119")
      gstrLlamador = ""
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      If gblnSelProd = False Then
        'No ha seleccionado ning�n producto
        MsgBox "No ha seleccionado ning�n producto", vbInformation, "Abono de Productos"
      Else
        ''Se pueden hacer los inserts
        For intV = 0 To gintprodtotal - 1
          codproducto = gintprodbuscado(intV, 0)
          Call insertar_en_grid_servicio("1", codproducto, 0, 0, "")
          tlbToolbar1.Buttons(4).Enabled = True
        Next intV
        auxDBGrid2(0).MoveLast
        auxDBGrid2(0).Col = 5
      End If
    End If
  
  Command1.Enabled = True
  Screen.MousePointer = vbDefault
  Me.Enabled = True

End Sub


Private Sub Form_Activate()
  Dim stra As String
  Dim qrya As rdoQuery
  Dim rsta As rdoResultset
  
  cboservicio.RemoveAll
  cboServAlm.RemoveAll
  stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
            "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
            "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL))) ORDER BY AD02DESDPTO"
  Set qrya = objApp.rdoConnect.CreateQuery("", stra)
  Set rsta = qrya.OpenResultset()
  While (Not rsta.EOF)
    Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
    Call cboServAlm.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
    rsta.MoveNext
  Wend
  qrya.Close
  Set qrya = Nothing
  Set rsta = Nothing
  
  auxDBGrid1(0).Columns("CodInt_dil").Width = 900  'interno_dil
  auxDBGrid1(0).Columns("Producto_dil").Width = 3300 'producto_dil
  auxDBGrid1(0).Columns("Cant_dil").Width = 800  'cantidad_dil
  auxDBGrid1(0).Columns("CodInt_mez").Width = 1000  'interno_mez
  auxDBGrid1(0).Columns("Producto_mez").Width = 3300 'producto_mez
  auxDBGrid1(0).Columns("Cant_mez").Width = 850  'cantidad_mez
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
'Dim objDetailInfo As New clsCWForm
'Dim objMultiInfo As New clsCWForm
Dim objDetailInfo1 As New clsCWForm
Dim objMultiInfo2 As New clsCWForm
Dim strKey As String
Dim selectDpto As String
Dim j As Integer
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo1
    .strName = "Pacientes"
    Set .objFormContainer = fraframe1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
    .strTable = "CI2200"
    .intAllowance = cwAllowReadOnly
    strKey = .strDataBase & .strTable
  End With
   
  With objMultiInfo2
    .strName = "Proc/Asis"
    Set .objFormContainer = fraframe1(3)
    Set .objFatherContainer = fraframe1(2)
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AD0804J"
    .intAllowance = cwAllowReadOnly
    
    '.strWhere = "AD01CODASISTENCI IN (SELECT AD01CODASISTENCI FROM AD0100 " & _
                                     " WHERE TO_DATE(TO_CHAR(AD01FECINICIO,'DD/MM/YYYY'),'DD/MM/YYYY')<=(SELECT TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') FROM DUAL))"
    Call .FormAddOrderField("AD08FECINICIO", cwDescending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)

    'strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "OM")
  
  End With
  
  'With objDetailInfo
  '  .strName = "Paciente"
  '  Set .objFormContainer = fraframe1(0)
  '  Set .objFatherContainer = fraframe1(3)
  '  Set .tabMainTab = Nothing
  '  Set .grdGrid = grdDBGrid1(0)
  '  .strTable = "FR6500"
  '  .blnAskPrimary = False
  '
  '  .strWhere = "TRUNC(FR65FECHA) BETWEEN TO_DATE('" & dtcFecDesde(0).Date & "','DD/MM/YYYY') AND NVL(TO_DATE('" & dtcFecDesde(1).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
  '
  '  Call .FormAddRelation("AD07CODPROCESO", txttext1(0))
  '  Call .FormAddRelation("AD01CODASISTENCI", txttext1(3))
  '
  '  '.blnMasiveOn = True
  '  Call .FormAddOrderField("FR65FECHA", cwDescending)
  '  Call .FormAddOrderField("FR65CODIGO", cwDescending)
  '  Call .FormAddOrderField("FR65HORA", cwDescending)
  '
  '  strKey = .strDataBase & .strTable
  '  .intAllowance = cwAllowDelete + cwAllowModify
  '
  'End With
   
  'With objMultiInfo
  '  .strName = "Servicio"
  '  Set .objFormContainer = fraframe1(1)
  '  Set .objFatherContainer = Nothing
  '  Set .tabMainTab = Nothing
  '  Set .grdGrid = grdDBGrid1(1)
  '  .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  '  .strTable = "FRK100"
  '  If IsNumeric(mstrCodMov) Then
  '    .strWhere = "FRK1CODMOV = " & mstrCodMov
  '  Else
  '    .strWhere = "FRK1CODMOV IS NULL "
  '  End If
  '  Call .FormAddOrderField("FRK1CODMOV", cwDescending)
  '  Call .FormAddOrderField("FRK1FECMOV", cwDescending)
  '  Call .FormAddOrderField("AD02CODDPTO", cwAscending)
  '  strKey = .strDataBase & .strTable
  '  .intAllowance = cwAllowDelete + cwAllowModify
  '
  'End With
 
  With objWinInfo
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
    Call .FormAddInfo(objMultiInfo2, cwFormDetail)
    'Call .FormAddInfo(objDetailInfo, cwFormMultiLine)
    'Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
    'Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
    'Call .GridAddColumn(objMultiInfo, "CodInt", "", cwString, 6)
    'Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 10)
    'Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 30)
    'Call .GridAddColumn(objMultiInfo, "Envase", "FRK1TAMENVASE", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Cant", "FRK1CANTIDAD", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Fecha", "FRK1FECMOV", cwDate, 10)
    'Call .GridAddColumn(objMultiInfo, "CodMov", "FRK1CODMOV", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "CodDpto", "AD02CODDPTO", cwNumeric, 3)
    'Call .GridAddColumn(objMultiInfo, "CodPrd", "FR73CODPRODUCTO", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "MOD", "FRK1INDMOD", cwNumeric, 1)
    'Call .GridAddColumn(objMultiInfo, "FAC", "FRK1INDFAC", cwNumeric, 1)
    'Call .GridAddColumn(objMultiInfo, "Precio", "FRK1PRECIO", cwDecimal, 19)
    'Call .GridAddColumn(objMultiInfo, "Desc.Prod. No Form.", "FRK1DESPRODUCTO", cwString, 30)
        
    ''Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
    'Call .GridAddColumn(objDetailInfo, "Cod.Abo.", "FR65CODIGO", cwNumeric, 9)
    'Call .GridAddColumn(objDetailInfo, "Proceso", "AD07CODPROCESO", cwNumeric, 10)
    'Call .GridAddColumn(objDetailInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
    'Call .GridAddColumn(objDetailInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    'Call .GridAddColumn(objDetailInfo, "Fecha", "FR65FECHA", cwDate)
    'Call .GridAddColumn(objDetailInfo, "Hora", "FR65HORA", cwDecimal, 6)
    'Call .GridAddColumn(objDetailInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    'Call .GridAddColumn(objDetailInfo, "CodInt", "", cwString, 6)
    'Call .GridAddColumn(objDetailInfo, "Referencia", "", cwString, 10)
    'Call .GridAddColumn(objDetailInfo, "Producto", "", cwString, 30)
    'Call .GridAddColumn(objDetailInfo, "Envase", "", cwNumeric, 9)
    'Call .GridAddColumn(objDetailInfo, "Cantidad", "FR65CANTIDAD", cwDecimal, 7)
    'Call .GridAddColumn(objDetailInfo, "Precio", "FR65PRECIO", cwDecimal, 19)
    'Call .GridAddColumn(objDetailInfo, "Desc.Prod. No Form.", "FR65DESPRODUCTO", cwString, 30)
    
    Call .FormCreateInfo(objDetailInfo1)
    'Call .FormCreateInfo(objMultiInfo)
    
    'Call .FormChangeColor(objMultiInfo)
    'Call .FormChangeColor(objDetailInfo)
    
    '.CtrlGetInfo(grdDBGrid1(0).Columns("Fecha")).blnReadOnly = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns("Hora")).blnReadOnly = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns("Cantidad")).blnMandatory = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns("Precio")).blnReadOnly = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns("Desc.Prod. No Form.")).blnReadOnly = True
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodDpto")), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodDpto")), grdDBGrid1(1).Columns("Servicio"), "AD02DESDPTO")

    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodPrd")), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR7300.FR73CODPRODUCTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodPrd")), grdDBGrid1(1).Columns("CodInt"), "FR73CODINTFAR")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodPrd")), grdDBGrid1(1).Columns("Referencia"), "FR73REFERENCIA")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodPrd")), grdDBGrid1(1).Columns("Producto"), "FR73DESPRODUCTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodPrd")), grdDBGrid1(1).Columns("Envase"), "FR73TAMENVASE")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("C�d.Producto")), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("C�d.Producto")), grdDBGrid1(0).Columns("CodInt"), "FR73CODINTFAR")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("C�d.Producto")), grdDBGrid1(0).Columns("Referencia"), "FR73REFERENCIA")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("C�d.Producto")), grdDBGrid1(0).Columns("Producto"), "FR73DESPRODUCTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns("C�d.Producto")), grdDBGrid1(0).Columns("Envase"), "FR73TAMENVASE")
    
    '.CtrlGetInfo(grdDBGrid1(0).Columns("CodInt")).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns("Producto")).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(3)), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txttext1(3)), txttext1(2), "GCFN06(AD15CODCAMA)")
    
    selectDpto = "SELECT AD0400.AD02CODDPTO,AD02DESDPTO,SG0200.SG02NOM || ',' || SG0200.SG02APE1 || ',' || SG0200.SG02APE2 DOCTORRESP,SG0200.SG02COD FROM AD0400,AD0200,SG0200 WHERE "
    selectDpto = selectDpto & " AD0200.AD02CODDPTO=AD0400.AD02CODDPTO"
    selectDpto = selectDpto & " AND AD0400.SG02COD=SG0200.SG02COD"
    selectDpto = selectDpto & " AND AD0400.AD07CODPROCESO=?"
    'selectDpto = selectDpto & " AND AD0500.AD01CODASISTENCI=?"
    Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(0)), "AD07CODPROCESO", selectDpto)
    Call .CtrlAddLinked(.CtrlGetInfo(txttext1(0)), txttext1(1), "DOCTORRESP")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(6)), "AD07CODPROCESO", selectDpto)
    Call .CtrlAddLinked(.CtrlGetInfo(txttext1(6)), txttext1(4), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(8)), "AD07CODPROCESO", selectDpto)
    Call .CtrlAddLinked(.CtrlGetInfo(txttext1(8)), txttext1(5), "AD02CODDPTO")
    
    Call .WinRegister
    Call .WinStabilize
    
    Frame2.Visible = False
    
    'grdDBGrid1(1).Columns("<Estado>").Width = 0
    'grdDBGrid1(1).Columns("CodMov").Visible = False
    'grdDBGrid1(1).Columns("CodDpto").Visible = False
    'grdDBGrid1(1).Columns("CodPrd").Visible = False
    'grdDBGrid1(1).Columns("MOD").Visible = False
    'grdDBGrid1(1).Columns("FAC").Visible = False
    'grdDBGrid1(1).Columns("Servicio").Width = 1800
    'grdDBGrid1(1).Columns("Servicio").Position = 1
    'grdDBGrid1(1).Columns("CodInt").Width = 700
    'grdDBGrid1(1).Columns("CodInt").Position = 2
    'grdDBGrid1(1).Columns("Referencia").Width = 1000
    'grdDBGrid1(1).Columns("Referencia").Position = 3
    'grdDBGrid1(1).Columns("Producto").Width = 2500
    'grdDBGrid1(1).Columns("Producto").Position = 4
    'grdDBGrid1(1).Columns("Envase").Width = 600
    'grdDBGrid1(1).Columns("Envase").Position = 5
    'grdDBGrid1(1).Columns("Cant").Width = 800
    'grdDBGrid1(1).Columns("Cant").Position = 6
    'grdDBGrid1(1).Columns("Fecha").Width = 1000
    'grdDBGrid1(1).Columns("Fecha").Position = 7
    'grdDBGrid1(1).Columns("Precio").Width = 800
    'grdDBGrid1(1).Columns("Desc.Prod. No Form.").Width = 3000
    
    'grdDBGrid1(0).Columns("Cod.Abo.").Visible = False
    'grdDBGrid1(0).Columns("<Estado>").Width = 0
    'grdDBGrid1(0).Columns("Proceso").Visible = False
    'grdDBGrid1(0).Columns("Asistencia").Visible = False
    'grdDBGrid1(0).Columns("Persona").Visible = False
    'grdDBGrid1(0).Columns("Fecha").Width = 1000
    'grdDBGrid1(0).Columns("Hora").Width = 500
    'grdDBGrid1(0).Columns("C�d.Producto").Visible = False
    'grdDBGrid1(0).Columns("Cantidad").Width = 800
    'grdDBGrid1(0).Columns("CodInt").Width = 700
    'grdDBGrid1(0).Columns("Referencia").Width = 1000
    'grdDBGrid1(0).Columns("Producto").Width = 3000
    'grdDBGrid1(0).Columns("Envase").Visible = False
    'grdDBGrid1(0).Columns("Precio").Width = 800
    'grdDBGrid1(0).Columns("Desc.Prod. No Form.").Width = 3000
    
  End With

  IdPersona1.blnSearchButton = True
  txtDescripcion.BackColor = vbCyan
  txtPrecio.BackColor = vbCyan

  cmdBusProdPac.Enabled = False
  cmdProdNoFormPac.Enabled = False
  cmdleerlector.Enabled = False

auxDBGrid2(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 14
  Call auxDBGrid2(0).Columns.Add(j)
Next j
auxDBGrid2(0).Columns(0).Caption = "Servicio"
auxDBGrid2(0).Columns(0).Visible = True
auxDBGrid2(0).Columns(0).Locked = True
auxDBGrid2(0).Columns(0).Width = 1800
auxDBGrid2(0).Columns(1).Caption = "CodInt"
auxDBGrid2(0).Columns(1).Visible = True
auxDBGrid2(0).Columns(1).Locked = True
auxDBGrid2(0).Columns(1).Width = 700
auxDBGrid2(0).Columns(2).Caption = "Referencia"
auxDBGrid2(0).Columns(2).Visible = True
auxDBGrid2(0).Columns(2).Locked = True
auxDBGrid2(0).Columns(2).Width = 1000
auxDBGrid2(0).Columns(3).Caption = "Producto"
auxDBGrid2(0).Columns(3).Visible = True
auxDBGrid2(0).Columns(3).Locked = True
auxDBGrid2(0).Columns(3).Width = 2500
auxDBGrid2(0).Columns(4).Caption = "Envase"
auxDBGrid2(0).Columns(4).Visible = True
auxDBGrid2(0).Columns(4).Locked = True
auxDBGrid2(0).Columns(5).Width = 600
auxDBGrid2(0).Columns(5).Caption = "Cant"
auxDBGrid2(0).Columns(5).Visible = True
auxDBGrid2(0).Columns(5).Locked = True
auxDBGrid2(0).Columns(5).Width = 800
auxDBGrid2(0).Columns(5).BackColor = vbCyan
auxDBGrid2(0).Columns(6).Caption = "Fecha"
auxDBGrid2(0).Columns(6).Visible = True
auxDBGrid2(0).Columns(6).Locked = True
auxDBGrid2(0).Columns(6).Width = 1000
auxDBGrid2(0).Columns(7).Caption = "CodMov"
auxDBGrid2(0).Columns(7).Visible = False
auxDBGrid2(0).Columns(7).Locked = True
auxDBGrid2(0).Columns(8).Caption = "CodDpto"
auxDBGrid2(0).Columns(8).Visible = False
auxDBGrid2(0).Columns(8).Locked = True
auxDBGrid2(0).Columns(9).Caption = "CodPrd"
auxDBGrid2(0).Columns(9).Visible = False
auxDBGrid2(0).Columns(9).Locked = True
auxDBGrid2(0).Columns(10).Caption = "MOD"
auxDBGrid2(0).Columns(10).Visible = False
auxDBGrid2(0).Columns(10).Locked = True
auxDBGrid2(0).Columns(11).Caption = "FAC"
auxDBGrid2(0).Columns(11).Visible = False
auxDBGrid2(0).Columns(11).Locked = True
auxDBGrid2(0).Columns(12).Caption = "Precio"
auxDBGrid2(0).Columns(12).Visible = True
auxDBGrid2(0).Columns(12).Locked = True
auxDBGrid2(0).Columns(12).Width = 800
auxDBGrid2(0).Columns(13).Caption = "Desc.Prod. No Form."
auxDBGrid2(0).Columns(13).Visible = True
auxDBGrid2(0).Columns(13).Locked = True
auxDBGrid2(0).Columns(13).Width = 3000
auxDBGrid2(0).Columns(14).Caption = "Nuevo"
auxDBGrid2(0).Columns(14).Visible = False
auxDBGrid2(0).Columns(14).Locked = True

auxDBGrid2(0).Redraw = True
glstrWhereSer = ""
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
        intCancel = -1
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        intCancel = -1
        Exit Sub
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        intCancel = -1
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        intCancel = -1
        Exit Sub
      End If
    End If
  End If
  intCancel = objWinInfo.WinExit

End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)

  If Index = 3 Then
    grdDBGrid1(Index).Columns("Proceso").Position = 0
    grdDBGrid1(Index).Columns("Asistencia").Position = 1
    grdDBGrid1(Index).Columns("Doctor Responsable").Position = 2
    grdDBGrid1(Index).Columns("Dpto.").Position = 3
    grdDBGrid1(Index).Columns("Dpto.").Width = 600
    grdDBGrid1(Index).Columns("Dpto. Responsable").Position = 4
    grdDBGrid1(Index).Columns("Cama").Position = 5
    grdDBGrid1(Index).Columns("Cama").Width = 600
    
    grdDBGrid1(Index).Columns("Fec.Inicio").Width = 1200
    grdDBGrid1(Index).Columns("Fec.Fin").Width = 1200
    
    grdDBGrid1(Index).Columns("Proceso2").Visible = False
    grdDBGrid1(Index).Columns("Proceso3").Visible = False
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  Select Case strCtrl
    Case "IdPersona1"                  'nuevo
      IdPersona1.SearchPersona         'nuevo
  End Select
  
  If strFormName = "Medicamentos" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = ""
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txttext1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strFormName = "Paciente" And (strCtrl = "txtText1(11)" Or strCtrl = "txtText1(12)") Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0800"
     
     .strWhere = " WHERE AD01CODASISTENCI IN (SELECT AD01CODASISTENCI FROM AD0100 WHERE CI21CODPERSONA=" & txttext1(0).Text & ")"
     
     .strOrder = "ORDER BY AD08FECINICIO DESC"
     
     Set objField = .AddField("AD07CODPROCESO")
     objField.strSmallDesc = "C�digo Proceso"
     
     Set objField = .AddField("AD01CODASISTENCI")
     objField.strSmallDesc = "C�digo Asistencia"
         
     Set objField = .AddField("AD08NUMCASO")
     objField.strSmallDesc = "Caso"
     
     Set objField = .AddField("AD08FECINICIO")
     objField.strSmallDesc = "Fecha Inicio"
     
     Set objField = .AddField("AD08FECFIN")
     objField.strSmallDesc = "Fecha Fin"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(txttext1(11), .cllValues("AD07CODPROCESO"))
      Call objWinInfo.CtrlSet(txttext1(12), .cllValues("AD01CODASISTENCI"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)

If strFormName = "Proc/Asis" Then
  If strCtrlName = "txttext1(3)" Then
    'aValues(2) = txttext1(0).Text  'proceso
    aValues(2) = objWinInfo.objWinActiveForm.rdoCursor("AD07CODPROCESO").Value
'  ElseIf strCtrlName = "txttext1(0)" Then
    'aValues(2) = txttext1(3).Text  'Asistencia
'    aValues(2) = objWinInfo.objWinActiveForm.rdoCursor("AD01CODASISTENCI").Value
  End If
End If

End Sub



Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

  If dtcDateCombo1(1).Text <> "" Then
    cmdBusProdPac.Enabled = False
    cmdProdNoFormPac.Enabled = False
    cmdleerlector.Enabled = False
  Else
    cmdBusProdPac.Enabled = True
    cmdProdNoFormPac.Enabled = True
    cmdleerlector.Enabled = True
  End If

End Sub

'Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  'Escribir los apuntes en FR8000 y FR3500
  'If strFormName = "Servicio" Then
  '  RealizarApuntes1
  'Else
  '  'RealizarApuntes2
  'End If
'End Sub


Private Sub objWinInfo_cwPreChangeForm(ByVal strFormName As String)
Dim Respuesta

  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    End If
  End If

End Sub

Private Sub optFactAlm_Click(Index As Integer)

  If Index = 0 Then
    cboServAlm = ""
  End If

End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If PreviousTab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = True Then
        Exit Sub
      End If
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      If btnButton.Index <> 4 Then
        Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
        Select Case Respuesta
        Case vbYes
          Call Guardar
        Case vbNo
          tlbToolbar1.Buttons(4).Enabled = False
          auxDBGrid1(0).RemoveAll
        Case vbCancel
          Exit Sub
        End Select
      Else
        Call Guardar
      End If
      If tlbToolbar1.Buttons(4).Enabled = False Then
        If btnButton.Index <> 4 Then
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    Else
      If btnButton.Index <> 4 Then
        Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
        Select Case Respuesta
        Case vbYes
          Call Guardar_Servicio
        Case vbNo
          tlbToolbar1.Buttons(4).Enabled = False
          auxDBGrid2(0).RemoveAll
        Case vbCancel
          Exit Sub
        End Select
      Else
        Call Guardar_Servicio
      End If
      If tlbToolbar1.Buttons(4).Enabled = False Then
        If btnButton.Index <> 4 Then
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  End If

End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  End If
  
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End If
  
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
Dim Respuesta
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    If SSTab1.Tab = 0 Then
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid1(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
      End If
    Else
      Respuesta = MsgBox("�Desea salvar los cambios realizados?", vbYesNo + vbInformation, "Pregunta")
      Select Case Respuesta
      Case vbYes
        Call Guardar_Servicio
      Case vbNo
        tlbToolbar1.Buttons(4).Enabled = False
        auxDBGrid2(0).RemoveAll
      Case vbCancel
        Exit Sub
      End Select
      If tlbToolbar1.Buttons(4).Enabled = False Then
        Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
      End If
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
    
Select Case KeyAscii
Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
Case 13
  Call Command2_Click
Case Else
  KeyAscii = 0
End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 5 Then
    If txttext1(5).Text <> "" Then
      cmdBusProdPac.Enabled = True
      cmdProdNoFormPac.Enabled = True
      cmdleerlector.Enabled = True
    Else
      cmdBusProdPac.Enabled = False
      cmdProdNoFormPac.Enabled = False
      cmdleerlector.Enabled = False
    End If
  End If
  
  If intIndex = 0 Then
    auxDBGrid1(0).RemoveAll
  End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Actualizar_Grid()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strproddil As String
Dim rstproddil As rdoResultset
Dim prod_dil As String
Dim descprod_dil As String
Dim cant_dil As String
Dim strprodmez As String
Dim rstprodmez As rdoResultset
Dim prod_mez As String
Dim descprod_mez As String
Dim cant_mez As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
strSelect = "SELECT FR6500.FR65CODIGO,"
strSelect = strSelect & "FR6500.AD07CODPROCESO,"
strSelect = strSelect & "FR6500.AD01CODASISTENCI,"
strSelect = strSelect & "FR6500.CI21CODPERSONA,"
strSelect = strSelect & "FR6500.FR65FECHA,"
strSelect = strSelect & "FR6500.FR65HORA,"
strSelect = strSelect & "FR6500.FR73CODPRODUCTO,"
strSelect = strSelect & "FR7300.FR73CODINTFAR,"
strSelect = strSelect & "FR7300.FR73REFERENCIA,"
strSelect = strSelect & "FR7300.FR73DESPRODUCTO,"
strSelect = strSelect & "FR7300.FR73TAMENVASE,"
strSelect = strSelect & "FR6500.FR65CANTIDAD,"
strSelect = strSelect & "FR6500.FR65PRECIO,"
strSelect = strSelect & "FR6500.FR65DESPRODUCTO,"

strSelect = strSelect & "FR6500.FR73CODPRODUCTO_DIL,"
strSelect = strSelect & "FR6500.FR65CANTIDADDIL,"
strSelect = strSelect & "FR6500.FR73CODPRODUCTO_2,"
strSelect = strSelect & "FR6500.FR65DOSIS_2"

strSelect = strSelect & " FROM FR6500,FR7300"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR6500.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"

strSelect = strSelect & " AND FR6500.AD07CODPROCESO=?"
strSelect = strSelect & " AND FR6500.AD01CODASISTENCI=?"

If IsDate(dtcFecDesde(0).Date) And IsDate(dtcFecDesde(1).Date) Then
  If DateDiff("d", dtcFecDesde(0).Date, dtcFecDesde(1).Date) < 0 Then
  Else
    strSelect = strSelect & " AND TRUNC(FR65FECHA) BETWEEN TO_DATE('" & dtcFecDesde(0).Date & "','DD/MM/YYYY') AND NVL(TO_DATE('" & dtcFecDesde(1).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
  End If
ElseIf IsDate(dtcFecDesde(0).Date) Then
  strSelect = strSelect & " AND TRUNC(FR65FECHA) BETWEEN TO_DATE('" & dtcFecDesde(0).Date & "','DD/MM/YYYY') AND TO_DATE('31/12/9999','DD/MM/YYYY') "
ElseIf IsDate(dtcFecDesde(1).Date) Then
  strSelect = strSelect & " AND TRUNC(FR65FECHA) <= NVL(TO_DATE('" & dtcFecDesde(1).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
Else
End If

strSelect = strSelect & " ORDER BY FR65FECHA DESC,FR65CODIGO DESC,FR65HORA DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
qrya(0) = txttext1(0).Text
qrya(1) = txttext1(3).Text
Set rsta = qrya.OpenResultset()

auxDBGrid1(0).Redraw = False
auxDBGrid1(0).RemoveAll
While Not rsta.EOF
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  'producto diluyente
  If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
    strproddil = "SELECT FR73CODINTFAR,FR73DESPRODUCTO FROM FR7300 WHERE "
    strproddil = strproddil & " FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
    Set rstproddil = objApp.rdoConnect.OpenResultset(strproddil)
    If Not rstproddil.EOF Then
      prod_dil = rstproddil.rdoColumns("FR73CODINTFAR").Value
      descprod_dil = rstproddil.rdoColumns("FR73DESPRODUCTO").Value
    Else
      prod_dil = ""
      descprod_dil = ""
    End If
    rstproddil.Close
    Set rstproddil = Nothing
    If Not IsNull(rsta.rdoColumns("FR65CANTIDADDIL").Value) Then
      cant_dil = rsta.rdoColumns("FR65CANTIDADDIL").Value
    Else
      cant_dil = ""
    End If
  Else
    prod_dil = ""
    descprod_dil = ""
    cant_dil = ""
  End If
  
  'producto mezcla
  If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
    strprodmez = "SELECT FR73CODINTFAR,FR73DESPRODUCTO FROM FR7300 WHERE "
    strprodmez = strprodmez & " FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO_2").Value
    Set rstprodmez = objApp.rdoConnect.OpenResultset(strprodmez)
    If Not rstprodmez.EOF Then
      prod_mez = rstprodmez.rdoColumns("FR73CODINTFAR").Value
      descprod_mez = rstprodmez.rdoColumns("FR73DESPRODUCTO").Value
    Else
      prod_mez = ""
      descprod_mez = ""
    End If
    rstprodmez.Close
    Set rstprodmez = Nothing
    If Not IsNull(rsta.rdoColumns("FR65DOSIS_2").Value) Then
      cant_mez = rsta.rdoColumns("FR65DOSIS_2").Value
    Else
      cant_mez = ""
    End If
  Else
    prod_mez = ""
    descprod_mez = ""
    cant_mez = ""
  End If
  
  
 ' prod_dil & Chr(vbKeyTab) & _
                   descprod_dil & Chr(vbKeyTab) & _
                   cant_dil & Chr(vbKeyTab) & _
                   prod_mez & Chr(vbKeyTab) & _
                   descprod_mez & Chr(vbKeyTab) & _
                   cant_mez & Chr(vbKeyTab) & _
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  auxDBGrid1(0).AddItem rsta("FR65CODIGO").Value & Chr(vbKeyTab) & _
                    rsta("AD07CODPROCESO").Value & Chr(vbKeyTab) & _
                    rsta("AD01CODASISTENCI").Value & Chr(vbKeyTab) & _
                    rsta("CI21CODPERSONA").Value & Chr(vbKeyTab) & _
                    rsta("FR65FECHA").Value & Chr(vbKeyTab) & _
                    rsta("FR65HORA").Value & Chr(vbKeyTab) & _
                    rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                    rsta("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                    rsta("FR73REFERENCIA").Value & Chr(vbKeyTab) & _
                    rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & _
                    rsta("FR73TAMENVASE").Value & Chr(vbKeyTab) & _
                    rsta("FR65CANTIDAD").Value & Chr(vbKeyTab) & _
                    rsta("FR65PRECIO").Value & Chr(vbKeyTab) & _
                    rsta("FR65DESPRODUCTO").Value & Chr(vbKeyTab) & _
                    prod_dil & Chr(vbKeyTab) & _
                    descprod_dil & Chr(vbKeyTab) & _
                    cant_dil & Chr(vbKeyTab) & _
                    prod_mez & Chr(vbKeyTab) & _
                    descprod_mez & Chr(vbKeyTab) & _
                    cant_mez & Chr(vbKeyTab)
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
qrya.Close
Set qrya = Nothing
auxDBGrid1(0).Redraw = True

Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Guardar()
Dim i As Integer
Dim strIns65 As String
Dim tipo As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
  auxDBGrid1(0).Redraw = False
  auxDBGrid1(0).MoveFirst
  For i = 0 To auxDBGrid1(0).Rows
    If auxDBGrid1(0).Columns("Cantidad").Value = "" Then
      MsgBox "El campo Cantidad es Obligatorio", vbInformation, "Aviso"
      auxDBGrid1(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf Not IsNumeric(auxDBGrid1(0).Columns("Cantidad").Value) Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      auxDBGrid1(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf auxDBGrid1(0).Columns("Cantidad").Value > 9999.99 Or auxDBGrid1(0).Columns("Cantidad").Value < -9999.99 Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      auxDBGrid1(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf auxDBGrid1(0).Columns("Cantidad").Value = 0 Then
      MsgBox "El campo Cantidad no puede ser 0", vbInformation, "Aviso"
      auxDBGrid1(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    auxDBGrid1(0).MoveNext
  Next i
  auxDBGrid1(0).Redraw = True
  
  auxDBGrid1(0).Redraw = False
  auxDBGrid1(0).MoveFirst
  For i = 0 To auxDBGrid1(0).Rows - 1
    If auxDBGrid1(0).Columns("Nuevo").Value <> "" Then
      If optFactAlm(0) Or optFactAlm(2) Then
        ''Se realiza el insert en FR6500
        strIns65 = "INSERT INTO FR6500 ("
        strIns65 = strIns65 & "FR65CODIGO,AD07CODPROCESO,AD01CODASISTENCI"
        strIns65 = strIns65 & ",CI21CODPERSONA,FR65FECHA,FR65HORA"
        strIns65 = strIns65 & ",FR73CODPRODUCTO,FR65CANTIDAD"
        If auxDBGrid1(0).Columns("Precio").Value <> "" Then
          strIns65 = strIns65 & ",FR65PRECIO"
        End If
        If auxDBGrid1(0).Columns("Desc.Prod. No Form.").Value <> "" Then
          strIns65 = strIns65 & ",FR65DESPRODUCTO"
        End If
        strIns65 = strIns65 & ") VALUES ("
        strIns65 = strIns65 & auxDBGrid1(0).Columns("Cod.Abo.").Value & ","
        strIns65 = strIns65 & auxDBGrid1(0).Columns("Proceso").Value & ","
        strIns65 = strIns65 & auxDBGrid1(0).Columns("Asistencia").Value & ","
        strIns65 = strIns65 & auxDBGrid1(0).Columns("Persona").Value & ","
        strIns65 = strIns65 & "TO_DATE('" & auxDBGrid1(0).Columns("Fecha").Value & "','DD/MM/YYYY HH24:MI:SS'),"
        strIns65 = strIns65 & objGen.ReplaceStr(auxDBGrid1(0).Columns("Hora").Value, ",", ".", 1) & ","
        strIns65 = strIns65 & auxDBGrid1(0).Columns("C�d.Producto").Value & ","
        strIns65 = strIns65 & objGen.ReplaceStr(auxDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1)
        If auxDBGrid1(0).Columns("Precio").Value <> "" Then
          strIns65 = strIns65 & "," & objGen.ReplaceStr(auxDBGrid1(0).Columns("Precio").Value, ",", ".", 1)
        End If
        If auxDBGrid1(0).Columns("Desc.Prod. No Form.").Value <> "" Then
          strIns65 = strIns65 & ",'" & auxDBGrid1(0).Columns("Desc.Prod. No Form.").Value & "'"
        End If
        strIns65 = strIns65 & ")"
        objApp.rdoConnect.Execute strIns65, 64
      End If
      If optFactAlm(1) Or optFactAlm(2) Then
        If auxDBGrid1(0).Columns("Cantidad").Value < 0 Then
          tipo = "abono"
        Else
          tipo = "cargo"
        End If
        If cboServAlm <> "" Then
          Call RealizarApuntes2(cboServAlm, auxDBGrid1(0).Columns("C�d.Producto").Value, Abs(auxDBGrid1(0).Columns("Cantidad").Value), tipo)
        Else
          Call RealizarApuntes2(txttext1(5).Text, auxDBGrid1(0).Columns("C�d.Producto").Value, Abs(auxDBGrid1(0).Columns("Cantidad").Value), tipo)
        End If
      End If
    End If
    auxDBGrid1(0).MoveNext
  Next i
  auxDBGrid1(0).MoveFirst
  auxDBGrid1(0).Redraw = True

tlbToolbar1.Buttons(4).Enabled = False
Call Actualizar_Grid
Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub

Public Sub insertar_en_grid(tipo As String, codproducto As Currency, cantidad As Currency, precio As Currency, descripcion As String)
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strLineaGrid As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
strSelect = "SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
strSelect = strSelect & txttext1(0).Text & ","
strSelect = strSelect & txttext1(3).Text & ","
strSelect = strSelect & IdPersona1.Text & ","
strSelect = strSelect & "SYSDATE,"
strSelect = strSelect & "TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
strSelect = strSelect & "FR73CODPRODUCTO,"
strSelect = strSelect & "FR73CODINTFAR,"
strSelect = strSelect & "FR73REFERENCIA,"
strSelect = strSelect & "FR73DESPRODUCTO,"
strSelect = strSelect & "FR73TAMENVASE"

strSelect = strSelect & " FROM FR7300"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR73CODPRODUCTO=?"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
qrya(0) = codproducto
Set rsta = qrya.OpenResultset()

auxDBGrid1(0).Redraw = False
While Not rsta.EOF
  strLineaGrid = rsta(0).Value & Chr(vbKeyTab)
  If Not IsNull(rsta(1).Value) Then
    strLineaGrid = strLineaGrid & rsta(1).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(2).Value) Then
    strLineaGrid = strLineaGrid & rsta(2).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(3).Value) Then
    strLineaGrid = strLineaGrid & rsta(3).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(4).Value) Then
    strLineaGrid = strLineaGrid & rsta(4).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(5).Value) Then
    strLineaGrid = strLineaGrid & rsta(5).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(6).Value) Then
    strLineaGrid = strLineaGrid & rsta(6).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(7).Value) Then
    strLineaGrid = strLineaGrid & rsta(7).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(8).Value) Then
    strLineaGrid = strLineaGrid & rsta(8).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(9).Value) Then
    strLineaGrid = strLineaGrid & rsta(9).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(10).Value) Then
    strLineaGrid = strLineaGrid & rsta(10).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If tipo = 2 Then
    strLineaGrid = strLineaGrid & cantidad & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If tipo = 3 Then
    strLineaGrid = strLineaGrid & precio & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If tipo = 3 Then
    strLineaGrid = strLineaGrid & descripcion & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  strLineaGrid = strLineaGrid & "-1"
  auxDBGrid1(0).AddItem strLineaGrid
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
qrya.Close
Set qrya = Nothing
auxDBGrid1(0).Redraw = True

Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Refrescar_Grid()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String

Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid2(0).Redraw = False
auxDBGrid2(0).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " AD0200.AD02DESDPTO"
strSelect = strSelect & ",FR7300.FR73CODINTFAR"
strSelect = strSelect & ",FR7300.FR73REFERENCIA"
strSelect = strSelect & ",FR7300.FR73DESPRODUCTO"
strSelect = strSelect & ",FRK100.FRK1TAMENVASE " 'FR7300.FR73TAMENVASE"
strSelect = strSelect & ",FRK100.FRK1CANTIDAD"
strSelect = strSelect & ",FRK100.FRK1FECMOV"
strSelect = strSelect & ",FRK100.FRK1CODMOV"
strSelect = strSelect & ",FRK100.AD02CODDPTO"
strSelect = strSelect & ",FRK100.FR73CODPRODUCTO"
strSelect = strSelect & ",FRK100.FRK1INDMOD"
strSelect = strSelect & ",FRK100.FRK1INDFAC"
strSelect = strSelect & ",FRK100.FRK1PRECIO"
strSelect = strSelect & ",FRK100.FRK1DESPRODUCTO"

strSelect = strSelect & " FROM FRK100,AD0200,FR7300"
strSelect = strSelect & " WHERE "

strSelect = strSelect & " FRK100.AD02CODDPTO=AD0200.AD02CODDPTO"
strSelect = strSelect & " AND FRK100.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
strSelect = strSelect & " AND " & glstrWhereSer

strSelect = strSelect & " ORDER BY FRK1CODMOV DESC,FRK1FECMOV DESC,AD02CODDPTO ASC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  strlinea = rsta("AD02DESDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODINTFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73REFERENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1TAMENVASE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1CANTIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1FECMOV").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1CODMOV").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02CODDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1INDMOD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1INDFAC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1PRECIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK1DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & ""
  auxDBGrid2(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid2(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub


Public Sub insertar_en_grid_servicio(tipo As String, codproducto As Currency, cantidad As Currency, precio As Currency, descripcion As String)
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strLineaGrid As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
strSelect = "SELECT "
strSelect = strSelect & "'" & txtServicio.Text & "'"
strSelect = strSelect & ",FR7300.FR73CODINTFAR"
strSelect = strSelect & ",FR7300.FR73REFERENCIA"
strSelect = strSelect & ",FR7300.FR73DESPRODUCTO"
strSelect = strSelect & ",FR7300.FR73TAMENVASE"
strSelect = strSelect & ",SYSDATE"
strSelect = strSelect & ",FRK1CODMOV_SEQUENCE.NEXTVAL"
strSelect = strSelect & "," & cboservicio.Text
strSelect = strSelect & ",FR73CODPRODUCTO"
strSelect = strSelect & ",0"
strSelect = strSelect & ",0"
strSelect = strSelect & " FROM FR7300"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR73CODPRODUCTO=?"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
qrya(0) = codproducto
Set rsta = qrya.OpenResultset()

auxDBGrid2(0).Redraw = False
While Not rsta.EOF
  strLineaGrid = rsta(0).Value & Chr(vbKeyTab)
  If Not IsNull(rsta(1).Value) Then
    strLineaGrid = strLineaGrid & rsta(1).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(2).Value) Then
    strLineaGrid = strLineaGrid & rsta(2).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(3).Value) Then
    strLineaGrid = strLineaGrid & rsta(3).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(4).Value) Then
    strLineaGrid = strLineaGrid & rsta(4).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "1" & Chr(vbKeyTab)
  End If
  strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  If Not IsNull(rsta(5).Value) Then
    strLineaGrid = strLineaGrid & rsta(5).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(6).Value) Then
    strLineaGrid = strLineaGrid & rsta(6).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(7).Value) Then
    strLineaGrid = strLineaGrid & rsta(7).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(8).Value) Then
    strLineaGrid = strLineaGrid & rsta(8).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(9).Value) Then
    strLineaGrid = strLineaGrid & rsta(9).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If Not IsNull(rsta(10).Value) Then
    strLineaGrid = strLineaGrid & rsta(10).Value & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If tipo = 3 Then
    strLineaGrid = strLineaGrid & precio & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  If tipo = 3 Then
    strLineaGrid = strLineaGrid & descripcion & Chr(vbKeyTab)
  Else
    strLineaGrid = strLineaGrid & "" & Chr(vbKeyTab)
  End If
  strLineaGrid = strLineaGrid & "-1"
  auxDBGrid2(0).AddItem strLineaGrid
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
qrya.Close
Set qrya = Nothing
auxDBGrid2(0).Redraw = True

Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Guardar_Servicio()
Dim i As Integer
Dim strInsK1 As String
Dim tipo As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
  auxDBGrid2(0).Redraw = False
  auxDBGrid2(0).MoveFirst
  For i = 0 To auxDBGrid2(0).Rows
    If auxDBGrid2(0).Columns("Cant").Value = "" Then
      MsgBox "El campo Cantidad es Obligatorio", vbInformation, "Aviso"
      auxDBGrid2(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf Not IsNumeric(auxDBGrid2(0).Columns("Cant").Value) Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      auxDBGrid2(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf auxDBGrid2(0).Columns("Cant").Value > 9999.99 Or auxDBGrid2(0).Columns("Cant").Value < -9999.99 Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      auxDBGrid2(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf auxDBGrid2(0).Columns("Cant").Value = 0 Then
      MsgBox "El campo Cantidad no puede ser 0", vbInformation, "Aviso"
      auxDBGrid2(0).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    auxDBGrid2(0).MoveNext
  Next i
  auxDBGrid2(0).Redraw = True
  
  auxDBGrid2(0).Redraw = False
  auxDBGrid2(0).MoveFirst
  For i = 0 To auxDBGrid2(0).Rows - 1
    If auxDBGrid2(0).Columns("Nuevo").Value <> "" Then
      If optFactAlm2(0) Or optFactAlm2(2) Then
        ''Se realiza el insert en FRK100
        strInsK1 = "INSERT INTO FRK100 ("
        strInsK1 = strInsK1 & "FRK1CODMOV,FRK1FECMOV,AD02CODDPTO,FR73CODPRODUCTO,"
        strInsK1 = strInsK1 & "FRK1CANTIDAD,FRK1TAMENVASE,FRK1INDMOD,FRK1INDFAC"
        If auxDBGrid2(0).Columns("Precio").Value <> "" Then
          strInsK1 = strInsK1 & ",FRK1PRECIO"
        End If
        If auxDBGrid2(0).Columns("Desc.Prod. No Form.").Value <> "" Then
          strInsK1 = strInsK1 & ",FRK1DESPRODUCTO"
        End If
        strInsK1 = strInsK1 & ") VALUES ("
        strInsK1 = strInsK1 & auxDBGrid2(0).Columns("CodMov").Value & ","
        strInsK1 = strInsK1 & "TO_DATE('" & auxDBGrid2(0).Columns("Fecha").Value & "','DD/MM/YYYY HH24:MI:SS'),"
        strInsK1 = strInsK1 & auxDBGrid2(0).Columns("CodDpto").Value & ","
        strInsK1 = strInsK1 & auxDBGrid2(0).Columns("CodPrd").Value & ","
        strInsK1 = strInsK1 & objGen.ReplaceStr(auxDBGrid2(0).Columns("Cant").Value, ",", ".", 1) & ","
        strInsK1 = strInsK1 & objGen.ReplaceStr(auxDBGrid2(0).Columns("Envase").Value, ",", ".", 1) & ","
        strInsK1 = strInsK1 & auxDBGrid2(0).Columns("MOD").Value & ","
        strInsK1 = strInsK1 & auxDBGrid2(0).Columns("FAC").Value
        If auxDBGrid2(0).Columns("Precio").Value <> "" Then
          strInsK1 = strInsK1 & "," & objGen.ReplaceStr(auxDBGrid2(0).Columns("Precio").Value, ",", ".", 1)
        End If
        If auxDBGrid2(0).Columns("Desc.Prod. No Form.").Value <> "" Then
          strInsK1 = strInsK1 & ",'" & auxDBGrid2(0).Columns("Desc.Prod. No Form.").Value & "'"
        End If
        strInsK1 = strInsK1 & ")"
        objApp.rdoConnect.Execute strInsK1, 64
      End If
      If optFactAlm2(1) Or optFactAlm2(2) Then
        If auxDBGrid2(0).Columns("Cant").Value < 0 Then
          tipo = "abono"
        Else
          tipo = "cargo"
        End If
        Call RealizarApuntes2(cboservicio, auxDBGrid2(0).Columns("CodPrd").Value, Abs(auxDBGrid2(0).Columns("Cant").Value), tipo)
      End If
    End If
    auxDBGrid2(0).MoveNext
  Next i
  auxDBGrid2(0).MoveFirst
  auxDBGrid2(0).Redraw = True

tlbToolbar1.Buttons(4).Enabled = False
glstrWhereSer = ""
If IsDate(dtcFecDesde(2).Date) And IsDate(dtcFecDesde(3).Date) Then
  If DateDiff("d", dtcFecDesde(2).Date, dtcFecDesde(3).Date) < 0 Then
    MsgBox "La fecha hasta debe ser mayor que la fecha desde", vbInformation, "Aviso"
    glstrWhereSer = "-1=0"
  Else
    glstrWhereSer = "TRUNC(FRK100.FRK1FECMOV) BETWEEN TO_DATE('" & dtcFecDesde(2).Date & "','DD/MM/YYYY') AND NVL(TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
  End If
ElseIf IsDate(dtcFecDesde(2).Date) Then
  glstrWhereSer = "TRUNC(FRK100.FRK1FECMOV) BETWEEN TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY') AND TO_DATE('31/12/9999','DD/MM/YYYY') "
ElseIf IsDate(dtcFecDesde(3).Date) Then
  glstrWhereSer = "TRUNC(FRK100.FRK1FECMOV) <= NVL(TO_DATE('" & dtcFecDesde(3).Date & "','DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY')) "
End If
If glstrWhereSer = "" Then
  glstrWhereSer = "1=1"
End If
If cboservicio.Text <> "" Then
  glstrWhereSer = glstrWhereSer & " AND FRK100.AD02CODDPTO =" & cboservicio.Text
End If

Call Refrescar_Grid
Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub

