VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmDispSinPRN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Stock de Planta. Dispensar sin PRN"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   45
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   46
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab Tab1 
      Height          =   7500
      Left            =   120
      TabIndex        =   47
      TabStop         =   0   'False
      Top             =   480
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   13229
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Vale Pendiente"
      TabPicture(0)   =   "FR0146.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Recepci�n de Vales"
      TabPicture(1)   =   "FR0146.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(3)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cmdasignar"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "fraFrame1(2)"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "fraFrame1(1)"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Frame2"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      Begin VB.Frame fraFrame1 
         Caption         =   "Vales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6735
         Index           =   3
         Left            =   -67200
         TabIndex        =   32
         Top             =   600
         Width           =   3885
         Begin TabDlg.SSTab tabTab1 
            Height          =   6300
            Index           =   3
            Left            =   120
            TabIndex        =   79
            TabStop         =   0   'False
            Top             =   360
            Width           =   3615
            _ExtentX        =   6376
            _ExtentY        =   11113
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0146.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(23)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(25)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(26)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(27)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(30)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(31)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(32)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(33)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(24)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "dtcDateCombo1(4)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "dtcDateCombo1(3)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(24)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(25)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(26)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(27)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(28)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(29)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(30)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(31)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(32)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).ControlCount=   20
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0146.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(3)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   32
               Left            =   600
               TabIndex        =   35
               TabStop         =   0   'False
               Tag             =   "Descripci�n Servicio"
               Top             =   960
               Width           =   2520
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   31
               Left            =   1320
               TabIndex        =   37
               TabStop         =   0   'False
               Tag             =   "Apellido 1� de la Persona"
               Top             =   1560
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR92OBSERV"
               Height          =   930
               Index           =   30
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   43
               Tag             =   "Observaciones"
               Top             =   5160
               Width           =   3015
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   29
               Left            =   120
               TabIndex        =   34
               Tag             =   "C�digo Servicio"
               Top             =   960
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   28
               Left            =   120
               TabIndex        =   36
               Tag             =   "C�digo Persona"
               Top             =   1560
               Width           =   1080
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR92CODTRANS"
               Height          =   330
               Index           =   27
               Left            =   120
               TabIndex        =   33
               Tag             =   "C�digo Transacci�n"
               Top             =   360
               Width           =   1812
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   26
               Left            =   120
               TabIndex        =   40
               Tag             =   "C�digo Producto"
               Top             =   3360
               Width           =   1092
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   25
               Left            =   120
               TabIndex        =   41
               TabStop         =   0   'False
               Tag             =   "Producto"
               Top             =   3960
               Width           =   3000
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR92CANTIDAD"
               Height          =   330
               Index           =   24
               Left            =   120
               TabIndex        =   42
               Tag             =   "Cantidad de producto"
               Top             =   4560
               Width           =   1080
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR92FECRECVALE"
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   39
               Tag             =   "Fecha REcepci�n de Vale"
               Top             =   2760
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR92FECDISP"
               Height          =   330
               Index           =   4
               Left            =   120
               TabIndex        =   38
               Tag             =   "Fecha Dispensaci�n"
               Top             =   2160
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5985
               Index           =   3
               Left            =   -74880
               TabIndex        =   44
               Top             =   120
               Width           =   3015
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   5318
               _ExtentY        =   10557
               _StockProps     =   79
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Desc. Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   120
               TabIndex        =   88
               Top             =   3720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   33
               Left            =   120
               TabIndex        =   87
               Top             =   1320
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   32
               Left            =   120
               TabIndex        =   86
               Top             =   4920
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Recepci�n de Vale"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   31
               Left            =   120
               TabIndex        =   85
               Top             =   2520
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   120
               TabIndex        =   84
               Top             =   720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Dispensaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   120
               TabIndex        =   83
               Top             =   1920
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Transacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   26
               Left            =   120
               TabIndex        =   82
               Top             =   120
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   120
               TabIndex        =   81
               Top             =   3120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   120
               TabIndex        =   80
               Top             =   4320
               Width           =   975
            End
         End
      End
      Begin VB.CommandButton cmdasignar 
         Caption         =   "Asignar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -68760
         TabIndex        =   78
         Top             =   840
         Width           =   1215
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Productos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1440
         Index           =   2
         Left            =   -74880
         TabIndex        =   30
         Top             =   6000
         Width           =   7575
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   945
            Index           =   2
            Left            =   120
            TabIndex        =   31
            Top             =   360
            Width           =   7320
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   12912
            _ExtentY        =   1667
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "PRN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4455
         Index           =   1
         Left            =   -74880
         TabIndex        =   12
         Top             =   1560
         Width           =   7545
         Begin TabDlg.SSTab tabTab1 
            Height          =   3975
            Index           =   1
            Left            =   120
            TabIndex        =   64
            TabStop         =   0   'False
            Top             =   360
            Width           =   7335
            _ExtentX        =   12938
            _ExtentY        =   7011
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0146.frx":0070
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(11)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(12)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(13)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(14)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(15)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(16)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(17)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(18)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(19)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(20)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(21)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(28)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(22)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "dtcDateCombo1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(14)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(13)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(12)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(11)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(10)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(9)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(15)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(16)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(17)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(18)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(19)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(20)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(21)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(22)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txtText1(23)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "Frame1"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).ControlCount=   30
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0146.frx":008C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(1)"
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame1 
               BorderStyle     =   0  'None
               Height          =   1095
               Left            =   4800
               TabIndex        =   89
               Top             =   120
               Width           =   2055
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR66CODPETICION"
               Height          =   330
               Index           =   23
               Left            =   240
               TabIndex        =   13
               Tag             =   "Petici�n"
               Top             =   360
               Width           =   1560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   22
               Left            =   240
               MultiLine       =   -1  'True
               TabIndex        =   22
               Tag             =   "C�d.Servicio"
               Top             =   2280
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   21
               Left            =   2160
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "Servicio"
               Top             =   2280
               Width           =   4560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   20
               Left            =   240
               MultiLine       =   -1  'True
               TabIndex        =   14
               Tag             =   "Paciente"
               Top             =   960
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   2160
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Historia"
               Top             =   960
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   18
               Left            =   240
               TabIndex        =   17
               TabStop         =   0   'False
               Tag             =   "Nombre"
               Top             =   1560
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   2160
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "Apellido 1�"
               Top             =   1560
               Width           =   2235
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   16
               Left            =   4560
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Apellido 2�"
               Top             =   1560
               Width           =   2115
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   15
               Left            =   5280
               MultiLine       =   -1  'True
               TabIndex        =   18
               Tag             =   "Paciente"
               Top             =   480
               Visible         =   0   'False
               Width           =   1320
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   9
               Left            =   5280
               MultiLine       =   -1  'True
               TabIndex        =   20
               Tag             =   "Paciente"
               Top             =   840
               Visible         =   0   'False
               Width           =   1320
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   10
               Left            =   5280
               MultiLine       =   -1  'True
               TabIndex        =   16
               Tag             =   "Paciente"
               Top             =   120
               Visible         =   0   'False
               Width           =   1320
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   11
               Left            =   240
               TabIndex        =   24
               Tag             =   "C�d.M�dico Firma"
               Top             =   2880
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   12
               Left            =   2160
               TabIndex        =   25
               TabStop         =   0   'False
               Tag             =   "Dr."
               Top             =   2880
               Width           =   2520
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   13
               Left            =   2160
               TabIndex        =   28
               TabStop         =   0   'False
               Tag             =   "Desc.Urgencia"
               Top             =   3480
               Width           =   4485
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   14
               Left            =   240
               TabIndex        =   27
               Tag             =   "Urgencia"
               Top             =   3480
               Width           =   1800
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   3825
               Index           =   1
               Left            =   -74880
               TabIndex        =   29
               Top             =   120
               Width           =   6735
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   11880
               _ExtentY        =   6747
               _StockProps     =   79
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   2
               Left            =   4920
               TabIndex        =   26
               Tag             =   "Fecha Firma"
               Top             =   2880
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   240
               TabIndex        =   77
               Top             =   2040
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   240
               TabIndex        =   76
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Desc.Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   21
               Left            =   2160
               TabIndex        =   75
               Top             =   2040
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   240
               TabIndex        =   74
               Top             =   720
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   2160
               TabIndex        =   73
               Top             =   720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   240
               TabIndex        =   72
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   2160
               TabIndex        =   71
               Top             =   1320
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   4560
               TabIndex        =   70
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   4920
               TabIndex        =   69
               Top             =   2640
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico Firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   240
               TabIndex        =   68
               Top             =   2640
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   2160
               TabIndex        =   67
               Top             =   2640
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Desc Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   2160
               TabIndex        =   66
               Top             =   3240
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   240
               TabIndex        =   65
               Top             =   3240
               Width           =   1455
            End
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Servicio :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   -74760
         TabIndex        =   61
         Top             =   360
         Width           =   5535
         Begin VB.TextBox txtServicio 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   90
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   720
            Width           =   3480
         End
         Begin VB.CheckBox chkservicio 
            Caption         =   "Todos los servicios"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   3240
            TabIndex        =   62
            Top             =   360
            Value           =   1  'Checked
            Width           =   2055
         End
         Begin SSDataWidgets_B.SSDBCombo cboservicio 
            Height          =   315
            Left            =   240
            TabIndex        =   63
            TabStop         =   0   'False
            Top             =   240
            Width           =   2655
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4683
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Vales Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6135
         Index           =   0
         Left            =   240
         TabIndex        =   0
         Top             =   600
         Width           =   10250
         Begin TabDlg.SSTab tabTab1 
            Height          =   5580
            Index           =   0
            Left            =   120
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   360
            Width           =   9855
            _ExtentX        =   17383
            _ExtentY        =   9843
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0146.frx":00A8
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(8)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(6)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(4)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(3)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(2)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(0)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(1)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(7)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(10)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "dtcDateCombo1(0)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "dtcDateCombo1(1)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(6)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(7)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(4)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(3)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(2)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(0)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(1)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(5)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(8)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).ControlCount=   22
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0146.frx":00C4
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR92CANTIDAD"
               Height          =   330
               Index           =   8
               Left            =   7680
               TabIndex        =   59
               Tag             =   "Cantidad de producto"
               Top             =   3480
               Width           =   1080
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   5
               Left            =   2040
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Producto"
               Top             =   3480
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   1
               Left            =   360
               TabIndex        =   8
               Tag             =   "C�digo Producto"
               Top             =   3480
               Width           =   1092
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR92CODTRANS"
               Height          =   330
               Index           =   0
               Left            =   360
               TabIndex        =   1
               Tag             =   "C�digo Transacci�n"
               Top             =   600
               Width           =   1812
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   2
               Left            =   360
               TabIndex        =   4
               Tag             =   "C�digo Persona"
               Top             =   2040
               Width           =   1080
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   3
               Left            =   360
               TabIndex        =   2
               Tag             =   "C�digo Servicio"
               Top             =   1320
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR92OBSERV"
               Height          =   930
               Index           =   4
               Left            =   360
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   10
               Tag             =   "Observaciones"
               Top             =   4200
               Width           =   8535
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   7
               Left            =   2040
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Apellido 1� de la Persona"
               Top             =   2040
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   6
               Left            =   2040
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Descripci�n Servicio"
               Top             =   1320
               Width           =   5400
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4905
               Index           =   0
               Left            =   -74760
               TabIndex        =   11
               Top             =   480
               Width           =   9015
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15901
               _ExtentY        =   8652
               _StockProps     =   79
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR92FECRECVALE"
               Height          =   330
               Index           =   1
               Left            =   2760
               TabIndex        =   7
               Tag             =   "Fecha Recepci�n de Vale"
               Top             =   2760
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR92FECDISP"
               Height          =   330
               Index           =   0
               Left            =   360
               TabIndex        =   6
               Tag             =   "Fecha Dispensaci�n"
               Top             =   2760
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   7680
               TabIndex        =   60
               Top             =   3240
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   2040
               TabIndex        =   58
               Top             =   3240
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   360
               TabIndex        =   57
               Top             =   3240
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Transacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   360
               TabIndex        =   56
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Dispensaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   360
               TabIndex        =   55
               Top             =   2520
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   360
               TabIndex        =   54
               Top             =   1800
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   360
               TabIndex        =   53
               Top             =   1080
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Recepci�n de Vale"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   2760
               TabIndex        =   52
               Top             =   2520
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   360
               TabIndex        =   51
               Top             =   3960
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   2040
               TabIndex        =   50
               Top             =   1800
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   2040
               TabIndex        =   49
               Top             =   1080
               Width           =   1935
            End
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDispSinPRN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDispSinPRN (FR0146.FRM)                                   *
'* AUTOR: IRENE VAZQUEZ MARTINEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: dispensar sin PRN                                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If strFormName = "Vale Pendiente" And intNewStatus = cwModeSingleAddRest Then
    objWinInfo.CtrlGetInfo(txtText1(3)).blnForeign = True
    objWinInfo.CtrlGetInfo(txtText1(2)).blnForeign = True
    objWinInfo.CtrlGetInfo(txtText1(1)).blnForeign = True
  Else
    objWinInfo.CtrlGetInfo(txtText1(3)).blnForeign = False
    objWinInfo.CtrlGetInfo(txtText1(2)).blnForeign = False
    objWinInfo.CtrlGetInfo(txtText1(1)).blnForeign = False
  End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cboservicio_CloseUp()
chkservicio.Value = 0
Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=4 AND (FR66INDOM=0 OR FR66INDOM IS NULL) " & _
                                      " AND AD02CODDPTO=" & cboservicio.Text
objWinInfo.DataRefresh
Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
objWinInfo.objWinActiveForm.strWhere = "FR92FECRECVALE is null" & _
                                      " AND AD02CODDPTO=" & cboservicio.Text
txtServicio.Text = cboservicio.Columns(1).Value
objWinInfo.DataRefresh
    
End Sub



Private Sub chkservicio_Click()
If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=4 AND (FR66INDOM=0 OR FR66INDOM IS NULL) " & _
                                      " AND AD02CODDPTO IS NOT NULL"
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FR92FECRECVALE is null" & _
                                      " AND AD02CODDPTO IS NOT NULL"
    objWinInfo.DataRefresh
End If
If chkservicio = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If cboservicio.Text = "" Then
    objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=4 AND (FR66INDOM=0 OR FR66INDOM IS NULL) " & _
                                      " AND AD02CODDPTO IS NULL"
    Else
    objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=4 AND (FR66INDOM=0 OR FR66INDOM IS NULL) " & _
                                      " AND AD02CODDPTO=" & cboservicio.Text
    End If
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If cboservicio.Text = "" Then
    objWinInfo.objWinActiveForm.strWhere = "FR92FECRECVALE is null" & _
                                      " AND AD02CODDPTO IS NULL"
    Else
    objWinInfo.objWinActiveForm.strWhere = "FR92FECRECVALE is null" & _
                                      " AND AD02CODDPTO=" & cboservicio.Text
    End If
    objWinInfo.DataRefresh
End If
End Sub

Private Sub cmdasignar_Click()
Dim strupdate66 As String
Dim strupdate92 As String
Dim mensaje As String

cmdasignar.Enabled = False
If txtText1(23).Text = "" Or txtText1(27).Text = "" Then
  Call MsgBox("Para asignar debe seleccionar una Petici�n y un Vale.", vbInformation, "Aviso")
Else

    mensaje = MsgBox("�Est� seguro que desea asignar la PRN n� " & txtText1(23).Text & _
                    " al Vale n� " & txtText1(27).Text & "?", vbYesNo, "Pregunta")
    If mensaje = 6 Then
            'la petici�n se cambia a estado 5 (Servida)
            strupdate66 = "UPDATE FR6600 SET FR26CODESTPETIC=5 WHERE FR66CODPETICION=" & _
                         txtText1(23).Text
            objApp.rdoConnect.Execute strupdate66, 64
            objApp.rdoConnect.Execute "Commit", 64
            
            'la fecha de recepci�n del vale se actualiza con la fecha actual
            strupdate92 = "UPDATE FR9200 SET FR92FECRECVALE=SYSDATE WHERE FR92CODTRANS=" & _
                         txtText1(27).Text
            objApp.rdoConnect.Execute strupdate92, 64
            objApp.rdoConnect.Execute "Commit", 64
            
            Call MsgBox("La hoja de PRN ha sido asignada al Vale", vbInformation, "Aviso")
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataRefresh
            Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
            objWinInfo.DataRefresh
    End If
End If

cmdasignar.Enabled = True
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset
Dim qrya As rdoQuery

cboservicio.RemoveAll

Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
'stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
'       "WHERE AD32CODTIPODPTO=3 AND " & _
'       "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND" & _
'       "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"
'Set rsta = objApp.rdoConnect.OpenResultset(stra)
stra = "SELECT AD02CODDPTO, AD02DESDPTO"
stra = stra & " FROM AD0200"
stra = stra & " WHERE SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
stra = stra & " AND AD32CODTIPODPTO=? "
stra = stra & " ORDER BY AD02CODDPTO"
Set qrya = objApp.rdoConnect.CreateQuery("", stra)
qrya(0) = 3
Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
While (Not rsta.EOF)
    Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
Tab1.Tab = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim objDetailInfo2 As New clsCWForm
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
    
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Vale Pendiente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FR9200"
    .intAllowance = cwAllowAdd
    .strWhere = "FR92FECRECVALE IS NULL"
    
    Call .FormAddOrderField("FR92FECDISP", cwDescending)
    Call .FormAddOrderField("FR92CODTRANS", cwDescending)
    
    '.blnHasMaint = True
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Vale Pendiente")
    Call .FormAddFilterWhere(strKey, "FR92CODTRANS", "C�digo Transacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD", "C�digo Persona", cwString)
    Call .FormAddFilterWhere(strKey, "FR92FECDISP", "Fecha Dispensaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR92FECRECVALE", "Fecha Recepci�n de Vale", cwDate)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR92CANTIDAD", "Cantidad", cwNumeric)

    Call .FormAddFilterOrder(strKey, "FR92CODTRANS", "C�digo Transacci�n")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")
    Call .FormAddFilterOrder(strKey, "SG02COD", "C�digo Persona")
    Call .FormAddFilterOrder(strKey, "FR92FECDISP", "Fecha Dispensaci�n")
    Call .FormAddFilterOrder(strKey, "FR92FECRECVALE", "Fecha Recepci�n de Vale")
  End With
  '************************************************************++++++
   With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    
    .strName = "Peticiones"
      
    .strTable = "FR6600"
    'Estado de la petici�n=4 (petici�n validada) y s�lo peticiones que sean PRN pero
    'no PRN de estupefacientes o f�rmula magistral
    .strWhere = "FR26CODESTPETIC=4 AND (FR66INDOM=0 OR FR66INDOM IS NULL)" & _
                 " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                 " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66FECFIRMMEDI", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Persona", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�digo M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma", cwDate)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�d.Urgencia", cwNumeric)

    Call .FormAddFilterOrder(strKey, "FR66CODPETICION", "C�digo Petici�n")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")
    Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "C�digo Persona")
    Call .FormAddFilterOrder(strKey, "FR66FECFIRMMEDI", "Fecha Firma")


  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "PRN"
    .strTable = "FR3200"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(23))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "PRN")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR28DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d.Unidad Medida", cwString)

    
  End With
  
  With objDetailInfo2
    .strName = "Vales"
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(3)
    Set .grdGrid = grdDBGrid1(3)
    .strTable = "FR9200"
    .intAllowance = cwAllowReadOnly
    .strWhere = "FR92FECRECVALE is null"
    
    Call .FormAddOrderField("FR92CODTRANS", cwAscending)
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Vales")
    Call .FormAddFilterWhere(strKey, "FR92CODTRANS", "C�digo Transacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD", "C�digo Persona", cwString)
    Call .FormAddFilterWhere(strKey, "FR92FECDISP", "Fecha Dispensaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR92FECRECVALE", "Fecha Recepci�n de Vale", cwDate)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR92CANTIDAD", "Cantidad", cwNumeric)

    Call .FormAddFilterOrder(strKey, "FR92CODTRANS", "C�digo Transacci�n")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")
    Call .FormAddFilterOrder(strKey, "SG02COD", "C�digo Persona")
    Call .FormAddFilterOrder(strKey, "FR92FECDISP", "Fecha Dispensaci�n")
    Call .FormAddFilterOrder(strKey, "FR92FECRECVALE", "Fecha Recepci�n de Vale")
  End With

   '**************************************************************+++++
   '-------------------------------------------------------------------
   
     With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo)
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo2)
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "C�d.Unidad Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Unidad Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Observaciones", "FR32OBSERVFARM", cwString, 2000)

    
    
    Call .FormCreateInfo(objMasterInfo)
    
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(23)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(27)).blnInFind = True
    .CtrlGetInfo(txtText1(29)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(txtText1(26)).blnInFind = True
    .CtrlGetInfo(txtText1(24)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(4)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(7)).blnInFind = True
    
    .CtrlGetInfo(txtText1(3)).blnForeign = True
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(4)), grdDBGrid1(2).Columns(5), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(7)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(7)), grdDBGrid1(2).Columns(8), "FR93DESUNIMEDIDA")
    
   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "SG02COD", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(7), "SG02APE1")

   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(6), "AD02DESDPTO")
    
   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "FR73DESPRODUCTO")
   
   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(28)), "SG02COD", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(28)), txtText1(31), "SG02APE1")

   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(29)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(29)), txtText1(32), "AD02DESDPTO")
    
   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(26)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(26)), txtText1(25), "FR73DESPRODUCTO")
    
   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(21), "AD02DESDPTO")
    
   Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(20)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
   Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(19), "CI22NUMHISTORIA")
    
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(18), "CI22NOMBRE")
    
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(17), "CI22PRIAPEL")
    
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(16), "CI22SEGAPEL")
    
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(12), "SG02APE1")
    
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(13), "FR91DESURGENCIA")
    
    
   grdDBGrid1(2).Columns(3).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
   '---------------------------------------------------------------------

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch
  
  If strCtrl = "txtText1(3)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
       "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND" & _
       "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"
         
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(3), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
         
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If

 If strCtrl = "txtText1(2)" Then
     Set objSearch = New clsCWSearch
     With objSearch
      .strTable = "SG0200"
      .strOrder = "ORDER BY SG02COD ASC"
          
      Set objField = .AddField("SG02COD")
      objField.strSmallDesc = "C�digo Persona Valida"
          
      Set objField = .AddField("SG02APE1")
      objField.strSmallDesc = "Nombre Persona"
          
      If .Search Then
       Call objWinInfo.CtrlSet(txtText1(2), .cllValues("SG02COD"))
      End If
    End With
    Set objSearch = Nothing
  End If
 



End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim qrydes As rdoQuery
Dim rstdes As rdoResultset
Dim strdes As String
Dim qryori As rdoQuery
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String
'Dim str35 As String
'Dim rst35 As rdoResultset
'Dim str80 As String
'Dim rst80 As rdoResultset
Dim i As Integer
Dim struni As String
Dim rstuni As rdoResultset
Dim qryuni As rdoQuery

        'se obtiene el almac�n que pide el producto
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'strdes = "SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & txtText1(3).Text
        'Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        strdes = "SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=?"
        Set qrydes = objApp.rdoConnect.CreateQuery("", rstdes)
        qrydes(0) = txtText1(3).Text
        Set rstdes = qrydes.OpenResultset(strdes)
        'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
        'strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
        '         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
        '         "AND FRH2CODPARAMGEN=3"
        strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
                 "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                 "AND FRH2CODPARAMGEN=?"
        Set qryori = objApp.rdoConnect.CreateQuery("", strori)
        qryori(0) = 3
        Set rstori = objApp.rdoConnect.OpenResultset(strori)
    
    If (Not rstdes.EOF And Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) _
         And Not IsNull(rstdes.rdoColumns(0).Value) Then
  
        'str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        'Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        
        'str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        'Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        
        'bind variables
        'struni = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(1).Text
        struni = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryuni = objApp.rdoConnect.CreateQuery("", struni)
        qryuni(0) = txtText1(1).Text
        Set rstuni = qryuni.OpenResultset(struni)
        
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es el almac�n al que se dispensa
        'strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
        '              "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
        '              "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
        '              rst35.rdoColumns(0).Value & "," & _
        '              rstori.rdoColumns(0).Value & "," & _
        '              rstdes.rdoColumns(0).Value & "," & _
        '              "8" & "," & _
        '              "SYSDATE" & "," & _
        '              "NULL" & "," & _
        '              txtText1(1).Text & "," & _
        '              txtText1(8).Text & "," & _
        '              "0" & ","
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                      "FR35CODMOVIMIENTO_SEQUENCE.nextval," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstdes.rdoColumns(0).Value & "," & _
                      "8" & "," & _
                      "SYSDATE" & "," & _
                      "NULL" & "," & _
                      txtText1(1).Text & "," & _
                      txtText1(8).Text & "," & _
                      "0" & ","
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertentrada = strinsertentrada & "'" & rstuni.rdoColumns(0).Value & "'" & ")"
        Else
            strinsertentrada = strinsertentrada & "1)"
        End If
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
'        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
'                     "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
'                     "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
'                      rst80.rdoColumns(0).Value & "," & _
'                      rstori.rdoColumns(0).Value & "," & _
'                      rstdes.rdoColumns(0).Value & "," & _
'                      "7" & "," & _
'                      "SYSDATE" & "," & _
'                      "NULL" & "," & _
'                      txtText1(1).Text & "," & _
'                      txtText1(8).Text & "," & _
'                      "0" & ","
        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                     "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                     "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                      "FR80NUMMOV_SEQUENCE.nextval," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstdes.rdoColumns(0).Value & "," & _
                      "7" & "," & _
                      "SYSDATE" & "," & _
                      "NULL" & "," & _
                      txtText1(1).Text & "," & _
                      txtText1(8).Text & "," & _
                      "0" & ","
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertsalida = strinsertsalida & "'" & rstuni.rdoColumns(0).Value & "'" & ")"
        Else
            strinsertsalida = strinsertsalida & "1)"
        End If
        objApp.rdoConnect.Execute strinsertsalida, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        
  'rst35.Close
  'Set rst35 = Nothing
  'rst80.Close
  'Set rst80 = Nothing

  rstuni.Close
  Set rstuni = Nothing
End If

rstdes.Close
Set rstdes = Nothing
rstori.Close
Set rstori = Nothing

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rdoQ As rdoQuery

If objWinInfo.objWinActiveForm.strName = "Vale Pendiente" Then

  'se verifica que el servicio introducido sea correcto antes de Guardar
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
  If txtText1(3).Text <> "" Then
    'sqlstr = "SELECT * FROM AD0200 " & _
           "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
           " AND AD02CODDPTO=" & txtText1(3).Text
    sqlstr = "SELECT * FROM AD0200 " & _
           "WHERE AD32CODTIPODPTO=? AND " & _
           "SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) " & _
           " AND AD02CODDPTO=?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sqlstr)
    rdoQ(0) = 3
    rdoQ(1) = txtText1(3).Text
    Set rsta = rdoQ.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El C�d.Servicio es incorrecto.", vbExclamation, "Aviso"
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  
  'se verifica que la persona introducida sea correcta antes de Guardar
  If txtText1(2).Text <> "" Then
    'bind variables
    'sqlstr = "SELECT SG02COD FROM SG0200 WHERE SG02COD=" & "'" & txtText1(2).Text & "'"
    sqlstr = "SELECT SG02COD FROM SG0200 WHERE SG02COD=?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sqlstr)
    rdoQ(0) = txtText1(2).Text
    Set rsta = rdoQ.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El c�digo de doctor es incorrecto.", vbExclamation, "Aviso"
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  
  'se verifica que el producto introducido sea correcto antes de Guardar
  If txtText1(1).Text <> "" Then
    'bind variables
    'sqlstr = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(1).Text
    sqlstr = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sqlstr)
    rdoQ(0) = txtText1(1).Text
    Set rsta = rdoQ.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El Producto es incorrecto.", vbExclamation, "Aviso"
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub Tab1_Click(PreviousTab As Integer)
If Tab1.Tab = 1 Then
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    objWinInfo.DataRefresh
    'se esconde en el grid el c�digo del paciente que est� 3 veces m�s porque si no
    'no hace el AddLinked
    grdDBGrid1(1).Columns(4).Width = 0
    grdDBGrid1(1).Columns(6).Width = 0
    grdDBGrid1(1).Columns(8).Width = 0
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Vale Pendiente" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR92CODTRANS_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    'SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String

  If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Vale Pendiente" Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    sqlstr = "SELECT FR92CODTRANS_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    'SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
