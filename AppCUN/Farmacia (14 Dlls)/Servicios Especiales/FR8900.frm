VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmCodBarrDpto 
   Caption         =   "P�ginas de C�digos de Barras de productos de farmacia"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11670
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11670
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consultar"
      Height          =   495
      Left            =   10200
      TabIndex        =   17
      Top             =   3720
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "A�adir/Borrar Productos de la P�gina"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   735
      Left            =   120
      TabIndex        =   9
      Top             =   1560
      Width           =   11415
      Begin VB.TextBox txtCodInt 
         Height          =   195
         Left            =   6960
         TabIndex        =   15
         Top             =   600
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton cmdBorrar 
         Caption         =   "Borrar Producto"
         Height          =   405
         Left            =   9360
         TabIndex        =   14
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox txtCantidad 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   1320
         TabIndex        =   11
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "A�adir Producto"
         Height          =   405
         Left            =   7680
         TabIndex        =   12
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox txtCodigo 
         BackColor       =   &H00FFFF00&
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblDescripcion 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1920
         TabIndex        =   13
         Top             =   240
         Width           =   5415
      End
   End
   Begin vsViewLib.vsPrinter vs 
      Height          =   375
      Left            =   10320
      TabIndex        =   8
      Top             =   6240
      Visible         =   0   'False
      Width           =   975
      _Version        =   196608
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      AbortWindow     =   0   'False
      PageBorder      =   0
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Height          =   495
      Left            =   10200
      TabIndex        =   7
      Top             =   4560
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid ssgrdProductos 
      Height          =   6135
      Left            =   120
      TabIndex        =   6
      Top             =   2400
      Width           =   9855
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   4
      AllowUpdate     =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   4
      Columns(0).Width=   1693
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "CODIGO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "CODIGOSEGURIDAD"
      Columns(1).Name =   "CODIGOSEGURIDAD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1429
      Columns(2).Caption=   "Cantidad"
      Columns(2).Name =   "CANTIDAD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16776960
      Columns(3).Width=   13097
      Columns(3).Caption=   "Descripci�n"
      Columns(3).Name =   "DESCRIPCION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      _ExtentX        =   17383
      _ExtentY        =   10821
      _StockProps     =   79
      Caption         =   "Productos de la p�gina"
      ForeColor       =   0
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   10320
      TabIndex        =   5
      Top             =   7440
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Departamento/P�ginas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11415
      Begin VB.CommandButton cmdBorrarPag 
         Caption         =   "Borrar P�gina"
         Height          =   495
         Left            =   8880
         TabIndex        =   18
         Top             =   840
         Width           =   1335
      End
      Begin VB.CommandButton cmdA�adirPag 
         Caption         =   "A�adir P�gina"
         Height          =   495
         Left            =   8880
         TabIndex        =   16
         Top             =   240
         Width           =   1335
      End
      Begin ComctlLib.ListView lvwPag 
         Height          =   1215
         Left            =   3360
         TabIndex        =   2
         Top             =   120
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   2143
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSDataWidgets_B.SSDBCombo sscboDpto 
         Height          =   255
         Left            =   600
         TabIndex        =   1
         Top             =   480
         Width           =   1815
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         Cols            =   2
         ColumnHeaders   =   0   'False
         DividerType     =   0
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   3201
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label2 
         Caption         =   "P�ginas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2640
         TabIndex        =   4
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Dpto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmCodBarrDpto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' varaibles para la impresi�n
Dim intPosx As Integer
Dim intPosy As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim intPX As Integer
Dim intPY As Integer

Dim POSX As Integer
Dim POSY As Integer
Dim TG(9) As String
Dim BNS As Integer
Dim BBS As Integer
Dim BAL As String

Dim intDptoSel%, strPagSel, strUltPag, intNumProd
'Dim intCodigos(50) As Long
Private Sub Obtener_Producto()
  Dim SQL As String
  Dim qry As rdoQuery
  Dim rs As rdoResultset
    'comprobar si existe el nuevo c�digo introducido
    SQL = "SELECT fr73desproducto,fr73codproducto"
    SQL = SQL & " FROM fr7300"
    SQL = SQL & " where fr73codintfar = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = txtCodigo
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        lblDescripcion = rs(0)
        txtCodInt = rs(1)
    Else
       MsgBox "c�digo no encontrado:" & txtCodigo
       txtCodInt = ""
       txtCodigo = ""
       lblDescripcion = ""
       txtCantidad = ""
       txtCodigo.SetFocus
       Exit Sub
    End If
    rs.Close
    qry.Close
End Sub
Private Sub pCargarProductos()
   Dim qry As rdoQuery
   Dim rs As rdoResultset
   Dim SQL As String
   Dim i As Integer
   intNumProd = 0 ' n�mero de productos de la p�gina
 SQL = "SELECT fr7300.fr73codintfar,fr7300.fr73codintfarseg,"
 SQL = SQL & " frl400.frl4cant,fr7300.fr73desproducto"
 SQL = SQL & " FROM frl400,fr7300"
 SQL = SQL & " WHERE"
 SQL = SQL & " frl400.fr73codproducto = fr7300.fr73codproducto and"
 SQL = SQL & " frl400.ad02coddpto = ? and"
 SQL = SQL & " frl400.frl3numpag = ? "
 SQL = SQL & " order by fr7300.fr73desproducto"
 Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    qry(1) = strPagSel
 Set rs = qry.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
 If Not rs.EOF Then
    cmdImprimir.Enabled = True
    Do While Not rs.EOF
         ssgrdProductos.AddItem rs(0) & Chr$(9) & rs(1) & Chr$(9) _
            & rs(2) & Chr$(9) & rs(3)
'         'llenar tabla de c�digos
'         intCodigos(i) = rs(0)
         intNumProd = intNumProd + 1 ' n�mero de productos que componen la p�gina

        rs.MoveNext
    Loop
 Else
    cmdImprimir.Enabled = True
 End If
End Sub
Private Sub pCargarPaginas(intDptoSel%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    strUltPag = 0
    lvwPag.ListItems.Clear
    SQL = "SELECT FRL3NUMPAG, FRL3DESPAG FROM FRL300 WHERE AD02CODDPTO = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        Set item = lvwPag.ListItems.Add(, , rs!frl3numpag & "-" & rs!FRL3DESPAG)
        item.Tag = rs!frl3numpag
        strUltPag = rs!frl3numpag ' �ltima p�gina del departamento
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub


Private Sub cmda�adir_Click()
  Dim SQL As String
  Dim qry As rdoQuery
  Dim rs As rdoResultset
  Dim strInsert As String
  Dim qryinsert As rdoQuery
  Dim Respuesta
  
  If intNumProd = 45 Then
    MsgBox "Esta p�gina ya tiene 45 productos", vbExclamation
    Exit Sub
  End If
  Respuesta = MsgBox("�Est� seguro que desea a�adir el registro?", vbYesNo)
  If Respuesta = vbYes Then
     If txtCodigo = "" Then
         MsgBox "indique c�digo del producto", vbExclamation
         Exit Sub
     End If
     If Not IsNumeric(txtCantidad) Then
          MsgBox "La cantidad debe ser un valor num�rico", vbExclamation
         Exit Sub
     End If
        ' no duplicar el nuevo c�digo introducido
      SQL = "SELECT fr73codproducto"
      SQL = SQL & " FROM frl400"
      SQL = SQL & " where ad02coddpto = ? and"
      SQL = SQL & " frl3numpag = ? and"
      SQL = SQL & " fr73codproducto = ? "
      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = intDptoSel
        qry(1) = strPagSel
        qry(2) = txtCodInt
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then
         MsgBox "el c�digo ya est� en esta p�gina"
         txtCodInt = ""
         txtCodigo = ""
         lblDescripcion = ""
         txtCantidad = ""
         txtCodigo.SetFocus
         Exit Sub
      End If
      rs.Close
      qry.Close
      'a�adir el registro
      strInsert = " insert into frl400 " & _
                " (ad02coddpto,frl3numpag, " & _
                " fr73codproducto,frl4cant)" & _
                " values (?, ?, ?, ?)"
            Set qryinsert = objApp.rdoConnect.CreateQuery("", strInsert)
            qryinsert(0) = intDptoSel
            qryinsert(1) = strPagSel
            qryinsert(2) = txtCodInt
            qryinsert(3) = txtCantidad
            qryinsert.Execute
            qryinsert.Close
            Set qryinsert = Nothing
            txtCodigo = ""
            lblDescripcion = ""
            txtCodInt = ""
            txtCantidad = ""
            intNumProd = intNumProd + 1
  Else
       txtCodigo = ""
       lblDescripcion = ""
       txtCodInt = ""
       txtCantidad = ""
  End If
  txtCodigo.SetFocus
End Sub

Private Sub cmdA�adirPag_Click()
  Dim NombrePagina As String
  Dim strInsert As String
  Dim qryinsert As rdoQuery

  NombrePagina = InputBox("introduzca nombre de la p�gina", "A�adir P�gina")
  If NombrePagina <> "" Then
     'a�adir el registro
      strInsert = " insert into frl300 " & _
              " (ad02coddpto,frl3numpag, " & _
              " frl3despag)" & _
              " values (?, ?, ?)"
     Set qryinsert = objApp.rdoConnect.CreateQuery("", strInsert)
         qryinsert(0) = intDptoSel
         qryinsert(1) = strUltPag + 1
         qryinsert(2) = NombrePagina
       qryinsert.Execute
       qryinsert.Close
       Set qryinsert = Nothing

  End If
  Call pCargarPaginas(intDptoSel)
'  txtCodigo.SetFocus

End Sub

Private Sub cmdBorrar_Click()
  Dim SQL As String
  Dim qry As rdoQuery
  Dim rs As rdoResultset
  Dim strdelete As String
  Dim qryDelete As rdoQuery
  Dim Respuesta
  
  Respuesta = MsgBox("�Est� seguro que desea borrar el registro?", vbYesNo)
  If Respuesta = vbYes Then
    If txtCodigo = "" Then
       MsgBox "indique c�digo del producto", vbExclamation
       Exit Sub
    End If
        ' comprobar que el c�digo est� en la p�gina
    SQL = "SELECT fr73codproducto"
    SQL = SQL & " FROM frl400"
    SQL = SQL & " where ad02coddpto = ? and"
    SQL = SQL & " frl3numpag = ? and"
    SQL = SQL & " fr73codproducto = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = intDptoSel
      qry(1) = strPagSel
      qry(2) = txtCodInt
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rs.EOF Then
       MsgBox "el c�digo no est� en esta p�gina"
       txtCodInt = ""
       txtCodigo = ""
       lblDescripcion = ""
       txtCantidad = ""
       txtCodigo.SetFocus
       Exit Sub
    End If
    rs.Close
    qry.Close
    'borrar el registro
    strdelete = " delete from frl400 where " & _
                " ad02coddpto = ? and " & _
                " frl3numpag = ? and " & _
                " fr73codproducto = ? "
          Set qryDelete = objApp.rdoConnect.CreateQuery("", strdelete)
          qryDelete(0) = intDptoSel
          qryDelete(1) = strPagSel
          qryDelete(2) = txtCodInt
          qryDelete.Execute
          qryDelete.Close
          Set qryDelete = Nothing
          intNumProd = intNumProd - 1
          txtCodigo = ""
          lblDescripcion = ""
          txtCodInt = ""
          txtCantidad = ""
  Else
       txtCodigo = ""
       lblDescripcion = ""
       txtCodInt = ""
       txtCantidad = ""
  End If


End Sub


Private Sub cmdBorrarPag_Click()
  Dim Respuesta
  Dim strdelete As String
  Dim qryDelete As rdoQuery
  
  Respuesta = MsgBox("�Est� seguro que desea borrar la p�gina?." & _
              "Si tiene productos se borrar�n", vbYesNo + vbQuestion, "Borrar P�gina")
  If Respuesta = vbYes Then
     If strPagSel = "" Then
         MsgBox "indique p�gina producto", vbExclamation
         Exit Sub
     End If
    'borrar los productos de la p�gina
     strdelete = " delete from frl400 where " & _
                " ad02coddpto = ? and " & _
                " frl3numpag = ? "
          Set qryDelete = objApp.rdoConnect.CreateQuery("", strdelete)
          qryDelete(0) = intDptoSel
          qryDelete(1) = strPagSel
          qryDelete.Execute
          qryDelete.Close
          Set qryDelete = Nothing
    'borrar la p�gina
     strdelete = " delete from frl300 where " & _
                " ad02coddpto = ? and " & _
                " frl3numpag = ? "
          Set qryDelete = objApp.rdoConnect.CreateQuery("", strdelete)
          qryDelete(0) = intDptoSel
          qryDelete(1) = strPagSel
          qryDelete.Execute
          qryDelete.Close
          Set qryDelete = Nothing
     Call pCargarPaginas(intDptoSel)
     cmdBorrarPag.Enabled = False
  End If
End Sub

Private Sub cmdConsultar_Click()
    Dim i%
     Screen.MousePointer = vbHourglass
     ssgrdProductos.RemoveAll
    If strPagSel <> "" Then
       Call pCargarProductos
    Else
        Screen.MousePointer = vbDefault
        MsgBox "No se ha seleccionado ninguna p�gina", vbExclamation
    End If
    Screen.MousePointer = vbDefault
    
End Sub
Private Sub cmdImprimir_Click()
Dim i As Integer
Dim resto As Integer
Dim j As String
Dim str1 As String
Dim str2 As String
Dim Num As String
Dim intI As Integer
     Screen.MousePointer = vbHourglass

vs.StartDoc
' imprimir t�tulo
' ------------------
   vs.CurrentX = 0
   vs.CurrentY = 0
   vs.FontBold = True
   vs.FontSize = 12
   vs.Text = sscboDpto.Columns(1).Value & "          " & lvwPag.SelectedItem
' imprimir etiqueta
'------------------
'blnSalir = False
  For intI = 0 To ssgrdProductos.Rows - 1

   If intI = 0 Then
     POSY = intPosy  '335
     POSX = intPosx
   Else
     If intI = 5 Or intI = 10 Or intI = 15 Or intI = 20 Or intI = 25 _
       Or intI = 30 Or intI = 35 Or intI = 40 Or intI = 45 Then
'       POSY = POSY + 1500
       POSY = POSY + 1600
       POSX = intPosx
     Else
       POSX = POSX + 725
     End If
   End If

     Num = Format(ssgrdProductos.Columns("CANTIDAD").Value, "000") & _
           ssgrdProductos.Columns("CODIGO").Value & _
           ssgrdProductos.Columns("CODIGOSEGURIDAD").Value
     str1 = "00"
     str2 = "00"
     For i = 0 To (Len(Num) / 2) - 1
       j = Mid(Num, i * 2 + 1, 1)
       str1 = str1 & TG(Int(j))
       j = Mid(Num, i * 2 + 2, 1)
       str2 = str2 & TG(Int(j))
     Next i
     str1 = str1 & "10"
     str2 = str2 & "00"
     For i = 0 To Len(str1) - 1
       If Mid(str1, i + 1, 1) = 0 Then
          Call vs.DrawRectangle(POSX, POSY, POSX + BNS, POSY + BAL)
          POSX = POSX + BNS
       Else
         Call vs.DrawRectangle(POSX, POSY, POSX + BBS, POSY + BAL)
         POSX = POSX + BBS
       End If
       If Mid(str2, i + 1, 1) = 0 Then
          POSX = POSX + BNS
       Else
          POSX = POSX + BBS
       End If
     Next i
 'imprimir texto
 '--------------
 
   vs.CurrentX = POSX - 1500
   vs.CurrentY = POSY + 730
   vs.FontBold = True
   vs.FontSize = 10
   vs.Text = Num
   vs.CurrentX = POSX - 1500
   vs.CurrentY = POSY + 930
   vs.FontSize = 8
   vs.Text = Mid(ssgrdProductos.Columns("DESCRIPCION").Value, 1, 17)
   vs.CurrentX = POSX - 1500
   vs.CurrentY = POSY + 1130
   vs.Text = Mid(ssgrdProductos.Columns("DESCRIPCION").Value, 18, 17)
   vs.CurrentX = POSX - 1500
   vs.CurrentY = POSY + 1330
   vs.Text = Mid(ssgrdProductos.Columns("DESCRIPCION").Value, 35)
   DoEvents
     ssgrdProductos.MoveNext

 Next intI
  vs.EndDoc
  vs.PrintDoc
     Screen.MousePointer = vbDefault

End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Load()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    ' datos para impresi�n de c�digos de barras
    ' -----------------------------------------
'    intPosx = 75
    intPosx = 0
    intPosy = 335
    intIncX = 3900
    intIncY = 1433
    TG(0) = "00110"
    TG(1) = "10001"
    TG(2) = "01001"
    TG(3) = "11000"
    TG(4) = "00101"
    TG(5) = "10100"
    TG(6) = "01100"
    TG(7) = "00011"
    TG(8) = "10010"
    TG(9) = "01010"


     BNS = 15 ' 1 � 0 ?
     BBS = 45 ' 3 veces BNS
     BAL = 700 ' altura

    '-------------------------------------------
    
    'se cargan los departamentos realizadores a los que tiene acceso el usuario
'    SQL = "SELECT AD02CODDPTO, AD02DESDPTO"
'    SQL = SQL & " FROM AD0200"
'    SQL = SQL & " WHERE AD02CODDPTO IN ("
'    SQL = SQL & " SELECT AD02CODDPTO FROM AD0300"
'    SQL = SQL & " WHERE SG02COD = ?"
'    SQL = SQL & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
'    SQL = SQL & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
'    SQL = SQL & " ORDER BY AD02DESDPTO"
    SQL = "SELECT AD02CODDPTO, AD02DESDPTO"
    SQL = SQL & " FROM AD0200"
    SQL = SQL & " where ad02indresponproc = -1 or"
    SQL = SQL & " ad02indcama = -1"
    SQL = SQL & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        intDptoSel = rs!AD02CODDPTO
        sscboDpto.Text = rs!AD02DESDPTO
        Do While Not rs.EOF
            sscboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
    
    'se cargan las p�ginas del Dpto. seleccionado
    lvwPag.ColumnHeaders.Add , , , 5000
    Call pCargarPaginas(intDptoSel)
    cmdBorrar.Enabled = False
    cmdImprimir.Enabled = False
    cmdA�adir.Enabled = False
    cmdConsultar.Enabled = False
    cmdBorrarPag.Enabled = False
    txtCodigo.Enabled = False
    txtCantidad.Enabled = False
    intNumProd = 0
    
End Sub

Private Sub lvwPag_Click()
     ssgrdProductos.RemoveAll
     cmdConsultar.Enabled = True
     cmdBorrarPag.Enabled = True
     txtCodigo.Enabled = True
     txtCantidad.Enabled = True
     strPagSel = lvwPag.SelectedItem.Tag
End Sub

Private Sub sscboDpto_Click()
     ssgrdProductos.RemoveAll
    intDptoSel = sscboDpto.Columns(0).Value
    Call pCargarPaginas(intDptoSel)

End Sub

Private Sub txtCantidad_Change()
   If txtCantidad <> "" Then
    cmdA�adir.Enabled = True
   Else
    cmdA�adir.Enabled = False
   End If
End Sub

Private Sub txtCodigo_Change()
   If Len(txtCodigo.Text) = 6 Then
    Obtener_Producto
'    txtCantidad.SetFocus
    txtCantidad = 1
    cmdBorrar.Enabled = True
   Else
    cmdBorrar.Enabled = False
   End If
End Sub
