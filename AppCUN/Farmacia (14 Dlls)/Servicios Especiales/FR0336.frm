VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmVerPedPtaNoValSec 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. PEDIR SERVICIO FARMACIA. Ver Pedido Planta no Dispensado"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0336.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Height          =   5775
      Left            =   120
      TabIndex        =   23
      Top             =   2160
      Width           =   10335
      Begin SSDataWidgets_B.SSDBGrid grdAux2 
         Height          =   5415
         Index           =   3
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   10170
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   15
         stylesets.count =   2
         stylesets(0).Name=   "CambioSec"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0336.frx":000C
         stylesets(1).Name=   "CambioSec2"
         stylesets(1).BackColor=   12632256
         stylesets(1).Picture=   "FR0336.frx":0028
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   15
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "CodProd"
         Columns(0).Name =   "CodProd"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   688
         Columns(1).Caption=   "Ubicaci�n"
         Columns(1).Name =   "Ubicaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1244
         Columns(2).Caption=   "CodInt"
         Columns(2).Name =   "CodInt"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1614
         Columns(3).Caption=   "Referencia"
         Columns(3).Name =   "Referencia"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   4313
         Columns(4).Caption=   "Descripci�n Producto"
         Columns(4).Name =   "Descripci�n Producto"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   1085
         Columns(5).Caption=   "Dosis"
         Columns(5).Name =   "Dosis"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   741
         Columns(6).Caption=   "FF"
         Columns(6).Name =   "FF"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   900
         Columns(7).Caption=   "UM"
         Columns(7).Name =   "UM"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   1032
         Columns(8).Caption=   "TamEnv"
         Columns(8).Name =   "TamEnv"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   1429
         Columns(9).Caption=   "CantPed"
         Columns(9).Name =   "CantPed"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   1482
         Columns(10).Caption=   "CantDisp"
         Columns(10).Name=   "CantDisp"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   16776960
         Columns(11).Width=   1482
         Columns(11).Caption=   "CantOri"
         Columns(11).Name=   "CantOri"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(12).Width=   688
         Columns(12).Caption=   "C�d.Sec."
         Columns(12).Name=   "C�d.Sec."
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   1244
         Columns(13).Caption=   "Secci�n"
         Columns(13).Name=   "Secci�n"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "Estilo"
         Columns(14).Name=   "Estilo"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(14).Locked=   -1  'True
         _ExtentX        =   17939
         _ExtentY        =   9551
         _StockProps     =   79
         Caption         =   "ACUMULADO DE NECESIDADES DE : "
      End
      Begin SSDataWidgets_B.SSDBGrid grdAux 
         Height          =   2295
         Index           =   1
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Visible         =   0   'False
         Width           =   10170
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   24
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0336.frx":0044
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   24
         Columns(0).Width=   1535
         Columns(0).Caption=   "CodProd"
         Columns(0).Name =   "CodProd"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1005
         Columns(1).Caption=   "Ubicaci�n"
         Columns(1).Name =   "Ubicaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1032
         Columns(2).Caption=   "CodInt"
         Columns(2).Name =   "CodInt"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1482
         Columns(3).Caption=   "Referencia"
         Columns(3).Name =   "Referencia"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3784
         Columns(4).Caption=   "Descripci�n Producto"
         Columns(4).Name =   "Descripci�n Producto"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   900
         Columns(5).Caption=   "Dosis"
         Columns(5).Name =   "Dosis"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   741
         Columns(6).Caption=   "FF"
         Columns(6).Name =   "FF"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   661
         Columns(7).Caption=   "UM"
         Columns(7).Name =   "UM"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   1217
         Columns(8).Caption=   "TamEnv"
         Columns(8).Name =   "TamEnv"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   1429
         Columns(9).Caption=   "CantPed"
         Columns(9).Name =   "CantPed"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   1561
         Columns(10).Caption=   "Ac.Ped."
         Columns(10).Name=   "Ac.Ped."
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   1482
         Columns(11).Caption=   "CantDisp"
         Columns(11).Name=   "CantDisp"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).HasBackColor=   -1  'True
         Columns(11).BackColor=   16776960
         Columns(12).Width=   3201
         Columns(12).Caption=   "Dispen?"
         Columns(12).Name=   "Dispen?"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   3201
         Columns(13).Caption=   "Dpto."
         Columns(13).Name=   "Dpto."
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   3201
         Columns(14).Caption=   "EstPet."
         Columns(14).Name=   "EstPet."
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3201
         Columns(15).Caption=   "Petici�n"
         Columns(15).Name=   "Petici�n"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   1535
         Columns(16).Caption=   "Ac.Disp."
         Columns(16).Name=   "Ac.Disp."
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(16).HasBackColor=   -1  'True
         Columns(16).BackColor=   16777215
         Columns(17).Width=   3201
         Columns(17).Caption=   "CodNecVal"
         Columns(17).Name=   "CodNecVal"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(18).Width=   3201
         Columns(18).Caption=   "NumLin"
         Columns(18).Name=   "NumLin"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(19).Width=   3200
         Columns(19).Caption=   "IndMod"
         Columns(19).Name=   "IndMod"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Caption=   "CantOri"
         Columns(20).Name=   "CantOri"
         Columns(20).DataField=   "Column 20"
         Columns(20).DataType=   8
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Caption=   "AcuOri"
         Columns(21).Name=   "AcuOri"
         Columns(21).DataField=   "Column 21"
         Columns(21).DataType=   8
         Columns(21).FieldLen=   256
         Columns(21).Locked=   -1  'True
         Columns(22).Width=   3200
         Columns(22).Caption=   "C�d.Sec."
         Columns(22).Name=   "C�d.Sec."
         Columns(22).DataField=   "Column 22"
         Columns(22).DataType=   8
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Caption=   "Secci�n"
         Columns(23).Name=   "Secci�n"
         Columns(23).DataField=   "Column 23"
         Columns(23).DataType=   8
         Columns(23).FieldLen=   256
         _ExtentX        =   17939
         _ExtentY        =   4048
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdAcumulado 
      Caption         =   "DISPENSAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   10680
      TabIndex        =   22
      Top             =   5280
      Width           =   1215
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   11040
      Top             =   7680
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdBloquear 
      Caption         =   "NO DISPENSAR"
      Height          =   615
      Left            =   10680
      TabIndex        =   20
      Top             =   3240
      Width           =   1215
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   1110
         Index           =   0
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   1958
         _Version        =   327681
         TabOrientation  =   3
         Tabs            =   2
         TabsPerRow      =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0336.frx":0060
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(16)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(19)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(23)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(28)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Label1"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txttext1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txttext1(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txttext1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txttext1(6)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txttext1(3)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txttext1(5)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txttext1(4)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0336.frx":007C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   8640
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Persona Peticionaria"
            Top             =   360
            Visible         =   0   'False
            Width           =   270
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   5
            Left            =   120
            TabIndex        =   4
            Tag             =   "C�digo Servicio"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txttext1 
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   3
            Left            =   8040
            TabIndex        =   6
            Tag             =   "Persona Peticionaria"
            Top             =   360
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   1680
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   360
            Width           =   5175
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   9000
            TabIndex        =   0
            Tag             =   "C�digo Necesidad"
            Top             =   360
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   9600
            TabIndex        =   1
            Tag             =   "Estado Petici�n"
            Top             =   360
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   10080
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Visible         =   0   'False
            Width           =   285
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   585
            Index           =   2
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   240
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   1032
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   6960
            TabIndex        =   3
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   360
            Visible         =   0   'False
            Width           =   780
            _Version        =   65537
            _ExtentX        =   1376
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "TODOS LOS SERVICIOS"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1320
            TabIndex        =   21
            Top             =   360
            Visible         =   0   'False
            Width           =   2565
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod.Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   6720
            TabIndex        =   19
            Top             =   120
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona Peticionaria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   6960
            TabIndex        =   18
            Top             =   120
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   17
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   1680
            TabIndex        =   16
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   6000
            TabIndex        =   15
            Top             =   120
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   8280
            TabIndex        =   14
            Top             =   120
            Visible         =   0   'False
            Width           =   1365
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   9600
            TabIndex        =   13
            Top             =   120
            Visible         =   0   'False
            Width           =   735
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmVerPedPtaNoValSec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmVerPedPtaNoValSec (FR0336.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Ver Pedido Planta no Validado                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim blnInload As Boolean
Dim glstrEstilo As String
Dim glstrSeccion As String
Dim blnInActualizar As Boolean


Private Sub Imprimir(strListado As String, intDes As Integer)
'JMRL 19991125
'Toma como par�metro el listado a imprimir y lo manda a la impresora;
'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
Dim strWhere As String
Dim strDNS As String
Dim strUser As String
Dim strPass As String
Dim strPATH As String
Dim contSecciones As Integer
Dim i As Integer
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado

strWhere = "{FR5501J.AD02CODDPTO}=" & gintservicio
' & " AND {FR5501J.FR19INDBLOQ}=0 "

'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
contSecciones = 0
If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
  If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
  Else
    For i = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
      If frmValPedPtaFarma.lvwSec.ListItems(i).Selected = True Then
        If contSecciones = 0 Then
          contSecciones = contSecciones + 1
          strWhere = strWhere & " AND ("
        Else
          strWhere = strWhere & " OR "
        End If
        If i = 2 Then
          strWhere = strWhere & "isnull({FR5501J.AD41CODSECCION})"
        Else
          strWhere = strWhere & "{FR5501J.AD41CODSECCION}=" & frmValPedPtaFarma.lvwSec.ListItems(i).Tag
        End If
      End If
    Next i
    If contSecciones > 0 Then
      strWhere = strWhere & ") "
    End If
  End If
End If
  
If gstrEst = "5" Then
  strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=5"
ElseIf gstrEst = "8" Then
  strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=8"
ElseIf gstrEst = "9" Then
  strWhere = strWhere & " AND {FR5501J.FR19INDDISPENSADO}=0"
  strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=9"
ElseIf gstrEst = "3" Then
  strWhere = strWhere & " AND {FR5501J.FR19INDDISPENSADO}=0"
  strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=3"
ElseIf gstrEst = "10" Then
  strWhere = strWhere & " AND {FR5501J.FR19INDDISPENSADO}=0"
  strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=10"
ElseIf gstrEst = "9,10" Then
  strWhere = strWhere & " AND {FR5501J.FR19INDDISPENSADO}=0"
  strWhere = strWhere & " AND ({FR5501J.FR26CODESTPETIC}=9 OR {FR5501J.FR26CODESTPETIC}=10)"
End If
             
If gstrLlamador = "frmValPedPtaFarma.Uno" Then
  strWhere = strWhere & " AND {FR5501J.FR55CODNECESUNID}=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
End If


  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp3
  CrystalReport1.Action = 1
Err_imp3:

End Sub

Private Sub cmdAcumulado_Click()
Dim i As Integer
Dim resto As Double
Dim codigo As Variant
Dim strupdate As String
Dim qryupdate As rdoQuery
Dim strFR04 As String
Dim qryFR04 As rdoQuery
Dim rdoFR04 As rdoResultset
Dim strAlmDes As String
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset
Dim strAlmFar As String
Dim strIns As String
Dim strIns80 As String
Dim strIns35 As String
Dim strFR55 As String
Dim qryFR55 As rdoQuery
Dim rdoFR55 As rdoResultset
Dim strFR19 As String
Dim qryFR19 As rdoQuery
Dim rdoFR19 As rdoResultset
Dim strUpd As String
Dim qryUpd As rdoQuery
Dim contSecciones As Integer
Dim j As Integer
Dim rstFR55 As rdoResultset
Dim dptocargo As String
Dim investigacion As String

'On Error GoTo ERR:
  
  cmdAcumulado.Enabled = False
  If grdAux(1).Rows > 0 Then
    Me.Enabled = False
    Screen.MousePointer = vbHourglass
    'grdAux(1).Redraw = False
    
    grdAux2(3).Redraw = False
    grdAux2(3).MoveFirst
    'Datos correctos
    For i = 0 To grdAux2(3).Rows - 1
      If IsNumeric(grdAux2(3).Columns("CantDisp").Value) = False Then
        Me.Enabled = True
        Screen.MousePointer = vbDefault
        cmdAcumulado.Enabled = True
        grdAux2(3).Redraw = True
        Call MsgBox("Datos Incorrectos", vbInformation, "Aviso")
        grdAux2(3).Col = 10
        Exit Sub
      End If
      If CCur(grdAux2(3).Columns("CantDisp").Value) > CCur(grdAux2(3).Columns("CantPed").Value) Then
        Me.Enabled = True
        Screen.MousePointer = vbDefault
        cmdAcumulado.Enabled = True
        grdAux2(3).Redraw = True
        Call MsgBox("No se puede dispensar mas de lo que se ha pedido", vbInformation, "Aviso")
        grdAux2(3).Col = 10
        Exit Sub
      End If
      grdAux2(3).Columns("CantDisp").Value = CCur(grdAux2(3).Columns("CantDisp").Value)
      grdAux2(3).MoveNext
    Next i
    
    strFR04 = "SELECT * FROM FR0400 WHERE AD02CODDPTO = ?"
    Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
    qryFR04(0) = gintservicio
    Set rdoFR04 = qryFR04.OpenResultset()
    If Not rdoFR04.EOF Then
      If Not IsNull(rdoFR04.rdoColumns("FR04CODALMACEN").Value) Then
        strAlmDes = rdoFR04.rdoColumns("FR04CODALMACEN").Value
      Else
       strAlmDes = 999 'Almac�n ficticio
      End If
    Else
      strAlmDes = 999 'Almac�n ficticio
    End If
    qryFR04.Close
    Set qryFR04 = Nothing
    Set rdoFR04 = Nothing
             
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 28
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strAlmFar = "0"
    Else
      strAlmFar = rdoFRH2.rdoColumns(0).Value
    End If
    qryFRH2.Close
    Set qryFRH2 = Nothing
    Set rdoFRH2 = Nothing
    
    strupdate = "UPDATE FR1900 SET FR19CANTSUMIFARM=TO_NUMBER(TO_CHAR(?)) WHERE FR19CODNECESVAL=? AND FR20NUMLINEA=?"
    Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
    grdAux(1).MoveLast
    grdAux2(3).MoveLast
    codigo = 0
    resto = 0
    For i = 0 To grdAux(1).Rows - 1
      If codigo <> grdAux(1).Columns("CodProd").Value Then
        grdAux(1).Columns("Ac.Disp.").Value = grdAux2(3).Columns("CantDisp").Value
        grdAux(1).Columns("CantDisp").Value = grdAux(1).Columns("Ac.Disp.").Value
        codigo = grdAux(1).Columns("CodProd").Value
        If CDec(grdAux(1).Columns("CantDisp").Value) > CDec(grdAux(1).Columns("CantPed").Value) Then
          resto = grdAux(1).Columns("CantDisp").Value - grdAux(1).Columns("CantPed").Value
          grdAux(1).Columns("CantDisp").Value = grdAux(1).Columns("CantPed").Value
        Else
          resto = 0
        End If
        qryupdate(0) = grdAux(1).Columns("CantDisp").Value
        qryupdate(1) = grdAux(1).Columns("CodNecVal").Value
        qryupdate(2) = grdAux(1).Columns("NumLin").Value
        qryupdate.Execute
        
        If CCur(grdAux(1).Columns("CantDisp").Value) > 0 Then
          'Se realiza el insert en FRK100
          strFR55 = "SELECT * FROM FR5500 WHERE FR55CODNECESUNID=" & grdAux(1).Columns("Petici�n").Value
          Set rstFR55 = objApp.rdoConnect.OpenResultset(strFR55)
          
          If IsNull(rstFR55.rdoColumns("FR55INDINTERCIENT").Value) Then
               investigacion = "null"
          Else
               investigacion = rstFR55.rdoColumns("FR55INDINTERCIENT").Value
          End If
          If IsNull(rstFR55.rdoColumns("AD02CODDPTO_CRG").Value) Then
               dptocargo = "null"
          Else
               dptocargo = rstFR55.rdoColumns("AD02CODDPTO_CRG").Value
          End If
          rstFR55.Close
          Set rstFR55 = Nothing
          
          strIns = "INSERT INTO FRK100 ("
          strIns = strIns & "FRK1CODMOV,FRK1FECMOV,"
          strIns = strIns & "AD02CODDPTO,FR73CODPRODUCTO,"
          strIns = strIns & "FRK1CANTIDAD,FRK1TAMENVASE,"
          strIns = strIns & "FRK1INDMOD,FRK1INDFAC,"
          strIns = strIns & "FRK1CODNECESUNID,FRK1INDINTERCIENT,AD02CODDPTO_CRG"
          strIns = strIns & ") VALUES ("
          strIns = strIns & "FRK1CODMOV_SEQUENCE.NEXTVAL" & ","
          strIns = strIns & "SYSDATE,"
          strIns = strIns & grdAux(1).Columns("Dpto.").Value & ","
          strIns = strIns & grdAux(1).Columns("CodProd").Value & ","
          strIns = strIns & grdAux(1).Columns("CantDisp").Value & ","
          If grdAux(1).Columns("TamEnv").Value = "" Then
            strIns = strIns & "1,"
          Else
            strIns = strIns & grdAux(1).Columns("TamEnv").Value & ","
          End If
          If grdAux(1).Columns("IndMod").Value = "" Then
            strIns = strIns & "0,"
          Else
            strIns = strIns & grdAux(1).Columns("IndMod").Value & ","
          End If
          strIns = strIns & "0," & grdAux(1).Columns("Petici�n").Value & ","
          strIns = strIns & investigacion & ","
          strIns = strIns & dptocargo & ")"
          objApp.rdoConnect.Execute strIns, 64
          
          'Se realiza rl insert en FR8000: salidas de almac�n
          strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,"
          strIns80 = strIns80 & "FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strIns80 = strIns80 & "FR90CODTIPMOV,FR80FECMOVIMIENTO,"
          strIns80 = strIns80 & "FR73CODPRODUCTO,FR80CANTPROD,"
          strIns80 = strIns80 & "FR80PRECUNI,FR93CODUNIMEDIDA)"
          strIns80 = strIns80 & " VALUES ("
          strIns80 = strIns80 & "FR80NUMMOV_SEQUENCE.nextval" & ","
          strIns80 = strIns80 & strAlmFar & "," & strAlmDes
          strIns80 = strIns80 & ",3,SYSDATE,"
          strIns80 = strIns80 & grdAux(1).Columns("CodProd").Value & ","
          strIns80 = strIns80 & grdAux(1).Columns("CantDisp").Value
          strIns80 = strIns80 & ",1,'NE')"
          objApp.rdoConnect.Execute strIns80, 64
          
          'Se realiza el insert en FR3500: entradas de almac�n
          strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,"
          strIns35 = strIns35 & "FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strIns35 = strIns35 & "FR90CODTIPMOV,FR35FECMOVIMIENTO,"
          strIns35 = strIns35 & "FR73CODPRODUCTO,FR35CANTPRODUCTO,"
          strIns35 = strIns35 & "FR35PRECIOUNIDAD,FR93CODUNIMEDIDA)"
          strIns35 = strIns35 & " VALUES ("
          strIns35 = strIns35 & "FR35CODMOVIMIENTO_SEQUENCE.nextval" & ","
          strIns35 = strIns35 & strAlmFar & "," & strAlmDes
          strIns35 = strIns35 & ",3,SYSDATE,"
          strIns35 = strIns35 & grdAux(1).Columns("CodProd").Value & ","
          strIns35 = strIns35 & grdAux(1).Columns("CantDisp").Value
          strIns35 = strIns35 & ",1,'NE')"
          objApp.rdoConnect.Execute strIns35, 64
          
        End If
        
        grdAux2(3).MovePrevious
      Else
        grdAux(1).Columns("CantDisp").Value = resto
        If CDec(grdAux(1).Columns("CantDisp").Value) > CDec(grdAux(1).Columns("CantPed").Value) Then
          resto = grdAux(1).Columns("CantDisp").Value - grdAux(1).Columns("CantPed").Value
          grdAux(1).Columns("CantDisp").Value = grdAux(1).Columns("CantPed").Value
        Else
          resto = 0
        End If
        qryupdate(0) = grdAux(1).Columns("CantDisp").Value
        qryupdate(1) = grdAux(1).Columns("CodNecVal").Value
        qryupdate(2) = grdAux(1).Columns("NumLin").Value
        qryupdate.Execute
      
        If CCur(grdAux(1).Columns("CantDisp").Value) > 0 Then
          'Se realiza el insert en FRK100
          strFR55 = "SELECT * FROM FR5500 WHERE FR55CODNECESUNID=" & grdAux(1).Columns("Petici�n").Value
          Set rstFR55 = objApp.rdoConnect.OpenResultset(strFR55)
          
          If IsNull(rstFR55.rdoColumns("FR55INDINTERCIENT").Value) Then
               investigacion = "null"
          Else
               investigacion = rstFR55.rdoColumns("FR55INDINTERCIENT").Value
          End If
          If IsNull(rstFR55.rdoColumns("AD02CODDPTO_CRG").Value) Then
               dptocargo = "null"
          Else
               dptocargo = rstFR55.rdoColumns("AD02CODDPTO_CRG").Value
          End If
          rstFR55.Close
          Set rstFR55 = Nothing
          
          strIns = "INSERT INTO FRK100 ("
          strIns = strIns & "FRK1CODMOV,FRK1FECMOV,"
          strIns = strIns & "AD02CODDPTO,FR73CODPRODUCTO,"
          strIns = strIns & "FRK1CANTIDAD,FRK1TAMENVASE,"
          strIns = strIns & "FRK1INDMOD,FRK1INDFAC,"
          strIns = strIns & "FRK1CODNECESUNID,FRK1INDINTERCIENT,AD02CODDPTO_CRG"
          strIns = strIns & ") VALUES ("
          strIns = strIns & "FRK1CODMOV_SEQUENCE.NEXTVAL" & ","
          strIns = strIns & "SYSDATE,"
          strIns = strIns & grdAux(1).Columns("Dpto.").Value & ","
          strIns = strIns & grdAux(1).Columns("CodProd").Value & ","
          strIns = strIns & grdAux(1).Columns("CantDisp").Value & ","
          If grdAux(1).Columns("TamEnv").Value = "" Then
            strIns = strIns & "1,"
          Else
            strIns = strIns & grdAux(1).Columns("TamEnv").Value & ","
          End If
          If grdAux(1).Columns("IndMod").Value = "" Then
            strIns = strIns & "0,"
          Else
            strIns = strIns & grdAux(1).Columns("IndMod").Value & ","
          End If
          strIns = strIns & "0," & grdAux(1).Columns("Petici�n").Value & ","
          strIns = strIns & investigacion & ","
          strIns = strIns & dptocargo & ")"
          objApp.rdoConnect.Execute strIns, 64
          
          'Se realiza rl insert en FR8000: salidas de almac�n
          strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,"
          strIns80 = strIns80 & "FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strIns80 = strIns80 & "FR90CODTIPMOV,FR80FECMOVIMIENTO,"
          strIns80 = strIns80 & "FR73CODPRODUCTO,FR80CANTPROD,"
          strIns80 = strIns80 & "FR80PRECUNI,FR93CODUNIMEDIDA)"
          strIns80 = strIns80 & " VALUES ("
          strIns80 = strIns80 & "FR80NUMMOV_SEQUENCE.nextval" & ","
          strIns80 = strIns80 & strAlmFar & "," & strAlmDes
          strIns80 = strIns80 & ",3,SYSDATE,"
          strIns80 = strIns80 & grdAux(1).Columns("CodProd").Value & ","
          strIns80 = strIns80 & grdAux(1).Columns("CantDisp").Value
          strIns80 = strIns80 & ",1,'NE')"
          objApp.rdoConnect.Execute strIns80, 64
          
          'Se realiza el insert en FR3500: entradas de almac�n
          strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,"
          strIns35 = strIns35 & "FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strIns35 = strIns35 & "FR90CODTIPMOV,FR35FECMOVIMIENTO,"
          strIns35 = strIns35 & "FR73CODPRODUCTO,FR35CANTPRODUCTO,"
          strIns35 = strIns35 & "FR35PRECIOUNIDAD,FR93CODUNIMEDIDA)"
          strIns35 = strIns35 & " VALUES ("
          strIns35 = strIns35 & "FR35CODMOVIMIENTO_SEQUENCE.nextval" & ","
          strIns35 = strIns35 & strAlmFar & "," & strAlmDes
          strIns35 = strIns35 & ",3,SYSDATE,"
          strIns35 = strIns35 & grdAux(1).Columns("CodProd").Value & ","
          strIns35 = strIns35 & grdAux(1).Columns("CantDisp").Value
          strIns35 = strIns35 & ",1,'NE')"
          objApp.rdoConnect.Execute strIns35, 64
        
        End If
      End If
      grdAux(1).MovePrevious
    Next i
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    'grdAux(1).Redraw = True
    grdAux2(3).Redraw = True
  End If
  Call Imprimir("FR3362.RPT", 1)
  
  Me.Enabled = False
  Screen.MousePointer = vbHourglass
  
  strUpd = "UPDATE FR1900 SET FR19INDDISPENSADO=?,FR19INDFAC=-1"
  strUpd = strUpd & " WHERE "
  strUpd = strUpd & " FR19CANTNECESQUIR=FR19CANTSUMIFARM"
  strUpd = strUpd & " AND NVL(FR19INDDISPENSADO,0)=0"
  strUpd = strUpd & " AND FR55CODNECESUNID IN "
  strUpd = strUpd & " (SELECT FR55CODNECESUNID FROM FR5500 "
  If gstrEst = "9" Then
    strUpd = strUpd & " WHERE FR26CODESTPETIC=9"
  ElseIf gstrEst = "10" Then
    strUpd = strUpd & " WHERE FR26CODESTPETIC=10"
  ElseIf gstrEst = "9,10" Then
    strUpd = strUpd & " WHERE FR26CODESTPETIC in (9,10)"
  Else '?
    strUpd = strUpd & " WHERE -1=0 "
  End If
  If gstrLlamador = "frmValPedPtaFarma.Uno" Then
    strUpd = strUpd & " AND FR5500.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
  End If
  strUpd = strUpd & " AND (FR5500.FR55INDESTUPEFACIENTE=0 OR FR5500.FR55INDESTUPEFACIENTE IS NULL)"
  strUpd = strUpd & " AND (FR5500.FR55INDFM=0 OR FR5500.FR55INDFM IS NULL)"
  
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
    If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
    Else
      For j = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
        If frmValPedPtaFarma.lvwSec.ListItems(j).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strUpd = strUpd & " AND ("
          Else
            strUpd = strUpd & " OR "
          End If
          If j = 2 Then
            strUpd = strUpd & "FR5500.AD41CODSECCION IS NULL"
          Else
            strUpd = strUpd & "FR5500.AD41CODSECCION=" & frmValPedPtaFarma.lvwSec.ListItems(j).Tag
          End If
        End If
      Next j
      If contSecciones > 0 Then
        strUpd = strUpd & ") "
      End If
    End If
  End If
  
  strUpd = strUpd & " AND AD02CODDPTO=? )"
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
  qryUpd(0) = -1
  qryUpd(1) = gintservicio
  qryUpd.Execute
  
  strUpd = "UPDATE FR1900 SET FR19CANTNECESQUIR=FR19CANTNECESQUIR-FR19CANTSUMIFARM,"
  strUpd = strUpd & " FR19CANTSUMIFARM=0 "
  strUpd = strUpd & " WHERE "
  strUpd = strUpd & " FR19CANTNECESQUIR>FR19CANTSUMIFARM"
  strUpd = strUpd & " AND NVL(FR19INDDISPENSADO,0)=0"
  strUpd = strUpd & " AND FR55CODNECESUNID IN "
  strUpd = strUpd & " (SELECT FR55CODNECESUNID FROM FR5500 "
  If gstrEst = "9" Then
    strUpd = strUpd & " WHERE FR26CODESTPETIC=9"
  ElseIf gstrEst = "10" Then
    strUpd = strUpd & " WHERE FR26CODESTPETIC=10"
  ElseIf gstrEst = "9,10" Then
    strUpd = strUpd & " WHERE FR26CODESTPETIC in (9,10)"
  Else '?
    strUpd = strUpd & " WHERE -1=0 "
  End If
  If gstrLlamador = "frmValPedPtaFarma.Uno" Then
    strUpd = strUpd & " AND FR5500.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
  End If
  strUpd = strUpd & " AND (FR5500.FR55INDESTUPEFACIENTE=0 OR FR5500.FR55INDESTUPEFACIENTE IS NULL)"
  strUpd = strUpd & " AND (FR5500.FR55INDFM=0 OR FR5500.FR55INDFM IS NULL)"
  
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
    If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
    Else
      For j = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
        If frmValPedPtaFarma.lvwSec.ListItems(j).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strUpd = strUpd & " AND ("
          Else
            strUpd = strUpd & " OR "
          End If
          If j = 2 Then
            strUpd = strUpd & "FR5500.AD41CODSECCION IS NULL"
          Else
            strUpd = strUpd & "FR5500.AD41CODSECCION=" & frmValPedPtaFarma.lvwSec.ListItems(j).Tag
          End If
        End If
      Next j
      If contSecciones > 0 Then
        strUpd = strUpd & ") "
      End If
    End If
  End If
  
  strUpd = strUpd & " AND AD02CODDPTO=? )"
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
  qryUpd(0) = gintservicio
  qryUpd.Execute
  
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  strUpd = "UPDATE FR5500 SET FR55FECDISPEN=SYSDATE,FR26CODESTPETIC=? "
  strUpd = strUpd & " WHERE FR55CODNECESUNID=? "
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
  
  strFR19 = "SELECT COUNT(*) FROM FR1900 "
  strFR19 = strFR19 & " WHERE FR55CODNECESUNID=?"
  strFR19 = strFR19 & " AND FR19INDBLOQ=? "
  strFR19 = strFR19 & " AND NVL(FR19INDDISPENSADO,0)=0"
  Set qryFR19 = objApp.rdoConnect.CreateQuery("", strFR19)
  
  strFR55 = "SELECT * FROM FR5500 "
  If gstrEst = "9" Then
    strFR55 = strFR55 & " WHERE FR26CODESTPETIC=9"
  ElseIf gstrEst = "10" Then
    strFR55 = strFR55 & " WHERE FR26CODESTPETIC=10"
  ElseIf gstrEst = "9,10" Then
    strFR55 = strFR55 & " WHERE FR26CODESTPETIC in (9,10)"
  Else '?
    strFR55 = strFR55 & " WHERE -1=0 "
  End If
  If gstrLlamador = "frmValPedPtaFarma.Uno" Then
    strFR55 = strFR55 & " AND FR5500.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
  End If
  strFR55 = strFR55 & " AND (FR5500.FR55INDESTUPEFACIENTE=0 OR FR5500.FR55INDESTUPEFACIENTE IS NULL)"
  strFR55 = strFR55 & " AND (FR5500.FR55INDFM=0 OR FR5500.FR55INDFM IS NULL)"
  
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
    If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
    Else
      For j = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
        If frmValPedPtaFarma.lvwSec.ListItems(j).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strFR55 = strFR55 & " AND ("
          Else
            strFR55 = strFR55 & " OR "
          End If
          If j = 2 Then
            strFR55 = strFR55 & "FR5500.AD41CODSECCION IS NULL"
          Else
            strFR55 = strFR55 & "FR5500.AD41CODSECCION=" & frmValPedPtaFarma.lvwSec.ListItems(j).Tag
          End If
        End If
      Next j
      If contSecciones > 0 Then
        strFR55 = strFR55 & ") "
      End If
    End If
  End If
  
  strFR55 = strFR55 & " AND AD02CODDPTO=? "
  Set qryFR55 = objApp.rdoConnect.CreateQuery("", strFR55)
  qryFR55(0) = gintservicio
  Set rdoFR55 = qryFR55.OpenResultset()
  While Not rdoFR55.EOF
    qryFR19(0) = rdoFR55("FR55CODNECESUNID").Value
    qryFR19(1) = 0
    Set rdoFR19 = qryFR19.OpenResultset()
    If rdoFR19(0).Value = 0 Then
      qryUpd(0) = 5
      qryUpd(1) = rdoFR55("FR55CODNECESUNID").Value
      'qryUpd(2) = 5
      qryUpd.Execute
    Else
      qryUpd(0) = 9
      qryUpd(1) = rdoFR55("FR55CODNECESUNID").Value
      'qryUpd(2) = 9
      qryUpd.Execute
    End If
    rdoFR55.MoveNext
  Wend
  qryFR55.Close
  Set qryFR55 = Nothing
  Set rdoFR55 = Nothing
  Call Actualizar_Grid

  Me.Enabled = True
  Screen.MousePointer = vbDefault
  Call MsgBox("Se ha realizado la Dispensaci�n", vbExclamation, "Aviso")
  cmdAcumulado.Enabled = True
  
  Exit Sub

ERR:
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  'grdAux(1).Redraw = True
  grdAux2(3).Redraw = True
  Call MsgBox("No se ha podido realizar la Dispensaci�n Correctamente", vbExclamation, "Aviso")
  cmdAcumulado.Enabled = True

End Sub


Private Sub Form_Activate()

If blnInload = True Then
  If gstrEst = "5" Then
    cmdBloquear.Enabled = False
    cmdAcumulado.Enabled = False
  ElseIf gstrEst = "8" Then
    cmdBloquear.Enabled = False
    cmdAcumulado.Enabled = False
  ElseIf gstrEst = "9" Then
  ElseIf gstrEst = "3" Then
    cmdBloquear.Enabled = False
    cmdAcumulado.Enabled = False
  ElseIf gstrEst = "10" Then
  ElseIf gstrEst = "9,10" Then
  End If
  
  grdAux2(3).Caption = grdAux2(3).Caption & txttext1(5).Text & "-" & txttext1(6).Text
  Call Actualizar_Grid
  blnInload = False
End If

End Sub

Private Sub grdAux2_GotFocus(Index As Integer)
    If grdAux2(3).Rows > 0 Then
      tlbToolbar1.Buttons(6).Enabled = True
    End If
End Sub

Private Sub Actualizar_Acumulados()
Dim i As Integer
  
  grdAux2(3).Redraw = False
  grdAux2(3).RemoveAll
  If grdAux(1).Rows > 0 Then
    grdAux(1).MoveFirst
    ReDim marca(grdAux(1).Rows)
    For i = 0 To grdAux(1).Rows - 1
      If grdAux(1).Columns("Ac.Disp.").Value <> "" Then
        grdAux2(3).AddItem grdAux(1).Columns("CodProd").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Ubicaci�n").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("CodInt").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Referencia").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Descripci�n Producto").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Dosis").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("FF").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("UM").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("TamEnv").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Ac.Ped.").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Ac.Disp.").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("AcuOri").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("C�d.Sec.").Value & Chr(vbKeyTab) & _
          grdAux(1).Columns("Secci�n").Value
      End If
      grdAux(1).MoveNext
    Next i
    grdAux2(3).MoveFirst
    grdAux(1).MoveFirst
  End If
  grdAux2(3).Redraw = True

End Sub


Private Sub grdAux2_KeyPress(Index As Integer, KeyAscii As Integer)

Select Case KeyAscii
Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
Case Else
  KeyAscii = 0
End Select

End Sub

Private Sub grdAux2RowLoaded()
Dim j As Integer

  If blnInActualizar = True Then
    glstrEstilo = "CambioSec2"
  End If
  grdAux2(3).Redraw = False
  grdAux2(3).MoveFirst
  For j = 0 To grdAux2(3).Rows - 1
    If glstrSeccion <> grdAux2(3).Columns("C�d.Sec.").Value Then
      If glstrEstilo = "CambioSec2" Then
        glstrEstilo = "CambioSec"
      Else
        glstrEstilo = "CambioSec2"
      End If
    End If
    glstrSeccion = grdAux2(3).Columns("C�d.Sec.").Value
    grdAux2(3).Columns("Estilo").Value = glstrEstilo
    grdAux2(3).MoveNext
  Next j
  grdAux2(3).Columns("C�d.Sec.").Position = 0
  grdAux2(3).Columns("Secci�n").Position = 1
  grdAux2(3).MoveFirst
  grdAux2(3).Redraw = True

End Sub

Private Sub grdAux2_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer

  If grdAux2(3).Columns("Estilo").Value = "CambioSec" Then
    For i = 0 To grdAux2(3).Cols - 1
      If i <> 10 Then
        grdAux2(3).Columns(i).CellStyleSet "CambioSec"
      End If
    Next i
  ElseIf grdAux2(3).Columns("Estilo").Value = "CambioSec2" Then
    For i = 0 To grdAux2(3).Cols - 1
      If i <> 10 Then
        grdAux2(3).Columns(i).CellStyleSet "CambioSec2"
      End If
    Next i
  End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdbloquear_Click()
Dim qryupdate As rdoQuery
Dim strupdate As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim sqlstr As String
Dim intMsg As Integer
Dim nTotalSelRows As Integer
Dim strUpd As String
Dim qryUpd As rdoQuery
Dim i As Integer
Dim bkmrk As Variant
Dim strFR19 As String
Dim qryFR19 As rdoQuery
Dim rdoFR19 As rdoResultset
Dim strFR55 As String
Dim qryFR55 As rdoQuery
Dim rdoFR55 As rdoResultset
Dim contSecciones As Integer
Dim j As Integer
    
  intMsg = MsgBox("�Est� seguro que NO desea DISPENSAR estos productos?", vbQuestion + vbYesNo)
  If intMsg = vbNo Then
    Exit Sub
  End If
    
  cmdBloquear.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  nTotalSelRows = grdAux2(3).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    strupdate = "UPDATE FR1900 SET FR19INDBLOQ=?"
    strupdate = strupdate & " WHERE "
    strupdate = strupdate & " FR73CODPRODUCTO=?"
    strupdate = strupdate & " AND NVL(FR19INDDISPENSADO,0)=0"
    strupdate = strupdate & " AND FR55CODNECESUNID IN "
    strupdate = strupdate & " (SELECT FR55CODNECESUNID FROM FR5500 "
    If gstrEst = "9" Then
      strupdate = strupdate & " WHERE FR26CODESTPETIC=9"
    ElseIf gstrEst = "10" Then
      strupdate = strupdate & " WHERE FR26CODESTPETIC=10"
    ElseIf gstrEst = "9,10" Then
      strupdate = strupdate & " WHERE FR26CODESTPETIC in (9,10)"
    Else '?
      strupdate = strupdate & " WHERE -1=0 "
    End If
    If gstrLlamador = "frmValPedPtaFarma.Uno" Then
      strupdate = strupdate & " AND FR5500.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
    End If
    strupdate = strupdate & " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)"
    strupdate = strupdate & " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
    
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
      If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
      Else
        For j = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
          If frmValPedPtaFarma.lvwSec.ListItems(j).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strupdate = strupdate & " AND ("
            Else
              strupdate = strupdate & " OR "
            End If
            If j = 2 Then
              strupdate = strupdate & "FR5500.AD41CODSECCION IS NULL"
            Else
              strupdate = strupdate & "FR5500.AD41CODSECCION=" & frmValPedPtaFarma.lvwSec.ListItems(j).Tag
            End If
          End If
        Next j
        If contSecciones > 0 Then
          strupdate = strupdate & ") "
        End If
      End If
    End If
    
    strupdate = strupdate & " AND AD02CODDPTO=? )"
    Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
    
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdAux2(3).SelBookmarks(i)
      qryupdate(0) = -1
      qryupdate(1) = grdAux2(3).Columns("CodProd").CellValue(bkmrk)
      qryupdate(2) = gintservicio
      qryupdate.Execute
    Next i
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    strUpd = "UPDATE FR5500 SET FR26CODESTPETIC=? "
    strUpd = strUpd & " WHERE FR55CODNECESUNID=? "
    strUpd = strUpd & " AND FR26CODESTPETIC<>? "
    Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
    
    strFR19 = "SELECT COUNT(*) FROM FR1900 "
    strFR19 = strFR19 & " WHERE FR55CODNECESUNID=?"
    strFR19 = strFR19 & " AND FR19INDBLOQ=? "
    strFR19 = strFR19 & " AND NVL(FR19INDDISPENSADO,0)=0"
    Set qryFR19 = objApp.rdoConnect.CreateQuery("", strFR19)
    
    strFR55 = "SELECT * FROM FR5500 "
    If gstrEst = "9" Then
      strFR55 = strFR55 & " WHERE FR26CODESTPETIC=9"
    ElseIf gstrEst = "10" Then
      strFR55 = strFR55 & " WHERE FR26CODESTPETIC=10"
    ElseIf gstrEst = "9,10" Then
      strFR55 = strFR55 & " WHERE FR26CODESTPETIC in (9,10)"
    Else '?
      strFR55 = strFR55 & " WHERE -1=0 "
    End If
    If gstrLlamador = "frmValPedPtaFarma.Uno" Then
      strFR55 = strFR55 & " AND FR5500.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
    End If
    strFR55 = strFR55 & " AND (FR5500.FR55INDESTUPEFACIENTE=0 OR FR5500.FR55INDESTUPEFACIENTE IS NULL)"
    strFR55 = strFR55 & " AND (FR5500.FR55INDFM=0 OR FR5500.FR55INDFM IS NULL)"
    
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
      If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
      Else
        For j = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
          If frmValPedPtaFarma.lvwSec.ListItems(j).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strFR55 = strFR55 & " AND ("
            Else
              strFR55 = strFR55 & " OR "
            End If
            If j = 2 Then
              strFR55 = strFR55 & "FR5500.AD41CODSECCION IS NULL"
            Else
              strFR55 = strFR55 & "FR5500.AD41CODSECCION=" & frmValPedPtaFarma.lvwSec.ListItems(j).Tag
            End If
          End If
        Next j
        If contSecciones > 0 Then
          strFR55 = strFR55 & ") "
        End If
      End If
    End If
    
    strFR55 = strFR55 & " AND AD02CODDPTO=? "
    Set qryFR55 = objApp.rdoConnect.CreateQuery("", strFR55)
    qryFR55(0) = gintservicio
    Set rdoFR55 = qryFR55.OpenResultset()
    While Not rdoFR55.EOF
      qryFR19(0) = rdoFR55("FR55CODNECESUNID").Value
      qryFR19(1) = 0
      Set rdoFR19 = qryFR19.OpenResultset()
      If rdoFR19(0).Value = 0 Then
        qryUpd(0) = 5
        qryUpd(1) = rdoFR55("FR55CODNECESUNID").Value
        qryUpd(2) = 5
        qryUpd.Execute
      Else
        qryUpd(0) = 9
        qryUpd(1) = rdoFR55("FR55CODNECESUNID").Value
        qryUpd(2) = 9
        qryUpd.Execute
      End If
      rdoFR55.MoveNext
    Wend
    qryFR55.Close
    Set qryFR55 = Nothing
    Set rdoFR55 = Nothing
  
  End If
  Call Actualizar_Grid
  Screen.MousePointer = vbDefault
  Me.Enabled = True
  cmdBloquear.Enabled = True

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String

blnInload = True

    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
      With objMasterInfo
        Set .objFormContainer = fraFrame1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(2)
        
        .strName = "Necesidad Unidad"
          
        .strTable = "FR5500"
        .strWhere = "FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
        .intAllowance = cwAllowReadOnly
        Call .FormAddOrderField("FR55CODNECESUNID", cwAscending)
       
        strKey = .strDataBase & .strTable
        'Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
        'Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
        'Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
        'Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
        'Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")
    
        Call .objPrinter.Add("FR3361", "Preparaci�n Secciones")
        Call .objPrinter.Add("FR3362", "Entrega Secciones")
    
      End With
'    With objMultiInfo
'        .strName = "Detalle Necesidad"
'        Set .objFormContainer = fraFrame1(2)
'        Set .objFatherContainer = fraFrame1(1)
'        Set .tabMainTab = Nothing
'        Set .grdGrid = grdAux(1)
'        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'        .strTable = "FR1900"
'
'        Select Case gstrEst
'        Case "3", "4" 'Enviada y Validada
'          .intAllowance = cwAllowAll
'          .strWhere = " FR55CODNECESUNID IN (SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=" & gstrEst & " AND AD02CODDPTO=" & frmValPedPtaFarma.cboservicio.Text & ") "
'        Case "5" 'Dispensada Total
'          .intAllowance = cwAllowReadOnly
'          .strWhere = " FR55CODNECESUNID IN (SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=" & gstrEst & " AND AD02CODDPTO=" & frmValPedPtaFarma.cboservicio.Text & ") "
'        Case "8" 'Anulada
'          .intAllowance = cwAllowAdd
'          .strWhere = " FR55CODNECESUNID IN (SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=" & gstrEst & " AND AD02CODDPTO=" & frmValPedPtaFarma.cboservicio.Text & ") "
'        Case "9" 'Dispensada Parcial
'          .intAllowance = cwAllowAll
'          '.strWhere = "FR19CANTNECESQUIR > FR19CANTSUMIFARM "
'          .strWhere = " NVL(FR19INDDISPENSADO,0)=0 "
'          .strWhere = .strWhere & " FR55CODNECESUNID IN (SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=" & gstrEst & " AND AD02CODDPTO=" & frmValPedPtaFarma.cboservicio.Text & ") "
'        Case Else
'          .intAllowance = cwAllowAll
'          .strWhere = " FR55CODNECESUNID IN (SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=" & gstrEst & " AND AD02CODDPTO=" & frmValPedPtaFarma.cboservicio.Text & ") "
'        End Select
'
'
'        Call .objPrinter.Add("FR1361", "Preparaci�n")
'        Call .objPrinter.Add("FR1362", "Entrega")
'
'        'Call .FormAddOrderField("FR19CODNECESVAL", cwAscending)
'        'Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
'        Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
'
'        .intCursorSize = 0
'
'        strKey = .strDataBase & .strTable
'
'        'Se establecen los campos por los que se puede filtrar
'        Call .FormCreateFilterWhere(strKey, "Necesidades")
'        Call .FormAddFilterWhere(strKey, "FR55CODNECESVAL", "C�d Necesidad Validado", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "FR20NUMLINEA", "N�mero de L�nea", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
'        'Call .FormAddFilterWhere(strKey, "", "Producto", cwString)
'        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida", cwString)
'        'Call .FormAddFilterWhere(strKey, "", "Unidad Medida", cwString)
'        Call .FormAddFilterWhere(strKey, "FR19CANTNECESQUIR", "Cantidad", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "FR19FECVALIDACION", "Fecha Validaci�n", cwDate)
'        Call .FormAddFilterWhere(strKey, "SG02COD_VAL", "Persona Valida", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA_SUM", "C�d Unidad Medida Sum", cwString)
'
'        'Se establecen los campos por los que se puede ordenar con el filtro
'        Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�d Necesidad Unidad")
'        Call .FormAddFilterOrder(strKey, "FR20NUMLINEA", "N�mero de L�nea")
'        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d. Producto")
'        'Call .FormAddFilterOrder(strKey, "", "Producto")
'        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida")
'        'Call .FormAddFilterOrder(strKey, "", "Unidad Medida")
'        Call .FormAddFilterOrder(strKey, "FR19CANTNECESQUIR", "Cantidad")
'        Call .FormAddFilterOrder(strKey, "FR19FECVALIDACION", "Fecha Validaci�n")
'        Call .FormAddFilterOrder(strKey, "SG02COD_VAL", "Persona Valida")
'        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA_SUM", "C�d Unidad Medida Sum")
'
'        'Call .FormAddRelation("FR55CODNECESUNID", txttext1(0))
'
'    End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
'        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
'
'        Call .GridAddColumn(objMultiInfo, "C�d. Necesidad", "FR55CODNECESUNID", cwNumeric, 9)
'        Call .GridAddColumn(objMultiInfo, "L�nea", "FR20NUMLINEA", cwNumeric, 3)
'        Call .GridAddColumn(objMultiInfo, "C�d. Prod", "FR73CODPRODUCTO", cwNumeric, 9)
'        Call .GridAddColumn(objMultiInfo, "Producto", "", cwNumeric, 7)
'        Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
'        Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
'        Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
'        Call .GridAddColumn(objMultiInfo, "FF", "", cwString, 5)
'        Call .GridAddColumn(objMultiInfo, "D�sis", "", cwDecimal, 9)
'        Call .GridAddColumn(objMultiInfo, "U.M.", "FR93CODUNIMEDIDA", cwString, 5)
'        Call .GridAddColumn(objMultiInfo, "Cantidad", "FR19CANTNECESQUIR", cwNumeric, 11)
'        Call .GridAddColumn(objMultiInfo, "Cant Disp", "FR19CANTSUMIFARM", cwNumeric, 11)
'        Call .GridAddColumn(objMultiInfo, "Unid.Medida", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Fecha Validaci�n", "FR19FECVALIDACION", cwDate)
'        Call .GridAddColumn(objMultiInfo, "C�d.Val", "SG02COD_VAL", cwString, 6)
'        Call .GridAddColumn(objMultiInfo, "Valida", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "C�d.Sum", "FR93CODUNIMEDIDA", cwString, 5)
'        Call .GridAddColumn(objMultiInfo, "Unid.Medida Sum", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Cod Neces Val", "FR19CODNECESVAL", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Bloq", "", cwBoolean)
'        Call .GridAddColumn(objMultiInfo, "Ubicaci�n", "", cwString, 10)
'        Call .GridAddColumn(objMultiInfo, "Mod", "FR19INDMOD", cwBoolean, 1)
'
        Call .FormCreateInfo(objMasterInfo)
    
        ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
'        .CtrlGetInfo(grdAux(1).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdAux(1).Columns(4)).intKeyNo = 1
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(txttext1(0)).intKeyNo = 1

'        Call .FormChangeColor(objMultiInfo)
    
        '.CtrlGetInfo(txttext1(0)).blnInFind = True
        '.CtrlGetInfo(txttext1(1)).blnInFind = True
        '.CtrlGetInfo(txttext1(2)).blnInFind = True
        '.CtrlGetInfo(txttext1(3)).blnInFind = True
        '.CtrlGetInfo(txttext1(4)).blnInFind = True
        '.CtrlGetInfo(txttext1(5)).blnInFind = True
        '.CtrlGetInfo(txttext1(6)).blnInFind = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
        
'       .CtrlGetInfo(grdAux(1).Columns(13)).blnReadOnly = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(3)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(3)), txttext1(4), "SG02APE1")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(5)), txttext1(6), "AD02DESDPTO")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(1)), txttext1(2), "FR26DESESTADOPET")
    
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdAux(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(6), "FR73CODINTFAR")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(8), "FR73DESPRODUCTO")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(7), "FR73REFERENCIA")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(11), "FR73DOSIS")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(10), "FRH7CODFORMFAR")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(9), "FR73TAMENVASE")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(5)), grdAux(1).Columns(23), "FRH9UBICACION")
        
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdAux(1).Columns(12)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(12)), grdAux(1).Columns(15), "FR93DESUNIMEDIDA")
    
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdAux(1).Columns(17)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(17)), grdAux(1).Columns(18), "SG02APE1")
        
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdAux(1).Columns(19)), "FR93CODUNIMEDIDA_SUM", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdAux(1).Columns(19)), grdAux(1).Columns(20), "FR93DESUNIMEDIDA")
        
        
'      .CtrlGetInfo(grdAux(1).Columns(12)).blnReadOnly = True
        
        
        Call .WinRegister
        Call .WinStabilize
    End With

glstrEstilo = "CambioSec2"
glstrSeccion = "-1"

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Function FillStr(ByVal strBuffer As String)
  FillStr = strBuffer & String(128 - Len(strBuffer), vbNullChar)
End Function



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strFilter As String

'  If strFormName = "Detalle Necesidad" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      If intReport = 1 Then
        Call Imprimir("FR3361.RPT", 0)
      Else
        Call Imprimir("FR3362.RPT", 0)
      End If
    End If
    Set objPrinter = Nothing
  'End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Actualizar_Grid()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim mvntAcumulado As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim mvntSeccion
Dim contSecciones As Integer

  
Me.Enabled = False
Screen.MousePointer = vbHourglass
blnInActualizar = True

strSelect = "SELECT FR5500.FR55CODNECESUNID,FR1900.FR73CODPRODUCTO,"
strSelect = strSelect & " FR7300.FRH9UBICACION,"
strSelect = strSelect & " FR7300.FR73CODINTFAR,"
strSelect = strSelect & " FR7300.FR73REFERENCIA,"
strSelect = strSelect & " FR7300.FR73DESPRODUCTO,"
strSelect = strSelect & " FR7300.FR73DOSIS,"
strSelect = strSelect & " FR7300.FRH7CODFORMFAR,"
strSelect = strSelect & " FR7300.FR93CODUNIMEDIDA,"
strSelect = strSelect & " FR7300.FR73TAMENVASE,"
'strSelect = strSelect & " SUM(FR1900.FR19CANTNECESQUIR) CANTNECESQUIR,"
'strSelect = strSelect & " SUM(FR1900.FR19CANTSUMIFARM) CANTSUMIFARM,"

strSelect = strSelect & " FR1900.FR19CANTNECESQUIR CANTNECESQUIR,"
strSelect = strSelect & " FR1900.FR19CANTSUMIFARM CANTSUMIFARM,"
strSelect = strSelect & " FR2000.FR20CANTNECESQUIR,"

strSelect = strSelect & " FR1900.FR19INDDISPENSADO,FR5500.AD02CODDPTO,FR5500.FR26CODESTPETIC"
strSelect = strSelect & " ,FR1900.FR19CODNECESVAL,FR1900.FR20NUMLINEA,FR1900.FR19INDMOD"
strSelect = strSelect & " ,FR5500.AD41CODSECCION,AD4100.AD41DESSECCION"

strSelect = strSelect & " FROM FR1900,FR2000,FR5500,FR7300,AD4100"
strSelect = strSelect & " WHERE FR1900.FR55CODNECESUNID=FR5500.FR55CODNECESUNID"
strSelect = strSelect & " AND FR5500.FR55CODNECESUNID=FR2000.FR55CODNECESUNID"
strSelect = strSelect & " AND FR1900.FR55CODNECESUNID=FR2000.FR55CODNECESUNID"
strSelect = strSelect & " AND FR1900.FR20NUMLINEA=FR2000.FR20NUMLINEA"
strSelect = strSelect & " AND FR1900.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
strSelect = strSelect & " AND FR5500.AD02CODDPTO=AD4100.AD02CODDPTO(+)"
strSelect = strSelect & " AND FR5500.AD41CODSECCION=AD4100.AD41CODSECCION(+)"
strSelect = strSelect & " AND (FR5500.FR55INDESTUPEFACIENTE=0 OR FR5500.FR55INDESTUPEFACIENTE IS NULL)"
strSelect = strSelect & " AND (FR5500.FR55INDFM=0 OR FR5500.FR55INDFM IS NULL)"

If gstrEst = "5" Then
  strSelect = strSelect & " AND NVL(FR19INDBLOQ,0)=0"
  strSelect = strSelect & " AND NVL(FR19INDDISPENSADO,0) in (0,-1) "
  strSelect = strSelect & " AND FR5500.FR26CODESTPETIC=5"
ElseIf gstrEst = "8" Then
  strSelect = strSelect & " AND NVL(FR19INDBLOQ,0)=0"
  strSelect = strSelect & " AND NVL(FR19INDDISPENSADO,0) in (0,-1) "
  strSelect = strSelect & " AND FR5500.FR26CODESTPETIC=8"
ElseIf gstrEst = "9" Then
  strSelect = strSelect & " AND NVL(FR19INDBLOQ,0)=0"
  strSelect = strSelect & " AND NVL(FR19INDDISPENSADO,0)=0 "
  strSelect = strSelect & " AND FR5500.FR26CODESTPETIC=9"
ElseIf gstrEst = "3" Then
  strSelect = strSelect & " AND NVL(FR19INDBLOQ,0)=0"
  strSelect = strSelect & " AND NVL(FR19INDDISPENSADO,0)=0 "
  strSelect = strSelect & " AND FR5500.FR26CODESTPETIC=3"
ElseIf gstrEst = "10" Then
  strSelect = strSelect & " AND NVL(FR19INDBLOQ,0)=0"
  strSelect = strSelect & " AND NVL(FR19INDDISPENSADO,0)=0 "
  strSelect = strSelect & " AND FR5500.FR26CODESTPETIC=10"
ElseIf gstrEst = "9,10" Then
  strSelect = strSelect & " AND NVL(FR19INDBLOQ,0)=0"
  strSelect = strSelect & " AND NVL(FR19INDDISPENSADO,0)=0 "
  strSelect = strSelect & " AND FR5500.FR26CODESTPETIC in (9,10)"
End If

  strSelect = strSelect & " AND FR5500.AD02CODDPTO=?"

If gstrLlamador = "frmValPedPtaFarma.Uno" Then
  strSelect = strSelect & " AND FR5500.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
  strSelect = strSelect & " AND FR2000.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
  strSelect = strSelect & " AND FR1900.FR55CODNECESUNID=" & frmValPedPtaFarma.auxDBGrid1(0).Columns("Petici�n").Value
End If

'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
contSecciones = 0
If frmValPedPtaFarma.lvwSec.ListItems.Count > 0 Then
  If frmValPedPtaFarma.lvwSec.ListItems(1).Selected Then
  Else
    For i = 2 To frmValPedPtaFarma.lvwSec.ListItems.Count
      If frmValPedPtaFarma.lvwSec.ListItems(i).Selected = True Then
        If contSecciones = 0 Then
          contSecciones = contSecciones + 1
          strSelect = strSelect & " AND ("
        Else
          strSelect = strSelect & " OR "
        End If
        If i = 2 Then
          strSelect = strSelect & "FR5500.AD41CODSECCION IS NULL"
        Else
          strSelect = strSelect & "FR5500.AD41CODSECCION=" & frmValPedPtaFarma.lvwSec.ListItems(i).Tag
        End If
      End If
    Next i
    If contSecciones > 0 Then
      strSelect = strSelect & ") "
    End If
  End If
End If

strSelect = strSelect & " ORDER BY AD4100.AD41DESSECCION,FR7300.FRH9UBICACION,"
strSelect = strSelect & " FR7300.FR73DESPRODUCTO,FR7300.FR73CODINTFAR,FR1900.FR19CANTNECESQUIR"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
qrya(0) = gintservicio
Set rsta = qrya.OpenResultset()
'grdAux(1).Redraw = False
grdAux(1).RemoveAll
While Not rsta.EOF
  grdAux(1).AddItem rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
    rsta("FRH9UBICACION").Value & Chr(vbKeyTab) & _
    rsta("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
    rsta("FR73REFERENCIA").Value & Chr(vbKeyTab) & _
    rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & _
    rsta("FR73DOSIS").Value & Chr(vbKeyTab) & _
    rsta("FRH7CODFORMFAR").Value & Chr(vbKeyTab) & _
    rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab) & _
    rsta("FR73TAMENVASE").Value & Chr(vbKeyTab) & _
    rsta("CANTNECESQUIR").Value & Chr(vbKeyTab) & _
    "" & Chr(vbKeyTab) & _
    rsta("CANTSUMIFARM").Value & Chr(vbKeyTab) & _
    rsta("FR19INDDISPENSADO").Value & Chr(vbKeyTab) & _
    rsta("AD02CODDPTO").Value & Chr(vbKeyTab) & _
    rsta("FR26CODESTPETIC").Value & Chr(vbKeyTab) & _
    rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab) & _
    "" & Chr(vbKeyTab) & _
    rsta("FR19CODNECESVAL").Value & Chr(vbKeyTab) & _
    rsta("FR20NUMLINEA").Value & Chr(vbKeyTab) & _
    rsta("FR19INDMOD").Value & Chr(vbKeyTab) & _
    rsta("FR20CANTNECESQUIR").Value & Chr(vbKeyTab) & _
    "" & Chr(vbKeyTab) & _
    rsta("AD41CODSECCION").Value & Chr(vbKeyTab) & _
    rsta("AD41DESSECCION").Value
rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
'grdAux(1).Update
'grdAux(1).Redraw = True


  If grdAux(1).Rows > 0 Then
    mvntAcumulado = 0
    mvntAcumuladoDisp = 0
    mvntAcumuladoOri = 0
    mvntSeccion = -1
    'grdAux(1).Redraw = False
    grdAux(1).MoveFirst
    For i = 0 To grdAux(1).Rows - 1
      If grdAux(1).Columns("CantPed").Value = "" Then 'cant.ped.
        grdAux(1).Columns("CantPed").Value = 0
      End If
      If grdAux(1).Columns("CantDisp").Value = "" Then 'cant.disp.
        grdAux(1).Columns("CantDisp").Value = 0
      End If
      If grdAux(1).Columns("CantOri").Value = "" Then 'cant.disp.
        grdAux(1).Columns("CantOri").Value = 0
      End If
      mvntAcumulado = mvntAcumulado + grdAux(1).Columns("CantPed").Value
      mvntAcumuladoDisp = mvntAcumuladoDisp + grdAux(1).Columns("CantDisp").Value
      mvntAcumuladoOri = mvntAcumuladoOri + grdAux(1).Columns("CantOri").Value
      mvntProducto = grdAux(1).Columns("CodProd").Value 'Cod.Prod.
      mvntSeccion = grdAux(1).Columns("C�d.Sec.").Value
      grdAux(1).MoveNext
      If mvntProducto <> grdAux(1).Columns("CodProd").Value Or mvntSeccion <> grdAux(1).Columns("C�d.Sec.").Value Then
        grdAux(1).MovePrevious
        grdAux(1).Columns("Ac.Ped.").Value = mvntAcumulado 'Ac.Ped.
        grdAux(1).Columns("Ac.Disp.").Value = mvntAcumuladoDisp 'Ac.Disp.
        grdAux(1).Columns("AcuOri").Value = mvntAcumuladoOri
        mvntAcumulado = 0
        mvntAcumuladoDisp = 0
        mvntAcumuladoOri = 0
        grdAux(1).MoveNext
      End If
    Next i
    grdAux(1).Columns("Ac.Ped.").Value = mvntAcumulado
    grdAux(1).Columns("Ac.Disp.").Value = mvntAcumuladoDisp
    grdAux(1).Columns("AcuOri").Value = mvntAcumuladoOri
    grdAux(1).MoveFirst
    'grdAux(1).Redraw = True
  End If

  tabTab1(0).TabVisible(1) = False
  tabTab1(0).TabCaption(0) = ""
  

  Call Actualizar_Acumulados
  Call grdAux2RowLoaded
  
  blnInActualizar = False
  Screen.MousePointer = vbDefault
  Me.Enabled = True

End Sub

