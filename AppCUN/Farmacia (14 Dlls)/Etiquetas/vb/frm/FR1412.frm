VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmETFarmacia 
   Caption         =   "Etiquetas de Carros"
   ClientHeight    =   5955
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6600
   LinkTopic       =   "Form1"
   ScaleHeight     =   5955
   ScaleWidth      =   6600
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check1 
      Caption         =   "Todos los carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4200
      TabIndex        =   3
      Top             =   960
      Width           =   1935
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   735
      Left            =   4200
      TabIndex        =   2
      Top             =   3360
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Imprimir Etiquetas"
      Height          =   735
      Left            =   4200
      TabIndex        =   1
      Top             =   2280
      Width           =   1695
   End
   Begin SSDataWidgets_B.SSDBGrid SDBCaRRO 
      Height          =   5295
      Left            =   600
      TabIndex        =   0
      Top             =   360
      Width           =   3255
      _Version        =   131078
      DataMode        =   2
      Cols            =   2
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      Col.Count       =   2
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   5741
      _ExtentY        =   9340
      _StockProps     =   79
      Caption         =   "Carros"
   End
End
Attribute VB_Name = "frmETFarmacia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
Dim str$, Num$, nCopias%
Dim rdCarro As rdoResultset
Dim qyCarro As rdoQuery
Dim RD As rdoResultset
Dim QY As rdoQuery
Dim Nombre$, CARRO$, CAMA$, FECHA$
Me.MousePointer = vbHourglass
nCopias = 1
If Check1.Value = 0 Then
    str = "SELECT FRJ906J.AD15CODCAMA, FR0700.FR07DESCARRO FROM FRJ906J,FR0700 WHERE " & _
    "FR0700.FR07CODCARRO=? AND FR0700.FR07CODCARRO=FRJ906J.FR07CODCARRO"
    Set qyCarro = objApp.rdoConnect.CreateQuery("", str)
    qyCarro(0) = SDBCaRRO.Columns(0).Text
    Set rdCarro = qyCarro.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rdCarro.EOF Then
      MsgBox "Este carro no tiene pacientes", vbInformation, "Etiquetas"
    End If
    Do While Not rdCarro.EOF
        str = "SELECT AD1512J.AD08FECINICIO, GCFN06(AD1512J.AD15CODCAMA) AS " & _
        "AD15CODCAMA, CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22NUMHISTORIA " & _
        "FROM AD1512J, AD0100, CI2200 " & _
        "WHERE AD1512J.AD15CODCAMA=? AND AD0100.AD01CODASISTENCI=AD1512J.AD01CODASISTENCI AND " & _
        "CI2200.CI21CODPERSONA=AD0100.CI21CODPERSONA"
            Set QY = objApp.rdoConnect.CreateQuery("", str)
            QY(0) = rdCarro!AD15CODCAMA
            Set RD = QY.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            If RD.RowCount > 0 Then
                Num = RD!CI22NUMHISTORIA
                Nombre = RD!CI22PRIAPEL & ", " & Left(RD!CI22NOMBRE, 1) & "."
                CAMA = RD!AD15CODCAMA
                CARRO = rdCarro!FR07DESCARRO
                FECHA = Format(RD!AD08FECINICIO, "DD/MM/YYYY")
                Call ImprimirEtiqueta(Num, nCopias, Nombre, CARRO, CAMA, FECHA)
            End If
          RD.Close
          QY.Close
    rdCarro.MoveNext
    Loop
    rdCarro.Close
    qyCarro.Close
    

Else
    str = "SELECT DISTINCT FRJ906J.AD15CODCAMA, FR0700.FR07DESCARRO FROM FRJ906J,FR0700 WHERE " & _
    "FR0700.FR07CODCARRO=FRJ906J.FR07CODCARRO " & _
    "ORDER BY FR0700.FR07DESCARRO ASC"
     Set rdCarro = objApp.rdoConnect.OpenResultset(str)
    Do While Not rdCarro.EOF
        str = "SELECT AD1512J.AD08FECINICIO, GCFN06(AD1512J.AD15CODCAMA) AS " & _
        "AD15CODCAMA, CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22NUMHISTORIA " & _
        "FROM AD1512J, AD0100, CI2200 " & _
        "WHERE AD1512J.AD15CODCAMA=? AND AD0100.AD01CODASISTENCI=AD1512J.AD01CODASISTENCI AND " & _
        "CI2200.CI21CODPERSONA=AD0100.CI21CODPERSONA"
            Set QY = objApp.rdoConnect.CreateQuery("", str)
            QY(0) = rdCarro!AD15CODCAMA
            Set RD = QY.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            If RD.RowCount > 0 Then
                Num = RD!CI22NUMHISTORIA
                Nombre = RD!CI22PRIAPEL & ", " & Left(RD!CI22NOMBRE, 1) & "."
                CAMA = RD!AD15CODCAMA
                CARRO = rdCarro!FR07DESCARRO
                FECHA = Format(RD!AD08FECINICIO, "DD/MM/YYYY")
                Call ImprimirEtiqueta(Num, nCopias, Nombre, CARRO, CAMA, FECHA)
            End If
    rdCarro.MoveNext
    Loop
    rdCarro.Close
    RD.Close
    QY.Close
End If
 Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
Dim str$
Dim rdCarro As rdoResultset
Dim qyCarro As rdoQuery
Dim S$
str = "SELECT * FROM FR0700 WHERE FR07FECFINVIG IS NULL OR FR07FECFINVIG < SYSDATE"
Set rdCarro = objApp.rdoConnect.OpenResultset(str)
SDBCaRRO.Columns(0).Visible = False
SDBCaRRO.Columns(1).Width = 3000
Do While Not rdCarro.EOF
    S = rdCarro!FR07CODCARRO & ";" & rdCarro!FR07DESCARRO
    SDBCaRRO.AddItem S
    rdCarro.MoveNext
Loop
End Sub
Sub ImprimirEtiqueta(Num As String, nCopias As Integer, Nombre As String, _
CARRO As String, CAMA As String, FECHA As String)
Dim crlf$, txt As String
Dim Impr$
Dim x$
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)

' Transformar caracteres para impresi�n de caracteres del alfabeto espa�ol
  Num = transfCaracteres1(Num)
  Nombre = transfCaracteres1(Nombre)
  CARRO = transfCaracteres1(CARRO)
  txt = crlf
  txt = txt & "N" & crlf
  txt = txt & "I8,1,034" & crlf
  txt = txt & "Q240,10" & crlf
  txt = txt & "R260,0" & crlf
  txt = txt & "S2" & crlf
  txt = txt & "D8" & crlf
  txt = txt & "ZB" & crlf
  txt = txt & "B6,14,0,2,4,6,70,B," & Chr$(34) & Num & Chr$(34) & crlf
  txt = txt & "A205,20,0,2,2,2,N," & Chr$(34) & CAMA & Chr$(34) & crlf
  txt = txt & "A6,120,0,1,2,2,N," & Chr$(34) & Nombre & Chr$(34) & crlf
  txt = txt & "A6,155,0,2,1,2,N," & Chr$(34) & CARRO & Chr$(34) & crlf
  txt = txt & "A6,200,0,1,1,2,N," & Chr$(34) & FECHA & Chr$(34) & crlf
  txt = txt & "P" & nCopias & crlf  'N� de etiquetas; debe ir al final
  txt = txt & " " & crlf
  Impr = Printer.DeviceName
  If ImpresoraDefecto("PRN") = False Then Exit Sub
  DoEvents
  x = Printer.DeviceName
  Printer.Print txt
  Printer.EndDoc
 Call ImpresoraDefecto(Impr)
End Sub

Function transfCaracteres1(palabra As String) As String
' Funci�n que transforma los caracteres especiales para que se puedan imprimir
Dim I As Integer

  For I = 1 To Len(palabra)
    Select Case Mid(palabra, I, 1)
      Case "�"
        Mid(palabra, I, 1) = Chr$(160)
      Case "�"
        Mid(palabra, I, 1) = Chr$(130)
      Case "�"
        Mid(palabra, I, 1) = Chr$(161)
      Case "�"
        Mid(palabra, I, 1) = Chr$(162)
      Case "�"
        Mid(palabra, I, 1) = Chr$(163)
      Case "�"
        Mid(palabra, I, 1) = Chr$(168)
      Case "�"
        Mid(palabra, I, 1) = Chr$(164)
      Case "�"
        Mid(palabra, I, 1) = Chr$(165)
      Case "�"
        Mid(palabra, I, 1) = Chr$(129)
      Case "�"
        Mid(palabra, I, 1) = Chr$(166)
      Case "�"
        Mid(palabra, I, 1) = Chr$(167)
    End Select
  Next

transfCaracteres1 = palabra

End Function

Function ImpresoraDefecto(Nombre As String) As Integer
Dim x As Printer
Dim ctrl As Integer
  ctrl = False
  For Each x In Printers
    If InStr(UCase(x.DeviceName), UCase(Nombre)) > 0 Then
      ' La define como predeterminada del sistema.
      ctrl = True
      Set Printer = x
      ' Sale del bucle.
      Exit For
    End If
  Next
  ImpresoraDefecto = ctrl
End Function


