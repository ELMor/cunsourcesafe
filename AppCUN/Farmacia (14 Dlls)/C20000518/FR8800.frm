VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmRegDev 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Registrar Devoluciones"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   HelpContextID   =   30001
   Icon            =   "FR8800.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Buscar &Productos"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8880
      TabIndex        =   5
      Tag             =   "Buscar el producto que hay que abonar"
      Top             =   1080
      Width           =   2055
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1455
      Index           =   1
      Left            =   0
      TabIndex        =   18
      Top             =   480
      Width           =   7575
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Tag             =   "Servicio"
         Top             =   960
         Width           =   3165
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   0
         Tag             =   "N� de cama"
         Top             =   480
         Width           =   930
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Buscar"
         Height          =   495
         Left            =   6480
         TabIndex        =   4
         Tag             =   "Buscar el paciente o el servicio al que se realizar� el abono"
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   1200
         TabIndex        =   1
         Tag             =   "C�digo del paciente"
         Top             =   480
         Width           =   1425
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataSource      =   "c"
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   2760
         TabIndex        =   2
         Tag             =   "N�mero de Historia Cl�nica del paciente"
         Top             =   480
         Width           =   1635
      End
      Begin VB.Frame Frame2 
         Caption         =   "Abono al"
         ForeColor       =   &H00FF0000&
         Height          =   975
         Left            =   4560
         TabIndex        =   19
         Top             =   360
         Width           =   1815
         Begin VB.OptionButton Option2 
            Caption         =   "Paciente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   21
            Tag             =   "El abono se realizar� al Paciente"
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   20
            Tag             =   "El abono se realizar� al Servicio"
            Top             =   600
            Width           =   1335
         End
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Tag             =   "C�digo Servicio"
         Top             =   960
         Width           =   855
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1508
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cama"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   480
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d.Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   1200
         TabIndex        =   23
         Top             =   240
         Width           =   1155
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N� Historia"
         DataSource      =   "c"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   2760
         TabIndex        =   22
         Top             =   240
         Width           =   930
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Abono de Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Left            =   0
      TabIndex        =   7
      Top             =   1920
      Width           =   11580
      Begin TabDlg.SSTab SSTab1 
         Height          =   5535
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   9763
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "A paciente"
         TabPicture(0)   =   "FR8800.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFrame1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "A Servicio"
         TabPicture(1)   =   "FR8800.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraFrame1(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.Frame fraFrame1 
            Caption         =   "Abono a Paciente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   4890
            Index           =   0
            Left            =   120
            TabIndex        =   13
            Top             =   360
            Width           =   10785
            Begin TabDlg.SSTab tabTab1 
               Height          =   4455
               HelpContextID   =   90001
               Index           =   0
               Left            =   120
               TabIndex        =   14
               TabStop         =   0   'False
               Top             =   360
               Width           =   10410
               _ExtentX        =   18362
               _ExtentY        =   7858
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "FR8800.frx":0044
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(1)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(0)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(2)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(3)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(4)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(5)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "lblLabel1(7)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "lblLabel1(8)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "lblLabel1(9)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "lblLabel1(11)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "lblLabel1(12)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "lblLabel1(13)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "lblLabel1(14)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "lblLabel1(15)"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).Control(14)=   "lblLabel1(16)"
               Tab(0).Control(14).Enabled=   0   'False
               Tab(0).Control(15)=   "txtText1(0)"
               Tab(0).Control(15).Enabled=   0   'False
               Tab(0).Control(16)=   "txtText1(1)"
               Tab(0).Control(16).Enabled=   0   'False
               Tab(0).Control(17)=   "txtText1(2)"
               Tab(0).Control(17).Enabled=   0   'False
               Tab(0).Control(18)=   "txtText1(3)"
               Tab(0).Control(18).Enabled=   0   'False
               Tab(0).Control(19)=   "txtText1(4)"
               Tab(0).Control(19).Enabled=   0   'False
               Tab(0).Control(20)=   "txtText1(5)"
               Tab(0).Control(20).Enabled=   0   'False
               Tab(0).Control(21)=   "txtText1(6)"
               Tab(0).Control(21).Enabled=   0   'False
               Tab(0).Control(22)=   "txtText1(7)"
               Tab(0).Control(22).Enabled=   0   'False
               Tab(0).Control(23)=   "txtText1(8)"
               Tab(0).Control(23).Enabled=   0   'False
               Tab(0).Control(24)=   "txtText1(9)"
               Tab(0).Control(24).Enabled=   0   'False
               Tab(0).Control(25)=   "txtText1(10)"
               Tab(0).Control(25).Enabled=   0   'False
               Tab(0).Control(26)=   "txtText1(11)"
               Tab(0).Control(26).Enabled=   0   'False
               Tab(0).Control(27)=   "txtText1(12)"
               Tab(0).Control(27).Enabled=   0   'False
               Tab(0).Control(28)=   "txtText1(13)"
               Tab(0).Control(28).Enabled=   0   'False
               Tab(0).Control(29)=   "txtText1(14)"
               Tab(0).Control(29).Enabled=   0   'False
               Tab(0).ControlCount=   30
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "FR8800.frx":0060
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdDBGrid1(0)"
               Tab(1).Control(0).Enabled=   0   'False
               Tab(1).ControlCount=   1
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "FR65HORA"
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   14
                  Left            =   5520
                  TabIndex        =   51
                  Tag             =   "Hora|Hora"
                  Top             =   2760
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "FR65FECHA"
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   13
                  Left            =   4200
                  TabIndex        =   49
                  Tag             =   "Fecha|Fecha"
                  Top             =   2760
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "AD01CODASISTENCI"
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   12
                  Left            =   2880
                  TabIndex        =   47
                  Tag             =   "Asistencia|Asistencia"
                  Top             =   2760
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "AD07CODPROCESO"
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   11
                  Left            =   1560
                  TabIndex        =   44
                  Tag             =   "Proceso|Proceso"
                  Top             =   2760
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   10
                  Left            =   7560
                  TabIndex        =   6
                  Tag             =   "Cantidad|Cantidad"
                  Top             =   1080
                  Width           =   1035
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   9
                  Left            =   2760
                  TabIndex        =   41
                  Tag             =   "Producto|Producto"
                  Top             =   1080
                  Width           =   4755
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   8
                  Left            =   1440
                  TabIndex        =   39
                  Tag             =   "Referencia|Referencia"
                  Top             =   1080
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   7
                  Left            =   120
                  TabIndex        =   37
                  Tag             =   "C�d.Int|C�digo del Interno del Paciente"
                  Top             =   1080
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "FR73CODPRODUCTO"
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   6
                  Left            =   120
                  TabIndex        =   46
                  Tag             =   "C�d.Prod|C�digo del Producto"
                  Top             =   2760
                  Width           =   1275
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   5
                  Left            =   7560
                  TabIndex        =   34
                  Tag             =   "Apellido2|Segundo Apellido"
                  Top             =   480
                  Width           =   2235
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   4
                  Left            =   5280
                  TabIndex        =   32
                  Tag             =   "Apellido1|Primer Apellido"
                  Top             =   480
                  Width           =   2235
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   3
                  Left            =   3360
                  TabIndex        =   30
                  Tag             =   "Nombre|Nombre Paciente"
                  Top             =   480
                  Width           =   1875
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   2
                  Left            =   2640
                  TabIndex        =   28
                  Tag             =   "Cama|N� de cama"
                  Top             =   480
                  Width           =   675
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   1
                  Left            =   1440
                  TabIndex        =   26
                  Tag             =   "N�Historia|N�Historia"
                  Top             =   480
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "CI21CODPERSONA"
                  Height          =   330
                  HelpContextID   =   40102
                  Index           =   0
                  Left            =   120
                  TabIndex        =   15
                  Tag             =   "C�d.Paciente|C�digo del Paciente"
                  Top             =   480
                  Width           =   1275
               End
               Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
                  Height          =   4170
                  Index           =   0
                  Left            =   -74880
                  TabIndex        =   16
                  Top             =   120
                  Width           =   9780
                  _Version        =   131078
                  DataMode        =   2
                  Col.Count       =   0
                  AllowUpdate     =   0   'False
                  AllowRowSizing  =   0   'False
                  SelectTypeRow   =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   0   'False
                  _ExtentX        =   17251
                  _ExtentY        =   7355
                  _StockProps     =   79
                  ForeColor       =   0
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Hora"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   16
                  Left            =   5520
                  TabIndex        =   52
                  Top             =   2520
                  Width           =   420
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fecha"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   15
                  Left            =   4200
                  TabIndex        =   50
                  Top             =   2520
                  Width           =   540
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Asistencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   14
                  Left            =   2880
                  TabIndex        =   48
                  Top             =   2520
                  Width           =   885
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Proceso"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   13
                  Left            =   1560
                  TabIndex        =   45
                  Top             =   2520
                  Width           =   705
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Cantidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   12
                  Left            =   7560
                  TabIndex        =   43
                  Top             =   840
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Producto"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   11
                  Left            =   2760
                  TabIndex        =   42
                  Top             =   840
                  Width           =   780
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Referencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   9
                  Left            =   1440
                  TabIndex        =   40
                  Top             =   840
                  Width           =   945
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d. Interno"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   8
                  Left            =   120
                  TabIndex        =   38
                  Top             =   840
                  Width           =   1065
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d. Producto"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   7
                  Left            =   120
                  TabIndex        =   36
                  Top             =   2520
                  Width           =   1230
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Apellido 2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   5
                  Left            =   7560
                  TabIndex        =   35
                  Top             =   240
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Apellido 1"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   4
                  Left            =   5280
                  TabIndex        =   33
                  Top             =   240
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   3
                  Left            =   3360
                  TabIndex        =   31
                  Top             =   240
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Cama"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   2
                  Left            =   2640
                  TabIndex        =   29
                  Top             =   240
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "N�Historia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   0
                  Left            =   1440
                  TabIndex        =   27
                  Top             =   240
                  Width           =   870
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d. Paciente"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   1
                  Left            =   120
                  TabIndex        =   17
                  Top             =   240
                  Width           =   1215
               End
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Abono al Servicio"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   4875
            Index           =   1
            Left            =   -74880
            TabIndex        =   11
            Top             =   360
            Width           =   10785
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4545
               Index           =   1
               Left            =   120
               TabIndex        =   12
               Top             =   240
               Width           =   10575
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowColumnMoving=   2
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   18653
               _ExtentY        =   8017
               _StockProps     =   79
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   8
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRegDev"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA, COMPRAS                                          *
'* NOMBRE: FrmBusMed (FR0502.FRM)                                       *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Buscar Medicamentos                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



Private Sub cboservicio_Click()
  txtServicio.Text = cboservicio.Columns(1).Value
End Sub

Private Sub cboservicio_CloseUp()
  txtServicio.Text = cboservicio.Columns(1).Value
End Sub



Private Sub Command1_Click()
Dim strTextoABuscar As String
Dim strCodGrpTerap As String
Dim strDesvMayQue As String
Dim strProdBajoStock As String
Dim strABC_C As String
Dim strABC_CE As String
Dim strClausulaWhere As String
Dim dblTP As Double
Dim strCodIntFar As String
Dim strCodSeguridad As String
  
Command1.Enabled = False
  
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strDesvMayQue = txtBusq1(17).Text
  strProdBajoStock = chkBusq1(4).Value
  strABC_C = cboBusq1(1).Value
  strABC_CE = cboBusq1(2).Value
  strCodIntFar = txtBusq1(2).Text
  strCodSeguridad = txtBusq1(1).Text
  
  strClausulaWhere = " -1=-1 "
  
  If strTextoABuscar <> "" Then
      If Option2(0).Value = True Then
         ' Producto
         strClausulaWhere = strClausulaWhere & " AND upper(FR73DESPRODUCTO) LIKE upper('%" & strTextoABuscar & "%') "
      ElseIf Option2(1).Value = True Then
         ' Referencia
         strClausulaWhere = strClausulaWhere & " AND upper(FR73REFERENCIA) LIKE upper('%" & strTextoABuscar & "%') "
      ElseIf Option2(2).Value = True Then
         'Principio Activo
         strClausulaWhere = strClausulaWhere & " AND (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400,FR6800 WHERE FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV AND UPPER(FR68DESPRINCACTIV) LIKE UPPER('%" & strTextoABuscar & "%') )" & _
                 " OR " & _
                 "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400 WHERE FR68CODPRINCACTIV IN (SELECT FR68CODPRINCACTIV FROM FRH400 WHERE UPPER(FRH4DESSINONIMO) LIKE UPPER('%" & strTextoABuscar & "%')))" & _
                 ") "
      ElseIf Option2(3).Value = True Then
          strClausulaWhere = strClausulaWhere & " AND ( "
          strClausulaWhere = strClausulaWhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " ) "
      End If
  End If
    
  If strCodGrpTerap <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND upper(FR00CODGRPTERAP) LIKE upper('%" & strCodGrpTerap & "%') "
    Else
      strClausulaWhere = " upper(FR00CODGRPTERAP) LIKE upper('%" & strCodGrpTerap & "%') "
    End If
  End If
  
  If strProdBajoStock = 1 Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73EXISTENCIAS<=FR73STOCKMIN "
    Else
      strClausulaWhere = " FR73EXISTENCIAS<=FR73STOCKMIN "
    End If
  End If
  
  If strABC_C <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_C & "' "
    Else
      strClausulaWhere = " FR73ABCCONS='" & strABC_C & "' "
    End If
  End If

  If strABC_CE <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_CE & "' "
    Else
      strClausulaWhere = " FR73ABCCONS='" & strABC_CE & "' "
    End If
  End If
  
  If strCodIntFar <> "" Then
   strClausulaWhere = strClausulaWhere & " AND FR73CODINTFAR LIKE '%" & strCodIntFar & "%' "
  End If
  
  If strCodSeguridad <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR73CODINTFARSEG=" & strCodSeguridad & " "
  End If


  If strDesvMayQue <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73DESVIACION>" & strDesvMayQue & " "
    Else
      strClausulaWhere = " FR73DESVIACION>" & strDesvMayQue & " "
    End If
  End If
  
  If chkBusq1(0).Value = 1 Then
    'Se ha solicitado que se propongan pedidos
    If strProdBajoStock = 0 Then
      If strClausulaWhere <> "" Then
        strClausulaWhere = strClausulaWhere & " AND FR73EXISTENCIAS<=FR73STOCKMIN "
      Else
        strClausulaWhere = " FR73EXISTENCIAS<=FR73STOCKMIN "
      End If
    End If
  End If
  
  If IsNumeric(Trim(txtBusq1(0).Text)) Then
    dblTP = Trim(txtBusq1(0).Text)
    strClausulaWhere = strClausulaWhere & " AND (FR73EXISTENCIAS < (FR73STOCKMIN + (FR73STOCKMIN * " & dblTP & " / 100))) "
  End If
  
  objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
  Call objWinInfo.DataRefresh
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  
  Command1.Enabled = True
  cmdGenerar.SetFocus

End Sub

Private Sub Form_Activate()
  Dim stra As String
  Dim qrya As rdoQuery
  Dim rsta As rdoResultset
  
  cboservicio.RemoveAll
  stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
            "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
            "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL))) ORDER BY AD02DESDPTO"
  Set qrya = objApp.rdoConnect.CreateQuery("", stra)
  Set rsta = qrya.OpenResultset()
  While (Not rsta.EOF)
    Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
    rsta.MoveNext
  Wend
  qrya.Close
  Set qrya = Nothing
  Set rsta = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FR6500"
    .blnAskPrimary = False
    Call .FormAddOrderField("FR65FECHA", cwDescending)
    
    strKey = .strDataBase & .strTable
 
  End With
   
  With objMultiInfo
    .strName = "Servicio"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "FRK100"
    Call .FormAddOrderField("FRK1FECMOV", cwDescending)
    strKey = .strDataBase & .strTable
    
  End With
 
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
    Call .GridAddColumn(objMultiInfo, "CodMov", "FRK1CODMOV", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Fecha", "FRK1FECMOV", cwDate, 10)
    Call .GridAddColumn(objMultiInfo, "CodDpto", "AD02CODDPTO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Fecha", "FR55FECPETICION", cwDate)
    Call .GridAddColumn(objMultiInfo, "CodProd", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "CodInt", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Envase", "FRK1TAMENVASE", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Cant", "FRK1CANTIDAD", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "MOD", "FRK1INDMOD", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "FAC", "FRK1INDFAC", cwNumeric, 1)
        
    Call .FormCreateInfo(objDetailInfo)
    Call .FormCreateInfo(objMultiInfo)
    Call .FormChangeColor(objMultiInfo)

    .CtrlGetInfo(txtText1(6)).blnInGrid = False 'C�digo Producto
                  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR79CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(8), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(9), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR79CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "AD07CODPROCESO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "AD01CODASISTENCI")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "DESCAMA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(1), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(3), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(4), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(5), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodDpto")), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodDpto")), grdDBGrid1(1).Columns("Servicio"), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodProd")), "", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("CodProd")), grdDBGrid1(1).Columns("Servicio"), "AD02DESDPTO")
    
    Call .WinRegister
    Call .WinStabilize
  End With




End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Medicamentos" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = ""
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  Dim dblPedir As Double
  Dim blnTamDef As Boolean
  
If grdDBGrid1(0).Rows > 0 Then
  grdDBGrid1(0).Columns(1).Caption = "C�digo" 'Cod.Int
  grdDBGrid1(0).Columns(1).Width = 740 'Cod.Int
  grdDBGrid1(0).Columns(2).Visible = False 'C.S.
  grdDBGrid1(0).Columns(3).Width = 3000 'Descripcion Mat.Sanitario
  'grdDBGrid1(0).Columns(4).Width = 1095 'F.F.
  grdDBGrid1(0).Columns(5).Width = 650 'Dosis
  'grdDBGrid1(0).Columns(6).Width = 1095 'U.M.
  grdDBGrid1(0).Columns(7).Width = 650 'Vol.
  grdDBGrid1(0).Columns(8).Width = 1095 'Referencia
  
  grdDBGrid1(0).Columns(9).Caption = "Exist." 'Existencias
  grdDBGrid1(0).Columns(9).Width = 510 'Existencias
  grdDBGrid1(0).Columns(10).Caption = "Stock Min" 'Stock Min.
  grdDBGrid1(0).Columns(10).Width = 900 'Stock Min.
  grdDBGrid1(0).Columns(11).Caption = "Pdte.Recibir" 'Pdte.Recibir
  grdDBGrid1(0).Columns(11).Width = 1019 'Pdte.Recibir
  grdDBGrid1(0).Columns(12).Caption = "Cons.Mens." 'Cons.Mensual
  grdDBGrid1(0).Columns(12).Width = 976 'Cons.Mensual
  grdDBGrid1(0).Columns(13).Caption = "Pdte.Bonif." 'Pdte.Bonificar
  grdDBGrid1(0).Columns(13).Width = 900 'Pdte.Bonificar
  grdDBGrid1(0).Columns(14).Caption = "P.Ult.Ent." 'P.Ult.Ent.
  grdDBGrid1(0).Columns(14).Width = 1200 'P.Ult.Ent.
End If
  blnTamDef = False
  If chkCheck1(0).Value = 1 Then
    'Gesti�n por consumos
    If IsNumeric(txtText1(31).Text) And IsNumeric(txtText1(37).Text) Then
      If txtText1(31).Text > 0 Then
        dblPedir = (txtText1(37).Text \ txtText1(31).Text) 'desviaci�n/tama�o envase
        If dblPedir < 0 Then
          dblPedir = dblPedir * (-1)
        End If
        If IsNumeric(txtText1(20).Text) And IsNumeric(txtText1(18).Text) Then
          'Si las existencias y el stock m�nimo son n�meros
          If txtText1(18).Text = 0 Then
            txtText1(36).Text = 0
            blnTamDef = True
          Else
            txtText1(36).Text = dblPedir + 1
            blnTamDef = True
          End If
        Else
          txtText1(36).Text = dblPedir + 1
          blnTamDef = True
        End If
      End If
    End If
  Else
    If chkCheck1(1).Value = 1 Then
      'Transito
      txtText1(36).Text = 1
      blnTamDef = True
    Else
      If (chkCheck1(2).Value = 1) Or (chkCheck1(3).Value = 1) Then
        'Programado o de gesti�n fija
        If IsNumeric(txtText1(23).Text) Then
          If IsNumeric(txtText1(13).Text) Then
            'Tama�o del pedido por el consumo diario
            txtText1(36).Text = Fix(txtText1(23).Text * txtText1(13).Text)
            blnTamDef = True
          Else
            txtText1(36).Text = Fix(txtText1(23).Text)
            blnTamDef = True
          End If
        Else
          txtText1(36).Text = 0
          blnTamDef = True
        End If
      End If
    End If
  End If
  If blnTamDef = False Then
    txtText1(36).Text = 0
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



