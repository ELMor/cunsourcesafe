Attribute VB_Name = "DEF001"
Option Explicit

Public objsecurity As clsCWSecurity
Public objmouse As clsCWMouse

Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object

Public gintProdBuscado
Public gintFR501Prov
Public gintCodProveedor    ' C�digo del Proveedor
Public gintFR67CODPETOFERT ' C�digo Petici�n Oferta
Public gintCodReclamacion  ' C�digo Reclamaci�n
Public gstrMoneda As String ' Tipo de Moneda
Public gstrPrecioOfertado As String 'Precio Ofertado
Public gintPedBuscado
Public gintFacBuscado
Public gintF As Integer
Public gstrLF(250, 2) As String
Public gintprodbuscado1()
Public gintprodtotal As Integer
Public gintProveedor(2)
Public gstrLlamador As String
Public gstrLlamadorOferta As String
Public gintLlamadorReclamar As String
Public gstrCodProv As String
Public gintPedidoRecibido
' C�digos de Albaranes
Public gintCodsAlbs As Integer
Public gstrCodsAlbs(250) As String
Public gstrCodAlb As String
Public gdblCodProd As Double
Public gdblCant As Double 'Cantidad propuesta para ser pedisa
Public gaList(250, 10) As Double 'Lista en la que se almacenan los productos para hacer el pedido
                             'En el primer campo se guarda el c�d. del producto y el segundo si est� eliminado de la lista: -1 si est� seleccionado, 0 si se ha eliminado de la lista
                             'El tercero tiene el precio, el cuarto la cantidad a pedir, el quinto &descuento,
                             'El sexto es %bonificaci�n, el septimo es %iva y el octavo UE
Public gintCont As Integer 'Sirve para llevar la cuenta de los elementos de la lista gaList
Public gaListAlb(250, 3) As Double 'Lista en la que se almacenan los productos del pedido con
                                   'el que se va a construir el albar�n. 1 es el c�digo de pedido,
                                   '2 es el c�digo de l�nea del detalle del pedido y 3 es el estado
                                   '(-1 el producto se ha de meter en el albar�n, 0 Producto quitado)
Public gintContAlb As Integer 'Sirve para llevar la cuenta de los elementos de la lsita gaListAlb
Public gstrLlamadorFac As String 'Almacena quien llama a la FR0527, "" Si se le llama desde el men� y "CA" si se le llama desde consultar albaranes
Public gstrCodPed As String
Public gcurCantEntre As Currency
Public gcurEnv As Currency
Public gcurUniRec As Currency
Public gstrNumAlb As String
Public gintNumLin As Integer
Public gblnModFac As Boolean
Public gstrNumFac As String
Public gstrCP As String
Public gaListPrd(250, 2) As Double 'Contiene la lista de producto seleccinados por el usuario en el buscador FR0502
                                   '1 Es el c�digo del producto; 2 Es la cantidad a pedir
'Public gintCont As Integer 'Nos indica el n� de elementos de la lista gaListPrd
Public Const gstrversion = "20000518.2"

Sub Main()
End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub



'Funci�n que devuelve el separador decimal que se corresponde con
'el de la configuraci�n regional de la m�quina.
Public Function SeparadorDec() As String

If Format("12,34", "00.00") = "12,34" Then
  SeparadorDec = ","
Else
  SeparadorDec = "."
End If

End Function

Public Function intCuentaCaracteres(ByVal strCaracter As String, ByVal strCadena As String) As Integer
  Dim intCuenta As Integer
  Dim intPos As Variant
  Dim strSubCadena As String
  Dim Fin As Boolean
  
  Fin = False
  intCuenta = 0
  intPos = 0
  strSubCadena = strCadena
  While Not Fin
    intPos = InStr(strSubCadena, strCaracter)
    If intPos = 0 Then
      Fin = True
    Else
      intCuenta = intCuenta + 1
      strSubCadena = Right(strSubCadena, Len(strSubCadena) - intPos)
    End If
  Wend
  intCuentaCaracteres = intCuenta
End Function

'************************************************
'* strMonedaActual                              *
'* Retorna la moneda actual por defecto,        *
'************************************************
Public Function strMonedaActual()
   Dim sqlsrt As String
   Dim rsta As rdoResultset
   
   ' Par�metro 19 de la tabla de
   ' par�metros generales
   sqlsrt = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = 19"
   Set rsta = objApp.rdoConnect.OpenResultset(sqlsrt)
   strMonedaActual = rsta.rdoColumns(0).Value
   rsta.Close
   Set rsta = Nothing

End Function

Public Function convFecha(ByVal strfecha As String) As String
  'JMRL: Esta funci�n toma como entrada una fecha (strFecha) y comprueba si el as�o est�a escrito con
  '2 0 4 d�gitos. si est� escrito con dos d�gitos le a�ade los anterio.
  Dim strAnyo As String 'a�o de la fecha
  Dim strSlas As String 'slas \ de la fecha
  Dim intAnyo As Integer 'a�o de la fecha en formato num�rico
  Dim strResto As String 'La fecha menos el a�o
  
  If Len(Trim(strfecha)) > 0 Then
    'Se guarda el �ltimo d�gito
    strAnyo = Right(strfecha, 2)
    strResto = Left(strfecha, Len(strfecha) - 2)
    strSlas = Right(strResto, 1)
    If Not IsNumeric(strSlas) Then
      If IsNumeric(strAnyo) Then
        intAnyo = strAnyo
        If intAnyo < 70 Then
          'Hay que a�adir un 20
          strAnyo = "20" & strAnyo
        Else
          'Hay que a�adir un 19
          strAnyo = "19" & strAnyo
        End If
        convFecha = strResto & strAnyo
      Else
        'Se ha producido un error 1
        convFecha = "Error"
      End If
    Else
      'El a�o est� compuesto por m�s de dos d�gitos
      convFecha = strfecha
    End If
  Else
    'Se ha producido un error
    convFecha = "Error"
  End If
  
End Function
