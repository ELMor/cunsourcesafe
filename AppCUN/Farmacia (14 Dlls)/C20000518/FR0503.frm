VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmBusMat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Buscar Material Sanitario"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0503.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   82
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1575
      Left            =   0
      TabIndex        =   83
      Top             =   480
      Width           =   11655
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   7680
         TabIndex        =   10
         Tag             =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   345
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   0
         Left            =   6480
         TabIndex        =   9
         Tag             =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   1185
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   4320
         TabIndex        =   8
         Tag             =   "Desviaci�n Mayor que"
         Top             =   1080
         Width           =   1905
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Productos Bajo Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   8400
         TabIndex        =   11
         Top             =   1080
         Width           =   2265
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   1800
         TabIndex        =   7
         Tag             =   "C�d. Grp. Terap�utico"
         Top             =   1080
         Width           =   1905
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Buscar"
         Default         =   -1  'True
         Height          =   495
         Left            =   10680
         TabIndex        =   12
         Top             =   360
         Width           =   855
      End
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   5280
         TabIndex        =   85
         Top             =   240
         Width           =   5295
         Begin VB.OptionButton Option2 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3840
            TabIndex        =   4
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Grp.Tera."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2640
            TabIndex        =   3
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   1
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   1320
            TabIndex        =   2
            Top             =   240
            Width           =   1335
         End
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   0
         Tag             =   "Texto a buscar"
         Top             =   480
         Width           =   5130
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0503.frx":000C
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   5
         Tag             =   "ABC(C)"
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0503.frx":001E
         Height          =   330
         Index           =   2
         Left            =   960
         TabIndex        =   6
         Tag             =   "ABC(CE)"
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Interno"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   6480
         TabIndex        =   139
         Top             =   840
         Width           =   1005
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "(Q...)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   3720
         TabIndex        =   106
         Top             =   1200
         Width           =   450
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desviaci�n Mayor que"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   4320
         TabIndex        =   89
         Top             =   840
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Grp. Terap�utico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   1800
         TabIndex        =   88
         Top             =   840
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(CE)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   960
         TabIndex        =   87
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(C)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   86
         Top             =   840
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   84
         Top             =   240
         Width           =   1290
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Material Sanitario"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Index           =   0
      Left            =   0
      TabIndex        =   80
      Top             =   2040
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   5340
         Index           =   0
         Left            =   240
         TabIndex        =   90
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   9419
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0503.frx":0030
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(27)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(26)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(25)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(24)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(23)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(22)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(21)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(20)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(18)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(17)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(16)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(4)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(57)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(56)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(55)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(14)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(15)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(2)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "lblLabel1(13)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "lblLabel1(58)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "lblLabel1(59)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "SSTab1"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(27)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(26)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(25)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(23)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(22)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(21)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(20)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(19)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(18)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(16)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(5)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(14)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(13)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(12)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "chkCheck1(0)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(6)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(0)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(17)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(9)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(10)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtText1(4)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(7)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).Control(47)=   "txtText1(24)"
         Tab(0).Control(47).Enabled=   0   'False
         Tab(0).Control(48)=   "txtText1(8)"
         Tab(0).Control(48).Enabled=   0   'False
         Tab(0).Control(49)=   "txtText1(11)"
         Tab(0).Control(49).Enabled=   0   'False
         Tab(0).Control(50)=   "txtText1(30)"
         Tab(0).Control(50).Enabled=   0   'False
         Tab(0).Control(51)=   "txtText1(39)"
         Tab(0).Control(51).Enabled=   0   'False
         Tab(0).ControlCount=   52
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0503.frx":004C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(1)=   "Command2(1)"
         Tab(1).ControlCount=   2
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTOPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   39
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   152
            TabStop         =   0   'False
            Tag             =   "Nom.Comercial|Nom.Comercial"
            Top             =   3960
            Visible         =   0   'False
            Width           =   4140
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECIONETCOMPRA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   30
            Left            =   8880
            TabIndex        =   38
            TabStop         =   0   'False
            Tag             =   "Precio Neto"
            Top             =   2160
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR88CODTIPIVA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8400
            TabIndex        =   37
            TabStop         =   0   'False
            Tag             =   "IVA"
            Top             =   2160
            Width           =   330
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECBASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   5640
            TabIndex        =   35
            TabStop         =   0   'False
            Tag             =   "Precio Base"
            Top             =   2160
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PREULTENT"
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   5640
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Precio de la �ltima entrada"
            Top             =   960
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PTEBONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   1440
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Pendiente Bonificar"
            Top             =   1560
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CANTPEND"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   1440
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Cantidad pendiente de recibir"
            Top             =   960
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8640
            Locked          =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Unidades Envase"
            Top             =   360
            Width           =   900
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODINTFARSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   360
            Width           =   300
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODINTFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   17
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "C�d.Interno"
            Top             =   360
            Width           =   720
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Traer Mat. Sanitario"
            Height          =   375
            Index           =   1
            Left            =   -70560
            TabIndex        =   140
            Top             =   4800
            Width           =   1935
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   0
            Left            =   3240
            TabIndex        =   13
            Tag             =   "C�d.Producto"
            Top             =   0
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73REFERENCIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   6600
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   360
            Width           =   1980
         End
         Begin VB.CheckBox chkCheck1 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Cons.Reg."
            DataField       =   "FR73INDCONSREGULAR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   4200
            TabIndex        =   34
            TabStop         =   0   'False
            Tag             =   "Consumo Regular"
            Top             =   2160
            Width           =   1305
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   7320
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Descuento"
            Top             =   960
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSDIARIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   2760
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "Consumo Diario"
            Top             =   960
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSMES"
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   2760
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "Consumo mensual"
            Top             =   1560
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTOPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   1320
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Material Sanitario"
            Top             =   360
            Width           =   5220
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSANUAL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   16
            Left            =   2760
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "Consumo Anual"
            Top             =   2160
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMIN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   18
            Left            =   120
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Stock m�nimo"
            Top             =   1560
            Width           =   1065
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMAX"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   120
            TabIndex        =   32
            TabStop         =   0   'False
            Tag             =   "Stock m�ximo"
            Top             =   2160
            Width           =   1065
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73EXISTENCIAS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   20
            Left            =   120
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Existencias"
            Top             =   960
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73RAPPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   7320
            TabIndex        =   36
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Rappel"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DIASSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   4200
            TabIndex        =   27
            TabStop         =   0   'False
            Tag             =   "D�as de Seguridad"
            Top             =   960
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73TAMPEDI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   4200
            TabIndex        =   29
            TabStop         =   0   'False
            Tag             =   "Tama�o del pedido en d�as"
            Top             =   1560
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECOFR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   5640
            TabIndex        =   30
            TabStop         =   0   'False
            Tag             =   "Precio Oferta de Compra"
            Top             =   1560
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73BONIFICACION"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   7320
            TabIndex        =   31
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Bonificaci�n"
            Top             =   1560
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   9600
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "Moneda"
            Top             =   360
            Width           =   930
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4545
            Index           =   0
            Left            =   -74880
            TabIndex        =   79
            TabStop         =   0   'False
            Top             =   90
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   8017
            _StockProps     =   79
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2535
            Left            =   120
            TabIndex        =   107
            Top             =   2640
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   4471
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Proveedores"
            TabPicture(0)   =   "FR0503.frx":0068
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "txtText1(1)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Frame3"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "txtText1(2)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "txtText1(3)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).ControlCount=   4
            TabCaption(1)   =   "Informaci�n Adicional"
            TabPicture(1)   =   "FR0503.frx":0084
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "SSTab2"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_A"
               Height          =   330
               HelpContextID   =   30104
               Index           =   3
               Left            =   840
               TabIndex        =   39
               TabStop         =   0   'False
               Tag             =   "C�d.Prov.A"
               Top             =   600
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_B"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   840
               TabIndex        =   41
               TabStop         =   0   'False
               Tag             =   "C�d.Prov.B"
               Top             =   960
               Width           =   930
            End
            Begin VB.Frame Frame3 
               Caption         =   "Producto de"
               ForeColor       =   &H00FF0000&
               Height          =   2055
               Left            =   240
               TabIndex        =   108
               Top             =   360
               Width           =   10215
               Begin VB.CommandButton Command2 
                  Caption         =   "&Traer Mat. Sanitario"
                  Height          =   375
                  Index           =   0
                  Left            =   3600
                  TabIndex        =   150
                  Top             =   1560
                  Width           =   1935
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   28
                  Left            =   1560
                  TabIndex        =   42
                  TabStop         =   0   'False
                  Tag             =   "Proveedor B"
                  Top             =   600
                  Width           =   8490
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   15
                  Left            =   1560
                  TabIndex        =   40
                  TabStop         =   0   'False
                  Tag             =   "Proveedor A"
                  Top             =   240
                  Width           =   8490
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   1
                  Left            =   360
                  TabIndex        =   46
                  Top             =   600
                  Width           =   255
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   0
                  Left            =   360
                  TabIndex        =   45
                  Top             =   240
                  Value           =   -1  'True
                  Width           =   255
               End
               Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                  Bindings        =   "FR0503.frx":00A0
                  Height          =   330
                  Index           =   0
                  Left            =   600
                  TabIndex        =   78
                  TabStop         =   0   'False
                  Top             =   1560
                  Visible         =   0   'False
                  Width           =   2130
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Row.Count       =   3
                  Row(0)          =   "Directo a Proveedor"
                  Row(1)          =   "Delegado 1"
                  Row(2)          =   "Delegado 2"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   3731
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  _ExtentX        =   3757
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   29
                  Left            =   3360
                  TabIndex        =   44
                  TabStop         =   0   'False
                  Tag             =   "Proveedor C"
                  Top             =   960
                  Visible         =   0   'False
                  Width           =   6690
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   2
                  Left            =   3000
                  TabIndex        =   47
                  Top             =   960
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre Comercial:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   67
                  Left            =   360
                  TabIndex        =   151
                  Top             =   1080
                  Visible         =   0   'False
                  Width           =   1605
               End
            End
            Begin TabDlg.SSTab SSTab2 
               Height          =   2175
               Left            =   -74880
               TabIndex        =   109
               Top             =   360
               Width           =   10335
               _ExtentX        =   18230
               _ExtentY        =   3836
               _Version        =   327681
               Style           =   1
               Tabs            =   4
               TabsPerRow      =   4
               TabHeight       =   520
               TabCaption(0)   =   "Directo a Proveedor"
               TabPicture(0)   =   "FR0503.frx":00B2
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(34)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(33)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(32)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(31)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(30)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(29)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "lblLabel1(7)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "lblLabel1(3)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "lblLabel1(1)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtProv1(38)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtProv1(37)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtProv1(36)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtProv1(35)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "txtProv1(34)"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).Control(14)=   "txtProv1(33)"
               Tab(0).Control(14).Enabled=   0   'False
               Tab(0).Control(15)=   "txtProv1(32)"
               Tab(0).Control(15).Enabled=   0   'False
               Tab(0).Control(16)=   "txtProv1(31)"
               Tab(0).Control(16).Enabled=   0   'False
               Tab(0).Control(17)=   "txtProv1(30)"
               Tab(0).Control(17).Enabled=   0   'False
               Tab(0).ControlCount=   18
               TabCaption(1)   =   "Delegado 1"
               TabPicture(1)   =   "FR0503.frx":00CE
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "lblLabel1(53)"
               Tab(1).Control(1)=   "lblLabel1(43)"
               Tab(1).Control(2)=   "lblLabel1(42)"
               Tab(1).Control(3)=   "lblLabel1(41)"
               Tab(1).Control(4)=   "lblLabel1(40)"
               Tab(1).Control(5)=   "lblLabel1(39)"
               Tab(1).Control(6)=   "lblLabel1(38)"
               Tab(1).Control(7)=   "lblLabel1(37)"
               Tab(1).Control(8)=   "lblLabel1(36)"
               Tab(1).Control(9)=   "lblLabel1(35)"
               Tab(1).Control(10)=   "txtProv1(57)"
               Tab(1).Control(10).Enabled=   0   'False
               Tab(1).Control(11)=   "txtProv1(47)"
               Tab(1).Control(11).Enabled=   0   'False
               Tab(1).Control(12)=   "txtProv1(46)"
               Tab(1).Control(12).Enabled=   0   'False
               Tab(1).Control(13)=   "txtProv1(45)"
               Tab(1).Control(13).Enabled=   0   'False
               Tab(1).Control(14)=   "txtProv1(44)"
               Tab(1).Control(14).Enabled=   0   'False
               Tab(1).Control(15)=   "txtProv1(43)"
               Tab(1).Control(15).Enabled=   0   'False
               Tab(1).Control(16)=   "txtProv1(42)"
               Tab(1).Control(16).Enabled=   0   'False
               Tab(1).Control(17)=   "txtProv1(41)"
               Tab(1).Control(17).Enabled=   0   'False
               Tab(1).Control(18)=   "txtProv1(40)"
               Tab(1).Control(18).Enabled=   0   'False
               Tab(1).Control(19)=   "txtProv1(39)"
               Tab(1).Control(19).Enabled=   0   'False
               Tab(1).ControlCount=   20
               TabCaption(2)   =   "Delegado 2"
               TabPicture(2)   =   "FR0503.frx":00EA
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "lblLabel1(44)"
               Tab(2).Control(1)=   "lblLabel1(45)"
               Tab(2).Control(2)=   "lblLabel1(46)"
               Tab(2).Control(3)=   "lblLabel1(47)"
               Tab(2).Control(4)=   "lblLabel1(48)"
               Tab(2).Control(5)=   "lblLabel1(49)"
               Tab(2).Control(6)=   "lblLabel1(50)"
               Tab(2).Control(7)=   "lblLabel1(51)"
               Tab(2).Control(8)=   "lblLabel1(52)"
               Tab(2).Control(9)=   "lblLabel1(54)"
               Tab(2).Control(10)=   "txtProv1(48)"
               Tab(2).Control(10).Enabled=   0   'False
               Tab(2).Control(11)=   "txtProv1(49)"
               Tab(2).Control(11).Enabled=   0   'False
               Tab(2).Control(12)=   "txtProv1(50)"
               Tab(2).Control(12).Enabled=   0   'False
               Tab(2).Control(13)=   "txtProv1(51)"
               Tab(2).Control(13).Enabled=   0   'False
               Tab(2).Control(14)=   "txtProv1(52)"
               Tab(2).Control(14).Enabled=   0   'False
               Tab(2).Control(15)=   "txtProv1(53)"
               Tab(2).Control(15).Enabled=   0   'False
               Tab(2).Control(16)=   "txtProv1(55)"
               Tab(2).Control(16).Enabled=   0   'False
               Tab(2).Control(17)=   "txtProv1(56)"
               Tab(2).Control(17).Enabled=   0   'False
               Tab(2).Control(18)=   "txtProv1(58)"
               Tab(2).Control(18).Enabled=   0   'False
               Tab(2).Control(19)=   "txtProv1(54)"
               Tab(2).Control(19).Enabled=   0   'False
               Tab(2).ControlCount=   20
               TabCaption(3)   =   "Comentarios"
               TabPicture(3)   =   "FR0503.frx":0106
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "txtProv1(59)"
               Tab(3).Control(0).Enabled=   0   'False
               Tab(3).ControlCount=   1
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   54
                  Left            =   -68280
                  TabIndex        =   71
                  TabStop         =   0   'False
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   1845
               End
               Begin VB.TextBox txtProv1 
                  Height          =   1530
                  Index           =   59
                  Left            =   -74880
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   77
                  TabStop         =   0   'False
                  Tag             =   "Comentarios"
                  Top             =   480
                  Width           =   10080
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   58
                  Left            =   -74880
                  TabIndex        =   69
                  TabStop         =   0   'False
                  Tag             =   "Distrito1"
                  Top             =   1140
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   56
                  Left            =   -70200
                  TabIndex        =   68
                  TabStop         =   0   'False
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   5295
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   55
                  Left            =   -72360
                  TabIndex        =   70
                  TabStop         =   0   'False
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   53
                  Left            =   -66360
                  TabIndex        =   72
                  TabStop         =   0   'False
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   52
                  Left            =   -72360
                  TabIndex        =   74
                  TabStop         =   0   'False
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   51
                  Left            =   -68280
                  TabIndex        =   76
                  TabStop         =   0   'False
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3360
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   50
                  Left            =   -74880
                  TabIndex        =   73
                  TabStop         =   0   'False
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   49
                  Left            =   -70320
                  TabIndex        =   75
                  TabStop         =   0   'False
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   48
                  Left            =   -74880
                  TabIndex        =   67
                  TabStop         =   0   'False
                  Tag             =   "Nombre2"
                  Top             =   540
                  Width           =   4605
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   30
                  Left            =   6240
                  TabIndex        =   49
                  TabStop         =   0   'False
                  Tag             =   "Distrito1"
                  Top             =   540
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   31
                  Left            =   120
                  TabIndex        =   48
                  TabStop         =   0   'False
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   6015
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   32
                  Left            =   120
                  TabIndex        =   50
                  TabStop         =   0   'False
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   33
                  Left            =   4200
                  TabIndex        =   51
                  TabStop         =   0   'False
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   2800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   34
                  Left            =   7080
                  TabIndex        =   52
                  TabStop         =   0   'False
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   2780
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   35
                  Left            =   2280
                  TabIndex        =   54
                  TabStop         =   0   'False
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   36
                  Left            =   6600
                  TabIndex        =   56
                  TabStop         =   0   'False
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3240
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   37
                  Left            =   120
                  TabIndex        =   53
                  TabStop         =   0   'False
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   38
                  Left            =   4440
                  TabIndex        =   55
                  TabStop         =   0   'False
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   39
                  Left            =   -74880
                  TabIndex        =   59
                  TabStop         =   0   'False
                  Tag             =   "Distrito1"
                  Top             =   1140
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   40
                  Left            =   -70200
                  TabIndex        =   58
                  TabStop         =   0   'False
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   5295
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   41
                  Left            =   -72360
                  TabIndex        =   60
                  TabStop         =   0   'False
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   42
                  Left            =   -68280
                  TabIndex        =   61
                  TabStop         =   0   'False
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   1845
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   43
                  Left            =   -66360
                  TabIndex        =   62
                  TabStop         =   0   'False
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   44
                  Left            =   -72360
                  TabIndex        =   64
                  TabStop         =   0   'False
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   45
                  Left            =   -68280
                  TabIndex        =   66
                  TabStop         =   0   'False
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3360
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   46
                  Left            =   -74880
                  TabIndex        =   63
                  TabStop         =   0   'False
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   47
                  Left            =   -70320
                  TabIndex        =   65
                  TabStop         =   0   'False
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   57
                  Left            =   -74880
                  TabIndex        =   57
                  TabStop         =   0   'False
                  Tag             =   "Nombre2"
                  Top             =   540
                  Width           =   4605
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   54
                  Left            =   -70200
                  TabIndex        =   138
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   52
                  Left            =   -74880
                  TabIndex        =   137
                  Top             =   960
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   -72360
                  TabIndex        =   136
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   -68280
                  TabIndex        =   135
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   -66360
                  TabIndex        =   134
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   48
                  Left            =   -72360
                  TabIndex        =   133
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   47
                  Left            =   -68280
                  TabIndex        =   132
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   46
                  Left            =   -70320
                  TabIndex        =   131
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   45
                  Left            =   -74880
                  TabIndex        =   130
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   44
                  Left            =   -74880
                  TabIndex        =   129
                  Top             =   360
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   1
                  Left            =   120
                  TabIndex        =   128
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   3
                  Left            =   6240
                  TabIndex        =   127
                  Top             =   360
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   7
                  Left            =   120
                  TabIndex        =   126
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   29
                  Left            =   4200
                  TabIndex        =   125
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   30
                  Left            =   7080
                  TabIndex        =   124
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   2280
                  TabIndex        =   123
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   32
                  Left            =   6600
                  TabIndex        =   122
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   33
                  Left            =   4440
                  TabIndex        =   121
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   34
                  Left            =   120
                  TabIndex        =   120
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   35
                  Left            =   -70200
                  TabIndex        =   119
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   36
                  Left            =   -74880
                  TabIndex        =   118
                  Top             =   960
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   37
                  Left            =   -72360
                  TabIndex        =   117
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   38
                  Left            =   -68280
                  TabIndex        =   116
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   39
                  Left            =   -66360
                  TabIndex        =   115
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   40
                  Left            =   -72360
                  TabIndex        =   114
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   41
                  Left            =   -68280
                  TabIndex        =   113
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   42
                  Left            =   -70320
                  TabIndex        =   112
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   43
                  Left            =   -74880
                  TabIndex        =   111
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   53
                  Left            =   -74880
                  TabIndex        =   110
                  Top             =   360
                  Width           =   660
               End
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_C"
               Height          =   330
               HelpContextID   =   30104
               Index           =   1
               Left            =   840
               TabIndex        =   43
               TabStop         =   0   'False
               Tag             =   "C�d.Prov.C"
               Top             =   1320
               Visible         =   0   'False
               Width           =   930
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   59
            Left            =   8880
            TabIndex        =   149
            Top             =   1920
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   58
            Left            =   8400
            TabIndex        =   148
            Top             =   1920
            Width           =   315
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Base"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   5640
            TabIndex        =   147
            Top             =   1920
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Prec.Ult.Entrada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   5640
            TabIndex        =   146
            Top             =   720
            Width           =   1425
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pdte.Bonificar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   1440
            TabIndex        =   145
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pdte.Recibir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   1440
            TabIndex        =   144
            Top             =   720
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.E."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   55
            Left            =   8640
            TabIndex        =   143
            Top             =   120
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   840
            TabIndex        =   142
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   120
            TabIndex        =   141
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   9600
            TabIndex        =   105
            Top             =   120
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Material Sanitario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1320
            TabIndex        =   104
            Top             =   120
            Width           =   1500
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   6600
            TabIndex        =   103
            Top             =   120
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Diario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   2760
            TabIndex        =   102
            Top             =   720
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Mensual"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   2760
            TabIndex        =   101
            Top             =   1320
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Anual"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   2760
            TabIndex        =   100
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�nimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   120
            TabIndex        =   99
            Top             =   1320
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�ximo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   120
            TabIndex        =   98
            Top             =   1920
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Existencias"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   120
            TabIndex        =   97
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Desc."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   7320
            TabIndex        =   96
            Top             =   720
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dias Seguridad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   4200
            TabIndex        =   95
            Top             =   720
            Width           =   1305
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tama�o Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   4200
            TabIndex        =   94
            Top             =   1320
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Rappel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   26
            Left            =   7320
            TabIndex        =   93
            Top             =   1920
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   27
            Left            =   5640
            TabIndex        =   92
            Top             =   1320
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Bonif."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   7320
            TabIndex        =   91
            Top             =   1320
            Width           =   705
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   81
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmBusMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA, COMPRAS                                          *
'* NOMBRE: FrmBusMat (FR0503.FRM)                                       *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Buscar Material Sanitario                               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Command1_Click()
Dim strTextoABuscar As String
Dim strCodGrpTerap As String
Dim strDesvMayQue As String
Dim strProdBajoStock As String
Dim strABC_C As String
Dim strABC_CE As String
Dim strCodIntFar As String
Dim strCodSeguridad As String
Dim strClausulaWhere As String
   
   Screen.MousePointer = vbHourglass
   Command1.Enabled = False
   
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strDesvMayQue = txtBusq1(17).Text
  strProdBajoStock = chkBusq1(4).Value
  strABC_C = cboBusq1(1).Value
  strABC_CE = cboBusq1(2).Value
  strCodIntFar = txtBusq1(0).Text
  strCodSeguridad = txtBusq1(1).Text
   
   If strCodGrpTerap <> "" Then
      If UCase(Left(strCodGrpTerap, 1)) <> "Q" Then
         MsgBox "El Grupo Terape�tico ha de comenzar por Q (Material Sanitario)", vbInformation, "Aviso"
         txtBusq1(9).SetFocus
         Command1.Enabled = True
         Exit Sub
      End If
   End If
   
   
   strClausulaWhere = " -1=-1 "
   If strTextoABuscar <> "" Then
      If Option2(0).Value = True Then
         ' Producto
         strClausulaWhere = strClausulaWhere & " AND (upper(FR73DESPRODUCTOPROV) LIKE upper('%" & strTextoABuscar & "%') OR upper(FR73DESPRODUCTO) LIKE upper('%" & strTextoABuscar & "%')) "
      ElseIf Option2(1).Value = True Then
         ' Referencia
         strClausulaWhere = strClausulaWhere & " AND upper(FR73REFERENCIA) LIKE upper('%" & strTextoABuscar & "%') "
      ElseIf Option2(2).Value = True Then
         ' Grupo Terap�utico
         strClausulaWhere = strClausulaWhere & " AND FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE upper(FR00DESGRPTERAP) LIKE upper('%" & strTextoABuscar & "%')) "
      ElseIf Option2(3).Value = True Then
         ' Proveedor
        If gstrLlamador = "FrmGenPetOfe" Then
           strClausulaWhere = strClausulaWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov
           strClausulaWhere = strClausulaWhere & " OR ( "
           strClausulaWhere = strClausulaWhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
           strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
           strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
           strClausulaWhere = strClausulaWhere & " )) "
        Else
          strClausulaWhere = strClausulaWhere & " AND ( "
          strClausulaWhere = strClausulaWhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " ) "
        End If
      End If
   End If
  
  If Option2(3).Value = False Then
     ' Proveedor
    If gstrLlamador = "FrmGenPetOfe" Then
       strClausulaWhere = strClausulaWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
    End If
  End If
   
   If strABC_C <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_C & "' "
   End If
   
   If strABC_CE <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_CE & "' "
   End If
   
   If strCodGrpTerap <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR00CODGRPTERAP LIKE '" & strCodGrpTerap & "%' "
   Else
      strClausulaWhere = strClausulaWhere & " AND FR00CODGRPTERAP LIKE 'Q%' "
   End If
   
   If strProdBajoStock = 1 Then
      strClausulaWhere = strClausulaWhere & " AND FR73EXISTENCIAS<=FR73STOCKMIN "
   End If
   
   If strDesvMayQue <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73DESVIACION>" & strDesvMayQue & " "
   End If
   
  If strCodIntFar <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR73CODINTFAR LIKE '%" & strCodIntFar & "%' "
  End If
  
  If strCodSeguridad <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR73CODINTFARSEG=" & strCodSeguridad & " "
  End If
      
   objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
   Call objWinInfo.DataRefresh
   Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
   
   Command1.Enabled = True
   Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Activate()
   gintProdBuscado = ""
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
   Dim objDetailInfo As New clsCWForm
   Dim strKey As String
  
   Set objWinInfo = New clsCWWin
   If gstrLlamador = "FrmGenPetOfe" Then
      Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
   Else
      Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                 Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
   End If
   With objDetailInfo
      .strName = "Material Sanitario"
      Set .objFormContainer = fraFrame1(0)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(0)
      Set .grdGrid = grdDBGrid1(0)
      '.strDataBase = objEnv.GetValue("Main")
      .strTable = "FR7300"
      .intCursorSize = 225
      .blnAskPrimary = False
      .intAllowance = cwAllowReadOnly
      .strWhere = " FR00CODGRPTERAP LIKE 'Q%' AND (FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL))"
      If gstrLlamador = "FrmGenPetOfe" Then
         .strWhere = .strWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
      End If
      
      Call .FormAddOrderField("FR73DESPRODUCTOPROV", cwAscending)
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Material Sanitario")
      
      Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno Material sanitario", cwString)
      Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Material sanitario", cwString)
      Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno Material sanitario")
      Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Material sanitario")
 
   End With
   
   With objWinInfo

      Call .FormAddInfo(objDetailInfo, cwFormDetail)
      
      Call .FormCreateInfo(objDetailInfo)
      
      .CtrlGetInfo(txtText1(0)).blnInGrid = False
      
      .CtrlGetInfo(txtText1(1)).blnInFind = True
      .CtrlGetInfo(txtText1(2)).blnInFind = True
      .CtrlGetInfo(txtText1(3)).blnInFind = True
      .CtrlGetInfo(txtText1(13)).blnInFind = True
      .CtrlGetInfo(txtText1(5)).blnInFind = True
      .CtrlGetInfo(txtText1(14)).blnInFind = True
      
      .CtrlGetInfo(txtProv1(59)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(31)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(30)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(32)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(33)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(34)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(37)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(35)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(38)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(36)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(57)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(40)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(39)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(41)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(42)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(43)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(46)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(44)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(47)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(45)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(48)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(56)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(58)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(55)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(54)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(53)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(50)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(52)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(49)).blnNegotiated = False
      .CtrlGetInfo(txtProv1(51)).blnNegotiated = False
      
      
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "FR79CODPROVEEDOR_A", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(15), "FR79PROVEEDOR")
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR_B", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(28), "FR79PROVEEDOR")
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR79CODPROVEEDOR_C", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(29), "FR79PROVEEDOR")
      
      Call .WinRegister
      Call .WinStabilize
   End With
  
  If gstrLlamador = "FrmGenPetOfe" Then
    tabTab1(0).Tab = 1
    grdDBGrid1(0).Columns(1).Caption = "C�digo" 'Cod.Int
    grdDBGrid1(0).Columns(1).Width = 740 'Cod.Int
    grdDBGrid1(0).Columns(2).Visible = False 'C.S.
    grdDBGrid1(0).Columns(3).Width = 3525 'Descripcion Mat.Sanitario
    grdDBGrid1(0).Columns(4).Width = 1095 'Referencia
    grdDBGrid1(0).Columns(5).Caption = "Exist." 'Existencias
    grdDBGrid1(0).Columns(5).Width = 510 'Existencias
    grdDBGrid1(0).Columns(6).Caption = "Stock Min" 'Stock Min.
    grdDBGrid1(0).Columns(6).Width = 900 'Stock Min.
    grdDBGrid1(0).Columns(7).Caption = "Pdte.Recibir" 'Pdte.Recibir
    grdDBGrid1(0).Columns(7).Width = 1019 'Pdte.Recibir
    grdDBGrid1(0).Columns(8).Caption = "Cons.Mens." 'Cons.Mensual
    grdDBGrid1(0).Columns(8).Width = 976 'Cons.Mensual
    grdDBGrid1(0).Columns(9).Caption = "Pdte.Bonif." 'Pdte.Bonificar
    grdDBGrid1(0).Columns(9).Width = 900 'Pdte.Bonificar
    grdDBGrid1(0).Columns(10).Caption = "P.Ult.Ent." 'P.Ult.Ent.
    grdDBGrid1(0).Columns(10).Width = 1200 'P.Ult.Ent.
  End If
  

End Sub
'Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If grdDBGrid1(0).Rows > 0 Then
'      grdDBGrid1(0).Columns(1).Caption = "C�digo" 'Cod.Int
'      grdDBGrid1(0).Columns(1).Width = 740 'Cod.Int
'      grdDBGrid1(0).Columns(2).Visible = False 'C.S.
'      grdDBGrid1(0).Columns(3).Width = 3525 'Descripcion Mat.Sanitario
'      grdDBGrid1(0).Columns(4).Width = 1095 'Referencia
'      grdDBGrid1(0).Columns(5).Caption = "Exist." 'Existencias
'      grdDBGrid1(0).Columns(5).Width = 510 'Existencias
'      grdDBGrid1(0).Columns(6).Caption = "Stock Min" 'Stock Min.
'      grdDBGrid1(0).Columns(6).Width = 900 'Stock Min.
'      grdDBGrid1(0).Columns(7).Caption = "Pdte.Recibir" 'Pdte.Recibir
'      grdDBGrid1(0).Columns(7).Width = 1019 'Pdte.Recibir
'      grdDBGrid1(0).Columns(8).Caption = "Cons.Mens." 'Cons.Mensual
'      grdDBGrid1(0).Columns(8).Width = 976 'Cons.Mensual
'      grdDBGrid1(0).Columns(9).Caption = "Pdte.Bonif." 'Pdte.Bonificar
'      grdDBGrid1(0).Columns(9).Width = 900 'Pdte.Bonificar
'      grdDBGrid1(0).Columns(10).Caption = "P.Ult.Ent." 'P.Ult.Ent.
'      grdDBGrid1(0).Columns(10).Width = 1200 'P.Ult.Ent.
'
'  End If
'End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
   Dim objField As clsCWFieldSearch
   Dim objSearch As clsCWSearch
  
   If strFormName = "Material Sanitario" And strCtrl = "txtText1(1)" Then
      Set objSearch = New clsCWSearch
      With objSearch
         .strTable = "FR7300"
         .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
         .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
         
         Set objField = .AddField("FR73CODPRODUCTO")
         objField.strSmallDesc = "C�digo Producto"
         
         Set objField = .AddField("FR73CODINTFAR")
         objField.strSmallDesc = "C�digo Interno Producto"
            
         Set objField = .AddField("FR73DESPRODUCTOPROV")
         objField.strSmallDesc = "Descripci�n Producto"
         
         Set objField = .AddField("FRH7CODFORMFAR")
         objField.strSmallDesc = "Forma Farmace�tica"
         
         Set objField = .AddField("FR73DOSIS")
         objField.strSmallDesc = "Dosis"
         
         Set objField = .AddField("FR93CODUNIMEDIDA")
         objField.strSmallDesc = "Unidad Medida"
         
         Set objField = .AddField("FR73REFERENCIA")
         objField.strSmallDesc = "Referencia"
            
         If .Search Then
            Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
         End If
      End With
      Set objSearch = Nothing
   End If
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

If grdDBGrid1(0).Rows > 0 Then
    grdDBGrid1(0).Columns(1).Caption = "C�digo" 'Cod.Int
    grdDBGrid1(0).Columns(1).Width = 740 'Cod.Int
    grdDBGrid1(0).Columns(2).Visible = False 'C.S.
    grdDBGrid1(0).Columns(3).Width = 3525 'Descripcion Mat.Sanitario
    grdDBGrid1(0).Columns(4).Width = 1095 'Referencia
    grdDBGrid1(0).Columns(5).Caption = "Exist." 'Existencias
    grdDBGrid1(0).Columns(5).Width = 510 'Existencias
    grdDBGrid1(0).Columns(6).Caption = "Stock Min" 'Stock Min.
    grdDBGrid1(0).Columns(6).Width = 900 'Stock Min.
    grdDBGrid1(0).Columns(7).Caption = "Pdte.Recibir" 'Pdte.Recibir
    grdDBGrid1(0).Columns(7).Width = 1019 'Pdte.Recibir
    grdDBGrid1(0).Columns(8).Caption = "Cons.Mens." 'Cons.Mensual
    grdDBGrid1(0).Columns(8).Width = 976 'Cons.Mensual
    grdDBGrid1(0).Columns(9).Caption = "Pdte.Bonif." 'Pdte.Bonificar
    grdDBGrid1(0).Columns(9).Width = 900 'Pdte.Bonificar
    grdDBGrid1(0).Columns(10).Caption = "P.Ult.Ent." 'P.Ult.Ent.
    grdDBGrid1(0).Columns(10).Width = 1200 'P.Ult.Ent.
End If

End Sub

Private Sub Option1_Click(Index As Integer)
   Dim rstp As rdoResultset
   Dim strP As String
   Dim qryFR79 As rdoQuery

   Select Case Index
   Case 0
      If txtText1(3).Text <> "" Then
         strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
         Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
         qryFR79(0) = txtText1(3).Text
         Set rstp = qryFR79.OpenResultset()
         If rstp.EOF = False Then
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            If IsNull(rstp("FR79COMENTARIOS").Value) Then
               txtProv1(59).Text = ""
            Else
               txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
            End If
            'rstp("FR79REPRESENT1").Value
            If IsNull(rstp("FR79DIRECCION1").Value) Then
               txtProv1(31).Text = ""
            Else
               txtProv1(31).Text = rstp("FR79DIRECCION1").Value
            End If
            If IsNull(rstp("FR79DISTRITO1").Value) Then
               txtProv1(30).Text = ""
            Else
               txtProv1(30).Text = rstp("FR79DISTRITO1").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD1").Value) Then
               txtProv1(32).Text = ""
            Else
               txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
            End If
            If IsNull(rstp("FR79PROVINCIA1").Value) Then
               txtProv1(33).Text = ""
            Else
               txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
            End If
            If IsNull(rstp("FR79PAIS1").Value) Then
               txtProv1(34).Text = ""
            Else
               txtProv1(34).Text = rstp("FR79PAIS1").Value
            End If
            If IsNull(rstp("FR79TFNO1").Value) Then
               txtProv1(37).Text = ""
            Else
               txtProv1(37).Text = rstp("FR79TFNO1").Value
            End If
            If IsNull(rstp("FR79TFNO11").Value) Then
               txtProv1(35).Text = ""
            Else
               txtProv1(35).Text = rstp("FR79TFNO11").Value
            End If
            If IsNull(rstp("FR79FAX1").Value) Then
               txtProv1(38).Text = ""
            Else
               txtProv1(38).Text = rstp("FR79FAX1").Value
            End If
            If IsNull(rstp("FR79EMAIL1").Value) Then
               txtProv1(36).Text = ""
            Else
               txtProv1(36).Text = rstp("FR79EMAIL1").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT2").Value) Then
               txtProv1(57).Text = ""
            Else
               txtProv1(57).Text = rstp("FR79REPRESENT2").Value
            End If
            If IsNull(rstp("FR79DIRECCION2").Value) Then
               txtProv1(40).Text = ""
            Else
               txtProv1(40).Text = rstp("FR79DIRECCION2").Value
            End If
            If IsNull(rstp("FR79DISTRITO2").Value) Then
               txtProv1(39).Text = ""
            Else
               txtProv1(39).Text = rstp("FR79DISTRITO2").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD2").Value) Then
               txtProv1(41).Text = ""
            Else
               txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
            End If
            If IsNull(rstp("FR79PROVINCIA2").Value) Then
               txtProv1(42).Text = ""
            Else
               txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
            End If
            If IsNull(rstp("FR79PAIS2").Value) Then
               txtProv1(43).Text = ""
            Else
               txtProv1(43).Text = rstp("FR79PAIS2").Value
            End If
            If IsNull(rstp("FR79TFNO2").Value) Then
               txtProv1(46).Text = ""
            Else
               txtProv1(46).Text = rstp("FR79TFNO2").Value
            End If
            If IsNull(rstp("FR79TFNO22").Value) Then
               txtProv1(44).Text = ""
            Else
               txtProv1(44).Text = rstp("FR79TFNO22").Value
            End If
            If IsNull(rstp("FR79FAX2").Value) Then
               txtProv1(47).Text = ""
            Else
               txtProv1(47).Text = rstp("FR79FAX2").Value
            End If
            If IsNull(rstp("FR79EMAIL2").Value) Then
               txtProv1(45).Text = ""
            Else
               txtProv1(45).Text = rstp("FR79EMAIL2").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT3").Value) Then
               txtProv1(48).Text = ""
            Else
               txtProv1(48).Text = rstp("FR79REPRESENT3").Value
            End If
            If IsNull(rstp("FR79DIRECCION3").Value) Then
               txtProv1(56).Text = ""
            Else
               txtProv1(56).Text = rstp("FR79DIRECCION3").Value
            End If
            If IsNull(rstp("FR79DISTRITO3").Value) Then
               txtProv1(58).Text = ""
            Else
               txtProv1(58).Text = rstp("FR79DISTRITO3").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD3").Value) Then
               txtProv1(55).Text = ""
            Else
               txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
            End If
            If IsNull(rstp("FR79PROVINCIA3").Value) Then
               txtProv1(54).Text = ""
            Else
               txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
            End If
            If IsNull(rstp("FR79PAIS3").Value) Then
               txtProv1(53).Text = ""
            Else
               txtProv1(53).Text = rstp("FR79PAIS3").Value
            End If
            If IsNull(rstp("FR79TFNO3").Value) Then
               txtProv1(50).Text = ""
            Else
               txtProv1(50).Text = rstp("FR79TFNO3").Value
            End If
            If IsNull(rstp("FR79TFNO33").Value) Then
               txtProv1(52).Text = ""
            Else
               txtProv1(52).Text = rstp("FR79TFNO33").Value
            End If
            If IsNull(rstp("FR79FAX3").Value) Then
               txtProv1(49).Text = ""
            Else
               txtProv1(49).Text = rstp("FR79FAX3").Value
            End If
            If IsNull(rstp("FR79EMAIL3").Value) Then
               txtProv1(51).Text = ""
            Else
               txtProv1(51).Text = rstp("FR79EMAIL3").Value
            End If
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         End If
         qryFR79.Close
         Set qryFR79 = Nothing
         Set rstp = Nothing
      Else
         'rstp("FR79CODPROVEEDOR").Value
         'rstp("FR79PROVEEDOR").Value
         txtProv1(59).Text = ""
         'rstp("FR79REPRESENT1").Value
         txtProv1(31).Text = ""
         txtProv1(30).Text = ""
         txtProv1(32).Text = ""
         txtProv1(33).Text = ""
         txtProv1(34).Text = ""
         txtProv1(37).Text = ""
         txtProv1(35).Text = ""
         txtProv1(38).Text = ""
         txtProv1(36).Text = ""
         '
         txtProv1(57).Text = ""
         txtProv1(40).Text = ""
         txtProv1(39).Text = ""
         txtProv1(41).Text = ""
         txtProv1(42).Text = ""
         txtProv1(43).Text = ""
         txtProv1(46).Text = ""
         txtProv1(44).Text = ""
         txtProv1(47).Text = ""
         txtProv1(45).Text = ""
         '
         txtProv1(48).Text = ""
         txtProv1(56).Text = ""
         txtProv1(58).Text = ""
         txtProv1(55).Text = ""
         txtProv1(54).Text = ""
         txtProv1(53).Text = ""
         txtProv1(50).Text = ""
         txtProv1(52).Text = ""
         txtProv1(49).Text = ""
         txtProv1(51).Text = ""
         'rstp("FR79NIF").Value
         'rstp("FR79CODCONT").Value
         'rstp("FR79CANTABC").Value
         'rstp("FR79DINABC").Value
         'rstp("FR79CTRLFIAB").Value
         'rstp("FR79INDPRONTOPAG").Value
         'rstp("FR79INDPTEBONIF").Value
      End If
   Case 1
      If txtText1(2).Text <> "" Then
         strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
         Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
         qryFR79(0) = txtText1(2).Text
         Set rstp = qryFR79.OpenResultset()
         If rstp.EOF = False Then
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            If IsNull(rstp("FR79COMENTARIOS").Value) Then
               txtProv1(59).Text = ""
            Else
               txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
            End If
            'rstp("FR79REPRESENT1").Value
            If IsNull(rstp("FR79DIRECCION1").Value) Then
               txtProv1(31).Text = ""
            Else
               txtProv1(31).Text = rstp("FR79DIRECCION1").Value
            End If
            If IsNull(rstp("FR79DISTRITO1").Value) Then
               txtProv1(30).Text = ""
            Else
               txtProv1(30).Text = rstp("FR79DISTRITO1").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD1").Value) Then
               txtProv1(32).Text = ""
            Else
               txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
            End If
            If IsNull(rstp("FR79PROVINCIA1").Value) Then
               txtProv1(33).Text = ""
            Else
               txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
            End If
            If IsNull(rstp("FR79PAIS1").Value) Then
               txtProv1(34).Text = ""
            Else
               txtProv1(34).Text = rstp("FR79PAIS1").Value
            End If
            If IsNull(rstp("FR79TFNO1").Value) Then
               txtProv1(37).Text = ""
            Else
               txtProv1(37).Text = rstp("FR79TFNO1").Value
            End If
            If IsNull(rstp("FR79TFNO11").Value) Then
               txtProv1(35).Text = ""
            Else
               txtProv1(35).Text = rstp("FR79TFNO11").Value
            End If
            If IsNull(rstp("FR79FAX1").Value) Then
               txtProv1(38).Text = ""
            Else
               txtProv1(38).Text = rstp("FR79FAX1").Value
            End If
            If IsNull(rstp("FR79EMAIL1").Value) Then
               txtProv1(36).Text = ""
            Else
               txtProv1(36).Text = rstp("FR79EMAIL1").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT2").Value) Then
               txtProv1(57).Text = ""
            Else
               txtProv1(57).Text = rstp("FR79REPRESENT2").Value
            End If
            If IsNull(rstp("FR79DIRECCION2").Value) Then
               txtProv1(40).Text = ""
            Else
               txtProv1(40).Text = rstp("FR79DIRECCION2").Value
            End If
            If IsNull(rstp("FR79DISTRITO2").Value) Then
               txtProv1(39).Text = ""
            Else
               txtProv1(39).Text = rstp("FR79DISTRITO2").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD2").Value) Then
               txtProv1(41).Text = ""
            Else
               txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
            End If
            If IsNull(rstp("FR79PROVINCIA2").Value) Then
               txtProv1(42).Text = ""
            Else
               txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
            End If
            If IsNull(rstp("FR79PAIS2").Value) Then
               txtProv1(43).Text = ""
            Else
               txtProv1(43).Text = rstp("FR79PAIS2").Value
            End If
            If IsNull(rstp("FR79TFNO2").Value) Then
               txtProv1(46).Text = ""
            Else
               txtProv1(46).Text = rstp("FR79TFNO2").Value
            End If
            If IsNull(rstp("FR79TFNO22").Value) Then
               txtProv1(44).Text = ""
            Else
               txtProv1(44).Text = rstp("FR79TFNO22").Value
            End If
            If IsNull(rstp("FR79FAX2").Value) Then
               txtProv1(47).Text = ""
            Else
               txtProv1(47).Text = rstp("FR79FAX2").Value
            End If
            If IsNull(rstp("FR79EMAIL2").Value) Then
               txtProv1(45).Text = ""
            Else
               txtProv1(45).Text = rstp("FR79EMAIL2").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT3").Value) Then
               txtProv1(48).Text = ""
            Else
               txtProv1(48).Text = rstp("FR79REPRESENT3").Value
            End If
            If IsNull(rstp("FR79DIRECCION3").Value) Then
               txtProv1(56).Text = ""
            Else
               txtProv1(56).Text = rstp("FR79DIRECCION3").Value
            End If
            If IsNull(rstp("FR79DISTRITO3").Value) Then
               txtProv1(58).Text = ""
            Else
               txtProv1(58).Text = rstp("FR79DISTRITO3").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD3").Value) Then
               txtProv1(55).Text = ""
            Else
               txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
            End If
            If IsNull(rstp("FR79PROVINCIA3").Value) Then
               txtProv1(54).Text = ""
            Else
               txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
            End If
            If IsNull(rstp("FR79PAIS3").Value) Then
               txtProv1(53).Text = ""
            Else
               txtProv1(53).Text = rstp("FR79PAIS3").Value
            End If
            If IsNull(rstp("FR79TFNO3").Value) Then
               txtProv1(50).Text = ""
            Else
               txtProv1(50).Text = rstp("FR79TFNO3").Value
            End If
            If IsNull(rstp("FR79TFNO33").Value) Then
               txtProv1(52).Text = ""
            Else
               txtProv1(52).Text = rstp("FR79TFNO33").Value
            End If
            If IsNull(rstp("FR79FAX3").Value) Then
               txtProv1(49).Text = ""
            Else
               txtProv1(49).Text = rstp("FR79FAX3").Value
            End If
            If IsNull(rstp("FR79EMAIL3").Value) Then
               txtProv1(51).Text = ""
            Else
               txtProv1(51).Text = rstp("FR79EMAIL3").Value
            End If
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         End If
         qryFR79.Close
         Set qryFR79 = Nothing
         Set rstp = Nothing
      Else
         'rstp("FR79CODPROVEEDOR").Value
         'rstp("FR79PROVEEDOR").Value
         txtProv1(59).Text = ""
         'rstp("FR79REPRESENT1").Value
         txtProv1(31).Text = ""
         txtProv1(30).Text = ""
         txtProv1(32).Text = ""
         txtProv1(33).Text = ""
         txtProv1(34).Text = ""
         txtProv1(37).Text = ""
         txtProv1(35).Text = ""
         txtProv1(38).Text = ""
         txtProv1(36).Text = ""
         '
         txtProv1(57).Text = ""
         txtProv1(40).Text = ""
         txtProv1(39).Text = ""
         txtProv1(41).Text = ""
         txtProv1(42).Text = ""
         txtProv1(43).Text = ""
         txtProv1(46).Text = ""
         txtProv1(44).Text = ""
         txtProv1(47).Text = ""
         txtProv1(45).Text = ""
         '
         txtProv1(48).Text = ""
         txtProv1(56).Text = ""
         txtProv1(58).Text = ""
         txtProv1(55).Text = ""
         txtProv1(54).Text = ""
         txtProv1(53).Text = ""
         txtProv1(50).Text = ""
         txtProv1(52).Text = ""
         txtProv1(49).Text = ""
         txtProv1(51).Text = ""
         'rstp("FR79NIF").Value
         'rstp("FR79CODCONT").Value
         'rstp("FR79CANTABC").Value
         'rstp("FR79DINABC").Value
         'rstp("FR79CTRLFIAB").Value
         'rstp("FR79INDPRONTOPAG").Value
         'rstp("FR79INDPTEBONIF").Value
      End If
   Case 2
      If txtText1(1).Text <> "" Then
         strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
         Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
         qryFR79(0) = txtText1(1).Text
         Set rstp = qryFR79.OpenResultset()
         If rstp.EOF = False Then
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            If IsNull(rstp("FR79COMENTARIOS").Value) Then
               txtProv1(59).Text = ""
            Else
               txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
            End If
            'rstp("FR79REPRESENT1").Value
            If IsNull(rstp("FR79DIRECCION1").Value) Then
               txtProv1(31).Text = ""
            Else
               txtProv1(31).Text = rstp("FR79DIRECCION1").Value
            End If
            If IsNull(rstp("FR79DISTRITO1").Value) Then
               txtProv1(30).Text = ""
            Else
               txtProv1(30).Text = rstp("FR79DISTRITO1").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD1").Value) Then
               txtProv1(32).Text = ""
            Else
               txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
            End If
            If IsNull(rstp("FR79PROVINCIA1").Value) Then
               txtProv1(33).Text = ""
            Else
               txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
            End If
            If IsNull(rstp("FR79PAIS1").Value) Then
               txtProv1(34).Text = ""
            Else
               txtProv1(34).Text = rstp("FR79PAIS1").Value
            End If
            If IsNull(rstp("FR79TFNO1").Value) Then
               txtProv1(37).Text = ""
            Else
               txtProv1(37).Text = rstp("FR79TFNO1").Value
            End If
            If IsNull(rstp("FR79TFNO11").Value) Then
               txtProv1(35).Text = ""
            Else
               txtProv1(35).Text = rstp("FR79TFNO11").Value
            End If
            If IsNull(rstp("FR79FAX1").Value) Then
               txtProv1(38).Text = ""
            Else
               txtProv1(38).Text = rstp("FR79FAX1").Value
            End If
            If IsNull(rstp("FR79EMAIL1").Value) Then
               txtProv1(36).Text = ""
            Else
               txtProv1(36).Text = rstp("FR79EMAIL1").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT2").Value) Then
               txtProv1(57).Text = ""
            Else
               txtProv1(57).Text = rstp("FR79REPRESENT2").Value
            End If
            If IsNull(rstp("FR79DIRECCION2").Value) Then
               txtProv1(40).Text = ""
            Else
               txtProv1(40).Text = rstp("FR79DIRECCION2").Value
            End If
            If IsNull(rstp("FR79DISTRITO2").Value) Then
               txtProv1(39).Text = ""
            Else
               txtProv1(39).Text = rstp("FR79DISTRITO2").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD2").Value) Then
               txtProv1(41).Text = ""
            Else
               txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
            End If
            If IsNull(rstp("FR79PROVINCIA2").Value) Then
               txtProv1(42).Text = ""
            Else
               txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
            End If
            If IsNull(rstp("FR79PAIS2").Value) Then
               txtProv1(43).Text = ""
            Else
               txtProv1(43).Text = rstp("FR79PAIS2").Value
            End If
            If IsNull(rstp("FR79TFNO2").Value) Then
               txtProv1(46).Text = ""
            Else
               txtProv1(46).Text = rstp("FR79TFNO2").Value
            End If
            If IsNull(rstp("FR79TFNO22").Value) Then
               txtProv1(44).Text = ""
            Else
               txtProv1(44).Text = rstp("FR79TFNO22").Value
            End If
            If IsNull(rstp("FR79FAX2").Value) Then
               txtProv1(47).Text = ""
            Else
               txtProv1(47).Text = rstp("FR79FAX2").Value
            End If
            If IsNull(rstp("FR79EMAIL2").Value) Then
               txtProv1(45).Text = ""
            Else
               txtProv1(45).Text = rstp("FR79EMAIL2").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT3").Value) Then
               txtProv1(48).Text = ""
            Else
               txtProv1(48).Text = rstp("FR79REPRESENT3").Value
            End If
            If IsNull(rstp("FR79DIRECCION3").Value) Then
               txtProv1(56).Text = ""
            Else
               txtProv1(56).Text = rstp("FR79DIRECCION3").Value
            End If
            If IsNull(rstp("FR79DISTRITO3").Value) Then
               txtProv1(58).Text = ""
            Else
               txtProv1(58).Text = rstp("FR79DISTRITO3").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD3").Value) Then
               txtProv1(55).Text = ""
            Else
               txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
            End If
            If IsNull(rstp("FR79PROVINCIA3").Value) Then
               txtProv1(54).Text = ""
            Else
               txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
            End If
            If IsNull(rstp("FR79PAIS3").Value) Then
               txtProv1(53).Text = ""
            Else
               txtProv1(53).Text = rstp("FR79PAIS3").Value
            End If
            If IsNull(rstp("FR79TFNO3").Value) Then
               txtProv1(50).Text = ""
            Else
               txtProv1(50).Text = rstp("FR79TFNO3").Value
            End If
            If IsNull(rstp("FR79TFNO33").Value) Then
               txtProv1(52).Text = ""
            Else
               txtProv1(52).Text = rstp("FR79TFNO33").Value
            End If
            If IsNull(rstp("FR79FAX3").Value) Then
               txtProv1(49).Text = ""
            Else
               txtProv1(49).Text = rstp("FR79FAX3").Value
            End If
            If IsNull(rstp("FR79EMAIL3").Value) Then
               txtProv1(51).Text = ""
            Else
               txtProv1(51).Text = rstp("FR79EMAIL3").Value
            End If
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         End If
         qryFR79.Close
         Set qryFR79 = Nothing
         Set rstp = Nothing
      Else
         'rstp("FR79CODPROVEEDOR").Value
         'rstp("FR79PROVEEDOR").Value
         txtProv1(59).Text = ""
         'rstp("FR79REPRESENT1").Value
         txtProv1(31).Text = ""
         txtProv1(30).Text = ""
         txtProv1(32).Text = ""
         txtProv1(33).Text = ""
         txtProv1(34).Text = ""
         txtProv1(37).Text = ""
         txtProv1(35).Text = ""
         txtProv1(38).Text = ""
         txtProv1(36).Text = ""
         '
         txtProv1(57).Text = ""
         txtProv1(40).Text = ""
         txtProv1(39).Text = ""
         txtProv1(41).Text = ""
         txtProv1(42).Text = ""
         txtProv1(43).Text = ""
         txtProv1(46).Text = ""
         txtProv1(44).Text = ""
         txtProv1(47).Text = ""
         txtProv1(45).Text = ""
         '
         txtProv1(48).Text = ""
         txtProv1(56).Text = ""
         txtProv1(58).Text = ""
         txtProv1(55).Text = ""
         txtProv1(54).Text = ""
         txtProv1(53).Text = ""
         txtProv1(50).Text = ""
         txtProv1(52).Text = ""
         txtProv1(49).Text = ""
         txtProv1(51).Text = ""
         'rstp("FR79NIF").Value
         'rstp("FR79CODCONT").Value
         'rstp("FR79CANTABC").Value
         'rstp("FR79DINABC").Value
         'rstp("FR79CTRLFIAB").Value
         'rstp("FR79INDPRONTOPAG").Value
         'rstp("FR79INDPTEBONIF").Value
      End If
   End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
   Dim rstp As rdoResultset
   Dim strP As String
  
   gintIndice = intIndex
   Call objWinInfo.CtrlDataChange


   Select Case intIndex
   Case 3
      If txtText1(3).Text <> "" Then
         strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(3).Text
         Set rstp = objApp.rdoConnect.OpenResultset(strP)
         If rstp.EOF = False Then
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            If IsNull(rstp("FR79COMENTARIOS").Value) Then
               txtProv1(59).Text = ""
            Else
               txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
            End If
            'rstp("FR79REPRESENT1").Value
            If IsNull(rstp("FR79DIRECCION1").Value) Then
               txtProv1(31).Text = ""
            Else
               txtProv1(31).Text = rstp("FR79DIRECCION1").Value
            End If
            If IsNull(rstp("FR79DISTRITO1").Value) Then
               txtProv1(30).Text = ""
            Else
               txtProv1(30).Text = rstp("FR79DISTRITO1").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD1").Value) Then
               txtProv1(32).Text = ""
            Else
               txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
            End If
            If IsNull(rstp("FR79PROVINCIA1").Value) Then
               txtProv1(33).Text = ""
            Else
               txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
            End If
            If IsNull(rstp("FR79PAIS1").Value) Then
               txtProv1(34).Text = ""
            Else
               txtProv1(34).Text = rstp("FR79PAIS1").Value
            End If
            If IsNull(rstp("FR79TFNO1").Value) Then
               txtProv1(37).Text = ""
            Else
               txtProv1(37).Text = rstp("FR79TFNO1").Value
            End If
            If IsNull(rstp("FR79TFNO11").Value) Then
               txtProv1(35).Text = ""
            Else
               txtProv1(35).Text = rstp("FR79TFNO11").Value
            End If
            If IsNull(rstp("FR79FAX1").Value) Then
               txtProv1(38).Text = ""
            Else
               txtProv1(38).Text = rstp("FR79FAX1").Value
            End If
            If IsNull(rstp("FR79EMAIL1").Value) Then
               txtProv1(36).Text = ""
            Else
               txtProv1(36).Text = rstp("FR79EMAIL1").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT2").Value) Then
               txtProv1(57).Text = ""
            Else
               txtProv1(57).Text = rstp("FR79REPRESENT2").Value
            End If
            If IsNull(rstp("FR79DIRECCION2").Value) Then
               txtProv1(40).Text = ""
            Else
               txtProv1(40).Text = rstp("FR79DIRECCION2").Value
            End If
            If IsNull(rstp("FR79DISTRITO2").Value) Then
               txtProv1(39).Text = ""
            Else
               txtProv1(39).Text = rstp("FR79DISTRITO2").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD2").Value) Then
               txtProv1(41).Text = ""
            Else
               txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
            End If
            If IsNull(rstp("FR79PROVINCIA2").Value) Then
               txtProv1(42).Text = ""
            Else
               txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
            End If
            If IsNull(rstp("FR79PAIS2").Value) Then
               txtProv1(43).Text = ""
            Else
               txtProv1(43).Text = rstp("FR79PAIS2").Value
            End If
            If IsNull(rstp("FR79TFNO2").Value) Then
               txtProv1(46).Text = ""
            Else
               txtProv1(46).Text = rstp("FR79TFNO2").Value
            End If
            If IsNull(rstp("FR79TFNO22").Value) Then
               txtProv1(44).Text = ""
            Else
               txtProv1(44).Text = rstp("FR79TFNO22").Value
            End If
            If IsNull(rstp("FR79FAX2").Value) Then
               txtProv1(47).Text = ""
            Else
               txtProv1(47).Text = rstp("FR79FAX2").Value
            End If
            If IsNull(rstp("FR79EMAIL2").Value) Then
               txtProv1(45).Text = ""
            Else
               txtProv1(45).Text = rstp("FR79EMAIL2").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT3").Value) Then
               txtProv1(48).Text = ""
            Else
               txtProv1(48).Text = rstp("FR79REPRESENT3").Value
            End If
            If IsNull(rstp("FR79DIRECCION3").Value) Then
               txtProv1(56).Text = ""
            Else
               txtProv1(56).Text = rstp("FR79DIRECCION3").Value
            End If
            If IsNull(rstp("FR79DISTRITO3").Value) Then
               txtProv1(58).Text = ""
            Else
               txtProv1(58).Text = rstp("FR79DISTRITO3").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD3").Value) Then
               txtProv1(55).Text = ""
            Else
               txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
            End If
            If IsNull(rstp("FR79PROVINCIA3").Value) Then
               txtProv1(54).Text = ""
            Else
               txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
            End If
            If IsNull(rstp("FR79PAIS3").Value) Then
               txtProv1(53).Text = ""
            Else
               txtProv1(53).Text = rstp("FR79PAIS3").Value
            End If
            If IsNull(rstp("FR79TFNO3").Value) Then
               txtProv1(50).Text = ""
            Else
               txtProv1(50).Text = rstp("FR79TFNO3").Value
            End If
            If IsNull(rstp("FR79TFNO33").Value) Then
               txtProv1(52).Text = ""
            Else
               txtProv1(52).Text = rstp("FR79TFNO33").Value
            End If
            If IsNull(rstp("FR79FAX3").Value) Then
               txtProv1(49).Text = ""
            Else
               txtProv1(49).Text = rstp("FR79FAX3").Value
            End If
            If IsNull(rstp("FR79EMAIL3").Value) Then
               txtProv1(51).Text = ""
            Else
               txtProv1(51).Text = rstp("FR79EMAIL3").Value
            End If
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         End If
         rstp.Close
         Set rstp = Nothing
      Else
         'rstp("FR79CODPROVEEDOR").Value
         'rstp("FR79PROVEEDOR").Value
         txtProv1(59).Text = ""
         'rstp("FR79REPRESENT1").Value
         txtProv1(31).Text = ""
         txtProv1(30).Text = ""
         txtProv1(32).Text = ""
         txtProv1(33).Text = ""
         txtProv1(34).Text = ""
         txtProv1(37).Text = ""
         txtProv1(35).Text = ""
         txtProv1(38).Text = ""
         txtProv1(36).Text = ""
         '
         txtProv1(57).Text = ""
         txtProv1(40).Text = ""
         txtProv1(39).Text = ""
         txtProv1(41).Text = ""
         txtProv1(42).Text = ""
         txtProv1(43).Text = ""
         txtProv1(46).Text = ""
         txtProv1(44).Text = ""
         txtProv1(47).Text = ""
         txtProv1(45).Text = ""
         '
         txtProv1(48).Text = ""
         txtProv1(56).Text = ""
         txtProv1(58).Text = ""
         txtProv1(55).Text = ""
         txtProv1(54).Text = ""
         txtProv1(53).Text = ""
         txtProv1(50).Text = ""
         txtProv1(52).Text = ""
         txtProv1(49).Text = ""
         txtProv1(51).Text = ""
         'rstp("FR79NIF").Value
         'rstp("FR79CODCONT").Value
         'rstp("FR79CANTABC").Value
         'rstp("FR79DINABC").Value
         'rstp("FR79CTRLFIAB").Value
         'rstp("FR79INDPRONTOPAG").Value
         'rstp("FR79INDPTEBONIF").Value
      End If
   Case 2
      If txtText1(2).Text <> "" Then
         strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(2).Text
         Set rstp = objApp.rdoConnect.OpenResultset(strP)
         If rstp.EOF = False Then
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            If IsNull(rstp("FR79COMENTARIOS").Value) Then
               txtProv1(59).Text = ""
            Else
               txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
            End If
            'rstp("FR79REPRESENT1").Value
            If IsNull(rstp("FR79DIRECCION1").Value) Then
               txtProv1(31).Text = ""
            Else
               txtProv1(31).Text = rstp("FR79DIRECCION1").Value
            End If
            If IsNull(rstp("FR79DISTRITO1").Value) Then
               txtProv1(30).Text = ""
            Else
               txtProv1(30).Text = rstp("FR79DISTRITO1").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD1").Value) Then
               txtProv1(32).Text = ""
            Else
               txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
            End If
            If IsNull(rstp("FR79PROVINCIA1").Value) Then
               txtProv1(33).Text = ""
            Else
               txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
            End If
            If IsNull(rstp("FR79PAIS1").Value) Then
               txtProv1(34).Text = ""
            Else
               txtProv1(34).Text = rstp("FR79PAIS1").Value
            End If
            If IsNull(rstp("FR79TFNO1").Value) Then
               txtProv1(37).Text = ""
            Else
               txtProv1(37).Text = rstp("FR79TFNO1").Value
            End If
            If IsNull(rstp("FR79TFNO11").Value) Then
               txtProv1(35).Text = ""
            Else
               txtProv1(35).Text = rstp("FR79TFNO11").Value
            End If
            If IsNull(rstp("FR79FAX1").Value) Then
               txtProv1(38).Text = ""
            Else
               txtProv1(38).Text = rstp("FR79FAX1").Value
            End If
            If IsNull(rstp("FR79EMAIL1").Value) Then
               txtProv1(36).Text = ""
            Else
               txtProv1(36).Text = rstp("FR79EMAIL1").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT2").Value) Then
               txtProv1(57).Text = ""
            Else
               txtProv1(57).Text = rstp("FR79REPRESENT2").Value
            End If
            If IsNull(rstp("FR79DIRECCION2").Value) Then
               txtProv1(40).Text = ""
            Else
               txtProv1(40).Text = rstp("FR79DIRECCION2").Value
            End If
            If IsNull(rstp("FR79DISTRITO2").Value) Then
               txtProv1(39).Text = ""
            Else
               txtProv1(39).Text = rstp("FR79DISTRITO2").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD2").Value) Then
               txtProv1(41).Text = ""
            Else
               txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
            End If
            If IsNull(rstp("FR79PROVINCIA2").Value) Then
               txtProv1(42).Text = ""
            Else
               txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
            End If
            If IsNull(rstp("FR79PAIS2").Value) Then
               txtProv1(43).Text = ""
            Else
               txtProv1(43).Text = rstp("FR79PAIS2").Value
            End If
            If IsNull(rstp("FR79TFNO2").Value) Then
               txtProv1(46).Text = ""
            Else
               txtProv1(46).Text = rstp("FR79TFNO2").Value
            End If
            If IsNull(rstp("FR79TFNO22").Value) Then
               txtProv1(44).Text = ""
            Else
               txtProv1(44).Text = rstp("FR79TFNO22").Value
            End If
            If IsNull(rstp("FR79FAX2").Value) Then
               txtProv1(47).Text = ""
            Else
               txtProv1(47).Text = rstp("FR79FAX2").Value
            End If
            If IsNull(rstp("FR79EMAIL2").Value) Then
               txtProv1(45).Text = ""
            Else
               txtProv1(45).Text = rstp("FR79EMAIL2").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT3").Value) Then
               txtProv1(48).Text = ""
            Else
               txtProv1(48).Text = rstp("FR79REPRESENT3").Value
            End If
            If IsNull(rstp("FR79DIRECCION3").Value) Then
               txtProv1(56).Text = ""
            Else
               txtProv1(56).Text = rstp("FR79DIRECCION3").Value
            End If
            If IsNull(rstp("FR79DISTRITO3").Value) Then
               txtProv1(58).Text = ""
            Else
               txtProv1(58).Text = rstp("FR79DISTRITO3").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD3").Value) Then
               txtProv1(55).Text = ""
            Else
               txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
            End If
            If IsNull(rstp("FR79PROVINCIA3").Value) Then
               txtProv1(54).Text = ""
            Else
               txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
            End If
            If IsNull(rstp("FR79PAIS3").Value) Then
               txtProv1(53).Text = ""
            Else
               txtProv1(53).Text = rstp("FR79PAIS3").Value
            End If
            If IsNull(rstp("FR79TFNO3").Value) Then
               txtProv1(50).Text = ""
            Else
               txtProv1(50).Text = rstp("FR79TFNO3").Value
            End If
            If IsNull(rstp("FR79TFNO33").Value) Then
               txtProv1(52).Text = ""
            Else
               txtProv1(52).Text = rstp("FR79TFNO33").Value
            End If
            If IsNull(rstp("FR79FAX3").Value) Then
               txtProv1(49).Text = ""
            Else
               txtProv1(49).Text = rstp("FR79FAX3").Value
            End If
            If IsNull(rstp("FR79EMAIL3").Value) Then
               txtProv1(51).Text = ""
            Else
               txtProv1(51).Text = rstp("FR79EMAIL3").Value
            End If
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         End If
         rstp.Close
         Set rstp = Nothing
      Else
         'rstp("FR79CODPROVEEDOR").Value
         'rstp("FR79PROVEEDOR").Value
         txtProv1(59).Text = ""
         'rstp("FR79REPRESENT1").Value
         txtProv1(31).Text = ""
         txtProv1(30).Text = ""
         txtProv1(32).Text = ""
         txtProv1(33).Text = ""
         txtProv1(34).Text = ""
         txtProv1(37).Text = ""
         txtProv1(35).Text = ""
         txtProv1(38).Text = ""
         txtProv1(36).Text = ""
         '
         txtProv1(57).Text = ""
         txtProv1(40).Text = ""
         txtProv1(39).Text = ""
         txtProv1(41).Text = ""
         txtProv1(42).Text = ""
         txtProv1(43).Text = ""
         txtProv1(46).Text = ""
         txtProv1(44).Text = ""
         txtProv1(47).Text = ""
         txtProv1(45).Text = ""
         '
         txtProv1(48).Text = ""
         txtProv1(56).Text = ""
         txtProv1(58).Text = ""
         txtProv1(55).Text = ""
         txtProv1(54).Text = ""
         txtProv1(53).Text = ""
         txtProv1(50).Text = ""
         txtProv1(52).Text = ""
         txtProv1(49).Text = ""
         txtProv1(51).Text = ""
         'rstp("FR79NIF").Value
         'rstp("FR79CODCONT").Value
         'rstp("FR79CANTABC").Value
         'rstp("FR79DINABC").Value
         'rstp("FR79CTRLFIAB").Value
         'rstp("FR79INDPRONTOPAG").Value
         'rstp("FR79INDPTEBONIF").Value
      End If
   Case 1
      If txtText1(1).Text <> "" Then
         strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
         Set rstp = objApp.rdoConnect.OpenResultset(strP)
         If rstp.EOF = False Then
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            If IsNull(rstp("FR79COMENTARIOS").Value) Then
               txtProv1(59).Text = ""
            Else
               txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
            End If
            'rstp("FR79REPRESENT1").Value
            If IsNull(rstp("FR79DIRECCION1").Value) Then
               txtProv1(31).Text = ""
            Else
               txtProv1(31).Text = rstp("FR79DIRECCION1").Value
            End If
            If IsNull(rstp("FR79DISTRITO1").Value) Then
               txtProv1(30).Text = ""
            Else
               txtProv1(30).Text = rstp("FR79DISTRITO1").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD1").Value) Then
               txtProv1(32).Text = ""
            Else
               txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
            End If
            If IsNull(rstp("FR79PROVINCIA1").Value) Then
               txtProv1(33).Text = ""
            Else
               txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
            End If
            If IsNull(rstp("FR79PAIS1").Value) Then
               txtProv1(34).Text = ""
            Else
               txtProv1(34).Text = rstp("FR79PAIS1").Value
            End If
            If IsNull(rstp("FR79TFNO1").Value) Then
               txtProv1(37).Text = ""
            Else
               txtProv1(37).Text = rstp("FR79TFNO1").Value
            End If
            If IsNull(rstp("FR79TFNO11").Value) Then
               txtProv1(35).Text = ""
            Else
               txtProv1(35).Text = rstp("FR79TFNO11").Value
            End If
            If IsNull(rstp("FR79FAX1").Value) Then
               txtProv1(38).Text = ""
            Else
               txtProv1(38).Text = rstp("FR79FAX1").Value
            End If
            If IsNull(rstp("FR79EMAIL1").Value) Then
               txtProv1(36).Text = ""
            Else
               txtProv1(36).Text = rstp("FR79EMAIL1").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT2").Value) Then
               txtProv1(57).Text = ""
            Else
               txtProv1(57).Text = rstp("FR79REPRESENT2").Value
            End If
            If IsNull(rstp("FR79DIRECCION2").Value) Then
               txtProv1(40).Text = ""
            Else
               txtProv1(40).Text = rstp("FR79DIRECCION2").Value
            End If
            If IsNull(rstp("FR79DISTRITO2").Value) Then
               txtProv1(39).Text = ""
            Else
               txtProv1(39).Text = rstp("FR79DISTRITO2").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD2").Value) Then
               txtProv1(41).Text = ""
            Else
               txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
            End If
            If IsNull(rstp("FR79PROVINCIA2").Value) Then
               txtProv1(42).Text = ""
            Else
               txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
            End If
            If IsNull(rstp("FR79PAIS2").Value) Then
               txtProv1(43).Text = ""
            Else
               txtProv1(43).Text = rstp("FR79PAIS2").Value
            End If
            If IsNull(rstp("FR79TFNO2").Value) Then
               txtProv1(46).Text = ""
            Else
               txtProv1(46).Text = rstp("FR79TFNO2").Value
            End If
            If IsNull(rstp("FR79TFNO22").Value) Then
               txtProv1(44).Text = ""
            Else
               txtProv1(44).Text = rstp("FR79TFNO22").Value
            End If
            If IsNull(rstp("FR79FAX2").Value) Then
               txtProv1(47).Text = ""
            Else
               txtProv1(47).Text = rstp("FR79FAX2").Value
            End If
            If IsNull(rstp("FR79EMAIL2").Value) Then
               txtProv1(45).Text = ""
            Else
               txtProv1(45).Text = rstp("FR79EMAIL2").Value
            End If
            '
            If IsNull(rstp("FR79REPRESENT3").Value) Then
               txtProv1(48).Text = ""
            Else
               txtProv1(48).Text = rstp("FR79REPRESENT3").Value
            End If
            If IsNull(rstp("FR79DIRECCION3").Value) Then
               txtProv1(56).Text = ""
            Else
               txtProv1(56).Text = rstp("FR79DIRECCION3").Value
            End If
            If IsNull(rstp("FR79DISTRITO3").Value) Then
               txtProv1(58).Text = ""
            Else
               txtProv1(58).Text = rstp("FR79DISTRITO3").Value
            End If
            If IsNull(rstp("FR79LOCALIDAD3").Value) Then
               txtProv1(55).Text = ""
            Else
               txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
            End If
            If IsNull(rstp("FR79PROVINCIA3").Value) Then
               txtProv1(54).Text = ""
            Else
               txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
            End If
            If IsNull(rstp("FR79PAIS3").Value) Then
               txtProv1(53).Text = ""
            Else
               txtProv1(53).Text = rstp("FR79PAIS3").Value
            End If
            If IsNull(rstp("FR79TFNO3").Value) Then
               txtProv1(50).Text = ""
            Else
               txtProv1(50).Text = rstp("FR79TFNO3").Value
            End If
            If IsNull(rstp("FR79TFNO33").Value) Then
               txtProv1(52).Text = ""
            Else
               txtProv1(52).Text = rstp("FR79TFNO33").Value
            End If
            If IsNull(rstp("FR79FAX3").Value) Then
               txtProv1(49).Text = ""
            Else
               txtProv1(49).Text = rstp("FR79FAX3").Value
            End If
            If IsNull(rstp("FR79EMAIL3").Value) Then
               txtProv1(51).Text = ""
            Else
               txtProv1(51).Text = rstp("FR79EMAIL3").Value
            End If
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         Else
            'rstp("FR79CODPROVEEDOR").Value
            'rstp("FR79PROVEEDOR").Value
            txtProv1(59).Text = ""
            'rstp("FR79REPRESENT1").Value
            txtProv1(31).Text = ""
            txtProv1(30).Text = ""
            txtProv1(32).Text = ""
            txtProv1(33).Text = ""
            txtProv1(34).Text = ""
            txtProv1(37).Text = ""
            txtProv1(35).Text = ""
            txtProv1(38).Text = ""
            txtProv1(36).Text = ""
            '
            txtProv1(57).Text = ""
            txtProv1(40).Text = ""
            txtProv1(39).Text = ""
            txtProv1(41).Text = ""
            txtProv1(42).Text = ""
            txtProv1(43).Text = ""
            txtProv1(46).Text = ""
            txtProv1(44).Text = ""
            txtProv1(47).Text = ""
            txtProv1(45).Text = ""
            '
            txtProv1(48).Text = ""
            txtProv1(56).Text = ""
            txtProv1(58).Text = ""
            txtProv1(55).Text = ""
            txtProv1(54).Text = ""
            txtProv1(53).Text = ""
            txtProv1(50).Text = ""
            txtProv1(52).Text = ""
            txtProv1(49).Text = ""
            txtProv1(51).Text = ""
            'rstp("FR79NIF").Value
            'rstp("FR79CODCONT").Value
            'rstp("FR79CANTABC").Value
            'rstp("FR79DINABC").Value
            'rstp("FR79CTRLFIAB").Value
            'rstp("FR79INDPRONTOPAG").Value
            'rstp("FR79INDPTEBONIF").Value
         End If
         rstp.Close
         Set rstp = Nothing
      End If
   End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Command2_Click(intIndex As Integer)
  gintProdBuscado = txtText1(0).Text
  Unload Me
End Sub

