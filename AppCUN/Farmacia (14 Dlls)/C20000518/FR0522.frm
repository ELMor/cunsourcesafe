VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmConSitPed 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consultar Situaci�n Pedidos."
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0522.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdintrecepcion 
      Caption         =   "Recibir &Pedido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10080
      TabIndex        =   80
      Top             =   4440
      Width           =   1575
   End
   Begin VB.Frame Frame2 
      Caption         =   "Criterios de B�queda"
      ForeColor       =   &H00FF0000&
      Height          =   1815
      Left            =   120
      TabIndex        =   44
      Top             =   480
      Width           =   11655
      Begin VB.CheckBox Check4 
         Caption         =   "Pedidos Reclamados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3600
         TabIndex        =   106
         Top             =   1440
         Width           =   2535
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Buscar Pedidos para Reclamar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   102
         Top             =   1440
         Width           =   3375
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Ver Perdidos Completos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6480
         TabIndex        =   86
         Top             =   1080
         Width           =   2415
      End
      Begin VB.TextBox txtpedido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataField       =   "Dname"
         Height          =   330
         HelpContextID   =   40102
         Left            =   4080
         MaxLength       =   9
         TabIndex        =   0
         Top             =   480
         Width           =   1320
      End
      Begin VB.TextBox txtbuscar 
         Height          =   330
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   3735
      End
      Begin VB.Frame Frame3 
         Caption         =   "Buscar en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   5640
         TabIndex        =   45
         Top             =   240
         Width           =   4095
         Begin VB.CheckBox Check3 
            Caption         =   "C�d.Inter."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   103
            Top             =   240
            Width           =   1215
         End
         Begin VB.CheckBox chkproducto 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   47
            Top             =   240
            Width           =   1215
         End
         Begin VB.CheckBox chkproveedor 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2640
            TabIndex        =   48
            Top             =   240
            Width           =   1335
         End
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "&Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9840
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox txtpersona 
         BackColor       =   &H00FFFFFF&
         DataField       =   "Dname"
         Height          =   330
         HelpContextID   =   40102
         Left            =   4080
         TabIndex        =   56
         Top             =   1080
         Width           =   2055
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDesde 
         Height          =   330
         Left            =   7080
         TabIndex        =   54
         Top             =   1320
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcpedido 
         Height          =   330
         Left            =   2040
         TabIndex        =   52
         Top             =   1080
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcentrega 
         Height          =   330
         Left            =   120
         TabIndex        =   50
         Top             =   1080
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcHasta 
         Height          =   330
         Left            =   9720
         TabIndex        =   87
         Top             =   1320
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin VB.Label Label2 
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   9120
         TabIndex        =   89
         Top             =   1440
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   6480
         TabIndex        =   88
         Top             =   1440
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "N� Pedido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   4080
         TabIndex        =   76
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha Entrega"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   51
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Fecha Pedido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2040
         TabIndex        =   49
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Persona que Pide"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   4080
         TabIndex        =   46
         Top             =   840
         Width           =   1935
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3315
      Index           =   1
      Left            =   120
      TabIndex        =   31
      Top             =   4800
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2940
         Index           =   1
         Left            =   120
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   240
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   5186
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0522.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(1)"
         Tab(0).Control(1)=   "cmdInListAll(1)"
         Tab(0).Control(2)=   "cmdCleanList(1)"
         Tab(0).Control(3)=   "cmdViewList(1)"
         Tab(0).Control(4)=   "cmdOutList(1)"
         Tab(0).Control(5)=   "cmdInList(1)"
         Tab(0).Control(6)=   "txtText1(29)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(28)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(27)"
         Tab(0).Control(9)=   "txtText1(26)"
         Tab(0).Control(10)=   "txtText1(25)"
         Tab(0).Control(11)=   "txtText1(5)"
         Tab(0).Control(12)=   "txtbonificacion"
         Tab(0).Control(13)=   "txtText1(23)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(24)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "Frame5"
         Tab(0).Control(16)=   "txtText1(21)"
         Tab(0).Control(17)=   "txtText1(19)"
         Tab(0).Control(18)=   "txtText1(18)"
         Tab(0).Control(19)=   "txtText1(17)"
         Tab(0).Control(20)=   "txtText1(15)"
         Tab(0).Control(21)=   "txtText1(4)"
         Tab(0).Control(22)=   "txtText1(3)"
         Tab(0).Control(23)=   "txtText1(2)"
         Tab(0).Control(24)=   "txtText1(14)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(12)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(10)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(13)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(11)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "dtcDateCombo1(0)"
         Tab(0).Control(30)=   "lblLabel1(24)"
         Tab(0).Control(31)=   "lblLabel1(12)"
         Tab(0).Control(32)=   "lblLabel1(23)"
         Tab(0).Control(33)=   "lblLabel1(22)"
         Tab(0).Control(34)=   "lblLabel1(21)"
         Tab(0).Control(35)=   "lblLabel1(20)"
         Tab(0).Control(36)=   "lblLabel1(19)"
         Tab(0).Control(37)=   "lblLabel1(13)"
         Tab(0).Control(38)=   "lblLabel1(5)"
         Tab(0).Control(39)=   "lblLabel1(1)"
         Tab(0).Control(40)=   "lblLabel1(56)"
         Tab(0).Control(41)=   "lblLabel1(57)"
         Tab(0).Control(42)=   "lblLabel1(9)"
         Tab(0).Control(43)=   "lblLabel1(4)"
         Tab(0).Control(44)=   "lblLabel1(3)"
         Tab(0).Control(45)=   "lblLabel1(2)"
         Tab(0).Control(46)=   "lblLabel1(0)"
         Tab(0).Control(47)=   "lblLabel1(14)"
         Tab(0).Control(48)=   "lblLabel1(15)"
         Tab(0).Control(49)=   "lblLabel1(16)"
         Tab(0).Control(50)=   "lblLabel1(17)"
         Tab(0).Control(51)=   "lblLabel1(18)"
         Tab(0).ControlCount=   52
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0522.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "cmdInList(0)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "cmdOutList(0)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "cmdViewList(0)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "cmdCleanList(0)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "cmdInListAll(0)"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).ControlCount=   6
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25UDESBONIF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   -65400
            Locked          =   -1  'True
            TabIndex        =   107
            Tag             =   "Pte.Bonif.||N� de unidades No Bonificadas; NO es el n� de envases"
            Top             =   960
            Width           =   1215
         End
         Begin VB.CommandButton cmdInListAll 
            Caption         =   "Pedido Completo"
            Height          =   375
            Index           =   1
            Left            =   -73080
            TabIndex        =   101
            ToolTipText     =   "A�ade todo el pedido a la lista"
            Top             =   2520
            Width           =   1575
         End
         Begin VB.CommandButton cmdInListAll 
            Caption         =   "Pedido Comp&leto"
            Height          =   375
            Index           =   0
            Left            =   1920
            TabIndex        =   100
            ToolTipText     =   "A�ade todo el pedido a la lista"
            Top             =   2520
            Width           =   1575
         End
         Begin VB.CommandButton cmdCleanList 
            Caption         =   "Vaciar Li&sta"
            Height          =   375
            Index           =   0
            Left            =   8400
            TabIndex        =   99
            ToolTipText     =   "Vacia la lista de los producto del albar�n"
            Top             =   2520
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.CommandButton cmdCleanList 
            Caption         =   "Vaciar Lista"
            Height          =   375
            Index           =   1
            Left            =   -66000
            TabIndex        =   98
            ToolTipText     =   "Vacia la lista de los producto del albar�n"
            Top             =   2520
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.CommandButton cmdViewList 
            Caption         =   "Ver Lista"
            Height          =   375
            Index           =   1
            Left            =   -68520
            TabIndex        =   95
            ToolTipText     =   "Muestra los producto del albar�n"
            Top             =   2520
            Width           =   1335
         End
         Begin VB.CommandButton cmdOutList 
            Caption         =   "Quitar"
            Height          =   375
            Index           =   1
            Left            =   -69960
            TabIndex        =   94
            ToolTipText     =   "Elimina un producto al albar�n"
            Top             =   2520
            Width           =   1335
         End
         Begin VB.CommandButton cmdInList 
            Caption         =   "Pedido Parcial"
            Height          =   375
            Index           =   1
            Left            =   -71400
            TabIndex        =   93
            ToolTipText     =   "A�ade un producto al albar�n"
            Top             =   2520
            Width           =   1335
         End
         Begin VB.CommandButton cmdViewList 
            Caption         =   "&Ver Lista"
            Height          =   375
            Index           =   0
            Left            =   6480
            TabIndex        =   92
            ToolTipText     =   "Muestra los producto del albar�n"
            Top             =   2520
            Width           =   1335
         End
         Begin VB.CommandButton cmdOutList 
            Caption         =   "&Quitar"
            Height          =   375
            Index           =   0
            Left            =   5040
            TabIndex        =   91
            ToolTipText     =   "Elimina un producto al albar�n"
            Top             =   2520
            Width           =   1335
         End
         Begin VB.CommandButton cmdInList 
            Caption         =   "Ped&ido Parcial"
            Height          =   375
            Index           =   0
            Left            =   3600
            TabIndex        =   90
            ToolTipText     =   "A�ade un producto al albar�n"
            Top             =   2520
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   29
            Left            =   -65040
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Envase|Unidades por envase"
            Top             =   360
            Width           =   945
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25PRECNET"
            Height          =   330
            HelpContextID   =   30104
            Index           =   28
            Left            =   -71760
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Prec.Neto|Precio Neto"
            Top             =   960
            Width           =   2100
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR88CODTIPIVA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   27
            Left            =   -72480
            TabIndex        =   19
            Tag             =   "IVA|%IVA"
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25BONIF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   26
            Left            =   -73560
            TabIndex        =   18
            Tag             =   "%Bonif|%Bonificaci�n"
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25DESCUENTO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   25
            Left            =   -73560
            TabIndex        =   17
            Tag             =   "%Desc|%Descuento"
            Top             =   1560
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25CANTENT"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   -66720
            TabIndex        =   14
            Tag             =   "Cant Acum|Cantudad recibida hasta la fecha"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtbonificacion 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40102
            Left            =   -74880
            Locked          =   -1  'True
            TabIndex        =   24
            Tag             =   "Bonificaci�n||N� de unidades pedidas; NO es el n� de envases"
            Top             =   1560
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   -74160
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   360
            Width           =   300
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   -74880
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "C�d.Interno"
            Top             =   360
            Width           =   720
         End
         Begin VB.Frame Frame5 
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   -67560
            TabIndex        =   33
            Top             =   1560
            Width           =   3495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25coddetpedcomp"
            Height          =   330
            HelpContextID   =   40102
            Index           =   21
            Left            =   -64680
            MaxLength       =   13
            TabIndex        =   97
            Tag             =   "CDP|C�digo Detalle Pedido"
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40102
            Index           =   19
            Left            =   -65280
            MaxLength       =   13
            TabIndex        =   96
            Tag             =   "CP|C�dido Pedido"
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "frh8moneda"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   -67440
            MaxLength       =   13
            TabIndex        =   23
            Tag             =   "Moneda"
            Top             =   960
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25imporlinea"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   -69600
            TabIndex        =   21
            Tag             =   "Importe"
            Top             =   960
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25cantpedida"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   -74880
            TabIndex        =   13
            Tag             =   "Cant Pedida||N� de envases pedidos"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr25preciounidad"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   -73560
            TabIndex        =   16
            Tag             =   "Precio Unidad"
            Top             =   960
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   -73800
            TabIndex        =   7
            Tag             =   "Descripci�n Producto"
            Top             =   360
            Width           =   4125
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr73codproducto"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   -67080
            TabIndex        =   2
            Tag             =   "C�d.Prod"
            Top             =   1920
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   -66720
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   360
            Width           =   1545
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   -67800
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   360
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   -69000
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   -69600
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "F.F"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   -68400
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "U.M"
            Top             =   360
            Width           =   540
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2355
            Index           =   1
            Left            =   120
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   120
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   4154
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR25FECPLAZENTRE"
            Height          =   330
            Index           =   0
            Left            =   -69600
            TabIndex        =   104
            Tag             =   "Plazo de entrega"
            Top             =   1560
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "PteBonf.(Udes.)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   -65400
            TabIndex        =   108
            Top             =   720
            Width           =   1365
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Plazo de Entrega"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   -69600
            TabIndex        =   105
            Top             =   1320
            Width           =   1470
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Envase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   -65040
            TabIndex        =   85
            Top             =   120
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   -71760
            TabIndex        =   84
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA(%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   -72480
            TabIndex        =   83
            Top             =   1920
            Width           =   570
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonif.(%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   -73560
            TabIndex        =   82
            Top             =   1920
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descuento(%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   -73560
            TabIndex        =   81
            Top             =   1320
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cant.Recibida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   -66720
            TabIndex        =   79
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Unidad.Bonif."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   -74880
            TabIndex        =   77
            Tag             =   "Bonificaci�n"
            Top             =   1320
            Width           =   1170
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   -73800
            TabIndex        =   75
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   -74160
            TabIndex        =   74
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Int."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   -74880
            TabIndex        =   73
            Top             =   120
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   -67440
            TabIndex        =   55
            Top             =   720
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   -69600
            TabIndex        =   42
            Top             =   720
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cant.Ped.(E)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   -74880
            TabIndex        =   41
            Tag             =   "Cant.Pedida"
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Base - PVL"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   -73560
            TabIndex        =   40
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   -67080
            TabIndex        =   39
            Top             =   1680
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   -66720
            TabIndex        =   38
            Top             =   120
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Vol. (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   -67800
            TabIndex        =   37
            Top             =   120
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   -68400
            TabIndex        =   36
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   -69000
            TabIndex        =   35
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   -69600
            TabIndex        =   34
            Top             =   120
            Width           =   345
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedido de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Index           =   0
      Left            =   120
      TabIndex        =   30
      Top             =   2280
      Width           =   9705
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   57
         TabStop         =   0   'False
         Top             =   360
         Width           =   9450
         _ExtentX        =   16669
         _ExtentY        =   3201
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0522.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(7)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(22)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(16)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Frame4"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(9)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(8)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(6)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(20)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "Frame6"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(7)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0522.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "fr62descpersonalizada"
            Height          =   615
            Index           =   7
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   67
            Tag             =   "Descripci�n Personalizada"
            Top             =   960
            Width           =   8385
         End
         Begin VB.Frame Frame6 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   7200
            TabIndex        =   78
            Top             =   1080
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "sg02cod_pes"
            Height          =   330
            HelpContextID   =   40102
            Index           =   20
            Left            =   8160
            MaxLength       =   13
            TabIndex        =   62
            Tag             =   "C�digo Estudio"
            Top             =   1320
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   120
            TabIndex        =   4
            Tag             =   "N� Pedido"
            Top             =   360
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr79codproveedor"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   3240
            TabIndex        =   61
            Tag             =   "C�d.Prov"
            Top             =   360
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   3960
            TabIndex        =   63
            Tag             =   "Proveedor"
            Top             =   360
            Width           =   2850
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62indrepre"
            Height          =   330
            HelpContextID   =   40102
            Index           =   0
            Left            =   7560
            MaxLength       =   13
            TabIndex        =   59
            Tag             =   "Representante"
            Top             =   1320
            Width           =   495
         End
         Begin VB.Frame Frame4 
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   8640
            TabIndex        =   58
            Top             =   1080
            Width           =   375
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR95CODESTPEDCOMPRA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   16
            Left            =   6840
            TabIndex        =   64
            Tag             =   "C�d.Estado"
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   7200
            TabIndex        =   66
            Tag             =   "Estado"
            Top             =   360
            Width           =   1890
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr62fecpedcompra"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   60
            Tag             =   "Fecha Pedido"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1530
            Index           =   0
            Left            =   -74880
            TabIndex        =   65
            Top             =   120
            Width           =   8805
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15531
            _ExtentY        =   2699
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   70
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   72
            Top             =   120
            Width           =   870
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1320
            TabIndex        =   71
            Top             =   120
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   3240
            TabIndex        =   69
            Top             =   120
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   6840
            TabIndex        =   68
            Top             =   120
            Width           =   600
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1695
      Left            =   10080
      TabIndex        =   26
      Top             =   2640
      Width           =   1575
      Begin VB.CommandButton cmdEmitir 
         Caption         =   "Emi&tir"
         Height          =   375
         Left            =   120
         TabIndex        =   29
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   120
         TabIndex        =   28
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton cmdReclamar 
         Caption         =   "Recla&mar"
         Height          =   375
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   1335
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmConSitPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0522.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: ABRIL 1999                                                    *
'* DESCRIPCION: Consultar Situaci�n Pedido                              *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnActCantPed As Boolean
Dim mblnActCantPend As Boolean
Dim mblnTamEnva As Boolean
Dim mdblCantPed As Double
Dim mdblCantPend As Double
Dim mdblCodProd As Double
Dim mdblTamEnva As Double
Dim mstrWhere As String


Private Sub calcular_precio_neto()
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset
Dim strPrecNetComp As String
Dim strupdate As String
Dim strPrecVenta As String
Dim strRecProd As String
Dim rstRecProd As rdoResultset
Dim qryFR73 As rdoQuery


'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
If txtText1(4).Text <> "" Then 'Precio_Base
  Precio_Base = txtText1(4).Text
Else
  Precio_Base = 0
End If
If txtText1(25).Text <> "" Then 'Descuento
  Descuento = Precio_Base * txtText1(25).Text / 100
Else
  Descuento = 0
End If

If txtText1(2).Text <> "" Then 'Recargo
  strRecProd = "SELECT FR73RECARGO FROM FR7300 WHERE FR73CODPRODUCTO =?"
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strRecProd)
  qryFR73(0) = txtText1(2).Text
  Set rstRecProd = qryFR73.OpenResultset()
  If Not IsNull(rstRecProd.rdoColumns("FR73RECARGO").Value) Then
    Recargo = Precio_Base * rstRecProd.rdoColumns("FR73RECARGO").Value / 100
  Else
    Recargo = 0
  End If
  qryFR73.Close
  Set qryFR73 = Nothing
  Set rstRecProd = Nothing
Else
  Recargo = 0
End If

If txtText1(27).Text <> "" Then 'IVA
  IVA = (Precio_Base - Descuento + Recargo) * txtText1(27).Text / 100
Else
  IVA = 0
End If
txtText1(28).Text = Precio_Base - Descuento + IVA + Recargo
If txtText1(28).Text <> Format(txtText1(28).Text, "0.00") Then
  txtText1(28).Text = Format(txtText1(28).Text, "0.00")
End If

End Sub



Private Sub cmdbuscar_Click()
Dim strClausulaWhere As String
Dim strFechaDesde As String
Dim strFechaHasta As String
Dim blnMostrarSoloPtes As Boolean

cmdbuscar.Enabled = False

strClausulaWhere = "-1=-1 "
blnMostrarSoloPtes = True
'se localiza un n� de pedido concreto
If IsNumeric(txtpedido.Text) Then
   strClausulaWhere = " FR62CODPEDCOMPRA=" & txtpedido.Text
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    blnMostrarSoloPtes = True
    'objWinInfo.objWinActiveForm.strWhere = strclausulawhere
    'Call objWinInfo.DataRefresh
    'cmdbuscar.Enabled = True
    'Exit Sub
End If

If Check2.Value = Checked Then
  'Se buscan pedidos para reclamar
  strClausulaWhere = strClausulaWhere & " AND FR95CODESTPEDCOMPRA IN (2,3,4) AND FR62FECESTUPED < SYSDATE-5 "
End If
If chkproveedor.Value = 1 Then
    If Trim(txtbuscar.Text) <> "" Then
        strClausulaWhere = strClausulaWhere & " AND FR79CODPROVEEDOR IN (" & _
            "SELECT FR79CODPROVEEDOR from fr7900 where " & _
            "upper(FR79PROVEEDOR) like upper('" & txtbuscar.Text & "%')" & _
            ")"
    blnMostrarSoloPtes = True
    End If
End If

If Check4.Value = Checked Then
    strClausulaWhere = strClausulaWhere & " AND FR95CODESTPEDCOMPRA = 3 "
End If

If Check3.Value = Checked Then
  If Trim(txtbuscar.Text) <> "" Then
    strClausulaWhere = strClausulaWhere & "  AND FR62CODPEDCOMPRA IN (" & _
            "SELECT FR62CODPEDCOMPRA FROM FR2500 WHERE FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR = '" & Trim(txtbuscar.Text) & "')) "
  End If
End If

If chkproducto.Value = 1 Then
    If txtbuscar.Text <> "" Then
        strClausulaWhere = strClausulaWhere & " AND FR62CODPEDCOMPRA IN (" & _
            "SELECT FR62CODPEDCOMPRA FROM FR2500 WHERE FR73CODPRODUCTO IN (" & _
            "SELECT FR73CODPRODUCTO from fr7300 where " & _
             "(upper(FR73DESPRODUCTOPROV) like upper('" & txtbuscar.Text & "%') OR upper(FR73DESPRODUCTO) like upper('" & txtbuscar.Text & "%'))" & _
             ")" & _
             ")"
    blnMostrarSoloPtes = True
    End If
End If

'fecha de pedido
If dtcpedido.Text <> "" Then
    strClausulaWhere = strClausulaWhere & " AND TO_CHAR(FR62FECPEDCOMPRA,'DD/MM/YYYY')=" & "'" & dtcpedido.Text & "'"
    blnMostrarSoloPtes = True
End If
'fecha de estudio
'If dtcestudio.Text <> "" Then
'    strclausulawhere = strclausulawhere & " AND TO_CHAR(FR62FECESTUPED,'DD/MM/YY')=" & "'" & dtcestudio.Text & "'"
'End If
'fecha de entrega
If dtcentrega.Text <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR62CODPEDCOMPRA IN (" & _
        "SELECT FR62CODPEDCOMPRA FROM FR2500 WHERE " & _
         "TO_CHAR(FR25FECPLAZENTRE,'DD/MM/YYYY')=" & "'" & dtcentrega.Text & "'" & _
         ")"
  blnMostrarSoloPtes = True
End If
    
'persona que pide
If txtpersona.Text <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR62CODPEDCOMPRA IN (" & _
        "SELECT FR62CODPEDCOMPRA FROM FR6200 WHERE SG02COD_PID IN (" & _
        "SELECT SG02COD FROM SG0200 WHERE " & _
        "upper(SG02APE1) like upper('" & txtpersona.Text & "')" & _
        ")" & _
        ")"
  blnMostrarSoloPtes = True
End If

If Check1.Value = 1 Then
  'Hay que ver los pedidos completos
  blnMostrarSoloPtes = False
  strClausulaWhere = strClausulaWhere & " AND  FR95CODESTPEDCOMPRA IN (1,2,3,4,5) AND (FR95CODESTPEDCOMPRA <> 0) "
  If Trim(dtcDesde.Text) <> "" Then
     strFechaDesde = convFecha(dtcDesde.Text)
     If strFechaDesde <> "Error" Then
       strClausulaWhere = strClausulaWhere & " AND TO_CHAR(FR62FECPEDCOMPRA,'DD/MM/YYYY')>=" & "'" & strFechaDesde & "'"
     End If
  End If
  If Trim(dtcHasta.Text) <> "" Then
    strFechaHasta = convFecha(dtcHasta.Text)
    If strFechaHasta <> "Error" Then
      strClausulaWhere = strClausulaWhere & " AND TO_CHAR(FR62FECPEDCOMPRA,'DD/MM/YYYY')<=" & "'" & strFechaHasta & "'"
    End If
  End If
End If

If blnMostrarSoloPtes = True Then
  strClausulaWhere = strClausulaWhere & " AND (FR95CODESTPEDCOMPRA <> 0 AND FR95CODESTPEDCOMPRA <> 5 AND FR95CODESTPEDCOMPRA <> 6 AND FR95CODESTPEDCOMPRA <> 7 ) "
End If


Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
If Check1.Value = 1 Then
   objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
Else
   objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
End If

If Check1.Value = 1 Then
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.objWinActiveForm.strWhere = ""
  objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
  'Call objWinInfo.DataRefresh
Else
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.objWinActiveForm.strWhere = mstrWhere
  objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
  'Call objWinInfo.DataRefresh
End If

Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
Call objWinInfo.DataRefresh

cmdbuscar.Enabled = True
End Sub

Private Sub cmdCancelar_Click()
    Dim strupdate As String
    Dim qryUpd As rdoQuery
    Me.Enabled = False
    tabTab1(0).Tab = 0
    If (txtText1(6).Text <> "") And ((txtText1(16).Text = 1) Or (txtText1(16).Text = 2) Or (txtText1(16).Text = 2)) Then
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataSave
        If txtText1(16).Text <> 1 Then
          'Se descuenta de la ficha del producto
          Dim strUpdFR73 As String
          Dim strFR25 As String
          Dim qryFR25 As rdoQuery
          Dim rstFR25 As rdoResultset
          Dim strPed As String
          Dim strBon As String
          
          strFR25 = "SELECT * " & _
                    "  FROM FR2500 " & _
                    " WHERE FR62CODPEDCOMPRA = ? "
          Set qryFR25 = objApp.rdoConnect.CreateQuery("", strFR25)
          qryFR25(0) = txtText1(6).Text
          Set rstFR25 = qryFR25.OpenResultset()
          While Not rstFR25.EOF
            If Not IsNull(rstFR25.rdoColumns("FR25CANTPEDIDA").Value) Then
              strPed = rstFR25.rdoColumns("FR25CANTPEDIDA").Value * rstFR25.rdoColumns("FR25TAMENVASE").Value
            Else
              strPed = "0"
            End If
            If Not IsNull(rstFR25.rdoColumns("FR25UDESBONIF").Value) Then
              strBon = rstFR25.rdoColumns("FR25UDESBONIF").Value
            Else
              strBon = "0"
            End If
            
            strUpdFR73 = "UPDATE FR7300 " & _
                         "   SET FR73CANTPEND = FR73CANTPEND - " & strPed & " ," & _
                         "       FR73PTEBONIF = FR73PTEBONIF - " & strBon & _
                         "  WHERE FR73CODPRODUCTO = " & rstFR25.rdoColumns("FR73CODPRODUCTO").Value
            objApp.rdoConnect.Execute strUpdFR73, 64
            rstFR25.MoveNext
          Wend
        End If
        strupdate = "UPDATE FR6200 SET FR95CODESTPEDCOMPRA=0 where fr62codpedcompra=?"
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
        qryUpd(0) = txtText1(6).Text
        qryUpd.Execute
        qryUpd.Close
        Set qryUpd = Nothing
        MsgBox "Pedido n� " & txtText1(6).Text & " Cancelado", vbExclamation, "Cancelar"
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        objWinInfo.DataRefresh
    End If
    Me.Enabled = True
End Sub

Private Sub cmdCleanList_Click(Index As Integer)
  'Se vacia la lista
  Dim intResp As Integer
  Dim intInd As Integer
  
  intResp = MsgBox("�Est� seguro que quiere vaciar la lista?", vbYesNo, "Vaciar la lista")
  
  Select Case intResp
    Case 6 'S� quiere vaciar la lista
      For intInd = 1 To 250
        gaListAlb(intInd, 1) = 0
        gaListAlb(intInd, 2) = 0
        gaListAlb(intInd, 3) = 0
      Next intInd
      gintContAlb = 0
      MsgBox "La lista ha sido vaciada", vbInformation, "Vaciar la lista"
    Case 7 'No quiere vaciar la lista
  End Select
End Sub

Private Sub cmdEmitir_Click()
    'Dim strUpdate As String
    Me.Enabled = False
    If txtText1(6).Text <> "" Then
        'strUpdate = "UPDATE FR6200 SET FR62FECESTUPEC=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                    "FR95ESTPEDCOMPRA=2 where fr62codpedcompra=" & txtText1(6).Text
        'objApp.rdoConnect.Execute strUpdate, 64
        If txtText1(8).Text <> "" Then
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataSave
            gintProveedor(1) = txtText1(8).Text
            gintProveedor(2) = txtText1(6).Text
            gstrLlamador = "FrmConSitPed"
            Call objsecurity.LaunchProcess("FR0524")
            Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
            objWinInfo.DataRefresh
        Else
            Call MsgBox("No hay ning�n Proveedor", vbExclamation, "Aviso")
        End If
    End If
    Me.Enabled = True
End Sub

Private Sub cmdInList_Click(Index As Integer)
  'Se a�ade el producto a la lista con la que se va a construir el albar�n
  Dim blnYaEsta As Boolean
  Dim intInd As Integer
  Dim mintNTotalSelRows As Integer
  Dim mvarBkmrk As Variant
  Dim intInd2 As Integer
  Dim blnAna As Boolean
    
  If IsNumeric(txtText1(16).Text) Then
    If txtText1(16).Text = 2 Or txtText1(16).Text = 3 Or txtText1(16).Text = 4 Then
    Else
      MsgBox "El pedido no se puede recibir", vbInformation, "A�adir todo el pedido"
      Exit Sub
    End If
  End If
    
  blnAna = False
  Select Case Index
    Case 0 'Modo Tabla
      'Se comprueba que ha seleccionado un producto
      mintNTotalSelRows = 0
      'Guardamos el n�mero de filas seleccionadas
      mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
      For intInd2 = 0 To mintNTotalSelRows - 1
        'Ha seleccionado alguna fila
        'Guardamos el n�mero de fila que est� seleccionada
        mvarBkmrk = grdDBGrid1(1).SelBookmarks(intInd2)
        'Se comprueba que no est� a�adido
        blnYaEsta = False
        For intInd = 1 To gintContAlb
          'Se comprueba si la l�nea de pedido ya estaba a�adida
          If (gaListAlb(intInd, 1) = grdDBGrid1(1).Columns("CP").CellValue(mvarBkmrk)) And _
             (gaListAlb(intInd, 2) = grdDBGrid1(1).Columns("CDP").CellValue(mvarBkmrk)) Then
            gaListAlb(intInd, 3) = -1
            'MsgBox "La l�nea de pedido se ha a�adido " & _
            '      "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
            blnAna = True
            blnYaEsta = True
            Exit For
          End If
        Next intInd
        'No estaba en la lista y se a�ade
        If blnYaEsta = False Then
          If gintContAlb > 0 Then
            For intInd = 0 To gintContAlb
              If (gaListAlb(intInd, 3) = -1) And (gaListAlb(intInd, 0) <> txtText1(8).Text) Then
                MsgBox "No se puede crear un albar�n de diferentes proveedores", vbInformation, "A�adir al albar�n"
                Exit Sub
              End If
            Next intInd
          End If
          If (grdDBGrid1(1).Columns("Cant Acum").CellValue(mvarBkmrk) >= _
             grdDBGrid1(1).Columns("Cant Pedida").CellValue(mvarBkmrk)) And (grdDBGrid1(1).Columns("Pte.Bonif.").CellValue(mvarBkmrk) = 0) Then
             MsgBox "La l�nea de pedido ha sido totalmente servida", vbInformation, "A�adir al albar�n"
             Exit Sub
          Else
            gintContAlb = gintContAlb + 1
            gaListAlb(gintContAlb, 0) = txtText1(8).Text 'C�digo Proveedor
            gaListAlb(gintContAlb, 1) = grdDBGrid1(1).Columns("CP").CellValue(mvarBkmrk) 'c�digo petici�n
            gaListAlb(gintContAlb, 2) = grdDBGrid1(1).Columns("CDP").CellValue(mvarBkmrk) 'c�digo detalle petici�n
            gaListAlb(gintContAlb, 3) = -1 'Estado en el array
            'MsgBox "La l�nea de pedido se ha a�adido " & _
            '      "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
            blnAna = True
          End If
        End If
      Next intInd2
      'Else
      If mintNTotalSelRows <= 0 Then
        'No ha seleccionado ninguna fila
        MsgBox "No ha seleccionado ning�n producto", vbInformation, "A�adir al albar�n"
      End If
    Case 1 'Modo Detalle
      If IsNumeric(txtText1(8).Text) Then
        If gintContAlb < 250 Then
          blnYaEsta = False
          For intInd = 1 To gintContAlb
            'Se comprueba si la l�nea de pedido ya estaba a�adida
            If (gaListAlb(intInd, 1) = txtText1(19).Text) And _
               (gaListAlb(intInd, 2) = txtText1(21).Text) Then
              gaListAlb(intInd, 3) = -1
              'MsgBox "La l�nea de pedido se ha a�adido " & Chr(13) & _
              '   "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
              blnAna = True
              blnYaEsta = True
              Exit For
            End If
          Next intInd
          If blnYaEsta = False Then
            If gintContAlb > 0 Then
              For intInd = 0 To gintContAlb
                If (gaListAlb(intInd, 3) = -1) And (gaListAlb(intInd, 0) <> txtText1(8).Text) Then
                  MsgBox "No se puede crear un albar�n de diferentes proveedores", vbInformation, "A�adir al albar�n"
                  Exit Sub
                End If
              Next intInd
            End If
            If txtText1(5).Text >= txtText1(15).Text And txtText1(1).Text = 0 Then
              MsgBox "La l�nea de pedido ha sido totalmente servida", vbInformation, "A�adir al albar�n"
              Exit Sub
            Else
              gintContAlb = gintContAlb + 1
              gaListAlb(gintContAlb, 0) = txtText1(8).Text 'C�digo Proveedor
              gaListAlb(gintContAlb, 1) = txtText1(19).Text 'C�digo pedido compra
              gaListAlb(gintContAlb, 2) = txtText1(21).Text 'C�digo detalle pedido compra
              gaListAlb(gintContAlb, 3) = -1 'Estado en el array
              'MsgBox "La l�nea de pedido se ha a�adido " & Chr(13) & _
              '       "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
              blnAna = True
            End If
          End If
        Else
          MsgBox "Ya ha a�adido 250 productos al albar�n", vbInformation, "A�adir al albar�n"
        End If
      Else
        MsgBox "No ha seleccionado ning�n producto", vbInformation, "A�adir al albar�n"
      End If
  End Select
  If blnAna Then
    MsgBox "La l�nea de pedido se ha a�adido " & _
           "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
  End If
   
End Sub

Private Sub cmdInListAll_Click(Index As Integer)
  'A�ade todo el pedido a la lista
  Dim strPed As String
  Dim qryPed As rdoQuery
  Dim rstPed As rdoResultset
  Dim intInd As Integer
  Dim blnYaEsta As Boolean
  Dim blnA�adido As Boolean
    
  If IsNumeric(txtText1(16).Text) Then
    If txtText1(16).Text = 2 Or txtText1(16).Text = 3 Or txtText1(16).Text = 4 Then
    Else
      MsgBox "El pedido no se puede recibir", vbInformation, "A�adir todo el pedido"
      Exit Sub
    End If
  End If
  If grdDBGrid1(1).SelBookmarks.Count > 0 Then
    MsgBox "Para a�adir al albar�n las l�neas seleccionadas debe hacer clic el bot�n Pedido Parcial", vbInformation, "A�adir todo el pedido"
    Exit Sub
  End If
  blnA�adido = False
  If IsNumeric(txtText1(6).Text) Then
    'Hay un pedido
    For intInd = 0 To gintContAlb
      If (gaListAlb(intInd, 3) = -1) And (gaListAlb(intInd, 0) <> txtText1(8).Text) Then
        MsgBox "No se puede crear un albar�n de diferentes proveedores", vbInformation, "A�adir al albar�n"
        Exit Sub
      End If
    Next intInd
            
    strPed = "SELECT * " & _
             "  FROM FR2500 " & _
             " WHERE FR62CODPEDCOMPRA = ? " & _
             "   AND ((FR25CANTPEDIDA > FR25CANTENT) OR ( FR25CANTENT IS NULL) OR (FR25UDESBONIF >0))"
    Set qryPed = objApp.rdoConnect.CreateQuery("", strPed)
    qryPed(0) = txtText1(6).Text
    Set rstPed = qryPed.OpenResultset()
    While Not rstPed.EOF
      'Para cada l�nea del pedido
      blnYaEsta = False
      For intInd = 1 To gintContAlb
        'Se comprueba si la l�nea de pedido ya estaba a�adida
        If (gaListAlb(intInd, 1) = rstPed.rdoColumns("FR62CODPEDCOMPRA").Value) And _
          (gaListAlb(intInd, 2) = rstPed.rdoColumns("FR25CODDETPEDCOMP").Value) Then
          gaListAlb(intInd, 3) = -1
          blnA�adido = True
          'MsgBox "La l�nea de pedido se ha a�adido " & _
          '       "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
          blnYaEsta = True
          Exit For
        End If
      Next intInd
      If blnYaEsta = False Then
        'Se a�ade a la lista
        gintContAlb = gintContAlb + 1
        gaListAlb(gintContAlb, 0) = txtText1(8).Text 'C�digo Proveedor
        gaListAlb(gintContAlb, 1) = rstPed.rdoColumns("FR62CODPEDCOMPRA").Value 'c�digo petici�n
        gaListAlb(gintContAlb, 2) = rstPed.rdoColumns("FR25CODDETPEDCOMP").Value 'c�digo detalle petici�n
        gaListAlb(gintContAlb, 3) = -1 'Estado en el array
        blnA�adido = True
        'MsgBox "La l�nea de pedido se ha a�adido " & _
        '       "a la lista con la que har� el albar�n", vbInformation, "A�adir al albar�n"
      End If
      rstPed.MoveNext
    Wend
    qryPed.Close
    Set qryPed = Nothing
    Set rstPed = Nothing
  End If
  If blnA�adido = True Then
    MsgBox "El pedido ha sido a�adido a la lista", vbInformation, "A�adir a la lista"
  End If
  
End Sub

Private Sub cmdintrecepcion_Click()
  Dim intInd As Integer
  Dim blnListaVacia As Boolean
  Me.Enabled = False
  blnListaVacia = True
  cmdintrecepcion.Enabled = False
  If gintContAlb = 0 Then
    MsgBox "No hay productos para crear el albar�n", vbInformation, "Recibir pedido"
  Else
    For intInd = 0 To gintContAlb
      If gaListAlb(intInd, 3) = -1 Then
        blnListaVacia = False
        Exit For
      End If
    Next intInd
    
    If blnListaVacia = True Then
      MsgBox "No hay productos para crear el albar�n", vbInformation, "Recibir pedido"
    Else
      gstrCodProv = txtText1(8).Text
      If IsNumeric(txtText1(6).Text) Then
        gintPedidoRecibido = txtText1(6).Text
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataSave
        Call objsecurity.LaunchProcess("FR0525")
        For intInd = 0 To gintContAlb
          gaListAlb(intInd, 3) = 0
        Next intInd
        gintContAlb = 0
        gintPedidoRecibido = ""
        DoEvents
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        objWinInfo.DataRefresh
      End If
    End If
  End If
  
  'gstrCodProv = Null
Me.Enabled = True
cmdintrecepcion.Enabled = True
End Sub

Private Sub cmdOutList_Click(Index As Integer)
  'Se elimina el producto seleccionado de la lsita con la que se construir� el albar�n
  Dim intInd As Integer
  Dim intResp As Integer
  Dim mintNTotalSelRows As Integer
  Dim mvarBkmrk As Variant
  
  Select Case Index
  Case 0 'Modo Tabla
    'Se comprueba que ha seleccionado un producto
    mintNTotalSelRows = 0
    'Guardamos el n�mero de filas seleccionadas
    mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
    If mintNTotalSelRows > 0 Then
      'Ha seleccionado alguna fila
      For intInd = 1 To gintContAlb
        If (gaListAlb(intInd, 1) = grdDBGrid1(1).Columns("CP").CellValue(mvarBkmrk)) And _
           (gaListAlb(intInd, 2) = grdDBGrid1(1).Columns("CDP").CellValue(mvarBkmrk)) Then
          'Se ha encontrado el elemento aliminar de la lista
          gaListAlb(intInd, 3) = 0
          intResp = MsgBox("El producto ha sido eliminado de la lista", vbInformation, "Quitar del albar�n")
          Exit For
        End If
      Next intInd
    Else
      MsgBox "No ha seleccionado ning�n producto", vbInformation, "Quitar del albar�n"
    End If
      
  Case 1 'Modo Detalle
    If (Not IsNull(txtText1(19).Text)) And (Not IsNull(txtText1(19).Text)) Then
      For intInd = 1 To gintContAlb
        If (gaListAlb(intInd, 1) = txtText1(19).Text) And (gaListAlb(intInd, 2) = txtText1(21).Text) Then
          'Se ha encontrado el elemento aliminar de la lista
          gaListAlb(intInd, 3) = 0
          intResp = MsgBox("El producto ha sido eliminado de la lista", vbInformation, "Quitar del albar�n")
          Exit For
        End If
      Next intInd
    End If
  End Select

End Sub

Private Sub cmdreclamar_Click()
    Dim strupdate As String
    Dim qryUpd As rdoQuery
    Me.Enabled = False
    If txtText1(6).Text <> "" Then
        gstrCodPed = txtText1(6).Text
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataSave
        strupdate = "UPDATE FR6200 SET FR95CODESTPEDCOMPRA=? where fr62codpedcompra=?"
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
        qryUpd(0) = 3
        qryUpd(1) = txtText1(6).Text
        qryUpd.Execute
        qryUpd.Close
        Set qryUpd = Nothing
        gintLlamadorReclamar = "FrmConSitPed"
        gstrLlamador = "FrmConSitPed"
        Call objsecurity.LaunchProcess("FR0523")
        gintLlamadorReclamar = ""
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        objWinInfo.DataRefresh
    End If
    Me.Enabled = True
End Sub

Private Sub cmdViewList_Click(Index As Integer)
  'Se muestra la lsita de productos a incluir en el albar�n
  cmdViewList(0).Enabled = False
  cmdViewList(1).Enabled = False
  
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataSave
  Call objsecurity.LaunchProcess("FR0556")
  
  cmdViewList(0).Enabled = True
  cmdViewList(1).Enabled = True
End Sub

'Private Sub dtcestudio_Change()''
'
'End Sub

Private Sub Form_Activate()
    txtbonificacion.Locked = True
    'gintContAlb = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------



Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  gintContAlb = 0

  Set objWinInfo = New clsCWWin
  
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Pedidos de Compra"
      
    .strTable = "FR6200"
    .strWhere = "fr95codestpedcompra<>0 AND FR95CODESTPEDCOMPRA <> 5 AND FR95CODESTPEDCOMPRA <> 7 AND FR95CODESTPEDCOMPRA <> 6"
    
    '.intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
   
    strKey = .strDataBase & .strTable
    .intAllowance = cwAllowModify
    
    Call .FormCreateFilterWhere(strKey, "Pedidos de Compra")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "N� Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62DESCPERSONALIZADA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR62FECPEDCOMPRA", "Fecha Compra", cwDate)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "Proveedor", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR95CODESTPEDCOMPRA", "Estado", cwNumeric)
        
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR62FECPEDCOMPRA", "Fecha Compra")
    
  End With
  With objDetailInfo
    .strName = "Detalle Petici�n"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2500"
    mstrWhere = "((fr25cantpedida > FR25CANTENT) OR (FR25UDESBONIF >0)) AND (fr25cantpedida > 0) "
    .strWhere = "((fr25cantpedida > FR25CANTENT) OR (FR25UDESBONIF >0)) AND (fr25cantpedida > 0) "
    .intAllowance = cwAllowModify
    '.intAllowance = cwAllowReadOnly
    .intCursorSize = 100
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
    Call .FormAddOrderField("FR25CODDETPEDCOMP", cwAscending)
    
    Call .FormAddRelation("FR62CODPEDCOMPRA", txtText1(6))
    '.intAllowance = cwAllowModify
    'Call .objPrinter.Add("PR1281", "Listado por Departamentos con sus Actuaciones")
    
    '.blnHasMaint = True
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Pedido")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CANTPEDIDA", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25IMPORLINEA", "Importe de Linea", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR50CODPRESENT", "C�digo Presentaci�n", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR65INDRECETA", "�Receta?", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR65CANTRECETA", "Cantidad Receta", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�digo V�a", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR65DOSIS", "D�sis", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido")
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    
    .CtrlGetInfo(txtbonificacion).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True 'C�d. Interno
    .CtrlGetInfo(txtText1(23)).blnReadOnly = True 'D�gito de control
    .CtrlGetInfo(txtText1(28)).blnReadOnly = True 'Precio Neto
    .CtrlGetInfo(txtText1(17)).blnReadOnly = True 'Importe
    .CtrlGetInfo(txtText1(18)).blnReadOnly = True 'Moneda
    '.CtrlGetInfo(txtText1(1)).blnReadOnly = True 'Cant. Pendiente
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True 'Acumulado recibido
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True 'Udes. pendientes de ser bonificadas
    
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    .CtrlGetInfo(txtText1(19)).blnInGrid = True
    .CtrlGetInfo(txtText1(20)).blnInGrid = False
    .CtrlGetInfo(txtText1(21)).blnInGrid = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR73DESPRODUCTOPROV")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(28), "FR73PRECIONETCOMPRA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(13), "FRH7CODFORMFAR")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(10), "FR73DOSIS")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(11), "FR93CODUNIMEDIDA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(12), "FR73VOLUMEN")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(14), "FR73REFERENCIA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "FR73PRECBASE")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(24), "FR73CODINTFAR")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "FR73CODINTFARSEG")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "FR95CODESTPEDCOMPRA", "SELECT * FROM FR9500 WHERE FR95CODESTPEDCOMPRA=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(22), "FR95DESESTPEDCOMPRA")
    
    Call .WinRegister
    Call .WinStabilize
  End With
 
 cmdEmitir.Enabled = False
 cmdReclamar.Enabled = False
 cmdCancelar.Enabled = False
 cmdintrecepcion.Enabled = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  If strFormName = "Pedidos de Compra" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     '.strWhere = "WHERE FR73FECFINVIG IS NULL" 'AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n del Proveedor "

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim intcantenvase As Integer
If grdDBGrid1(0).Rows > 0 Then
     grdDBGrid1(0).Columns(1).Width = 1000 'N�Pedido
     grdDBGrid1(0).Columns(2).Width = 1400 'FechaPedido
     grdDBGrid1(0).Columns(3).Width = 1000 'c�d.prov
     grdDBGrid1(0).Columns(4).Width = 2600 'proveedor
     grdDBGrid1(0).Columns(5).Width = 0 'c�d.estado
     grdDBGrid1(0).Columns(6).Width = 1200 'estado
     grdDBGrid1(0).Columns(7).Width = 3100 'descripci�n
End If
If grdDBGrid1(1).Rows > 0 Then
    grdDBGrid1(1).Columns("C�d.Prod").Width = 0
    'grdDBGrid1(1).Columns("C�d.Interno").Width =
    'grdDBGrid1(1).Columns("Seg").Width =
    grdDBGrid1(1).Columns("Descripci�n Producto").Width = 3000
    'grdDBGrid1(1).Columns("F.F").Width =
    'grdDBGrid1(1).Columns("Dosis").Width =
    'grdDBGrid1(1).Columns("U.M").Width =
    'grdDBGrid1(1).Columns("Volumen").Width =
    'grdDBGrid1(1).Columns("Referencia").Width = 1200
    grdDBGrid1(1).Columns("Precio Unidad").Width = 1100
    grdDBGrid1(1).Columns("Importe").Width = 1300
    grdDBGrid1(1).Columns("Moneda").Width = 800
    grdDBGrid1(1).Columns("Cant Pedida").Width = 1100
    'grdDBGrid1(1).Columns("Cant Recibida").Width = 1220
    grdDBGrid1(1).Columns("Cant Acum").Width = 1100
    grdDBGrid1(1).Columns("Prec.Neto").Width = 1100
    
End If
calcular_precio_neto
If txtText1(2).Text <> "" Then
   If (txtText1(15).Text <> "") And (txtText1(26).Text <> "") Then
    If IsNumeric(txtText1(29).Text) And (txtText1(29).Text <> "") Then
      intcantenvase = txtText1(29).Text
    Else
      intcantenvase = 1
    End If
     txtbonificacion.Text = Fix(((txtText1(15).Text * txtText1(26).Text) / 100) * intcantenvase)
     Dim strFRJ3 As String
     Dim qryFRJ3 As rdoQuery
     Dim rstFRJ3 As rdoResultset
     If Len(Trim(txtText1(19).Text)) > 0 And Len(Trim(txtText1(21).Text)) > 0 Then
      strFRJ3 = "SELECT SUM(FRJ3PTEBONIF) " & _
                "  FROM FRJ300 " & _
                " WHERE FR62CODPEDCOMPRA = ? " & _
                "   AND FR25CODDETPEDCOMP = ? "
      Set qryFRJ3 = objApp.rdoConnect.CreateQuery("", strFRJ3)
      qryFRJ3(0) = txtText1(19).Text
      qryFRJ3(1) = txtText1(21).Text
      Set rstFRJ3 = qryFRJ3.OpenResultset()
      If Not rstFRJ3.EOF Then
        If IsNull(rstFRJ3.rdoColumns(0).Value) Then
          Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
        Else
          If rstFRJ3.rdoColumns(0).Value = 0 Then
            Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
          End If
        End If
       qryFRJ3.Close
       Set qryFRJ3 = Nothing
       Set rstFRJ3 = Nothing
      End If
     End If
   Else
    txtbonificacion.Text = 0
    'Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
   End If
 Else
  txtbonificacion.Text = 0
  'Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
 End If
  
    'Se ha modificado la cantidad o la bonificaci�n por lo que hay que actualizar la ficha del producto
    If (IsNumeric(txtText1(15).Text)) And (IsNumeric(txtText1(2).Text)) Then
      mdblCantPed = txtText1(15).Text
      mdblCodProd = txtText1(2).Text
      If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
        mdblTamEnva = txtText1(29).Text
      Else
        If (IsNumeric(txtText1(2).Text)) Then
          mdblTamEnva = 1
          mdblCodProd = txtText1(2).Text
        End If
      End If
    End If
    
    If (IsNumeric(txtText1(26).Text)) And (IsNumeric(txtText1(2).Text)) Then
      mdblCantPend = txtText1(26).Text
      mdblCodProd = txtText1(2).Text
      If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
        mdblTamEnva = txtText1(29).Text
      Else
        If (IsNumeric(txtText1(2).Text)) Then
          mdblTamEnva = 1
          mdblCodProd = txtText1(2).Text
        End If
      End If
    End If
    
    If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
      mdblTamEnva = txtText1(29).Text
      mdblCodProd = txtText1(2).Text
    Else
      If (IsNumeric(txtText1(2).Text)) Then
        mdblTamEnva = 1
        mdblCodProd = txtText1(2).Text
      End If
    End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strUpdProd As String
  Dim dblBonifi As Double
  Dim dblEnvase As Double
  Dim dblOLDBonif As Double
  Dim qryUpd As rdoQuery
  Dim blnActMaestro As Boolean
  
  blnActMaestro = False
  If tabTab1(0).Tab = 0 Then
    If txtText1(16).Text = 1 Then
      blnActMaestro = False
    Else
      blnActMaestro = True
    End If
  Else
    If grdDBGrid1(0).Columns(5).Value = 1 Then
      blnActMaestro = False
    Else
      blnActMaestro = True
    End If
  End If
  If mblnActCantPend = True And blnActMaestro Then
    'Hay que actualizar la ficha del producto con las unidades pendientes de entregar
      If (IsNumeric(mdblCantPed)) And (IsNumeric(txtText1(15).Text)) Then
        mblnActCantPed = False
        If IsNumeric(txtText1(29).Text) Then
          dblEnvase = txtText1(29).Text
        Else
          dblEnvase = 1
        End If
        strUpdProd = "UPDATE FR7300 " & _
                     " SET FR73CANTPEND = FR73CANTPEND + ? " & _
                     " WHERE FR73CODPRODUCTO = ?"
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpdProd)
        qryUpd(0) = Fix(objGen.ReplaceStr(((txtText1(15).Text * dblEnvase) - (mdblCantPed * mdblTamEnva)), ",", ".", 1))
        qryUpd(1) = mdblCodProd
        qryUpd.Execute
        qryUpd.Close
        Set qryUpd = Nothing
      End If
      If (IsNumeric(txtText1(26).Text)) And (IsNumeric(mdblCantPend)) And mblnActCantPend Then
        mblnActCantPend = True
        If IsNumeric(txtText1(29).Text) Then
          dblEnvase = txtText1(29).Text
        Else
          dblEnvase = 1
        End If
        dblBonifi = Fix(((txtText1(15).Text * txtText1(26).Text) / 100) * dblEnvase)
        dblOLDBonif = Fix(((mdblCantPed * mdblCantPend) / 100) * mdblTamEnva)
        strUpdProd = "UPDATE FR7300 " & _
                     " SET FR73PTEBONIF = FR73PTEBONIF + ? " & _
                     " WHERE FR73CODPRODUCTO = ? "
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpdProd)
        qryUpd(0) = Fix(objGen.ReplaceStr((dblBonifi - dblOLDBonif), ",", ".", 1))
        qryUpd(1) = mdblCodProd
        qryUpd.Execute
        qryUpd.Close
        Set qryUpd = Nothing
      End If
    End If
  

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Departamentos Realizadores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'Localizar
If (btnButton.Index = 16) Or (btnButton.Index = 21) Or (btnButton.Index = 22) Or (btnButton.Index = 23) Or (btnButton.Index = 24) Then
    Select Case txtText1(16).Text
        Case 0, 5, 7 'cancelado,recibido completo,cerrado
            cmdEmitir.Enabled = False
            cmdReclamar.Enabled = False
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
        Case 1 'generado
            cmdEmitir.Enabled = True
            cmdReclamar.Enabled = False
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
        Case 2, 3, 4 'emitido
            cmdEmitir.Enabled = True
            cmdReclamar.Enabled = True
            cmdCancelar.Enabled = True
            cmdintrecepcion.Enabled = True
        'Case 3 'reclamado,recibido parcial
        '    cmdEmitir.Enabled = False
        '    cmdReclamar.Enabled = False
        '    cmdCancelar.Enabled = True
        '    cmdintrecepcion.Enabled = False
        Case 6 'cerrado parcialmente completo
            cmdEmitir.Enabled = False
            cmdReclamar.Enabled = True
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
    End Select
    
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
'Localizar
If (intIndex = 10) Or (intIndex = 40) Or (intIndex = 50) Or (intIndex = 60) Or (intIndex = 70) Then
    Select Case txtText1(16).Text
        Case 0, 5, 7 'cancelado,recibido completo,cerrado
            cmdEmitir.Enabled = False
            cmdReclamar.Enabled = False
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
        Case 1 'generado
            cmdEmitir.Enabled = True
            cmdReclamar.Enabled = False
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
        Case 2, 3, 4 'emitido
            cmdEmitir.Enabled = True
            cmdReclamar.Enabled = True
            cmdCancelar.Enabled = True
            cmdintrecepcion.Enabled = True
        'Case 3 'reclamado
        '    cmdEmitir.Enabled = False
        '    cmdReclamar.Enabled = False
        '    cmdCancelar.Enabled = True
        '    cmdintrecepcion.Enabled = False
        Case 6  'recibido parcial,cerrado parcialmente completo
            cmdEmitir.Enabled = False
            cmdReclamar.Enabled = True
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
    End Select
End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub





Private Sub txtbuscar_GotFocus()
    Me.KeyPreview = False
End Sub

Private Sub txtbuscar_LostFocus()
    Me.KeyPreview = True
End Sub

Private Sub txtpersona_GotFocus()
    Me.KeyPreview = False
End Sub

Private Sub txtpersona_LostFocus()
    Me.KeyPreview = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim intcantenvase As Integer
Dim curImporte As Currency
Dim intResp As Integer
    
  If (intIndex = 15) Or (intIndex = 26) Or (intIndex = 29) Then
    'Se ha modificado la cantidad o la bonificaci�n por lo que hay que actualizar la ficha del producto
    If (IsNumeric(txtText1(15).Text)) And (IsNumeric(txtText1(2).Text)) Then
      mblnActCantPed = True
    End If
    If (IsNumeric(txtText1(26).Text)) And (IsNumeric(txtText1(2).Text)) Then
      mblnActCantPend = True
    End If
    If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
      mblnTamEnva = True
    End If
  End If
  
  Call objWinInfo.CtrlDataChange
  
'cuando se introduce la cantidad de producto a pedir, se calcula la bonificaci�n
' If intIndex = 15 And txtText1(2).Text <> "" Then
'   If txtText1(15).Text <> "" Then
'     sqlstr = "SELECT FR25BONIF FROM FR2500 WHERE FR73CODPRODUCTO=" & txtText1(2).Text
'     Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'     If Not rsta.EOF Then
'      If Not IsNull(rsta.rdoColumns(0).Value) Then
'           txtbonificacion.Text = (txtText1(15).Text * rsta.rdoColumns(0).Value) / 100
'      Else
'           txtbonificacion.Text = 0
'      End If
'     End If
'     rsta.Close
'     Set rsta = Nothing
'   End If
' End If
'  If (intIndex = 15) Or (intIndex = 26) Then
'    'Se ha modificado la cantidad o la bonificaci�n por lo que hay que actualizar la ficha del producto
'    mblnActualizar = True
'  End If
  If intIndex = 15 Or intIndex = 28 Then
    'Se calcula el importe
    If IsNumeric(txtText1(15).Text) And IsNumeric(txtText1(28).Text) Then
      curImporte = 0
      curImporte = txtText1(15).Text * txtText1(28).Text
      If curImporte > 100000000 Then
      '  intresp = MsgBox("Revise el pedido ya que el importe es superior a cien millones", vbInformation, "Informaci�n")
      Else
        txtText1(17).Text = Format(curImporte, "0.###")
      End If
    End If
  End If
If (intIndex = 15) Or (intIndex = 26) And txtText1(2).Text <> "" Then
   If (txtText1(15).Text <> "") And (txtText1(26).Text <> "") Then
    If IsNumeric(txtText1(29).Text) And (txtText1(29).Text <> "") Then
      intcantenvase = txtText1(29).Text
    Else
      intcantenvase = 1
    End If
     txtbonificacion.Text = Fix(((txtText1(15).Text * txtText1(26).Text) / 100) * intcantenvase)
     Dim strFRJ3 As String
     Dim qryFRJ3 As rdoQuery
     Dim rstFRJ3 As rdoResultset
     If Len(Trim(txtText1(19).Text)) > 0 And Len(Trim(txtText1(21).Text)) > 0 Then
      strFRJ3 = "SELECT SUM(FRJ3PTEBONIF) " & _
                "  FROM FRJ300 " & _
                " WHERE FR62CODPEDCOMPRA = ? " & _
                "   AND FR25CODDETPEDCOMP = ? "
      Set qryFRJ3 = objApp.rdoConnect.CreateQuery("", strFRJ3)
      qryFRJ3(0) = txtText1(19).Text
      qryFRJ3(1) = txtText1(21).Text
      Set rstFRJ3 = qryFRJ3.OpenResultset()
      If Not rstFRJ3.EOF Then
        If IsNull(rstFRJ3.rdoColumns(0).Value) Then
          Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
        Else
          If rstFRJ3.rdoColumns(0).Value = 0 Then
            Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
          End If
        End If
       qryFRJ3.Close
       Set qryFRJ3 = Nothing
       Set rstFRJ3 = Nothing
      End If
     End If
   Else
    txtbonificacion.Text = 0
    'Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
   End If
 Else
  txtbonificacion.Text = 0
  'Call objWinInfo.CtrlSet(txtText1(1), txtbonificacion.Text)
 End If
  
 If (intIndex = 4) Or (intIndex = 25) Or (intIndex = 27) Then 'Se ha cambiado el precio por unidad, el descuento o el iva
  calcular_precio_neto
 End If
 'estado del pedido
 If intIndex = 16 Then
    Select Case txtText1(16).Text
        Case 0, 5, 7 'cancelado,recibido completo,cerrado
            cmdEmitir.Enabled = False
            cmdReclamar.Enabled = False
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
        Case 1 'generado
            cmdEmitir.Enabled = True
            cmdReclamar.Enabled = False
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
        Case 2, 3, 4 'emitido
            cmdEmitir.Enabled = True
            cmdReclamar.Enabled = True
            cmdCancelar.Enabled = True
            cmdintrecepcion.Enabled = True
        'Case 3 'reclamado
        '    cmdEmitir.Enabled = False
        '    cmdReclamar.Enabled = False
        '    cmdCancelar.Enabled = True
        '    cmdintrecepcion.Enabled = False
        Case 6  'recibido parcial,cerrado parcialmente completo
            cmdEmitir.Enabled = False
            cmdReclamar.Enabled = True
            cmdCancelar.Enabled = False
            cmdintrecepcion.Enabled = False
    End Select
 
 End If
 If intIndex = 1 Then
  txtbonificacion.Text = txtText1(1).Text
 End If
 
End Sub








